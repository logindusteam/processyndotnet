// CleLogicielle.cpp: implementation of the CCleLogicielle class.
//
//////////////////////////////////////////////////////////////////////

#include "Stdafx.h"
#include "FileTime.h"
#include "CleRegistre.h"
#include "CleLogicielle.h"

//Valeur de la cl� de cryptage sur la base de registre
#define CRYPT_32 0x7ae439fb // d�cimal 2061777403

//Path dans la base de registres pour la signature de machine (dans HKEY_LOCAL_MACHINE\...)
static PCSTR SZ_PATH_SIGNATURE = "Software\\SR Class";
//Nom de la cl� contenant la valeur de la signature
static PCSTR SZ_CLE_SIGNATURE  = "Initial Build";
//Path dans la base de registres pour l'application (dans HKEY_LOCAL_MACHINE\...)
static PCSTR STR_PATH_RACINE_INFO_LICENCE = "SOFTWARE\\Logique Industrie\\";
//Nom de la cl� contenant la cl� de validation de la licence
static PCSTR STR_NAME_KEY_CLE    = "SoftKey 0";
//Nom de la cl� contenant le premier param�tre de licence
static PCSTR STR_NAME_KEY_PARAM1 = "SoftKey 1";
//Nom de la cl� contenant le deuxi�me param�tre de licence
static PCSTR STR_NAME_KEY_PARAM2 = "SoftKey 2";
//Nom de la cl� contenant la date limite
static PCSTR STR_NAME_KEY_DATE   = "SoftKey 3";


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCleLogicielle::CCleLogicielle()
	{
	m_bProtectionActive = TRUE;

	m_InfoProduit.dwNProduit = 0;
	m_InfoProduit.dwNFonctionProduit = 0;
	m_InfoProduit.dwVersionProtectionMajor = 0;
	m_InfoProduit.dwVersionProtectionMinor = 0;

  m_bCleLicenceValide = FALSE;
	m_dwNumeroDeSerie = 0;
	m_szPathInfoLicence[0] = 0;
	ResetLicence();
	}

CCleLogicielle::~CCleLogicielle()
	{
  m_bCleLicenceValide = FALSE;
	}

//-----------------------------------------------------------------------------------------
// Met toutes les infos de licence � leur valeur par d�faut (0 ou illimit�)
void CCleLogicielle::ResetLicence()
	{
	m_Licence.dwCle = 0;
	m_Licence.dwParam1 = 0;
	m_Licence.dwParam2 = 0;
	m_Licence.stDateLimite.wMilliseconds = 0;
	m_Licence.stDateLimite.wSecond = 0;
	m_Licence.stDateLimite.wMinute = 0;
	m_Licence.stDateLimite.wHour = 0;
	m_Licence.stDateLimite.wDayOfWeek = 0;
	m_Licence.stDateLimite.wDay = 0;
	m_Licence.stDateLimite.wMonth = 0;
	m_Licence.stDateLimite.wYear = 0;
	}

//-----------------------------------------------------------------------------------------
BOOL CCleLogicielle::bSignatureMachineEnregistree (DWORD &dwSignature) // TRUE si Ok sinon GetLastError pour code d'erreur
  {
  BOOL bRes = FALSE;
	
	CCleRegistre CleRegistre;

  // Lecture d'une signature machine d�ja enregistr�e ?
  if (CleRegistre.bRegOuvreEnLecture(HKEY_LOCAL_MACHINE, SZ_PATH_SIGNATURE) && 
		CleRegistre.bGetDWordValue(SZ_CLE_SIGNATURE, dwSignature, CRYPT_32))
    // oui => OK
    bRes = TRUE;
  else
    {
    // non => G�n�ration d'une signature machine (� partir d'un GUID) Ok ?
		GUID  Guid;
		if (S_OK == CoCreateGuid(&Guid))
			{
			// oui => signature = xor des 4 DWORD composant le GUID
			PDWORD  pdw = (PDWORD)&Guid;
			dwSignature = (*pdw) ^ (*(pdw+1)) ^ (*(pdw+2)) ^ (*(pdw+3));

			// Enregistrement de la signature Ok ?
			if (CleRegistre.bRegOuvreOuCreeChemin(HKEY_LOCAL_MACHINE, SZ_PATH_SIGNATURE, TRUE) &&
				CleRegistre.bLetDWordValue(SZ_CLE_SIGNATURE, dwSignature, CRYPT_32))
				{
				// oui => OK
				bRes = TRUE;
				}
			}
    }

  return bRes;
  }

//-----------------------------------------------------------------------------------------
// Renvoie un Num�ro caract�ristique de la machine (en fait du formatage de son disque dur)
BOOL CCleLogicielle::bGetIdMachine
  (
  DWORD &dwIdMachine    // Adresse de l'Id Machine Lu
  )                      // VARIANT_TRUE si Ok sinon GetLastError pour code d'erreur
  {
  BOOL bRes = FALSE;

  // Lecture des caract�ristiques du disque C: et 
  // r�cuperation de la signature machine enregistr�e Ok ?
  char pszNomVolume [MAX_PATH];
  char pszFileSystemNameBuffer [MAX_PATH];
  DWORD dwVolumeSerialNumber = 0;
  DWORD dwMaximumComponentLength = 0;
  DWORD dwFileSystemFlags = 0;
  DWORD dwSignatureMachine =0;

  if (GetVolumeInformation(
    "C:\\",  // address of root directory of the file system 
    pszNomVolume,  // address of name of the volume 
    MAX_PATH,  // length of lpVolumeNameBuffer 
    &dwVolumeSerialNumber,  // address of volume serial number 
    &dwMaximumComponentLength,  // address of system's maximum filename length
    &dwFileSystemFlags,  // address of file system flags 
    pszFileSystemNameBuffer,  // address of name of file system 
    MAX_PATH  // length of lpFileSystemNameBuffer 
   ) && bSignatureMachineEnregistree(dwSignatureMachine)
   )
    {
    // oui => Cryptage inviolable : rend un Id 32 bits � partir de l'Id Disque et de la signature machine enregistr�e
    dwVolumeSerialNumber ^= ((dwVolumeSerialNumber+CRYPT_32) << 23) ^ ((dwVolumeSerialNumber+CRYPT_32) << 14);
    dwVolumeSerialNumber ^= dwSignatureMachine;
    dwVolumeSerialNumber &= 0xffffefff;
    dwIdMachine = dwVolumeSerialNumber + 23;
    bRes = TRUE;
    }

  return bRes;
  }

//-----------------------------------------------------------------------------------
//Encodage sp�cifique de dates en long : AN = b31..16 ; MOIS = b15..b8; JOUR = b7..b0
//-----------------------------------------------------------------------------------
BOOL CCleLogicielle::bLongToDate(DWORD dwDate, SYSTEMTIME & stDate)
	{
  BOOL bOk = FALSE;
  if (dwDate == 0)
		{
    stDate.wYear = 0; // Signifie PROTECTION_PAS_DE_LIMITE;
    bOk = TRUE;
		}
  else
		{
    stDate.wYear = (WORD) (1997 + (dwDate / (32 * 13)));
    stDate.wMonth = (WORD)((dwDate / 32) % 13);
    stDate.wDay = (WORD)(dwDate % 32);
    stDate.wMilliseconds = 0;
    stDate.wSecond = 0;
    stDate.wMinute = 0;
    stDate.wHour = 0;
    bOk = TRUE; // $$ valider la date
		}
  return bOk;
	}

//--------------------------------------------------------------------------------------------
BOOL CCleLogicielle::bDateToLong(SYSTEMTIME stDate, DWORD & dwDate)
	{
  BOOL bOk = FALSE;

  if (stDate.wYear == 0) // signifie PROTECTION_PAS_DE_LIMITE;
		{
    bOk = TRUE;
    dwDate = 0;
		}
  else
		{
    DWORD dwAn = stDate.wYear;
    if ((dwAn >= 1997) && (dwAn <= 2090)) 
			{
      dwDate = ((dwAn - 1997) * 32 * 13) + (stDate.wMonth * 32) + (stDate.wDay);
			bOk = TRUE;
			}
		}
	return bOk;
	}

//--------------------------------------------------------------------------------------------
// Note les caract�ristiques du produit dont on veut g�rer la cl� logicielle
void CCleLogicielle::SetProduit(INFO_PRODUIT InfoProduit, PCSTR szNomSousCleAppli)
	{
	// Note les infos produit
	m_InfoProduit = InfoProduit;
	
	// cr�e le path dans la base de registre contenant la licence du produit
	if ((szNomSousCleAppli == NULL) || (strlen(szNomSousCleAppli) == 0))
		{
		wsprintf(m_szPathInfoLicence, "%sStatus %lu.%lu.%lu.%lu", STR_PATH_RACINE_INFO_LICENCE,InfoProduit.dwNProduit,InfoProduit.dwNFonctionProduit,
			InfoProduit.dwVersionProtectionMajor,InfoProduit.dwVersionProtectionMinor);
		}
	else
		{
		wsprintf(m_szPathInfoLicence, "%s%s", STR_PATH_RACINE_INFO_LICENCE, szNomSousCleAppli);
		}
	}

//--------------------------------------------------------------------------------------------
// R�cup�re les infos de licence du produit et v�rifie si la Cl� Licence est valide
// Renvoie TRUE si la lecture a �t� possible
BOOL CCleLogicielle::bLectureEtVerificationLicence ()
	{
	BOOL bOk = FALSE;

	CCleRegistre CleRegistre;

  // Lecture d'une signature machine d�ja enregistr�e ?
	if (bGetIdMachine(m_dwNumeroDeSerie))
		{
		// oui => ouverture de la bonne cl� dans la base de registre ?
		if (CleRegistre.bRegOuvreEnLecture(HKEY_LOCAL_MACHINE, m_szPathInfoLicence))
			{
			// oui => lecture des bons param�tres ?
			if (CleRegistre.bGetDWordValue(STR_NAME_KEY_CLE, m_Licence.dwCle, CRYPT_32)) 
				{
				if (CleRegistre.bGetDWordValue(STR_NAME_KEY_PARAM1, m_Licence.dwParam1, CRYPT_32)) 
					{
					if (CleRegistre.bGetDWordValue(STR_NAME_KEY_PARAM2, m_Licence.dwParam2, CRYPT_32)) 
						{
						DWORD dwDateLimite = 0;
						if (CleRegistre.bGetDWordValue(STR_NAME_KEY_DATE, dwDateLimite, CRYPT_32))
							{
							if (bLongToDate(dwDateLimite, m_Licence.stDateLimite))
								{
								VerifieLicenceValide();
								bOk = TRUE;
								}
							}
						}
					}
				}
			}
		}
	return bOk;
	}

//------------------------------------------------------------------------------------------------------------------------
// Lecture / �criture de tous les donn�es de la licence dans la base de registre
//------------------------------------------------------------------------------------------------------------------------
BOOL CCleLogicielle::bEnregistreLicence()
	{
	BOOL bOk = FALSE;
	CCleRegistre CleRegistre;
  // Enregistre N� Licence et param�tres sur la base de registre
	DWORD dwDateLimite = 0;
  if (bDateToLong(m_Licence.stDateLimite, dwDateLimite))
		{
		if (CleRegistre.bRegOuvreOuCreeChemin(HKEY_LOCAL_MACHINE, m_szPathInfoLicence, TRUE))
			{
			if (CleRegistre.bLetDWordValue(STR_NAME_KEY_CLE, m_Licence.dwCle, CRYPT_32))
				{
				if (CleRegistre.bLetDWordValue(STR_NAME_KEY_PARAM1, m_Licence.dwParam1, CRYPT_32))
					{
					if (CleRegistre.bLetDWordValue(STR_NAME_KEY_PARAM2, m_Licence.dwParam2, CRYPT_32))
						{
						if (CleRegistre.bLetDWordValue(STR_NAME_KEY_DATE, dwDateLimite, CRYPT_32))
							{
							bOk = TRUE;
							}
						}
					}
				}
			}
		}
 
  //Attends un peu pour �viter l'automatisation de cette fonction
  Sleep (800); //Millisecondes
	return bOk;
	}

//------------------------------------------------------------------------------------------------------------------------
// V�rification de la cl� logicielle (met � jour mvarbLicenceValide)
//------------------------------------------------------------------------------------------------------------------------
void CCleLogicielle::VerifieLicenceValide()
	{
  DWORD dwSoftwareVersion = (100 * m_InfoProduit.dwVersionProtectionMajor) + (10 * m_InfoProduit.dwVersionProtectionMinor);
  DWORD dwDateLimite;
  if (bDateToLong(m_Licence.stDateLimite, dwDateLimite))
		{
    m_bCleLicenceValide = (m_Licence.dwCle == dwGetRandOSWIN32 (m_dwNumeroDeSerie, m_InfoProduit.dwNProduit, 
			m_InfoProduit.dwNFonctionProduit, dwSoftwareVersion, m_Licence.dwParam1, m_Licence.dwParam2, dwDateLimite, 12, 0, 12, 7));
		}
  else
    m_bCleLicenceValide = FALSE;
	}


//------------------------------------------------------------------------------------------------------------------------
// Renvoie la valeur d'une fonction pseudo-al�atoire fonction de n variables (1<=n<=10)
//------------------------------------------------------------------------------------------------------------------------
DWORD CCleLogicielle::dwGetRandOSWIN32
	(
	DWORD dwNb1, DWORD dwNb2, DWORD dwNb3,
	DWORD dwNb4, DWORD dwNb5, DWORD dwNb6,
	DWORD dwNb7, DWORD dwNb8, DWORD dwNb9,
	DWORD dwNb10, DWORD dwNbParam
	)
	{
  DWORD dwRes =  0;
	
	if (dwNbParam >= 1)
		{
		dwRes = dwNb1  * (DWORD)3880178699;
		if (dwNbParam >= 2)
			{
			dwRes ^= dwNb2 * (DWORD)  1772906939;
			if (dwNbParam >= 3)
				{
				dwRes ^= dwNb3 * (DWORD) 1974410527;
				dwRes ^= dwNb1 << 23;
				if (dwNbParam >= 4)
					{
					dwRes ^= dwNb4 * (DWORD) 273296411;
					dwRes ^= dwNb1 >> 15;
					if (dwNbParam >= 5)
						{
						dwRes ^= dwNb5 * (DWORD)  268555801;
						if (dwNbParam >= 6)
							{
							dwRes ^= dwNb6 * (DWORD)   1418111953;
							if (dwNbParam >= 7)
								{
								dwRes ^= dwNb7 * (DWORD)    2511021581;
								if (dwNbParam >= 8)
									{
									dwRes ^= dwNb8 * (DWORD)   4045657261;
									if (dwNbParam >= 9)
										{
										dwRes ^=  dwNb9 * (DWORD)   2023013381;
										if (dwNbParam >= 10)
											{
											dwRes ^= dwNb10 * (DWORD)   2966460019;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
  return dwRes;
	}

//---------------------------------------------------
// Renvoie TRUE si la date sp�cifi�e est compatible avec la date limite
BOOL CCleLogicielle::bDateExecutionValide (SYSTEMTIME stDate)
	{
	BOOL bRes = FALSE;
  // Protection active et date de fonctionnement limit�e ?
  if (m_bProtectionActive && (m_Licence.stDateLimite.wYear != 0))
		{
		// oui =>
		CFileTime ftTemp;
		CFileTime ftLimite;
		if ((ftTemp.bSetLocal(stDate)) && (ftLimite.bSetLocal(m_Licence.stDateLimite)))
			{
			ftLimite.AddJours(1);
			bRes = ftTemp.ullGetLocal() < ftLimite.ullGetLocal();
			}
		}
  else
    bRes = TRUE;
	return bRes;
	}


//---------------------------------------------------
// Renvoie TRUE si la licence est valide et que la date sp�cifi�e est compatible avec la date limite
BOOL CCleLogicielle::bExecutionAutorisee()
	{
	CFileTime ftNow;
	ftNow.SetLocalNow();
  return m_bCleLicenceValide && bDateExecutionValide(ftNow.stGetLocal());
	}


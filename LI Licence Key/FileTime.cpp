// FileTime.cpp: implementation of the CFileTime class.
//INFO: Working with the FILETIME Structure
//ID: Q188768 
//
//////////////////////////////////////////////////////////////////////

#include "Stdafx.h"
#include "FileTime.h"
// A file time represents the specific date and time at which a given file was created, last accessed, or last written to. A file time is stored in a FILETIME structure. This structure is used with various Win32 API calls. 
// The FILETIME structure represents the number of 100-nanosecond intervals since January 1, 1601. The structure consists of two 32-bit values that combine to form a single 64-bit value. 
// Note that the FILETIME structure is based on 100-nanosecond intervals. 
// It is helpful to define the following symbols when working with file times. For example: 

#define _SECOND ((__int64) 10000000)
#define _MINUTE (60 * _SECOND)
#define _HOUR   (60 * _MINUTE)
#define _DAY    (24 * _HOUR) 

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CFileTime::CFileTime()
	{
	m_ft.dwHighDateTime = 0;
	m_ft.dwLowDateTime = 0;
	m_bLocalTime = FALSE;
	}

CFileTime::~CFileTime()
	{
	
	}

BOOL CFileTime::bSetLocal(SYSTEMTIME st)
	{
	BOOL bRes = FALSE;
	if (SystemTimeToFileTime(&st, &m_ft))
		{
		m_bLocalTime = TRUE;
		bRes = TRUE;
		}
	return bRes;
	}

BOOL CFileTime::bSetSystem(SYSTEMTIME st)
	{
	BOOL bRes = FALSE;
	if (SystemTimeToFileTime(&st, &m_ft))
		{
		m_bLocalTime = FALSE;
		bRes = TRUE;
		}
	return bRes;
	}

void CFileTime::SetLocal(FILETIME ft)
	{
	m_ft = ft;
	m_bLocalTime = TRUE;
	}

void CFileTime::SetSystem(FILETIME ft)
	{
	m_ft = ft;
	m_bLocalTime = FALSE;
	}

void CFileTime::SetLocalNow()
	{
	GetSystemTimeAsFileTime(&m_ft);
	FileTimeToLocalFileTime(&m_ft, &m_ft);
	m_bLocalTime = TRUE;
	}

void CFileTime::SetUTCNow()
	{
	GetSystemTimeAsFileTime(&m_ft);
	m_bLocalTime = FALSE;
	}


FILETIME CFileTime::GetLocal()
	{
	FILETIME ftRes = m_ft;
	if (!m_bLocalTime)
		{
		FileTimeToLocalFileTime (&ftRes, &ftRes);
		}
	return ftRes;
	}

FILETIME CFileTime::GetUTC()
	{
	FILETIME ftRes = m_ft;
	if (m_bLocalTime)
		{
		LocalFileTimeToFileTime (&ftRes, &ftRes);
		}
	return ftRes;
	}

ULONGLONG CFileTime::ullGetLocal()
	{
	FILETIME ftRes = m_ft;
	if (!m_bLocalTime)
		{
		FileTimeToLocalFileTime (&ftRes, &ftRes);
		}
	return (((ULONGLONG) ftRes.dwHighDateTime) << 32) + ftRes.dwLowDateTime;
	}

ULONGLONG CFileTime::ullGetUTC()
	{
	FILETIME ftRes = m_ft;
	if (m_bLocalTime)
		{
		LocalFileTimeToFileTime (&ftRes, &ftRes);
		}
	return (((ULONGLONG) ftRes.dwHighDateTime) << 32) + ftRes.dwLowDateTime;
	}

SYSTEMTIME CFileTime::stGetLocal()
	{
	SYSTEMTIME stRes;
	FileTimeToSystemTime(&GetLocal(), &stRes);
	return stRes;
	}

SYSTEMTIME CFileTime::stGetUTC()
	{
	SYSTEMTIME stRes;
	FileTimeToSystemTime(&GetUTC(), &stRes);
	return stRes;
	}

// Assuming ft is a FILETIME structure containing the creation time of a file, the following sample code adds 30 days to the time: 
void CFileTime::AddJours(LONG lNbJours)
	{
	ULONGLONG qwResult;
	
	// Copie l'heure en ULONGLONG.
	qwResult = (((ULONGLONG) m_ft.dwHighDateTime) << 32) + m_ft.dwLowDateTime;
	
	// Ajoute les jours
	qwResult += lNbJours * _DAY;
	
	// Retransfère les jours.
	m_ft.dwLowDateTime  = (DWORD) (qwResult & 0xFFFFFFFF );
	m_ft.dwHighDateTime = (DWORD) (qwResult >> 32 ); 
	}

/*
Setting File Times
You can set the file times for a file by using the SetFileTime() function. 

   BOOL SetFileTime(
     HANDLE hFile,                     // Handle to the file.
     CONST FILETIME *lpCreationTime,   // Time the file was created.
     CONST FILETIME *lpLastAccessTime, // Time the file was last accessed.
     CONST FILETIME *lpLastWriteTime   // Time the file was last
                                       // written to.
   ); 
This function lets you modify creation, last access, and last write times without changing the content of the file. To use this function, you must have a handle to the open file. This file handle can be obtained from a call to CreateFile() or OpenFile(). The file must be opened with GENERIC_WRITE access. After the file times have been set, you should release the file handle through a call to CloseHandle(). 

Assuming szFilename is a valid filename and ft is a FILETIME structure, the following sample code sets the creation date for the file to the time contained in ft: 

   BOOL bResult;
   HANDLE hFile = CreateFile( szFilename,
      GENERIC_WRITE, // The file must be opened with write access.
      FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL );

   if (hFile != INVALID_HANDLE_VALUE) {
      bResult = SetFileTime( hFile, &ft, NULL, NULL );
      CloseHandle(hFile);
   } 
Displaying File Times
The file time is based on coordinated universal time (UTC). UTC-based time is loosely defined as the current date and time of day in Greenwich, England. You will most likely want to display the file time with respect to the local time (that is, the date and time of day for your time zone). To do this, you can use FileTimeToLocalFileTime() as follows: 

   BOOL FileTimeToLocalFileTime(
     CONST FILETIME *lpFileTime,  // Pointer to UTC file time to convert.
     LPFILETIME lpLocalFileTime   // Pointer to converted file time.
   ); 
Note that this function uses the current settings for the time zone and daylight saving time. Therefore, if it is daylight savings time, this function will take daylight savings time into account, even if the time you are converting is in standard time. 

To display the file time in a meaningful way, you first need to convert it to a system time using FileTimeToSystemTime() as follows: 

   BOOL FileTimeToSystemTime(
     CONST FILETIME *lpFileTime, // Pointer to file time to convert.
     LPSYSTEMTIME lpSystemTime   // Pointer to structure to receive
   );                            // system time. 
The SYSTEMTIME structure represents a date and time using individual members for the month, day, year, weekday, hour, minute, second, and millisecond. 

It is also preferable to display the date and time in a format consistent with the current locale selected for the system. You can do this by using GetDateFormat() and GetTimeFormat() as follows: 

   int GetDateFormat(
     LCID Locale,              // Locale for which date is to be formatted.
     DWORD dwFlags,            // Flags specifying function options.
     CONST SYSTEMTIME *lpDate, // Date to be formatted.
     LPCTSTR lpFormat,         // Date format string.
     LPTSTR lpDateStr,         // Buffer for storing formatted string.
     int cchDate               // Size of buffer.
   );

   int GetTimeFormat(
     LCID Locale,              // Locale for which time is to be formatted.
     DWORD dwFlags,            // Flags specifying function options.
     CONST SYSTEMTIME *lpTime, // Time to be formatted.
     LPCTSTR lpFormat,         // Time format string.
     LPTSTR lpTimeStr,         // Buffer for storing formatted string.
     int cchTime               // Size of buffer.
   ); 
By passing LOCALE_USER_DEFAULT as the first parameter to these functions, you tell them to format the passed date/time according to the default format for the current locale. In this case, you can pass NULL for the lpFormat parameters. 

Assuming ft is a FILETIME structure containing a UTC value, the following sample code prints the date stored in ft: 

   SYSTEMTIME st;
   char szLocalDate[255], szLocalTime[255];

   FileTimeToLocalFileTime( &ft, &ft );
   FileTimeToSystemTime( &ft, &st );
   GetDateFormat( LOCALE_USER_DEFAULT, DATE_LONGDATE, &st, NULL,
     szLocalDate, 255 );
   GetTimeFormat( LOCALE_USER_DEFAULT, 0, &st, NULL, szLocalTime, 255 );
   printf( "%s %s\n", szLocalDate, szLocalTime ); 
*/

// CleLogicielle.h: interface for the CCleLogicielle class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLELOGICIELLE_H__EE48D164_3E68_4623_A236_F75C15429A80__INCLUDED_)
#define AFX_CLELOGICIELLE_H__EE48D164_3E68_4623_A236_F75C15429A80__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCleLogicielle  
	{
	public:
		CCleLogicielle();
		virtual ~CCleLogicielle();

		typedef struct
			{
			DWORD dwNProduit;
			DWORD dwNFonctionProduit;
			DWORD dwVersionProtectionMajor;
			DWORD dwVersionProtectionMinor;
			} INFO_PRODUIT;

		typedef struct
			{
			DWORD dwCle;		// Num�ro de licence fourni par LI
			DWORD dwParam1;	// Param�tre de licence 1
			DWORD dwParam2;	// Param�tre de licence 2
			SYSTEMTIME stDateLimite;// Date limite de fonctionnement: illimit� si .wYear = 0 !
			} INFO_LICENCE;

		// --------------------- m�thodes ----------------------------
		// Note les caract�ristiques du produit dont on veut g�rer la cl� logicielle
		// dans m_InfoProduit et m_szPathInfoLicence
		void SetProduit(INFO_PRODUIT InfoProduit, PCSTR szNomSousCleAppli = NULL);

		// R�cup�re les infos de licence du produit sp�cifi� grace � SetProduit et v�rifie si la Cl� Licence est valide
		// Met � jour m_dwNumeroDeSerie, m_Licence et m_bCleLicenceValide
		// Renvoie TRUE si la lecture a �t� possible - m�me si la cl� est invalide
		BOOL bLectureEtVerificationLicence();

		// Met toutes les infos de licence � leur valeur par d�faut (0 ou illimit�)
		void ResetLicence();

		// Lecture / �criture de tous les donn�es de la licence dans la base de registre
		BOOL bEnregistreLicence();

		// Renvoie TRUE si la date sp�cifi�e est compatible avec la date limite
		BOOL bDateExecutionValide (SYSTEMTIME stDate);

		// Renvoie TRUE si la licence est valide et que la date sp�cifi�e est compatible avec la date limite
		BOOL bExecutionAutorisee();

	private:
		// Renvoie un Num�ro caract�ristique de la machine (en fait du formatage de son disque dur)
		BOOL bGetIdMachine (DWORD &dwIdMachine); // TRUE si Ok sinon GetLastError pour code d'erreur
		BOOL bSignatureMachineEnregistree (DWORD &dwSignature); // TRUE si Ok sinon GetLastError pour code d'erreur
		BOOL bLongToDate(DWORD dwDate, SYSTEMTIME & stDate);
		BOOL bDateToLong(SYSTEMTIME stDate, DWORD & dwDate);
		void VerifieLicenceValide();
		// Renvoie la valeur d'une fonction pseudo-al�atoire fonction de n variables (1<=n<=10)
		DWORD dwGetRandOSWIN32(
			DWORD dwNb1, DWORD dwNb2, DWORD dwNb3, DWORD dwNb4, DWORD dwNb5, DWORD dwNb6,
			DWORD dwNb7, DWORD dwNb8, DWORD dwNb9, DWORD dwNb10, DWORD dwNbParam);

		// ------- propri�t�s ---------------
	public:
		// caract�ristiques du produit prot�g�
		INFO_PRODUIT m_InfoProduit;
		// caract�ristiques de la licence
		INFO_LICENCE m_Licence;

		DWORD m_dwNumeroDeSerie;	// Num�ro de s�rie de la machine
		BOOL m_bCleLicenceValide;		// Licence valide (MAJ par bLectureEtVerificationLicence)

	private:
		CHAR m_szPathInfoLicence [MAX_PATH];
		BOOL m_bProtectionActive;
	};

#endif // !defined(AFX_CLELOGICIELLE_H__EE48D164_3E68_4623_A236_F75C15429A80__INCLUDED_)

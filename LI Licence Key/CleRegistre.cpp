// CleRegistre.cpp: implementation of the CCleRegistre class.
//
//////////////////////////////////////////////////////////////////////

#include "Stdafx.h"
#include "CleRegistre.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCleRegistre::CCleRegistre()
	{
	m_hKey = NULL;
	}

CCleRegistre::~CCleRegistre()
	{
	Ferme();
	}

//--------------------------------------------------
void CCleRegistre::Ferme ()
	{
	if (m_hKey != NULL)
		{
    // Fermeture de la cl�
    RegCloseKey (m_hKey);
		m_hKey = NULL;
		}
	}

//--------------------------------------------------
BOOL CCleRegistre::bRegOuvreOuCreeChemin (
	HKEY hKeyParente, 
	PCSTR pszPathKey,
	BOOL bAutoriseCreation)
	{
	Ferme();
  m_hKey = hkeyCreeOuOuvre(hKeyParente,pszPathKey,bAutoriseCreation);
	return m_hKey != NULL;
	}

//--------------------------------------------------
BOOL CCleRegistre::bRegOuvreEnLecture (
	HKEY hKeyParente, 
	PCSTR pszPathKey)
	{
	Ferme();
  RegOpenKeyEx (hKeyParente,  pszPathKey, 0, KEY_QUERY_VALUE, &m_hKey);
	return m_hKey != NULL;
	}

//--------------------------------------------------
HKEY CCleRegistre::hkeyCreeOuOuvre (
	HKEY hKeyParente,
	PCSTR pszPath,
	BOOL bAutoriseCreation)
	{
  HKEY hKeyRet = NULL;
  HKEY hKeyCreee = NULL;

  // Recherche si le path contient plusieurs sous-cl�
  BOOL bCheminComplexe = FALSE;
  for (LONG nNChar = 0; nNChar < lstrlen (pszPath); nNChar++)
    {
    if (pszPath [nNChar] =='\\')
      {
      // Le path contient plusieurs sous-cl�s
      bCheminComplexe = TRUE;
      break;
      }
    }

  // Le path contient plusieurs sous-cl� ?
  DWORD  dwAction = 0;
  if (bCheminComplexe)
    {
    // oui => cr�e ou ouvre une cl� sur la premi�re cl� trouv�e
    char szSousCle [MAX_PATH];
    REGSAM  regsam = (bAutoriseCreation?KEY_CREATE_SUB_KEY:KEY_READ);

    // R�cup�re la sous cl�
    lstrcpy (szSousCle, pszPath);
    szSousCle [nNChar] = '\0';
    // Sous cl� ouverte ou cr��e ?
    if (NO_ERROR == RegCreateKeyEx (hKeyParente, szSousCle, 0, NULL, 
      REG_OPTION_NON_VOLATILE, regsam, NULL, &hKeyCreee, &dwAction))
      {
      // oui => r�entre pour continuer le parcours de l'arborescence
      hKeyRet = hkeyCreeOuOuvre (hKeyCreee, &pszPath[nNChar+1], bAutoriseCreation);
      RegCloseKey (hKeyCreee);
      }
    }
  else
    {
    // non => cr�e ou ouvre la cl� sp�cifi�e
    REGSAM  regsam = (bAutoriseCreation?KEY_SET_VALUE:KEY_QUERY_VALUE);

    if (NO_ERROR == RegCreateKeyEx (hKeyParente, pszPath, 0, NULL, 
      REG_OPTION_NON_VOLATILE, regsam, NULL, &hKeyCreee, &dwAction))
      {
      // oui => Ok, renvoie le handle de cl� ouvert
      hKeyRet = hKeyCreee;
      }
    }

  return hKeyRet;
	}

//--------------------------------------------------
// Crypte et enregistre un DWORD dans la base de registre sur HKEY_LOCAL_MACHINE
BOOL CCleRegistre::bLetDWordValue 
	(
  PCSTR szNomKey,      // Nom de la cl�
  DWORD dwValeur,      // Valeur � affecter � la cl�
  DWORD dwCleCryptage  // Cryptage de la cl� (aucun si 0)
  )                    // TRUE si Ok, FALSE sinon; GetLastError pour code d'erreur
  {
  BOOL bRes = FALSE;

  // Cl� courante charg�e ?
  if (m_hKey != NULL)
    {
    // oui => enregistrement de la valeur de cl� Ok ?
    dwValeur ^= dwCleCryptage;
    if (ERROR_SUCCESS == RegSetValueEx(m_hKey, szNomKey, 0, REG_DWORD, (LPBYTE)&dwValeur, sizeof(dwValeur)))
      {
      // oui => Ok
      bRes = TRUE;
      }
    }
  return bRes;
  }

//-----------------------------------------------------------------------------------------
// Renvoie en clair une valeur DWORD crypt�e stock�e dans la cl� d�sign�e sur le handle courant
BOOL CCleRegistre::bGetDWordValue
  (
  PCSTR szNomKey,      // Nom de la cl�
  DWORD &dwValeur,    // Adresse de la valeur � lire
  DWORD dwCleCryptage  // Cryptage de la cl� (aucun si 0)
  )                    // VARIANT_TRUE si Ok, VARIANT_FALSE sinon ; GetLastError pour code d'erreur
  {
  BOOL bRes = FALSE;

  // Cl� courante charg�e ?
  if (m_hKey != NULL)
    {
    DWORD dwTypeCle = 0;
    DWORD dwTailleData = sizeof(DWORD);

    // Lecture de la valeur de cl� Ok ?
    if ((NO_ERROR == RegQueryValueEx (m_hKey, szNomKey, NULL, &dwTypeCle, (LPBYTE)&dwValeur, &dwTailleData)) &&
      (dwTypeCle == REG_DWORD))
      {
      // oui => r�cup�re la valeur d�crypt�e du DWORD d�crypt�e
      dwValeur ^= dwCleCryptage;
      bRes = TRUE;
      }
    }
 
  return bRes;
  }

//-----------------------------------------------------------------------------------------
// Lit une string dans la base de registre  (en la cryptant si dwCleCryptage <> 0)
// Renvoie FALSE si StringLue n'est pas une string (elle n'a pas besoin d'�tre initialis�e)
BOOL CCleRegistre::bGetStringValue
  (
  PCSTR szNomKey,      // Nom de la cl�
  LPVARIANT pvStringLue,  // Variant auquel on doit passer une string VB
  DWORD dwCleCryptage  // Cryptage de la cl� (aucun si 0)
  )                    // VARIANT_TRUE si Ok, VARIANT_FALSE sinon (StringLue non d�clar�e as String, cl� non trouv�e ou valeur non STRING) Cf. GetLastError
  {
  BOOL bRes = FALSE;

  // Acces � la cl� Ok ?
  if (m_hKey!= NULL)
    {
    #define TAILLE_BUF_CLE 1000
    char szBuf[TAILLE_BUF_CLE];
    WCHAR pBufUnicode[TAILLE_BUF_CLE*2] = L"";
    DWORD dwTypeCle;
    DWORD dwTailleData = TAILLE_BUF_CLE;

    // oui => lecture d'une valeur de cl� Ok ? (Nota la lecture de la cl� est faite en ANSI pour compatibilit� W95)
    if ((NO_ERROR == RegQueryValueEx (m_hKey, szNomKey, NULL, &dwTypeCle, (LPBYTE)szBuf, &dwTailleData)) &&
      (dwTypeCle == REG_SZ))
      {
      // oui => type de variant = string byref ?
      if (pvStringLue->vt == (VT_BSTR | VT_BYREF))
        {
        // oui=> lib�re la string r�sultat courante
        SysFreeString(*pvStringLue->pbstrVal);
        *pvStringLue->pbstrVal = NULL;

        // $$ Ajouter le d�cryptage (dwValeur ^= dwCleCryptage...)

        // r�cup�re la valeur lue en unicode
        MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, // ANSII, laisse les caract�res avec leurs accents
          szBuf,          // address of string to map
          dwTailleData,   // number of bytes in string
          pBufUnicode,    // address of wide-character buffer
          TAILLE_BUF_CLE  // size of buffer
          );

        *pvStringLue->pbstrVal = SysAllocString(pBufUnicode);
        // Ok
        bRes = VARIANT_TRUE;
        }
      else
        SetLastError(ERROR_INVALID_PARAMETER);
      }
    }
 
  return bRes;
  }


//-----------------------------------------------------------------------------------------
// Crypte et enregistre une string dans la base de registre
BOOL CCleRegistre::bLetStringValue
  (
  PCSTR szNomKey,      // Nom de la cl�
  PCSTR pszValeur,    // Valeur � affecter � la cl�
  DWORD dwCleCryptage  // Cryptage de la cl� (aucun si 0)
  )                    // VARIANT_TRUE si Ok, VARIANT_FALSE sinon (erreur cr�ation chemin ou �criture valeur) Cf. GetLastError
  {
  VARIANT_BOOL bRes = VARIANT_FALSE;
  HKEY         hKey = 0;

  // Lecture de la valeur de cl� Ok ?
  if (m_hKey != NULL)
    {
    // Enregistrement de la valeur de cl� Ok ?
    // $$ Ajouter le cryptage (dwValeur ^= dwCleCryptage...)
    if (ERROR_SUCCESS == RegSetValueEx(
      hKey,      // handle of key to query 
      szNomKey,  // address of name of value to query 
      0,        // reserved 
      REG_SZ,    // value type 
      (LPBYTE)pszValeur,  // address of data buffer 
      strlen(pszValeur)+1   // address of data buffer size 
     ))
      {
      // oui => Ok
      bRes = VARIANT_TRUE;
      }

    // Fermeture de la cl�
    RegCloseKey (hKey);
    }
  
  return bRes;
  }


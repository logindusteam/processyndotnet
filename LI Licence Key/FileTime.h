// FileTime.h: interface for the CFileTime class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FILETIME_H__FCC1DA5A_069F_41CA_A335_840D9648C0C2__INCLUDED_)
#define AFX_FILETIME_H__FCC1DA5A_069F_41CA_A335_840D9648C0C2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFileTime  
	{
	public:
		CFileTime();
		virtual ~CFileTime();

		SYSTEMTIME operator=(CFileTime &cft);
   	BOOL bSetLocal(SYSTEMTIME st);
   	BOOL bSetSystem(SYSTEMTIME st);
		void SetLocal(FILETIME ft);
		void SetSystem(FILETIME ft);
   	void SetLocalNow();
   	void SetUTCNow();

		FILETIME GetLocal();
		FILETIME GetUTC();
		SYSTEMTIME stGetLocal();
		SYSTEMTIME stGetUTC();
		ULONGLONG ullGetLocal();
		ULONGLONG ullGetUTC();

	public:
		void AddJours(LONG lNbJours);
	private:
		FILETIME m_ft;
		BOOL m_bLocalTime;
	};

#endif // !defined(AFX_FILETIME_H__FCC1DA5A_069F_41CA_A335_840D9648C0C2__INCLUDED_)

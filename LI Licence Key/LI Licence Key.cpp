#include "Stdafx.h"
#include "IdLIKey.h"
#include "CleLogicielle.h"
#include "Produits.h"

//------------------------------------------------------
/* Exemple Seriz 2G
#define N_PRODUIT PRODUIT_SERIZ_2G
#define N_FONCTION_PRODUIT FONCTION_SERIZ_2G
#define N_PROTECTION_MAJOR 5
#define N_PROTECTION_MINOR 0
#define NOM_SOUS_CLE_APPLI "Seriz"
*/

// Serveur OPC standard
#define N_PRODUIT						PRODUIT_SERVEUR_OPC
#define N_FONCTION_PRODUIT	FONCTION_SERVEUR_OPC_STANDARD
#define N_PROTECTION_MAJOR 1
#define N_PROTECTION_MINOR 0
#define NOM_SOUS_CLE_APPLI ""
#define DESCRIPTION_PRODUIT "Serveur OPC / standard 1.00"

static CCleLogicielle CleLogicielle;

static ChargeInfoLicence(HWND hwnd)
	{
	if(CleLogicielle.bLectureEtVerificationLicence())
		{
		if (CleLogicielle.m_bCleLicenceValide)
			{
			if (CleLogicielle.bExecutionAutorisee())
				{
				MessageBox(hwnd, "Licence Ok", DESCRIPTION_PRODUIT, MB_OK | MB_ICONINFORMATION);
				}
			else
				MessageBox(hwnd, "Licence Ok - Date limite d'utilisation d�pass�e", DESCRIPTION_PRODUIT, MB_OK | MB_ICONEXCLAMATION);
			}
		else
			MessageBox(hwnd, "Cl� de Licence invalide", DESCRIPTION_PRODUIT, MB_OK | MB_ICONEXCLAMATION);
		}
	else
		{
		MessageBox(hwnd, "Licence invalide ou Acc�s refus�", DESCRIPTION_PRODUIT, MB_OK | MB_ICONEXCLAMATION);
		}
	SetDlgItemInt (hwnd, IDC_NUMERO_CLE, CleLogicielle.m_Licence.dwCle, FALSE);
	SetDlgItemInt (hwnd, IDC_ID_MACHINE, CleLogicielle.m_dwNumeroDeSerie, FALSE);
	}

//-----------------------------------------------------------------------
// G�re la boite de dialogue dlgprocParcours
BOOL CALLBACK dlgprocMain (HWND hwnd, UINT wMessage, WPARAM wParam, LPARAM lParam)
	{
	switch (wMessage)
		{
		case WM_INITDIALOG:
			{
			// On associe l'icone � la fenetre
			HICON  hIcon = LoadIcon ((HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), MAKEINTRESOURCE(ID_ICO_DONGLE));

			::SendMessage (hwnd, WM_SETICON, TRUE, (LPARAM)hIcon);

			// Mise � jour des contr�les avec les infos de protection
			SetDlgItemText (hwnd, IDC_NOM_PRODUIT, DESCRIPTION_PRODUIT);

			DWORD dwIdMachine = 0;
			CCleLogicielle::INFO_PRODUIT InfoProduit;
			InfoProduit.dwNProduit = N_PRODUIT;
			InfoProduit.dwNFonctionProduit = N_FONCTION_PRODUIT;
			InfoProduit.dwVersionProtectionMajor = N_PROTECTION_MAJOR;
			InfoProduit.dwVersionProtectionMinor = N_PROTECTION_MINOR;

			CleLogicielle.SetProduit(InfoProduit, NOM_SOUS_CLE_APPLI);
			Sleep(500);
			ChargeInfoLicence(hwnd);
			}
			return TRUE;
		
		case WM_COMMAND:
			switch (LOWORD(wParam))
				{
				case IDOK:
					{
					// Programmation de la cl� logicielle :
					// Valeur saisie Ok ?
					BOOL	bOk = FALSE;
					DWORD	dwNumCleSaisie = GetDlgItemInt (hwnd, IDC_NUMERO_CLE, &bOk, FALSE);

					if (bOk)
						{
						// oui => Enregistrement de la cl� Ok ?
						HCURSOR	hOldCursor = SetCursor (LoadCursor (NULL, IDC_WAIT));

						CleLogicielle.ResetLicence();
						CleLogicielle.m_Licence.dwCle = dwNumCleSaisie;
						bOk = CleLogicielle.bEnregistreLicence();
						SetCursor (hOldCursor);

						if (bOk)
							{
							// oui => Recharge les infos
							ChargeInfoLicence(hwnd);

							// termin� si Ok
							if (CleLogicielle.m_bCleLicenceValide)
								EndDialog (hwnd, TRUE);
							}
						else
							::MessageBox (hwnd, "La programmation de la cl� logicielle a �chou�.", NULL, MB_ICONEXCLAMATION | MB_OK);
						}
					else
						::MessageBox (hwnd, "Le num�ro de cl� saisi est invalide;\n"
							"Veuillez le corriger.", NULL, MB_ICONEXCLAMATION | MB_OK);
					}
					break;

				case IDCANCEL:
					// Termin� Abandon
					EndDialog (hwnd, FALSE);
					return TRUE;

				case IDC_NUMERO_CLE:
					// Modification et num�ro valide => Bouton programmer par d�faut
					break;
				}
			break;
		}
	return FALSE;		// Didn't process a wMessage 
	} // dlgprocMain


// corps de l'application
int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
	{
	DialogBox (hInstance, MAKEINTRESOURCE(IDD_DIALOG), NULL, (DLGPROC)dlgprocMain);
	return 0;
	}

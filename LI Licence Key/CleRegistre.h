// CleRegistre.h: interface for the CCleRegistre class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLEREGISTRE_H__3274F9F6_8A11_49F8_8051_AD343E67245D__INCLUDED_)
#define AFX_CLEREGISTRE_H__3274F9F6_8A11_49F8_8051_AD343E67245D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Gestion de la base de registres
// JS WIN32 20/6/2001

class CCleRegistre  
	{
	public:
		CCleRegistre();
		virtual ~CCleRegistre();

		// Ouvre ou cr�e un chemin compos� d'1 � N sous-cl�s
		BOOL bRegOuvreOuCreeChemin (
			HKEY hKeyParente, // Cl� de base (ex: HKEY_LOCAL_MACHINE) ou cl� dja ouverte
			PCSTR pszPathKey,
			BOOL bAutoriseCreation);

		BOOL bRegOuvreEnLecture (
			HKEY hKeyParente,  // Cl� de base (ex: HKEY_LOCAL_MACHINE) ou cl� dja ouverte
			PCSTR pszPathKey);

		// Ferme la cl� couramment ouverte
		void Ferme ();

		// Renvoie en clair une valeur DWORD crypt�e stock�e dans la base de registre sur HKEY_LOCAL_MACHINE
		BOOL bGetDWordValue
			(
			PCSTR szNomKey,      // Nom de la cl�
			DWORD &dwValeur,    // Adresse de la valeur � lire
			DWORD dwCleCryptage = 0  // Cryptage de la cl� (aucun si 0)
			);                   // TRUE si Ok, FALSE sinon ; GetLastError pour code d'erreur

		// Crypte et enregistre un DWORD dans la base de registre sur HKEY_LOCAL_MACHINE
		BOOL bLetDWordValue 
			(
		  PCSTR szNomKey,      // Nom de la cl�
			DWORD dwValeur,      // Valeur � affecter � la cl�
			DWORD dwCleCryptage  // Cryptage de la cl� (aucun si 0)
			);                 // TRUE si Ok, FALSE sinon; GetLastError pour code d'erreur

		// Lit une string dans la base de registre (en la cryptant si dwCleCryptage <> 0)
		// Renvoie VARIANT_FALSE si StringLue n'est pas une string (elle n'a pas besoin d'�tre initialis�e)
		BOOL bGetStringValue
			(
			PCSTR szNomKey,      // Nom de la cl�
			LPVARIANT pvStringLue,  // Variant auquel on doit passer une string VB
			DWORD dwCleCryptage  // Cryptage de la cl� (aucun si 0)
			);                    // TRUE si Ok, FALSE sinon (StringLue non d�clar�e as String, cl� non trouv�e ou valeur non STRING) Cf. GetLastError

		// Crypte et enregistre une string dans la base de registre
		BOOL bLetStringValue
			(
			PCSTR szNomKey,      // Nom de la cl�
			PCSTR pszValeur,    // Valeur � affecter � la cl�
			DWORD dwCleCryptage  // Cryptage de la cl� (aucun si 0)
			);                    // TRUE si Ok, FALSE sinon (erreur cr�ation chemin ou �criture valeur) Cf. GetLastError

		HKEY hkeyGet()const {return m_hKey;}

	private:
		// Utilitaire : cr�e ou ouvre un chemin et renvoie le HKEY si Ok
		HKEY hkeyCreeOuOuvre (HKEY hKeyParente, PCSTR pszPath, BOOL bAutoriseCreation);

	private:
		HKEY m_hKey;
	};


#endif // !defined(AFX_CLEREGISTRE_H__3274F9F6_8A11_49F8_8051_AD343E67245D__INCLUDED_)

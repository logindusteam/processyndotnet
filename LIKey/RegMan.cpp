// RegMan.c
// Gestion de la base de registres
// JS WIN32 28/3/97

#include <stdafx.h>
#include "RegMan.h"

//--------------------------------------------------------
// Ouvre ou cr�e un chemin compos� d'1 � N sous-cl�s
BOOL bRegOuvreOuCreeChemin (HKEY hKeyOuverte, PCSTR pszPath, PHKEY phKey)
	{
	BOOL	bOk = FALSE;
	BOOL	bCheminComplexe = FALSE;
	int		nNChar;
	DWORD dwAction;
	DWORD dwErr;
	HKEY	hKeyCreee;

	// Recherche si le path contient plusieurs sous-cl�
	for (nNChar = 0; nNChar < lstrlen (pszPath); nNChar++)
		{
		if (pszPath [nNChar] =='\\')
			{
			// Le path contient plusieurs sous-cl�s
			bCheminComplexe = TRUE;
			break;
			}
		}

	// Le path contient plusieurs sous-cl� ?
	if (bCheminComplexe)
		{
		// oui => cr�e ou ouvre une cl� sur la premi�re cl� trouv�e
		char szSousCle [MAX_PATH];

		// R�cup�re la sous cl�
		lstrcpy (szSousCle, pszPath);
		szSousCle [nNChar] = '\0';

		// Sous cl� ouverte ou cr��e ?
		dwErr = RegCreateKeyEx (hKeyOuverte, szSousCle, 0, NULL, 
			REG_OPTION_NON_VOLATILE, KEY_CREATE_SUB_KEY, NULL, &hKeyCreee, &dwAction);

		if (dwErr == NO_ERROR)
			{
			// oui => r�entre pour continuer la cr�ation de l'arborescence
			bOk = bRegOuvreOuCreeChemin (hKeyCreee, &pszPath[nNChar+1], phKey);
			RegCloseKey (hKeyCreee);
			}
		}
	else
		{
		// non => cr�e ou ouvre la cl� sp�cifi�e
		// Sous cl� ouverte ou cr��e ?
		dwErr = RegCreateKeyEx (hKeyOuverte, pszPath, 0, NULL, 
			REG_OPTION_NON_VOLATILE, KEY_SET_VALUE, NULL, &hKeyCreee, &dwAction);

		if (dwErr == NO_ERROR)
			{
			// oui => Ok, renvoie le handle de cl� ouvert
			bOk = TRUE;
			*phKey = hKeyCreee;
			}
		}

	return bOk;
	} // bRegOuvreOuCreeChemin


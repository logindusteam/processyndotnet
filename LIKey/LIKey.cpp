#include <stdafx.h>
#include "IdLIKey.h"
#include "SoftKey.h"

static const char szPathKey [] = "SOFTWARE\\Logique Industrie\\DLLs\\1.00";
static const char szNomKey [] = "SoftKey";

//-----------------------------------------------------------------------
// G�re la boite de dialogue dlgprocParcours
BOOL CALLBACK dlgprocMain (HWND hwnd, UINT wMessage, WPARAM wParam, LPARAM lParam)
	{
	switch (wMessage)
		{
		case WM_INITDIALOG:
			{
			// On associe l'icone � la fenetre
			HICON  hIcon = LoadIcon ((HINSTANCE) GetWindowLong (hwnd, GWL_HINSTANCE), MAKEINTRESOURCE(ID_ICO_DONGLE));

			::SendMessage (hwnd, WM_SETICON, TRUE, (LPARAM)hIcon);

			// Mise � jour des contr�les avec les infos de protection
			SetDlgItemInt (hwnd, IDC_ID_MACHINE, dwGetIdMachine(), FALSE);
			SetDlgItemInt (hwnd, IDC_NUMERO_CLE, dwGetNumeroCle(szPathKey, szNomKey), FALSE);
			}
			return TRUE;
		
		case WM_COMMAND:
			switch (LOWORD(wParam))
				{
				case IDOK:
					{
					// Programmation de la cl� logicielle :
					// Valeur saisie Ok ?
					BOOL	bOk;
					DWORD	dwNumCleSaisie = GetDlgItemInt (hwnd, IDC_NUMERO_CLE, &bOk, FALSE);

					if (bOk)
						{
						// oui => Enregistrement de la cl� Ok ?
						HCURSOR	hOldCursor = SetCursor (LoadCursor (NULL, IDC_WAIT));

						bOk = bSetNumeroCle (szPathKey, szNomKey, dwNumCleSaisie);
						SetCursor (hOldCursor);

						if (bOk)
							{
							// oui => termin� Ok
							::MessageBox (hwnd, "Cl� logicielle programm�e !", "Logique Industrie", MB_OK);
							EndDialog (hwnd, TRUE);
							}
						else
							::MessageBox (hwnd, "La programmation de la cl� logicielle a �chou�.", NULL, MB_ICONSTOP | MB_OK);
						}
					else
						::MessageBox (hwnd, "Le num�ro de cl� saisi est invalide;\n"
							"Veuillez le corriger.", NULL, MB_ICONSTOP | MB_OK);
					}
					break;

				case IDCANCEL:
					// Termin� Abandon
					EndDialog (hwnd, FALSE);
					return TRUE;

				case IDC_NUMERO_CLE:
					// Modification et num�ro valide => Bouton programmer par d�faut
					break;
				}
			break;
		}
	return FALSE;		// Didn't process a wMessage 
	} // dlgprocMain


// corps de l'application
int PASCAL WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
	{
	DialogBox (hInstance, MAKEINTRESOURCE(IDD_DIALOG), NULL, (DLGPROC)dlgprocMain);
	return 0;
	}

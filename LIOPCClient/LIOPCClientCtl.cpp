// LIOPCClientCtl.cpp : Implementation of the CLIOPCClientCtrl ActiveX Control class.

#include "stdafx.h"
#include "LIOPCClient.h"
#include "LIOPCClientCtl.h"
#include "LIOPCClientPpg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CLIOPCClientCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CLIOPCClientCtrl, COleControl)
	//{{AFX_MSG_MAP(CLIOPCClientCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_EDIT, OnEdit)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CLIOPCClientCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CLIOPCClientCtrl)
	DISP_FUNCTION(CLIOPCClientCtrl, "bConfigureAccesServeur", bConfigureAccesServeur, VT_BOOL, VTS_BSTR VTS_I4 VTS_BSTR)
	DISP_FUNCTION(CLIOPCClientCtrl, "AjouteGroupe", AjouteGroupe, VT_BOOL, VTS_PI4 VTS_BSTR VTS_BOOL VTS_I4)
	DISP_FUNCTION(CLIOPCClientCtrl, "Execution", Execution, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CLIOPCClientCtrl, "SupprimeGroupes", SupprimeGroupes, VT_EMPTY, VTS_NONE)
	DISP_FUNCTION(CLIOPCClientCtrl, "FinExecution", FinExecution, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CLIOPCClientCtrl, "ExecutionEnCours", ExecutionEnCours, VT_BOOL, VTS_NONE)
	DISP_FUNCTION(CLIOPCClientCtrl, "DerniereErreur", DerniereErreur, VT_I4, VTS_NONE)
	DISP_FUNCTION(CLIOPCClientCtrl, "DerniereErreurGroupe", DerniereErreurGroupe, VT_I4, VTS_I4)
	DISP_FUNCTION(CLIOPCClientCtrl, "ExecutionGroupeEnCours", ExecutionGroupeEnCours, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CLIOPCClientCtrl, "ActiveRafraichissementGroupe", ActiveRafraichissementGroupe, VT_BOOL, VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CLIOPCClientCtrl, "AjouteItem", AjouteItem, VT_BOOL, VTS_I4 VTS_PI4 VTS_BSTR VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CLIOPCClientCtrl, "LectureSyncGroupe", LectureSyncGroupe, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CLIOPCClientCtrl, "EcritureSyncGroupe", EcritureSyncGroupe, VT_BOOL, VTS_I4)
	DISP_FUNCTION(CLIOPCClientCtrl, "ActiveTransfertItem", ActiveTransfertItem, VT_EMPTY, VTS_I4 VTS_I4 VTS_BOOL)
	DISP_FUNCTION(CLIOPCClientCtrl, "ExecutionItemEnCours", ExecutionItemEnCours, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CLIOPCClientCtrl, "ItemATransferer", ItemATransferer, VT_BOOL, VTS_I4 VTS_I4)
	DISP_FUNCTION(CLIOPCClientCtrl, "GetValeurItem", GetValeurItem, VT_BOOL, VTS_I4 VTS_I4 VTS_PVARIANT VTS_I4 VTS_PI4)
	DISP_FUNCTION(CLIOPCClientCtrl, "SetValeurItem", SetValeurItem, VT_BOOL, VTS_I4 VTS_I4 VTS_PVARIANT VTS_BOOL)
	DISP_FUNCTION(CLIOPCClientCtrl, "DerniereErreurItem", DerniereErreurItem, VT_I4, VTS_I4 VTS_I4)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CLIOPCClientCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CLIOPCClientCtrl, COleControl)
	//{{AFX_EVENT_MAP(CLIOPCClientCtrl)
	EVENT_CUSTOM("FireJS", FireJS, VTS_NONE)
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CLIOPCClientCtrl, 1)
	PROPPAGEID(CLIOPCClientPropPage::guid)
END_PROPPAGEIDS(CLIOPCClientCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CLIOPCClientCtrl, "LIOPCCLIENT.LIOPCClientCtrl.1",
	0xf4f81d68, 0x3f15, 0x11d2, 0xa1, 0x3e, 0, 0, 0xe8, 0xd9, 0x7, 0x4)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CLIOPCClientCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DLIOPCClient =
		{ 0xf4f81d66, 0x3f15, 0x11d2, { 0xa1, 0x3e, 0, 0, 0xe8, 0xd9, 0x7, 0x4 } };
const IID BASED_CODE IID_DLIOPCClientEvents =
		{ 0xf4f81d67, 0x3f15, 0x11d2, { 0xa1, 0x3e, 0, 0, 0xe8, 0xd9, 0x7, 0x4 } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwLIOPCClientOleMisc =
	OLEMISC_INVISIBLEATRUNTIME |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CLIOPCClientCtrl, IDS_LIOPCCLIENT, _dwLIOPCClientOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl::CLIOPCClientCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CLIOPCClientCtrl

BOOL CLIOPCClientCtrl::CLIOPCClientCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegInsertable | afxRegApartmentThreading to afxRegInsertable.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_LIOPCCLIENT,
			IDB_LIOPCCLIENT,
			afxRegInsertable | afxRegApartmentThreading,
			_dwLIOPCClientOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl::CLIOPCClientCtrl - Constructor

CLIOPCClientCtrl::CLIOPCClientCtrl()
{
	InitializeIIDs(&IID_DLIOPCClient, &IID_DLIOPCClientEvents);

	// TODO: Initialize your control's instance data here.

}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl::~CLIOPCClientCtrl - Destructor

CLIOPCClientCtrl::~CLIOPCClientCtrl()
{
	// TODO: Cleanup your control's instance data here.
}

static BOOL bCToVB (const BOOL bBOOLEnC)
	{
	return (bBOOLEnC ? -1 :0);
	}
/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl::OnDraw - Drawing function

void CLIOPCClientCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));
	pdc->SelectObject(CPen::FromHandle((HPEN)GetStockObject(BLACK_PEN)));
	pdc->Ellipse(&rcBounds);
	pdc->DrawIcon(rcBounds.left,rcBounds.top, theApp.LoadIcon(IDI_ABOUTDLL));
}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl::DoPropExchange - Persistence support

void CLIOPCClientCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.

}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl::OnResetState - Reset control to default state

void CLIOPCClientCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl::AboutBox - Display an "About" box to the user

void CLIOPCClientCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_LIOPCCLIENT);
	dlgAbout.DoModal();
	// Exemple de notification : FireJS();
}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl message handlers


BOOL CLIOPCClientCtrl::bConfigureAccesServeur(LPCTSTR NomServeur, long TypeServeur, LPCTSTR NomPosteRemote) 
{
	// TODO: Add your dispatch handler code here
	return bCToVB(m_OPCClient.bConfigureAccesServeur(NomServeur, (COPCClient::TYPE_SERVEUR)TypeServeur, NomPosteRemote));
}


BOOL CLIOPCClientCtrl::AjouteGroupe(long FAR* IdGroupeCree, LPCTSTR NomGroupe, BOOL GroupeActif, long DureeRafraichissementMini) 
{
	// TODO: Add your dispatch handler code here
	if (DureeRafraichissementMini < 0)
		DureeRafraichissementMini = 0;
	return bCToVB(m_OPCClient.bAjouteGroupe((PINT) IdGroupeCree, (OPCHANDLE) 0, NomGroupe, GroupeActif, (DWORD) DureeRafraichissementMini));
}

BOOL CLIOPCClientCtrl::Execution() 
{
	// TODO: Add your dispatch handler code here

	return bCToVB(m_OPCClient.bExecute());
}

void CLIOPCClientCtrl::SupprimeGroupes() 
{
	// TODO: Add your dispatch handler code here
	m_OPCClient.SupprimeGroupes();
}

BOOL CLIOPCClientCtrl::FinExecution() 
{
	// TODO: Add your dispatch handler code here

	return bCToVB(m_OPCClient.bFinExecute());
}

BOOL CLIOPCClientCtrl::ExecutionEnCours() 
{
	// TODO: Add your dispatch handler code here
	return bCToVB(m_OPCClient.bExecutionEnCours());
}

long CLIOPCClientCtrl::DerniereErreur() 
{
	// TODO: Add your dispatch handler code here
	return (long)m_OPCClient.hrDerniereErreur();
}

long CLIOPCClientCtrl::DerniereErreurGroupe(long IdGroupe) 
{
	// TODO: Add your dispatch handler code here
	return (long)m_OPCClient.pGetCOPCGroupe(IdGroupe)->hrDerniereErreur();
}

BOOL CLIOPCClientCtrl::ExecutionGroupeEnCours(long IdGroupe) 
{
	// TODO: Add your dispatch handler code here

	return bCToVB(m_OPCClient.pGetCOPCGroupe(IdGroupe)->bExecutionEnCours());
}

BOOL CLIOPCClientCtrl::ActiveRafraichissementGroupe(long IdGroupe, BOOL Actif) 
{
	// TODO: Add your dispatch handler code here

	return bCToVB(m_OPCClient.pGetCOPCGroupe(IdGroupe)->bActivation(Actif));
}

BOOL CLIOPCClientCtrl::AjouteItem(long IdGroupe, long FAR* IdItemCree, LPCTSTR AdresseItem, long VarType, BOOL ActivationRafraichissement) 
{
	// TODO: Add your dispatch handler code here
	return bCToVB(m_OPCClient.pGetCOPCGroupe(IdGroupe)->bAjouteItem ((PINT)IdItemCree, NULL, AdresseItem, (VARTYPE)VarType, ActivationRafraichissement));
}

BOOL CLIOPCClientCtrl::LectureSyncGroupe(long IdGroupe, long TypeDeLecture) 
{
	// TODO: Add your dispatch handler code here
	return bCToVB(m_OPCClient.pGetCOPCGroupe(IdGroupe)->bLectureSync((OPCDATASOURCE)TypeDeLecture));
}

BOOL CLIOPCClientCtrl::EcritureSyncGroupe(long IdGroupe) 
{
	// TODO: Add your dispatch handler code here
	return bCToVB(m_OPCClient.pGetCOPCGroupe(IdGroupe)->bEcritureSync());
}

void CLIOPCClientCtrl::ActiveTransfertItem(long IdGroupe, long IdItem, BOOL ATransferer) 
{
	// TODO: Add your dispatch handler code here
	m_OPCClient.pGetCOPCItem(IdGroupe, IdItem)->SetATransferer(ATransferer);
}

BOOL CLIOPCClientCtrl::ExecutionItemEnCours(long IdGroupe, long IdItem) 
{
	// TODO: Add your dispatch handler code here
	return bCToVB(m_OPCClient.pGetCOPCItem(IdGroupe, IdItem)->bExecutionEnCours());
}

BOOL CLIOPCClientCtrl::ItemATransferer(long IdGroupe, long IdItem) 
{
	// TODO: Add your dispatch handler code here
	return bCToVB(m_OPCClient.pGetCOPCItem(IdGroupe, IdItem)->bGetATransferer());
}


BOOL CLIOPCClientCtrl::GetValeurItem(long IdGroupe, long IdItem, VARIANT FAR* Valeur, long VarType, long FAR* Qualite) 
{
	// TODO: Add your dispatch handler code here
	FILETIME FileTime = {0,0};
	WORD wQuality = 0;
	BOOL bRes = m_OPCClient.pGetCOPCItem(IdGroupe, IdItem)->bGetValeur(Valeur, (VARTYPE) VarType, &wQuality, &FileTime);
	*Qualite = wQuality;
	return bCToVB(bRes);
}

BOOL CLIOPCClientCtrl::SetValeurItem(long IdGroupe, long IdItem, VARIANT FAR* Valeur, BOOL ATransferer) 
{
	// TODO: Add your dispatch handler code here
	return bCToVB(m_OPCClient.pGetCOPCItem(IdGroupe, IdItem)->bSetValeur(Valeur, ATransferer));
}


LONG CLIOPCClientCtrl::DerniereErreurItem(long IdGroupe, long IdItem) 
{
	// TODO: Add your dispatch handler code here
	return m_OPCClient.pGetCOPCItem(IdGroupe, IdItem)->hrDerniereErreur();
}

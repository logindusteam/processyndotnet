//////////////////////////////////////////////////////////////////////
// OPCGroupe.cpp: implementation of the COPCGroupe class.
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include <windows.h>
//#include "opc.h"
#include "UDCOM.h"
#include "Verif.h"
//#include "TemplateArray.h"
#include "OPCClient.h"
VerifInit;

// $$ pas de gestion de trou si mise en oeuvre suppression

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------
COPCGroupe::COPCGroupe 
	(
	COPCClient * pOPCClientParent, OPCHANDLE hClientGroupe,
	PCSTR pszNom, BOOL bActif, DWORD dwRafraichissementMs
	)
	{
	m_pCOPCClientParent = pOPCClientParent;
	m_hresult = NO_ERROR;

	// allocation �ventuelle m_pszNom
	if (pszNom && pszNom[0])
		{
		PSTR pszTemp = (PSTR)malloc(strlen(pszNom)+1);
		lstrcpy (pszTemp, pszNom);
		m_pszNom = pszTemp;
		}
	else
		m_pszNom = NULL;

	m_bActifInitial = bActif;
	m_bXActif = FALSE;
	m_dwRafraichissementMsInitial = dwRafraichissementMs; // 0 = aussi vite que possible
	m_dwXRafraichissementMs = 0;
	m_hClientGroupe = hClientGroupe;
	m_hXGroupe = 0;
	m_hrExecution = HR_NON_CONNECTE;

	// Interfaces de groupe
	m_pIOPCGroupStateMgt = NULL;
	m_pIOPCSyncIO = NULL;
	m_pIOPCAsyncIO = NULL;
	m_pIOPCItemMgt = NULL;

	//m_pIOPCPublicGroupStateMgt = NULL;
	} // COPCGroupe

//------------------------------------------------------------
COPCGroupe::~COPCGroupe()
	{
	bFinExecute();
	m_pCOPCClientParent = NULL;

	// D�truit les items
	for (int nNItem = 0; nNItem < m_apItems.GetSize(); nNItem++)
		{
		COPCItem * pOPCItem = m_apItems [nNItem];

		delete pOPCItem;
		}

	// Supprime les groupes du tableau
	m_apItems.RemoveAll ();

	// Libere les strings
	if (m_pszNom)
		{
		free ((PVOID)m_pszNom);
		m_pszNom = NULL;
		}
	} // ~COPCGroupe

//////////////////////////////////////////////////////////////////////
// Fonctions membres
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------
// Lib�re les interfaces d'ex�cution de ce groupe
void COPCGroupe::LibereInterfacesExecution (void)
	{
	// Groupe : obligatoires
	if (m_pIOPCGroupStateMgt)
		{
		m_pIOPCGroupStateMgt->Release();
		m_pIOPCGroupStateMgt = NULL;
		}
	
	if (m_pIOPCSyncIO)
		{
		m_pIOPCSyncIO->Release();
		m_pIOPCSyncIO = NULL;
		}
	
	if (m_pIOPCAsyncIO)
		{
		m_pIOPCAsyncIO->Release();
		m_pIOPCAsyncIO = NULL;
		}
	
	if (m_pIOPCItemMgt)
		{
		m_pIOPCItemMgt->Release();
		m_pIOPCItemMgt = NULL;
		}
	
	// Groupe : optionnels
	/*
	if(m_pIOPCPublicGroupStateMgt)
	{
	m_pIOPCPublicGroupStateMgt->Release();
	m_pIOPCPublicGroupStateMgt = NULL;
	}
			*/
	}

//------------------------------------------------------------
// Cr�� ce groupe dans le serveur
BOOL COPCGroupe::bExecute()
	{
	BOOL	bRes = FALSE;
	LPUNKNOWN pIUnknownOPC;
	PWCHAR pszwNom = NULL;

	// Essaye de cr�er le groupe sur le serveur
	if (m_pszNom)
		pszwNom= pWSTRFromSTR (m_pszNom, 0);
	HRESULT hr = m_pCOPCClientParent->m_hrExecution;
	if (m_pCOPCClientParent->bExecutionEnCours())
		{
		hr = m_pCOPCClientParent->m_pIOPCServer->AddGroup (pszwNom,
			m_bActifInitial, m_dwRafraichissementMsInitial,
			m_hClientGroupe, NULL, 0, LOCALE_USER_DEFAULT, &m_hXGroupe,
			&m_dwXRafraichissementMs,	IID_IUnknown, &pIUnknownOPC);
		}

	// Lib�re la chaine OLE du nom du groupe
	WSTRFree(pszwNom, 0);

	// groupe cr�� Ok ?
	if (SUCCEEDED (hr))
		{
		// oui => Enregistre son �tat d'activation
		m_bXActif = m_bActifInitial;

		// Lien avec les interfaces obligatoires de OPC Group s'il ne sont pas d�ja effectu�s
		bRes = FALSE;

		if (!m_pIOPCGroupStateMgt)
			{
			hr = m_pCOPCClientParent->hLieInterface (pIUnknownOPC, IID_IOPCGroupStateMgt, (void**)&m_pIOPCGroupStateMgt);
			if (SUCCEEDED (hr))
				{
				if (!m_pIOPCSyncIO)
					{
					hr = m_pCOPCClientParent->hLieInterface (pIUnknownOPC, IID_IOPCSyncIO, (void**)&m_pIOPCSyncIO);
					if (SUCCEEDED (hr))
						{
						if (!m_pIOPCAsyncIO)
							{
							hr = m_pCOPCClientParent->hLieInterface (pIUnknownOPC, IID_IOPCAsyncIO, (void**)&m_pIOPCAsyncIO);
							if (SUCCEEDED (hr))
								{
								if (!m_pIOPCItemMgt)
									{
									hr = m_pCOPCClientParent->hLieInterface (pIUnknownOPC, IID_IOPCItemMgt, (void**)&m_pIOPCItemMgt);
									}
								}
							}
						}
					}
				}
			}

		// lien avec les interface Ok ?
		if (SUCCEEDED (hr))
			// oui => c'est bon
			bRes = TRUE;
		else
			{
			// non =>lib�re l'interface fourni lors du add group
			pIUnknownOPC->Release();

			// Nettoie ce qui doit l'�tre
			LibereInterfacesExecution();

			// lib�re la ressource fournie par le AddGroup
			m_pCOPCClientParent->m_pIOPCServer->RemoveGroup (m_hXGroupe, FALSE);
			}
		} // if (SUCCEEDED (hr))

	// prends en compte le statut de la connexion
	m_hrExecution = hr;
	SetErreur (hr);

	// Declare tous les items du groupe sur le serveur
	if (!bExecuteItems (0, m_apItems.GetSize()))
		bRes = FALSE;

	return bRes;
	} //bExecute

//------------------------------------------------------------
// Instancie les items sp�cifi�s dans le serveur
BOOL COPCGroupe::bExecuteItems (int nNPremierItem, int nNbItems)
	{
	BOOL	bRes = TRUE;

	// des items � cr�er ?
	if (nNbItems > 0)
		{
		// Pr�pare les param�tres pour ajouter les items au serveur
		OPCITEMDEF * pItemDefs = (OPCITEMDEF *)malloc(nNbItems * sizeof(OPCITEMDEF));

		// Initialise le tableau des caract�ristiques des items � ajouter
		for (int nNItem = nNPremierItem; nNItem < nNPremierItem+nNbItems; nNItem++)
			{
			// R�cup�re les infos de lancement de l'ex�cution de l'item
			COPCItem * pOPCItem = m_apItems[nNItem];
			pOPCItem->GetOPCItemDef (&pItemDefs[nNItem]);
			} // for (nNItem

		// Ajout des items si possible
		OPCITEMRESULT * aItemResults = NULL;
		HRESULT * ahr = NULL;
		HRESULT hrAddItem = m_hrExecution;
		if (bExecutionEnCours())
			hrAddItem = m_pIOPCItemMgt->AddItems(nNbItems, pItemDefs, &aItemResults, &ahr);


		// traite les r�sultats d'ajout
		for (nNItem = nNPremierItem; nNItem < nNPremierItem+nNbItems; nNItem++)
			{
			// pour chaque item
			OPCITEMDEF * pItemDef = &pItemDefs[nNItem];
			COPCItem * pOPCItem = m_apItems[nNItem];

			// prise en compte de l'erreur renvoy�e si ajout effectu�
			HRESULT hr = hrAddItem;
			if (hr == S_FALSE)
				{
				hr = ahr[nNItem-nNPremierItem];
				}

			// tout est Ok ?
			if (SUCCEEDED(hr))
				{
				// oui => enregistre les param�tres d'ex�cution
				OPCITEMRESULT * pOPCItemResult = &aItemResults[nNItem-nNPremierItem];
				pOPCItem->DebutExecution (pOPCItemResult->hServer, pOPCItemResult->vtCanonicalDataType, pOPCItemResult->dwAccessRights, hr);
				// $$ L'impl�mentation courante ne g�re pas le Blob (utilis� optionellement pour acc�lerer l'adressage des serveurs qui l'impl�mentent)
				if (pOPCItemResult->dwBlobSize)
					CoTaskMemFree(pOPCItemResult->pBlob);
				}
			else
				{
				// non => note l'�chec de l'item
				bRes = FALSE;
				pOPCItem->EchecExecution (hr);
				}

			// Lib�re les infos allou�es
			WSTRFree(pItemDef->szAccessPath, 0);
			WSTRFree(pItemDef->szItemID, 0);
			} // for (nNItem
		
		// Lib�re les r�sultats renvoy�s par le serveur
		if (SUCCEEDED(hrAddItem))
			{
			CoTaskMemFree(aItemResults);
			CoTaskMemFree(ahr);
			}

		// Mise � jour du statut
		SetErreur (hrAddItem);
		
		// Lib�re le tableau d'items defs
		free (pItemDefs);
		} // if (nNbItems > 0)

	return bRes;
	} // bExecuteItems

//------------------------------------------------------------
// Arrete l'ex�cution de ce groupe et le supprime du serveur
BOOL COPCGroupe::bFinExecute()
	{
	BOOL	bRes = FALSE;

	// Ex�cution du parent et du groupe en cours ?
	if (m_pCOPCClientParent->bExecutionEnCours() && bExecutionEnCours())
		{
		// Suppression du groupe dans les serveur - sans for�age
		HRESULT hr = m_pCOPCClientParent->m_pIOPCServer->RemoveGroup (m_hXGroupe, FALSE);

		if (SUCCEEDED (hr))
			{
			// Effacement Ok => Fin d'ex�cution des items 
			for (int nNItem = 0; nNItem < m_apItems.GetSize(); nNItem++)
				{
				COPCItem * pOPCItem = m_apItems[nNItem];
				pOPCItem->FinExecution();
				} // for (nNItem

			bRes = TRUE;

			// lib�re les interfaces ...
			LibereInterfacesExecution();
			m_hrExecution = HR_NON_CONNECTE;
			}
		else
			SetErreur (hr);
		}

	return bRes;
	}

//------------------------------------------------------------
// Ajoute un item � ce groupe
BOOL COPCGroupe::bAjouteItem (PINT pnNNouvelItem, OPCHANDLE hClientItem, PCSTR pszItemID,
	VARTYPE vtRequestedDataType, DWORD dwTaille, BOOL bActifInitial, PCSTR pszAccessPath)
	{
	BOOL bRes = TRUE;

	m_apItems.Add (new COPCItem(hClientItem, pszItemID, vtRequestedDataType, dwTaille, bActifInitial, pszAccessPath));
	*pnNNouvelItem = m_apItems.GetSize() - 1;

	// Instancie le nouvel item si ex�cution en cours
	if (bExecutionEnCours())
		{
		if (!bExecuteItems(*pnNNouvelItem, 1))
			bRes = FALSE;
		}
	return bRes;
	}
		
//------------------------------------------------------------
// r�cup�re l'adresse d'un item ou NULL
COPCItem* COPCGroupe::pGetCOPCItem (int nIdItem) const
	{
	COPCItem * pOPCItem = m_apItems[nIdItem];
	return pOPCItem;
	}

//------------------------------------------------------------
// Valide ou invalide le rafraichissement de ce groupe dans le serveur
BOOL COPCGroupe::bActivation (BOOL bActif)
	{
	BOOL	bRes = TRUE;
	SetErreur (NO_ERROR);

	if (bActif != m_bXActif)
		{
		// Ex�cution du parent et du groupe en cours ?
		if (bExecutionEnCours())
			{
			// Change l'�tat d'activation du groupe en cours d'ex�cution
			DWORD dwRevisedUpdateRate;
			HRESULT hr = m_pIOPCGroupStateMgt->SetState (NULL, &dwRevisedUpdateRate, &bActif, NULL, NULL, NULL, NULL);

			if (SUCCEEDED (hr))
				{
				m_bXActif = bActif;
				}
			else
				{
				SetErreur (hr);
				bRes = FALSE;
				}
			}
		else
			{
			SetErreur (HR_NON_CONNECTE);
			bRes = FALSE;
			}
		}

	return bRes;
	}

//------------------------------------------------------------
// Demande de lecture synchrone de tous les �l�ments du groupe marqu�s "� transf�rer"
BOOL COPCGroupe::bLectureSync(OPCDATASOURCE dwSource)
	{
	BOOL bRes = TRUE;
	SetErreur (NO_ERROR);
	int	nNbItems = m_apItems.GetSize();

	// pr�paration des param�tres
	OPCHANDLE * ahServeurs = (OPCHANDLE *)malloc(nNbItems * sizeof(OPCHANDLE));
	int nNbItemsALire = 0;

	// Initialise le tableau de handles des items � lire
	for (int nNItem = 0; nNItem < nNbItems; nNItem++)
		{
		COPCItem * pOPCItem = m_apItems[nNItem];

		// Item � transf�rer
		if (pOPCItem->bGetATransferer())
			{
			// Item en cours d'ex�cution ?
			if (pOPCItem->bExecutionEnCours())
				{
				// oui => ajoute le
				ahServeurs [nNbItemsALire] = pOPCItem->m_hServeur;
				nNbItemsALire++;
				}
			else
				{
				// non => erreur : pas de connexion
				pOPCItem->SetErreurLecture (pOPCItem->hrExecution());
				bRes = FALSE;
				}
			} // if (pOPCItem->bGetATransferer())
		} // for (int nNItem = 0; 

	// Demande de lecture
	OPCITEMSTATE * aOPCItemStates = NULL;
	HRESULT * ahr = NULL;
	HRESULT hrLecture = NO_ERROR;

	// Y a t'il des items � lire ?
	if (nNbItemsALire)
		{
		// Interface de lecture disponible ?
		if (bExecutionEnCours())
			{
			// oui => demande lecture ?
			hrLecture = m_pIOPCSyncIO->Read (dwSource, (DWORD)nNbItemsALire, ahServeurs, 
				&aOPCItemStates, &ahr);
			}
		else
			hrLecture = HR_NON_CONNECTE;
		}
		
	nNbItemsALire = 0;
	for (nNItem = 0; nNItem < nNbItems; nNItem++)
		{
		COPCItem * pOPCItem = m_apItems[nNItem];

		// Item � transf�rer
		if (pOPCItem->bGetATransferer())
			{
			// Item en cours d'ex�cution ?
			if (pOPCItem->bExecutionEnCours())
				{
				if (SUCCEEDED (hrLecture))
					{
					// r�cup�re les r�sultats
					OPCITEMSTATE * pOPCItemState = &aOPCItemStates[nNbItemsALire];

					// lecture Ok ?
					HRESULT hr = ahr[nNbItemsALire];

					if (SUCCEEDED (hr))
						{
						// oui => assigne l'item avec la valeur lue (met � jour le statut d'erreur)
						hr = pOPCItem->hrNouvelleValeur (pOPCItemState->ftTimeStamp, pOPCItemState->wQuality, pOPCItemState->vDataValue);

						// prise en compte d'une �ventuelle erreur lors de l'assignation
						if (FAILED (hr))
							bRes = FALSE;
						}
					else
						// non => enregistre l'erreur sur l'item
						pOPCItem->SetErreurLecture (hr);

					// Lib�re les ressources
					hr = VariantClear(&pOPCItemState->vDataValue);
					if (FAILED (hr))
						{
						// non => enregistre l'erreur
						pOPCItem->SetErreurLecture (hr);
						bRes = FALSE;
						}						
					} // if (SUCCEEDED (hrLecture))
				else
					{
					pOPCItem->SetErreurLecture (hrLecture);
					bRes = FALSE;
					}

				// Un item trait� en plus
				nNbItemsALire++;
				} // if (pOPCItem->m_bExecutionEnCours)
			} // if (pOPCItem->bGetATransferer())
		} // for (nNItem = 0;

	// Demande de lecture des items Ok ?
	if (SUCCEEDED (hrLecture))
		{
		// oui =>lib�re les r�sultats renvoy�s par le serveur
		CoTaskMemFree(aOPCItemStates);
		CoTaskMemFree(ahr);
		}
	else
		{
		// non => note l'erreur
		SetErreur (hrLecture);
		bRes = FALSE;
		}

	// lib�re le tableau de handles serveurs
	free (ahServeurs);
	return bRes;
	} // bLectureSync

//------------------------------------------------------------
// Demande d'�criture synchrone de tous les �l�ments du groupe marqu�s "� transf�rer"
BOOL COPCGroupe::bEcritureSync()
	{
	BOOL bRes = TRUE;
	SetErreur (NO_ERROR);
	int	nNbItems = m_apItems.GetSize();

	// des items dans le groupe ?
	if (nNbItems > 0)
		{
		// oui => demande l'�criture de tous les items marqu�s � �crire :
		// pr�paration des param�tres
		VARIANT * apvValues = (VARIANT *)malloc(nNbItems * sizeof(VARIANT));
		OPCHANDLE * ahServeurs = (OPCHANDLE *)malloc(nNbItems * sizeof(OPCHANDLE));
		HRESULT * ahr = NULL;

		int nNbItemsAEcrire = 0;

		// Initialise les tableaux de param�tres
		for (int nNItem = 0; nNItem < nNbItems; nNItem++)
			{
			// Item n'existe pas sur le serveur ?
			COPCItem * pOPCItem = m_apItems[nNItem];

			// Une sortie � faire pour cet item ?
			if (pOPCItem->bGetATransferer())
				{
				// oui => Reset de l'erreur de l'item
				pOPCItem->SetErreur(NO_ERROR);

				// item en cours d'ex�cution ?
				if (pOPCItem->bExecutionEnCours())
					{
					//r�cup�re les infos � �crire
					ahServeurs [nNbItemsAEcrire] = pOPCItem->m_hServeur;
					VariantInit (&apvValues[nNbItemsAEcrire]);
					// transfert Ok ?
					if (pOPCItem->bGetValeur (&apvValues[nNbItemsAEcrire], pOPCItem->m_vtCanonicalDataType))
						// oui => un item de plus � �crire
						nNbItemsAEcrire++;
					} // if (pOPCItem->bExecutionEnCours())
				else
					{
					// oui => une erreur pour cet item
					pOPCItem->SetErreur(HR_NON_CONNECTE);
					}
				} // if (pOPCItem->bGetATransferer())
			} // for (int nNItem = 0; nNItem < nNbItems; nNItem++)

		// Des items � �crire ?
		HRESULT hrWrite = NO_ERROR;
		if (nNbItemsAEcrire)
			{
			// oui => �criture si possible
			if (bExecutionEnCours())
				hrWrite = m_pIOPCSyncIO->Write ((DWORD)nNbItemsAEcrire, ahServeurs, apvValues, &ahr);
			else
				hrWrite = HR_NON_CONNECTE;

			if (!SUCCEEDED (hrWrite))
				{
				// non => enregistre l'erreur
				bRes = FALSE;
				SetErreur (hrWrite);
				}
			}

		// Prise en compte de l'�criture
		nNbItemsAEcrire = 0;
		for (nNItem = 0; nNItem < nNbItems; nNItem++)
			{
			// Item devait �tre �crit ?
			COPCItem * pOPCItem = m_apItems[nNItem];
			if (pOPCItem->bGetATransferer())
				{
				// Pas d'erreur sur cet item ?
				if (pOPCItem->hrDerniereErreur() == NO_ERROR)
					{
					// oui => il pouvait �tre transf�r� : �criture Ok ?
					HRESULT hr = (hrWrite);
					if (SUCCEEDED (hr))
						hr = ahr[nNbItemsAEcrire];

					// enregistre les r�sultats d'�criture :
					// �criture Ok ?
					if (FAILED (hr))
						{
						// non => enregistre l'erreur sur l'item
						pOPCItem->SetErreur(hr);
						}
					// Lib�re le variant d'erreurs
					hr = VariantClear(&apvValues [nNbItemsAEcrire]);
					if (FAILED (hr))
						{
						// non => enregistre l'erreur
						pOPCItem->SetErreur (hr);
						}
					nNbItemsAEcrire ++;
					} // if (pOPCItem->hrDerniereErreur() == NO_ERROR)
				} // if (pOPCItem->bGetATransferer())
			} // for (nNItem = 0;

		// Lib�re les r�sultats renvoy�s par le serveur
		if (ahr)
			CoTaskMemFree(ahr);
		
		// lib�re le tableau de handles serveurs
		free (ahServeurs);
		free (apvValues);
		} // if (nNbItems)

	return bRes;
	} // bEcritureSync


//------------------------------------------------------------
// Demande d'�criture de l'�tat d'activation de tous les �l�ments du groupe marqu�s "� transf�rer"
BOOL COPCGroupe::bActivationItems()
	{
	BOOL bRes = TRUE;
	SetErreur (NO_ERROR);
	int	nNbItems = m_apItems.GetSize();

	// des items dans le groupe ?
	if (nNbItems > 0)
		{
		// oui => demande l'�criture de l'�tat d'activation de tous les items marqu�s "� transf�rer"
		// pr�paration des param�tres
		OPCHANDLE * ahServeursItemAActiver = (OPCHANDLE *)malloc(nNbItems * sizeof(OPCHANDLE));
		OPCHANDLE * ahServeursItemADesactiver = (OPCHANDLE *)malloc(nNbItems * sizeof(OPCHANDLE));
		HRESULT * ahrActivation = NULL;
		HRESULT * ahrDesactivation = NULL;


		int nNbItemsAActiver = 0;
		int nNbItemsADesactiver = 0;

		// Initialise les tableaux de param�tres
		for (int nNItem = 0; nNItem < nNbItems; nNItem++)
			{
			// Item n'existe pas sur le serveur ?
			COPCItem * pOPCItem = m_apItems[nNItem];

			// Un transfert � faire pour cet item ?
			if (pOPCItem->bGetATransferer())
				{
				// oui => Reset de l'erreur de l'item
				pOPCItem->SetErreur(NO_ERROR);

				// item en cours d'ex�cution ?
				if (pOPCItem->bExecutionEnCours())
					{
					if (pOPCItem->bGetActivation())
						{
						// oui => un item de plus � Activer
						ahServeursItemAActiver [nNbItemsAActiver] = pOPCItem->m_hServeur;
						nNbItemsAActiver++;
						}
					else
						{
						// non => un item de plus � D�sactiver
						ahServeursItemADesactiver [nNbItemsADesactiver] = pOPCItem->m_hServeur;
						nNbItemsADesactiver++;
						}
					} // if (pOPCItem->bExecutionEnCours())
				else
					{
					// oui => une erreur pour cet item
					pOPCItem->SetErreur(HR_NON_CONNECTE);
					}
				} // if (pOPCItem->bGetATransferer())
			} // for (int nNItem = 0; nNItem < nNbItems; nNItem++)

		// Des items � Desactiver ?
		HRESULT hrItemMgtDesactivation = NO_ERROR;
		if (nNbItemsADesactiver)
			{
			// oui => Desactivation si possible
			if (bExecutionEnCours())
				hrItemMgtDesactivation = m_pIOPCItemMgt->SetActiveState ((DWORD)nNbItemsADesactiver, ahServeursItemADesactiver, FALSE, &ahrDesactivation);
			else
				hrItemMgtDesactivation = HR_NON_CONNECTE;

			if (!SUCCEEDED (hrItemMgtDesactivation))
				{
				// non => enregistre l'erreur
				bRes = FALSE;
				SetErreur (hrItemMgtDesactivation);
				}
			}

		// Des items � Activer ?
		HRESULT hrItemMgtActivation = NO_ERROR;
		if (nNbItemsAActiver)
			{
			// oui => Activation si possible
			if (bExecutionEnCours())
				hrItemMgtActivation = m_pIOPCItemMgt->SetActiveState ((DWORD)nNbItemsAActiver, ahServeursItemAActiver, TRUE, &ahrActivation);
			else
				hrItemMgtActivation = HR_NON_CONNECTE;

			if (!SUCCEEDED (hrItemMgtActivation))
				{
				// non => enregistre l'erreur
				bRes = FALSE;
				SetErreur (hrItemMgtActivation);
				}
			}


		// Prise en compte de l'activation/D�sactivation
		nNbItemsAActiver = 0;
		nNbItemsADesactiver = 0;
		for (nNItem = 0; nNItem < nNbItems; nNItem++)
			{
			// Item devait �tre �crit ?
			COPCItem * pOPCItem = m_apItems[nNItem];
			if (pOPCItem->bGetATransferer())
				{
				// Pas d'erreur sur cet item ?
				if (pOPCItem->hrDerniereErreur() == NO_ERROR)
					{
					// oui => il pouvait �tre transf�r� : Activation ?
					if (pOPCItem->bGetActivation())
						{
						// oui => Enregistre resultat activation ?
						HRESULT hr = (hrItemMgtActivation);
						if (SUCCEEDED (hr))
							hr = ahrActivation[nNbItemsAActiver];

						// activation Ok ?
						if (FAILED (hr))
							{
							// non => enregistre l'erreur sur l'item
							pOPCItem->SetErreur(hr);
							}
						nNbItemsAActiver ++;
						}
					else
						{
						// non => Enregistre resultat Desactivation ?
						HRESULT hr = (hrItemMgtDesactivation);
						if (SUCCEEDED (hr))
							hr = ahrDesactivation[nNbItemsADesactiver];

						// activation Ok ?
						if (FAILED (hr))
							{
							// non => enregistre l'erreur sur l'item
							pOPCItem->SetErreur(hr);
							}
						nNbItemsADesactiver ++;
						} //if (pOPCItem->bGetActivation())
					} // if (pOPCItem->hrDerniereErreur() == NO_ERROR)
				} // if (pOPCItem->bGetATransferer())
			} // for (nNItem = 0;

		// Lib�re les r�sultats renvoy�s par le serveur
		if (ahrActivation)
			CoTaskMemFree(ahrActivation);
		if (ahrDesactivation)
			CoTaskMemFree(ahrDesactivation);
		
		// lib�re le tableau de handles serveurs
		free (ahServeursItemAActiver);
		free (ahServeursItemADesactiver);
		} // if (nNbItems)

	return bRes;
	} // bActivationItems

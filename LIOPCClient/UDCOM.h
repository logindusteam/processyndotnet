//-------------------------------------------------------------
// UDCOM.h
// Outils pour la mise en oeuvre de DCOM
//---------------------------------------------------------------------------------

//-----------------------------------------------------------------
// Fonctions DCOM
//-----------------------------------------------------------------
// Ouverture / fermeture de l'acc�s � DCOM (lien dynamique � la DLL)
BOOL bDCOMInitialiseLocal(void);
void DCOMFermeLocal(void);

//--------------------------------------------------------------------
// Transfert et conversions de chaines pour OLE
// Remarque : toutes les chaines OLE sont unicodes allou�es pour OLE
//--------------------------------------------------------------------
// Alloue et duplique une chaine WCHAR dans une chaine WCHAR
WCHAR * WSTRClone(const WCHAR *oldstr, IMalloc *pmem);

// Alloue et duplique une chaine ANSII dans une chaine WCHAR
WCHAR * pWSTRFromSTR(const CHAR *temp, IMalloc *p);

// Alloue et duplique une chaine WCHAR dans une chaine ANSII
CHAR * pSTRFromWSTR(const WCHAR *temp, IMalloc *p);

// Lib�re une chaine WCHAR
void WSTRFree(WCHAR * c, IMalloc *pmem);

// Lib�re une chaine ANSII
void SBCSFree(CHAR *temp, IMalloc *p);


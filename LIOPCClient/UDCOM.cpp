// UDCOM.cpp
// Gestion DCOM Client
#include "StdAfx.h"
//#define _WIN32_DCOM
//#include <windows.h>
//#include <OLECTL.h>
//#include <OLE2.h>
#include <string.h>
//#include "DLLMan.h"
//#include "USignale.h"
//#include "Verif.h"
#include "UDCOM.h"
//VerifInit;

//---------------------------------------------------------
// Initialisation d'une t�che utilisant DCOM
BOOL bDCOMInitialiseLocal(void)
	{
	BOOL	bRes = FALSE;
	
	// initialisation COM en mode appartement thread Ok ?
	HRESULT	hr = CoInitialize(NULL); // $$ appeler CoInitializeEx

	if (SUCCEEDED(hr)) 
		{
		// oui => initialisation DCOM Ok ?
		hr = CoInitializeSecurity(
			NULL,   // Droits d'acc�s par d�faut
			-1,     // Nombre d'entr�es dans asAuthSvc 
			NULL,   // Noms � enregistrer
			NULL,   // Reserv�
			RPC_C_AUTHN_LEVEL_NONE,    // authentication pour les proxy
			RPC_C_IMP_LEVEL_IMPERSONATE,// impersonation par d�faut pour les proxy
			NULL,                      // r�serv�
			EOAC_NONE,                 // pas de capacit� particuli�re
			NULL                       // r�serv�
			); 
		if (SUCCEEDED(hr))
			{
			bRes = TRUE;
			}
		else
			{
			CoUninitialize();
			}
		}

	return bRes;
	} // bDCOMInitialiseLocal

//---------------------------------------------------------
// Fermeture DCOM pour une t�che.
void DCOMFermeLocal (void)
	{
	// Libere COM
	CoUninitialize();
	}

//---------------------------------------------------------
// Alloue et duplique une chaine WCHAR
//---------------------------------------------------------
WCHAR * WSTRClone (const WCHAR *oldstr, IMalloc *pmem)
	{
	WCHAR *newstr;
	
	if (pmem)
		newstr = (WCHAR*)pmem->Alloc(sizeof(WCHAR) * (wcslen(oldstr) + 1));
	else
		newstr = new WCHAR[wcslen(oldstr) + 1];
	
	if(newstr)
		wcscpy(newstr, oldstr);

	return newstr;
	}

//---------------------------------------------------------
// Lib�re une chaine WCHAR
//---------------------------------------------------------
void WSTRFree (WCHAR * c, IMalloc *pmem)
	{
	if(c != NULL)
		{
		if(pmem)
			pmem->Free(c);
		else
			delete c;
		}
	}

//---------------------------------------------------------
// Lib�re une chaine ANSII
//---------------------------------------------------------
void SBCSFree (CHAR * c, IMalloc *pmem)
	{
	if (c != NULL)
		{
		if(pmem)
			pmem->Free(c);
		else
			delete c;
		}
	}


//---------------------------------------------------------
// Alloue et duplique une chaine WCHAR dans une chaine ANSII
//---------------------------------------------------------
CHAR * pSTRFromWSTR (const WCHAR *wcbuf, IMalloc * pmem)
	{
	int	length = wcslen(wcbuf) + 1;
	CHAR * pszRes;
	
	if(pmem)
		pszRes = (CHAR *) pmem->Alloc( length );
	else
		pszRes = new CHAR [length];
	
	if (pszRes)
		{
		for (int i=0; i<length; i++)
			pszRes[i] = (CHAR) wcbuf[i];
		}

	return pszRes;
	}

//---------------------------------------------------------
// Alloue et duplique une chaine ANSII dans une chaine WCHAR
//---------------------------------------------------------
WCHAR * pWSTRFromSTR (const CHAR *buf, IMalloc * pmem)
	{
	int	length = strlen(buf) + 1;
	WCHAR	*pszRes;
	
	if (pmem)
		pszRes = (WCHAR*)pmem->Alloc(sizeof(WCHAR) * (strlen(buf) + 1));
	else
		pszRes = new WCHAR[strlen(buf) + 1];
	
	if(pszRes)
		{
		for(int i=0; i<length; i++)
			pszRes[i] = (WCHAR) buf[i];
		}
	return pszRes;
	}

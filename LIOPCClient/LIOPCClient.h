#if !defined(AFX_LIOPCCLIENT_H__F4F81D6E_3F15_11D2_A13E_0000E8D90704__INCLUDED_)
#define AFX_LIOPCCLIENT_H__F4F81D6E_3F15_11D2_A13E_0000E8D90704__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// LIOPCClient.h : main header file for LIOPCCLIENT.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientApp : See LIOPCClient.cpp for implementation.

class CLIOPCClientApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;
extern CLIOPCClientApp NEAR theApp;
//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIOPCCLIENT_H__F4F81D6E_3F15_11D2_A13E_0000E8D90704__INCLUDED)

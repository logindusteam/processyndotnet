// LIOPCClientPpg.cpp : Implementation of the CLIOPCClientPropPage property page class.

#include "stdafx.h"
#include "LIOPCClient.h"
#include "LIOPCClientPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CLIOPCClientPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CLIOPCClientPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CLIOPCClientPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CLIOPCClientPropPage, "LIOPCCLIENT.LIOPCClientPropPage.1",
	0xf4f81d69, 0x3f15, 0x11d2, 0xa1, 0x3e, 0, 0, 0xe8, 0xd9, 0x7, 0x4)


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientPropPage::CLIOPCClientPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CLIOPCClientPropPage

BOOL CLIOPCClientPropPage::CLIOPCClientPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_LIOPCCLIENT_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientPropPage::CLIOPCClientPropPage - Constructor

CLIOPCClientPropPage::CLIOPCClientPropPage() :
	COlePropertyPage(IDD, IDS_LIOPCCLIENT_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CLIOPCClientPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientPropPage::DoDataExchange - Moves data between page and properties

void CLIOPCClientPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CLIOPCClientPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientPropPage message handlers

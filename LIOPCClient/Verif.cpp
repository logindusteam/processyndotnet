#include <windows.h>
#include "Appli.h"
#include "UExcept.h"
#include "Verif.h"

//--------------------------------------------------
// appel de la fonction de sortie en indiquant la ligne et source
// ou l'erreur est apparue
void _ExitVerif (int sLig, const char * szNomSource )
	{
	char szErr[EXCEP_NB_CAR_MAX_EXPLICATION] = "";

	wsprintf (szErr, "L'erreur de fonctionnement r�f�rence '%i-%s' vient de se produire.\n"\
		"Cette erreur grave n�cessite la fermeture imm�diate de %s.\n",
		sLig, szNomSource, Appli.szNom);
	ExcepContexte (EXCEPTION_VERIF, szErr);
	} // _ExitVerif

//--------------------------------------------------
// appel de la fonction de sortie en indiquant la ligne et source
// ou l'erreur est apparue
// Renvoie toujours FALSE s'il retourne � l'appelant
BOOL _bDireWarning (int sLig, const char * szNomSource )
	{
	char szErr[EXCEP_NB_CAR_MAX_EXPLICATION];

	wsprintf (szErr, "L'erreur de fonctionnement r�f�rence '%i-%s' vient de se produire.\n"\
		"%s peut continuer sans que son fonctionnement soit garanti.\n"\
		"Signaler les avertissements suivants ? (Annuler pour terminer l'application)",
		sLig, szNomSource, Appli.szNom);
	ExcepWarnContexte (EXCEPTION_VERIF, szErr);
	return FALSE;
	} // _ExitWarn

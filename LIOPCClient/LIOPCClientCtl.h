#if !defined(AFX_LIOPCCLIENTCTL_H__F4F81D76_3F15_11D2_A13E_0000E8D90704__INCLUDED_)
#define AFX_LIOPCCLIENTCTL_H__F4F81D76_3F15_11D2_A13E_0000E8D90704__INCLUDED_

#include "OPCClient.h"	// Added by ClassView
#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// LIOPCClientCtl.h : Declaration of the CLIOPCClientCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CLIOPCClientCtrl : See LIOPCClientCtl.cpp for implementation.

class CLIOPCClientCtrl : public COleControl
{
	DECLARE_DYNCREATE(CLIOPCClientCtrl)

// Constructor
public:
	CLIOPCClientCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLIOPCClientCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CLIOPCClientCtrl();

	DECLARE_OLECREATE_EX(CLIOPCClientCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CLIOPCClientCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CLIOPCClientCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CLIOPCClientCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CLIOPCClientCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CLIOPCClientCtrl)
	afx_msg BOOL bConfigureAccesServeur(LPCTSTR NomServeur, long TypeServeur, LPCTSTR NomPosteRemote);
	afx_msg BOOL AjouteGroupe(long FAR* IdGroupeCree, LPCTSTR NomGroupe, BOOL GroupeActif, long DureeRafraichissementMini);
	afx_msg BOOL Execution();
	afx_msg void SupprimeGroupes();
	afx_msg BOOL FinExecution();
	afx_msg BOOL ExecutionEnCours();
	afx_msg long DerniereErreur();
	afx_msg long DerniereErreurGroupe(long IdGroupe);
	afx_msg BOOL ExecutionGroupeEnCours(long IdGroupe);
	afx_msg BOOL ActiveRafraichissementGroupe(long IdGroupe, BOOL Actif);
	afx_msg BOOL AjouteItem(long IdGroupe, long FAR* IdItemCree, LPCTSTR AdresseItem, long VarType, BOOL ActivationRafraichissement);
	afx_msg BOOL LectureSyncGroupe(long IdGroupe, long TypeDeLecture);
	afx_msg BOOL EcritureSyncGroupe(long IdGroupe);
	afx_msg void ActiveTransfertItem(long IdGroupe, long IdItem, BOOL ATransferer);
	afx_msg BOOL ExecutionItemEnCours(long IdGroupe, long IdItem);
	afx_msg BOOL ItemATransferer(long IdGroupe, long IdItem);
	afx_msg BOOL GetValeurItem(long IdGroupe, long IdItem, VARIANT FAR* Valeur, long VarType, long FAR* Qualite);
	afx_msg BOOL SetValeurItem(long IdGroupe, long IdItem, VARIANT FAR* Valeur, BOOL ATransferer);
	afx_msg long DerniereErreurItem(long IdGroupe, long IdItem);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CLIOPCClientCtrl)
	void FireJS()
		{FireEvent(eventidFireJS,EVENT_PARAM(VTS_NONE));}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CLIOPCClientCtrl)
	dispidBConfigureAccesServeur = 1L,
	dispidAjouteGroupe = 2L,
	dispidExecution = 3L,
	dispidSupprimeGroupes = 4L,
	dispidFinExecution = 5L,
	dispidExecutionEnCours = 6L,
	dispidDerniereErreur = 7L,
	dispidDerniereErreurGroupe = 8L,
	dispidExecutionGroupeEnCours = 9L,
	dispidActiveRafraichissementGroupe = 10L,
	dispidAjouteItem = 11L,
	dispidLectureSyncGroupe = 12L,
	dispidEcritureSyncGroupe = 13L,
	dispidActiveTransfertItem = 14L,
	dispidExecutionItemEnCours = 15L,
	dispidItemATransferer = 16L,
	dispidGetValeurItem = 17L,
	dispidSetValeurItem = 18L,
	dispidDerniereErreurItem = 19L,
	eventidFireJS = 1L,
	//}}AFX_DISP_ID
	};
private:
	COPCClient m_OPCClient;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIOPCCLIENTCTL_H__F4F81D76_3F15_11D2_A13E_0000E8D90704__INCLUDED)

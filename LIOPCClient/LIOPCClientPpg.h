#if !defined(AFX_LIOPCCLIENTPPG_H__F4F81D78_3F15_11D2_A13E_0000E8D90704__INCLUDED_)
#define AFX_LIOPCCLIENTPPG_H__F4F81D78_3F15_11D2_A13E_0000E8D90704__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// LIOPCClientPpg.h : Declaration of the CLIOPCClientPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CLIOPCClientPropPage : See LIOPCClientPpg.cpp.cpp for implementation.

class CLIOPCClientPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CLIOPCClientPropPage)
	DECLARE_OLECREATE_EX(CLIOPCClientPropPage)

// Constructor
public:
	CLIOPCClientPropPage();

// Dialog Data
	//{{AFX_DATA(CLIOPCClientPropPage)
	enum { IDD = IDD_PROPPAGE_LIOPCCLIENT };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CLIOPCClientPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LIOPCCLIENTPPG_H__F4F81D78_3F15_11D2_A13E_0000E8D90704__INCLUDED)

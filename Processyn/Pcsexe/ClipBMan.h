//----------------------------------------------
// ClipBMan.h
// Fonctions li�es au presse papier syst�me
// Version WIN32
//----------------------------------------------

//--------------------------------
// Ecriture dans le presse papier
//--------------------------------

// Vide le presse papier puis y place le texte sp�cifi�
BOOL bClipBEcritTexte (PCSTR pszSource);

// Vide le presse papier puis y place la donn�e au format sp�cifi�
BOOL bClipBEcritFormat (UINT uFormat, PVOID pBuf, DWORD dwTailleBuf);

// Ajoute le texte sp�cifi� au presse papier
BOOL bClipBAjouteTexte (PCSTR pszSource);

// Ajoute la donn�e au format sp�cifi� au presse papier
BOOL bClipBAjouteFormat (UINT uFormat, PVOID pBuf, DWORD dwTailleBuf);

//--------------------------------
// Lecture du presse papier
//--------------------------------

// R�cup�re le texte du presse papier si pr�sent
// Le buffer peut �ventuellement �tre surdimensionn�
// ATTENTION : faire un MemLibere apr�s utilisation du pointeur retourn�
PSTR pszClipBLitTexte (void);

// R�cup�re la donn�e au format sp�cifi� pr�sente dans le presse papier
// ATTENTION : faire un MemLibere apr�s utilisation du pointeur retourn�
BOOL bClipBLitFormat (UINT uFormat, PVOID * ppBuf, DWORD * dwTailleBuf);



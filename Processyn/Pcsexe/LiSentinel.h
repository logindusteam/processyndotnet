// LiSentinel.h


// Interface d'acces au cle Sentinel.
// Les valeurs contenues dans ls registres sont definies dans Produits.h


// D�finitions de constantes definissant les fonctions d'acces aux cl�s
#define LECTURE_CLE  1
#define ECRITURE_CLE 2
#define SURECRITURE_CLE 3

// D�finitions de constantes definissant les registres utilis�s dans les cl�s
#define REGISTRE_FONCTION        8         
#define REGISTRE_NUMERO_SERIE   14         
#define REGISTRE_MODELE_DONGLE  16         
#define REGISTRE_DEPANNAGE       9
#define REGISTRE_VERSION        10
#define REGISTRE_TYPE_PRODUIT   11
#define REGISTRE_NB_TENTATIVES  12
#define REGISTRE_NB_VARIABLES   17
#define REGISTRE_DATE_LIMITE    18


// D�finition de constante pour l'init
#define DEV_ID 0xB4CA


BOOL InitSentinel (DWORD gDevID);
BOOL SentinelSuperProAcces (DWORD fct, DWORD registre, DWORD *valeur, DWORD PasseWrite, DWORD PasseOverwrite1, DWORD PasseOverwrite2);

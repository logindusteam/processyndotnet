// OPCClient.h: interface for the COPCClient class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OPCCLIENT_H__52EFF703_1640_11D2_A0F3_0000E8D90704__INCLUDED_)
#define AFX_OPCCLIENT_H__52EFF703_1640_11D2_A0F3_0000E8D90704__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define HR_NON_CONNECTE E_FAIL

class COPCItem  
	{
	friend class COPCGroupe;
	// Constructeurs destructeurs
	public:
		COPCItem (OPCHANDLE hClient, PCSTR pszItemID, VARTYPE vtRequestedDataType, DWORD dwTaille, BOOL bActif = TRUE, PCSTR pszAccessPath = NULL);
		virtual ~COPCItem();
		
	// m�thodes
	public:
		// Valide / invalide le transfert de l'item lors de la prochaine op�ration sur le serveur
		void SetATransferer (BOOL bTransfert = TRUE) {m_bATransferer = bTransfert;}
 
		// Positionne l'information d'Activation et force l'info de transfert
		// La m�thode bActivationItems de l'objet groupe associ� doit �tre appel�e pour forcer l'activation dans le serveur
		void SetActivation (BOOL bActif, BOOL bATransferer = TRUE);

		// -------- Acc�s aux infos de l'item ----------
		BOOL bGetValeur (VARIANT * pVariantValeur, VARTYPE varTypeDesire, PWORD pwQuality = 0, PFILETIME pTimeStamp = 0);
		BOOL bExecutionEnCours (void) const {return SUCCEEDED(m_hrExecution);}
		BOOL bSetValeur (VARIANT * pVariantValeur, BOOL bATransferer = TRUE);
		BOOL bGetATransferer (void) {return m_bATransferer;}
		BOOL bGetActivation (void) {return m_bActive;}

		// --------- Statut d'erreur --------------
		HRESULT hrDerniereErreur (void) const {return m_hresult;}
		HRESULT hrExecution (void) const {return m_hrExecution;}


	private:
		// R�cup�re les infos de lancement de l'ex�cution de l'item
		// !! Attention lib�rer les strings allou�es
		void GetOPCItemDef (OPCITEMDEF * pItemDef);

		// -------- Notification d�but - fin de connexion ----------
		void DebutExecution (OPCHANDLE hServeur, VARTYPE vtCanonicalDataType, DWORD dwAccessRights, HRESULT hr);
		void EchecExecution (HRESULT hr);
		void FinExecution ();

		// --------- Notification r�sultat de lecture --------------
		HRESULT hrNouvelleValeur (FILETIME ftTimeStamp, WORD wQuality,  VARIANT &vDataValue);
		void SetErreurLecture (HRESULT hr);


		// Enregistre l'erreur courante de cet item
		void SetErreur(HRESULT hresult) {m_hresult = hresult;}

		// ---- membres -------
	private:
															// Permet de construire le tableau d'items � �changer avec le serveur
		BOOL	m_bATransferer;	    // si TRUE, item � �crire ou � lire ou � changer d'�tat
		OPCHANDLE m_hServeur;
		BOOL m_bActive;							// Item actif en ex�cution (voir lectures)
		OPCHANDLE m_hClient;
		PCSTR m_pszAccessPath;			// NULL ou String ANSI allou�e = chaine access path item
		PCSTR m_pszItemID;					// NULL ou String ANSI allou�e = chaine nom item (son adresse OPC)
		HRESULT	m_hresult;					// Derni�re erreur
		VARTYPE m_vtCanonicalDataType;
		DWORD m_dwTaille;
		DWORD m_dwAccessRights;
		WORD m_wQuality;
		FILETIME m_ftTimeStamp;
		VARIANT m_vDataValue;
		HRESULT m_hrExecution;			// R�sultat de l'ex�cution de l'item (HR_NON_CONNECTE si arr�t�)
	};

class COPCClient; // forward

class COPCGroupe  
	{
	// Constructeur / destructeur
	public:
		COPCGroupe (COPCClient * pOPCClientParent, OPCHANDLE hClientGroupe, 
			PCSTR pszNom = NULL, BOOL bActif = FALSE, DWORD dwRafraichissementMs = 0);
		virtual ~COPCGroupe();
	
	// M�thodes publiques
	public:
		// ---------- Connexion - d�connexion au serveur ---------
		BOOL bExecute ();
		BOOL bFinExecute(); // Arr�te l'ex�cution.
		BOOL bExecutionEnCours (void) const {return SUCCEEDED (m_hrExecution);}

		// ------ Gestion d'erreur ----------
		HRESULT hrDerniereErreur (void) const {return m_hresult;}

		// ---------- Configuration -----------
		BOOL bAjouteItem (PINT pnNNouvelItem, OPCHANDLE hClientItem, PCSTR pszItemID,
			VARTYPE vtRequestedDataType, DWORD dwTaille, BOOL bActifInitial = TRUE, PCSTR pszAccessPath = NULL);

		// ---------- Commandes en connect�es ---------
		BOOL bActivation (BOOL bActif);

		// r�cup�re l'adresse d'un item ou NULL
		COPCItem* pGetCOPCItem (int nIdItem) const;

		// Demande de lecture synchrone de tous les �l�ments du groupe marqu�s "� transf�rer"
		BOOL bLectureSync(OPCDATASOURCE dwSource);

		// Demande d'�criture synchrone de tous les �l�ments du groupe marqu�s "� transf�rer"
		BOOL bEcritureSync();

		// Demande d'�criture de l'�tat d'activation de tous les �l�ments du groupe marqu�s "� transf�rer"
		BOOL bActivationItems();

	private:
		void SetErreur(HRESULT hresult);
		void LibereInterfacesExecution (void);

	private:
		// Instancie les items sp�cifi�s dans le serveur
		BOOL bExecuteItems(int nNPremierItem, int nNbItems);

	// Membres
	private:
		CArray <COPCItem *, COPCItem *> m_apItems; // Tableau des groupes
		IOPCItemMgt * m_pIOPCItemMgt;

		HRESULT	m_hresult;									// Derni�re erreur
		COPCClient * m_pCOPCClientParent;
		PCSTR	m_pszNom;					// NULL ou Nom du groupe
		BOOL	m_bActifInitial;	// valeur initiale de l'�tat actif du groupe en ex�cution
		BOOL	m_bXActif;				// valeur courante de l'�tat actif du groupe en ex�cution
		DWORD m_dwRafraichissementMsInitial; // nb ms mini rafraichissement lecture par d�faut
		DWORD m_dwXRafraichissementMs; // nb ms mini rafraichissement lecture courante
		OPCHANDLE m_hClientGroupe;	// valeur client du handle de groupe
		OPCHANDLE m_hXGroupe;	// valeur serveur du handle de groupe en ex�cution
		HRESULT m_hrExecution;	// R�sultat de l'ex�cution du groupe (HR_NON_CONNECTE si arr�t�)

		// Interfaces obligatoires OPC Group
		IOPCGroupStateMgt * m_pIOPCGroupStateMgt;
		IOPCSyncIO * m_pIOPCSyncIO;
		IOPCAsyncIO * m_pIOPCAsyncIO;

		// Interfaces optionnels OPC Group
		//IOPCPublicGroupStateMgt * m_pIOPCPublicGroupStateMgt;
	};

class COPCClient  
	{
	friend class COPCGroupe; // Mise en commun des membres et de la gestion d'erreur
	public:
		enum TYPE_SERVEUR {SERVEUR_INVALIDE, SERVEUR_AUTO, SERVEUR_INPROC, SERVEUR_LOCAL, SERVEUR_REMOTE};
		
	// ------------ M�thodes -----------
	public:
	// constructeur destructeur
		COPCClient();
		virtual ~COPCClient();

		// Renvoie la derni�re erreur du client
		HRESULT hrDerniereErreur (void) const {return m_hresult;}

		// Configuration des param�tres d'acc�s au serveur 
		// Erreur si serveur en cours d'ex�cution
		BOOL bConfigureAccesServeur (PCSTR pszNomServeur, TYPE_SERVEUR nType, PCSTR pszNomStation = NULL);

		// ajoute un groupe au serveur et le lance s'il fonctionne
		// renvoie son index dans le tableau de groupes
		BOOL bAjouteGroupe (PINT pnNNouveauGroupe, OPCHANDLE hClientGroupe, PCSTR pszNomGroupe = NULL,
			BOOL bActif = FALSE, DWORD dwRafraichissementMs = 0);

		// r�cup�re l'adresse d'un groupe ou NULL
		COPCGroupe* pGetCOPCGroupe (int nIdGroup) const;

		// r�cup�re l'adresse d'un item ou NULL
		COPCItem* pGetCOPCItem (int nIdGroupe, int nIdItem) const {return (pGetCOPCGroupe (nIdGroupe)->pGetCOPCItem(nIdItem));};

		void SupprimeGroupes(void);
		PCSTR pszNomServeur (void) const {return m_szNomServeur;}

		// -------- Connexion - d�connexion au serveur OPC ----------
		// Connexion au serveur OPC et sa configuration
		BOOL bExecute (); 
		// Autorise l'interruption d'ex�cution (sans mise � jour du serveur) si erreur RPC.
		void AutoriseInterruptionExecution (BOOL bAutoriseInterruption){m_bAutoriseInterruptionExecution = bAutoriseInterruption;}	
		// Arr�te l'ex�cution.
		BOOL bFinExecute();
		// Indique si la connexion au serveur est op�rationnelle
		BOOL bExecutionEnCours (void) const {return SUCCEEDED (m_hrExecution);}
		// Indique si la connexion au serveur a �t� interrompue
		BOOL bExecutionInterrompue (void) const {return !SUCCEEDED (m_hrInterruptionExecution);}
		HRESULT hrExecution (void) const {return m_hrExecution;}
		HRESULT hrInterruptionExecution (void) const {return m_hrInterruptionExecution;}

	// M�thodes prot�g�es:
	private:
		void SetErreur (HRESULT hresult);
		HRESULT hLieInterface (IUnknown * pIUnknownOPC, IID iid, void** pInterface);
	protected:
		// Interromps l'ex�cution si c'est possible et que l'erreur le justifie
		void InterruptionEventuelleExecution (HRESULT hresult);
		
	// Propri�t�s prot�g�es
	private:
		CArray <COPCGroupe *, COPCGroupe *> m_apGroupes; // Tableau des groupes
		TYPE_SERVEUR m_nTypeServeur;				// mode d'ouverture du serveur
		char m_szNomServeur [MAX_PATH];			// Nom du serveur
		char m_szNomStation [MAX_PATH];			// Nom de station ou NULL
		BOOL m_bAutoriseInterruptionExecution;		// Ferme le client (sans mise � jour du serveur) si erreur RPC.

		HRESULT	m_hresult;									// Derni�re erreur
		HRESULT m_hrExecution;							// R�sultat de l'ex�cution du serveur (HR_NON_CONNECTE si arr�t�)
		HRESULT m_hrInterruptionExecution;		// Erreur ayant provoqu� la fin d'ex�cution du client.

		// Interfaces obligatoires OPC Server
		IOPCServer * m_pIOPCServer;					// (gpOPCS) Connexion OLE avec l'objet serveur ou NULL

		// Interfaces optionnels OPC Server
		// IOPCServerPublicGroups * m_pIOPCServerPublicGroups;	// (gpOPCPG) Connexion OLE avec l'objet? IOPCServerPublicGroups ou NULL
		// IOPCBrowseServerAddressSpace * m_pIOPCBrowseServerAddressSpace;	// (gpOPCBA) Connexion OLE avec l'objet? IOPCBrowseServerAddressSpace ou NULL
		// IPersistFile * m_pIPersistFile;			// (gpOPCPF) Connexion OLE avec l'objet PeristFile ou NULL

		// Interfaces obligatoires OPC EnumOPCItemAttributes
		// IEnumOPCItemAttributes * m_pIEnumOPCItemAttributes;
	};

#endif // !defined(AFX_OPCCLIENT_H__52EFF703_1640_11D2_A0F3_0000E8D90704__INCLUDED_)

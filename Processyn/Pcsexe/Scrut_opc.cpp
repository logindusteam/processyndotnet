/*----------------------------------------------------------------------+
 |	 Ce fichier est la propriete de 																		|
 |							Societe LOGIQUE INDUSTRIE 															|
 |			 Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3									|
 |	 Il est demeure sa propriete exclusive et est confidentiel. 				|
 |	 Aucune diffusion n'est possible sans accord ecrit									|
 |----------------------------------------------------------------------*/
// Scrut_opc.cpp
// Unite de scrutation en tant que client OPC
// 1/7/98 Win32 JS
// ----------------------------------------------------------------------

#include "stdafx.h"
#include "lng_res.h"
#include "tipe.h"
#include "opc.h"
#include "OPCError.h"
#include "UDCOM.h"
#include "mem.h"
#include "UStr.h"
#include "pcs_sys.h"
#include "PcsVarEx.h"
#include "Verif.h"
#include "TemplateArray.h"
#include "OPCClient.h"
#include "tipe_opc.h"
#include "scrut_opc.h"
VerifInit;

// Var globales
static BOOL bInitOPC = FALSE;

#define VAR_SYS_NOM_CLIENT_ERR VA_SYS_REG_MES1
#define VAR_SYS_NOM_GROUPE_ERR VA_SYS_REG_MES1
#define VAR_SYS_NOM_ITEM_ERR VA_SYS_REG_MES2

typedef enum
	{
	ERREUR_CLIENT_OPC_OK									= 0x0,
	ERREUR_CLIENT_OPC_INIT_DCOM						= 0x1,
	ERREUR_CLIENT_OPC_CONNEXION_SERVEUR		= 0x2,
	ERREUR_CLIENT_OPC_CREATION_GROUPE			= 0x4,
	ERREUR_CLIENT_OPC_CREATION_ITEM				= 0x8,
	ERREUR_CLIENT_OPC_LECTURE_GROUPE			= 0x10,
	ERREUR_CLIENT_OPC_LECTURE_ITEM				= 0x20,
	ERREUR_CLIENT_OPC_ACTIVATION_GROUPE		= 0x40,
	ERREUR_CLIENT_OPC_ACTIVATION_ITEM			= 0x80,
	ERREUR_CLIENT_OPC_ECRITURE_GROUPE			= 0x100,
	ERREUR_CLIENT_OPC_ECRITURE_ITEM				= 0x200
	} BITS_ERREUR_CLIENT_OPC;
_inline BITS_ERREUR_CLIENT_OPC operator |= (BITS_ERREUR_CLIENT_OPC &dest, const BITS_ERREUR_CLIENT_OPC bits)
	{
	return dest = (BITS_ERREUR_CLIENT_OPC)((DWORD)dest |(DWORD)bits);
	}

//---------------------------------------------------------------------------
// Monte des bits dans le statut global OPC
static void MajStatutOPC (BITS_ERREUR_CLIENT_OPC dwMasqueStatut)
	{
	CPcsVarEx::SetBitsValVarSysNumEx (VA_SYS_STATUS_OPC, dwMasqueStatut);
	}

//---------------------------------------------------------------------------
// Met � jour une variable de statut Processyn avec une erreur COM
// Le codage standard de l'erreur OPC est le suivant:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//
// On masque Les 4 bits de poids fort pour r�cup�rer la valeur
// dans un float.
static void MajVarStatutCOM (DWORD IdVarStatutNum, HRESULT hrStatut)
	{
	CPcsVarEx::SetValVarNumEx (IdVarStatutNum, (FLOAT)(0x0fffffff &(DWORD)hrStatut));
	}

//-------------------------------------------------------------
// Renvoie l'�tat de la variable de demande de connexion d'un client OPC
// � son serveur ou TRUE si elle n'existe pas
static BOOL bExcutionClientDemandee (PX_CLIENT_OPC pXClientOPC)
	{
	return (pXClientOPC->dwIdESExecution == 0) || (CPcsVarEx::bGetValVarLogEx(pXClientOPC->dwIdESExecution));
	}

//-------------------------------------------------------------
// Met � jour l'�ventuelle variable de Connexion d'un client OPC
// avec l'�tat ConnexionEnCours du Client 
static void MiseAjourVariablesDeConnexionClient(
	PX_CLIENT_OPC pXClientOPC,
	BITS_ERREUR_CLIENT_OPC & dwBitsStatutOPC // Statut � mettre � jour
	)
	{
	// Mise � jour de la variable de commande d'ex�cution
	if (pXClientOPC->dwIdESExecution != 0)
		{
		CPcsVarEx::SetValVarLogEx(pXClientOPC->dwIdESExecution, pXClientOPC->pCOPCClientEx->bExecutionEnCours());
		}
	if (pXClientOPC->pCOPCClientEx->bExecutionInterrompue())
		{
		// Mise � jour du statut global OPC
		dwBitsStatutOPC |= ERREUR_CLIENT_OPC_CONNEXION_SERVEUR;

		// Mise � jour du statut du client
		HRESULT hr = pXClientOPC->pCOPCClientEx->hrInterruptionExecution();
		if (FAILED (hr))
			{
			MajVarStatutCOM (pXClientOPC->dwIdESStatut, hr);
			}
		}
	}

//-------------------------------------------------------------
// Renvoie TRUE si le lient est d�connect� suite � une interruption, FALSE sinon
static BOOL bExecutionClientInterrompue(PX_CLIENT_OPC pXClientOPC)
	{
	return pXClientOPC->pCOPCClientEx->bExecutionInterrompue();
	}

//--------------------------------------------------------
// Gestion arr�t / red�marrage d'un client OPC
static void GereExecutionClient(
	BOOL bLance,															// Lancement ou Arr�t � effectuer
	BITS_ERREUR_CLIENT_OPC & dwBitsStatutOPC, // Statut � mettre � jour
	PX_CLIENT_OPC pXClientOPC)								// Infos sur la gestion du client
	{
	COPCClient * pOPCClient = pXClientOPC->pCOPCClientEx;

	// Demande de connexion au serveur ?
	if (bLance)
		{
		// oui => connexion 
		pOPCClient->bExecute();

		// Prise en compte des r�sultats serveur
		HRESULT hr = pOPCClient->hrDerniereErreur();
		if (FAILED (hr))
			{
			dwBitsStatutOPC |= ERREUR_CLIENT_OPC_CONNEXION_SERVEUR;
			MajVarStatutCOM (pXClientOPC->dwIdESStatut, hr);
			}

		// Prise en compte des r�sultats des groupes
		for (DWORD dwNGroupe = pXClientOPC->dwNPremierGroupeEx; dwNGroupe < (pXClientOPC->dwNPremierGroupeEx + pXClientOPC->dwNbGroupesEx); dwNGroupe++)
			{
			PX_GROUPE_OPC pXGroupeOPC = (PX_GROUPE_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_groupe, dwNGroupe);
			hr = pXGroupeOPC->pCOPCGroupeEx->hrDerniereErreur();
			if (FAILED (hr))
				{
				dwBitsStatutOPC |= ERREUR_CLIENT_OPC_CREATION_GROUPE;
				MajVarStatutCOM (pXGroupeOPC->dwIdESStatut, hr);
				}

			// Prise en compte des r�sultats des items
			for (DWORD dwNItem = pXGroupeOPC->dwNPremierItemEx; dwNItem < (pXGroupeOPC->dwNPremierItemEx + pXGroupeOPC->dwNbItemsEx); dwNItem++)
				{
				PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_item, dwNItem);
				hr = pXItemOPC->pCOPCItemEx->hrDerniereErreur();
				if (FAILED (hr))
					{
					dwBitsStatutOPC |= ERREUR_CLIENT_OPC_CREATION_ITEM;
					MajVarStatutCOM (pXItemOPC->dwIdESStatut, hr);
					}
				}
			} // for (DWORD dwNGroupe = 
		} // if (bExcutionClientDemandee (pXClientOPC))
	else
		{
		if (pOPCClient->bExecutionEnCours())
			{
			// Desactivation de tous les groupes en lecture cache
			for (DWORD dwNGroupe = pXClientOPC->dwNPremierGroupeEx; dwNGroupe < (pXClientOPC->dwNPremierGroupeEx + pXClientOPC->dwNbGroupesEx); dwNGroupe++)
				{
				// Groupe en lecture ?
				PX_GROUPE_OPC pXGroupeOPC = (PX_GROUPE_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_groupe, dwNGroupe);
				if (pXGroupeOPC->IdSens == c_res_e)
					{
					// oui => activation / d�sactivation du rafraichissement ce groupe si lecture cache:
					BOOL bLectureDansCache = pXGroupeOPC->dwIdOperation == c_res_opc_entree_cache;
					COPCGroupe * pCOPCGroupe = pXGroupeOPC->pCOPCGroupeEx;

					// groupe en entr�e cache?
					if (bLectureDansCache)
						{
						// oui => d�sactive le rafraichissement du groupe 
						pCOPCGroupe->bActivation (FALSE);
						} // groupe en entr�e cache
					} // Groupe en lecture
				} //for (DWORD dwNGroupe

			// Arr�te le client
			pOPCClient->bFinExecute();
			} // if (pOPCClient->bExecutionEnCours)
		} // else (bExcutionClientDemandee (pXClientOPC))
	} // GereExecutionClient

//---------------------------------------------------------------------------
// Initialise l'ex�cution d'OPC
void initialise_opc (void)
	{
	BITS_ERREUR_CLIENT_OPC dwBitsStatutOPC = ERREUR_CLIENT_OPC_OK;

	// Init statut client_opc � Ok
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_STATUS_OPC, (FLOAT)ERREUR_CLIENT_OPC_OK);

	// Ouverture et initialisation DCOM
	bInitOPC = bDCOMInitialiseLocal (TRUE);
	
	// Signale si erreur DCOM
	if (!bInitOPC)
		dwBitsStatutOPC |= ERREUR_CLIENT_OPC_INIT_DCOM;

	// Configuration des clients OPC
	DWORD dwNbClients = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_opc_serveur);
	for (DWORD dwNClient = 1; dwNClient <= dwNbClients; dwNClient++)
		{
		// acc�s aux infos du serveur
		PX_CLIENT_OPC pXClientOPC = (PX_CLIENT_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_serveur, dwNClient);

		// Init statut du serveur � Ok
		MajVarStatutCOM (pXClientOPC->dwIdESStatut, NO_ERROR);

		// Cr�e un objet client OPC
		COPCClient * pOPCClient = new COPCClient();
		BOOL bOk = FALSE;

		// Configuration de l'acc�s au serveur
		// Objet cr�� : MAJ Pointeur sur objet Client en ex�cution
		pXClientOPC->pCOPCClientEx = pOPCClient;

		switch (pXClientOPC->dwIdAcces)
			{
			case c_res_opc_inproc:
				bOk = pOPCClient->bConfigureAccesServeur(pXClientOPC->szNomServeur, COPCClient::SERVEUR_INPROC);
				break;
			case c_res_opc_local:
				bOk = pOPCClient->bConfigureAccesServeur(pXClientOPC->szNomServeur, COPCClient::SERVEUR_LOCAL);
				break;
			case c_res_opc_remote:
				bOk = pOPCClient->bConfigureAccesServeur(pXClientOPC->szNomServeur, COPCClient::SERVEUR_REMOTE, pXClientOPC->szNomPoste);
				break;
			case libre:
				bOk = pOPCClient->bConfigureAccesServeur(pXClientOPC->szNomServeur, COPCClient::SERVEUR_AUTO);
				break;
			}

		// Configuration de l'acc�s Ok ?
		HRESULT hr = pOPCClient->hrDerniereErreur();
		if (FAILED (hr))
			{
			dwBitsStatutOPC |= ERREUR_CLIENT_OPC_CONNEXION_SERVEUR;
			MajVarStatutCOM (pXClientOPC->dwIdESStatut, hr);
			}

		// Autorise l'interruption automatique de connexion en cas d'erreur - si le client a une variable de connexion
		pOPCClient->AutoriseInterruptionExecution (pXClientOPC->dwIdESExecution != 0); 

		// Configuration des groupes du client
		for (DWORD dwNGroupe = pXClientOPC->dwNPremierGroupeEx; dwNGroupe < (pXClientOPC->dwNPremierGroupeEx + pXClientOPC->dwNbGroupesEx); dwNGroupe++)
			{
			PX_GROUPE_OPC pXGroupeOPC = (PX_GROUPE_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_groupe, dwNGroupe);

			// reset statut groupe
			MajVarStatutCOM (pXGroupeOPC->dwIdESStatut, NO_ERROR);
			
			// Configuration du groupe : demande de rafraichissement du groupe uniquement si lecture cache
			BOOL bGroupeActif = FALSE;
			BOOL bLectureDansCache = pXGroupeOPC->dwIdOperation == c_res_opc_entree_cache;
			if (bLectureDansCache)
				bGroupeActif = CPcsVarEx::bGetValVarLogEx(pXGroupeOPC->dwIdESRaf);

			pOPCClient->bAjouteGroupe (&pXGroupeOPC->nIdGroupeEx, NULL, pXGroupeOPC->szNomGroupe, bGroupeActif, pXGroupeOPC->dwCadenceMS);
			COPCGroupe * pCOPCGroupe = pOPCClient->pGetCOPCGroupe(pXGroupeOPC->nIdGroupeEx);
			pXGroupeOPC->pCOPCGroupeEx = pCOPCGroupe;

			// configuration des items du groupe
			for (DWORD dwNItem = pXGroupeOPC->dwNPremierItemEx; dwNItem < (pXGroupeOPC->dwNPremierItemEx + pXGroupeOPC->dwNbItemsEx); dwNItem++)
				{
				PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_item, dwNItem);
				
				// reset statut item
				MajVarStatutCOM (pXItemOPC->dwIdESStatut, NO_ERROR);
				//$$AC BOOL bActivation = bLectureDansCache && bGetValVarLogEx(pXItemOPC->dwIdESRaf);
				BOOL bActivation = CPcsVarEx::bGetValVarLogEx(pXItemOPC->dwIdESRaf);
			
				// Ajoute l'Item au groupe
				// V6.06 : adresse de l'item sur le serveur + 2 Extensions de l'adresse de l'item
				char szAdresseItemExtensionTemp[c_nb_car_ex_mes*3];
				StrCopy(szAdresseItemExtensionTemp,pXItemOPC->szAdresseItem);
				StrConcat(szAdresseItemExtensionTemp,pXItemOPC->szAdresseItemExt1);
				StrConcat(szAdresseItemExtensionTemp,pXItemOPC->szAdresseItemExt2);
				pCOPCGroupe->bAjouteItem (&pXItemOPC->nIdItemEx,
					NULL, szAdresseItemExtensionTemp, pXItemOPC->VarType, pXItemOPC->dwTaille, bActivation);
				pXItemOPC->pCOPCItemEx = pCOPCGroupe->pGetCOPCItem (pXItemOPC->nIdItemEx);
				}
			} // for (DWORD dwNGroupe

		// D�marrage du client demand� ?
		if (bExcutionClientDemandee(pXClientOPC))
			{
			// oui => lance le d�marrage
			GereExecutionClient(TRUE, dwBitsStatutOPC, pXClientOPC);
			MiseAjourVariablesDeConnexionClient(pXClientOPC, dwBitsStatutOPC);
			}
		} // for (DWORD dwNClient


	// MAJ �ventuelle du statut OPC
	if (dwBitsStatutOPC)
		MajStatutOPC (dwBitsStatutOPC);
	} // initialise_opc

//---------------------------------------------------------------------------
// Termine l'ex�cution d'Applicom
void ferme_opc (void)
	{
	DWORD dwNbClients = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_opc_serveur);

	// Parcours des serveurs dont on est client
	for (DWORD dwNClient = 1; dwNClient <= dwNbClients; dwNClient++)
		{
		PX_CLIENT_OPC pXClientOPC = (PX_CLIENT_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_serveur, dwNClient);
		
		// Arr�te l'ex�cution �ventuelle du client OPC
		BITS_ERREUR_CLIENT_OPC dwBitsStatutOPC = ERREUR_CLIENT_OPC_OK;
		GereExecutionClient(FALSE, dwBitsStatutOPC, pXClientOPC);
		MiseAjourVariablesDeConnexionClient(pXClientOPC, dwBitsStatutOPC);

		// Suppression de l'objet client OPC
		COPCClient * pOPCClient = pXClientOPC->pCOPCClientEx;
		pXClientOPC->pCOPCClientEx = NULL;
		delete pOPCClient;
		} //for (DWORD dwNClient

	if (bInitOPC)
		{
		// $$ mettre ailleurs car trop long
		// Fermeture et lib�ration DCOM
		DCOMFermeLocal (TRUE);
		bInitOPC = FALSE;
		}
	} // ferme_opc

//--------------------------------------------------------------------------
void recoit_opc (BOOL bPremier)
	{
	BITS_ERREUR_CLIENT_OPC dwBitsStatutOPC = ERREUR_CLIENT_OPC_OK;

	// Monte le statut OPC si erreur initialisation invalide
	if (!bInitOPC)
 		dwBitsStatutOPC |= ERREUR_CLIENT_OPC_INIT_DCOM;

	// Parcours des serveurs dont on est client
	DWORD dwNbClients = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_opc_serveur);
	for (DWORD dwNClient = 1; dwNClient <= dwNbClients; dwNClient++)
		{
		PX_CLIENT_OPC pXClientOPC = (PX_CLIENT_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_serveur, dwNClient);
		COPCClient * pOPCClient = pXClientOPC->pCOPCClientEx;

		// Ex�cution du client demand�e ?
		if (bExcutionClientDemandee(pXClientOPC))
			{
			// oui => lecture � faire :
			// Prise en compte erreur ex�cution serveur
			HRESULT hr = pOPCClient->hrExecution();
			if (FAILED (hr))
				{
				dwBitsStatutOPC |= ERREUR_CLIENT_OPC_CONNEXION_SERVEUR;
				MajVarStatutCOM (pXClientOPC->dwIdESStatut, hr);
				}

			// Serveur Existe : lecture groupe apr�s groupe
			for (DWORD dwNGroupe = pXClientOPC->dwNPremierGroupeEx; dwNGroupe < (pXClientOPC->dwNPremierGroupeEx + pXClientOPC->dwNbGroupesEx); dwNGroupe++)
				{
				// Groupe en lecture ?
				PX_GROUPE_OPC pXGroupeOPC = (PX_GROUPE_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_groupe, dwNGroupe);
				if (pXGroupeOPC->IdSens == c_res_e)
					{
					// oui => activation / d�sactivation du rafraichissement ce groupe si lecture cache:
					BOOL bGroupeALire = CPcsVarEx::bGetValVarLogEx(pXGroupeOPC->dwIdESRaf);
					BOOL bLectureDansCache = pXGroupeOPC->dwIdOperation == c_res_opc_entree_cache;
					COPCGroupe * pCOPCGroupe = pXGroupeOPC->pCOPCGroupeEx;

					// groupe en entr�e cache?
					if (bLectureDansCache)
						{
						// oui => active ou d�sactive le rafraichissement du groupe s'il est � lire ou pas
						if (!pCOPCGroupe->bActivation (bGroupeALire))
							{
							// Mise � jour du status serveur avec l'erreur OS du client
							MajVarStatutCOM (pXGroupeOPC->dwIdESStatut, pCOPCGroupe->hrDerniereErreur()); 
							dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ACTIVATION_GROUPE;
							}
						if (bGroupeALire)
							{
							// Activation / d�sactivation des items
							for (DWORD dwNItem = pXGroupeOPC->dwNPremierItemEx; dwNItem < (pXGroupeOPC->dwNPremierItemEx + pXGroupeOPC->dwNbItemsEx); dwNItem++)
								{
								PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_item, dwNItem);
								BOOL bActivation = CPcsVarEx::bGetValVarLogEx(pXItemOPC->dwIdESRaf);

								if (bActivation != pXItemOPC->pCOPCItemEx->bGetActivation())
									{
									pXItemOPC->pCOPCItemEx->SetActivation (bActivation, TRUE);
									}
							} // for item
							pCOPCGroupe->bActivationItems();

							hr = pXGroupeOPC->pCOPCGroupeEx->hrDerniereErreur();
							if (FAILED (hr))
								{
								MajVarStatutCOM (pXGroupeOPC->dwIdESStatut, hr);
								dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ACTIVATION_ITEM;
								}

							// Reset de l'information � transferer
							for (DWORD dwNItem = pXGroupeOPC->dwNPremierItemEx; dwNItem < (pXGroupeOPC->dwNPremierItemEx + pXGroupeOPC->dwNbItemsEx); dwNItem++)
								{
								PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_item, dwNItem);
								COPCItem * pCOPCItem = pXItemOPC->pCOPCItemEx;

								// Activation / d�sactivation item demand�e ?
								if (pCOPCItem->bGetATransferer())
									{
									// demarque l'item
									pCOPCItem->SetATransferer (FALSE);
									// recupere l'erreur
									HRESULT hr = pCOPCItem->hrDerniereErreur ();
									if (FAILED(hr))
										{
										MajVarStatutCOM (pXItemOPC->dwIdESStatut, hr);
										dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ACTIVATION_ITEM;
										}
									}
								}
							}// Activation / d�sactivation des items
						}

					// Groupe � lire ?
					if (bGroupeALire)
						{
						// Marque les items du groupe � lire
						for (DWORD dwNItem = pXGroupeOPC->dwNPremierItemEx; dwNItem < (pXGroupeOPC->dwNPremierItemEx + pXGroupeOPC->dwNbItemsEx); dwNItem++)
							{
							PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_item, dwNItem);
							pXItemOPC->pCOPCItemEx->SetATransferer (CPcsVarEx::bGetValVarLogEx(pXItemOPC->dwIdESRaf));
							}

						// lecture des items du groupe
						OPCDATASOURCE OpcDataSource = bLectureDansCache ? OPC_DS_CACHE : OPC_DS_DEVICE;
						pCOPCGroupe->bLectureSync (OpcDataSource);

						hr = pXGroupeOPC->pCOPCGroupeEx->hrDerniereErreur();
						if (FAILED (hr))
							{
							dwBitsStatutOPC |= ERREUR_CLIENT_OPC_LECTURE_GROUPE;
							MajVarStatutCOM (pXGroupeOPC->dwIdESStatut, hr);
							}

						// R�cup�re les r�sultats de la lecture item par item
						for (DWORD dwNItem = pXGroupeOPC->dwNPremierItemEx; dwNItem < (pXGroupeOPC->dwNPremierItemEx + pXGroupeOPC->dwNbItemsEx); dwNItem++)
							{
							PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_item, dwNItem);
							COPCItem * pCOPCItem = pXItemOPC->pCOPCItemEx;

							// Lecture item demand�e ?
							if (pCOPCItem->bGetATransferer())
								{
								// demarque les items du groupe
								pCOPCItem->SetATransferer (FALSE);

								// R�cup�ration Ok ?
								VARIANT varValeur;
								FILETIME TimeStamp;
								WORD wQuality;
								VariantInit (&varValeur);
								if (pCOPCItem->bGetValeur (&varValeur, pXItemOPC->VarType, &wQuality, &TimeStamp))
									{
									// oui => qualit� de l'item bonne ?
									if ((wQuality & OPC_QUALITY_MASK) == OPC_QUALITY_GOOD)
										{
										// oui => affecte la valeur lue � sa variable
										switch (pXItemOPC->IdGenre)
											{

											case c_res_logique:
												{
												if (pXItemOPC->dwTaille == 0) 
													{
													// variable simple
													if (pXItemOPC->VarType == VT_BOOL)
														{
														if (varValeur.boolVal == VARIANT_FALSE)
															{
															CPcsVarEx::SetValVarLogEx (pXItemOPC->dwIdES, FALSE);
															}
														else
															{
															CPcsVarEx::SetValVarLogEx (pXItemOPC->dwIdES, TRUE);
															}
														}
													else
														{
														wQuality = OPC_QUALITY_BAD;
														}
													}
												else
													{
													// variable tableau
													if (varValeur.parray != NULL)
														{
														LONG aIndex [1];
														VARIANT_BOOL vVal;
														HRESULT hr1 = S_OK;
														for (DWORD dwIndex = 0; ((dwIndex < pXItemOPC->dwTaille) && (hr1 == S_OK)); dwIndex++)
															{
															aIndex [0] = (LONG)dwIndex;
															hr1 = SafeArrayGetElement (varValeur.parray, aIndex, &vVal);
															if (vVal == VARIANT_FALSE)
																{
																CPcsVarEx::SetValVarLogEx(pXItemOPC->dwIdES+dwIndex, FALSE);
																}
															else
																{
																CPcsVarEx::SetValVarLogEx(pXItemOPC->dwIdES+dwIndex, TRUE);
																}
															}
														if (FAILED (hr1))
															{
															wQuality = OPC_QUALITY_BAD;
															}
														}
													else
														{
														wQuality = OPC_QUALITY_BAD;
														}
													} // Tableau
												}
												break;

											case c_res_numerique:
												{
												if (pXItemOPC->dwTaille == 0) 
													{
													// variable simple
													VARIANT vValeurR4;
													VariantInit (&vValeurR4);
													HRESULT hr = VariantChangeType (&vValeurR4, &varValeur, VARIANT_NOVALUEPROP, VT_R4);
													if (SUCCEEDED (hr))
														{
														CPcsVarEx::SetValVarNumEx (pXItemOPC->dwIdES, vValeurR4.fltVal);
														}
													else
														{
														wQuality = OPC_QUALITY_BAD;
														}
													VariantClear (&vValeurR4);
													}
												else
													{
													// variable tableau
													VARTYPE vtVarType = (pXItemOPC->VarType & (~VT_ARRAY));
													if (varValeur.parray != NULL) 
														{
														LONG aIndex [1];
														HRESULT hr1 = S_OK;
														for (DWORD dwIndex = 0; ((dwIndex < pXItemOPC->dwTaille) && (hr1 == S_OK)); dwIndex++)
															{
															aIndex [0] = (LONG)dwIndex;
															switch (vtVarType)
																{
																case VT_UI1:
																	{
																	BYTE vLue = 0;
																	hr1 = SafeArrayGetElement (varValeur.parray, aIndex, &vLue);
																	CPcsVarEx::SetValVarNumEx (pXItemOPC->dwIdES+dwIndex, (FLOAT)vLue);
																	}
																	break;
																case VT_UI2:
																	{
																	USHORT vLue = 0;
																	hr1 = SafeArrayGetElement (varValeur.parray, aIndex, &vLue);
																	CPcsVarEx::SetValVarNumEx (pXItemOPC->dwIdES+dwIndex, (FLOAT)vLue);
																	}
																	break;
																case VT_I2:
																	{
																	SHORT vLue = 0;
																	hr1 = SafeArrayGetElement (varValeur.parray, aIndex, &vLue);
																	CPcsVarEx::SetValVarNumEx (pXItemOPC->dwIdES+dwIndex, (FLOAT)vLue);
																	}
																	break;
																case VT_I4:
																	{
																	LONG vLue = 0;
																	hr1 = SafeArrayGetElement (varValeur.parray, aIndex, &vLue);
																	CPcsVarEx::SetValVarNumEx (pXItemOPC->dwIdES+dwIndex, (FLOAT)vLue);
																	}
																	break;
																case VT_R4:
																	{
																	FLOAT vLue = 0;
																	hr1 = SafeArrayGetElement (varValeur.parray, aIndex, &vLue);
																	CPcsVarEx::SetValVarNumEx (pXItemOPC->dwIdES+dwIndex, (FLOAT)vLue);
																	}
																	break;
																case VT_R8:
																	{
																	DOUBLE vLue = 0;
																	hr1 = SafeArrayGetElement (varValeur.parray, aIndex, &vLue);
																	CPcsVarEx::SetValVarNumEx (pXItemOPC->dwIdES+dwIndex, (FLOAT)vLue);
																	}
																	break;
																default:
																	break;
																}
															}
														if (FAILED (hr1))
															{
															wQuality = OPC_QUALITY_BAD;
															}
														}
													else
														{
														wQuality = OPC_QUALITY_BAD;
														}
													}
												}
												break;

											case c_res_message:
												{
												if (pXItemOPC->dwTaille == 0) 
													{
													// variable simple
													VARIANT vValeurBSTR;
													VariantInit (&vValeurBSTR);
													HRESULT hr = VariantChangeType (&vValeurBSTR, &varValeur, VARIANT_NOVALUEPROP, VT_BSTR);
													if (SUCCEEDED (hr))
														{
														CHAR szTexte[c_nb_car_ex_mes] = "";
														BOOL bCharDefautUtilise = FALSE;
														int iNbChars = SysStringLen(vValeurBSTR.bstrVal);
														int iRes = WideCharToMultiByte(
															CP_ACP,            // code page
															0,            // performance and mapping flags
															(LPCWSTR)vValeurBSTR.pbstrVal,    // wide-character string $$ VERIFIER
															iNbChars,          // number of chars in string.
															szTexte,     // buffer for new string
															c_nb_car_ex_mes-1,          // size of buffer
															" ",     // default for unmappable chars
															&bCharDefautUtilise  // set when default char used
															);
														if (iRes > 0)
															{
															CPcsVarEx::SetValVarMesEx (pXItemOPC->dwIdES, szTexte);
															}
														else
															{
															wQuality = OPC_QUALITY_BAD;
															}
														}
													else
														{
														wQuality = OPC_QUALITY_BAD;
														}
													VariantClear (&vValeurBSTR);
													}
												else
													{
													// variable tableau $$ A Terminer
													if (varValeur.parray != NULL)
														{
														LONG aIndex [1];
														// VARIANT_BOOL vVal;
														HRESULT hr1 = S_OK;
														for (DWORD dwIndex = 0; ((dwIndex < pXItemOPC->dwTaille) && (hr1 == S_OK)); dwIndex++)
															{
															aIndex [0] = (LONG)dwIndex;
															//hr1 = SafeArrayGetElement (varValeur.parray, aIndex, &vVal);
															//if (vVal == VARIANT_FALSE)
															//	{
															//	CPcsVarEx::SetValVarLogEx(pXItemOPC->dwIdES+dwIndex, FALSE);
															//	}
															//else
															//	{
															//	CPcsVarEx::SetValVarLogEx(pXItemOPC->dwIdES+dwIndex, TRUE);
															//	}
															}
														// $$ For�age non implant�
															wQuality = OPC_QUALITY_BAD;
														if (FAILED (hr1))
															{
															wQuality = OPC_QUALITY_BAD;
															}
														}
													else
														{
														wQuality = OPC_QUALITY_BAD;
														}
													} // Tableau
												}
												break;
											}
										}
									}
								else
									{
									wQuality = OPC_QUALITY_BAD;
									}
								VariantClear (&varValeur);

								// Mise � jour de la qualit� dans la BDD
								CPcsVarEx::SetValVarNumEx (pXItemOPC->dwIdESStatut, wQuality);

								// Monte un status si la qualit� n'est pas bonne
								if (wQuality != OPC_QUALITY_GOOD)
									{
									dwBitsStatutOPC |= ERREUR_CLIENT_OPC_LECTURE_ITEM;
									}
								} // if (pCOPCItem->bGetATransferer())
							} // for (DWORD dwNItem
						} // if (bGroupeActif)
					} // if (pXGroupeOPC->IdSens == c_res_e)
				} // for (DWORD dwNGroupe = 

			// Note l'�ventuelle interruption de connexion
			MiseAjourVariablesDeConnexionClient(pXClientOPC, dwBitsStatutOPC);

			} // if (bExcutionClientDemandee(pXClientOPC))
		} // for (DWORD dwNClient = 1; dwNClient <= dwNbClients; dwNClient++)

	// MAJ �ventuelle du statut OPC
	if (dwBitsStatutOPC)
		MajStatutOPC (dwBitsStatutOPC);
	} // recoit_opc

//---------------------------------------------------------------------------
void emet_opc 	(BOOL bPremier)
	{
	BITS_ERREUR_CLIENT_OPC dwBitsStatutOPC = ERREUR_CLIENT_OPC_OK;

	// Monte le statut OPC si erreur si initialisation invalide
	if (!bInitOPC)
 		dwBitsStatutOPC |= ERREUR_CLIENT_OPC_INIT_DCOM;

	// Parcours des serveurs dont on est client
	DWORD dwNbClients = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_opc_serveur);
	for (DWORD dwNClient = 1; dwNClient <= dwNbClients; dwNClient++)
		{
		PX_CLIENT_OPC pXClientOPC = (PX_CLIENT_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_serveur, dwNClient);
		COPCClient * pOPCClient = pXClientOPC->pCOPCClientEx;

		// Changement de l'�tat de Connexion ou d�connexion du client au serveur ?
		if (bExcutionClientDemandee(pXClientOPC) != pOPCClient->bExecutionEnCours())
			// oui => met � jour le nouvel �tat
			{
			GereExecutionClient(bExcutionClientDemandee(pXClientOPC), dwBitsStatutOPC, pXClientOPC);
			MiseAjourVariablesDeConnexionClient(pXClientOPC, dwBitsStatutOPC);
			}

		// Connexion demand�e ?
		if (bExcutionClientDemandee(pXClientOPC))
			{
			// oui => ecriture des sorties :
			// Prise en compte erreur ex�cution serveur
			HRESULT hr = pOPCClient->hrExecution();
			if (FAILED (hr))
				{
				dwBitsStatutOPC |= ERREUR_CLIENT_OPC_CONNEXION_SERVEUR;
				MajVarStatutCOM (pXClientOPC->dwIdESStatut, hr);
				}

			// Lecture groupe apr�s groupe
			for (DWORD dwNGroupe = pXClientOPC->dwNPremierGroupeEx; dwNGroupe < (pXClientOPC->dwNPremierGroupeEx + pXClientOPC->dwNbGroupesEx); dwNGroupe++)
				{
				// Groupe en �criture ?
				PX_GROUPE_OPC pXGroupeOPC = (PX_GROUPE_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_groupe, dwNGroupe);
				if (pXGroupeOPC->IdSens == c_res_s)
					{
					// oui => �criture des items du groupe :
					// D�termine le type de rafraichissement � faire
					BOOL bAuMoinsUneSortie = FALSE;
					BOOL bSortieRepetititive = CPcsVarEx::bGetValVarLogEx (pXGroupeOPC->dwIdESRaf);
					COPCGroupe * pCOPCGroupe = pXGroupeOPC->pCOPCGroupeEx;

					// R�cup�re les valeurs � �crire de chaque item du groupe
					for (DWORD dwNItem = pXGroupeOPC->dwNPremierItemEx; dwNItem < (pXGroupeOPC->dwNPremierItemEx + pXGroupeOPC->dwNbItemsEx); dwNItem++)
						{
						PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_item, dwNItem);
						COPCItem * pCOPCItem = pXItemOPC->pCOPCItemEx;

						// D�termine s'il faut �crire la variable
						BOOL bEcritureSortie = CPcsVarEx::bGetValVarLogEx (pXItemOPC->dwIdESRaf);

						// Si variable simple, test de changt d'�tat pour d�terminer si envoi � effectuer
						if (pXItemOPC->dwTaille == 0)
							{
							// Raf demand� ?
							if (bEcritureSortie && (!bSortieRepetititive))
								{
								// oui => Envoie sa valeur et informe le client qu'il faut l'�crire si Ok
								switch (pXItemOPC->IdGenre)
									{
									case c_res_logique:
										bEcritureSortie = CPcsVarEx::bGetBlocCourLogExValeurModifie(pXItemOPC->dwIdES);
										break;
									case c_res_numerique:
										bEcritureSortie = CPcsVarEx::bGetBlocCourNumExValeurModifie(pXItemOPC->dwIdES);
										break;
									//case c_res_message:
									//	varValeur.vt = VT_BSTR;
									//	bEcritureSortie = bSortieRepetititive || bGetBlocCourMesExValeurModifie(pXItemOPC->dwIdES);
									//	break; $$
									} // switch (pXItemOPC->IdGenre)
								} // if (bEcritureSortie && 
							}
						else
							{
							// variable tableau l'envoi est syst�matiquement effectu� si la raf de l'item est H
							}

						// Ecriture sortie demand� ?
						if (bEcritureSortie)
							{
							// oui => Envoie sa valeur et informe le client qu'il faut l'�crire si Ok
							VARIANT varValeur;
							SAFEARRAYBOUND aDim[1];
							
							VariantInit (&varValeur);
							switch (pXItemOPC->IdGenre)
								{
								case c_res_logique:
									{
									if (pXItemOPC->dwTaille == 0) 
										{
										// variable simple
										varValeur.vt = VT_BOOL;
										if (CPcsVarEx::bGetValVarLogEx(pXItemOPC->dwIdES))
											{
											varValeur.boolVal = VARIANT_TRUE;
											}
										else
											{
											varValeur.boolVal = VARIANT_FALSE;
											}
										}
									else
										{
										// tableau
										varValeur.vt = VT_ARRAY | VT_BOOL;
										aDim[0].lLbound = 0;
										aDim[0].cElements = (ULONG)pXItemOPC->dwTaille;
										varValeur.parray = SafeArrayCreate (VT_BOOL, 1,aDim);
										if (varValeur.parray != NULL)
											{
											HRESULT hr = S_OK;
											VARIANT_BOOL vVal;
											LONG aIndex [1];
											for (DWORD dwIndex = 0; ((dwIndex < pXItemOPC->dwTaille) && (hr == S_OK)); dwIndex++)
												{
												aIndex [0] = (LONG)dwIndex;
												if (CPcsVarEx::bGetValVarLogEx(pXItemOPC->dwIdES+dwIndex))
													{
													vVal = VARIANT_TRUE;
													}
												else
													{
													vVal = VARIANT_FALSE;
													}
												hr = SafeArrayPutElement (varValeur.parray, aIndex, &vVal);
												}
											if (FAILED (hr))
												{
												dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ECRITURE_ITEM;
												MajVarStatutCOM (pXItemOPC->dwIdESStatut, hr);
												}
											}
										else
											{
											// Erreur pour cet item $$il ne faudrait pas l'envoyer
											dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ECRITURE_ITEM;
											MajVarStatutCOM (pXItemOPC->dwIdESStatut, ERROR_OUTOFMEMORY);
											}
										}
									}
									break;

								case c_res_numerique:
									{
									if (pXItemOPC->dwTaille == 0) 
										{
										// variable simple
										VARIANT varValeurR4;
										VariantInit (&varValeurR4);
										varValeurR4.vt = VT_R4;
										varValeurR4.fltVal = CPcsVarEx::fGetValVarNumEx(pXItemOPC->dwIdES);
										HRESULT hr = VariantChangeType (&varValeur, &varValeurR4, VARIANT_NOVALUEPROP, pXItemOPC->VarType);
										if (FAILED (hr))
											{
											//$$il ne faudrait pas l'envoyer
											dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ECRITURE_ITEM;
											MajVarStatutCOM (pXItemOPC->dwIdESStatut, hr);
											}
										VariantClear (&varValeurR4);
										}
									else
										{
										// tableau
										VARTYPE vtVarType = (pXItemOPC->VarType & (~VT_ARRAY));
										varValeur.vt = pXItemOPC->VarType;
										aDim[0].lLbound = 0;
										aDim[0].cElements = (ULONG)pXItemOPC->dwTaille;
										varValeur.parray = SafeArrayCreate (vtVarType, 1,aDim);
										if (varValeur.parray != NULL)
											{
											HRESULT hr = S_OK;
											LONG aIndex [1];
											for (DWORD dwIndex = 0; ((dwIndex < pXItemOPC->dwTaille) && (hr == S_OK)); dwIndex++)
												{
												aIndex [0] = (LONG)dwIndex;
												switch (vtVarType)
													{
													case VT_UI1:
														{
														BYTE Val= (BYTE)CPcsVarEx::fGetValVarNumEx(pXItemOPC->dwIdES+dwIndex);
														hr = SafeArrayPutElement (varValeur.parray, aIndex, &Val);
														}
														break;
													case VT_UI2:
														{
														USHORT Val = (USHORT)CPcsVarEx::fGetValVarNumEx(pXItemOPC->dwIdES+dwIndex);
														hr = SafeArrayPutElement (varValeur.parray, aIndex, &Val);
														}
														break;
													case VT_I2:
														{
														SHORT Val = (SHORT)CPcsVarEx::fGetValVarNumEx(pXItemOPC->dwIdES+dwIndex);
														hr = SafeArrayPutElement (varValeur.parray, aIndex, &Val);
														}
														break;
													case VT_I4:
														{
														LONG Val = (LONG)CPcsVarEx::fGetValVarNumEx(pXItemOPC->dwIdES+dwIndex);
														hr = SafeArrayPutElement (varValeur.parray, aIndex, &Val);
														}
														break;
													case VT_R4:
														{
														FLOAT Val = CPcsVarEx::fGetValVarNumEx(pXItemOPC->dwIdES+dwIndex);
														hr = SafeArrayPutElement (varValeur.parray, aIndex, &Val);
														}
														break;
													case VT_R8:
														{
														DOUBLE Val = (DOUBLE)CPcsVarEx::fGetValVarNumEx(pXItemOPC->dwIdES+dwIndex);
														hr = SafeArrayPutElement (varValeur.parray, aIndex, &Val);
														}
														break;
													default:
														break;
													}
												}
											if (FAILED (hr))
												{
												dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ECRITURE_ITEM;
												MajVarStatutCOM (pXItemOPC->dwIdESStatut, hr);
												}
											}
										else
											{
											// Erreur pour cet item $$il ne faudrait pas l'envoyer
											dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ECRITURE_ITEM;
											MajVarStatutCOM (pXItemOPC->dwIdESStatut, ERROR_OUTOFMEMORY);
											}
										} // Tableau
									}
									break;

								default:
									varValeur.vt = VT_EMPTY; //$$ implanter les autres types
								//case c_res_message:
								//	varValeur.vt = VT_BSTR;
								//	break; $$
									break;
								}
							// Demande d'�criture de cet item accept�e ?
							if (pCOPCItem->bSetValeur (&varValeur, TRUE))
								bAuMoinsUneSortie = TRUE;
							else
								{
								// oui => Erreur pour cet item
								dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ECRITURE_ITEM;
								MajVarStatutCOM (pXItemOPC->dwIdESStatut, pCOPCItem->hrDerniereErreur ());
								}

							VariantClear (&varValeur);
							} // if (bEcritureSortie)
						} // for (DWORD dwNItem 

					// Demande l'�criture du groupe s'il y a au moins une sortie � faire
					if (bAuMoinsUneSortie)
						{
						pCOPCGroupe->bEcritureSync ();
						hr = pXGroupeOPC->pCOPCGroupeEx->hrDerniereErreur();
						if (FAILED (hr))
							{
							dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ECRITURE_GROUPE;
							MajVarStatutCOM (pXGroupeOPC->dwIdESStatut, hr);
							}

						// remont�e des statuts d'�criture de chaque item
						for (DWORD dwNItem = pXGroupeOPC->dwNPremierItemEx; dwNItem < (pXGroupeOPC->dwNPremierItemEx + pXGroupeOPC->dwNbItemsEx); dwNItem++)
							{
							PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_item, dwNItem);
							COPCItem * pCOPCItem = pXItemOPC->pCOPCItemEx;

							// D�termine s'il faut �crire la variable
							BOOL bEcritureSortie = CPcsVarEx::bGetValVarLogEx (pXItemOPC->dwIdESRaf);

							// Si variable simple, test de changt d'�tat pour d�terminer si envoi a �t� effectu�
							if (pXItemOPC->dwTaille == 0)
								{
								// Raf demand� ?
								if (bEcritureSortie && (!bSortieRepetititive))
									{
									switch (pXItemOPC->IdGenre)
										{
										case c_res_logique:
											bEcritureSortie = CPcsVarEx::bGetBlocCourLogExValeurModifie(pXItemOPC->dwIdES);
											break;
										case c_res_numerique:
											bEcritureSortie = CPcsVarEx::bGetBlocCourNumExValeurModifie(pXItemOPC->dwIdES);
											break;
										//case c_res_message:
										//	varValeur.vt = VT_BSTR;
										//	bEcritureSortie = bSortieRepetititive || bGetBlocCourMesExValeurModifie(pXItemOPC->dwIdES);
										//	break; $$
										}
									}
								}

							// Ecriture sortie demand� ?
							if (bEcritureSortie)
								{
								// r�cup�re le statut d'erreur de cet item
								HRESULT hr = pCOPCItem->hrDerniereErreur ();
								if (FAILED(hr))
									{
									MajVarStatutCOM (pXItemOPC->dwIdESStatut, hr);
									dwBitsStatutOPC |= ERREUR_CLIENT_OPC_ECRITURE_ITEM;
									}

								// Desactive le transfert
								pCOPCItem->SetATransferer(FALSE);

								// Remet automatiquement � bas la variable de raf du tableau
								if (pXItemOPC->dwTaille != 0)
									{
									CPcsVarEx::SetValVarLogEx (pXItemOPC->dwIdESRaf, FALSE);
									}
								} // if (bEcritureSortie)
							} // for (DWORD dwNItem 
						} // if (bAuMoinsUneSortie)
					} // if (pXGroupeOPC->IdSens == c_res_s)
				} // for (DWORD dwNGroupe ...
			} // if (bExcutionClientDemandee(pXClientOPC))

		MiseAjourVariablesDeConnexionClient(pXClientOPC, dwBitsStatutOPC);
		} // for (DWORD dwNClient = 1 ...

	// MAJ �ventuelle du statut OPC
	if (dwBitsStatutOPC)
		MajStatutOPC (dwBitsStatutOPC);
	} // emet_opc

//-----------------------------------fin Scrut_opc.c -----------------------------------


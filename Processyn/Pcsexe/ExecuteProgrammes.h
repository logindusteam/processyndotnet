// ExecuteProgrammes.h: interface for the CExecuteProgrammes class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EXECUTEPROGRAMMES_H__6A2956F4_7D56_11D2_A1A5_0000E8D90704__INCLUDED_)
#define AFX_EXECUTEPROGRAMMES_H__6A2956F4_7D56_11D2_A1A5_0000E8D90704__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// appel de pgm externe pour pcs exe
// WIN32 10/11/98

#define	NB_MAX_PROGRAMMES_LANCES_SIMULTANNES 10
class CExecuteProgrammes  
	{
	protected:
		// structure interne de contr�le d'un programme
		typedef struct
			{
			PROCESS_INFORMATION	ProcessInformation;
			CHAR szNom [MAX_PATH];
			} PROGRAMME, *PPROGRAMME;

	public:
		CExecuteProgrammes();
		virtual ~CExecuteProgrammes();

	public:
		// Lance un programme externe
		DWORD dwLance (PCSTR pszNomPgm, PCSTR pszParam); // renvoie une erreur OS

		// Test etat execution d'un programme externe 
		BOOL bExecutionEnCours (PCSTR pszNomPgm);

		// Passage en avant plan d'un programme externe
		DWORD dwActive (PCSTR pszNomPgm); // renvoie une erreur OS

		// Arret d'un programme externe
		DWORD dwArrete (PCSTR pszNomPgm);

		// Arr�te la prise en compte d'un programme sans l'arr�ter
		DWORD dwLibere (PCSTR pszNomPgm); // renvoie une erreur OS

	protected:
		// Renvoie le premier programme libre (non utilis�) ou NULL si plus de programme libre
		PPROGRAMME pProgrammeLibre (void);

		// Renvoie l'adresse d'un programme en cours d'ex�cution ou NULL sinon
		PPROGRAMME pProgrammeTrouve (PCSTR pszNomPgm);
	
	//membres
	protected:
		PROGRAMME m_aProgrammes [NB_MAX_PROGRAMMES_LANCES_SIMULTANNES];
	};

#endif // !defined(AFX_EXECUTEPROGRAMMES_H__6A2956F4_7D56_11D2_A1A5_0000E8D90704__INCLUDED_)

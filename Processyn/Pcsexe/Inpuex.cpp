/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il demeure sa propriete exclusive et est confidentiel.             |
 |   Aucune diffusion n'est possible sans accord ecrit.                 |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Input - Pcs-EXE                                          |
 |   Auteur  : JB                                                       |
 |   Date    : 12/02/93                                                 |
 |   Version :								|
 |                                                                      |
 |   Remarques :                                                        |
 |                                                                      |
 |   lire_commandes (.., MODE_DEMARAGE,..) interprette la ligne de cmd; |
 |      PCSEXE   [NomApplic]   --> Lance                                |
 |      PCSEXE /X:NomApplic    --> Lance                                |
 |      PCSEXE /x:NomApplic    --> Lance                                |
 |      PCSEXE /R:NomApplic    --> ReLance                              |
 |      PCSEXE /r:NomApplic    --> ReLance                              |
 |                                                                      |
 |   Si le Nom de l'application n'est pas sp�cifi� dans la ligne de cmd,|
 |   le nom est pris dans le fichier CMDPCS.                            |
 |                                                                      |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"                   // Types Standards
#include "mem.h"                   // pointe_enr ...
#include "UStr.h"                // Chaines de Caracteres: Strxxx ()
#include "lance.h"
#include "ActionEx.h"              // ajouter_action ()
#include "cmdpcs.h"                // LitNomFichier ()
#include "PathMan.h"           
#include "DocMan.h"           
#include "USem.h"           
#include "WExe.h"           
#include "Lng_res.h"           
#include "Tipe.h"           
#include "Appli.h"           
#include "Verif.h"           
#include "InpuEx.h"
VerifInit;

// Variables locales
static char pszLigneDeCommande [260];
static BOOL bCdeValide;
static HSEM	hSemAttenteActionsExe = NULL;//  Handle semaphore attente d'une nouvelle action

// Handle de la fen�tre au comportement "boite de dialogue" active
// pour le thread WinMain
HWND	hDlgGrMainActive = NULL;

// ---------------------------------------------------------------
// Boucle de message : traite un message re�u
// Renvoie TRUE si message != WM_QUIT
static BOOL bTraiteMessage (MSG * pmsg)
	{
	// boite de dialogue active ?
	if (hDlgGrMainActive)
		{
		// acc�l�rateur ?
		if (!TranslateAccelerator (Appli.hwnd, hAccelTouchesFonctions, pmsg))
			{
			// non => Message autre que Message dialogue ?
			if (!IsDialogMessage (hDlgGrMainActive, pmsg))
				{
				// oui => traite le
				TranslateMessage(pmsg);
				DispatchMessage (pmsg);
				}
			}
		}
	else
		{
		// non => traite le
		TranslateMessage(pmsg);
		DispatchMessage (pmsg);
		}

	return pmsg->message != WM_QUIT;
	} // bTraiteMessage


// --------------------------------------------------------------------------
// Ecoule tous les messages de cette queue de message
// Redirige les touches acc�l�ratrices sur Appli.hwnd
// et g�re la boite de dialogue active pour ce thread
// Renvoie FALSE si le message WM_QUIT est rencontr�
BOOL	bVideQueueDeMessage (void)
	{
	BOOL	bRes = TRUE;
	MSG		msg;

	// Y a t'il des messages dans la queue de message
	while (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
		{
		// oui => traite le
		if (!bTraiteMessage (&msg))
			bRes = FALSE;
		}
	return bRes;
	}

// --------------------------------------------------------------------------
// Queue d'action de WinMain PcsExe : attends qu'il y ait quelque chose � faire
static void AttenteActionsExe (void)
  {
	BOOL	bEnd = FALSE;

	// Attends une action
	Verif (bSemPrends(hSemAttenteActionsExe));

	// Attente d'un message ou d'une nouvelle commande
	if (!bVideQueueDeMessage ())
		bEnd = TRUE;

	while (!bEnd)
		{
		// Attente de l'arriv�e d'un message dans la queue ou en r�ception
		DWORD dwCause = MsgWaitForMultipleObjects (1, (PHANDLE)&hSemAttenteActionsExe, FALSE, INFINITE, QS_ALLINPUT);

		// traitement selon la cause de la fin de l'attente
		switch (dwCause)
			{
			// �v�nement li� � la queue de message
			case WAIT_OBJECT_0+1:
				if (!bVideQueueDeMessage ())
					bEnd = TRUE;
				break;

			// �v�nement li� � la r�ception d'une action
			case WAIT_OBJECT_0:
				// on sort pour la traiter
				bEnd = TRUE;
				break;

			case WAIT_TIMEOUT:			// time out (impossible car INFINITE)
			case WAIT_ABANDONED_0:	// Erreur en attente de synchro r�ception
			default:
				bEnd = TRUE;
				break;
			} // switch (dwCause)
		} // while (!bEnd)
  } // AttenteActionsExe

// --------------------------------------------------------------------------
// D�bloque l'attente de lecture de la queue d'actions
void ActionsExeLibereAttente (void)
  {
  bSemLibere (hSemAttenteActionsExe);
  }

// --------------------------------------------------------------------------
// analyse du bloc de commandes pour identifier la pr�sence du menu
// --------------------------------------------------------------------------
static BOOL bCommandesFenetreExeVisible (BOOL bCdeValide)
  {
	BOOL	bRetour = TRUE;
  if (bCdeValide)
    {
	  DWORD	wNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, BLOC_COMMANDES);
    if (wNbEnr)
      {
      for (DWORD wIndex = 1; wIndex <= wNbEnr; wIndex++)
        {
				PCOMMANDE_PCS	pCommandes = (PCOMMANDE_PCS)pointe_enr (szVERIFSource, __LINE__, BLOC_COMMANDES, wIndex);
        switch (pCommandes->carCmd)
          {
          case  '\0': // chaine sans commande
            break;

          case  'X':  // Lance
						bRetour = FALSE;		// pas de Menu sauf si /M derriere
            break;

          case  'R':  // Relance
						bRetour = FALSE;		// pas de Menu sauf si /M derriere
            break;

          case  'Q':  // quitte
            break;

          case  'M':  // Menu
						bRetour = TRUE;
            break;

          default:
            break;
          }  // ------------ switch (char_cmd)
        } // for
      }
    }
	return bRetour;
  }

// --------------------------------------------------------------------------
// analyse du bloc de commandes et ajout des actions associees
// --------------------------------------------------------------------------
static void InterpreteBlocCmd (BOOL bCdeValide)
  {
		t_param_action param_action={0,0,""};
  if (bCdeValide)
    {
 	  DWORD	wNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, BLOC_COMMANDES);
    if (wNbEnr)
      {
      for (DWORD wIndex = 1; wIndex <= wNbEnr; wIndex++)
        {
				PCOMMANDE_PCS	pCommandes = (PCOMMANDE_PCS)pointe_enr (szVERIFSource, __LINE__, BLOC_COMMANDES, wIndex);
        StrCopy (param_action.chaine, pCommandes->pszChaine);
        switch (pCommandes->carCmd)
          {
          case  '\0': // chaine sans commande
						{
						pszPathNameAbsolu (param_action.chaine, Appli.szPathInitial);
						if (bExtensionPresente (param_action.chaine, EXTENSION_A_FROID))
							pszSupprimeExt(param_action.chaine);
						if (bExtensionPresente (param_action.chaine, EXTENSION_A_CHAUD))
							pszSupprimeExt(param_action.chaine);
            ajouter_action (action_charge_applic_froide_mnu, &param_action);
            ajouter_action (action_charge_lien_dde, &param_action);
            ajouter_action (action_initialise_menu, &param_action);
						}
            break;

          case  'X':  // Lance
						{	// La fen�tre de l'application est elle visible ?
						pszPathNameAbsolu (param_action.chaine, Appli.szPathInitial);
						if (bExtensionPresente (param_action.chaine, EXTENSION_A_FROID))
							pszSupprimeExt(param_action.chaine);
            if (ExisteMenu())
              { // oui
              ajouter_action (action_init_ressources, &param_action);
              ajouter_action (action_charge_applic_froide_mnu, &param_action);
              ajouter_action (action_charge_lien_dde, &param_action);
              ajouter_action (action_lance_application_mnu, &param_action);
              }
            else
              { // non
              ajouter_action (action_init_ressources, &param_action);
              ajouter_action (action_charge_applic_froide, &param_action);
              ajouter_action (action_charge_lien_dde, &param_action);
              ajouter_action (action_lance_application, &param_action);
              }
						}
            break;

          case  'R':  // Relance
						{
						pszPathNameAbsolu (param_action.chaine, Appli.szPathInitial);
						if (bExtensionPresente (param_action.chaine, EXTENSION_A_CHAUD))
							pszSupprimeExt(param_action.chaine);
            if (ExisteMenu())
              { // il y a un menu
              ajouter_action (action_init_ressources, &param_action);
              ajouter_action (action_charge_applic_chaude_mnu , &param_action);
              ajouter_action (action_charge_lien_dde, &param_action);
              ajouter_action (action_relance_application_mnu, &param_action);
              }
            else
              { // il n'y a pas de menu
              ajouter_action (action_init_ressources, &param_action);
              ajouter_action (action_charge_applic_chaude, &param_action);
              ajouter_action (action_charge_lien_dde, &param_action);
              ajouter_action (action_relance_application, &param_action);
              }
						}
            break;

          case  'Q':  // quitte
            ajouter_action (action_quitte, &param_action);
            break;

          default:
            break;
          }  // switch (char_cmd)
        }
      enleve_bloc (BLOC_COMMANDES);
      }
    else
      { // rien ds la cde ni ds cmdpcs
			StrCopy (param_action.chaine, pszPathNameDoc());
      ajouter_action (action_charge_applic_froide_mnu, &param_action);
      ajouter_action (action_charge_lien_dde, &param_action);
      ajouter_action (action_initialise_menu, &param_action);
      }
    }
  else
    { // cde non valide
    param_action.entier = 0;
    ajouter_action (action_quitte, &param_action);
    }
  }

// --------------------------------------------------------------------------
// Lecture commandes ==> Ajouts Actions
// --------------------------------------------------------------------------
int lire_commandes (t_param_lecture *param_lecture)
  {
  t_param_action param_action={0,0,""};

  // initialisation du s�maphore d'attente d'actions $$ rajout ici pour initialisation unique
	if (!hSemAttenteActionsExe)
		hSemAttenteActionsExe = hSemCree (TRUE);

  switch (param_lecture->mode_lecture)
    {
    case MODE_INITIALISATION:
			{
      ajouter_action (action_initialisation, &param_action);
      StrCopy (pszLigneDeCommande,param_lecture->chaine);
      bCdeValide = FabriqueBlocCmd (pszLigneDeCommande);
		  BOOL bTemp = bCommandesFenetreExeVisible (bCdeValide);
      SetMenuExe (bTemp);
			}
      break;

    case MODE_FINALISATION:
			{
			param_action.entier = 0;
      ajouter_action (action_finalisation, &param_action);
			}
      break;

    case MODE_DEMARAGE:
      InterpreteBlocCmd (bCdeValide);
      break;

    case MODE_ENCHAINEMENT:
			{
      // ---------------
      // ICI, attente saisie utilisateur pour enchainement
      // ---------------
      if ((ExisteMenu())&(!RetourSysteme()))
        {
        AttenteActionsExe();
        }
      else
        {
        ajouter_action (action_quitte, &param_action);
        }
			}
      break;

    default:
      ajouter_action (action_rien, &param_action);
      break;
    }

  return 0;
  } // lire_commandes

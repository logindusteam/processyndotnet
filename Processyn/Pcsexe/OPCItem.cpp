// OPCItem.cpp: implementation of the COPCItem class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "opc.h"
#include "UDCOM.h"
#include "TemplateArray.h"
#include "OPCClient.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------
COPCItem::COPCItem (OPCHANDLE hClient, PCSTR pszItemID, VARTYPE vtRequestedDataType, DWORD dwTaille, BOOL bActif, PCSTR pszAccessPath)
	{
	m_hresult = NO_ERROR;

	// allocation �ventuelle m_pszAccessPath
	if (pszAccessPath)
		{
		PSTR pszTemp = (PSTR)malloc(strlen(pszAccessPath)+1);
		lstrcpy (pszTemp, pszAccessPath);
		m_pszAccessPath = pszTemp;
		}
	else
		m_pszAccessPath = NULL;

	// allocation m_pszItemID
	PSTR pszTemp = (PSTR)malloc(strlen(pszItemID)+1);
	lstrcpy (pszTemp, pszItemID);
	m_pszItemID = pszTemp;

	m_bActive = bActif;
	m_bATransferer  = FALSE;
	m_hClient = hClient;
	m_hServeur = NULL;
	m_vtCanonicalDataType = VT_ILLEGAL;
	m_dwTaille = dwTaille;
	m_dwAccessRights = 0; // Ni lecture ni �criture par d�faut
	m_ftTimeStamp.dwLowDateTime = 0;
	m_ftTimeStamp.dwHighDateTime = 0;
	m_wQuality = OPC_QUALITY_NOT_CONNECTED;
	VariantInit (&m_vDataValue);
	m_vDataValue.vt = vtRequestedDataType;;
	if ((vtRequestedDataType & VT_ARRAY) == VT_ARRAY)
		{
		SAFEARRAYBOUND aDim[1];
		aDim[0].lLbound = 0;
		aDim[0].cElements = (ULONG)dwTaille;
		m_vDataValue.parray = SafeArrayCreate ((vtRequestedDataType & ~VT_ARRAY), 1,aDim);
		}
	m_hrExecution = HR_NON_CONNECTE;
	}

//------------------------------------------------------------------------
COPCItem::~COPCItem()
	{
	// Lib�re les valeurs
	VariantClear (&m_vDataValue);

	// Libere les strings
	if (m_pszAccessPath)
		{
		free ((PVOID)m_pszAccessPath);
		m_pszAccessPath = NULL;
		}
	if (m_pszItemID)
		{
		free ((PVOID)m_pszItemID);
		m_pszItemID = NULL;
		}
	m_hrExecution = HR_NON_CONNECTE;
	}

//////////////////////////////////////////////////////////////////////
// Membres
//////////////////////////////////////////////////////////////////////

// Positionne l'information d'Activation et force l'info de transfert
// La m�thode bActivationItems de l'objet groupe associ� doit �tre appel�e pour forcer l'activation dans le serveur
void COPCItem::SetActivation (BOOL bActif, BOOL bATransferer) 
	{
	m_bActive = bActif;
	SetATransferer (bATransferer);
	}


//------------------------------------------------------------------------
// Lecture de la valeur courante de l'item et de ses infos
// Si tableau, c'est ici que l'on fait la creation du safarray en retour
// le clear doit etre fait dans l'appelant
BOOL COPCItem::bGetValeur (VARIANT * pVariantValeur, VARTYPE varTypeDesire, PWORD pwQuality, PFILETIME pTimeStamp)
	{
	HRESULT hr = S_OK;
	if ((m_vDataValue.vt & VT_ARRAY) == VT_ARRAY)
		{
		if (m_vDataValue.vt == varTypeDesire)
			{
			VariantInit(pVariantValeur);
			pVariantValeur->vt = m_vDataValue.vt;
			SAFEARRAYBOUND aDim[1];
			aDim[0].lLbound = 0;
			aDim[0].cElements = (ULONG)m_dwTaille;
			pVariantValeur->parray = SafeArrayCreate ((m_vDataValue.vt & ~VT_ARRAY), 1,aDim);
			if (pVariantValeur->parray != NULL)
				{
				hr = VariantCopy (pVariantValeur, &m_vDataValue);
				}
			else
				{
				hr = ERROR_OUTOFMEMORY;
				}
			}
		else
			{
			hr = E_FAIL; // type incompatible. les tableaux doivent �tre de m�me type
			}
		}
	else
		{
		hr = VariantChangeType (pVariantValeur, &m_vDataValue, VARIANT_NOVALUEPROP, varTypeDesire);
		}
	BOOL bRes = SUCCEEDED (hr);
	if (!bRes)
		SetErreur (hr);
	if (pwQuality)
		{
		if (bRes)
			*pwQuality = m_wQuality;
		else
			*pwQuality = OPC_QUALITY_BAD;
		}

	if (pTimeStamp)
		*pTimeStamp = m_ftTimeStamp;

	return bRes;
	}

//------------------------------------------------------------------------
// Affectation de valeur et force l'info de transfert
BOOL COPCItem::bSetValeur (VARIANT * pVariantValeur, BOOL bATransferer)
	{
	HRESULT hr = S_OK;
	if ((m_vDataValue.vt & VT_ARRAY) == VT_ARRAY)
		{
		if (m_vDataValue.vt == pVariantValeur->vt)
			{
			hr = VariantCopy (&m_vDataValue, pVariantValeur);
			}
		else
			{
			hr = E_FAIL;
			}
		}
	else
		{
		hr = VariantChangeType (&m_vDataValue, pVariantValeur, VARIANT_NOVALUEPROP, m_vDataValue.vt);
		}
	BOOL bRes = SUCCEEDED (hr);
	if (bRes)
		{
		SetATransferer (bATransferer);
		m_wQuality = OPC_QUALITY_GOOD; //$$
		}
	else
		{
		SetErreur(hr);
		SetATransferer (FALSE);
		m_wQuality = OPC_QUALITY_BAD;
		}
	return bRes;
	}

//------------------------------------------------------------------------
// Appel�s lors d'op�rations sur le groupe
//------------------------------------------------------------------------
// R�cup�re les infos de lancement de l'ex�cution de l'item
// !! Attention lib�rer les strings allou�es
void COPCItem::GetOPCItemDef (OPCITEMDEF * pItemDef)
	{
	if (m_pszAccessPath)
		pItemDef->szAccessPath = pWSTRFromSTR(m_pszAccessPath, 0);
	else
		pItemDef->szAccessPath = NULL;
	pItemDef->szItemID = pWSTRFromSTR(m_pszItemID, 0);
	pItemDef->bActive = m_bActive;
	pItemDef->hClient = m_hClient;
	pItemDef->dwBlobSize = 0;
	pItemDef->pBlob = NULL;
	pItemDef->vtRequestedDataType = m_vDataValue.vt;
	}

//------------------------------------------------------------------------
// Ex�cution : appel� lorsque des nouvelles donn�es de l'item ont �t� lues sur le serveur
HRESULT COPCItem::hrNouvelleValeur (FILETIME ftTimeStamp, WORD wQuality, VARIANT &vDataValue)
	{
	HRESULT hr = VariantCopy (&m_vDataValue, &vDataValue);

	if (SUCCEEDED (hr))
		{
		m_ftTimeStamp = ftTimeStamp;
		m_wQuality = wQuality;
		}
	else
		{
		m_wQuality = OPC_QUALITY_BAD; // $$ am�liorer le diagnostic
		}

	// renvoie le statut de la copie du variant
	return hr;
	}

//------------------------------------------------------------------------
// Ex�cution : appel� lorsque il y a eu une erreur de lecture sur l'item sur le serveur
void COPCItem::SetErreurLecture (HRESULT hr)
	{
	SetErreur(hr);
	m_wQuality = OPC_QUALITY_BAD; // $$
	}

//------------------------------------------------------------------------
// appel� lorsque l'item est cr�� sur le serveur
void COPCItem::DebutExecution (OPCHANDLE hServeur, VARTYPE vtCanonicalDataType, DWORD dwAccessRights, HRESULT hr)
	{
	m_hServeur = hServeur;
	m_vtCanonicalDataType = vtCanonicalDataType;
	m_dwAccessRights = dwAccessRights; // Ni lecture ni �criture par d�faut
	m_hrExecution = hr;
	SetErreur(NO_ERROR);
	}

//------------------------------------------------------------------------
// appel� lorsque la cr�ation de l'item sur le serveur a �chou�
void COPCItem::EchecExecution (HRESULT hr)
	{
	m_hServeur = NULL;
	m_dwAccessRights = 0; // Ni lecture ni �criture en cas d'�chec
	m_wQuality = OPC_QUALITY_NOT_CONNECTED;
	SetErreur(hr);
	m_hrExecution = hr;
	}

//------------------------------------------------------------------------
// appel� lorsque l'item est d�truit sur le serveur
void COPCItem::FinExecution (void)
	{
	m_hServeur = NULL;
	m_dwAccessRights = 0; // Ni lecture ni �criture par d�faut
	m_hrExecution = HR_NON_CONNECTE;
	m_wQuality = OPC_QUALITY_NOT_CONNECTED;
	}


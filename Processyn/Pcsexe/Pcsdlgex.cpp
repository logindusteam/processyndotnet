/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgex.c                                              |
 |   Auteur  :	MC							|
 |   Date    :	12/01/94						|
 |   Version :								|
 |                                                                      |
 |   Remarques : gestion des boites de dialogue dans gr                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "MemMan.h"
#include "Appli.h"
#include "UStr.h"
#include "UEnv.h"
#include "CheckMan.h"
#include "FMan.h"
#include "FileMan.h"
#include "USem.h"
#include "UChrono.h"
#include "dongle.h"
#include "produits.h"
#include "IdExeLng.h"
#include "ULangues.h"
#include "G_Objets.h"
#include "g_sys.h"
#include "BdGr.h"
#include "lng_res.h"
#include "tipe_gr.h"
#include "Driv_sys.h"
#include "Verif.h"
#include "pcsdlgex.h"
VerifInit;

static void centrage_boite (HWND hwnd, int offset);
static void init_zone_imp(HWND hwnd,t_dlg_liste_imp_ex *data);
static void met_a_jour_buffer_imp(HWND hwnd, DWORD item,DWORD choix_imp,char *choix_doc);

// -----------------------------------------------------------------------
static DWORD get_no_syn (char *choisie)
  {
  DWORD syn;
  char tampon [NB_MAX_CAR];

  if (StrSearchChar (choisie, '-') != STR_NOT_FOUND)
    {
    StrCopyUpToChar (tampon, choisie, '-');
    StrDeleteLastSpaces (tampon);
    if (!StrToDWORD (&syn, tampon))
      {
      syn = ((DWORD) -1);
      }
    }
  else
    {
    syn = ((DWORD) -1);
    }

  return(syn);
  }


// -----------------------------------------------------------------------
static void init_zone_imp(HWND hwnd,t_dlg_liste_imp_ex *data)
  {
  BOOL  visible;
  int nb_pages;
	DWORD	w;
  t_buff_syn_ex  *ptr_imp;
  char    chaine [NB_MAX_CAR];

  nb_pages = SendDlgItemMessage (hwnd, ID_IMP_LIST,LB_GETCOUNT,0L,0L);
  data->ptr_zone1 = pMemAlloue (sizeof(t_buff_syn_ex)*nb_pages);
  data->ptr_zone2 = pMemAlloue (sizeof(t_buff_syn_ex)*nb_pages);
  ptr_imp = (t_buff_syn_ex*) data->ptr_zone1;
  data->nb_synoptiques = nb_pages;

  chaine [0] = '\0';
  w = 1;
  do
    {
    (*data->ptr_proc_maj_liste) (&w,&visible,chaine);
    if (w != 0)
      {
      if (visible)
        {
        ptr_imp->no_synoptique = get_no_syn(chaine);
        ptr_imp->etat = FALSE;
        ptr_imp->index = w;
        ptr_imp++;
        }
      }
    }
    while (w != 0);
  }

// ------------------------------------------------------
// Centrage des boites
static void centrage_boite (HWND hwnd,int offset)
  {
  RECT rcl_hwnd;
  int x, y;

  ::GetWindowRect (hwnd, &rcl_hwnd);
  x = (Appli.sizEcran.cx - rcl_hwnd.right + rcl_hwnd.left) / 2;
  y = (Appli.sizEcran.cy - rcl_hwnd.bottom + rcl_hwnd.top) / 2;
  x += offset;
  y += offset;
  ::SetWindowPos (hwnd, HWND_TOP, x, y, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
  }

// ------------------------------------------------------
// programmation cle
static BOOL CALLBACK FNWP_EX6 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT           mres = 0;		   // Valeur de retour
  char              pszNum[80];
  t_dlg_release_cle *pDataReleaseCle;

  switch (msg)
    {

    case WM_INITDIALOG:
			{
			INT nItem = 0;
			char szTemp[80];

      pDataReleaseCle = (t_dlg_release_cle *) (PVOID) (mp2);
      pSelectEnv (hwnd, pDataReleaseCle);
			//
			nItem = SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_ADDSTRING, 0, (LPARAM)pszNomFonction(FONCTION_PCS_NORMAL));
			SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_SETITEMDATA, nItem, (LPARAM)(DWORD)FONCTION_PCS_NORMAL);
			if (pDataReleaseCle->iNumFonction == FONCTION_PCS_NORMAL)
				{
				SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_SETCURSEL, nItem, 0);
				}
			//
			nItem = SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_ADDSTRING, 0, (LPARAM)pszNomFonction(FONCTION_PCS_RUN_TIME));
			SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_SETITEMDATA, nItem, (LPARAM)(DWORD)FONCTION_PCS_RUN_TIME);
			if (pDataReleaseCle->iNumFonction == FONCTION_PCS_RUN_TIME)
				{
				SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_SETCURSEL, nItem, 0);
				}
			//
			nItem = SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_ADDSTRING, 0, (LPARAM)pszNomFonction(FONCTION_PCS_SD1));
			SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_SETITEMDATA, nItem, (LPARAM)(DWORD)FONCTION_PCS_SD1);
			if (pDataReleaseCle->iNumFonction == FONCTION_PCS_SD1)
				{
				SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_SETCURSEL, nItem, 0);
				}
			//
			StrDWordToStr(szTemp, pDataReleaseCle->dwNbVariables);
			SetDlgItemText (hwnd, ID_DLG_PGM_INP_VARIABLE, szTemp);
			//
      SetDlgItemText(hwnd,ID_DLG_PGM_INP_RELEASE, " ");

      mres = (LRESULT)TRUE;
			}
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
    	case IDOK:
					{
					INT nItem;

          pDataReleaseCle = (t_dlg_release_cle *)pGetEnv(hwnd);
          pDataReleaseCle->a_programmer = TRUE;
					nItem = SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_GETCURSEL, 0, 0);
					pDataReleaseCle->iNumFonction = SendDlgItemMessage (hwnd, ID_DLG_PGM_COMBO_FCT, CB_GETITEMDATA, nItem, 0);
          GetDlgItemText(hwnd,ID_DLG_PGM_INP_RELEASE, pszNum,80);
          StrCopy (pDataReleaseCle->szNumRelease,pszNum);
          GetDlgItemText(hwnd,ID_DLG_PGM_INP_VARIABLE, pszNum,80);
					StrToDWORD (&(pDataReleaseCle->dwNbVariables), pszNum);
          EndDialog(hwnd, TRUE);
					}
          break;

    	case IDCANCEL:
          EndDialog(hwnd, TRUE);
          break;

	default:
          break;
        }
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      break;

    case WM_DESTROY:
      break;

    default:
      mres = 0;
      break;
    }
  return (mres);
  }


// -------------diagnostic de cle------------------------------------------

static BOOL CALLBACK FNWP_EX5 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;		   // Valeur de retour
  BOOL     touche_traite = FALSE;

  // ------------ Switch message re�u.... --------------------------------
  switch (msg)
    {

    case WM_INITDIALOG:
			{
			int i = 106;
			char nom[12];
		  HFIC hFic;
		  char pszInfo[255];

      mres = (LRESULT)TRUE;
			StrSetNull (nom);
			spirale_n (&i, nom);

		  if (uFileTexteOuvre  (&hFic, nom, OF_DRIV_DQ) == 0)
				{
				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
        SetDlgItemText(hwnd,ID_DLG_DIAG0_EX,pszInfo); //modele cl�

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
        SetDlgItemText(hwnd,ID_DLG_DIAG1_EX,pszInfo); //num serie

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
        SetDlgItemText(hwnd,ID_DLG_DIAG2_EX,pszInfo); //produit

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
        SetDlgItemText(hwnd,ID_DLG_DIAG3_EX,pszInfo); //mode

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
        SetDlgItemText(hwnd,ID_DLG_DIAG4_EX,pszInfo); //version

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
				if (strcmp(pszInfo,"0")==0)
					StrCopy (pszInfo,"No limit");
        SetDlgItemText(hwnd,ID_DLG_DIAG5_EX,pszInfo); // nb variable

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
				if (strcmp(pszInfo,"0")==0)
					StrCopy (pszInfo,"No limit");
        SetDlgItemText(hwnd,ID_DLG_DIAG6_EX,pszInfo); // nb heure depannage

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
				DWORD dwNbTentatives = 0;
				if (StrToDWORD(&dwNbTentatives, pszInfo))
					{
					StrDWordToStr (pszInfo,5-dwNbTentatives);
					}

        SetDlgItemText(hwnd,ID_DLG_DIAG7_EX,pszInfo); // nb tentatives

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
        SetDlgItemText(hwnd,ID_DLG_DIAG8_EX,pszInfo); // produit logiciel

				uFileLireLn (hFic, pszInfo, 255);
				uFileLireLn (hFic, pszInfo, 255);
        SetDlgItemText(hwnd,ID_DLG_DIAG9_EX,pszInfo); // version logiciel

				uFileFerme (&hFic);

				StrDWordToStr (pszInfo, lit_nb_variables ());
				SetDlgItemText(hwnd,ID_DLG_DIAG10_EX,pszInfo); 
				}

      pSelectEnv (hwnd, (PVOID) (mp2));
			}
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
    		case ID_MODIFIER:
          touche_traite = LangueDialogBoxParam (MAKEINTRESOURCE (ID_DLG_PGM_EX), hwnd, FNWP_EX6, (LPARAM)pGetEnv (hwnd));
          EndDialog(hwnd, TRUE);
          break;

    		case IDCANCEL:
          EndDialog(hwnd, TRUE);
          break;

				default:
					break;
        }
      break;

    case WM_CHAR:
      if ((char) mp1 == VK_ESCAPE||(char) mp1 == VK_RETURN)
        {
        touche_traite = TRUE;
        mres = (LRESULT) TRUE;
        EndDialog(hwnd, FALSE);
        }
      if (!touche_traite)
        {
        mres = 0;
        }
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      break;

    case WM_DESTROY:
      break;

    default:
      mres = 0;
      break;
    }
  return (mres);
  }

// -----------------------------------------------------------------------

static void met_a_jour_buffer_imp (HWND hwnd, DWORD item, DWORD choix_imp, char *choix_doc)
  {
  t_dlg_liste_imp_ex*ptr_sur_data_a_init = (t_dlg_liste_imp_ex*)pGetEnv (hwnd); // recupere le pteur sur la structure
  t_buff_syn_ex*ptr_imp;
  t_buff_syn_ex*ptr_imp2;
  char                 pszTmp[80];
  char                 pszNumSyn[80];

  ptr_sur_data_a_init->a_ecrire = TRUE;
  ptr_imp = (t_buff_syn_ex*)ptr_sur_data_a_init->ptr_zone1;
  ptr_imp2 = (t_buff_syn_ex*)ptr_sur_data_a_init->ptr_zone2;
  ptr_imp  = ptr_imp + item;
  ptr_imp2  = ptr_imp2 + item;
  ptr_imp->etat = TRUE;
  ptr_imp->valeur = choix_imp;
  StrCopy (pszTmp, choix_doc);
  if (item !=0)
    {
    StrDWordToStr (pszNumSyn, ptr_imp->no_synoptique);
    if((StrLength (pszNumSyn) + StrLength (pszTmp)) > 8)
      {
      StrDelete (pszTmp, 8 - StrLength (pszNumSyn) - 1, StrLength (pszNumSyn));
      }
    StrConcat (pszTmp,pszNumSyn);
    }
  StrCopy (ptr_imp->message,pszTmp);
  *ptr_imp2 = *ptr_imp;
  }

// ---------------impression synoptiques-----------------------------------
static BOOL CALLBACK dlgprocListeImp (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT              mres = 0;		   // Valeur de retour
  t_dlg_liste_imp_ex  *ptr_sur_data_a_init;
	//  t_buff_syn_ex       *ptr_imp;
	//  t_buff_syn_ex       *ptr_imp2;
  BOOL              touche_traite = FALSE;
  BOOL              fin = FALSE;
  BOOL              refus;
  DWORD                 item;
  DWORD                 choix_imp;
  char                 choix_doc[80];
	
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// $$ v�rifier
			BOOL visible;
			char    chaine [NB_MAX_CAR];
			DWORD w;
			DWORD item;
			
      ptr_sur_data_a_init = (t_dlg_liste_imp_ex *) (PVOID) (mp2);
      pSelectEnv (hwnd, ptr_sur_data_a_init);
      SendDlgItemMessage (hwnd, ID_IMP_N1, EM_SETLIMITTEXT, 12, 0);
      SetDlgItemText(hwnd,ID_IMP_N1,ptr_sur_data_a_init->fen_document);
			chaine [0] = '\0';
			w = 1;
			do
				{
				(*ptr_sur_data_a_init->ptr_proc_maj_liste) (&w,&visible,chaine);
				if (w != 0)
					{
					if (visible)
						{
						item = LOWORD(SendDlgItemMessage (hwnd, ID_IMP_LIST, LB_INSERTSTRING,
							(WPARAM)-1, (LPARAM) chaine));
						}
					}
				} while (w != 0);
				init_zone_imp (hwnd, ptr_sur_data_a_init);
				mres = (LRESULT)TRUE;
			}
      break;
			
    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_IMP_OK:
          refus = FALSE;
					
          // r�cup�re l'index du texte s�lectionn�
          GetDlgItemText (hwnd,ID_IMP_N1,choix_doc,30);
          //item =  LOWORD(SendDlgItemMessage(hwnd, ID_IMP_LIST, LM_QUERYSELECTION, LIT_FIRST, 0));
					item =   SendDlgItemMessage (hwnd, ID_IMP_LIST, LB_GETCURSEL, 0, 0);
					
          choix_imp = (DWORD) nCheckIndex (hwnd,ID_IMP_R1);// $$LOWORD(SendDlgItemMessage (hwnd,ID_IMP_R1,BM_QUERYCHECKINDEX, 0, 0));
          switch (choix_imp)
            {
            case 0:
              choix_imp = 7;
							break;
							
            case 1:
              choix_imp = 9;
							break;
							
            case 2:
              choix_imp = 3;
							break;
							
            case 3:
              choix_imp = 5;
							break;
							
            default:
              choix_imp = 0;
							break;
            }
					
          //if (item == LIT_NONE)
					if (SendDlgItemMessage (hwnd, ID_IMP_LIST, LB_GETSEL, (WPARAM)item, 0) == 0)
            {
            if ((choix_imp == 7)||(choix_imp == 3)||(choix_imp == 0))
              {
              refus = TRUE;
              }
            else
              {
              item = 0;
              }
            }
					
          //if ((item != LIT_NONE)&&(choix_imp != 0))
          if (!refus)
            {
            met_a_jour_buffer_imp(hwnd,item,choix_imp,choix_doc);
            while (!fin)
              {
              // r�cup�re l'index du texte s�lectionn�
              // item =  LOWORD(SendDlgItemMessage(hwnd, ID_IMP_LIST, LM_QUERYSELECTION, item, 0L));
              // if (item == LIT_NONE)
							item =   SendDlgItemMessage (hwnd, ID_IMP_LIST, LB_GETCURSEL, 0, 0);
							if (SendDlgItemMessage (hwnd, ID_IMP_LIST, LB_GETSEL, (WPARAM)item, 0) == 0)
                break;
              met_a_jour_buffer_imp(hwnd,item,choix_imp,choix_doc);
              }
            EndDialog(hwnd, TRUE);
            }
          else
            {
            Beep(1000,100);
            }
          break;

				case IDCANCEL:
				case ID_IMP_ABANDON:
					EndDialog(hwnd, FALSE);
					break;
					
				default:
					break;
        } // switch(LOWORD(mp1))
      break;
			
		case WM_CLOSE:
			EndDialog(hwnd, FALSE);
			break;
			
		default:
			mres = 0;
			break;
    }
		return (mres);
  }

// -----------------------------------------------------------------------
// Boite de dialogue d'activation /d�sactivation de pages du synoptique
static BOOL CALLBACK dlgprocChoixPagesSynoptique (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
#define NB_MAX_PAGES_SELECTIONNEES	256	// Nb max de pages s�lectionn�es
  LRESULT              mres = 0;		   // Valeur de retour
  CHOIX_PAGES_AFFICHEES_EXE * pEnv;

  switch (msg)
    {
    case WM_INITDIALOG:
			{ 
			// Fait un instantan� des pages actives en m�moire :
			DWORD NPage = 1;
			DWORD item;
			t_buff_syn_ex  * p_buff_syn_ex;
			int i = 0;
			int NbPages = 0;

      pEnv = (CHOIX_PAGES_AFFICHEES_EXE *) (PVOID) (mp2);
      pSelectEnv (hwnd, pEnv);
      pEnv->a_ecrire = FALSE;

			// Remplis la list box des pages de synoptiques existantes :
			// pour chaque page du synoptique
			NPage = 1;
			do
				{
				char  szTitre [NB_MAX_CAR];
				BOOL	bVisible;

				// r�cup�re son titre, son activation et le num�ro de la page suivante 
				initialise_num_page_dlg (&NPage, &bVisible, szTitre); // (pEnv->ptr_proc_maj_liste) (&NPage,&visible,chaine);
				if (NPage != 0)
					{
					// ajoute son titre � la liste des pages et s�lectionne la si elle est visible
					item = LOWORD(SendDlgItemMessage (hwnd, ID_LIST_EX, LB_ADDSTRING, 0, (LPARAM)szTitre));
					if (bVisible)
						SendDlgItemMessage (hwnd, ID_LIST_EX, LB_SETSEL, TRUE, NbPages);

					// Une page de plus
					NbPages++;
					}
				} while (NPage != 0);

			// Cr�e en m�moire la liste des pages actives
			pEnv->nb_synoptiques = NbPages;
			pEnv->ptr_zone1 = pMemAlloue (sizeof(t_buff_syn_ex) * NbPages);
			p_buff_syn_ex = (t_buff_syn_ex*) pEnv->ptr_zone1;
			NPage = 1;

			// pour chaque page du synoptique
			do
				{
				char  szTitre [NB_MAX_CAR];
				BOOL  bVisible;

				// r�cup�re son titre, son activation et le num�ro de la page suivante
				initialise_num_page_dlg (&NPage, &bVisible,szTitre); //(*pEnv->ptr_proc_maj_liste) (&NPage,&visible,chaine);
				if (NPage != 0)
					{
					p_buff_syn_ex->no_synoptique = get_no_syn(szTitre);
					p_buff_syn_ex->index = i;
					p_buff_syn_ex->etat = FALSE;

					p_buff_syn_ex++;
					i++;
					}
				} while (NPage != 0);

      mres = TRUE;
			}
      break;

    case WM_CLOSE:
      EndDialog (hwnd, FALSE);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_LIST_EX:
					//double click sur la List box ?
					if (HIWORD(mp1) == LBN_DBLCLK) 
						// oui => valide la s�lection courante
						::PostMessage (hwnd, WM_COMMAND, IDOK, 0);
					break;

	    	case IDOK:
					// Validation de la boite de dialogue
          pEnv = (CHOIX_PAGES_AFFICHEES_EXE *)pGetEnv (hwnd);
					{
					// Fait un instantan� des pages actives en m�moire
					DWORD NPage = 1;
					t_buff_syn_ex  *p_buff_syn_ex;
					int i = 0;

					// r�cup�re l'index du texte s�lectionn�
					pEnv->a_ecrire = TRUE;
					p_buff_syn_ex = (t_buff_syn_ex*) pEnv->ptr_zone1;
					NPage = 1;

					// pour chaque page du synoptique
					do
						{
						char  chaine [NB_MAX_CAR];
						BOOL	visible;

						// r�cup�re son titre, son activation et le num�ro de la page suivante
						initialise_num_page_dlg (&NPage,&visible,chaine); //(*pEnv->ptr_proc_maj_liste) (&NPage,&visible,chaine);
						if (NPage != 0)
							{
							int nRes = SendDlgItemMessage (hwnd, ID_LIST_EX, LB_GETSEL, i, 0);

							VerifWarning (nRes !=LB_ERR);
							if ((DWORD)i == p_buff_syn_ex[i].index)
								p_buff_syn_ex[i].etat = nRes;

							i++;
							}
						} while (NPage != 0);

					// Valide la boite de dialogue
					EndDialog(hwnd, TRUE);
					}
          break;

				case IDCANCEL:
          EndDialog(hwnd, FALSE);
          break;
        }
      break;

    default:
      mres = 0;
      break;
    }
  return mres;
  } // dlgprocChoixPagesSynoptique

// -----------------------------------------------------------------------
// appel de la boite de dialogue Choix Pages Synoptique
BOOL  dlg_liste_syn_ex (HWND hwnd, CHOIX_PAGES_AFFICHEES_EXE *data)
  {
  int nRes = LangueDialogBoxParam (MAKEINTRESOURCE (ID_DLG_SYN_EX), hwnd, dlgprocChoixPagesSynoptique, (LPARAM)data);
  
	// erreur => renvoie FALSE
	if (nRes == -1)
		nRes = 0;

  return nRes;
  }

// -----------------------------------------------------------------------
BOOL  dlg_liste_imp_ex (HWND hwnd, t_dlg_liste_imp_ex *data)
  {
  return LangueDialogBoxParam (MAKEINTRESOURCE(ID_DLG_IMP_EX),hwnd, dlgprocListeImp, (LPARAM)data);
  }


// -----------------------------------------------------------------------
BOOL  dlg_diag_ex (HWND hwnd, t_dlg_release_cle *data)
  {
  return LangueDialogBoxParam (MAKEINTRESOURCE(ID_DLG_DIA_EX), hwnd, FNWP_EX5, (LPARAM)data);
  }


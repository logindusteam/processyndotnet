// --------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"        // Types standard
#include "lng_res.h"
#include "tipe.h"       //
#include "Pcs_sys.h"    //
#include "memman.h" 
#include "mem.h"        // Memory Manager
#include "UStr.h"
#include "ULpt.h"
#include "driv_im.h"     // Driver Imprimante.........................Prnxxx()
#include "PcsVarEx.h"
#include "scrut_im.h"
#include "Verif.h"
VerifInit;

// --------------------------------------------------------------------------
#define IM_STATUS_OK                    0x0000
// -------------------------------------------- Imprimante s�rie
#define IM_STATUS_QUEUE_OVERRUN         0x0001
#define IM_STATUS_HARD_OVERRUN          0x0002
#define IM_STATUS_PARITY_ERROR          0x0004
#define IM_STATUS_FRAMING_ERROR         0x0008
// -------------------------------------------- Imprimante parallele
#define IM_STATUS_ACKNOWLEDGED          0x0001
#define IM_STATUS_OUT_OF_PAPER          0x0002
#define IM_STATUS_NOT_SELECTED          0x0004
#define IM_STATUS_IO_ERROR              0x0008
#define IM_STATUS_TIMEOUT               0x0010
// -------------------------------------------- Imprimante g�n�ral
#define IM_STATUS_xxx                   0x0020
#define IM_STATUS_WRITE                 0x0040
#define IM_STATUS_TIMEOUT_WRITE         0x0080
#define IM_STATUS_BUSY                  0x0100
#define IM_STATUS_OVERFLOW              0x0200
#define IM_STATUS_NO_PRINTER            0x0400
#define IM_STATUS_INIT_ERROR            0x0800
#define IM_STATUS_OPEN_ERROR            0x1000
#define IM_STATUS_PRINTER_REDEFINITION  0x2000
#define IM_STATUS_INVALID_PRINTER       0x4000
#define IM_STATUS_SYSTEM_ERROR          0x8000

// --------------------------------------------------------------------------
#define CAR_XON  '\021'      // Code ASCII en OCTAL == [CTL] + Q  == CHR (17)
#define CAR_XOFF '\023'      // Code ASCII en OCTAL == [CTL] + S  == CHR (19)

// --------------------------------------------------------------------------
#define PSZ_A_LA_LIGNE    "\x0D\x0A"  // Code ASCII en HEXA == CR + LF
#define NB_CAR_A_LA_LIGNE     2       // ..................... CR + LF

// --------------------------------------------------------------------------
#define bx_imprimantes 52

// --------------------------------------------------------------------------
typedef struct
  {
  BOOL	bOuverte;
  BOOL	bEnTimeOut;
  DWORD	wNbLgnCycle;
  UINT	wTailleMaxBuf;
  UINT	wTailleBuf;
  PBYTE	pbBuffer;
  } X_IMPRIMANTE, *PX_IMPRIMANTE;


// --------------------------------------------------------------------------
// Bit: Hexa   : Deci  : Constantes                  :
// ---+--------+-------+-----------------------------+-----------------------
//  - : 0x0000 :     0 :OK                           :
//  0 : 0x0001 :     1 :ACKNOWLEDGED ou QUEUE_OVERRUN:-+
//  1 : 0x0002 :     2 :OUT_OF_PAPER ou HARD_OVERRUN : | Status Retourn� par
//  2 : 0x0004 :     4 :NOT_SELECTED ou PARITY_ERROR : | PrnTxt
//  3 : 0x0008 :     8 :IO_ERROR     ou FRAMING_ERROR: |
//  4 : 0x0010 :    16 :TIMEOUT                      :-+
//  5 : 0x0020 :    32 :                             :
//  6 : 0x0040 :    64 :WRITE                        :
//  7 : 0x0080 :   128 :TIMEOUT_WRITE                :
//  8 : 0x0100 :   256 :BUSY                         :
//  9 : 0x0200 :   512 :OVERFLOW                     :
// 10 : 0x0400 :  1024 :NO_PRINTER                   :
// 11 : 0x0800 :  2048 :INIT_ERROR                   :
// 12 : 0x1000 :  4096 :OPEN_ERROR                   :
// 13 : 0x2000 :  8192 :PRINTER_REDEFINITION         :
// 14 : 0x4000 : 16384 :INVALID_PRINTER              :
// 15 : 0x8000 : 32768 :SYSTEM_ERROR                 :
// ---+--------+-------+-----------------------------+-----------------------

//---------------------------------------------------------------------
// Conversion Erreur OS en Statut de l'imprimante
		//#define PRN_STATUS_OK                    0x0000
		//#define PRN_STATUS_ACKNOWLEDGED          0x0001
		//#define PRN_STATUS_OUT_OF_PAPER          0x0002
		//#define PRN_STATUS_NOT_SELECTED          0x0004
		//#define PRN_STATUS_IO_ERROR              0x0008
		//#define PRN_STATUS_TIMEOUT               0x0010
		//#define PRN_STATUS_OTHER_ERROR           0x0020 // $$ nouveau Windows 95
static void CvtErrorOSToStatut (DWORD * pdwStatut)
	{
	if (*pdwStatut != NO_ERROR)
		{
		PX_VAR_SYS	pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, status_systeme);
		PFLOAT			prValeur = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pwPosition->dwPosCourEx));
		(*prValeur)= (FLOAT) (*pdwStatut);
		switch (*pdwStatut)
			{
			case ERROR_IO_INCOMPLETE:break;
			case ERROR_NOT_READY: *pdwStatut = PRN_STATUS_NOT_SELECTED; break;
			case ERROR_OUT_OF_PAPER: *pdwStatut = PRN_STATUS_OUT_OF_PAPER; break;
				break;
			default :
				{
				*pdwStatut = PRN_STATUS_OTHER_ERROR; 
				break;
				}
			}
		}
	}

//---------------------------------------------------------------------
static void maj_status_imprimante (UINT dwImp, DWORD wStatusToSet)
  {
  PX_VAR_SYS	pwPositionPremier = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, VA_SYS_STATUS_IMPRIMANTE);
  PFLOAT			prValeur = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pwPositionPremier->dwPosCourEx) + dwImp - 1);
  DWORD       wStatus = (DWORD) (*prValeur);

  wStatus    = wStatus | wStatusToSet;
  (*prValeur)= (FLOAT) wStatus;
  }

// --------------------------------------------------------------------------
static BOOL bNumeroImprimanteOk (DWORD dwImp)
	{
	BOOL bOk;
	DWORD dwNbImp;

  dwNbImp = nb_enregistrements (szVERIFSource, __LINE__, bx_imprimantes);
  bOk = ((dwImp > 0) && (dwImp <= dwNbImp));
  if (!bOk) // on monte le statut de toutes les imprimantes puisque le numero pass� ne permet pas d'acc�der � un statut
		{
    for (dwImp = 1; dwImp <= dwNbImp; dwImp++)
      {
      maj_status_imprimante (dwImp, IM_STATUS_INVALID_PRINTER);
      }
		}
	return (bOk);
	}

// --------------------------------------------------------------------------
// renvoie true si deja ouverte. Fait une tentative d'ouverture sinon
static BOOL bOuvrirImprimante (DWORD dwImp)
	{
  PX_IMPRIMANTE pxImprimante = (PX_IMPRIMANTE)pointe_enr (szVERIFSource, __LINE__, bx_imprimantes, dwImp);
	if (!pxImprimante->bOuverte)
		{
		DWORD             wPort=0;
		BOOL							bIsSerial=FALSE;
		LPRN_CONFIG_COM		ConfigCom;
		definition_port_imprimante (dwImp, &wPort, &ConfigCom.wBaudRate, &ConfigCom.wDataBits, &ConfigCom.wParity, &ConfigCom.wStopBits, &bIsSerial);
		ConfigCom.cXOn  = CAR_XON;
		ConfigCom.cXOff = CAR_XOFF;

		CLpt::ERREUR_LPT lptError = LImprimanteOuvre (dwImp, wPort, &ConfigCom);

		switch (lptError)
			{
			case CLpt::ERREUR_LPT_NONE:
				pxImprimante->bOuverte = TRUE;
				break;
			case CLpt::ERREUR_LPT_OPEN:
				maj_status_imprimante (dwImp, IM_STATUS_OPEN_ERROR);
				break;
			case CLpt::ERREUR_LPT_PORT_REDEFINITION:
				maj_status_imprimante (dwImp, IM_STATUS_PRINTER_REDEFINITION);
				break;
			case CLpt::ERREUR_LPT_PORT_UNDEFINE:
				maj_status_imprimante (dwImp, IM_STATUS_NO_PRINTER);
				break;
			default:
				maj_status_imprimante (dwImp, IM_STATUS_OPEN_ERROR);
				break;
			}
		}
	return (pxImprimante->bOuverte);
	}

// --------------------------------------------------------------------------
// renvoie true si deja fermee. Fait une tentative de fermeture sinon
static BOOL bFermerImprimante (DWORD dwImp, BOOL bFermeCanalEnPlusDuPort)
	{
  PX_IMPRIMANTE			pxImprimante = (PX_IMPRIMANTE)pointe_enr (szVERIFSource, __LINE__, bx_imprimantes, dwImp);
	if (pxImprimante->bOuverte)
		{
		pxImprimante->bOuverte = FALSE;
    CLpt::ERREUR_LPT lptError = LImprimanteFerme (dwImp);
    if (bFermeCanalEnPlusDuPort)
			MemLibere ((PVOID *)(&pxImprimante->pbBuffer));

		switch (lptError)
			{
			case CLpt::ERREUR_LPT_NONE:
				pxImprimante->bOuverte = FALSE;
				break;
			case CLpt::ERREUR_LPT_OPEN:
				maj_status_imprimante (dwImp, IM_STATUS_OPEN_ERROR);
				break;
			case CLpt::ERREUR_LPT_PORT_REDEFINITION:
				maj_status_imprimante (dwImp, IM_STATUS_PRINTER_REDEFINITION);
				break;
			case CLpt::ERREUR_LPT_PORT_UNDEFINE:
				maj_status_imprimante (dwImp, IM_STATUS_NO_PRINTER);
				break;
			default:
				maj_status_imprimante (dwImp, IM_STATUS_OPEN_ERROR);
				break;
			}
		}
	return (!pxImprimante->bOuverte);
	} // bFermerImprimante

// --------------------------------------------------------------------------
static DWORD CalculTailleAImprimer (PX_IMPRIMANTE	pxImprimante)
  {
  UINT wTailleAImprimer = 0;
  //
  if (pxImprimante->wTailleBuf > 0)
    {
    if (pxImprimante->wTailleBuf > NB_CAR_A_LA_LIGNE)
      {
			UINT wNbLgn = 0;
      while ((wNbLgn < pxImprimante->wNbLgnCycle) && (wTailleAImprimer < (pxImprimante->wTailleBuf - NB_CAR_A_LA_LIGNE)))
        {
        if (MemCompare (&pxImprimante->pbBuffer [wTailleAImprimer], PSZ_A_LA_LIGNE, NB_CAR_A_LA_LIGNE) == 0)
          {
          wNbLgn++;
          wTailleAImprimer = wTailleAImprimer + NB_CAR_A_LA_LIGNE;
          }
        else
          {
          wTailleAImprimer++;
          }
        }
      if (wNbLgn < pxImprimante->wNbLgnCycle)
        {
        wTailleAImprimer = pxImprimante->wTailleBuf;
        }
      }
    else
      {
      wTailleAImprimer = pxImprimante->wTailleBuf;
      }
    }

  return (wTailleAImprimer);
  }

// --------------------------------------------------------------------------
void ouvrir_imprimantes (void)
  {
  if (existe_repere (bx_imprimantes))
    {
    enleve_bloc (bx_imprimantes);
    }
  cree_bloc (bx_imprimantes, sizeof (X_IMPRIMANTE), NB_MAX_PRN);

	// Init data et r�servation buffer de chaque imprimante configur�e par d�faut
  for (DWORD dwImp = 1; dwImp <= NB_MAX_PRN; dwImp++)
    {
		DWORD wSizeBuf = 0;
		DWORD wLinePerCycle = 0;
    definition_buffer_imprimante (dwImp, &wSizeBuf, &wLinePerCycle);
		PX_IMPRIMANTE	pxImprimante = (PX_IMPRIMANTE)pointe_enr (szVERIFSource, __LINE__, bx_imprimantes, dwImp);
    pxImprimante->bOuverte      = FALSE;
    pxImprimante->bEnTimeOut    = FALSE;
    pxImprimante->wNbLgnCycle   = wLinePerCycle;
    pxImprimante->wTailleMaxBuf = 1024 * wSizeBuf;
    pxImprimante->wTailleBuf    = 0;
    pxImprimante->pbBuffer      = NULL;
    //
    if (pxImprimante->wNbLgnCycle > 0)
      {
      pxImprimante->pbBuffer = (PBYTE)pMemAlloue (pxImprimante->wTailleMaxBuf);
      }
    }
  }

// --------------------------------------------------------------------------
void fermer_imprimantes (void)
  {
  DWORD	dwImp;
  DWORD	wNbImp = nb_enregistrements (szVERIFSource, __LINE__, bx_imprimantes);

  for (dwImp = 1; dwImp <= wNbImp; dwImp++)
    {
		bFermerImprimante (dwImp, TRUE);
    }
  //
  enleve_bloc (bx_imprimantes);
  }

// --------------------------------------------------------------------------
void stocker_imprimante (UINT wNbCar, char *BufCar, BOOL bALaLigne, BOOL bAvecConversion)
  {
  UINT dwImp = (UINT) CPcsVarEx::fGetValVarSysNumEx(imprimante);

  if (bNumeroImprimanteOk (dwImp))
    {
		PX_IMPRIMANTE	pxImprimante = (PX_IMPRIMANTE)pointe_enr (szVERIFSource, __LINE__, bx_imprimantes, dwImp);

		if (pxImprimante->pbBuffer != NULL)
			{
			DWORD wTailleAStocker = wNbCar;
			if (bALaLigne)
				{
				wTailleAStocker = wTailleAStocker + NB_CAR_A_LA_LIGNE;
				}
			//
			if (pxImprimante->wTailleBuf + wTailleAStocker <= pxImprimante->wTailleMaxBuf)
				{
				MemCopy (&pxImprimante->pbBuffer [pxImprimante->wTailleBuf], BufCar, wNbCar);

				// Conversion ansi - ascii pr�sum�e utilis� sur les imprimantes
				if (bAvecConversion)
					CharToOemBuff ((PCSTR)&pxImprimante->pbBuffer [pxImprimante->wTailleBuf], (PSTR)&pxImprimante->pbBuffer [pxImprimante->wTailleBuf], wNbCar);
				// 

				pxImprimante->wTailleBuf = pxImprimante->wTailleBuf + wNbCar;

				if (bALaLigne)
					{
					MemCopy (&pxImprimante->pbBuffer [pxImprimante->wTailleBuf], PSZ_A_LA_LIGNE, NB_CAR_A_LA_LIGNE);
					pxImprimante->wTailleBuf = pxImprimante->wTailleBuf + NB_CAR_A_LA_LIGNE;
					}
				}
			else
				{
				maj_status_imprimante (dwImp, IM_STATUS_OVERFLOW);
				}
			}
		else // Nbligneparcycle = 0 => pas de place r�serv�e pour le buffer
			{
			maj_status_imprimante (dwImp, IM_STATUS_NO_PRINTER);
			}
    } // bNumeroImprimanteOk
  }

// --------------------------------------------------------------------------
void scruter_imprimantes (void)
  {
  DWORD          wNbImp;
  UINT           dwImp;
  CLpt::ERREUR_LPT     wError = CLpt::ERREUR_LPT_NONE;
  BOOL		       bWaitWrite;
  DWORD			     wTailleAImprimer;
  DWORD          wStatusWrite;
  DWORD          wSizeBufWritten;

  wNbImp = nb_enregistrements (szVERIFSource, __LINE__, bx_imprimantes);
  for (dwImp = 1; dwImp <= wNbImp; dwImp++)
    {
		PX_IMPRIMANTE	pxImprimante = (PX_IMPRIMANTE)pointe_enr (szVERIFSource, __LINE__, bx_imprimantes, dwImp);
    bWaitWrite = FALSE;
    if (pxImprimante->bEnTimeOut)
      {
      pxImprimante->bEnTimeOut = FALSE;
      bWaitWrite = TRUE;
      }
    else
      {
      wTailleAImprimer = CalculTailleAImprimer (pxImprimante);
      if (wTailleAImprimer > 0)
        {
				if (bOuvrirImprimante (dwImp))
					{
					wError = LImprimanteWriteAsync (dwImp, wTailleAImprimer, pxImprimante->pbBuffer, &wStatusWrite, FALSE);
					if ((wError == CLpt::ERREUR_LPT_NONE) || (wError == CLpt::ERREUR_LPT_STATUS_WRITE))
						{
						bWaitWrite = TRUE;
						}
					else
						{
						maj_status_imprimante (dwImp, IM_STATUS_NO_PRINTER);
						}
					}
        }
      }
    // Attends si n�cessaire, � concurrence d'une dur�e de 1000 ms max, que l'�criture se termine
    if (bWaitWrite)
      {
			wError = LImprimanteWaitWrite (dwImp, &wStatusWrite, &wSizeBufWritten);
			switch (wError)
				{
				case CLpt::ERREUR_LPT_NONE:
					MemMove (&pxImprimante->pbBuffer [0], &pxImprimante->pbBuffer [wSizeBufWritten], pxImprimante->wTailleBuf - wSizeBufWritten);
					pxImprimante->wTailleBuf = pxImprimante->wTailleBuf - wSizeBufWritten;
					bFermerImprimante (dwImp,FALSE); // Permet de terminer le document si utilisation d'un spooler
					break;

				case CLpt::ERREUR_LPT_STATUS_WRITE:
					CvtErrorOSToStatut (&wStatusWrite);
					maj_status_imprimante (dwImp, wStatusWrite);
					bFermerImprimante (dwImp,FALSE); // Permet de terminer le document si utilisation d'un spooler
					break;

				case CLpt::ERREUR_LPT_WRITE_TIME_OUT:
					pxImprimante->bEnTimeOut = TRUE;
					maj_status_imprimante (dwImp, IM_STATUS_TIMEOUT_WRITE);
					break;

				default:
					bFermerImprimante (dwImp,FALSE); // Permet de terminer le document si utilisation d'un spooler
					maj_status_imprimante (dwImp, IM_STATUS_SYSTEM_ERROR);
					break;

				} // switch wError LImprimanteWaitWrite
      } // if bWaitWrite
    } // for dwImp
  } // scruter_imprimantes

// --------------------------------------------------------------------------
BOOL espace_libre_tampon_imprimante (UINT dwImp, DWORD *pwSpaceFree, DWORD * pwTailleOccupee)
  {
  BOOL        bOk = FALSE;

  (*pwSpaceFree) = 0;
  if (bNumeroImprimanteOk (dwImp))
    {
		PX_IMPRIMANTE	pxImprimante = (PX_IMPRIMANTE)pointe_enr (szVERIFSource, __LINE__, bx_imprimantes, dwImp);

    (*pwSpaceFree) = pxImprimante->wTailleMaxBuf - pxImprimante->wTailleBuf;
    (*pwTailleOccupee) = pxImprimante->wTailleBuf;
    bOk = TRUE;
    }

  return bOk;
  }

// --------------------------------------------------------------------------
BOOL vide_buffer_imprimante (UINT dwImp)
  {
  BOOL	bOk = FALSE;

  if (bNumeroImprimanteOk (dwImp))
    {
		PX_IMPRIMANTE	pxImprimante = (PX_IMPRIMANTE)pointe_enr (szVERIFSource, __LINE__, bx_imprimantes, dwImp);
    pxImprimante->wTailleBuf = 0;
    pxImprimante->bEnTimeOut = FALSE;   // $$ audacieux
    bOk = TRUE;
    }
  return bOk;
  }

// --------------------------------------------------------------------------
BOOL ferme_port_imprimante (UINT dwImp)
  {
  BOOL	bOk = FALSE;

  if (bNumeroImprimanteOk (dwImp))
    {
		bOk = bFermerImprimante(dwImp, FALSE);
    }

  return bOk;
  }


// --------------------------------------------------------------------------
BOOL maj_time_out_imprimante (UINT dwImp, DWORD lwTimeOut)
  {
  BOOL        bOk;

  bOk = FALSE;
  if (bNumeroImprimanteOk (dwImp))
    {
    if (bOuvrirImprimante (dwImp))
      {
      if (LImprimanteTimeOut (dwImp, lwTimeOut) == CLpt::ERREUR_LPT_NONE)
        {
        bOk = TRUE;
        }
      else
        {
        maj_status_imprimante (dwImp, IM_STATUS_NO_PRINTER);
        }
      }
    else
      {
      maj_status_imprimante (dwImp, IM_STATUS_NO_PRINTER);
      }
    }
  return (bOk);
  }

// --------------------------------------------------------------------------
BOOL initialiser_imprimante (UINT dwImp)
  {
  BOOL        bOk;
  CLpt::ERREUR_LPT  wError;

  bOk = FALSE;
  if (bNumeroImprimanteOk (dwImp))
    {
    if (bOuvrirImprimante (dwImp))
      {
      wError = LImprimanteInit (dwImp);
      if (wError == CLpt::ERREUR_LPT_NONE)
        {
        bOk = TRUE;
        }
      else
        {
        if (wError == CLpt::ERREUR_LPT_DRIVER_BUSY)
          {
          maj_status_imprimante (dwImp, IM_STATUS_BUSY);
          }
        else
          {
          maj_status_imprimante (dwImp, IM_STATUS_INIT_ERROR);
          }
        }
      }
    else
      {
      maj_status_imprimante (dwImp, IM_STATUS_NO_PRINTER);
      }
    }
  return (bOk);
  }


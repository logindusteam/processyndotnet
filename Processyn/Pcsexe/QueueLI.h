// QueueLI.h: interface for the CQueueLI class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_QUEUELI_H__181415BF_8000_4EBA_A902_722902895762__INCLUDED_)
#define AFX_QUEUELI_H__181415BF_8000_4EBA_A902_722902895762__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Gestion Win32 de queues FIFO
// n�cessite include "USem.h" pour HSEM
// RESTRICTIONS : 
//	pas d'IPC
// Erreurs renvoy�es = erreurs OS

class CQueueLI  
	{
	public:
		CQueueLI();
		virtual ~CQueueLI();

	// �l�ments utilisateurs d'une queue
	typedef struct
		{
		PVOID	pbBuf;		// pointer to buffer with element to write
		UINT	uiUser;		// request/identification data
		DWORD	cbBuf;		// number of bytes to write
		} ELEMENT_QUEUE_LI;

	typedef ELEMENT_QUEUE_LI * PELEMENT_QUEUE_LI;

	//------------------------------------------------------------------------------
	// Renvoie TRUE si la queue est ouverte, FALSE sinon
	BOOL bOuverte()	{return m_hMutexAcces != NULL;}

	//------------------------------------------------------------------------------
	// Cr�e et ouvre une queue accessible par ce seul process.
	// Renvoie TRUE si Ok - sinon NULL.
	// Ajoute une r�f�rence.
	BOOL bCree ();

	//------------------------------------------------------------------------------
	// Partage de la queue : ajout et suppression de r�f�rences � la queue
	// Ajoute une r�f�rence � cette queue 
	void AjouteReference();

	// Supprime une r�f�rence � cette queue et renvoie le compte de r�f�rences.
	// Si renvoie 0, la queue peut �tre d�truite avec delete
	DWORD dwSupprimeReference();

	//------------------------------------------------------------------------------
	// Ferme et d�truit la queue
	void Ferme ();

	//------------------------------------------------------------------------------
	// Supprime tous les �l�ments dans la queue sp�cifi�e
	// Renvoie TRUE si la queue existe
	BOOL bVide ();

	//------------------------------------------------------------------------------
	// Ecriture d'un �lement dans la queue sp�cifi�e.
	// Apr�s que l'�l�ment sp�cifi� soit �crit, le process propri�taire de la queue
	// peut lire l'�l�ment � l'aide de uLit
	BOOL bEcrit
		(
		UINT	uiUser,			// valeur 32 bit associ�e � l'�l�ment
		DWORD cbBuf,      // Taille en octets de l'�l�ment � ajouter � la pile
		PVOID pbBuf       // Adresse du buffer contenant l'�l�ment � �crire
		);

	//------------------------------------------------------------------------------
	// Renvoie le plus ancien des �l�ments restants dans la queue.
	// Si la queue est vide, retourne imm�diatement ou attend l'arriv�e 
	// du prochain �l�ment selon bAttente.
	// Le buffer r�cup�r� sera lib�r� (free) par le lecteur.
	// Seul le process qui a cr�� la queue utilisera uLit.
	// Renvoie 0 si Ok sinon ERROR_HANDLE_EOF ou ERROR_INVALID_HANDLE
	UINT uLit
		(
		UINT *	puiUser,	// adresse d'une variable pour r�cup�rer la valeur 32 bit associ�e � l'�l�ment ou NULL
		DWORD * pcbData,	// adresse de la variable pour r�cup�rer la longueur de l'�l�ment
		PVOID * ppbuf,		// Adresse d'un pointeur pour r�cup�rer l'�lement
		BOOL		bAttente,	// Attente illimit�e de l'arriv�e d'un �l�ment si TRUE, pas d'attente sinon
		PHSEM		phsem			// adresse pour r�cup�rer un handle de s�maphore libre d�s l'arriv�e d'un �l�ment ou NULL
		);

	// -----------------------------------------------------------------------
	// Renvoie TRUE si la demande d'AR a pu �tre prise, FALSE sinon
	BOOL CQueueLI::bDemandeAR ();

		// -----------------------------------------------------------------------
	// Renvoie TRUE si l'envoi d'AR a pu �tre honor�e, FALSE sinon
	BOOL bEnvoiAR ();

	//-------------------------------------------------------------------------
	// Attente d'un accus� de reception sur une connexion pendant au plus timeout ms.
	BOOL bAttenteAR (DWORD dwTimeOutAttente);	// Time out max d'attente de l'accus� de r�ception (ms) ou ITC_INDEFINITE_WAIT

	//------------------------------------------------------------------------------
	// R�cup�re le s�maphore d'attente d'au moins un �l�ment de la queue
	// Renvoie TRUE si le handle de s�maphore est recopi�, FALSE sinon
	BOOL bGetSemAttenteElement (PHSEM phsemDest);	// adresse pour recopie du s�maphore de pr�sence d'un �l�ment dans la queue

	//------------------------------------------------------------------------------
	// R�cup�re le s�maphore d'attente de connexion
	// Renvoie TRUE si le handle de s�maphore est recopi�, FALSE sinon
	BOOL bGetSemAttenteConnexion (PHSEM phsemDest);	// adresse pour recopie du s�maphore de pr�sence d'un �l�ment dans la queue

	private:
		// Acc�s exclusif � la queue $$ �viter si non multithread ???
		BOOL bAccesExclusif ();
		// Fin Acc�s exclusif � la queue
		void FinAccesExclusif ();
		// Supprime tous les �l�ments dans la queue sp�cifi�e
		void Vide ();


		// ------------------------------------------------------------------------------
		// Membres priv�s
		HANDLE	m_hMutexAcces;							// s�maphore d'acc�s mutellement exclusif � cette queue
		HSEM		m_hsemAttenteConnexion;			// s�maphore d'attente "au moins un �l�ment dans cette queue
		HSEM		m_hsemAttenteRX;						// s�maphore d'attente "au moins un �l�ment dans cette queue"
		HSEM		m_hsemAttenteAR;						// s�maphore d'attente "Accus� de r�ception"
		DWORD		m_dwNbElementsUtilisateurs;	// Nombre d'�l�ments utilisateurs dans la queue
		DWORD		m_dwTailleBufElements;			// Taille en octets du buffer d'�l�ments utilisateur
		DWORD		m_dwNbReferences;						// Nombres de References � la queue
		PELEMENT_QUEUE_LI m_paElements;			// Adresse du tableau d'�l�ments utilisateur
	};

#endif // !defined(AFX_QUEUELI_H__181415BF_8000_4EBA_A902_722902895762__INCLUDED_)

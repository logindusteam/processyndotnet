// --------------------------------------------------------------------------
typedef void ( *t_fct_inits  ) (void);             //
typedef void ( *t_fct_entrees) (BOOL bPremier); //
typedef void ( *t_fct_traites) (void);             //
typedef void ( *t_fct_sorties) (BOOL bPremier); //
typedef void ( *t_fct_fins   ) (void);             //

typedef struct
	{
	t_fct_inits		fnInit;						// NULL ou Fonctions d'acc�s aux diff�rentes phases des descripteurs
	t_fct_entrees	fnEntrees;				// "
	t_fct_traites	fnTraite;					// "
	t_fct_sorties	fnSorties;				// "
	t_fct_fins		fnTermine;				// "
	BOOL	bEntreesDeclarees;				// TRUE si des variables du descriteur sont d�clar�es	
	BOOL	bSortiesVersPeripheriques;	// TRUE si communique vers des p�riph�riques externes
	BOOL	bSortiesDeclarees;
	} DESCRIPTEUR_EX, *PDESCRIPTEUR_EX;
// --------------------------------------------------------------------------
BOOL bGetDescripteurEx (DWORD num_descripteur, PDESCRIPTEUR_EX pDescripteurEx);

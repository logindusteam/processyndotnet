//  ----------------------------------------------- Includes
#include "stdafx.h"
//
#include "std.h"            // Types Standards
#include "lng_res.h"        // Mots Reserves pour tests alarmes
#include "tipe.h"            
#include "Appli.h"
#include "UStr.h"         // Chaines de Caract�res            Strxxx ()
#include "UEnv.h"
#include "PathMan.h"
#include "DocMan.h"
#include "BitMan.h"
#include "mem.h"            // Memory Manager
#include "MemMan.h"
#include "MenuMan.h"
#include "threads.h"        // Threads                          thrd_xxx ()
#include "CheckMan.h"
#include "couleurs.h"
#include "temps.h"          // date et heure                    Tpsxxx ()
#include "UChrono.h"                
#include "FMan.h"
#include "FileMan.h"
#include "WBarOuti.h"
#include "LireLng.h"        // Textes pour boites de dialogue
#include "pcs_sys.h"        //
#include "IdLngLng.h"        //
#include "init_txt.h"       // D�finitions dans PCS.INI
#include "scrut_im.h"         
#include "PcsSpace.h"
#include "Space.h"

#include "tri_al.h"         // consultation d'archives d'alarmes
#include "IdExeLng.h"					// cstes boites de dlg ID_DLG_AL_xxx + Identifiants controles
#include "ULangues.h"
#include "tipe_al.h"        // Types des Blocs
#include "SrvAnmAl.h"       // Types Services Animateur Alarmes
#include "USem.h"       // Types Services Animateur Alarmes
#include "ITC.h"       // Types Services Animateur Alarmes
#include "CliAnmAl.h"       // Mail Box Animateur               MbxAnmAlxxx ()
#include "WScrExe.h"				// bScrOnKeyDownTouchePcs
#include "WExe.h"
#include "PcsVarEx.h"
#include "FichierF.h"
#include "PcsFont.h"
#include "Verif.h"
#include "DebugDumpVie.h"

#include "WAnmAl.h"
VerifInit;

// Type des fonctions de service
typedef void (PFN_SERVICE) (BYTE **pBuff, DWORD Position);

// Type d'un service de l'animateur
typedef struct
  {
  PFN_SERVICE * Anim [NB_TYPES_SERVICES_ANM_AL];
  } t_al_srv;


// gestion impressions d'alarmes
//  (voir PCS.INI)
#define NUMERO_IMPRIMANTE 1

// gestion chronometres pour pavhd, pavbd
//
#define TPS_ECH_TEMPO  333   // un tiers de seconde
#define ID_TIMER_TEMPO 259

#define WM_BREAK_PM_ANMAL_LOOP       (WM_USER + 2)

// Handles des fen�tres principales de l'animation d'alarme
static HWND hwndAlDyn					= NULL;	// fen�tre d'affichage dynamique des alarmes
static HWND	hwndAlArchive			= NULL;	// fen�tre de consultation des archives d'alarmes
static HWND hwndBreakAnmAl		= NULL;	// fen�tre pour rupture de l'attente message

// thread Boucle de message
static HWND hwndDialogueActive = NULL; // Handle Boite de dialogue Modeless Active

// Noms de classes de fenetres
static const char szClasseAlDyn				[] = "ClasseAlDyn";
static const char szClasseAlArchive		[] = "ClasseAlArchive";
static const char szClasseBreakAnmAl	[] = "ClasseBreakAnmAl";

// Dimensions des Fenetres
static HSPACE hSpace = NULL;

#define CX_FEN_AL_DYN  500
#define CY_FEN_AL_DYN  300
#define CX_FEN_AL_ARCHIVE 500
#define CY_FEN_AL_ARCHIVE 200

#define NB_CAR_LIST_AL_DYN  160
#define NB_CAR_LIST_AL_ARCHIVE  80

#define CY_BOUTONS_AL    30
#define CX_MARGE_BOUTONS	4	// Marge autour de la barre d'outil
#define CY_MARGE_BOUTONS	4	// Marge autour de la barre d'outil

// -------------------------------------- Identificateurs Fenetres
//
#define ID_BMP_MNU_SURV       1
#define ID_BMP_MNU_VISU       2
#define ID_BMP_MNU_ARCH       3
#define ID_BMP_MNU_IMPR       4
#define ID_BMP_MNU_CPTR       5
#define ID_BMP_MNU_CONS       6
#define ID_BMP_MNU_ACQ        7

#define ID_BMP_MNU_SCROLL_ON  8
#define ID_BMP_MNU_SCROLL_OFF 9

#define ID_BMP_MNU_CONS_VALI  7
#define ID_BMP_MNU_CONS_ARCH  8
#define ID_BMP_MNU_CONS_IMPR  9
#define ID_BMP_MNU_CONS_QUIT 10

#define ID_LISTBOX_AL_DYN   256
#define ID_LISTBOX_AL_CONS  257
#define ID_CONTEXTE_AL_CONS 258

//  Data Locales Al Dyn
//
typedef struct
  {
  HWND    hwndListAlDyn;
  HBO			hboAlDyn;				// NULL ou handle barre d'outil op�rations sur les alarmes
  HBO			hboAlDyn2;			// NULL ou handle barre d'outil scroll ON/OFF sur les alarmes
  HWND    hwndSurvAlDyn;	// NULL ou handle boite de dialogue modeless de surveillance d'alarme
  HWND    hwndVisuAlDyn;	// "																				 de visualisation $$ ?? d'alarme
  HWND    hwndArchAlDyn;	// "																				 d'archivage d'alarme
  HWND    hwndImprAlDyn;	// "																				 d'impression d'alarme
  HWND    hwndCptrAlDyn;	// "																				 de compteur d'alarme
  BOOL		bFenAlArchiveOuverte;
  char    szFicTri[MAX_PATH];
  char    szDateDeb[12];
  char    szHeureDeb[12];
  char    szDateFin[12];
  char    szHeureFin[12];
  char    szGroupe[12];
  char    szPriorite[4];
  char    szEvenement[4];
  char    szFicArch[MAX_PATH];
  LONG    cxCarListDyn;
  LONG    cyCarListDyn;
	HFONT		hFontList;
  } ENV_AL_DYN;

typedef ENV_AL_DYN * PENV_AL_DYN;

static BOOL bOkListe = FALSE;
static BOOL bFicArchOuvert = FALSE;
static HFIC hficFichierAscii;
static HFIC hficFichierArch1;
static HFIC hficFichierArch2;
static HFIC hficFichierArch3;
static HFIC hficFichierArch4;
static CFichierF FichierJournalCSV;
static char pszNomFicArch[MAX_PATH];   // exemple : archive

// Masques de bits param�trage PARAM_FENETRE_AL_DYN_MASQUE de la fen�tre Alarmes dynamiques
#define PARAM_FENETRE_AL_DYN_MASQUE_FENETRE_ALM_FIXE 1
#define PARAM_FENETRE_AL_DYN_MASQUE_TOUTES_BARRE_OUTIL_INVISIBLE 2
#define PARAM_FENETRE_AL_DYN_MASQUE_BARRE_OUTIL_MARCHE_PAUSE_INVISIBLE 4
#define PARAM_FENETRE_AL_DYN_MASQUE_LOOK_AMELIORE 8
static DWORD dwParamFenetreAlDyn=0; // �tat courant param�trage bits PARAM_FENETRE_AL_DYN_MASQUE de la fen�tre Alarmes dynamiques

// Gestion des tabulations dans l'affichage des alarmes
#define CAR_SEPARATEUR_TABLEAU '|'           //((char)128) Pipe

// Data Locales boites Al Dyn 
//
typedef struct
  {
  DWORD wIndexGroupe;
  DWORD TabPriorite[NB_GROUPE_MAX_AL];
  BOOL TabAlActive[NB_GROUPE_MAX_AL];
  } DATA_DLG_AL_DYN;

static BOOL bOkCptr = FALSE;
static DWORD wIndexCptrGrp; // affichage cptrs priorites
static DWORD TabCptrPri [NB_GROUPE_MAX_AL * NB_PRIORITE_MAX_AL];

// Data Locales Al Consult
//
typedef struct
  {
  HBO			hboAlCons;
  HWND    hwndListAlCons;
  HWND    hwndContexteAlCons;
  HWND    hwndTriAlCons;
  BOOL		bDlgTriOuverte;
  HWND    hwndEnregistreConsultationDArchive;
  BOOL		bDlgEnregistreConsultationDArchiveOuverte;
  LONG    CxCarListCons;
  LONG    CyCarListCons;
  } ENV_AL_CONS;
typedef ENV_AL_CONS * PENV_AL_CONS;

static HFIC hficFichierCons6;
static BOOL bFicConsOuvert = FALSE;

// Types d'Evenements Alarmes
//
#define EVT_RIEN_AL          0
#define APPARITION_AL        1
#define DISPARITION_AL       2
#define ACQUITTEMENT_AL      3
#define ACQUITTEMENT_DISP_AL 4

// Tests Bits Alarmes
//
#define C_LONG_MIN ((FLOAT)(-2147483647))
#define C_LONG_MAX ((FLOAT)(2147483647))

// Couleurs Affichage
//
static COLORREF TabCouleur[16] = 
	{
	CLR_WHITE, CLR_BLACK, CLR_BLUE, CLR_RED, CLR_PINK, CLR_GREEN, CLR_CYAN, CLR_YELLOW,
	CLR_DARKGRAY, CLR_DARKBLUE, CLR_DARKRED, CLR_DARKPINK, CLR_DARKGREEN, CLR_DARKCYAN, CLR_BROWN, CLR_PALEGRAY
	};

static HICON hIconAlPresente;
static HICON hIconAlAcquitee;

// Types Affichage
#define AFFICHAGE_ETAT   0 // Liste des alarmes non acquitt�es, par alarme
#define AFFICHAGE_HISTO  1 // Historique des modifications des �tats des alarmes, par ordre d'apparition (buffer tournant)
#define AFFICHAGE_CONSIGNATION  2 // Historique des apparitions des alarmes, par ordre d'apparition (buffer tournant)


// --------------------------------------------------------------------------
// Fonctions d'Animation:
// void Anim (BYTE **pBuff, DWORD Position);
// --------------------------------------------------------------------------
static PFN_SERVICE AnimNull       ;
static PFN_SERVICE AnimFin        ;
static PFN_SERVICE AnimAck        ;
static PFN_SERVICE AnimForcage    ;
static PFN_SERVICE AnimGeo        ;
static PFN_SERVICE AnimRaz        ;
static PFN_SERVICE AnimEvtInit    ;
static PFN_SERVICE AnimEvt        ;

// ---------------------------------- Table des services de l'animateur
//
static t_al_srv aServicesAnmAl [NB_SERVICES_ANM_AL] =
  {
  {AnimAck        , AnimAck        , AnimAck        }, //  0 ANM_AL_SRV_ACK
  {AnimForcage    , AnimForcage    , AnimForcage    }, //  1 ANM_AL_SRV_FORCAGE
  {AnimGeo        , AnimGeo        , AnimGeo        }, //  2 ANM_AL_SRV_GEO
  {AnimRaz        , AnimRaz        , AnimRaz        }, //  3 ANM_AL_SRV_RAZ
  {AnimEvtInit    , AnimEvt        , AnimEvt        }, //  4 ANM_AL_SRV_EVT
  {AnimFin        , AnimFin        , AnimFin        }  //  5 ANM_AL_SRV_FIN
  };


// --------------------------------------------------------------------------
// Fonctions d'Ecritures
// --------------------------------------------------------------------------
static BOOL NotificationBoolean (DWORD CodeRet, BOOL Val)
  {
  BOOL               bOk;
  t_anm_al_notification MsgNotification;

  MsgNotification.wCodeRetour = CodeRet;
  MsgNotification.Valeur.bValLogique = Val;

  bOk = MbxAnmAlSend (sizeof (CodeRet) + sizeof (Val), &MsgNotification);

  return (bOk);
  }

// --------------------------------------------------------------------------
static BOOL NotificationReel (DWORD CodeRet, FLOAT Val)
  {
  BOOL               bOk;
  t_anm_al_notification MsgNotification;

  MsgNotification.wCodeRetour = CodeRet;
  MsgNotification.Valeur.rValNumerique = Val;

  bOk = MbxAnmAlSend (sizeof (CodeRet) + sizeof (Val), &MsgNotification);

  return (bOk);
  }

// --------------------------------------------------------------------------
// Fonctions Gestion Fenetres
// --------------------------------------------------------------------------
static BOOL bCreeFenAlDyn  (void);
static void FermeFenetresAnimAlarmes (void);
static BOOL bCreeFenetreAlArchive (void);

static LRESULT CALLBACK wndprocAlArchive (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2);

// --------------------------------------------------------------------------
// Met � jour la variable globale contenant le param�trage  de la fen�tre des alarmes dynamiques (affichage fen�tre/barre d'outils...)
// c'est le dernier param�tre de la ligne de description surveillance (ancien lTempoStabiliteEvt)
static void MajParamFenetreAlDyn()
{
	DWORD dwRet=0;
	tx_anm_glob_al*	pGlobAl=NULL;

	if (existe_repere (bx_anm_glob_al))
	{
		pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1);
		dwRet = pGlobAl->lTempoStabiliteEvt;
	}
	dwParamFenetreAlDyn=dwRet;
}

// -----------------------------------------------------------------------
// Notification � PCS des appuis touches de fonction F1-->F20
// -----------------------------------------------------------------------
static BOOL bOnKeyDown (WPARAM mp1)
	{
	// R�cup�re le code de touches de fonctions (de 0 � 19)
	DWORD	dwTouche;
	BOOL	bTraite = bScrOnKeyDownTouchePcs (mp1, &dwTouche);
	if (bTraite)
		bTraite = (dwTouche <= TOUCHE_SHIFT_F10);
	if (bTraite)
		{
		// oui => Notifie Pcs de l'appui
		NotificationBoolean (AL_TOUCHES_FCT + dwTouche, TRUE);
		}
	return bTraite;
	}

//-------------------------------------------------------------------------
//
static void maj_statut_al (DWORD statut_al)
  {
  tx_anm_geo_al *pGeoAl = (tx_anm_geo_al *)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_STATUT);

  NotificationReel (pGeoAl->wPosRet, (FLOAT)statut_al);
  }

// --------------------------------------------------------------------------
// Window proc d'une fen�tre re�evant un break $$???
static LRESULT CALLBACK wndprocBreakAnmAl (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT mres = 0;
  BOOL    bTraite = TRUE;
  PBOOL	pbBreak;

  switch (msg)
    {
		case WM_CREATE:
      if (!pSetEnvOnCreateParam (hwnd, mp2))
				mres = -1;
			break;

    case WM_BREAK_PM_ANMAL_LOOP:
			pbBreak = (PBOOL)pGetEnv (hwnd);
			(*pbBreak) = TRUE;
			break;

    default:
			bTraite = FALSE;
			break;
    }
  if (!bTraite)
//TODO: Calls to ::DefWindowProc should be replaced with your base class's WindowProc
    mres = DefWindowProc (hwnd, msg, mp1, mp2);
  return mres;
  }

// --------------------------------------------------------------------------
// cr�e la fen�tre BreakAnmAl
static BOOL bCreeFenetreBreakAnmAl (BOOL *pbBreak)
  {
  BOOL		bOk = FALSE;	
	static	BOOL bClasseEnregistree = FALSE;

	// enregistre la classe si n�cessaire
	if (!bClasseEnregistree)
		{
//TODO:  WNDCLASS used for registering the main frame or view class should be replaced by the MFC skeleton
		WNDCLASS wc;

		wc.style					= 0;//CS_HREDRAW | CS_VREDRAW; 
		wc.lpfnWndProc		= wndprocBreakAnmAl; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= NULL; //(HBRUSH)(COLOR_APPWORKSPACE+1); pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseBreakAnmAl;
		bClasseEnregistree = RegisterClass (&wc);
		}
	// classe enregistr�e ?
  if (bClasseEnregistree)
    {
		// oui => cr�e la fen�tre (invisible)
    hwndBreakAnmAl = CreateWindow (szClasseBreakAnmAl, "", WS_POPUP, 0, 0, 0, 0, hwndAlDyn, //$$ Appli.hwnd ?
			(HMENU) 0, Appli.hinst, pbBreak);
    if (hwndBreakAnmAl)
      bOk = TRUE;
    }
  return (bOk);
  }

// --------------------------------------------------------------------------
static void FermeFenetreBreakAnmAl (void)
  {
  if (hwndBreakAnmAl)
    {
    ::DestroyWindow (hwndBreakAnmAl);
		hwndBreakAnmAl = NULL;
    }
  }


// --------------------------------------------------------------------------
// Fonctions d'Extraction de parametres
// --------------------------------------------------------------------------
static HANDLE_ANM_AL ExtraireHandleAnmAl (BYTE **ppBuff)
  {
  HANDLE_ANM_AL handleAnmAl;

  handleAnmAl =  *((HANDLE_ANM_AL *) *ppBuff);
  (*ppBuff) = (*ppBuff) + sizeof (handleAnmAl);

  return (handleAnmAl);
  }

// --------------------------------------------------------------------------
static BOOL ExtraireBoolean (BYTE **pBuff)
  {
  BOOL Valeur;

  Valeur = *((BOOL *) *pBuff);
  (*pBuff) = (*pBuff) + sizeof (Valeur);

  return (Valeur);
  }
// --------------------------------------------------------------------------
static DWORD ExtraireWord (BYTE **pBuff)
  {
  DWORD Valeur;

  Valeur = *((DWORD *) *pBuff);
  (*pBuff) = (*pBuff) + sizeof (Valeur);

  return (Valeur);
  }
// --------------------------------------------------------------------------
static FLOAT ExtraireReel (BYTE **pBuff)
  {
  FLOAT Valeur;

  Valeur = *((FLOAT *) *pBuff);
  (*pBuff) = (*pBuff) + sizeof (Valeur);

  return (Valeur);
  }
// --------------------------------------------------------------------------
static char *ExtrairePsz (BYTE **pBuff, char *PszChaine)
  {
  char *Valeur;

  Valeur = (char *) (*pBuff);
  StrCopy (PszChaine, Valeur);
  (*pBuff) = (*pBuff) + StrLength (Valeur) + 1;

  return (PszChaine);
  }

// --------------------------------------------------------------------------
// Fonctions de Gestion des Alarmes
// --------------------------------------------------------------------------
static void FabriqueDateHeure (char *pszText, LONG *iDate, LONG *iHeure, tx_anm_horodate* pHoroDate)
  {
  tx_anm_glob_al*pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1);

  if (pGlobAl->wTypeHoroDatage == HORODATAGE_SOURCE)
    {
    StrPrintFormat (pszText, "%02d/%02d/%02d * %02d:%02d:%02d:%03d * ",
                    pHoroDate->Jour, pHoroDate->Mois, pHoroDate->Annee,
                    pHoroDate->Heures, pHoroDate->Minutes,
                    pHoroDate->Secondes, pHoroDate->MilliSecondes);
    }
  else
    {
    StrPrintFormat (pszText, "%02d/%02d/%02d * %02d:%02d:%02d * ",
                    pHoroDate->Jour, pHoroDate->Mois, pHoroDate->Annee,
                    pHoroDate->Heures, pHoroDate->Minutes, pHoroDate->Secondes);
    }
  StrDelete (pszText, 6, 2); // enleve 19 de 1993
  (*iDate) = CalculDate (pHoroDate->Annee, pHoroDate->Mois, pHoroDate->Jour);
  (*iHeure) = CalculHeure (pHoroDate->Heures, pHoroDate->Minutes,
                           pHoroDate->Secondes, pHoroDate->MilliSecondes);
  }

// --------------------------------------------------------------------------
// traite les messages avec $ (valeurs num�riques et variable message facultative)
// renvoie le message d'alarmes : il peut �tre superieur � 40 caract�res
//
static void TraiteMessageAl (char *pszMessage, DWORD wPosElement)
  {
  tx_anm_element_al*pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, wPosElement);
  tx_anm_variable_al*pVariableAl = (tx_anm_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_variable_al, pElementAl->wPosPremVariable);
  PFLOAT pNum;
  PSTR pMes;
  DWORD                wRetour;
  char                pszTmp[20];

  if (pVariableAl->wBlocGenre == bx_anm_cour_num_al)
    { // c'est une variable numerique
    if ((wRetour = StrSearchChar (pszMessage, '$')) != STR_NOT_FOUND)
      {
      pNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_num_al, pVariableAl->wPosEs);
      StrFLOATToStr (pszTmp, (*pNum));
      StrDelete (pszMessage, wRetour, 1);
      StrInsertStr (pszMessage, pszTmp, wRetour);
      }
    }

  pVariableAl = (tx_anm_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_variable_al, pElementAl->wPosPremVariable + pElementAl->wNbreVariable - 1);
  if ((pVariableAl->wBlocGenre == bx_anm_cour_mes_al) &&
      (pVariableAl->wTypeVar == VARIABLE_AL))
    { // c'est une variable message
    pMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_mes_al, pVariableAl->wPosEs);
    StrConcatChar (pszMessage, ' ');
    StrConcat (pszMessage, pMes);
    }
  }

// --------------------------------------------------------------------------
// renvoie le message d'alarmes du fichier .pa1: maximum 80 caracteres + \0
//
static BOOL LireMessageAla (char *pszMessageAlarme, DWORD wPosFicMess)
  {
  BOOL	bValide;
  char  pszMessageTamp[TAILLE_MAX_MESSAGE_AL];

  bValide = FALSE;
	if (uFileLireDirect (hficFichierAscii, wPosFicMess - 1, pszMessageTamp) == NO_ERROR)
		{
		StrSetLength (pszMessageTamp, TAILLE_MAX_MESSAGE_AL - 2); // positionne fin chaine a la place de CR
		StrDeleteFirstSpaces (pszMessageTamp);
		StrDeleteLastSpaces (pszMessageTamp);
		bValide = TRUE;
		}
  if (!bValide)
    {
    StrCopy (pszMessageTamp, "error message");
    }
  StrCopy (pszMessageAlarme, pszMessageTamp);
  return (bValide);
  }

// --------------------------------------------------------------------------
// Texte visualis� dans la liste des alarmes
// $$ Attention : justifie le texte par ajout d'espaces;
// $$ impropre en affichage � espacement proportionnel.
static void FabriqueTexteAl (DWORD wPosElement, char *pszText, tx_anm_horodate * pHoroDate)
  {
  tx_anm_element_al  *pElementAl;
  char                pszMessage [TAILLE_MAX_MES_AL_COMPLET+1];
  LONG                 iDate;
  LONG                 iHeure;
  tx_anm_groupe_al*pGroupeAl;
  UINT                wIndex;

  StrSetNull (pszMessage);
  pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, wPosElement);
  pGroupeAl = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, pElementAl->wNumeroGroupe);
  FabriqueDateHeure (pszText, &iDate, &iHeure, pHoroDate);
  StrConcatChar (pszText, ' ');
  StrCopy (pszMessage, pGroupeAl->pszNomGroupe);
  for (wIndex = StrLength(pszMessage) + 1; wIndex <= 11; wIndex++)
    {
    StrConcatChar (pszMessage, ' ');
    }
  StrConcat (pszText, pszMessage);
  StrPrintFormat (pszMessage, "%02d", pElementAl->wNumeroPriorite);
  StrConcat (pszText, pszMessage);
  StrConcatChar (pszText, ' ');
  //
  LireMessageAla (pszMessage, pElementAl->wNumeroEnregistrement);
  TraiteMessageAl (pszMessage, wPosElement); // test $ + ajout message
  StrConcat (pszText, pszMessage);
  }

// --------------------------------------------------------------------------
// GESTION DE L'ARCHIVAGE DES ALARMES
// --------------------------------------------------------------------------
// fic1 et fic2 : record indispensable pour les tris. fic3 et fic4 : textes
// ouverture des fichiers d'archivages d'alarmes
//
static void OuvreFicArch (void)
  {
  char pszNomFic[MAX_PATH];
  DWORD wNbCarMaxMesAl;
  DWORD wNbCarMaxFicArch;
  DWORD wNbCarMaxLineImp;

  bFicArchOuvert = TRUE;
  StrCopy (pszNomFic, pszNomFicArch);
  StrConcat (pszNomFic, ".3"); // fichier texte
  definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
  if (uFileOuvre (&hficFichierArch3, pszNomFic, wNbCarMaxFicArch+2, CFman::OF_OUVRE_OU_CREE) != NO_ERROR)
    {
    maj_statut_al(0x04);
    }

  StrCopy (pszNomFic, pszNomFicArch);
  StrConcat (pszNomFic, ".1"); // fichier enregistrement
  if (uFileOuvre (&hficFichierArch1, pszNomFic, sizeof(X_ANIM_ALARMES_ID_ALARME_ARCHIVE), CFman::OF_OUVRE_OU_CREE) != NO_ERROR)
    {
    maj_statut_al(0x04);
    }
  }

// --------------------------------------------------------------------------
// ferme fic 1 et 3
//
static void FermeFicArch (void)
  {
  bFicArchOuvert = FALSE;
  if (uFileFerme (&hficFichierArch1) != 0)
    {
    maj_statut_al(0x04);
    }
  if (uFileFerme (&hficFichierArch3) != 0)
    {
    maj_statut_al(0x04);
    }
	FichierJournalCSV.bFerme();
  }

// --------------------------------------------------------------------------
// Pour CSV remplace les caract�res de tabulation d'un message d'alarme 
// par un caract�re donn�e tabulations param�tr�s
// --------------------------------------------------------------------------
static void SubstitueTexteAlarmeAvecTabs(PSTR szFinal, PCSTR szTexteAl, CHAR chrFinal)
	{
	// Recopie le texte source dans le texte destination
	StrCopyUpToLength(szFinal,szTexteAl, TAILLE_MAX_MES_AL_COMPLET);

	// Taille du Texte
	LONG nTailleTexteAl=StrLength(szFinal);

	// Le texte contient un caract�re de tabulation ?
	DWORD dwNCharTAB = StrSearchChar(szFinal,CAR_SEPARATEUR_TABLEAU);

	if ((nNbAlarmTabsFormat > 0) && (dwNCharTAB!=STR_NOT_FOUND))
		{
		//Remplace les caract�res, TAB par TAB :
		DWORD dwNCharDebut = 0;
		LONG lNTab = 0;

		// Parcours du texte, TAB par TAB
		while((lNTab < nNbAlarmTabsFormat) && (dwNCharTAB!=STR_NOT_FOUND))
			{
			szFinal[dwNCharDebut+dwNCharTAB]=chrFinal;
			// Une tabulation trait�e en plus
			lNTab+=1;
			// Cherche la tabulation suivante
			dwNCharDebut+=dwNCharTAB+1;
			dwNCharTAB = StrSearchChar(szTexteAl+dwNCharDebut,CAR_SEPARATEUR_TABLEAU);
			}
		}
	}

// --------------------------------------------------------------------------
// quand fic1 et fic3 sont termin�s, on les renomme en fic2 et fic4
// et on repart au d�but de fic1 et fic3
//
static void ArchiveAl (DWORD wPosElement, DWORD wTypeEvt, tx_anm_horodate * pHoroDate)
  {
  BOOL bArchive = FALSE;

  tx_anm_element_al  *pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, wPosElement);
  tx_anm_groupe_al   *pGroupeAl = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, pElementAl->wNumeroGroupe);
  X_ANIM_ALARMES_ID_ALARME_ARCHIVE IdAlarmeArchive;
  StrCopy (IdAlarmeArchive.szNomGroupe, pGroupeAl->pszNomGroupe);
  IdAlarmeArchive.wPriorite = LOBYTE(pElementAl->wNumeroPriorite);
  IdAlarmeArchive.wTypeEvt = LOBYTE(wTypeEvt);

  char pszMessage[TAILLE_MAX_MES_AL_COMPLET + 1];
  StrSetNull (pszMessage);
  FabriqueDateHeure (pszMessage, &IdAlarmeArchive.iDate, &IdAlarmeArchive.iHeure, pHoroDate);
  StrConcatChar (pszMessage, ' ');
  char pszMessageTamp[TAILLE_MAX_MES_AL_COMPLET + 1];
  StrCopy (pszMessageTamp, pGroupeAl->pszNomGroupe);
  for (DWORD wIndex = StrLength(pszMessageTamp) + 1; wIndex <= 11; wIndex++)
    {
    StrConcatChar (pszMessageTamp, ' ');
    }
  StrConcat (pszMessage, pszMessageTamp);

  StrPrintFormat (pszMessageTamp, "%02d", pElementAl->wNumeroPriorite);
  StrConcat (pszMessage, pszMessageTamp);

  char pszMessageEvt[TAILLE_MAX_MES_AL_COMPLET + 1];
  switch (wTypeEvt)
    {
    case APPARITION_AL : // apparition
      StrCopy (pszMessageEvt, "+ ");
      bArchive = TRUE;
      break;

    case DISPARITION_AL : // disparition
      StrCopy (pszMessageEvt, "- ");
      bArchive = TRUE;
      break;

    case ACQUITTEMENT_AL : // acquittement
      StrCopy (pszMessageEvt, "+*");
      bArchive = TRUE;
      break;

    case ACQUITTEMENT_DISP_AL : // acquittement et disparition
      StrCopy (pszMessageEvt, "-*");
      bArchive = TRUE;
      break;
    } // switch evt

  if (bArchive)
    {
    if (!bFicArchOuvert)
      {
      OuvreFicArch(); // ouvre fic1 et fic3
      }

    StrConcatChar (pszMessage, ' '); // 
    StrConcat (pszMessage, pszMessageEvt); // type d'evenement
    StrConcatChar (pszMessage, ' '); // 

		DWORD wNbCarMaxMesAl=0;
		DWORD wNbCarMaxFicArch=0;
		DWORD wNbCarMaxLineImp=0;
		char pszMessageAlarme[TAILLE_MAX_MES_AL_COMPLET + 1];
		pszMessageAlarme[0] = 0;
    definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
    if (!LireMessageAla (pszMessageAlarme, pElementAl->wNumeroEnregistrement))
      {
      maj_statut_al(0x08);
      }
    else
      {
      TraiteMessageAl (pszMessageAlarme, wPosElement);
			DWORD wLongueurMaxMessageTamp = wNbCarMaxFicArch - StrLength (pszMessage);
      if (StrLength (pszMessageAlarme) > wLongueurMaxMessageTamp)
        {
        StrSetLength (pszMessageAlarme, wLongueurMaxMessageTamp);
        }
      StrConcat (pszMessage, pszMessageAlarme);
      }

    for (DWORD wIndex = StrLength (pszMessage) + 1; wIndex <= wNbCarMaxFicArch; wIndex++)
      {
      StrConcatChar (pszMessage, ' ');
      }

		DWORD wPosCarFin = StrLength (pszMessage);
    StrInsertChar (pszMessage, CAR_CR, wPosCarFin);
    wPosCarFin++;
    StrInsertChar (pszMessage, CAR_LF, wPosCarFin);

		DWORD wPosFicArch;
    uFileNbEnr (hficFichierArch1, &wPosFicArch);
		wPosFicArch++;

		tx_anm_glob_al* pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // un seul enreg.

    if (wPosFicArch > pGlobAl->wTailleFicMax)
      { // on a atteint la limite max enreg arch
      FermeFicArch();  // on ferme fic1 et fic3

			char pszNomFic[MAX_PATH];
			char pszNameFic[MAX_PATH];
      StrCopy (pszNomFic, pszNomFicArch);
      StrCopy (pszNameFic, pszNomFicArch);
      StrConcat (pszNomFic, ".1");
      StrConcat (pszNameFic, ".2");
      if (bFileExiste (pszNameFic))
        {
        // on efface fic2
        if (uFileEfface (pszNameFic) != 0)
          {
          maj_statut_al(0x04);
          }
        }
      // on renomme fic1 en fic2
      if (uFileRenomme (pszNomFic, pszNameFic) != 0)
        {
        maj_statut_al(0x04);
        }

      StrCopy (pszNomFic ,pszNomFicArch);
      StrCopy (pszNameFic ,pszNomFicArch);
      StrConcat (pszNomFic, ".3");
      StrConcat (pszNameFic, ".4");

      if (bFileExiste (pszNameFic))
        {
        // on efface fic4
        if (uFileEfface (pszNameFic) != 0)
          {
          maj_statut_al(0x04);
          }
        }
      // on renomme fic3 en fic4
      if (uFileRenomme (pszNomFic, pszNameFic) != 0)
        {
        maj_statut_al(0x04);
        }

      OuvreFicArch();  // on reouvre fic1 et fic3

      wPosFicArch = 1;
      }  // on a atteint la limite max enreg arch

    if (uFileEcrireDirect (hficFichierArch1, wPosFicArch-1, &IdAlarmeArchive) != 0)
      {
      maj_statut_al(0x04); // pb. ecriture ds fic archivage
      }

    if (uFileEcrireDirect (hficFichierArch3, wPosFicArch-1, pszMessage) != 0)
      {
      maj_statut_al(0x04); // pb. ecriture ds fic archivage
      }

		// Archiver dans fichier CSV 
		if (CPcsVarEx::bGetValVarSysLogEx(VA_SYS_AL_ARCHIVE_CSV))
			{
			if (!FichierJournalCSV.bOuvert())
				{
				char szNomFichier[MAX_PATH];
				SYSTEMTIME st;
				GetLocalTime(&st);
				StrPrintFormat(szNomFichier, "AL%02lu%02lu%02lu.csv",((DWORD)st.wYear)%100,(DWORD)st.wMonth,(DWORD)st.wDay);
				char szPathName[MAX_PATH];
				CopiePathNameAbsoluAuDoc (szPathName, szNomFichier);
				FichierJournalCSV.bOuvre(szPathName,F_OF_OUVRE_OU_CREE);
				}
			if (FichierJournalCSV.bOuvert())
				{
				// Synth�se du message d'alarme
				char szDate[20];
				Appli.FormateDateInternationale(szDate, pHoroDate->Jour, pHoroDate->Mois, pHoroDate->Annee);
				char szMesCSV[TAILLE_MAX_MES_AL_COMPLET + 1];
				char szMessage[TAILLE_MAX_MES_AL_COMPLET + 1];
				// Formatte le message d'alarme
				SubstitueTexteAlarmeAvecTabs(szMessage,pszMessageAlarme,Appli.cSeparateurListe);
				StrPrintFormat(szMesCSV, "%s %02lu:%02lu:%02lu%c%s%c%02d%c%s%c%s",
					szDate, pHoroDate->Heures,pHoroDate->Minutes,pHoroDate->Secondes,Appli.cSeparateurListe,// Date internationale et heure
					pGroupeAl->pszNomGroupe,Appli.cSeparateurListe, // Nom groupe
					pElementAl->wNumeroPriorite,Appli.cSeparateurListe, // priorit�
					pszMessageEvt,Appli.cSeparateurListe, // Evenement
					szMessage); // Nom alarme

				// Ecriture � la fin du fichier journal
				if ((!FichierJournalCSV.bPointeFin()) || (FichierJournalCSV.bEcrireSzLn(szMesCSV)))
					maj_statut_al(0x04); // pb. ecriture ds fic archivage

				FichierJournalCSV.bFerme();
				} // if (FichierJournalCSV.bOuvert())
			} // archive oui
		}
  }

// --------------------------------------------------------------------------
//
static void ImprimeAl (DWORD wPosElement, DWORD wTypeEvt, tx_anm_horodate * pHoroDate)
  {
  BOOL            bImprime;
  char               pszMessage[TAILLE_MAX_MES_AL_COMPLET + 1];
  char               pszMessageTamp[TAILLE_MAX_MES_AL_COMPLET + 1];
  UINT               wIndex;
  tx_anm_element_al *pElementAl;
  tx_anm_groupe_al  *pGroupeAl;
  LONG                iDate;
  LONG                iHeure;
	DWORD								wNbCarMaxMesAl;
  DWORD               wNbCarMaxFicArch;
  DWORD               wNbCarMaxLineImp;
  DWORD               wLongueurMaxMessageTamp;
	PX_VAR_SYS	pwPosition;
  PFLOAT prValeur;
	FLOAT								fOldImp;

  bImprime = FALSE;
  StrSetNull (pszMessage);

  pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, wPosElement);
  pGroupeAl = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, pElementAl->wNumeroGroupe);
  FabriqueDateHeure (pszMessage, &iDate, &iHeure, pHoroDate);

  StrConcatChar (pszMessage, ' ');

  StrCopy (pszMessageTamp, pGroupeAl->pszNomGroupe);
  for (wIndex = StrLength(pszMessageTamp) + 1; wIndex <= 11; wIndex++)
    {
    StrConcatChar (pszMessageTamp, ' ');
    }
  StrConcat (pszMessage, pszMessageTamp);

  StrPrintFormat (pszMessageTamp, "%02d", pElementAl->wNumeroPriorite);
  StrConcat (pszMessage, pszMessageTamp);

  switch (wTypeEvt)
    {
    case APPARITION_AL : // apparition
      StrCopy (pszMessageTamp, " +  ");
      bImprime = TRUE;
      break;

    case DISPARITION_AL : // disparition
      StrCopy (pszMessageTamp, " -  ");
      bImprime = TRUE;
      break;

    case ACQUITTEMENT_AL : // acquittement
      StrCopy (pszMessageTamp, " +* ");
      bImprime = TRUE;
      break;

    case ACQUITTEMENT_DISP_AL : // acquittement et disparition
      StrCopy (pszMessageTamp, " -* ");
      bImprime = TRUE;
      break;

    default :
      break;
    } // switch evt

  if (bImprime)
    {
    StrConcat (pszMessage, pszMessageTamp); // type d'evenement

    if (!LireMessageAla (pszMessageTamp, pElementAl->wNumeroEnregistrement))
      {
      maj_statut_al(0x08);
      }
    else
      {
      TraiteMessageAl (pszMessageTamp, wPosElement);
      definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
      wLongueurMaxMessageTamp = wNbCarMaxLineImp - StrLength (pszMessage);
      if (StrLength (pszMessageTamp) > wLongueurMaxMessageTamp)
        {
        StrSetLength (pszMessageTamp, wLongueurMaxMessageTamp);
        }
      StrConcat (pszMessage, pszMessageTamp);
      }

    for (wIndex = StrLength (pszMessage) + 1; wIndex <= wNbCarMaxLineImp; wIndex++)
      {
      StrConcatChar (pszMessage, ' ');
      }

		pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, imprimante);
    prValeur   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pwPosition->dwPosCourEx);
		fOldImp = (*prValeur);
		(*prValeur) = NUMERO_IMPRIMANTE;
		stocker_imprimante (StrLength (pszMessage), pszMessage, TRUE, TRUE);
		(*prValeur) = fOldImp;
    } // imprime oui
  }

// --------------------------------------------------------------------------
// GESTION DE LA SURVEILLANCE ET DE LA VISUALISATION
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// retourne vrai si l'alarme est deja dans le buffer de surveillance
//
static BOOL recherche_alarme_surv (DWORD PosElement, DWORD *PosBuffer)
  {
  BOOL								trouve;
  DWORD                nbr_enr;
  t_buffer_anm_surv_al*pBuffer;

  trouve = FALSE;
  (*PosBuffer) = 0;
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
  while (((*PosBuffer) < nbr_enr) && (!trouve))
    {
    (*PosBuffer)++;
    pBuffer = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, (*PosBuffer));
    trouve = (pBuffer->wPosEsElement == PosElement);
    }

  return (trouve);
  }

// --------------------------------------------------------------------------
// retourne vrai si l'alarme est deja dans le buffer de visualisation
//
static BOOL recherche_alarme_visu (DWORD PosElement, DWORD *PosBuffer)
  {
  BOOL               trouve;
  DWORD                  nbr_enr;
  t_buffer_anm_visu_al*pBuffer; // wPosEsElement

  trouve = FALSE;
  (*PosBuffer) = 0;
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al);
  while (((*PosBuffer) < nbr_enr) && (!trouve))
    {
    (*PosBuffer)++;
    pBuffer = (t_buffer_anm_visu_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_visu_al, (*PosBuffer));
    trouve =  (pBuffer->wPosEsElement == PosElement);
    }

  return (trouve);
  }

// --------------------------------------------------------------------------
// pcsexe envoie a chaque cycle toutes les valeurs d'alarmes qui changent,
// on teste s'il y a des alarmes qui montent ou descendent
//
static void test_evenement (BYTE **pBuff, DWORD *EvenementAl, DWORD pElement, tx_anm_horodate * pHoroDate)
  {
  tx_anm_element_al  *pElementAl;
  tx_anm_variable_al *pVariableAl;
  tx_anm_tempo_al*pTempoAl;
  DWORD                IndexVariable;
  DWORD                IndexTab;
  DWORD                wDernierIndice;
  t_valeur_evt        TabVal[NB_VAL_TOTAL_AL];
  t_valeur_evt        TabValMem [NB_VAL_TOTAL_AL];
  PBOOL pLog;
  FLOAT                *pNum;
  char               *pMes;
  BOOL             bConditionAl;
  BOOL             bTestTempo;
  tx_anm_glob_al     *pGlobAl;
  DWORD               Centiemes;

  pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, pElement);
  IndexTab = 0;
  for (IndexVariable = pElementAl->wPosPremVariable; IndexVariable < pElementAl->wPosPremVariable + pElementAl->wNbreVariable; IndexVariable++)
    {
    pVariableAl = (tx_anm_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_variable_al, IndexVariable);
    if (pVariableAl->wTypeVar == CONSTANTE_AL)
      {
      switch (pVariableAl->wBlocGenre)
        {
        case bx_anm_cour_log_al:
          pLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_log_al, pVariableAl->wPosEs);
          TabValMem[IndexTab].wTypeValeur = LOGIQUE_AL;
          TabValMem[IndexTab].Valeur.bValLogique = (*pLog);
          TabVal[IndexTab].wTypeValeur = LOGIQUE_AL;
          TabVal[IndexTab].Valeur.bValLogique = (*pLog);
          break;

        case bx_anm_cour_num_al:
          pNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_num_al, pVariableAl->wPosEs);
          TabValMem[IndexTab].wTypeValeur = NUMERIQUE_AL;
          TabValMem[IndexTab].Valeur.rValNumerique = (*pNum);
          TabVal[IndexTab].wTypeValeur = NUMERIQUE_AL;
          TabVal[IndexTab].Valeur.rValNumerique = (*pNum);
          break;

        case bx_anm_cour_mes_al: // pas de cstes messages
          break;

        default:
          break;
        } // switch
      } // constante
    else
      { // variable
      switch (pVariableAl->wBlocGenre)
        {
        case bx_anm_cour_log_al:
          pLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_log_al, pVariableAl->wPosEs);
          TabValMem[IndexTab].wTypeValeur = LOGIQUE_AL;
          TabValMem[IndexTab].Valeur.bValLogique = (*pLog);
          (*pLog) = ExtraireBoolean (pBuff);
          TabVal[IndexTab].wTypeValeur = LOGIQUE_AL;
          TabVal[IndexTab].Valeur.bValLogique = (*pLog);
          break;

        case bx_anm_cour_num_al:
          pNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_num_al, pVariableAl->wPosEs);
          TabValMem[IndexTab].wTypeValeur = NUMERIQUE_AL;
          TabValMem[IndexTab].Valeur.rValNumerique = (*pNum);
          (*pNum) = ExtraireReel (pBuff);
          TabVal[IndexTab].wTypeValeur = NUMERIQUE_AL;
          TabVal[IndexTab].Valeur.rValNumerique = (*pNum);
          break;

        case bx_anm_cour_mes_al: // maj BD animateur
          pMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_mes_al, pVariableAl->wPosEs);
          ExtrairePsz (pBuff, pMes);
          break;

        default:
          break;
        } // switch
      } // variable
    wDernierIndice = IndexTab;
    IndexTab++;
    } // for IndexVariable

  bConditionAl = FALSE;
  bTestTempo = FALSE;
  switch (pElementAl->wTestAlarme)
    {
    case c_res_change:
      switch (TabValMem[0].wTypeValeur)
        {
        case LOGIQUE_AL:
          bConditionAl = (TabVal[0].Valeur.bValLogique != TabValMem[0].Valeur.bValLogique);
          break;
        case NUMERIQUE_AL:
          bConditionAl = (TabVal[0].Valeur.rValNumerique != TabValMem[0].Valeur.rValNumerique);
          break;
        default:
          break;
        }
      break;

    case c_res_stable:
      switch (TabValMem[0].wTypeValeur)
        {
        case LOGIQUE_AL:
          bConditionAl = (TabVal[0].Valeur.bValLogique == TabValMem[0].Valeur.bValLogique);
          break;
        case NUMERIQUE_AL:
          bConditionAl = (TabVal[0].Valeur.rValNumerique == TabValMem[0].Valeur.rValNumerique);
          break;
        default:
          break;
        }
      break;

    case c_res_un:
      bConditionAl = TabVal[0].Valeur.bValLogique;
      break;

    case c_res_zero:
      bConditionAl =  !TabVal[0].Valeur.bValLogique;
      break;

    case c_res_egal:
      bConditionAl = (TabVal[0].Valeur.rValNumerique == TabVal[1].Valeur.rValNumerique);
      break;

    case c_res_different:
      bConditionAl = (TabVal[0].Valeur.rValNumerique != TabVal[1].Valeur.rValNumerique);
      break;

    case c_res_superieur:
      bConditionAl = (TabVal[0].Valeur.rValNumerique > TabVal[1].Valeur.rValNumerique);
      break;

    case c_res_inferieur:
      bConditionAl = (TabVal[0].Valeur.rValNumerique < TabVal[1].Valeur.rValNumerique);
      break;

    case c_res_superieur_egal:
      bConditionAl = (TabVal[0].Valeur.rValNumerique >= TabVal[1].Valeur.rValNumerique);
      break;

    case c_res_inferieur_egal:
      bConditionAl = (TabVal[0].Valeur.rValNumerique <= TabVal[1].Valeur.rValNumerique);
      break;

    case c_res_passe_valeur:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique < TabVal[1].Valeur.rValNumerique) &&
                           (TabVal[0].Valeur.rValNumerique >= TabVal[1].Valeur.rValNumerique)) ||
		          ((TabValMem[0].Valeur.rValNumerique > TabVal[1].Valeur.rValNumerique) &&
                           (TabVal[0].Valeur.rValNumerique <= TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_passe_valeur_vers_h:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique < TabVal[1].Valeur.rValNumerique) && (TabVal[0].Valeur.rValNumerique >= TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_passe_valeur_vers_b:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique > TabVal[1].Valeur.rValNumerique) && (TabVal[0].Valeur.rValNumerique <= TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_prend_valeur:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique != TabVal[1].Valeur.rValNumerique) && (TabVal[0].Valeur.rValNumerique == TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_prend_valeur_vers_h:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique < TabVal[1].Valeur.rValNumerique) && (TabVal[0].Valeur.rValNumerique == TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_prend_valeur_vers_b:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique > TabVal[1].Valeur.rValNumerique) && (TabVal[0].Valeur.rValNumerique == TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_quitte_valeur:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique == TabVal[1].Valeur.rValNumerique) && (TabVal[0].Valeur.rValNumerique != TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_quitte_valeur_vers_h:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique == TabVal[1].Valeur.rValNumerique) && (TabVal[0].Valeur.rValNumerique > TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_quitte_valeur_vers_b:
      bConditionAl = (((TabValMem[0].Valeur.rValNumerique == TabVal[1].Valeur.rValNumerique) && (TabVal[0].Valeur.rValNumerique < TabVal[1].Valeur.rValNumerique)));
      break;

    case c_res_testbit_h:
      if ((TabVal[0].Valeur.rValNumerique < C_LONG_MIN) || (TabVal[0].Valeur.rValNumerique > C_LONG_MAX))
        {
        maj_statut_al (0x02); // superieur � 16 bits (pb. testbit)
        }
      else
        {
        if ((TabVal[1].Valeur.rValNumerique < 0) || (TabVal[1].Valeur.rValNumerique > 15))
          {
          maj_statut_al (0x02); // numero de bit invalide (pb. testbit)
          }
        else
          {
          bConditionAl = tstbit ((DWORD)TabVal[0].Valeur.rValNumerique, (DWORD)TabVal[1].Valeur.rValNumerique);
          }
        }
      break;

    case c_res_testbit_b:
      if ((TabVal[0].Valeur.rValNumerique < C_LONG_MIN) || (TabVal[0].Valeur.rValNumerique > C_LONG_MAX))
        {
        maj_statut_al (0x02); // superieur � 16 bits (pb. testbit)
        }
      else
        {
        if ((TabVal[1].Valeur.rValNumerique < 0) || (TabVal[1].Valeur.rValNumerique > 15))
          {
          maj_statut_al (0x02); // numero de bit invalide (pb. testbit)
          }
        else
          {
          bConditionAl =  ! tstbit ((DWORD)TabVal[0].Valeur.rValNumerique, (DWORD)TabVal[1].Valeur.rValNumerique);
          }
        }
      break;

    case c_res_entre_alarme:
      if (TabVal[1].Valeur.rValNumerique >= TabVal[2].Valeur.rValNumerique)
        {
        maj_statut_al (0x80);
        }
      else
        {
        bConditionAl = ((TabVal[0].Valeur.rValNumerique < TabVal[1].Valeur.rValNumerique) ||
                            (TabVal[0].Valeur.rValNumerique > TabVal[2].Valeur.rValNumerique));
        }
      break;

    case c_res_pavhd:
      bTestTempo = TRUE;
      bConditionAl = (TabVal[0].Valeur.rValNumerique > TabVal[1].Valeur.rValNumerique);
      break;

    case c_res_pavbd:
      bTestTempo = TRUE;
      bConditionAl = (TabVal[0].Valeur.rValNumerique < TabVal[1].Valeur.rValNumerique);
      break;

    default:
      break;
    } // switch

  (*EvenementAl) = EVT_RIEN_AL;
  if (bConditionAl)
    {
    if ((!pElementAl->bEtatPresent) || (pElementAl->wTypeTestAlarme == TEST_AL_FRONT))
      {
      if (bTestTempo)
        { // test avec tempo : pavhd et pavbd
        pTempoAl = (tx_anm_tempo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_tempo_al, pElementAl->wPosTempo);
        if (!pTempoAl->bTempoEnCour)
          { 
					// tempo non lanc�e
          pTempoAl->bTempoEnCour = TRUE;
          MinuterieLanceMs (&pTempoAl->lwTempo, (DWORD)(TabVal[2].Valeur.rValNumerique * NB_MS_PAR_SECONDE));
          }
        }
      else
        {
        (*EvenementAl) = APPARITION_AL;
        }
      }
    }
  else
    {
    if ((pElementAl->bEtatPresent) && (pElementAl->wTypeTestAlarme == TEST_AL_ETAT))
      {
      (*EvenementAl) = DISPARITION_AL;
      if (bTestTempo)
        { // test avec tempo : pavhd et pavbd
        pTempoAl = (tx_anm_tempo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_tempo_al, pElementAl->wPosTempo);
        if (pTempoAl->bTempoEnCour)
          { // tempo lanc�e
          pTempoAl->bTempoEnCour = FALSE;
          }
        }
      }
    }
  pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1);
  if (pGlobAl->wTypeHoroDatage == HORODATAGE_SOURCE)
    {
    // le wDernier Indice n'est pas joli joli.
    // on peut faire autrement en calculant dans tous les tests
    // les differentes positions de depart.
    // ici on suppose salement que l'horodatage est tjrs � la fin.
    pHoroDate->MilliSecondes = (DWORD)TabVal[wDernierIndice].Valeur.rValNumerique;
    wDernierIndice--;
    pHoroDate->Secondes = (DWORD)TabVal[wDernierIndice].Valeur.rValNumerique;
    wDernierIndice--;
    pHoroDate->Minutes = (DWORD)TabVal[wDernierIndice].Valeur.rValNumerique;
    wDernierIndice--;
    pHoroDate->Heures = (DWORD)TabVal[wDernierIndice].Valeur.rValNumerique;
    wDernierIndice--;
    pHoroDate->Annee = (DWORD)TabVal[wDernierIndice].Valeur.rValNumerique;
    wDernierIndice--;
    pHoroDate->Mois = (DWORD)TabVal[wDernierIndice].Valeur.rValNumerique;
    wDernierIndice--;
    pHoroDate->Jour = (DWORD)TabVal[wDernierIndice].Valeur.rValNumerique;
    }
  else
    {
    TpsLireDateHeure (&(pHoroDate->Jour), &(pHoroDate->Mois), &(pHoroDate->Annee),
                      &(pHoroDate->JourDeSemaine), &(pHoroDate->Heures), &(pHoroDate->Minutes),
                      &(pHoroDate->Secondes), &Centiemes);
    pHoroDate->MilliSecondes = 10 * Centiemes;
    }
  return;
  }



// --------------------------------------------------------------------------
// Renvoi index couleur fore et back suivant etat element
static void get_elt_animation_buffer_visu_al (DWORD wPosElement, DWORD *wIndexClrFore, DWORD *wIndexClrBack, BOOL *bAlPresente, BOOL *bAlAcquitee)
  {
  tx_anm_element_al      *pElementAl;
  tx_anm_groupe_al       *pGroupe;

  pElementAl  = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, wPosElement);
  pGroupe     = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, pElementAl->wNumeroGroupe);
	(*bAlPresente) = pElementAl->bEtatPresent;
	(*bAlAcquitee) = pElementAl->bEtatAcq;
  if (pElementAl->bEtatPresent)
    {
    if (pElementAl->bEtatAcq)
      {
      (*wIndexClrFore) = pGroupe->coul_al_pres_ack;
      (*wIndexClrBack) = pGroupe->fcoul_al_pres_ack;
      }
    else
      {
      (*wIndexClrFore) = pGroupe->coul_al_pres_non_ack;
      (*wIndexClrBack) = pGroupe->fcoul_al_pres_non_ack;
      }
    }
  else
    {
    if (pElementAl->bEtatAcq)
      {
      // cas impossible
      }
    else
      {
      (*wIndexClrFore) = pGroupe->coul_al_non_pres_non_ack;
      (*wIndexClrBack) = pGroupe->fcoul_al_non_pres_non_ack;
      }
    }
  }
// --------------------------------------------------------------------------
static void ajout_buffer_visu (DWORD wPosElement, DWORD wIndexClrFore, DWORD wIndexClrBack, BOOL bAlPresente, BOOL bAlAcquitee)
  {
  t_buffer_anm_visu_al   *pBufferVisu;

  insere_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, (nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al) + 1));
  pBufferVisu = (t_buffer_anm_visu_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_visu_al, nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al));
  pBufferVisu->wPosEsElement = wPosElement;
  pBufferVisu->wIndexClrFore = wIndexClrFore;
  pBufferVisu->wIndexClrBack = wIndexClrBack;
	pBufferVisu->bAlPresente = bAlPresente;
	pBufferVisu->bAlAcquitee = bAlAcquitee;
  }

// --------------------------------------------------------------------------
static void maj_buffer_visu (DWORD wBufferVisu, DWORD wIndexClrFore, DWORD wIndexClrBack, BOOL bAlPresente, BOOL bAlAcquitee)
  {
  t_buffer_anm_visu_al   *pBufferVisu;

  pBufferVisu = (t_buffer_anm_visu_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_visu_al, wBufferVisu);
  pBufferVisu->wIndexClrFore = wIndexClrFore;
  pBufferVisu->wIndexClrBack = wIndexClrBack;
	pBufferVisu->bAlPresente = bAlPresente;
	pBufferVisu->bAlAcquitee = bAlAcquitee;
  }


// --------------------------------------------------------------------------
static void bascule_scroll (BOOL bScroll)
  {
  tx_anm_glob_al       *pGlobAl;

  pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
  pGlobAl->bScroll = bScroll;
  }
// --------------------------------------------------------------------------
// ajoute une alarme dans la liste : le texte est mis a la bonne couleur
// dans WM_DRAWITEM : les textes st ds le buffer de surveillance
//
static void ajout_liste_al (char *pszTexte)
  {
  PENV_AL_DYN				pEnv;
  tx_anm_glob_al *	pGlobAl;
  DWORD             iItem;

  pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
  iItem = ::SendMessage (pEnv->hwndListAlDyn, LB_ADDSTRING,	0, (LPARAM)pszTexte);
  pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
  if (pGlobAl->bScroll)
    {
    ::SendMessage (pEnv->hwndListAlDyn,LB_SETCURSEL, iItem, 0);
    }
  return;
  }

// --------------------------------------------------------------------------
// permet de changer la couleur du message : trait� dans WM_DRAWITEM
//
static void maj_couleur_liste_al (DWORD wPosBufferVisu)
  {
  PENV_AL_DYN	pEnv= (PENV_AL_DYN)pGetEnv (hwndAlDyn);;
  char TexteAl[TAILLE_MAX_MES_AL_COMPLET+1];

  ::SendMessage (pEnv->hwndListAlDyn, LB_GETTEXT, (WPARAM)(wPosBufferVisu - 1), (LPARAM)TexteAl);
  //$$::SendMessage (pEnv->hwndListAlDyn, LM_SETITEMTEXT, wPosBufferVisu - 1, (LPARAM)TexteAl);
  ::SendMessage (pEnv->hwndListAlDyn, LB_DELETESTRING, wPosBufferVisu - 1, 0);
  ::SendMessage (pEnv->hwndListAlDyn, LB_INSERTSTRING, wPosBufferVisu - 1, (LPARAM)TexteAl);
  return;
  }

// --------------------------------------------------------------------------
static void maj_liste_al (DWORD wPosBufferVisu, char *pszTexte)
  {
  PENV_AL_DYN	pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
  //::SendMessage (pEnv->hwndListAlDyn, LM_SETITEMTEXT, wPosBufferVisu - 1, (LPARAM)pszTexte);
  ::SendMessage (pEnv->hwndListAlDyn, LB_DELETESTRING, wPosBufferVisu - 1, 0);
  ::SendMessage (pEnv->hwndListAlDyn, LB_INSERTSTRING, wPosBufferVisu - 1, (LPARAM)pszTexte);
  return;
  }

// --------------------------------------------------------------------------
// enleve un element de la liste des alarmes
//
static void enleve_liste_al (DWORD pBufferVisu) // $$ nom � la con.
  {
  PENV_AL_DYN	pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
  ::SendMessage (pEnv->hwndListAlDyn, LB_DELETESTRING, pBufferVisu - 1, 0);
  return;
  }

// --------------------------------------------------------------------------
// r�cup�re une valeur de variable systeme de la BD animateur
//
static void get_valeur_geo_al (DWORD wPosGeoAl, BOOL *bLog, FLOAT *rNum)
  {
  tx_anm_geo_al*pDonneexGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, wPosGeoAl);

  switch (pDonneexGeoAl->wTypeValeur)
    {
    case LOGIQUE_AL:
      (*bLog) = pDonneexGeoAl->Valeur.bValLogique;
      break;
    case NUMERIQUE_AL:
      (*rNum) = pDonneexGeoAl->Valeur.rValNumerique;
      break;
    default:
      break;
    } // switch
  }

// --------------------------------------------------------------------------
// teste si l'alarme est visualisee
//
static BOOL test_visu (DWORD wNumeroGroupe, DWORD wNumeroPriorite, BOOL bEtatPresent)
  {
  BOOL bLog;
  FLOAT     rNum;
  BOOL bPrioriteUnique;
  BOOL bTest;

  bTest = FALSE;
  get_valeur_geo_al (AL_VISU_GLOB, &bLog, &rNum);
  if (bLog)
    { // visu glob
    get_valeur_geo_al (AL_VISU_GRP + wNumeroGroupe - 1, &bLog, &rNum);
    if (bLog)
      { // visu grp
      get_valeur_geo_al (AL_VISU_ACTIVE + wNumeroGroupe - 1, &bLog, &rNum);
      if (!bLog || (bLog && bEtatPresent))
        { // visu active
        get_valeur_geo_al (AL_PRIORITE_UNIQUE, &bPrioriteUnique, &rNum);
        get_valeur_geo_al (AL_VISU_PRI + wNumeroGroupe - 1, &bLog, &rNum);
        if (bPrioriteUnique)
          { // pour Lagarde
          bTest = (wNumeroPriorite == (DWORD)rNum);
          }
        else
          {
          bTest = (wNumeroPriorite <= (DWORD)rNum);
          }
        }
      }
    }
  return (bTest);
  }

// --------------------------------------------------------------------------
// quand un parametre de visualisation change, on, refabrique la liste de
// visualisation a partir du buffer de surveillance
//
static void refabrique_liste_al (void)
  {
  PENV_AL_DYN								pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
  DWORD											wIndexSurv;
  DWORD											wNbEnrSurv;
  tx_anm_element_al					*pElementAl;
  t_buffer_anm_surv_al			*pBufferSurv;
  DWORD											wIndexClrFore;
  DWORD											wIndexClrBack;
  BOOL											bLog;
  FLOAT                     rNum;
	BOOL											bAlPresente;
	BOOL											bAlAcquitee;

  vide_bloc (buffer_anm_visu_al);
  ::SendMessage (pEnv->hwndListAlDyn, LB_RESETCONTENT, 0, 0);
  get_valeur_geo_al (AL_TYPE_VISU, &bLog, &rNum);
  if (rNum == AFFICHAGE_ETAT)
    {
    wNbEnrSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
    for (wIndexSurv = 1; wIndexSurv <= wNbEnrSurv; wIndexSurv++)
      {
      pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wIndexSurv);
      pElementAl  = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, pBufferSurv->wPosEsElement);
      if (test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
        { // elle est visualisee
        pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wIndexSurv);
        get_elt_animation_buffer_visu_al (pBufferSurv->wPosEsElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
        ajout_buffer_visu (pBufferSurv->wPosEsElement, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
        pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wIndexSurv);
        ajout_liste_al (pBufferSurv->pszTextAl);
        }
      } // for
    }

  return;
  }

// --------------------------------------------------------------------------
// teste si l'alarme est a archiver
//
static BOOL test_archivage (DWORD wNumeroGroupe, DWORD wNumeroPriorite)
  {
  BOOL bLog;
  FLOAT     rNum;
  BOOL bPrioriteUnique;
  BOOL bTest;

  bTest = FALSE;
  get_valeur_geo_al (AL_ARCH_GLOB, &bLog, &rNum);
  if (bLog)
    { // arch glob
    get_valeur_geo_al (AL_ARCH_GRP + wNumeroGroupe - 1, &bLog, &rNum);
    if (bLog)
      { // arch grp
      get_valeur_geo_al (AL_PRIORITE_UNIQUE, &bPrioriteUnique, &rNum);
      get_valeur_geo_al (AL_ARCH_PRI + wNumeroGroupe - 1, &bLog, &rNum);
      if (bPrioriteUnique)
        { // pour Lagarde
        bTest = (wNumeroPriorite == (DWORD)rNum);
        }
      else
        {
        bTest = (wNumeroPriorite <= (DWORD)rNum);
        }
      }
    }
  return (bTest);
  }

// --------------------------------------------------------------------------
// teste si l'alarme est a imprimer
//
static BOOL test_impression (DWORD wNumeroGroupe, DWORD wNumeroPriorite)
  {
  BOOL bLog;
  FLOAT     rNum;
  BOOL bPrioriteUnique;
  BOOL bTest;

  bTest = FALSE;
  get_valeur_geo_al (AL_IMPR_GLOB, &bLog, &rNum);
  if (bLog)
    { // impr glob
    get_valeur_geo_al (AL_IMPR_GRP + wNumeroGroupe - 1, &bLog, &rNum);
    if (bLog)
      { // impr grp
      get_valeur_geo_al (AL_PRIORITE_UNIQUE, &bPrioriteUnique, &rNum);
      get_valeur_geo_al (AL_IMPR_PRI + wNumeroGroupe - 1, &bLog, &rNum);
      if (bPrioriteUnique)
        { // pour Lagarde
        bTest = (wNumeroPriorite == (DWORD)rNum);
        }
      else
        {
        bTest = (wNumeroPriorite <= (DWORD)rNum);
        }
      }
    }
  return (bTest);
  }

// --------------------------------------------------------------------------
// gestion des compteurs d'alarmes apparues
//
static void MajCptrAlApparue (DWORD wIndexGrp, DWORD wNumeroPriorite)
  {
  tx_anm_geo_al *pGeoAl;

  pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GLOB);
  pGeoAl->Valeur.rValNumerique++;
  if (!NotificationReel (pGeoAl->wPosRet, pGeoAl->Valeur.rValNumerique))
    {
    maj_statut_al (0x8000);
    }

  pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GRP + wIndexGrp);
  pGeoAl->Valeur.rValNumerique++;
  if (!NotificationReel (pGeoAl->wPosRet, pGeoAl->Valeur.rValNumerique))
    {
    maj_statut_al (0x8000);
    }

  TabCptrPri [(wIndexGrp * NB_PRIORITE_MAX_AL) + wNumeroPriorite - 1]++;

  bOkCptr = TRUE; // pour reafficher une fois le message traite entierement
  }

// -----------------------------------------------------------------------
// Impression de la consultation d'archive d'alarme
static void ImprimerRtatConsultation (void)
  {
  BOOL                 bFicConsOuvertDebut;
  DWORD                    wIndex;
  DWORD                    wNbEnr;
  PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE pConsult;
  PENV_AL_DYN							pEnvAlDyn;
  char                    pszTextAl[TAILLE_MAX_MES_AL_COMPLET + 1];
  DWORD                    wNbCarMaxMesAl;
  DWORD                    wNbCarMaxFicArch;
  DWORD                    wNbCarMaxLineImp;
	PX_VAR_SYS	pwPosition;
  FLOAT										*prValeur;
	FLOAT										fOldImp;

	// existe t'il un bloc de consultation d'archive d'alarme ?
  if (existe_repere (bx_anm_consultation_al))
    {
		// oui
    bFicConsOuvertDebut = bFicConsOuvert;
    pEnvAlDyn = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
    OuvreFicCons (pEnvAlDyn->szFicTri, &hficFichierCons6, &bFicConsOuvert);
    wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_consultation_al);
    if (wNbEnr != 0)
      {
			pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, imprimante);
			prValeur   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pwPosition->dwPosCourEx);
			fOldImp = (*prValeur);
			(*prValeur) = NUMERO_IMPRIMANTE;
      for (wIndex = 1; wIndex <= wNbEnr; wIndex++)
        {
        pConsult = (PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE)pointe_enr (szVERIFSource, __LINE__, bx_anm_consultation_al, wIndex);
        if (uFileLireDirect (hficFichierCons6, (pConsult->wPosEnreg - 1), pszTextAl) == NO_ERROR)
          {
          definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
          pszTextAl[wNbCarMaxLineImp] = CAR_FIN;
					stocker_imprimante (StrLength (pszTextAl), pszTextAl, TRUE, TRUE);
          }
        }
      StrSetNull (pszTextAl);
			stocker_imprimante (StrLength (pszTextAl), pszTextAl, TRUE, TRUE);

			pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, imprimante);
			prValeur   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pwPosition->dwPosCourEx);
			(*prValeur) = fOldImp;
      }
    if (!bFicConsOuvertDebut)
      {
      FermeFicCons (&hficFichierCons6, &bFicConsOuvert);
      }
    } // existe bloc bx_anm_consultation_al
  }

// --------------------------------------------------------------------------
//
static void apparition_alarme (DWORD pElement, tx_anm_horodate * pHoroDate)
  {
  tx_anm_element_al    *pElementAl;
  tx_anm_groupe_al     *pGroupeAl;
  DWORD                  wBufferSurv;
  DWORD                  wBufferVisu;
  t_buffer_anm_surv_al *pBufferSurv;
  tx_anm_glob_al       *pGlobAl;
  DWORD                  wIndexClrFore;
  DWORD                  wIndexClrBack;
  BOOL               bLog;
  FLOAT                   rNum;
  char                  pszTextAl[TAILLE_MAX_MES_AL_COMPLET+1];
  BOOL               a_traiter;
	BOOL											bAlPresente;
	BOOL											bAlAcquitee;


  pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, pElement);
  pGroupeAl = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, pElementAl->wNumeroGroupe);
  if (pGroupeAl->type_ack == ACQUIT_AUTO)
    {
    a_traiter = (pElementAl->wTypeTestAlarme == TEST_AL_ETAT);
    }
  else
    {
    a_traiter = TRUE;
    }
  if (a_traiter)
    {
    pElementAl->bEtatPresent = TRUE;
    if (pGroupeAl->type_ack == ACQUIT_AUTO)
      {
      pElementAl->bEtatAcq = TRUE;
      }
    get_valeur_geo_al (AL_TYPE_VISU, &bLog, &rNum);
		DWORD dwAffichageType = (DWORD)rNum;
    switch (dwAffichageType)
      {
      case AFFICHAGE_ETAT:
        if (recherche_alarme_surv (pElement, &wBufferSurv))
          { // elle est deja ds buffer_anm_surv_al
          pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
          FabriqueTexteAl (pElement, pBufferSurv->pszTextAl, pHoroDate);
          if (recherche_alarme_visu (pElement, &wBufferVisu))
            { // elle est deja ds la liste
            get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
            maj_buffer_visu (wBufferVisu, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
            pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
            maj_liste_al (wBufferVisu, pBufferSurv->pszTextAl);
            }
          else
            { // elle n'est pas encore ds le buffer_visu
            if (test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
              { // elle est visualisee
              get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
              ajout_buffer_visu (pElement, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
              pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
              ajout_liste_al (pBufferSurv->pszTextAl);
              }
            }
          }
        else
          { // elle n'est pas encore ds le buffer_surv
          wBufferSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
          pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
          if (wBufferSurv >= pGlobAl->wNbreAlMax)
            { // detruire la plus vieille alarme
            pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, 1);
            if (recherche_alarme_visu (pBufferSurv->wPosEsElement, &wBufferVisu))
              { // elle est ds la liste
              enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, wBufferVisu);
              enleve_liste_al (wBufferVisu);
              }
            enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, 1); // detruit la plus vieille alarme
            maj_statut_al (0x01);
            }
          insere_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, (nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al) + 1));
          wBufferSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
          pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
          pBufferSurv->wPosEsElement = pElement;
          FabriqueTexteAl (pElement, pBufferSurv->pszTextAl, pHoroDate);
          if (test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
            { // elle est visualisee
            get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
            ajout_buffer_visu (pElement, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
            pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
            ajout_liste_al (pBufferSurv->pszTextAl);
            }
          }
        break;

      case AFFICHAGE_HISTO:
			case AFFICHAGE_CONSIGNATION:
        if (recherche_alarme_surv (pElement, &wBufferSurv))
          { // elle est deja ds buffer_anm_surv_al
          if (test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
            { // elle est visualisee
            wBufferVisu = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al);
            pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
            if (wBufferVisu >= pGlobAl->wNbreAlMax)
              {
              enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, 1);
              enleve_liste_al (1);
              }
            get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
            ajout_buffer_visu (pElement, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
            FabriqueTexteAl (pElement, pszTextAl, pHoroDate);
            ajout_liste_al (pszTextAl);
            }
          }
        else
          { // elle n'est pas encore ds le buffer_surv
          wBufferSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
          pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
          if (wBufferSurv >= pGlobAl->wNbreAlMax)
            { // detruire la plus vieille alarme
            pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, 1);
            enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, 1); // detruit la plus vieille alarme
            }
          insere_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, (nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al) + 1));
          wBufferSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
          pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
          pBufferSurv->wPosEsElement = pElement;
          FabriqueTexteAl (pElement, pBufferSurv->pszTextAl, pHoroDate);
					// Alarme � visualiser ?
					if (test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
						{ 
						// Oui => Ajoute le nouvel �v�nement dans l'affichage
						wBufferVisu = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al);
						pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
						if (wBufferVisu >= pGlobAl->wNbreAlMax)
							{
							enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, 1);
							enleve_liste_al (1);
							}
						get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
						ajout_buffer_visu (pElement, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
						pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
						ajout_liste_al (pBufferSurv->pszTextAl);
						}
          }
        break;

      default:
        break;
      }
    if (test_archivage (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite))
      {
      ArchiveAl (pElement, APPARITION_AL, pHoroDate);
      }
    if (test_impression (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite))
      {
      ImprimeAl (pElement, APPARITION_AL, pHoroDate);
      }
    // Gestion des compteurs d'alarmes apparues
    MajCptrAlApparue (pElementAl->wNumeroGroupe - 1, pElementAl->wNumeroPriorite);
    }
  }

// --------------------------------------------------------------------------
// Traite l'�v�nement disparition d'alarme pour un �l�ment
static void disparition_alarme (DWORD pElement, tx_anm_horodate * pHoroDate)
  {
  tx_anm_element_al    *pElementAl;
  tx_anm_glob_al       *pGlobAl;
  t_buffer_anm_surv_al *pBufferSurv;
  DWORD                  wIndexClrFore;
  DWORD                  wIndexClrBack;
  DWORD                  wBufferVisu;
  DWORD                  wBufferSurv;
  BOOL               bLog;
  FLOAT                   rNum;
  char                  pszTextAl[TAILLE_MAX_MES_AL_COMPLET+1];
	BOOL											bAlPresente;
	BOOL											bAlAcquitee;


	// R�cup�re l'�l�ment
  pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, pElement);
	// L'alarme a disparu
  pElementAl->bEtatPresent = FALSE;

	// Selon le type d'affichage courant
  get_valeur_geo_al (AL_TYPE_VISU, &bLog, &rNum);
	DWORD dwAffichageType = (DWORD)rNum;
	switch (dwAffichageType)
    {
    case AFFICHAGE_ETAT:
      if (recherche_alarme_surv (pElement, &wBufferSurv))
        { // elle est deja ds buffer_anm_surv_al
        if (pElementAl->bEtatAcq)
          { // elle disparait
          pElementAl->bEtatAcq = FALSE;
          enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, wBufferSurv);
          if (recherche_alarme_visu (pElement, &wBufferVisu))
            { // elle est ds la liste
            enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, wBufferVisu);
            enleve_liste_al (wBufferVisu);
            }
          }
        else // pas acquitt�e
          {
          if (recherche_alarme_visu (pElement, &wBufferVisu))
            {
            if (test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
              { // elle est toujours visualisee
              get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
              maj_buffer_visu (wBufferVisu, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
              maj_couleur_liste_al (wBufferVisu);
              }
            else
              { // elle n'est plus visualisee
              enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, wBufferVisu);
              enleve_liste_al (wBufferVisu);
              }
            }
          }
        }
      else // pas dans buffer surv
        {
        if (!pElementAl->bEtatAcq)
          { // elle reapparait
          wBufferSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
          pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
          if (wBufferSurv >= pGlobAl->wNbreAlMax)
            { // detruire la plus vieille alarme
            pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, 1);
            if (recherche_alarme_visu (pBufferSurv->wPosEsElement, &wBufferVisu))
              { // elle est ds la liste
              enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, wBufferVisu);
              enleve_liste_al (wBufferVisu);
              }
            enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, 1); // detruit la plus vieille alarme
            maj_statut_al (0x01);
            }
          insere_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, (nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al) + 1));
          wBufferSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
          pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
          pBufferSurv->wPosEsElement = pElement;
          FabriqueTexteAl (pElement, pBufferSurv->pszTextAl, pHoroDate);
					 // Alarme � visualiser ?
          if (test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
            { // elle est visualisee
            get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
            ajout_buffer_visu (pElement, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
            pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
            ajout_liste_al (pBufferSurv->pszTextAl);
            }
          }
        else
          {
          pElementAl->bEtatAcq = FALSE; //reinit
          }
        }
      break;

		case AFFICHAGE_CONSIGNATION:
    case AFFICHAGE_HISTO: // Affichage de l'historique des alarmes
      if (recherche_alarme_surv (pElement, &wBufferSurv))
        { // elle est deja ds buffer_anm_surv_al
        if (pElementAl->bEtatAcq)
          { // elle disparait du buffer de surveillance
          pElementAl->bEtatAcq = FALSE;
          enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, wBufferSurv);
          }
				// Alarme � visualiser ?
        if ((dwAffichageType==AFFICHAGE_HISTO)&& test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
          {
					// oui 
          wBufferVisu = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al);
          pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
          if (wBufferVisu >= pGlobAl->wNbreAlMax)
            {
            enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, 1);
            enleve_liste_al (1);
            }
          get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
          ajout_buffer_visu (pElement, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
          FabriqueTexteAl (pElement, pszTextAl, pHoroDate);
          ajout_liste_al (pszTextAl);
          }
        } // if (recherche_alarme_surv (pElement, &wBufferSurv))
      else
        {
        pElementAl->bEtatAcq = FALSE;
        wBufferSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
        pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
        if (wBufferSurv >= pGlobAl->wNbreAlMax)
          { // detruire la plus vieille alarme
          pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, 1);
          enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, 1); // detruit la plus vieille alarme
          }
        insere_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, (nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al) + 1));
        wBufferSurv = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_surv_al);
        pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
        pBufferSurv->wPosEsElement = pElement;
        FabriqueTexteAl (pElement, pBufferSurv->pszTextAl, pHoroDate);
				// Alarme � visualiser ?
        if ((dwAffichageType=AFFICHAGE_HISTO)&& test_visu (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite, pElementAl->bEtatPresent))
          { 
					// Oui
          wBufferVisu = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al);
          pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
          if (wBufferVisu >= pGlobAl->wNbreAlMax)
            {// detruire la plus vieille alarme
            enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, 1);
            enleve_liste_al (1);
            }
          get_elt_animation_buffer_visu_al (pElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
          ajout_buffer_visu (pElement, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
          pBufferSurv = (t_buffer_anm_surv_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_surv_al, wBufferSurv);
          ajout_liste_al (pBufferSurv->pszTextAl);
          }
        }
      break;

    default:
      break;
    }

  if (test_archivage (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite))
    {
    ArchiveAl (pElement, DISPARITION_AL, pHoroDate);
    }
  if (test_impression (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite))
    {
    ImprimeAl (pElement, DISPARITION_AL, pHoroDate);
    }
  }

// --------------------------------------------------------------------------
//
static void acquittement_alarme (DWORD wBufferVisu)
  {
  tx_anm_element_al    *pElementAl;
  tx_anm_groupe_al     *pGroupeAl;
  t_buffer_anm_visu_al *pBufferVisuAl;
  DWORD                  wBufferSurv;
  DWORD                  wTypeEvt;
  DWORD                 wPosElement;
  DWORD                  wIndexClrFore;
  DWORD                  wIndexClrBack;
  BOOL               bLog;
  FLOAT                   rNum;
  tx_anm_horodate       HoroDate;
  DWORD                  Centiemes;
	BOOL											bAlPresente;
	BOOL											bAlAcquitee;



  wTypeEvt = EVT_RIEN_AL;
  pBufferVisuAl = (t_buffer_anm_visu_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_visu_al, wBufferVisu);
  wPosElement = pBufferVisuAl->wPosEsElement;
  pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, wPosElement);
  pGroupeAl = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, pElementAl->wNumeroGroupe);
  get_valeur_geo_al (AL_TYPE_VISU, &bLog, &rNum);
  if ((pGroupeAl->type_ack != ACQUIT_NONE) && (rNum == AFFICHAGE_ETAT))
    {
    if (!pElementAl->bEtatAcq)
      { // elle n'est pas deja acquittee
      pElementAl->bEtatAcq = TRUE;
      if ((pElementAl->bEtatPresent) && (pElementAl->wTypeTestAlarme == TEST_AL_ETAT))
        { // il faut qu'elle change de couleur
        get_elt_animation_buffer_visu_al (wPosElement, &wIndexClrFore, &wIndexClrBack, &bAlPresente, &bAlAcquitee);
        maj_buffer_visu (wBufferVisu, wIndexClrFore, wIndexClrBack, bAlPresente, bAlAcquitee);
        maj_couleur_liste_al (wBufferVisu);
        wTypeEvt = ACQUITTEMENT_AL;
        }
      else
        { // elle disparait de la liste
        pElementAl->bEtatAcq = FALSE;
        pElementAl->bEtatPresent = FALSE;
        if (recherche_alarme_surv (pBufferVisuAl->wPosEsElement, &wBufferSurv))
          { // elle est bien sur ds buffer_anm_surv_al
          enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_surv_al, wBufferSurv);
          }
        enleve_enr (szVERIFSource, __LINE__, 1, buffer_anm_visu_al, wBufferVisu);
        enleve_liste_al (wBufferVisu);
        wTypeEvt = ACQUITTEMENT_DISP_AL;
        } // pas etat_present
      } // pas acq

    if (wTypeEvt)
      {
      TpsLireDateHeure (&(HoroDate.Jour), &(HoroDate.Mois), &(HoroDate.Annee),
                        &(HoroDate.JourDeSemaine), &(HoroDate.Heures), &(HoroDate.Minutes),
                        &(HoroDate.Secondes), &Centiemes);
      HoroDate.MilliSecondes = 10 * Centiemes;
      pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, wPosElement);
      if (test_archivage (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite))
        {
        ArchiveAl (wPosElement, wTypeEvt, &HoroDate);
        }
      if (test_impression (pElementAl->wNumeroGroupe, pElementAl->wNumeroPriorite))
        {
        ImprimeAl (wPosElement, wTypeEvt, &HoroDate);
        }
      }
    }
  }

// --------------------------------------------------------------------------
//
static void acquittement_global (void)
  {
  DWORD wIndex;
  DWORD wNbEnr;

  wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al);
  for (wIndex = wNbEnr; wIndex > 0; wIndex--)
    {
    acquittement_alarme (wIndex);
    }
  }

// --------------------------------------------------------------------------
// Fonctions d'Animation:
// void Anim (BYTE **pBuff, DWORD Position);
// --------------------------------------------------------------------------
static void AnimNull (BYTE **pBuff, DWORD Position)
  {
  return;
  }
// --------------------------------------------------------------------------
// un cpteur a change (ou plusieurs) : on reaffiche tout
// MAJ des compteurs priorit� a partir de TabCptrPri
// --------------------------------------------------------------------------
static void ReafficheBoiteCptr (void)
  {
  char            pszText[TAILLE_MAX_MES_AL_COMPLET+1];
  tx_anm_geo_al *	pGeoAl;
  DWORD           wIndex;
  DWORD           wDebut;
  DWORD           wFin;

  if (existe_repere (bx_anm_groupe_al))
    {
		PENV_AL_DYN			pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
    if (pEnv->hwndCptrAlDyn)
      {
      StrSetNull (pszText);
      pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GLOB);
      SetDlgItemInt (pEnv->hwndCptrAlDyn, ID_CPTR_GLOB_AL_DYN, (USHORT)pGeoAl->Valeur.rValNumerique, FALSE);

      pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GRP + wIndexCptrGrp);
      SetDlgItemInt (pEnv->hwndCptrAlDyn, ID_CPTR_GRP_AL_DYN, (USHORT)pGeoAl->Valeur.rValNumerique, FALSE);

      SendDlgItemMessage (pEnv->hwndCptrAlDyn, ID_NUM_PRI_AL_DYN, LB_RESETCONTENT, 0, 0);
      wDebut = wIndexCptrGrp * NB_PRIORITE_MAX_AL;
      wFin = wDebut + NB_PRIORITE_MAX_AL;
      for (wIndex = wDebut; wIndex < wFin; wIndex++)
        {
        StrDWordToStr (pszText, TabCptrPri[wIndex]);
        SendDlgItemMessage (pEnv->hwndCptrAlDyn, ID_NUM_PRI_AL_DYN, LB_INSERTSTRING,
					(WPARAM)-1, (LPARAM)pszText);
        }
      }
    }
  }


// --------------------------------------------------------------------------
// Re�u a chaque fin de message venant de pcsexe
// --------------------------------------------------------------------------
static void AnimFin (BYTE **pBuff, DWORD Position)
  {
  if (bOkListe)
    { // un ou plusieurs parametres de visualisation a chang�
    refabrique_liste_al ();
    bOkListe = FALSE;
    }

  if (bOkCptr)
    { // un compteur a change d'etat (ou plusieurs)
    ReafficheBoiteCptr ();
    bOkCptr = FALSE;
    }

  if (bFicArchOuvert) // on a archiv� pdt le cycle
    {
    FermeFicArch();
    bFicArchOuvert = FALSE;
    }
  return;
  }

// --------------------------------------------------------------------------
static void AnimAck (BYTE **pBuff, DWORD Position)
  {
  MbxAnmAlAck ();

  return;
  }

// --------------------------------------------------------------------------
static void AnimForcage (BYTE **pBuff, DWORD Position)
  {
  }

// --------------------------------------------------------------------------
// gestion des variables systemes de pcsexe
//
static void AnimGeo (BYTE **pBuff, DWORD pElement)
  {
  tx_anm_geo_al *	pAnmGeoAl;
  PENV_AL_DYN			pEnv;

  pAnmGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, pElement);

  // il ne s'agit pas d'un compteur car ils ne sont pas envoyes
  //
  switch (pAnmGeoAl->wTypeValeur)
    {
    case LOGIQUE_AL :
      pAnmGeoAl->Valeur.bValLogique = ExtraireBoolean (pBuff);
      switch (pElement)
        {
        case AL_SURV_GRP :
          break;
        case AL_VISU_GRP :
          break;

        case AL_IMPR_GLOB :
          if (pAnmGeoAl->Valeur.bValLogique)
            { // init impr si pas deja initialis�e
            //InitImprimante ();
            }
          else
            { // arret impr si pas deja arret�e
            //TermineImprimante ();
            }
          break;

        case AL_VISIBLE :
          if (pAnmGeoAl->Valeur.bValLogique)
            { // fenetre visible
            ::ShowWindow (hwndAlDyn, SW_RESTORE); //$$::SetWindowPos (hwndAlDyn, HWND_TOP, 0, 0, 0, 0, SWP_RESTORE | SWP_ZORDER);
            }
          else
            { // fenetre en icone
            ::ShowWindow (hwndAlDyn, SW_MINIMIZE); //$$::SetWindowPos (hwndAlDyn, HWND_BOTTOM, 0, 0, 0, 0, SWP_MINIMIZE | SWP_ZORDER);
            pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
            if (pEnv->hwndSurvAlDyn)
              {
              ::DestroyWindow (pEnv->hwndSurvAlDyn);
              }
            if (pEnv->hwndVisuAlDyn)
              {
              ::DestroyWindow (pEnv->hwndVisuAlDyn);
              }
            if (pEnv->hwndArchAlDyn)
              {
              ::DestroyWindow (pEnv->hwndArchAlDyn);
              }
            if (pEnv->hwndImprAlDyn)
              {
              ::DestroyWindow (pEnv->hwndImprAlDyn);
              }
            if (pEnv->hwndCptrAlDyn)
              {
              ::DestroyWindow (pEnv->hwndCptrAlDyn);
              }
            if (pEnv->bFenAlArchiveOuverte)
              {
              ::DestroyWindow (hwndAlArchive);
							hwndAlArchive = NULL;
              }
            }
          break;

        default :
          break;
        }
      break;

    case NUMERIQUE_AL :
      pAnmGeoAl->Valeur.rValNumerique = ExtraireReel (pBuff);
      break;

    default :
      break;
    } // switch

  if ((pElement == AL_VISU_GLOB) ||
      (pElement == AL_PRIORITE_UNIQUE) ||
      (pElement == AL_TYPE_VISU) ||
      ((pElement >= (AL_VISU_GRP + 0)) && (pElement <= (AL_VISU_GRP + NB_GROUPE_MAX_AL - 1))) ||
      ((pElement >= (AL_VISU_PRI + 0)) && (pElement <= (AL_VISU_PRI + NB_GROUPE_MAX_AL - 1))) ||
      ((pElement >= (AL_VISU_ACTIVE + 0)) && (pElement <= (AL_VISU_ACTIVE + NB_GROUPE_MAX_AL - 1))))
    {
    bOkListe = TRUE;
    }

  return;
  }

// --------------------------------------------------------------------------
// Boite de dialogue alarmes dynamiques : COMPTEURS :
// mise � 0 des compteurs groupes et priorit�s ds BD animateur + notification
// --------------------------------------------------------------------------
static void RazEtNotificationCptrGrpEtPri (void)
  {
  DWORD           wIndex;
  DWORD           wIndexPri;
  DWORD           wNbEnr;
  tx_anm_geo_al *pGeoAl;

  if (existe_repere (bx_anm_groupe_al))
    {
    wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al);
    for (wIndex = 0; wIndex < wNbEnr; wIndex++)
      {
      pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GRP + wIndex);
      pGeoAl->Valeur.rValNumerique = (FLOAT) 0;
      if (!NotificationReel (pGeoAl->wPosRet, (FLOAT) 0))
        {
        maj_statut_al (0x8000);
        }

      for (wIndexPri = 0; wIndexPri < NB_PRIORITE_MAX_AL; wIndexPri++)
        {
        TabCptrPri [wIndex + wIndexPri] = 0;
        }
      }
    }
  }

// --------------------------------------------------------------------------
// RAZ d'un cpteur groupe : mettre ses priorites a 0
// --------------------------------------------------------------------------
static void RazCptrPri (DWORD wIndexGrp)
  {
  DWORD wDebut;
  DWORD wFin;
  DWORD wIndexPri;

  wDebut = wIndexGrp * NB_PRIORITE_MAX_AL;
  wFin = wDebut + NB_PRIORITE_MAX_AL;
  for (wIndexPri = wDebut; wIndexPri < wFin; wIndexPri++)
    {
    TabCptrPri [wIndexPri] = 0;
    }
  }

// --------------------------------------------------------------------------
// remise a zero des compteurs d'alarmes apparues
//
static void AnimRaz (BYTE **pBuff, DWORD pElement)
  {
  tx_anm_geo_al *pAnmGeoAl;
  FLOAT            rValeur;
  DWORD           wNumGrp;
  DWORD           wSousFonction;

  bOkCptr = TRUE; // rafraichit la boite en fin de trame

  wSousFonction = ExtraireWord (pBuff);
  switch (wSousFonction)
    {
    case ANM_AL_RAZ_GLOB:   // RAZ compteur global
      pAnmGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GLOB);
      pAnmGeoAl->Valeur.rValNumerique = (FLOAT) 0;
      if (!NotificationReel (pAnmGeoAl->wPosRet, pAnmGeoAl->Valeur.rValNumerique))
        {
        maj_statut_al (0x8000);
        }
      RazEtNotificationCptrGrpEtPri ();
      break;

    case ANM_AL_RAZ_GRP:    // RAZ compteur groupe
      wNumGrp = (DWORD)ExtraireReel (pBuff);
      RazCptrPri (wNumGrp - 1); // index groupe

      pAnmGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GRP + wNumGrp - 1);
      rValeur = pAnmGeoAl->Valeur.rValNumerique;
      pAnmGeoAl->Valeur.rValNumerique = (FLOAT) 0;
      if (!NotificationReel (pAnmGeoAl->wPosRet, pAnmGeoAl->Valeur.rValNumerique))
        {
        maj_statut_al (0x8000);
        }

      pAnmGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GLOB);
      pAnmGeoAl->Valeur.rValNumerique = pAnmGeoAl->Valeur.rValNumerique - rValeur;
      if (pAnmGeoAl->Valeur.rValNumerique < 0)
        {
        pAnmGeoAl->Valeur.rValNumerique = (FLOAT) 0;
        }
      if (!NotificationReel (pAnmGeoAl->wPosRet, pAnmGeoAl->Valeur.rValNumerique))
        {
        maj_statut_al (0x8000);
        }
      break;

    case ANM_AL_ACQ_GLOB:    // acquittement global
      acquittement_global ();
      break;

    default:
      break;
    }

  return;
  }

// --------------------------------------------------------------------------
// initialise les variables des blocs bx_anm_cour_xxx_al
// --------------------------------------------------------------------------
static void AnimEvtInit (BYTE **pBuff, DWORD pElement)
  {
  tx_anm_element_al  *pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, pElement);
  tx_anm_variable_al *pVariableAl;
  DWORD                IndexVariable;
  BOOL            *pLog;
  FLOAT                *pNum;
  char               *pMes;

  for (IndexVariable = pElementAl->wPosPremVariable; IndexVariable < pElementAl->wPosPremVariable + pElementAl->wNbreVariable; IndexVariable++)
    {
    pVariableAl = (tx_anm_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_variable_al, IndexVariable);
    if (pVariableAl->wTypeVar == VARIABLE_AL)
      { // variable
      switch (pVariableAl->wBlocGenre)
        {
        case bx_anm_cour_log_al:
          pLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_log_al, pVariableAl->wPosEs);
          (*pLog) = ExtraireBoolean (pBuff);
          break;

        case bx_anm_cour_num_al:
          pNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_num_al, pVariableAl->wPosEs);
          (*pNum) = ExtraireReel (pBuff);
          break;

        case bx_anm_cour_mes_al:
          pMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_mes_al, pVariableAl->wPosEs);
          ExtrairePsz (pBuff, pMes);
          break;

        default:
          break;
        } // switch
      } // variable
    } // for IndexVariable

  return;
  }

// --------------------------------------------------------------------------
// Des valeurs d'alarmes ont chang�
// --------------------------------------------------------------------------
static void AnimEvt (BYTE **pBuff, DWORD pElement)
  {
  DWORD EvenementAl;
  tx_anm_horodate  HoroDate;


  test_evenement (pBuff, &EvenementAl, pElement, &HoroDate);
  switch (EvenementAl)
    {
    case APPARITION_AL :
      apparition_alarme (pElement, &HoroDate);
      break;

    case DISPARITION_AL :
      disparition_alarme (pElement, &HoroDate);
      break;

    default :
      break;
    } // switch

  return;
  }

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
static void ExecService (DWORD TypeService, BYTE ** ppBuff)
  {
  HANDLE_ANM_AL handleAnmAl = ExtraireHandleAnmAl (ppBuff);

  aServicesAnmAl [handleAnmAl.Service].Anim [TypeService] (ppBuff, handleAnmAl.Element);
  return;
  }

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
static void anm_cnv_coord_pm (PLONG x, PLONG y, PLONG cx, PLONG cy)
  {
  LONG c1,c2;

  c1 = *x;
  c2 = *y;
  Spc_CvtPtEnPixel (hSpace, &c1, &c2);
  *x = c1;
  *y = c2;
  c1 = *cx;
  c2 = *cy;
  Spc_CvtSizeEnPixel (hSpace, &c1, &c2);
  *cx = c1;
  *cy = c2;
  }

// --------------------------------------------------------------------------
// creation de la fenetre de consulation d'archives d'alarme (appelee par la fenetre al dyn)
// --------------------------------------------------------------------------
static BOOL bCreeFenetreAlArchive (void)
  {
  BOOL		bOk = FALSE;
  char    pszMessInf[c_nb_car_message_inf];
	static	BOOL bClasseAlArchiveEnregistree = FALSE;

	// enregistre la classe si n�cessaire
	if (!bClasseAlArchiveEnregistree)
		{
		WNDCLASS wc;

		wc.style					= CS_HREDRAW | CS_VREDRAW; 
		wc.lpfnWndProc		= wndprocAlArchive; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= (HBRUSH)(COLOR_MENU+1);
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseAlArchive; 
		bClasseAlArchiveEnregistree = RegisterClass (&wc);
		}

	// classe enregistr�e ?
	if (bClasseAlArchiveEnregistree)
    {
		// cr�e la fen�tre
    bLngLireInformation (c_inf_titre_al_cons, pszMessInf);

    // FCF_STANDARD & ~FCF_MENU & ~FCF_ACCELTABLE & ~FCF_TASKLIST & ~FCF_ICON & ~FCF_MINBUTTON;
		hwndAlArchive = CreateWindow (
			szClasseAlArchive,	// class name
			pszMessInf,					// window name
			WS_VISIBLE | WS_OVERLAPPEDWINDOW, // | WS_CLIPCHILDREN |WS_CLIPSIBLINGS |  window style
			(Appli.sizEcran.cx - CX_FEN_AL_ARCHIVE) / 2,	(Appli.sizEcran.cy - CY_FEN_AL_ARCHIVE) / 2,	
			CX_FEN_AL_ARCHIVE,	CY_FEN_AL_ARCHIVE,
			hwndAlDyn,					// handle parent window $$ ??
			NULL,								// handle to menu or child-window identifier
			Appli.hinst, NULL);

    bOk = (hwndAlArchive != NULL);
    }
  return (bOk);
  } // bCreeFenetreAlArchive

// --------------------------------------------------------------------------
//
static void FermeFenetresAnimAlarmes (void)
  {
  if (existe_repere (bx_anm_element_al))
    {
		if (hwndAlDyn)
			{
			::DestroyWindow (hwndAlDyn);
			hwndAlDyn = NULL;
			}
    }
  SpcFerme (&hSpace);
  return;
  }

//------------------------------------------------------------------------
// GESTION DES BOITES DE DIALOGUE PARAMETRAGE alarmes dynamiques :
// surv, visu, arch, impr
//------------------------------------------------------------------------
// init des textes (titre, var globale ...)
//------------------------------------------------------------------------
static void InitBoiteParametrage (HWND hwnd,
                                  DWORD wInfTitre,
                                  DWORD wInfVarGlob,
                                  DWORD wPosGeoGlob,
                                  DWORD wInfGroupes,
                                  DWORD wInfPriorites)
  {
  char           pszMessInf[c_nb_car_message_inf];
  tx_anm_geo_al *pGeoAl;

  bLngLireInformation (wInfTitre, pszMessInf);
  ::SetWindowText (hwnd, pszMessInf);

  bLngLireInformation (wInfVarGlob, pszMessInf);
  SetDlgItemText (hwnd, ID_GLOB_AL_DYN, pszMessInf);

  bLngLireInformation (wInfGroupes, pszMessInf);
  SetDlgItemText (hwnd, ID_GRP_AL_DYN, pszMessInf);

  bLngLireInformation (wInfPriorites, pszMessInf);
  SetDlgItemText (hwnd, ID_PRI_AL_DYN, pszMessInf);

  pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, wPosGeoGlob);
	::CheckDlgButton (hwnd, ID_GLOB_AL_DYN, pGeoAl->Valeur.bValLogique ? BST_CHECKED : BST_UNCHECKED);
  }

//-------------------------------------------------------------------------
// init contexte a partir BD animateur
//-------------------------------------------------------------------------
static void InitContexteBoiteParametrage (DWORD wPosGeoPri, DWORD *wIndexGroupe, DWORD TabPriorite[])
  {
  DWORD           wIndex;
  DWORD           wNbEnr;
  tx_anm_geo_al *pGeoAl;

  wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al);
  (*wIndexGroupe) = 0;
  for (wIndex = 0; wIndex < wNbEnr; wIndex++)
    {
    pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, wPosGeoPri + wIndex);
    TabPriorite [wIndex] = (DWORD)pGeoAl->Valeur.rValNumerique;
    }
  }
//------------------------------------------------------------------------
// init des groupes dans la liste_box et dans la combo_box
//------------------------------------------------------------------------
static void InitBoiteParametrageGroupe (HWND hwnd, DWORD wPosGeoGrp)
  {
  DWORD              wNbEnr;
  DWORD              wIndex;
  tx_anm_groupe_al *pGroupeAl;
  tx_anm_geo_al    *pGeoAl;

  wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al);
  for (wIndex = 1; wIndex <= wNbEnr; wIndex++)
    {
    pGroupeAl = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, wIndex);
    SendDlgItemMessage (hwnd, ID_LIST_GRP_AL_DYN, LB_INSERTSTRING,
			(WPARAM)-1, (LPARAM)(pGroupeAl->pszNomGroupe));
    SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN, CB_INSERTSTRING,
			(WPARAM)-1, (LPARAM)(pGroupeAl->pszNomGroupe));

    pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, wPosGeoGrp + wIndex - 1);
    SendDlgItemMessage (hwnd, ID_LIST_GRP_AL_DYN, LB_SETSEL, pGeoAl->Valeur.bValLogique,wIndex - 1);
    }
  }

//-----------------------------
// Dessin de la liste priorit� 
//-----------------------------
static void MajListeParametragePriorite (HWND hwnd, INT iIndex)
  {
  FLOAT     rNum;
  BOOL bPrioriteUnique;

	SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_SETSEL, FALSE, -1); // deselectionne tout
	get_valeur_geo_al (AL_PRIORITE_UNIQUE, &bPrioriteUnique, &rNum);
	if ((bPrioriteUnique) || (iIndex == 0))
		{
		SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_SETSEL, TRUE, iIndex);
		}
	else
		{
		// selection du premier niveau au dernier niveau
		SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_SELITEMRANGEEX , 0, iIndex);
		}
	}

//------------------------------------------------------------------------
// init de la list_box numeros priorit�
//------------------------------------------------------------------------
static void InitBoiteParametragePriorite (HWND hwnd, DWORD wIndexGroupe, DWORD TabPriorite [])
  {
  DWORD wIndex;
  char szPriorite[3];

  StrSetNull (szPriorite);
  for (wIndex = 1; wIndex <= NB_PRIORITE_MAX_AL; wIndex++)
    {
    StrDWordToStr (szPriorite, wIndex);
    SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_INSERTSTRING,
			(WPARAM)-1, (LPARAM)szPriorite);
    }
	MajListeParametragePriorite (hwnd, (TabPriorite[wIndexGroupe] - 1));
  }

//--------------------------------------------------------------------------
// sauve le parametrage intermediaire des priorites
//--------------------------------------------------------------------------
static void SauveBoiteParametragePriorite (HWND hwnd, DWORD wIndexGroupe, DWORD TabPriorite[])
  {
  DWORD wIndex;
  FLOAT     rNum;
  BOOL bPrioriteUnique;
	INT TabSelection [NB_PRIORITE_MAX_AL];

	get_valeur_geo_al (AL_PRIORITE_UNIQUE, &bPrioriteUnique, &rNum);
	if (bPrioriteUnique)
		{
		// Un seul est selectionn� par forcage
		TabPriorite[wIndexGroupe] = 0; 
		SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_GETSELITEMS, NB_PRIORITE_MAX_AL, (LPARAM)TabSelection);
	  for (wIndex = 0; wIndex < NB_PRIORITE_MAX_AL; wIndex++)
	    {
			if (TabSelection [wIndex] > 0)
				{
				if ((DWORD)TabSelection [wIndex]	> TabPriorite[wIndexGroupe]) 
					{
					TabPriorite[wIndexGroupe] = TabSelection [wIndex];
					}
				}
			}
		TabPriorite[wIndexGroupe]++;
		}
	else
		{
		// Astuce le nombre de selection est egale au numero(+1) du dernier selectionne
		TabPriorite[wIndexGroupe] = (DWORD) SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_GETSELCOUNT, 0, 0);
		}
  }

//------------------------------------------------------------------------
// init de la priorit� suivant le groupe selectionn�
//------------------------------------------------------------------------
static void MajBoiteParametragePriorite (HWND hwnd, DWORD *wIndexGroupe, DWORD TabPriorite [])
  {

  (*wIndexGroupe) = (DWORD)  SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN, CB_GETCURSEL, 0, 0);
	MajListeParametragePriorite (hwnd, (TabPriorite[*wIndexGroupe] - 1));
  }

//------------------------------------------------------------------------
// maj BD animateur et notification a pcsexe
//------------------------------------------------------------------------
static void ValidationBoiteParametrageGrp (HWND hwnd, DWORD wPosGeoGrp)
  {
  DWORD           wNbEnr;
  DWORD           wIndex;
  DWORD           wRetour;
  INT							TabGroupe [NB_GROUPE_MAX_AL];
  tx_anm_geo_al  *pGeoAl;

  wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al);
  for (wIndex = 0; wIndex < wNbEnr; wIndex++)
    {
    TabGroupe [wIndex] = -1;
		pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, wPosGeoGrp + wIndex);
		pGeoAl->Valeur.bValLogique = FALSE;
    }
	wRetour = (DWORD)  SendDlgItemMessage (hwnd, ID_LIST_GRP_AL_DYN, LB_GETSELITEMS, wNbEnr, (LPARAM)TabGroupe);
  for (wIndex = 0; wIndex < wNbEnr; wIndex++)
    {
		if (TabGroupe [wIndex] != -1)
			{
			pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, wPosGeoGrp + TabGroupe [wIndex]);
			pGeoAl->Valeur.bValLogique = TRUE;
			}
    }
  for (wIndex = 0; wIndex < wNbEnr; wIndex++)
    {
		pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, wPosGeoGrp + wIndex);
		if (!NotificationBoolean (pGeoAl->wPosRet, pGeoAl->Valeur.bValLogique))
			{
			maj_statut_al (0x8000);
			}
		}
  }

//--------------------------------------------------------------------------
// maj. BD animateur et notification vers pcsexe � partir de TabPriorite
//--------------------------------------------------------------------------
static void ValidationBoiteParametragePri (HWND hwnd, DWORD wPosGeoPri, DWORD wIndexGroupe, DWORD TabPriorite[])
  {
  DWORD           wNbEnr;
  DWORD           wIndex;
  tx_anm_geo_al *pGeoAl;

  wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al);
  for (wIndex = 0; wIndex < wNbEnr; wIndex++)
    {
    pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, wPosGeoPri + wIndex);
    pGeoAl->Valeur.rValNumerique = (FLOAT) TabPriorite [wIndex];
    if (!NotificationReel (pGeoAl->wPosRet, pGeoAl->Valeur.rValNumerique))
      {
      maj_statut_al (0x8000);
      }
    }
  }

// -----------------------------------------------------------------------
// Traitement du message WM_ACTIVATE dans une boite de dialogue modeless
// -----------------------------------------------------------------------
static void OnActivateCommunBoiteModeless (HWND hwnd, WPARAM wParam)
	{
	if (LOWORD (wParam) == WA_INACTIVE)
		hwndDialogueActive = NULL;
	else
		hwndDialogueActive = hwnd;
	}

// ------------------------------------------------------------------------
// Boite de dialogue modeless de parametrage de la surveillance des alarmes
// ------------------------------------------------------------------------
static BOOL CALLBACK dlgprocSurvAlDyn (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL           mres = 0;              // Valeur de retour
  tx_anm_geo_al*		pGeoAl;
  PENV_AL_DYN				pEnv;
	INT								iIndexSel;
  DATA_DLG_AL_DYN*	pDataDlg;

  switch (msg)
    {
    case WM_INITDIALOG:
      pDataDlg = (DATA_DLG_AL_DYN*)pCreeEnv (hwnd, sizeof (DATA_DLG_AL_DYN));
      if (pDataDlg)
        {
        // initialisation des titres
        InitBoiteParametrage (hwnd, c_inf_param_surv_al, c_inf_surv_glob_al, AL_SURV_GLOB,
                              c_inf_groupes_al, c_inf_priorites_al);

        if (existe_repere (bx_anm_groupe_al))
          {
          // initialisation des textes des groupes et des valeurs initiales
          InitBoiteParametrageGroupe (hwnd, AL_SURV_GRP);

          // initialisation contexte : num_grp = 0 et TabPriorite <-- BD animateur
          InitContexteBoiteParametrage (AL_SURV_PRI, &pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

          // initialisation des cases � cocher priorit�s (ordre � respecter)
          InitBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

          // selectionne le premier groupe : A FAIRE APRES pSelectEnv
          SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN,CB_SETCURSEL, pDataDlg->wIndexGroupe, 0);
          }
        else
          { 
					// bx_anm_groupe_al n'existe pas
          ::EnableWindow (::GetDlgItem (hwnd, ID_LIST_GRP_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_LIST_PRI_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_NUM_PRI_AL_DYN), FALSE);
          }
        }

      mres = FALSE;
      break;

    case WM_DESTROY:
      pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
			if (pEnv)
				pEnv->hwndSurvAlDyn = NULL;

			bLibereEnv (hwnd);
      break;

    case WM_CLOSE:
      ::DestroyWindow (hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_LIST_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              if (existe_repere (bx_anm_groupe_al))
                {
                pDataDlg = (DATA_DLG_AL_DYN*)pGetEnv (hwnd);

                // sauve les priorites de la selection precedente
                SauveBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

                // remet les priorites du nouveau groupe selectionne
                MajBoiteParametragePriorite (hwnd, &pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
                }
              break;
            } // HIWORD(mp1)
          break; //case ID_LIST_PRI_AL_DYN

        case ID_NUM_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case LBN_SELCHANGE:
              if (existe_repere (bx_anm_groupe_al))
                {
								iIndexSel = SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_GETCARETINDEX, 0, 0);
								MajListeParametragePriorite (hwnd, iIndexSel);
                }
              break;
            } // HIWORD(mp1)
          break; //case ID_NUM_PRI_AL_DYN


				case ID_VALIDER_AL:
					{
				  BOOL              bRetour;

          pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_SURV_GLOB);
          bRetour = IsDlgButtonChecked (hwnd, ID_GLOB_AL_DYN) == BST_CHECKED;
          pGeoAl->Valeur.bValLogique = bRetour;
          if (!NotificationBoolean (pGeoAl->wPosRet, bRetour))
            {
            maj_statut_al (0x8000);
            }

          if (existe_repere (bx_anm_groupe_al))
            {
            pDataDlg = (DATA_DLG_AL_DYN*)pGetEnv (hwnd);

            // Groupes : maj BD animateur et notification vers pcsexe
            ValidationBoiteParametrageGrp (hwnd, AL_SURV_GRP);

            // sauve les priorites de la selection precedente
            SauveBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

            // Priorit�s : maj BD animateur et notification vers pcsexe
            ValidationBoiteParametragePri (hwnd, AL_SURV_PRI, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
            }
          ::DestroyWindow (hwnd);
					}
          break;

				case IDCANCEL:
				case ID_QUITTER_AL:
          ::DestroyWindow (hwnd);
          break;
        }  //(LOWORD(mp1))
      break; // WM_COMMAND

		case WM_ACTIVATE:
			OnActivateCommunBoiteModeless (hwnd, mp1);
			break;
    }
  return mres;
  }

// -----------------------------------------------------------------------
// Boite de dialogue modeless de parametrage
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocVisuAlDyn (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL             mres = 0;              // Valeur de retour
  BOOL                bTraite = TRUE;        // Indique commande trait�e
  BOOL                bRetour;
  int                 iRetour;
  tx_anm_geo_al *			pGeoAl;
  PENV_AL_DYN					pEnv;
  DATA_DLG_AL_DYN*		pDataDlg;
  char                pszMessInf[c_nb_car_message_inf];
  DWORD               wNbEnr;
  DWORD               wIndex;
  INT									iIndexSel;
	
  switch (msg)
    {
    case WM_INITDIALOG:
			{
      pDataDlg = (DATA_DLG_AL_DYN*)pCreeEnv (hwnd, sizeof (DATA_DLG_AL_DYN));
      if (pDataDlg)
        {
        // initialisation des titres
        InitBoiteParametrage (hwnd, c_inf_param_visu_al, c_inf_visu_glob_al, AL_VISU_GLOB,
                              c_inf_groupes_al, c_inf_priorites_al);

        bLngLireInformation (c_inf_active_al, pszMessInf);
        SetDlgItemText (hwnd, ID_ACTIVE_AL_DYN, pszMessInf);

        bLngLireInformation (c_inf_type_etat_al, pszMessInf);
        SetDlgItemText (hwnd, ID_TYPE_ETAT_AL_DYN, pszMessInf);

        bLngLireInformation (c_inf_type_histo_al, pszMessInf);
        SetDlgItemText (hwnd, ID_TYPE_HISTO_AL_DYN, pszMessInf);

        // initialisation boutons radio type visu
        pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_TYPE_VISU);
        switch ((DWORD)pGeoAl->Valeur.rValNumerique)
          {
          case AFFICHAGE_ETAT:
						::CheckDlgButton (hwnd, ID_TYPE_ETAT_AL_DYN, BST_CHECKED);
						::CheckDlgButton (hwnd, ID_TYPE_HISTO_AL_DYN, BST_UNCHECKED);
						::CheckDlgButton (hwnd, ID_TYPE_CONSIGNATION_AL_DYN, BST_UNCHECKED);
            break;
          case AFFICHAGE_HISTO:
						::CheckDlgButton (hwnd, ID_TYPE_ETAT_AL_DYN, BST_UNCHECKED);
						::CheckDlgButton (hwnd, ID_TYPE_HISTO_AL_DYN, BST_CHECKED);
						::CheckDlgButton (hwnd, ID_TYPE_CONSIGNATION_AL_DYN, BST_UNCHECKED);
            break;
					case AFFICHAGE_CONSIGNATION:
						::CheckDlgButton (hwnd, ID_TYPE_ETAT_AL_DYN, BST_UNCHECKED);
						::CheckDlgButton (hwnd, ID_TYPE_HISTO_AL_DYN, BST_UNCHECKED);
						::CheckDlgButton (hwnd, ID_TYPE_CONSIGNATION_AL_DYN, BST_CHECKED);
          default:
            break;
          }

        if (existe_repere (bx_anm_groupe_al))
          {
          // initialisation des textes des groupes et des valeurs initiales
          InitBoiteParametrageGroupe (hwnd, AL_VISU_GRP);

          // initialisation contexte : num_grp=0 TabPriorite & TabAlActive <-- BD animateur
          InitContexteBoiteParametrage (AL_VISU_PRI, &pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
          wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al);
          for (wIndex = 0; wIndex < wNbEnr; wIndex++)
            {
            pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_VISU_ACTIVE + wIndex);
            pDataDlg->TabAlActive [wIndex] = pGeoAl->Valeur.bValLogique;
            }

          // initialisation des cases � cocher priorit�s (ordre � respecter)
          InitBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
					::CheckDlgButton (hwnd, ID_ACTIVE_AL_DYN, pDataDlg->TabAlActive[pDataDlg->wIndexGroupe] ? BST_CHECKED : BST_UNCHECKED);

          // selectionne le premier groupe : A FAIRE APRES pSelectEnv
          SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN,CB_SETCURSEL, pDataDlg->wIndexGroupe, 0);
          }
        else
          { 
					// bx_anm_groupe_al n'existe pas
          ::EnableWindow (::GetDlgItem (hwnd, ID_LIST_GRP_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_LIST_PRI_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_NUM_PRI_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_ACTIVE_AL_DYN), FALSE);
          }
        }

      mres = (LRESULT)FALSE;
			}
      break;

    case WM_DESTROY:
			{
      pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
			if (pEnv)
				pEnv->hwndVisuAlDyn = NULL;
      
			bLibereEnv (hwnd);
			}
			break;

    case WM_CLOSE:
			{
      ::DestroyWindow (hwnd);
			}
      break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
				case ID_LIST_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              bTraite = TRUE;
              if (existe_repere (bx_anm_groupe_al))
                {
                pDataDlg = (DATA_DLG_AL_DYN*)pGetEnv (hwnd);

                // sauve les priorites de la selection precedente et al_visu_active
                SauveBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
                pDataDlg->TabAlActive[pDataDlg->wIndexGroupe] = (IsDlgButtonChecked (hwnd, ID_ACTIVE_AL_DYN) == BST_CHECKED);

                // remet les priorites du nouveau groupe selectionne et al_visu_active
                MajBoiteParametragePriorite (hwnd, &pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
								::CheckDlgButton (hwnd, ID_ACTIVE_AL_DYN, pDataDlg->TabAlActive[pDataDlg->wIndexGroupe] ? BST_CHECKED : BST_UNCHECKED);
                }
              break;
            }
          break;

        case ID_NUM_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case LBN_SELCHANGE:
              if (existe_repere (bx_anm_groupe_al))
                {
								iIndexSel = SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_GETCARETINDEX, 0, 0);
								MajListeParametragePriorite (hwnd, iIndexSel);
                }
              break;

            default:
              break;
            } // HIWORD(mp1)
          break; //case ID_NUM_PRI_AL_DYN

				case ID_VALIDER_AL:
          pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_VISU_GLOB);
          bRetour = (IsDlgButtonChecked (hwnd, ID_GLOB_AL_DYN) == BST_CHECKED);
          pGeoAl->Valeur.bValLogique = bRetour;
          if (!NotificationBoolean (pGeoAl->wPosRet, bRetour))
            {
            maj_statut_al (0x8000);
            }

          // Type affichage : maj BD animateur et notification vers pcsexe
          pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_TYPE_VISU);
          iRetour = (int)nCheckIndex (hwnd, ID_TYPE_ETAT_AL_DYN); //$$LOWORD(SendDlgItemMessage (hwnd, ID_TYPE_ETAT_AL_DYN, BM_QUERYCHECKINDEX, 0L, 0L));
          if (iRetour != -1)
            {
            pGeoAl->Valeur.rValNumerique = (FLOAT)iRetour - 1;
            if (!NotificationReel (pGeoAl->wPosRet, pGeoAl->Valeur.rValNumerique))
              {
              maj_statut_al (0x8000);
              }
            }

          if (existe_repere (bx_anm_groupe_al))
            {
            pDataDlg = (DATA_DLG_AL_DYN*)pGetEnv (hwnd);

            // Groupes : maj BD animateur et notification vers pcsexe
            ValidationBoiteParametrageGrp (hwnd, AL_VISU_GRP);

            // sauve les priorites et al_visu_active de la selection precedente
            SauveBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
            pDataDlg->TabAlActive[pDataDlg->wIndexGroupe] = (IsDlgButtonChecked (hwnd, ID_ACTIVE_AL_DYN) == BST_CHECKED);

            // Priorit�s : maj BD animateur et notification vers pcsexe
            ValidationBoiteParametragePri (hwnd, AL_VISU_PRI, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

            wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al);
            for (wIndex = 0; wIndex < wNbEnr; wIndex++)
              {
              pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_VISU_ACTIVE + wIndex);
              pGeoAl->Valeur.bValLogique = pDataDlg->TabAlActive [wIndex];
              if (!NotificationBoolean (pGeoAl->wPosRet, pGeoAl->Valeur.bValLogique))
                {
                maj_statut_al (0x8000);
                }
              }
            }
          ::DestroyWindow (hwnd);
          break;

				case IDCANCEL:
				case ID_QUITTER_AL:
          ::DestroyWindow (hwnd);
          break;

				default:
          bTraite = FALSE;
          break;
        }
			}
      break;

		case WM_ACTIVATE:
			{
			OnActivateCommunBoiteModeless (hwnd, mp1);
			}
			break;

    default:
      bTraite = FALSE;
      break;
    }

  if (!bTraite)
    {
    mres = 0;
    }

  return mres;
  } // dlgprocVisuAlDyn

// -----------------------------------------------------------------------
// Boite de dialogue modeless de parametrage - de l'archivage des alarmes ?
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocArchAlDyn (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL           mres = 0;              // Valeur de retour
  BOOL              bTraite = TRUE;        // Indique commande trait�e
  BOOL              bRetour;
  tx_anm_geo_al *		pGeoAl;
  PENV_AL_DYN				pEnv;
  DATA_DLG_AL_DYN*	pDataDlg;
	INT								iIndexSel;

  switch (msg)
    {
    case WM_INITDIALOG:
      pDataDlg = (DATA_DLG_AL_DYN*)pCreeEnv (hwnd, sizeof (DATA_DLG_AL_DYN));
      if (pDataDlg)
        {
        // initialisation des titres
        InitBoiteParametrage (hwnd, c_inf_param_arch_al, c_inf_arch_glob_al, AL_ARCH_GLOB,
                              c_inf_groupes_al, c_inf_priorites_al);

        if (existe_repere (bx_anm_groupe_al))
          {
          // initialisation des textes des groupes et des valeurs initiales
          InitBoiteParametrageGroupe (hwnd, AL_ARCH_GRP);

          // initialisation contexte : num_grp = 0 et TabPriorite <-- BD animateur
          InitContexteBoiteParametrage (AL_ARCH_PRI, &pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

          // initialisation des cases � cocher priorit�s (ordre � respecter)
          InitBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

          // selectionne le premier groupe : A FAIRE APRES pSelectEnv
          SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN,CB_SETCURSEL, pDataDlg->wIndexGroupe, 0);
          }
        else
          { 
					// bx_anm_groupe_al n'existe pas
          ::EnableWindow (::GetDlgItem (hwnd, ID_LIST_GRP_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_LIST_PRI_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_NUM_PRI_AL_DYN), FALSE);
          }
        }

      mres = FALSE;
      break;

    case WM_DESTROY:
      pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
			if (pEnv)
				pEnv->hwndArchAlDyn = NULL;
			
			bLibereEnv (hwnd);
      break;

    case WM_CLOSE:
      ::DestroyWindow (hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_LIST_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              bTraite = TRUE;
              if (existe_repere (bx_anm_groupe_al))
                {
                pDataDlg = (DATA_DLG_AL_DYN*)pGetEnv (hwnd);

                // sauve les priorites de la selection precedente
                SauveBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

                // remet les priorites du nouveau groupe selectionne
                MajBoiteParametragePriorite (hwnd, &pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
                }
              break;
            }
          break;

        case ID_NUM_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case LBN_SELCHANGE:
              if (existe_repere (bx_anm_groupe_al))
                {
								iIndexSel = SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_GETCARETINDEX, 0, 0);
								MajListeParametragePriorite (hwnd, iIndexSel);
                }
              break;

            default:
              break;
            } // HIWORD(mp1)
          break; //case ID_NUM_PRI_AL_DYN

				case ID_VALIDER_AL:
          pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_ARCH_GLOB);
          bRetour = (IsDlgButtonChecked (hwnd, ID_GLOB_AL_DYN) == BST_CHECKED);
          pGeoAl->Valeur.bValLogique = bRetour;
          if (!NotificationBoolean (pGeoAl->wPosRet, bRetour))
            {
            maj_statut_al (0x8000);
            }

          if (existe_repere (bx_anm_groupe_al))
            {
            pDataDlg = (DATA_DLG_AL_DYN*)pGetEnv (hwnd);
            
						// Groupes : maj BD animateur et notification vers pcsexe
            ValidationBoiteParametrageGrp (hwnd, AL_ARCH_GRP);

            // sauve les priorites de la selection precedente
            SauveBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

            // Priorit�s : maj BD animateur et notification vers pcsexe
            ValidationBoiteParametragePri (hwnd, AL_ARCH_PRI, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
            }
          ::DestroyWindow (hwnd);
          break;

				case IDCANCEL:
				case ID_QUITTER_AL:
          ::DestroyWindow (hwnd);
          break;

				default:
          bTraite = FALSE;
          break;
        }
      break;

		case WM_ACTIVATE:
			OnActivateCommunBoiteModeless (hwnd, mp1);
			break;

    default:
      bTraite = FALSE;
      break;
    }

  if (!bTraite)
    {
    mres = 0;
    }

  return mres;
  }

// -----------------------------------------------------------------------
// Boite de dialogue modeless de parametrage des impressions d'alarme ?
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocImprAlDyn (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL           mres = 0;              // Valeur de retour
  BOOL              bTraite = TRUE;        // Indique commande trait�e
  BOOL              bRetour;
  tx_anm_geo_al *		pGeoAl;
  PENV_AL_DYN				pEnv;
  DATA_DLG_AL_DYN*	pDataDlg;
	INT								iIndexSel;

  switch (msg)
    {
    case WM_INITDIALOG:
      pDataDlg = (DATA_DLG_AL_DYN*)pCreeEnv (hwnd, sizeof (DATA_DLG_AL_DYN));
      if (pDataDlg)
        {
        // initialisation des titres
        InitBoiteParametrage (hwnd, c_inf_param_impr_al, c_inf_impr_glob_al, AL_IMPR_GLOB,
                              c_inf_groupes_al, c_inf_priorites_al);

        if (existe_repere (bx_anm_groupe_al))
          {
          // initialisation des textes des groupes et des valeurs initiales
          InitBoiteParametrageGroupe (hwnd, AL_IMPR_GRP);

          // initialisation contexte : num_grp = 0 et TabPriorite <-- BD animateur
          InitContexteBoiteParametrage (AL_IMPR_PRI, &pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

          // initialisation des cases � cocher priorit�s (ordre � respecter)
          InitBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

          // selectionne le premier groupe : A FAIRE APRES pSelectEnv
          SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN, CB_SETCURSEL, pDataDlg->wIndexGroupe, 0);
          }
        else
          { 
					// bx_anm_groupe_al n'existe pas
          ::EnableWindow (::GetDlgItem (hwnd, ID_LIST_GRP_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_LIST_PRI_AL_DYN), FALSE);
          ::EnableWindow (::GetDlgItem (hwnd, ID_NUM_PRI_AL_DYN), FALSE);
          }
        }

      mres = (LRESULT)FALSE;
      break;

    case WM_DESTROY:
      pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
			if (pEnv)
				pEnv->hwndImprAlDyn = NULL;
			
			bLibereEnv (hwnd);
      break;

    case WM_CLOSE:
      ::DestroyWindow (hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_LIST_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              bTraite = TRUE;
              if (existe_repere (bx_anm_groupe_al))
                {
                pDataDlg = (DATA_DLG_AL_DYN*)pGetEnv (hwnd);

                // sauve les priorites de la selection precedente
                SauveBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

                // remet les priorites du nouveau groupe selectionne
                MajBoiteParametragePriorite (hwnd, &pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
                }
              break;
            }
          break;

        case ID_NUM_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case LBN_SELCHANGE:
              if (existe_repere (bx_anm_groupe_al))
                {
								iIndexSel = SendDlgItemMessage (hwnd, ID_NUM_PRI_AL_DYN, LB_GETCARETINDEX, 0, 0);
								MajListeParametragePriorite (hwnd, iIndexSel);
                }
              break;

            default:
              break;
            } // HIWORD(mp1)
          break; //case ID_NUM_PRI_AL_DYN

				case ID_VALIDER_AL:
          pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_IMPR_GLOB);
          bRetour = (IsDlgButtonChecked (hwnd, ID_GLOB_AL_DYN) == BST_CHECKED);
          pGeoAl->Valeur.bValLogique = bRetour;
          if (!NotificationBoolean (pGeoAl->wPosRet, bRetour))
            {
            maj_statut_al (0x8000);
            }

          if (existe_repere (bx_anm_groupe_al))
            {
            pDataDlg = (DATA_DLG_AL_DYN*)pGetEnv (hwnd);

            // Groupes : maj BD animateur et notification vers pcsexe
            ValidationBoiteParametrageGrp (hwnd, AL_IMPR_GRP);

            // sauve les priorites de la selection precedente
            SauveBoiteParametragePriorite (hwnd, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);

            // Priorit�s : maj BD animateur et notification vers pcsexe
            ValidationBoiteParametragePri (hwnd, AL_IMPR_PRI, pDataDlg->wIndexGroupe, pDataDlg->TabPriorite);
            }
          ::DestroyWindow (hwnd);
          break;

				case IDCANCEL:
				case ID_QUITTER_AL:
          ::DestroyWindow (hwnd);
          break;

				default:
          bTraite = FALSE;
          break;
        }
      break;

		case WM_ACTIVATE:
			OnActivateCommunBoiteModeless (hwnd, mp1);
			break;

    default:
      bTraite = FALSE;
      break;
    }

  if (!bTraite)
    {
    mres = 0;
    }

  return mres;
  }

//------------------------------------------------------------------------
// init des textes boite compteur (titre, var globale ...)
//------------------------------------------------------------------------
static void InitBoiteCompteur (HWND hwnd,
                               DWORD wInfTitre,
                               DWORD wInfVarGlob,
                               DWORD wInfGroupes,
                               DWORD wInfCptrGrp,
                               DWORD wInfCptrPri)
  {
  char pszMessInf[c_nb_car_message_inf];

  bLngLireInformation (wInfTitre, pszMessInf);
  ::SetWindowText (hwnd, pszMessInf);

  bLngLireInformation (wInfVarGlob, pszMessInf);
  SetDlgItemText (hwnd, ID_GLOB_AL_DYN, pszMessInf);

  bLngLireInformation (wInfGroupes, pszMessInf);
  SetDlgItemText (hwnd, ID_GRP_AL_DYN, pszMessInf);

  bLngLireInformation (wInfCptrGrp, pszMessInf);
  SetDlgItemText (hwnd, ID_GRP_TOTAL_AL_DYN, pszMessInf);

  bLngLireInformation (wInfCptrPri, pszMessInf);
  SetDlgItemText (hwnd, ID_PRI_AL_DYN, pszMessInf);
  }

//------------------------------------------------------------------------
// init des groupes dans la liste_box
//------------------------------------------------------------------------
static void InitBoiteCompteurGroupe (HWND hwnd)
  {
  DWORD              wNbEnr;
  DWORD              wIndex;
  tx_anm_groupe_al *pGroupeAl;

  wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al);
  for (wIndex = 1; wIndex <= wNbEnr; wIndex++)
    {
    pGroupeAl = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, wIndex);
//$ac    SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN, LB_INSERTSTRING,
    SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN, CB_ADDSTRING,0, (LPARAM)(pGroupeAl->pszNomGroupe));
    }
  }

//-------------------------------------------------------------------------
// init contexte a partir BD animateur appel� au demarrage de l'animateur
//-------------------------------------------------------------------------
static void InitContexteBoiteCompteur (void)
  {
  DWORD wIndex;
  DWORD wTailleTab;

  wTailleTab = NB_GROUPE_MAX_AL * NB_PRIORITE_MAX_AL;
  for (wIndex = 0; wIndex < wTailleTab; wIndex++)
    {
    TabCptrPri [wIndex] = 0;
    }
  }

// -----------------------------------------------------------------------
// Boite de dialogue modeless de gestion des compteurs dynamiques
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocCptrAlDyn (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL		         mres = 0;              // Valeur de retour
  BOOL            bTraite = TRUE;        // Indique commande trait�e
  PENV_AL_DYN			pEnv;
  tx_anm_geo_al *	pGeoAl;
  FLOAT             rValeur;

  // ------------ Switch message re�u.... --------------------------------
  switch (msg)
    {
    // WM_INITDIALOG fait apres le WinLoadDlg $$

    case WM_DESTROY:
      pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
      pEnv->hwndCptrAlDyn = NULL;
      break;

    case WM_CLOSE:
      ::DestroyWindow (hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_LIST_PRI_AL_DYN:
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              bTraite = TRUE;
              if (existe_repere (bx_anm_groupe_al))
                {
                wIndexCptrGrp = (DWORD)  SendDlgItemMessage (hwnd, ID_LIST_PRI_AL_DYN, CB_GETCURSEL, 0, 0);
                ReafficheBoiteCptr ();
                }
              break;
            }
          break;

				case ID_RAZ_GLOB_AL_DYN:
          pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GLOB);
          pGeoAl->Valeur.rValNumerique = (FLOAT) 0;
          if (!NotificationReel (pGeoAl->wPosRet, (FLOAT) 0))
            {
            maj_statut_al (0x8000);
            }
          RazEtNotificationCptrGrpEtPri ();
          ReafficheBoiteCptr ();
          break;

				case ID_RAZ_GRP_AL_DYN:
          pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GRP + wIndexCptrGrp);
          rValeur = pGeoAl->Valeur.rValNumerique;
          pGeoAl->Valeur.rValNumerique = (FLOAT) 0;
          if (!NotificationReel (pGeoAl->wPosRet, (FLOAT) 0))
            {
            maj_statut_al (0x8000);
            }
          RazCptrPri (wIndexCptrGrp); // raz compteurs priorites du grp
          pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_CPTR_GLOB);
          pGeoAl->Valeur.rValNumerique = pGeoAl->Valeur.rValNumerique - rValeur;
          if (pGeoAl->Valeur.rValNumerique < 0)
            {
            pGeoAl->Valeur.rValNumerique = (FLOAT) 0;
            }
          if (!NotificationReel (pGeoAl->wPosRet, pGeoAl->Valeur.rValNumerique))
            {
            maj_statut_al (0x8000);
            }
          ReafficheBoiteCptr ();
          break;

				case IDCANCEL:
				case ID_QUITTER_AL:
          ::DestroyWindow (hwnd);
          break;

				default:
          bTraite = FALSE;
          break;
        }
      break;

		case WM_ACTIVATE:
			OnActivateCommunBoiteModeless (hwnd, mp1);
			break;

    default:
      bTraite = FALSE;
      break;
    }

  if (!bTraite)
    {
    mres = 0;
    }

  return mres;
  }

// -----------------------------------------------------------------------
// window proc des alarmes dynamiques
// -----------------------------------------------------------------------

// -----------------------------------------------------------------------
static BOOL OnCreateAlDyn (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_DYN	pEnv;

  pEnv = (PENV_AL_DYN)pCreeEnv (hwnd, sizeof (ENV_AL_DYN));
  if (pEnv)
    {
		// interdit l'item fermer du menu systeme
		MenuGriseItem (::GetSystemMenu (hwnd, FALSE), SC_CLOSE, TRUE);

		// ajouter l'icone alarme � la boite de dialogue
		// $$

		// cr�ation des barres d'outil :
		// Barre d'outil de traitement associ� aux alarmes
		  BOOL bBarreOutil1Invisible=((dwParamFenetreAlDyn & PARAM_FENETRE_AL_DYN_MASQUE_TOUTES_BARRE_OUTIL_INVISIBLE)!=0);
			if (bBarreOutil1Invisible)
				{
				pEnv->hboAlDyn=NULL;
				}
			else
				{
				pEnv->hboAlDyn = hBOCree (NULL, FALSE, FALSE, FALSE, CX_MARGE_BOUTONS, CY_MARGE_BOUTONS);

				if (pEnv->hboAlDyn)
					{
					bBOAjouteBouton (pEnv->hboAlDyn, "IDB_CMD_ALM_CLOCHE",  BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_SURV);
					bBOAjouteBouton (pEnv->hboAlDyn, "IDB_CMD_ALM_VISU",    BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_VISU);
					bBOAjouteBouton (pEnv->hboAlDyn, "IDB_CMD_SAUVER",			BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_ARCH);
					bBOAjouteBouton (pEnv->hboAlDyn, "IDB_CMD_IMPRIMER",		BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_IMPR);
					bBOAjouteBouton (pEnv->hboAlDyn, "IDB_CMD_ALM_COMPTEUR",BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_CPTR);
					bBOAjouteBouton (pEnv->hboAlDyn, "IDB_CMD_ALM_CONSULT", BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_CONS);
					bBOAjouteBouton (pEnv->hboAlDyn, "IDB_CMD_ALM_ACQUIT",  BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_ACQ);

					bBOOuvre (pEnv->hboAlDyn, FALSE, hwnd, 0, 0, BO_POS_HAUTE | BO_POS_CENTRE, TRUE);

					BOMontre (pEnv->hboAlDyn, !bBarreOutil1Invisible);
					}
				}

	// Cr�ation de la barre d'outil marche / pause
	BOOL bBarreOutil2Invisible=((dwParamFenetreAlDyn & PARAM_FENETRE_AL_DYN_MASQUE_BARRE_OUTIL_MARCHE_PAUSE_INVISIBLE)!=0);
	if (bBarreOutil2Invisible)	
		{
    pEnv->hboAlDyn2 = NULL;
		// envoi de la notification Scroll autoris�
		::PostMessage (hwnd, WM_COMMAND, (WPARAM) ID_BMP_MNU_SCROLL_ON, 0);
		}
	else
		{
    pEnv->hboAlDyn2 = hBOCree (NULL, FALSE, FALSE, FALSE, CX_MARGE_BOUTONS, CY_MARGE_BOUTONS);
    if (pEnv->hboAlDyn2)
      {
      bBOAjouteBouton (pEnv->hboAlDyn2, "IDB_CMD_ALM_MARCHE", BO_COL_SUIVANTE, BO_COCHE, ID_BMP_MNU_SCROLL_ON);
      bBOAjouteBouton (pEnv->hboAlDyn2, "IDB_CMD_ALM_PAUSE", BO_COL_SUIVANTE, BO_COCHE, ID_BMP_MNU_SCROLL_OFF);

      bBOOuvre (pEnv->hboAlDyn2, FALSE, hwnd, 0, 0, BO_POS_HAUTE | BO_POS_DROITE, TRUE);

			BOCliqueBouton (pEnv->hboAlDyn2, ID_BMP_MNU_SCROLL_ON);

      BOMontre (pEnv->hboAlDyn2, TRUE);
      }
		}
    pEnv->hwndSurvAlDyn   = NULL;
    pEnv->hwndVisuAlDyn   = NULL;
    pEnv->hwndArchAlDyn   = NULL;
    pEnv->hwndImprAlDyn   = NULL;
    pEnv->hwndCptrAlDyn   = NULL;
    pEnv->bFenAlArchiveOuverte = FALSE;

		//// Initialise le nom du fichier d'archivage
    StrCopyUpToLength (pEnv->szFicTri,pszNomFicArch,MAX_PATH);

		// Initailise les crit�res de tri
    StrCopy (pEnv->szDateDeb, "?");
    StrCopy (pEnv->szHeureDeb, "?");
    StrCopy (pEnv->szDateFin, "?");
    StrCopy (pEnv->szHeureFin, "?");
    StrCopy (pEnv->szGroupe, "?");
    StrCopy (pEnv->szPriorite, "?");
    StrCopy (pEnv->szEvenement, "?");
    StrSetNull (pEnv->szFicArch);

		// chargement des icones visualis�es dans la liste des alarmes
		hIconAlPresente = LoadIcon (Appli.hinstDllLng, MAKEINTRESOURCE(ID_ICON_AL_PRESENTE));

		hIconAlAcquitee = LoadIcon (Appli.hinstDllLng, MAKEINTRESOURCE(ID_ICON_AL_ACQUITE));

    pEnv->hwndListAlDyn = CreateWindowEx 
			(WS_EX_CLIENTEDGE, "LISTBOX", "", WS_CHILD | WS_VISIBLE | LBS_NOINTEGRALHEIGHT | LBS_HASSTRINGS | LBS_OWNERDRAWFIXED | LBS_NOTIFY | WS_HSCROLL | WS_VSCROLL, 
			0, 0, 0, 0, hwnd, (HMENU) ID_LISTBOX_AL_DYN, Appli.hinst, NULL);

		//Si pr�sentation am�lior�es, change la police de la listbox en arial
		if ((dwParamFenetreAlDyn & PARAM_FENETRE_AL_DYN_MASQUE_LOOK_AMELIORE)!=0)
		{
			// Init Police
			HDC hdc=GetDC(pEnv->hwndListAlDyn);
			HFONT hFont = fnt_create(hdc,1,PCS_FONT_STYLE_GRAS,14,Appli.byCharSetFont); // ARIAL 14 normal
			// Nouvelle fonte et redessin de l'exemple
			SendMessage (pEnv->hwndListAlDyn, WM_SETFONT, (WPARAM)hFont,0);
			pEnv->hFontList=hFont;
		}
		else
		{
			pEnv->hFontList=NULL;
		}

		*pmres = 0;
    }
	else
		*pmres = -1;

  // Valeurs retourn�es si le message a �t� trait�
  return TRUE;
  }

// -----------------------------------------------------------------------
// gestion des tempos pour les alarmes pavhd ...
// -----------------------------------------------------------------------
static BOOL OnTimerAlDyn (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  tx_anm_tempo_al *pTempoAl;
  DWORD             wNbEnr;
  DWORD             wIndex;
  DWORD             idTimer;
  DWORD             Centiemes;
  tx_anm_horodate  HoroDate;


  idTimer = LOWORD(mp1);
  if (idTimer == ID_TIMER_TEMPO)
    {
    wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_tempo_al);
    for (wIndex = 1; wIndex <= wNbEnr; wIndex++)
      {
      pTempoAl = (tx_anm_tempo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_tempo_al, wIndex);
      if (pTempoAl->bTempoEnCour)
        {
        if (bEcheanceMinuterie (pTempoAl->lwTempo))
          { 
					// tempo termin�e : l'alarme est apparue
          pTempoAl->bTempoEnCour = FALSE;
          TpsLireDateHeure (&(HoroDate.Jour), &(HoroDate.Mois), &(HoroDate.Annee),
                            &(HoroDate.JourDeSemaine), &(HoroDate.Heures), &(HoroDate.Minutes),
                            &(HoroDate.Secondes), &Centiemes);
          HoroDate.MilliSecondes = 10 * Centiemes;
          apparition_alarme (pTempoAl->wPosEsElement, &HoroDate);
          }
        }
      }
    }

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = (LRESULT) 0;
  return TRUE;
  }

// -----------------------------------------------------------------------
// WM_SIZE AlDyn
// -----------------------------------------------------------------------
static BOOL OnSizeAlDyn (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_DYN	pEnv = (PENV_AL_DYN)pGetEnv (hwnd);

	if (mp1 != SIZE_MINIMIZED)
		{
		if (pEnv->hboAlDyn)
			{
			BODeplace (pEnv->hboAlDyn, 0, 0, BO_POS_HAUTE | BO_POS_CENTRE);
			BOMontre (pEnv->hboAlDyn, TRUE); //$$ n�cessaire ?
			}
		if (pEnv->hboAlDyn2)
			{
			BODeplace (pEnv->hboAlDyn2, 0, 0, BO_POS_HAUTE | BO_POS_DROITE);
			BOMontre (pEnv->hboAlDyn2, TRUE);
			}
		// Redimensionne la list box des alarmes
		::MoveWindow (pEnv->hwndListAlDyn, 0, CY_BOUTONS_AL, LOWORD (mp2), HIWORD (mp2) - CY_BOUTONS_AL, TRUE);
		}
  
  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return TRUE;
  }

// -----------------------------------------------------------------------
// WM_MEASUREITEM AlDyn
// On utilise le hDC de la fen�tre AlDyn car hwndList n'est pas
// encore affect�. Apparemment, le for�age ne fonctionne que la 1� fois. $$???
// -----------------------------------------------------------------------
static BOOL OnMeasureItemAlDyn (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_DYN		pEnv = (PENV_AL_DYN)pGetEnv (hwnd);

  if (LOWORD(mp1) == ID_LISTBOX_AL_DYN)
    { 
	  HDC           hdc;
		TEXTMETRIC    fm;

		// Calcul de la hauteur et de la largeur des textes de la liste
    hdc = ::GetDC (hwnd); // hwndList n'est pas encore affect� $$ pas le bon objet
    GetTextMetrics (hdc, &fm);
    ::ReleaseDC (hwnd, hdc);
    pEnv->cxCarListDyn = NB_CAR_LIST_AL_DYN * fm.tmMaxCharWidth; // $$ taille du M Majuscule fm.lEmInc; =
    pEnv->cyCarListDyn = fm.tmHeight;

    // Valeurs retourn�es si le message a �t� trait�
    *pmres = MAKELPARAM (pEnv->cyCarListDyn, pEnv->cxCarListDyn);
    return TRUE;
    }
  else
    { // message non trait�
    return (FALSE);
    }
  }


// --------------------------------------------------------------------------
// Pour WM_DRAWITEM : dessine le texte en utilisant les tabulations courantes
// --------------------------------------------------------------------------
static void DessineTexteAvecTabs(PCSTR szTexteAl, LPDRAWITEMSTRUCT dis, LONG OffsetXSansTab)
	{
	// Taille du Texte
	LONG nTailleTexteAl=StrLength(szTexteAl);

	// r�cup�re la zone d'affichage du texte, en sautant l'ic�ne
	RECT rectClip = dis->rcItem;
	rectClip.left = OffsetXSansTab;
	// Force une zone de clip valide
	if (rectClip.left > rectClip.right)
		{
		rectClip.left = rectClip.right;
		}

	// Le texte contient un caract�re de tabulation ?
	DWORD dwNCharTAB = StrSearchChar(szTexteAl,CAR_SEPARATEUR_TABLEAU);

	if ((nNbAlarmTabsFormat > 0) && (dwNCharTAB!=STR_NOT_FOUND))
		{
		//affiche Les portions de Texte, TAB par TAB :
		DWORD dwNCharDebut = 0;
		LONG lNTab = 0;

		// Parcours du texte, TAB par TAB
		while((lNTab < nNbAlarmTabsFormat) && (dwNCharTAB!=STR_NOT_FOUND))
			{
			// Calcule la zone de clip
			rectClip.right = alPosXTab[lNTab];
			// Ecriture de la sous cha�ne d�limit�e par les TABs, clip�e
			//ExtTextOut(dis->hDC, rectClip.left, dis->rcItem.top,ETO_CLIPPED,&rectClip,szTexteAl+dwNCharDebut,dwNCharTAB,NULL);
			DrawText(dis->hDC, (LPSTR)(szTexteAl+dwNCharDebut),dwNCharTAB, &rectClip,DT_LEFT|DT_HIDEPREFIX|DT_SINGLELINE);

			// Une tabulation trait�e en plus
			rectClip.left=alPosXTab[lNTab];
			lNTab+=1;
			// Cherche la tabulation suivante
			dwNCharDebut+=dwNCharTAB+1;
			dwNCharTAB = StrSearchChar(szTexteAl+dwNCharDebut,CAR_SEPARATEUR_TABLEAU);
			}
		// Fin de chaine � afficher ?
		if (((LONG)dwNCharDebut)<nTailleTexteAl)
			{
			// Oui => affiche la jusqu'au bout
			TextOut (dis->hDC, rectClip.left, dis->rcItem.top, szTexteAl+dwNCharDebut, nTailleTexteAl - dwNCharDebut);
			// DrawText (dis->hDC, (LPSTR)(szTexteAl+dwNCharDebut), nTailleTexteAl - dwNCharDebut, &rectClip, DT_LEFT|DT_HIDEPREFIX|DT_SINGLELINE);
			}
		}
	else
		{
		// Non => affiche le texte d'un seul coup, en sautant l'offset �ventuel Ic�ne
		TextOut (dis->hDC, rectClip.left, dis->rcItem.top, szTexteAl, nTailleTexteAl);
		//	DrawText (dis->hDC, (LPSTR)szTexteAl, nTailleTexteAl , &rectClip, DT_LEFT|DT_HIDEPREFIX|DT_SINGLELINE|DT_NOCLIP);
		}
	}

static void DessineFondAlarme(COLORREF colorFond, RECT rcItem, HDC hDC, BOOL bDegrade)
{
	if (!bDegrade)
	{
		// Remplissage fond uniforme
		HBRUSH hBrushFond = CreateSolidBrush (colorFond);
		FillRect (hDC, &rcItem ,hBrushFond);
		DeleteObject (hBrushFond);
	}
	else
	{
		// Remplissage fond avec d�grad�
		// M�thode avec des lignes d�grad�e
		COLORREF colorLigne = colorFond;
		// COLORREF colorFond2 = RGB(R+(R2-R)*nLigne/hDessin,G+(G2-G)*nLigne/hDessin,B+(B2-B)*nLigne/hDessin);
		int R =GetRValue(colorFond);
		int G =GetGValue(colorFond);
		int B =GetBValue(colorFond);
					
		int R2;
		int G2;
		int B2;
		// Couleur claire ?
		if ((R>220) & (G>220) & (B>220))
		{
			// Oui => d�grad� entre couleur claire et gris
			R2 =R;
			G2 =R;
			B2 =R;
			R =200;
			G =200;
			B =200;
		}
		else
		{ // Non => d�gard� entre la couleur en plus clair et la couleur elle m�me 
			R2 =(R+512)/3;
			G2 =(G+512)/3;
			B2 =(B+512)/3;
		}

		RECT rcDessin=rcItem;
		int hDessin=rcDessin.bottom-rcDessin.top;
		HPEN hOldPenLigne=(HPEN)GetCurrentObject(hDC, OBJ_PEN); 
		HPEN hPenLigne=NULL; 
		for (int nLigne=0;nLigne < hDessin; nLigne++)
		{
			MoveToEx (hDC, rcDessin.left,rcDessin.top+nLigne,NULL);
			colorLigne=RGB(R2+(R-R2)*nLigne/hDessin,G2+(G-G2)*nLigne/hDessin,B2+(B-B2)*nLigne/hDessin);
			hPenLigne=CreatePen (PS_SOLID, 1, colorLigne);
			SelectObject (hDC, hPenLigne);
			LineTo(hDC, rcDessin.right,rcDessin.top+nLigne);
			// $$ lib�rer l'objet
			DeleteObject(hPenLigne);
		}
		SelectObject (hDC, hOldPenLigne);
	}
	/*M�thode avec GDI plus qui suppose plein de trucs en plus...
		void VerticalGradient(HDC hDC, const RECT& GradientFill, COLORREF rgbTop, COLORREF rgbBottom)
 {
    GRADIENT_RECT gradientRect = { 0, 1 };
		COLORREF colorFond = TabCouleur [pBufferVisu->wIndexClrBack];
		COLORREF colorFond2 = TabCouleur [CLR_WHITE];
    TRIVERTEX triVertext[ 2 ] = {
        dis->rcItem.left - 1,
        dis->rcItem.top - 1,
        GetRValue(colorFond2) << 8,
        GetGValue(colorFond2) << 8,
        GetBValue(colorFond2) << 8,
        0x0000, 		
        dis->rcItem.right,
        dis->rcItem.bottom,
        GetRValue(colorFond) << 8,
        GetGValue(colorFond) << 8,
        GetBValue(colorFond) << 8,
        0x0000
    };
		::GradientFill(dis->hDC, triVertext, 2, &gradientRect, 1, GRADIENT_FILL_RECT_V);
}*/
}

// --------------------------------------------------------------------------
// Traitement WM_DRAWITEM : dessin dans la listbox d'une ligne d'alarme AlDyn
// --------------------------------------------------------------------------
static BOOL OnDrawItemAlDyn (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
	//$$ g�rer le bon �tat, le focus etc...
  if (LOWORD(mp1) == ID_LISTBOX_AL_DYN)
    {
    if (nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al) > 0)
      {
			LPDRAWITEMSTRUCT	dis = (LPDRAWITEMSTRUCT) (mp2);
			char              szTexteAl[TAILLE_MAX_MES_AL_COMPLET+1];
			t_buffer_anm_visu_al *pBufferVisu;
			int nTailleTexteAl = ::SendMessage (dis->hwndItem, LB_GETTEXT, (WPARAM) (dis->itemID), (LPARAM)szTexteAl);

			// Liste vide ? $$ eventuellement am�liorer
			if (dis->itemID != -1)
				{
				// Non => r�cupre les infos et dessine les
				if (nTailleTexteAl == LB_ERR)
					{
					StrCopy (szTexteAl, "?");
					nTailleTexteAl = 1;
					}

				pBufferVisu = (t_buffer_anm_visu_al*)pointe_enr (szVERIFSource, __LINE__, buffer_anm_visu_al, dis->itemID + 1);
				DessineFondAlarme( TabCouleur [pBufferVisu->wIndexClrBack], dis->rcItem,dis->hDC, dwParamFenetreAlDyn & PARAM_FENETRE_AL_DYN_MASQUE_LOOK_AMELIORE);
				
				// Dessine les �ventuelles ic�nes d'�tat
				if (pBufferVisu->bAlPresente)
					{
					DrawIcon (dis->hDC, dis->rcItem.left, dis->rcItem.top, hIconAlPresente);
					}
				if (pBufferVisu->bAlAcquitee)
					{
					DrawIcon (dis->hDC, dis->rcItem.left, dis->rcItem.top, hIconAlAcquitee);
					}

				// Couleur du texte de l'alarme et de son fond
				SetTextColor (dis->hDC, TabCouleur [pBufferVisu->wIndexClrFore]); //$$ restaurer
				//SetBkColor (dis->hDC, TabCouleur [pBufferVisu->wIndexClrBack]); //$$ restaurer
				SetBkMode(dis->hDC, TRANSPARENT); 

				// Dessine le texte avec tabs (offset de 20 pour l'ic�ne si pas de Tab)
				DessineTexteAvecTabs (szTexteAl, dis, 20);
				} // if (dis->itemID != -1)
			} // if (nb_enregistrements (szVERIFSource, __LINE__, buffer_anm_visu_al) > 0)

    //$$ ?? dis->fsState = dis->fsStateOld = FALSE;
    }

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = (LRESULT) TRUE;
  return TRUE;
  }

// -----------------------------------------------------------------------
// 
// -----------------------------------------------------------------------
static BOOL OnCommandAlDyn (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  DWORD         ItemSelect;
  PENV_AL_DYN		pEnv;
  char          pszTitre[TAILLE_MAX_MES_AL_COMPLET+1];
  char          pszTexte[TAILLE_MAX_MES_AL_COMPLET+1];

  switch (LOWORD (mp1))
    {
		case ID_LISTBOX_AL_DYN :
			{
			if (HIWORD(mp1) == LBN_DBLCLK)
				{
				pEnv = (PENV_AL_DYN)pGetEnv (hwnd);
				// ItemSelect = (DWORD) LOWORD (::SendMessage (pEnv->hwndListAlDyn,LM_QUERYSELECTION,LIT_FIRST, 0);
				// if (ItemSelect != LIT_NONE)
				ItemSelect = (DWORD)  ::SendMessage (pEnv->hwndListAlDyn, LB_GETCURSEL, 0, 0);
				if (::SendMessage (pEnv->hwndListAlDyn, LB_GETSEL, (WPARAM)ItemSelect, 0) != 0)
					{
					acquittement_alarme (ItemSelect + 1);
					}
				}
			}
			break;

    case ID_BMP_MNU_SURV:
      pEnv = (PENV_AL_DYN)pGetEnv (hwnd);
      if (pEnv->hwndSurvAlDyn)
        {
        ::ShowWindow (pEnv->hwndSurvAlDyn, SW_SHOW);
        }
      else
        {
				pEnv->hwndSurvAlDyn = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_AL_DYN), hwndAlDyn, dlgprocSurvAlDyn, 0);
        ::ShowWindow (pEnv->hwndSurvAlDyn, SW_SHOW);
        }
      break;

    case ID_BMP_MNU_VISU:
      pEnv = (PENV_AL_DYN)pGetEnv (hwnd);
      if (pEnv->hwndVisuAlDyn)
        {
        ::SetWindowPos (pEnv->hwndVisuAlDyn, HWND_TOP, 0, 0, 0, 0,
          SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
        }
      else
        {
        pEnv->hwndVisuAlDyn = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_AL_VISU_AL_DYN), hwndAlDyn, dlgprocVisuAlDyn, 0);
        ::ShowWindow (pEnv->hwndVisuAlDyn, SW_SHOW);
        }
      break;

    case ID_BMP_MNU_ARCH:
      pEnv = (PENV_AL_DYN)pGetEnv (hwnd);
      if (pEnv->hwndArchAlDyn)
        {
        ::SetWindowPos (pEnv->hwndArchAlDyn, HWND_TOP, 0, 0, 0, 0,
          SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
        }
      else
        {
        pEnv->hwndArchAlDyn = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_AL_DYN), hwndAlDyn, dlgprocArchAlDyn, 0);
        ::ShowWindow (pEnv->hwndArchAlDyn, SW_SHOW);
        }
      break;

    case ID_BMP_MNU_IMPR:
      pEnv = (PENV_AL_DYN)pGetEnv (hwnd);
      if (pEnv->hwndImprAlDyn)
        {
        ::SetWindowPos (pEnv->hwndImprAlDyn, HWND_TOP, 0, 0, 0, 0,
           SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
        }
      else
        {
        pEnv->hwndImprAlDyn = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_AL_DYN), hwndAlDyn, dlgprocImprAlDyn, 0);
        ::ShowWindow (pEnv->hwndImprAlDyn, SW_SHOW);
        }
      break;

    case ID_BMP_MNU_CPTR:
      pEnv = (PENV_AL_DYN)pGetEnv (hwnd);
      if (pEnv->hwndCptrAlDyn)
        {
        ::SetWindowPos (pEnv->hwndCptrAlDyn, HWND_TOP, 0, 0, 0, 0,
          SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
        }
      else
        {
				pEnv->hwndCptrAlDyn = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_AL_CPTR), hwndAlDyn, dlgprocCptrAlDyn, 0);

        // initialisation des titres :
        InitBoiteCompteur (pEnv->hwndCptrAlDyn, c_inf_param_cptr_al, c_inf_cptr_glob_al,
                           c_inf_groupes_al, c_inf_cptr_groupe_al, c_inf_cptr_priorite_al);


        if (existe_repere (bx_anm_groupe_al))
					{
          // initialisation des textes des groupes ds la liste_box
          InitBoiteCompteurGroupe (pEnv->hwndCptrAlDyn);

          wIndexCptrGrp = 0;

          // selectionne le premier groupe
          SendDlgItemMessage (pEnv->hwndCptrAlDyn, ID_LIST_PRI_AL_DYN, CB_SETCURSEL, wIndexCptrGrp, 0);
          }
        else
          { 
					// bx_anm_groupe_al n'existe pas
          ::EnableWindow (::GetDlgItem (pEnv->hwndCptrAlDyn, ID_GRP_AL_DYN), FALSE);
          }

        ReafficheBoiteCptr ();
        ::ShowWindow (pEnv->hwndCptrAlDyn, SW_SHOW);
        }
      break;

    case ID_BMP_MNU_CONS:
      pEnv = (PENV_AL_DYN)pGetEnv (hwnd);
      if (pEnv->bFenAlArchiveOuverte)
        {
        ::SetWindowPos (hwndAlArchive, HWND_TOP, 0, 0, 0, 0,
          SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
        }
      else
        {
        if (bCreeFenetreAlArchive())
          {
          pEnv->bFenAlArchiveOuverte = TRUE;
          }
        }
      break;

    case ID_BMP_MNU_ACQ:
      bLngLireInformation (c_inf_titre_al_dyn, pszTitre);
      bLngLireInformation (c_inf_acq_al_dyn, pszTexte);
      if (::MessageBox (hwndAlDyn, pszTexte, pszTitre, 
				MB_ICONEXCLAMATION | MB_OKCANCEL | MB_DEFBUTTON2 | MB_APPLMODAL) == IDOK)
        {
        acquittement_global ();
        }
      break;

    case ID_BMP_MNU_SCROLL_ON:
      bascule_scroll (TRUE);
      break;

    case ID_BMP_MNU_SCROLL_OFF:
      bascule_scroll (FALSE);
      break;

    default :
      break;
    }

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return TRUE;
  }

// -----------------------------------------------------------------------
// les fen filles st d�truites automatiquement
// -----------------------------------------------------------------------
static BOOL OnDestroyAlDyn (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_DYN	pEnv = (PENV_AL_DYN)pGetEnv (hwnd);

  if (pEnv->hwndSurvAlDyn)
    {
    ::DestroyWindow (pEnv->hwndSurvAlDyn);
    }
  if (pEnv->hwndVisuAlDyn)
    {
    ::DestroyWindow (pEnv->hwndVisuAlDyn);
    }
  if (pEnv->hwndArchAlDyn)
    {
    ::DestroyWindow (pEnv->hwndArchAlDyn);
    }
  if (pEnv->hwndImprAlDyn)
    {
    ::DestroyWindow (pEnv->hwndImprAlDyn);
    }
  if (pEnv->hwndCptrAlDyn)
    {
    ::DestroyWindow (pEnv->hwndCptrAlDyn);
    }
  if (pEnv->bFenAlArchiveOuverte)
    {
    ::DestroyWindow (hwndAlArchive);
    }
	if (pEnv->hboAlDyn)
		{
		BODetruit (&pEnv->hboAlDyn);
		}
	if (pEnv->hboAlDyn2)
		{
		BODetruit (&pEnv->hboAlDyn2);
		}
	DestroyIcon (hIconAlPresente);
	DestroyIcon (hIconAlAcquitee);

	if (pEnv->hFontList)
		{
		fnt_destroy (pEnv->hFontList);
		}
  bLibereEnv (hwnd);

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = (LRESULT) 0;
  return TRUE;
  }

// -----------------------------------------------------------------------
// Traitement sp�cial sur mise en icone / restauration de la fen�tre AlDyn
// -----------------------------------------------------------------------
static BOOL OnSysCommandAlDyn (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
	{
	switch (mp1 & 0xFFF0)
		{
		PENV_AL_DYN			pEnv;
		tx_anm_geo_al *	pGeoAl;

		case SC_MINIMIZE:
			pEnv = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
			if (pEnv->hwndSurvAlDyn)
				{
				::DestroyWindow (pEnv->hwndSurvAlDyn);
				}
			if (pEnv->hwndVisuAlDyn)
				{
				::DestroyWindow (pEnv->hwndVisuAlDyn);
				}
			if (pEnv->hwndArchAlDyn)
				{
				::DestroyWindow (pEnv->hwndArchAlDyn);
				}
			if (pEnv->hwndImprAlDyn)
				{
				::DestroyWindow (pEnv->hwndImprAlDyn);
				}
			if (pEnv->hwndCptrAlDyn)
				{
				::DestroyWindow (pEnv->hwndCptrAlDyn);
				}
			if (pEnv->bFenAlArchiveOuverte)
				{
				::DestroyWindow (hwndAlArchive);
				}
			//
			pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_VISIBLE);
			pGeoAl->Valeur.bValLogique = FALSE;
			if (!NotificationBoolean (pGeoAl->wPosRet, pGeoAl->Valeur.bValLogique))
				maj_statut_al (0x8000);
			break;

		case SC_RESTORE:
			{
			pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, AL_VISIBLE);
			pGeoAl->Valeur.bValLogique = TRUE;
			if (!NotificationBoolean (pGeoAl->wPosRet, pGeoAl->Valeur.bValLogique))
				maj_statut_al (0x8000);
			}
			break;
		}
	return FALSE;
	}

// --------------------------------------------------------------------------
// Window-Procedure Alarmes Dynamiques
// --------------------------------------------------------------------------
static LRESULT CALLBACK wndprocAlDyn (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;              // Valeur de retour
  BOOL     bTraite = TRUE;        // Indique commande trait�e

  switch (msg)
    {
    case WM_CREATE:
			bTraite = OnCreateAlDyn (hwnd, mp1, mp2, &mres);
			break;

    case WM_DESTROY:
			bTraite = OnDestroyAlDyn (hwnd, mp1, mp2, &mres);
			break;

    case WM_KEYDOWN:
			bTraite = bOnKeyDown (mp1);
			break;

    case WM_TIMER:
			bTraite = OnTimerAlDyn (hwnd, mp1, mp2, &mres);
			break;

    case WM_SIZE:
			bTraite = OnSizeAlDyn (hwnd, mp1, mp2, &mres);
			break;

    case WM_MEASUREITEM:
			bTraite = OnMeasureItemAlDyn (hwnd, mp1, mp2, &mres);
			break;

    case WM_DRAWITEM:
			bTraite = OnDrawItemAlDyn (hwnd, mp1, mp2, &mres);
			break;

    case WM_SYSCOMMAND:
			bTraite = OnSysCommandAlDyn (hwnd, mp1, mp2, &mres);
      break;

    case WM_COMMAND:
			bTraite = OnCommandAlDyn (hwnd, mp1, mp2, &mres);
			break;

    default:
			bTraite = FALSE;
			break;
    }

  if (!bTraite)
    {
    mres = DefWindowProc (hwnd, msg, mp1, mp2);
    }

  return mres;
  }

// -----------------------------------------------------------------------
//                GESTION WINDOW PROC FENETRE CONSULTATION
// -----------------------------------------------------------------------
// --------------------------------------------------------------------------
//
static void delete_liste_al_cons (void)
  {
  PENV_AL_CONS	pEnv = (PENV_AL_CONS)pGetEnv (hwndAlArchive);

  ::SendMessage (pEnv->hwndListAlCons, LB_RESETCONTENT, 0, 0);
  }

// -----------------------------------------------------------------------
// Boite de dialogue modeless de parametrage du tri
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocTriAlArchive (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL           mres=0;     // Valeur de retour
  BOOL              bTraite=TRUE;  // Indique commande trait�e
  PENV_AL_CONS			pEnv;
  PENV_AL_DYN				pEnvAlDyn;
  char              pszMessInf[c_nb_car_message_inf];
  DWORD             wIndex;
  DWORD             wNbEnr;
  LONG               date_deb_num;
  LONG               heure_deb_num;
  LONG               date_fin_num;
  LONG               heure_fin_num;
  BYTE              priorite;
  BYTE              evenement;

  switch (msg)
    {
    case WM_INITDIALOG:
      //Initialise les libell�s de cette boite de dialogue
			bLngLireInformation (c_inf_criteres_tri_al, pszMessInf);
      ::SetWindowText (hwnd, pszMessInf);
			
			bLngLireInformation (c_inf_nom_fic_al, pszMessInf);
      SetDlgItemText (hwnd, ID_1_AL_CONS, pszMessInf);
      bLngLireInformation (c_inf_date_deb_al, pszMessInf);
      SetDlgItemText (hwnd, ID_2_AL_CONS, pszMessInf);
      bLngLireInformation (c_inf_heure_deb_al, pszMessInf);
      SetDlgItemText (hwnd, ID_3_AL_CONS, pszMessInf);
      bLngLireInformation (c_inf_date_fin_al, pszMessInf);
      SetDlgItemText (hwnd, ID_4_AL_CONS, pszMessInf);
      bLngLireInformation (c_inf_heure_fin_al, pszMessInf);
      SetDlgItemText (hwnd, ID_5_AL_CONS, pszMessInf);
      bLngLireInformation (c_inf_groupe_al, pszMessInf);
      SetDlgItemText (hwnd, ID_6_AL_CONS, pszMessInf);
      bLngLireInformation (c_inf_priorite_al, pszMessInf);
      SetDlgItemText (hwnd, ID_7_AL_CONS, pszMessInf);
      bLngLireInformation (c_inf_evenement_al, pszMessInf);
      SetDlgItemText (hwnd, ID_8_AL_CONS, pszMessInf);
      break;

    case WM_DESTROY:
      pEnv = (PENV_AL_CONS)pGetEnv (hwndAlArchive);
      pEnv->bDlgTriOuverte = FALSE;
      break;

    case WM_CLOSE:
      ::DestroyWindow (hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_VALIDER_AL: // consultation d'archives d'alarmes
					{
					BOOL bValideCriteresTriAlarmes = TRUE;

          pEnvAlDyn = (PENV_AL_DYN)pGetEnv (hwndAlDyn);

          if (bFicConsOuvert)
            { // refermer le fichier pr�c�dent
            FermeFicCons (&hficFichierCons6, &bFicConsOuvert);
            }

          GetDlgItemText (hwnd, ID_9_AL_CONS, pEnvAlDyn->szFicTri,  MAX_PATH);
          GetDlgItemText (hwnd, ID_10_AL_CONS, pEnvAlDyn->szDateDeb, 11);
          GetDlgItemText (hwnd, ID_11_AL_CONS, pEnvAlDyn->szHeureDeb, 10);
          GetDlgItemText (hwnd, ID_12_AL_CONS, pEnvAlDyn->szDateFin, 11);
          GetDlgItemText (hwnd, ID_13_AL_CONS, pEnvAlDyn->szHeureFin, 10);
          GetDlgItemText (hwnd, ID_14_AL_CONS, pEnvAlDyn->szGroupe, 12);
          GetDlgItemText (hwnd, ID_15_AL_CONS, pEnvAlDyn->szPriorite,  4);
          GetDlgItemText (hwnd, ID_16_AL_CONS, pEnvAlDyn->szEvenement,  4);

					  // valide date debut
						if (!bValideCritereDateAlarme (pEnvAlDyn->szDateDeb, &date_deb_num))
							{
							bValideCriteresTriAlarmes = FALSE;
							StrSetNull (pEnvAlDyn->szDateDeb);
							}

						// valide heure debut
						if (!bValideCritereHeureAlarme (pEnvAlDyn->szHeureDeb, TRUE, &heure_deb_num))
							{
							bValideCriteresTriAlarmes = FALSE;
							StrSetNull (pEnvAlDyn->szHeureDeb);
							}

						// valide date fin
						if (!bValideCritereDateAlarme (pEnvAlDyn->szDateFin, &date_fin_num))
							{
							bValideCriteresTriAlarmes = FALSE;
							StrSetNull (pEnvAlDyn->szDateFin);
							}

						// valide heure fin
						if (!bValideCritereHeureAlarme (pEnvAlDyn->szHeureFin, FALSE, &heure_fin_num))
							{
							bValideCriteresTriAlarmes = FALSE;
							StrSetNull (pEnvAlDyn->szHeureFin);
							}

						// valide priorit�
						if (!bValideCriterePrioriteAlarme (pEnvAlDyn->szPriorite, &priorite))
							{
							bValideCriteresTriAlarmes = FALSE;
							StrSetNull (pEnvAlDyn->szPriorite);
							}

						// valide evenements
						if (!bValideCritereEvenementsAlarme (pEnvAlDyn->szEvenement, &evenement))
							{
							bValideCriteresTriAlarmes = FALSE;
							StrSetNull (pEnvAlDyn->szEvenement);
							}

					// tous les crit�res sont bons ?
          if (bValideCriteresTriAlarmes)
            {
						// oui => lance la consultation :
						// reset du bloc
            enleve_bloc (bx_anm_consultation_al);
            delete_liste_al_cons ();

            if (bAlConsulteArchive (date_deb_num, date_fin_num, heure_deb_num, heure_fin_num,
							pEnvAlDyn->szFicTri, pEnvAlDyn->szGroupe, priorite, evenement))
              {
              if (existe_repere (bx_anm_consultation_al))
                {
                wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_consultation_al);
                for (wIndex = 1; wIndex <= wNbEnr; wIndex++)
                  {
									PENV_AL_CONS	pEnv = (PENV_AL_CONS)pGetEnv (hwndAlArchive);

									::SendMessage (pEnv->hwndListAlCons, LB_INSERTSTRING, (WPARAM)-1, (LPARAM)((PSTR)" "));
                  }
                }
              } // if (bAlConsulteArchive
            } // bValideCriteresTriAlarmes
          else
            {
						// non => r�affiche les parametres �ventuellement remis � NUL
            SetDlgItemText (hwnd, ID_10_AL_CONS, pEnvAlDyn->szDateDeb);
            SetDlgItemText (hwnd, ID_11_AL_CONS, pEnvAlDyn->szHeureDeb);
            SetDlgItemText (hwnd, ID_12_AL_CONS, pEnvAlDyn->szDateFin);
            SetDlgItemText (hwnd, ID_13_AL_CONS, pEnvAlDyn->szHeureFin);
            SetDlgItemText (hwnd, ID_15_AL_CONS, pEnvAlDyn->szPriorite);
            SetDlgItemText (hwnd, ID_16_AL_CONS, pEnvAlDyn->szEvenement);
            }
					}
          break;

				case IDCANCEL:
				case ID_QUITTER_AL:
          ::DestroyWindow (hwnd);
          break;
        }
      break;

		case WM_ACTIVATE:
			OnActivateCommunBoiteModeless (hwnd, mp1);
			break;

    default:
      bTraite = FALSE;
      break;
    }

  if (!bTraite)
    {
    mres = 0;
    }

  return mres;
  } //dlgprocTriAlArchive

// -----------------------------------------------------------------------
// Boite de dialogue modeless d'archivage du resultat de consultation
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocEnregistreConsultationDArchive (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL	bRes = FALSE;     // Valeur de retour : message trait�
  BOOL  bTraite = TRUE;  // Indique commande trait�e
  
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			PENV_AL_DYN	pEnvAlDyn = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
		  char	szMessInf[c_nb_car_message_inf];

			// Initialise le nom de fichier
      SetDlgItemText (hwnd, ID_2_AL_CONS, pEnvAlDyn->szFicArch);

			// mise � jour des libell�s
      bLngLireInformation (c_inf_arch_cons_al, szMessInf);
      ::SetWindowText (hwnd, szMessInf);

      bLngLireInformation (c_inf_nom_fic_al, szMessInf);
      SetDlgItemText (hwnd, ID_1_AL_CONS, szMessInf);

      bRes = TRUE;
			}
      break;

    case WM_DESTROY:
			{
			PENV_AL_CONS	pEnv = (PENV_AL_CONS)pGetEnv (hwndAlArchive);

      pEnv->bDlgEnregistreConsultationDArchiveOuverte = FALSE;
      pEnv->hwndEnregistreConsultationDArchive = NULL;
			}
      break;

    case WM_CLOSE:
      ::DestroyWindow (hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_VALIDER_AL:
					{
          PENV_AL_DYN pEnvAlDyn = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
          GetDlgItemText (hwnd, ID_2_AL_CONS, pEnvAlDyn->szFicArch, MAX_PATH);
          ArchivageRtatConsult (&hficFichierCons6, &bFicConsOuvert, pEnvAlDyn->szFicTri, pEnvAlDyn->szFicArch);
					}
          break;

				case IDCANCEL:
				//case ID_QUITTER_AL:
          ::DestroyWindow (hwnd);
          break;

				default:
					bRes = TRUE;
					break;
        }
      break;

		case WM_ACTIVATE:
			OnActivateCommunBoiteModeless (hwnd, mp1);
			break;

		default:
      bRes = FALSE; // message non trait�
      break;
    }

  return bRes;
  } // dlgprocEnregistreConsultationDArchive

// -----------------------------------------------------------------------
// Cr�ation de la fenetre de consultation des archives
// -----------------------------------------------------------------------
static BOOL OnCreateAlArchive (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_CONS	pEnv = (PENV_AL_CONS)pCreeEnv (hwnd, sizeof (ENV_AL_CONS));
  if (pEnv)
    {
    pEnv->hboAlCons = hBOCree (NULL, TRUE, FALSE, FALSE, CX_MARGE_BOUTONS, CY_MARGE_BOUTONS);

    if (pEnv->hboAlCons)
      {
      bBOAjouteBouton (pEnv->hboAlCons, "IDB_CMD_ALM_CONSULT",  BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_CONS_VALI);
      bBOAjouteBouton (pEnv->hboAlCons, "IDB_CMD_SAUVER",   BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_CONS_ARCH);
      bBOAjouteBouton (pEnv->hboAlCons, "IDB_CMD_IMPRIMER", BO_COL_SUIVANTE, BO_BOUTON, ID_BMP_MNU_CONS_IMPR);
      bBOOuvre (pEnv->hboAlCons, FALSE, hwnd, 0, 0, BO_POS_HAUTE | BO_POS_CENTRE, TRUE);
      BOMontre (pEnv->hboAlCons, TRUE);
      }
    pEnv->bDlgTriOuverte       = FALSE;
    pEnv->bDlgEnregistreConsultationDArchiveOuverte = FALSE;
    pEnv->hwndEnregistreConsultationDArchive = NULL;
    pEnv->hwndListAlCons = CreateWindow ("LISTBOX", "",
			WS_CHILD | WS_VISIBLE | LBS_OWNERDRAWFIXED | LBS_HASSTRINGS| WS_HSCROLL| WS_VSCROLL ,
			0, 0, 0, 0, hwnd, (HMENU) ID_LISTBOX_AL_CONS, Appli.hinst, NULL);
    }
			//Si pr�sentation am�lior�es, change la police de la listbox en arial
		if ((dwParamFenetreAlDyn & PARAM_FENETRE_AL_DYN_MASQUE_LOOK_AMELIORE)!=0)
		{
			// Init Police
			HDC hdc=GetDC(pEnv->hwndListAlCons);
			HFONT hFont = fnt_create(hdc,1,PCS_FONT_STYLE_GRAS,14,Appli.byCharSetFont); // ARIAL 14 normal
			// Nouvelle fonte et redessin de l'exemple
			SendMessage (pEnv->hwndListAlCons, WM_SETFONT, (WPARAM)hFont,0);
		}
  *pmres = (LRESULT) FALSE;
  return TRUE;
  }

// -----------------------------------------------------------------------
static BOOL OnSizeAlArchive (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_CONS	pEnv = (PENV_AL_CONS)pGetEnv (hwnd);

	if (mp1 != SIZE_MINIMIZED)
		{
		BODeplace (pEnv->hboAlCons, 0, 0, BO_POS_HAUTE | BO_POS_CENTRE);
		BOMontre (pEnv->hboAlCons, TRUE);

  		// Redimensionne la list box des alarmes
		::MoveWindow (pEnv->hwndListAlCons, 0, CY_BOUTONS_AL, LOWORD (mp2), HIWORD (mp2) - CY_BOUTONS_AL, TRUE);
		}

  *pmres = (LRESULT) 0;
  return TRUE;
  }

// -----------------------------------------------------------------------
// Remarque : $$ on utilise hwnd Client de la fen�tre dynamique car hwndList n'est pas
// encore affect�. Apparemment, le for�age ne fonctionne que la 1� fois.
// -----------------------------------------------------------------------
static BOOL OnMeasureItemAlArchive (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_CONS	pEnv = (PENV_AL_CONS)pGetEnv (hwnd);

  if (LOWORD(mp1) == ID_LISTBOX_AL_CONS)
    { 
		TEXTMETRIC  fm;
		HDC         hdc = ::GetDC (hwnd); // hwndList n'est pas encore affect�

		// Calcul de la hauteur et de la largeur des textes de la liste
    GetTextMetrics (hdc, &fm);
    ::ReleaseDC (hwnd, hdc);
    pEnv->CxCarListCons = NB_CAR_LIST_AL_ARCHIVE * fm.tmMaxCharWidth; // $$ taille du M Majuscule fm.lEmInc;
    pEnv->CyCarListCons = fm.tmHeight;

    // $$Valeurs retourn�es si le message a �t� trait�
    *pmres = MAKELONG (pEnv->CyCarListCons, pEnv->CxCarListCons);
    return TRUE;
    }
  else
    { // message non trait�
    return FALSE;
    }
  }

// -----------------------------------------------------------------------
// Re�u quand on ins�re ou change un texte
// -----------------------------------------------------------------------
static BOOL OnDrawItemAlArchive (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
	// $$ g�rer le bon �tat et le focus
  if (LOWORD (mp1) == ID_LISTBOX_AL_CONS)
    {
		DWORD	wNbrEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_anm_consultation_al);

    if (wNbrEnr)
      {
			LPDRAWITEMSTRUCT					dis = (LPDRAWITEMSTRUCT) (mp2);
			PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE	pConsult = (PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE)pointe_enr (szVERIFSource, __LINE__, bx_anm_consultation_al, dis->itemID + 1);
			char											pszTextAl[TAILLE_MAX_MES_AL_COMPLET+1];
			PENV_AL_CONS							pEnv = (PENV_AL_CONS)pGetEnv (hwnd);

      if (!bFicConsOuvert)
        { // c'est le premier element de la liste
				PENV_AL_DYN	pEnvAlDyn = (PENV_AL_DYN)pGetEnv (hwndAlDyn);

        OuvreFicCons (pEnvAlDyn->szFicTri, &hficFichierCons6, &bFicConsOuvert);
        }
      if (lire_mess_rtat_consult (hficFichierCons6, (pConsult->wPosEnreg-1), pszTextAl))
        {
					DessineFondAlarme( CLR_GREEN, dis->rcItem,dis->hDC, TRUE);
				
 				SetTextColor(dis->hDC, CLR_BLACK); //$$ restaurer
				//$$ SetBkColor (dis->hDC, CLR_GREEN); //$$ restaurer
				SetBkMode(dis->hDC, TRANSPARENT); 
				// Dessine le texte avec tabs (offset de 0 car pas d'ic�ne)
				DessineTexteAvecTabs (pszTextAl, dis, 0);
				//DrawText (dis->hDC, pszTextAl, -1, &(dis->rcItem), DT_LEFT | DT_VCENTER | DT_NOPREFIX); //$$DT_ERASERECT);
        }  // lire_mess ok
      }  // nb_enr > 0
    // $$ dis->fsState = dis->fsStateOld = FALSE;
    }

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = (LRESULT) TRUE;
  return TRUE;
  }

// -----------------------------------------------------------------------
// 
// -----------------------------------------------------------------------
static BOOL OnCommandAlArchive (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_CONS			pEnv;
  PENV_AL_DYN				pEnvAlDyn;
  tx_anm_glob_al *	pGlobAl;

  switch (LOWORD (mp1))
    {
    case ID_BMP_MNU_CONS_VALI:
      pEnv = (PENV_AL_CONS)pGetEnv (hwnd);
      if (pEnv->bDlgTriOuverte)
        {
        ::SetWindowPos (pEnv->hwndTriAlCons, HWND_TOP, 0, 0, 0, 0,
          SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
        }
      else
        {
        pEnv->bDlgTriOuverte = TRUE;
        pEnv->hwndTriAlCons = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_AL_CONS_TRI), hwndAlDyn, dlgprocTriAlArchive, 0);
        ::ShowWindow (pEnv->hwndTriAlCons, SW_SHOW);

        pEnvAlDyn = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
        SetDlgItemText (pEnv->hwndTriAlCons, ID_9_AL_CONS,  pEnvAlDyn->szFicTri);
        SetDlgItemText (pEnv->hwndTriAlCons, ID_10_AL_CONS, pEnvAlDyn->szDateDeb);
        SetDlgItemText (pEnv->hwndTriAlCons, ID_11_AL_CONS, pEnvAlDyn->szHeureDeb);
        SetDlgItemText (pEnv->hwndTriAlCons, ID_12_AL_CONS, pEnvAlDyn->szDateFin);
        SetDlgItemText (pEnv->hwndTriAlCons, ID_13_AL_CONS, pEnvAlDyn->szHeureFin);
        SetDlgItemText (pEnv->hwndTriAlCons, ID_14_AL_CONS, pEnvAlDyn->szGroupe);
        SetDlgItemText (pEnv->hwndTriAlCons, ID_15_AL_CONS, pEnvAlDyn->szPriorite);
        SetDlgItemText (pEnv->hwndTriAlCons, ID_16_AL_CONS, pEnvAlDyn->szEvenement);
				
        }
      break;

    case ID_BMP_MNU_CONS_ARCH:
      pEnv = (PENV_AL_CONS)pGetEnv (hwnd);
      if (pEnv->bDlgEnregistreConsultationDArchiveOuverte)
        {
        ::SetWindowPos (pEnv->hwndEnregistreConsultationDArchive, HWND_TOP, 0, 0, 0, 0,
          SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
        }
      else
        {
        pEnv->bDlgEnregistreConsultationDArchiveOuverte = TRUE;
        pEnv->hwndEnregistreConsultationDArchive = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_AL_CONS_ARCH), hwndAlDyn, dlgprocEnregistreConsultationDArchive, 0);
        //::ShowWindow (pEnv->hwndEnregistreConsultationDArchive, SW_SHOW);
        }
      break;

    case ID_BMP_MNU_CONS_IMPR:
      pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // 1 seul enregistrement
      if (!pGlobAl->bImprimanteOuverte)
        {
        ImprimerRtatConsultation ();
        }
      else
        {
        ImprimerRtatConsultation ();
        }
      break;

		case IDCANCEL:
			::DestroyWindow (hwnd);
			break;
    }

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return TRUE;
  } // OnCommandAlArchive

// -----------------------------------------------------------------------
// 
// -----------------------------------------------------------------------
static BOOL OnDestroyAlArchive (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_AL_CONS	pDataCons = (PENV_AL_CONS)pGetEnv (hwnd);
  PENV_AL_DYN		pEnvAlDyn;

  if (pDataCons->bDlgTriOuverte)
    {
    ::DestroyWindow (pDataCons->hwndTriAlCons);
    }
  if (pDataCons->bDlgEnregistreConsultationDArchiveOuverte)
    {
    ::DestroyWindow (pDataCons->hwndEnregistreConsultationDArchive);
    }
  bLibereEnv (hwnd);

	pEnvAlDyn = (PENV_AL_DYN)pGetEnv (hwndAlDyn);
  if (pEnvAlDyn)
		pEnvAlDyn->bFenAlArchiveOuverte = FALSE;

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = (LRESULT) 0;
  return TRUE;
  }

// --------------------------------------------------------------------------
// Window-Procedure Consultation d'Archives d'Alarmes
// --------------------------------------------------------------------------
static LRESULT CALLBACK wndprocAlArchive (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;              // Valeur de retour
  BOOL     bTraite = TRUE;        // Indique commande trait�e

  switch (msg)
    {
    case WM_CREATE:
			bTraite = OnCreateAlArchive (hwnd, mp1, mp2, &mres);
			break;

    case WM_DESTROY:
			bTraite = OnDestroyAlArchive (hwnd, mp1, mp2, &mres);
			break;

    case WM_SIZE:
			bTraite = OnSizeAlArchive (hwnd, mp1, mp2, &mres);
			break;

    case WM_MEASUREITEM:
			bTraite = OnMeasureItemAlArchive (hwnd, mp1, mp2, &mres);
			break;

    case WM_DRAWITEM:
			bTraite = OnDrawItemAlArchive (hwnd, mp1, mp2, &mres);
			break;

    case WM_COMMAND:
			bTraite = OnCommandAlArchive (hwnd, mp1, mp2, &mres);
			break;

    default:
			bTraite = FALSE;
			break;
    }

  if (!bTraite)
    {
    mres = DefWindowProc (hwnd, msg, mp1, mp2);
    }

  return mres;
  }


// --------------------------------------------------------------------------
// Cr�ee la fen�tre de visualisation dynamique des alarmes
static BOOL bCreeFenAlDyn (void)
  {
  BOOL	bOk = FALSE;

  if (existe_repere (bx_anm_element_al))
    {
		static BOOL bClasseEnregistree = FALSE;

		// enregistre la classe si n�cessaire
		if (!bClasseEnregistree)
			{
			WNDCLASS wc;

			wc.style					= 0;
			wc.lpfnWndProc		= wndprocAlDyn; 
			wc.cbClsExtra			= 0; 
			wc.cbWndExtra			= 0; 
			wc.hInstance			= Appli.hinst; 
			wc.hIcon					= NULL;
			wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
			wc.hbrBackground	= (HBRUSH)(COLOR_MENU+1);
			wc.lpszMenuName		= NULL; 
			wc.lpszClassName	= szClasseAlDyn; 
			bClasseEnregistree = RegisterClass (&wc);
			}

		// Classe enregistr�e ?
		if (bClasseEnregistree)
      {
			// oui => cr�e la fen�tre :
			LONG               x=0;
			LONG               y=0;
			LONG               cx=0;
			LONG               cy=0;
			char              pszMessInf[c_nb_car_message_inf];
			tx_anm_glob_al*		pGlobAl=NULL;

			// calcul des coordonn�es de la fen�tre
      hSpace = hSpcCree (Appli.sizEcran.cx, Appli.sizEcran.cy, c_G_NB_X, c_G_NB_Y);
      x = (Appli.sizEcran.cx - CX_FEN_AL_DYN) / 2;
      y = (Appli.sizEcran.cy - CY_FEN_AL_DYN) / 2;
      cx = CX_FEN_AL_DYN;
      cy = CY_FEN_AL_DYN;
      if (existe_repere (bx_anm_glob_al))
        {
        pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1);
        if (pGlobAl->iFenAlX != -1)
          {
          x =  pGlobAl->iFenAlX;
          y =  pGlobAl->iFenAlY;
          cx = pGlobAl->iFenAlCx;
          cy = pGlobAl->iFenAlCy;
          anm_cnv_coord_pm (&x, &y, &cx, &cy);
          }
        }

		// Mise � jour de la variable Globale de parm�trage des alarmes dynamique
		MajParamFenetreAlDyn();

		// La fen�tre est fixe si le dernier param�tre de la ligne de description surveillance 
		// � son bit 0 � 1
		  BOOL bFenetreFixe=(dwParamFenetreAlDyn & PARAM_FENETRE_AL_DYN_MASQUE_FENETRE_ALM_FIXE)!=0;

		  bLngLireInformation (c_inf_titre_al_dyn, pszMessInf);
      // FCF_STANDARD & ~FCF_MENU & ~FCF_ACCELTABLE & ~FCF_TASKLIST | FCF_NOBYTEALIGN;
		DWORD dwWindowStyle = WS_OVERLAPPEDWINDOW | WS_VISIBLE;
		if (bFenetreFixe)
		{
			dwWindowStyle = WS_POPUP | WS_VISIBLE;
		}
	    hwndAlDyn = CreateWindow (
				szClasseAlDyn,	// class name
				pszMessInf,	// window name
				dwWindowStyle, // |WS_CLIPSIBLINGS | WS_CHILD window style
				x, y, cx, cy,
				NULL,	// handle parent window
				NULL,	// handle to menu or child-window identifier
				Appli.hinst,	// handle to application instance
				NULL);	// pointer to window-creation data

      bOk = (hwndAlDyn != NULL);
      if (bOk)
        {
				// Met la fen�tre en ic�ne
				::ShowWindow (hwndAlDyn, SW_MINIMIZE);

				// Tempo
				if (existe_repere (bx_anm_tempo_al))
					{
					::SetTimer (hwndAlDyn, ID_TIMER_TEMPO, TPS_ECH_TEMPO, NULL);
					}
				}
      }
    }
  return bOk;
  }

// -------------------------------------------
// Initialisation de l'animateur d'alarme
//
static void init_anim_al (void)
  {
  char              fic_messages[MAX_PATH];
  tx_anm_glob_al *pGlobAl;

  //$$ OuvrirMotsReserves ();

  // inits gestion dynamique (surveillance et visualisation)
  enleve_bloc (buffer_anm_surv_al); // bloc utilis�s uniquement dans ce module
  enleve_bloc (buffer_anm_visu_al);	// bloc utilis�s uniquement dans ce module
  cree_bloc (buffer_anm_surv_al, sizeof (t_buffer_anm_surv_al), 0);
  cree_bloc (buffer_anm_visu_al, sizeof (t_buffer_anm_visu_al), 0);

  // inits gestion archivage
  StrCopy (fic_messages, pszPathNameDoc());
  pszNomAjouteExt (fic_messages, MAX_PATH, EXTENSION_PA1);
  if (uFileOuvre (&hficFichierAscii, fic_messages, TAILLE_MAX_MESSAGE_AL, CFman::OF_OUVRE_LECTURE_SEULE) != NO_ERROR)
		{
		maj_statut_al (0x8);
		}
  pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1); // un seul enregistrement
  StrCopy (pszNomFicArch, pGlobAl->pszNomFic);
  StrDeleteDoubleQuotation (pszNomFicArch);
	pszPathNameAbsoluAuDoc (pszNomFicArch);

  // initialiation du tableau de compteurs de priorites
  InitContexteBoiteCompteur ();
  }

// --------------------------------------------------------------------------
// Fin de l'animateur d'alarmes
// $$ V�rifier correspondance fermetures /ouvertures multiples
static void termine_anim_al (void)
  {
  //FermerMotsReserves ();

  // fermeture fichier de messages d'alarmes
  uFileFerme (&hficFichierAscii);

  // fermeture fichiers archivages
  if (bFicArchOuvert)
    {
    FermeFicArch();
    bFicArchOuvert = FALSE;
    }
  if (bFicConsOuvert)
    {
    FermeFicCons (&hficFichierCons6, &bFicConsOuvert);
    }

  }

// --------------------------------------------------------------------------
// Thread scrutation des messages PM (Boucle WinGet/Dispatch)
//                                 +(Executions Services)
// --------------------------------------------------------------------------
UINT __stdcall uThreadWAnmAl (void *hThrd)
  {
	DumpVie(uThreadWAnmAl);
  UINT				wFct;
  PVOID				pParamIn;
  DWORD *			pwError;
  BOOL				bEndFct;
  //
  BOOL				bBreak = FALSE;
  ANM_AL_INFOS_SERVICES* pInfosServicesAnmAl;
  DWORD				wNbServices;
  DWORD				wTypeService;
  BYTE *			pServices;
  MSG					msg;

  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
  while (bThreadAttenteMessage ((HTHREAD)hThrd, &pParamIn, NULL, &pwError, &wFct))
    {
    bEndFct = TRUE;
    //
    switch (wFct)
      {
      case W_ANM_AL_INIT:
        //
        if (existe_repere (bx_anm_element_al))
          {
          init_anim_al ();
          (*pwError) = W_ANM_AL_ERR_NONE;
          }
        else
          {
          (*pwError) = W_ANM_AL_ERR_INIT;
          }
        break;

      case W_ANM_AL_INIT_MODE_PM:
        //
        (*pwError) = W_ANM_AL_ERR_NONE;
        break;

      case W_ANM_AL_CREATE_WINDOWS:
        //
        bCreeFenetreBreakAnmAl (&bBreak);
        if (bCreeFenAlDyn ())
          {
          (*pwError) = W_ANM_AL_ERR_NONE;
          }
        else
          {
          (*pwError) = W_ANM_AL_ERR_CREATE_WINDOWS;
          }
        break;

      case W_ANM_AL_PM_LOOP:
        // Une it�ration dans la boucle de message
        if (GetMessage (&msg, NULL, 0, 0))
          {
					if (!TranslateAccelerator (Appli.hwnd, hAccelTouchesFonctions, &msg))
						{
						if (!IsDialogMessage (hwndDialogueActive, &msg))
							DispatchMessage (&msg);
						}

          bEndFct = FALSE;
          }
        if (bBreak)
          {
          bBreak = FALSE;
          bEndFct = TRUE;
          }
        break;

      case W_ANM_AL_READ_SERVICES:
        //
        pInfosServicesAnmAl  = (ANM_AL_INFOS_SERVICES*)pParamIn;
				if (pInfosServicesAnmAl)
					{
					wNbServices  = pInfosServicesAnmAl->NbServices;
					wTypeService = pInfosServicesAnmAl->TypeService;
					pServices    = &(pInfosServicesAnmAl->bBufferServices [0]);
					}
				else
					{
					VerifWarningExit;
					wNbServices = 0;
					}
        (*pwError)   = W_ANM_AL_ERR_NONE;
        break;

      case W_ANM_AL_EXEC_SERVICES:
        // 8
        if (wNbServices)
          {
          ExecService (wTypeService, &pServices);
          wNbServices--;

          while (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
            {
						if (!TranslateAccelerator (Appli.hwnd, hAccelTouchesFonctions, &msg))
							{
							if (!IsDialogMessage (hwndDialogueActive, &msg))
								DispatchMessage (&msg);
							}
            }
          bEndFct = FALSE;
          }
        break;

      case W_ANM_AL_DEL_WINDOWS:
        //
        FermeFenetreBreakAnmAl ();
        FermeFenetresAnimAlarmes ();
        (*pwError) = W_ANM_AL_ERR_NONE;
        break;

      case W_ANM_AL_END_MODE_PM:
        //
        (*pwError) = W_ANM_AL_ERR_NONE;
        break;

      case W_ANM_AL_TERMINE:
        //
        termine_anim_al ();
        (*pwError) = W_ANM_AL_ERR_NONE;
        break;

      default:
        break;
      }
    //
    if (bEndFct)
      {
      ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
      ThreadFinTraiteMessage ((HTHREAD)hThrd);
      }
    else
      {
      ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, 0);
      }
    //
    }
  return uThreadTermine ((HTHREAD)hThrd, 0);
  }

// --------------------------------------------------------------------------
DWORD dwTailleInfosServicesAnmAl (void)
  {
  return (sizeof (ANM_AL_INFOS_SERVICES));
  }

// --------------------------------------------------------------------------
void WAnmAlBreakPmLoop (void)
  {
  ::PostMessage (hwndBreakAnmAl, WM_BREAK_PM_ANMAL_LOOP, 0, 0);
  }

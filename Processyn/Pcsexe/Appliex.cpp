/*-------------------------------------------------------------------------*
 | Ce fichier est la propriete de					   |
 |									   |
 |			 Societe LOGIQUE INDUSTRIE			   |
 |	       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		   |
 |									   |
 | Il demeure sa propriete exclusive et est confidentiel.		   |
 | Aucune diffusion n'est possible sans accord ecrit.                      |
 |-------------------------------------------------------------------------*/
// Appliex.c	   Gestion des Applications  en execution
// Win32 MC 22/7/97

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"         // OuvrirMotsReserves ()
#include "tipe.h"
#include "DocMan.h"
#include "UStr.h"
#include "FMan.h"
#include "PathMan.h"
#include "DocMan.h"
#include "Appli.h"
#include "USem.h"
#include "mem.h"
#include "UChrono.h"                
#include "dongle.h"
#include "lance.h"           // InitFctDrivers ();
#include "IdLngLng.h"
#include "LireLng.h"
#include "init_txt.h"				// PCS.INI......InterpreteFichierIni ()
#include <UCom.h>         // nComFerme (), raccroche_tache ()
#include "cmdpcs.h"
#include "WExe.h"
#include "IdStrProcessyn.h"
#include "SignatureFichier.h"
#include "appliex.h"

// --------------------------------------------------------------------------
BOOL ChargeApplication (char * pszNom, BOOL aChaud)
  {
  BOOL bOk = FALSE;
  char szNomApplicationChargee [MAX_PATH] = "";

	// Cr�e le nom du fichier � charger
  StrCopy (szNomApplicationChargee,pszNom);
  if (aChaud)
    bOk = (pszNomAjouteExt (szNomApplicationChargee, MAX_PATH, EXTENSION_A_CHAUD)!= NULL);
  else
    bOk = (pszNomAjouteExt (szNomApplicationChargee, MAX_PATH, EXTENSION_A_FROID)!= NULL);

  if (bOk)
    {
		// Essaye de charger l'application :
    raz_mem ();
    bOk = CFman::bFAcces (szNomApplicationChargee, CFman::OF_OUVRE_LECTURE_SEULE);
		if (bOk)
		{
			bOk = (charge_memoire (szNomApplicationChargee) == 0);
			if (bOk)
			{
				CSignatureFichier SignatureFichier(SIGNATURE_FORMAT_XPC_EN_COURS);
				bOk = SignatureFichier.bDBPCSSignatureValide();
			}
		}

    if (bOk)
      {
			bSetPathNameDoc (pszNom);
			SetNouveauDoc (FALSE);
			MajFichierCmdPcs (pszPathNameDoc());
			InterpreteFichierIni ();
      OuvrirMotsReserves ();
      InitFctDrivers ();
      }
    else
      {
			InitPathNameDoc ();
      }
    }

  return (bOk);
  }

// --------------------------------------------------------------------------
void SauveApplication (char *pszNom,BOOL aChaud)
  {
  char pszNomApplicationChargee [MAX_PATH] = "";
	BOOL bOk;

  encrypte_hot ();
  bOk = FALSE;
  StrCopy (pszNomApplicationChargee,pszNom);
  if (aChaud)
    {
    bOk = (pszNomAjouteExt (pszNomApplicationChargee, MAX_PATH, EXTENSION_A_CHAUD)!= NULL); //MetSuffixeHot (pszNomApplicationChargee);
    }
  else
    {
    bOk = (pszNomAjouteExt (pszNomApplicationChargee, MAX_PATH, EXTENSION_A_FROID)!= NULL); //MetSuffixeXpc (pszNomApplicationChargee);
    }
	if (bOk)
		{
		CSignatureFichier SignatureFichier(SIGNATURE_FORMAT_XPC_EN_COURS);
		SignatureFichier.DBPCSSetSignature();
    sauve_memoire (pszNomApplicationChargee);
		}

  return;
  }

// --------------------------------------------------------------------------
BOOL DecodeApplicSuivante (char *pszApplicationSuivante, BOOL *bAChaud)
  {
  BOOL  bUneApplicSuivante;
	BOOL	bTemperature;
  DWORD wIdxSeparateurTemperature;

  bTemperature = FALSE;
  wIdxSeparateurTemperature = StrSearchChar (pszApplicationSuivante, CAR_SEPARATEUR_LIGNE_CDE);
  if (wIdxSeparateurTemperature != STR_NOT_FOUND)
    {
    bTemperature = (BOOL) (StrGetChar (pszApplicationSuivante, wIdxSeparateurTemperature + 1) == CARACTERE_ENCHAINE_A_CHAUD);
    StrSetLength (pszApplicationSuivante, wIdxSeparateurTemperature);
    }

  bUneApplicSuivante = (BOOL) (!StrIsNull (pszApplicationSuivante));
	if (bUneApplicSuivante)
		{
		(*bAChaud) = bTemperature;
		}

  return (bUneApplicSuivante);
  }

// --------------------------------------------------------------------------
void InitRessourcesAppli(void)
  {
  DWORD wCanal;

  //pour les com
  for (wCanal = 1; wCanal <= NB_MAX_VOIE; wCanal++)
    {
    nComFerme (wCanal);
    }
  }

/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de																				    |
 |		    Societe LOGIQUE INDUSTRIE																					|
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3											|
 | Il est et demeure sa propriete exclusive et est confidentiel.								|
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/
// DllMuxEx.c (Dynamic Link Librarie Multiplexeur en Exploitation)
// Acc�s aux services Processyn en exploitation (utilis�s par les DLLs utilisateurs)

#include "stdafx.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "Pcs_Sys.h"
#include "PcsVarEx.h"
#include "DllMuxEx.h"


// --------------------------------------------------------------------------
//                        PROCEDURES  LOCALES
// --------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Traitement des fonctions du service versions
static UINT uServiceVersions (PHEADER_MESSAGE_MUX pMes)
	{
	UINT uRes = MUX_ERR_OK;

	// traitement selon le type de fonction demand�e
	switch (pMes->uIdFonction)
		{
		case ID_FONCTION_MXVR_VERSION_MUX: // Version du multiplexeur
			{
			PPARAM_MXVR_VERSION_MUX pMes2 = (PPARAM_MXVR_VERSION_MUX)pMes;

			if (pMes->dwTaille == sizeof (*pMes2))
				pMes2->uVersionCourante = VERSION_MUX_COURANTE;
			else
				uRes = MUX_ERR_TAILLE_PARAMETRES;
			}
			break;

		default: // Signature Id Fonction non reconnue
			uRes = MUX_ERR_ID_FONCTION;
			break;
		}
	return uRes;
	} // uServiceVersions

//---------------------------------------------------------------------------
// Traitement des fonctions du service DB PCS
static UINT uServiceDB (PHEADER_MESSAGE_MUX pMes)
	{
	UINT uRes = MUX_ERR_OK;

	// traitement selon le type de fonction demand�e
	switch (pMes->uIdFonction)
		{
		case ID_FONCTION_MXDB_COPIE_VAR_LOG:	// lit/�crit la valeur d'une var logique
			{
			PPARAM_MXDB_COPIE_VAR_LOG pMes2 = (PPARAM_MXDB_COPIE_VAR_LOG)pMes;

			// taille buffer Ok ?
			if (pMes->dwTaille == sizeof (*pMes2))
				{
				// oui => trouve la variable sp�cifi�e ?
				DWORD	dwBloc;
				DWORD	dwPos = CPcsVarEx::dwGetVarNomEx (pMes2->szNomVar, &dwBloc);

				dwPos = CPcsVarEx::dwPosVarLogEx (dwPos, dwBloc, pMes2->dwIndex);
				if (dwPos)
					{
					// oui => effectue le transfert dans le sens demand�
					if (pMes2->bAffecte)
						CPcsVarEx::SetValVarLogEx (dwPos, pMes2->bValeur);
					else
						pMes2->bValeur = CPcsVarEx::bGetValVarLogEx (dwPos);
					}
				else
					uRes = MUX_ERR_RESULTAT_FONCTION;
				}
			else
				uRes = MUX_ERR_TAILLE_PARAMETRES;
			}
			break;

		case ID_FONCTION_MXDB_COPIE_VAR_NUM:	// lit/�crit la valeur d'une var num�rique
			{
			PPARAM_MXDB_COPIE_VAR_NUM pMes2 = (PPARAM_MXDB_COPIE_VAR_NUM)pMes;

			if (pMes->dwTaille == sizeof (*pMes2))
				{
				// oui => trouve la variable sp�cifi�e ?
				DWORD	dwBloc;
				DWORD	dwPos = CPcsVarEx::dwGetVarNomEx (pMes2->szNomVar, &dwBloc);

				dwPos = CPcsVarEx::dwPosVarNumEx (dwPos, dwBloc, pMes2->dwIndex);
				if (dwPos)
					{
					// oui => effectue le transfert dans le sens demand�
					if (pMes2->bAffecte)
						CPcsVarEx::SetValVarNumEx (dwPos, pMes2->fValeur);
					else
						pMes2->fValeur = CPcsVarEx::fGetValVarNumEx (dwPos);
					}
				else
					uRes = MUX_ERR_RESULTAT_FONCTION;
				}
			else
				uRes = MUX_ERR_TAILLE_PARAMETRES;
			}
			break;

		case ID_FONCTION_MXDB_COPIE_VAR_MES:	// lit/�crit la valeur d'une var message
			{
			PPARAM_MXDB_COPIE_VAR_MES pMes2 = (PPARAM_MXDB_COPIE_VAR_MES)pMes;

			if (pMes->dwTaille == sizeof (*pMes2))
				{
				// oui => trouve la variable sp�cifi�e ?
				DWORD	dwBloc;
				DWORD	dwPos = CPcsVarEx::dwGetVarNomEx (pMes2->szNomVar, &dwBloc);

				dwPos = CPcsVarEx::dwPosVarMesEx (dwPos, dwBloc, pMes2->dwIndex);
				if (dwPos)
					{
					// oui => effectue le transfert dans le sens demand�
					if (pMes2->bAffecte)
						CPcsVarEx::SetValVarMesEx (dwPos, pMes2->szValeur);
					else
						CPcsVarEx::pszGetValVarMesEx (pMes2->szValeur, dwPos);
					}
				else
					uRes = MUX_ERR_RESULTAT_FONCTION;
				}
			else
				uRes = MUX_ERR_TAILLE_PARAMETRES;
			}
			break;

		case ID_FONCTION_MXDB_COPIE_VAR_SYS_LOG:	// lit/�crit la valeur d'une var logique
			{
			PPARAM_MXDB_COPIE_VAR_SYS_LOG pMes2 = (PPARAM_MXDB_COPIE_VAR_SYS_LOG)pMes;

			// taille buffer Ok ?
			if (pMes->dwTaille == sizeof (*pMes2))
				{
				// oui => trouve la variable sp�cifi�e ?
				DWORD	dwBloc;
				DWORD	dwPos = CPcsVarEx::dwGetVarSysEx (pMes2->dwIdVarSys, &dwBloc);

				dwPos = CPcsVarEx::dwPosVarLogEx (dwPos, dwBloc, pMes2->dwIndex);
				if (dwPos)
					{
					// oui => effectue le transfert dans le sens demand�
					if (pMes2->bAffecte)
						CPcsVarEx::SetValVarLogEx (dwPos, pMes2->bValeur);
					else
						pMes2->bValeur = CPcsVarEx::bGetValVarLogEx (dwPos);
					}
				else
					uRes = MUX_ERR_RESULTAT_FONCTION;
				}
			else
				uRes = MUX_ERR_TAILLE_PARAMETRES;
			}
			break;

		case ID_FONCTION_MXDB_COPIE_VAR_SYS_NUM:	// lit/�crit la valeur d'une var num�rique
			{
			PPARAM_MXDB_COPIE_VAR_SYS_NUM pMes2 = (PPARAM_MXDB_COPIE_VAR_SYS_NUM)pMes;

			if (pMes->dwTaille == sizeof (*pMes2))
				{
				// oui => trouve la variable sp�cifi�e ?
				DWORD	dwBloc;
				DWORD	dwPos = CPcsVarEx::dwGetVarSysEx (pMes2->dwIdVarSys, &dwBloc);

				dwPos = CPcsVarEx::dwPosVarNumEx (dwPos, dwBloc, pMes2->dwIndex);
				if (dwPos)
					{
					// oui => effectue le transfert dans le sens demand�
					if (pMes2->bAffecte)
						CPcsVarEx::SetValVarNumEx (dwPos, pMes2->fValeur);
					else
						pMes2->fValeur = CPcsVarEx::fGetValVarNumEx (dwPos);
					}
				else
					uRes = MUX_ERR_RESULTAT_FONCTION;
				}
			else
				uRes = MUX_ERR_TAILLE_PARAMETRES;
			}
			break;

		case ID_FONCTION_MXDB_COPIE_VAR_SYS_MES:	// lit/�crit la valeur d'une var message
			{
			PPARAM_MXDB_COPIE_VAR_SYS_MES pMes2 = (PPARAM_MXDB_COPIE_VAR_SYS_MES)pMes;

			if (pMes->dwTaille == sizeof (*pMes2))
				{
				// oui => trouve la variable sp�cifi�e ?
				DWORD	dwBloc;
				DWORD	dwPos = CPcsVarEx::dwGetVarSysEx (pMes2->dwIdVarSys, &dwBloc);

				dwPos = CPcsVarEx::dwPosVarMesEx (dwPos, dwBloc, pMes2->dwIndex);
				if (dwPos)
					{
					// oui => effectue le transfert dans le sens demand�
					if (pMes2->bAffecte)
						CPcsVarEx::SetValVarMesEx (dwPos, pMes2->szValeur);
					else
						CPcsVarEx::pszGetValVarMesEx (pMes2->szValeur, dwPos);
					} 
				else
					uRes = MUX_ERR_RESULTAT_FONCTION;
				}
			else
				uRes = MUX_ERR_TAILLE_PARAMETRES;
			}
			break;

		case ID_FONCTION_MXDB_INFO_VAR:	// lit des infos d'une var
			{
			PPARAM_MXDB_INFO_VAR pMes2 = (PPARAM_MXDB_INFO_VAR)pMes;

			if (pMes->dwTaille == sizeof (*pMes2))
				{
				// oui => trouve la variable sp�cifi�e ?
				DWORD	dwBloc;
				DWORD	dwPos = CPcsVarEx::dwGetVarNomEx(pMes2->szNomVar, &dwBloc);
				X_E_S x_e_s;	// adresse des infos de la var voulue

				// R�cup�re les infos d'une Var en exploitation
				if (CPcsVarEx::bInfoVarEx (dwPos, dwBloc, &x_e_s))
					{
					pMes2->dwGenre = x_e_s.wBlocGenre;		// c_res_message, c_res_numerique ou c_res_logique
					pMes2->dwIndexMax = x_e_s.wTaille;	// 0 = var simple ou index max de tableau (1..n)
					}
				else
					uRes = MUX_ERR_RESULTAT_FONCTION;
				}
			else
				uRes = MUX_ERR_TAILLE_PARAMETRES;
			}
			break;

		default: // Signature Id Fonction non reconnue
			uRes = MUX_ERR_ID_FONCTION;
			break;
		}
	return uRes;
	} // uServiceDB

// --------------------------------------------------------------------------
// Point d'entr�e de la fonction de multiplexage des services Processyn en exploitation
static UINT	uMuxServiceEnExploitation (PHEADER_MESSAGE_MUX pMes)
	{
	UINT uRes = MUX_ERR_OK;

	// adresse param�tres valide ?
	if (pMes)
		{
		// oui => traitement selon le type de service demand�
		switch (pMes->uIdService)
			{
			case ID_SERVICE_MX_VERSIONS: // Service Version processyn
				uRes = uServiceVersions (pMes);
				break;

			case ID_SERVICE_MX_DB_PCS: // Service Base de donn�es Processyn
				uRes = uServiceDB (pMes);
				break;

			default: // Signature Id Service non reconnue
				uRes = MUX_ERR_ID_SERVICE;
				break;
			} // switch (pMes->uIdService)
		} // if (pMes)
	else
		uRes = MUX_ERR_PARAMETRES_INVALIDES;
	return uRes;
	} // uMuxServiceEnExploitation

// --------------------------------------------------------------------------
//                        PROCEDURES  PUBLIQUES
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Renvoie un handle sur le point d'entr�e des services Processyn en exploitation
HANDLE hMuxExProcessyn (void)
	{
	return (HANDLE)(uMuxServiceEnExploitation);
	}


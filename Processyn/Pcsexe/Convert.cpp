#include "stdafx.h"
#include "std.h"
#include "Convert.h"

// --------------------------------------------------------------------------
#define FIRST_HALF_BYTE_MASK  ((WORD) 0x000F)
#define BIT_SIZE_OF_HALF_BYTE 4
#define TEN_BASE 10

// --------------------------------------------------------------------------
BOOL cvtBcdToBin (WORD wBcdValue, WORD *pwBinValue)
  {
  BOOL bOk;
  WORD    wBinValue;
  WORD    wTenUnit;
  WORD    wDigit;

  bOk       = TRUE;
  wBinValue = 0;
  wTenUnit  = 1;
  while ((wBcdValue != 0) && (bOk))
    {
    wDigit    = wBcdValue & FIRST_HALF_BYTE_MASK;
    bOk       = (BOOL) (wDigit < TEN_BASE);
    wBinValue = wBinValue + (wTenUnit * wDigit);
    wTenUnit  = wTenUnit * TEN_BASE;
    wBcdValue = wBcdValue >> BIT_SIZE_OF_HALF_BYTE;
    }

  if (bOk)
    {
    (*pwBinValue) = wBinValue;
    }

  return (bOk);
  }


// --------------------------------------------------------------------------
BOOL cvtBinToBcd (WORD wBinValue, WORD *pwBcdValue)
  {
  BOOL bOk;
  WORD    wBcdValue;
  WORD    wTenUnit;
  WORD    wNbZero;


  wTenUnit = 1;
  for (wNbZero = (2 * sizeof (wBinValue)) - 1; wNbZero > 0; wNbZero--)
    {
    wTenUnit = wTenUnit * TEN_BASE;
    }

  bOk = (BOOL) (wBinValue < (wTenUnit * TEN_BASE));

  if (bOk)
    {
    wBcdValue = 0;
    while (wTenUnit)
      {
      wBcdValue = wBcdValue << BIT_SIZE_OF_HALF_BYTE;
      wBcdValue = wBcdValue + (wBinValue / wTenUnit);
      wBinValue = wBinValue % wTenUnit;
      wTenUnit  = wTenUnit / TEN_BASE;
      }
    (*pwBcdValue) = wBcdValue;
    }

  return (bOk);
  }

//------------------------------------
// WEntLog.h
//------------------------------------

#ifndef   WENTLOG_H
  #define WENTLOG_H

// cr�ation d'une fen�tre entr�e logique (pour animation de synoptique)
HWND hwndCreeFenetreEntLog (HWND hwndParent, INT x, INT y, INT cx, INT cy, UINT uId);

#endif // WENTLOG_H
//----------------------------------------------------------------------+
//|   Ce fichier est la propriete de                                     |
//|              Societe LOGIQUE INDUSTRIE                               |
//|       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
//|   Il est demeure sa propriete exclusive et est confidentiel.         |
//|   Aucune diffusion n'est possible sans accord ecrit                  |
//|----------------------------------------------------------------------|
//|                                                                      |
//|   Titre   : SCRUT_JB.C            Unite de Scrutation sur ligne JBUS |
//|                                                                      |
//|   Mise a jours:                                                      |
//|                                                                      |
//|   +--------+--------+---+--------------------------------------+     |
//|   | date   | qui    |no | raisons                              |     |
//|   +--------+--------+---+--------------------------------------+     |
//|   |04/08/92| PR     |80 | Int�gration Time_out April s�rie 1000|     |
//|   |25/03/93| JB     |xx | Regroupement des fonction recoit_jbus|     |
//|   |        |        |   | en recoit_jb, et des fonctions       |     |
//|   |        |        |   | emet_jbus en emet_jb                 |     |
//|   |        |        |   |                                      |     |
//|   +--------+--------+---+--------------------------------------+     |
//|                                                                      |
//+----------------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "UStr.h"
#include "tipe_jb.h"
#include "Modbus.h"
#include "mem.h"
#include "UCom.h"
#include "cvt_jb.h"
#include "Descripteur.h"
#include "pcs_sys.h"
#include "PcsVarEx.h"
#include "Verif.h"
#include "scrut_jb.h"
VerifInit;

#define FACTEUR_TIME_OUT_MODBUS 500

#define c_i16_max ((FLOAT)(32767))
#define c_i16_min ((FLOAT)(-32768))

// ----------------------------------------------------------------------
// recoit pointeur sur adr bd de la va du canal.
// si le canal obtenu est != 0, teste si le canal est ouvert, ouvre sinon, si erreur renvoi 0 sinon le n� de canal
static DWORD dwCanalModbusOuvert (DWORD ptr_numero_canal)
	{
	PFLOAT cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_numero_canal);
	DWORD dwNumeroCanal = (DWORD)(*cour_num);

	if (dwNumeroCanal != 0)
		{
		if (!bComOuverte (dwNumeroCanal))
			{
			// Recherche des parametres de communication
			BOOL bTrouve = FALSE;
			DWORD dwIndex = 1;
			DWORD nb_enr = 0;
			t_parametre_com*para_com;
			
			if (existe_repere (bx_para_jbus_com))
				{
				nb_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_para_jbus_com);
				}
			while ((!bTrouve) && (dwIndex <= nb_enr)) 
				{
		    para_com = (t_parametre_com*)pointe_enr (szVERIFSource, __LINE__, bx_para_jbus_com, dwIndex);
				bTrouve = ((para_com->CanalVarOuCste == ID_CHAMP_VAR) && (para_com->numero_canal == ptr_numero_canal));
				dwIndex++;
				}
			// Ouverture voie
			if (bTrouve)
				{
				PX_VAR_SYS position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, VA_SYS_STATUS_JBUS);
				FLOAT *ptr_num= (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (position->dwPosCourEx));

				(*ptr_num) = (FLOAT)nOuvertureVoieModbus (dwNumeroCanal, para_com->vitesse, para_com->nb_de_bits,para_com->parite,para_com->encadrement);
				}
			else
				{
				dwNumeroCanal = 0;
				VerifWarningExit;
				}
			}
		}
	else // ferme si ancien canal != 0
		{
		PFLOAT mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_numero_canal);

		dwNumeroCanal = (DWORD)(*mem_num);
		if (dwNumeroCanal != 0)
			{
			if (bComOuverte (dwNumeroCanal))
				{
				PX_VAR_SYS position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, VA_SYS_STATUS_JBUS);
				FLOAT *ptr_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (position->dwPosCourEx));

				(*ptr_num) = (FLOAT)nFermetureVoieModbus (dwNumeroCanal);
				}
			dwNumeroCanal = 0;
			}
		}
	return (dwNumeroCanal);
	}
// ----------------------------------------------------------------------
static void maj_statut (DWORD statut_jb, DWORD no_esclave)
  {
  CPcsVarEx::SetBitsValVarSysNumEx (VA_SYS_STATUS_JBUS, statut_jb);
  CPcsVarEx::SetValVarSysNumEx (c_sys_num_esclave_jb, (FLOAT)no_esclave);
  }

//--------------------------------------------------------------------------
// Conversion d'un type de variable (nomenclature base de donn�es PCS) en type de variable MODBUS
static MODBUS_VAR nModbusVarFromPcsVar (DWORD TypeVarPcs)
	{
	switch (TypeVarPcs)
		{
		case c_res_logique: return MODBUS_VAR_LOG;
		case c_res_entier:	return MODBUS_VAR_ENTIER;
		case c_res_reels:		return MODBUS_VAR_REEL;
		case c_res_mots:		return MODBUS_VAR_MOT;
		case c_res_message: return MODBUS_VAR_MESSAGE;
		default: VerifWarningExit;return MODBUS_VAR_LOG; // par d�faut
		}
	}

//--------------------------------------------------------------------------
static void recoit_jbus (DWORD bx_para_jbus, DWORD bx_jbus_e, DWORD le_genre)
  {
  PBUFFER_MODBUS	table_trans_jbus;
  PBOOL cour_log;
  FLOAT *cour_num;
  PSTR	cour_mes;
  PX_PARAMETRE_TRAME_MODBUS	para_trame;
  PX_MODBUS_E	donneex_jbus;
  DWORD adresse_automate_depart;
  DWORD nbr_trame;
  DWORD index_para_trame;
  DWORD pos_donneex_jbus;
  DWORD pos_debut_donneex_jbus;
  DWORD pos_fin_donneex_jbus;
  DWORD wNumeroEsclave;
  DWORD numero_canal;
  DWORD erreur;
  int  index_l;
  char tempo_mes[82];

  table_trans_jbus = (PBUFFER_MODBUS)pointe_enr (szVERIFSource, __LINE__, bx_table_transfert_jb, 1);
  if (existe_repere (bx_para_jbus) )
    {
    nbr_trame = nb_enregistrements (szVERIFSource, __LINE__, bx_para_jbus);
    }
  else
    {
    nbr_trame = 0;
    }
  for (index_para_trame = 1; (index_para_trame <= nbr_trame); index_para_trame++)
    {
    para_trame = (PX_PARAMETRE_TRAME_MODBUS)pointe_enr (szVERIFSource, __LINE__, bx_para_jbus, index_para_trame);
		if (para_trame->CanalVarOuCste == ID_CHAMP_VAR)
			{
			numero_canal = dwCanalModbusOuvert (para_trame->numero_canal); 
			}
		else
			{
			numero_canal = para_trame->numero_canal;
			}
		//
		if (numero_canal != 0)
			{
			cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, para_trame->i_cmd_rafraich);
			if (*cour_log)
				{
			 if (para_trame->EsclVarOuCste==ID_CHAMP_VAR)
					{
					cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,para_trame->numero_esclave);
					wNumeroEsclave = (DWORD)(*cour_num);
					}
				else
					{
					wNumeroEsclave = para_trame->numero_esclave;
					}
				pos_debut_donneex_jbus = para_trame->index_debut;
				pos_fin_donneex_jbus   = para_trame->index_fin;
				adresse_automate_depart = para_trame->adresse_automate_depart;
				switch (le_genre)
					{
					case c_res_logique :
						erreur = nEchangeModbus (numero_canal,
														 (MODBUS_F)para_trame->numero_fonction,
														 para_trame->nbre_a_lire,
														 table_trans_jbus,
														 (DWORD)wNumeroEsclave,
														 (MODBUS_AUTOMATE)para_trame->tipe_esclave,
														 para_trame->adresse_automate_depart,
														 (DWORD)(FACTEUR_TIME_OUT_MODBUS * para_trame->fNbUnitesTimeOut),
														 para_trame->retry,
														 MODBUS_VAR_LOG);
						if (erreur == 0)
							{
							for (pos_donneex_jbus = pos_debut_donneex_jbus; (pos_donneex_jbus <= pos_fin_donneex_jbus); pos_donneex_jbus++)
								{
								donneex_jbus = (PX_MODBUS_E)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e, pos_donneex_jbus);
								cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, donneex_jbus->index_es_associe);
								(*cour_log) = (BOOL)table_trans_jbus->table_log [donneex_jbus->i_tab_recep_significatif-1];
								} // for 
							}// pas d'erreur 
						else
							maj_statut (erreur,para_trame->numero_esclave);
						break; // logique 

					case c_res_entier :
						erreur = nEchangeModbus (numero_canal,
														 (MODBUS_F)para_trame->numero_fonction,
														 para_trame->nbre_a_lire,
														 table_trans_jbus,
														 (DWORD)wNumeroEsclave,
														 (MODBUS_AUTOMATE)para_trame->tipe_esclave,
														 para_trame->adresse_automate_depart,
														 (DWORD)(FACTEUR_TIME_OUT_MODBUS * para_trame->fNbUnitesTimeOut),
														 para_trame->retry,
														 MODBUS_VAR_ENTIER);
						if (erreur == 0)
							{
							for (pos_donneex_jbus = pos_debut_donneex_jbus; (pos_donneex_jbus <= pos_fin_donneex_jbus); pos_donneex_jbus++)
								{
								donneex_jbus = (PX_MODBUS_E)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e,pos_donneex_jbus);
								cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, donneex_jbus->index_es_associe);
								(*cour_num) = (FLOAT) (table_trans_jbus->table_num_entier [donneex_jbus->i_tab_recep_significatif-1]);
								} // for 
							}// pas d'erreur 
						else
							maj_statut (erreur,para_trame->numero_esclave);
						break; // entier 

					case c_res_mots :
						erreur = nEchangeModbus (numero_canal,
														 (MODBUS_F)para_trame->numero_fonction,
														 para_trame->nbre_a_lire,
														 table_trans_jbus,
														 (DWORD)wNumeroEsclave,
														 (MODBUS_AUTOMATE)para_trame->tipe_esclave,
														 para_trame->adresse_automate_depart,
														 (DWORD)(FACTEUR_TIME_OUT_MODBUS * para_trame->fNbUnitesTimeOut),
														 para_trame->retry,
														 MODBUS_VAR_MOT);
						if (erreur == 0)
							{
							for (pos_donneex_jbus = pos_debut_donneex_jbus; (pos_donneex_jbus <= pos_fin_donneex_jbus); pos_donneex_jbus++)
								{
								donneex_jbus = (PX_MODBUS_E)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e,pos_donneex_jbus);
								cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, donneex_jbus->index_es_associe);
								(*cour_num) = (FLOAT)table_trans_jbus->table_num_mot [donneex_jbus->i_tab_recep_significatif-1];;
								} // for 
							}// pas d'erreur 
						else
							maj_statut (erreur,para_trame->numero_esclave);
						break; // entier 

					case c_res_reels :
						erreur = nEchangeModbus (numero_canal,
														 (MODBUS_F)para_trame->numero_fonction,
														 para_trame->nbre_a_lire,
														 table_trans_jbus,
														 (DWORD)wNumeroEsclave,
														 (MODBUS_AUTOMATE)para_trame->tipe_esclave,
														 para_trame->adresse_automate_depart,
														 (DWORD)(FACTEUR_TIME_OUT_MODBUS * para_trame->fNbUnitesTimeOut),
														 para_trame->retry,
														 MODBUS_VAR_REEL);
						if (erreur == 0)
							{
							for (pos_donneex_jbus = pos_debut_donneex_jbus; (pos_donneex_jbus <= pos_fin_donneex_jbus); pos_donneex_jbus++)
								{
								donneex_jbus = (PX_MODBUS_E)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e,pos_donneex_jbus);
								cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, donneex_jbus->index_es_associe);
								(*cour_num) = table_trans_jbus->table_num_reel [donneex_jbus->i_tab_recep_significatif-1];
								} // for 
							}// pas d'erreur 
						else
							maj_statut (erreur,para_trame->numero_esclave);
						break; // entier 

					case c_res_message :
						erreur = nEchangeModbus (numero_canal,
														 (MODBUS_F)para_trame->numero_fonction,
														 para_trame->nbre_a_lire,
														 table_trans_jbus,
														 (DWORD)wNumeroEsclave,
														 (MODBUS_AUTOMATE)para_trame->tipe_esclave,
														 para_trame->adresse_automate_depart,
														 (DWORD)(FACTEUR_TIME_OUT_MODBUS * para_trame->fNbUnitesTimeOut),
														 para_trame->retry,
														 MODBUS_VAR_MESSAGE);
						if (erreur == 0)
							{
							for (pos_donneex_jbus = pos_debut_donneex_jbus; (pos_donneex_jbus <= pos_fin_donneex_jbus); pos_donneex_jbus++)
								{
								donneex_jbus = (PX_MODBUS_E)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e,pos_donneex_jbus);
								index_l = 0 ;
								while ( (index_l < (int)(donneex_jbus->longueur)) &&
											(table_trans_jbus->table_car[donneex_jbus->i_tab_recep_significatif + index_l - 1] != (char)0))
									{
									tempo_mes[index_l] = table_trans_jbus->table_car[donneex_jbus->i_tab_recep_significatif + index_l - 1];
									index_l++;
									}
								StrSetLength (tempo_mes, index_l);
								cour_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, donneex_jbus->index_es_associe);
								StrCopy (cour_mes, tempo_mes);
								} // for 
							}// pas d'erreur 
						else
							maj_statut (erreur,para_trame->numero_esclave);
						break; // message 

					default:
						break;
					} // switch le_genre 
				} // va rafraichissement a 1 
			} // canal � 0
    } // for trame 
  }

// ---------------------------------------------------------------------------
static void emet_jbus (DWORD bx_jbus_s, DWORD le_genre)
  {
  PBUFFER_MODBUS	table_trans_jbus = (PBUFFER_MODBUS)pointe_enr (szVERIFSource, __LINE__, bx_table_transfert_jb, 1);
  DWORD nbr_emission = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_jbus_s);

  DWORD index_donneex_jbus;
  DWORD numero_esclave_cour;
  DWORD tipe_esclave_cour;
  DWORD adresse_automate_cour;
  FLOAT time_out_cour;
  DWORD retry_cour;
  DWORD numero_canal;
  DWORD erreur;
  FLOAT *mem_num;
  FLOAT *cour_num;
  PBOOL mem_log;
  PBOOL	cour_log;
  PSTR	mem_mes;
  PSTR	cour_mes;
  PX_MODBUS_S	donneex_jbus;

  for (int i_tab = 1; (i_tab < 8); i_tab++)
    {
    table_trans_jbus->table_log [i_tab] = FALSE;  // adaptation pour ecrbit CGEE
    }

	BOOL bForcageSorties = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_JBUS_FORCE_SORTIES);
  for (index_donneex_jbus = 1; (index_donneex_jbus <= nbr_emission); index_donneex_jbus++)
    {
    donneex_jbus = (PX_MODBUS_S)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s, index_donneex_jbus);
		if (donneex_jbus->CanalVarOuCste == ID_CHAMP_VAR)
			{
			numero_canal = dwCanalModbusOuvert (donneex_jbus->numero_canal); 
			}
		else
			{
			numero_canal = donneex_jbus->numero_canal;
			}
		//
		if (numero_canal != 0)
			{
			if (donneex_jbus->EsclVarOuCste==ID_CHAMP_VAR)
				{
				cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus->numero_esclave);
				numero_esclave_cour = (DWORD)(*cour_num);
				}
			else
				{
				numero_esclave_cour = donneex_jbus->numero_esclave;
				}
			tipe_esclave_cour = donneex_jbus->tipe_esclave;
			adresse_automate_cour = donneex_jbus->adresse_automate;
			time_out_cour = donneex_jbus->fNbUnitesTimeOut;
			retry_cour = donneex_jbus->retry;
			switch (le_genre)
				{
				case c_res_logique :
					{
					cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus->index_es_associe);
					mem_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log,donneex_jbus->index_es_associe);
					if (((*cour_log) != (*mem_log)) || bForcageSorties)
						{
						table_trans_jbus->table_log [0] = (*cour_log);
						erreur = nEchangeModbus (numero_canal, (MODBUS_F)donneex_jbus->numero_fonction,
							donneex_jbus->nbre_a_ecrire, table_trans_jbus, 
							(DWORD)numero_esclave_cour, (MODBUS_AUTOMATE)tipe_esclave_cour, adresse_automate_cour,
							(DWORD)(FACTEUR_TIME_OUT_MODBUS * time_out_cour), retry_cour, MODBUS_VAR_LOG);
					 if (erreur != 0)
							{
							maj_statut (erreur,numero_esclave_cour);
							}
						} // valeurs memorisees differentes 
					}
					break;

				case c_res_entier :
					{
					cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus->index_es_associe);
					mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,donneex_jbus->index_es_associe);
					if (((*cour_num) != (*mem_num)) || bForcageSorties)
						{
						if ( ((*cour_num) <= c_i16_max) && ((*cour_num) >= c_i16_min) )
							{ // pas overflow 
							table_trans_jbus->table_num_entier [0] = (int)(*cour_num);
							erreur = nEchangeModbus (numero_canal, (MODBUS_F)donneex_jbus->numero_fonction,
								donneex_jbus->nbre_a_ecrire, table_trans_jbus,
								(DWORD)numero_esclave_cour, (MODBUS_AUTOMATE)tipe_esclave_cour, adresse_automate_cour,
								(DWORD)(FACTEUR_TIME_OUT_MODBUS * time_out_cour), retry_cour, MODBUS_VAR_ENTIER);
						 if (erreur != 0)
								{
								maj_statut (erreur,numero_esclave_cour);
								}
							}// pas overflow 
						else
							{ // overflow 
							maj_statut (MODBUS_ERR_OVERFLOW_VALEUR, numero_esclave_cour);
							} // overflow 
						} // a emettre 
					}
					break; // entier 

				case c_res_mots :
					{
					cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus->index_es_associe);
					mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,donneex_jbus->index_es_associe);
					if (((*cour_num) != (*mem_num)) || bForcageSorties)
						{
						if (((*cour_num) <= (FLOAT)65535.0) && ((*cour_num) >= 0) )
							{ // pas overflow 
							table_trans_jbus->table_num_mot[0] = (WORD) (*cour_num); 
							erreur = nEchangeModbus (numero_canal, (MODBUS_F)donneex_jbus->numero_fonction,
								donneex_jbus->nbre_a_ecrire, table_trans_jbus, (DWORD)numero_esclave_cour,
								(MODBUS_AUTOMATE)tipe_esclave_cour, adresse_automate_cour, 
								(DWORD)(FACTEUR_TIME_OUT_MODBUS * time_out_cour), retry_cour, MODBUS_VAR_MOT);
						 if (erreur != 0)
								{
								maj_statut (erreur,numero_esclave_cour);
								}
							}// pas overflow 
						else
							{ // overflow 
							maj_statut (MODBUS_ERR_OVERFLOW_VALEUR, numero_esclave_cour);
							} // overflow 
						} // a emettre 
					}
					break; // mots 

				case c_res_reels :
					{
					cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus->index_es_associe);
					mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,donneex_jbus->index_es_associe);
					if (((*cour_num) != (*mem_num)) || bForcageSorties)
						{
						table_trans_jbus->table_num_reel [0] = (*cour_num);
						erreur = nEchangeModbus (numero_canal, (MODBUS_F)donneex_jbus->numero_fonction,
							donneex_jbus->nbre_a_ecrire, table_trans_jbus, (DWORD)numero_esclave_cour,
							(MODBUS_AUTOMATE)tipe_esclave_cour, adresse_automate_cour,
							(DWORD)(FACTEUR_TIME_OUT_MODBUS * time_out_cour), retry_cour, MODBUS_VAR_REEL);
					 if (erreur != 0)
							{
							maj_statut (erreur,numero_esclave_cour);
							}
						} // a emettre
					}
					break; // reels 

				case c_res_message :
					{
					cour_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes,donneex_jbus->index_es_associe);
					mem_mes  = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes,donneex_jbus->index_es_associe);
					if ((!bStrEgales (cour_mes, mem_mes)) || bForcageSorties)
						{
						DWORD i_tab = 0;
						for (i_tab = 0; (i_tab < (int)StrLength (cour_mes)); i_tab++)
							{
							table_trans_jbus->table_car [i_tab] = cour_mes[i_tab];
							}
						while (i_tab < (int)(donneex_jbus->longueur))
							{
							table_trans_jbus->table_car [i_tab] = '\0';
							i_tab++;
							}

						erreur = nEchangeModbus (numero_canal, (MODBUS_F)donneex_jbus->numero_fonction,
							donneex_jbus->nbre_a_ecrire, table_trans_jbus, (DWORD)numero_esclave_cour,
							(MODBUS_AUTOMATE)tipe_esclave_cour, adresse_automate_cour,
							(DWORD)(FACTEUR_TIME_OUT_MODBUS * time_out_cour), retry_cour, MODBUS_VAR_MESSAGE);
					 if (erreur != 0)
							{
							maj_statut (erreur,numero_esclave_cour);
							}
						} // a emettre 
					}
					break; // c_res_message 

				default :
					break;
				} // switch le_genre 
			} // canal != 0
    } // for 

  }




//--------------------------------------------------------------------------
//                      traitement des tableaux
//--------------------------------------------------------------------------
static DWORD GetLongueur  (PX_MODBUS_TAB	donneex_jbus_tab)
  {
  FLOAT *cour_num;
  DWORD wLongueur;


  if (donneex_jbus_tab->LongVarOuCste==ID_CHAMP_VAR)
    {
    cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus_tab->longueur);
    wLongueur = (DWORD)(*cour_num);
    }
  else
    {
    wLongueur =donneex_jbus_tab->longueur;
    }
  return(wLongueur);
  }

//--------------------------------------------------------------------------

static DWORD charge_drv_jbus_tab (PBUFFER_MODBUS table_trans_jbus,
	PX_MODBUS_TAB	donneex_jbus_tab, DWORD *wLongueur, DWORD tipe_data,DWORD sens_data)
  {
  FLOAT *cour_num;
  char *PszAdresseEsclave;
  DWORD wNumeroEsclave;
  DWORD wNumeroCanal;
  DWORD wErreur;
  DWORD wAdresseAutomate;
  DWORD sens_sur_auto;

  wErreur = 0;
	(*wLongueur) = 0;
  if (donneex_jbus_tab->CanalVarOuCste == ID_CHAMP_VAR)
    {
    wNumeroCanal = dwCanalModbusOuvert (donneex_jbus_tab->numero_canal); 
    }
  else
    {
    wNumeroCanal = donneex_jbus_tab->numero_canal;
    }
  //
	if (wNumeroCanal != 0)
		{
		if (donneex_jbus_tab->EsclVarOuCste==ID_CHAMP_VAR)
			{
			cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus_tab->numero_esclave);
			wNumeroEsclave = (DWORD)(*cour_num);
			}
		else
			{
			wNumeroEsclave = donneex_jbus_tab->numero_esclave;
			}
	  //
		if (donneex_jbus_tab->AdrVarOuCste==ID_CHAMP_VAR)
			{
			PszAdresseEsclave = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes,donneex_jbus_tab->PosVarAdresse);
			wErreur = dwConversionAdresseModbus ((MODBUS_AUTOMATE)(donneex_jbus_tab->espece_d_auto),
				sens_data,tipe_data, PszAdresseEsclave, &wAdresseAutomate, &sens_sur_auto);
			if (wErreur == 0)
				{
				switch (sens_data)
					{
					case c_res_e :
						if (tipe_data == c_res_logique)
							{
							if (sens_sur_auto == c_res_s)
								{
								 donneex_jbus_tab->numero_fonction = 1;
								}
							else
								{
								donneex_jbus_tab->numero_fonction = 2;
								}
							}
						else
							{
							if (sens_sur_auto == c_res_s)
								{
								 donneex_jbus_tab->numero_fonction = 3;
								}
							else
								{
								donneex_jbus_tab->numero_fonction = 4;
								}
							}
					break;

					case c_res_s :
						if (tipe_data == c_res_logique)
							{
							donneex_jbus_tab->numero_fonction = 15;
							}
						else
							{
							donneex_jbus_tab->numero_fonction = 16;
							}
					break;

					default:
					break;

					}
				}
			}
		else
			{
			wAdresseAutomate =donneex_jbus_tab->adresse_automate;
			}
	//
		if (donneex_jbus_tab->LongVarOuCste==ID_CHAMP_VAR)
			{
			cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus_tab->longueur);
			*wLongueur = (DWORD)(*cour_num);
			switch (tipe_data)
				{
				case c_res_entier:
				case c_res_mots :
				if (*wLongueur > 128)
						{
						wErreur = 128;
						}
					break;
				case c_res_reels :
				if (*wLongueur > 64)
						{
						wErreur = 128;
						}
					break;
				case c_res_logique :
				if (*wLongueur > 1024)
						{
						wErreur = 128;
						}
					break;
				default:
						wErreur = 128;
					break;
				}
			if (*wLongueur == 0)
				{
				wErreur = 128;
				}
			}
		else
			{
			*wLongueur =donneex_jbus_tab->longueur;
			}
		if (wErreur==0)
			{
			wErreur = nEchangeModbus (wNumeroCanal,
											 (MODBUS_F)donneex_jbus_tab->numero_fonction,
											 (DWORD)(*wLongueur),
											 table_trans_jbus,
											 (DWORD)wNumeroEsclave,
											 (MODBUS_AUTOMATE)donneex_jbus_tab->tipe_esclave,
											 wAdresseAutomate,
											 (DWORD)(FACTEUR_TIME_OUT_MODBUS * donneex_jbus_tab->fNbUnitesTimeOut),
											 donneex_jbus_tab->retry,
											 nModbusVarFromPcsVar(tipe_data));
			}
		}
	else // canal � 0
		{
		}
  return(wErreur);
  }


//----------------------------------------------------------------------------------------------
static void recoit_jbus_tab (void)
  {
  PBUFFER_MODBUS table_trans_jbus;
  BOOL *cour_log;
  FLOAT *cour_num;
  PX_MODBUS_TAB	donneex_jbus_tab;
  DWORD index_tab;
  DWORD pos_donneex_jbus_tab;
  DWORD erreur;
  DWORD wLongueur;

  table_trans_jbus = (PBUFFER_MODBUS)pointe_enr (szVERIFSource, __LINE__, bx_table_transfert_jb, 1);
  if (existe_repere (bx_jbus_e_log_tab) )     // tableau de logiques
    {
    for (index_tab = 1; (index_tab <= nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_log_tab)); index_tab++)
      {
      donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e_log_tab, index_tab);
      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->i_cmd_rafraich);
      if (*cour_log)
        {
        erreur = charge_drv_jbus_tab (table_trans_jbus,donneex_jbus_tab,&wLongueur,c_res_logique,c_res_e);
        if (erreur == 0)
          {
          for (pos_donneex_jbus_tab = 1;  (pos_donneex_jbus_tab <= wLongueur); pos_donneex_jbus_tab++)
            {
            cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, donneex_jbus_tab->index_es_associe + pos_donneex_jbus_tab - 1);
            (*cour_log) = table_trans_jbus->table_log [pos_donneex_jbus_tab-1];
            } // for 
          }// pas d'erreur 
        else
          {
          maj_statut (erreur, donneex_jbus_tab->numero_esclave);
          }
        }// cour_log 
      } // for index_tab 
    } // existe bx_jbus_e_log_tab 

  if (existe_repere (bx_jbus_e_num_entier_tab) )     // tableau d'entiers
    {
    for (index_tab = 1; (index_tab <= nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_entier_tab)); index_tab++)
      {
      donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e_num_entier_tab, index_tab);
      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->i_cmd_rafraich);
      if (*cour_log)
        {
        erreur = charge_drv_jbus_tab (table_trans_jbus,donneex_jbus_tab,&wLongueur,c_res_entier,c_res_e);
        if (erreur == 0)
          {
          for (pos_donneex_jbus_tab = 1;  (pos_donneex_jbus_tab <= wLongueur); pos_donneex_jbus_tab++)
            {
            cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, donneex_jbus_tab->index_es_associe + pos_donneex_jbus_tab - 1);
            (*cour_num) = (FLOAT) (table_trans_jbus->table_num_entier [pos_donneex_jbus_tab-1]);
            }
          }// pas d'erreur 
        else
          {
          maj_statut (erreur, donneex_jbus_tab->numero_esclave);
          }
        }// (*cour_log) 
      } // index_tab <= nb_enr 
    } // existe bx_jbus_e_num_entier_tab 

  if (existe_repere (bx_jbus_e_num_mot_tab))     // tableau de mots
    {
    for (index_tab = 1; (index_tab <= nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_mot_tab)); index_tab++)
      {
      donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e_num_mot_tab, index_tab);
      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->i_cmd_rafraich);
      if (*cour_log)
        {
        erreur = charge_drv_jbus_tab (table_trans_jbus,donneex_jbus_tab,&wLongueur,c_res_mots,c_res_e);
        if (erreur == 0)
          {
          for (pos_donneex_jbus_tab = 1; (pos_donneex_jbus_tab <= wLongueur); pos_donneex_jbus_tab++)
            {
            cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, donneex_jbus_tab->index_es_associe + pos_donneex_jbus_tab - 1);
            (*cour_num) = (FLOAT) (table_trans_jbus->table_num_mot [pos_donneex_jbus_tab-1]);
            }
          }// pas d'erreur 
        else
          {
          maj_statut (erreur, donneex_jbus_tab->numero_esclave);
          }
        }// (*cour_log) 
      } // index_tab <= nb_enr 
    } // existe bx_jbus_e_num_mot_tab 

  if (existe_repere (bx_jbus_e_num_reel_tab) )     // tableau de reels
    {
    for (index_tab = 1; (index_tab <= nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_reel_tab)); index_tab++)
      {
      donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e_num_reel_tab, index_tab);
      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->i_cmd_rafraich);
      if (*cour_log)
        {
        erreur = charge_drv_jbus_tab (table_trans_jbus,donneex_jbus_tab,&wLongueur,c_res_reels,c_res_e);
        if (erreur == 0)
          {
          for (pos_donneex_jbus_tab = 1; (pos_donneex_jbus_tab <= wLongueur); pos_donneex_jbus_tab++)
            {
            cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, donneex_jbus_tab->index_es_associe + pos_donneex_jbus_tab - 1);
            (*cour_num) = table_trans_jbus->table_num_reel [pos_donneex_jbus_tab-1];
            }
          }// pas d'erreur 
        else
          {
          maj_statut (erreur, donneex_jbus_tab->numero_esclave);
          }
        }// (*cour_log) 
      } // index_tab <= nb_enr 
    } // existe bx_jbus_e_num_reel_tab 

  } // recoit_jbus_tab 

//--------------------------------------------------------------------------
static void emet_jbus_tab (void)
  {
  PBUFFER_MODBUS table_trans_jbus;
  BOOL *mem_log;
  BOOL *cour_log;
  FLOAT *mem_num;
  PX_MODBUS_TAB	donneex_jbus_tab;
  DWORD index_tab;
  DWORD pos_donneex_jbus_tab;
  DWORD modulo8;
  DWORD erreur;
  int i;
  DWORD wLongueur;


  table_trans_jbus = (PBUFFER_MODBUS)pointe_enr (szVERIFSource, __LINE__, bx_table_transfert_jb, 1);
  for (i = 1; (i < 8 ); i++)
    {
    table_trans_jbus->table_log [i] = FALSE;  // adaptation pour ecrbit CGEE
    }
  if (existe_repere (bx_jbus_s_log_tab))     // tableau de logiques
    {
    for (index_tab = 1; (index_tab <= nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_log_tab)); index_tab++)
      {
      donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_log_tab, index_tab);
      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->i_cmd_rafraich);
      if (*cour_log)
        {
        (*cour_log) = FALSE;
        wLongueur = GetLongueur  (donneex_jbus_tab);
        for (pos_donneex_jbus_tab = 1; (pos_donneex_jbus_tab <= wLongueur); pos_donneex_jbus_tab++)
          {
          mem_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->index_es_associe + pos_donneex_jbus_tab - 1);
          table_trans_jbus->table_log [pos_donneex_jbus_tab-1] = (*mem_log);
          }
        pos_donneex_jbus_tab = wLongueur; // c'est plus s�r ////
        modulo8 = pos_donneex_jbus_tab % 8;
        if (modulo8 != 0 )
          {
          for (i = pos_donneex_jbus_tab; (i  < (int)(pos_donneex_jbus_tab + (8 - modulo8) )); i++)
            {
            table_trans_jbus->table_log [i] = FALSE;
            }
          }
        erreur = charge_drv_jbus_tab (table_trans_jbus,donneex_jbus_tab,&wLongueur,c_res_logique,c_res_s);
        if (erreur != 0)
          {
          maj_statut (erreur,donneex_jbus_tab->numero_esclave);
          }
        }
      }
    }

  if (existe_repere (bx_jbus_s_num_entier_tab))     // tableau d'entiers
    {
    for (index_tab = 1; (index_tab <= nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_entier_tab)); index_tab++)
      {
      donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_entier_tab, index_tab);
      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->i_cmd_rafraich);
      if (*cour_log)
        {
        (*cour_log) = FALSE;
        wLongueur = GetLongueur  (donneex_jbus_tab);
        for (pos_donneex_jbus_tab = 1; (pos_donneex_jbus_tab <= wLongueur); pos_donneex_jbus_tab++)
          {
          mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus_tab->index_es_associe + pos_donneex_jbus_tab - 1);
          if ( ((*mem_num) <= c_i16_max) && ( (*mem_num) >= c_i16_min))
            { // pas overflow 
            table_trans_jbus->table_num_entier [pos_donneex_jbus_tab-1] = (int) (*mem_num);
            }
          else
            {  // overflow 
            maj_statut (MODBUS_ERR_OVERFLOW_VALEUR,donneex_jbus_tab->numero_esclave);
            }
          }
        erreur = charge_drv_jbus_tab (table_trans_jbus,donneex_jbus_tab,&wLongueur,c_res_entier,c_res_s);
        if (erreur != 0)
          {
          maj_statut (erreur, donneex_jbus_tab->numero_esclave);
          }
        }  // *cour_log 
      } // index_tab <= nb_enr 
    } // existe bx_jbus_s_num_entier_tab 

  if (existe_repere (bx_jbus_s_num_mot_tab) )     // tableau de mots
    {
    for (index_tab = 1; (index_tab <= nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_mot_tab)); index_tab++)
      {
      donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_mot_tab,index_tab);
      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->i_cmd_rafraich);
      if (*cour_log)
        {
        (*cour_log) = FALSE;
        wLongueur = GetLongueur  (donneex_jbus_tab);
        for (pos_donneex_jbus_tab = 1; (pos_donneex_jbus_tab <= wLongueur); pos_donneex_jbus_tab++)
          {
          mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus_tab->index_es_associe + pos_donneex_jbus_tab - 1);
          if (((*mem_num) <= (FLOAT)65535.0) && ((*mem_num) >= 0))
            { // pas overflow 
            table_trans_jbus->table_num_mot [pos_donneex_jbus_tab-1] = (WORD) (*mem_num);
            }
          else
            {  // overflow 
            maj_statut (MODBUS_ERR_OVERFLOW_VALEUR, donneex_jbus_tab->numero_esclave);
            }
          }
        erreur = charge_drv_jbus_tab (table_trans_jbus,donneex_jbus_tab,&wLongueur,c_res_mots,c_res_s);
       if (erreur != 0)
          {
          maj_statut (erreur,donneex_jbus_tab->numero_esclave);
          }
        }  // *cour_log 
      } // index_tab <= nb_enr 
    } // existe bx_jbus_s_num_mot_tab 

  if (existe_repere (bx_jbus_s_num_reel_tab) )     // tableau de reels
    {
    for (index_tab = 1; (index_tab <= nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_reel_tab)); index_tab++)
      {
      donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_reel_tab,index_tab);
      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,donneex_jbus_tab->i_cmd_rafraich);
      if (*cour_log)
        {
        (*cour_log) = FALSE;
        wLongueur = GetLongueur  (donneex_jbus_tab);
        for (pos_donneex_jbus_tab = 1; (pos_donneex_jbus_tab <= wLongueur); pos_donneex_jbus_tab++)
          {
          mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,donneex_jbus_tab->index_es_associe + pos_donneex_jbus_tab - 1);
          table_trans_jbus->table_num_reel [pos_donneex_jbus_tab-1] = (*mem_num);
          }
        erreur = charge_drv_jbus_tab (table_trans_jbus,donneex_jbus_tab,&wLongueur,c_res_reels,c_res_s);
       if (erreur != 0)
          {
          maj_statut (erreur,donneex_jbus_tab->numero_esclave);
          }
        }  // *cour_log 
      } // index_tab <= nb_enr 
    } // existe bx_jbus_s_num_reel_tab 

  } // emet_jbus_tab 


// ---------------------------------------------------------------------------
void  recoit_jb (BOOL bPremier)
  {
  recoit_jbus (bx_para_jbus_log       , bx_jbus_e_log       , c_res_logique);
  recoit_jbus (bx_para_jbus_num_entier, bx_jbus_e_num_entier, c_res_entier );
  recoit_jbus (bx_para_jbus_num_mot   , bx_jbus_e_num_mot   , c_res_mots   );
  recoit_jbus (bx_para_jbus_num_reel  , bx_jbus_e_num_reel  , c_res_reels  );
  recoit_jbus (bx_para_jbus_mes       , bx_jbus_e_mes       , c_res_message);
  recoit_jbus_tab ();
  }

// ---------------------------------------------------------------------------
void  emet_jb   (BOOL bPremier)
  {
  emet_jbus (bx_jbus_s_log       , c_res_logique);
  emet_jbus (bx_jbus_s_num_entier, c_res_entier);
  emet_jbus (bx_jbus_s_num_mot   , c_res_mots);
  emet_jbus (bx_jbus_s_num_reel  , c_res_reels);
  emet_jbus (bx_jbus_s_mes       , c_res_message);
  emet_jbus_tab ();
	// Remise � bas de la variable de for�age des sorties
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_JBUS_FORCE_SORTIES, FALSE);
  }

//----------------------------------------------------------------------
void  initialise_jbus (void)
  {
  PX_VAR_SYS	position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, VA_SYS_STATUS_JBUS); //$$ Mise � jour statut bidon
  PFLOAT ptr_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (position->dwPosCourEx));
  DWORD i;
  DWORD nb_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_para_jbus_com);

  for (i = 1; (i <= nb_enr); i++)
    {
		t_parametre_com*para_com = (t_parametre_com*)pointe_enr (szVERIFSource, __LINE__, bx_para_jbus_com, i);

		if ((para_com->CanalVarOuCste == ID_CHAMP_CTE) && (para_com->numero_canal != 0)) // ouverture uniquement pour les canaux constants et <> 0
			{
			(*ptr_num) = (FLOAT)nOuvertureVoieModbus (para_com->numero_canal, para_com->vitesse, para_com->nb_de_bits,para_com->parite,para_com->encadrement);
			}
    } // for
  if (!existe_repere (bx_table_transfert_jb))
     {
     cree_bloc (bx_table_transfert_jb, sizeof (BUFFER_MODBUS), 1);
     }
   }

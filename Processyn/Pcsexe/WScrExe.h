
// --------------------------------------------------------------------------

// ------------------------------------
#define TOUCHE_F1           0
#define TOUCHE_F2           1
#define TOUCHE_F3           2
#define TOUCHE_F4           3
#define TOUCHE_F5           4
#define TOUCHE_F6           5
#define TOUCHE_F7           6
#define TOUCHE_F8           7
#define TOUCHE_F9           8
#define TOUCHE_F10          9
#define TOUCHE_SHIFT_F1    10
#define TOUCHE_SHIFT_F2    11
#define TOUCHE_SHIFT_F3    12
#define TOUCHE_SHIFT_F4    13
#define TOUCHE_SHIFT_F5    14
#define TOUCHE_SHIFT_F6    15
#define TOUCHE_SHIFT_F7    16
#define TOUCHE_SHIFT_F8    17
#define TOUCHE_SHIFT_F9    18
#define TOUCHE_SHIFT_F10   19

#define TOUCHE_RETURN      20

#define TOUCHE_LEFT        21
#define TOUCHE_RIGHT       22
#define TOUCHE_UP          23
#define TOUCHE_DOWN        24

#define TOUCHE_PAGE_UP     25
#define TOUCHE_PAGE_DOWN   26

#define NB_TOUCHES         27


// ---------------------------------------------------------------
// cr�ation de la fen�tre fille ScrExe
BOOL bScrCreeFenetre (HWND hwndParent, UINT uId);

// ---------------------------------------------------------------
void ScrAcces					(void);
void ScrFinAcces			(void);
void ScrClear         (void);
void ScrGotoLgnCol    (INT  iLgn, INT  iCol);
void ScrGetLgnCol     (INT * pnLigne, INT *pnColonne);
//void ScrWrite       (CHAR *pszString);
void ScrWriteLn       (CHAR *pszString);
void ScrRefresh       (void);
BOOL bScrRAZToucheAppuyee (INT iKey);

// Prise en compte de l'appui d'une touche (selon la nomenclature ci dessus)
void ScrAppuiTouche   (DWORD dwCodePcsTouche);

BOOL ScrOnChar        (WPARAM mp1);

// message WM_KEYDOWN : d�tecte et renvoie une touche g�r�e par les applications PCSEXE
BOOL bScrOnKeyDownTouchePcs
	(
	WPARAM mp1,
	DWORD * pdwCodeTouchePcs	// Code mis � jour s'il s'agit d'une touche g�r�e par PCS
	); // Renvoie TRUE s'il s'agit d'une touche g�r�e par PCS

void ScrReSize        (void);

// --------------------------------------------------------------------------
void ScrQhelpOpen			(BOOL ouverture);
void ScrQhelpLoadTxt	(char *pszNomFic, BOOL bUtilisateur);
void ScrQhelpSetIdx		(char *pszNomFic, char *pszTitre);

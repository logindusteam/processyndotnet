// Modbus.h
// Gestion du protocole Modbus WIN32 27/3/96
#ifndef MODBUS_H
#define MODBUS_H

// types d'automates
typedef enum
	{
	MODBUS_AUTOMATE_SMC       = 0,
	MODBUS_AUTOMATE_PB        = 1,
	MODBUS_AUTOMATE_TELE      = 2,
	MODBUS_AUTOMATE_GOULD     = 3,
	MODBUS_AUTOMATE_SIEMENS   = 4,
	MODBUS_AUTOMATE_ALSPA     = 5,
	MODBUS_AUTOMATE_F15_F16   = 6,
	MODBUS_AUTOMATE_SMCX00    = 7
	} MODBUS_AUTOMATE;

// fonctions Modbus
typedef enum
	{
	MODBUS_F_LECTURE_BITS_SORTIE  = 1,
	MODBUS_F_LECTURE_BITS_ENTREE  = 2,
	MODBUS_F_LECTURE_MOTS_SORTIE  = 3,
	MODBUS_F_LECTURE_MOTS_ENTREE  = 4,
	MODBUS_F_ECRITURE_BIT					= 5,
	MODBUS_F_ECRITURE_MOT					= 6,
	MODBUS_F_ECRITURE_N_BITS			= 15,
	MODBUS_F_ECRITURE_N_MOTS			= 16
	} MODBUS_F;

// erreurs EchangeModbus
typedef enum
	{
	MODBUS_ERR_OK										= 0,
	// Les bits 0 1 2 contiennent les bits de Com
	MODBUS_ERR_AUTOMATE							= 0x0008, // Dans ce cas les bits 8 � 13 contiennent les erreurs protocoles
	MODBUS_ERR_TIME_OUT							= 0x0010,
	MODBUS_ERR_RECEPTION						= 0x0020,
	MODBUS_ERR_CRC									= 0x0040,
	MODBUS_ERR_N_FONCTION						= 0x0080,
	MODBUS_ERR_AUTO_FONCTION_INCONNUE			= 0x0100, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_ADRESSE_INCORRECTE		= 0x0200, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_DONNEE_INCORRECTE			= 0x0300, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_NON_PRET							= 0x0400, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_GOULD_ACK							= 0x0500, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_GOULD_SIEMENS_OCCUPE	= 0x0600, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_GOULD_NACK						= 0x0700, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_DEFAUT_ECRITURE				= 0x0800, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_APRIL_CHEVAUCHEMENT		= 0x0900, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_APRIL_ENTETE					= 0x0A00, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_APRIL_ESCLAVE_ABSENT	= 0x0B00, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_APRIL_CRC							= 0x0C00, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_AUTO_APRIL_EMISSION_BLOQUEE = 0x0D00, // Dans ce cas les bits 8 � 14 contiennent les erreurs protocoles
	MODBUS_ERR_OVERFLOW_VALEUR			= 0x4000,
	MODBUS_ERR_EMISSION							= 0x8000, // not�e erreur canal dans mode op
	MODBUS_ERR_OUVERTURE						= 0xFFFF, 
	MODBUS_ERR_FERMETURE						= 0xFFFF // $$ cochon
	} MODBUS_ERR;

// Buffer de variables � �changer avec l'automate
// Le module  effectue la conversion entre le type des tables et la trame �mise
typedef union
  {
  BOOL	table_log [2048];					// BOOL  = bit MODBUS
  int		table_num_entier [128];		// int   = mot MODBUS 
  DWORD table_num_mot [128];			// DWORD = mot MODBUS
  FLOAT table_num_reel [64];			// REEL  = 2 mots MODBUS 
  char	table_car[256];						// 1 caract�re = 1 octet MODBUS
  } BUFFER_MODBUS, *PBUFFER_MODBUS;

// Types de variables � �changer avec l'automate
typedef enum
	{
	MODBUS_VAR_LOG,
	MODBUS_VAR_ENTIER,
	MODBUS_VAR_REEL,
	MODBUS_VAR_MOT,
	MODBUS_VAR_MESSAGE
	} MODBUS_VAR;

//-----------------------------------------------------------------
// Ouverture voie de communication pour dialogue modbus
MODBUS_ERR nOuvertureVoieModbus (
	UINT uCanal,					// num�ro de COM de 1 � NB_MAX_VOIE
	UINT vitesse,					// en Bauds (ex: 9600)
	UINT nb_de_bits,			// 5, 6, 7 ou 8 (selon les capacit�s du driver)
	UINT parite,					// 0 = sans, 1 = impaire, 2 = paire
	UINT encadrement			// 1 = 1 stop bit, 2 = 2 stop bit, autre = erreur
	); // Renvoie le code d'erreur (test� si != 0 dans Pcs)

//-----------------------------------------------------------------
// Fermeture voie de communication pour dialogue modbus
MODBUS_ERR nFermetureVoieModbus (
	UINT uCanal					// num�ro de COM de 1 � NB_MAX_VOIE
	);

//-------------------------------------------------------------------------
// Question - r�ponse Modbus �l�mentaire sur un canal ouvert
// Le poste est maitre et l'automate est esclave
MODBUS_ERR nEchangeModbus 
	(
	DWORD canal,									// canal ouvert
	MODBUS_F fonction,						// Fonction MODBUS � faire ex�cuter par l'automate
	DWORD nombre,									// Nombre de variables � �changer(bit, mot, r�el ou nombre de (caractere/2))
	PBUFFER_MODBUS	table_modbus,	// Adresse de la table des variables � �changer
	DWORD esclave,								// Num�ro d'esclave de l'automate cible
	MODBUS_AUTOMATE tipe_esclave,	// Type de comportement de l'automate cible
	DWORD adresse,								// Adresse MODBUS de la premi�re variable adress�e
	DWORD time_out,								// Dur�e du time out en millisecondes
	DWORD retry,									// Nombres d'essais de transmissions en cas d'erreur
	MODBUS_VAR genre_var					// Type de donn�es �chang�es
	);

#endif


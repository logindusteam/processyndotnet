//-------------------------------------------------------
// ExeDde.h
// ex�cution Pcs : Gestion DDE
// JS Win32 11/06/97
//-------------------------------------------------------

// --------------------------------------------------------------------------
// R�cup�re le nom d'une variable serveur ou cliente non pr�d�finie
BOOL bNomVarDdeDyn (DWORD index, PSTR pszNomVar, BOOL bServeur);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//								Interface DDE
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#define DDE_ERR_NONE                 0
#define DDE_ERR_PM_INIT              1
#define DDE_ERR_CREATE_THREAD        2
#define DDE_ERR_NO_DATA              3
#define DDE_ERR_NO_MEM               4
#define DDE_ERR_CREATE_LINK          5
#define DDE_ERR_WIN_CREATE           6
#define DDE_ERR_NO_SERVER            7
#define DDE_ERR_POST_MSG             8
#define DDE_ERR_NOT_INDET_STATE      9
#define DDE_ERR_SEG_ALLOC           10
#define DDE_ERR_USR_NOT_ADV         11
#define DDE_ERR_NO_LINK             12
#define DDE_ERR_TIME_OUT            13
#define DDE_ERR_NO_ITEM             14

//--------- longueur max trame dde, applics, topics et items
//
#define DDE_MAX_BLOC_SIZE          256
#define DDE_MAX_NAME_SIZE          256
#define DDE_MAX_ITEM_SIZE           80

// type fonction callback, pour notifier la fin d'un lien
typedef void (*t_func_terminate)(LPARAM lParam);

// type fonction callback, pour notifier l'arrivee d'une donnee
typedef void (*t_func_notif)(LPARAM lParam, DWORD wEvenement, void *Data, DWORD wSizeData);

// evenement possible pour le parametre evenement de la fonction callback
#define DDE_DATA_RECU 1 // recu valeur avec WM_DDE_DATA

// creation d'un lien DDE client avec un serveur externe
UINT uDDEOuvreConversationClient (PSTR pszApplic, PSTR pszTopic, UINT wNbItems,
	t_func_terminate FctTerminate, LPARAM lParam, HCONV * phConv);

// destruction d'un lien DDE
UINT uDDEFermeConversationClient (HCONV * phConv);

//----------------------------------------------------------
// recuperer un item en lecture : WM_DDE_REQUEST
// initialiser une transaction en lecture : WM_DDE_ADVISE
// on precise le timeout d'attente d'acquittement du serveur
// on peut initialiser la zone data de reception des donnees
// et (ou) specifier une fct de retour
//
DWORD uDdeClientAdviseItem (PCSTR pszItem, UINT wNumItem,	void *pData, UINT wSizeData,
	t_func_notif FctNotif, LPARAM lParam, HCONV hConv, DWORD dwTimeOut);

// terminer toutes les transactions advise d'un lien en meme temps
DWORD DdeUnadviseLink (HCONV hConv, DWORD dwTimeOut);

// lire la valeur d'un item dans un lien : WM_DDE_REQUEST
DWORD uDdeClientRequestItem (PSTR pszItem, UINT wNumItem,
	t_func_notif FctNotif, LPARAM lParam, HCONV hConv, DWORD dwTimeOut);

// lire la valeur d'un item dans un lien
DWORD uDdeClientGetItemValeur (HCONV hConv, UINT wNumItem, void *pData, UINT *wSizeData);

// ecrire la valeur d'un item dans un lien : WM_DDE_POKE
DWORD uDdeClientPokeItem (char *pszItem, UINT wNumItem, void *pData, UINT wSizeData,
	HCONV hConv, DWORD dwTimeOut);

// renvoie le statut d'un lien donn�
//$$ obsolete ? DWORD DdeGetStatus (HCONV hConv, UINT wNumItem, DWORD *wStatusLien, DWORD *wStatusItem);


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//					PARTIE SERVEUR
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// initialisation serveur DDE
BOOL bDdeExeInitServeur (PCSTR pszApplic, PCSTR pszTopic);

// envoyer une valeur qui a chang� � un client : WM_DDE_DATA
//DWORD DdeDataItem (void *handleClient, void *handleServeur, char *pszItem,
//  void *pData, UINT wSizeData);


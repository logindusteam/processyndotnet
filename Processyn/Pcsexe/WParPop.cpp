// -------------------------------------------------------------------------
// WParPop.c
// Fen�tre parente de Pop ups
// -------------------------------------------------------------------------
#include "stdafx.h"
#include "UEnv.h"
#include "Appli.h"
#include "UListeH.h"
#include "Verif.h"
#include "WParPop.h"
VerifInit;

static PCSTR szClasseParentPopUp = "ClasseParentPopUp";

// Messages envoy� par un pop up au parent (obligatoires si non POPUP_INDEPENDANT)
#define WM_ONPOPUP_CREATE (WM_USER + 4)
#define WM_ONPOPUP_DESTROY (WM_USER + 5)
#define WM_ONPOPUP_ACTIVATE (WM_USER + 6)
#define WM_POPUP_CHANGE_MODALITE (WM_USER + 7)

typedef struct
	{
	HWND			hwndModaleActive;
	HLISTE_H	hListehwndPopUps;
	UINT      uiIdTimer;
	BOOL      bEtatTimer;
	} ENV, *PENV;


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//	Diffusion de message aux fenetres PopUP
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
static void DiffusionEvtTimerPopUp (PENV pEnv)
	{
	DWORD dwNPage;
	DWORD	dwNbHandles = dwNbHDansListeH (pEnv->hListehwndPopUps);

	for (dwNPage = 0; dwNPage < dwNbHandles; dwNPage++)
		{
		HWND hwndTemp = (HWND)hDansListeH (pEnv->hListehwndPopUps, dwNPage);
		if (hwndTemp)
			{
			::SendMessage (hwndTemp, WM_TIMER_PARPOP, (WPARAM)pEnv->bEtatTimer, 0);
			}
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//	Gestion de "modalit�" entre les fen�tres pop ups
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// -----------------------------------------------------------------------
// Interroge une fen�tre sur sa modalit�
static MODALITE_POPUP modalitePopUp (HWND hwnd)
	{
	MODALITE_POPUP	nRes = POPUP_INDEPENDANT;

	if (hwnd)
		{
		nRes = (MODALITE_POPUP)::SendMessage (hwnd, WM_GET_MODALITE_POPUP, 0, 0);
		if ((nRes != POPUP_MODAL) && (nRes != POPUP_NON_MODAL))
			nRes = POPUP_INDEPENDANT;
		}
		
	return nRes;
	}

//-----------------------------------------------------
// Change l'�tat des pop ups pour refl�ter la pr�sence du pop up modal
// $$  v�rifier modalit� et parent ?
static void RendFenetreModale (PENV pEnv, HWND hwnd)
	{
	DWORD dwNPage;
	DWORD	dwNbHandles = dwNbHDansListeH (pEnv->hListehwndPopUps);

	// Note la nouvelle fen�tre modale active
	pEnv->hwndModaleActive = hwnd;

	// Active la fen�tre d�sign�e
	::EnableWindow (hwnd, TRUE);
	SetForegroundWindow (hwnd);

	// D�sactive les autres pages non modales
	for (dwNPage = 0; dwNPage < dwNbHandles; dwNPage++)
		{
		HWND hwndTemp = (HWND)hDansListeH (pEnv->hListehwndPopUps, dwNPage);

		// Autre page non marqu�e modale ?
		if (hwndTemp && (hwndTemp != hwnd) && (modalitePopUp (hwndTemp) == POPUP_NON_MODAL))
			// oui => inhibe la
			::EnableWindow (hwndTemp, FALSE);
		}
	} // RendFenetreModale

//----------------------------------------------------------------------
// Modifie les attributs des fen�tres compte tenu de la fin de modalit�
// de la fen�tre sp�cifi�e
// => appel� uniquement par les fonctions de gestion modale
static void FinModaliteFenetre (PENV pEnv, HWND hwnd)
	{
	// Mode modal ?
	if (pEnv->hwndModaleActive)
		{
		// oui => Parcours les pages
		DWORD	dwNPage;
		DWORD	dwNbHandles = dwNbHDansListeH (pEnv->hListehwndPopUps);
		HWND	hwndAutreFenetreModale = NULL;

		// Recherche d'une autre fen�tre marqu�e modale
		for (dwNPage = 0; dwNPage < dwNbHandles; dwNPage++)
			{
			HWND hwndTemp = (HWND)hDansListeH (pEnv->hListehwndPopUps, dwNPage);

			// Regarde les autres fen�tres marqu�es modales
			if (hwndTemp && (hwndTemp != hwnd) && modalitePopUp (hwndTemp) == POPUP_MODAL)
				{
				hwndAutreFenetreModale = hwndTemp;
				break;
				}
			}

		// Reste t'il une autre fen�tre modale ?
		if (hwndAutreFenetreModale)
			{
			// oui => Rends la modale
			RendFenetreModale (pEnv, hwndAutreFenetreModale);
			}
		else
			{
			// mise � jour du nombre de pop ups
			dwNbHandles = dwNbHDansListeH (pEnv->hListehwndPopUps);

			// non => r�active les autres pages valides
			for (dwNPage = 0; dwNPage < dwNbHandles; dwNPage++)
				{
				HWND hwndTemp = (HWND)hDansListeH (pEnv->hListehwndPopUps, dwNPage);

				// R�active les autres pages valides
				if (hwndTemp && (hwndTemp != hwnd) && (modalitePopUp (hwndTemp) == POPUP_NON_MODAL))
					::EnableWindow (hwndTemp, TRUE);
				}
			// Plus de page modale
			pEnv->hwndModaleActive = NULL;

			// R�active le parent pour r�activer les fen�tres valid�es
			::BringWindowToTop (::GetParent(hwnd));
			}
		}
	} // FinModaliteFenetre


// -----------------------------------------------------------------------
// Fen�tre de l'appli
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocParentPopUp (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
  {
  LRESULT    mres = 0;                      // Valeur de retour
  BOOL       bTraite = TRUE;                // Indique commande non trait�e

  switch (msg)
    {
		case WM_CREATE:
			{
			PENV pEnv = (PENV)pCreeEnv (hwnd, sizeof(ENV));

			pEnv->hwndModaleActive = NULL;
			pEnv->uiIdTimer = 0;
			pEnv->bEtatTimer = FALSE;
			// Init liste pop up: pas encore de pop up, HWND invalide, Pas d'acc�s multi thread
			VerifWarning (bCreeListeH (&pEnv->hListehwndPopUps, NULL, FALSE));
			}
			break;

		case WM_DESTROY:
			{
			PENV pEnv = (PENV)pGetEnv (hwnd);

			if (pEnv->uiIdTimer != 0)
				{
				::KillTimer (hwnd, pEnv->uiIdTimer);
				}
			VerifWarning (bFermeListeH (&pEnv->hListehwndPopUps));
			bLibereEnv (hwnd);
			PostQuitMessage (0);
			}
			break;

    case WM_TIMER:
			{
			PENV pEnv = (PENV)pGetEnv (hwnd);

			pEnv->bEtatTimer = (!pEnv->bEtatTimer);
			DiffusionEvtTimerPopUp (pEnv);
			}
      break;

		case WM_ACTIVATEAPP:
			{
			PENV pEnv = (PENV)pGetEnv (hwnd);

			// Thread application activ� ?
			if (pEnv&& (wParam))
				{
				// oui existe t'il une fen�tre modale qui ne soit pas d�ja activ�e ?
				if (pEnv->hwndModaleActive)
					{
					// oui => active le pop up actif courant si n�cessaire
					if (pEnv->hwndModaleActive != GetActiveWindow())
						::BringWindowToTop (pEnv->hwndModaleActive);
					//SetForegroundWindow (pEnv->hwndModaleActive);
					}
				else
					{
					// non => active la fen�tre fille le plus en avant plan :
					//HWND hwndTemp = (HWND)hDansListeH (pEnv->hListehwndPopUps, 0);
					HWND hwndTemp = hwnd;
					if (hwndTemp)
						{
						hwndTemp = GetWindow(hwndTemp, GW_HWNDFIRST);

						while (hwndTemp)
							{
							if (phTrouveDansListeH(pEnv->hListehwndPopUps, hwndTemp))
								{
								if (pEnv->hwndModaleActive != GetActiveWindow())
									::BringWindowToTop (hwndTemp);
								hwndTemp = NULL;
								}
							else
								{
								hwndTemp = GetWindow(hwndTemp, GW_HWNDNEXT);
								}
							}
						}

					/*
					HWND hwndTemp = GetLastActivePopup(hwnd);
					if (hwndTemp != hwnd)
						::BringWindowToTop (hwndTemp);

					ShowOwnedPopups (hwnd, TRUE);
					/*
					HWND hwndTemp = (HWND)hDansListeH (pEnv->hListehwndPopUps, 0);
					if (hwndTemp)
						{
						HWND hwndFillePremierPlan = GetWindow(hwndTemp, GW_HWNDFIRST);

						// active cette fen�tre si elle existe
						if (hwndFillePremierPlan)
							::BringWindowToTop (hwndFillePremierPlan);
						}
					*/
					}
				}
			}
			break;

		case WM_ONPOPUP_CREATE:
			{
			PENV	pEnv = (PENV)pGetEnv (hwnd);
			HWND				hwndPopUp = (HWND) wParam;

			// Ajout du pop up � la liste
			VerifWarning (bAjouteDansListeH (pEnv->hListehwndPopUps, hwndPopUp));

			// traitement selon modalit�
			switch (modalitePopUp (hwndPopUp))
				{
				case POPUP_MODAL:
					// rends la modale.
					RendFenetreModale (pEnv, hwndPopUp);
					break;

				case POPUP_NON_MODAL:
					// non => mode modal en cours ?
					if (pEnv->hwndModaleActive)
						{
						// oui => fen�tre inhib�e
						::EnableWindow (hwndPopUp, FALSE);
						// et active la fen�tre active
						::BringWindowToTop (pEnv->hwndModaleActive);
						}
					break;
				}
			}
			break;

		case WM_ONPOPUP_DESTROY:
			{
			PENV	pEnv = (PENV)pGetEnv (hwnd);
			HWND				hwndPopUp = (HWND) wParam;

			// Suppression du pop up dans la liste
			VerifWarning (bSupprimeDansListeH (pEnv->hListehwndPopUps, hwndPopUp));

			// Mode modal et fen�tre marqu�e modale?
			if (pEnv->hwndModaleActive && (modalitePopUp (hwndPopUp) == POPUP_MODAL))
				// oui => mise � jour de la fen�tres modale
				FinModaliteFenetre (pEnv, hwndPopUp);
			}
			break;

		case WM_ONPOPUP_ACTIVATE:
			{
			PENV	pEnv = (PENV)pGetEnv (hwnd);
			HWND	hwndPopUp = (HWND) wParam;

			// Mode modale et Fen�tre activ�e ?
			if (pEnv->hwndModaleActive && (LOWORD(wParam) != WA_INACTIVE))
				{
				// oui : fen�tre marqu�e modale ?
				if (modalitePopUp (hwndPopUp) == POPUP_MODAL)
					{
					// oui => note qu'elle est active
					pEnv->hwndModaleActive = hwndPopUp;
					}
				}
			}
			break;

		case WM_POPUP_CHANGE_MODALITE:
			{
			PENV	pEnv = (PENV)pGetEnv (hwnd);
			HWND				hwndPopUp = (HWND) wParam;

			// oui : fen�tre marqu�e modale ?
			if (modalitePopUp (hwndPopUp) == POPUP_MODAL)
				// oui => enregistre la comme fen�tre modale
				RendFenetreModale (pEnv, hwndPopUp);
			else
				FinModaliteFenetre (pEnv, hwndPopUp);
			}
			break;

    default:
			bTraite = FALSE;
			break;
		} // switch (msg)

  // Message non trait� => traitement par d�faut
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, wParam, lParam);

  return mres;
  } // wndprocParentPopUp

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//				FONCTIONS EXPORTEES
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// ------------------------------------------------------------------------------------------------------------------
// Demarrage d'une fonction qui signale sous forme de message a toutes les fenetres filles un evenement de type timer
BOOL bLanceDiffusionEvtTimerPopUp (HWND hwndParentPopUp, UINT uiIntervalleTimer)
	{
	PENV pEnv = (PENV)pGetEnv (hwndParentPopUp);

  pEnv->uiIdTimer = ::SetTimer (hwndParentPopUp, 1, uiIntervalleTimer, NULL);
	return (pEnv->uiIdTimer != 0);
	}

// -----------------------------------------
// Cr�e une fen�tre Parente des pop ups
HWND hwndCreeParentPopUp (void)
	{
	HWND		hwndRes = NULL;
	static BOOL	bClasseEnregistree = FALSE;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;
		
		wc.style					= 0;
		wc.lpfnWndProc		= wndprocParentPopUp; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= NULL;
		wc.hbrBackground	= NULL;
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseParentPopUp; 
		bClasseEnregistree = (RegisterClass (&wc) != 0);
		}

	if (bClasseEnregistree)
		{
		// cr�ation de la fen�tre
		hwndRes = CreateWindow (
			szClasseParentPopUp,	// class name
			NULL,
			WS_OVERLAPPEDWINDOW, CW_USEDEFAULT,	CW_USEDEFAULT, CW_USEDEFAULT,	CW_USEDEFAULT, // Non Visible
			NULL,	// handle parent window
			NULL,	// handle to menu or child-window identifier
			Appli.hinst,	// handle to application instance
			NULL);	// pointer to window-creation data
		}
	return hwndRes;
	} // hwndCreeParentPopUp



//---------------------------------------------------------------------------------------
// Cr�ation de fen�tre page : met la fen�tre en conformit� avec son marquage modal ou pas
void OnCreatePopUp (HWND hwndParent, HWND hwndPopUp)
	{
	// handle valide ?
	if (hwndParent)
		::SendMessage (hwndParent, WM_ONPOPUP_CREATE, (WPARAM)hwndPopUp, 0);
	}

//-----------------------------------------------------
// Destruction d'une fen�tre appartenant � un pool de fen�tres modales
void OnDestroyPopUp (HWND hwndParent, HWND hwndPopUp)
	{
	// handle valide ?
	if (hwndParent)
		::SendMessage (hwndParent, WM_ONPOPUP_DESTROY, (WPARAM)hwndPopUp, 0);
	}

// -------------------------------------------------------
// WM_ACTIVATE PopUp
void OnActivatePopUp (HWND hwndParent, HWND hwndPopUp, WPARAM wParam)
	{
	// handle valide ?
	if (hwndParent)
		::SendMessage (hwndParent, WM_ONPOPUP_ACTIVATE, (WPARAM)hwndPopUp, 0);
	}

// -------------------------------------------------------
// Fen�tre �ventuellement modale : met la fen�tre en conformit� avec son nouveau marquage modal ou pas
void ChangeModalitePopUp (HWND hwndParent, HWND hwndPopUp)
	{
	// handle valide ?
	if (hwndParent)
		::SendMessage (hwndParent, WM_POPUP_CHANGE_MODALITE, (WPARAM)hwndPopUp, 0);
	}

// -------------------------------- fin -----------------------------------

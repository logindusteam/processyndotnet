/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il demeure sa propriete exclusive et est confidentiel.             |
 |   Aucune diffusion n'est possible sans accord ecrit.                 |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Actions - PcsEXE                                         |
 |   Auteur  : JB                                                       |
 |   Date    : 12/02/93                                                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"              
#include "UStr.h"              

#include "USem.h"                
#include "UChrono.h"                
#include "Dongle.h"               
#include "lance.h"              
#include "appliex.h"          
#include "WExe.h"            
#include "LireLng.h"
#include "pcsdlg.h"
#include "Appli.h"
#include "lng_res.h"
#include "Tipe_de.h"
#include "WDde.h"

#include "Verif.h"
#include "ActionEx.h"
VerifInit;

// D�finitions
#define NB_MAX_ACTIONS 40 // $$

typedef struct
	{
	DWORD IdAction;
	int		entier;
	DWORD dwMot;
	char  chaine [MAX_PATH];
	} ACTION;

typedef struct
  {
  DWORD  NbActions;
  ACTION aActions [NB_MAX_ACTIONS];
  } PILE_ACTION;

// Instanciation de la pile d'actions
static PILE_ACTION PileAction;

// ----------------------------------------------------------------------
void CreerAction (void)
  {
  PileAction.NbActions = 0;
  }

// ----------------------------------------------------------------------
void ajouter_action (DWORD IdAction, t_param_action *param)
  {
  if (PileAction.NbActions < NB_MAX_ACTIONS)
    {
    PileAction.aActions [PileAction.NbActions].IdAction = IdAction;
    switch (IdAction)
      {
			case action_charge_applic_froide:
			case action_charge_applic_chaude:
			case action_lance_application:
			case action_relance_application:
			case action_charge_applic_froide_mnu:
			case action_charge_applic_chaude_mnu:
			case action_lance_application_mnu:
			case action_relance_application_mnu:
			case action_sauve_application_mnu:
			case action_sauve_lien_dde:
			case action_trace_application:
				StrCopy (PileAction.aActions [PileAction.NbActions].chaine, param->chaine);
				break;

			case action_programme_dongle:
				StrCopy (PileAction.aActions [PileAction.NbActions].chaine, param->chaine);
				PileAction.aActions [PileAction.NbActions].entier = param->entier;
				PileAction.aActions [PileAction.NbActions].dwMot = param->dwMot;
				break;

			case action_quitte:
				PileAction.aActions [PileAction.NbActions].entier = param->entier;
				break;

/*
			case action_rien:
			case action_initialisation:
			case action_finalisation:
			case action_initialise_menu :
			case action_examine_produit:
			case action_init_ressources:
			case action_aide:
			default:
				break;
*/
      }
    PileAction.NbActions++;
    } // if (PileAction.NbActions < NB_MAX_ACTIONS)
  else
    VerifWarningExit;

  } // ajouter_action

// ----------------------------------------------------------------------
int executer_action (t_retour_action * pRetourAction)
  {
  int								code_retour = action_rien;
  int								n_action = 0;
	t_param_action		param_action = {0,0,""};

  while (PileAction.NbActions > 0)
    {
		ACTION * pAction = &PileAction.aActions [n_action];

    code_retour = pAction->IdAction;
    switch (pAction->IdAction)
      {
      case action_rien:
      case action_initialisation: 
      case action_finalisation: 
				break;

      case action_initialise_menu :
				{
				MnuEtatCharge();
				}
				break;

      case action_charge_applic_froide:
				{
				if (ChargeApplication (pAction->chaine,FALSE))
					{
					MnuTitreAJour();
					}
				else
					{
					param_action.entier = 0;
					ajouter_action (action_quitte, &param_action);
					}
				}
				break;

      case action_charge_applic_froide_mnu:
				{
				if (ChargeApplication (pAction->chaine,FALSE))
					{
					MnuEtatCharge();
					MnuTitreAJour();
					}
				else
					{
					MnuEtatVide();
					MnuTitreAJour();
					dlg_num_erreur (4);
				  code_retour = action_rien;
					PileAction.NbActions = 1;
					}
				}
				break;


      case action_charge_applic_chaude:
				{
				if (ChargeApplication (pAction->chaine,TRUE))
					{
					MnuTitreAJour();
					}
				else
					{
					param_action.entier = 0;
					ajouter_action (action_quitte, &param_action);
					}
				}
				break;

      case action_charge_applic_chaude_mnu:
				{
				if (ChargeApplication (pAction->chaine,TRUE))
					{
					MnuEtatCharge();
					MnuTitreAJour();
					}
				else
					{
					MnuEtatVide();
					MnuTitreAJour();
					dlg_num_erreur (4);
				  code_retour = action_rien;
					PileAction.NbActions = 1;
					}
				}
				break;

      case action_charge_lien_dde:
				if (ChargeLiensDde())
					{
					MnuEtatCharge(); //$$ A V�rifier
					MnuTitreAJour();
					}
				else
					{
					MnuEtatVide();
					MnuTitreAJour();
					}
				break;

      case action_copier_lien_dde:
				EditionCopierLienServeurDDE (Appli.hwnd);
				break;

      case action_coller_lien_dde:
				EditionCollerLienClientDDE (Appli.hwnd);
				break;

      case action_lance_application:
				{
				MnuTitreAJourRun();
			  BOOL bFinPcs;
			  int	NumErreur;
				if (ExecuteApplicationsUtilisateur (pAction->chaine, FALSE, &bFinPcs, &NumErreur))
					{
					MnuTitreAJour();
					if (bFinPcs)
						{
						param_action.entier = NumErreur;
						ajouter_action (action_quitte, &param_action);
						}
					}
				else
					{
					MnuTitreAJour();
					param_action.entier = 0;
					ajouter_action (action_quitte, &param_action);
					}
				}
				break;

      case action_lance_application_mnu:
				{
				MnuTitreAJourRun();
				MnuEtatLance();
				BOOL bFinPcs;
				int	NumErreur;
				if (ExecuteApplicationsUtilisateur (pAction->chaine, FALSE, &bFinPcs, &NumErreur))
					{
					MnuEtatArrete();
					MnuTitreAJour();
					if (bFinPcs)
						{
						param_action.entier = NumErreur;
						ajouter_action (action_quitte, &param_action);
						}
					}
				else
					{
					MnuTitreAJour();
					MnuEtatVide();
					param_action.entier = 0;
					ajouter_action (action_quitte, &param_action);
					}
				}
				break;

      case action_relance_application:
				{
				MnuTitreAJourRun();
				BOOL bFinPcs;
				int	NumErreur;
				if (ExecuteApplicationsUtilisateur (pAction->chaine, TRUE, &bFinPcs, &NumErreur))
					{
					MnuTitreAJour();
					if (bFinPcs)
						{
						param_action.entier = NumErreur;
						ajouter_action (action_quitte, &param_action);
						}
					}
				else
					{
					MnuTitreAJour();
					param_action.entier = 0;
					ajouter_action (action_quitte, &param_action);
					}
				}
				break;

      case action_relance_application_mnu:
				{
				MnuTitreAJourRun();
				MnuEtatLance();
				BOOL bFinPcs;
				int	NumErreur;
				if (ExecuteApplicationsUtilisateur (pAction->chaine, TRUE, &bFinPcs, &NumErreur))
					{
					MnuTitreAJour();
					MnuEtatArrete();
					if (bFinPcs)
						{
						param_action.entier = NumErreur;
						ajouter_action (action_quitte, &param_action);
						}
					}
				else
					{
					MnuTitreAJour();
					MnuEtatVide();
					param_action.entier = 0;
					ajouter_action (action_quitte, &param_action);
					}
				}
				break;

      case action_sauve_application_mnu:
				//SauveApplication(pAction->chaine);
				break;

      case action_sauve_lien_dde:
				SauveLienDde(pAction->chaine);
				break;

      case action_init_ressources:
				InitRessourcesAppli();
				break;

      case action_trace_application:
				param_action.entier = 0;
				ajouter_action (action_quitte, &param_action);
				break;

      case action_programme_dongle:
				{
			  DWORD	wNumRelease;
				if (StrToDWORD (&wNumRelease, pAction->chaine))
					{
					programmer_dongle (wNumRelease, (DWORD)pAction->entier, pAction->dwMot);
					}
				else
					{
					programmer_dongle (0, 0, 0);
					}
				}
				break;

      case action_examine_produit:
				{
				DWORD dwNumFonction, dwNbVariables;

				diagnostic_dongle (&dwNumFonction, &dwNbVariables);
				}
				break;

      case action_quitte:
				pRetourAction->entier = pAction->entier;
				break;

      case action_aide:
				param_action.entier = 0;
				ajouter_action (action_quitte, &param_action);
				break;

      default:
				param_action.entier = 0;
				ajouter_action (action_quitte, &param_action);
				break;
      }
    n_action++;
    PileAction.NbActions--;
    }

  return code_retour;
  }

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------*/
// Win32 22/6/98

#include "stdafx.h"
#include <math.h>
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_co.h"
#include "pcs_sys.h"
#include "UStr.h"
#include "mem.h"
#include "driv_dq.h"
#include "temps.h"
#include "UChrono.h"                
#include "..\Commun\UCom.h"
#include "PathMan.h"
#include "DocMan.h"
#include "PcsVarEx.h"
#include "ExeOuvFic.h"
#include "DllMuxEx.h"
#include "UDde.h"
#include "scrut_tm.h"
#include "scrut_im.h"
#include "ExecuteProgrammes.h"
#include "driv_dif.h"
#include "UStr.h"
#include "DLLMan.h"
#include "ULangues.h"
#include "DLLMan.h"
#include "MemMan.h"
#include "Dicho.h"
#include "Appli.h"
#include "ExecuteF.h"
//$$ A Terminer #import ".\..\PcsMail\Bin\Debug\PcsMail.tlb"
//-----------------------------------------------
// Types des execute_f DLL utilisateur PcsUsrNT
// Prototype point d'entr�e Dll user
typedef void (_stdcall *PFN_PCSENTRY) (HANDLE hProcessyn);
typedef enum 
	{
	APPEL
	} FNS_DLL_USR_NT;
#define NB_FN_DLL_USR_NT (APPEL + 1)

static const LIEN_FONCTION LienPcsUserNTInitial = {NULL, "_PcsEntry@4"};

// Contexte d'ex�cution des execute_f DLL User
typedef struct
	{
	LIEN_FONCTION aLiensDLLUsrNT [NB_FN_DLL_USR_NT];// Table de lien � l'ex�cution avec la DLL utilisateur
	HINSTANCE hmodDLLUsrNT;
	} CONTEXTE_DLL_USER;

//-----------------------------------------------
// Types des execute_f DLL Inhibition clavier Liclock
// Prototype point d'entr�e Dll LiClock
typedef void (_stdcall *PFN_LICLOCKENTRY) (BOOL bInhibeTache, BOOL bInhibeFenetre);
typedef enum 
	{
	APPEL_INHIBITION
	} FNS_DLL_LICLOCK;
#define NB_FN_DLL_LICLOCK (APPEL_INHIBITION + 1)

static const LIEN_FONCTION LienLiclockInitial = {NULL, "bInhibeClavier"};

// Contexte d'ex�cution des execute_f DLL LiClock
typedef struct
	{
	LIEN_FONCTION aLiensDLLLiclock [NB_FN_DLL_LICLOCK];// Table de lien � l'ex�cution avec la DLL LiClock
	HINSTANCE hmodDLLLiclock;
	} CONTEXTE_DLL_LICLOCK;

//-----------------------------------------------
// Types ex�cution des execute_f DDE
#define TIMEOUT_DDE_PAR_DEFAUT_MS 60000  // une minute en millisecondes
#define NB_CLIENTS_DDE_EX 32
typedef struct // Contexte d'ex�cution d'un client DDE
	{
	BOOL bModeGestionAuto;
	DWORD dwTimeOutMs;
	PENV_CONV pEnvConvClient;
	DWORD dwNConversation;	// de 0 � NB_CLIENTS_DDE_EX -1
	char  szServeur[nb_max_caractere+1];
	char  szTopic[nb_max_caractere+1];
	} CLIENT_DDE_EX, *PCLIENT_DDE_EX;

// Contexte d'ex�cution des execute_f DDE
typedef struct
	{
	BOOL bInitialisationDDELocale; // DDE initialis� par ce module
	CLIENT_DDE_EX aClientsDDE[NB_CLIENTS_DDE_EX];
	} CONTEXTE_DDE;

typedef struct
	{
	CONTEXTE_DLL_USER ContexteDLLUser;
	CONTEXTE_DLL_LICLOCK ContexteDLLLiclock;
	CONTEXTE_DDE ContexteDDE;
	CExecuteProgrammes * pExecuteProgrammes;
	} CONTEXTE_EXECUTE_F, *PCONTEXTE_EXECUTE_F;

//$$ Global � instancier et � passer en param�tre
static CONTEXTE_EXECUTE_F ContexteExecuteF;

//-----------------------------------------------------------------------------------
// Utilitaires DDE (fonctions locales)
//-----------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
// Fonction client DDE pour les fonctions execute_f DDE imm�diat 
// sert a la prise en compte des d�connexion dues au serveur distant
static HDDEDATA FDDEClientImmediat (struct tagENV_CONV * pEnvConv, UINT wType, UINT wFmt, HCONV hConv,
		HSZ hsz1, HSZ hsz2, HDDEDATA hData, DWORD dwData1, DWORD dwData2)
	{
	HDDEDATA hDDEdataRes = NULL;

	switch (wType)
		{
		case XTYP_DISCONNECT:
			{
			// la conversation avec le serveur est termin�e :
			// note que la conversation concern�e est ferm�e
			DWORD dwNConversation	= (DWORD)pEnvConv->lParam1;

			// Num�ro de conversation valide ?
			if (dwNConversation < NB_CLIENTS_DDE_EX)
				{
				// oui => conversation ferm�e
				PCLIENT_DDE_EX pClientDDE = &ContexteExecuteF.ContexteDDE.aClientsDDE[dwNConversation];
				pClientDDE->pEnvConvClient = NULL;
				}
			}
			// pas de code de retour
			break;
		}
	return hDDEdataRes;
	} // FClient


// Ouvre une conversation DDE si elle n'est pas d�ja ouverte
// renvoie FALSE s'il y a eu une erreur d'ouverture
static BOOL bOuvreConversation (PCLIENT_DDE_EX pClientDDE)
	{
	// Conversation ferm�e ?
	if (!pClientDDE->pEnvConvClient)
		{
		// oui => initialisation DDE a la vol�e si n�cessaire
		if (!bDDEOuvert())
			{	
			if (!ContexteExecuteF.ContexteDDE.bInitialisationDDELocale)
				{
				ContexteExecuteF.ContexteDDE.bInitialisationDDELocale = bInitDDE (Appli.hinst, TRUE, TRUE);
				}
			}

		// essai d'ouverture de la conversation
		pClientDDE->pEnvConvClient = pInitConversationDDE (pClientDDE->szServeur, pClientDDE->szTopic, FDDEClientImmediat, (LPARAM)pClientDDE->dwNConversation, 0);
		}

	// Ok si conversation ouverte
	return (pClientDDE->pEnvConvClient != NULL);
	}

// Ferme une �ventuelle conversation DDE ouverte
// renvoie FALSE s'il y eu une erreur de fermeture
static BOOL bFermeConversationOuverte (PCLIENT_DDE_EX pClientDDE)
	{
	BOOL bRes = TRUE;
	if (pClientDDE->pEnvConvClient)
		{
		// fermeture imm�diate de la conversation en cours
		if (!bFermeConversationDDE (&pClientDDE->pEnvConvClient))
			bRes = FALSE;
		}
	return bRes;
	}

// --------------------------------------------------------------------
// Ouverture auto d'une conversation DDE si mode auto et conversation ferm�e
static UINT uOuvreConversationDDEEnModeAuto(PCLIENT_DDE_EX pClientDDE)
	{
	DWORD dwErr = NO_ERROR;
	
	if (pClientDDE->bModeGestionAuto)
		{
		if (!bOuvreConversation (pClientDDE))
			dwErr = uDerniereErreurDDE ();
		}
	else
		{
		if (!pClientDDE->pEnvConvClient)
			dwErr = ERROR_INVALID_FUNCTION;
		}
	return dwErr;
	}

//----------------------------------------------------------------------------
//							Ouverture / fermeture des execute_f
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// Initialisation des contextes d'ex�cution des execute_f
void InitExecuteF (void)
	{
	// Initialisation lien Dll PcsUsrNt
	static PCSTR pszNomDll = "PcsUsrNT.DLL";
	char szPathDll[MAX_PATH];

	pszCreePathName (szPathDll, MAX_PATH, pszPathDoc(), pszNomDll);
	ContexteExecuteF.ContexteDLLUser.aLiensDLLUsrNT[APPEL] = LienPcsUserNTInitial;
	ContexteExecuteF.ContexteDLLUser.hmodDLLUsrNT = NULL;

	// Charge la DLL utilisateur dans le r�pertoire de l'appli Ok ?
	if (!bLieDLL (&ContexteExecuteF.ContexteDLLUser.hmodDLLUsrNT, szPathDll, ContexteExecuteF.ContexteDLLUser.aLiensDLLUsrNT, NB_FN_DLL_USR_NT))
		{
		// non => lib�re la DLL utilisateur �ventuellement charg�e
		bLibereLienDLL (&ContexteExecuteF.ContexteDLLUser.hmodDLLUsrNT, ContexteExecuteF.ContexteDLLUser.aLiensDLLUsrNT, NB_FN_DLL_USR_NT);

		// charge la DLL utilisateur avec l'exe ou path ou syst�me
		bLieDLL (&ContexteExecuteF.ContexteDLLUser.hmodDLLUsrNT, pszNomDll, ContexteExecuteF.ContexteDLLUser.aLiensDLLUsrNT, NB_FN_DLL_USR_NT);
		}

	// Initialisation lien Dll Liclock
	static PCSTR pszNomDllLiclock = "Liclock.DLL";
	char szPathDllLiclock[MAX_PATH];

	pszCreePathName (szPathDllLiclock, MAX_PATH, pszPathDoc(), pszNomDllLiclock);
	ContexteExecuteF.ContexteDLLLiclock.aLiensDLLLiclock[APPEL_INHIBITION] = LienLiclockInitial;
	ContexteExecuteF.ContexteDLLLiclock.hmodDLLLiclock = NULL;

	// Charge la DLL Liclock dans le r�pertoire de l'appli Ok ?
	if (!bLieDLL (&ContexteExecuteF.ContexteDLLLiclock.hmodDLLLiclock, szPathDllLiclock, ContexteExecuteF.ContexteDLLLiclock.aLiensDLLLiclock, NB_FN_DLL_LICLOCK))
		{
		// non => lib�re la DLL Liclock �ventuellement charg�e
		bLibereLienDLL (&ContexteExecuteF.ContexteDLLLiclock.hmodDLLLiclock, ContexteExecuteF.ContexteDLLLiclock.aLiensDLLLiclock, NB_FN_DLL_LICLOCK);

		// charge la DLL utilisateur avec l'exe ou path ou syst�me
		bLieDLL (&ContexteExecuteF.ContexteDLLLiclock.hmodDLLLiclock, pszNomDllLiclock, ContexteExecuteF.ContexteDLLLiclock.aLiensDLLLiclock, NB_FN_DLL_LICLOCK);
		}

	// Initialisation DDE
	ContexteExecuteF.ContexteDDE.bInitialisationDDELocale = FALSE;
	for (DWORD dwNClient = 0; dwNClient < NB_CLIENTS_DDE_EX; dwNClient++)
		{
		PCLIENT_DDE_EX pClientDDE = &ContexteExecuteF.ContexteDDE.aClientsDDE[dwNClient];
		pClientDDE->bModeGestionAuto = FALSE;
		pClientDDE->pEnvConvClient = NULL;
		pClientDDE->dwTimeOutMs = TIMEOUT_DDE_PAR_DEFAUT_MS;
		pClientDDE->dwNConversation = dwNClient;
		StrSetNull(pClientDDE->szServeur);
		StrSetNull(pClientDDE->szTopic);
		}

	// Initialisation ExecuteProgrammes
	ContexteExecuteF.pExecuteProgrammes = new CExecuteProgrammes;
	}

//----------------------------------------------------------------------------
// Fermeture des contextes d'ex�cution des execute_f
void TermineExecuteF (void)
	{
	// Fermeture Dll User
	// Lib�re les liens avec l'ensemble des fonctions de la DLL
	bLibereLienDLL (&ContexteExecuteF.ContexteDLLUser.hmodDLLUsrNT, ContexteExecuteF.ContexteDLLUser.aLiensDLLUsrNT, NB_FN_DLL_USR_NT);

	// Fermeture DDE
	for (DWORD dwNClient = 0; dwNClient < NB_CLIENTS_DDE_EX; dwNClient++)
		{
		PCLIENT_DDE_EX pClientDDE = &ContexteExecuteF.ContexteDDE.aClientsDDE[dwNClient];
		bFermeConversationOuverte(pClientDDE);
		pClientDDE->bModeGestionAuto = FALSE;
		StrSetNull(pClientDDE->szServeur);
		StrSetNull(pClientDDE->szTopic);
		}
	if (ContexteExecuteF.ContexteDDE.bInitialisationDDELocale)
		{
		ContexteExecuteF.ContexteDDE.bInitialisationDDELocale = FALSE;
		FermeDDE();
		}

	// Initialisation ExecuteProgrammes
	delete ContexteExecuteF.pExecuteProgrammes;
	}

//----------------------------------------------------------------------------
// 1     numero fonction invalide (< 0 ou > MAXINT4)
// 2     numero de drive invalide sur fonction 1
// 4     la valeur ecrite dans STATUS 7552 )it etre comprise entre 0 et 65535
// 8     la valeur ecrite dans la sortie digitale du 7552 )it etre 1 ou 2
// 16
// 32
// 64
// 128
// 256
// 512
// 1024
// 2048
// 4096
// 8192
// 16384
// 32768
void maj_status_fct (DWORD erreur)
  {
	CPcsVarEx::SetBitsValVarSysNumEx (VA_SYS_STATUS_FONCTION, erreur);
  }

// Fonction place libre sur un disque
void ExecuteF1 (void)
	{
	int nTemp = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM7);
  if ((nTemp < 0) || (nTemp > 26))
    {
    maj_status_fct (2);
    }
  else
    {
		__int64  place_disque_octet;
		DWORD    result = place_disque_libre (nTemp, &place_disque_octet);

    // mise a jour place libre
		CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)place_disque_octet);

    // mise a jour status
    CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)result);
    }
	}

// Fonction Initialisation imprimante
void ExecuteF9 (void)
	{
  DWORD wImprimante = (DWORD) CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);

  if (!initialiser_imprimante (wImprimante))
    {
    maj_status_fct(16);
    }
	}

// Fonction Place libre dans tampon imprimante
void ExecuteF10 (void)
	{
  DWORD wImprimante = (DWORD) CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
	DWORD wPlaceTamponImprimante = 0;
	DWORD dwTailleOccupee = 0;

  if (!espace_libre_tampon_imprimante (wImprimante, &wPlaceTamponImprimante, &dwTailleOccupee))
    {
    maj_status_fct(16);
    }
  // Mise � jour r�sultat
  CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)wPlaceTamponImprimante);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM3, (FLOAT)dwTailleOccupee);
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG2, wPlaceTamponImprimante > 82);
	}

/*
//Fonction test d'�criture sur unite en gerant abort retry ignore
void ExecuteF11 (void)
	{
	int	nTemp = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM7);
  if ((nTemp < 0) || (nTemp > 26) )
    {
    maj_status_fct(2);
    }
  else
    {
    result   = etat_media_c ((DWORD)nTemp);

		// mise a jour status
		CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)result)
    }
	}
*/

// fonction PCS <----> DIf
void ExecuteF12 (void)
	{
	char	fic_source_dif [MAX_PATH];
	char	fic_dest_dif [MAX_PATH];
	char	sens_dif;
	BOOL	reinit_dif;
	DWORD enr_depart_dif;

	// fichier source
	StrSetNull (fic_source_dif);
	CPcsVarEx::pszGetValVarSysMesEx (fic_source_dif, VA_SYS_REG_MES1);
	// fichier destination
	StrSetNull (fic_dest_dif);
	CPcsVarEx::pszGetValVarSysMesEx (fic_dest_dif, VA_SYS_REG_MES2);
	// enr de depart
	enr_depart_dif = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
	// sens
	if (CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG1))
		sens_dif = '1';
	else
		sens_dif = '2';
	// reinit effacement
	reinit_dif = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG2);

	DWORD    result = fonction_dif (sens_dif, reinit_dif, fic_source_dif, fic_dest_dif, enr_depart_dif);

	// MAJ r�sultat
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)result);
	}

// Fonction gestion time-out imprimante
void ExecuteF13 (void)
	{
	int nTimeOut = (int) CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
  DWORD wImprimante = (DWORD) CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);

  if (nTimeOut >= 0)
    {
    if (!maj_time_out_imprimante (wImprimante, (DWORD) nTimeOut))
      maj_status_fct(16);
    }
  else
    maj_status_fct(16);
	}

// Fonction mise � jour horloge : date
void ExecuteF16 (void)
	{
	DWORD dwJour = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
	DWORD dwMois = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
	DWORD dwAnnee = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);

	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, TpsForcerDate (dwJour, dwMois, dwAnnee));
	}

// Fonction mise � jour horloge : heure
void ExecuteF17 (void)
	{
	DWORD dwHeure = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
	DWORD	dwMinute = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
	DWORD	dwSeconde = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);

  CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, TpsForcerHeure (dwHeure, dwMinute, dwSeconde, 0));
	}

// Fonction longueur d'une chaine
void ExecuteF18 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];

  CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)StrLength (mes_tempo1));
	}

// Fonction Recherche d'un caract�re avec codage ascii
void ExecuteF19 (void)
	{
  BOOL existe = FALSE;
  int car_ascii = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
  int position_debut = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
  int position_fin = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);
  BOOL recherche_g_d = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG1);
  CHAR car_recherche = (char)(car_ascii);
  char       mes_entre [nb_max_caractere+1] = "";
  CPcsVarEx::pszGetValVarSysMesEx (mes_entre, VA_SYS_REG_MES1);

  if ((position_debut > position_fin) || (position_fin > (int)StrLength(mes_entre)) )
    {
    maj_status_fct(0x0020);
    existe = FALSE;
    }
  else
    {
		int position_trouve;

    if (recherche_g_d)
      {
      for (position_trouve = position_debut-1; ((position_trouve < position_fin) && (!existe)); position_trouve++)
        {
        existe = (mes_entre[position_trouve] == car_recherche);
        }
      }
    else
      {
      for (position_trouve = position_fin-1; ((position_trouve >= position_debut-1) && (!existe)); position_trouve--)
        {
        existe = (mes_entre[position_trouve] == car_recherche);
        }
      position_trouve = position_trouve + 2;
      }
		CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG2, existe);
		CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM4, (FLOAT)position_trouve);
    }
	}

// Fonction Recherche d'une chaine
void ExecuteF20 (void)
	{
  int car_ascii = 0;
  BOOL existe = FALSE;
	int position_trouve = 0;
	char mes_entre [nb_max_caractere+1] = "";
	char mes_recherche [nb_max_caractere+1] = "";
  CPcsVarEx::pszGetValVarSysMesEx (mes_entre, VA_SYS_REG_MES1);
  CPcsVarEx::pszGetValVarSysMesEx (mes_recherche, VA_SYS_REG_MES2);
  int position_debut = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
	if (position_debut == 0)
		position_debut = 1;
  int position_fin = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);
  BOOL recherche_g_d = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG1);

  if ((position_debut > position_fin ) || (position_fin > (int)StrLength(mes_entre)) )
    {
    maj_status_fct(32);
    }
  else
    {
		char  mes_tempo1[nb_max_caractere+1];
    StrSetNull (mes_tempo1);
    StrExtract (mes_tempo1, mes_entre, (DWORD)position_debut - 1, (DWORD)(position_fin-position_debut+1));
    if (StrLength (mes_tempo1) < StrLength (mes_recherche))
      {
      maj_status_fct(32);
      }
    else
      {
      if (recherche_g_d)
        {
				position_trouve = StrSearchStr (mes_tempo1, mes_recherche);
				if (position_trouve != STR_NOT_FOUND)
					{
					existe = TRUE;
					position_trouve = position_trouve + position_debut;
					}
				else
					{
					existe = FALSE;
					}
        }
      else
        {
        for (position_trouve = StrLength (mes_tempo1)-1; ((position_trouve >= 0) && (!existe)); position_trouve--)
          {
          existe = (BOOL) (StrSearchStr (& (mes_tempo1[position_trouve]), mes_recherche) != STR_NOT_FOUND);
          }
        position_trouve = position_trouve  + position_debut + 1;
        }
      }
    }
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG2, existe);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM4, (FLOAT)position_trouve);
	}

// Fonction Insertion d'une chaine
void ExecuteF21 (void)
	{
	char mes_entre [nb_max_caractere+1] = "";
  char mes_recherche [nb_max_caractere+1] = "";
	char mes_tempo1[nb_max_caractere+1] = "";
  CPcsVarEx::pszGetValVarSysMesEx (mes_entre, VA_SYS_REG_MES1);
  CPcsVarEx::pszGetValVarSysMesEx (mes_recherche, VA_SYS_REG_MES2);
  int position_debut = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
  if ((position_debut > (int)StrLength (mes_entre)+1) ||  ( (StrLength(mes_entre) + StrLength (mes_recherche)) > nb_max_caractere) )
    {
    maj_status_fct(0x0020);
    }
  else
    {
    StrCopy (mes_tempo1, mes_entre);
    if (position_debut == 1)
      {
      StrConcat (mes_recherche, mes_tempo1);
      StrCopy (mes_tempo1, mes_recherche);
      }
    else
      {
      if (position_debut == (int)StrLength (mes_entre)+1)
        {
        StrConcat (mes_tempo1, mes_recherche);
        }
      else
        {
        StrDelete (mes_tempo1, position_debut - 1, (StrLength(mes_entre) - position_debut + 1 ));
        StrConcat (mes_tempo1, mes_recherche);
        StrDelete (mes_entre, 0, position_debut - 1);
        StrConcat (mes_tempo1, mes_entre);
        }
      }
		CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES2, mes_tempo1);
    }
	}

// Fonction Suppression d'une partie de cha�ne
void ExecuteF22 (void)
	{
	char mes_entre [nb_max_caractere+1] = "";
  CPcsVarEx::pszGetValVarSysMesEx (mes_entre, VA_SYS_REG_MES1);
  int position_debut = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
  int position_fin = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);
  if ((position_debut + position_fin)  > (int)(StrLength(mes_entre) + 1) )
    {
    maj_status_fct(0x0020);
    }
  else
    {
    StrDelete (mes_entre, position_debut - 1, position_fin);
		CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES2, mes_entre);
    }
	}

// Fonction Substitution dans une cha�ne
void ExecuteF23 (void)
	{
	char mes_entre [nb_max_caractere+1] = "";
	char mes_recherche [nb_max_caractere+1] = "";
  CPcsVarEx::pszGetValVarSysMesEx (mes_entre, VA_SYS_REG_MES1);
  CPcsVarEx::pszGetValVarSysMesEx (mes_recherche, VA_SYS_REG_MES2);
  int position_debut = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
  if (((position_debut - 1 + (int)StrLength(mes_recherche)) > (int)StrLength(mes_entre) ) || (position_debut < 1) )
    {
    maj_status_fct(32);
    }
  else
    {
    for (int i = 0; (i < (int)StrLength(mes_recherche)); i++)
      {
      mes_entre [position_debut+i-1] = mes_recherche [i];
      }
    }
  CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES2, mes_entre);
	}

// Fonction Extrait une partie de cha�ne � droite
void ExecuteF24 (void)
	{
	char mes_entre [nb_max_caractere+1] = "";
  CPcsVarEx::pszGetValVarSysMesEx (mes_entre, VA_SYS_REG_MES1);
  int position_debut = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
  if (position_debut > (int)StrLength(mes_entre))
    {
    maj_status_fct(0x0020);
    }
  else
    {
    StrDelete (mes_entre, 0,((int)StrLength(mes_entre) - position_debut));
    CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES2, mes_entre);
    }
	}

// Fonction Extrait une partie de cha�ne � gauche
void ExecuteF25 (void)
	{
	char mes_entre [nb_max_caractere+1] = "";
  CPcsVarEx::pszGetValVarSysMesEx (mes_entre, VA_SYS_REG_MES1);
  int position_debut = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
  if (position_debut > (int)StrLength(mes_entre) )
    {
    maj_status_fct(0x0020);
    }
  else
    {
    StrDelete (mes_entre, position_debut, ((int)StrLength(mes_entre) - position_debut));
		CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES2, mes_entre);
    }
	}

// Fonction Extrait une partie de cha�ne
void ExecuteF26 (void)
	{
	char mes_entre [nb_max_caractere+1] = "";
  CPcsVarEx::pszGetValVarSysMesEx (mes_entre, VA_SYS_REG_MES1);
  int position_debut = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
  int position_fin	 = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);
  if (position_debut - 1 + position_fin > (int)StrLength (mes_entre))
    {
    maj_status_fct(0x0020);
    }
  else
    {
    if (position_debut > 0 )
      {
      StrDelete (mes_entre, 0, position_debut-1);
      }
    StrDelete (mes_entre, position_fin, ((int)StrLength(mes_entre) - position_fin));
		CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES2, mes_entre);
    }
	}

// Fonction r�gulateur Eurotherm 822 : cr�ation de fichier "de trames"
//case 27 : break;

// Fonction r�gulateur Eurotherm 822 : fichier ascii -> fichier reels
//case 28 : break;

// Fonction lancement de la fonction transfert
//case 29 : break;

// Fonction Ordonne le depart_de_transfert
//case 30 : break;

// Fonction envoi message ASCII
void ExecuteF31 (void)
	{
	int	canal    = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
	int	vitesse  = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
	int	nb_bit   = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);
	int	parite   = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM4);
	int	bit_stop = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM5);
	BOOL decroche_a_faire = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG1); // H -> Raccroche,Hard_com,Decroche a l'init
	BOOL raccroche_a_faire = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG2); // H -> Raccroche en fin de fonction

	int int_tempo1 = 0;
	// Doit on d�crocher ?
	if (decroche_a_faire)
		{
		int_tempo1 = nComFerme (canal);
		if (int_tempo1 == ERREUR_COM_CANAL_INVALIDE)
			int_tempo1 = 0;
    
		if (int_tempo1 == 0)
			{
			int_tempo1 = nComTestCanal (canal);
			if (int_tempo1 != 0)
				{
				maj_status_fct(0x0040);
				}
			else
				{
				int_tempo1 = nComOuvre (canal, FALSE, vitesse, nb_bit, parite, bit_stop);
				if (int_tempo1 != 0)
					{
					maj_status_fct(0x0080);
					}
				} // nComTestCanal OK
			} // nComFerme OK
		else
			maj_status_fct(0x00C0); // bits 6 et 7
		} // decroche_a_faire

	if (int_tempo1 == 0)
		{
		char mes_tempo1[nb_max_caractere+1] = "";
		CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
		bComVideReception (canal);
		int nb_car = (int)StrLength (mes_tempo1);

		if (nb_car > 0)
			{
			if (uComEcrire (canal, mes_tempo1 ,(DWORD)(nb_car), TRUE) != 0)
				{
				maj_status_fct(0x0080);
				}
			} // nb_car > 0
		} // init OK

	if (raccroche_a_faire)  // si nComFerme en fin
		{
		if (nComFerme (canal) != 0)
			{
			maj_status_fct(0x00C0);
			} // erreur nComFerme
		} // raccroche_a_faire
	}

// Fonction reception message ASCII
void ExecuteF32 (void)
	{
#define ERR_STATUS_F_COM_CARTE_INVALIDE		0x0040
#define ERR_STATUS_F_COM_INIT_CANAL				0x0080
#define ERR_STATUS_F_COM_LIBERATION_CANAL	0x00C0
#define ERR_STATUS_F_COM_NB_CARS_INVALIDE	0x0100
#define ERR_STATUS_F_COM_PB_RX						0x0140

	int	canal    = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
	int	vitesse  = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
	int	nb_bit   = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);
	int	parite   = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM4);
	int	bit_stop = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM5);
	int	time_out = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM6);
	BOOL decroche_a_faire = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG1); // H -> Raccroche,Hard_com,Decroche a l'init
	BOOL reset_a_faire		= CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG2); // H -> Reset_ecoute

  int nStatutCom = 0;
	// d�croche � faire ?
  if (decroche_a_faire)
    {
		// oui => raccroche puis d�croche :
    nStatutCom = nComFerme (canal);
		if (nStatutCom == ERREUR_COM_CANAL_INVALIDE)
			nStatutCom = 0;
    if (nStatutCom == 0)
      {
      nStatutCom = nComTestCanal (canal);
      if (nStatutCom != 0)
        {
				maj_status_fct(ERR_STATUS_F_COM_CARTE_INVALIDE);
        }
      else
        {
        nStatutCom = nComOuvre (canal, FALSE, vitesse, nb_bit, parite, bit_stop);
				if (nStatutCom != 0)
          {
					maj_status_fct(ERR_STATUS_F_COM_INIT_CANAL);
          }
        } // nComTestCanal OK
      } // nComFerme OK
		else
      {
			maj_status_fct(ERR_STATUS_F_COM_LIBERATION_CANAL); // bits 6 et 7
      }// erreur nComFerme
    } // if (decroche_a_faire)

	// pas d'erreur rencontr�e ?
  if (nStatutCom == 0)
    {
		// non : r�ception

		// vide si demand� le buffer reception
    if (reset_a_faire)
			bComVideReception (canal);
		
		// prise en compte du nombre de caract�res � lire demand�
		int nNbCarsAttendus = (int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM7);      // reg_num7

		// invalide ?
		if ((nNbCarsAttendus > 80) || (nNbCarsAttendus < 0))
			{
			// oui => erreur
			maj_status_fct(ERR_STATUS_F_COM_NB_CARS_INVALIDE);
			}
		else
			{
			// au moins un caract�re � lire ?
			if (nNbCarsAttendus > 0)
				{
				char mes_tempo1[nb_max_caractere+1] = "";
				MINUTERIE tempo_recept;
				int   cptr_recu = 0;
				BOOL	b_sortie = FALSE;
				nStatutCom = 0;
				MinuterieLanceMs (&tempo_recept, NB_MS_PAR_CENTIEME (time_out));

				do
					{
					// boucle de r�ception de caract�res
					CHAR recu;
					int statut_phone = nComLireCar (canal, &recu);
					// recu qq chose ?
					if (statut_phone != ERREUR_COM_BUFFER_VIDE)
						{
						// oui => prise en compte du caract�re re�u
						mes_tempo1[cptr_recu] = recu;
						cptr_recu++;
						b_sortie = (cptr_recu == nNbCarsAttendus);

						// Caract�re entach� d'erreur
						if (statut_phone != 0)
							{ 
							// oui => erreur de type parite ou encadrement
							if (statut_phone < ERREUR_COM_OVERRUN)
								{ 
								// prise en compte de l'erreur $$ ERREUR_COM_OVERRUN masqu�e !!!
								nStatutCom = nStatutCom | statut_phone;
								}
							}
						// relance le time out
						MinuterieLanceMs (&tempo_recept, NB_MS_PAR_CENTIEME(time_out));
						}
					else
						{
						// non => rien recu
						if (bEcheanceMinuterie (tempo_recept))
							{ // time out reception
							b_sortie = TRUE;
							nStatutCom = nStatutCom | ERREUR_COM_TIME_OUT;
							} // time out reception
						} // rien recu
					} while (! b_sortie);

				if (nStatutCom == 0)
					{
					mes_tempo1[cptr_recu] = '\0';
					CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES1, mes_tempo1);
					}
				else
					{
					// TIME_OUT ?
					if (nStatutCom & ERREUR_COM_TIME_OUT)
						{ 
						// oui => r�ception termin�e
						mes_tempo1[cptr_recu] = '\0';
						CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES1, mes_tempo1);
						CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)cptr_recu);
						}
					CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)nStatutCom);
					maj_status_fct (ERR_STATUS_F_COM_PB_RX);
					} // if (nStatutCom == 0)
				} // if (nNbCarsAttendus > 0)
			} // else ((nNbCarsAttendus > 80) || (nNbCarsAttendus < 0))
		} // if (nStatutCom == 0)
	}

// Affichage Qhelp
void ExecuteF34 (void)
	{
	BOOL bTemp = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG1);
  ouvre_qhelp (&bTemp);
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, bTemp);
	}

// Affichage dans l'aide d'un fichier
void ExecuteF35 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];
	CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
  affiche_qhelp (mes_tempo1);
	}

// init index Qhelp
void ExecuteF36 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];
	char  mes_tempo2[nb_max_caractere+1];
	CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
	CPcsVarEx::pszGetValVarSysMesEx (mes_tempo2, VA_SYS_REG_MES2);
  init_qhelp (mes_tempo1, mes_tempo2);
	}

// vide buffer imprimante -- avec fermeture du port d'impression si num imp
void ExecuteF37 (void)
	{
	// Num�ro d'imprimante pass� en param�tres
	DWORD wImprimante = (DWORD) CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
	BOOL bFermetureDemandee = FALSE;

	// num imprimante n�gatif ?
	if (((LONG)wImprimante) < 0)
		{
		// oui => fermeture en plus du reset demand�e.
		bFermetureDemandee = TRUE;
		wImprimante = (DWORD)-((LONG)wImprimante);
		}

	// reset et �ventuelle fermeture
	if (vide_buffer_imprimante (wImprimante))
		{
		if (bFermetureDemandee)
			{
			if (!ferme_port_imprimante (wImprimante))
				maj_status_fct(16);
			}
		}
	else
		{
		maj_status_fct(16);
		}
	}

// Recupere un nom de fichier via une dlg
void ExecuteF38 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];
	char  mes_tempo2[nb_max_caractere+1];
	DWORD  word_tempo;
	// REG_LOG1 : vrai : ouvre la boite
	//	REG_MES2 : type de fichier a chercher (*.*)
	//	en sortie
	//	REG_NUM1 : code erreur
	//	REG_MES1 : nom du fichier retour si code erreur = 0
	// REG_LOG1 : faux : demande si la boite s'est ferm�e
	//	en sortie
	//	REG_LOG1 : �tat de la boite : vrai ferm�e
	//	REG_NUM1 : code erreur

	// REG_LOG1 : demande l'ouverture de la boite ?
	if (CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG1))
		{
		// oui => r�cup�re les param�tres
		// REG_MES1 contient le chemin de s�lection
		CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
		// REG_MES2 contient le type de fichier a chercher (*.* pris si"")
		CPcsVarEx::pszGetValVarSysMesEx (mes_tempo2, VA_SYS_REG_MES2);

		// ouverture de la boite
		word_tempo = wDlgNomFichierPcs (TRUE,	// ouvre la boite
						mes_tempo1,	// path
						mes_tempo2);	// type fic
		// met � jour le r�sultat et le path de recherche utilis�
		CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)word_tempo);
		CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES1, mes_tempo1);
		}
	else
		{
		// non => demande l'�tat de la boite
		word_tempo = wDlgNomFichierPcs (FALSE,	  // demande si boite s'est ferm�e
						mes_tempo1, // nom du fic
						NULL);	  // rien
		CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)word_tempo);
		CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, (word_tempo != 0));
		if (word_tempo == 1)
			{
			CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES1, mes_tempo1);
			}
		}
	}

// Appel dll user
void ExecuteF39 (void)
	{
	// pour execute_f 39
	HANDLE hProcessyn = hMuxExProcessyn ();
	FARPROC pfnDllUserEntry = ContexteExecuteF.ContexteDLLUser.aLiensDLLUsrNT[APPEL].pfn;

	// lien avec le point d'entr�e Ok ?
	if (pfnDllUserEntry)
		{
		// oui => appel fonction dans DLL Utilisateur
		// en lui passant le handle du multiplexeur de services
		// pour que celle ci puisse appeler des services de Processyn
		((PFN_PCSENTRY) pfnDllUserEntry) (hProcessyn);
		}
	}

// Appel d'un programme externe
void ExecuteF40 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];
	char  mes_tempo2[nb_max_caractere+1];

  CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
  CPcsVarEx::pszGetValVarSysMesEx (mes_tempo2, VA_SYS_REG_MES2);
	DWORD	dwTemp = ContexteExecuteF.pExecuteProgrammes->dwLance (mes_tempo1, mes_tempo2);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)dwTemp);
	}

// Test etat execution d'un programme externe
void ExecuteF41 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];

	CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
	BOOL bRes = ContexteExecuteF.pExecuteProgrammes->bExecutionEnCours (mes_tempo1);
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, bRes);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)NO_ERROR);
	}

// Passage en avant plan d'un programme externe
void ExecuteF42 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];
	CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
  DWORD dwTemp = ContexteExecuteF.pExecuteProgrammes->dwActive (mes_tempo1);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)dwTemp);
	}

// Arret d'un programme externe
void ExecuteF43 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];
	CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
  DWORD dwTemp = ContexteExecuteF.pExecuteProgrammes->dwArrete (mes_tempo1);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)dwTemp);
	}

// Lib�ration d'un programme externe
void ExecuteF44 (void)
	{
	char  mes_tempo1[nb_max_caractere+1];
	CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);
  DWORD dwTemp = ContexteExecuteF.pExecuteProgrammes->dwLibere (mes_tempo1);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)dwTemp);
	}

// Joue un son  (si un autre process n'est pas en train de jouer)
void ExecuteF45 (void)
	{
	BOOL	bOk = FALSE;
	DWORD dwFlagSonProcessyn;
	DWORD	dwTypeDeSon;
	PSTR	pszNomSon = NULL;
	char  mes_tempo1[nb_max_caractere+1];

	// Signification des Bits de REG_NUM1	
	#define BIT_JOUE_SON_SYNCHRONE		1	// Bit 0 : 1 jouer synchrone (garde la main pendant l'ex�cution) - 0 asynchrone
	#define BIT_JOUE_SON_EN_BOUCLE		2	// Bit 1 : 1 jouer continuement en boucle (en asynchrone) - 0 jouer une fois
	#define BIT_JOUE_SANS_SON_DEFAUT	4	// Bit 2 : 1 ne pas jouer de son par d�faut  - 0 jouer un son par d�faut si son introuvable
	#define BIT_JOUE_SON_SI_LIBRE			8	// Bit 3 : 1 ne jouer que si pas de son en cours d'ex�cution - 0 jouer un son et arr�ter le son asynchrone en cours d'ex�cution
	#define BIT_JOUE_ALIAS_SON				16// Bit 4 : 1 nom = alias base de registre - 0 nom = Nom d'un fichier
	#define BIT_JOUE_SON_ARRETE				32// Bit 5 : 1 nom = son � arr�ter ou "" pour tous - 0 ne fait rien

	// nom du fichier dans REG_MES1 ou "" pour arr�ter de jouer
  CPcsVarEx::pszGetValVarSysMesEx (mes_tempo1, VA_SYS_REG_MES1);

	// Type de lecture � effectuer dans REG_NUM1
	dwFlagSonProcessyn = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);

	// Conversion flags Processyn en flags OS
	if (dwFlagSonProcessyn & BIT_JOUE_SON_SYNCHRONE)
		dwTypeDeSon = SND_SYNC;
	else
		dwTypeDeSon = SND_ASYNC;

	if (dwFlagSonProcessyn & BIT_JOUE_SON_EN_BOUCLE)
		dwTypeDeSon |= SND_LOOP;

	if (dwFlagSonProcessyn & BIT_JOUE_SANS_SON_DEFAUT)
		dwTypeDeSon |= SND_NODEFAULT;

	if (dwFlagSonProcessyn & BIT_JOUE_SON_SI_LIBRE)
		dwTypeDeSon |= SND_NOSTOP;

	if (dwFlagSonProcessyn & BIT_JOUE_ALIAS_SON)
		dwTypeDeSon |= SND_ALIAS;
	else
		dwTypeDeSon |= SND_FILENAME;

	if (dwFlagSonProcessyn & BIT_JOUE_SON_ARRETE)
		dwTypeDeSon |= SND_PURGE;

	// Pas de nom de son => nom de son NULL pour arr�ter le son courant
	if (mes_tempo1[0])
		pszNomSon = mes_tempo1;

	// demande � l'OS de "jouer" le fichier en asynchrone
	bOk = PlaySound (pszNomSon, NULL, dwTypeDeSon);

	// R�sultat dans REG_LOG1
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, bOk);
  }

// R�cup�re le texte d'une erreur OS
// num�ro d'erreur OS (dans REG_NUM1)
// r�sultat mis dans REG_LOG1 : si TRUE :  => message mis dans REG_MES1 (termin� par ... si coup�)
void ExecuteF46 (void)
	{
	char	szTemp[256];
	// nota : les erreurs syst�me OLE d�marrant par 0x80000000 sont introduites comme valeurs n�gatives
	DWORD dwErr = (DWORD)(LONG)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);

	BOOL bOk = bChaineErreurSysteme (szTemp, sizeof(szTemp), dwErr);

	if (bOk)
		{
		// Message trop long pour Processyn ?
		if (StrLength (szTemp) >= nb_max_caractere)
			{
			// oui => coupe et copie ... � la fin
			StrCopy (&szTemp [nb_max_caractere - 4], "...");
			}
		// Met � jour le r�sultat
		CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES1, szTemp);
		}

	// R�sultat dans REG_LOG1
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, bOk);
	}

//-----------------------------------------------------------------------------------
// Ex�cution synchrone DDE : ouverture / fermeture de conversation avec un serveur pour un topic
// en entr�e
// REG_NUM1 = Num�ro de conversation (de 1 � 32) : il peut y avoir au plus 32 conversations ouvertes simultan�m�nt.
// REG_MES1 = Nom du serveur
// REG_MES2 = Nom du topic
// REG_NUM2 = Mode de fonctionnement : 
//	0 Mode Automatique : l'ouverture ou fermeture de conversation sont faites avec les param�tres courants au fil de l'eau
//	1 ouverture imm�diate de la conversation (mode non automatique).
//	2 fermeture imm�diate de la conversation (mode non automatique).
// REG_NUM3 Time out des commandes en millisecondes. Si 0 = une minute

// en sortie
// REG_LOG1 = TRUE si odre pris en compte - FALSE sinon
// REG_NUM2 = Num�ro d'erreur OS (0 si Ok)
// Remarque : il n'est possible d'avoir qu'une seule conversation 
// avec un serveur, � un instant donn�.
// L'appel de cette fonction ferme le cas �ch�ant la conversation en cours pour ce num�ro de conversation.
// Chaque action ult�rieure sur une conversation ouverte en mode auto sera pr�c�d�e d'une 
// tentative d'ouverture de la conversation si celle ci n'est pas d�ja ouverte (premier acc�s ou rupture de connection par le serveur).
void ExecuteF50 (void)
	{
	DWORD dwErr = NO_ERROR;	// Num�ro d'erreur OS

	// Num�ro de conversation invalide ?
	DWORD dwNConversation = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1))-1;
	if (dwNConversation >= NB_CLIENTS_DDE_EX)
		// oui => erreur
		dwErr = ERROR_INVALID_PARAMETER;
	else
		{
		// non => continue
		PCLIENT_DDE_EX pClientDDE = &ContexteExecuteF.ContexteDDE.aClientsDDE[dwNConversation];

		// Mode de fonctionnement et time out dans REG_NUM2 et REG_NUM3
		DWORD dwCommande = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
		DWORD dwTimeOutMs = (DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);
		if (dwTimeOutMs == 0)
			dwTimeOutMs = TIMEOUT_DDE_PAR_DEFAUT_MS;


		// En premier : fermeture de la conversation en cours
		if (!bFermeConversationOuverte(pClientDDE))
			dwErr = uDerniereErreurDDE();

		switch (dwCommande)
			{
			case 0: // mode ouverture fermeture auto
				{
				// Enregistre les param�tres
				CPcsVarEx::pszGetValVarSysMesEx (pClientDDE->szServeur, VA_SYS_REG_MES1);
				CPcsVarEx::pszGetValVarSysMesEx (pClientDDE->szTopic, VA_SYS_REG_MES2);
				pClientDDE->bModeGestionAuto = TRUE;
				pClientDDE->dwTimeOutMs = dwTimeOutMs;
				}
				break;
			case 1: // ouverture imm�diate
				{
				// fin du mode auto
				pClientDDE->bModeGestionAuto = FALSE;
				// Enregistre les param�tres
				CPcsVarEx::pszGetValVarSysMesEx (pClientDDE->szServeur, VA_SYS_REG_MES1);
				CPcsVarEx::pszGetValVarSysMesEx (pClientDDE->szTopic, VA_SYS_REG_MES2);
				pClientDDE->dwTimeOutMs = dwTimeOutMs;
				if (!bOuvreConversation(pClientDDE))
					dwErr = uDerniereErreurDDE();
				}
				break;
			case 2: // fermeture imm�diate
				{
				// Conversation d�ja ferm�e si elle existait
				// fin du mode auto
				pClientDDE->bModeGestionAuto = FALSE;
				}
				break;
			default:
				dwErr = ERROR_INVALID_PARAMETER;
				break;
			} // switch (dwCommande)
		}

	// Met � jour Ok dans REG_LOG1 et num�ro d'erreur OS dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, dwErr == NO_ERROR);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwErr);
	} // ExecuteF50

//-----------------------------------------------------------------------------------
// Ex�cution synchrone imm�diate de la commande DDE Peek
// sur la conversation courante.
// en entr�e
// REG_NUM1 = Num�ro de conversation (de 1 � 32). La conversation avec serveur et topic doit �tre couramment ouverte.
// REG_MES1 = Nom de l'item � lire
// en sortie
// REG_LOG1 = si TRUE lecture de l'item effectu�e FALSE sinon
// REG_NUM2 = num�ro d'erreur OS
// REG_MES2 = Texte lu si pas d'erreur
// Remarque = l'item est demand� sur le serveur et le topic de la conversation ouverte en cours
void ExecuteF51 (void)
	{
	DWORD dwErr = NO_ERROR;

	// Num�ro de conversation invalide ?
	DWORD dwNConversation = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1))-1;
	if (dwNConversation >= NB_CLIENTS_DDE_EX)
		// oui => erreur
		dwErr = ERROR_INVALID_PARAMETER;
	else
		{
		// non => ouverture auto de la conversation si n�cessaire sans erreur ?
		PCLIENT_DDE_EX pClientDDE = &ContexteExecuteF.ContexteDDE.aClientsDDE[dwNConversation];
		dwErr = uOuvreConversationDDEEnModeAuto (pClientDDE);
		if (!dwErr)
			{
			// oui => nom item dans REG_MES1
			char szItem[nb_max_caractere+1];
			CPcsVarEx::pszGetValVarSysMesEx (szItem, VA_SYS_REG_MES1);

			// requete au serveur
			char szTexte[nb_max_caractere+1] = "";
			if (bRequeteTexteDDE (pClientDDE->pEnvConvClient, szItem,	szTexte, nb_max_caractere+1, pClientDDE->dwTimeOutMs))
				CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES2, szTexte);
			else
				dwErr = uDerniereErreurDDE ();
			}
		}

	// Met � jour Ok dans REG_LOG1 et num�ro d'erreur OS dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, dwErr == NO_ERROR);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwErr);
	} // ExecuteF51

//-----------------------------------------------------------------------------------
// Ex�cution synchrone imm�diate de la commande DDE Poke
// sur la conversation courante.
// en entr�e :
// REG_NUM1 = Num�ro de conversation (de 1 � 32). La conversation avec serveur et topic doit �tre couramment ouverte.
// REG_MES1 = Nom de l'item � lire
// REG_MES2 = Texte � envoyer
// en sortie :
// REG_LOG1 = TRUE texte envoy� pris en compte par le serveur, FALSE sinon
// REG_NUM2 = Num�ro d'erreur OS
// Remarque : l'item est envoy� au serveur et topic de la conversation ouverte en cours
void ExecuteF52 (void)
	{
	DWORD dwErr = NO_ERROR;

	// Num�ro de conversation invalide ?
	DWORD dwNConversation = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1))-1;
	if (dwNConversation >= NB_CLIENTS_DDE_EX)
		// oui => erreur
		dwErr = ERROR_INVALID_PARAMETER;
	else
		{
		// non => ouverture auto de la conversation si n�cessaire sans erreur ?
		PCLIENT_DDE_EX pClientDDE = &ContexteExecuteF.ContexteDDE.aClientsDDE[dwNConversation];
		dwErr = uOuvreConversationDDEEnModeAuto (pClientDDE);
		if (!dwErr)
			{
			// oui => nom item et texte � envoyer dans REG_MES1 et REG_MES2
			char szItem[nb_max_caractere+1];
			char szTexte[nb_max_caractere+1];
			CPcsVarEx::pszGetValVarSysMesEx (szItem, VA_SYS_REG_MES1);
			CPcsVarEx::pszGetValVarSysMesEx (szTexte, VA_SYS_REG_MES2);

			// Envoi au serveur :
			if (!bPokeTexteDDE (pClientDDE->pEnvConvClient, szItem,	szTexte, pClientDDE->dwTimeOutMs))
				dwErr = uDerniereErreurDDE ();
			}
		}

	// Met � jour Ok dans REG_LOG1 et num�ro d'erreur OS dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, dwErr == NO_ERROR);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwErr);
	} // ExecuteF52

//-----------------------------------------------------------------------------------
// Ex�cution synchrone imm�diate de la commande Execute DDE
// sur la conversation courante.
// commande prise dans REG_MES1 accept� par le serveur
// en entr�e :
// REG_NUM1 = Num�ro de conversation (de 1 � 32). La conversation avec serveur et topic doit �tre couramment ouverte.
// REG_MES2 = Texte de la commande � faire ex�cuter
// en sortie :
// REG_LOG1 = TRUE si commande envoy�e prise en compte par le serveur, FALSE sinon
// REG_NUM2 = num�ro d'erreur OS
// Remarque : la commande est envoy� au serveur et topic de la conversation ouverte en cours
void ExecuteF53 (void)
	{
	DWORD dwErr = NO_ERROR;

	// Num�ro de conversation invalide ?
	DWORD dwNConversation = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1))-1;
	if (dwNConversation >= NB_CLIENTS_DDE_EX)
		// oui => erreur
		dwErr = ERROR_INVALID_PARAMETER;
	else
		{
		// non => ouverture auto de la conversation si n�cessaire sans erreur ?
		PCLIENT_DDE_EX pClientDDE = &ContexteExecuteF.ContexteDDE.aClientsDDE[dwNConversation];
		dwErr = uOuvreConversationDDEEnModeAuto (pClientDDE);
		if (!dwErr)
			{
			// oui => texte � envoyer REG_MES2
			char szTexte[nb_max_caractere+1];
			CPcsVarEx::pszGetValVarSysMesEx (szTexte, VA_SYS_REG_MES2);

			// Envoi au serveur
			if (!bExecuteCommandeTexteDDE (pClientDDE->pEnvConvClient, szTexte, pClientDDE->dwTimeOutMs))
				dwErr = uDerniereErreurDDE ();
			}
		}

	// Met � jour Ok dans REG_LOG1 et num�ro d'erreur OS dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, dwErr == NO_ERROR);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwErr);
	} // ExecuteF53


//-----------------------------------------------------------------------------------
// Inhibition/restauration des actions effectu�es
// par appui des touches CRTL+ESC, ALT+ESC, ALT+TAB, Touche"windows", ALT+F4
// en entr�e :
// REG_LOG1 : si true, inhibition des touches CRTL+ESC, ALT+ESC, ALT+TAB, Touche"windows"
// REG_LOG2 : si true, inhibition ALT+F4
// REG_LOG1 et REG_LOG2 : False simultan�ment, restauration action sur touche
// en sortie :
// REG_NUM1 : 0 si Ok 0xFFFF si Dll non trouv�
void ExecuteF54 (void)
	{
	// pour execute_f 54
	FARPROC pfnDllLiclockEntry = ContexteExecuteF.ContexteDLLLiclock.aLiensDLLLiclock[APPEL_INHIBITION].pfn;

	// lien avec le point d'entr�e Ok ?
	if (pfnDllLiclockEntry)
		{
		// oui => appel fonction dans DLL Utilisateur
		((PFN_LICLOCKENTRY) pfnDllLiclockEntry) (CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG1),CPcsVarEx::bGetValVarSysLogEx (VA_SYS_REG_LOG2));
		CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, 0);
		}
	else
		{
		CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, 0xFFFF);
		}
	}

// Tri de tableau bool�en pour ExecuteF 60 � 64
// dwPosIndexDansBlocNum = 0 => pas d'indexation
static void TriVarTableauLog(DWORD dwPosTriDansBloc, DWORD dwNbItemsATrier, DWORD dwOffsetIndex, DWORD dwPosIndexDansBlocNum, DWORD &dwNbItemsModifies, BOOL bCroissant)
	{
	// D�clare un tableau de [valeur, index] de la m�me taille que le tableau � trier
	typedef struct
		{
		BOOL bVal;
		DWORD dwIndex;
		} BOOL_INDEX;

	// Dans lequel seront ins�r�s chaque �l�ment de fa�on � ce que les valeurs soient tri�es
	BOOL_INDEX abValTriees[NB_MAX_ELEMENTS_VAR_TABLEAU];
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		abValTriees[dwNItem].bVal = FALSE;
		abValTriees[dwNItem].dwIndex = 0;
		}

	// Ins�re les �l�ments du tableau source au bon emplacement...
	// Pour chaque �l�ment source
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
		BOOL bSource = CPcsVarEx::bGetValVarLogEx (dwPosTriDansBloc + dwNItem); 

		// Par d�faut, insertion � la fin
		DWORD dwNPosInsertion = dwNItem;

		// Recherche croissant ou d�croissant
		if (bCroissant)
			{
			// Trouve dans le tableau le premier �l�ment sup�rieure � source ?
			for (DWORD dwNPos = 0; dwNPos<dwNItem ; dwNPos++)
				{
				// Source < Dest ?
				if (bSource < abValTriees[dwNPos].bVal)
					{
					// Oui => position d'insertion trouv�e
					dwNPosInsertion = dwNPos;
					// recherche termin�e
					break;
					}
				}
			}
		else
			{
			// Trouve dans le tableau le premier �l�ment inf�rieure � source ?
			for (DWORD dwNPos = 0; dwNPos<dwNItem ; dwNPos++)
				{
				// Source > Dest ?
				if (bSource > abValTriees[dwNPos].bVal)
					{
					// Oui => position d'insertion trouv�e
					dwNPosInsertion = dwNPos;
					// recherche termin�e
					break;
					}
				}
			}
		// D�calage d'une position vers la fin des �l�ments suivants la position d'insertion 
		for (DWORD dwNPos = dwNItem; dwNPos>dwNPosInsertion ; dwNPos--)
			{
			abValTriees[dwNPos] = abValTriees[dwNPos-1];
			}

		// Insertion du nouvel �l�ment � la position d'insertion
		abValTriees[dwNPosInsertion].bVal = bSource;
		abValTriees[dwNPosInsertion].dwIndex = 1 + dwNItem + dwOffsetIndex;
		}

	// Transfert des �l�ments du tableau tri� dans le tableau d'origine :
	// Pour l'instant, aucun item de modifi�
	dwNbItemsModifies = 0;

	// Pour chaque �l�ment du Tableau tri�
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
		if (CPcsVarEx::bSetValVarLogExDif (dwPosTriDansBloc + dwNItem, abValTriees[dwNItem].bVal))
			dwNbItemsModifies ++;
		}

	// Transfert des indexs du tableau tri� dans le tableau d'index ?
	if (dwPosIndexDansBlocNum)
		{
		// Oui => pour chaque �l�ment de l'index du Tableau tri�
		for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
			{
			// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
			CPcsVarEx::bSetValVarNumExDif (dwPosIndexDansBlocNum + dwNItem, (FLOAT)abValTriees[dwNItem].dwIndex);
			}
		}
	}

// Tri de tableau num�rique pour ExecuteF 60 � 64
// dwPosIndexDansBlocNum = 0 => pas d'indexation
static void TriVarTableauNum(DWORD dwPosTriDansBloc, DWORD dwNbItemsATrier, DWORD dwOffsetIndex, DWORD dwPosIndexDansBlocNum, DWORD &dwNbItemsModifies, BOOL bCroissant)
	{
	// D�clare un tableau de [valeur, index] de la m�me taille que le tableau � trier
	// Dans lequel seront ins�r�s chaque �l�ment de fa�on � ce que les valeurs soient tri�es
	typedef struct
		{
		FLOAT fVal;
		DWORD dwIndex;
		} NUM_INDEX;
	NUM_INDEX afValTriees[NB_MAX_ELEMENTS_VAR_TABLEAU];
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		afValTriees[dwNItem].fVal = 0;
		afValTriees[dwNItem].dwIndex = 0;
		}

	// Ins�re les �l�ments du tableau source au bon emplacement...
	// Pour chaque �l�ment source
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
		FLOAT fSource = CPcsVarEx::fGetValVarNumEx (dwPosTriDansBloc + dwNItem); 

		// Par d�faut, insertion � la fin
		DWORD dwNPosInsertion = dwNItem;

		// Recherche croissant ou d�croissant
		if (bCroissant)
			{
			// Trouve dans le tableau le premier �l�ment sup�rieure � source ?
			for (DWORD dwNPos = 0; dwNPos<dwNItem ; dwNPos++)
				{
				// Source < Dest ?
				if (fSource < afValTriees[dwNPos].fVal)
					{
					// Oui => position d'insertion trouv�e
					dwNPosInsertion = dwNPos;
					// recherche termin�e
					break;
					}
				}
			}
		else
			{
			// Trouve dans le tableau le premier �l�ment inf�rieure � source ?
			for (DWORD dwNPos = 0; dwNPos<dwNItem ; dwNPos++)
				{
				// Source > Dest ?
				if (fSource > afValTriees[dwNPos].fVal)
					{
					// Oui => position d'insertion trouv�e
					dwNPosInsertion = dwNPos;
					// recherche termin�e
					break;
					}
				}
			}
		// D�calage d'une position vers la fin des �l�ments suivants la position d'insertion 
		for (DWORD dwNPos = dwNItem; dwNPos>dwNPosInsertion ; dwNPos--)
			{
			afValTriees[dwNPos] = afValTriees[dwNPos-1];
			}

		// Insertion du nouvel �l�ment � la position d'insertion
		afValTriees[dwNPosInsertion].fVal = fSource;
		afValTriees[dwNPosInsertion].dwIndex = 1 + dwNItem + dwOffsetIndex;
		}

	// Transfert des �l�ments du tableau tri� dans le tableau d'origine :
	// Pour l'instant, aucun item de modifi�
	dwNbItemsModifies = 0;

	// Pour chaque �l�ment du Tableau tri�
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
		if (CPcsVarEx::bSetValVarNumExDif (dwPosTriDansBloc + dwNItem, afValTriees[dwNItem].fVal))
			dwNbItemsModifies ++;
		}

	// Transfert des indexs du tableau tri� dans le tableau d'index ?
	if (dwPosIndexDansBlocNum)
		{
		// Oui => pour chaque �l�ment de l'index du Tableau tri�
		for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
			{
			// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
			CPcsVarEx::bSetValVarNumExDif (dwPosIndexDansBlocNum + dwNItem, (FLOAT)afValTriees[dwNItem].dwIndex);
			}
		}
	}

// Tri de tableau message Pour ExecuteF 60 � 64
// dwPosIndexDansBlocNum = 0 => pas d'indexation
static void TriVarTableauMes(DWORD dwPosTriDansBloc, DWORD dwNbItemsATrier, DWORD dwOffsetIndex, DWORD dwPosIndexDansBlocNum, DWORD &dwNbItemsModifies, BOOL bCroissant, BOOL bRespecteCasse)
	{
	// D�clare un tableau de [pointeurValeur, index] de la m�me taille que le tableau � trier
	// Dans lequel seront ins�r�s chaque �l�ment de fa�on � ce que les valeurs soient tri�es
	typedef struct
		{
		PSTR pszVal;
		DWORD dwIndex;
		} PSZ_INDEX;
	PSZ_INDEX apszValTriees[NB_MAX_ELEMENTS_VAR_TABLEAU];
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		apszValTriees[dwNItem].pszVal = NULL;
		apszValTriees[dwNItem].dwIndex = 0;
		}

	// Ins�re les �l�ments du tableau source au bon emplacement...
	// Pour chaque �l�ment source
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		CHAR szSource[c_nb_car_ex_mes] = "";
		// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
		CPcsVarEx::pszGetValVarMesEx (szSource, dwPosTriDansBloc + dwNItem); 

		// Par d�faut, insertion � la fin
		DWORD dwNPosInsertion = dwNItem;

		if (bRespecteCasse)
			{
			// Recherche croissant ou d�croissant avec respect de la casse
			if (bCroissant)
				{
				// Trouve dans le tableau le premier �l�ment sup�rieure � source ?
				for (DWORD dwNPos = 0; dwNPos<dwNItem ; dwNPos++)
					{
					PCSTR pszDest = apszValTriees[dwNPos].pszVal;
					// Source < Dest ?
					if (StrCompare(szSource,pszDest)<0)
						{
						// Oui => position d'insertion trouv�e
						dwNPosInsertion = dwNPos;
						// recherche termin�e
						break;
						}
					}
				}
			else
				{
				// Trouve dans le tableau le premier �l�ment inf�rieure � source ?
				for (DWORD dwNPos = 0; dwNPos<dwNItem ; dwNPos++)
					{
					PCSTR pszDest = apszValTriees[dwNPos].pszVal;
					// Source > Dest ?
					if (StrCompare(szSource,pszDest)>0)
						{
						// Oui => position d'insertion trouv�e
						dwNPosInsertion = dwNPos;
						// recherche termin�e
						break;
						}
					}
				}
			}
		else
			{
			// Recherche croissant ou d�croissant sans respect de la casse
			if (bCroissant)
				{
				// Trouve dans le tableau le premier �l�ment sup�rieure � source ?
				for (DWORD dwNPos = 0; dwNPos<dwNItem ; dwNPos++)
					{
					PCSTR pszDest = apszValTriees[dwNPos].pszVal;
					// Source < Dest ?
					INT iComparaison = StrICompare(szSource,pszDest);
					if (iComparaison == 0)
						iComparaison = StrCompare(szSource,pszDest);
					if (iComparaison < 0)
						{
						// Oui => position d'insertion trouv�e
						dwNPosInsertion = dwNPos;
						// recherche termin�e
						break;
						}
					}
				}
			else
				{
				// Trouve dans le tableau le premier �l�ment inf�rieure � source ?
				for (DWORD dwNPos = 0; dwNPos<dwNItem ; dwNPos++)
					{
					PCSTR pszDest = apszValTriees[dwNPos].pszVal;
					// Source > Dest ?
					INT iComparaison = StrICompare(szSource,pszDest);
					if (iComparaison == 0)
						iComparaison = StrCompare(szSource,pszDest);
					if (iComparaison > 0)
						{
						// Oui => position d'insertion trouv�e
						dwNPosInsertion = dwNPos;
						// recherche termin�e
						break;
						}
					}
				}
			}

		// D�calage d'une position vers la fin des �l�ments suivants la position d'insertion 
		for (DWORD dwNPos = dwNItem; dwNPos>dwNPosInsertion ; dwNPos--)
			{
			apszValTriees[dwNPos] = apszValTriees[dwNPos-1];
			}

		// Insertion du nouvel �l�ment � la position d'insertion
		apszValTriees[dwNPosInsertion].pszVal = (PSTR)pMemAlloueInit(c_nb_car_ex_mes, szSource);
		apszValTriees[dwNPosInsertion].dwIndex = 1 + dwNItem + dwOffsetIndex;
		}

	// Transfert des �l�ments du tableau tri� dans le tableau d'origine :
	// Pour l'instant, aucun item de modifi�
	dwNbItemsModifies = 0;

	// Pour chaque �l�ment du Tableau tri�
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
		if (CPcsVarEx::bSetValVarMesExDif (dwPosTriDansBloc + dwNItem, apszValTriees[dwNItem].pszVal))
			dwNbItemsModifies ++;
		}

	// Transfert des indexs du tableau tri� dans le tableau d'index ?
	if (dwPosIndexDansBlocNum)
		{
		// Oui => pour chaque �l�ment de l'index du Tableau tri�
		for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
			{
			// Les �l�ments d'un tableau sont stock�s dans des emplacements successifs (BEURK)
			CPcsVarEx::bSetValVarNumExDif (dwPosIndexDansBlocNum + dwNItem, (FLOAT)apszValTriees[dwNItem].dwIndex);
			}
		}

	// Lib�re le tableau de travail
	for (DWORD dwNItem = 0; dwNItem < dwNbItemsATrier; dwNItem++)
		{
		MemLibere((PVOID *)&(apszValTriees[dwNItem].pszVal));
		}
	}

//-----------------------------------------------------------------------------------
// Tri de tous les �l�ments d'une variable tableau (quelque soit son type)
// en entr�e :
// REG_MES1 = Nom du tableau � trier (TOR, num�rique ou message)
// REG_LOG1 = TRUE => tri par ordre croissant, FALSE = d�croissant
// REG_LOG2 = Respect de la casse pour les variables message (ignor� pour les autres types):
//						TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM2 = Nombre d'�l�ments du tableau modifi�s par le tri
void ExecuteF60 (void)
	{
	BOOL bOk = FALSE;
	DWORD dwNbItemsModifies = 0;

	// R�cup�ration des param�tres
	char szNomTableauTri[nb_max_caractere+1]="";
	CPcsVarEx::pszGetValVarSysMesEx (szNomTableauTri, VA_SYS_REG_MES1);
	BOOL bTriCroissant = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG1);
	BOOL bRespecteCasse = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG2);

	// Variable trouv�e, de type tableau, nb d'�l�ments � trier Ok ?
	DWORD dwNbItemsTableauTri = 0; // nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	DWORD dwNBlocVarTableauTri = 0; // genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	DWORD dwPosTableauTriDansBloc = CPcsVarEx::dwGetVarNomTailleEx	(szNomTableauTri, &dwNBlocVarTableauTri, &dwNbItemsTableauTri);
	if (dwPosTableauTriDansBloc && dwNbItemsTableauTri)
		{
		// oui => Nb d'�l�ments � trier Ok ?
		if (dwNbItemsTableauTri <= NB_MAX_ELEMENTS_VAR_TABLEAU)
			{
			bOk = TRUE;
				{
				// quelque chose � trier ?
				if (dwNbItemsTableauTri)
					{
					// oui => lance le tri selon le type de variable
					switch (dwNBlocVarTableauTri)
						{
						case b_cour_log:
							{
							TriVarTableauLog(dwPosTableauTriDansBloc, dwNbItemsTableauTri, 0, 0, dwNbItemsModifies, bTriCroissant);
							}
						break;
						case b_cour_num:
							{
							TriVarTableauNum(dwPosTableauTriDansBloc, dwNbItemsTableauTri, 0, 0, dwNbItemsModifies, bTriCroissant);
							}
						break;
						case b_cour_mes:
							{
							TriVarTableauMes(dwPosTableauTriDansBloc, dwNbItemsTableauTri, 0, 0, dwNbItemsModifies, bTriCroissant, bRespecteCasse);
							}
						break;
						}
					}
				}
			}
		}
	
	// Termin� : met � jour Ok dans REG_LOG2 et nb d'�l�ments modifi�s dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG2, bOk);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwNbItemsModifies);
	} // ExecuteF60

//-----------------------------------------------------------------------------------
// Tri d'une partie des �l�ments d'une variable tableau
// en entr�e :
// REG_MES1 = Nom du tableau � trier (TOR, num�rique ou message)
// REG_NUM1 = Num�ro du premier �l�ment du tableau � trier (1 � nb �l�ments max du tableau)
// REG_NUM2 = Nombre d'�l�ments dans le tableau � trier (de REG_NUM1 REG_NUM1 - 1 + nb �l�ments max du tableau)
// REG_LOG1 = TRUE => tri par ordre croissant, FALSE = d�croissant
// REG_LOG2 = Respect de la casse pour les variables message (ignor� pour les autres types):
//						TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM2 = Nombre d'�l�ments du tableau modifi�s par le tri
void ExecuteF61 (void)
	{
	BOOL bOk = FALSE;
	DWORD dwNbItemsModifies = 0;

	// R�cup�ration des param�tres
	char szNomTableauTri[nb_max_caractere+1]="";
	CPcsVarEx::pszGetValVarSysMesEx (szNomTableauTri, VA_SYS_REG_MES1);
	DWORD dwNPremierItemATrier = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1));
	DWORD dwNbItemsATrier = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2));
	BOOL bTriCroissant = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG1);
	BOOL bRespecteCasse = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG2);

	// Variable trouv�e, de type tableau, nb d'�l�ments � trier Ok ?
	DWORD dwNbItemsTableauTri = 0; // nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	DWORD dwNBlocVarTableauTri = 0; // genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	DWORD dwPosTableauTriDansBloc = CPcsVarEx::dwGetVarNomTailleEx	(szNomTableauTri, &dwNBlocVarTableauTri, &dwNbItemsTableauTri);
	if ((dwPosTableauTriDansBloc && dwNbItemsTableauTri) && (dwNbItemsTableauTri <= NB_MAX_ELEMENTS_VAR_TABLEAU))
		{
		// oui => Nb d'�l�ments � trier Ok ?
		if ((dwNPremierItemATrier >= 1) && (((dwNPremierItemATrier-1) + dwNbItemsATrier) <= dwNbItemsTableauTri) && (dwNbItemsATrier <= NB_MAX_ELEMENTS_VAR_TABLEAU))
			{
			bOk = TRUE;
				{
				// quelque chose � trier ?
				if (dwNbItemsATrier)
					{
					// oui => lance le tri selon le type de variable
					switch (dwNBlocVarTableauTri)
						{
						case b_cour_log:
							{
							TriVarTableauLog(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, 0, 0, dwNbItemsModifies, bTriCroissant);
							}
						break;
						case b_cour_num:
							{
							TriVarTableauNum(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, 0, 0, dwNbItemsModifies, bTriCroissant);
							}
						break;
						case b_cour_mes:
							{
							TriVarTableauMes(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, 0, 0, dwNbItemsModifies, bTriCroissant, bRespecteCasse);
							}
						break;
						}
					}
				}
			}
		}
	
	// Termin� : met � jour Ok dans REG_LOG2 et nb d'�l�ments modifi�s dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG2, bOk);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwNbItemsModifies);
	} // ExecuteF61

//-----------------------------------------------------------------------------------
// Tri de tous les �l�ments d'une variable tableau (quelque soit son type)
// Avec mise � jour d'un Index num�rique
// en entr�e :
// REG_MES1 = Nom du tableau � trier (TOR, num�rique ou message)
// REG_MES2 = Nom du tableau num�rique devant contenir les indexs du tableau tri�
// ex : Si TableauTri[2] se retrouve en TableauTri[5] apr�s tri, alors TableauIndex[5] = 2
// remarque : la taille de TableauIndex doit �tre sup�rieure ou �gale � celle de TableauTri
// REG_LOG1 = TRUE => tri par ordre croissant, FALSE = d�croissant
// REG_LOG2 = Respect de la casse pour les variables message (ignor� pour les autres types):
//						TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM2 = Nombre d'�l�ments du tableau modifi�s par le tri
void ExecuteF62 (void)
	{
	BOOL bOk = FALSE;
	DWORD dwNbItemsModifies = 0;

	// R�cup�ration des param�tres
	char szNomTableauTri[nb_max_caractere+1]="";
	char szNomTableauIndex[nb_max_caractere+1]="";
	CPcsVarEx::pszGetValVarSysMesEx (szNomTableauTri, VA_SYS_REG_MES1);
	CPcsVarEx::pszGetValVarSysMesEx (szNomTableauIndex, VA_SYS_REG_MES2);
	BOOL bTriCroissant = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG1);
	BOOL bRespecteCasse = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG2);

	// Variables tableau trouv�es, de bons types  ?
	DWORD dwNbItemsTableauTri = 0; // nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	DWORD dwNBlocVarTableauTri = 0; // genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	DWORD dwPosTableauTriDansBloc = CPcsVarEx::dwGetVarNomTailleEx	(szNomTableauTri, &dwNBlocVarTableauTri, &dwNbItemsTableauTri);

	DWORD dwNbItemsTableauIndex = 0; // nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	DWORD dwNBlocVarTableauIndex = 0; // genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	DWORD dwPosTableauIndexDansBloc = CPcsVarEx::dwGetVarNomTailleEx	(szNomTableauIndex, &dwNBlocVarTableauIndex, &dwNbItemsTableauIndex);

	if ((dwPosTableauTriDansBloc && dwNbItemsTableauTri) && 
		(dwPosTableauIndexDansBloc && dwNbItemsTableauIndex) &&
		(dwNBlocVarTableauIndex == b_cour_num))
		{
		// oui => autres param�tres de tri Ok ?
		if ((dwNbItemsTableauTri <= NB_MAX_ELEMENTS_VAR_TABLEAU) && 
			(dwNbItemsTableauIndex >= dwNbItemsTableauTri))
			{
			bOk = TRUE;
				{
				// quelque chose � trier ?
				if (dwNbItemsTableauTri)
					{
					// oui => lance le tri selon le type de variable
					switch (dwNBlocVarTableauTri)
						{
						case b_cour_log:
							{
							TriVarTableauLog(dwPosTableauTriDansBloc, dwNbItemsTableauTri, 0, dwPosTableauIndexDansBloc, dwNbItemsModifies, bTriCroissant);
							}
						break;
						case b_cour_num:
							{
							TriVarTableauNum(dwPosTableauTriDansBloc, dwNbItemsTableauTri, 0, dwPosTableauIndexDansBloc, dwNbItemsModifies, bTriCroissant);
							}
						break;
						case b_cour_mes:
							{
							TriVarTableauMes(dwPosTableauTriDansBloc, dwNbItemsTableauTri, 0, dwPosTableauIndexDansBloc, dwNbItemsModifies, bTriCroissant, bRespecteCasse);
							}
						break;
						}
					}
				}
			}
		}
	
	// Termin� : met � jour Ok dans REG_LOG2 et nb d'�l�ments modifi�s dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG2, bOk);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwNbItemsModifies);
	} // ExecuteF62

//-----------------------------------------------------------------------------------
// Tri d'une partie des �l�ments d'une variable tableau
// en entr�e :
// REG_MES1 = Nom du tableau � trier (TOR, num�rique ou message)
// REG_MES2 = Nom du tableau num�rique devant contenir les indexs du tableau tri�
// ex : Si TableauTri[2] se retrouve en TableauTri[5] apr�s tri, alors TableauIndex[5] = 2
// remarques : 
// la taille de TableauIndex doit �tre sup�rieure ou �gale � celle de TableauTri
// seuls les index correspondants aux �l�ments � trier sont mis � jour dans le tableau d'index,
// les autres n'�tant pas touch�s
// REG_NUM1 = Num�ro du premier �l�ment du tableau � trier (1 � nb �l�ments max du tableau)
// REG_NUM2 = Nombre d'�l�ments dans le tableau � trier (de REG_NUM1 REG_NUM1 - 1 + nb �l�ments max du tableau)
// REG_LOG1 = TRUE => tri par ordre croissant, FALSE = d�croissant
// REG_LOG2 = Respect de la casse pour les variables message (ignor� pour les autres types):
//						TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM2 = Nombre d'�l�ments du tableau modifi�s par le tri
void ExecuteF63 (void)
	{
	BOOL bOk = FALSE;
	DWORD dwNbItemsModifies = 0;

	// R�cup�ration des param�tres
	char szNomTableauTri[nb_max_caractere+1]="";
	char szNomTableauIndex[nb_max_caractere+1]="";
	CPcsVarEx::pszGetValVarSysMesEx (szNomTableauTri, VA_SYS_REG_MES1);
	CPcsVarEx::pszGetValVarSysMesEx (szNomTableauIndex, VA_SYS_REG_MES2);
	DWORD dwNPremierItemATrier = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1));
	DWORD dwNbItemsATrier = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2));
	BOOL bTriCroissant = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG1);
	BOOL bRespecteCasse = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG2);

	// Variables de tableau trouv�es, de bons types ?
	DWORD dwNbItemsTableauTri = 0; // nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	DWORD dwNBlocVarTableauTri = 0; // genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	DWORD dwPosTableauTriDansBloc = CPcsVarEx::dwGetVarNomTailleEx	(szNomTableauTri, &dwNBlocVarTableauTri, &dwNbItemsTableauTri);

	DWORD dwNbItemsTableauIndex = 0; // nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	DWORD dwNBlocVarTableauIndex = 0; // genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	DWORD dwPosTableauIndexDansBloc = CPcsVarEx::dwGetVarNomTailleEx	(szNomTableauIndex, &dwNBlocVarTableauIndex, &dwNbItemsTableauIndex);

	if ((dwPosTableauTriDansBloc && dwNbItemsTableauTri) && 
		(dwPosTableauIndexDansBloc && dwNbItemsTableauIndex) &&
		(dwNBlocVarTableauIndex == b_cour_num))
		{
		// oui => autres param�tres de tri Ok ?
		if ((dwNbItemsTableauTri <= NB_MAX_ELEMENTS_VAR_TABLEAU) &&
			(dwNbItemsTableauIndex >= dwNbItemsTableauTri) &&		
			(dwNPremierItemATrier >= 1) && 
			(((dwNPremierItemATrier-1) + dwNbItemsATrier) <= dwNbItemsTableauTri) && 
			(dwNbItemsATrier <= NB_MAX_ELEMENTS_VAR_TABLEAU))
			{
			bOk = TRUE;
				{
				// quelque chose � trier ?
				if (dwNbItemsATrier)
					{
					// oui => lance le tri selon le type de variable
					switch (dwNBlocVarTableauTri)
						{
						case b_cour_log:
							{
							TriVarTableauLog(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, dwNPremierItemATrier -1, dwPosTableauIndexDansBloc + dwNPremierItemATrier -1, dwNbItemsModifies, bTriCroissant);
							}
						break;
						case b_cour_num:
							{
							TriVarTableauNum(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, dwNPremierItemATrier -1, dwPosTableauIndexDansBloc + dwNPremierItemATrier -1, dwNbItemsModifies, bTriCroissant);
							}
						break;
						case b_cour_mes:
							{
							TriVarTableauMes(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, dwNPremierItemATrier -1, dwPosTableauIndexDansBloc + dwNPremierItemATrier -1, dwNbItemsModifies, bTriCroissant, bRespecteCasse);
							}
						break;
						}
					}
				}
			}
		}
	
	// Termin� : met � jour Ok dans REG_LOG2 et nb d'�l�ments modifi�s dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG2, bOk);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwNbItemsModifies);
	} // ExecuteF63

// Initialisation envoi mail
void ExecuteF64()
	{
	BOOL bOk = FALSE;
	DWORD dwNbItemsModifies = 0;

	// R�cup�ration des param�tres
	char szNomTableauTri[nb_max_caractere+1]="";
	char szNomTableauIndex[nb_max_caractere+1]="";
	CPcsVarEx::pszGetValVarSysMesEx (szNomTableauTri, VA_SYS_REG_MES1);
	CPcsVarEx::pszGetValVarSysMesEx (szNomTableauIndex, VA_SYS_REG_MES2);
	DWORD dwNPremierItemATrier = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1));
	DWORD dwNbItemsATrier = ((DWORD)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2));
	BOOL bTriCroissant = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG1);
	BOOL bRespecteCasse = CPcsVarEx::bGetValVarSysLogEx(VA_SYS_REG_LOG2);

	//PcsMail::MonTest *ptr = NULL;
	//CoInitialize(NULL);
	//// CLSID_Out 
	//HRESULT hr = 0; //$$$$ CoCreateInstance("0D0F7000-788D-4b7f-AB96-178A2BCA9C66",NULL,CLSCTX_INPROC_SERVER,PcsMail::MonTest,reinterpret_cast<void**>(&ptr));
	//if (FAILED(hr))
	//	{
	//	}
	//else
	//	{
	//	}
	//CoUninitialize();

	//// Variables de tableau trouv�es, de bons types ?
	//DWORD dwNbItemsTableauTri = 0; // nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	//DWORD dwNBlocVarTableauTri = 0; // genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	//DWORD dwPosTableauTriDansBloc = CPcsVarEx::dwGetVarNomTailleEx	(szNomTableauTri, &dwNBlocVarTableauTri, &dwNbItemsTableauTri);

	//DWORD dwNbItemsTableauIndex = 0; // nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	//DWORD dwNBlocVarTableauIndex = 0; // genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	//DWORD dwPosTableauIndexDansBloc = CPcsVarEx::dwGetVarNomTailleEx	(szNomTableauIndex, &dwNBlocVarTableauIndex, &dwNbItemsTableauIndex);

	//if ((dwPosTableauTriDansBloc && dwNbItemsTableauTri) && 
	//	(dwPosTableauIndexDansBloc && dwNbItemsTableauIndex) &&
	//	(dwNBlocVarTableauIndex == b_cour_num))
	//	{
	//	// oui => autres param�tres de tri Ok ?
	//	if ((dwNbItemsTableauTri <= NB_MAX_ELEMENTS_VAR_TABLEAU) &&
	//		(dwNbItemsTableauIndex >= dwNbItemsTableauTri) &&		
	//		(dwNPremierItemATrier >= 1) && 
	//		(((dwNPremierItemATrier-1) + dwNbItemsATrier) <= dwNbItemsTableauTri) && 
	//		(dwNbItemsATrier <= NB_MAX_ELEMENTS_VAR_TABLEAU))
	//		{
	//		bOk = TRUE;
	//			{
	//			// quelque chose � trier ?
	//			if (dwNbItemsATrier)
	//				{
	//				// oui => lance le tri selon le type de variable
	//				switch (dwNBlocVarTableauTri)
	//					{
	//					case b_cour_log:
	//						{
	//						TriVarTableauLog(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, dwNPremierItemATrier -1, dwPosTableauIndexDansBloc + dwNPremierItemATrier -1, dwNbItemsModifies, bTriCroissant);
	//						}
	//					break;
	//					case b_cour_num:
	//						{
	//						TriVarTableauNum(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, dwNPremierItemATrier -1, dwPosTableauIndexDansBloc + dwNPremierItemATrier -1, dwNbItemsModifies, bTriCroissant);
	//						}
	//					break;
	//					case b_cour_mes:
	//						{
	//						TriVarTableauMes(dwPosTableauTriDansBloc + dwNPremierItemATrier -1, dwNbItemsATrier, dwNPremierItemATrier -1, dwPosTableauIndexDansBloc + dwNPremierItemATrier -1, dwNbItemsModifies, bTriCroissant, bRespecteCasse);
	//						}
	//					break;
	//					}
	//				}
	//			}
	//		}
	//	}
	
	// Termin� : met � jour Ok dans REG_LOG2 et nb d'�l�ments modifi�s dans REG_NUM2
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG2, bOk);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)dwNbItemsModifies);
	} // ExecuteF64

static double const d1E6 = 1000000.0;
static void MuVersDouble(FLOAT MSource, FLOAT USource,double & dDest)
	{
	dDest = ((double)USource)+((double)MSource)*d1E6;
	}

static void DoubleVersMu(double dSource, FLOAT &MDest, FLOAT & UDest)
	{
	long long dH =((long long)(dSource/d1E6))*(long long)d1E6;//d1E6*Round(dSource/d1E6);

	UDest = (FLOAT) (dSource-dH);
	MDest=(FLOAT)(dH/d1E6);
	}

// ExecuteF70 MU1=MU1+MU2
// ExecuteF71 MU1=MU1-MU2
// ExecuteF72 MU1=MU1xMU2
// ExecuteF73 MU1=MU1/MU2
// ExecuteF74 MU1=MU1 Modulo MU2 (division enti�re)
// en entr�e :
// REG_NUM1 = U1
// REG_NUM2 = M1
// REG_NUM3 = U2
// REG_NUM4 = M2
// en sortie :
// REG_NUM1 = U r�sultat
// REG_NUM2 = M r�sultat
// REG_LOG1 = diviseur <>0 
void ExecuteF70 (void)
	{
	double d1=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1), d1);
	double d2=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM4),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3),d2);
	FLOAT rM=0;
	FLOAT rU=0;
	DoubleVersMu(d1+d2,rM,rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, rM);
	}

void ExecuteF71 (void)
	{
	double d1=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1), d1);
	double d2=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM4),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3),d2);
	FLOAT rM=0;
	FLOAT rU=0;
	DoubleVersMu(d1-d2,rM,rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, rM);
	}
void ExecuteF72 (void)
	{
	double d1=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1), d1);
	double d2=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM4),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3),d2);
	FLOAT rM=0;
	FLOAT rU=0;
	DoubleVersMu(d1*d2,rM,rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, rM);
	}
void ExecuteF73 (void)
	{
	double d1=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1), d1);
	double d2=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM4),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3),d2);
	FLOAT rM=0;
	FLOAT rU=0;
	BOOL bRes=d2!=0;
	if (bRes)
		{
		DoubleVersMu(d1/d2,rM,rU);
		}
	else
		{
		DoubleVersMu(0,rM,rU);
		}
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, bRes);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, rM);
	}
void ExecuteF74 (void)
	{
	double d1=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1), d1);
	double d2=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM4),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3),d2);
	FLOAT rM=0;
	FLOAT rU=0;
	BOOL bRes=d2!=0;
	if (bRes)
		{
		DoubleVersMu(fmod(d1,d2),rM,rU);
		}
	else
		{
		DoubleVersMu(0,rM,rU);
		}
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, bRes);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, rM);
	}

// ExecuteF75 Conversion de MU1 en un double mot non sign� 32 bits, r�parti sur WL, WH avec WL = partie basse 16 bit du double mot et WH partie haute 16 bit du double mot
// en entr�e :
// REG_NUM1 = U1
// REG_NUM2 = M1
// en sortie :
// REG_NUM1 = WL
// REG_NUM2 = WH
void ExecuteF75 (void)
	{
	double d1=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1), d1);
	long long lRes=(long long)d1;
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, (FLOAT)(lRes & 65535));
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, (FLOAT)((lRes>>16) & 65535));
	}

// ExecuteF76 Conversion d'un Double mot DW 32 bit en MU, avec MU = 65536x(WH Mot 16 bit) + WL (Mot 16 bit)
// en entr�e :
// REG_NUM1 = WL
// REG_NUM2 = WH
// en sortie :
// REG_NUM1 = U r�sultat
// REG_NUM2 = M r�sultat
void ExecuteF76 (void)
	{
	long long dH=(long long)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2);
	long long dL=(long long)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1);
	long long lRes=(long long)dL + (((long long)dH)*65536);
	double d1=(double)lRes;
	FLOAT rM=0;
	FLOAT rU=0;
	DoubleVersMu(d1,rM,rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, rM);
	}

//---------------------------------------------------------
// Formate dans tous les cas un r�el double pour affichage dans Processyn
// Renvoie FALSE si le format d�sir� n'est pas atteint
static BOOL bFormatteDoubleProcessyn (PSTR pszResultat, int NbCarsMax, int NbDecimales, BOOL bNumScientifique, DOUBLE dVal)
	{
	DWORD dwTailleResultat = 0;
#define NB_CARS_NUMERIQUE_MAX 60
	// Blindage
	if (NbDecimales > (NB_CARS_NUMERIQUE_MAX-18))
		NbDecimales = (NB_CARS_NUMERIQUE_MAX-18);
	else
		{
		if (NbDecimales < 0)
			NbDecimales = 0;
		}

	// Demande d'un affichage en scientifique ?
	if (bNumScientifique)
		{
		// oui => conversion au format scientifique en sp�cifiant le nombre de chiffres apr�s la virgule
		StrPrintFormat (pszResultat, "%.*E", NbDecimales, dVal);
		dwTailleResultat = StrLength (pszResultat);;
		}
	else
		{
		// non => conversion de la valeur en une chaine au format d�cimal (forme NNN.DDD)
		StrPrintFormat (pszResultat, "%.*f", NbDecimales, dVal);

		// chaine d�passe la taille sp�cifi�e ?
		dwTailleResultat = StrLength (pszResultat);
		if (dwTailleResultat > (DWORD) NbCarsMax)
			{
			// oui => un format scientifique N.DDDDDDE+XXX peut �tre plus compact ?
			if (dwTailleResultat > 18)
				{
				// conversion au format scientifique (6 chiffres significatifs)
				char  szScientifique[NB_CARS_NUMERIQUE_MAX];
				DWORD dwTailleScientifique;

				StrPrintFormat (szScientifique, "%E", dVal);
				dwTailleScientifique = StrLength (szScientifique);

				// r�sultat plus compact ?
				if (dwTailleScientifique < dwTailleResultat)
					// oui => utilise ce format (pas demise � jour de dwTailleResultat pour garder l'erreur)
					strcpy (pszResultat, szScientifique);
				}
			}
		}

	// Renvoie TRUE si la taille de la chaine formatt�e est valide
	return dwTailleResultat <= (DWORD)NbCarsMax;
	} // bFormatteFloatProcessyn

// ExecuteF77 Conversion de MU1 en chaine de caract�re
// en entr�e :
// REG_NUM1 = U1
// REG_NUM2 = M1
// REG_NUM3 = Nb de caract�res min
// REG_NUM4 = Nb de d�cimales
// en sortie :
// REG_MES1 = conversion chaine de caract�res
void ExecuteF77 (void)
	{
	double d1=0;
	MuVersDouble(CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM2),CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM1), d1);
	char szRes[nb_max_caractere+1]="";
	int NbCarsMax=(int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM3);
	int NbDecimales=(int)CPcsVarEx::fGetValVarSysNumEx (VA_SYS_REG_NUM4);
	//StrDOUBLEToStr(szRes,d1);
	bFormatteDoubleProcessyn(szRes,NbCarsMax,NbDecimales, FALSE,d1);
	CPcsVarEx::SetValVarSysMesEx (VA_SYS_REG_MES1, szRes);
	}

// ExecuteF78 Conversion Chaine de caract�re num�rique vers MU
// en entr�e :
// REG_MES1 = Chaine num�rique � convertir
// en sortie :
// REG_LOG1 = format valide (conversion effectu�e)
// REG_NUM1 = U1 r�sultat
// REG_NUM2 = M1 r�sultat
void ExecuteF78 (void)
	{
	double d1=0;
	char szMes1[nb_max_caractere+1]="";
	CPcsVarEx::pszGetValVarSysMesEx (szMes1, VA_SYS_REG_MES1);
	BOOL bRes = StrToDOUBLE(&d1, szMes1);
	FLOAT rM=0;
	FLOAT rU=0;
	DoubleVersMu(d1,rM,rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM1, rU);
	CPcsVarEx::SetValVarSysNumEx (VA_SYS_REG_NUM2, rM);
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_REG_LOG1, bRes);
	}


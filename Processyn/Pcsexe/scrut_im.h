//
// --------------------------------------------------------------------------
// Status Imprimantes
// --------------------------------------------------------------------------
#define PRN_STATUS_OK                    0x0000
// -------------------------------------------- Imprimante parallele
#define PRN_STATUS_ACKNOWLEDGED          0x0001
#define PRN_STATUS_OUT_OF_PAPER          0x0002
#define PRN_STATUS_NOT_SELECTED          0x0004
#define PRN_STATUS_IO_ERROR              0x0008
#define PRN_STATUS_TIMEOUT               0x0010
#define PRN_STATUS_OTHER_ERROR           0x0020 // $$ nouveau Windows 95


void		ouvrir_imprimantes			(void);
void		fermer_imprimantes			(void);
void    maj_status_imprimante   (UINT wImp, DWORD wStatusToSet);
void    stocker_imprimante      (UINT wNbCar, char *BufCar, BOOL bALaLigne, BOOL bAvecConversion);
void    scruter_imprimantes     (void);
//
BOOL espace_libre_tampon_imprimante (UINT wImp,  DWORD *pwSpaceFree, DWORD * pwTailleOccupee);
BOOL maj_time_out_imprimante        (UINT wImp, DWORD  lwTimeOut);
BOOL initialiser_imprimante         (UINT wImp);
BOOL vide_buffer_imprimante         (UINT wImp);
BOOL ferme_port_imprimante						(UINT dwImp);

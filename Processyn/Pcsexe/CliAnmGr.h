// --------------------------------------------------------------------------
// Partie asynchrone de l'animation de synoptique
// Elle fonctionne en liaison avec scrut_gr.c (PcsExe)
// Version Win32 du 20/2/97 par JS
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
BOOL  bCliAnmGrEnvoiAR (void);
BOOL  bCliAnmGrEnvoiMessage (DWORD wSizeMsg, void * pMsg);

// --------------------------------------------------------------------------
// cr�ation du thread d'animation du graphisme
BOOL bLanceCliAnmGr (PITC_TX_RX pITCServeurAnmGr);

// --------------------------------------------------------------------------
// fermeture du thread d'animation du graphisme
BOOL bFermeCliAnmGr (void);

// Handle de la fen�tre au comportement "boite de dialogue" active
extern HWND	hDlgGrActive;
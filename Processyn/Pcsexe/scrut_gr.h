/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :                                           		|
 |	       								|
 |   Auteur  : LM							|
 |   Date    : 20/04/93 						|
 +----------------------------------------------------------------------*/


//---------------------------- codes erreurs ----------------------------

#define VI_REC_ERR_SIZE_DATA              0X0001
#define VI_REC_ERR_GET_MBX                0X0002
#define VI_REC_ERR_DATA_TYPE              0X0004

#define VI_SEND_ERR_PUT_MBX               0X0008
#define VI_SEND_ERR_DATA_TYPE             0X0010
#define VI_SEND_ERR_SIG_WAIT              0X0020
#define VI_SEND_ERR_WAIT_ACK              0X0040
#define VI_SEND_ERR_RQU_SEMA              0X0080
#define VI_SEND_ERR_ANIM                  0X0100
#define VI_SEND_ERR_PUT_ACK               0X0200
#define VI_SEND_TIMEOUT                   0X0400

#define VI_MBX_ERR_CLOSE                  0X0800

#define VI_PARAM_CB_ARCHIVE               0X1000

#define VI_TIMEOUT_ACK                    10000


//---------------------------- Fonctions ----------------------------
// fonctions
void  initialise_gr (void);
void  termine_gr (void);
void  sorties_gr (BOOL bPremierPassage);
void  recoit_gr (BOOL bPremierPassage);

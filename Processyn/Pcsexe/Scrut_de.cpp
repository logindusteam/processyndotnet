/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 +----------------------------------------------------------------------*/
// Init_de.c
// DDE pour exploitation pcsexe
// Win32 JS 17/7/97

#include "stdafx.h"
#include "std.h"
#include "USem.h"
#include "lng_res.h"
#include "tipe.h"
#include "UStr.h"
#include "MemMan.h"
#include "dicho.h"
#include "tipe_de.h"
#include "ExeDde.h"
#include "mem.h"
#include "Appli.h"
#include "UDde.h"
#include "DocMan.h"
#include "PathMan.h"
#include "USignale.h"
#include "Pcs_Sys.h"
#include "PcsVarEx.h"
#include "InpuEx.h" // bVideQueueDeMessage (void);
#include "scrut_de.h"
#include "Verif.h"
VerifInit;


static PCSTR szNomServeurDDE = "PCSEXE";

// ------------------ gestion des erreurs
#define ERR_DDE_NONE                  0x0
#define ERR_DDE_TIME_OUT              0x1
#define ERR_DDE_CREATE_LINK           0x2
#define ERR_DDE_SRV_TERM_LINK         0x4
#define ERR_DDE_LECTURE               0x8
#define ERR_DDE_ECRITURE             0x10
#define ERR_DDE_DECODE_LOG           0x20
#define ERR_DDE_DECODE_NUM           0x40
#define ERR_DDE_TAILLE_TAB           0x80
#define ERR_DDE_SERVEUR             0x100

//----------------------- constantes
#define CAR_CR  '\015'           //((char)13) en OCTAL
#define CAR_LF  '\012'           //((char)10)
#define CAR_TAB '\011'           //((char)09)
#define CAR_FIN '\000'           //((char)00)

// ---------------------- fonctions
static void test_str_cat (char **pszDest, char *pszChaine);
static DWORD InitialisationLienEntree (DWORD wIndexLien);
static DWORD LienEntree (DWORD wIndexLien);
static DWORD LienReflet (UINT wIndexLien);
static void MajMemLienEntreeDde (void);

//------------------------------------------- PARTIE SERVEUR
//static BOOL ClientPresent (DWORD wPosition, DWORD *wNumClient, void *handleNewClient);
//static int ComparaisonNomVar (void *element_1, UINT numero);

// ----------------------------------------------------------------------
static void maj_statut_de (DWORD statut_de)
  {
  PX_VAR_SYS	position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_status_de);
  PFLOAT  ptr_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, position->dwPosCourEx);
  DWORD w;

  w = (DWORD) (*ptr_num);
  w = w | statut_de;
  (*ptr_num) = (FLOAT)w;
  }

//--------------------------------------------------------------------------
// FONCTIONS UTILISEES POUR LES LIENS PREDEFINIS
//--------------------------------------------------------------------------
// un serveur vient d'arreter le lien, demander s'il faut le sauvegarder
// DdeDelLien sera fait ds la phase des entrees
//--------------------------------------------------------------------------
static void TerminateLienERecu (LPARAM lParam)
  {
  tx_dde_lien_e*pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, (DWORD)lParam);

  pxDdeLienE->bLienOk = FALSE;
  }

//--------------------------------------------------------------------------
// un serveur vient d'arreter le lien, demander s'il faut le sauvegarder
// DdeDelLien sera fait ds la phase des entrees
//--------------------------------------------------------------------------
static void TerminateLienRRecu (LPARAM lParam)
  {
  tx_dde_lien_r*pxDdeLienR = (tx_dde_lien_r*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_r, (DWORD)lParam);

  pxDdeLienR->bLienOk = FALSE;
  }

//--------------------------------------------------------------------------
// indication reception de datas en mode advise
//--------------------------------------------------------------------------
static void DataAdviseRecue (LPARAM lParam, DWORD wEvent, void *pData, DWORD wSizeData)
  {
  switch (wEvent)
    {
    case DDE_DATA_RECU :
			{
			tx_dde_e_advise*pxDdeEAdvise = (tx_dde_e_advise*)pointe_enr (szVERIFSource, __LINE__, bx_dde_e_advise, (DWORD)lParam);

      pxDdeEAdvise->bDataRecue = TRUE;
			}
      break;
    }
  }

//------------------------------------ 
// Mise � jour valeur variables dans BD Pcsexe
DWORD PutValeurBD (DWORD wPosEs, UINT wTaille, DWORD wGenre, char *pszData)
  {
  DWORD     wErreur = 0;
  DWORD     wIndex;
  PBOOL		pbCourLog;
  FLOAT     *rCourNum;
  FLOAT      rValeur;
  PSTR    pszCourMes;
  DWORD     wIndexTmp;
  DWORD     wLongueurTrame;
  DWORD     wNbChamps;
  char     pszTmp[256];

  // enlever les caracteres CR et LF a la fin
  //
  wIndex = StrLength(pszData) - 1;
  if ((pszData[wIndex] == CAR_CR) || (pszData[wIndex] == CAR_LF))
    {
    pszData[wIndex] = '\0';
    wIndex--;
    if ((pszData[wIndex] == CAR_CR) || (pszData[wIndex] == CAR_LF))
      {
      pszData[wIndex] = '\0';
      }
    }
  if (wTaille)
    { // Tableau
    wIndexTmp = 0;
    wLongueurTrame = StrLength (pszData) + 1;
    wIndex = 0;
    wNbChamps = 0;
    while ((wIndex < wLongueurTrame) && (wNbChamps < wTaille))
      {
      if ((pszData[wIndex] == CAR_TAB) || (pszData[wIndex] == CAR_FIN) ||
          (pszData[wIndex] == CAR_CR) || (pszData[wIndex] == CAR_LF))
        { // mettre pszTmp dans BD
        pszTmp[wIndexTmp] = '\0';
        switch (wGenre)
          {
          case b_cour_log :
          if (wIndexTmp == 1)
            {
            pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, wPosEs + wNbChamps);
            if (pszTmp[0] == '0')
              {
              (*pbCourLog) = FALSE;
              }
            else
              {
              (*pbCourLog) = TRUE;
              }
            }
          else
            {
            wErreur = wErreur | ERR_DDE_DECODE_LOG;
            }
            break;

          case b_cour_num :
            if (StrToFLOAT (&rValeur, pszTmp))
              {
              rCourNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, wPosEs + wNbChamps);
              (*rCourNum) = rValeur;
              }
            else
              {
              wErreur = wErreur | ERR_DDE_DECODE_NUM;
              }
            break;

          case b_cour_mes :
            pszCourMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, wPosEs + wNbChamps);
            StrCopy (pszCourMes, pszTmp);
            break;

          default :
            break;
          }
        //
        wIndexTmp = 0;
        pszTmp[0] = '\0';
        wNbChamps++;
        // cas separateur CrLf
        if ((pszData[wIndex + 1] == CAR_CR) || (pszData[wIndex + 1] == CAR_LF))
          {
          wIndex++;
          }
        }
      else
        { // car a concatener
        pszTmp[wIndexTmp++] = pszData[wIndex];
        }
      //
      wIndex++;
      } // fin while
    //
    if (wIndex == wLongueurTrame)
      {
      if (wNbChamps != wTaille)
        { // on a recu moins de valeurs que la taille tableau pcs
        wErreur = wErreur | ERR_DDE_TAILLE_TAB;
        }
      }
    else
      { // on a recu plus de valeurs que la taille tableau pcs
      wErreur = wErreur | ERR_DDE_TAILLE_TAB;
      }
    }
  else
    { // VAR SIMPLE
    switch (wGenre)
      {
      case b_cour_log :
        if (StrLength (pszData) == 1)
          {
          pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, wPosEs);
          if (pszData[0] == '0')
            {
            (*pbCourLog) = FALSE;
            }
          else
            {
            (*pbCourLog) = TRUE;
            }
          }
        else
          {
          wErreur = ERR_DDE_DECODE_LOG;
          }
        break;

      case b_cour_num :
        if (StrToFLOAT (&rValeur, pszData))
          {
          rCourNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, wPosEs);
          (*rCourNum) = rValeur;
          }
        else
          {
          wErreur = ERR_DDE_DECODE_NUM;
          }
        break;

      case b_cour_mes :
        pszCourMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, wPosEs);
        StrCopy (pszCourMes, pszData);
        break;

      default :
        break;
      }
    }

  return wErreur;
  } // PutValeurBD

//-------------------------------------------------------------------------
static void test_str_cat (char **pszDest, char *pszChaine)
  {
  if ((StrLength (*pszDest) + StrLength (pszChaine)) < DDE_MAX_BLOC_SIZE)
    {
    StrConcat ((*pszDest), pszChaine);
    }
  else
    {
    maj_statut_de (ERR_DDE_TAILLE_TAB);
    }
  }

//-----------------------------------------------------------
// R�cuperation des valeur De la BD Pcs
DWORD GetValeurBD (DWORD wPosEs, DWORD wTaille, DWORD wGenre,
                  BOOL bTestRaf, DWORD wPosEsRaf,
                  char *pszData, UINT *wSizeData,
                  BOOL bTestOld, BOOL *bAEnvoyer)
  {
  DWORD   wErreur = 0;
  DWORD   wIndex;
  PBOOL 	pbCourLog;
  PBOOL 	bMemLog;
  PFLOAT 	rCourNum;
  PFLOAT 	rMemNum;
  PSTR	pszCourMes;
  PSTR	pszMemMes;
  char    pszTmp[DDE_MAX_BLOC_SIZE];
  char    pszTab[2];
  BOOL		bTest;

  pszData[0] = '\0';
  (*wSizeData) = 0;
  (*bAEnvoyer) = FALSE;

	// Plusieurs variables ?
  if (wTaille)
    { // Tableau
    pszTab[0] = CAR_TAB;
    pszTab[1] = '\0';
    bTest = TRUE;
    if (bTestRaf)
      {
      pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, wPosEsRaf);
      bTest = (*pbCourLog);
      if (*pbCourLog)
        { // test si la raf vient de passer a haut
        bMemLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, wPosEsRaf);
        (*bAEnvoyer) = (BOOL)((*pbCourLog) != (*bMemLog));
        }
      }
    if (bTest)
      {
      switch (wGenre)
        {
        case b_cour_log :
          for (wIndex = 0; wIndex < wTaille; wIndex++)
            {
            pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, wPosEs + wIndex);
            if (bTestOld && !(*bAEnvoyer))
              {
              bMemLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, wPosEs + wIndex);
              (*bAEnvoyer) = (BOOL)((*pbCourLog) != (*bMemLog));
              }
            //
            if (*pbCourLog)
              {
              test_str_cat (&pszData, "1");
              }
            else
              {
              test_str_cat (&pszData, "0");
              }
            test_str_cat (&pszData, pszTab);
            }
          break;

        case b_cour_num :
          for (wIndex = 0; wIndex < wTaille; wIndex++)
            {
            rCourNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, wPosEs + wIndex);
            if (bTestOld && !(*bAEnvoyer))
              {
              rMemNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, wPosEs + wIndex);
              (*bAEnvoyer) = (BOOL)((*rCourNum) != (*rMemNum));
              }
            //
            StrFLOATToStr (pszTmp, (*rCourNum));
            test_str_cat (&pszData, pszTmp);
            test_str_cat (&pszData, pszTab);
            }
          break;

        case b_cour_mes :
          for (wIndex = 0; wIndex < wTaille; wIndex++)
            {
            pszCourMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, wPosEs + wIndex);
            if (bTestOld && !(*bAEnvoyer))
              {
              pszMemMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes, wPosEs + wIndex);
              (*bAEnvoyer) = (BOOL) (!bStrEgales (pszCourMes, pszMemMes));
              }
            //
            test_str_cat (&pszData, pszCourMes);
            test_str_cat (&pszData, pszTab);
            }
          break;
        }
      //
      pszData[StrLength(pszData) - 1] = '\0'; // enleve le dernier car_tab
      } // var activation a haut
    }
  else
    { // VAR SIMPLE
    switch (wGenre)
      {
      case b_cour_log :
        pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, wPosEs);
        if (bTestOld)
          {
          bMemLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, wPosEs);
          (*bAEnvoyer) = (BOOL)((*pbCourLog) != (*bMemLog));
          }
        else
          {
          (*bAEnvoyer) = TRUE;
          }
        //
        if (*pbCourLog)
          {
          StrCopy (pszData, "1");
          }
        else
          {
          StrCopy (pszData, "0");
          }
        break;

      case b_cour_num :
        rCourNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, wPosEs);
        if (bTestOld)
          {
          rMemNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, wPosEs);
          (*bAEnvoyer) = (BOOL)((*rCourNum) != (*rMemNum));
          }
        else
          {
          (*bAEnvoyer) = TRUE;
          }
        //
        StrFLOATToStr (pszData, (*rCourNum));
        break;

      case b_cour_mes :
        pszCourMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, wPosEs);
        if (bTestOld)
          {
          pszMemMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes, wPosEs);
          (*bAEnvoyer) = (BOOL) (!bStrEgales (pszCourMes, pszMemMes));
          }
        else
          {
          (*bAEnvoyer) = TRUE;
          }
        //
        StrCopy (pszData, pszCourMes);
        break;
      }
    }
  //
  (*wSizeData) = StrLength(pszData) + 1; // compter '\0'
  return wErreur;
  }

// ----------------------------------------------------- 
// Init lien entree + lecture des valeurs initiales
static DWORD InitialisationLienEntree (DWORD wIndexLien)
  {
  DWORD              wErreur = 0;
  tx_dde_lien_e    *pxDdeLienE;
  tx_dde_e_advise  *pxDdeEAdvise;
  tx_dde_e_request*pxDdeERequest;
  DWORD              wRetour;
  DWORD             wIndex;
  DWORD             wIndexItemDsLien;
  BOOL          *pbCourLog;
  char              pszDataTmp[256];
  UINT              wSizeDataTmp;

  pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, wIndexLien);
  if ((uDDEOuvreConversationClient (pxDdeLienE->pszApplic,  pxDdeLienE->pszTopic,
		pxDdeLienE->wNbItemRequest + pxDdeLienE->wNbItemAdvise, TerminateLienERecu, (LPARAM)wIndexLien,
		&pxDdeLienE->hconvClient)) == 0)
    {
    pxDdeLienE->bLienOk = TRUE;
    pxDdeLienE->bLienDetruit = FALSE;
    //
    wIndexItemDsLien = 0;
    //---------------------------- BOUCLE SUR LES REQUEST
    //
    for (wIndex = pxDdeLienE->wPremItemRequest;
         wIndex < (pxDdeLienE->wPremItemRequest + pxDdeLienE->wNbItemRequest);
         wIndex++)
      {
      wIndexItemDsLien++;
      pxDdeERequest = (tx_dde_e_request*)pointe_enr (szVERIFSource, __LINE__, bx_dde_e_request, wIndex);
      pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pxDdeERequest->wPosEsRaf);
      if ((*pbCourLog) && (pxDdeLienE->bLienOk))
        { // Raf a haut et lien ok
        if (uDdeClientRequestItem (pxDdeERequest->pszItem, wIndexItemDsLien,
          NULL, 0, pxDdeLienE->hconvClient, pxDdeLienE->wTimeOut) == 0)
          {
          if (uDdeClientGetItemValeur (pxDdeLienE->hconvClient, wIndexItemDsLien,
                                &pszDataTmp, &wSizeDataTmp) == 0)
            {
            if ((wRetour = PutValeurBD (pxDdeERequest->wPosEs,
                                        pxDdeERequest->wTaille,
                                        pxDdeERequest->wGenre,
                                        pszDataTmp)) != 0)
              {
              wErreur = wErreur | wRetour;
              }
            }
          else
            {
            wErreur = wErreur | ERR_DDE_LECTURE;
            }
          }
        else
          {
          wErreur = wErreur | ERR_DDE_LECTURE;
          }
        }
      } // for request
    //---------------------------- BOUCLE SUR LES ADVISE
    //
    for (wIndex = pxDdeLienE->wPremItemAdvise;
         wIndex < (pxDdeLienE->wPremItemAdvise + pxDdeLienE->wNbItemAdvise);
         wIndex++)
      {
      wIndexItemDsLien++;
      pxDdeEAdvise = (tx_dde_e_advise*)pointe_enr (szVERIFSource, __LINE__, bx_dde_e_advise, wIndex);
      if (pxDdeLienE->bLienOk)
        {
        if (uDdeClientRequestItem (pxDdeEAdvise->pszItem, wIndexItemDsLien,
					NULL, 0, pxDdeLienE->hconvClient, pxDdeLienE->wTimeOut) == 0)
          {
          if (uDdeClientGetItemValeur (pxDdeLienE->hconvClient, wIndexItemDsLien,
                                &pszDataTmp, &wSizeDataTmp) == 0)
            {
            pxDdeEAdvise->bDataRecue = FALSE;
            if (uDdeClientAdviseItem (pxDdeEAdvise->pszItem, wIndexItemDsLien,
                               (void *)pszDataTmp, wSizeDataTmp,
                               DataAdviseRecue, (LPARAM)wIndex,
                               pxDdeLienE->hconvClient, pxDdeLienE->wTimeOut) == 0)
              {
              if ((wRetour = PutValeurBD (pxDdeEAdvise->wPosEs,
                                          pxDdeEAdvise->wTaille,
                                          pxDdeEAdvise->wGenre,
                                          pszDataTmp)) != 0)
                {
                wErreur = wErreur | wRetour;
                }
              }
            else
              {
              wErreur = wErreur | ERR_DDE_LECTURE;
              }
            }
          else
            {
            wErreur = wErreur | ERR_DDE_LECTURE;
            }
          }
        else
          {
          wErreur = wErreur | ERR_DDE_LECTURE;
          }
        } // lien ok
      } // for advise
    }
  else
    {
    wErreur = wErreur | ERR_DDE_CREATE_LINK;
    }

  return (wErreur);
  }


//-----------------------------------------------------
// lien a haut : lecture des valeurs request et advise
//
static DWORD LienEntree (DWORD wIndexLien)
  {
  DWORD              wErreur = 0;
  tx_dde_lien_e    *pxDdeLienE;
  tx_dde_e_advise  *pxDdeEAdvise;
  tx_dde_e_request *pxDdeERequest;
  DWORD             wRetour;
  DWORD            wIndex;
  DWORD            wIndexItemDsLien;
  BOOL             *pbCourLog;
  char             pszDataTmp[256];
  UINT             wSizeDataTmp;

  pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, wIndexLien);
  wIndexItemDsLien = 0;

  // BOUCLE SUR LES REQUEST
  for (wIndex = pxDdeLienE->wPremItemRequest;
       wIndex < (pxDdeLienE->wPremItemRequest + pxDdeLienE->wNbItemRequest);
       wIndex++)
    {
    wIndexItemDsLien++;
    pxDdeERequest = (tx_dde_e_request*)pointe_enr (szVERIFSource, __LINE__, bx_dde_e_request, wIndex);
    pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pxDdeERequest->wPosEsRaf);
    if (*pbCourLog)
      { // Raf a haut
      if (pxDdeLienE->bLienOk)
        { // ne fait pas request si le serveur est arrete entre temps
        if (uDdeClientRequestItem (pxDdeERequest->pszItem, wIndexItemDsLien,
          NULL, 0, pxDdeLienE->hconvClient, pxDdeLienE->wTimeOut) == 0)
          {
          }
        else
          wErreur = wErreur | ERR_DDE_LECTURE;
        } // bLienOk
      //
      if (uDdeClientGetItemValeur (pxDdeLienE->hconvClient, wIndexItemDsLien,
                            &pszDataTmp, &wSizeDataTmp) == 0)
        {
        if ((wRetour = PutValeurBD (pxDdeERequest->wPosEs,
                                    pxDdeERequest->wTaille,
                                    pxDdeERequest->wGenre,
                                    pszDataTmp)) != 0)
          wErreur = wErreur | wRetour;
        }
      else
        wErreur = wErreur | ERR_DDE_LECTURE;
      } // raf a haut
    } // for request
  //---------------------------- BOUCLE SUR LES ADVISE
  //
  for (wIndex = pxDdeLienE->wPremItemAdvise;
       wIndex < (pxDdeLienE->wPremItemAdvise + pxDdeLienE->wNbItemAdvise);
       wIndex++)
    {
    wIndexItemDsLien++;
    pxDdeEAdvise = (tx_dde_e_advise*)pointe_enr (szVERIFSource, __LINE__, bx_dde_e_advise, wIndex);
    if (pxDdeEAdvise->bDataRecue)
      {
      pxDdeEAdvise->bDataRecue = FALSE;
      if (uDdeClientGetItemValeur (pxDdeLienE->hconvClient, wIndexItemDsLien,
                            &pszDataTmp, &wSizeDataTmp) == 0)
        {
        if ((wRetour = PutValeurBD (pxDdeEAdvise->wPosEs,
                                    pxDdeEAdvise->wTaille,
                                    pxDdeEAdvise->wGenre,
                                    pszDataTmp)) != 0)
          wErreur = wErreur | wRetour;
        }
      else
        wErreur = wErreur | ERR_DDE_LECTURE;
      }
    } // for advise

  return (wErreur);
  }

//------------------------- 
// Init lien reflet + ecriture des valeurs initiales
static DWORD InitialisationLienReflet (UINT wIndexLien)
  {
  DWORD           wErreur = 0;
  tx_dde_lien_r *pxDdeLienR;
  PX_VAR_DDE_CLIENT_R pxDdeR;
  DWORD           wRetour;
  DWORD           wIndex;
  DWORD           wIndexItemDsLien;
  BOOL        bTest;
  char           pszDataTmp[256];
  UINT           wSizeDataTmp;

  pxDdeLienR = (tx_dde_lien_r*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_r, wIndexLien);
  if ((uDDEOuvreConversationClient (pxDdeLienR->pszApplic, pxDdeLienR->pszTopic, pxDdeLienR->wNbItem,
		TerminateLienRRecu, (LPARAM)wIndexLien, &pxDdeLienR->hconvClient)) == 0)
    {
    pxDdeLienR->bLienOk = TRUE;
    pxDdeLienR->bLienDetruit = FALSE;
    //
    wIndexItemDsLien = 0;
    for (wIndex = pxDdeLienR->wPremItem;
         wIndex < (pxDdeLienR->wPremItem + pxDdeLienR->wNbItem);
         wIndex++)
      {
      wIndexItemDsLien++;
      if (pxDdeLienR->bLienOk)
        {
        pxDdeR = (PX_VAR_DDE_CLIENT_R)pointe_enr (szVERIFSource, __LINE__, bx_dde_r, wIndex);
        if ((wRetour = GetValeurBD (pxDdeR->wPosEs, pxDdeR->wTaille, pxDdeR->wGenre,
                                    TRUE, pxDdeR->wPosEsRaf,
                                    pszDataTmp, &wSizeDataTmp,
                                    FALSE, &bTest)) == 0)
          {
          if (uDdeClientPokeItem (pxDdeR->pszItem, wIndexItemDsLien,
                           pszDataTmp, wSizeDataTmp,
                           pxDdeLienR->hconvClient, pxDdeLienR->wTimeOut) == 0)
            {
            }
          else
            wErreur = wErreur | ERR_DDE_ECRITURE;
          }
        else
          wErreur = wErreur | wRetour;
        }
      } // for reflets
    }
  else
    wErreur = wErreur | ERR_DDE_CREATE_LINK;

  return wErreur;
  }


//-----------------------------------------
// lien a haut : ecriture des valeurs qui ont change
//
static DWORD LienReflet (UINT wIndexLien)
  {
  DWORD           wErreur = 0;
  tx_dde_lien_r *pxDdeLienR;
  PX_VAR_DDE_CLIENT_R pxDdeR;
  DWORD           wRetour;
  DWORD           wIndex;
  DWORD           wIndexItemDsLien;
  BOOL        bTest;
  char           pszDataTmp[256];
  UINT           wSizeDataTmp;

  pxDdeLienR = (tx_dde_lien_r*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_r, wIndexLien);
  wIndexItemDsLien = 0;
  for (wIndex = pxDdeLienR->wPremItem;
       wIndex < (pxDdeLienR->wPremItem + pxDdeLienR->wNbItem);
       wIndex++)
    {
    wIndexItemDsLien++;
    if (pxDdeLienR->bLienOk)
      {
      pxDdeR = (PX_VAR_DDE_CLIENT_R)pointe_enr (szVERIFSource, __LINE__, bx_dde_r, wIndex);
      if ((wRetour = GetValeurBD (pxDdeR->wPosEs, pxDdeR->wTaille, pxDdeR->wGenre,
                                  TRUE, pxDdeR->wPosEsRaf,
                                  pszDataTmp, &wSizeDataTmp,
                                  TRUE, &bTest)) == 0)
        {
        if (bTest)
          { // la valeur a change : il faut l'envoyer
          if (uDdeClientPokeItem (pxDdeR->pszItem, wIndexItemDsLien,
                           pszDataTmp, wSizeDataTmp,
                           pxDdeLienR->hconvClient, pxDdeLienR->wTimeOut) == 0)
            {
            }
          else
            wErreur = wErreur | ERR_DDE_ECRITURE;
          }
        }
      else
        wErreur = wErreur | wRetour;
      }
    } // for reflets

  return wErreur;
  }

// -----------------------------------------------------------------------
// Note les variables en entr�es marqu�es modifi�es
static void MajMemLienEntreeDde (void)
  {
  DWORD wNbLien = 0;

  if (existe_repere (bx_dde_lien_e))
    {
    wNbLien = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_e);
    }
  //
  for (DWORD wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
    {
		tx_dde_lien_e *pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, wIndexLien);
		BOOL       *bMemLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, pxDdeLienE->wPosEsLien);
    pxDdeLienE->bMemLien = (*bMemLog);
    } // for wNbLien
  } // MajMemLienEntreeDde

// --------------------------------------------------------------------------
// FONCTIONS UTILISEES POUR LES LIENS DYNAMIQUES SERVEUR
// --------------------------------------------------------------------------

//--------------------------------------------------------------------------
// Initialise d�but exploitation DDE
void initialise_de (void)
  {
  if (existe_repere (bx_dde_lien_e) || existe_repere (bx_dde_lien_r) ||
      existe_repere (bx_var_dde_client_dyn)  || existe_repere (bx_dde_var_serveur))
    {
		// Ouverture de DDEML Ok ?
		if (bInitDDE (Appli.hinst, TRUE, TRUE))
	    {
			// Fonction serveur DDE ?
			if (existe_repere (bx_dde_var_serveur))
				{
				// oui => Lancement du serveur : supporte le nom de serveur et le sujet Nom du document
				if (!bDdeExeInitServeur (szNomServeurDDE, pszNameDoc()))
			    SignaleWarnExit ("Erreur lors de la cr�ation du serveur");
				}
		  }
		else
			SignaleWarnExit ("Erreur en initialisation DDE");
	  }
  } // initialise_de

//--------------------------------------------------------------------------
// Termine l'exploitation DDE
void termine_de (void)
  {
  if (bDDEOuvert())
    {
    // arreter ts les liens entrees en cours
		DWORD           wNbLien;
		DWORD           wIndexLien;
		tx_dde_lien_e *pxDdeLienE;
		tx_dde_lien_r *pxDdeLienR;
		PX_VAR_DDE_CLIENT_DYN pVarClientDyn;

    if (existe_repere (bx_dde_lien_e))
      wNbLien = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_e);
    else
      wNbLien = 0;

    for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
      {
      pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, wIndexLien);
      if ((pxDdeLienE->wNbItemAdvise) && (pxDdeLienE->bLienOk))
        { // il y a des advise : il faut les arreter
        DdeUnadviseLink (pxDdeLienE->hconvClient, pxDdeLienE->wTimeOut);
        }
      if (!pxDdeLienE->bLienDetruit)
        {
        uDDEFermeConversationClient (&pxDdeLienE->hconvClient);
        }
      }

    // arreter ts les liens entrees dynamiques en cours
    if (existe_repere (bx_var_dde_client_dyn))
      {
      wNbLien = nb_enregistrements (szVERIFSource, __LINE__, bx_var_dde_client_dyn);
      }
    else
      {
      wNbLien = 0;
      }
    for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
      {
      pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, wIndexLien);
      if (pVarClientDyn->bLienOk)
        {
        DdeUnadviseLink (pVarClientDyn->hconvClient, TIMEOUTDDE); //$$ D�faut : V�rifier
        }
      if (!pVarClientDyn->bLienDetruit)
        {
        uDDEFermeConversationClient (&pVarClientDyn->hconvClient);
        }
      }

    // arreter ts les liens reflets en cours
    if (existe_repere (bx_dde_lien_r))
      {
      wNbLien = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_r);
      }
    else
      {
      wNbLien = 0;
      }
    for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
      {
      pxDdeLienR = (tx_dde_lien_r*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_r, wIndexLien);
      if (!pxDdeLienR->bLienDetruit)
        {
        uDDEFermeConversationClient (&pxDdeLienR->hconvClient);
        }
      }

		// Fonction serveur DDE ?
		if (existe_repere (bx_dde_var_serveur))
			// oui => ferme le serveur
			bFermeServeurDDE ();

    // arret DDEML
		FermeDDE();
    } // if (bDDEOuvert())
  } // termine_de

// -----------------------------------------------------------------------
// Laisse se faire les entr�es sorties du serveur DDE
static void EntreesSortiesServeurDDE (void)
	{
	MSG		msg;

	// Y a t'il des messages dans la queue de message ?
	if (PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE))
		{
		#define DUREE_TOTALE_MAX_ES_DDE_MS 1000
		#define	ATTENTE_RELANCE_MS 200

		BOOL	bFin = FALSE;

		// oui => on scrute les messages pendant au plus DUREE_TOTALE_MAX_ES_DDE_MS;
		DWORD dwTickMax = GetTickCount() + DUREE_TOTALE_MAX_ES_DDE_MS;

		// Ecoule tous les messages de la queue de message de ce thread
		bVideQueueDeMessage ();

		// boucle tant que n�cessaire
		while (!bFin)
			{
			// Time out global �coul� ?
			if (GetTickCount() > dwTickMax) // $$ Overflow
				// oui => plus rien � faire
				bFin = TRUE;
			else
				{
				// non => attends ATTENTE_RELANCE_MS un �ventuel nouveau message
				switch (MsgWaitForMultipleObjects (0, NULL, FALSE, ATTENTE_RELANCE_MS, QS_ALLINPUT))
					{
					// fin d'attente pour cause de r�ception de message
					case WAIT_OBJECT_0: 
						bVideQueueDeMessage ();
						break;

					case WAIT_TIMEOUT:			// time out
					case WAIT_ABANDONED_0:	// Erreur en attente de synchro r�ception
					default:
						// fin d'attente pour cause d'absence de r�ception de message
						bFin = TRUE;
						break;
					} // switch (MsgWaitForMultipleObjects...
				}
			} // while (!bFin)
		} // if (PeekMessage (&msg, NULL, 0, 0, PM_NOREMOVE))
	} // EntreesSortiesServeurDDE

// -----------------------------------------------------------------------
// ENTREES DDE
void recoit_de (BOOL bPremierPassage)
  {
	// Laisse se faire les entr�es sorties DDE
	EntreesSortiesServeurDDE();

	// Dde ouvert ?
  if (bDDEOuvert())
    {
		DWORD           wErreur = 0;
		DWORD           wNbLien;
		DWORD           wIndexLien;
		tx_dde_lien_e *pxDdeLienE;
		PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;
		BOOL       *pbCourLog;
		DWORD           wRetour;
		char           pszDataTmp[256];
		UINT           wSizeDataTmp;

    // oui=> effectue les diverses entr�es client

		// entr�es liens preconfigures clients
    if (existe_repere (bx_dde_lien_e))
      {
      wNbLien = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_e);
      }
    else
      {
      wNbLien = 0;
      }
    //
    if (bPremierPassage)
      {
      for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
        {
        pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, wIndexLien);
        pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pxDdeLienE->wPosEsLien);
        if (*pbCourLog)
          { // Lien a haut
          if ((wRetour = InitialisationLienEntree (wIndexLien)) != 0)
            {
            wErreur = wErreur | wRetour;
            }
          } // Lien a haut
        } // for wNbLien
      } // PREMIER_PASSAGE
    else
      { // PAS PREMIER_PASSAGE
      for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
        {
        pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, wIndexLien);
        pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pxDdeLienE->wPosEsLien);
        if ((*pbCourLog) != pxDdeLienE->bMemLien)
          { // Le Lien a change
          if (*pbCourLog)
            { // Le Lien vient de passer a haut
            if ((wRetour = InitialisationLienEntree (wIndexLien)) != 0)
              {
              wErreur = wErreur | wRetour;
              }
            } // Le Lien vient de passer a haut
          else
            { // Le Lien vient de passer a bas
            if ((pxDdeLienE->wNbItemAdvise) && (pxDdeLienE->bLienOk))
              { // il y a des advise : il faut les arreter
              if (DdeUnadviseLink (pxDdeLienE->hconvClient, pxDdeLienE->wTimeOut) == 0)
                {
                }
              else
                {
                wErreur = wErreur | ERR_DDE_LECTURE;
                }
              }
            //
            if (!pxDdeLienE->bLienDetruit)
              {
              uDDEFermeConversationClient (&pxDdeLienE->hconvClient);
              }
            pxDdeLienE->bLienOk = FALSE;
            pxDdeLienE->bLienDetruit = TRUE;
            } // Le Lien vient de passer a bas
          } // Lien change
        else
          { // Lien non change
          if (*pbCourLog)
            { // Lien non change et a haut
            if (pxDdeLienE->bLienOk)
              {
              if ((wRetour = LienEntree (wIndexLien)) != 0)
                {
                wErreur = wErreur | wRetour;
                }
              }
            //----------- NE PAS METTRE ELSE sur bLienOk
            if ((!pxDdeLienE->bLienOk) && (!pxDdeLienE->bLienDetruit))
              { // le lien a ete termine par un serveur
              wErreur = wErreur | ERR_DDE_SRV_TERM_LINK;
              uDDEFermeConversationClient (&pxDdeLienE->hconvClient);
              pxDdeLienE->bLienDetruit = TRUE;
              }
            } // Lien non change et a haut
          }
        } // for wNbLien
      } // PAS PREMIER_PASSAGE

    //-------------------------------------------------------------
    //         gestion des liens dynamiques clients : COLLER
    //-------------------------------------------------------------
    if (existe_repere (bx_var_dde_client_dyn))
      {
      wNbLien = nb_enregistrements (szVERIFSource, __LINE__, bx_var_dde_client_dyn);
      }
    else
      {
      wNbLien = 0;
      }
    //
    for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
      {
      pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, wIndexLien);
      if ((pVarClientDyn->bLienOk) && (pVarClientDyn->bDataRecue))
        {
        pVarClientDyn->bDataRecue = FALSE;
        if (uDdeClientGetItemValeur (pVarClientDyn->hconvClient, 1,
                              &pszDataTmp, &wSizeDataTmp) == 0)
          {
          if ((wRetour = PutValeurBD (pVarClientDyn->wPosEs,
                                      pVarClientDyn->wTaille,
                                      pVarClientDyn->wGenre,
                                      pszDataTmp)) != 0)
            {
            wErreur = wErreur | wRetour;
            }
          }
        else
          {
          wErreur = wErreur | ERR_DDE_LECTURE;
          }
        } // lien ok et data recue
      //
      if ((!pVarClientDyn->bLienOk) && (!pVarClientDyn->bLienDetruit))
        { // le lien a ete termine par un serveur
        wErreur = wErreur | ERR_DDE_SRV_TERM_LINK;
        uDDEFermeConversationClient (&pVarClientDyn->hconvClient);
        pVarClientDyn->bLienDetruit = TRUE;
        }
      } // for wNbLien
    //
    if (wErreur != 0)
      {
      maj_statut_de (wErreur);
      }
		}
  } // recoit_de

// ---------------------------------------------------------------------------
// Emission des sorties DDE de pcsExe
void emet_de (BOOL bPremierPassage)
  {
  DWORD           wErreur = 0;
  DWORD           wNbLien;
  DWORD           wIndexLien;
  tx_dde_lien_r *pxDdeLienR;
  BOOL       *pbCourLog;
  BOOL       *bMemLog;
  DWORD           wRetour;
  //BOOL        bDataOk;
  //BOOL        bTest;
  //char           pszDataTmp[256];
  //UINT           wSizeDataTmp;
  //DWORD           wIndex;
  PX_VAR_SERVEUR_DDE	pVarServeurDde;
  //t_dde_srv     *pDdeSrv;

  if (bDDEOuvert())
    {
    // sauvegarde des liens pour les entrees predefinies
    MajMemLienEntreeDde ();
    //
    if (existe_repere (bx_dde_lien_r))
      {
      wNbLien = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_r);
      }
    else
      {
      wNbLien = 0;
      }
    //
    if (bPremierPassage)
      {
      for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
        {
        pxDdeLienR = (tx_dde_lien_r*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_r, wIndexLien);
        pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pxDdeLienR->wPosEsLien);
        if (*pbCourLog)
          { // Lien a haut
          if ((wRetour = InitialisationLienReflet (wIndexLien)) != 0)
            {
            wErreur = wErreur | wRetour;
            }
          } // Lien a haut
        } // for wNbLien
      } // PREMIER_PASSAGE
    else
      { // PAS PREMIER_PASSAGE
      for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
        {
        pxDdeLienR = (tx_dde_lien_r*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_r, wIndexLien);
        pbCourLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pxDdeLienR->wPosEsLien);
        bMemLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, pxDdeLienR->wPosEsLien);
        if ((*pbCourLog) != (*bMemLog))
          { // Le Lien a change
          if (*pbCourLog)
            { // Le Lien vient de passer a haut
            if ((wRetour = InitialisationLienReflet (wIndexLien)) != 0)
              {
              wErreur = wErreur | wRetour;
              }
            } // Le Lien vient de passer a haut
          else
            { // Le Lien vient de passer a bas
            if (!pxDdeLienR->bLienDetruit)
              {
              uDDEFermeConversationClient (&pxDdeLienR->hconvClient);
              }
            pxDdeLienR->bLienOk = FALSE;
            pxDdeLienR->bLienDetruit = TRUE;
            } // Le Lien vient de passer a bas
          } // Lien change
        else
          { // Lien non change
          if (*pbCourLog)
            { // Lien non change et a haut
            if (pxDdeLienR->bLienOk)
              {
              if ((wRetour = LienReflet (wIndexLien)) != 0)
                {
                wErreur = wErreur | wRetour;
                }
              }
            // NE PAS METTRE ELSE sur bLienOk
            if ((!pxDdeLienR->bLienOk) && (!pxDdeLienR->bLienDetruit))
              { // le lien a ete termine par un serveur
              wErreur = wErreur | ERR_DDE_SRV_TERM_LINK;
              uDDEFermeConversationClient (&pxDdeLienR->hconvClient);
              pxDdeLienR->bLienDetruit = TRUE;
              }
            } // Lien non change et a haut
          }
        } // for wNbLien
      } // PAS PREMIER_PASSAGE

    //-------------------------------------------------------------------
    // Serveur DDE : �mission des variables modifi�es sous surveillance ADVISE
    //-------------------------------------------------------------------
    if (existe_repere (bx_dde_var_serveur))
      {
      wNbLien = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_var_serveur);

			// pour chaque variable expos�e par le serveur
      for (wIndexLien = 1; wIndexLien <= wNbLien; wIndexLien++)
        {
        pVarServeurDde = (PX_VAR_SERVEUR_DDE)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, wIndexLien);

				// La variable est elle soumise � une surveillance de type boucle Advise ?
        if (pVarServeurDde->wNbClientsEnAdvise > 0)
          {
					// oui => La variable a t'elle �t� modifi�e ?
					PX_E_S pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pVarServeurDde->wPosBxEs);
					if (CPcsVarEx::bGetBlocExValeurModifie	(pxES->wPosEx,pxES->wBlocGenre))
						{
						// oui => Mettre � jour les clients concern�s
						// $$ Monter statut si n�cessaire
						bChangementItemServeurDansAdvise (pszNameDoc(), pxES->pszNomVar);
						}
          }
        } // for wNbLien
      }

    if (wErreur != 0)
      {
      maj_statut_de (wErreur);
      }
		} // if (bDDEOuvert)
  } // emet_de 

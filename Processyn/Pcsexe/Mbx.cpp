//--------------------------------------------------------
// MBX.
// syst�me de communication client - serveur processyn.
//
// 1) Cot� serveur : cr�er des services en attente d'appels (dwCnxCreeServeur)
//		puis attendre  la connexion pour chaque service (bCnxAUnCorrespondant)
// 2) Cot� client : cr�ation et attente de connexion � un service serveur par dwMbxAppelServeur
// 3) Communication �tablie (jusqu'� dwMbxFermeConnexion d'un des correspondants) : 
//			dwMbxEcriture, dwMbxLecture, dwMbxEnvoiAR + dwMbxAttenteAR bCnxAUnCorrespondant 
//
// Remarques : 
//	Impl�mentation multi-thread en intra-process.
//--------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "USem.h"
#include "QueueMan.h"
#include "UStr.h"
#include "threads.h"
#include "MemMan.h"
#include "UListeH.h"
#include "Verif.h"

#include "mbx.h"

VerifInit;

// $$ remplacer par des notifications

#define SRV_MAX_BUFF_SIZE 32767	// taille maximale de donn�es que l'on peut envoyer $$

//--------------------------------------------------
// �tats d'une connexion
typedef enum
	{
	CNX_INVALIDE,									// Connexion invalide (non initialis�e)
	CNX_EN_ATTENTE_CONNEXION,			// Connexion en attente d'�tablissement d'une communication
	CNX_CONNEXION_EN_COURS,				// Connexion avec un correspondant en cours
	CNX_FIN_CONNEXION							// Connexion ferm�e par le correspondant
	} ETAT_CNX;

// Structure interne associ�e � un handle connexion
typedef struct _tagCONNEXION
	{
	ETAT_CNX				nEtat;													// Etat courant de la connexion
	HSEM						hsemAccesCnxExclusif;						// s�maphore auto pris d'acc�s exclusif � cette connexion
	HQUEUE					hQueueRX;												// queue receptrice des messages 
	HSEM						hsemAttenteAR;									// s�maphore d'attente de l'arriv�e d'un Accus� de R�ception
	char						szNomTopic [MBX_USR_MAX_NAME];	// nom du service g�r� par cette connexion
	HCNX						hCnxCorrespondant;							// handle Connexion du correspondant ou null
	BOOL						bServeur;												// Connexion de type serveur (TRUE) ou client (FALSE)
	BOOL						bDansListeAttente;							// Vrai si le handle de cette connexion est actuellement dans hListeHcnxAConnecter
	} CONNEXION;

typedef CONNEXION * PCONNEXION;

// Impl�mentation intra process : acc�s � toutes les connexions
static HLISTE_H hListeHcnxAConnecter;

//----------------------------------------------------------------------------
// conversions handle <-> pointeur CONNEXION 
static _inline PCONNEXION PfromHCNX (HCNX  hConnexion)	{return (PCONNEXION) hConnexion;}

static _inline HCNX  HfromPCONNEXION (PCONNEXION pConnexion)	{return (HCNX) pConnexion;}

//-----------------------------------------------------------------
// Demande d'un acc�s excusif � une connexion (Attente infinie si n�cessaire)
static void AccesCnxExclusif (PCONNEXION pConnexion)
	{
	Verif (!dwSemAttenteLibre (pConnexion->hsemAccesCnxExclusif, INFINITE));
	}

//-----------------------------------------------------------------
// Demande d'un acc�s excusif � une connexion (Attente max : 1 s)
// Renvoie TRUE si l'acc�s exclusif a �t� obtenu, FALSE sinon.
static BOOL bAccesCnxExclusifAttenteLimitee (PCONNEXION pConnexion)
	{
	return dwSemAttenteLibre (pConnexion->hsemAccesCnxExclusif, 1000)==0;
	}

//-----------------------------------------------------------------
// Fin d'un acc�s excusif � une connexion
static void FinAccesCnxExclusif (PCONNEXION pConnexion)
	{
	Verif (bSemLibere (pConnexion->hsemAccesCnxExclusif));
	}

//--------------------------------------------------------------------------
// Initialisation automatique de la liste des handles en attente de connexion
//--------------------------------------------------------------------------
static void InitAutoListeHcnxAConnecter (void)
	{
	static BOOL bPoolConnexionsValide = FALSE;

	// Initialisation du Pool de connexions si n�cessaire
	if (!bPoolConnexionsValide)
		{
		Verif (bCreeListeH (&hListeHcnxAConnecter, INVALID_HCNX, TRUE));
		bPoolConnexionsValide = TRUE;
		}
	}

//--------------------------------------------------------------------------
// Ajout d'une connexion � la liste des handles en attente de connexion
//--------------------------------------------------------------------------
static void AjoutAListeHcnxAConnecter (PCONNEXION pConnexion)
	{
	AccesModifListeH (hListeHcnxAConnecter);
	Verif (bAjouteDansListeH (hListeHcnxAConnecter, HfromPCONNEXION(pConnexion)));
	pConnexion->bDansListeAttente = TRUE;
	FinAccesModifListeH (hListeHcnxAConnecter);
	}

//--------------------------------------------------------------------------
// Suppression d'une connexion de la liste des handles en attente de connexion
//--------------------------------------------------------------------------
static BOOL bSupprimeDeListeHcnxAConnecter (PCONNEXION pConnexion)
	{
	BOOL	bRes = FALSE;

	AccesModifListeH (hListeHcnxAConnecter);
	if (bSupprimeDansListeH (hListeHcnxAConnecter, HfromPCONNEXION(pConnexion)))
		{
		pConnexion->bDansListeAttente = FALSE;
		bRes = TRUE;
		}
	FinAccesModifListeH (hListeHcnxAConnecter);

	return bRes;
	}

//-----------------------------------------------------------------
// Lib�re les ressources d'une connexion
//-----------------------------------------------------------------
static BOOL bSupprimeConnexion (PCONNEXION pConnexion)
  {
  BOOL bRes	= FALSE;

	if (pConnexion)
		{
		HCNX hcnx = HfromPCONNEXION (pConnexion);

		pConnexion->nEtat = CNX_INVALIDE;
		if (pConnexion->hsemAttenteAR)
			Verif (bSemFerme (&pConnexion->hsemAttenteAR));

		// pas de fermeture d'une connexion en cours de communication
		Verif (!pConnexion->hCnxCorrespondant);	

		// Supprime le handle de la liste d'attente
		if (pConnexion->bDansListeAttente)
			bSupprimeDeListeHcnxAConnecter (pConnexion);

		// Ferme les ressources restantes
		if (pConnexion->hQueueRX)
			bFermeQueue (&pConnexion->hQueueRX);
		if (pConnexion->hsemAccesCnxExclusif)
			bSemFerme (&pConnexion->hsemAccesCnxExclusif);
		MemLibere ((PVOID *)(&pConnexion));
		bRes = TRUE;
		}

  return bRes;
  }

//--------------------------------------------------
// Envoi d'un message � la connexion correspondante
// Si Ok, alloue un buffer compos� du header suivi �ventuellement par des donn�es
// Renvoie TRUE si envoi Ok, FALSE sinon.       
//--------------------------------------------------
static BOOL bEnvoiMessageAuCorrespondant 
	(
	PCONNEXION pConnexion, 
	PMBX_HEADER_MESSAGE pEntete, // doit �tre rempli
	PVOID pvDonnees, // peut �tre NULL si pEntete.dwTailleDonnees = 0
	BOOL bDemandeAR)
  {
	BOOL bRet = FALSE;

	// enregistre l'�ventuelle attente d'AR
  if (bDemandeAR)
    {
    bSemPrends (pConnexion->hsemAttenteAR);
    }

	// Message compatible avec l'�tat actuel du CNX ?
	if (pEntete &&
		(((pEntete->IdMessage == MBX_MSG_OUVRE) && (pConnexion->nEtat == CNX_EN_ATTENTE_CONNEXION)) ||
		(pConnexion->nEtat == CNX_CONNEXION_EN_COURS)) &&
		(pConnexion->hCnxCorrespondant))
		{
		// oui => transfert des donn�es au correspondant :
		PCONNEXION pConnexionCorrespondant = PfromHCNX (pConnexion->hCnxCorrespondant);

		// recopie les donn�es dans un buffer allou� dynamiquement
		DWORD	dwTailleEnvoi = sizeof (MBX_HEADER_MESSAGE) + pEntete->dwTailleDonnees;
		PVOID	pBufEnvoi = pMemAlloue (dwTailleEnvoi);

		// Copie de l'ent�te
		CopyMemory (pBufEnvoi, pEntete, sizeof (*pEntete));

		// Copie �ventuelle des donn�es
		if (pEntete->dwTailleDonnees)
			CopyMemory (&((PMESSAGE_MBX)pBufEnvoi)->aDonnees, pvDonnees, pEntete->dwTailleDonnees);

		// acc�s au correspondant
		AccesCnxExclusif (pConnexionCorrespondant);

		// �tat compatible avec �criture ?
		if (pBufEnvoi &&
			(pConnexionCorrespondant->nEtat == CNX_CONNEXION_EN_COURS) ||
			(pConnexionCorrespondant->nEtat == CNX_FIN_CONNEXION))
			{
			// oui => ajout du message dans la queue du correspondant
			if (bEcritQueue (pConnexionCorrespondant->hQueueRX, 0, dwTailleEnvoi, pBufEnvoi))
				bRet = TRUE;
			}
		FinAccesCnxExclusif (pConnexionCorrespondant);

		// lib�re le buffer si quelque chose n'a pas fonctionn�
		if ((!bRet) && pBufEnvoi)
			MemLibere (&pBufEnvoi);
		}
  if (bDemandeAR && (!bRet))
    {
    bSemLibere(pConnexion->hsemAttenteAR); //$$ v�rifier si pas mont� pour rien
    }
  return bRet;
  } // bEnvoiMessageAuCorrespondant


//-----------------------------------------------------------------------------
// �tablis une connexion client - serveur
static BOOL bConnecteCorrespondant (PCONNEXION pConnexion)
	{
	BOOL	bRes = FALSE;
	DWORD	dwNHandle;
	DWORD dwNbHandles = dwNbHDansListeH(hListeHcnxAConnecter);

	// recherche d'un correspondant � connecter dans la table globale des connexions :
	// acc�s global aux handles
	AccesModifListeH (hListeHcnxAConnecter);

	// Parcours de la liste des connexions en attentes
	for (dwNHandle = 0; dwNHandle < dwNbHandles; dwNHandle++)
		{
		PCONNEXION pConnexionCorrespondant = (PCONNEXION)hDansListeH (hListeHcnxAConnecter, dwNHandle);

		// Connexion valide ?
		if (pConnexionCorrespondant)
			{
			// oui => acc�s exclusif au correspondant
			AccesCnxExclusif (pConnexionCorrespondant);

			// correspondant en attente de conexion, l'un est client et l'autre serveur, m�me nom de service ?
			if ((pConnexionCorrespondant->nEtat == CNX_EN_ATTENTE_CONNEXION) &&
				(pConnexion->bServeur ^ pConnexionCorrespondant->bServeur) &&
				(StrICompare (pConnexion->szNomTopic, pConnexionCorrespondant->szNomTopic) == 0))
				{
				// oui => �tablissement de la communication
				MBX_HEADER_MESSAGE headerOuverture = {MBX_MSG_OUVRE, 0};
				HCNX	hCnxCorrespondant = HfromPCONNEXION (pConnexionCorrespondant);

				pConnexion->hCnxCorrespondant = hCnxCorrespondant;
				pConnexion->nEtat = CNX_CONNEXION_EN_COURS;
				pConnexionCorrespondant->nEtat = CNX_CONNEXION_EN_COURS;
				pConnexionCorrespondant->hCnxCorrespondant = HfromPCONNEXION (pConnexion);

				// Sors le correspondant de la liste des connexions en attente
				Verif (bSupprimeDeListeHcnxAConnecter (pConnexionCorrespondant));

				// lib�re le s�maphore d'AR utilis� en attente de connexion
				FinAccesCnxExclusif (pConnexionCorrespondant);

				// Envoi du message d'ouverture au correspondant
				Verif (bEnvoiMessageAuCorrespondant (pConnexion, &headerOuverture, NULL, FALSE));
				bRes = TRUE;
				break;
				}
			else
				{
				// non => correspondant inexploitable
				FinAccesCnxExclusif (pConnexionCorrespondant);
				}
			} // if (pConnexionCorrespondant)
		} // for (dwNHandle = 0; dwNHandle < dwNbHandles; dwNHandle++)
	// fin acc�s global aux handles
	FinAccesModifListeH (hListeHcnxAConnecter);

	return bRes;
	} // bConnecteCorrespondant

//--------------------------------------------------------------------------
// Cr�ation d'une connexion serveur ou client.
//--------------------------------------------------------------------------
static DWORD dwCreeCnx (PCONNEXION * ppConnexion, PCSTR pszNomServeur, PCSTR pszNomTopic,
												BOOL bServeur, DWORD dwTimeOutAttente)
  {
  DWORD				dwRes = 0;
	// Connexion libre trouv�e : on alloue et initialise l'objet
	PCONNEXION	pConnexion = (PCONNEXION)pMemAlloue (sizeof(CONNEXION));
	char				queue_name [255];


	if (bServeur)
		StrPrintFormat (queue_name, "S/%s/%s", pszNomServeur, pszNomTopic);
	else
		StrPrintFormat (queue_name, "C/%s/%s", pszNomServeur, pszNomTopic);
	pConnexion->hQueueRX = hCreeQueue (queue_name);

	// acc�s exclusif � cette connexion
	pConnexion->hsemAccesCnxExclusif = hSemCree (TRUE);

	// recopie du nom du service
	StrCopy (pConnexion->szNomTopic, pszNomTopic);

	pConnexion->hsemAttenteAR = hSemCreeAttenteSeule (TRUE);
	pConnexion->nEtat = CNX_EN_ATTENTE_CONNEXION;
	pConnexion->hCnxCorrespondant = NULL;	
	pConnexion->bServeur = bServeur;
	pConnexion->bDansListeAttente = FALSE;

	// Allocation des ressources ok ?
	if (pConnexion->hsemAccesCnxExclusif && pConnexion->hsemAttenteAR && pConnexion->hQueueRX)
		{
		// oui => initialisation de la liste des handles de connexions en attente
		InitAutoListeHcnxAConnecter();

		// Connexion avec un correspondant ?
		if (bConnecteCorrespondant (pConnexion))
			{
			// oui => termin�
			FinAccesCnxExclusif (pConnexion);
			}
		else
			{
			// non => rends la connexion accessible aux correspondants � venir
			AjoutAListeHcnxAConnecter (pConnexion);

			FinAccesCnxExclusif (pConnexion);

			// Attente le temps demand� de la connexion d'un correspondant infructueux ?
			if (!bCnxAUnCorrespondant	(HfromPCONNEXION (pConnexion), dwTimeOutAttente))
				// oui => time out
				dwRes = MBX_ERR_TIMEOUT;
			}
		}
	else
		{
		// non => on lib�re tout
		Verif (bSupprimeConnexion (pConnexion));
		pConnexion = NULL;
		dwRes = MBX_ERR_ALLOC;
		}
 
	// retour des infos
	* ppConnexion = pConnexion;
	return dwRes;
  } // dwCreeCnx

//------------------------------------------------------------------------
// Detruit une connexion MBX
// Renvoie TRUE si la d�connexion s'est bien pass� (* phcnx mis � NULL)
// FALSE sinon
//------------------------------------------------------------------------
static DWORD dwMbxDeconnecte (HCNX * phcnx) // handle du canal mailbox a deconnecter. 
  {
  DWORD				dwRet = MBX_ERR_MBX_UNKOWN_STATE;
  PCONNEXION	pConnexion = PfromHCNX (*phcnx);

	Verif (pConnexion);
  switch (pConnexion->nEtat)
    {
    case CNX_EN_ATTENTE_CONNEXION :
    case CNX_FIN_CONNEXION :
    case CNX_CONNEXION_EN_COURS:
      // on supprime le handle et on libere l'entree
			AccesModifListeH (hListeHcnxAConnecter);

      if (bSupprimeConnexion (pConnexion))
				dwRet = 0;

      // on supprime le handle et on libere l'entree
			FinAccesModifListeH (hListeHcnxAConnecter);
      break;

		default:
			//case CNX_INVALIDE :
      dwRet = MBX_ERR_MBX_NOT_IN_USE;
      break;
    }
  return dwRet;
  } // dwMbxDeconnecte


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//						Fonctions export�es
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//-------------------------------------------------------------------------
// Cr�ation d'une connexion en attente d'un appel d'un client.
//------------------------------------------------------------------------
DWORD dwCnxCreeServeur 
	(
	HCNX * phcnx,								// adresse du handle de connexion � utiliser
	const char * pszNomServeur,	// Nom du serveur support� ou NULL.
	const char * pszNomTopic		// Nom du service support� ou NULL.
	)
  {
	PCONNEXION pConnexion = NULL;

	// cr�ation d'un objet de connexion Ok ?
  DWORD dwRet = dwCreeCnx (&pConnexion, pszNomServeur, pszNomTopic, TRUE, 0);

	// renvoie le handle obtenu
	*phcnx = HfromPCONNEXION (pConnexion);
  return dwRet;
  } // dwCnxCreeServeur

//-------------------------------------------------------------------------
// Cr�ation d'une connexion en communication avec un service d'un serveur Mbx.
// -----------------------------------------------------------------------
DWORD dwMbxAppelServeur 
	(
	HCNX *	phcnx,							// adresse du handle de connexion � utiliser
	const char * pszNomServeur,	// Nom du serveur auquel on veut se connecter
	const char * pszNomTopic,		// Nom du service auquel on veut se connecter
	DWORD dwTimeOutAttente			// Dur�e d'attente max de la connexion en ms
	)
  {
	PCONNEXION pConnexion = NULL;

	// cr�ation d'un objet de connexion Ok ?
  DWORD dwRet = dwCreeCnx (&pConnexion, pszNomServeur,	pszNomTopic, FALSE, dwTimeOutAttente);

	// renvoie le handle obtenu
	*phcnx = HfromPCONNEXION (pConnexion);
  return dwRet;
  } // dwMbxAppelServeur

//------------------------------------------------------------------------
// Ferme une connexion et d�connecte l'�ventuel correspondant
//------------------------------------------------------------------------
DWORD dwMbxFermeConnexion (HCNX * phcnx)
  {
  DWORD					dwRet = MBX_ERR_MBX_UNKOWN_STATE;
  PCONNEXION		pConnexion = PfromHCNX (* phcnx);

	Verif (pConnexion);
	AccesCnxExclusif (pConnexion);
	switch (pConnexion->nEtat)
		{
		case CNX_EN_ATTENTE_CONNEXION :  // ligne en attente d'ouverture 
		case CNX_CONNEXION_EN_COURS :
			{
			PCONNEXION	pConnexionCorrespondant = PfromHCNX (pConnexion->hCnxCorrespondant);

			// d�connexion du correspondant
			if (pConnexionCorrespondant)
				{
			  MBX_HEADER_MESSAGE headerFermeture = {MBX_MSG_FERME, 0};

				BOOL bAccesOk = bAccesCnxExclusifAttenteLimitee (pConnexionCorrespondant); // �treinte mortelle entre deux connexions en fermeture limit�e
				Verif (pConnexionCorrespondant->hCnxCorrespondant);

				// correspondant d�connect�
				pConnexionCorrespondant->hCnxCorrespondant = NULL;
				pConnexionCorrespondant->nEtat = CNX_FIN_CONNEXION;

				if (bAccesOk)
					FinAccesCnxExclusif (pConnexionCorrespondant);

				// envoi du message de fermeture au correspondant
				Verif (bEnvoiMessageAuCorrespondant (pConnexion, &headerFermeture, NULL, FALSE));
				pConnexion->hCnxCorrespondant = NULL;
				}

			// fermeture de la connexion locale et de son correspondant 
			dwRet = dwMbxDeconnecte (phcnx);
			}
			break;

		case CNX_FIN_CONNEXION :
			dwRet = dwMbxDeconnecte (phcnx);
			//dwRet = MBX_ERR_MBX_ALREADY_CLOSED;
			break;

		default:
			//case CNX_INVALIDE :
			dwRet = MBX_ERR_MBX_NOT_CONNECTED;
			FinAccesCnxExclusif (pConnexion);
			break;
		}
	
  return dwRet;
  } // dwMbxFermeConnexion

//----------------------------------------------------------------------
// Envoi d'un message � un correspondant.
// $$ Le buffer de donn�es doit commencer par une structure MBX_HEADER_MESSAGE
// (la valeur de ses champs sera mise � jour par cette fonction)
//-----------------------------------------------------------------------
DWORD dwMbxEcriture
	(
	HCNX			hcnx,							// handle de connexion mailbox � utiliser
	PMBX_HEADER_MESSAGE	pHeaderMessage,	// adresse du header contenant les donn�es a envoyer suivies par les donn�es
	DWORD			dwTailleMessage,	// taille du buffer 
	BOOL			bDemandeAR				// active l'attente d'Accus� de R�ception si TRUE
	)
  {
  PCONNEXION	pConnexion = PfromHCNX (hcnx);
  DWORD				dwRet = MBX_ERR_MBX_UNKOWN_STATE;
  
	Verif (pConnexion);
	AccesCnxExclusif (pConnexion);
  if (pConnexion->nEtat == CNX_CONNEXION_EN_COURS)
    {
		// Mise � jour du header de message
    pHeaderMessage->IdMessage = MBX_MSG_USER;
    pHeaderMessage->dwTailleDonnees = dwTailleMessage - sizeof(MBX_HEADER_MESSAGE);

		// Envoi du message utilisateur au correspondant
    if (bEnvoiMessageAuCorrespondant (pConnexion, pHeaderMessage, &((PMESSAGE_MBX)pHeaderMessage)->aDonnees, bDemandeAR))
			dwRet = 0;
		else
      dwRet = MBX_ERR_PUT_MSG;
    }
  else
    dwRet = MBX_ERR_MBX_NOT_COMM_STATE;
	FinAccesCnxExclusif (pConnexion);
  return dwRet;
  } // dwMbxEcriture

//----------------------------------------------------------------------
// Envoi d'un message Quelconque � un correspondant.
// Le buffer de donn�es NE NECESSITE PAS de commencer par une structure MBX_HEADER_MESSAGE
//-----------------------------------------------------------------------
DWORD dwMbxEcritureBuffer
	(
	HCNX			hcnx,							// handle de connexion mailbox � utiliser
	PVOID			pBufMessage,	// adresse du header contenant les donn�es a envoyer suivies par les donn�es
	DWORD			dwTailleMessage,	// taille du buffer 
	BOOL			bDemandeAR				// active l'attente d'Accus� de R�ception si TRUE
	)
  {
  PCONNEXION	pConnexion = PfromHCNX (hcnx);
  DWORD				dwRet = MBX_ERR_MBX_UNKOWN_STATE;
  
	Verif (pConnexion);
	if(dwTailleMessage)
		Verif (pBufMessage);
	AccesCnxExclusif (pConnexion);
	if (pConnexion->nEtat == CNX_CONNEXION_EN_COURS)
		{
		// Mise � jour du header de message
		MBX_HEADER_MESSAGE HeaderMessage;
		HeaderMessage.IdMessage = MBX_MSG_USER;
		HeaderMessage.dwTailleDonnees = dwTailleMessage;

		// Envoi du message utilisateur au correspondant
		if (bEnvoiMessageAuCorrespondant (pConnexion, &HeaderMessage, pBufMessage, bDemandeAR))
			dwRet = 0;
		else
			dwRet = MBX_ERR_PUT_MSG;
		}
	else
		dwRet = MBX_ERR_MBX_NOT_COMM_STATE;
	FinAccesCnxExclusif (pConnexion);
  return dwRet;
  } // dwMbxEcriture

//--------------------------------------------------------------------------
// R�ception d'un message syst�me ou issu du correspondant.
//--------------------------------------------------------------------------
DWORD dwMbxLecture 
	(
	HCNX	hcnx,					// handle de connexion mailbox � utiliser
	PVOID * ppBufDest,			// adresse du pointeur sur le buffer recevant les donn�es lues
	BOOL	bAvecAttente,	// Attente avec time out courant si TRUE, pas d'attente sinon
	BOOL	bCopieBuffer	// Si TRUE recopie le message initial, sinon donne le message initial � lib�rer
	)
  {
  DWORD				dwRet = 0;
  DWORD				code_ret_sem = 0;
  PVOID				pBufRX = NULL;
  PCONNEXION	pConnexion = PfromHCNX (hcnx);

	Verif (pConnexion);

	AccesCnxExclusif (pConnexion);

	// lit la queue de r�ception
  DWORD	dwTailleMessageRecu = 0;
	HSEM	hsemQueueRX = NULL;
  dwRet = uLitQueue (pConnexion->hQueueRX, NULL, &dwTailleMessageRecu, &pBufRX, FALSE, &hsemQueueRX);

	// pas de message re�u ?
	if (dwRet != 0)
		{
		// oui => attente d'un message n�cessaire ?
		if (bAvecAttente)
			{
			// oui => attente avec time out de l'arriv�e d'un message
			FinAccesCnxExclusif (pConnexion);
			dwRet = dwSemAttenteLibre (hsemQueueRX, MBX_DEF_ACK_TIMEOUT);//$$pConnexion->dwTimeOutRX);
			AccesCnxExclusif (pConnexion);

			// traite selon le r�sultat de l'attente
			switch (dwRet)
				{
				case 0:
					dwRet = uLitQueue (pConnexion->hQueueRX, NULL, &dwTailleMessageRecu, &pBufRX, FALSE, NULL);
					if (dwRet != 0)
						dwRet = MBX_NO_MSG;
					break;
				case SEM_TIMEOUT:
					dwRet = MBX_ERR_TIMEOUT;
					break;
				default:
					dwRet = MBX_ERR_WAIT_EVENT;
				}
			}
		else
			dwRet = MBX_NO_MSG;
		}
	FinAccesCnxExclusif (pConnexion);

	// un message a �t� re�u ?
	if (dwRet == 0)
    {
		// oui => prise en compte du message re�u
		if (bCopieBuffer)
			{
			// on le recopie dans le buffer de l'appelant et on le lib�re
			memcpy (*ppBufDest, pBufRX, dwTailleMessageRecu);
			MemLibere (&pBufRX);
			}
		else
			{
			// on renvoie directement l'adresse du buffer message sans le liberer
			*ppBufDest = pBufRX;
			}
    }

  return dwRet;
  } // dwMbxLecture

//--------------------------------------------------------------------------
// Renvoie le s�maphore d'attente de r�ception de la connexion, NULL si erreur
//--------------------------------------------------------------------------
HSEM hsemCnxAttenteReception (HCNX	hcnx)			// handle de connexion � utiliser
	{
	HSEM				hsemRes = NULL;
	PCONNEXION	pConnexion = PfromHCNX (hcnx);

	if (pConnexion)
		{
		// r�cup�re les infos de de la connexion courante
		AccesCnxExclusif (pConnexion);
		Verif (bQueueGetSemAttenteElement (pConnexion->hQueueRX, &hsemRes));
		FinAccesCnxExclusif (pConnexion);
		}

	return hsemRes;
  } // hsemCnxAttenteReception

//--------------------------------------------------------------------------
// Savoir si une connexion serveur est reli�e � un correspondant
//--------------------------------------------------------------------------
BOOL bCnxAUnCorrespondant	
	(
	HCNX hcnx,								// handle de connexion � tester
	DWORD dwTimeOutAttente		// Time out max d'attente de la connexion en ms ou MBX_INDEFINITE_WAIT
	)
  {
	BOOL				bRes = FALSE;
	PCONNEXION	pConnexion = PfromHCNX (hcnx);
	int					nEtat;
	HSEM				hsemAttenteConnexion;

	Verif (pConnexion);

	// r�cup�re les infos de de la connexion courante
	AccesCnxExclusif (pConnexion);
	nEtat = pConnexion->nEtat;
	if (dwTimeOutAttente)
		Verif (bQueueGetSemAttenteElement (pConnexion->hQueueRX, &hsemAttenteConnexion));
	FinAccesCnxExclusif (pConnexion);

	// connexion en cours ?
	if (nEtat == CNX_CONNEXION_EN_COURS)
		// oui => un correspondant
		bRes = TRUE;
	else
		{
		// attente connexion possible ?
		if (dwTimeOutAttente && (nEtat == CNX_EN_ATTENTE_CONNEXION))
			{
			// oui => attente connexion
			bRes = (dwSemAttenteLibre (hsemAttenteConnexion, dwTimeOutAttente) == 0);
			}
		}

	return bRes;
  } // bCnxAUnCorrespondant

//------------------------------------------------------------------------
// Envoi d'un accus� de reception. 
//------------------------------------------------------------------------
DWORD dwMbxEnvoiAR 
	(
	HCNX hcnx	// handle de connexion � utiliser
	)
  {
	DWORD				dwRes = MBX_ERR_MBX_NOT_COMM_STATE;
  PCONNEXION	pConnexion = PfromHCNX (hcnx);

	Verif (pConnexion);
	AccesCnxExclusif (pConnexion);

	// Action compatible avec l'�tat actuel du CNX ?
  if ((pConnexion->nEtat == CNX_CONNEXION_EN_COURS)&&
			(pConnexion->hCnxCorrespondant))
    {
		PCONNEXION pConnexionCorrespondant = PfromHCNX (pConnexion->hCnxCorrespondant);

		// oui => acc�s au correspondant
		AccesCnxExclusif (pConnexionCorrespondant);

		// Action compatible avec l'�tat du CNX correspondant ?
		if (pConnexionCorrespondant->nEtat == CNX_CONNEXION_EN_COURS)
			{
			// oui => envoi du message dans sa queue
	    if (bSemLibere (pConnexionCorrespondant->hsemAttenteAR))
	      dwRes = 0;
			}
		FinAccesCnxExclusif (pConnexionCorrespondant);
		}

	FinAccesCnxExclusif (pConnexion);
	return dwRes;
  } // dwMbxEnvoiAR

//-------------------------------------------------------------------------
// Attente d'un accus� de reception sur une connexion pendant au plus timeout ms.
//-------------------------------------------------------------------------
DWORD dwMbxAttenteAR 
	(
	HCNX hcnx,		// handle de connexion � utiliser
	DWORD dwTimeOutAttente	// Time out max d'attente de l'accus� de r�ception (ms) ou MBX_INDEFINITE_WAIT
	)
  {
  PCONNEXION	pConnexion = PfromHCNX (hcnx);
  DWORD				dwRet =  MBX_ERR_MBX_NOT_COMM_STATE;

	Verif (pConnexion);
	AccesCnxExclusif (pConnexion);

	// attente AR possible ?
  if (pConnexion->nEtat == CNX_CONNEXION_EN_COURS)
    {
		HSEM	hsemAttenteAR = pConnexion->hsemAttenteAR;

		// attente de l'arriv�e de l'AR
		FinAccesCnxExclusif (pConnexion);
		dwRet = dwSemAttenteLibre (hsemAttenteAR, dwTimeOutAttente);
		if (dwRet)
			{
			if (dwRet == SEM_TIMEOUT)
				dwRet = MBX_ERR_TIMEOUT;
			else
				dwRet = MBX_ERR_WAIT_EVENT;
			}
    }
	else
		FinAccesCnxExclusif (pConnexion);

	return dwRet;
  } // dwMbxAttenteAR

// ----------------------------- fin Mbx.c -------------------------------



/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgex.h                                              |
 |   Auteur  :	MC							|
 |   Date    :	12/01/94						|
 |                                                                      |
 |   Remarques : gestion des boites de dialogue de pcsexe              |
 +----------------------------------------------------------------------*/

#define NB_MAX_TITRE_FENETRE 80
#define NB_MAX_CAR 80


typedef struct
	{
	BOOL a_ecrire;
	void *ptr_zone1;
	int nb_car;
	DWORD nb_synoptiques;
	} CHOIX_PAGES_AFFICHEES_EXE;

typedef void t_proc_maj_liste (DWORD *index, BOOL *visible, char *chaine);

typedef struct
	{
	t_proc_maj_liste *ptr_proc_maj_liste;
	BOOL a_ecrire;
	void *ptr_zone1;
	void *ptr_zone2;
	DWORD nb_synoptiques;
	DWORD fen_impression;
	char fen_document[NB_MAX_CAR];
	} t_dlg_liste_imp_ex;

typedef struct
	{
	DWORD index;
	DWORD no_synoptique;
	BOOL etat;
	DWORD valeur;
	char message[NB_MAX_CAR];
	} t_buff_syn_ex;


typedef struct
	{
	char szNumRelease[80];
	int iNumFonction;
	DWORD dwNbVariables;
	BOOL a_programmer;
	} t_dlg_release_cle;


BOOL  dlg_liste_syn_ex (HWND hwnd, CHOIX_PAGES_AFFICHEES_EXE *data);
BOOL  dlg_liste_imp_ex (HWND hwnd, t_dlg_liste_imp_ex *data);
BOOL  dlg_diag_ex (HWND hwnd, t_dlg_release_cle *data);

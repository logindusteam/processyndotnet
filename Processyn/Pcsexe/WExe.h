// ------------------------------------------------------------------
// WExe.h
// Win32 JS 11/10/97
// Fen�tre principale de processyn Exe (Dans un thread � part)
// HWND mis dans Appli.hwnd
// --------------------------------------------------------------------------

// Messages sp�cifiques g�r�s par cette fen�tre
#define WM_CONFIRME						WM_USER+3
#define WM_SET_DLG_SELECT_FIC	WM_USER+4

// thread fen�tre principale de PcsExe
UINT __stdcall uThreadFenEx (void *hThrd);

// Fermeture de la fen�tre WExe (inter thread)
BOOL bPostDestroyWExe (void);

// Change le titre de la fen�tre WExe (m�me thread)
void FenSetTitre (char *pszTitre);

// Accessibilit� du menu de la fen�tre
BOOL ExisteMenu(void);
void SetMenuExe(BOOL set);

// D�bloque l'attente de lecture de la queue d'actions
void ActionsExeLibereAttente(void);

// Modifications de l'�tat du menu
void MnuEtatCharge(void);
void MnuEtatLance(void);
void MnuEtatArrete(void);
void MnuEtatVide(void);
void MnuEtatToutGris(void);

// Mise � jour du titre
void MnuTitreAJour(void);
void MnuTitreAJourRun(void);

// Fait apparaitre un statut dans la barre de titre
void SetAlarmeCode (BOOL set);
BOOL bAlarmeCode (void);

// Handle table d'acc�l�rateurs pour r�cup�rer les touches fonctions
extern HACCEL hAccelTouchesFonctions;

// Message WM_COMMAND Prise en compte des acc�l�rateurs des touches fonctions
// Renvoie TRUE s'il s'agissait d'une commande acc�l�rateur
BOOL bOnCommandAccelTouchesFonctions (WPARAM wparam);

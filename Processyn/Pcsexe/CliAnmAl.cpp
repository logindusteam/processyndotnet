// --------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"               // Types Standards
#include "MemMan.h"
#include "DocMan.h"
#include "threads.h"           // Multi-T�ches................: thrd_xxx()
#include "init_txt.h"          // Mode lancement Local - Remote
#include "USem.h"
#include "ITC.h"               // Mail Box....................: mbx_xxx ()
#include "tipe_mbx.h"          // Nom (MBX) du canal PROCESSYN

#include "SrvAnmAl.h"          // Types Donn�es Animateur <--> Processyn
#include "WAnmAl.h"           // Fenetres PM.................: PmAnmAlxxx ()
#include "DebugDumpVie.h"

#include "CliAnmAl.h"
#include "DebugDumpVie.h"
#include "Verif.h"
VerifInit;

// Messages re�us par uThreadClientCommunicationAnmAl
#define ITC_ANM_AL_CREATE_ITC         (THRD_FCT_USER +  0)
#define MBX_ANM_AL_CONNECT_EMISS      (THRD_FCT_USER +  1)
#define MBX_ANM_AL_CONNECT_RECEP      (THRD_FCT_USER +  2)
#define MBX_ANM_AL_CALL               (THRD_FCT_USER +  3)
#define MBX_ANM_AL_CREATE_MEM_READ    (THRD_FCT_USER +  4)
#define MBX_ANM_AL_LISTEN             (THRD_FCT_USER +  5)
#define MBX_ANM_AL_READ_MSG           (THRD_FCT_USER +  6)
#define MBX_ANM_AL_DEL_MEM_READ       (THRD_FCT_USER +  7)
#define MBX_ANM_AL_CLOSE_RECEP        (THRD_FCT_USER +  8)
#define MBX_ANM_AL_CLOSE_EMISS        (THRD_FCT_USER +  9)
#define MBX_ANM_AL_DISCONNECT_RECEP   (THRD_FCT_USER + 10)
#define MBX_ANM_AL_DISCONNECT_EMISS   (THRD_FCT_USER + 11)
#define MBX_ANM_AL_DEL_MBX            (THRD_FCT_USER + 12)

// Erreurs envoy�es par uThreadClientCommunicationAnmAl
#define MBX_ANM_AL_ERR_NONE           0
#define MBX_ANM_AL_ERR_CREATE_MBX     1
#define MBX_ANM_AL_ERR_CONNECT_EMISS  2
#define MBX_ANM_AL_ERR_CONNECT_RECEP  3
#define MBX_ANM_AL_ERR_CALL           4
#define MBX_ANM_AL_ERR_CREATE_MEM     5
#define MBX_ANM_AL_ERR_LISTEN         6
#define MBX_ANM_AL_ERR_END            7

// Messages re�us par l'animateur Emis par PCS-EXE
typedef struct
  {
	CITC::ITC_HEADER_MESSAGE  Entete;
  PVOID								Donnee;
  } MESSAGE_RECEPTION_MBX_ANM_AL;

// Messages �mis par l'animateur Re�u par PCS-EXE
typedef struct
  {
  CITC::ITC_HEADER_MESSAGE  Entete;
  PVOID								Donnee;
  } MESSAGE_EMISSION_MBX_ANM_AL, *PMESSAGE_EMISSION_MBX_ANM_AL;


// Handles communication avec PcsExe
static CITC ITCClientReceptionAnmAl;
static CITC ITCClientEmissionAnmAl;

// Tailles des piles des t�ches cr��es
#define SIZE_STACK_THREAD_MBX   4096
#define SIZE_STACK_THREAD_PM    6144

static 	HTHREAD	hThrdAnimAlarmes = NULL;


//---------------------------------------------------------------------------
// thread client communication avec scrut_al (Pcsexe)
static UINT __stdcall uThreadClientCommunicationAnmAl (void *hThrd)
  {
	DumpVie(uThreadClientCommunicationAnmAl);
  //
  MESSAGE_RECEPTION_MBX_ANM_AL* pMessageRecu;

  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);

	// boucle tant que le thread re�oit des messages de son pilote
  UINT		IdMessage=0;
  PVOID		pParamIn = NULL;
  PVOID		pParamOut = NULL;
  DWORD *	pwError = NULL;
  PVOID * pTemp = NULL;
	ITC_TX_RX ITCServeurAnmAl = {NULL,NULL};
  while (bThreadAttenteMessage ((HTHREAD)hThrd, &pParamIn, &pParamOut, &pwError, &IdMessage))
    {
		// traitement selon le message
    switch (IdMessage)
      {
      case ITC_ANM_AL_CREATE_ITC: // 1
				{
				// R�cup�re les ITC de communication serveurs
				ITCServeurAnmAl = *(PITC_TX_RX)pParamIn;

				//hmbxClientAnmAl = hmbxCreeClient (szNomServeurPcsExe);
				// if (hmbxClientAnmAl != NULL)
          *pwError = MBX_ANM_AL_ERR_NONE;
				// else
        //  *pwError = MBX_ANM_AL_ERR_CREATE_MBX;
				}
        break;

      case MBX_ANM_AL_CONNECT_EMISS:
      case MBX_ANM_AL_CONNECT_RECEP:
				{
        // $$ obsolete (bMbxConnecte) 2 et 3
        *pwError = MBX_ANM_AL_ERR_NONE;
				}
        break;

      case MBX_ANM_AL_CALL: // 4
				{
        // devrait sp�cifier aussi szNomServeurPcsExe
        if (ITCClientEmissionAnmAl.bCreeClientTX(szTopicReceptionAnmAl, ITCServeurAnmAl.pITCRX))
          *pwError = MBX_ANM_AL_ERR_NONE;
        else
          *pwError = MBX_ANM_AL_ERR_CALL;
				}
        break;

      case MBX_ANM_AL_CREATE_MEM_READ: // 5
				{
			  DWORD   wSizeData = dwTailleInfosServicesAnmAl ();
        pMessageRecu = (MESSAGE_RECEPTION_MBX_ANM_AL*)pMemAlloue ((sizeof (CITC::ITC_HEADER_MESSAGE)) + wSizeData);
        if (pMessageRecu != NULL)
          {
          pTemp = (void **) pParamOut;
          (*pTemp) = pMessageRecu;
          *pwError = MBX_ANM_AL_ERR_NONE;
          }
        else
          *pwError = MBX_ANM_AL_ERR_CREATE_MEM;
				}
        break;

      case MBX_ANM_AL_LISTEN: // 6
				{
        if (ITCClientReceptionAnmAl.bCreeClientRX (szTopicEmissionAnmAl, ITCServeurAnmAl.pITCTX))
					*pwError = MBX_ANM_AL_ERR_NONE;
        else
          *pwError = MBX_ANM_AL_ERR_LISTEN;
        //MbxSetTimeOutAttenteAR (ITCClientReceptionAnmAl, MBX_DEF_ACK_TIMEOUT);
				}
        break;

			// 7 connexion �tablie : boucle de traitement jusqu'� la fin de l'ex�cution
      case MBX_ANM_AL_READ_MSG:
				{
        pMessageRecu = (MESSAGE_RECEPTION_MBX_ANM_AL*)pParamIn;
        if (ITCClientReceptionAnmAl.dwLecture ((PVOID*)&pMessageRecu, TRUE, TRUE) == 0)
          {
          switch ((pMessageRecu->Entete).IdMessage)
            {
            case ITC_MSG_USER:
							pTemp = (void **) pParamOut;
							(*pTemp) = &(pMessageRecu->Donnee);
							*pwError = MBX_ANM_AL_ERR_NONE;
							break;
            default:
							//case MBX_MSG_FERME:
							*pwError = MBX_ANM_AL_ERR_END;
							break;
            }
          }
				}
        break;

      case MBX_ANM_AL_DEL_MEM_READ:
				{
        pMessageRecu = (MESSAGE_RECEPTION_MBX_ANM_AL*)pParamIn;
        MemLibere ((PVOID *)(&pMessageRecu));
        *pwError = MBX_ANM_AL_ERR_NONE;
				}
        break;

      case MBX_ANM_AL_CLOSE_RECEP:
        {
        ITCClientReceptionAnmAl.Ferme ();
        *pwError = MBX_ANM_AL_ERR_NONE;
				}
        break;

      case MBX_ANM_AL_CLOSE_EMISS:
        {
        ITCClientEmissionAnmAl.Ferme ();
        *pwError = MBX_ANM_AL_ERR_NONE;
				}
        break;

      case MBX_ANM_AL_DISCONNECT_RECEP:
      case MBX_ANM_AL_DISCONNECT_EMISS:
				{
        // $$ obsolete (bMbxDeconnecte)
        *pwError = MBX_ANM_AL_ERR_NONE;
				}
        break;

      case MBX_ANM_AL_DEL_MBX:
				{
        //dwMbxFerme (&hmbxClientAnmAl);
        *pwError = MBX_ANM_AL_ERR_NONE;
				}
        break;

      default:
        break;
      }
    // Message trait�
    ThreadFinTraiteMessage ((HTHREAD)hThrd);
    } // while (bThreadAttenteMessage ((HTHREAD)hThrd, &pParamIn, &pParamOut, &pwError, &IdMessage))
  return uThreadTermine ((HTHREAD)hThrd, 0);
  } // uThreadClientCommunicationAnmAl

// ---------------------------------------------------------------
// Thread correspondant � l'ancien process d'animation d'alarme
//---------------------------------------------------------------------------
static UINT __stdcall uThreadAnimationAlarme (void *hThrd)
  {
	DumpVie(uThreadAnimationAlarme);
	PVOID pParamIn = NULL;
  UINT   wIdMessage = 0;
	static ITC_TX_RX ITCServeurAnmAl = {NULL,NULL};

  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
  while (bThreadAttenteMessage ((HTHREAD)hThrd, &pParamIn, NULL, NULL, &wIdMessage))
    {
    switch (wIdMessage)
      {
      case THRD_FCT_START:
				{
				HTHREAD	hThrdMbx = NULL;
				HTHREAD	hThrdPm = NULL;
				DWORD   wErrorMbx = 0;
				DWORD   wErrorPm = 0;
				PVOID	  pBufMemRead = NULL;
				PVOID		pBufServices = NULL;
				BOOL		bEnd = 0;
				BOOL		bFctMbxToWait = 0;
				ITCServeurAnmAl = *(PITC_TX_RX)pParamIn;

				// Connexion avec PcsExe via Mbx
				if (bThreadCree (&hThrdMbx, uThreadClientCommunicationAnmAl, SIZE_STACK_THREAD_MBX))
					{
					if (dwThreadExecuteMessage (hThrdMbx, ITC_ANM_AL_CREATE_ITC, &ITCServeurAnmAl, NULL, &wErrorMbx) == MBX_ANM_AL_ERR_NONE)
						{
						if (dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_CONNECT_EMISS, NULL, NULL, &wErrorMbx) == MBX_ANM_AL_ERR_NONE)
							{
							if (dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_CONNECT_RECEP, NULL, NULL, &wErrorMbx) == MBX_ANM_AL_ERR_NONE)
								{
								if (dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_CALL, NULL, NULL, &wErrorMbx) == MBX_ANM_AL_ERR_NONE)
									{
									if (dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_CREATE_MEM_READ, NULL, &pBufMemRead, &wErrorMbx) == MBX_ANM_AL_ERR_NONE)
										{
										ThreadEnvoiMessage (hThrdMbx, MBX_ANM_AL_LISTEN, NULL, NULL, &wErrorMbx);
										bFctMbxToWait = TRUE;

										// cr�ation de l'interface utilisateur de l'animateur
										if (bThreadCree (&hThrdPm, uThreadWAnmAl, SIZE_STACK_THREAD_PM))
											{
											if (dwThreadExecuteMessage (hThrdPm, W_ANM_AL_INIT, NULL, NULL, &wErrorPm) == W_ANM_AL_ERR_NONE)
												{
												if (dwThreadExecuteMessage (hThrdPm, W_ANM_AL_INIT_MODE_PM, NULL, NULL, &wErrorPm) == W_ANM_AL_ERR_NONE)
													{
													if (dwThreadExecuteMessage (hThrdPm, W_ANM_AL_CREATE_WINDOWS, NULL, NULL, &wErrorPm) == W_ANM_AL_ERR_NONE)
														{
														// Ecoute asynchrone des messages Windows
														ThreadEnvoiMessage (hThrdPm, W_ANM_AL_PM_LOOP, NULL, NULL, &wErrorPm);

														// Attente connection processyn au MBX
														bThreadAttenteFinTraiteMessage (hThrdMbx, INFINITE);
														//
														WAnmAlBreakPmLoop ();
														bThreadAttenteFinTraiteMessage (hThrdPm, INFINITE);

														bFctMbxToWait = FALSE;
														if (wErrorMbx == MBX_ANM_AL_ERR_NONE)
															{
															// Animateur d'alarme correctement connect� au serveur, fen�tres op�rationelles :
															// boucle tant que le serveur fonctionne
															bEnd = FALSE;
															while (!bEnd)
																{
																ThreadEnvoiMessage (hThrdPm, W_ANM_AL_PM_LOOP, NULL, NULL, &wErrorPm);
																//
																bEnd = (dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_READ_MSG, pBufMemRead, &pBufServices, &wErrorMbx) == MBX_ANM_AL_ERR_END);
																//
																WAnmAlBreakPmLoop ();
																bThreadAttenteFinTraiteMessage (hThrdPm, INFINITE);
																//
																if (!bEnd)
																	{
																	dwThreadExecuteMessage (hThrdPm, W_ANM_AL_READ_SERVICES, pBufServices, NULL, &wErrorPm);
																	dwThreadExecuteMessage (hThrdPm, W_ANM_AL_EXEC_SERVICES, NULL, NULL, &wErrorPm);
																	}
																} // while (!bEnd)
															}
														dwThreadExecuteMessage (hThrdPm, W_ANM_AL_DEL_WINDOWS, NULL, NULL, &wErrorPm);
														}
													dwThreadExecuteMessage (hThrdPm, W_ANM_AL_END_MODE_PM, NULL, NULL, &wErrorPm);
													}
												dwThreadExecuteMessage (hThrdPm, W_ANM_AL_TERMINE, NULL, NULL, &wErrorPm);
												}
											bThreadFerme (&hThrdPm, INFINITE);
											}
										//
										if (bFctMbxToWait)
											{
											bThreadAttenteFinTraiteMessage (hThrdMbx, INFINITE);
											}
										dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_DEL_MEM_READ, pBufMemRead, NULL, &wErrorMbx);
										}
									}
								dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_CLOSE_RECEP, NULL, NULL, &wErrorMbx);
								dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_DISCONNECT_RECEP, NULL, NULL, &wErrorMbx);
								}
							dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_CLOSE_EMISS, NULL, NULL, &wErrorMbx);
							dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_DISCONNECT_EMISS, NULL, NULL, &wErrorMbx);
							}
						dwThreadExecuteMessage (hThrdMbx, MBX_ANM_AL_DEL_MBX, NULL, NULL, &wErrorMbx);
						}
					bThreadFerme (&hThrdMbx, INFINITE);
					}
				ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, 0);
				} // case THRD_FCT_START:
				break;
      } // switch (wIdMessage)
    } // while (bThreadAttenteMessage ((HTHREAD)hThrd, ...
	ITCServeurAnmAl.pITCRX = NULL;
	ITCServeurAnmAl.pITCTX = NULL;
  return uThreadTermine ((HTHREAD)hThrd, 0);
  } // uThreadAnimationAlarme

// --------------------------------------------------------------------------
//											FONCTIONS EXPORTEES
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
void MbxAnmAlAck (void)
  {
  ITCClientReceptionAnmAl.dwEnvoiAR ();
  }

// --------------------------------------------------------------------------
BOOL MbxAnmAlSend (DWORD wSizeMsg, PVOID pMsg)
  {
  BOOL	bOk = FALSE;

  if (ITCClientEmissionAnmAl.bAUnCorrespondant(0))
    {
		DWORD                        wSizeMsgEmission = sizeof (CITC::ITC_HEADER_MESSAGE) + wSizeMsg;
		PMESSAGE_EMISSION_MBX_ANM_AL pMessageEmission = (PMESSAGE_EMISSION_MBX_ANM_AL)pMemAlloue (wSizeMsgEmission);

    if (pMessageEmission != NULL)
      {
      memcpy (&(pMessageEmission->Donnee), pMsg, wSizeMsg);
      if (ITCClientEmissionAnmAl.dwEcriture (&pMessageEmission->Entete, wSizeMsgEmission ,FALSE) == 0)
        bOk = TRUE;
      MemLibere ((PVOID *)(&pMessageEmission));
      }
    }

  return bOk;
  }

// --------------------------------------------------------------------------
// cr�ation du thread d'animation d'alarme
BOOL bExecuteAnimAlarmes (PITC_TX_RX pITCServeurAnmAl)
	{
	BOOL		bRes = FALSE;

	// cr�ation du Thread d'animation Alarme
	if (bThreadCree (&hThrdAnimAlarmes, uThreadAnimationAlarme, SIZE_STACK_THREAD_MBX))
		{
		ThreadEnvoiMessage (hThrdAnimAlarmes, THRD_FCT_START, pITCServeurAnmAl, NULL, NULL);
		bRes = TRUE;
		}
	return bRes;
  } // bExecuteAnimAlarmes

// --------------------------------------------------------------------------
// fermeture du thread d'animation d'alarme
BOOL bFinExecuteAnimAlarmes (void)
	{
	BOOL bRes = FALSE;

	// fermeture du Thread d'animation Alarme
	if (hThrdAnimAlarmes)
		{
		bRes = TRUE;
		bThreadFerme (&hThrdAnimAlarmes, INFINITE);
		}

	return bRes;
  } // bFinExecuteAnimAlarmes

// ----------------------- fin Mbxanmal.c -------------------------------

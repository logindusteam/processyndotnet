//-------------------------------------------------------------
// UDCOM.h
// Outils pour la mise en oeuvre de DCOM
//---------------------------------------------------------------------------------

//-----------------------------------------------------------------
// Fonctions DCOM
//-----------------------------------------------------------------
// Ouverture / fermeture de l'acc�s � DCOM (lien dynamique � la DLL)
BOOL bDCOMInitialiseLocal (BOOL bInitialiseCOM);
void DCOMFermeLocal (BOOL bLibereCOM);

// Interface sur le gestionnaire m�moire de COM
extern IMalloc * pDCOMMem;

#ifdef ACCES_FONCTIONS_COM_DYNAMIQUE
	// Liens blind� � chaque fonction DCOM �ventuellement charg�e
	HRESULT _CLSIDFromProgID (LPCOLESTR lpszProgID, LPCLSID lpclsid);
	HRESULT _CoGetClassObject (REFCLSID rclsid, DWORD dwClsContext, LPVOID pvReserved, REFIID riid, LPVOID FAR* ppv);
	HRESULT _CoGetMalloc (DWORD dwMemContext, LPMALLOC FAR* ppMalloc);
	HRESULT _CoInitialize (LPVOID pvReserved);
	void _CoUninitialize (void);
	HRESULT _CoCreateInstanceEx (REFCLSID Clsid, IUnknown * punkOuter, DWORD dwClsCtx, COSERVERINFO * pServerInfo, DWORD dwCount, MULTI_QI * pResults);
	#ifdef _WIN32_DCOM
	HRESULT _CoInitializeSecurity (PSECURITY_DESCRIPTOR pSecDesc, LONG cAuthSvc,
		SOLE_AUTHENTICATION_SERVICE *asAuthSvc, void *pReserved1, DWORD dwAuthnLevel,
		DWORD dwImpLevel, void *pReserved2, DWORD dwCapabilities, void *pReserved3);
	#endif

	UINT _SysStringLen (BSTR bstrSource);
	void _VariantInit(VARIANTARG * pvarg);
	HRESULT _VariantClear (VARIANTARG * pvarg);
	HRESULT _VariantCopy (VARIANTARG * pvargDest, VARIANTARG * pvargSrc);
	HRESULT _VariantChangeType (VARIANTARG * pvargDest, VARIANTARG * pvarSrc, USHORT wFlags, VARTYPE vt);

	#define CLSIDFromProgID _CLSIDFromProgID
	#define CoGetClassObject _CoGetClassObject
	#define CoGetMalloc _CoGetMalloc
	#define CoInitialize _CoInitialize
	#define CoUninitialize _CoUninitialize
	#define CoCreateInstanceEx _CoCreateInstanceEx 
	#define CoInitializeSecurity _CoInitializeSecurity

	#define SysStringLen _SysStringLen
	#define VariantInit _VariantInit
	#define VariantClear _VariantClear
	#define VariantCopy _VariantCopy
	#define VariantChangeType _VariantChangeType
#endif
//--------------------------------------------------------------------
// Transfert et conversions de chaines pour OLE
// Remarque : toutes les chaines OLE sont unicodes allou�es pour OLE
//--------------------------------------------------------------------
// Alloue et duplique une chaine WCHAR dans une chaine WCHAR
WCHAR * WSTRClone(const WCHAR *oldstr, IMalloc *pmem);

// Alloue et duplique une chaine ANSII dans une chaine WCHAR
WCHAR * pWSTRFromSTR(const CHAR *temp, IMalloc *p);

// Alloue et duplique une chaine WCHAR dans une chaine ANSII
CHAR * pSTRFromWSTR(const WCHAR *temp, IMalloc *p);

// Lib�re une chaine WCHAR
void WSTRFree(WCHAR * c, IMalloc *pmem);

// Lib�re une chaine ANSII
void SBCSFree(CHAR *temp, IMalloc *p);


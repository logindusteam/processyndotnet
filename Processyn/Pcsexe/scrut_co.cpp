/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : unite du traitement des instructions;                    |
 |   Auteur  : AC							|
 |   Date    : 1993							|
 +----------------------------------------------------------------------*/
// Win32 24/9/97

#include "stdafx.h"
#include <math.h>
#include "std.h"
#include "lng_res.h"    // bMotReserve () pour les logiques 'H' & 'B'
#include "tipe.h"
#include "tipe_co.h"
#include "tipe_re.h"
#include "tipe_tb.h"
#include "pcs_sys.h"
#include "UStr.h"
#include "USem.h"
#include "BitMan.h"
#include "mem.h"
#include "driv_dq.h"
#include "temps.h"
#include "UChrono.h"                
#include "UCom.h"
#include "convert.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "PcsVarEx.h"
#include "ExeDebug.h"
#include "ExeOuvFic.h"
#include "DllMuxEx.h"

#include "scrut_tm.h"
#include "scrut_tp.h"
#include "scrut_dq.h"
#include "scrut_im.h"

#include "driv_dif.h"
#include "DLLMan.h"
#include "PathMan.h"
#include "DocMan.h"
#include "ULangues.h"
#include "ExecuteF.h"
#include "scrut_co.h"
#include "Verif.h"
VerifInit;


#define c_max_reel ((FLOAT)1.0E38)
#define c_sin_cos_max ((FLOAT)1.0E8)
#define c_demi_max_reel ((FLOAT)0.5E38)
#define c_long_min ((FLOAT)(-2147483647))
#define c_long_max ((FLOAT)(2147483647))
#define c_i16_max ((FLOAT)(32767))
#define c_i16_min ((FLOAT)(-32768))
#define c_word_max ((FLOAT)(65535))
#define c_init_ret 0xffff

//------------------------------------------------------------------------
#define c_taille_pile 256

typedef union
  {
  BOOL clog;
  FLOAT cnum;
  char cmes[82];
  } element_pile;

//-------------------------------------------------------------------
// 1     pile pleine
// 2     log ou racine carre de valeur negative
// 4     overflow sur exponentielle
// 8     overflow sur fct trigo
// 16    overflow sur fct arithmetique
// 32    operandes de testbit hors limites
// 64    conversion bcd invalide
// 128   conversion binaire invalide
// 256   conversion numerique invalide
// 512   overflow calcul arithmetique
// 1024  division par 0
// 2048  conversion par CAR invalide
// 4096  concatenation impossible
// 8192  formatage impossible
// 16384 borne liste > taille liste
// 32768 instruct relative a chrono(metro) sur va generique non = chrono(metro)
static void maj_status (DWORD erreur)
  {
	CPcsVarEx::SetBitsValVarSysNumEx (VA_SYS_STATUS_CODE, erreur);
  }

//-------------------------------------------------------------------
// 1      index tableau LOG > taille
// 2      index tableau NUM > taille
// 4      index tableau MES > taille
// 8      taille tableau LOG source d�pass�e
// 16     taille tableau LOG destination d�pass�e
// 32     taille tableau NUM source d�pass�e
// 64     taille tableau NUM destination d�pass�e
// 128    taille tableau MES source d�pass�e
// 256    taille tableau MES destination d�pass�e
// 512
// 1024
// 2048
// 4096
// 8192
// 16384
// 32768
static void maj_status_tableau (DWORD erreur)
  {
	CPcsVarEx::SetBitsValVarSysNumEx (VA_SYS_STATUS_TABLEAU, erreur);
  }

//------------------------------------------------------------------------
// initialisation du code de l'application utilisateur
//------------------------------------------------------------------------
void init_co (void)
  {
	// $$ gestion des op�rations en virgule flottante : autorisation des exceptions
	// _controlfp (_MCW_EM, _EM_INVALID | _EM_DENORMAL | _EM_ZERODIVIDE | _EM_OVERFLOW | _EM_UNDERFLOW | _EM_INEXACT);

	// gestion des op�rations en virgule flottante : interdiction des exceptions
	// _controlfp (_MCW_EM, 0);

	// Initialisation du contexte d'ex�cution des execute_f
	InitExecuteF();
	}

//------------------------------------------------------------------------
// fin du code de l'application utilisateur
//------------------------------------------------------------------------
void termine_co (void)
  {
	// Fermeture du contexte d'ex�cution des execute_f
	TermineExecuteF();
	}

//------------------------------------------------------------------------
// traitement du code de l'application utilisateur
//------------------------------------------------------------------------
void traitement_co (void)
  {
  if (existe_repere (bx_code))
    {
		PBOOL 			ptr_b;
		PFLOAT			ptr_r;
		PSTR				ptr_m;
		DWORD       dwNPCodeDebutMain;
		DWORD       dwNPCodeExecution;
		DWORD        index_x1;
		DWORD        index_ret;
		DWORD        dwNDernierPCodeExecutionPartielle;
		DWORD        dwNbTotalPCodes;
		DWORD        dwNPremierPCodeExecutionPartielle;
		DWORD        taille_liste;
		DWORD        unite_de_saut;
		DWORD        index_iteration_courante;
		DWORD        offset_liste;
		LONG         taille_x_tableau;
		LONG         pos_x_tableau;
		element_pile pile [c_taille_pile];
		int          i;
		//
		int   taille_mes1;
		int   taille_mes2;
		char  mes_tempo1[nb_max_caractere+1];
		char  mes_tempo2[nb_max_caractere+1];
		char  mes_tempo3[nb_max_caractere+1];
		FLOAT   reel_tempo;
		int   int_tempo1;
		int   int_tempo2;
		DWORD  word_tempo;
		PDWORD       adresse_donnee;
		PX_TABLEAU	adresse_tab;
		int         fonction_pcs;
		char       pszEtatBas  [c_nb_car_message_res];
		char       pszEtatHaut [c_nb_car_message_res];
		int   val_ascii;
		const double log_c_max_reel = log (c_max_reel);
		int   sp = 0;
		DWORD ligne_en_cours = 0;

    if (existe_repere (bx_main_nb))
      {
   		PDWORD	pdwNPCode = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_main_nb,1); //$$ donner un type sp�cifique
      dwNPCodeDebutMain = *pdwNPCode;
      }
    else
      {
      dwNPCodeDebutMain = 1;
      }
    dwNbTotalPCodes = nb_enregistrements (szVERIFSource, __LINE__, bx_code);

    //2 boucles for imbriqu�es pour optimiser le test en mode debugger
    for (index_x1 = dwNPCodeDebutMain; (index_x1 <= dwNbTotalPCodes); index_x1++)
      {
      if (CtxtDebugger.On)
        {
			  PDWORD pdwNLigneCode = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_debug_code, index_x1);

        if (*pdwNLigneCode != ligne_en_cours)
          {
          AttentePasAPas(*pdwNLigneCode);
          if (*pdwNLigneCode == PointArret())
            {
            AttentePArret();
            }
          ligne_en_cours = (*pdwNLigneCode);
          }
        dwNPremierPCodeExecutionPartielle = index_x1;
        dwNDernierPCodeExecutionPartielle = index_x1;
        index_ret = c_init_ret;
        }
      else
        {
        dwNPremierPCodeExecutionPartielle = dwNPCodeDebutMain;
        dwNDernierPCodeExecutionPartielle = dwNbTotalPCodes;
        }
      //debugger
      for (dwNPCodeExecution = dwNPremierPCodeExecutionPartielle; 
				dwNPCodeExecution <= dwNDernierPCodeExecutionPartielle; dwNPCodeExecution++)
        {
				// D�passement de la pile ?
        if ((sp < 0) || (sp > c_taille_pile))
          {
					// oui => err
          maj_status (1);
          }
        else
          {
					// non => Traite une instruction
          PP_CODE pPCode = (PP_CODE)pointe_enr (szVERIFSource, __LINE__, bx_code, dwNPCodeExecution);
          switch (pPCode->PcsOpCode)
            {
            case push_var_log:
              sp++;
              pile[sp].clog = CPcsVarEx::bGetValVarLogEx (pPCode->operande);
              break;

            case push_avar_log:
              ptr_b = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, pPCode->operande);
              sp++;
              pile[sp].clog = (*ptr_b);
              break;

            case push_var_num:
              sp++;
              pile[sp].cnum = CPcsVarEx::fGetValVarNumEx(pPCode->operande);
              break;

            case push_avar_num:
              ptr_r = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,pPCode->operande);
              sp++;
              pile[sp].cnum = (*ptr_r);
              break;

            case push_var_mes:
              sp++;
							CPcsVarEx::pszGetValVarMesEx (pile[sp].cmes, pPCode->operande);
              break;

            case push_avar_mes:
              ptr_m = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes,pPCode->operande);
              sp++;
              StrCopy (pile[sp].cmes, ptr_m);
              break;

            case push_const_0:
              sp++;
              pile[sp].clog = FALSE;
              break;

            case push_const_1:
              sp++;
              pile[sp].clog = TRUE;
              break;

            case push_const_num:
              ptr_r = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_num,pPCode->operande);
              sp++;
              pile[sp].cnum = (*ptr_r);
              break;

            case push_const_mes:
              ptr_m = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_mes,pPCode->operande);
              sp++;
              StrCopy (pile[sp].cmes, ptr_m);
              break;

            case push_var_log_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              sp++;
              pile[sp].clog = CPcsVarEx::bGetValVarLogEx(*adresse_donnee);
              break;

            case push_var_num_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              sp++;
              pile[sp].cnum = CPcsVarEx::fGetValVarNumEx(*adresse_donnee);
              break;

            case push_var_mes_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              sp++;
              CPcsVarEx::pszGetValVarMesEx (pile[sp].cmes, *adresse_donnee);
              break;

            case push_var_log_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp].cnum <= 0))
                {
                maj_status_tableau (1);
                }
              else
                {
                pile[sp].clog = CPcsVarEx::bGetValVarLogEx (adresse_tab->pos_x_es + (DWORD)(pile[sp].cnum) - 1);
                }
              break;

            case push_var_num_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp].cnum <= 0))
                {
                maj_status_tableau(2);
                }
              else
                {
                pile[sp].cnum = CPcsVarEx::fGetValVarNumEx (adresse_tab->pos_x_es + (DWORD)(pile[sp].cnum) - 1);
                }
              break;

            case push_var_mes_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp].cnum <= 0))
                {
                maj_status_tableau (4);
                }
              else
                {
                CPcsVarEx::pszGetValVarMesEx (pile[sp].cmes, adresse_tab->pos_x_es + (DWORD)(pile[sp].cnum) - 1);
                }
              break;

            case push_avar_log_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp].cnum <= 0))
                {
                maj_status_tableau (1);
                }
              else
                {
                ptr_b = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log,adresse_tab->pos_x_es + (DWORD)(pile[sp].cnum) - 1);
                pile[sp].clog = (*ptr_b);
                }
              break;

            case push_avar_num_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau, pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp].cnum <= 0))
                {
                maj_status_tableau (2);
                }
              else
                {
                ptr_r = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, adresse_tab->pos_x_es + (DWORD)(pile[sp].cnum) - 1);
                pile[sp].cnum = (*ptr_r);
                }
              break;

            case push_avar_mes_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp].cnum <= 0))
                {
                maj_status_tableau (4);
                }
              else
                {
                ptr_m = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes,adresse_tab->pos_x_es + (DWORD)(pile[sp].cnum) - 1);
                StrCopy (pile[sp].cmes, ptr_m);
                }
              break;

            case push_tableau:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              pos_x_tableau = adresse_tab->pos_x_es;
              pile[sp].cnum = (FLOAT)(pos_x_tableau) + pile[sp].cnum - 1;
              sp++;
              pile[sp].cnum = (FLOAT)(pos_x_tableau + adresse_tab->taille);
              break;

            case trans_tab_log:
              if ((pile[sp-4].cnum + pile[sp].cnum) > pile[sp-3].cnum)
                {
                maj_status_tableau (8);
                }
              else
                {
                if ((pile[sp-2].cnum + pile[sp].cnum) > pile[sp-1].cnum)
                  {
                  maj_status_tableau (16);
                  }
                else
                  {
									CPcsVarEx::TransfertBlocCourLogEx ((DWORD)(pile[sp].cnum), (DWORD)(pile[sp-4].cnum),(DWORD)(pile[sp-2].cnum));
                  }
                }
              sp = sp - 5;
              break;

            case trans_tab_num:
              if ((pile[sp-4].cnum + pile[sp].cnum) > pile[sp-3].cnum)
                {
                maj_status_tableau (32);
                }
              else
                {
                if ((pile[sp-2].cnum + pile[sp].cnum) > pile[sp-1].cnum)
                  {
                  maj_status_tableau (64);
                  }
                else
                  {
									CPcsVarEx::TransfertBlocCourNumEx ((DWORD)(pile[sp].cnum), (DWORD)(pile[sp-4].cnum),(DWORD)(pile[sp-2].cnum));
                  }
                }
              sp = sp - 5;
              break;

            case trans_tab_mes:
              if ((pile[sp-4].cnum + pile[sp].cnum) > pile[sp-3].cnum)
                {
                maj_status_tableau (128);
                }
              else
                {
                if ((pile[sp-2].cnum + pile[sp].cnum) > pile[sp-1].cnum)
                  {
                  maj_status_tableau (256);
                  }
                else
                  {
									CPcsVarEx::TransfertBlocCourMesEx ((DWORD)(pile[sp].cnum), (DWORD)(pile[sp-4].cnum),(DWORD)(pile[sp-2].cnum));
                  }
                }
              sp = sp - 5;
              break;

            case push_avar_log_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              ptr_b = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log,(*adresse_donnee));
              sp++;
              pile[sp].clog = (*ptr_b);
              break;

            case push_avar_num_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              ptr_r = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,(*adresse_donnee));
              sp++;
              pile[sp].cnum = (*ptr_r);
              break;

            case push_avar_mes_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              ptr_m = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes,(*adresse_donnee));
              sp++;
              StrCopy (pile[sp].cmes, ptr_m);
              break;

            case push_const_log_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              ptr_m = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_mes, (*adresse_donnee));
              sp++;
              if (!bMotReserve (c_res_zero, pszEtatBas))
                {
                StrCopy (pszEtatBas, "B");
                }
              if (!bMotReserve (c_res_un, pszEtatHaut))
                {
                StrCopy (pszEtatHaut, "H");
                }
              if (!StrToBOOL (&pile[sp].clog, ptr_m, pszEtatBas, pszEtatHaut))
                {
                pile[sp].clog = FALSE;
                }

              break;

            case push_const_num_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              ptr_r = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_num,(*adresse_donnee));
              sp++;
              pile[sp].cnum = (*ptr_r);
              break;

            case push_const_mes_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              ptr_m = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_mes,(*adresse_donnee));
              sp++;
              StrCopy (pile[sp].cmes, ptr_m);
              break;

            case push_val_operande:
            case push_val_operande_log:
            case push_val_operande_num:
            case push_val_operande_mes:
              sp++;
              pile[sp].cnum = (FLOAT) (pPCode->operande);
              break;

            case push_val_operande_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              sp++;
              pile[sp].cnum = (FLOAT)(*adresse_donnee);
              break;

            case push_index_chrono_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              sp++;
              pile[sp].cnum = (FLOAT) (index_spec_chrono ((*adresse_donnee)));
              if (pile[sp].cnum == 0)
                maj_status ((DWORD)32768);
              break;

            case push_index_metro_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
              sp++;
              pile[sp].cnum = (FLOAT) (index_spec_metro ((*adresse_donnee)));
              if (pile[sp].cnum == 0)
                maj_status ((DWORD)32768);
              break;

            case pop_var_log:
							CPcsVarEx::SetValVarLogEx (pPCode->operande, pile[sp].clog);
              sp--;
              break;

            case pop_var_num:
							CPcsVarEx::SetValVarNumEx (pPCode->operande, pile[sp].cnum);
              sp--;
              break;

            case pop_var_mes:
							CPcsVarEx::SetValVarMesEx (pPCode->operande, pile[sp].cmes);
              sp--;
              break;

            case pop_var_log_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
							CPcsVarEx::SetValVarLogEx ((*adresse_donnee), pile[sp].clog);
              sp--;
              break;

            case pop_var_num_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
							CPcsVarEx::SetValVarNumEx ((*adresse_donnee), pile[sp].cnum);
              sp--;
              break;

            case pop_var_mes_r:
              adresse_donnee = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,pPCode->operande+offset_liste);
							CPcsVarEx::SetValVarMesEx ((*adresse_donnee), pile[sp].cmes);
              sp--;
              break;

            case pop_var_log_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp-1].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp-1].cnum <= 0))
                maj_status_tableau(1);
              else
                {
								CPcsVarEx::SetValVarLogEx (adresse_tab->pos_x_es + (DWORD)(pile[sp-1].cnum) - 1, pile[sp].clog);
                }
              sp = sp - 2;
              break;

            case pop_var_num_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp-1].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp-1].cnum <= 0))
                maj_status_tableau (2);
              else
                {
								CPcsVarEx::SetValVarNumEx (adresse_tab->pos_x_es + (DWORD)(pile[sp-1].cnum) - 1, pile[sp].cnum);
                }
              sp = sp - 2;
              break;

            case pop_var_mes_t:
              adresse_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,pPCode->operande);
              taille_x_tableau = adresse_tab->taille;
              if ((pile[sp-1].cnum > (FLOAT)(taille_x_tableau)) || (pile[sp-1].cnum <= 0) )
                maj_status_tableau (4);
              else
                {
								CPcsVarEx::SetValVarMesEx (adresse_tab->pos_x_es + (DWORD)(pile[sp-1].cnum) - 1, pile[sp].cmes);
                }
              sp = sp - 2;
              break;

            case inferieur_log:
              pile[sp-1].clog = (BOOL)(pile[sp-1].clog < pile[sp].clog);
              sp--;
              break;

            case inferieur_num:
              pile[sp-1].clog = (BOOL)(pile[sp-1].cnum < pile[sp].cnum);
              sp--;
              break;

            case inferieur_mes:
              pile[sp-1].clog = (BOOL)(StrCompare (pile[sp-1].cmes, pile[sp].cmes) < 0);
              sp--;
              break;

            case inferieur_egal_log:
              pile[sp-1].clog = (BOOL)(pile[sp-1].clog <= pile[sp].clog);
              sp--;
              break;

            case inferieur_egal_num:
              pile[sp-1].clog = (BOOL)(pile[sp-1].cnum <= pile[sp].cnum);
              sp--;
              break;

            case inferieur_egal_mes:
              pile[sp-1].clog = (BOOL)(StrCompare (pile[sp-1].cmes, pile[sp].cmes) <= 0);
              sp--;
              break;

            case different_log:
              pile[sp-1].clog = (BOOL)(pile[sp-1].clog != pile[sp].clog);
              sp--;
              break;

            case different_num:
              pile[sp-1].clog = (BOOL)(pile[sp-1].cnum != pile[sp].cnum);
              sp--;
              break;

            case different_mes:
              pile[sp-1].clog = (BOOL)(StrCompare (pile[sp-1].cmes, pile[sp].cmes) != 0);
              sp--;
              break;

            case superieur_log:
              pile[sp-1].clog = (BOOL)(pile[sp-1].clog > pile[sp].clog);
              sp--;
              break;

            case superieur_num:
              pile[sp-1].clog = (BOOL)(pile[sp-1].cnum > pile[sp].cnum);
              sp--;
              break;

            case superieur_mes:
              pile[sp-1].clog = (BOOL)(StrCompare (pile[sp-1].cmes, pile[sp].cmes) > 0);
              sp--;
              break;

            case superieur_egal_log:
              pile[sp-1].clog = (BOOL)(pile[sp-1].clog >= pile[sp].clog);
              sp--;
              break;

            case superieur_egal_num:
              pile[sp-1].clog = (BOOL)(pile[sp-1].cnum >= pile[sp].cnum);
              sp--;
              break;

            case superieur_egal_mes:
              pile[sp-1].clog = (BOOL)(StrCompare (pile[sp-1].cmes, pile[sp].cmes) >= 0);
              sp--;
              break;

            case egal_log:
              pile[sp-1].clog = (BOOL)(pile[sp-1].clog == pile[sp].clog);
              sp--;
              break;

            case egal_num:
              pile[sp-1].clog = (BOOL)(((pile[sp-1].cnum) == (pile[sp].cnum)));
              sp--;
              break;

            case egal_mes:
              pile[sp-1].clog = (BOOL)(StrCompare (pile[sp-1].cmes, pile[sp].cmes) == 0);
              sp--;
              break;

            case deb_par:
              pile[sp-1].clog = (BOOL) (StrSearchStr (pile[sp-1].cmes, pile[sp].cmes) == 0);
              sp  = sp-1;
              break;

            case fin_par:
							{
              StrSetNull (mes_tempo1);
              taille_mes1 = StrLength (pile[sp].cmes);
              taille_mes2 = StrLength (pile[sp-1].cmes);
              if (taille_mes1 <= taille_mes2)
                {
                for (i = (taille_mes2 - taille_mes1); (i <= taille_mes2-1); i++ )
                  {
                  StrConcatChar (mes_tempo1, pile[sp-1].cmes[i]);
                  }
                }
              pile[sp-1].clog = bStrEgales (mes_tempo1, pile[sp].cmes);
              sp--;
							}
              break;

            case bit_test:
              if ((pile[sp-1].cnum < c_long_min) || (pile[sp-1].cnum > c_long_max))
                maj_status (32);
              else
                {
                if ((pile[sp].cnum < 0) || (pile[sp].cnum > 15) )
                  maj_status (32);
                else
                  pile[sp-1].clog = tstbit ((DWORD)(pile[sp-1].cnum), (DWORD)(pile[sp].cnum));
                }
              sp--;
              break;

            case passe_val:
              pile[sp-2].clog = (BOOL)
               (((pile[sp].cnum < pile[sp-1].cnum) &&
                 (pile[sp-2].cnum >= pile[sp-1].cnum))
              ||
               ((pile[sp].cnum > pile[sp-1].cnum) &&
                (pile[sp-2].cnum <= pile[sp-1].cnum)));
              sp = sp - 2;
              break;

            case passe_val_h:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].cnum < pile[sp-1].cnum) &&
               (pile[sp-2].cnum >= pile[sp-1].cnum));
              sp = sp - 2;
              break;

            case passe_val_b:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].cnum > pile[sp-1].cnum) &&
                (pile[sp-2].cnum <= pile[sp-1].cnum));
              sp = sp - 2;
              break;

            case prend_val_log:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].clog != pile[sp-1].clog) &&
               (pile[sp-2].clog == pile[sp-1].clog));
              sp = sp - 2;
              break;

            case prend_val_num:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].cnum != pile[sp-1].cnum) &&
               (pile[sp-2].cnum == pile[sp-1].cnum));
              sp = sp - 2;
              break;

            case prend_val_mes:
              pile[sp-2].clog = (BOOL)
               (!bStrEgales (pile[sp].cmes, pile[sp-1].cmes) &&
                bStrEgales (pile[sp-2].cmes, pile[sp-1].cmes));
              sp = sp - 2;
              break;

            case quitte_val_log:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].clog == pile[sp-1].clog) &&
               (pile[sp-2].clog != pile[sp-1].clog));
              sp = sp - 2;
              break;

            case quitte_val_num:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].cnum == pile[sp-1].cnum) &&
               (pile[sp-2].cnum != pile[sp-1].cnum));
              sp = sp - 2;
              break;

            case quitte_val_mes:
              pile[sp-2].clog = (BOOL)
               ((bStrEgales (pile[sp].cmes, pile[sp-1].cmes)) &&
               (!bStrEgales (pile[sp-2].cmes, pile[sp-1].cmes)));
              sp = sp - 2;
              break;

            case prend_val_h:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].cnum < pile[sp-1].cnum) &&
               (pile[sp-2].cnum == pile[sp-1].cnum));
              sp = sp - 2;
              break;

            case prend_val_b:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].cnum > pile[sp-1].cnum) &&
               (pile[sp-2].cnum == pile[sp-1].cnum));
              sp = sp - 2;
              break;

            case quitte_val_h:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].cnum == pile[sp-1].cnum) &&
               (pile[sp-2].cnum > pile[sp-1].cnum));
              sp = sp - 2;
              break;

            case quitte_val_b:
              pile[sp-2].clog = (BOOL)
               ((pile[sp].cnum == pile[sp-1].cnum) &&
               (pile[sp-2].cnum < pile[sp-1].cnum));
              sp = sp - 2;
              break;

            case saut:
              dwNPCodeExecution = pPCode->operande - 1 ;
              break;

            case saut_cond:
              if (!pile[sp].clog)
                {
                dwNPCodeExecution = pPCode->operande - 1 ;
                }
              sp--;
              break;

            case push_goto:  // sauve le Num�ro de PCode sur la pile et saute � l'adresse en op�rande
              sp++;
              pile[sp].cnum = (FLOAT)dwNPCodeExecution;
              if (CtxtDebugger.On)
                {
                index_ret = pPCode->operande - 1;  // $$ manque MAJ dwNPCodeExecution ?
                }
              else
                {
                dwNPCodeExecution = pPCode->operande - 1;
                }
              break;

            case pop_ret:
              dwNPCodeExecution = (DWORD)pile[sp].cnum;
              if (CtxtDebugger.On)
                {
                index_ret = dwNPCodeExecution;
                }
              sp--;
              break;

            case change_signe:
              pile[sp].cnum = - pile[sp].cnum ;
              break;

            case op_non:
              pile[sp].clog = (BOOL)(!pile[sp].clog) ;
              break;

            case op_logarithme:
              if (pile[sp].cnum <= 0)
                maj_status (2);
              else
                pile[sp].cnum = (FLOAT)log (pile[sp].cnum) ;
              break;

            case op_exponentiel :
              if (pile[sp].cnum > (FLOAT)log_c_max_reel)
                maj_status (4);
              else
                pile[sp].cnum = (FLOAT)exp (pile[sp].cnum) ;
              break;

            case op_sinus:
              if (pile[sp].cnum > c_sin_cos_max )
                maj_status (8);
              else
                pile[sp].cnum = (FLOAT)sin (pile[sp].cnum) ;
              break;

            case op_cosinus:
              if (pile[sp].cnum > c_sin_cos_max )
                maj_status (8);
              else
                pile[sp].cnum = (FLOAT)cos (pile[sp].cnum) ;
              break;

            case op_tangente:
              if ((pile[sp].cnum > c_sin_cos_max) ||  ((FLOAT)cos (pile[sp].cnum) == 0))
                maj_status (8);
              else
                pile[sp].cnum = (FLOAT)sin (pile[sp].cnum) / (FLOAT)cos(pile[sp].cnum);
              break;

            case op_absolue:
              pile[sp].cnum = (FLOAT)fabs (pile[sp].cnum) ;
              break;

            case op_carre:
              if (fabs(pile[sp].cnum) > sqrt(c_max_reel))
                maj_status (16);
              else
                pile[sp].cnum = pile[sp].cnum * pile[sp].cnum ;
              break;

            case op_racine:
              if (pile[sp].cnum < 0 )
                maj_status (2);
              else
                pile[sp].cnum = (FLOAT)sqrt(pile[sp].cnum) ;
              break;

            case op_log_10:
              if (pile[sp].cnum <= 0 )
                maj_status (2);
              else
                pile[sp].cnum = (FLOAT)log10(pile[sp].cnum);
              break;

            case op_partie_entiere:
              if (fabs (pile[sp].cnum - 1) >= c_long_max)
                maj_status (16);
              else
                pile[sp].cnum = (FLOAT)((LONG)(pile[sp].cnum)) ;
              break;

            case op_bcd:
							if ((pile[sp].cnum > c_word_max) ||
                  (pile[sp].cnum < c_i16_min))
                 maj_status (64);
              else
                {
								WORD wTemp;

                if (!cvtBcdToBin ((WORD)(pile[sp].cnum), &wTemp))
                   maj_status (64);
                else
                  {
                  pile[sp].cnum = (FLOAT) (wTemp);
                  }
                }
              break;

            case op_valbcd:
							{
							if ((pile[sp].cnum > c_word_max) ||
                  (pile[sp].cnum < c_i16_min))
                 maj_status (64);
              else
                {
								WORD wTemp;

                if (!cvtBinToBcd ((WORD)(pile[sp].cnum), &wTemp))
                   maj_status (64);
                else
                  {
                  pile[sp].cnum = (FLOAT) (wTemp);
                  }
                }
							}
              break;

            case op_bin:
							{
              if (fabs(pile[sp].cnum + 0.5) >= c_long_max )
                maj_status (128);
              else
                {
                StrDWordToStrBase (mes_tempo1, (DWORD)(pile[sp].cnum), 2);
                for (i = StrLength (mes_tempo1); (i <= 15); i++)
                  {
                  StrInsertChar (mes_tempo1, '0', 0);
                  }
                StrCopy (pile[sp].cmes, mes_tempo1);
                }
							}
              break;

            case op_valbin:
              if (StrToDWORDBase (&word_tempo, pile[sp].cmes, 2))
                {
                pile[sp].cnum = (FLOAT)word_tempo;
                }
              else
                {
                pile[sp].cnum = (FLOAT) 0;
                maj_status (128);
                }
              break;

            case op_val:
              if (StrAllCharsAreInStr (pile[sp].cmes," 0123456789+-.eE"))
                {
                if (StrToFLOAT (&reel_tempo, pile[sp].cmes))
                  {
                  pile[sp].cnum = reel_tempo;
                  }
                else
                  {
                  pile[sp].cnum = (FLOAT) 0;
                  maj_status (256);
                  }
                }
              else
                {
                pile[sp].cnum = (FLOAT) 0;
                maj_status (256);
                }
              break;

            case op_val_ascii:
							{
              if (StrLength (pile[sp].cmes) > 0)
                {
								char  premier_car;

                if (StrToCHAR (&premier_car, pile[sp].cmes))
                  {
                  val_ascii = (int)(premier_car);
                  if (val_ascii < 0)
                    {
                    val_ascii = val_ascii + 256;
                    }
                  pile[sp].cnum = (FLOAT)val_ascii;
                  }
                else
                  {
                  pile[sp].cnum = (FLOAT)0;
                  maj_status (256);
                  }
                }
              else
                {
                pile[sp].cnum = (FLOAT)0;
                }
							}
              break;

            case op_car:
              if ((pile[sp].cnum < 0) || (pile[sp].cnum > 255) )
                maj_status (2048);
              else
                {
                StrCHARToStr (pile[sp].cmes, (char) pile[sp].cnum);
                }
              break;

            case op_tronc:
              if (pile[sp].cnum >= 0 )
                {
                if (pile[sp].cnum < StrLength (pile[sp-1].cmes))
                  {
                  StrSetLength (pile[sp-1].cmes, (DWORD) pile[sp].cnum);
                  }
                }
              else
                {
                if (-pile[sp].cnum <= StrLength (pile[sp-1].cmes))
                StrDelete (pile[sp-1].cmes, 0, StrLength (pile[sp-1].cmes) + (DWORD)(pile[sp].cnum)) ;
                }
              sp--;
              break;

            case op_hexa:
							{
              if (fabs(pile[sp].cnum + 0.5) >= c_long_max )
                maj_status (128);
              else
                {
                StrDWordToStrBase (mes_tempo1, (DWORD)(pile[sp].cnum), 16);
                for (i = StrLength (mes_tempo1); (i <= 7); i++)
                  {
                  StrInsertChar (mes_tempo1, '0', 0);
                  }
                StrCopy (pile[sp].cmes, mes_tempo1);
                }
							}
              break;

            case op_valhexa:
							{
              if (StrToDWORDBase (&word_tempo, pile[sp].cmes, 16))
                {
                pile[sp].cnum = (FLOAT)word_tempo;
                }
              else
                {
                pile[sp].cnum = (FLOAT) 0;
                maj_status (128);
                }
							}
              break;

            case op_ou:
              pile[sp-1].clog = (BOOL)((pile[sp-1].clog) || (pile[sp].clog));
              sp-- ;
              break;

            case op_ou_num:
              if (( (pile[sp-1] .cnum < -c_long_max) || (pile[sp-1].cnum > c_long_max)) &&
                (pile[sp-1] .cnum - (FLOAT)((LONG)(pile[sp-1] .cnum)) != 0))
                {
                maj_status (32);
                }
              else
                {
                if (((pile[sp] .cnum < -c_long_max) || (pile[sp].cnum > c_long_max)) &&
                   (pile[sp] .cnum - (FLOAT)((LONG)(pile[sp] .cnum)) != 0) )
                  {
                  maj_status (32);
                  }
                else
                  {
                  pile[sp-1].cnum = (FLOAT)( ((LONG)(pile[sp-1].cnum)) | ((LONG)(pile[sp].cnum)) );
                  }
                }
              sp-- ;
              break;

            case op_ou_x:
              pile[sp-1].clog = (BOOL) (!(pile[sp-1].clog == pile[sp].clog)) ;
              sp-- ;
              break;

            case op_et:
              pile[sp-1].clog = (BOOL)(pile[sp-1].clog && pile[sp].clog) ;
              sp-- ;
              break;

            case op_et_num:
              if (( (pile[sp-1] .cnum < -c_long_max) || (pile[sp-1].cnum > c_long_max)) &&
                (pile[sp-1] .cnum - (FLOAT)((LONG)(pile[sp-1] .cnum)) != 0))
                {
                maj_status (32);
                }
              else
                {
                if (((pile[sp] .cnum < -c_long_max) || (pile[sp].cnum > c_long_max)) &&
                   (pile[sp] .cnum - (FLOAT)((LONG)(pile[sp] .cnum)) != 0) )
                  {
                  maj_status (32);
                  }
                else
                  {
                  pile[sp-1].cnum = (FLOAT)( ((LONG)(pile[sp-1].cnum)) & ((LONG)(pile[sp].cnum)) );
                  }
                }
              sp-- ;
              break;

            case op_plus:
              if ((fabs(pile[sp].cnum) > c_demi_max_reel) ||
    	      (fabs(pile[sp-1].cnum) > c_demi_max_reel) )
                {
                maj_status (512);
                }
              else
                {
                pile[sp-1].cnum = pile[sp-1].cnum + pile[sp].cnum ;
                }
              sp-- ;
              break;

            case op_moins:
              pile[sp-1].cnum = pile[sp-1].cnum - pile[sp].cnum ;
              sp-- ;
              break;

            case op_multiplie:
							{
							// v�rifie si le produit des deux op�randes est en overflow
							BOOL bOverFlow = FALSE;
							FLOAT fOperande1 = (FLOAT)fabs (pile[sp-1].cnum);
							FLOAT fOperande2 = (FLOAT)fabs (pile[sp].cnum);

							if ((fOperande1 > 1) && (fOperande2 > 1))
								{
								bOverFlow = (BOOL) ((log(fOperande1) + log(fOperande2)) > log_c_max_reel);
								}

							// Overflow ?
              if (bOverFlow)
                {
								// oui = mise � jour status
                maj_status (512);
                }
              else
                {
								// non => effectue l'op�ration
                pile[sp-1].cnum = pile[sp-1].cnum * pile[sp].cnum ;
                }
              sp-- ;
							}
              break;

            case op_divise:
							// division par 0 ?
              if (pile[sp].cnum == 0)
                {
								// oui => erreur
                maj_status (1024);
                }
              else
                {
								// v�rifie si le produit des deux op�randes est en overflow
								BOOL bOverFlow = FALSE;
								FLOAT fOperande1 = (FLOAT)fabs (pile[sp-1].cnum);
								FLOAT fOperande2 = (FLOAT)fabs (pile[sp].cnum);

								if ((fOperande1 != 0) && (fOperande2 != 0))
									{
									bOverFlow = (log(fOperande1) - log(fOperande2)) > log_c_max_reel;
									}

								// Overflow ?
                if (bOverFlow)
                  {
									// oui => err
                  maj_status (512);
                  }
                else
                  {
									// non => r�sultat div
                  pile[sp-1].cnum = pile[sp-1].cnum / pile[sp].cnum ;
                  }
                }
              sp-- ;
              break;

            case op_concatene:
              if ((StrLength (pile[sp-1].cmes) + StrLength (pile[sp].cmes)) <= nb_max_caractere )
                {
                StrConcat (pile[sp-1].cmes, pile[sp].cmes);
                }
              else
                {
                maj_status (4096);
                }
              sp-- ;
              break;

            case op_pointe_log :
            case op_pointe_num :
            case op_pointe_mes :
              sp-- ;
              break;

            case op_detecte_log :
            case op_detecte_num :
            case op_detecte_mes :
              pile[sp].clog = FALSE;
              break;

            case formate_log:
              if (!bMotReserve (c_res_zero, pszEtatBas))
                {
                StrCopy (pszEtatBas, "B");
                }
              if (!bMotReserve (c_res_un, pszEtatHaut))
                {
                StrCopy (pszEtatHaut, "H");
                }
              StrBOOLToStr (pile[sp-1].cmes, pile[sp-1].clog, pszEtatBas, pszEtatHaut);
              if (pile[sp].cnum <= nb_max_caractere)
                {
                if ((fabs (pile[sp].cnum + 0.5) >= c_i16_max))
                  {
                  maj_status (8192);
                  }
                else
                  {
                  taille_mes1 = (DWORD)((int)(pile[sp].cnum) - (int)StrLength (pile[sp-1].cmes));
                  for (i = 1; (i <= (int)taille_mes1); i++)
                    {
                    StrInsertChar (pile[sp-1].cmes, ' ', 0);
                    }
                  }
                }
              else
                {
                maj_status (8192);
                }
              sp-- ;
              break;

            case formate_mes:
              if (pile[sp].cnum <= nb_max_caractere)
                {
                if ((fabs (pile[sp].cnum + 0.5) >= c_i16_max))
                  {
                  maj_status (8192);
                  }
                else
                  {
                  if (pile[sp].cnum < 0 )
                    {
                    for (i = StrLength (pile[sp-1].cmes) + 1; (i <= (int)(-pile[sp].cnum)); i++ )
                      {
                      StrConcatChar (pile[sp-1].cmes, ' ');
                      }
                    }
                  else
                    {
                    taille_mes1 = (DWORD)(pile[sp].cnum) - StrLength (pile[sp-1].cmes);
                    for (i = 1; (i <= (int)taille_mes1); i++)
                      {
                      StrInsertChar (pile[sp-1].cmes, ' ', 0);
                      }
                    }
                  }
                }
              else
                {
                maj_status (8192);
                }
              sp-- ;
              break;

            case formate_num:
              if ((fabs (pile[sp-1].cnum + 0.5) >= c_i16_max) ||
                (fabs (pile[sp].cnum + 0.5) >= c_i16_max) )
                {
                maj_status (256);
                }
              else
                {
                if (pile[sp-1].cnum > nb_max_caractere )
                  {
                  pile[sp-1].cnum = (FLOAT) 0;
                  }
                reel_tempo = pile[sp-2].cnum;
                StrSetNull (mes_tempo1);
                StrSetNull (mes_tempo2);
                StrSetNull (mes_tempo3);
                StrCopy (mes_tempo2, "%");
                int_tempo1 = (int)(pile[sp-1].cnum);
                int_tempo2 = (int)(pile[sp].cnum);
                if (int_tempo1 < 0)
                  {
                  StrConcatChar (mes_tempo2, '-');
                  int_tempo1 = -int_tempo1;
                  }
                StrDWordToStr (mes_tempo3, (DWORD)int_tempo1);
                StrConcat (mes_tempo2, mes_tempo3);
                StrConcatChar (mes_tempo2, '.');
                StrDWordToStr (mes_tempo3, (DWORD)int_tempo2);
                StrConcat (mes_tempo2, mes_tempo3);
                if (int_tempo1 == 0)
                  {
                  if(int_tempo2 == 0)
                    {
                    StrConcatChar (mes_tempo2, 'f');
                    }
                  else
                    {
                    StrConcatChar (mes_tempo2, 'E');
                    }
                  }
                else
                  {
                  StrConcatChar (mes_tempo2, 'f');
                  }
                StrPrintFormat (mes_tempo1, mes_tempo2, reel_tempo);
                StrCopy (pile[sp-2].cmes, mes_tempo1);
                }
              sp = sp - 2 ;
              break;

            case formate_num_sc:
              if ((fabs(pile[sp].cnum) > nb_max_caractere) ||
                 (pile[sp].cnum == 0) )
                {
                reel_tempo = pile[sp-1].cnum;
                StrSetNull (mes_tempo1);
                StrPrintFormat (mes_tempo1, "%E", reel_tempo);
                StrCopy (pile[sp-1].cmes, mes_tempo1);
                }
              else
                {
                if (fabs(pile[sp].cnum + 0.5) >= c_i16_max )
                  {
                  maj_status (256);
                  }
                else
                  {
                  StrSetNull (mes_tempo1);
                  StrSetNull (mes_tempo2);
                  StrSetNull (mes_tempo3);
                  StrCopy (mes_tempo2, "%");
                  reel_tempo = pile[sp-1].cnum;
                  int_tempo1 = (int)(pile[sp].cnum);
                  if (int_tempo1 < 0)
                    {
                    StrConcatChar (mes_tempo2, '-');
                    int_tempo1 = -int_tempo1;
                    }
                  StrDWordToStr (mes_tempo3, (DWORD)int_tempo1);
                  StrConcat (mes_tempo2, mes_tempo3);
                  StrConcat (mes_tempo2, ".0f");
                  StrPrintFormat (mes_tempo1, mes_tempo2, reel_tempo);
                  StrCopy (pile[sp-1].cmes, mes_tempo1);
                  }
                }
              sp--;
              break;

            case ecr_ecran:
              // ----- transformation Message en pszMessage
              affiche_message_texte (pile[sp].cmes);
              sp--;
              break;

            case ecr_imp:
              stocker_imprimante (StrLength (pile[sp].cmes), pile[sp].cmes, TRUE, TRUE);
              sp--;
              break;

    				case ecr_imp_mlg:
              stocker_imprimante (StrLength (pile[sp].cmes), pile[sp].cmes, FALSE, TRUE);
              sp--;
              break;

    				case ecr_imp_brut:
              stocker_imprimante (StrLength (pile[sp].cmes), pile[sp].cmes, FALSE, FALSE);
              sp--;
              break;

            case ouvre_disq:
              scrute_ouvre_dq (pile[sp].cmes);
              sp--;
              break;

            case ecr_disq:
              scrute_ecrit_dq (pile[sp].cmes);
              sp--;
              break;

            case lec_disq:
              scrute_lit_dq (pile[sp].cmes);
              sp--;
              break;

            case ferme_disq:
              scrute_ferme_dq (pile[sp].cmes);
              sp--;
              break;

            case efface_disq:
              efface_dq (pile[sp].cmes);
              sp--;
              break;

            case copie_disq:
              copie_dq (pile[sp-1].cmes, pile[sp].cmes);
              sp = sp - 2;
              break;

            case renomme_disq:
              renomme_dq (pile[sp-1].cmes, pile[sp].cmes);
              sp = sp - 2;
              break;

            case depart_chrono:
              lance_chronometre ((DWORD)pile[sp].cnum); //transmet l'index de la valeur du chrono
              sp--;
              break;

            case arret_chrono:
              arrete_chronometre ((DWORD)pile[sp].cnum); //transmet l'index de la valeur du chrono
              sp--;
              break;

            case raz_chrono:
              raz_chronometre ((DWORD)pile[sp].cnum); //transmet l'index de la valeur du chrono
              sp--;
              break;

            case continu_chrono:
              relance_chronometre ((DWORD)pile[sp].cnum); //transmet l'index de la valeur du chrono
              sp--;
              break;

            case depart_metro:
              lance_metronome ((DWORD)pile[sp].cnum); //transmet l'index de la valeur du metronome
              sp--;
              break;

            case arret_metro:
              arrete_metronome ((DWORD)pile[sp].cnum); //transmet l'index de la valeur du metronome
              sp--;
              break;


            case deb_execute:
              unite_de_saut = pPCode->operande;
    					taille_liste = (DWORD)pile[sp].cnum;
    					if ((pile[sp-1].cnum > 0) && (pile[sp-1].cnum <= pile[sp].cnum) )
    						{
    						index_iteration_courante = (DWORD)pile[sp-1].cnum;
    						offset_liste = unite_de_saut * (index_iteration_courante - 1);
								}
							else
    						{
    						index_iteration_courante = 1;
    						offset_liste = 0;
								maj_status ((DWORD)32768);
    						}
							sp = sp - 2;
							break;

						case fin_execute:
    					if (pile[sp].cnum > 0 )
                {
		    	      if (index_iteration_courante < (DWORD)pile[sp].cnum )
                  {
					        index_iteration_courante++;
    							if (index_iteration_courante > taille_liste )
                    {
    								maj_status (16384);
                    }
    							else
    								{
    								offset_liste = offset_liste + unite_de_saut;
    								// operande pointe sur deb execute : increment du while => pointe sur premiere instruc execute
										if (CtxtDebugger.On)
											{
				    					index_ret = pPCode->operande;
											}
										else
											{
    									dwNPCodeExecution = pPCode->operande;
											}
    								}
    							}
                }
              sp--;
              break;

            case saut_boucle:
              pile[sp].clog = (BOOL)(pile[sp-1].cnum <= pile[sp].cnum);
              if (!pile[sp].clog)
                {
                dwNPCodeExecution = pPCode->operande - 1;
                sp = sp - 2;
                }
              else
                {
                sp--;
                }
              break;

            case fin_boucle:
              if (pile[sp-1].cnum < pile[sp].cnum )
                {
                (pile[sp-1].cnum)++;
                if (CtxtDebugger.On)
                  {
									index_ret = pPCode->operande - 1;
                  }
                else
                  {
                  dwNPCodeExecution = pPCode->operande - 1;
                  }
                }
              else
                {
                sp--;
                }
              sp--;
              break;

            case lance_resident :
              break;

    				case lance_ov1 :
							sp--;
              break;

			 			case lance_ov2 :
              sp--;
              break;

            case lance_fonction :
              if ( (pile[sp].cnum < 0) || (pile[sp].cnum > c_i16_max) ||
                  ((FLOAT)((LONG)(pile[sp].cnum)) != pile[sp].cnum) )
                {
                maj_status_fct(1);
                }
              else
                {
                fonction_pcs = (int)(pile[sp].cnum);
                switch (fonction_pcs)
                  {
									// Fonction place libre sur un disque
                  case 1 :
										ExecuteF1();
                    break;

                  // Fonction Initialisation imprimante
                  case 9 :ExecuteF9();
                    break;

                  // Fonction Place libre dans tampon imprimante
                  case 10 :
										ExecuteF10();
                    break;

									case 12 :// fonction PCS <----> DIf
										ExecuteF12();
										break;

                  // Fonction gestion time-out imprimante
                  case 13 :
										ExecuteF13();
                    break;

									// Fonction mise � jour horloge : date
                  case 16 :
										ExecuteF16();
                    break;

									// Fonction mise � jour horloge : heure
                  case 17 :
										ExecuteF17();
                    break;

									// Fonction longueur d'une chaine
                  case 18 :
										ExecuteF18();
                    break;

									// Fonction Recherche d'un caract�re avec codage ascii
                  case 19 :
										ExecuteF19();
                    break;

									// Fonction Recherche d'une chaine
                  case 20 :
										ExecuteF20();
                    break;

									// Fonction Insertion d'une chaine
                  case 21 :
										ExecuteF21();
                    break;

									// Fonction Suppression d'une partie de cha�ne
                  case 22 :
										ExecuteF22();
                    break;

									// Fonction Substitution dans une cha�ne
                  case 23 :
										ExecuteF23();
                    break;

									// Fonction Extrait une partie de cha�ne � droite
                  case 24 :
										ExecuteF24();
                    break;

									// Fonction Extrait une partie de cha�ne � gauche
                  case 25 :
										ExecuteF25();
                    break;

									// Fonction Extrait une partie de cha�ne
                  case 26 :
										ExecuteF26();
                    break;

									// Fonction r�gulateur Eurotherm 822 : cr�ation de fichier "de trames"
                  //case 27 : break;

									// Fonction r�gulateur Eurotherm 822 : fichier ascii -> fichier reels
                  //case 28 : break;

									// Fonction lancement de la fonction transfert
                  //case 29 : break;

									// Fonction Ordonne le depart_de_transfert
                  //case 30 : break;

									// Fonction envoi message ASCII
									case 31 :
										ExecuteF31();
                    break;

									// Fonction reception message ASCII
									case 32 :
										ExecuteF32();
                    break;

                  case 34 :  // Affichage Qhelp
										ExecuteF34();
                    break;

                  case 35 :  // Affichage dans l'aide d'un fichier
										ExecuteF35();
                    break;

                  case 36 :  // init index Qhelp
										ExecuteF36();
                    break;

									case 37 :  // vide buffer imprimante
										ExecuteF37();
										break;

									case 38 :  // Recupere un nom de fichier via une dlg
										ExecuteF38();
										break;

                  case 39:
										ExecuteF39();
                    break;

                  case 40:	// Appel d'un programme externe
										ExecuteF40();
                    break;

                  case 41:	// Test etat execution d'un programme externe
										ExecuteF41();
                    break;

                  case 42: // Passage en avant plan d'un programme externe
										ExecuteF42();
                    break;

                  case 43:   // Arret d'un programme externe
										ExecuteF43();
                    break;

                  case 44:   // Lib�ration d'un programme externe
										ExecuteF44();
                    break;

                  case 45:   // Joue un son  (si un autre process n'est pas en train de jouer)
										ExecuteF45();
                    break;

									case 46: // Conversion num�ro d'erreur OS (dans REG_NUM1) => Chaine (mis dans REG_MES1)
										// r�sultat mis dans REG_LOG1
										ExecuteF46();
										break;

									case 50: // Ex�cution synchone imm�diate Ouverture fermeture conversation DDE
										ExecuteF50();
										break;

									case 51: // Ex�cution synchone imm�diate de la commande commande DDE Request
										ExecuteF51();
										break;

									case 52: // Ex�cution synchone imm�diate de la commande commande DDE Poke
										ExecuteF52();
										break;

									case 53: // Ex�cution synchone imm�diate de la commande commande DDE Execute
										ExecuteF53();
										break;

									case 54: // Inhibition touches clavier
										ExecuteF54();
										break;

                  // Fonction Tri global de tableau
                  case 60 :ExecuteF60();
                    break;

                  // Fonction Tri partiel de tableau
                  case 61 :ExecuteF61();
                    break;

                  // Fonction Tri global de tableau avec mise � jour d'un index
                  case 62 :ExecuteF62();
                    break;

                  // Fonction Tri partiel de tableau avec mise � jour d'un index
                  case 63 :ExecuteF63();
                    break;

                  // Fonction Grand nombres 
									//MU1=MU1-MU2
                  case 70 :ExecuteF70();
                    break;
									// ExecuteF71 MU1=MU1-MU2
                  case 71 :ExecuteF71();
                    break;
									// ExecuteF72 MU1=MU1xMU2
                  case 72 :ExecuteF72();
                    break;
									// ExecuteF73 MU1=MU1/MU2
                  case 73 :ExecuteF73();
                    break;
									// ExecuteF74 MU1=MU1 Modulo MU2 (division enti�re)
                  case 74 :ExecuteF74();
                    break;
                  // ExecuteF75 Conversion de MU1 en un double mot non sign� 32 bits, r�parti sur WL, WH avec WL = partie basse 16 bit du double mot et WH partie haute 16 bit du double mot
                  case 75 :ExecuteF75();
                    break;
                  // ExecuteF76 Conversion d'un Double mot DW 32 bit en MU, avec MU = 65536x(WH Mot 16 bit) + WL (Mot 16 bit)
                  case 76 :ExecuteF76();
                    break;
                  // ExecuteF77 Conversion de MU1 en chaine de caract�re
                  case 77 :ExecuteF77();
                    break;
                  // ExecuteF78 R�sultat MU1=Conversion Chaine de caract�re num�rique
                  case 78 :ExecuteF78();
                    break;

										//default: break;
                  } // switch fonction_pcs
                } // switch (pPCode->ordre)
              sp--;
              break;

            default:
              break;
            } // switch instruct
          } // else sp
        } // for dwNPCodeExecution
      if (CtxtDebugger.On)
        {
        if (index_ret != c_init_ret)
          {
          index_x1 = index_ret;
          }
        else
          {
          dwNPCodeExecution --;
          index_x1 = dwNPCodeExecution ;
          }
        }
      else
        {
        break;
        }
      } // for index_x1
    } // if (existe_repere (bx_code))
  } // traitement_co

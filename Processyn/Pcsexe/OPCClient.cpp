// OPCClient.cpp: implementation of the COPCClient class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "opc.h"	// Include the GENERIC OPC header file
#include "OLECTL.h"
#include "UDCOM.h"
#include "Verif.h"
#include "TemplateArray.h"
#include "OPCClient.h"
VerifInit;

//-------------------------------------------------------------------
// Constructeur
COPCClient::COPCClient()
	{
	// Initialisation des membres par d�faut (serveur ferm�)
	m_hresult = NO_ERROR;						// Derni�re erreur
	m_hrExecution = HR_NON_CONNECTE;
	m_hrInterruptionExecution = NO_ERROR;						// Derni�re erreur
	m_bAutoriseInterruptionExecution = FALSE;
	m_pIOPCServer = NULL;
	m_nTypeServeur = SERVEUR_INVALIDE;
	m_szNomServeur [0]= '\0';
	m_szNomStation [0]= '\0';
	//m_pIOPCServerPublicGroups = NULL;
	//m_pIOPCBrowseServerAddressSpace = NULL;
	//m_pIPersistFile = NULL;
	//m_pIEnumOPCItemAttributes = NULL;
	}

//-------------------------------------------------------------------
// Destructeur
COPCClient::~COPCClient()
	{
	// Arr�te l'ex�cution si elle est en cours
	if (bExecutionEnCours())
		bFinExecute();

	// Lib�ration des ressources
	SupprimeGroupes ();

	// rend un �ventuel acc�s post�rieur invalide
	m_nTypeServeur = SERVEUR_INVALIDE;
	}

//-------------------------------------------------------------------
// Configuration des param�tres d'acc�s au serveur
BOOL COPCClient::bConfigureAccesServeur (PCSTR pszNomServeur, TYPE_SERVEUR nType, PCSTR pszNomStation)
	{
	// pas d'acc�s en cours ?
	BOOL bRes = !bExecutionEnCours();

	if (bRes)
		{
		// Ok par d�faut
		SetErreur (NO_ERROR);

		// param�tres valides ?
		bRes = (pszNomServeur && pszNomServeur[0] &&
		(nType >= SERVEUR_AUTO) && (nType <= SERVEUR_REMOTE));

		if (bRes)
			{
			// oui : met � jour les param�tres d'acc�s
			// valide le nom de station
			if (nType == SERVEUR_REMOTE)
				{
				if (pszNomStation && pszNomStation[0])
					strcpy_s (m_szNomStation, sizeof(m_szNomStation),pszNomStation);
				else
					{
					SetErreur (E_INVALIDARG);
					bRes = FALSE;
					}
				}
			else
				m_szNomStation [0] = '\0';

			// Ok ?
			if (bRes)
				{
				// oui => copie type et nom du serveur
				m_nTypeServeur = nType;
				strcpy_s (m_szNomServeur, sizeof(m_szNomServeur),pszNomServeur);
				}
			else
				SetErreur (E_INVALIDARG);
			}
		}
	else
		SetErreur (E_NOTIMPL);

	// Erreur de configuration ?
	if (!bRes)
		// oui => type de serveur invalide
		m_nTypeServeur = SERVEUR_INVALIDE;

	return bRes;
	} // bConfigureAccesServeur

//-------------------------------------------------------------------
// ajoute syst�matiquement un groupe au serveur
// renvoie son index dans le tableau de groupes
BOOL COPCClient::bAjouteGroupe(PINT pnNNouveauGroupe, OPCHANDLE hClientGroupe, PCSTR pszNomGroupe, BOOL bActif, DWORD dwRafraichissementMs)
	{
	BOOL bRes = TRUE;

	// Ajoute le groupe dans la liste
	COPCGroupe * pGroupeOPC = new COPCGroupe (this, hClientGroupe, pszNomGroupe, bActif, dwRafraichissementMs);
	m_apGroupes.Add (pGroupeOPC);

	// Mise � jour de son num�ro pour le parent
	*pnNNouveauGroupe = m_apGroupes.GetSize() - 1;

	// Ex�cution du serveur en cours ?
	if (bExecutionEnCours())
		{
		// oui => lancement du groupe Ok ?
		if (!pGroupeOPC->bExecute ())
			{
			// non => note l'erreur 
			m_hresult = pGroupeOPC->hrDerniereErreur();
			bRes = FALSE;
			}
		}
	return bRes;
	}

//-------------------------------------------------------------------
// r�cup�re l'adresse d'un groupe ou NULL
COPCGroupe* COPCClient::pGetCOPCGroupe (int nIdGroupe) const
	{
	COPCGroupe * pOPCGroupe = m_apGroupes[nIdGroupe];
	return pOPCGroupe;
	}

//////////////////////////////////////////////////////////////////////
// Connexion - d�connexion
//////////////////////////////////////////////////////////////////////

// Instancie et configure un serveur selon sa description
BOOL COPCClient::bExecute (void)
	{
	BOOL bRes = FALSE;

	// Ex�cution d�ja en cours ?
	if (bExecutionEnCours())
		{
		// oui => erreur
		SetErreur(ERROR_INVALID_FUNCTION);
		}
	else
		{
		// non => tentative de connexion :
		// Pas d'interruption en cours
		m_hrInterruptionExecution = NO_ERROR;						// Derni�re erreur

		// reset statut
		m_hrExecution = HR_NON_CONNECTE;

		// R�cuperation du CLSID du serveur � partir de son nom Ok ?
		CLSID clsid;
		PWCHAR pszwNomServeur = pWSTRFromSTR (m_szNomServeur, 0);
		HRESULT hr = CLSIDFromProgID (pszwNomServeur, &clsid);
		WSTRFree(pszwNomServeur, 0);

		LPUNKNOWN pIUnknownOPC = NULL;
		if (SUCCEEDED (hr))
			{
			CLSCTX clsctx = CLSCTX_LOCAL_SERVER;
			// par d�faut pas de nom de serveur remote
			COSERVERINFO	CoServerInfo;
			COSERVERINFO* pCoServerInfo = NULL;

			// oui => prise en compte du type d'acc�s au serveur
			switch (m_nTypeServeur)
				{
				case SERVEUR_AUTO:
					clsctx = (CLSCTX) (CLSCTX_INPROC_SERVER | CLSCTX_LOCAL_SERVER | CLSCTX_REMOTE_SERVER);
					break;
				case SERVEUR_INPROC:
					clsctx = CLSCTX_INPROC_SERVER;
					break;
				case SERVEUR_LOCAL:
					clsctx = CLSCTX_LOCAL_SERVER;
					break;

				case SERVEUR_REMOTE:
					{
					clsctx = CLSCTX_REMOTE_SERVER;
					if(m_szNomStation && m_szNomStation[0])
						{
						pCoServerInfo = &CoServerInfo;
						CoServerInfo.dwReserved1 = 0;
						CoServerInfo.dwReserved2 = 0;
						CoServerInfo.pwszName = pWSTRFromSTR (m_szNomStation, 0);
						CoServerInfo.pAuthInfo = 0;
						}
					else
						hr = E_INVALIDARG;
					}
					break;

				case SERVEUR_INVALIDE:
				default:
					{
					hr = E_INVALIDARG;
					}
					break;
				} // switch (m_nTypeServeur)

			// type de serveur valide ?
			if (SUCCEEDED (hr))
				{
				// oui => chargement du serveur :
				// demande de l'interface
				MULTI_QI multi_qi;
				multi_qi.pIID = &IID_IUnknown;
				multi_qi.hr = 0;
				multi_qi.pItf = 0;
				
				// demande de chargement du serveur
				hr = CoCreateInstanceEx(clsid, NULL, clsctx, pCoServerInfo, 1, &multi_qi);
				
				// Chargement Ok ?
				if (SUCCEEDED(hr))
					{
					// non => enregistre l'erreur
					if (FAILED(multi_qi.hr))
						hr = multi_qi.hr;
					}
				if (SUCCEEDED(hr))
					{
					pIUnknownOPC = (IUnknown*)multi_qi.pItf;
					}

				// Lib�re la chaine OLE du nom de station
				if (pCoServerInfo && pCoServerInfo->pwszName)
					WSTRFree(pCoServerInfo->pwszName, 0);
				} // if (SUCCEEDED (hr))
			} // if (SUCCEEDED (hr))

		// cr�ation du serveur Ok ?
		if (SUCCEEDED(hr))
			{
			// oui => charge les interfaces OLE serveur si ce n'est pas d�ja fait
			// Lien avec l'interface obligatoires de OPC server s'il n'est pas d�ja effectu� Ok ?
			hr = hLieInterface(pIUnknownOPC, IID_IOPCServer, (void**)&m_pIOPCServer);
			if (SUCCEEDED (hr))
				bRes = TRUE;
			} // if (SUCCEEDED(hr))

		// mise � jour des statuts
		m_hrExecution = hr;
		SetErreur(hr);

		// Lib�re les ressources inutiles
		if (pIUnknownOPC)
			{
			// la lib�ration si pas d'interruption de connexion en cours
			if (!bExecutionInterrompue())
				pIUnknownOPC->Release();
			}

		// Ex�cute les groupes actuellement enregistr�s
		for (int nNGroupe = 0; nNGroupe < m_apGroupes.GetSize(); nNGroupe++)
			{
			COPCGroupe * pGroupeOPC = pGetCOPCGroupe (nNGroupe);

			// Ex�cution du groupe Ok ?
			if (!pGroupeOPC->bExecute ())
				bRes = FALSE;
			} // for (int nNGroupe ...
		}

	return bRes;
	} // bExecute

//-----------------------------------------------------------------
// Termine l'acc�s au serveur
BOOL COPCClient::bFinExecute()
	{
	BOOL	bRes = TRUE;

	// Acc�s au serveur en cours ?
	if (bExecutionEnCours())
		{
		// oui => Arr�te l'ex�cution des groupes en cours
		for (int nNGroupe = 0; nNGroupe < m_apGroupes.GetSize(); nNGroupe++)
			{
			COPCGroupe * pGroupeOPC = pGetCOPCGroupe (nNGroupe);

			// Fin d'ex�cution du groupe Ok ?
			if (!pGroupeOPC->bFinExecute ())
				bRes = FALSE;
			}

		// puis lib�re les Interfaces Server obligatoires
		if (!bExecutionInterrompue())
			m_pIOPCServer->Release();
		m_pIOPCServer = NULL;
		m_hrExecution = HR_NON_CONNECTE;
		SetErreur (NO_ERROR);
		}
	else
		SetErreur (HR_NON_CONNECTE);

	return bRes;
	} // bFinExecute

//-------------------------------------------------------------------
// Supprime tous les groupe du serveur
void COPCClient::SupprimeGroupes (void)
	{
	// D�truit les groupes
	for (int nNGroupe = 0; nNGroupe < m_apGroupes.GetSize(); nNGroupe++)
		{
		COPCGroupe * pGroupeOPC = pGetCOPCGroupe (nNGroupe);

		delete pGroupeOPC;
		}

	// Supprime les groupes du tableau
	m_apGroupes.RemoveAll ();
	} // SupprimeGroupes

//-----------------------------------------------------------------
void COPCClient::InterruptionEventuelleExecution (HRESULT hresult)
	{
	// A t'on le droit d'interrompre l'ex�cution en cours et l'ex�cution est elle en cours ?
	if (m_bAutoriseInterruptionExecution && bExecutionEnCours())
		{
		// oui : s'agit il d'une erreur n�cessitant l'interruption de l'ex�cution ?
		// (erreur avec FACILITY_RPC, FACILITY_WINDOWS, ou (FACILITY_WIN32 et code entre 1700 et1938))?.
		if (FAILED(hresult))
			{
			HRESULT hrFacility = HRESULT_FACILITY(hresult);
			if ((hrFacility == FACILITY_RPC) || (hrFacility == FACILITY_WINDOWS) || 
				((hrFacility == FACILITY_WIN32) && (HRESULT_CODE(hresult) >= 1700) && (HRESULT_CODE(hresult) <= 1938)))
				{
				// oui => Enregistre l'interruption...
				m_hrInterruptionExecution = hresult;
				// et interromps l'ex�cution
				bFinExecute();
				}
			}
		}
	} // InterruptionEventuelleExecution

//-----------------------------------------------------------------
void COPCClient::SetErreur (HRESULT hresult)
	{
	// Enregistre l'erreur
	m_hresult = hresult;
	InterruptionEventuelleExecution (hresult);
	}

//-----------------------------------------------------------------
// Encapsulation de QueryInterface
HRESULT COPCClient::hLieInterface(IUnknown * pIUnknownOPC, IID iid, void** ppInterface)
	{
	HRESULT hr = E_POINTER;
	
	// Lien avec l'Active X ouvert ?
	if (pIUnknownOPC)
		{
		// oui => essai de r�cup�rer l'interface demand�
		hr = pIUnknownOPC->QueryInterface (iid, ppInterface);
		}
		
	return hr;
	} // bLieInterface

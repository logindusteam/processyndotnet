/*----------------------------------------------------------------------+
 |	 Ce fichier est la propriete de 																		|
 |							Societe LOGIQUE INDUSTRIE 															|
 |			 Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3									|
 |	 Il est demeure sa propriete exclusive et est confidentiel. 				|
 |	 Aucune diffusion n'est possible sans accord ecrit									|
 |----------------------------------------------------------------------|
 |																																			|
 |	 Titre	 : SCRUT_AP.PAS 																						|
 |	 Auteur  : PR 																											|
 |	 Date 	 : 29/12/92 																								|
 |	 Version : 3.22 																										|
 |	 Mise a jours:																											|
 |																																			|
 |	 +--------+--------+---+--------------------------------------+ 		|
 |	 | date 	| qui 	 |no | raisons															| 		|
 |	 +--------+--------+---+--------------------------------------+ 		|
 |	 |29/01/92|PR 		 |115|Var Globale d'init APP								| 		|
 |	 |25/05/93|PR 		 |126|Optimisation Fct Cyc Applicom Modbus	|			|
 |	 |02/10/97|JS			 |	 |Correction conversions mots sign�s 32 | 		|
 |	 |30/06/98|JS			 |	 |Correction inversion formats r�els    | 		|
 |	 |30/06/98|JS			 |	 |Correction init. DLL applicom         | 		|
 |	 |07/12/98|JS			 |	 |Variable de for�age des sorties       | 		|
 |	 +--------+--------+---+--------------------------------------+ 		|
 |																																			|
 +----------------------------------------------------------------------*/
// unite de scrutation APPLICOM-ESCLAVE pour PcsExe
// ----------------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "tipe_ap.h"
#include "Applicom.h"
#include "pcs_sys.h"
#include "PcsVarEx.h"
#include "scrut_ap.h"
#include "Verif.h"
VerifInit;

#define Hibyte(w) ((BYTE)((w >> 8) & 0xff))
#define c_i16_max ((FLOAT)(32767))
#define c_i16_min ((FLOAT)(-32768))

// ----------------  Var globales
static BOOL bInitApplicomOk = FALSE;

//---------------------------------------------------------------------------
// interface 32 bits / 16 bits
static void InitBus32 (int * pn1)
	{
	SHORT n1 = *pn1;
	
	initbus (&n1);
	
	*pn1 = n1;
	}

//---------------------------------------------------------------------------
// interface 32 bits / 16 bits
static void GetPackBit32 (int * pn1, int* pn2,int* pn3, SHORT* pwBufMot,int* pn5)
	{
	SHORT n1 = *pn1;
	SHORT n2 = *pn2;
	SHORT n3 = *pn3;
	SHORT n5 = *pn5;
	
	getpackbit (&n1, &n2, &n3, pwBufMot, &n5);
	
	*pn1 = n1;
	*pn2 = n2;
	*pn3 = n3;
	*pn5 = n5;
	}

//---------------------------------------------------------------------------
// interface 32 bits / 16 bits
static void GetWord32 (int * pn1, int* pn2,int* pn3, SHORT* pwBufMot,int* pn5)
	{
	SHORT n1 = *pn1;
	SHORT n2 = *pn2;
	SHORT n3 = *pn3;
	SHORT n5 = *pn5;
	
	getword (&n1, &n2, &n3, pwBufMot, &n5);
	
	*pn1 = n1;
	*pn2 = n2;
	*pn3 = n3;
	*pn5 = n5;
	}

//---------------------------------------------------------------------------
// interface 32 bits / 16 bits
static void SetWord32 (int * pn1, int* pn2,int* pn3, SHORT* pwBufMot,int* pn5)
	{
	SHORT n1 = *pn1;
	SHORT n2 = *pn2;
	SHORT n3 = *pn3;
	SHORT n5 = *pn5;
	
	setword (&n1, &n2, &n3, pwBufMot, &n5);
	
	*pn1 = n1;
	*pn2 = n2;
	*pn3 = n3;
	*pn5 = n5;
	}

//---------------------------------------------------------------------------
// interface 32 bits / 16 bits
static void SetPackBit32 (int * pn1, int* pn2,int* pn3, SHORT* pwBufMot,int* pn5)
	{
	SHORT n1 = *pn1;
	SHORT n2 = *pn2;
	SHORT n3 = *pn3;
	SHORT n5 = *pn5;
	
	setpackbit (&n1, &n2, &n3, pwBufMot, &n5);
	
	*pn1 = n1;
	*pn2 = n2;
	*pn3 = n3;
	*pn5 = n5;
	}

//---------------------------------------------------------------------------
// interface 32 bits / 16 bits
static void StartCyc32 (int * pn1, int* pn2,int* pn3)
	{
	SHORT n1 = *pn1;
	SHORT n2 = *pn2;
	SHORT n3 = *pn3;
	
	startcyc (&n1, &n2, &n3);
	
	*pn1 = n1;
	*pn2 = n2;
	*pn3 = n3;
	}

//---------------------------------------------------------------------------
// interface 32 bits / 16 bits
static void StopCyc32 (int * pn1, int* pn2,int* pn3)
	{
	SHORT n1 = *pn1;
	SHORT n2 = *pn2;
	SHORT n3 = *pn3;
	
	stopcyc (&n1, &n2, &n3);
	
	*pn1 = n1;
	*pn2 = n2;
	*pn3 = n3;
	}

//---------------------------------------------------------------------------
// interface 32 bits / 16 bits
static void ActCyc32 (int * pn1, int* pn2,int* pn3)
	{
	SHORT n1 = *pn1;
	SHORT n2 = *pn2;
	SHORT n3 = *pn3;
	
	actcyc (&n1, &n2, &n3);
	
	*pn1 = n1;
	*pn2 = n2;
	*pn3 = n3;
	}

//---------------------------------------------------------------------------
static DWORD deux_puissance (DWORD n)
	{
	return 1 << n;
	}

//---------------------------------------------------------------------------
static void maj_statut (DWORD statut_ap)
	{
	PX_VAR_SYS	position;
	PFLOAT	ads_num;
	DWORD w;
	DWORD w1;
	
	// Bit 0-5 : Reserve retour Fonctions Applicom
	//				 0		 : Ok
	//				 1..31 : Retour Code fonction erreur du coupleur JBUS
	//				 32 	 : Mauvais parametres passe au Sous-Prog.
	//				 33 	 : D�faut Temps de reponse (Time-out)
	//				 34 	 : D�faut de CRC16 ou Parit�
	//				 35 	 : donn�es non disponibles en lecture cyclique
	//				 36 	 : Automate non configur�
	//				 38 	 : Instruction de Comparaison a detect� un changement
	//				 41 	 : Erreur donn�es Diff�r�es
	//				 42 	 : Erreur donn�es Diff�r�es
	//				 43 	 : Status de l'automate inconnu
	//				 45 	 : Logiciel non-r�sident
	//				 46 	 : Maitre appelant un bus esclave ou inversement
	//				 : ou num�ro de Carte non configur�e
	//				 48 	 : Erreur en RS232 Half-duplex
	// bit 6	 : Fonction Cyclique Non cr��e
	// Bit 7	 : Adresse DB invalide
	// Bit 8	 : Erreur Type de Donn�es invalide (Fct. Cyc.)
	// Bit 9	 : Overflow sur Num�rique
	// Bit 10  : Message trop grand en Sortie
	// Bit 11	 : Erreur ouverture Dll
	//
	position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, status_ap);
	ads_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (position->dwPosCourEx));
	w = (DWORD)(*ads_num);
	w1 = w & 63;				 //2#0000000000111111;
	if ((statut_ap < 64 ) && (w1 == 0))
		{
		w = w | statut_ap;
		}
	if (statut_ap > 63 )
		{
		w = w | statut_ap;
		}
	(*ads_num) = (FLOAT)w;
	} // maj_statut

//---------------------------------------------------------------------------
// Initialise l'ex�cution d'Applicom
void initialise_ap (void)
	{
	int erreur = 0;
	DWORD index;
	DWORD index_max;
	X_APPLICOM_FCT_CYC*ax_ap_fct_cyc;
	PBOOL cour_log;
	
	bInitApplicomOk = bInitDLLApplicom ();

	if (bInitApplicomOk) // if nOk
		{
		InitBus32(&erreur);
		if (erreur != 0)
			bInitApplicomOk = FALSE;
		}
	if (!bInitApplicomOk)
		maj_statut(0x0800);

	if (bInitApplicomOk)
		{
		if (existe_repere(bx_ap_fct_cyc))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_fct_cyc);
			for (index = 1;index <= index_max;index++)
				{
				ax_ap_fct_cyc = (X_APPLICOM_FCT_CYC*)pointe_enr (szVERIFSource, __LINE__, bx_ap_fct_cyc,index);
				cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_fct_cyc->index_es_associe);
				if (*cour_log)
					{
					StartCyc32((PINT)&ax_ap_fct_cyc->numero_canal, (PINT)&ax_ap_fct_cyc->numero_fonction_cyclique, (PINT)&erreur);
					}
				else
					{
					StopCyc32((PINT)&ax_ap_fct_cyc->numero_canal, (PINT)&ax_ap_fct_cyc->numero_fonction_cyclique, (PINT)&erreur);
					}
				if (erreur != 0)
					{
					maj_statut(0x0020);
					}
				} // for
			} // if existe
		} // if ok
	} // initialise_ap

//---------------------------------------------------------------------------
// Termine l'ex�cution d'Applicom
void ferme_ap (void)
	{
	if (bInitApplicomOk)
		{
		short int snTemp = 0;
		exitbus (&snTemp);
		}

	// Libere la DLL Applicom
	bLibereDLLApplicom ();
	}

//--------------------------------------------------------------------------
void recoit_ap (BOOL bPremierPassage)
	{
	if (bInitApplicomOk) // if Ok
		{
		int erreur = 0;
		DWORD longueur_reel = 1;
		DWORD index_bit;
		DWORD index_car;
		DWORD index;
		DWORD index_max;
		char tempo_mes[82];
		FLOAT reel;
		BYTE *ptr_reel;
		BYTE *ptr_buffer;
		WORD awBuffer[128];
		PBOOL raf;
		PFLOAT new_num;
		PBOOL new_log;
		PSTR new_mes;
		X_APPLICOM_E*ax_ap_e;
		X_APPLICOM_TAB_MES*ax_ap_tab_mes;
		//------------------------
		// ENTREES : Logiques
		if (existe_repere (bx_ap_e_log))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_log);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_e = (X_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_log,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_e->i_cmd_rafraich);
				if (*raf)
					{
					// lecture info
					GetPackBit32 ((PINT)&(ax_ap_e->numero_carte), (PINT)&longueur_reel, (PINT)&ax_ap_e->adresse, (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						new_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_e->index_es_associe);
						(*new_log) = (BOOL)((awBuffer[0] & 0x0001) != 0);
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // if existe

		//------------------------
		// ENTREES : Numeriques Entiers
		if (existe_repere (bx_ap_e_num_entier))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_entier);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_e = (X_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_entier, index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, ax_ap_e->i_cmd_rafraich);
				if (*raf)
					{
					// lecture info
					GetWord32 ((PINT)&ax_ap_e->numero_carte, (PINT)&longueur_reel, (PINT)&ax_ap_e->adresse, (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ax_ap_e->index_es_associe);
						(*new_num) = (FLOAT)((int)(short)awBuffer[0]);
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // if existe

		//------------------------
		// ENTREES : Numeriques Mots
		if (existe_repere (bx_ap_e_num_mot))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_mot);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_e = (X_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_mot,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_e->i_cmd_rafraich);
				if (*raf)
					{
					// lecture info
					GetWord32 ((PINT)&ax_ap_e->numero_carte, (PINT)&longueur_reel, (PINT)&ax_ap_e->adresse,(PSHORT)awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,ax_ap_e->index_es_associe);
						(*new_num) = (FLOAT)((DWORD)awBuffer[0]);
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // if existe

		//------------------------
		// ENTREES : Numeriques Reels
		if (existe_repere (bx_ap_e_num_reel))
			{
			longueur_reel = 2;
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_reel);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_e = (X_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_reel,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_e->i_cmd_rafraich);
				if (*raf)
					{
					// lecture info
					GetWord32 ((PINT)&ax_ap_e->numero_carte, (PINT)&longueur_reel, (PINT)&ax_ap_e->adresse, (PSHORT) awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						ptr_reel	= (BYTE *)&reel;
						ptr_buffer = (BYTE *)&awBuffer[0];

/*					substitu� JS	30/6/98 pour cause de bug : standard applicom non respect�
						(*ptr_reel) = (*(ptr_buffer + 2)); // 1� octet
						ptr_reel++;
						(*ptr_reel) = (*(ptr_buffer + 3)); // 2� octet
						ptr_reel++;
						(*ptr_reel) = (*(ptr_buffer));		 // 3� octet
						ptr_reel++;
						(*ptr_reel) = (*(ptr_buffer + 1)); // 4� octet
*/
						(*ptr_reel) = (*(ptr_buffer + 0)); // 1� octet
						ptr_reel++;
						(*ptr_reel) = (*(ptr_buffer + 1)); // 2� octet
						ptr_reel++;
						(*ptr_reel) = (*(ptr_buffer + 2)); // 3� octet
						ptr_reel++;
						(*ptr_reel) = (*(ptr_buffer + 3)); // 4� octet
						
						new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,ax_ap_e->index_es_associe);
						(*new_num) = reel;
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // if existe

		//------------------------
		// ENTREES : Messages
		if (existe_repere (bx_ap_e_mes))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_mes);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_mes,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					// lecture info
					GetWord32 ((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre, (PINT)&ax_ap_tab_mes->adresse, (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						longueur_reel = 0;
						index_car = 0;
						while ((index_car < ax_ap_tab_mes->longueur) &&
							(((BYTE)awBuffer[longueur_reel]) != 0))
							{
							tempo_mes[index_car] = (char)((BYTE)awBuffer[longueur_reel]);
							index_car++;
							if ((index_car < ax_ap_tab_mes->longueur) &&
								(Hibyte(awBuffer[longueur_reel]) != 0))
								{
								tempo_mes[index_car] = (char)(Hibyte(awBuffer[longueur_reel]));
								index_car++;
								}
							longueur_reel++;
							} // while
						tempo_mes[index_car] = '\0';
						new_mes  = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes,ax_ap_tab_mes->index_es_associe);
						StrCopy (new_mes, tempo_mes);
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // if existe

		//------------------------
		// ENTREES : Tableaux Logiques
		if (existe_repere (bx_ap_e_log_tab))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_log_tab);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_log_tab,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					// trame
					GetPackBit32 ((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre, (PINT)&ax_ap_tab_mes->adresse, (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						// index_bit	// pointeur bit dans BDD PCS
						index_car = 0;	// pointeur mot dans buffer APP
						longueur_reel = 0;	// compteur de 0 � 15
						for (index_bit = 0;(index_bit < ax_ap_tab_mes->longueur);index_bit++)
							{
							new_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,index_bit + ax_ap_tab_mes->index_es_associe);
							(*new_log) = (BOOL)((awBuffer[index_car] & deux_puissance (longueur_reel)) != 0);
							if (longueur_reel == 15)
								{
								longueur_reel = 0;
								index_car++;
								}
							else
								{
								longueur_reel++;
								}
							} // for
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // existe

		//------------------------
		// ENTREES : Tableaux Numeriques Entiers
		if (existe_repere (bx_ap_e_num_entier_tab))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_entier_tab);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_entier_tab,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					// trame
					GetWord32 ((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre, (PINT)(&ax_ap_tab_mes->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						for (index_car = 0;index_car < ax_ap_tab_mes->longueur;index_car++)
							{
							new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,index_car + ax_ap_tab_mes->index_es_associe);
							(*new_num) = (FLOAT)((int)(short)awBuffer[index_car]);
							}
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // existe

		//------------------------
		// ENTREES : Tableaux Numeriques Mots
		if (existe_repere (bx_ap_e_num_mot_tab))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_mot_tab);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_mot_tab,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					// trame
					GetWord32 ((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre, (PINT)(&ax_ap_tab_mes->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						for (index_car = 0;index_car < ax_ap_tab_mes->longueur;index_car++)
							{
							new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,index_car + ax_ap_tab_mes->index_es_associe);
							(*new_num) = (FLOAT)((DWORD)awBuffer[index_car]);
							}
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // existe

		//------------------------
		// ENTREES : Tableaux Numeriques Reels
		if (existe_repere (bx_ap_e_num_reel_tab))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_reel_tab);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_reel_tab,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					// trame
					GetWord32 ((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre, (PINT)(&ax_ap_tab_mes->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur == 0)
						{
						for (index_car = 0;index_car < ax_ap_tab_mes->longueur;index_car++)
							{
							ptr_reel	= (BYTE *)&reel;
							ptr_buffer = (BYTE *)&awBuffer[index_car*2];
							
/*					substitu� JS	30/6/98 pour cause de bug : standard applicom non respect�
							(*ptr_reel) = (*(ptr_buffer + 2)); // 1� octet
							ptr_reel++;
							(*ptr_reel) = (*(ptr_buffer + 3)); // 2� octet
							ptr_reel++;
							(*ptr_reel) = (*(ptr_buffer));	 // 3� octet
							ptr_reel++;
							(*ptr_reel) = (*(ptr_buffer + 1)); // 4� octet
*/
							(*ptr_reel) = (*(ptr_buffer + 0)); // 1� octet
							ptr_reel++;
							(*ptr_reel) = (*(ptr_buffer + 1)); // 2� octet
							ptr_reel++;
							(*ptr_reel) = (*(ptr_buffer + 2)); // 3� octet
							ptr_reel++;
							(*ptr_reel) = (*(ptr_buffer + 3)); // 4� octet
							
							new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,index_car + ax_ap_tab_mes->index_es_associe);
							(*new_num) = reel;
							}
						} // pas d'erreur
					else
						{
						maj_statut((DWORD)erreur);
						}
					} // raf
				} // for
			} // existe
		} // if (bInitApplicomOk)
	else
		{
		maj_statut (45);
		}
	} // recoit_ap

//---------------------------------------------------------------------------
void	emet_ap 	(BOOL bPremierPassage)
	{
	if (bInitApplicomOk) // if Ok
		{
		BOOL overflow;
		DWORD index_tab;
		DWORD index_mes;
		DWORD index;
		DWORD index_max;
		FLOAT reel;
		BOOL *raf;
		PBOOL new_log;
		PBOOL old_log;
		PFLOAT new_num;
		PFLOAT old_num;
		PSTR new_mes;
		PSTR old_mes;
		WORD awBuffer[128];
		BYTE *ptr_reel;
		BYTE *ptr_buffer;
		X_APPLICOM_S*ax_ap_s;
		X_APPLICOM_TAB_MES *ax_ap_tab_mes;
		X_APPLICOM_TAB_S_REFLET_MES*ax_ap_tab_mes_reflet;
		X_APPLICOM_FCT_CYC *ax_ap_fct_cyc;
		X_APPLICOM_FCT_CYC *ax_ap_fct_cyc_salve;
		
		int erreur = 0;
		DWORD longueur_reel = 1;
		BOOL bForcageSorties = CPcsVarEx::bGetValVarSysLogEx (VA_SYS_APBD_FORCE_SORTIES);

		//----------------------------------------
		// SORTIES : Logiques
		if (existe_repere(bx_ap_s_log))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_s = (X_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_log,index);
				new_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_s->index_es_associe);
				old_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log,ax_ap_s->index_es_associe);
				if ((*old_log ^ *new_log) || bForcageSorties)
					{
					awBuffer[0] = (int)(*new_log);
					
					SetPackBit32((PINT)&ax_ap_s->numero_carte, (PINT)&longueur_reel, (PINT)(&ax_ap_s->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur != 0)
						{
						maj_statut((DWORD)erreur);
						}
					} // if <>
				} // for
			} // if existe

		//----------------------------------------
		// SORTIES : Numeriques Entiers
		if (existe_repere(bx_ap_s_num_entier))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_s = (X_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_entier,index);
				new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,ax_ap_s->index_es_associe);
				old_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,ax_ap_s->index_es_associe);
				if (((*old_num) != (*new_num)) || bForcageSorties)
					{
					if (((*new_num) <= c_i16_max) && ((*new_num) >= c_i16_min))
						{
						awBuffer[0] = (WORD)(*new_num);
						
						SetWord32((PINT)&ax_ap_s->numero_carte, (PINT)&longueur_reel, (PINT)(&ax_ap_s->adresse), (PSHORT)awBuffer, (PINT)&erreur);
						if (erreur != 0)
							{
							maj_statut((DWORD)erreur);
							}
						} // if
					else
						{
						maj_statut(0x0200);  // overflow
						}
					} // if <>
				} // for
			} // if existe

		//----------------------------------------
		// SORTIES : Numeriques Mots
		if (existe_repere(bx_ap_s_num_mot))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_s = (X_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_mot,index);
				new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,ax_ap_s->index_es_associe);
				old_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,ax_ap_s->index_es_associe);
				if (((*old_num) != (*new_num)) || bForcageSorties)
					{
					if (((*new_num) <= (FLOAT)65535.0) && ((*new_num) >= 0) )
						{
						awBuffer[0] = (WORD)(*new_num);
						
						SetWord32((PINT)&ax_ap_s->numero_carte, (PINT)&longueur_reel, (PINT)(&ax_ap_s->adresse), (PSHORT)awBuffer, (PINT)&erreur);
						if (erreur != 0)
							{
							maj_statut((DWORD)erreur);
							}
						} // if
					else
						{
						maj_statut(0x0200); // overflow
						}
					} // if <>
				}  // for
			}  // if existe

		//----------------------------------------
		// SORTIES : Numeriques Reels
		if (existe_repere(bx_ap_s_num_reel))
			{
			longueur_reel = 2;
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_s = (X_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_reel,index);
				new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,ax_ap_s->index_es_associe);
				old_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,ax_ap_s->index_es_associe);
				if (((*old_num) != (*new_num)) || bForcageSorties)
					{
					reel			 = (*new_num);
					ptr_reel	 = (BYTE *)&reel;
					ptr_buffer = (BYTE *)&awBuffer[0];

/*				$$ substitu� JS	30/6/98 pour cause de bug : standard non respect�
					(*(ptr_buffer + 2)) = (*ptr_reel); // 3� octet
					ptr_reel++;
					(*(ptr_buffer + 3)) = (*ptr_reel); // 4� octet
					ptr_reel++;
					(*(ptr_buffer)) = (*ptr_reel);		 // 1� octet
					ptr_reel++;
					(*(ptr_buffer + 1)) = (*ptr_reel); // 2� octet

*/
					(*(ptr_buffer + 0)) = (*ptr_reel); // 1� octet
					ptr_reel++;
					(*(ptr_buffer + 1)) = (*ptr_reel); // 2� octet
					ptr_reel++;
					(*(ptr_buffer + 2)) = (*ptr_reel); // 3� octet
					ptr_reel++;
					(*(ptr_buffer + 3)) = (*ptr_reel); // 4� octet
					
					SetWord32((PINT)&ax_ap_s->numero_carte, (PINT)&longueur_reel, (PINT)(&ax_ap_s->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur != 0)
						{
						maj_statut((DWORD)erreur);
						}
					} // if <>
				} // for
			} // if existe

		//----------------------------------------
		// SORTIES : Messages
		if (existe_repere(bx_ap_s_mes))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_mes);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_mes,index);
				new_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes,ax_ap_tab_mes->index_es_associe);
				old_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes,ax_ap_tab_mes->index_es_associe);
				if ((!bStrEgales (new_mes, old_mes)) || bForcageSorties)
					{
					longueur_reel = StrLength (new_mes);
					if ((ax_ap_tab_mes->longueur == 0) ||
						(longueur_reel <= ax_ap_tab_mes->longueur))
						{
						index_tab = 0;
						index_mes = 0;
						while (index_mes < longueur_reel)
							{
							if (index_mes % 2 == 0) 	// si index_mes pair
								{
								awBuffer[index_tab] = (int)(new_mes[index_mes]);
								}
							else
								{
								awBuffer[index_tab] = ((int)(new_mes[index_mes]) << 8) + awBuffer[index_tab];
								index_tab++;
								}
							index_mes++;
							} // while
						
						if (index_mes <= 80)
							{
							if (index_mes % 2 != 0)
								{
								awBuffer[index_tab] = ((WORD)(32) << 8) + awBuffer[index_tab];	// chr 20
								index_tab++;
								index_mes++;
								}
							}
						if (ax_ap_tab_mes->longueur == 0)
							{ 	 // reflet
							while (index_mes < 80)
								{
								awBuffer [index_tab] = 8224;	// chr 20 dans PF et Pf
								index_mes += 2;
								index_tab++;
								}
							} // reflet
						else
							{ // Declare
							while (index_mes < ax_ap_tab_mes->longueur)
								{
								awBuffer [index_tab] = 8224;	// chr 20 dans PF et Pf
								index_mes += 2;
								index_tab++;
								}
							} // declare
						
						SetWord32((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&index_tab, (PINT)(&ax_ap_tab_mes->adresse), (PSHORT)awBuffer, (PINT)&erreur);
						
						if (erreur != 0)
							{
							maj_statut((DWORD)erreur);
							}
						}
					else
						{
						maj_statut(0x0400);
						}
					} // if <>
				} // for
			} // if existe

		//----------------------------------------
		// SORTIES : Tableaux Logiques
		if (existe_repere(bx_ap_s_log_tab))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log_tab);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_log_tab,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					(*raf) = FALSE;
					for (index_tab = 0;index_tab < 128;index_tab++)
						{
						awBuffer [index_tab] = 0;
						}
					for (index_tab = 0;index_tab < ax_ap_tab_mes->longueur;index_tab++)
						{
						new_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,index_tab + ax_ap_tab_mes->index_es_associe);
						if (*new_log)
							{
							awBuffer[index_tab / 16] = awBuffer[index_tab / 16] + (WORD)deux_puissance((DWORD)(index_tab % 16));
							}
						}
					SetPackBit32((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre, (PINT)(&ax_ap_tab_mes->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur != 0)
						{
						maj_statut((DWORD)erreur);
						}
					} // if raf
				} // for
			} // if exist

		//----------------------------------------
		// SORTIES : Tableaux Numeriques Entiers
		if (existe_repere(bx_ap_s_num_entier_tab))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					(*raf) = FALSE;
					overflow = FALSE;
					index_tab = 0;
					while ((index_tab < ax_ap_tab_mes->longueur) && (!overflow))
						{
						new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,index_tab + ax_ap_tab_mes->index_es_associe);
						overflow = (BOOL)(((*new_num) > c_i16_max) || ((*new_num) < c_i16_min));
						if (!overflow)
							{
							awBuffer[index_tab] = (int)(*new_num);
							}
						index_tab++;
						} // while
					if (overflow)
						{
						maj_statut(0x0200); // overflow
						}
					else
						{
						SetWord32((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre, (PINT)(&ax_ap_tab_mes->adresse), (PSHORT)awBuffer, (PINT)&erreur);
						if (erreur != 0)
							{
							maj_statut((DWORD)erreur);
							}
						} // ok
					} // if raf
				} // for
			} // if exist

		//----------------------------------------
		// SORTIES : Tableaux Numeriques Mots
		if (existe_repere(bx_ap_s_num_mot_tab))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					(*raf) = FALSE;
					overflow = FALSE;
					index_tab = 0;
					while ((index_tab < ax_ap_tab_mes->longueur) && (!overflow))
						{
						new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,index_tab + ax_ap_tab_mes->index_es_associe);
						overflow = (BOOL) (((*new_num) > (FLOAT)65535.0) || ((*new_num) < 0));
						if (!overflow)
							{
							awBuffer[index_tab] = (WORD)(*new_num);
							}
						index_tab++;
						}
					if (overflow)
						{
						maj_statut(0x0200); // overflow
						}
					else
						{
						SetWord32((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre,(PINT)(&ax_ap_tab_mes->adresse), (PSHORT)awBuffer, (PINT)&erreur);
						if (erreur != 0)
							{
							maj_statut((DWORD)erreur);
							}
						} // ok
					} // if raf
				} // for
			} // if exist

		//----------------------------------------
		// SORTIES : Tableaux Numeriques Reels
		if (existe_repere(bx_ap_s_num_reel_tab))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab,index);
				raf = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_tab_mes->i_cmd_rafraich);
				if (*raf)
					{
					(*raf) = FALSE;
					for (index_tab = 0;index_tab < ax_ap_tab_mes->longueur;index_tab++)
						{
						new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,index_tab + ax_ap_tab_mes->index_es_associe);
						
						reel			 = (*new_num);
						ptr_reel	 = (BYTE *)&reel;
						ptr_buffer = (BYTE *)&awBuffer[index_tab*2];
						
/*					substitu� JS	30/6/98 pour cause de bug : standard applicom non respect�
						(*(ptr_buffer + 2)) = (*ptr_reel); // 3� octet
						ptr_reel++;
						(*(ptr_buffer + 3)) = (*ptr_reel); // 4� octet
						ptr_reel++;
						(*(ptr_buffer)) = (*ptr_reel);		 // 1� octet
						ptr_reel++;
						(*(ptr_buffer + 1)) = (*ptr_reel); // 2� octet
*/
						(*(ptr_buffer + 0)) = (*ptr_reel); // 1� octet
						ptr_reel++;
						(*(ptr_buffer + 1)) = (*ptr_reel); // 2� octet
						ptr_reel++;
						(*(ptr_buffer + 2)) = (*ptr_reel); // 3� octet
						ptr_reel++;
						(*(ptr_buffer + 3)) = (*ptr_reel); // 4� octet
						} // for
					
					SetWord32((PINT)&ax_ap_tab_mes->numero_carte, (PINT)&ax_ap_tab_mes->nbre, (PINT)(&ax_ap_tab_mes->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur != 0)
						{
						maj_statut((DWORD)erreur);
						}
					} // if raf
				} // for
			} // if exist

		longueur_reel = 1;

		//----------------------------------------
		// SORTIES : Tableaux Logiques Reflets
		if (existe_repere(bx_ap_s_log_tab_reflet))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log_tab_reflet);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_log_tab_reflet,index);
				new_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_tab_mes_reflet->index_es_associe);
				old_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log,ax_ap_tab_mes_reflet->index_es_associe);
				if (((*old_log) ^ (*new_log)) || bForcageSorties)
					{
					awBuffer[0] = (WORD)(*new_log);
					SetPackBit32((PINT)&ax_ap_tab_mes_reflet->numero_carte, (PINT)&longueur_reel, (PINT)(&ax_ap_tab_mes_reflet->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur != 0)
						{
						maj_statut((DWORD)erreur);
						}
					} // <>
				} // for
			} // if exist

		//----------------------------------------
		// SORTIES : Tableaux Numeriques Entiers Reflets
		if (existe_repere(bx_ap_s_num_entier_tab_reflet))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab_reflet);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab_reflet,index);
				new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,ax_ap_tab_mes_reflet->index_es_associe);
				old_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,ax_ap_tab_mes_reflet->index_es_associe);
				if (((*old_num) != (*new_num)) || bForcageSorties)
					{
					if ( ((*new_num) <= c_i16_max) && ((*new_num) >= c_i16_min) )
						{
						awBuffer[0] = (WORD)(*new_num);
						SetWord32((PINT)&ax_ap_tab_mes_reflet->numero_carte, (PINT)&longueur_reel, (PINT)(&ax_ap_tab_mes_reflet->adresse), (PSHORT)awBuffer, (PINT)&erreur);
						if (erreur != 0)
							{
							maj_statut((DWORD)erreur);
							}
						} // ok
					else
						{
						maj_statut(0x0200); 	// overflow
						}
					} // <>
				} // for
			} // if exist

		//----------------------------------------
		// SORTIES : Tableaux Numeriques Mots Reflets
		if (existe_repere(bx_ap_s_num_mot_tab_reflet))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab_reflet);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab_reflet,index);
				new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,ax_ap_tab_mes_reflet->index_es_associe);
				old_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,ax_ap_tab_mes_reflet->index_es_associe);
				if (((*old_num) != (*new_num)) || bForcageSorties)
					{
					if (((*new_num) <= (FLOAT)65535.0) && ((*new_num) >= 0))
						{
						awBuffer[0] = (WORD)(*new_num);
						
						SetWord32((PINT)&ax_ap_tab_mes_reflet->numero_carte, (PINT)&longueur_reel, (PINT)(&ax_ap_tab_mes_reflet->adresse), (PSHORT)awBuffer, (PINT)&erreur);
						if (erreur != 0)
							{
							maj_statut((DWORD)erreur);
							}
						} // ok
					else
						{
						maj_statut(0x0200);  // overflow
						}
					} // if <>
				} // for
			} // if exist

		//----------------------------------------
		// SORTIES : Tableaux Numeriques Reels Reflets
		if (existe_repere(bx_ap_s_num_reel_tab_reflet))
			{
			longueur_reel = 2;
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab_reflet);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab_reflet,index);
				new_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,ax_ap_tab_mes_reflet->index_es_associe);
				old_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,ax_ap_tab_mes_reflet->index_es_associe);
				if (((*old_num) != (*new_num)) || bForcageSorties)
					{
					reel			 = (*new_num);
					ptr_reel	 = (BYTE *)&reel;
					ptr_buffer = (BYTE *)&awBuffer[0];

/*					substitu� JS	30/6/98 pour cause de bug : standard applicom non respect�
					(*(ptr_buffer + 2)) = (*ptr_reel); // 3� octet
					ptr_reel++;
					(*(ptr_buffer + 3)) = (*ptr_reel); // 4� octet
					ptr_reel++;
					(*(ptr_buffer)) 		= (*ptr_reel);	 // 1� octet
					ptr_reel++;
					(*(ptr_buffer + 1)) = (*ptr_reel); // 2� octet
*/
					(*(ptr_buffer + 1)) = (*ptr_reel); // 1� octet
					ptr_reel++;
					(*(ptr_buffer + 2)) = (*ptr_reel); // 2� octet
					ptr_reel++;
					(*(ptr_buffer + 3)) = (*ptr_reel); // 3� octet
					ptr_reel++;
					(*(ptr_buffer + 4)) = (*ptr_reel); // 4� octet
					
					SetWord32((PINT)&ax_ap_tab_mes_reflet->numero_carte, (PINT)&longueur_reel, (PINT)(&ax_ap_tab_mes_reflet->adresse), (PSHORT)awBuffer, (PINT)&erreur);
					if (erreur != 0)
						{
						maj_statut((DWORD)erreur);
						}
					} // if <>
				} // for
			} // if exist

		//----------------------------------------
		// SORTIES : Tableaux Messages Reflets
		if (existe_repere(bx_ap_s_tab_mes_reflet))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_tab_mes_reflet);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_tab_mes_reflet,index);
				new_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes,ax_ap_tab_mes_reflet->index_es_associe);
				old_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes,ax_ap_tab_mes_reflet->index_es_associe);
				if ((!bStrEgales (new_mes, old_mes)) || bForcageSorties)
					{
					longueur_reel = StrLength(new_mes);
					if (longueur_reel < 81)
						{
						index_tab = 0;
						index_mes = 0;
						while (index_mes < longueur_reel)
							{
							if (index_mes % 2 == 0) 	// si index_mes pair
								{
								awBuffer[index_tab] = (WORD)(new_mes[index_mes]);
								}
							else
								{
								awBuffer[index_tab] = ((WORD)(new_mes[index_mes]) << 8) + awBuffer[index_tab];
								index_tab++;
								}
							index_mes++;
							}
						
						if (index_mes <= 80)
							{
							if (index_mes % 2 != 0)
								{
								awBuffer[index_tab] = ((WORD)(32) << 8) + awBuffer[index_tab];	// chr 20
								index_tab++;
								index_mes++;
								}
							}
						while (index_mes < 80)
							{
							awBuffer [index_tab] = 8224;	// chr 20 dans PF et Pf
							index_mes += 2;
							index_tab++;
							}
						
						SetWord32((PINT)&ax_ap_tab_mes_reflet->numero_carte, (PINT)&index_tab, (PINT)(&ax_ap_tab_mes_reflet->adresse), (PSHORT)awBuffer, (PINT)&erreur);
						if (erreur != 0)
							{
							maj_statut((DWORD)erreur);
							}
						}
					else
						{
						maj_statut(0x0800);
						}
					} // if !=
				} // for
			} // existe_repere

		//----------------------------------------
		// Gestion des Fonctions Cycliques
		if (existe_repere(bx_ap_fct_cyc))
			{
			index_max = nb_enregistrements (szVERIFSource, __LINE__, bx_ap_fct_cyc);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_fct_cyc = (X_APPLICOM_FCT_CYC*)pointe_enr (szVERIFSource, __LINE__, bx_ap_fct_cyc,index);
				new_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_fct_cyc->index_es_associe);
				old_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log,ax_ap_fct_cyc->index_es_associe);
				if ((*old_log) ^ (*new_log))
					{
					if (*new_log)
						{
						if (ax_ap_fct_cyc->numero_fonction_cyclique > 255)
							{
							INT iNumeroFonctionCyclique;

							iNumeroFonctionCyclique = ax_ap_fct_cyc->numero_fonction_cyclique - 255;
							ActCyc32((PINT)&ax_ap_fct_cyc->numero_canal, &iNumeroFonctionCyclique, (PINT)&erreur);
							(*new_log) = FALSE;
							}
						else
							{
							StartCyc32((PINT)&ax_ap_fct_cyc->numero_canal, (PINT)&ax_ap_fct_cyc->numero_fonction_cyclique, (PINT)&erreur);
							}
						}
					else
						{
						if (ax_ap_fct_cyc->numero_fonction_cyclique <= 255)
							{
							StopCyc32((PINT)&ax_ap_fct_cyc->numero_canal, (PINT)&ax_ap_fct_cyc->numero_fonction_cyclique, (PINT)&erreur);
							}
						}
					if (erreur != 0)
						{
						maj_statut(0x0020);
						}
					} // <>
				} // For
			} // Existe

		//----------------------------------------
		// Gestion des Fonctions Cycliques Salves
		if (FALSE) //$$existe_repere(bx_ap_fct_cyc_salve))
			{
			index_max = 0;//$$nb_enregistrements (szVERIFSource, __LINE__, bx_ap_fct_cyc_salve);
			for (index = 1;index <= index_max;index++)
				{
				erreur = 0;
				ax_ap_fct_cyc_salve = NULL;//$$pointe_enr (szVERIFSource, __LINE__, bx_ap_fct_cyc_salve,index);
				new_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,ax_ap_fct_cyc_salve->index_es_associe);
				if (*new_log)
					{
					ActCyc32((PINT)&ax_ap_fct_cyc_salve->numero_canal, (PINT)&ax_ap_fct_cyc_salve->numero_fonction_cyclique, (PINT)&erreur);
					(*new_log) = FALSE;
					}
				if (erreur != 0)
					{
					maj_statut(0x0020);
					}
				} // For
			} // Existe
		} // if (bInitApplicomOk)
	else
		{
		maj_statut(45);
		}

	// Remise � bas de la variable de for�age des sorties
	CPcsVarEx::SetValVarSysLogEx (VA_SYS_APBD_FORCE_SORTIES, FALSE);
	} // emet_ap

//-----------------------------------fin Scrut_ap.c -----------------------------------


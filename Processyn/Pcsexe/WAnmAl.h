// ---------------------------------- Fonctions
#define W_ANM_AL_INIT               (THRD_FCT_USER +  0)
#define W_ANM_AL_INIT_MODE_PM       (THRD_FCT_USER +  1)
#define W_ANM_AL_CREATE_WINDOWS     (THRD_FCT_USER +  2)
#define W_ANM_AL_PM_LOOP            (THRD_FCT_USER +  3)
#define W_ANM_AL_READ_SERVICES      (THRD_FCT_USER +  4)
#define W_ANM_AL_EXEC_SERVICES      (THRD_FCT_USER +  5)
#define W_ANM_AL_DEL_WINDOWS        (THRD_FCT_USER +  6)
#define W_ANM_AL_END_MODE_PM        (THRD_FCT_USER +  7)
#define W_ANM_AL_TERMINE            (THRD_FCT_USER +  8)

// ---------------------------------- Erreurs
#define W_ANM_AL_ERR_NONE            0
#define W_ANM_AL_ERR_INIT_MODE_PM    1
#define W_ANM_AL_ERR_CREATE_WINDOWS  2
#define W_ANM_AL_ERR_INIT            3

// --------------------------------------------------------------------------
UINT __stdcall uThreadWAnmAl (void *hThrd);
void WAnmAlBreakPmLoop (void);
DWORD dwTailleInfosServicesAnmAl (void);


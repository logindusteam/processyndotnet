//-------------------------------------------------------
// ExeDebug.c
// ex�cution Pcs : debugger
// JS Win32 11/06/97
//-------------------------------------------------------

#include "stdafx.h"
#include "USem.h"
#include "Appli.h"
#include "DocMan.h"
#include "MemMan.h"
#include "UStr.h"
#include "UEnv.h"
#include "FMan.h"
#include "lng_res.h"
#include "tipe.h"        // Types + Constantes Processyn
#include "mem.h"         // Memory Manager
#include "actionex.h"
#include "IdExeLng.h"
#include "ULangues.h"
#include "Driv_sys.h"
#include "PathMan.h"
#include "pcs_sys.h"
#include "PcsVarEx.h"
#include "Verif.h"
#include "ExeDebug.h"
VerifInit;

#define COULEUR_VAR_MODIFIEE RGB(255,0,0)
#define TAILLE_LIGNE_MAX_DEBUG 1000
// ---------------------------------------------------------------
#define b_var_deb 893

typedef struct
	{
	char pszNomVar[c_nb_car_es];
	DWORD wPosEx;
	DWORD wBlocGenre;
	DWORD wTaille;
	BOOL wOuvert;
	} tx_e_sd;


CONTEXTE_DEBUGGER CtxtDebugger = {FALSE,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,0,FALSE,""};

// --------------------------------------------------------------------------
DWORD PointArret(void)
  {
  return CtxtDebugger.point_arret;
  }

// --------------------------------------------------------------------------
// Appel� par traitement_co : affichage de la fen�tre debugger sur point d'arr�t atteint
void AttentePArret(void)
  {
  if (CtxtDebugger.hwndCode != NULL)
    {
    if (!CtxtDebugger.pas)
      {
      ::PostMessage(CtxtDebugger.hwndCode, WM_COMMAND, ID_AFFI, 0);
      DosSemSetWait(CtxtDebugger.hSemDeb,INFINITE);
      }
    }
  }

// --------------------------------------------------------------------------
void AttentePasAPas(DWORD ligne)
  {
  if (CtxtDebugger.hwndCode != NULL)
    {
    if (CtxtDebugger.pas)
      {
      CtxtDebugger.point_arret = ligne ;
      ::PostMessage(CtxtDebugger.hwndCode, WM_COMMAND, ID_AFFI, 0);
      DosSemSetWait(CtxtDebugger.hSemDeb,INFINITE);
      }
    }
  }

//--------------------------------------------------------
// Renvoie TRUE si le fichier d'infos de debug est pr�sent
BOOL bInfoDebugPresente (void)
	{
  char psz[MAX_PATH];

  pszCopiePathNameDocExt (psz, MAX_PATH, EXTENSION_DBG);
	return CFman::bFAcces (psz, CFman::OF_OUVRE_LECTURE_SEULE);
	}


//--------------------- centrage des boites -----------------------
static void centrage_boite (HWND hwnd, int offset_x, int offset_y)
  {
  RECT rcl_hwnd;

  ::GetWindowRect (hwnd, &rcl_hwnd);
  int x = (Appli.sizEcran.cx - rcl_hwnd.right + rcl_hwnd.left) / 2;
  int y = (Appli.sizEcran.cy - rcl_hwnd.bottom + rcl_hwnd.top) / 2;
  x += offset_x;
  y += offset_y;
  ::SetWindowPos (hwnd, HWND_TOP, x, y, 0, 0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
  }


// -------------------DEBUGGER----------------------------------------------

// ---------------------------------------------------------------------------
static void supprime_windows_variables(void)
  {
	if (CtxtDebugger.hwndChoixVariable != NULL)
		{
		::DestroyWindow(CtxtDebugger.hwndChoixVariable);
		}
	if (CtxtDebugger.hwndTraceVariable != NULL)
		{
		::DestroyWindow(CtxtDebugger.hwndTraceVariable);
		}
  }
// ---------------------------------------------------------------------------
static void supprime_windows_suivi_variable(void)
  {
	if (CtxtDebugger.hwndTraceVaCycle != NULL)
		{
		::DestroyWindow(CtxtDebugger.hwndTraceVaCycle);
		}
	if (CtxtDebugger.hwndForceVariable != NULL)
		{
		::DestroyWindow(CtxtDebugger.hwndForceVariable);
		}
  }
// ---------------------------------------------------------------------------

static void cache_windows(void)
  {
  if (CtxtDebugger.hwndCode!=NULL) 
		::ShowWindow(CtxtDebugger.hwndCode,SW_HIDE);
  if (CtxtDebugger.hwndChoixVariable!=NULL) 
	  ::ShowWindow(CtxtDebugger.hwndChoixVariable, SW_HIDE);
  if (CtxtDebugger.hwndTraceVariable!=NULL) 
		::ShowWindow(CtxtDebugger.hwndTraceVariable, SW_HIDE);
  if (CtxtDebugger.hwndForceVariable!=NULL) 
	  ::ShowWindow(CtxtDebugger.hwndForceVariable, SW_HIDE);
  if (CtxtDebugger.hwndGoLigne!=NULL) 
		::ShowWindow(CtxtDebugger.hwndGoLigne, SW_HIDE);
  if (CtxtDebugger.hwndTraceVaCycle!=NULL) 
	  ::ShowWindow(CtxtDebugger.hwndTraceVaCycle, SW_HIDE);
  }

// ---------------------------------------------------------------------------
static void montre_windows(void)
  {
	// Active chaque fen�tre debugger couramment ouvertes
  if (CtxtDebugger.hwndCode!=NULL)
		::SetWindowPos(CtxtDebugger.hwndCode,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
  if (CtxtDebugger.hwndChoixVariable!=NULL) 
		::SetWindowPos(CtxtDebugger.hwndChoixVariable,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
  if (CtxtDebugger.hwndTraceVariable!=NULL) 
		::SetWindowPos(CtxtDebugger.hwndTraceVariable,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); 
  if (CtxtDebugger.hwndForceVariable!=NULL) 
		::SetWindowPos(CtxtDebugger.hwndForceVariable,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
  if (CtxtDebugger.hwndTraceVaCycle!=NULL) 
		::SetWindowPos(CtxtDebugger.hwndTraceVaCycle,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); 
  }

// --------------------------------------------------------------------------
static void  SetTitreWindowCode (PCSTR pszMode)
	{
	char pszTitre[MAX_PATH+100];

	StrCopy (pszTitre, pszMode);
	StrConcat (pszTitre, " ");
	StrConcat (pszTitre, pszNameDoc ());
	::SetWindowText (CtxtDebugger.hwndCode, pszTitre);
	}

// --------------------------------------------------------------------------
static void ConcatValLog (BOOL bValeur, char *pszAAfficher)
  {
  char pszTemp    [c_nb_car_message_res];

	if (bValeur)
		{
		if (!bMotReserve (c_res_un, pszTemp))
			StrCopy (pszTemp, "H");
		}
	else
		{
		if (!bMotReserve (c_res_zero, pszTemp))
			StrCopy (pszTemp, "B");
		}
  StrConcat (pszAAfficher,pszTemp);
  }

// ---------------------------------------------------------------------------
static void ConcatValNum (FLOAT fValeur, char *pszAAfficher)
  {
  char   pszReel [40];
	StrFLOATToStr (pszReel, fValeur);
  StrConcat (pszAAfficher,pszReel);
  }

// ---------------------------------------------------------------------------
static void ConcatValMes (PSTR pszValeur, char *pszAAfficher, BOOL bAvecGuillemets)
  {
  if (StrIsNull (pszValeur))
    {
		if (bAvecGuillemets)
			StrConcat (pszAAfficher,"\"\"");
    }
  else
    {
		if (bAvecGuillemets)
			{
			StrConcatChar (pszAAfficher, '"');
			StrConcat (pszAAfficher, pszValeur);
			StrConcatChar (pszAAfficher, '"');
			}
		else
			StrConcat (pszAAfficher, pszValeur);
    }
  }

// ---------------------------------------------------------------------------
static BOOL bValStrLog(PCSTR pszLog)
  {
  BOOL bLog = FALSE;
  char pszEtatBas  [c_nb_car_message_res];
  char pszEtatHaut [c_nb_car_message_res];

  if (!bMotReserve (c_res_zero, pszEtatBas))
    {
    StrCopy (pszEtatBas, "B");
    }
  if (!bMotReserve (c_res_un, pszEtatHaut))
    {
    StrCopy (pszEtatHaut, "H");
    }
  StrToBOOL (&bLog, pszLog, pszEtatBas, pszEtatHaut);
	return bLog;
  }

// ---------------------------------------------------------------------------
static FLOAT fValStrNum(PCSTR pszNum)
  {
  FLOAT fValeur = (FLOAT)0;
  StrToFLOAT (&fValeur, pszNum);
	return fValeur;
  }

// ---------------------------------------------------------------------------
static void ConcatValeurCouranteES (DWORD wGenre, DWORD wPosEx, char *pszAAfficher, BOOL bAvecGuillemets)
  {
  switch (wGenre)
    {
    case c_res_logique:
			ConcatValLog(CPcsVarEx::bGetValVarLogEx(wPosEx),pszAAfficher);
			break;

    case c_res_numerique:
			ConcatValNum (CPcsVarEx::fGetValVarNumEx(wPosEx), pszAAfficher);
			break;

    case c_res_message:
      {
			char szMesTemp [c_nb_car_ex_mes+2];
			ConcatValMes (CPcsVarEx::pszGetValVarMesEx(szMesTemp,wPosEx), pszAAfficher, bAvecGuillemets);
      }
      break;
    }
  }

// ---------------------------------------------------------------------------
static void ConcatValeurMemES(DWORD wGenre, DWORD wPosEx, char *pszAAfficher, BOOL bAvecGuillemets)
  {
  switch (wGenre)
    {
    case c_res_logique:
			ConcatValLog(CPcsVarEx::bGetValMemVarLogEx(wPosEx),pszAAfficher);
			break;

    case c_res_numerique:
      ConcatValNum (CPcsVarEx::fGetValMemVarNumEx(wPosEx), pszAAfficher);
      break;
    
		case c_res_message:
			{
			char szMesTemp [c_nb_car_ex_mes+2];
			ConcatValMes (CPcsVarEx::pszGetValMemVarMesEx(szMesTemp,wPosEx), pszAAfficher, bAvecGuillemets);
			}
      break;
    }
  }

// ---------------------------------------------------------------------------
static void ForceValeurCouranteDansBd (DWORD wGenre, DWORD wPosEx, PCSTR pszValeur)
  {
  switch (wGenre)
    {
    case c_res_logique:
			CPcsVarEx::SetValVarLogEx(wPosEx, bValStrLog(pszValeur));
			break;

    case c_res_numerique:
			CPcsVarEx::SetValVarNumEx(wPosEx,fValStrNum(pszValeur));
			break;

    case c_res_message:
			CPcsVarEx::SetValVarMesEx(wPosEx,pszValeur);
      break;
    }
  }

// ---------------------------------------------------------------------------
static void ForceValeurMemDansBd (DWORD wGenre, DWORD wPosEx, PCSTR pszValeur)
  {
  switch (wGenre)
    {
    case c_res_logique:
			CPcsVarEx::SetValMemVarLogEx(wPosEx, bValStrLog(pszValeur));
      break;
    case c_res_numerique:
			CPcsVarEx::SetValMemVarNumEx(wPosEx,fValStrNum(pszValeur));
      break;
    case c_res_message:
			CPcsVarEx::SetValMemVarMesEx(wPosEx,pszValeur);
      break;
    }
  }
// ---------------------------------------------------------------------------
// concatene a pszAAfficher la valeur au cycle n suivi entre() de la valeur au cycle n-1 pour les log et les num
static void FormateValeursDansBd(DWORD wGenre,DWORD wPosEx,char *pszAAfficher)
// valeur n uniquement pour les mes 
	{
  switch (wGenre)
    {
    case c_res_logique:
			ConcatValLog(CPcsVarEx::bGetValVarLogEx(wPosEx),pszAAfficher);
			StrConcat (pszAAfficher, " - (");
			ConcatValLog(CPcsVarEx::bGetValMemVarLogEx(wPosEx),pszAAfficher);
			StrConcat (pszAAfficher, ")");
      break;

    case c_res_numerique:
			ConcatValNum (CPcsVarEx::fGetValVarNumEx(wPosEx), pszAAfficher);
			StrConcat (pszAAfficher, " - (");
			ConcatValNum (CPcsVarEx::fGetValMemVarNumEx(wPosEx), pszAAfficher);
			StrConcat (pszAAfficher, ")");
      break;

    case c_res_message:
			{
			char szMesTemp [c_nb_car_ex_mes+2];
			ConcatValMes (CPcsVarEx::pszGetValVarMesEx(szMesTemp,wPosEx), pszAAfficher, TRUE);
			}
      break;
    }
	}

// ---------------------------------------------------------------------------
static void supprime_tableau (DWORD item)
  {
	// big danger : si no_enr > FFFF
  DWORD    no_enr = LOWORD (SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1, LB_GETITEMDATA, item,0));
  tx_e_sd*ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,no_enr);
  char     pszAAfficher [255];

  StrCopy (pszAAfficher, "+");
  StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
	// $$ un simple AddItemData doit suffire � provoquer le r�affichage
  // $$ item1= LOWORD(SendDlgItemMessage (hwnd,ID_DEB_LIST_VAR1, LM_SETITEMTEXT, item, (LPARAM)pszAAfficher));
  SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1, LB_DELETESTRING, item, 0);
  SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1, LB_INSERTSTRING, item, (LPARAM)pszAAfficher);
  SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETITEMDATA, item, (LPARAM)no_enr);

  for (DWORD w=0;w < ptr_e_s->wTaille;w++)
    {
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1, LB_DELETESTRING, item+1,0);
    }
  }

// ---------------------------------------------------------------------------
static void insere_tableau (DWORD item)
  {
  // big danger : si no_enr > FFFF
  DWORD    no_enr = LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETITEMDATA,item,0));
  tx_e_sd*ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,no_enr);
  char     pszAAfficher [255];
  char     pszReel [20];
  DWORD    item1;
  DWORD    w;

  StrCopy (pszAAfficher,"-");
  StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
  SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_DELETESTRING, item, 0);
  item1= LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_INSERTSTRING, item, (LPARAM)pszAAfficher));
  SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETITEMDATA, item, (LPARAM)no_enr);

  for (w=0;w < ptr_e_s->wTaille;w++)
    {
    StrCopy (pszAAfficher,"  [ ");
    StrDWordToStr (pszReel, w + 1);
    StrConcat (pszAAfficher,pszReel);
    StrConcat (pszAAfficher," ] =");

    FormateValeursDansBd(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx+w,pszAAfficher);

    item1 = LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_INSERTSTRING,item+w+1, (LPARAM)pszAAfficher));
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETITEMDATA,item1, MAKELPARAM(no_enr,w+1));
    }
  }

// ---------------------------------------------------------------------------
static void maj_tableau (DWORD item)
  {
	// big danger : si no_enr > FFFF
  DWORD    no_enr = LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETITEMDATA,item,0));
  DWORD    index_tab = HIWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETITEMDATA,item,0));
  tx_e_sd*ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,no_enr);
  char     pszAAfficher [255];
  char     pszReel [20];

  if (index_tab==0)
    {
    StrCopy (pszAAfficher,"-");
    StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_DELETESTRING, item, 0);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_INSERTSTRING, item, (LPARAM)pszAAfficher);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETITEMDATA, item, (LPARAM)no_enr);
    }
  else
    {
    StrCopy (pszAAfficher,"  [ ");
    StrDWordToStr (pszReel, index_tab);
    StrConcat (pszAAfficher,pszReel);
    StrConcat (pszAAfficher," ] =");
    FormateValeursDansBd(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx+index_tab-1,pszAAfficher);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_DELETESTRING, item, 0);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_INSERTSTRING, item, (LPARAM)pszAAfficher);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETITEMDATA, item, MAKELPARAM(no_enr,index_tab));
   }
  }

// ---------------------------------------------------------------------------
static void MiseAJourTraceVariables (DWORD item)
  {
  tx_e_sd*ptr_e_s;
  char     pszAAfficher [255];
  DWORD    no_enr;

  StrSetNull (pszAAfficher);
	// big danger : si no_enr > FFFF
  no_enr = LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETITEMDATA,item,0));
  ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,no_enr);
  if(ptr_e_s->wTaille == 0)
    {
    StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
    StrConcat (pszAAfficher," = ");
    FormateValeursDansBd(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx,pszAAfficher);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_DELETESTRING, item, 0);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_INSERTSTRING, item, (LPARAM)pszAAfficher);
    SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETITEMDATA, item, (LPARAM)no_enr);
    }
  else
    {
    if(ptr_e_s->wOuvert)
      {
      maj_tableau(item);
      }
    else
      {
      StrConcatChar (pszAAfficher, '+');
      StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
      SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_DELETESTRING, item, 0);
      SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_INSERTSTRING, item, (LPARAM)pszAAfficher);
      SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETITEMDATA, item, (LPARAM)no_enr);
      }
    }
  }

// ---------------------------------------------------------------------------
static void insere_boite_deb (DWORD item)
  {
  tx_e_sd  *ptr_e_s;
  PX_E_S   ptr_e_s1;
  char     pszAAfficher [255];
  DWORD    item1;
  DWORD    no_enr;
  DWORD    w;
  BOOL     trouve;

  StrSetNull (pszAAfficher);
  ptr_e_s1 = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s,item+1);
  no_enr = nb_enregistrements (szVERIFSource, __LINE__, b_var_deb);

  trouve = FALSE;
  for (w=1;w <= no_enr;w++)
    {
    ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,w);
    if (ptr_e_s->wPosEx == ptr_e_s1->wPosEx)
      {
      trouve = TRUE;
      break;
      }
    }
  if (!trouve) // n existe pas
    {
    trouve = FALSE;
    for (w=1;w <= no_enr;w++)
      {
      ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,w);
      if (ptr_e_s->wPosEx == 0)
        {
        trouve = TRUE;
        break;
        }
      }
    if (trouve)
      {
      no_enr = w;
      }
    else
      {
      insere_enr (szVERIFSource, __LINE__, 1,b_var_deb,nb_enregistrements (szVERIFSource, __LINE__, b_var_deb)+1);
      no_enr = nb_enregistrements (szVERIFSource, __LINE__, b_var_deb);
      }
    ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,no_enr);
    StrCopy (ptr_e_s->pszNomVar,ptr_e_s1->pszNomVar);
    ptr_e_s->wPosEx     = ptr_e_s1->wPosEx;
    ptr_e_s->wBlocGenre = ptr_e_s1->wBlocGenre;
    ptr_e_s->wTaille    = ptr_e_s1->wTaille;
    ptr_e_s->wOuvert = FALSE;

    if(ptr_e_s->wTaille == 0)
      {
      StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
      StrConcat (pszAAfficher," = ");

      FormateValeursDansBd(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx,pszAAfficher);
      }
    else
      {
      StrConcatChar (pszAAfficher, '+');
      StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
      }
      item1 = LOWORD (SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1, LB_INSERTSTRING,(WPARAM)-1, (LPARAM)pszAAfficher));
      SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETITEMDATA,item1, (LPARAM) no_enr);
    }
  }

// -------------------------------------------------------------------
// Visu cycle n-1
static BOOL CALLBACK dlgprocDebugVisuValCyclePrecedent (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL     mres = 0;		   // Valeur de retour
	typedef struct
		{
		DWORD                 indice;
		DWORD                 no_enr;
		} ENV_DBG6,*PENV_DBG6;

	PENV_DBG6		pEnv;
  DWORD       item;
  char        chaine[255];
  tx_e_sd     *ptr_e_s;
  char        pszAAfficher [255];

  switch (msg)
    {
    case WM_INITDIALOG:
      centrage_boite (hwnd,-155,0);
			pEnv = (PENV_DBG6)pCreeEnv (hwnd, sizeof(ENV_DBG6));
			pEnv->no_enr = 0;
			pEnv->indice = 0;
			item =  SendDlgItemMessage (CtxtDebugger.hwndTraceVariable, ID_DEB_LIST_VAR1, LB_GETCURSEL, 0, 0);
      pEnv->no_enr = LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETITEMDATA, item,0));
      pEnv->indice = HIWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETITEMDATA, item,0));
      ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,pEnv->no_enr);
      StrSetNull (pszAAfficher);
      StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
      if (pEnv->indice != 0)
        {
        StrDWordToStr (chaine, pEnv->indice);
        StrConcat (pszAAfficher," [ ");
        StrConcat (pszAAfficher,chaine);
        StrConcat (pszAAfficher," ] ");
        pEnv->indice--;
        }
      SetDlgItemText(hwnd,ID_AFFI,pszAAfficher);
      StrSetNull (pszAAfficher);
			ConcatValeurMemES(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx+pEnv->indice,pszAAfficher, TRUE);
      SetDlgItemText(hwnd,ID_VAR,pszAAfficher);
			::SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
      break;

    case WM_DESTROY:
			bLibereEnv(hwnd);
			CtxtDebugger.hwndTraceVaCycle = NULL;
      break;

    case WM_COMMAND:
      pEnv = (PENV_DBG6)pGetEnv(hwnd);
      switch(LOWORD(mp1))
        {
    		case IDOK:
					ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,pEnv->no_enr);
					StrSetNull (pszAAfficher);
					ConcatValeurMemES(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx+pEnv->indice,pszAAfficher, TRUE);
					SetDlgItemText(hwnd,ID_VAR,pszAAfficher);
					break;
        }
      break;

		case WM_CLOSE:
			::DestroyWindow (hwnd);
			break;
    }
  return mres;
  }

// -------------Forcage variables-----------------------------------------
static BOOL CALLBACK dlgprocFebugForcageVariables (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL   mres = 0;		   // Valeur de retour
	typedef struct
		{
		DWORD          indice;
		DWORD          no_enr;
		} ENV_DBG5,*PENV_DBG5;

	PENV_DBG5	pEnv;
  DWORD     item;
  char      chaine[255];
  tx_e_sd   *ptr_e_s;
  char      pszAAfficher [255];

  switch (msg)
    {
    case WM_INITDIALOG:
			{
      centrage_boite (hwnd,0,0);
			pEnv = (PENV_DBG5)pCreeEnv (hwnd, sizeof(ENV_DBG5));
			pEnv->no_enr = 0;
			pEnv->indice = 0;
			item =  SendDlgItemMessage (CtxtDebugger.hwndTraceVariable, ID_DEB_LIST_VAR1, LB_GETCURSEL, 0, 0);
      pEnv->no_enr = LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETITEMDATA, item,0));
      pEnv->indice = HIWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETITEMDATA, item,0));
      ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,pEnv->no_enr);
      StrSetNull (pszAAfficher);
      StrConcat (pszAAfficher,ptr_e_s->pszNomVar);
      if (pEnv->indice != 0)
        {
        StrDWordToStr (chaine, pEnv->indice);
        StrConcat (pszAAfficher," [ ");
        StrConcat (pszAAfficher,chaine);
        StrConcat (pszAAfficher," ] ");
        pEnv->indice -= 1;
        }
      SetDlgItemText(hwnd,ID_AFFI,pszAAfficher);
      StrSetNull (pszAAfficher);
			ConcatValeurCouranteES(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx+pEnv->indice,pszAAfficher, FALSE);
      SetDlgItemText(hwnd,ID_VAR,pszAAfficher);
      StrSetNull (pszAAfficher);
			ConcatValeurMemES(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx+pEnv->indice,pszAAfficher, FALSE);
      SetDlgItemText(hwnd,ID_PAS,pszAAfficher);
			::SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
			}
      break;

    case WM_DESTROY:
			{
			bLibereEnv (hwnd);
			CtxtDebugger.hwndForceVariable = NULL;
			}
			break;

    case WM_COMMAND:
      pEnv = (PENV_DBG5)pGetEnv(hwnd);
      switch(LOWORD(mp1))
        {
    		case ID_ABANDON:
					::DestroyWindow(hwnd);
					break;

    		case IDOK:
					{
					ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,pEnv->no_enr);
					GetDlgItemText(hwnd,ID_VAR,pszAAfficher,255);
					ForceValeurCouranteDansBd(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx+pEnv->indice,pszAAfficher);
					GetDlgItemText(hwnd,ID_PAS,pszAAfficher,255);
					ForceValeurMemDansBd(ptr_e_s->wBlocGenre,ptr_e_s->wPosEx+pEnv->indice,pszAAfficher);
					::PostMessage(CtxtDebugger.hwndCode, WM_COMMAND,ID_AFFI,0);
					::DestroyWindow(hwnd);
					}
					break;
        }
      break;

		case WM_CLOSE:
			::DestroyWindow (hwnd);
			break;
    }
  return mres;
  }

// ---------------------------------------------------------------------
// boite Aller a la ligne
static BOOL CALLBACK dlgprocAllerALaLigne (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL	mres = 0;		   // Valeur de retour
  DWORD	item;
  char	chaine[80];

  switch (msg)
    {
    case WM_INITDIALOG:
      centrage_boite (hwnd,0,0);
			::SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
      break;

    case WM_DESTROY:
			CtxtDebugger.hwndGoLigne = NULL;
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
	    	case ID_ABANDON:
					::DestroyWindow(hwnd);
					break;

    		case IDOK:
					GetDlgItemText(hwnd,ID_LIGNE, chaine,80);
					if (StrToDWORD (&item, chaine))
						{
						if ((item > 0) && (item < 0xFFFF))
							{
							SendDlgItemMessage (CtxtDebugger.hwndCode, ID_CODE, LB_SETCURSEL, item-1, 0);
							}
						}
					::DestroyWindow(hwnd);
					break;
        }
      break;

		case WM_CLOSE:
			::DestroyWindow (hwnd);
			break;

    default:
      break;
    }
  return mres;
  } // dlgprocAllerALaLigne

//-----------------------------------------------------------------------
// boite variables a surveiller
static BOOL CALLBACK dlgprocSurveillanceVariables (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL	mres = 0;		   // Valeur de retour
  DWORD	item;
  char	chaine[80];
  DWORD	no_enr;
  tx_e_sd*ptr_e_s;

  switch (msg)
    {
    case WM_INITDIALOG:
      centrage_boite (hwnd,160,155);
			cree_bloc(b_var_deb,sizeof(tx_e_sd),0);
			::SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
      break;
			
		case WM_DESTROY:
			supprime_windows_suivi_variable();
			enleve_bloc(b_var_deb);
			CtxtDebugger.hwndTraceVariable = NULL;
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_DEB_LIST_VAR1:
					if (HIWORD(mp1) == LBN_DBLCLK) //double click
						{
						// r�cup�re l'index du texte s�lectionn�
						item =  SendDlgItemMessage (hwnd, ID_DEB_LIST_VAR1, LB_GETCURSEL, 0, 0);
						no_enr = LOWORD(SendDlgItemMessage (hwnd,ID_DEB_LIST_VAR1,LB_GETITEMDATA,item,0));
						SendDlgItemMessage(hwnd,ID_DEB_LIST_VAR1,LB_GETTEXT,(WPARAM)item,(LPARAM)chaine);
						ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,no_enr);
						if (chaine[0] == '+')
							{
							ptr_e_s->wOuvert = TRUE;
							insere_tableau(item);
							}
						if (chaine[0] == '-')
							{
							ptr_e_s->wOuvert = FALSE;
							supprime_tableau(item);
							}
						}
					break;
					
				case ID_SUPPRIMER:
					item =  SendDlgItemMessage (hwnd, ID_DEB_LIST_VAR1, LB_GETCURSEL, 0, 0);
					if (item != LB_ERR) // au moins une selection
						{
						if (SendDlgItemMessage (hwnd, ID_DEB_LIST_VAR1, LB_GETSEL, (WPARAM)item, 0) != 0)
							{
							SendDlgItemMessage(hwnd,ID_DEB_LIST_VAR1,LB_GETTEXT,(WPARAM)item,(LPARAM)chaine);
							if ((chaine[0] != ' ')&&(chaine[0] != '-'))
								{
								no_enr= LOWORD(SendDlgItemMessage (hwnd,ID_DEB_LIST_VAR1,LB_GETITEMDATA,item,0));
								SendDlgItemMessage(hwnd,ID_DEB_LIST_VAR1, LB_DELETESTRING,item,0);
								ptr_e_s = (tx_e_sd*)pointe_enr (szVERIFSource, __LINE__, b_var_deb,no_enr);
								ptr_e_s->wPosEx = 0;
								supprime_windows_suivi_variable();
								}
							}
						}
					break;
										
				case ID_FORCER:
					item =  SendDlgItemMessage (hwnd, ID_DEB_LIST_VAR1, LB_GETCURSEL, 0, 0);
					if (item != LB_ERR) // au moins une selection
						{
						if (SendDlgItemMessage (hwnd, ID_DEB_LIST_VAR1, LB_GETSEL, (WPARAM)item, 0) != 0)
							{
							SendDlgItemMessage(hwnd,ID_DEB_LIST_VAR1,LB_GETTEXT,(WPARAM)item,(LPARAM)chaine);
							if ((chaine[0] != '+')&&(chaine[0] != '-'))
								{
								if (CtxtDebugger.hwndForceVariable != NULL)
									{
									::DestroyWindow(CtxtDebugger.hwndForceVariable);
									}
								CtxtDebugger.hwndForceVariable = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_VAR_FORC), Appli.hwnd, dlgprocFebugForcageVariables, (LPARAM)NULL);
								}
							}
						}
					break;
					
				case ID_N_1:
					item = SendDlgItemMessage (hwnd, ID_DEB_LIST_VAR1, LB_GETCURSEL, 0, 0);
					if (item != LB_ERR) // au moins une selection
						{
						if (SendDlgItemMessage (hwnd, ID_DEB_LIST_VAR1, LB_GETSEL, (WPARAM)item, 0) != 0)
							{
							SendDlgItemMessage(hwnd,ID_DEB_LIST_VAR1,LB_GETTEXT,(WPARAM)item,(LPARAM)chaine);
							if ((chaine[0] != '+')&&(chaine[0] != '-'))
								{
								if (CtxtDebugger.hwndTraceVaCycle != NULL)
									{
									::DestroyWindow(CtxtDebugger.hwndTraceVaCycle);
									}
								CtxtDebugger.hwndTraceVaCycle = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_VAR_N_1), Appli.hwnd, dlgprocDebugVisuValCyclePrecedent, (LPARAM)NULL);
								break;
								}
							}
						}
					break;
					
				default:
					break;
					
        }
			break;
			
			
		case WM_CLOSE:
			::DestroyWindow (hwnd);
			break;

    default:
      break;
    }
		return (mres);
  }

//--------------------------------------------------
// Arrange un bouton de la fen�tre debug code
static BOOL bGetItemRect(HWND hwnd, int nIDDlgItem, LPRECT lpRect)
	{
	BOOL bRes = FALSE;
  HWND hwndItem = ::GetDlgItem (hwnd,nIDDlgItem);
	RECT rect;
  if (hwndItem && ::GetWindowRect (hwndItem, &rect))
		{
		POINT point;
		point.x = rect.left; 
		point.y = rect.top;
		ScreenToClient (hwnd, &point);
		lpRect->left = point.x; 
		lpRect->top = point.y;

		point.x = rect.right;
		point.y = rect.bottom; 
		ScreenToClient (hwnd, &point);
		lpRect->right = point.x; 
		lpRect->bottom = point.y;
		bRes = TRUE;
		}
	return bRes;
	}

//--------------------------------------------------
// Arrange les diff�rents �l�ments de la fen�tre Liste Variables
static void ArrangeVueDebugListeVariables (HWND hwnd, BOOL bRedessine)
  {
	// R�cup�re la zone cliente de la boite de dialogue
  RECT rcClient;
	::GetClientRect (hwnd, &rcClient);

	// redimensionne la liste des variables (zone cliente - boutons)
  RECT rcTemp;
	if (bGetItemRect(hwnd, ID_DEB_LIST_VAR, &rcTemp))
		{
		if (rcClient.bottom > rcTemp.top)
			{
			::MoveWindow (GetDlgItem (hwnd,ID_DEB_LIST_VAR), rcTemp.left, rcTemp.top, 
				rcClient.right, rcClient.bottom, bRedessine);
			}
		}
	
  //if (bRedessine)
  //  ::InvalidateRect(hwnd,NULL,TRUE);
  } // ArrangeVueDebugCode


//----------------------------------------------------------------------
static BOOL bExecutionCodeSuspendueParDebug(void)
	{
	return (CtxtDebugger.On && (dwSemAttenteLibre(CtxtDebugger.hSemDeb, 0)==SEM_TIMEOUT));
	}

//------------------------------------------------------------------
// Renvoie TRUE si la variable est marqu�e modifi�e
static BOOL bVarExModifiee (X_E_S x_e_s)
	{
	BOOL bRet = FALSE;

	if (CPcsVarEx::bGetBlocExValeurModifie(x_e_s.wPosEx, x_e_s.wBlocGenre))
		bRet = TRUE;
	return bRet;
	}

//----------------------------------------------------------------------
// boite de dialogue de la liste des variables bd
static BOOL CALLBACK dlgprocDebugListeVariables (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL	mres = FALSE;		   // Valeur de retour

  switch (msg)
    {
    case WM_INITDIALOG:
			{
      centrage_boite (hwnd,-160,155);

			// Initialise la list box avec des num�ros de variables pour chaque entr�e/sortie
			DWORD dwNbVars = CPcsVarEx::dwGetNbGlobalVarsEx();
			for (DWORD dwNItem = 1; dwNItem < dwNbVars; dwNItem++)
				{
				SendDlgItemMessage (hwnd,ID_DEB_LIST_VAR, LB_ADDSTRING,0, (LPARAM) dwNItem);
				}
			ArrangeVueDebugListeVariables (hwnd, FALSE);
			::SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
			}
      break;

    case WM_DESTROY:
			CtxtDebugger.hwndChoixVariable = NULL;
      break;

		case WM_SIZE:
			{
			if (mp1 != SIZE_MINIMIZED)
				ArrangeVueDebugListeVariables (hwnd, TRUE);
			}
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_DEB_LIST_VAR:
					{
					if (HIWORD(mp1) == LBN_DBLCLK) //double click
						{
						if (CtxtDebugger.hwndTraceVariable==NULL)
							{
							CtxtDebugger.hwndTraceVariable = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_VAR_DEB1), Appli.hwnd, dlgprocSurveillanceVariables, (LPARAM)NULL);
							}
						// r�cup�re l'index du texte s�lectionn�
						DWORD	dwNItem = SendDlgItemMessage (hwnd, ID_DEB_LIST_VAR, LB_GETCURSEL, 0, 0);
						insere_boite_deb (dwNItem);
						}
					}
					break;

				case ID_AFFI:
					InvalidateRect(GetDlgItem (hwnd, ID_DEB_LIST_VAR), NULL, FALSE);
					break;
        }
      break;

    case WM_MEASUREITEM:
			{
			// Taille d'un item owner draw : liste des variables ?
			if (mp1 == ID_DEB_LIST_VAR)
				{ 
				// oui => renseigne hauteur et largeur d'un item de la liste :
				// r�cup�re les caract�ristiques de la police de la boite de dialogue parent
				LPMEASUREITEMSTRUCT pMeasureItem = (LPMEASUREITEMSTRUCT)mp2;
				HDC hdc = ::GetDC (hwnd); // hwndList n'est pas encore affect�
				TEXTMETRIC  tm;
				GetTextMetrics (hdc, &tm);
				::ReleaseDC (hwnd, hdc);
				//$$wmgetfont
				pMeasureItem->itemHeight = tm.tmHeight;
				pMeasureItem->itemWidth = 80 * tm.tmMaxCharWidth; // $$ taille du M Majuscule fm.lEmInc; =
				mres = TRUE;
				}
			}
			break;


    case WM_DRAWITEM:
			{
			// dessin dans la listbox d'une ligne repr�sentant une entr�e sortie
			LPDRAWITEMSTRUCT pDIS = (LPDRAWITEMSTRUCT) mp2;

			// Dessine le fond de l'item
			if (pDIS->itemState & ODS_SELECTED)
				FillRect (pDIS->hDC, &pDIS->rcItem, GetSysColorBrush(COLOR_HIGHLIGHT));
			else
				FillRect (pDIS->hDC, &pDIS->rcItem, GetSysColorBrush(COLOR_WINDOW));

			// Un item existant � dessiner ?
			if (pDIS->itemID != -1) 
				{
				// oui => met en forme le texte...
				CHAR szAAfficher[1000] = "?"; 
				BOOL bVarModifee = FALSE;

				DWORD dwPos = 0;
				DWORD dwBloc = 0;

				// Les donn�es de la variable � repr�senter sont disponibles ?
				if (bExecutionCodeSuspendueParDebug() && 
					CPcsVarEx::bListeGlobalVarEx(pDIS->itemData, &dwPos, &dwBloc))
					{
					CHAR szTemp[1000]; 
					X_E_S x_e_s;
					if (CPcsVarEx::bInfoVarEx (dwPos, dwBloc, &x_e_s))
						{
						bVarModifee = bVarExModifiee(x_e_s);
						StrCopy (szAAfficher,x_e_s.pszNomVar);
						if (x_e_s.wTaille > 0)
							{
							StrConcat (szAAfficher, " (");
							StrConcat (szAAfficher, StrDWordToStr(szTemp,x_e_s.wTaille));
							StrConcat (szAAfficher, ") [1] = ");
							}
						else
							StrConcat (szAAfficher, " = ");

						ConcatValeurCouranteES(dwBloc,dwPos,szAAfficher, TRUE);
						StrConcat (szAAfficher, " - (");
						ConcatValeurMemES(dwBloc,dwPos,szAAfficher, TRUE);
						StrConcat (szAAfficher, ")");
						}
					}

				// Dessine le avec une couleur et un fond repr�sentant l'�tat de l'item pour l'utilisateur
				if (pDIS->itemState & ODS_SELECTED)
					{
					if (bVarModifee)
						SetTextColor (pDIS->hDC, COULEUR_VAR_MODIFIEE);
					else
						SetTextColor (pDIS->hDC, GetSysColor (COLOR_HIGHLIGHTTEXT));
					SetBkColor(pDIS->hDC, GetSysColor (COLOR_HIGHLIGHT));
					}
				else
					{
					if (bVarModifee)
						SetTextColor (pDIS->hDC, COULEUR_VAR_MODIFIEE);
					else
						SetTextColor (pDIS->hDC, GetSysColor (COLOR_WINDOWTEXT));
					SetBkColor(pDIS->hDC, GetSysColor (COLOR_WINDOW));
					}
				TEXTMETRIC  tm;							
				GetTextMetrics(pDIS->hDC, &tm); 
				TextOut(pDIS->hDC, 2, (pDIS->rcItem.bottom + pDIS->rcItem.top - tm.tmHeight) / 2, 
					szAAfficher, strlen(szAAfficher)); 
				}

			// L'item a t'il le focus ? 
			if (pDIS->itemState & ODS_FOCUS) 
				{ 
				// oui => dessine un rectangle de s�lection autour de l'item
				DrawFocusRect(pDIS->hDC, &pDIS->rcItem);
				} 

			// message trait�
			mres = TRUE;
			}
			break;

		case WM_CLOSE:
			::DestroyWindow (hwnd);
			break;
    }
  return mres;
  }


//--------------------------------------------------
// Arrange les diff�rents �l�ments de la fen�tre debug code
static void ArrangeVueDebugCode (HWND hwnd, BOOL bRedessine)
  {
	// R�cup�re la zone cliente de la boite de dialogue
  RECT rcClient;
	::GetClientRect (hwnd, &rcClient);

	// redimensionne la liste des lignes de code (zone cliente - boutons)
  RECT rcTemp;
	if (bGetItemRect(hwnd, ID_CODE, &rcTemp))
		{
		if (rcClient.bottom > rcTemp.top)
			{
			::MoveWindow (GetDlgItem (hwnd,ID_CODE), rcTemp.left, rcTemp.top, 
				rcClient.right, rcClient.bottom - rcTemp.top, bRedessine);
			}
		}
  } // ArrangeVueDebugCode


//----------------------------------------------------------------------
// Boite principale de debug du code
static BOOL CALLBACK dlgprocDebugCode (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL	mres = 0;		   // Valeur de retour
	
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			char pszNomFic[MAX_PATH];
			HICON	hIcone = LoadIcon(Appli.hinstDllLng, MAKEINTRESOURCE (IDICO_APPLI_EXE_EN_COURS));

      centrage_boite (hwnd,10,10);
			::SendMessage(hwnd, WM_SETICON, FALSE, (LPARAM)hIcone); 

      CtxtDebugger.point_arret = 1;
      CtxtDebugger.pas = TRUE;
		  DWORD	ligne = 0;

			// ouverture du fichier .dbg
      StrCopy (pszNomFic, (PSTR) (mp2));
			pszNomAjouteExt (pszNomFic, MAX_PATH, EXTENSION_DBG);

			CFman::HF hFile;
			if (CFman::bFOuvre (&hFile, pszNomFic, CFman::OF_OUVRE_LECTURE_SEULE))
				{
				// Ajout de chaque ligne dans la liste box code
				char pszSource[TAILLE_LIGNE_MAX_DEBUG];
				while(CFman::bFLireLn (hFile, pszSource, 255))
					{
					// Format : num�ro de ligne suivi de la ligne elle m�me
					ligne = SendDlgItemMessage (hwnd,ID_CODE ,LB_GETCOUNT,0,0);
					char pszLigne[TAILLE_LIGNE_MAX_DEBUG];
					StrPrintFormat (pszLigne, "%5lu %s", ligne+1, pszSource);
					ligne = LOWORD(SendDlgItemMessage (hwnd,ID_CODE, LB_INSERTSTRING,(WPARAM)-1, (LPARAM) pszLigne));
					}
				CFman::bFFerme(&hFile);
				}
      CtxtDebugger.nb_lignes = ligne + 1;
			SendDlgItemMessage (hwnd, ID_CODE, EM_LIMITTEXT, (WPARAM) (LG_RECHERCHE_MAX-1), 0);
			SetDlgItemText (hwnd, QHELP_SRCH_CHAINE, CtxtDebugger.szRecherche);
			SendDlgItemMessage (hwnd, ID_CODE, LB_SETCURSEL, CtxtDebugger.point_arret - 1, 0);

			//
			CtxtDebugger.hSemDeb = hSemCree (TRUE);
			CtxtDebugger.On = TRUE; 
			ArrangeVueDebugCode (hwnd, FALSE);

			::SetWindowPos(hwnd,HWND_TOPMOST,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
			}
      break;
			
    case WM_DESTROY:
			{
			supprime_windows_variables();
			if (CtxtDebugger.hwndGoLigne != NULL)
				{
				::DestroyWindow (CtxtDebugger.hwndGoLigne);
				}
			GetDlgItemText (hwnd, QHELP_SRCH_CHAINE, CtxtDebugger.szRecherche, LG_RECHERCHE_MAX-1);
			CtxtDebugger.On = FALSE; 
			bSemLibere (CtxtDebugger.hSemDeb);		
			bSemFerme	(&CtxtDebugger.hSemDeb);		
			CtxtDebugger.hwndCode = NULL;
			}
      break;

		case WM_SIZE:
			{
			if (mp1 != SIZE_MINIMIZED)
				ArrangeVueDebugCode (hwnd, TRUE);
			}
			break;


    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
				case QHELP_RCH_TXT:
					{
					// Initialise la recherche
					EnableWindow(GetDlgItem(hwnd, QHELP_SRCH_CHAINE), FALSE);
					GetDlgItemText (hwnd, QHELP_SRCH_CHAINE, CtxtDebugger.szRecherche, LG_RECHERCHE_MAX-1);
					BOOL bTrouve = FALSE;
					DWORD dwNLigneDepart = CtxtDebugger.point_arret;
					DWORD dwNLigne = 0;

					// Recherche � partir de la ligne suivant le point d'arr�t courant
					for (dwNLigne = dwNLigneDepart+1; dwNLigne <= CtxtDebugger.nb_lignes;dwNLigne++)
						{
						char szLigneTemp[TAILLE_LIGNE_MAX_DEBUG]="";

						if (SendDlgItemMessage (hwnd, ID_CODE, LB_GETTEXT, (WPARAM) (dwNLigne-1), (LPARAM)(PCSTR)szLigneTemp) != LB_ERR)
							{
							// Chaine trouv�e dans cette ligne ?
							if (StrSearchStr(szLigneTemp, CtxtDebugger.szRecherche) != STR_NOT_FOUND)
								{
								bTrouve = TRUE;
								break;
								}
							}
						else
							{
							VerifWarningExit;
							break;
							}
						}

					// chaine non trouv�e en fin ?
					if (!bTrouve)
						{
						// oui => recherche du d�but � la ligne pr�c�dant le point d'arr�t courant
						for (DWORD dwNLigne = 1; dwNLigne < dwNLigneDepart;dwNLigne++)
							{
							char szLigneTemp[TAILLE_LIGNE_MAX_DEBUG]="";

							if (SendDlgItemMessage (hwnd, ID_CODE, LB_GETTEXT, (WPARAM) (dwNLigne-1), (LPARAM)(PCSTR)szLigneTemp) != LB_ERR)
								{
								// Chaine trouv�e dans cette ligne ?
								if (StrSearchStr(szLigneTemp, CtxtDebugger.szRecherche) != STR_NOT_FOUND)
									{
									bTrouve = TRUE;
									break;
									}
								}
							else
								{
								VerifWarningExit;
								break;
								}
							}
						} // if (!bTrouve)

					// La chaine a �t� trouv�e quelque part ?
					if (bTrouve)
						{
						DWORD	item =  SendDlgItemMessage (hwnd, ID_CODE, LB_SETCURSEL, (WPARAM)(dwNLigne-1), 0);
						if (item != LB_ERR)
							CtxtDebugger.point_arret = dwNLigne;
						else
							CtxtDebugger.point_arret = 1;
						}
					EnableWindow(GetDlgItem(hwnd, QHELP_SRCH_CHAINE), TRUE);
					}
					break;

				case ID_ALLER:
					{
					CtxtDebugger.pas = FALSE;
					SetTitreWindowCode (" [Run]");
					DWORD	item =  SendDlgItemMessage (hwnd, ID_CODE, LB_GETCURSEL, 0, 0);
					if (item != LB_ERR)
						{
						if (SendDlgItemMessage (hwnd, ID_CODE, LB_GETSEL, (WPARAM)item, 0) != 0)
							{
							CtxtDebugger.point_arret =item + 1;
							}
						else
							{
							CtxtDebugger.point_arret =1;
							}
						cache_windows();
						bSemLibere (CtxtDebugger.hSemDeb);		
						}
					}
					break;
					
				case ID_PAS:
					{
					CtxtDebugger.pas = TRUE;
					if (CtxtDebugger.point_arret == (CtxtDebugger.nb_lignes+1))
						{
						CtxtDebugger.point_arret = 1;
						}
					SetTitreWindowCode (" [Run]");
					bSemLibere (CtxtDebugger.hSemDeb);		
					}
          break;
					
				case ID_VAR:
					if (CtxtDebugger.hwndChoixVariable==NULL)
						{
						CtxtDebugger.hwndChoixVariable = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_VAR_DEB), Appli.hwnd, dlgprocDebugListeVariables, (LPARAM)NULL);
						}
					else
						{
						::SetWindowPos(CtxtDebugger.hwndChoixVariable,HWND_TOP,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
						}
					if (CtxtDebugger.hwndTraceVariable==NULL)
						{
						CtxtDebugger.hwndTraceVariable = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_VAR_DEB1), Appli.hwnd, dlgprocSurveillanceVariables, (LPARAM)NULL);
						}
					else
						{
						::SetWindowPos(CtxtDebugger.hwndTraceVariable,HWND_TOP,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
						}
          break;
					
					
				case ID_LIGNE:
					{
					if (CtxtDebugger.hwndGoLigne==NULL)
						{
						CtxtDebugger.hwndGoLigne = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_GO_LIGNE), Appli.hwnd, dlgprocAllerALaLigne, (LPARAM)NULL);
						}
					else
						{
						::SetWindowPos(CtxtDebugger.hwndGoLigne,HWND_TOP,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE); //SWP_ZORDER | SWP_ACTIVATE);
						}
					}
          break;
					

				case IDCANCEL:
				case ID_ABANDON:
					{
					::DestroyWindow(hwnd);
					}
          break;
					
				case ID_AFFI:
					{
					if (CtxtDebugger.hwndChoixVariable != NULL)
						SendMessage(CtxtDebugger.hwndChoixVariable, WM_COMMAND, ID_AFFI, 0);
					montre_windows();
					SetTitreWindowCode (" [Break]");
					SendDlgItemMessage (hwnd, ID_CODE, LB_SETCURSEL, CtxtDebugger.point_arret - 1, 0);
					if (CtxtDebugger.hwndTraceVariable != NULL)
						{
						DWORD	ligne = LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETTOPINDEX,0,0));
						DWORD item = LOWORD(SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_GETCOUNT,0,0));
						for (DWORD w=0;w < item;w++)
							{
							MiseAJourTraceVariables (w);
							}
						SendDlgItemMessage (CtxtDebugger.hwndTraceVariable,ID_DEB_LIST_VAR1,LB_SETTOPINDEX, ligne,0);
						if (CtxtDebugger.hwndTraceVaCycle != NULL)
							{
							::PostMessage(CtxtDebugger.hwndTraceVaCycle, WM_COMMAND,IDOK,0);
							}
						}
					}
					break;
        } //switch loword mp1
			}
			break;//case WM_COMMAND
    }

	return mres;
  } // dlgprocDebugCode

// --------------------------------------------------------------------------
// Traitement de la commande menu Execution Pas � Pas
void DebuggerExecutionPasAPas (HWND hwnd)
	{
	CtxtDebugger.hwndCode = LangueCreateDialogParam (MAKEINTRESOURCE(ID_DLG_DEB_EX), NULL, dlgprocDebugCode, (LPARAM)pszPathNameDoc());
	if (CtxtDebugger.On)
		{
			t_param_action  param_action={0,0,""};

		StrCopy (param_action.chaine, pszPathNameDoc());
		ajouter_action (action_init_ressources, &param_action);
		ajouter_action (action_charge_applic_froide_mnu, &param_action);
		ajouter_action (action_charge_lien_dde, &param_action);
		ajouter_action (action_lance_application_mnu, &param_action);
		VarSysExeEcritLog (IdxVarSysExeArretExploitation,FALSE);
		}
	}


// ------------------------ fin ExeDebug.c -----------------------------




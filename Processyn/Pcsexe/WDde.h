//-------------------------------------------------------
// WDde.h
// ex�cution Pcs : Fen�tres de gestion DDE
// JS Win32 08/09/97
//-------------------------------------------------------

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//								Gestion des liens DDE par l'utilisateur
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Traitement de la commande menu Edition lien DDE
void EditionLienDDE (HWND hwnd);

// Traitement de la commande menu Edition copier lien DDE
void EditionCopierLienServeurDDE (HWND hwnd);

// Traitement de la commande menu Edition Coller avec liaison DDE
void EditionCollerLienClientDDE (HWND hwnd);

// Enregistre les liaisons DDE en cours dans un fichier PathAppli.DDE
void SauveLienDde (PCSTR pszNom);

// Charge les liaisons et r�tablis les liaisons DDE � partir d'un fichier PathAppli.DDE
BOOL ChargeLiensDde (void);


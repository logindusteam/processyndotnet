// EnregistreMoniteur.cpp: implementation of the CEnregistreMoniteur class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EnregistreMoniteur.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEnregistreMoniteur::CEnregistreMoniteur()
	{
	InitializeCriticalSection(&m_cs);
	EnterCriticalSection(&m_cs);
	m_bEnregistre = FALSE;
	m_szNomFichier[0]=0;
	m_bErreurEnregistrement = FALSE;
	m_dwErrCodeEnregistrement = NO_ERROR;
	m_dwNbCarMaxFichier = 1000000000; // par d�faut, 1G de log
	m_dwNbCarDansFichier = 0; // Compteur octets fichiers (append)
	LeaveCriticalSection(&m_cs);
	}

CEnregistreMoniteur::~CEnregistreMoniteur()
	{
	EnterCriticalSection(&m_cs);
	m_FichierF.bFerme();
	m_bEnregistre = FALSE;
	LeaveCriticalSection(&m_cs);
	DeleteCriticalSection(&m_cs);
	}

void CEnregistreMoniteur::NommeFichier(PCSTR pszNomFichier)
	{
	EnterCriticalSection(&m_cs);
	#pragma warning(disable : 4996)
	strcpy(m_szNomFichier,pszNomFichier);
	LeaveCriticalSection(&m_cs);
	}

void CEnregistreMoniteur::SetEnregistrement(BOOL bOn)
	{
	EnterCriticalSection(&m_cs);
	if (bOn)
		{
		m_bErreurEnregistrement = !m_FichierF.bOuvre(m_szNomFichier, F_OF_OUVRE_OU_CREE);
		m_dwNbCarDansFichier = 0; // rien dans le fichier

		if (m_bErreurEnregistrement)
			m_dwErrCodeEnregistrement = GetLastError();
		m_bEnregistre = m_FichierF.bOuvert();
		m_FichierF.bPointeFin();
		m_FichierF.bValPointe(&m_dwNbCarDansFichier);

		if (m_dwNbCarDansFichier < m_dwNbCarMaxFichier)
			{
			EnregistreDonnees("Audit;ON");
			}
		}
	else
		{
		if (m_dwNbCarDansFichier < m_dwNbCarMaxFichier)
			{
			EnregistreDonnees("Audit;OFF");
			}
		m_bErreurEnregistrement = !m_FichierF.bFerme();
		if (m_bErreurEnregistrement)
			m_dwErrCodeEnregistrement = GetLastError();
		m_bEnregistre = m_FichierF.bOuvert();
		}
	LeaveCriticalSection(&m_cs);
	}

void CEnregistreMoniteur::EnregistreDonnees(PCSTR pszDonnees)
	{
	// Fichier ouvert sans erreur
	if ((!m_bErreurEnregistrement) && (m_bEnregistre))
		{
		// oui => enregistre la donn�e

		//Format :date/heure syst�me;Donn�es CrLf
		//Format des donn�es
		char szAudit[500]="";
		_SYSTEMTIME udtHeureSysteme;
		GetSystemTime(&udtHeureSysteme);

		wsprintf(szAudit, "%04u/%02u/%02u %02u:%02u:%02u:%03u;%s\r\n", 
			udtHeureSysteme.wYear,
			udtHeureSysteme.wMonth,
			udtHeureSysteme.wDay,
			udtHeureSysteme.wHour,
			udtHeureSysteme.wMinute,
			udtHeureSysteme.wSecond,
			udtHeureSysteme.wMilliseconds,
			pszDonnees);

		// d�compte les octets � �crire
		EnterCriticalSection(&m_cs);
		m_dwNbCarDansFichier += (DWORD)lstrlen(szAudit);

		// d�passe la limite possible ?
		if (m_dwNbCarDansFichier > m_dwNbCarMaxFichier)
			// oui => arr�te l'enregistrement
			SetEnregistrement(FALSE);
		else
			{
			// non => ajoute � la fin du fichier
			m_FichierF.bPointeFin();
			m_bErreurEnregistrement = !m_FichierF.bEcrireSz(szAudit);
			}
		LeaveCriticalSection(&m_cs);
		}
	}

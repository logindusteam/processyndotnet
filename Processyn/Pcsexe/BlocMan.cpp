#include "stdafx.h"
#include "std.h"
#include "MemMan.h"
#include "UExcept.h"
#include "BlocMan.h"

// Ent�te d'un enregistrement
typedef struct
  {
  DWORD		dwTailleEnr;						// Taille d'un enregistrement
  PBYTE   pEnr;                   // Pointeur sur enregistrement
  } HEADER_ENR;

typedef HEADER_ENR *PHEADER_ENR;

// Structure interne d'un objet HBLOC
typedef struct
  {
  BOOL				bExist;             // Indicateur de cr�ation $$ remplacer par signature
  DWORD				dwTailleEnrDeBase;  // Taille de base d'un enregistrement
  DWORD				dwNbEnr;						// Nombre d'enregistrements
  PHEADER_ENR pHdrEnr;						// Pointeur sur Tableau d'ent�te d'un enregistrement
  } BLOC, *PBLOC;

static const char pszNomModule[] = "BlocMan";

//---------------------------------------------------------------
// Envoie une exception avec deux param�tres (en plus du nom de la fonction)
static void Exception2 (PCSTR	pszNomFonction, DWORD p1, DWORD p2)
	{
	DWORD adwArguments [4] = {(DWORD)pszNomModule, (DWORD)pszNomFonction, p1, p2};

	RaiseException (
		EXCEPTION_BLOCMAN, 				// exception code
		EXCEPTION_NONCONTINUABLE,	// continuable exception flag
    4,												// number of arguments in array
    adwArguments							// address of array of arguments
   );
	}

//---------------------------------------------------------------
// Envoie une exception avec aucun param�tres (en plus du nom de la fonction)
static void Exception0 (PCSTR	pszNomFonction)
	{
	DWORD adwArguments [2] = {(DWORD)pszNomModule, (DWORD)pszNomFonction};

	RaiseException (
		EXCEPTION_BLOCMAN, 				// exception code
		EXCEPTION_NONCONTINUABLE,	// continuable exception flag
    2,												// number of arguments in array
    adwArguments							// address of array of arguments
   );
	}

//---------------------------------------------------------------
// conversions typ�es
static _inline HBLOC HFromPBloc (PBLOC pbloc)	{return (HBLOC)pbloc;}
static _inline PBLOC PFromHBloc (HBLOC hbloc)	{return (PBLOC)hbloc;}

//---------------------------------------------------------------------------
//										FONCTIONS EXPORTEES
//---------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Cr�e un bloc
HBLOC	hBlocCree	(DWORD dwTailleEnr, DWORD dwNbEnr)
	{
	//static char szF[]="hBlocCree";
  DWORD wIdx;
	HBLOC	hRet;

  // Allocation de la structure du bloc
  PBLOC pBloc = (PBLOC)pMemAlloue (sizeof (*pBloc));

	pBloc->bExist = TRUE;
	pBloc->dwTailleEnrDeBase  = dwTailleEnr;
	pBloc->dwNbEnr = dwNbEnr;
	pBloc->pHdrEnr = NULL;

	if (dwNbEnr > 0)
		{
		pBloc->pHdrEnr = (PHEADER_ENR)pMemAlloue (dwNbEnr * sizeof (*pBloc->pHdrEnr));
		for (wIdx = 0; wIdx < dwNbEnr; wIdx++)
			{
			pBloc->pHdrEnr [wIdx].dwTailleEnr = dwTailleEnr;
			pBloc->pHdrEnr [wIdx].pEnr     = (PBYTE)pMemAlloue (dwTailleEnr);
			}
		hRet = HFromPBloc (pBloc);
		}
  return hRet;
	}

// --------------------------------------------------------------------------
// lib�re et d�truit un bloc
void BlocFerme (PHBLOC phbloc)
  {
	static const char szF[]="bBlocFerme";
  PBLOC pBloc = PFromHBloc (*phbloc);
	DWORD     wIdx;

	// Validit� du bloc
  if (!pBloc || (!pBloc->bExist))
		Exception0 (szF);

	// fermeture du bloc
	pBloc->bExist = FALSE;

	for (wIdx = 0; wIdx < pBloc->dwNbEnr; wIdx++)
		{
		MemLibere ((PVOID *)(&pBloc->pHdrEnr [wIdx].pEnr));
		}

	MemLibere ((PVOID *)(&pBloc->pHdrEnr));
	pBloc->dwTailleEnrDeBase  = 0;
	pBloc->dwNbEnr     = 0;
	pBloc->pHdrEnr     = NULL;
  } // bBlocFerme

// --------------------------------------------------------------------------
// Renvoie le nombre d'enregistrements dans ce bloc
DWORD dwBlocNbEnr (HBLOC hbloc)
	{
	static const char szF[]="dwBlocNbEnr";
  PBLOC pBloc = PFromHBloc (hbloc);

	// Validit� du bloc
  if (!pBloc || (!pBloc->bExist))
		Exception0 (szF);

	// renvoie le nombre d'enregistrements du bloc
	return pBloc->dwNbEnr;
	}

// --------------------------------------------------------------------------
// Renvoie la taille de l'enregistrement de base dans ce bloc 
DWORD dwBlocTailleEnrDeBase	(HBLOC hbloc)
	{
	static const char szF[]="dwBlocNbEnr";
  PBLOC pBloc = PFromHBloc (hbloc);

	// Validit� du bloc
  if (!pBloc || (!pBloc->bExist))
		Exception0 (szF);

	// renvoie la taille de base d'un enregistrement du bloc
	return pBloc->dwTailleEnrDeBase;
	}

// --------------------------------------------------------------------------
// Ins�re de 0 � N enregistrements dans ce bloc
// Renvoie l'adresse du premier enr ins�r�
PVOID pBlocInsereEnr (HBLOC hbloc, DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr)
	{
	PVOID pRes;
	static const char szF[]="pBlocInsereEnr";
  PBLOC pBloc = PFromHBloc (hbloc);

	PHEADER_ENR       pHdrEnr;
	DWORD           wNewRecCount;
	DWORD           wRealRecId;
	DWORD           wLastRecId;
	DWORD           wRealRecSize;
	DWORD           wIdx;

	// Validit� du bloc
  if (!pBloc || (!pBloc->bExist))
		Exception0 (szF);


	// V�rification du num�ro d'enr de d�part
	if (FALSE) //$$dwNEnr == BLK_INSERT_END)
		{
		wRealRecId = pBloc->dwNbEnr;
		}
	else
		{
		wRealRecId = dwNEnr;
		if (wRealRecId > pBloc->dwNbEnr)
			{
			Exception0 (szF);
			//$$BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERROR_RECORD_OVER_END, pszBlkRecInsert, "");
			}
		}

	// V�rification du nombre d'enregistrements total
	wNewRecCount = pBloc->dwNbEnr + dwNbEnr;
	if ((wNewRecCount < pBloc->dwNbEnr) || (wNewRecCount < dwNbEnr))
		{
		Exception0 (szF);
		// $$BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERROR_RECORD_OVERFLOW, pszBlkRecInsert, "");
		}

	// V�rification taille Enr
	if (TRUE)//$$ dwTailleEnr == BLK_REC_BASE_SIZE)
		{
		wRealRecSize = pBloc->dwTailleEnrDeBase;
		}
	else
		{
		wRealRecSize = dwTailleEnr;
		if (wRealRecSize < pBloc->dwTailleEnrDeBase)
			{
			Exception0 (szF);
			//$$ BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERROR_RECORD_SIZE, pszBlkRecInsert, "");
			}
		}

	// Insertion proprement dite � faire ?
	if (dwNbEnr > 0)
		{
		// oui => ajoute les entr�es dans la table des pointeurs d'enr
		if (wRealRecId == pBloc->dwNbEnr)
			MemRealloue ((PVOID *)(&pBloc->pHdrEnr), wNewRecCount * sizeof (*pBloc->pHdrEnr));
		else
			MemInsere ((PVOID *)(&pBloc->pHdrEnr), wRealRecId * sizeof (*pBloc->pHdrEnr), dwNbEnr * sizeof (*pBloc->pHdrEnr));
		pBloc->dwNbEnr = wNewRecCount;
		//
		wLastRecId = wRealRecId + dwNbEnr;
		for (wIdx = wRealRecId; wIdx < wLastRecId; wIdx++)
			{
			pHdrEnr = &pBloc->pHdrEnr [wIdx];
			pHdrEnr->dwTailleEnr = wRealRecSize;
			pHdrEnr->pEnr     = (PBYTE)pMemAlloue (wRealRecSize);
			if (pHdrEnr->pEnr == NULL)
				{
				Exception0 (szF);
				//$$BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERROR_MEMORY, pszBlkRecInsert, "");
				}
			}
		pRes = pBloc->pHdrEnr [wRealRecId].pEnr;
		} // if (dwNbEnr > 0)
	else
		pRes = NULL;

	return pRes;
	}

// --------------------------------------------------------------------------
// Supprime de 0 � N enregistrements dans ce bloc
void BlocEnleveEnr (HBLOC hbloc, DWORD dwNEnr, DWORD dwNbEnr);

// --------------------------------------------------------------------------
// Change la taille de 0 � N enregistrements dans ce bloc
PVOID pBlocChangeTailleEnr (HBLOC hbloc, DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr);

// --------------------------------------------------------------------------
// Renvoie la taille d'un enregistrement dans ce bloc
DWORD dwBlocTailleEnr (HBLOC hbloc, DWORD dwNEnr);

// --------------------------------------------------------------------------
// Renvoie l'adresse d'un enregistrement dans ce bloc
PVOID pBlocPointeEnr (HBLOC hbloc, DWORD dwNEnr);

// --------------------------------------------------------------------------
// Copie de 0 � N enregistrements entre deux blocs
void BlocCopieEnr (HBLOC hblocDst, DWORD wRecIdDst, HBLOC hblocSrc, DWORD wRecIdSrc, DWORD dwNbEnr);

// --------------------------------------------------------------------------
// Echange de 0 � N enregistrements entre deux blocs
void BlocEchangeEnr (HBLOC hbloc1, DWORD wRecId1, HBLOC hbloc2, DWORD wRecId2, DWORD dwNbEnr);


// --------------------------------------------------------------------------
// Driv_dq.h
// Gestion Disque
// Une liste de taille limit�e de fichiers ouverts est maintenue
// Les fichiers binaires ont une taille d'enregistrement sp�cifi�e
// Un pointeur de lecture et d'�criture ind�pendants sont maintenus
// A l'ouverture :
//	Fichier d�ja ouverts par ce module : leur handle est r�cup�r� dans l'�tat courant;
//  Fichier non ouverts par ce module et existants : les fichiers sont ouverts avec leur longueur actuelle;
//  Fichier non ouverts par ce module et inexistants : les fichiers sont cr��s avec une longueur nulle.
// --------------------------------------------------------------------------
#ifndef DRIV_DQ_H
#define DRIV_DQ_H

#define DQ_NB_FIC_MAX 50 //$$

DECLARE_HANDLE (HFDQ);
typedef HFDQ * PHFDQ;
#define INVALID_HFDQ	((HFDQ)-1)

//---------------------------------------------------------------------------
// ouvre et ferme ce driver
void init_driv_dq (void);
void termine_driv_dq (void);
void reinit_driv_dq_c (void);

//---------------------------------------------------------------------------
// Fichier binaire : ouverture ou r�cup�ration du handle
UINT uFicOuvre (PHFDQ phfdq, PCSTR nom_fich, DWORD dwTailleEnr);

// Fichier texte : ouverture ou r�cup�ration du handle
UINT uFicTexteOuvre (PHFDQ phfdq, PCSTR nom_fich);

// R�cup�ration du handle d'un fichier d�ja ouvert (avec sa taille d'enr courante)
BOOL bFicOuvert (PHFDQ phfdq, PCSTR nom_fich);

// Fermeture de fichier; *phfdq remis � INVALID_HFDQ si fermeture Ok
UINT uFicFerme (PHFDQ phfdq);

// R�cup�re le nombre courant d'enregistrements du fichier
// !!! Renvoie une erreur syst�me
UINT uFicNbEnr (HFDQ hfdq, DWORD * pdwNbEnr);

// Acc�s direct (sp�cifi� en num�ro d'enr de 1 � N) - pointeurs lecture et �criture ind�pendants.

// Enregistre le num�ro du prochain enr � lire et renvoie une err syst�me
UINT uFicPointeLecture		(HFDQ hfdq, DWORD dwNEnr);
// Recopie le Num�ro du prochain enr lu et renvoie une err syst�me
UINT uFicPointeurLecture	(HFDQ hfdq, DWORD * pdwNEnr);
// Renvoie le num�ro du prochain Enr �crit et renvoie une err syst�me
UINT uFicPointeEcriture		(HFDQ hfdq, DWORD dwNEnr);
// Enregistre le num�ro du prochain enr � �crire et renvoie une err syst�me
UINT uFicPointeurEcriture (HFDQ hfdq, DWORD * pdwNEnr);

// acc�s direct
UINT uFicLireDirect	(HFDQ hfdq, DWORD dwNEnr, PVOID buffer);
UINT uFicEcrireDirect	(HFDQ hfdq, DWORD dwNEnr, PVOID buffer);

// Lecture s�quentielle
// renvoie FALSE si tout n'a pas pu �tre lu
UINT uFicLireTout (HFDQ hfdq, PVOID buffer, DWORD dwNbEnrALire);

// Ecriture s�quentielle
// renvoie FALSE si tout n'a pas pu �tre �crit
UINT uFicEcrireTout (HFDQ hfdq, const void * buffer, DWORD dwNbEnrAEcrire);

// acc�s aux s�quentiel aux fichiers textes
UINT uFicLireLn (HFDQ hfdq, PSTR pszDest, UINT nTailleDest);
UINT uFicEcrireLn  (HFDQ hfdq, PCSTR pszSource);

//---------------------------------------------------------------------------
// manipulations globales de fichier :
BOOL bFicPresent (PCSTR nom_fich);
UINT uFicTaille (PCSTR nom_fich);
UINT uFicEfface (PCSTR nom_fich);
UINT uFicCopie (PCSTR nom_source, PCSTR nom_destination, BOOL merge);
UINT uFicRenomme (PCSTR nom_source, PCSTR nom_destination);

//---------------------------------------------------------------------------
// r�cup�re la valeur d'une variable d'environnement (DOS SET)
BOOL bFicValeurEnv (PCSTR var_env, PSTR valeur);

// test de la vraisemblance d'un nom de fichier
BOOL bFicNomValide (PCSTR pszNom);

// r�cup�re la place libre sur un disque
UINT place_disque_libre (DWORD unite, __int64 * taille);

#endif


/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Auteur  : LM							|
 |   Date    : 27/07/93 						|
 |                                                                      |
 |   Remarques : gestion des entrees et des sorties alarmes             |
 |                                                                      |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "pcs_sys.h"  // variables Systemes
#include "UStr.h"
#include "USem.h"
#include "ITC.h"
#include "Tipe_mbx.h"
#include "MemMan.h"
#include "mem.h"
#include "dicho.h"
#include "threads.h"
#include "srvanmal.h"
#include "UChrono.h"                
#include "tipe_al.h"
#include "CliAnmAl.h"                

#include "scrut_al.h"
#include "DebugDumpVie.h"
#include "Verif.h"
VerifInit;

// Messages re�us par l'animateur et �mis par PCS-EXE
typedef struct
  {
  CITC::ITC_HEADER_MESSAGE     Entete;
  ANM_AL_INFOS_SERVICES  Donnee;
  } t_anm_al_msg_service;


//----------------------------------- Variables Locales
//
static BOOL bDemarrageAl;
static BOOL bOkInitAl;
static DWORD wTypeHoroDatage;
static LONG  lTempoStabiliteEvt;

#define TIMEOUT_DEMARRAGE_AL 30000L

#define AL_INIT_ERR_MBX_CALL           1
#define AL_INIT_ERR_ACK_THREAD         2
#define AL_INIT_ERR_NO_SPACE           3
#define AL_INIT_ERR_MBX_LISTEN         4
#define AL_INIT_ERR_ANM                5

#define AL_THR_SIZE 4096


static CITC ITCServeurReceptionAnmAl;
static CITC ITCServeurEmissionAnmAl;

static t_anm_al_msg_service*pt_msg_emission_al = NULL; //
static HTHREAD hThrdAckAl = NULL;

//-------------------------------------------------------------------------
// thread $$ ? � remplacer par un appel direct � dwMbxAttenteAR
static UINT __stdcall attend_ack_al (void *hThrd)
  {
	DumpVie(attend_ack_al);
  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
  while (bThreadAttenteMessage ((HTHREAD)hThrd, NULL, NULL, NULL, NULL))
    {
    ITCServeurEmissionAnmAl.bAttenteAR (INFINITE);
    ThreadFinTraiteMessage ((HTHREAD)hThrd);
    }
  return uThreadTermine ((HTHREAD)hThrd, 0);
  }

//--------------------------------------------------------------------------
//
static void init_al (void)
  {
  tx_glob_al*pdonneex_glob_al;
  tx_geo_al*pdonneex_geo_al;
  PBOOL plog;
  DWORD       index;
  tx_anm_glob_al*pGlobAl; // beurk !!

  // chargement des valeurs initiales du descripteur AL_xxxx_GLOB
  for (index = 1; index <= AL_NB_VAR_GLOB_AL; index++)
    {
    pdonneex_glob_al = (tx_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_glob_al, index);
    pdonneex_geo_al  = (tx_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_geo_al,  index);
    plog             = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pdonneex_geo_al->wPosEs);
    (*plog) = pdonneex_glob_al->bValInit;
    }

  bDemarrageAl = TRUE;
  bOkInitAl = FALSE;

  // re-beurk !!!
  pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1);
  wTypeHoroDatage = pGlobAl->wTypeHoroDatage;
  lTempoStabiliteEvt = pGlobAl->lTempoStabiliteEvt;
  enleve_bloc (bx_buffer_horodate_al);
  if (wTypeHoroDatage == HORODATAGE_SOURCE)
    {
    cree_bloc (bx_buffer_horodate_al, sizeof (tx_buffer_horodate_al), 0);
    }
  }


//--------------------------------------------------------------------------
void initialise_al(void) // $$ renvoyer une erreur
  {
  DWORD	dwCodeEchec = 0;

  //
	static ITC_TX_RX ITCServeurAnmAl = {NULL, NULL};
	ITCServeurAnmAl.pITCRX = &ITCServeurReceptionAnmAl;
	ITCServeurAnmAl.pITCTX = &ITCServeurEmissionAnmAl;

  pt_msg_emission_al = (t_anm_al_msg_service*)pMemAlloue (sizeof(t_anm_al_msg_service));
  if (pt_msg_emission_al != NULL)
    {
		// Ouvre
    if (ITCServeurReceptionAnmAl.bCreeServeurRX (szTopicReceptionAnmAl))
      {
      // on ouvre la communication avec la reception de l'autre cot�
      if (ITCServeurEmissionAnmAl.bCreeServeurTX (szTopicEmissionAnmAl))
        {
				init_al ();
				// lancement de l'animateur d'alarme Ok ?
				if (bExecuteAnimAlarmes (&ITCServeurAnmAl))
					{
					// oui => : attente de l'�tablissement de la connexion
					if (ITCServeurReceptionAnmAl.bAUnCorrespondant ( TIMEOUT_DEMARRAGE_AL) && 
						ITCServeurEmissionAnmAl.bAUnCorrespondant (TIMEOUT_DEMARRAGE_AL))
						{
						// cr�ation du thread d'attente d'acquitement de l'alarme Ok ?//$$ A am�liorer 
						if (bThreadCree (&hThrdAckAl, attend_ack_al, AL_THR_SIZE) && 
							bThreadSetPriorite (hThrdAckAl, PRIORITE_TIMECRITICAL))
							// oui => pas d'erreur
							dwCodeEchec = 0;
						else
							dwCodeEchec = AL_INIT_ERR_ACK_THREAD;
						}
					else
						dwCodeEchec = AL_INIT_ERR_ANM;
					}
		    else
					dwCodeEchec = AL_INIT_ERR_ANM;
				}
      else
				dwCodeEchec = AL_INIT_ERR_MBX_CALL;
			}
      //MbxSetTimeOutAttenteAR (ITCServeurEmissionAnmAl, MBX_DEF_ACK_TIMEOUT);
    else
      dwCodeEchec = AL_INIT_ERR_MBX_LISTEN;
		}
    //MbxSetTimeOutAttenteAR (ITCServeurReceptionAnmAl, MBX_DEF_ACK_TIMEOUT);
  else
		dwCodeEchec = AL_INIT_ERR_NO_SPACE;

	// L'initialisation a �chou�e ?
	if (dwCodeEchec)
		{
		// oui => lib�re les ressources
		termine_al();
		}

	ITCServeurAnmAl.pITCRX = NULL;
	ITCServeurAnmAl.pITCTX = NULL;
  } // initialise_al

//--------------------------------------------------------------------------
// fermeture de la tache d'animation alarmes
void termine_al (void)
  {
  // ferme les communications
	if (hThrdAckAl)
		bThreadFerme (&hThrdAckAl, 1000);   // $$ ancien commentaire "Attente 1s : ne pas faire destroy car possible blocage dans mbx_receive ack

  ITCServeurEmissionAnmAl.Ferme ();

  ITCServeurReceptionAnmAl.Ferme();

	if (pt_msg_emission_al)
		MemLibere ((PVOID *)(&pt_msg_emission_al));

	// ferme la t�che d'animation d'alarme
	bFinExecuteAnimAlarmes();
  }

//-------------------------------------------------------------------------
//
static void maj_statut_al (DWORD statut_al)
  {
  PX_VAR_SYS	position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, status_al);
  PFLOAT  ads_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (position->dwPosCourEx));
  DWORD w = (DWORD)(*ads_num);
  //w = (w | statut_al);
  w = (w | 0x8000); //$$ V�rifier la valeur
  (*ads_num) = (FLOAT) w;
  }

// --------------------------------------------------------------------------
//                           GESTION DES ENTREES
// --------------------------------------------------------------------------
//
static void traite_message_recu (t_anm_al_notification *pnotif)
  {
  tx_geo_al*	donnees_geo_al = (tx_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_geo_al, pnotif->wCodeRetour);
  BOOL *			plog;
  FLOAT *				pnum;

  donnees_geo_al->bForcage = TRUE;
  switch (donnees_geo_al->wBlocGenre)
    {
    case b_cour_log :
      plog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, donnees_geo_al->wPosEs);
      (*plog) = (pnotif->Valeur).bValLogique;
      break;
    case b_cour_num :
      pnum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, donnees_geo_al->wPosEs);
      (*pnum) = (pnotif->Valeur).rValNumerique;
      break;
    case b_va_systeme : // gestion de status_al
      pnum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, donnees_geo_al->wPosEs);
			//$$ v�rifier
      (*pnum) = (FLOAT) (((DWORD)(*pnum)) | (DWORD)((pnotif->Valeur).rValNumerique));
      break;
    default :
      break;
    }
  }

// --------------------------------------------------------------------------
void  recoit_al (BOOL first_pass)
  {
	// Messages re�us par PCS_EXE et �mis par l'animateur
	typedef struct
		{
		CITC::ITC_HEADER_MESSAGE     Entete;
		t_anm_al_notification  Donnee;
		} t_anm_al_msg_event;

  t_anm_al_msg_event MessageRecu;
	PVOID pMessageRecu = &MessageRecu;
  while (ITCServeurReceptionAnmAl.dwLecture (&pMessageRecu, FALSE, TRUE) == 0)
    {
    switch ((MessageRecu.Entete).IdMessage)
      {
      case ITC_MSG_OUVRE:
				break;
      case ITC_MSG_FERME:
				maj_statut_al (AL_REC_ERR_GET_MBX); // animateur arrete
				break;
      case ITC_MSG_USER:
				traite_message_recu (&(MessageRecu.Donnee));
				break;
      default:
        break;
      }
    }
  }


// ------------------------------------------------------------------------
//                           GESTION DES SORTIES
//-------------------------------------------------------------------------
//
static void reset_buff_al (DWORD *pos_buff, DWORD anm_action)
  {
  pt_msg_emission_al->Donnee.NbServices = 0;
  pt_msg_emission_al->Donnee.TypeService = anm_action;
  (*pos_buff) = 0;
  }

//-------------------------------------------------------------------------
// Envoi d'un message � l'animateur d'alarme
static BOOL envoi_message_mbx (DWORD *pos_buff, BOOL avec_ack)
  {
  DWORD    code_ret;
  BOOL ok_envoi = FALSE;

  if ((code_ret = ITCServeurEmissionAnmAl.dwEcriture (&pt_msg_emission_al->Entete,
                           sizeof(CITC::ITC_HEADER_MESSAGE) +    // ent�te
                           sizeof(DWORD) + sizeof(DWORD) + // TypeService + NbServices
                           (*pos_buff),                    // buffer
                           avec_ack                        // pour retour
                           )) == 0)
    {
    ok_envoi  = TRUE;
    bOkInitAl = TRUE;
    }
  else
    {
    if (code_ret == ITC_ERR_ITC_NOT_COMM_STATE)
      {
      maj_statut_al (AL_MBX_ERR_CLOSE);
      }
    else
      {
      maj_statut_al (AL_SEND_ERR_PUT_MBX);
      }
    }
  return (ok_envoi);
  }

//-------------------------------------------------------------------------
//
static void bEnvoieBufExistantSiNecessaire (DWORD *pdwPosBuff, DWORD taille, DWORD anm_action)
  {
  if (((*pdwPosBuff) + taille) >= ANM_AL_MAX_BUFF_SERVICES)
    {
    envoi_message_mbx (pdwPosBuff,FALSE);
    reset_buff_al (pdwPosBuff, anm_action);
    }
  }

//-------------------------------------------------------------------------
//
static void add_buff_handle_al (DWORD *pos_buff, HANDLE_ANM_AL anm_hdl)
  {
  PBYTE ptr_buff = (pt_msg_emission_al->Donnee).bBufferServices + (*pos_buff);

  (*(HANDLE_ANM_AL*)ptr_buff) = anm_hdl;
  (*pos_buff) = (*pos_buff) + sizeof (HANDLE_ANM_AL);
  pt_msg_emission_al->Donnee.NbServices++;
  }

//-------------------------------------------------------------------------
//
static void add_buff_logique_al (DWORD *pos_buff, BOOL log)
  {
  PBYTE ptr_buff = (pt_msg_emission_al->Donnee).bBufferServices + (*pos_buff);

  (*(PBOOL)ptr_buff) = log;
  (*pos_buff) = (*pos_buff) + sizeof(BOOL);
  }

//-------------------------------------------------------------------------
//
static void add_buff_numerique_al (DWORD *pos_buff, FLOAT num)
  {
  PBYTE ptr_buff = (pt_msg_emission_al->Donnee).bBufferServices + (*pos_buff);

  (*(PFLOAT)ptr_buff) = num;
  (*pos_buff) = (*pos_buff) + sizeof(FLOAT);
  }

//-------------------------------------------------------------------------
//
static void add_buff_word_al (DWORD *pos_buff, DWORD num)
  {
  PBYTE ptr_buff = (pt_msg_emission_al->Donnee).bBufferServices + (*pos_buff);

  (*(PDWORD)ptr_buff) = num;
  (*pos_buff) = (*pos_buff) + sizeof(DWORD);
  }

//-------------------------------------------------------------------------
//
static void add_buff_message_al (DWORD *pos_buff, char *message)
  {
  PBYTE ptr_buff = (pt_msg_emission_al->Donnee).bBufferServices + (*pos_buff);

  StrCopy ((PSTR)ptr_buff, message);
  (*pos_buff) = (*pos_buff) + StrLength(message) + 1;
  }

//-------------------- envoi des valeurs de bx_geo_al
//
static void test_add_buff_geo_al (DWORD *pos_buff, DWORD anm_action, BOOL test_old)
  {
  PBOOL    plog;
  PBOOL    pmemlog;
  FLOAT        *pnum;
  FLOAT        *pmemnum;
  BOOL     a_envoyer;
  DWORD        index;
  tx_geo_al  *pdonneex_geo_al;

  for (index = 1; index <= AL_NB_VAR_GEO_AL; index++)
    {
    pdonneex_geo_al = (tx_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_geo_al, index);
    switch (pdonneex_geo_al->wBlocGenre)
      {
      case b_cour_log :
        plog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pdonneex_geo_al->wPosEs);
        if (test_old)
          {
          if (pdonneex_geo_al->bForcage)
            {
            pdonneex_geo_al->bForcage = FALSE;
            a_envoyer = TRUE;
            }
          else
            {
            pmemlog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, pdonneex_geo_al->wPosEs);
            a_envoyer = (BOOL)((*plog) != (*pmemlog));
            }
          }
        else
          { // pas test_old
          a_envoyer = TRUE;
          }
        if (a_envoyer)
          {
          bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HANDLE_ANM_AL) + sizeof(BOOL), anm_action);
          add_buff_handle_al  (pos_buff, pdonneex_geo_al->handle_al);
          add_buff_logique_al (pos_buff, (*plog));
          }
        break;

      case b_cour_num :
        if ((index == AL_CPTR_GLOB) ||
            ((index >= (AL_CPTR_GRP + 0)) && (index <= (AL_CPTR_GRP + NB_GROUPE_MAX_AL - 1))))
          { // on n'envoie pas les compteurs d'alarmes apparues
          } // variables d'entrees
        else
          {
          pnum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pdonneex_geo_al->wPosEs);
          if (test_old)
            {
            if (pdonneex_geo_al->bForcage)
              {
              pdonneex_geo_al->bForcage = FALSE;
              a_envoyer = TRUE;
              }
            else
              {
              pmemnum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pdonneex_geo_al->wPosEs);
              a_envoyer = (BOOL)((*pnum) != (*pmemnum));
              }
            }
          else
            { // pas test_old
            a_envoyer = TRUE;
            }
          if (a_envoyer)
            {
            bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HANDLE_ANM_AL) + sizeof(FLOAT), anm_action);
            add_buff_handle_al  (pos_buff, pdonneex_geo_al->handle_al);
            add_buff_numerique_al (pos_buff, (*pnum));
            }
          }
        break;

      case b_va_systeme : // on n'envoie pas le statut
        break;

      default :
        break;
      } // switch
    } // for
  }

//--------------- envoi de la raz des compteurs d'alarmes apparues
// et de l'acquittement global : ces var. st. remises � B automatiquement
//
static void test_add_buff_raz_al (DWORD *pos_buff, DWORD anm_action)
  {
  DWORD       index;
  PX_VAR_SYS	position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, al_raz_cptr_glob);
  BOOL      * ads_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (position->dwPosCourEx));
  tx_services_speciaux_al*pdonneex_bx_srv_spec;

  if (*ads_log)
    {
    (*ads_log) = FALSE;
    bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HANDLE_ANM_AL) + sizeof(DWORD), anm_action);
    pdonneex_bx_srv_spec = (tx_services_speciaux_al*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_al, PTR_SRV_SPEC_AL_RAZ);
    add_buff_handle_al (pos_buff, pdonneex_bx_srv_spec->handle_al);
    add_buff_word_al (pos_buff, ANM_AL_RAZ_GLOB);
    }

  for (index = 1; index <= NB_GROUPE_MAX_AL; index++)
    {
    position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, al_raz_cptr_grp);
    ads_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (position->dwPosCourEx) + index - 1);
    if (*ads_log)
      {
      (*ads_log) = FALSE;
      bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HANDLE_ANM_AL) + sizeof(DWORD) + sizeof(FLOAT), anm_action);
      pdonneex_bx_srv_spec = (tx_services_speciaux_al*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_al, PTR_SRV_SPEC_AL_RAZ);
      add_buff_handle_al (pos_buff, pdonneex_bx_srv_spec->handle_al);
      add_buff_word_al (pos_buff, ANM_AL_RAZ_GRP);
      add_buff_numerique_al (pos_buff, (FLOAT)index);
      }
    }

  position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, al_acq_glob);
  ads_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (position->dwPosCourEx));
  if (*ads_log)
    {
    (*ads_log) = FALSE;
    bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HANDLE_ANM_AL) + sizeof(DWORD), anm_action);
    pdonneex_bx_srv_spec = (tx_services_speciaux_al*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_al, PTR_SRV_SPEC_AL_RAZ);
    add_buff_handle_al (pos_buff, pdonneex_bx_srv_spec->handle_al);
    add_buff_word_al (pos_buff, ANM_AL_ACQ_GLOB);
    }
  }

//-------------------------------------------------------------------------
//
static void add_buff_ack_al (DWORD *pos_buff, DWORD anm_action)
  {
  tx_services_speciaux_al *pdonneex_bx_srv_spec;

  bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HANDLE_ANM_AL), anm_action);
  pdonneex_bx_srv_spec = (tx_services_speciaux_al*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_al, PTR_SRV_SPEC_AL_ACK);
  add_buff_handle_al (pos_buff, pdonneex_bx_srv_spec->handle_al);
  }

//-------------------------------------------------------------------------
//
static void add_buff_fin_al (DWORD *pos_buff, DWORD anm_action)
  {
  tx_services_speciaux_al *pdonneex_bx_srv_spec;

  bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HANDLE_ANM_AL), anm_action);
  pdonneex_bx_srv_spec = (tx_services_speciaux_al*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_al, PTR_SRV_SPEC_AL_FIN);
  add_buff_handle_al (pos_buff, pdonneex_bx_srv_spec->handle_al);
  }

//------------------------------- renvoie la valeur des variables geo_al
//
static void get_valeur_geo_al (DWORD p_geo_al, DWORD wBlocGenre,
                               BOOL *log, FLOAT *num)
  {
  tx_geo_al *pdonneex_geo_al;
  BOOL   *plog;
  FLOAT       *pnum;

  pdonneex_geo_al = (tx_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_geo_al, p_geo_al);
  switch (wBlocGenre)
    {
    case b_cour_log:
      plog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pdonneex_geo_al->wPosEs);
      (*log) = (*plog);
      break;
    case bva_mem_log:
      plog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, pdonneex_geo_al->wPosEs);
      (*log) = (*plog);
      break;
    case b_cour_num:
      pnum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pdonneex_geo_al->wPosEs);
      (*num) = (*pnum);
      break;
    case bva_mem_num:
      pnum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pdonneex_geo_al->wPosEs);
      (*num) = (*pnum);
      break;
    default:
      break;
    } // switch
  }

//------------------------------- calcul taille buffer d'emission
//
static DWORD wTailleEmission (DWORD *pos_buff, DWORD anm_action, DWORD wNumeroElement)
  {
  DWORD            wTailleRetour = 0;
  DWORD            index_variable;
  tx_variable_al*pdonneex_variable_al;
  tx_element_al*pdonneex_element_al = (tx_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_element_al, wNumeroElement);

  for (index_variable = pdonneex_element_al->wPosPremVariable;
       index_variable < pdonneex_element_al->wPosPremVariable + pdonneex_element_al->wNbreVariable;
       index_variable++)
    {
    pdonneex_variable_al = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, index_variable);
    switch (pdonneex_variable_al->wBlocGenre)
      {
      case b_cour_log:
        wTailleRetour = wTailleRetour + sizeof(BOOL);
        break;
      case b_cour_num:
      case VA_HORODATAGE_AL:
          wTailleRetour = wTailleRetour + sizeof(FLOAT);
        break;
      case b_cour_mes:
        wTailleRetour = wTailleRetour + 82;  // maximum
        break;
      default:
        break;
      }
    }
  if (wTailleRetour != 0)            // si param�tres du handle message non nuls
    {
    wTailleRetour = wTailleRetour + sizeof (HANDLE_ANM_AL); // taille + handle $$  ?
    bEnvoieBufExistantSiNecessaire (pos_buff, wTailleRetour, anm_action);
    }
  return (wTailleRetour);
  }
//------------------------------- mise dans le buffer d'emission
//
static void push_emission (DWORD *pos_buff, DWORD anm_action, DWORD wNumeroElement)
  {
  DWORD            index_variable;
  DWORD            taille;
  tx_variable_al *pdonneex_variable_al;
  BOOL        *cour_log;
  FLOAT            *cour_num;
  PSTR cour_mes;
  tx_element_al  *pdonneex_element_al;

  taille = wTailleEmission (pos_buff, anm_action, wNumeroElement);
  //-------------------------------------------- envoi du message complet
  if (taille != 0)            // si param�tres du handle message non nuls
    {
    pdonneex_element_al = (tx_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_element_al, wNumeroElement);
    add_buff_handle_al (pos_buff, pdonneex_element_al->handle_al);
    for (index_variable = pdonneex_element_al->wPosPremVariable;
         index_variable < pdonneex_element_al->wPosPremVariable + pdonneex_element_al->wNbreVariable;
         index_variable++)
      {
      pdonneex_variable_al = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, index_variable);
      switch (pdonneex_variable_al->wBlocGenre)
        {
        case b_cour_log:
          cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pdonneex_variable_al->wPosEs);
          add_buff_logique_al (pos_buff, (*cour_log));
          break;

        case b_cour_num:
        case VA_HORODATAGE_AL:
            cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pdonneex_variable_al->wPosEs);
            add_buff_numerique_al (pos_buff, (*cour_num));
          break;

        case b_cour_mes:
          cour_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, pdonneex_variable_al->wPosEs);
          add_buff_message_al (pos_buff, cour_mes);
          break;
        }
      } // boucle 1� var a nbr_var
    } // taille != 0
  }

//------------------------------- Comparaison dichotomie
//
static int CmpHorodatage (const void *pElement, DWORD wIdxComparaison)
  {
  tx_buffer_horodate_al *pValCour = (tx_buffer_horodate_al *)pElement;
  tx_buffer_horodate_al*pVal = (tx_buffer_horodate_al*)pointe_enr (szVERIFSource, __LINE__, bx_buffer_horodate_al, wIdxComparaison);
  int iRetour;
  DWORD wIndexTab;


  iRetour = 0;
  wIndexTab = 0;
  while ((wIndexTab < NB_VAL_HORODATAGE) && (iRetour == 0))
    {
    if (pValCour->rTabValHorodatage[wIndexTab] != pVal->rTabValHorodatage[wIndexTab])
      {
      iRetour = (int)(pValCour->rTabValHorodatage[wIndexTab] - pVal->rTabValHorodatage[wIndexTab]);
      }
    wIndexTab++;
    }
  return (iRetour);
  }

//------------------------------- mise dans le buffer temporis�
//
static void push_bufferisation (DWORD anm_action, DWORD wNumeroElement)
  {
  tx_buffer_horodate_al *pdonneex_buffer_horodate_al;
  tx_buffer_horodate_al donneex_buffer_horodate_al_cour;
  tx_variable_al *pdonneex_variable_al;
  tx_element_al  *pdonneex_element_al;
  DWORD index_variable;
  DWORD wPosInsertion;
  DWORD wIndexValNum;
  DWORD wIndexValHorodatage;
  BOOL *cour_log;
  FLOAT *cour_num;
  PSTR cour_mes;

  donneex_buffer_horodate_al_cour.wNumeroElement = wNumeroElement;
  donneex_buffer_horodate_al_cour.wAnmAction = anm_action;
  wIndexValNum = 0;
  wIndexValHorodatage = 0;
  pdonneex_element_al = (tx_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_element_al, wNumeroElement);
  for (index_variable = pdonneex_element_al->wPosPremVariable;
       index_variable < pdonneex_element_al->wPosPremVariable + pdonneex_element_al->wNbreVariable;
       index_variable++)
    {
    pdonneex_variable_al = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, index_variable);
    switch (pdonneex_variable_al->wBlocGenre)
      {
      case b_cour_log:
        cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pdonneex_variable_al->wPosEs);
        donneex_buffer_horodate_al_cour.rTabValLogique = (*cour_log);
        break;

      case b_cour_num:
        cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pdonneex_variable_al->wPosEs);
        donneex_buffer_horodate_al_cour.rTabValNumerique [wIndexValNum] = (*cour_num);
        wIndexValNum++;
        break;

      case VA_HORODATAGE_AL:
        cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pdonneex_variable_al->wPosEs);
        donneex_buffer_horodate_al_cour.rTabValHorodatage [wIndexValHorodatage] = (*cour_num);
        wIndexValHorodatage++;
        break;

      case b_cour_mes:
        cour_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, pdonneex_variable_al->wPosEs);
        StrCopy (donneex_buffer_horodate_al_cour.pszTabValMessage, cour_mes);
        break;

      default:
        break;
      }
    }
  trouve_position_dichotomie (&donneex_buffer_horodate_al_cour, 1,
		nb_enregistrements (szVERIFSource, __LINE__, bx_buffer_horodate_al), CmpHorodatage, &wPosInsertion);
  insere_enr (szVERIFSource, __LINE__, 1,bx_buffer_horodate_al, wPosInsertion);
  pdonneex_buffer_horodate_al = (tx_buffer_horodate_al*)pointe_enr (szVERIFSource, __LINE__, bx_buffer_horodate_al, wPosInsertion);
  (*pdonneex_buffer_horodate_al) = donneex_buffer_horodate_al_cour;
  MinuterieLanceMs (&(pdonneex_buffer_horodate_al->lValTempo), NB_MS_PAR_CENTIEME (lTempoStabiliteEvt));
  }

// --------------------------------------------------------------------------
// emission des donn�es bufferris�es
// --------------------------------------------------------------------------
static void pop_bufferisation_push_emission (DWORD *pos_buff)
  {
  tx_buffer_horodate_al *pdonneex_buffer_horodate_al;
  tx_element_al  *pdonneex_element_al;
  tx_variable_al *pdonneex_variable_al;
  DWORD index_variable;
  BOOL bFin;
  DWORD taille;
  DWORD wIndexValNum;
  DWORD wIndexValHorodatage;

  bFin = FALSE;
  while ((nb_enregistrements (szVERIFSource, __LINE__, bx_buffer_horodate_al) > 0) && (!bFin))
    {
    pdonneex_buffer_horodate_al = (tx_buffer_horodate_al*)pointe_enr (szVERIFSource, __LINE__, bx_buffer_horodate_al, 1);
    if (bEcheanceMinuterie (pdonneex_buffer_horodate_al->lValTempo))
      {
      taille = wTailleEmission (pos_buff, pdonneex_buffer_horodate_al->wAnmAction,
                                pdonneex_buffer_horodate_al->wNumeroElement);
      if (taille != 0)
        {
        pdonneex_element_al = (tx_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_element_al, pdonneex_buffer_horodate_al->wNumeroElement);
        add_buff_handle_al (pos_buff, pdonneex_element_al->handle_al);
        wIndexValNum = 0;
        wIndexValHorodatage = 0;
        for (index_variable = pdonneex_element_al->wPosPremVariable;
             index_variable < pdonneex_element_al->wPosPremVariable + pdonneex_element_al->wNbreVariable;
             index_variable++)
          {
          pdonneex_variable_al = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, index_variable);
          switch (pdonneex_variable_al->wBlocGenre)
            {
            case b_cour_log:
              add_buff_logique_al (pos_buff, pdonneex_buffer_horodate_al->rTabValLogique);
              break;

            case b_cour_num:
              add_buff_numerique_al (pos_buff, pdonneex_buffer_horodate_al->rTabValNumerique [wIndexValNum]);
              wIndexValNum++;
              break;

            case VA_HORODATAGE_AL:
              add_buff_numerique_al (pos_buff, pdonneex_buffer_horodate_al->rTabValHorodatage [wIndexValHorodatage]);
              wIndexValHorodatage++;
              break;

            case b_cour_mes:
              add_buff_message_al (pos_buff, pdonneex_buffer_horodate_al->pszTabValMessage);
              break;

            default:
              break;
            }
          } // boucle 1� var a nbr_var
        } // taille != 0
      enleve_enr (szVERIFSource, __LINE__, 1, bx_buffer_horodate_al, 1);
      }
    else  // tempo fini
      {
      bFin = TRUE;
      }
    } // While
  }

// -----------------------------------------------------------------------
// envoi de toutes les variables d'alarmes a l'initialisation
// -----------------------------------------------------------------------
static void send_msg_init_al (DWORD *pos_buff, DWORD anm_action)
  {
  tx_groupe_al*pdonneex_groupe_al;
  tx_priorite_al*pdonneex_priorite_al;
  DWORD            nbr_groupe;
  DWORD            index_groupe;
  DWORD            index_priorite;
  DWORD            index_element;
  DWORD            wNumeroElement;

  if (existe_repere(bx_element_al))
    {
    nbr_groupe = nb_enregistrements (szVERIFSource, __LINE__, bx_groupe_al);
    for (index_groupe = 1; index_groupe <= nbr_groupe; index_groupe++)
      {
      pdonneex_groupe_al = (tx_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_groupe_al, index_groupe);
      for (index_priorite = 1; index_priorite <= pdonneex_groupe_al->wNbrePriorite; index_priorite++)
        {
        pdonneex_priorite_al = (tx_priorite_al*)pointe_enr (szVERIFSource, __LINE__, bx_priorite_al, pdonneex_groupe_al->wPosPremPriorite + index_priorite - 1);

        for (index_element = 1; index_element <= pdonneex_priorite_al->wNbreAlarme; index_element++)
          {
          wNumeroElement = pdonneex_priorite_al->wPosPremAlarme + index_element - 1;
          push_emission (pos_buff, anm_action, wNumeroElement);
          }
        }
      }
    }
  }

// --------------------------------------------------------------------------
// envoi des variables d'alarmes
//
static void send_msg_al (DWORD *pos_buff, DWORD anm_action, BOOL test_old)
  {
  tx_groupe_al*pdonneex_groupe_al;
  tx_priorite_al *pdonneex_priorite_al;
  tx_element_al  *pdonneex_element_al;
  tx_variable_al *pdonneex_variable_al;
  DWORD            nbr_groupe;
  BOOL         log;
  FLOAT             num;
  FLOAT             priorite_courante;
  BOOL         priorite_unique;
  BOOL         test_priorite;
  DWORD            index_groupe;
  DWORD            index_priorite;
  DWORD            index_element;
  DWORD            index_variable;
  DWORD            wNumeroElement;
  BOOL         a_envoyer_tout;
  BOOL         a_envoyer_seul;
  PBOOL        cour_log;
	PBOOL				mem_log;
  PFLOAT      cour_num;
	PFLOAT	mem_num;

  a_envoyer_tout = (BOOL)!test_old;
  if (existe_repere(bx_element_al))
    {
    get_valeur_geo_al (AL_SURV_GLOB, b_cour_log, &log, &num);
    if (log)
      { // surveillance globale
      get_valeur_geo_al (AL_SURV_GLOB, bva_mem_log, &log, &num);
      if (!a_envoyer_tout)
        { // surv_glob vient de passer a haut
        a_envoyer_tout = (BOOL)!log;
        }

      nbr_groupe = nb_enregistrements (szVERIFSource, __LINE__, bx_groupe_al);
      for (index_groupe = 1; index_groupe <= nbr_groupe; index_groupe++)
        {
        get_valeur_geo_al (AL_SURV_GRP + index_groupe - 1, b_cour_log, &log, &num);
        if (log)
          { // surveillance groupe
          get_valeur_geo_al (AL_SURV_GRP + index_groupe - 1, bva_mem_log, &log, &num);
          if (!a_envoyer_tout)
            { // surv_grp vient de passer a haut
            a_envoyer_tout = (BOOL)!log;
            }
          pdonneex_groupe_al = (tx_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_groupe_al, index_groupe);
          get_valeur_geo_al (AL_PRIORITE_UNIQUE, b_cour_log, &priorite_unique, &num);
          for (index_priorite = 1; index_priorite <= pdonneex_groupe_al->wNbrePriorite; index_priorite++)
            {
            pdonneex_priorite_al = (tx_priorite_al*)pointe_enr (szVERIFSource, __LINE__, bx_priorite_al, pdonneex_groupe_al->wPosPremPriorite + index_priorite - 1);
            get_valeur_geo_al (AL_SURV_PRI + index_groupe - 1, b_cour_num, &log, &priorite_courante);
            get_valeur_geo_al (AL_SURV_PRI + index_groupe - 1, bva_mem_num, &log, &num);
            if (priorite_unique)
              { // pour Lagarde
              test_priorite = (BOOL)(pdonneex_priorite_al->wNumeroPriorite == (DWORD)priorite_courante);
              if ((BOOL)(!a_envoyer_tout) && test_priorite)
                { // surv_pri vient d'etre validee
                a_envoyer_tout = (BOOL)(priorite_courante != num);
                }
              }
            else
              {
              test_priorite = (BOOL)(pdonneex_priorite_al->wNumeroPriorite <= (DWORD)priorite_courante);
              if ((BOOL)(!a_envoyer_tout) && test_priorite)
                { // surv_pri vient d'etre validee
                a_envoyer_tout = (BOOL)((num < pdonneex_priorite_al->wNumeroPriorite) &&
                                      (priorite_courante >= pdonneex_priorite_al->wNumeroPriorite));
                }
              }
            if (test_priorite)
              { // surveillance priorite
              for (index_element = 1; index_element <= pdonneex_priorite_al->wNbreAlarme; index_element++)
                {
                wNumeroElement = pdonneex_priorite_al->wPosPremAlarme + index_element - 1;
                pdonneex_element_al = (tx_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_element_al, wNumeroElement);
                a_envoyer_seul = FALSE;
                index_variable = pdonneex_element_al->wPosPremVariable;
                while ((index_variable < pdonneex_element_al->wPosPremVariable + pdonneex_element_al->wNbreVariable)
                      && (!a_envoyer_tout) && (!a_envoyer_seul))
                  {
                  pdonneex_variable_al = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, index_variable);
                  switch (pdonneex_variable_al->wBlocGenre)
                    {
                    case b_cour_log:
                      cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pdonneex_variable_al->wPosEs);
                      mem_log  = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, pdonneex_variable_al->wPosEs);
                      if (pdonneex_variable_al->bTestStable)
                        {
                        a_envoyer_seul = (BOOL) ((*cour_log) == (*mem_log));
                        }
                      else
                        {
                        a_envoyer_seul = (BOOL) ((*cour_log) != (*mem_log));
                        }
                      break;

                    case b_cour_num:
                      cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pdonneex_variable_al->wPosEs);
                      mem_num  = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pdonneex_variable_al->wPosEs);
                      a_envoyer_seul = (BOOL) ((*cour_num) != (*mem_num));
                      break;

                    case b_cour_mes:
                      break;

                    case VA_HORODATAGE_AL:
                      break;

                    default:
                      break;
                    }
                  index_variable++;
                  } //while

                if ((a_envoyer_tout) || (a_envoyer_seul))
                  {
                  if (wTypeHoroDatage == HORODATAGE_PC)
                    {
                    push_emission (pos_buff, anm_action, wNumeroElement);
                    }
                  else
                    {
                    push_bufferisation (anm_action, wNumeroElement);
                    }
                  }
                }  // boucle 1�elt a nbr_elt
              } // surv_pri
            } // for priorite
          } // surv_grp
        } // for groupe
      } // surv_glob
    if (wTypeHoroDatage == HORODATAGE_SOURCE)
      {
      pop_bufferisation_push_emission (pos_buff);
      }
    } // existe_repere
  }


//-------------------------------------------------------------------------
//-------------------------- emission des sorties -------------------------
//-------------------------------------------------------------------------
void  sorties_al (BOOL first_pass)
  {
  DWORD pos_buff;
  DWORD wError;

  //
  //------------------------------------------------------------ Canal Ouvert
  //
  if (ITCServeurEmissionAnmAl.bAUnCorrespondant (0))
    {
    if (bDemarrageAl)
      {
      // ----------------------------------------------------------
      // initialisation des variables des blocs bx_anm_cour_XXX_al
      // ----------------------------------------------------------
      reset_buff_al (&pos_buff, ANM_AL_TYPE_SRV_INIT);
      send_msg_init_al (&pos_buff, ANM_AL_TYPE_SRV_INIT);
      if (pos_buff != 0)
        {
        envoi_message_mbx (&pos_buff,FALSE);
        }
      }

    //--------------------------------------------------------- 1� Passage
    //
    if (bOkInitAl)
      {
      if (first_pass || bDemarrageAl)
        {
        // -------------------------------------------------------------
        // envoi des variables systemes et de toutes les valeurs alarmes
        // -------------------------------------------------------------

        bDemarrageAl = FALSE;
        //
        reset_buff_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC);
        //
        test_add_buff_geo_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC, FALSE);
        //
        test_add_buff_raz_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC);
        //
        send_msg_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC, FALSE);

        //
        //---------------------------------- Test si Existe Message � Emettre
        //
        if (pos_buff != 0)
          {
          //
          // On attend ack pour un eventuel forcage de premier passage dans code
          // Au vrai premier passage le wait sort de suite car pas de fct async en cours
          bThreadAttenteFinTraiteMessage (hThrdAckAl, AL_TIMEOUT_ACK);

          add_buff_fin_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC);
          add_buff_ack_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC);
          if (envoi_message_mbx (&pos_buff,TRUE))
            {
            // On lance la reception ack, qui sera teste aux cycles suivants...
            ThreadEnvoiMessage (hThrdAckAl, THRD_FCT_EXEC, NULL, NULL, &wError);
            }
          //
          } //--- fin Existe Message (pos_buff != 0)
        } //--- fin 1� Passage ou demarrage
      //------------------------------------------------------ Pas 1� Passage
      else
        {
        //------------------------------ Attente Acquittement cycle pr�c�dent
        //
        if (bThreadAttenteFinTraiteMessage (hThrdAckAl, AL_TIMEOUT_ACK))
          {
          //---------------------------------------- On a recu l'acquittement
          //
          reset_buff_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC);
          //
          test_add_buff_geo_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC, TRUE);
          //
          test_add_buff_raz_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC);
          //
          send_msg_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC, TRUE);
          //
          //-------------------------------- Test si Existe Message � Emettre
          //
          if (pos_buff != 0)
            {
            //---------------------------------------------------------------
            // On lance la reception ack, qui sera teste aux cycles suivants...
            //---------------------------------------------------------------
            add_buff_fin_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC);
            add_buff_ack_al (&pos_buff, ANM_AL_TYPE_SRV_EXEC);
            if (envoi_message_mbx (&pos_buff,TRUE))
              {
              // On lance la reception ack, qui sera teste aux cycles suivants...
              ThreadEnvoiMessage (hThrdAckAl, THRD_FCT_EXEC, NULL, NULL, &wError);
              }
            else
              {
              maj_statut_al (AL_SEND_ERR_PUT_MBX);
              }
            } // pos_buff != 0
          } // acquittement
        else
          { // time_out ack d�pass�
          maj_statut_al (AL_SEND_TIMEOUT);
          }
        } // pas 1� passage
      } // bOkInitAl
    } // le canal est ouvert
  }

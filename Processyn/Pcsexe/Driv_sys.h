// -----------------------------------------------------------------------
// Driv_sys.h
// Acc�s en modification de quelques variables syst�me
// JS win32 12/10/97
// -----------------------------------------------------------------------

#define IdxVarSysExeArretExploitation             0
#define IdxVarSysExeRetourSysteme                 1
#define IdxVarSysExeApplicSuivante                2
#define IdxVarSysExeFenVisible              3
#define IdxVarSysExeFenX                    4
#define IdxVarSysExeFenY                    5
#define IdxVarSysExeFenCx                   6
#define IdxVarSysExeFenCy                   7
#define IdxVarSysExeFenEtat                 8
#define IdxVarSysExeFenImpression           9
#define IdxVarSysExeFenNomDocument        10
#define IdxVarSysExeEffaceVisu                   11
#define IdxVarSysExeAlarmeVisible                    12
#define IdxVarSysExeFenUser                13

#define NB_VAR  14

// -----------------------------------------------------------------------
// Initialisation des structures de modification des variables syst�me
void VarSysExeInit (void);

// --------------------------------------------------------------------------
void VarSysExeEcritLog (DWORD dwIdVarSys, BOOL SysValue);

// --------------------------------------------------------------------------
void VarSysExeEcritZone (DWORD dwIdVarSys, void *ptr_zone, DWORD taille);

// --------------------------------------------------------------------------
BOOL bVarSysExeEstModifiee (DWORD dwIdVarSys);

// --------------------------------------------------------------------------
DWORD dwPosVarSysExe (DWORD dwIdVarSys);

// --------------------------------------------------------------------------
DWORD dwVarSysExeGetType (DWORD dwIdVarSys);

// --------------------------------------------------------------------------
DWORD dwVarSysExeGetSens (DWORD dwIdVarSys);

// --------------------------------------------------------------------------
BOOL bVarSysExeReadLog (DWORD dwIdVarSys);

// --------------------------------------------------------------------------
DWORD dwVarSysExeReadNum (DWORD dwIdVarSys);

// --------------------------------------------------------------------------
PCSTR pszVarSysExeReadMess (DWORD dwIdVarSys);
  
// --------------------------------------------------------------------------
DWORD dwVarSysExeReadPtr (DWORD dwIdVarSys, void **ptr_zone);

// -----------------------------------------------------------------------
void  initialise_num_page_dlg (DWORD *index,BOOL *visible,char *chaine);






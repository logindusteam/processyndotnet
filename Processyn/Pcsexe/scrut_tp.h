// --------------------------------------------------------------------------
// interface du driver horloge et chronometre
// --------------------------------------------------------------------------
void scrute_horo (void);              // ---- Maj Date + Heure
void  scrute_cn (BOOL bPremier);  // ---- Maj Metro + Chrono

// --------------------------------------------------------------------------
void lance_chronometre   (DWORD index_cn);
void relance_chronometre (DWORD index_cn);
void arrete_chronometre  (DWORD index_cn);
void raz_chronometre     (DWORD index_cn);
void lance_metronome     (DWORD index_cn);
void arrete_metronome    (DWORD index_cn);

// --------------------------------------------------------------------------
DWORD index_spec_metro  (DWORD index_dans_bd);
DWORD index_spec_chrono (DWORD index_dans_bd);

//-------------------------------------------------------
//	WExe.c
// Fenetre principale de l'ex�cution Pcs
// JS Win32 11/06/97
//-------------------------------------------------------
#include "stdafx.h"
#include "USem.h"
#include "UStr.h"
#include "Appli.h"
#include "Threads.h"               // Multi-T�ches.........thrd_xxx ()
#include "WScrExe.h"               // ..................Scrxx ()
#include "ExeOuvFic.h"
#include "ExeDebug.h"
#include "WAide.h"
#include "FMan.h"
#include "ActionEx.h"
#include "DocMan.h"
#include "PathMan.h"
#include "Mem.h"
#include "MenuMan.h"
#include "lng_res.h"
#include "Tipe.h"
#include "Tipe_de.h"
#include "WDde.h"
#include "IdLngLng.h"
#include "pcs_sys.h"
#include "LireLng.h"
#include "pcsdlg.h"
#include "appliex.h"
#include "pcsdlgex.h"
#include "WScrExe.h"
#include "IdExeLng.h"
#include "IdExe.h"
#include "UChrono.h"                
#include "dongle.h"
#include "VersMan.h"
#include "Driv_sys.h"
#include "DebugDumpVie.h"
#include "Verif.h"
#include "WExe.h"
VerifInit;


#define  WM_USER_DESTROY WM_USER+10 // $$ centraliser
#define	 WM_SET_TITRE		 WM_USER+11

// positions des sous menus
#define SUB_MENU_FICHIER		0
#define SUB_MENU_EDITION		1
#define SUB_MENU_EXECUTION	2
#define SUB_MENU_PAGES			3
#define SUB_MENU_AIDE				4

static CHAR szClasseExe [] = "ClasseFenExe";
static HSEM hSemAckErreurMem = 0;
static BOOL	bFenetreVisible = TRUE;

// $$ d�placer
static char pszTitre [100 + MAX_PATH];
static BOOL	bAlarmeCodeEnCours = FALSE;

// Table d'acc�l�rateurs export�e
HACCEL hAccelTouchesFonctions = NULL;

// --------------------------------------------------------------------------
void SetAlarmeCode (BOOL set)
  {
	// changement de l'�tat d'alarme code ?
	if (bAlarmeCodeEnCours != set)
		{
		// oui => m�morise son nouvel �tat
		bAlarmeCodeEnCours = set;

		// apparition de l'alarme ?
		if (set)
			{
			// oui => Flash de la fen�tre, son, attente et nouveau flash
			::FlashWindow (Appli.hwnd, TRUE); // inversion

			// Son syst�me exclamation
			MessageBeep (MB_ICONEXCLAMATION);

			Sleep(50);
			::FlashWindow (Appli.hwnd, TRUE); // puis nouvelle inversion
			}
		else
			// non => restaure la fen�tre dans son �tat initial
			::FlashWindow (Appli.hwnd, FALSE);

		// Mise � jour menu
		MnuTitreAJourRun ();
		}
  } // SetAlarmeCode

// --------------------------------------------------------------------------
BOOL bAlarmeCode (void)
  {
  return bAlarmeCodeEnCours;
  }

// --------------------------------------------------------------------------
void SetMenuExe (BOOL bVisible)
  {
	//$$ doit interdire l'acc�s au menu � l'utilisateur
  bFenetreVisible = bVisible;
	::ShowWindow (Appli.hwnd, bVisible ? SW_SHOW : SW_HIDE);
	// $$ pas a sa place ?
  CtxtDebugger.On = FALSE;
  }
// --------------------------------------------------------------------------
BOOL ExisteMenu (void)
  {
  return bFenetreVisible;
  }

// -----------------------------------------------------------------------
// maj pages alarmes valide
// Retour TRUE si le message a �t� trait�, FALSE si non
// -----------------------------------------------------------------------
static BOOL MnuOnMenuSelect(HWND hwnd, PUINT pmsg, WPARAM * pmp1, LPARAM * pmp2)
  {
  BOOL   bErreur = FALSE;
	/*$$
  int     cmd_menu;
  UINT     etat;
  PX_VAR_SYS pwPosition;
  BOOL   *ptr_log;
  BOOL   gris;

  bErreur;
  gris = FALSE;

  cmd_menu = LOWORD (*pmp1);
  etat = GetMenuState (::GetMenu (Appli.hwnd), cmd_menu, MF_BYCOMMAND);
		//LOWORD(::SendMessage (hmenuAppli, MM_QUERYITEMATTR, MAKEWPARAM (cmd_menu, TRUE), MIA_DISABLED));

  if ((etat != (UINT)-1) && (etat & MF_DISABLED))
    gris = TRUE;
  if (!gris)
    {
    switch (cmd_menu)
      {
      case SUB_MENU_PAGES:
        pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme,al_visible);
        ptr_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, *pwPosition);
        if (*ptr_log)
          {
          MenuAppliCocheItem(CMD_MENU_ALARMES,TRUE);
          }
        else
          {
          MenuAppliCocheItem(CMD_MENU_ALARMES,FALSE);
          }
        bErreur = TRUE;
        break;

      }
    }
	*/
  // Valeurs retourn�es si le message a �t� trait�
  return bErreur;
  }


//----------------------------------------------------
// Mise � jour du titre de la fen�tre (hors ex�cution)
void MnuTitreAJour(void)
  {
  char pszMessInfo [255];

  bLngLireInformation (c_inf_titre_pcs, pszMessInfo);
  StrCopy   (pszTitre, pszMessInfo);
  StrConcat (pszTitre, " ");
  StrConcat (pszTitre, pszNameDoc());
	//$$ Attention risque d'�crasement du texte pendant la transmission du ::PostMessage
  ::PostMessage(Appli.hwnd, WM_SET_TITRE, (WPARAM)pszTitre, 0);
  }

//----------------------------------------------------
// Mise � jour du titre de la fen�tre (en cours d'ex�cution)
void MnuTitreAJourRun(void)
  {
  PX_VAR_SYS	pPosition;
  PFLOAT	 pValReel;
  FLOAT	 StatusCode;
  FLOAT	 MasqueStatusCode;
  DWORD	 StatusCodeMasque;
  char	 pszMessInfo [255];
  char	 pszTampon [16];

  bLngLireInformation (c_inf_titre_pcs, pszMessInfo);
  StrCopy   (pszTitre, pszMessInfo);
  StrConcat (pszTitre, " ");
  StrConcat (pszTitre, pszNameDoc());
  StrConcat (pszTitre, " ");
  bLngLireInformation (c_inf_en_cours, pszMessInfo);
  StrConcat (pszTitre, pszMessInfo);

  pPosition  = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, VA_SYS_STATUS_CODE);
  pValReel   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,  (pPosition->dwPosCourEx));
  StatusCode = (*pValReel);
  pPosition  = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, VA_SYS_MASQUE_STATUS_CODE);
  pValReel   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,  (pPosition->dwPosCourEx));
  MasqueStatusCode = (*pValReel);
  StatusCodeMasque = ((DWORD)StatusCode) & ((DWORD)MasqueStatusCode);
  if (StatusCodeMasque != 0)
    {
    StrConcat (pszTitre, "     (");
    StrDWordToStrBase (pszTampon, StatusCodeMasque, 2);
    while (StrLength (pszTampon) < 16)
      {
      StrInsertChar (pszTampon, '0', 0);
      }
    StrConcat (pszTitre, pszTampon);
    StrConcat (pszTitre, ")");
    }

  ::PostMessage(Appli.hwnd, WM_SET_TITRE, (WPARAM)pszTitre, 0);
  }

//--------------------------------------------------------------
static void MnuEtatDebugger(void)
  {
  if (bInfoDebugPresente())
    {
    MenuAppliGriseItem (CMD_MENU_DEBUGGER,FALSE);
    }
  else
    {
    MenuAppliGriseItem (CMD_MENU_DEBUGGER,TRUE);
    }
  }

//--------------------------------------------------------------
static void MnuEtatChaud(void)
  {
  char psz[MAX_PATH];

	pszCopiePathNameDocExt (psz, MAX_PATH, EXTENSION_A_CHAUD);
  if (CFman::bFAcces (psz, CFman::OF_OUVRE_LECTURE_SEULE))
    {
    MenuAppliGriseItem (CMD_MENU_LANCER_APPLI_CH,FALSE);
    }
  else
    {
    MenuAppliGriseItem(CMD_MENU_LANCER_APPLI_CH,TRUE);
    }
  }

//--------------------------------------------------------------
void MnuEtatCharge(void)
  {
  if (!StrIsNull(pszNameDoc()))
    {
	  MenuAppliGriseSubMenu(SUB_MENU_FICHIER,FALSE);
    MenuAppliGriseSubMenu(SUB_MENU_EDITION,FALSE);
    MenuAppliGriseSubMenu(SUB_MENU_EXECUTION,FALSE);
	  MenuAppliGriseSubMenu(SUB_MENU_PAGES,TRUE);
		MenuAppliGriseSubMenu(SUB_MENU_AIDE,FALSE);
		//
	  MenuAppliGriseItem(CMD_MENU_OUVRE,FALSE);
    MenuAppliGriseItem(CMD_MENU_SAUVE,FALSE);
    MenuAppliGriseItem(CMD_MENU_SAUVE_SOUS,FALSE);
    MenuAppliGriseItem(CMD_MENU_IMPRIME,TRUE);
    MenuAppliGriseItem(CMD_MENU_ARRETER_APPLI,TRUE);
    MenuAppliGriseItem(CMD_MENU_LANCER_APPLI,FALSE);
    MnuEtatDebugger();
    MnuEtatChaud();
    }
  MnuTitreAJour();
  }

// -----------------------------------------------------------------------
void MnuEtatLance(void)
  {
  MenuAppliGriseSubMenu(SUB_MENU_FICHIER,FALSE);
  MenuAppliGriseSubMenu(SUB_MENU_EDITION,FALSE);
  MenuAppliGriseSubMenu(SUB_MENU_EXECUTION,FALSE);
  MenuAppliGriseSubMenu(SUB_MENU_PAGES,FALSE);
  MenuAppliGriseSubMenu(SUB_MENU_AIDE,FALSE);
	//
  MenuAppliGriseItem(CMD_MENU_OUVRE,TRUE);
  MenuAppliGriseItem(CMD_MENU_SAUVE,FALSE);
  MenuAppliGriseItem(CMD_MENU_SAUVE_SOUS,FALSE);
  MenuAppliGriseItem(CMD_MENU_ARRETER_APPLI,FALSE);
  MenuAppliGriseItem(CMD_MENU_LANCER_APPLI,TRUE);
  MenuAppliGriseItem(CMD_MENU_LANCER_APPLI_CH,TRUE);
  MenuAppliGriseItem(CMD_MENU_DEBUGGER,TRUE);
  MenuAppliGriseItem(CMD_MENU_IMPRIME,FALSE);
  }

// -----------------------------------------------------------------------
void MnuEtatArrete(void)
  {
	MenuAppliGriseSubMenu(SUB_MENU_FICHIER,FALSE);
  MenuAppliGriseSubMenu(SUB_MENU_EDITION,FALSE);
  MenuAppliGriseSubMenu(SUB_MENU_EXECUTION,FALSE);
	MenuAppliGriseSubMenu(SUB_MENU_PAGES,TRUE);
	MenuAppliGriseSubMenu(SUB_MENU_AIDE,FALSE);
	//
  MenuAppliGriseItem(CMD_MENU_OUVRE,FALSE);
  MenuAppliGriseItem(CMD_MENU_SAUVE,FALSE);
  MenuAppliGriseItem(CMD_MENU_SAUVE_SOUS,FALSE);
  MenuAppliGriseItem(CMD_MENU_ARRETER_APPLI,TRUE);
  MenuAppliGriseItem(CMD_MENU_LANCER_APPLI,FALSE);
  MenuAppliGriseItem(CMD_MENU_IMPRIME,TRUE);
  MnuEtatDebugger();
  MnuEtatChaud();
  SetAlarmeCode (FALSE);
  }

// -----------------------------------------------------------------------
void MnuEtatToutGris(void)
  {
	MenuAppliGriseSubMenu(SUB_MENU_FICHIER,TRUE);
  MenuAppliGriseSubMenu(SUB_MENU_EDITION,TRUE);
  MenuAppliGriseSubMenu(SUB_MENU_EXECUTION,TRUE);
  MenuAppliGriseSubMenu(SUB_MENU_PAGES,TRUE);
  MenuAppliGriseSubMenu(SUB_MENU_AIDE,TRUE);
  }

// -----------------------------------------------------------------------
void MnuEtatVide(void)
  {
	MenuAppliGriseSubMenu(SUB_MENU_EDITION,TRUE);
  MenuAppliGriseSubMenu(SUB_MENU_EXECUTION,TRUE);
  MenuAppliGriseSubMenu(SUB_MENU_PAGES,TRUE);
	//
  MenuAppliGriseItem(CMD_MENU_SAUVE,TRUE);
  MenuAppliGriseItem(CMD_MENU_SAUVE_SOUS,TRUE);
  MenuAppliGriseItem(CMD_MENU_IMPRIME,TRUE);
  }

// ----------------------------------------------------------
// Message WM_COMMAND Prise en compte des acc�l�rateurs des touches fonctions
// Renvoie TRUE s'il s'agissait d'une commande acc�l�rateur
BOOL bOnCommandAccelTouchesFonctions (WPARAM wparam)
	{
	BOOL bRes = TRUE;
	DWORD dwCodePcsTouche = 0;

	switch (LOWORD (wparam))
		{
		case ID_ACCEL_F1: dwCodePcsTouche = TOUCHE_F1;	break;
		case ID_ACCEL_F2: dwCodePcsTouche = TOUCHE_F2;	break;
		case ID_ACCEL_F3: dwCodePcsTouche = TOUCHE_F3;	break;
		case ID_ACCEL_F4: dwCodePcsTouche = TOUCHE_F4;	break;
		case ID_ACCEL_F5: dwCodePcsTouche = TOUCHE_F5;	break;
		case ID_ACCEL_F6: dwCodePcsTouche = TOUCHE_F6;	break;
		case ID_ACCEL_F7: dwCodePcsTouche = TOUCHE_F7;	break;
		case ID_ACCEL_F8: dwCodePcsTouche = TOUCHE_F8;	break;
		case ID_ACCEL_F9: dwCodePcsTouche = TOUCHE_F9;	break;
		case ID_ACCEL_F10: dwCodePcsTouche = TOUCHE_F10;	break;
		case ID_ACCEL_F11: dwCodePcsTouche = TOUCHE_SHIFT_F1;	break;
		case ID_ACCEL_F12: dwCodePcsTouche = TOUCHE_SHIFT_F2;	break;
		case ID_ACCEL_F13: dwCodePcsTouche = TOUCHE_SHIFT_F3;	break;
		case ID_ACCEL_F14: dwCodePcsTouche = TOUCHE_SHIFT_F4;	break;
		case ID_ACCEL_F15: dwCodePcsTouche = TOUCHE_SHIFT_F5;	break;
		case ID_ACCEL_F16: dwCodePcsTouche = TOUCHE_SHIFT_F6;	break;
		case ID_ACCEL_F17: dwCodePcsTouche = TOUCHE_SHIFT_F7;	break;
		case ID_ACCEL_F18: dwCodePcsTouche = TOUCHE_SHIFT_F8;	break;
		case ID_ACCEL_F19: dwCodePcsTouche = TOUCHE_SHIFT_F9;	break;
		case ID_ACCEL_F20: dwCodePcsTouche = TOUCHE_SHIFT_F10;break;
		default:
			bRes = FALSE;
			break;
		}

	if (bRes)
		ScrAppuiTouche (dwCodePcsTouche);

	return bRes;
	} // bOnCommandAccelTouchesFonctions


// -----------------------------------------------------------------------
// WM_COMMAND 
// -----------------------------------------------------------------------
static BOOL bOnCommand (HWND hwnd, WPARAM mp1, LPARAM mp2)
  {
  t_param_action  param_action={0,0,""};
  char						pszFic[MAX_PATH]="";
  char					  titre[260]="";
  char					  action[30];
  PBOOL 					ptr_log=NULL;
  PX_VAR_SYS			pwPosition=NULL;
  	
  switch (LOWORD (mp1))
    {
    case CMD_MENU_OUVRE:
      bLngLireInformation (c_inf_tdlg_ouvrir, titre);
      bLngLireInformation (c_inf_action_ouvre, action);
			pszCopiePathNameDocExt (pszFic, MAX_PATH, EXTENSION_A_FROID);
      if (bDlgFichierOuvrir (hwnd, pszFic, titre, "xpc", action))
        {
        StrCopy (param_action.chaine, pszSupprimeExt(pszFic));
        ajouter_action (action_charge_applic_froide_mnu, &param_action);
        ajouter_action (action_charge_lien_dde, &param_action);
        }
      else
        {
        StrCopy (param_action.chaine, pszPathNameDoc());
        ajouter_action (action_charge_applic_froide_mnu, &param_action);
        ajouter_action (action_charge_lien_dde, &param_action);
				}
			break;
			
    case CMD_MENU_SAUVE :
			StrCopy (pszFic, pszPathNameDoc());
			SauveLienDde (pszFic);
			SauveApplication(pszFic, TRUE);
      break;
			
    case CMD_MENU_SAUVE_SOUS :
      bLngLireInformation (c_inf_tdlg_sauve, titre);
      bLngLireInformation (c_inf_action_sauve, action);
			pszCopiePathNameDocExt (pszFic, MAX_PATH, EXTENSION_A_CHAUD);
      if (bDlgFichierEnregistrerSous (hwnd, pszFic, titre, EXTENSION_A_CHAUD, action))
        {
        StrCopy (pszFic, pszSupprimeExt(pszFic));
				SauveLienDde (pszFic);
        SauveApplication(pszFic, TRUE);
        }
      break;
			
    case CMD_MENU_IMPRIME :
			{
			t_dlg_liste_imp_ex retour_imp_choisi = {NULL,FALSE,NULL,0,0,0,""};
			
      retour_imp_choisi.ptr_proc_maj_liste = initialise_num_page_dlg;
      StrCopy (retour_imp_choisi.fen_document, pszPathNameDoc());
      if (dlg_liste_imp_ex (hwnd, &retour_imp_choisi))
        {
        if (retour_imp_choisi.a_ecrire)
          {
          VarSysExeEcritZone(IdxVarSysExeFenImpression, retour_imp_choisi.ptr_zone1, retour_imp_choisi.nb_synoptiques);
          VarSysExeEcritZone(IdxVarSysExeFenNomDocument, retour_imp_choisi.ptr_zone2, retour_imp_choisi.nb_synoptiques);
          }
        }
			}
      break;
			
    case CMD_MENU_QUITTE :
			{
      ::PostMessage (::GetDlgItem(Appli.hwnd, ID_DLG_DEB_EX), WM_COMMAND, ID_ABANDON, 0);
      VarSysExeEcritLog (IdxVarSysExeRetourSysteme,TRUE);
      ajouter_action (action_quitte, &param_action);
			}
      break;
			
    case CMD_MENU_DDE_LIEN:
			EditionLienDDE (hwnd);
      break;
			
    case CMD_MENU_COPIER :
      ajouter_action (action_copier_lien_dde, &param_action);
      break;
			
    case CMD_MENU_COLLER: // coller avec liaison
      ajouter_action (action_coller_lien_dde, &param_action);
      break;
			
    case CMD_MENU_LANCER_APPLI :
      CtxtDebugger.On = FALSE;
      StrCopy (param_action.chaine, pszPathNameDoc());
      ajouter_action (action_init_ressources, &param_action);
      ajouter_action (action_charge_applic_froide_mnu, &param_action);
      ajouter_action (action_charge_lien_dde, &param_action);
      ajouter_action (action_lance_application_mnu, &param_action);
      VarSysExeEcritLog(IdxVarSysExeArretExploitation,FALSE);
      break;
			
    case CMD_MENU_LANCER_APPLI_CH :
      CtxtDebugger.On = FALSE;
      StrCopy (param_action.chaine, pszPathNameDoc());
      ajouter_action (action_init_ressources, &param_action);
      ajouter_action (action_charge_applic_chaude_mnu , &param_action);
      ajouter_action (action_charge_lien_dde, &param_action);
      ajouter_action (action_relance_application_mnu, &param_action);
      VarSysExeEcritLog (IdxVarSysExeArretExploitation,FALSE);
      break;
			
    case CMD_MENU_ARRETER_APPLI :
      ::PostMessage(::GetDlgItem(Appli.hwnd,ID_DLG_DEB_EX), WM_COMMAND, ID_ABANDON, 0); // $$ v�rifier
      VarSysExeEcritLog (IdxVarSysExeArretExploitation,TRUE);
      VarSysExeEcritLog (IdxVarSysExeEffaceVisu,TRUE);
      MnuTitreAJour();
      MnuEtatArrete();
      break;
			
    case CMD_MENU_SYNOPTIQUES :
			{
			// Choix d'affichage ou de fermeture de pages de synoptique
			CHOIX_PAGES_AFFICHEES_EXE retour_syn_choisi = {FALSE,NULL,0,0};// {NULL,FALSE,NULL,0,0};
			
      //retour_syn_choisi.ptr_proc_maj_liste = initialise_num_page_dlg;
      if (dlg_liste_syn_ex (hwnd, &retour_syn_choisi))
        {
        if (retour_syn_choisi.a_ecrire)
          {
          VarSysExeEcritZone (IdxVarSysExeFenVisible, retour_syn_choisi.ptr_zone1, retour_syn_choisi.nb_synoptiques);
          }
        }
			}
      break;
			
    case CMD_MENU_ALARMES :
      pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme,al_visible);
      ptr_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pwPosition->dwPosCourEx);
      if (*ptr_log)
        {
        VarSysExeEcritLog (IdxVarSysExeAlarmeVisible,FALSE);
        }
      else
        {
        VarSysExeEcritLog (IdxVarSysExeAlarmeVisible,TRUE);
        }
      break;
			
    case CMD_MENU_PRODUIT :
			{
			t_dlg_release_cle retour_release_cle = {"",0,0,FALSE};
			
			/* $$ac ajouter_action (action_examine_produit, &param_action);
			ActionsExeLibereAttente();
			Sleep(1000); //$$ ??
			*/
			
			diagnostic_dongle ((PDWORD)(&retour_release_cle.iNumFonction), &retour_release_cle.dwNbVariables);
			dlg_diag_ex (hwnd,&retour_release_cle);
			if (retour_release_cle.a_programmer)
				{
				// $$ac bLngLireInformation (c_inf_confir_sauve, mess);
				StrCopy (param_action.chaine,retour_release_cle.szNumRelease);
				param_action.entier = retour_release_cle.iNumFonction;
				param_action.dwMot = retour_release_cle.dwNbVariables;
				ajouter_action (action_programme_dongle , &param_action);
				}
			// MenuAppliGriseItem(CMD_MENU_A_PROPOS,TRUE);
			}
      break;
			
    case CMD_MENU_A_PROPOS:
			bAfficheInfoVersion ();
			//dlg_preambule (hwnd,FALSE);
			// MenuAppliGriseItem(CMD_MENU_A_PROPOS,TRUE);
      break;
			
    case CMD_MENU_DEBUGGER:
			DebuggerExecutionPasAPas (hwnd);
      break;
			
			
    case CMD_MENU_AIDE_CONSIGNES: //SUB_MENU_AIDE:
      ScrQhelpOpen (TRUE);
      break;

		default:
			bOnCommandAccelTouchesFonctions (mp1);
			break;
    }

	// D�bloque l'attente de lecture de la queue d'actions
	ActionsExeLibereAttente();
	return TRUE;
  }

// --------------------------------------------------------------------------
// Window-Procedure
// --------------------------------------------------------------------------
static LRESULT CALLBACK wndprocExe (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT      mres = 0;              // Valeur de retour
  BOOL         bTraite = TRUE;        // Indique commande trait�e
  
  switch (msg)
    {
		case WM_CREATE:
			{
			// Enregistre le handle de l'application
			Verif(Appli.bSetHWndPrincipal (hwnd));

			// Grise les items de menu non op�rationnels
			if (ExisteMenu())
				MnuEtatVide();

			// cr�e la fen�tre fille ScrExe
			Verif (bScrCreeFenetre (hwnd, 0));

			// $$ obsolete (� v�rifier) wBoiteSelectFicOuverte = 2;

			InitAide ();
			}
			break;

    case WM_DESTROY:
			{
			FermeAide ();
			// Handle de l'appli ferm�
			Appli.bSetHWndPrincipal (NULL);
			PostQuitMessage(0);
			}
			break;

		case WM_CLOSE:
			// commande de fermeture de l'appli : redirection sur menuex $$ am�liorable
			::PostMessage (hwnd, WM_COMMAND, CMD_MENU_QUITTE, 0);
			break;

    case WM_CHAR:
			bTraite = ScrOnChar (mp1);
			mres = !bTraite;
			break;

    case WM_KEYDOWN:
			{
			DWORD	dwTouche;

			bTraite = bScrOnKeyDownTouchePcs (mp1, &dwTouche);
			if (bTraite)
				// Prise en compte de l'appui d'une touche (selon la nomenclature ci dessus)
				ScrAppuiTouche   (dwTouche);
			mres = (LRESULT) !bTraite;
			}
			break;

    case WM_SIZE:
			if (mp1 != SIZE_MINIMIZED)
				ScrReSize ();
			break;

    case WM_COMMAND:
			bTraite = bOnCommand (hwnd, mp1, mp2);
			break;

    case WM_HELP:
			bTraite = bOnCommand (hwnd, mp1, mp2); //$$ ???
			break;

    case WM_USER_DESTROY:
			// Relais d'un ::PostMessage issu d'un autre thread
			::DestroyWindow (hwnd);
			break;

    case WM_MENUSELECT:
			bTraite = FALSE;
			MnuOnMenuSelect (hwnd, &msg, &mp1, &mp2);
			break;

    case WM_SET_TITRE:
			// Relais d'un ::PostMessage issu d'un autre thread
			::SetWindowText (hwnd, (char*)mp1);
			break;

    case WM_SET_DLG_SELECT_FIC:
			// Envoy� par ExeOuvFic comme message inter-thread pour demander 
			// l'ouverture de la boite de dialogue de choix de fichier (EXECUTE_F 38)
			// Active la fen�tre
			SetForegroundWindow (hwnd);
			// Demande l'ouverture de la boite
			DlgOuvreBoiteSelectFic ();
			break;

    default:
			bTraite = FALSE;
			break;
    }

  if (!bTraite)
    {
    mres = DefWindowProc (hwnd, msg, mp1, mp2);
    }

  return (mres);
  } // wndprocExe

// --------------------------------------------------------------------------
// cr�ation de la fen�tre de l'application pcsexe
static BOOL bCreeFenEx ()
  {
  BOOL			bRes = FALSE; // Valeur retour
static BOOL	bClasseEnregistree = FALSE;

	// Enregistre la classe de la fen�tre WExe si n�cessaire
	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= 0;
		wc.lpfnWndProc		= wndprocExe; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= LoadIcon (Appli.hinst, MAKEINTRESOURCE(IDICO_APPLI));
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= (HBRUSH)(COLOR_APPWORKSPACE+1); //pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseExe; 
		bClasseEnregistree = RegisterClass (&wc);
		}

  if (bClasseEnregistree)
    {
		// Dimension de la fen�tre en vertical : 2*bordure redimensionnable+ titre+menu
	  int	cyMenu = (2 * GetSystemMetrics (SM_CYSIZEFRAME)) + GetSystemMetrics (SM_CYMENU) + GetSystemMetrics (SM_CYCAPTION);
		char szTitre [MAX_PATH];

		bLngLireInformation (c_inf_titre_pcs, szTitre);

   // cr�e la fen�tre
		if (CreateWindow (szClasseExe, szTitre, WS_OVERLAPPEDWINDOW | WS_VISIBLE,
			0, 0, Appli.sizEcran.cx, cyMenu, NULL, LoadMenu (Appli.hinstDllLng, MAKEINTRESOURCE (IDMENU_PCS_EXE)), Appli.hinst,	NULL))
			bRes = TRUE;
    }

  return bRes;
  }	// bCreeFenEx

// --------------------------------------------------------------------------
// thread fen�tre principale de PcsExe
UINT __stdcall uThreadFenEx (void *hThrd)
  {
	DumpVie(uThreadFenEx);
  UINT		wFct;
  DWORD *	pwError;

  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
  while (bThreadAttenteMessage ((HTHREAD)hThrd, NULL, NULL, &pwError, &wFct))
    {
		BOOL    bFinMessageThread = TRUE;
		MSG			msg;

		// Traitement selon le message envoy� au thread
    switch (wFct)
      {
      case THRD_FCT_CREATE:   //Cr�ation des ressources de la t�che
        (*pwError) = 1;

				// Charge la table d'acc�l�rateurs
				hAccelTouchesFonctions = LoadAccelerators (Appli.hinstDllLng,  MAKEINTRESOURCE(ID_ACCEL_T_FONCTIONS));

				// Cr�e la fen�tre de l'application
        if (bCreeFenEx ())
          (*pwError) = 0;
        break;

      case THRD_FCT_EXEC:
				// Re�u WM_QUIT ?
        if (GetMessage (&msg, NULL, 0, 0))
          {
					// non => Traitement du message
					if (!TranslateAccelerator (Appli.hwnd, hAccelTouchesFonctions, &msg))
						{
						TranslateMessage (&msg);
						DispatchMessage (&msg);
						}

					// Continue THRD_FCT_EXEC
          bFinMessageThread = FALSE;
          }
				else
					{ 
					// oui => sors de la boucle de traitement des actions
					t_param_action param_action={0,0,""};
					
					param_action.entier = 1; //$$ v�rifier code
          ajouter_action (action_quitte, &param_action);			
					}
        break;
      }

    // Message thread termin� ?
    if (bFinMessageThread)
      {
			// oui => attente du message thread suivant
      ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);

			// Le message courant a �t� trait�
      ThreadFinTraiteMessage ((HTHREAD)hThrd);
      }
    else
			// non => message windows suivant
      ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, 0L);
    } // while (bThreadAttenteMessage ((HTHREAD)hThrd, NULL, NULL, &pwError, &wFct))
  return uThreadTermine ((HTHREAD)hThrd, 0);
  } // ThreadFenEx

// --------------------------------------------------------------------------
// Fermeture de la fen�tre WExe (inter thread)
BOOL bPostDestroyWExe (void)
  {
  return ::PostMessage (Appli.hwnd, WM_USER_DESTROY, 0 ,0);
  }

// --------------------------------------------------------------------------
// Change le titre de la fen�tre WExe (m�me thread)
void FenSetTitre(char * pszTitre)
  {
  ::SetWindowText (Appli.hwnd, pszTitre);
  }


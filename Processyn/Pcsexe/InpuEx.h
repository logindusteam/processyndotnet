/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il demeure sa propriete exclusive et est confidentiel.             |
 |   Aucune diffusion n'est possible sans accord ecrit.                 |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Input - Pcs-EXE                                          |
 |   Auteur  : JB                                                       |
 |   Date    : 12/02/93                                                 |
 +----------------------------------------------------------------------*/


// ---------------------------------- Types --------------------------------
typedef struct
	{
	int      mode_lecture;
	char     chaine [256];
	} t_param_lecture;

// ---------------------------------- Constantes ---------------------------
#define MODE_INITIALISATION  0
#define MODE_FINALISATION    1
#define MODE_DEMARAGE        2
#define MODE_ENCHAINEMENT    3

// Handle de la fen�tre au comportement "boite de dialogue" active
// pour le thread WinMain
extern HWND	hDlgGrMainActive;

// ---------------------------------- Verbes -------------------------------
int lire_commandes (t_param_lecture *param_lecture);

// ---------------------------------- Verbes -------------------------------
// Signifie � la tache de lecture des actions qu'une nouvelle action est pr�sente
//D�bloque l'attente de lecture de la queue d'actions
void ActionsExeLibereAttente (void);

// Ecoule tous les messages de cette queue de message
// Redirige les touches acc�l�ratrices sur Appli.hwnd
// et g�re la boite de dialogue active pour ce thread
// Renvoie FALSE si le message WM_QUIT est rencontr�, TRUE sinon
BOOL bVideQueueDeMessage (void);


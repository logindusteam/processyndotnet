// --------------------------------------------------------------------------
// CliAnmAl.h
// Win32
// T�che d'animation des alarmes reli�e � Pcsexe via Mbx
// 16/10/96
// --------------------------------------------------------------------------


// --------------------------------------------------------------------------
void  MbxAnmAlAck  (void);
BOOL  MbxAnmAlSend (DWORD wSizeMsg, void *pMsg);

// --------------------------------------------------------------------------
// cr�ation du thread d'animation d'alarme
BOOL bExecuteAnimAlarmes (PITC_TX_RX pITCServeurAnmAl);

// --------------------------------------------------------------------------
// fermeture du thread d'animation d'alarme
BOOL bFinExecuteAnimAlarmes (void);

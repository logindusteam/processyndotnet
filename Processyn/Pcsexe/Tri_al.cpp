#include "stdafx.h"
#include "std.h"           // Types Standards
#include "UStr.h"        // Chaines de Caract�res            Strxxx ()
#include "FMan.h"       // Acc�s Disques
#include "FileMan.h"       // Acc�s Disques
#include "init_txt.h"      // D�finitions des imprimantes dans PCS.INI
#include "UChrono.h"
#include "lng_res.h"       // Types des Blocs
#include "tipe_al.h"       // Types des Blocs
#include "mem.h"           // Memory Manager
#include "MemMan.h"
#include "PathMan.h"
#include "tri_al.h"
#include "Verif.h"
VerifInit;

//-------------------------------- Variables Locales
static DWORD max_fic;
static DWORD pteur_fic;
static DWORD pteur_date_debut;
static DWORD pteur_date_fin;
static BOOL trouve_date_fin;
static BOOL test_debut_fin;

// -----------------------------------------------------------------------
// Renvoie une valeur caract�ristique de la date respectant la relation d'ordre
LONG CalculDate (DWORD wAnnee, DWORD wMois, DWORD wJour)
  {
  if (wMois >= 3)
    {
    wMois++;
    }
  else
    {
    wAnnee--;
    wMois = wMois + 13;
    }
  return ( ((LONG) (365.25 * wAnnee)) + ((LONG) (30.6 * wMois)) + wJour); // TRUNC4
  }

// -----------------------------------------------------------------------
// Renvoie une valeur caract�ristique de l'heure respectant la relation d'ordre
LONG CalculHeure (DWORD wHeure, DWORD wMinute, DWORD wSeconde, DWORD wMilliSeconde)
  {
  return ((wHeure * 3600000L) + (wMinute * 60000) + (wSeconde * 1000) + wMilliSeconde); // ROUND4
  }

//-------------------------------------------------------------------------
// Convertis une chaine de caract�re au format date JJxMMxAAAA en date sur un LONG et renvoie TRUE
// si format "?..." renvoie TRUE et le LONG vaut 0
BOOL bValideCritereDateAlarme (PCSTR pszDate, PLONG plDate)
  {
  BOOL bRes = FALSE;

  if (StrLength (pszDate) == 10)
    {
		char szTemp[12];
		int nJour = 0;
		int nMois = 0;
		int nAnnee = 0;

		// S�pare dans la chaine Jour, mois et an (format fixe)
		StrCopy (szTemp,pszDate);
		szTemp [2] = 0;
		szTemp [5] = 0;
		szTemp [10] = 0;

    // conversion chaine en num�rique
    StrToINT (&nJour, szTemp);
    StrToINT (&nMois, &szTemp[3]);
    StrToINT (&nAnnee, &szTemp[6]);
    if ((nJour > 0) && (nJour < 32))
      {
      if ((nMois > 0) && (nMois < 13))
        {
        *plDate = CalculDate (nAnnee, nMois, nJour);
        bRes = TRUE;
        } // mois ok
      } // jour ok
    }  // date.len == 10
  else
    { // date.len != 10
    if (pszDate[0] == '?')
      {
      *plDate = 0;
      bRes = TRUE;
      }
    }  // date.len != 10

  return bRes;
  } // bValideCritereDateAlarme

//-------------------------------------------------------------------------
// Convertis une chaine de caract�re au format HHxMMxSS en heure sur un LONG et renvoie TRUE
// si format "?..." renvoie TRUE et le LONG vaut 0 ou 23:59:59+1 si bHeureDebut TRUE ou FALSE
BOOL bValideCritereHeureAlarme (PCSTR pszHeure, BOOL bHeureDebut, LONG * plHeure)
  {
  BOOL bRes = FALSE;

  if (StrLength(pszHeure) == 8)
    {
		char szTemp[10];
		int nHeure = 0;
		int nMinute = 0;
		int nSeconde = 0;

		// S�pare dans la chaine heure, minute et seconde (format fixe)
		StrCopy (szTemp, pszHeure);
		szTemp [2] = 0;
		szTemp [5] = 0;
		szTemp [8] = 0;

    StrToINT (&nHeure, szTemp);
    StrToINT (&nMinute, &szTemp[3]);
    StrToINT (&nSeconde, &szTemp[6]);
    if ((nHeure >= 0) && (nHeure <= 23))
      {
      if ((nMinute >= 0) && (nMinute <= 59))
        {
        if ((nSeconde >= 0) && (nSeconde <= 59))
          {
          // calcul heure
          *plHeure = CalculHeure (nHeure, nMinute, nSeconde, 0);
          bRes = TRUE;
          }
        }
      }
    }  // pszHeure.len = 8
  else
    { // pszHeure.len != 8
    if (pszHeure[0] == '?')
      {
      if (bHeureDebut)
        *plHeure = 0;
      else
        *plHeure = 86400; // 23:59:59 + 1
      bRes = TRUE;
      }
    }  // pszHeure.len != 10

  return bRes;
  } // bValideCritereHeureAlarme

//-------------------------------------------------------------------------
// Convertis une chaine de caract�re au format num�rique en niveau de priorit� et renvoie TRUE
// si format "?..." renvoie TRUE
BOOL bValideCriterePrioriteAlarme (PCSTR pszPriorite, PBYTE pbyPriorite)
  {
  BOOL bRes = TRUE;

  // valide la priorite
  *pbyPriorite = 0; // d�faut
  if (pszPriorite[0] != '?')
    {
    if ((!StrToBYTE (pbyPriorite, pszPriorite)) || ((*pbyPriorite) < 1) || ((*pbyPriorite) > 10))
      {
      bRes = FALSE;
      }
    }
	return bRes;
  } // bValideCriterePrioriteAlarme

//-------------------------------------------------------------------------
// Convertis une chaine de caract�re au format num�rique en �venement et renvoie TRUE
// si format "?..." renvoie TRUE
BOOL bValideCritereEvenementsAlarme (PCSTR pszEvenement, PBYTE pbyEvenement)
  {
  BOOL bRes = TRUE;

  // valide l'�venement
  if (pszEvenement[0] == '?')
    (*pbyEvenement) = 0;
  else
    {
    if (bStrEgales (pszEvenement, "+"))
      (*pbyEvenement) = 1;
    else
      {
      if (bStrEgales (pszEvenement, "-"))
        (*pbyEvenement) = 2;
      else
        {
        if (bStrEgales (pszEvenement, "*+"))
          (*pbyEvenement) = 3;
        else
          {
          if (bStrEgales (pszEvenement, "*-"))
            (*pbyEvenement) = 4;
          else
            { // pas dans l'ensemble
            bRes = FALSE;
            }
          }
        }
      }
    }

	return bRes;
  } // bValideCritereEvenementsAlarme


//-----------------------------------------------------------------------
// Resultat : var pteur_renvoye
// Renvoie faux s'il y a pb sur un fichier
//-----------------------------------------------------------------------
static BOOL recherche_date (PX_ANIM_ALARMES_ID_ALARME_ARCHIVE pIdAlarmeArchive, HFIC hficImageIds,
	LONG date_cherchee, DWORD debut, DWORD fin, BOOL bDateDebut, DWORD *pteur_renvoye, PBOOL pbTrouve)
  {
  BOOL suite;
  DWORD erreur = NO_ERROR;

  (*pbTrouve) = FALSE;
  if (bDateDebut) // gain de temps si la date est juste au debut ou a la fin
    { // on cherche la date de debut : test si 1� date_fichier < date_cherchee
    pteur_fic = 1;
    if ((erreur = uFileLireDirect (hficImageIds, pteur_fic-1, pIdAlarmeArchive)) == NO_ERROR)
      {
      suite = (pIdAlarmeArchive->iDate < date_cherchee);
      }
    }
  else
    { // on cherche la date de fin : test si derniere_date_fichier > lCritereDateFin
    pteur_fic = max_fic;
    if ((erreur = uFileLireDirect (hficImageIds, pteur_fic-1, pIdAlarmeArchive)) == NO_ERROR)
      {
      suite = (pIdAlarmeArchive->iDate > date_cherchee);
      }
    }

  if (suite && (erreur == NO_ERROR))
    { // la date n'est ni au debut ni a la fin
    while (((fin - debut) > 1) && (!(*pbTrouve)) && (erreur == NO_ERROR))
      {
			//------------- recherche dichotomique
      pteur_fic = (fin + debut) / 2;
      if ((erreur = uFileLireDirect (hficImageIds, pteur_fic-1, pIdAlarmeArchive)) == NO_ERROR)
        {
        if (pIdAlarmeArchive->iDate == date_cherchee)
          {
          (*pbTrouve) = TRUE;
          }
        else
          { // pIdAlarmeArchive->iDate != date_cherchee
          if (pIdAlarmeArchive->iDate > date_cherchee)
            {
            fin = pteur_fic;
            }
          else
            { // pIdAlarmeArchive->iDate < date_cherchee
            debut = pteur_fic;
            }
          }
        } // erreur = NO_ERROR
      } // while (fin - debut) > 1  et pas trouve

    if (erreur == NO_ERROR)
      {
      if (*pbTrouve)
        {
        if (bDateDebut)
          { // trouve = vrai et on cherche la date de debut
          while ((pIdAlarmeArchive->iDate == date_cherchee) && (pteur_fic > 0) && (erreur == NO_ERROR))
            { // cherche la premiere date
            if ((erreur = uFileLireDirect (hficImageIds, pteur_fic-1, pIdAlarmeArchive)) == NO_ERROR)
              {
              pteur_fic--;
              }
            } // while

          if (pteur_fic == 0)
            { // on est remont� au d�but du fichier
            pteur_fic = 1;
            }
          else
            {
            pteur_fic = pteur_fic + 2;
            }
          } // 
        else
          { // trouve = vrai et on cherche la date de fin
          while ((pIdAlarmeArchive->iDate == date_cherchee) && (pteur_fic <= max_fic) && (erreur == NO_ERROR))
            { // cherche la derniere date
            if ((erreur = uFileLireDirect (hficImageIds, pteur_fic-1, pIdAlarmeArchive)) == NO_ERROR)
              {
              pteur_fic++;
              }
            } // while

          if (pteur_fic > max_fic)
            { // on est all� � la fin du fichier
            pteur_fic--;
            }
          else
            {
            pteur_fic = pteur_fic - 2;
            }

          } // date de fin
        }// trouve
      else
        { // pas trouve
        if (bDateDebut)
          { // trouve = faux et on cherche la date de debut
          pteur_fic = fin;
          }
        else
          { // trouve = faux et on cherche la date de fin
          pteur_fic = debut;
          } // date de fin
        } // pas trouve
      } // erreur = 0
    } // suite

  (*pteur_renvoye) = pteur_fic;

  return (erreur == NO_ERROR);
  }

//-----------------------------------------------------------------------
// Resultat : var pteur_renvoye
// Renvoie  : vrai si erreur = 0
//-----------------------------------------------------------------------
static BOOL recherche_heure (PX_ANIM_ALARMES_ID_ALARME_ARCHIVE pIdAlarmeArchive, HFIC hficImageIds,
	LONG lDateDebut, LONG lDateFin, LONG heure_cherchee, DWORD pteur_depart, BOOL bHeureDebut, DWORD *pteur_renvoye)
  {
  DWORD erreur = NO_ERROR;
  BOOL trouve = FALSE;

  pteur_fic = pteur_depart;
  if ((erreur = uFileLireDirect (hficImageIds, pteur_fic-1, pIdAlarmeArchive)) == NO_ERROR)
    {
    if (bHeureDebut)
      { // on recherche heure debut
      while ((pIdAlarmeArchive->iDate == lDateDebut) && (!trouve) &&
             (pteur_fic < max_fic) && (erreur == NO_ERROR))
        {
        trouve = (pIdAlarmeArchive->iHeure >= heure_cherchee);
        pteur_fic++;
        erreur = uFileLireDirect (hficImageIds, pteur_fic-1, pIdAlarmeArchive);
        } // while
      if (trouve)
        {
        pteur_fic--;
        }
      } // recherche de heure debut
    else
      { // on recherche heure fin
      while ((pIdAlarmeArchive->iDate == lDateFin) && (!trouve) &&
             (pteur_fic > 1) && (erreur == NO_ERROR))
        {
        trouve = (pIdAlarmeArchive->iHeure <= heure_cherchee);
        pteur_fic--;
        erreur = uFileLireDirect (hficImageIds, pteur_fic-1, pIdAlarmeArchive);
        } // while
      if (trouve)
        {
        pteur_fic++;
        }
      } // recherche de heure fin
    } // erreur = NO_ERROR

  (*pteur_renvoye) = pteur_fic;

  return (erreur == NO_ERROR);
  }

//-----------------------------------------------------------------------
//
static BOOL test_dates (PBOOL pbChercherDateDebut,  PBOOL pbChercherDateFin,
PX_ANIM_ALARMES_ID_ALARME_ARCHIVE pIdAlarmeArchive, HFIC hficImageIds,
	LONG lDateDebut, LONG lDateFin, BOOL * pbContinuer,	PBOOL pbtrouve_date_debut
)
  {
  BOOL test_debut;
  BOOL test_fin;
  DWORD erreur = NO_ERROR;

  *pbChercherDateDebut = TRUE;
  *pbChercherDateFin   = TRUE;
  if (max_fic >= 1)
    {
		// lDateDebut wildcard ?
    if (lDateDebut <= 0)
      {
			// oui
      if ((erreur = uFileLireDirect (hficImageIds, 0, pIdAlarmeArchive)) == NO_ERROR)
        {
        lDateDebut = pIdAlarmeArchive->iDate;
        pteur_date_debut = 1;
        *pbtrouve_date_debut = FALSE;
        *pbChercherDateDebut = FALSE;
        }
      }

		// lDateFin wildcard ?
    if ((lDateFin <= 0) && (erreur == NO_ERROR))
      {
			// oui
      if ((erreur = uFileLireDirect (hficImageIds, max_fic-1, pIdAlarmeArchive)) == NO_ERROR)
        {
        lDateFin = pIdAlarmeArchive->iDate;
        pteur_date_fin = max_fic;
        trouve_date_fin = FALSE;
        *pbChercherDateFin = FALSE;
        }
      }

    test_debut = FALSE;
    test_fin = FALSE;
    if ((lDateDebut <= lDateFin) && (erreur == NO_ERROR))
      {
      if ((erreur = uFileLireDirect (hficImageIds, max_fic-1, pIdAlarmeArchive)) == NO_ERROR)
        {
        test_debut = (lDateDebut <= pIdAlarmeArchive->iDate);

        if ((erreur = uFileLireDirect (hficImageIds, 0, pIdAlarmeArchive)) == NO_ERROR)
          {
          test_fin = (lDateFin >= pIdAlarmeArchive->iDate);
          }
        }
      }
    } // max_fic >= 1
  else
    {
    erreur = 16;
    }

  (*pbContinuer) = (test_debut && test_fin);

  return (erreur == NO_ERROR);
  }

//-----------------------------------------------------------------------
//
static BOOL bGroupeDansCritere (PCSTR pszCritereNomGroupe, PCSTR pszGroupeTeste)
  {
  BOOL bRes = TRUE;

  if (pszCritereNomGroupe[0] != '?')
    bRes = bStrEgales (pszGroupeTeste, pszCritereNomGroupe);

  return bRes;
  }

//-----------------------------------------------------------------------
//
static BOOL bPrioriteDansCritere (int nCriterePriorite, int nPrioriteTestee)
  {
  BOOL bRes = TRUE;

  if (nCriterePriorite != 0)
    bRes = (nPrioriteTestee <= nCriterePriorite);

  return bRes;
  }

//-----------------------------------------------------------------------
//
static BOOL bEvenementDansCritere (int nCritereEvenement, int nEvenementTeste)
  {
  BOOL bRes = TRUE;

  if (nCritereEvenement != 0)
    bRes = (nEvenementTeste == nCritereEvenement);

  return bRes;
  }


//-----------------------------------------------------------------------
// ajoute dans le bloc bx_anm_consultation_al les alarmes satisfaisant
// aux crit�re sp�cifi�s (lDateDebut, lCritereDateFin, lCritereHeureDebut, lCritereHeureFin, nom_groupe, priorite, type_evenement)
// Resultat dans le bloc bx_anm_consultation_al
// si on ne trouve pas la lDateDebut, on renvoie un bloc vide
// si on ne trouve pas la lDateFin, on teste a partir de lDateDebut
//-----------------------------------------------------------------------
BOOL bAlConsulteArchive
	(LONG lCritereDateDebut, LONG lCritereDateFin, LONG lCritereHeureDebut, LONG lCritereHeureFin,
	PCSTR pszFicTri, char *nom_groupe, int priorite, int evenement)
  {
  UINT	uErr = NO_ERROR;
  char  szNomIds1[MAX_PATH];
  char  szNomIds2[MAX_PATH];

	// un ou deux fichier d'ids d'archivage d'alarme existent ?
	pszCreeNameExt (szNomIds1, MAX_PATH, pszFicTri, ".1"); // premier  fichier index archivage alarme
  pszCreeNameExt (szNomIds2, MAX_PATH, pszFicTri, ".2"); // deuxieme fichier index archivage alarme

  if (bFileExiste (szNomIds1) || bFileExiste (szNomIds2))
    {
		// oui => cr�ation d'une image des fichiers Index et Messages ...
		char  szNomTexte1[MAX_PATH];
		char  szNomTexte2[MAX_PATH];
		char  szNomImageIds[MAX_PATH];
		char  szNomImageTexte[MAX_PATH];

		pszCreeNameExt (szNomTexte1, MAX_PATH, pszFicTri, ".3"); // premier  fichier archivage message alarme
		pszCreeNameExt (szNomTexte2, MAX_PATH, pszFicTri, ".4"); // deuxieme fichier archivage message alarme
		pszCreeNameExt (szNomImageIds, MAX_PATH, pszFicTri, ".5"); // fichier index consultation archivage message alarme
		pszCreeNameExt (szNomImageTexte, MAX_PATH, pszFicTri, ".6"); // fichier message consultation archivage message alarme

		// le deuxi�me fichier d'id existe ?
    if (bFileExiste (szNomIds2))
      { 
			// oui => on le copie � la place de l'image Ok ?
			uErr = uFileCopie (szNomIds2, szNomImageIds, FALSE);
      if (uErr == NO_ERROR)
        {
        // oui => copie du texte � la place de l'image texte
        uErr = uFileCopie (szNomTexte2, szNomImageTexte, FALSE);
        }
      }
    else
      { // non => efface les fichiers image (erreur ignor�e)
      uFileEfface (szNomImageIds);
      uFileEfface (szNomImageTexte);
      }

		// pas d'erreur de copie ?
    if (uErr == NO_ERROR)
      {
      // oui Copie index � la fin de l'image index Ok ?
			uErr = uFileCopie (szNomIds1, szNomImageIds, TRUE);

      if (uErr == NO_ERROR)
        {
				// oui => copie texte � la fin de l'image texte Ok ?
				uErr = uFileCopie (szNomTexte1, szNomImageTexte, TRUE);
        if (uErr == NO_ERROR)
          {
					// oui => Ouverture de l'image du fichier index d'archivage Ok ?
					HFIC hficImageIds;

          if ((uErr = uFileOuvre (&hficImageIds, szNomImageIds, sizeof(X_ANIM_ALARMES_ID_ALARME_ARCHIVE), CFman::OF_OUVRE_OU_CREE)) == 0)
            {
						// oui => cr�e un bloc dans lequel sera stock� le r�sultat de la consultation d'archivage
						X_ANIM_ALARMES_ID_ALARME_ARCHIVE IdAlarmeArchive;
						DWORD pteur_heure_debut;
						DWORD pteur_heure_fin;
						BOOL trouve_date_debut;
						BOOL chercher_date_debut;
						BOOL chercher_date_fin;

            cree_bloc (bx_anm_consultation_al, sizeof(X_ANIM_ALARMES_CONSULTATION_ARCHIVAGE), 0);
            
						// d�but de la recherche
            uFileNbEnr (hficImageIds, &max_fic);
            if (test_dates (&chercher_date_debut, &chercher_date_fin, &IdAlarmeArchive, hficImageIds, lCritereDateDebut, lCritereDateFin, &test_debut_fin, &trouve_date_debut))
              {
              if (test_debut_fin)
                { // la recherche a partir de date debut est possible
								BOOL continuer = TRUE;

                if (chercher_date_debut)
                  {
           				if (!recherche_date (&IdAlarmeArchive, hficImageIds, lCritereDateDebut, 1, max_fic, TRUE, &pteur_date_debut, &trouve_date_debut))
                    uErr = 16;
                  }

                if ((chercher_date_fin) && (uErr == NO_ERROR))
                  {
                  if (!recherche_date (&IdAlarmeArchive, hficImageIds, lCritereDateFin, pteur_date_debut, max_fic, FALSE, &pteur_date_fin, &trouve_date_fin))
                    uErr = 16;
                  }

                if ((!trouve_date_debut) && (!trouve_date_fin))
                  {
                  //continuer = ((pteur_date_fin - pteur_date_debut) >= 1);
                  continuer = (pteur_date_fin >= pteur_date_debut);
                  }
                if (continuer && (uErr == NO_ERROR))
                  {
                  if (chercher_date_debut)
                    {
                    if (!recherche_heure (&IdAlarmeArchive, hficImageIds, lCritereDateDebut, lCritereDateFin, lCritereHeureDebut, pteur_date_debut, TRUE, &pteur_heure_debut))
                      uErr = 16;
                    }
                  else
                    {
                    pteur_heure_debut = 1;
                    }

                  if (chercher_date_fin && (uErr == NO_ERROR))
                    {
                    if (!recherche_heure (&IdAlarmeArchive, hficImageIds, lCritereDateDebut, lCritereDateFin, lCritereHeureFin, pteur_date_fin, FALSE, &pteur_heure_fin))
                      uErr = 16;
                    }
                  else
                    {
                    pteur_heure_fin = max_fic;
                    }

                  // test des champs nom_groupe, priorite et evt
                  pteur_fic = pteur_heure_debut;
                  while ((pteur_fic <= pteur_heure_fin) && (uErr == NO_ERROR))
                    {
                    if ((uErr = uFileLireDirect (hficImageIds, pteur_fic-1, &IdAlarmeArchive)) == NO_ERROR)
                      {
											// Crit�res de tri remplis ?
                      if (bGroupeDansCritere (nom_groupe, IdAlarmeArchive.szNomGroupe) &&
                          bPrioriteDansCritere (priorite, IdAlarmeArchive.wPriorite) &&
                          bEvenementDansCritere (evenement, IdAlarmeArchive.wTypeEvt))
                        {
												// l'enregistrement correspond
												PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE ads_tx_consultation;

                        insere_enr (szVERIFSource, __LINE__, 1, bx_anm_consultation_al, (nb_enregistrements (szVERIFSource, __LINE__, bx_anm_consultation_al) + 1));
                        ads_tx_consultation = (PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE)pointe_enr (szVERIFSource, __LINE__, bx_anm_consultation_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_consultation_al));
                        ads_tx_consultation->wPosEnreg = pteur_fic;
                        } // l'enregistrement correspond
                      }
                    pteur_fic++;
                    } // while
                  } // continuer et uErr == 0
                } // test debut fin
              } // if (test_dates

						// fermeture du fichier source
            if (uFileFerme (&hficImageIds) != NO_ERROR)
              uErr = 16;
            } // ouvre_fichier fic5
          } // copy_fichier fic3 a la fin de fic6
        } // copy_fichier fic1 a la fin de fic5
      } // uErr = 0
    } // fichier_present

  return uErr == NO_ERROR;
  } // bAlConsulteArchive

// --------------------------------------------------------------------------
// ouverture du fichier texte de resultat de consultation
//
void OuvreFicCons (char *pszNomFicArch, PHFIC phficFichierArch6, BOOL *bFicConsOuvert)
  {
  char pszNomFic[MAX_PATH];
  DWORD wNbCarMaxMesAl;
  DWORD wNbCarMaxFicArch;
  DWORD wNbCarMaxLineImp;

  if (!(*bFicConsOuvert))
    {
    StrCopy (pszNomFic, pszNomFicArch);
    StrConcat (pszNomFic, ".6"); // fichier texte
    definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
    if (uFileOuvre (phficFichierArch6, pszNomFic, wNbCarMaxFicArch+2, CFman::OF_OUVRE_OU_CREE) == NO_ERROR)
      {
      (*bFicConsOuvert) = TRUE;
      }
    else
      {
      // maj_statut_al(4);
      }
    }
  }

// --------------------------------------------------------------------------
// ferme fic 6 texte resultat consultation
//
void FermeFicCons (PHFIC phficFichierArch6, BOOL *bFicConsOuvert)
  {
  if (*bFicConsOuvert)
    {
    if (uFileFerme (phficFichierArch6) == NO_ERROR)
      {
      (*bFicConsOuvert) = FALSE;
      }
    else
      {
      // maj_statut_al(4);
      }
    }
  }

//-----------------------------------------------------------------------
//
BOOL lire_mess_rtat_consult (HFIC hficFichierArch6, DWORD position, char *tampon_mes)
  {
  BOOL btest;
  DWORD wNbCarMaxMesAl;
  DWORD wNbCarMaxFicArch;
  DWORD wNbCarMaxLineImp;

  if (btest = (uFileLireDirect (hficFichierArch6, position, tampon_mes) == NO_ERROR))
    {
    definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
    StrSetLength (tampon_mes, wNbCarMaxFicArch);
    }
  return (btest);
  }

//-----------------------------------------------------------------------
void ArchivageRtatConsult (PHFIC phficFichierArch6, BOOL *bFicConsOuvert, char *pszFicTri, char *pszFicArch)
  {
  if (existe_repere (bx_anm_consultation_al))
    {
		DWORD                   wIndex;
		DWORD                   wNbEnr;
		HFIC                    hficFichierArchRtat;
		PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE pConsult;
		char                    pszTextAl[TAILLE_MAX_MES_AL_COMPLET+1];
		BOOL										bFicConsOuvertDebut;
		DWORD                   wNbCarMaxMesAl;
		DWORD                   wNbCarMaxFicArch;
		DWORD                   wNbCarMaxLineImp;

		// efface le fichier d'archivage d'alarme
    uFileEfface (pszFicArch);
    definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
    if (uFileOuvre (&hficFichierArchRtat, pszFicArch, wNbCarMaxFicArch+2, CFman::OF_OUVRE_OU_CREE) == NO_ERROR)
      {
      bFicConsOuvertDebut = (*bFicConsOuvert);
      OuvreFicCons (pszFicTri, phficFichierArch6, bFicConsOuvert);
      wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_consultation_al);
      for (wIndex = 1; wIndex <= wNbEnr; wIndex++)
        {
        pConsult = (PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE)pointe_enr (szVERIFSource, __LINE__, bx_anm_consultation_al, wIndex);
        if (uFileLireDirect ((*phficFichierArch6), (pConsult->wPosEnreg-1), pszTextAl) == NO_ERROR)
          {
          uFileEcrireDirect (hficFichierArchRtat, wIndex-1, pszTextAl);
          }  // lire_mess ok
        }
      if (!bFicConsOuvertDebut)
        {
        FermeFicCons (phficFichierArch6, bFicConsOuvert);
        }
      uFileFerme (&hficFichierArchRtat);
      } // ouvre_fichier ok
    else
      {
      // maj_statut_al(4);
      }
    } // existe bloc bx_anm_consultation_al
  } // ArchivageRtatConsult

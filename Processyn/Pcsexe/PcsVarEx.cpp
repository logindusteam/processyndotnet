/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |									    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 |									    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |   Titre   : GEREBDC.C 						    |
 |   Auteur  : LM							    |
 |   Date    : 06/02/93 						    |
 +--------------------------------------------------------------------------*/
// Version Win32

#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "mem.h"
#include "dicho.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "Verif.h"
#include "Pcs_Sys.h"
#include "PcsVarEx.h"
VerifInit;

#define b_cour_log_modifie 8 
#define b_cour_num_modifie 9 
#define b_cour_mes_modifie 23

//---------------------------------------------------------------------------
// Nombre d'entr�es sorties prise en compte pour les droits commerciaux d'utilisation
DWORD CPcsVarEx::wGetNombreEsApplicBis (void)
  {
  DWORD i;
  DWORD wNombreEnreBEs;
  DWORD wNombreEs = 0;
  PENTREE_SORTIE	ptEs;

  if ((existe_repere (b_e_s)) &&
      (nb_enregistrements (szVERIFSource, __LINE__, b_e_s) > 1))
    {
    wNombreEnreBEs = nb_enregistrements (szVERIFSource, __LINE__, b_e_s);
    for (i=2;i <= wNombreEnreBEs;i++)
      {
      ptEs = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, i);
      if ( (ptEs->n_desc != d_systeme) &&
				(ptEs->n_desc != d_mem) &&
				(ptEs->n_desc != d_repete) &&
				(ptEs->n_desc != d_disq) &&
				(ptEs->n_desc != d_graf) &&
				(ptEs->n_desc != d_al) &&
				(ptEs->n_desc != d_chrono)
				)
				{
				if (ptEs->genre != libre)
					{
					if (ptEs->taille == 0)
						{
						wNombreEs++;
						}
					else
						{
						wNombreEs = wNombreEs + ptEs->taille;
						}
					} // if pas libre
				} // if pas systeme
      } // for
    } // if existe
  return (wNombreEs);
  }

//---------------------------------------------------------------------------
// creation avec le m�me nombre d'enregistrement que les valeurs courantes
// des blocs contenant l'indicateur de modification
void CPcsVarEx::CreeBlocsExValeurModifie (void)
  {
  if (!existe_repere (b_cour_log_modifie))
		cree_bloc (b_cour_log_modifie,sizeof (BOOL),nb_enregistrements (szVERIFSource, __LINE__, b_cour_log));
  if (!existe_repere (b_cour_num_modifie))
		cree_bloc (b_cour_num_modifie,sizeof (BOOL),nb_enregistrements (szVERIFSource, __LINE__, b_cour_num));
  if (!existe_repere (b_cour_mes_modifie))
		cree_bloc (b_cour_mes_modifie,sizeof (BOOL),nb_enregistrements (szVERIFSource, __LINE__, b_cour_mes));
	}

//---------------------------------------------------------------------------
// remise � la valeur false de tous les enregistrements
// des blocs contenant l'indicateur de modification
void CPcsVarEx::ResetBlocsExValeurModifie (void)
	{
	BOOL *pbValCourModifie;
	DWORD dwIndex;
	DWORD	dwNbEnr;

	dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_cour_log);
	for (dwIndex = 1; dwIndex <= dwNbEnr; dwIndex++)
		{
		pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_log_modifie, dwIndex);
    (*pbValCourModifie) = FALSE;
		}
	dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_cour_num);
	for (dwIndex = 1; dwIndex <= dwNbEnr; dwIndex++)
		{
		pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_num_modifie, dwIndex);
    (*pbValCourModifie) = FALSE;
		}
	dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_cour_mes);
	for (dwIndex = 1; dwIndex <= dwNbEnr; dwIndex++)
		{
		pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_mes_modifie, dwIndex);
    (*pbValCourModifie) = FALSE;
		}
	}

//------------------------------------------------------------------------------------------
// Private Comparaison pour recherche d'une variable d'entr�e sortie en exploitation d'apr�s son nom
int CPcsVarEx::ComparaisonNomVar (const void *pszNomCherche, DWORD numero)
  {
  PX_E_S px_e_s = (X_E_S *) pointe_enr (szVERIFSource, __LINE__, bx_e_s, numero);

  return StrCompare ((const char *)pszNomCherche, px_e_s->pszNomVar);
  }

//---------------------------------------------------------------------------
// Exploitation : renvoie position et bloc (b_cour_log, b_cour_num ou b_cour_mes) d'une variable - ou 0 
DWORD CPcsVarEx::dwGetVarNomEx
	(
	PCSTR pszNomVar,	// Adresse du nom de la variable recherch�e.
	DWORD * pdwBloc		// Adresse du genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	)									// Retour : 0  si var introuvable, sinon position dans b_cour de la variable
	{
  DWORD	wPosition = 0;
  DWORD	wRes = 0;

  // Trouve le nom de la variable dans bx_e_s ?
  if (trouve_position_dichotomie (pszNomVar, 1, nb_enregistrements (szVERIFSource, __LINE__, bx_e_s), ComparaisonNomVar, &wPosition))
    {
    // oui => renvoie son nloc et sa position 
		PX_E_S px_e_s = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, wPosition);

		* pdwBloc = px_e_s->wBlocGenre;
    wRes = px_e_s->wPosEx;
    }
	else
		// non => bloc genre mis � 0
		* pdwBloc = 0;

  return wRes;
	}

	//---------------------------------------------------------------------------
// Exploitation : renvoie position, nombre d'enregistrements, et bloc d'une variable - ou 0  
DWORD CPcsVarEx::dwGetVarNomTailleEx
	(
	PCSTR pszNomVar,	// Adresse du nom de la variable recherch�e.
	DWORD * pdwBloc,		// Adresse du genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	DWORD * pdwTaille	// Adresse du nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
	)									// Retour : 0  si var introuvable, sinon position dans b_cour de la variable
	{
  DWORD	wPosition=0;
  DWORD	wRes = 0;

  // Trouve le nom de la variable dans bx_e_s ?
  if ((pszNomVar && pdwBloc && pdwTaille) && (trouve_position_dichotomie (pszNomVar, 1, nb_enregistrements (szVERIFSource, __LINE__, bx_e_s), ComparaisonNomVar, &wPosition)))
    {
    // oui => renvoie son nloc et sa position 
		PX_E_S px_e_s = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, wPosition);

		* pdwBloc = px_e_s->wBlocGenre;
		* pdwTaille = px_e_s->wTaille;
    wRes = px_e_s->wPosEx;
    }
	else
		{
		// non => bloc genre mis � 0
		* pdwBloc = 0;
		}
  return wRes;
	}

//---------------------------------------------------------------------------
// Exploitation : renvoie le nombre total d'entr�es sorties - tous genres confondus
DWORD CPcsVarEx::dwGetNbGlobalVarsEx	()	// Retour : le nombre
	{
  return dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_e_s);
	}

//---------------------------------------------------------------------------
// Exploitation : renvoie des infos sur une var d'entr�es sorties - tous genres confondus
BOOL CPcsVarEx::bListeGlobalVarEx(
	DWORD dwNGlobalVarEx, // Num�ro Global de la variable d'entr�e sortie (de 1 � dwGetNbGlobalVarsEx)
	PDWORD pdwPos,				// position dans le bloc
	PDWORD pdwBloc				// bloc (b_cour_log, b_cour_num ou b_cour_mes)
	)
	{
	BOOL bTrouve = pdwPos && pdwBloc &&(dwNGlobalVarEx>0) && (dwNGlobalVarEx<=CPcsVarEx::dwGetNbGlobalVarsEx());
	if (bTrouve)
		{
		PX_E_S px_e_s = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s,dwNGlobalVarEx);
		*pdwPos = px_e_s->wPosEx;
		*pdwBloc = px_e_s->wBlocGenre;
		}
	return bTrouve;
	}

//---------------------------------------------------------------------------
// Exploitation : renvoie position, bloc (b_cour_log, b_cour_num ou b_cour_mes) d'une variable syst�me- ou 0 
DWORD CPcsVarEx::dwGetVarSysEx
	(
	DWORD dwIdVarSys,	// Id de la variable syst�me (= Position dans le bloc b_va_systeme) ex : premier_passage
	DWORD * pdwBloc		// Adresse du genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
	)									// Retour : 0  si var introuvable, sinon position dans b_cour de la variable
	{
	// R�cup�re la position de la variable syst�me dans b_courxxx
	PX_VAR_SYS pxva_sys = (X_VAR_SYS *)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, dwIdVarSys);

	// renvoie les r�sultats
	* pdwBloc = pxva_sys->wBlocGenreEx;
  return pxva_sys->dwPosCourEx;
	}

//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification
void CPcsVarEx::SetValVarLogEx (DWORD dwIndex, BOOL bVal)
	{
	BOOL *pBool = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_log, dwIndex);

	if ((*pBool) != bVal)
		{
		BOOL * pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_log_modifie, dwIndex);

		*pbValCourModifie = TRUE;
		*pBool = bVal;
		}
	}

//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification
void CPcsVarEx::SetValVarNumEx (DWORD dwIndex, FLOAT fVal)
	{
	FLOAT *pfNum = (FLOAT *)pointe_enr (szVERIFSource, __LINE__, b_cour_num, dwIndex);

	if ((*pfNum) != fVal)
		{
		BOOL *pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_num_modifie, dwIndex);

		(*pbValCourModifie) = TRUE;
		(*pfNum) = fVal;
		}
	}

//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification
void CPcsVarEx::SetValVarMesEx (DWORD dwIndex, PCSTR pszVal)
	{
	char *pszMess = (char *)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, dwIndex);

	if (!bStrEgales (pszMess, pszVal))
		{
		BOOL *pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_mes_modifie, dwIndex);

		(*pbValCourModifie) = TRUE;
		StrCopy (pszMess, pszVal);
		}
	}

//---------------------------------------------------------------------------
// Exploitation : mise � jour de la valeur d'une variable (d'apr�s sa position dans b_courXXXX)
// et de son indicateur de modification - SEULEMENT si la valeur est diff�rente

//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification - SEULEMENT si la valeur est diff�rente
BOOL CPcsVarEx::bSetValVarLogExDif (DWORD dwIndex, BOOL bVal)
	{
	BOOL bDifferent = FALSE;
	BOOL *pBool = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_log, dwIndex);

	if ((*pBool) != bVal)
		{
		BOOL * pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_log_modifie, dwIndex);

		if (*pBool != bVal)
			{
			*pbValCourModifie = TRUE;
			*pBool = bVal;
			bDifferent = TRUE;
			}
		}
	return 	bDifferent;
	}

//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification - SEULEMENT si la valeur est diff�rente
BOOL CPcsVarEx::bSetValVarNumExDif (DWORD dwIndex, FLOAT fVal)
	{
	BOOL bDifferent = FALSE;
	FLOAT *pfNum = (FLOAT *)pointe_enr (szVERIFSource, __LINE__, b_cour_num, dwIndex);

	if ((*pfNum) != fVal)
		{
		BOOL *pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_num_modifie, dwIndex);

		if ((*pfNum) != fVal)
			{
			(*pbValCourModifie) = TRUE;
			(*pfNum) = fVal;
			bDifferent = TRUE;
			}
		}
	return 	bDifferent;
	}

//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification - SEULEMENT si la valeur est diff�rente
BOOL CPcsVarEx::bSetValVarMesExDif (DWORD dwIndex, PCSTR pszVal)
	{
	BOOL bDifferent = FALSE;
	char *pszMess = (char *)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, dwIndex);

	if (!bStrEgales (pszMess, pszVal))
		{
		BOOL *pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_mes_modifie, dwIndex);

		(*pbValCourModifie) = TRUE;
		StrCopy (pszMess, pszVal);
		bDifferent = TRUE;
		}
	return 	bDifferent;
	}







//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification
void CPcsVarEx::SetValMemVarLogEx (DWORD dwIndex, BOOL bVal)
	{
	BOOL *pBool = (BOOL *)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, dwIndex);
	*pBool = bVal;
	}

//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification
void CPcsVarEx::SetValMemVarNumEx (DWORD dwIndex, FLOAT fVal)
	{
	FLOAT *pfNum = (FLOAT *)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, dwIndex);
	(*pfNum) = fVal;
	}

//---------------------------------------------------------------------------
// mise � jour du bloc b_cour et de la valeur TRUE de l'enregistrement
// du bloc contenant l'indicateur de modification
void CPcsVarEx::SetValMemVarMesEx (DWORD dwIndex, PCSTR pszVal)
	{
	char *pszMess = (char *)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes, dwIndex);
	StrCopy (pszMess, pszVal);
	}

//---------------------------------------------------------------------------
// lecture de la valeur de la variable logique courante dwIndex dans les bloc b_cour
BOOL CPcsVarEx::bGetValVarLogEx (DWORD dwIndex) // Renvoie la valeur courante
	{
	BOOL * pBool = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_log, dwIndex);

	return *pBool;
	}

//---------------------------------------------------------------------------
// lecture de la valeur num�rique courante dwIndex dans les bloc b_cour
FLOAT CPcsVarEx::fGetValVarNumEx (DWORD dwIndex) // Renvoie la valeur courante
	{
	FLOAT *pfNum = (FLOAT *)pointe_enr (szVERIFSource, __LINE__, b_cour_num, dwIndex);

	return *pfNum;
	}

//---------------------------------------------------------------------------
// lecture de la valeur message courante dwIndex dans les bloc b_cour
// pszDest est un buffer dimensionn� � au moins c_nb_car_ex_mes car dans lequel le message est recopi�
PSTR CPcsVarEx::pszGetValVarMesEx (PSTR pszDest, DWORD dwIndex) // Renvoie pszDest mis � jour avec la valeur courante
	{
	PCSTR pszMess = (PCSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, dwIndex);

	StrCopy (pszDest, pszMess);
	return pszDest;
	}


//---------------------------------------------------------------------------
// lecture de l'ancienne valeur de la variable logique courante dwIndex dans les bloc b_cour
BOOL CPcsVarEx::bGetValMemVarLogEx (DWORD dwIndex) // Renvoie l'ancienne valeur
	{
	BOOL * pBool = (BOOL *)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, dwIndex);

	return *pBool;
	}

//---------------------------------------------------------------------------
// lecture de l'ancienne valeur num�rique courante dwIndex dans les bloc b_cour
FLOAT CPcsVarEx::fGetValMemVarNumEx (DWORD dwIndex) // Renvoie l'ancienne valeur
	{
	FLOAT *pfNum = (FLOAT *)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, dwIndex);

	return *pfNum;
	}

//---------------------------------------------------------------------------
// lecture de l'ancienne valeur message courante dwIndex dans les bloc b_cour
// pszDest est un buffer dimensionn� � au moins c_nb_car_ex_mes car dans lequel le message est recopi�
PSTR CPcsVarEx::pszGetValMemVarMesEx (PSTR pszDest, DWORD dwIndex) // Renvoie pszDest mis � jour avec l'ancienne valeur
	{
	PCSTR pszMess = (PCSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes, dwIndex);

	StrCopy (pszDest, pszMess);
	return pszDest;
	}


//---------------------------------------------------------------------------
// VarSys
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Exploitation : mise � jour de la valeur d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
// et de son indicateur de modification (d'apr�s son Id dans b_va_systeme)
void CPcsVarEx::SetValVarSysLogEx (ID_VAR_SYS dwIdVarSys, BOOL bVal)
	{
	DWORD	dwBloc;
	DWORD	dwPos = dwGetVarSysEx (dwIdVarSys, &dwBloc);

	Verif (dwBloc == b_cour_log);

	CPcsVarEx::SetValVarLogEx (dwPos, bVal);
	}

//---------------------------------------------------------------------------
// Exploitation : mise � jour de la valeur d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
// et de son indicateur de modification (d'apr�s son Id dans b_va_systeme)
void CPcsVarEx::SetValVarSysNumEx (ID_VAR_SYS dwIdVarSys, FLOAT fVal)
	{
	DWORD	dwBloc;
	DWORD	dwPos = dwGetVarSysEx (dwIdVarSys, &dwBloc);

	Verif (dwBloc == b_cour_num);

	CPcsVarEx::SetValVarNumEx (dwPos, fVal);
	}

//---------------------------------------------------------------------------
// Exploitation : mise � jour de la valeur d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
// et de son indicateur de modification (d'apr�s son Id dans b_va_systeme)
void CPcsVarEx::SetBitsValVarSysNumEx (ID_VAR_SYS dwIdVarSys, DWORD dwBitsOu) // (DWORD)val |= dwBitsOu;
	{
	DWORD	dwBloc;
	DWORD	dwPos = dwGetVarSysEx (dwIdVarSys, &dwBloc);

	Verif (dwBloc == b_cour_num);

	DWORD	dwVal = dwBitsOu | ((DWORD)fGetValVarNumEx (dwPos));
	CPcsVarEx::SetValVarNumEx (dwPos, (FLOAT)dwVal);
	}

//---------------------------------------------------------------------------
// Exploitation : mise � jour de la valeur d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
// et de son indicateur de modification (d'apr�s son Id dans b_va_systeme)
void CPcsVarEx::SetValVarSysMesEx (ID_VAR_SYS dwIdVarSys, PCSTR pszVal)
	{
	DWORD	dwBloc;
	DWORD	dwPos = dwGetVarSysEx (dwIdVarSys, &dwBloc);

	Verif (dwBloc == b_cour_mes);

	CPcsVarEx::SetValVarMesEx (dwPos, pszVal);
	}

//---------------------------------------------------------------------------
// Exploitation : lecture de la valeur courante d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
BOOL	CPcsVarEx::bGetValVarSysLogEx		(ID_VAR_SYS dwIdVarSys) // Renvoie la valeur courante
	{
	DWORD	dwBloc;
	DWORD	dwPos = dwGetVarSysEx (dwIdVarSys, &dwBloc);

	Verif (dwBloc == b_cour_log);

	return CPcsVarEx::bGetValVarLogEx (dwPos);
	}

//---------------------------------------------------------------------------
// Exploitation : lecture de la valeur courante d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
FLOAT	CPcsVarEx::fGetValVarSysNumEx		(ID_VAR_SYS dwIdVarSys) // Renvoie la valeur courante
	{
	DWORD	dwBloc;
	DWORD	dwPos = dwGetVarSysEx (dwIdVarSys, &dwBloc);

	Verif (dwBloc == b_cour_num);

	return fGetValVarNumEx (dwPos);
	}

//---------------------------------------------------------------------------
// Exploitation : lecture de la valeur courante d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
// pszDest est un buffer dimensionn� � au moins c_nb_car_ex_mes car dans lequel le message est recopi�
PSTR	CPcsVarEx::pszGetValVarSysMesEx (PSTR pszDest, ID_VAR_SYS dwIdVarSys) // Renvoie pszDest mis � jour avec la valeur courante
	{
	DWORD	dwBloc;
	DWORD	dwPos = dwGetVarSysEx (dwIdVarSys, &dwBloc);

	Verif (dwBloc == b_cour_mes);

	return CPcsVarEx::pszGetValVarMesEx (pszDest, dwPos);
	}

//---------------------------------------------------------------------------
// Transfert de valeurs du bloc b_cour pour un nombre d'enregistrement donn�
// de la position source � la position destination ainsi que de la valeur TRUE de l'enregistrement
// correspondant du bloc contenant l'indicateur de modification
void CPcsVarEx::TransfertBlocCourLogEx (DWORD dwNbATransferer, DWORD dwIndexSource, DWORD dwIndexDestination)
	{
  DWORD dwIndex;

	trans_enr (szVERIFSource, __LINE__, dwNbATransferer, b_cour_log, b_cour_log, dwIndexSource, dwIndexDestination);
  for (dwIndex = 0; dwIndex < dwNbATransferer; dwIndex++)
		{
		BOOL *pBoolModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_log_modifie, dwIndexDestination + dwIndex);

		(*pBoolModifie) = TRUE;
		}
	}
//---------------------------------------------------------------------------
// Transfert de valeurs du bloc b_cour pour un nombre d'enregistrement donn�
// de la position source � la position destination ainsi que de la valeur TRUE de l'enregistrement
// correspondant du bloc contenant l'indicateur de modification
void CPcsVarEx::TransfertBlocCourNumEx (DWORD dwNbATransferer, DWORD dwIndexSource, DWORD dwIndexDestination)
	{
  DWORD dwIndex;

  trans_enr (szVERIFSource, __LINE__, dwNbATransferer, b_cour_num, b_cour_num, dwIndexSource, dwIndexDestination);
  for (dwIndex = 0; dwIndex < dwNbATransferer; dwIndex++)
		{
		BOOL *pBool = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_num_modifie, dwIndexDestination + dwIndex);

		(*pBool) = TRUE;
		}
	}
//---------------------------------------------------------------------------
// Transfert de valeurs du bloc b_cour pour un nombre d'enregistrement donn�
// de la position source � la position destination ainsi que de la valeur TRUE de l'enregistrement
// correspondant du bloc contenant l'indicateur de modification
void CPcsVarEx::TransfertBlocCourMesEx (DWORD dwNbATransferer, DWORD dwIndexSource, DWORD dwIndexDestination)
	{
  DWORD dwIndex;

  trans_enr (szVERIFSource, __LINE__, dwNbATransferer, b_cour_mes, b_cour_mes, dwIndexSource, dwIndexDestination);
  for (dwIndex = 0; dwIndex < dwNbATransferer; dwIndex++)
		{
		BOOL *pBool = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_mes_modifie, dwIndexDestination + dwIndex);

		(*pBool) = TRUE;
		}
	}

//---------------------------------------------------------------------------
// Renvoie l'indicateur de modification de la valeur de la variable dwIndex dans b_cour_log
BOOL CPcsVarEx::bGetBlocCourLogExValeurModifie (DWORD dwIndex)
	{
	BOOL *pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_log_modifie, dwIndex);

  return (*pbValCourModifie);

	}

//---------------------------------------------------------------------------
// Renvoie l'indicateur de modification de la valeur de la variable dwIndex dans b_cour_num
BOOL CPcsVarEx::bGetBlocCourNumExValeurModifie (DWORD dwIndex)
	{
	BOOL *pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_num_modifie, dwIndex);

  return (*pbValCourModifie);

	}

//---------------------------------------------------------------------------
// Renvoie l'indicateur de modification de la valeur de la variable dwIndex dans b_cour_mes
BOOL CPcsVarEx::bGetBlocCourMesExValeurModifie (DWORD dwIndex)
	{
	BOOL *pbValCourModifie = (BOOL *)pointe_enr (szVERIFSource, __LINE__, b_cour_mes_modifie, dwIndex);

  return (*pbValCourModifie);
	}

//---------------------------------------------------------------------------
// Renvoie l'indicateur de modification de la valeur de la variable dwPos dans le bloc sp�cifi�
BOOL CPcsVarEx::bGetBlocExValeurModifie
	(
	DWORD dwIndex,
	DWORD dwBloc	// b_cour_log, b_cour_num, b_cour_mes
	)
	{
	BOOL bRes = FALSE;
	switch (dwBloc)
		{
		case b_cour_log: 
			bRes = CPcsVarEx::bGetBlocCourLogExValeurModifie(dwIndex);
			break;
		case b_cour_num: 
			bRes = CPcsVarEx::bGetBlocCourNumExValeurModifie(dwIndex);
			break;
		case b_cour_mes: 
			bRes = bGetBlocCourMesExValeurModifie(dwIndex);
			break;
		default: 
			VerifWarningExit;
		}
	return bRes;
	}

//---------------------------------------------------------------------------
// Renvoie la position dans b_cour_log d'une variable logique ou 0 si un param�tre est invalide
DWORD CPcsVarEx::dwPosVarLogEx
	(
	DWORD dwPos, // position dans b_cour_log
	DWORD dwBloc,// doit �tre b_cour_log
	DWORD dwIndex// 0 pour une variable simple 1..Nb �l�ments tableau pour un tableau
	)
	{
	DWORD	dwRes = 0;

	// bloc et position compatibles avec le type ?
	if ((dwBloc == b_cour_log) && (dwPos > 0) && (dwPos <= nb_enregistrements (szVERIFSource, __LINE__, b_cour_log)))
		{
		// oui => v�rifie les autres param�tres
	  PX_E_S_COUR pxESCour = (PX_E_S_COUR)pointe_enr (szVERIFSource, __LINE__, bx_e_s_cour_log, dwPos);
		PX_E_S px_e_s = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pxESCour->dwPosDansBx_e_s);

    // V�rifie le genre de la variable et son indice
		if (c_res_logique == px_e_s->wBlocGenre)
			{
			// tableau ?
			if (px_e_s->wTaille)
				{
				// oui =>Index compatible avec la taille ?
				if ((dwIndex > 0) && (dwIndex <= px_e_s->wTaille))
					// oui => Ok : position = position de base index�e
					dwRes = (px_e_s->wPosEx) + dwIndex - 1;
				}
			else
				{
				// non => Index � 0 ?
				if (!dwIndex)
					// oui => Ok position = position de base
					dwRes = px_e_s->wPosEx;
				}
      }
		}
	return dwRes;
	} // dwPosVarLogEx

//---------------------------------------------------------------------------
// Renvoie la position dans b_cour_num d'une variable num�rique ou 0 si un param�tre est invalide
DWORD CPcsVarEx::dwPosVarNumEx
	(
	DWORD dwPos, // position dans b_cour_num
	DWORD dwBloc,// doit �tre b_cour_num
	DWORD dwIndex// 0 pour une variable simple 1..Nb �l�ments tableau pour un tableau
	)
	{
	DWORD	dwRes = 0;

	// bloc et position compatibles avec le type ?
	if ((dwBloc == b_cour_num) && (dwPos > 0) && (dwPos <= nb_enregistrements (szVERIFSource, __LINE__, b_cour_num)))
		{
		// oui => v�rifie les autres param�tres
	  PX_E_S_COUR pxESCour = (PX_E_S_COUR)pointe_enr (szVERIFSource, __LINE__, bx_e_s_cour_num, dwPos);
		PX_E_S px_e_s = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pxESCour->dwPosDansBx_e_s);

    // V�rifie le genre de la variable et son indice
		if (c_res_numerique == px_e_s->wBlocGenre)
			{
			// tableau ?
			if (px_e_s->wTaille)
				{
				// oui =>Index compatible avec la taille ?
				if ((dwIndex > 0) && (dwIndex <= px_e_s->wTaille))
					// oui => Ok : position = position de base index�e
					dwRes = (px_e_s->wPosEx) + dwIndex - 1;
				}
			else
				{
				// non => Index � 0 ?
				if (!dwIndex)
					// oui => Ok position = position de base
					dwRes = px_e_s->wPosEx;
				}
      }
		}
	return dwRes;
	} // dwPosVarNumEx

//---------------------------------------------------------------------------
// Renvoie la position dans b_cour_mes d'une variable num�rique ou 0 si un param�tre est invalide
DWORD CPcsVarEx::dwPosVarMesEx
	(
	DWORD dwPos, // position dans b_cour_mes
	DWORD dwBloc,// doit �tre b_cour_mes
	DWORD dwIndex// 0 pour une variable simple 1..Nb �l�ments tableau pour un tableau
	)
	{
	DWORD	dwRes = 0;

	// bloc et position compatibles avec le type ?
	if ((dwBloc == b_cour_mes) && (dwPos > 0) && (dwPos <= nb_enregistrements (szVERIFSource, __LINE__, b_cour_mes)))
		{
		// oui => v�rifie les autres param�tres
	  PX_E_S_COUR pxESCour = (PX_E_S_COUR)pointe_enr (szVERIFSource, __LINE__, bx_e_s_cour_mes, dwPos);
		PX_E_S px_e_s = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pxESCour->dwPosDansBx_e_s);

    // V�rifie le genre de la variable et son indice
		if (c_res_message == px_e_s->wBlocGenre)
			{
			// tableau ?
			if (px_e_s->wTaille)
				{
				// oui =>Index compatible avec la taille ?
				if ((dwIndex > 0) && (dwIndex <= px_e_s->wTaille))
					// oui => Ok : position = position de base index�e
					dwRes = (px_e_s->wPosEx) + dwIndex - 1;
				}
			else
				{
				// non => Index � 0 ?
				if (!dwIndex)
					// oui => Ok position = position de base
					dwRes = px_e_s->wPosEx;
				}
      }
		}
	return dwRes;
	} // dwPosVarMesEx

//---------------------------------------------------------------------------
// Renvoie les infos d'une Var en exploitation
BOOL CPcsVarEx::bInfoVarEx
	(
	DWORD dwPos, // position dans le bloc
	DWORD dwBloc,// bloc (b_cour_log, b_cour_num ou b_cour_mes)
	PX_E_S px_e_s	// adresse des infos de la var voulue
	)
	{
	BOOL	bRes = FALSE;
	DWORD	dwBloc_x_e_s_cour = 0;

	// bloc et position Ok?
	switch (dwBloc)
		{
		case b_cour_log : 
			dwBloc_x_e_s_cour = bx_e_s_cour_log;
			break;
		case  b_cour_num: 
			dwBloc_x_e_s_cour = bx_e_s_cour_num;
			break;
		case  b_cour_mes: 
			dwBloc_x_e_s_cour = bx_e_s_cour_mes;
			break;
		}
	if (dwBloc_x_e_s_cour && (dwPos > 0) && (dwPos <= nb_enregistrements (szVERIFSource, __LINE__, dwBloc_x_e_s_cour)))
		{
		// oui => r�cup�re les infos de la variable
	  PX_E_S_COUR pxESCour = (PX_E_S_COUR)pointe_enr (szVERIFSource, __LINE__, dwBloc_x_e_s_cour, dwPos);
		PX_E_S px_e_sTemp = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pxESCour->dwPosDansBx_e_s);

		*px_e_s = *px_e_sTemp;
		bRes = TRUE;
		}
	return bRes;
	} // bInfoVarEx

	
//------------------------------- fin Gerebdc.c -------------------------


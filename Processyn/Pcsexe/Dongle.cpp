/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : dongle.c                                                 |
 |   Auteur  : AC					        	|
 |   Date    : 16/6/93 					        	|
 |   Version : 4.00							|
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include "FMan.h"
#include "IdStrProcessyn.h"
#include "Verif.h"
VerifInit;

//----------------------------------------------------------------------------
typedef CHAR t_mes_dongle [60];

static t_mes_dongle mes_dongle [11]= 
	{ 
	"Key not found",
	"Key overwritten",
	"Key : prohibited use",
	"Wrong version for key",
	"Key : rental time execeed",
	"Application size too large for key capacity",
	"Generation and execution keys not compatible",
	"Wrong date key",
	"Wrong key",
	"Demo time execeed",
	"Key : development time execeed"
	};

static CHAR nom_sema_dongle[20] = {"cle"};
//----------------------------------------------------------------------------
#include <stdlib.h>

#include "std.h"
#include "USem.h"
#include "UStr.h"
#include "mem.h"
#include "MemMan.h"
#include "temps.h"
#include "UChrono.h"                
#include "FileMan.h"
#include "lng_res.h"
#include "tipe.h"
#include "Pcs_Sys.h"
#include "PcsVarEx.h"
#include "produits.h"
#include "USignale.h"
#include "LiSentinel.h"

#include "dongle.h"

//#include "numser"


//----------------------------------------------------------------------------
#define b_info_applic 453
#define PTR_INFO_RUNTIME 1          // indique si le .XPC � �t� g�n�r� par un dongle standard ou developpement
#define PTR_INFO_NB_VA   2					// indique le nombre de variable de l'application
#define PTR_INFO_NB_VA_RUNTIME  3		// indique la valeur du nb de variable du dongle de generation
//----------------------------------------------------------------------------
#define MIN_ID 1
#define MAX_ID RAND_MAX
#define getrandom( min, max ) ((rand() % (int)(((max)+1) - (min))) + (min))
//----------------------------------------------------------------------------

#define c_lettre_a     97  //220
#define c_lettre_c     99  //222
#define c_lettre_e     101 //224
#define c_lettre_l     108 //231
#define c_lettre_f     102 //225
#define c_lettre_g     103 //226
#define c_lettre_t     116 //239
#define c_lettre_d     100 //223
#define c_lettre_s     115 //238
#define c_lettre_r     114 //237
#define c_lettre_y     121 //244
#define c_lettre_p     112 //235
#define c_lettre_o     111 //234
#define c_lettre_m     109 //232
#define c_lettre_v     118 //241


#define  _indetermine     0


#define c_limite_mem_enseignement 80000L
#define c_limite_mem_nano         30000L
#define c_limite_mem_micro        80000L
#define c_limite_mem_gratuit      30000L

#define c_limite_var_enseignement 250
#define c_limite_var_micro        250
#define c_limite_var_nano         150

// dur�es en millisecondes
#define c_tempo_enchainement      (6 * NB_MS_PAR_SECONDE)
#define c_tempo_une_heure_trente  (90 * NB_MS_PAR_MINUTE)
#define c_tempo_une_heure         NB_MS_PAR_HEURE
#define c_tempo_24_heures         NB_MS_PAR_JOUR
#define c_tempo_deux_minutes      (2 * NB_MS_PAR_MINUTE)
#define c_tempo_dix_minutes       (10 * NB_MS_PAR_MINUTE)
#define c_tempo_trente_minutes    (30 * NB_MS_PAR_MINUTE)


#define ARRET_AU_DEMARRAGE 1
#define ARRET_EN_COURS_EXPLOITATION 2
#define CONTINUE_EN_MODE_DEMO 3
#define CONTINUE_EN_MODE_DEVELOPPEMENT 4

//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
static t_dongle *ctxt_dongle;

static int depart_perdu = 1;


//----------------------------------------------------------------------------
static void petit_pushet (void)
  {
  if (existe_repere (b_info_applic))
    {
		PDWORD ptr_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_info_applic,PTR_INFO_RUNTIME);

    (*ptr_word) = 1;
    ptr_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_info_applic,PTR_INFO_NB_VA_RUNTIME);
    (*ptr_word) = ctxt_dongle->nb_variables;
    }
  }

//----------------------------------------------------------------------------
static void toboggan (int numero)
  {
  if (numero > 0)
    {
    toboggan ((int)(numero-1));
    }
  }

//----------------------------------------------------------------------------
void spirale_n (int *i, CHAR *nom)
  {

  (*i)++;
  switch (*i)
    {
    case 112: //p
      StrInsertChar (nom, (CHAR)(*i), StrLength (nom));
      *i = 98;
      spirale_n (i, nom);
      break;

    case 99:	 //c
      StrInsertChar (nom, (CHAR)(*i), StrLength (nom));
      *i = 113;
      spirale_n (i, nom);
      break;

    case 115:  //s
      StrInsertChar (nom, (CHAR)(*i), StrLength (nom));
      *i = 0;
      spirale_n (i, nom);
      break;

    case 95:	 // _
      StrInsertChar (nom, (CHAR)(*i), StrLength (nom));
      *i = 99;
      spirale_n (i, nom);
      break;

    case 100: //d
      StrInsertChar (nom, (CHAR)(*i), StrLength (nom));
      *i = 103;
      spirale_n (i, nom);
      break;

    case 105: //i
      StrInsertChar (nom, (CHAR)(*i), StrLength (nom));
      *i = 95;
      spirale_n (i, nom);
      break;

    case 97: //a
      StrInsertChar (nom, (CHAR)(*i), StrLength (nom));
      *i = 100;
      spirale_n (i, nom);
      break;

    case 103:  //g
      StrInsertChar (nom, (CHAR)(*i), StrLength (nom));
      break;

    default:
      spirale_n (i, nom);
      break;
    }
  }

//-----------------------
#include "rand.h"

//----------------------------------------------------------------------------
static void vendu (int z, CHAR *mes, DWORD dwTypeAppel)
  {
  FLOAT tab [10];
  int i;
	char szMessUser[255];

  for (i = z; (i <= 2*z); i++)
    {
    tab[i] = (FLOAT)i;
    }
  if (i == 7)
    {
		switch (dwTypeAppel)
			{
			case ARRET_AU_DEMARRAGE:
				{
				StrSetNull (szMessUser);
				StrConcat (szMessUser, "\n"); 
				StrConcat (szMessUser, mes);
				StrConcat (szMessUser, "\n"); 
				SignaleExit (szMessUser);
				}
				break;
			
			case ARRET_EN_COURS_EXPLOITATION:
				{
				(*ctxt_dongle->fct_fermer) ();
				StrSetNull (szMessUser);
				StrConcat (szMessUser, "\n"); 
				StrConcat (szMessUser, mes);
				StrConcat (szMessUser, "\n"); 
				SignaleExit (szMessUser);
				}
				break;
			
			case CONTINUE_EN_MODE_DEMO:
				{
				StrSetNull (szMessUser);
				StrConcat (szMessUser, "\n"); 
				StrConcat (szMessUser, mes);
				StrConcat (szMessUser, ".  Running demo mode for 30 minutes.\n");
				SignaleWarnExit (szMessUser);
				}
				break;

			case CONTINUE_EN_MODE_DEVELOPPEMENT:
				{
				StrSetNull (szMessUser);
				StrConcat (szMessUser, "\n"); 
				StrConcat (szMessUser, "Running development mode for 24 hours.\n");
				SignaleWarnExit (szMessUser);
				}
				break;
			}
    }
  else
    {
    vendu (i, mes, dwTypeAppel);
    }
  }

//----------------------------------------------------------------------------
static void perdu (int i, int *index, DWORD dwTypeAppel)
  {
  t_mes_dongle mes;

  if (*index == 10)
    {
    StrSetNull (mes);
    StrCopy (mes, mes_dongle[i]);
    vendu (1, mes, dwTypeAppel);
    }
  else
    {
    (*index)++;
    perdu (i, index, dwTypeAppel);
    }
  }

//----------------------------------------------------------------------------
BOOL InitDongle (DWORD gDevID)
  {
	BOOL bOk = InitSentinel (gDevID);
	return (bOk);
  }

//----------------------------------------------------------------------------
BOOL SentinelSuperPro_acces (DWORD fct, DWORD registre, DWORD *valeur, DWORD PasseOverwrite1, DWORD PasseOverwrite2)
  {
  BOOL rretour;
	DWORD gWritePW = 0x4197;

  dwSemAttenteLibre (ctxt_dongle->semhdl, INFINITE);  // attente ressource pour acces simultan�
	rretour = SentinelSuperProAcces (fct, registre, valeur,  gWritePW, PasseOverwrite1, PasseOverwrite2);
  bSemLibere (ctxt_dongle->semhdl);  // liberation ressource pour acces simultan�
  return (rretour);
  }

//----------------------------------------------------------------------------
static void maj_modele (DWORD Action)
  {
	DWORD gDevID = 0xB4CA;

	switch (Action)
		{
		case LECTURE_CLE:
			ctxt_dongle->modele = _indetermine;
			if (InitDongle (gDevID))
				{
				SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_MODELE_DONGLE, &ctxt_dongle->modele,0,0);
				}
			break;
		case ECRITURE_CLE:
				SentinelSuperPro_acces (ECRITURE_CLE, REGISTRE_MODELE_DONGLE, &ctxt_dongle->modele,0,0);
			break;
		default:
			break;
		}
  }

//----------------------------------------------------------------------------
static void maj_fonction (DWORD Action)
  {

	switch (Action)
		{
		case LECTURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_FONCTION, &(ctxt_dongle->fonction),0,0);
					break;
				default :
					ctxt_dongle->fonction = 0;
					break;
				}
			break;
		case ECRITURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (ECRITURE_CLE, REGISTRE_FONCTION, &(ctxt_dongle->fonction),0,0);
					break;
				default :
					ctxt_dongle->fonction = 0;
					break;
				}
			break;
		default:
			break;
		}
  }

//----------------------------------------------------------------------------
static void maj_n_serie (DWORD Action, DWORD PasseOverwrite1, DWORD PasseOverwrite2)
  {

	switch (Action)
		{
		case LECTURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_NUMERO_SERIE, &(ctxt_dongle->n_serie), 0, 0);
					break;
				default :
					ctxt_dongle->n_serie = 0;
					break;
				}
			break;
		case SURECRITURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (SURECRITURE_CLE, REGISTRE_NUMERO_SERIE, &(ctxt_dongle->n_serie), PasseOverwrite1, PasseOverwrite2);
					break;
				default :
					ctxt_dongle->n_serie = 0;
					break;
				}
			break;
		default:
			break;
		}
  }

//----------------------------------------------------------------------------
static void maj_version (DWORD Action)
  {
	switch (Action)
		{
		case LECTURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_VERSION, &(ctxt_dongle->version),0,0);
					break;
				default :
					ctxt_dongle->version = 0;
					break;
				}
			break;
		case ECRITURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (ECRITURE_CLE, REGISTRE_VERSION, &(ctxt_dongle->version),0,0);
					break;
				default :
					ctxt_dongle->version = 0;
					break;
				}
			break;
		default:
			break;
		}

  }

//----------------------------------------------------------------------------
static void maj_depannage (DWORD Action)
  {

	switch (Action)
		{
		case LECTURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_DEPANNAGE, &(ctxt_dongle->nb_heures_depannage),0,0);
					break;
				default :
					ctxt_dongle->nb_heures_depannage = 0;
					break;
				}
			break;
		case ECRITURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (ECRITURE_CLE, REGISTRE_DEPANNAGE, &(ctxt_dongle->nb_heures_depannage),0,0);
					break;
				default :
					ctxt_dongle->nb_heures_depannage = 0;
					break;
				}
			break;
		default:
			break;
		}
  }

//----------------------------------------------------------------------------
static void maj_produit (DWORD Action)
  {

	switch (Action)
		{
		case LECTURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_TYPE_PRODUIT, &(ctxt_dongle->produit),0,0);
					break;
				default :
					ctxt_dongle->produit = 0;
					break;
				}
			break;
		case ECRITURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (ECRITURE_CLE, REGISTRE_TYPE_PRODUIT, &(ctxt_dongle->produit),0,0);
					break;
				default :
					ctxt_dongle->produit = 0;
					break;
				}
			break;
		default:
			break;
		}
  }

//----------------------------------------------------------------------------
static void maj_nombre_va (DWORD Action)
  {

	switch (Action)
		{
		case LECTURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_NB_VARIABLES, &(ctxt_dongle->nb_variables),0,0);
					break;
				default :
					ctxt_dongle->nb_variables = 0;
					break;
				}
			break;
		case ECRITURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (ECRITURE_CLE, REGISTRE_NB_VARIABLES, &(ctxt_dongle->nb_variables),0,0);
					break;
				default :
					ctxt_dongle->nb_variables = 0;
					break;
				}
			break;
		default:
			break;
		}
  }

//----------------------------------------------------------------------------
static void maj_date_limite( DWORD Action)
  {

	switch (Action)
		{
		case LECTURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_DATE_LIMITE, &(ctxt_dongle->date_limite),0,0);
					break;
				default :
					ctxt_dongle->date_limite = 0;
					break;
				}
			break;
		case ECRITURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (ECRITURE_CLE, REGISTRE_DATE_LIMITE, &(ctxt_dongle->date_limite),0,0);
					break;
				default :
					ctxt_dongle->date_limite = 0;
					break;
				}
			break;
		default:
			break;
		}
  }

//----------------------------------------------------------------------------
static void maj_nb_tentatives (DWORD Action)
  {

	switch (Action)
		{
		case LECTURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (LECTURE_CLE, REGISTRE_NB_TENTATIVES, &(ctxt_dongle->nb_tentatives),0,0);
					break;
				default :
					ctxt_dongle->nb_tentatives = 0;
					break;
				}
			break;
		case ECRITURE_CLE:
			switch (ctxt_dongle->modele)
				{
				case modele_SentinelSuperPro1:
					SentinelSuperPro_acces (ECRITURE_CLE, REGISTRE_NB_TENTATIVES, &(ctxt_dongle->nb_tentatives),0,0);
					break;
				default :
					ctxt_dongle->nb_tentatives = 0;
					break;
				}
			break;
		default:
			break;
		}
  }

//----------------------------------------------------------------------------
DWORD lit_nb_variables (void)
  {

  if (existe_repere (b_info_applic))
    {
		PDWORD ptr_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_info_applic,PTR_INFO_NB_VA);

    return(*ptr_word);
    }
  else
    {
    return 0;
    }
  }
//----------------------------------------------------------------------------
static BOOL appli_sd_rt (void)
  {
	BOOL bOk = FALSE;

  if (existe_repere (b_info_applic))
    {
		PDWORD ptr_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_info_applic,PTR_INFO_RUNTIME);

		if ((*ptr_word) == 1)
			{
	    ptr_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_info_applic,PTR_INFO_NB_VA_RUNTIME);
	
			bOk = ((*ptr_word) == 0) || (ctxt_dongle->nb_variables <= (*ptr_word));
			}
    }
  return(bOk);
  }
//----------------------------------------------------------------------------
static void init_info_applic(void)
  {
  DWORD *ptr_word;

	enleve_bloc (b_info_applic);
  cree_bloc (b_info_applic,sizeof(*ptr_word),3);
  ptr_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_info_applic,PTR_INFO_RUNTIME);
  (*ptr_word) = 0;
  ptr_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_info_applic,PTR_INFO_NB_VA);
	(*ptr_word) = CPcsVarEx::wGetNombreEsApplicBis ();
  ptr_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_info_applic,PTR_INFO_NB_VA_RUNTIME);
  (*ptr_word) = 0;
  }

//----------------------------------------------------------------------------
static void maj_limitations_diverses (void)
  {
  switch (ctxt_dongle->fonction)
    {
    case FONCTION_PCS_ENSEIGNEMENT:
      ctxt_dongle->limitation_temps = 0L;
      ctxt_dongle->limitation_enchainement = FALSE;
      ctxt_dongle->limitation_memoire = 0L;
      ctxt_dongle->nb_variables = c_limite_var_enseignement;
      break;
    case FONCTION_PCS_SD1:
    case FONCTION_PCS_MAINTENANCE:
      ctxt_dongle->limitation_temps = c_tempo_24_heures;
      ctxt_dongle->limitation_enchainement = TRUE;
      ctxt_dongle->limitation_memoire = 0L;
      break;
    case FONCTION_PCS_NANO:
      ctxt_dongle->limitation_temps = 0L;
      ctxt_dongle->limitation_enchainement = TRUE;
      ctxt_dongle->limitation_memoire = 0L;
      ctxt_dongle->nb_variables = c_limite_var_nano;
      break;
    case FONCTION_PCS_MICRO:
    case FONCTION_PCS_MICRO_RUNTIME:
      ctxt_dongle->limitation_temps = 0L;
      ctxt_dongle->limitation_enchainement = TRUE;
      ctxt_dongle->limitation_memoire = 0L;
      ctxt_dongle->nb_variables = c_limite_var_micro;
      break;
    case FONCTION_PCS_DEPANNAGE:
      ctxt_dongle->limitation_temps = c_tempo_une_heure;
      ctxt_dongle->limitation_enchainement = FALSE;
      ctxt_dongle->limitation_memoire = 0L;
      break;
    case FONCTION_PCS_PARAMETRAGE:
    case FONCTION_PCS_SD2:
    case FONCTION_PCS_RUN_TIME:
    case FONCTION_PCS_NORMAL:
      ctxt_dongle->limitation_temps = 0L;
      ctxt_dongle->limitation_enchainement = FALSE;
      ctxt_dongle->limitation_memoire = 0L;
      break;
    default :
      ctxt_dongle->limitation_temps = c_tempo_trente_minutes;
      ctxt_dongle->limitation_enchainement = FALSE;
      ctxt_dongle->limitation_memoire = 0L;
      break;
    }
  }

//----------------------------------------------------------------------------
static void decremente_temps_depannage (void)
  {
  maj_depannage (LECTURE_CLE);
  ecrit_dongle ((DWORD)(ctxt_dongle->nb_heures_depannage-1), REGISTRE_DEPANNAGE);
  maj_depannage (LECTURE_CLE);
  }

//----------------------------------------------------------------------------
//                        PROCEDURES PUBLIQUES
//----------------------------------------------------------------------------
DWORD ouvrir_dongle (void (*ptr_fermeture)(void))
  {
  DWORD erreur;

  ctxt_dongle = (t_dongle *)pMemAlloue (sizeof (t_dongle));
	ctxt_dongle->modele = 0;
  ctxt_dongle->n_serie = 0;
  ctxt_dongle->produit = 0;
  ctxt_dongle->fonction = 0;
  ctxt_dongle->version = 0;
  ctxt_dongle->nb_heures_depannage = 0;
  ctxt_dongle->nb_tentatives = 0;
	ctxt_dongle->nb_variables = 0;
	ctxt_dongle->date_limite = 0;
  ctxt_dongle->fct_fermer = ptr_fermeture;

	erreur = SemNommeCree (&(ctxt_dongle->semhdl), nom_sema_dongle);
	if (erreur == ERROR_ALREADY_EXISTS)
    erreur = 0;
  if (erreur == 0)
    {
    maj_modele (LECTURE_CLE);
    maj_n_serie (LECTURE_CLE, 0, 0);
    maj_produit (LECTURE_CLE);
    maj_fonction (LECTURE_CLE);
    maj_version (LECTURE_CLE);
    maj_depannage (LECTURE_CLE);
    maj_nb_tentatives (LECTURE_CLE);
		maj_nombre_va (LECTURE_CLE);
		maj_date_limite(LECTURE_CLE);
    }
  maj_limitations_diverses ();
  ctxt_dongle->n_serie_precedent = ctxt_dongle->n_serie;
  return erreur;
  }

//----------------------------------------------------------------------------
void fermer_dongle (void)
  {
  bSemFerme (&ctxt_dongle->semhdl);
  MemLibere ((PVOID *)(&ctxt_dongle));
  }

//----------------------------------------------------------------------------
void init_test_dongle (void)
  {
  maj_modele (LECTURE_CLE);
  maj_n_serie (LECTURE_CLE, 0, 0);
  maj_produit (LECTURE_CLE);
  maj_fonction (LECTURE_CLE);
  maj_version (LECTURE_CLE);
  maj_depannage (LECTURE_CLE);
  maj_nb_tentatives (LECTURE_CLE);
	maj_nombre_va (LECTURE_CLE);
	maj_date_limite(LECTURE_CLE);
  maj_limitations_diverses ();

	BOOL bOk = TRUE;
	// Limitation de date ?
  if (ctxt_dongle->date_limite != 0)
		{
		// Oui => Date courante autoris�e ?
		DWORD dwDateDuJour, Jour, Mois, Annee, JourDeSemaine, Heures, Minutes, Secondes, Centiemes;

		TpsLireDateHeure (&Jour, &Mois, &Annee, &JourDeSemaine, &Heures, &Minutes, &Secondes, &Centiemes);
		compose_date_limite (&dwDateDuJour, Jour, Mois, Annee);
		if (dwDateDuJour > ctxt_dongle->date_limite)
			{
			// Non => termin�
			bOk = FALSE;
      perdu (7, &depart_perdu, CONTINUE_EN_MODE_DEMO);
			ctxt_dongle->fonction = 0; // force a chaque demarrage le fonctionnement en demo
			}
		}

	// Continue les autres tests ?
	if (bOk)
		{
		// oui => on y va
    if ((ctxt_dongle->n_serie == 0) || (ctxt_dongle->fonction == 0)) //pas de cle
      {
      perdu (0, &depart_perdu, CONTINUE_EN_MODE_DEMO);
      }
		else
			{
			//limiter_nb_variables ();

			if ((ctxt_dongle->fonction != FONCTION_PCS_SD1) &&
				 (ctxt_dongle->fonction != FONCTION_PCS_SD2) &&
				 (ctxt_dongle->fonction != FONCTION_PCS_MAINTENANCE) &&
				 (ctxt_dongle->fonction != FONCTION_PCS_DEPANNAGE))
				{
				if (ctxt_dongle->version < (DWORD) NUM_VERSION_PROCESSYN_MAJORMINOR)
					{
					perdu (3, &depart_perdu, CONTINUE_EN_MODE_DEMO);
					ctxt_dongle->fonction = 0; // force a chaque demarrage le fonctionnement en demo
					}
				if ((ctxt_dongle->produit != (DWORD) PRODUIT_PROCESSYN_WIN32))
					{
					perdu (8, &depart_perdu, CONTINUE_EN_MODE_DEMO);
					ctxt_dongle->fonction = 0; // force a chaque demarrage  le fonctionnement en demo
					}
				}
			else
				{
				perdu (0, &depart_perdu, CONTINUE_EN_MODE_DEVELOPPEMENT);
				}
			}

		// Lancement en mode d�mo : test toutes les 30 minutes
		//MinuterieLanceMs (&(ctxt_dongle->tempo_test), c_tempo_trente_minutes);
		}
 	// Lancement en mode d�mo : test toutes les 30 minutes
	MinuterieLanceMs (&(ctxt_dongle->tempo_test), c_tempo_trente_minutes);
} // init_test_dongle

//----------------------------------------------------------------------------
void test_dongle (void)
  {
	// Est ce le moment de tester le dongle ?
  if (bEcheanceMinuterie (ctxt_dongle->tempo_test))
    {
		// oui
    maj_n_serie (LECTURE_CLE, 0, 0);
    if (ctxt_dongle->n_serie_precedent != ctxt_dongle->n_serie) // changement de cle en cours de route => plante
      {
      perdu (2, &depart_perdu, ARRET_EN_COURS_EXPLOITATION);
      }

    if ((ctxt_dongle->n_serie == 0) || (ctxt_dongle->fonction == 0)) //pas de cle ou mode force demo
      {
      perdu (9, &depart_perdu, ARRET_EN_COURS_EXPLOITATION);
      }

    if (ctxt_dongle->fonction == FONCTION_PCS_DEPANNAGE)
      {
      maj_depannage (LECTURE_CLE);
      if (ctxt_dongle->nb_heures_depannage <= 0)
        {
        perdu (4, &depart_perdu, ARRET_EN_COURS_EXPLOITATION);
        }
      }

		// Prochain test du dongle dans 10 minutes
		MinuterieLanceMs (&(ctxt_dongle->tempo_test), c_tempo_dix_minutes);
    } // if (bEcheanceMinuterie (ctxt_dongle->tempo_test))
  } // test_dongle

//----------------------------------------------------------------------------
void encrypte_xpc (void)
  {
  init_info_applic();
  if ((ctxt_dongle->fonction != FONCTION_PCS_RUN_TIME) &&
      (ctxt_dongle->fonction != FONCTION_PCS_MICRO_RUNTIME) &&
			(ctxt_dongle->fonction != 0))
    {
    petit_pushet ();
    }
  }
//----------------------------------------------------------------------------
void encrypte_hot (void)
  {
  init_info_applic();
  petit_pushet ();
  }
//----------------------------------------------------------------------------
void suit_cailloux (void)
  {

  if ((ctxt_dongle->fonction == FONCTION_PCS_RUN_TIME) ||
      (ctxt_dongle->fonction == FONCTION_PCS_MICRO_RUNTIME) )
    {
    if (!appli_sd_rt()) //cl� runtime avec xpc invalide
      {
      perdu (6, &depart_perdu, CONTINUE_EN_MODE_DEMO);
			ctxt_dongle->fonction = 0; // force le fonctionnement en demo
      }
    }
  }

//----------------------------------------------------------------------------
void limiter_temps (void)
  {
  if (ctxt_dongle->limitation_temps != 0)
    {
    MinuterieLanceMs (&(ctxt_dongle->tempo_limitation_temps), ctxt_dongle->limitation_temps);
    }
  }

//----------------------------------------------------------------------------
void limiter_memoire (void)
  {
  if ((ctxt_dongle->limitation_memoire != 0) && (ctxt_dongle->fonction != 0))
    {
    if  (long_user() > ctxt_dongle->limitation_memoire)
      {
      perdu (5, &depart_perdu, CONTINUE_EN_MODE_DEMO);
			ctxt_dongle->fonction = 0; // force le fonctionnement en demo
      }
    }
  }

//----------------------------------------------------------------------------
void limiter_nb_variables (void)
  {
  if ((ctxt_dongle->nb_variables != 0) && (ctxt_dongle->fonction != 0))
    {

    if  (lit_nb_variables () > ctxt_dongle->nb_variables)
      {
      perdu (5, &depart_perdu, CONTINUE_EN_MODE_DEMO);
			ctxt_dongle->fonction = 0; // force le fonctionnement en demo
      }
    }
  }

//----------------------------------------------------------------------------
void limiter_enchainement (void)
  {
  if (ctxt_dongle->limitation_enchainement)
    Sleep (c_tempo_enchainement);
  }

//----------------------------------------------------------------------------
BOOL limite_temps_ecoule (void)
  {
  BOOL rretour;

  if (ctxt_dongle->limitation_temps != 0)
    {
    if (ctxt_dongle->fonction != FONCTION_PCS_DEPANNAGE)
      {
      rretour = bEcheanceMinuterie (ctxt_dongle->tempo_limitation_temps);
			if (rretour)
				{
	      perdu (10, &depart_perdu, ARRET_EN_COURS_EXPLOITATION);
				}
      }
    else
      {
      if (bEcheanceMinuterie (ctxt_dongle->tempo_limitation_temps))
        {
        decremente_temps_depannage ();
        MinuterieLanceMs (&(ctxt_dongle->tempo_limitation_temps), ctxt_dongle->limitation_temps);
        }
      rretour = FALSE;
      }
    }
  else
    {
    rretour = FALSE;
    }
  return (rretour);
  }

//----------------------------------------------------------------------------
BOOL ecrit_dongle (DWORD valeur, DWORD reg)
  {
  BOOL rretour;

  switch (ctxt_dongle->modele)
    {
    case modele_SentinelSuperPro1:
      rretour = SentinelSuperPro_acces (ECRITURE_CLE, reg, &valeur,0,0);
      break;
    default:
      rretour = FALSE;
      break;
    }
  return (rretour);
  }


//----------------------------------------------------------------------------
void ecrit_tout_dongle (DWORD PasseOverwrite1, DWORD PasseOverwrite2)
  {
  switch (ctxt_dongle->modele)
    {
    case modele_SentinelSuperPro1:
			maj_modele (ECRITURE_CLE);
			maj_n_serie (SURECRITURE_CLE, PasseOverwrite1, PasseOverwrite2);
			maj_produit (ECRITURE_CLE);
			maj_fonction (ECRITURE_CLE);
			maj_version (ECRITURE_CLE);
			maj_depannage (ECRITURE_CLE);
			maj_nb_tentatives (ECRITURE_CLE);
			maj_nombre_va (ECRITURE_CLE);
			maj_date_limite(ECRITURE_CLE);
      break;
    default:
      break;
    }
  }


//----------------------------------------------------------------------------
void diagnostic_dongle (DWORD *n_fonction, DWORD *n_variable)
  {
  CHAR chaine_num[255];
  DWORD wProduit;
  DWORD wVersion;
	DWORD dwJour, dwMois, dwAnnee;
  HFIC hfic;

  int i = 0;
  CHAR nom[80];
  StrSetNull (nom);
  DWORD Heures,Minutes,Secondes,Centiemes;
  TpsLireHeure (&Heures, &Minutes, &Secondes, &Centiemes);
  toboggan ((int)((Secondes+10)*20));
  wProduit = (DWORD) PRODUIT_PROCESSYN_WIN32;
  wVersion = (DWORD) NUM_VERSION_PROCESSYN_MAJORMINOR;
  toboggan ((int)((Secondes+10)*20));
  i = 106;
  spirale_n (&i, nom);
  uFileEfface (nom);
  if (uFileTexteOuvre  (&hfic, nom, OF_DRIV_DQ)==0)
		{
		CHAR ligne[255];
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_c, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_l, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_e, StrLength (ligne));
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_t, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_y, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_p, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrConcat (ligne, pszNomModele(ctxt_dongle->modele));
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_s, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_e, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_r, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (chaine_num);
		StrSetNull (ligne);
		StrDWordToStr (chaine_num, ctxt_dongle->n_serie);
		StrConcat (ligne, chaine_num);
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_p, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_r, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_t, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrConcat (ligne, pszNomProduit(ctxt_dongle->produit));
		uFileEcrireSzLn (hfic, ligne);
		//

		StrSetNull (ligne);
		StrSetNull (chaine_num);
		StrInsertChar (ligne, (CHAR) c_lettre_m, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_o, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_d, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		(*n_fonction) = ctxt_dongle->fonction;
		StrConcat (ligne, pszNomFonction(ctxt_dongle->fonction));
		uFileEcrireSzLn (hfic, ligne);

		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_v, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_e, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_r, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrDWordToStr (ligne, ctxt_dongle->version);
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_v, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_a, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_r, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		(*n_variable) = ctxt_dongle->nb_variables;
		StrDWordToStr (ligne, ctxt_dongle->nb_variables);
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_d, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_e, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_p, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrDWordToStr (ligne, ctxt_dongle->nb_heures_depannage);
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_d, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_a, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_t, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrSetNull (chaine_num);
		decompose_date_limite (ctxt_dongle->date_limite, &dwJour, &dwMois, &dwAnnee);
		StrDWordToStr (chaine_num, dwJour);
		StrConcat (ligne, chaine_num);
		StrConcat (ligne, "/");
		StrDWordToStr (chaine_num, dwMois);
		StrConcat (ligne, chaine_num);
		StrConcat (ligne, "/");
		StrDWordToStr (chaine_num, dwAnnee);
		StrConcat (ligne, chaine_num);
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_t, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_v, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_s, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrDWordToStr (ligne, ctxt_dongle->nb_tentatives);
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrConcat (ligne, "  ");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_l, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_o, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_g, StrLength (ligne));
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrSetNull (chaine_num);
		StrInsertChar (ligne, (CHAR) c_lettre_p, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_r, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_t, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrConcat (ligne, pszNomProduit(wProduit));
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrInsertChar (ligne, (CHAR) c_lettre_v, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_e, StrLength (ligne));
		StrInsertChar (ligne, (CHAR) c_lettre_r, StrLength (ligne));
		StrConcat (ligne, ":");
		uFileEcrireSzLn (hfic, ligne);
		//
		StrSetNull (ligne);
		StrDWordToStr (ligne, wVersion);
		uFileEcrireSzLn (hfic, ligne);
		//
		uFileFerme (&hfic);
		}
  }

//----------------------------------------------------------------------------
void programmer_dongle (DWORD n_saisie, DWORD n_fonction, DWORD n_variable)
  {
  int i;
  CHAR nom[80];
  DWORD wProduit;
  DWORD wVersion;
  DWORD Heures,Minutes,Secondes,Centiemes;

  i = 0;
  StrSetNull (nom);
  TpsLireHeure (&Heures, &Minutes, &Secondes, &Centiemes);
  toboggan ((int)((Secondes+10)*20));
  wProduit = (DWORD) PRODUIT_PROCESSYN_WIN32;
  wVersion = (DWORD) NUM_VERSION_PROCESSYN_MAJORMINOR;
  toboggan ((int)((Secondes+10)*20));
  i = 106;
  spirale_n (&i, nom);
  if (ctxt_dongle->nb_tentatives == 5)   // = 5 cas ou argos2  ou argos1 initialise > a 5
    {
    perdu (1, &depart_perdu, ARRET_AU_DEMARRAGE);
    }

  // ----------------------------------------------------------------------------
  // ecriture de la version du type de produit et de l os
  // ----------------------------------------------------------------------------

  if (CalculNumeroLi (ctxt_dongle->n_serie, wVersion, wProduit, n_fonction, n_variable, 0, 0) != n_saisie)
    {
    // ----------------------------------------------------------------------------
    // ecriture du nombre de tentatives et plante -> tricheur
    // ----------------------------------------------------------------------------
    if (!ecrit_dongle ((DWORD)(ctxt_dongle->nb_tentatives+1), REGISTRE_NB_TENTATIVES))
      {
      perdu (0, &depart_perdu, ARRET_AU_DEMARRAGE);
      }
    perdu (3, &depart_perdu, ARRET_AU_DEMARRAGE);
    }
  else
    {
    // ----------------------------------------------------------------------------
    // ecriture des informations cod�es dans le produit sur le dongle
    // ----------------------------------------------------------------------------
    if (!ecrit_dongle (wProduit, REGISTRE_TYPE_PRODUIT))
      {
      perdu (0, &depart_perdu, ARRET_AU_DEMARRAGE);
      }
    if (!ecrit_dongle (wVersion, REGISTRE_VERSION))
      {
      perdu (0, &depart_perdu, ARRET_AU_DEMARRAGE);
      }
    if (!ecrit_dongle (n_fonction, REGISTRE_FONCTION))
      {
      perdu (0, &depart_perdu, ARRET_AU_DEMARRAGE);
      }
    if (!ecrit_dongle (n_variable, REGISTRE_NB_VARIABLES))
      {
      perdu (0, &depart_perdu, ARRET_AU_DEMARRAGE);
      }
    if (!ecrit_dongle (0, REGISTRE_NB_TENTATIVES))
      {
      perdu (0, &depart_perdu, ARRET_AU_DEMARRAGE);
      }
    if (!ecrit_dongle (0, REGISTRE_DATE_LIMITE))
      {
      perdu (0, &depart_perdu, ARRET_AU_DEMARRAGE);
      }
    }
  }

//----------------------------------------------------------------------------
void compose_date_limite (DWORD *pdwDateLimite, DWORD dwNumJour, DWORD dwNumMois, DWORD dwNumAnnee)
	{
	(*pdwDateLimite) = dwNumJour + (dwNumMois * 32) + ((dwNumAnnee - 1997) * (32 * 13));
	}

//----------------------------------------------------------------------------
void decompose_date_limite (DWORD dwDateLimite, DWORD *pdwNumJour, DWORD *pdwNumMois, DWORD *pdwNumAnnee)
	{
	(*pdwNumJour) = dwDateLimite % 32;
	(*pdwNumMois) = (dwDateLimite / 32) % 13;
	(*pdwNumAnnee) = 1997 + (dwDateLimite / (32 * 13));
	}

//----------------------------------------------------------------------------
t_dongle *get_dongle(void)
	{
	return (ctxt_dongle);
	}

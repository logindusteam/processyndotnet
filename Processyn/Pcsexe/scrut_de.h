/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Auteur  : LM							|
 |   Date    : 10/02/94 						|
 +----------------------------------------------------------------------*/
// Exploitation Processyn : initialisation, entr�es, sorties et fermeture DDE
// Win32 3/9/97

void initialise_de (void);
void termine_de (void);

void recoit_de (BOOL bPremier);
void emet_de   (BOOL bPremier);


//----------------------------- PARTIE SERVEUR
#define DDE_WAIT_SEM_SERVEUR 10000L

typedef struct
  {
  void   *handleClient;
  void   *handleServeur;
  BOOL bAdviseOn;
  BOOL bValInit;
  } t_dde_srv;

DWORD GetValeurBD (DWORD wPosEs, DWORD wTaille, DWORD wGenre,
                  BOOL bTestRaf, DWORD wPosEsRaf,
                  char *pszData, UINT *wSizeData,
                  BOOL bTestOld, BOOL *bAEnvoyer);

// Mise � jour valeur variables dans BD Pcsexe
DWORD PutValeurBD (DWORD wPosEs, UINT wTaille, DWORD wGenre, char *pszData);


//BOOL SrvAdviseRecu (void *handleClient, void *handleServeur, char *pszItem, DWORD *wPosRet);
//BOOL SrvRequestRecu (void *handleClient, void *handleServeur, char *pszItem, DWORD *wPosRet);
//BOOL SrvUnadviseRecu (void *handleClient, DWORD wPosRet);
//void SrvTerminateRecu (void *handleClient);

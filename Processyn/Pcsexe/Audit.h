// Audit.h: interface for the CAudit class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AUDIT_H__EC215C33_3308_4A1A_8FC0_95F3718CA36D__INCLUDED_)
#define AFX_AUDIT_H__EC215C33_3308_4A1A_8FC0_95F3718CA36D__INCLUDED_

#include "EnregistreMoniteur.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CAudit  
{
	public:
		enum eNiveauAudit
			{
			AUDIT_AUCUN = 0,
			AUDIT_ERREUR = 1,
			AUDIT_WARNING = 2,
			AUDIT_INFO = 3,
			AUDIT_DETAIL = 4
			};


public:
	CAudit();
	CAudit(PCSTR NomFichier, eNiveauAudit NiveauAudit);

	void Auditer(eNiveauAudit NiveauAudit, PCSTR pszLocalisation, CONST LONG NumeroLigne, PCSTR pszDescription = NULL);
	void AuditErreur(PCSTR pszLocalisation, CONST LONG NumeroLigne, PCSTR pszDescription = NULL) {Auditer(AUDIT_ERREUR, pszLocalisation, NumeroLigne, pszDescription);}
	void AuditWarning(PCSTR pszLocalisation, CONST LONG NumeroLigne, PCSTR pszDescription = NULL) {Auditer(AUDIT_WARNING, pszLocalisation, NumeroLigne, pszDescription);}
	void AuditInfo(PCSTR pszLocalisation, CONST LONG NumeroLigne, PCSTR pszDescription = NULL) {Auditer(AUDIT_INFO, pszLocalisation, NumeroLigne, pszDescription);}
	void AuditDetail(PCSTR pszLocalisation, CONST LONG NumeroLigne, PCSTR pszDescription = NULL) {Auditer(AUDIT_DETAIL, pszLocalisation, NumeroLigne, pszDescription);}

	void Auditer(eNiveauAudit NiveauAudit, PCSTR pszLocalisation, PCSTR pszDescription = NULL);
	void AuditErreur(PCSTR pszLocalisation, PCSTR pszDescription = NULL) {Auditer(AUDIT_ERREUR, pszLocalisation, pszDescription);}
	void AuditWarning(PCSTR pszLocalisation, PCSTR pszDescription = NULL) {Auditer(AUDIT_WARNING, pszLocalisation, pszDescription);}
	void AuditInfo(PCSTR pszLocalisation, PCSTR pszDescription = NULL) {Auditer(AUDIT_INFO, pszLocalisation, pszDescription);}
	void AuditDetail(PCSTR pszLocalisation, PCSTR pszDescription = NULL) {Auditer(AUDIT_DETAIL, pszLocalisation, pszDescription);}
	
	eNiveauAudit GetNiveauAudit() {return m_enuNiveauAudit;}
	void SetNiveauAudit(eNiveauAudit NiveauAudit);

	virtual ~CAudit();

private:
	PCSTR pszNiveauAudit(void);
	void GetHeureSysteme(LPSTR HeureSysteme);
	eNiveauAudit m_enuNiveauAudit;
	CEnregistreMoniteur *m_pEnregistreMoniteur;
};

// Instance globale de l'audit
extern CAudit Audit;

// Macro provoquant l'instanciation d'un DebugDumpVie
#define AUDIT_ERREUR(pszDescription) Audit.AuditErreur(szVERIFSource,__LINE__,pszDescription)
#define AUDIT_WARNING(pszDescription) Audit.AuditWarning(szVERIFSource,__LINE__,pszDescription)
#define AUDIT_INFO(pszDescription) Audit.AuditInfo(szVERIFSource,__LINE__,pszDescription)
#define AUDIT_DETAIL(pszDescription) Audit.AuditDetail(szVERIFSource,__LINE__,pszDescription)


#endif // !defined(AFX_AUDIT_H__EC215C33_3308_4A1A_8FC0_95F3718CA36D__INCLUDED_)

// Audit.cpp: implementation of the CAudit class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "EnregistreMoniteur.h"
#include "Audit.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
static PCSTR szNomFichierAuditInitial ="C://Audit.log";

CAudit::CAudit()
	{
	m_pEnregistreMoniteur=new CEnregistreMoniteur();
	m_pEnregistreMoniteur->NommeFichier(szNomFichierAuditInitial);
	SetNiveauAudit(AUDIT_AUCUN);
	}

CAudit::CAudit(PCSTR NomFichier, eNiveauAudit NiveauAudit)
	{
	m_pEnregistreMoniteur=new CEnregistreMoniteur();
	m_pEnregistreMoniteur->NommeFichier(NomFichier);
	SetNiveauAudit(NiveauAudit);
	}

CAudit::~CAudit()
	{
	m_pEnregistreMoniteur->SetEnregistrement(FALSE);
	delete m_pEnregistreMoniteur;
	}


void CAudit::SetNiveauAudit(eNiveauAudit NiveauAudit)
	{
	m_enuNiveauAudit=NiveauAudit;
	if (NiveauAudit!=AUDIT_AUCUN)
		{
		m_pEnregistreMoniteur->SetEnregistrement(TRUE);
		}
	else
		{
		m_pEnregistreMoniteur->SetEnregistrement(FALSE);
		}
	}


void CAudit::Auditer(eNiveauAudit NiveauAudit, PCSTR pszLocalisation, PCSTR pszDescription)
	{
	BOOL bOnAudite=(m_pEnregistreMoniteur->bEnregistrement())
		&&
		((long)NiveauAudit<=(long)m_enuNiveauAudit);
	
	if (bOnAudite)
		{
		//Format des donn�es
		char szAudit[500]="";
		if (pszDescription)
			wsprintf(szAudit, "%s;%s;%s", pszNiveauAudit(), pszLocalisation, pszDescription);
		else
			wsprintf(szAudit, "%s;%s;%s", pszNiveauAudit(), pszLocalisation);

		//Enregistre les donn�es de l'audit
		m_pEnregistreMoniteur->EnregistreDonnees(szAudit);
		}
	}

void CAudit::Auditer(eNiveauAudit NiveauAudit, PCSTR pszLocalisation, CONST LONG NumeroLigne, PCSTR pszDescription)
	{
	BOOL bOnAudite=(m_pEnregistreMoniteur->bEnregistrement())
		&&
		((long)NiveauAudit<=(long)m_enuNiveauAudit);
	
	DWORD dwNCharFichier=0;
	if (bOnAudite)
		{
		for (dwNCharFichier = strlen(pszLocalisation);dwNCharFichier>=0;dwNCharFichier--)
			{
			if (pszLocalisation[dwNCharFichier] == '\\')
				break;
			}

		//Format des donn�es
		char szAudit[500]="";
		if (pszDescription)
			wsprintf(szAudit, "%s;%s(%i);%s", pszNiveauAudit(), &pszLocalisation[dwNCharFichier], NumeroLigne, pszDescription);
		else
			wsprintf(szAudit, "%s;%s(%i)", pszNiveauAudit(), &pszLocalisation[dwNCharFichier], NumeroLigne);

		//Enregistre les donn�es de l'audit
		m_pEnregistreMoniteur->EnregistreDonnees(szAudit);
		}
	}

PCSTR CAudit::pszNiveauAudit(void)
	{
	PCSTR pszRet = NULL;
	switch (m_enuNiveauAudit)
		{
		case AUDIT_AUCUN:
			pszRet = "None";
			break;
		case AUDIT_ERREUR:
			pszRet = "Error";
			break;
		case AUDIT_WARNING:
			pszRet = "Warning";
			break;
		case AUDIT_INFO:
			pszRet = "Info";
			break;
		case AUDIT_DETAIL:
			pszRet = "Detail";
			break;
		default:
			pszRet = "Invalid Audit Level";
		}
	return pszRet;
	}


// Gestion Win32 de queues FIFO
// n�cessite include "USem.h" pour HSEM
// RESTRICTIONS : 
//	pas d'IPC
// Erreurs renvoy�es = erreurs OS

// handles de queue
DECLARE_HANDLE (HQUEUE);

typedef HQUEUE * PHQUEUE;
#define INVALID_HQUEUE NULL

//------------------------------------------------------------------------------
// hCreeQueue Cr�e et ouvre une queue accessible par ce seul process.
// Renvoie le handle de queue si Ok - sinon NULL.
HQUEUE hCreeQueue
	(
	PCSTR pszQueueName		// pointer to queue name
	);

//------------------------------------------------------------------------------
// Ferme et d�truit la queue, et met le handle de queue � NULL
BOOL bFermeQueue (PHQUEUE phq);

//------------------------------------------------------------------------------
// Supprime tous les �l�ments dans la queue sp�cifi�e
BOOL bVideQueue (HQUEUE hq);

//------------------------------------------------------------------------------
// Ecriture d'un �lement dans la queue sp�cifi�e.
// Apr�s que l'�l�ment sp�cifi� soit �crit, le process propri�taire de la queue
// peut lire l'�l�ment � l'aide de uLitQueue
BOOL bEcritQueue
	(
	HQUEUE hqueue,    // handle de la queue cible
	UINT	uiUser,			// valeur 32 bit associ�e � l'�l�ment
	DWORD cbBuf,      // Taille en octets de l'�l�ment � ajouter � la pile
	PVOID pbBuf       // Adresse du buffer contenant l'�l�ment � �crire
	);

//------------------------------------------------------------------------------
// Renvoie le plus ancien des �l�ments restants dans la queue.
// Si la queue est vide, retourne imm�diatement ou attend l'arriv�e 
// du prochain �l�ment selon bAttente.
// Le buffer r�cup�r� sera lib�r� (free) par le lecteur.
// Seul le process qui a cr�� la queue utilisera uLitQueue.
// Renvoie 0 si Ok sinon ERROR_HANDLE_EOF ou ERROR_INVALID_HANDLE
UINT uLitQueue
	(
	HQUEUE	hqueue,				// handle de la queue cible
	UINT *	puiUser,	// adresse d'une variable pour r�cup�rer la valeur 32 bit associ�e � l'�l�ment ou NULL
	DWORD * pcbData,	// adresse de la variable pour r�cup�rer la longueur de l'�l�ment
	PVOID * ppbuf,		// Adresse d'un pointeur pour r�cup�rer l'�lement
	BOOL		bAttente,	// Attente illimit�e de l'arriv�e d'un �l�ment si TRUE, pas d'attente sinon
	PHSEM		phsem			// adresse pour r�cup�rer un handle de s�maphore libre d�s l'arriv�e d'un �l�ment ou NULL
	);

//------------------------------------------------------------------------------
// R�cup�re le s�maphore d'attente d'au moins un �l�ment de la queue
BOOL bQueueGetSemAttenteElement
	(HQUEUE hqueue,		// handle de la queue voulue
	PHSEM phsemDest);	// adresse pour recopie du s�maphore de pr�sence d'un �l�ment dans la queue
										// Renvoie TRUE si le handle de s�maphore est recopi�, FALSE sinon

//------------------- fin QueueMan.h --------------------------------------









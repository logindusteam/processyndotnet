/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |---------------------------------------------------------------------*/
// scrut_opc.h
// Unite de Scrutation client OPC
// Win32 30/3/98/JS

//----------------------------------------------
// Initialise et termine l'ex�cution du client OPC
//----------------------------------------------
void  initialise_opc (void);
void	ferme_opc (void);

// ------------------------------------
// interface scrutation client OPC
// ------------------------------------
void  recoit_opc (BOOL bPremier);
void  emet_opc   (BOOL bPremier);





	

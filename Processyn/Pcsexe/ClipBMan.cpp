//----------------------------------------------
// ClipBMan.c
// Fonctions li�es au presse papier syst�me
// Version WIN32
//----------------------------------------------
#include "stdafx.h"
#include "UStr.h"
#include "MemMan.h"
#include "ClipBMan.h"

//-----------------------------------------------------------------
// Remplace le contenu du presse papier par le contenu d'un buffer
static BOOL bClipBEcritBuf 
	(
	UINT uCF,						// Format des donn�es � mettre dans le presse papier CF_XX
	const void * pBuf,	// Buffer 
	DWORD dwTailleBuf,	// Taille du buffer (en octets)
	BOOL	bVideLePressePapierAvant
	)
	{
	BOOL	bOk = FALSE;

	// Ouverture Presse papier (pas de fen�tre propri�taire) Ok ?
	if (OpenClipboard (NULL))
		{
		// oui => pas de probl�me du � une �ventuelle vidange du presse papier ?
		if (!bVideLePressePapierAvant || EmptyClipboard ())
			{
			HGLOBAL	hMem;

			// oui => copie de la source :
			// Cr�e un handle m�moire � partir du texte ?
			if (hMem = GlobalAlloc (GMEM_MOVEABLE | GMEM_DDESHARE, dwTailleBuf))
				{
				PVOID pBufDest = GlobalLock (hMem);

				// Copie la source vers l'objet m�moire
				CopyMemory (pBufDest, pBuf, dwTailleBuf);

				// Lib�re l'objet du presse papier
				GlobalUnlock (hMem);

				// handle m�moire mis dans le presse papier ?
				if (SetClipboardData (uCF, hMem))
					bOk = TRUE;
				else
					// non => on lib�re l'objet
					GlobalFree (hMem);
				}
			}

		// Fin de l'acc�s exclusif au presse papier
		CloseClipboard();
		}

	return bOk;
	} // bClipBEcritBuf

//------------------------------------------------------------------------
// R�cup�re le buffer de donn�es du presse papier dans le format sp�cifi�
static void * pClipBLitBuf 
	(
	UINT		uCF,						// Format des donn�es � lire dans le presse papier CF_XX
	DWORD * pdwTailleObjet	// Taille (�ventuellement major�e) des donn�es
	)												// Renvoie un pointeur sur un buffer allou� par pMemAlloue
	{
	void * pRes = NULL;

	// Format disponible et ouverture Presse papier Ok ?
	if (IsClipboardFormatAvailable (uCF) && OpenClipboard (NULL))
		{
		// oui => r�cup�re le contenu du presse papier au bon format ?
		HGLOBAL	hMem = GetClipboardData (uCF);

		if (hMem)
			{
			// oui => duplique le buffer afin qu'il puisse �tre lib�r� par un simple free :
			// R�cup�re correctement les donn�es de l'objet ?
			PVOID pBuf = GlobalLock (hMem);
			*pdwTailleObjet = GlobalSize (hMem);

			if (pBuf && (*pdwTailleObjet))
				{
				// oui => alloue et init le Buffer r�sultat � partir de l'objet du presse papier
				pRes = pMemAlloueInit (*pdwTailleObjet, pBuf);

				// Lib�re l'objet du presse papier
				GlobalUnlock (hMem);
				}
			}

		// Fin de l'acc�s exclusif au presse papier
		CloseClipboard();
		}

	return pRes;
	} // pClipBLitBuf

//----------------------------------------------
// Vide le presse papier puis y place la donn�e au format sp�cifi�
BOOL bClipBEcritFormat (UINT uFormat, PVOID pBuf, DWORD dwTailleBuf)
	{
	return bClipBEcritBuf (uFormat, pBuf, dwTailleBuf, TRUE);
	}

//----------------------------------------------
// Vide le presse papier puis y place le texte sp�cifi�
BOOL bClipBEcritTexte (PCSTR pszSource)
	{
	return bClipBEcritBuf (CF_TEXT, pszSource, 1 + StrLength (pszSource), TRUE);
	}

//----------------------------------------------
// Ajoute le texte sp�cifi� au presse papier
BOOL bClipBAjouteTexte (PCSTR pszSource)
	{
	return bClipBEcritBuf (CF_TEXT, pszSource, 1 + StrLength (pszSource), FALSE);
	}

//----------------------------------------------
// Ajoute la donn�e au format sp�cifi� au presse papier
BOOL bClipBAjouteFormat (UINT uFormat, PVOID pBuf, DWORD dwTailleBuf)
	{
	return bClipBEcritBuf (uFormat, pBuf, dwTailleBuf, FALSE);
	}

//----------------------------------------------
// R�cup�re le texte du presse papier si pr�sent
// Le buffer peut �ventuellement �tre surdimensionn�
// ATTENTION : faire un MemLibere apr�s utilisation du pointeur retourn�
PSTR pszClipBLitTexte (void)
	{
	DWORD	dwTailleObjet;

	return (PSTR)pClipBLitBuf (CF_TEXT, &dwTailleObjet);
	}

//----------------------------------------------
// R�cup�re la donn�e au format sp�cifi� pr�sente dans le presse papier
// ATTENTION : faire un MemLibere apr�s utilisation du pointeur retourn�
BOOL bClipBLitFormat (UINT uFormat, PVOID * ppBuf, DWORD * pdwTailleBuf)
	{
	*ppBuf = pClipBLitBuf (uFormat, pdwTailleBuf);
	return (*ppBuf) != NULL;
	}


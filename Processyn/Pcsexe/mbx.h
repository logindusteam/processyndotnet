// Codes d'erreurs renvoy�es
#define MBX_ERR_MBX_IS_IN_USE      1
#define MBX_ERR_NO_MORE_HDL        2
#define MBX_ERR_MBX_UNKOWN_STATE   3
#define MBX_ERR_MBX_NOT_IN_USE     4
#define MBX_ERR_USR_BUSY           5
#define MBX_ERR_TIMEOUT            6
#define MBX_ERR_WAIT_EVENT         7
#define MBX_ERR_MBX_OPEN_WAIT      8
#define MBX_ERR_MBX_ALREADY_CLOSED 9
#define MBX_ERR_MBX_NOT_CONNECTED  10
#define MBX_ERR_PUT_MSG            11
#define MBX_ERR_MBX_NOT_COMM_STATE 12
#define MBX_NO_MSG                 13
#define MBX_ERR_MBX_READ_MSG       14
#define MBX_ERR_MBX_DISCONNECTED   15
#define MBX_ERR_SEND_RQU           16
#define MBX_ERR_ALLOC              17
#define MBX_ERR_INVALID_PARAM      18

#define MBX_INDEFINITE_WAIT        INFINITE
#define MBX_DEF_ACK_TIMEOUT        120000 //$$6400 // timeout d'attente d'accuse reception par defaut

// Ids messages
#define MBX_MSG_OUVRE    1
#define MBX_MSG_FERME    2
#define MBX_MSG_USER     3

// entete d'un message transmis
typedef struct
	{
	DWORD IdMessage;				// Ids messages
	DWORD dwTailleDonnees;	// Taille en octets du message, ce Header non compris
	} MBX_HEADER_MESSAGE, *PMBX_HEADER_MESSAGE;		

// Messages transmis
typedef struct
  {
  MBX_HEADER_MESSAGE  Entete;
  BYTE								aDonnees[1]; // partie utilisateur de 0 � n octets
  } MESSAGE_MBX, *PMESSAGE_MBX;

// type utilisateur
#define MBX_USR_MAX_NAME 32

// Handle de connexion client serveur
DECLARE_HANDLE (HCNX);
#define INVALID_HCNX NULL

//-------------------------------------------------------------------------
// Cr�ation d'une connexion serveur de communication.
// Si un client est en attente de connexion on ouvre la communication avec lui.
// Sinon, on attend au plus dwTimeOutAttente qu'il soit connect�.
// Si la connexion n'a pas pu �tre �tablie pendant cette la dur�e, un appel ulterieur � la fonction
// bCnxAUnCorrespondant pourra determiner si l'appel a abouti.
// L'attente d'appel court jusqu' � l'�tablissement de la connexion ou un appel � dwMbxFermeConnexion
// Codes de retour :                                                      
//    0                         : la fonction s'est bien deroulee         
// MBX_ERR_SEND_RQU             : erreur dans l'envoie de la demande      
// MBX_ERR_MBX_OPEN_WAIT        : canal est deja en attente d'ouverture   
// MBX_ERR_MBX_ALREADY_OPEN     : canal est deja ouvert                   
// MBX_ERR_MBX_NOT_CONNECTED    : canal non enregistre                    
// MBX_ERR_MBX_UNKOWN_STATE     : canal en derangement                    
// MBX_ERR_WAIT_EVENT           : erreur attente d'ouverture              
//------------------------------------------------------------------------
DWORD dwCnxCreeServeur 
	(
	HCNX * phcnx,								// adresse du handle de connexion � cr�er
	const char * pszNomServeur,	// Nom du serveur support�.
	const char * pszNomTopic		// Nom du service support�.
	);

//-------------------------------------------------------------------------
// Cr�ation d'une connexion client de communication.
// Si un serveur est en attente de connexion on ouvre la communication avec lui.
// Sinon, on attend au plus dwTimeOutAttente qu'il soit connect�.
// Si la connexion n'a pas pu �tre �tablie pendant cette la dur�e, un appel ulterieur � la fonction
// bCnxAUnCorrespondant pourra determiner si l'appel a abouti.
// L'attente d'appel court jusqu' � l'�tablissement de la connexion ou un appel � dwMbxFermeConnexion
// Renvoi :                                                     
//    0                         : la fonction s'est bien deroulee        
// MBX_ERR_TIMEOUT             : pas de connexion �tablie
// ...
// -----------------------------------------------------------------------
DWORD dwMbxAppelServeur
	(
	HCNX *	phcnx,								// adresse du handle de connexion cr��
	const char *pszNomServeur,		// Nom du serveur auquel on veut se connecter
	const char *pszNomTopic,			// Nom du service auquel on veut se connecter
	DWORD dwTimeOutAttente				// Dur�e d'attente max de la connexion en ms
	);

//--------------------------------------------------------------------------
// Savoir si une connexion est reli�e � un correspondant
// Renvoie TRUE si c'est le cas, FALSE sinon
//--------------------------------------------------------------------------
BOOL bCnxAUnCorrespondant
	(
	HCNX hcnx,								// handle de connexion � tester
	DWORD dwTimeOutAttente		// Time out max d'attente de la connexion en ms ou MBX_INDEFINITE_WAIT
	);

//-------------------------------------------------------------------------
// Ferme une connexion et d�connecte l'�ventuel correspondant
// Renvoie 0 si la fermeture s'est bien d�roul�e
//-------------------------------------------------------------------------
DWORD dwMbxFermeConnexion	(HCNX * phcnx); // adresse du handle de connexion � fermer (mis � NULL si fermeture)

//----------------------------------------------------------------------
// Envoi d'un message � un correspondant.
// $$ Le buffer de donn�es doit commencer par une structure MBX_HEADER_MESSAGE
// (la valeur de ses champs sera mise � jour par cette fonction)
// Codes de retour :                                                    
//    0                        : la fonction s'est bien deroulee        
// MBX_ERR_PUT_MSG             : erreur lors de l'envoi du message        
// MBX_ERR_MBX_NOT_COMM_STATE  : pas de correspondant connect�
//-----------------------------------------------------------------------
DWORD dwMbxEcriture
	(
	HCNX			hcnx,							// handle de connexion � utiliser
	PMBX_HEADER_MESSAGE	pBufMessage,	// adresse du header contenant les donn�es a envoyer suivies par les donn�es
	DWORD			dwTailleMessage,	// taille du buffer 
	BOOL			bDemandeAR				// active l'attente d'Accus� de R�ception si TRUE
	);

//----------------------------------------------------------------------
// Envoi d'un message Quelconque � un correspondant.
// Le buffer de donn�es NE NECESSITE PAS de commencer par une structure MBX_HEADER_MESSAGE
//-----------------------------------------------------------------------
DWORD dwMbxEcritureBuffer
	(
	HCNX			hcnx,							// handle de connexion mailbox � utiliser
	PVOID			pBufMessage,			// adresse du header contenant les donn�es a envoyer suivies par les donn�es
	DWORD			dwTailleMessage,	// taille du buffer 
	BOOL			bDemandeAR				// active l'attente d'Accus� de R�ception si TRUE
	);

//--------------------------------------------------------------------------
// R�ception d'un message syst�me ou issu du correspondant.
// Les messages sont au format : MBX_HEADER_MESSAGE  +  donn�es
// L'entete du message contient notamment l'id du message valant
// MBX_MSG_USER  : message utilisateur (datas libres)
// MBX_MSG_FERME : fermeture de la communication
// MBX_MSG_OUVRE : ouverture de la communication
//                                                                          
// Codes de retour :                                                        
//    0                    : la fonction s'est bien deroulee                
// MBX_ERR_MBX_READ_MSG    : erreur de lecture message                      
// MBX_ERR_TIMEOUT         : sortie par time out                            
// MBX_ERR_WAIT_EVENT      : erreur lors attente evenement                  
// MBX_ERR_MBX_DISCONNECTED: mbx a ete deconnecte pendant l'attente         
// MBX_NO_MSG              : retourne en mode non blocante et pas de message
//--------------------------------------------------------------------------
DWORD dwMbxLecture 
	(
	HCNX	hcnx,					// handle de connexion � utiliser
	PVOID * ppBufDest,			// adresse du pointeur sur le buffer recevant les donn�es lues
	BOOL	bAvecAttente,	// Attente avec time out courant si TRUE, pas d'attente sinon
	BOOL	bCopieBuffer	// Si TRUE recopie le message initial, sinon donne le message initial � lib�rer
	);

//--------------------------------------------------------------------------
// Renvoie le s�maphore d'attente de r�ception de la connexion, NULL si erreur
// Il peut �tre utilis� pour attendre une r�ception de message (connexion, r�ception, d�connexion)
//--------------------------------------------------------------------------
HSEM hsemCnxAttenteReception (HCNX	hcnx);			// handle de connexion � utiliser

//------------------------------------------------------------------------
// Envoi d'un accuse de reception au correspondant connect�. 
// Le correspondant peut attendre l'AR avec dwMbxAttenteAR
// Les AR ne sont pas empil�s pour chaque correspondant.
// Codes de retour :
//   0 : la fonction s'est bien deroulee.
//   MBX_ERR_SEND_RQU : erreur dans l'envoie de la requete
//   MBX_ERR_MBX_NOT_COMM_STATE : canal n'est pas en mode de communication
//------------------------------------------------------------------------
DWORD dwMbxEnvoiAR 
	(
	HCNX hcnx	// handle de connexion � utiliser
	);

//-------------------------------------------------------------------------
// Attente d'un accus� de reception sur une connexion pendant au plus timeout ms.
// Codes de retour :                                                      
//   0 : la fonction s'est bien deroulee.                                 
//   MBX_ERR_MBX_NOT_COMM_STATE : canal n'est pas en mode de communication
//   MBX_ERR_TIMEOUT : l'ack n'est pas arrive au bout du timeout          
//   MBX_ERR_WAIT_EVENT : une erreur systeme est arrive pendant l'attente 
//-------------------------------------------------------------------------
DWORD dwMbxAttenteAR 
	(
	HCNX hcnx,							// handle de connexion � utiliser
	DWORD dwTimeOutAttente	// Time out max d'attente de l'accus� de r�ception (ms) ou MBX_INDEFINITE_WAIT
	);

//----------------------------- fin Mbx.h -------------------------------


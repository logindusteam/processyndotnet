/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : DRIV_DIF.c                                             |
 |   Auteur  : Patrice							|
 |   Date    : 21/08/94							|
 |   Version :                                                          |
 |                                                                      |
 |   +--------+--------+---+--------------------------------------+     |
 |   | date   | qui    |no | raisons                              |     |
 |   +--------+--------+---+--------------------------------------+     |
 |   |20/09/91| JCL    | 5 | Integration tableaux                 |     |
 |   |        |        |   |                                      |     |
 |   +--------+--------+---+--------------------------------------+     |
 |                                                                      |
 |   Remarques :  PCS et LIFT                                           |
 |                                                                      |
 +----------------------------------------------------------------------*/


#include "stdafx.h"
#include "std.h"
#include "driv_dq.h"
#include "lng_res.h"
#include "tipe.h"
#include "UStr.h"
#include "mem.h"
#include "tipe_dq.h"
#include "MemMan.h"
#include "DocMan.h"

#include "driv_dif.h"
#include "Verif.h"
VerifInit;

#define TAILLE_MESS 84

static DWORD nb_enr_t;
static DWORD enr_cour;
static HFDQ hdl_local;
static HFDQ hFicOrigOuDest;


//---------------------------------------------------------------------------
static PSTR StrCToPascal (PSTR pStrDst, PCSTR pStrSrc)
  {
  DWORD wIdx;
  CHAR cSrc;
  CHAR cTmp;

  wIdx = 0;
  cSrc = pStrSrc [wIdx];
  while (cSrc != '\0')
    {
    wIdx++;
    cTmp = pStrSrc [wIdx];
    pStrDst [wIdx] = cSrc;
    cSrc = cTmp;
    }
  pStrDst [0] = (CHAR) wIdx;

  return (pStrDst);
  }

//---------------------------------------------------------------------------
static PSTR StrPascalToC (PSTR pStrDst, PCSTR pStrSrc)
  {
  DWORD wIdxEnd;
  DWORD wIdx;

  wIdxEnd = (DWORD) pStrSrc [0];
  wIdx = 0;
  while (wIdx < wIdxEnd)
    {
    pStrDst [wIdx] = pStrSrc [wIdx + 1];
    wIdx++;
    }
  pStrDst [wIdx] = '\0';

  return (pStrDst);
  }


//----------------------------------------------------------------------------
static void* inc_ptr (void* ptrsource,int offset)
  {
  PBYTE ptrbyte = (PBYTE)ptrsource;

  return ptrbyte + offset;
  }

//----------------------------------------------------------------------------
static DWORD trans_pcs_dif (char *fic_source, char *fic_destination, DWORD num_fichier)
	{
	PFICHIER_DQ ads_fichier;
	t_tableau_vars_fichiers*ads_var;
	BOOL *ads_fen_log;
	PFLOAT ads_courante;
	PFLOAT ads_courante_deb;
	FLOAT *ads_fen_num;
	char *ads_fen_mes;
	DWORD numero_err;
	DWORD i;
	DWORD nb_enr_source;
	DWORD nb_rubrique;
	DWORD taille_enr;
	HFDQ hBidon;
	char lng_tampon1[TAILLE_MESS];
	char lng_tampon2[TAILLE_MESS];
	DWORD j;
	DWORD nb_enr_cour;
	
	numero_err = 0;
	ads_fichier = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,num_fichier);
	nb_rubrique = ads_fichier->pos_fin_var - ads_fichier->pos_debut_var + 1;
	i = ads_fichier->pos_debut_var - 1;
	taille_enr = 0;
	while (i < ads_fichier->pos_fin_var)
		{
		i = i + 1;
		ads_var = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq,i);
		if (ads_var->taille != 0)
			{
			switch (ads_var->genre)
				{
				case c_res_logique	:
					taille_enr =	taille_enr + (2 * ads_var->taille);
					break;
					
				case c_res_numerique :
					taille_enr =	taille_enr + (4 * ads_var->taille);
					break;
					
				case c_res_message   :
					taille_enr =  taille_enr + (82 * ads_var->taille);
					break;
				} // Switch
			} // (ads_var->taille != 0)
		else
			{
			switch (ads_var->genre)
				{
				case c_res_logique   :
					taille_enr =	taille_enr + 2;
					break;
					
				case c_res_numerique :
					taille_enr =	taille_enr + 4;
					break;
					
				case c_res_message :
					taille_enr =  taille_enr + 82;
					break;
				} // switch (ads_var.genre
			} // (ads_var->taille != 0)
		} // fin while
	
	if (!bFicOuvert(&hBidon, fic_source))
		{
		// Reservation Buffer Ecriture (pour remplacer le pointe_fenetre)
		ads_courante_deb = (PFLOAT)pMemAlloue(taille_enr);
		
		uFicOuvre(&hdl_local, fic_source, taille_enr); // $$ err !!!
		uFicNbEnr(hdl_local, &nb_enr_source);
		nb_enr_t = nb_enr_source;
		if (nb_enr_source <= 0)
			{
			numero_err = 128;
			}
		else
			{
			uFicEfface (fic_destination);
			
			// Ouverture Ok ?
			if (uFicTexteOuvre  (&hFicOrigOuDest, fic_destination) == 0)
				{
				// oui => on l'�crit
				uFicEcrireLn (hFicOrigOuDest, "TABLE");
				uFicEcrireLn (hFicOrigOuDest, "0,1");
				uFicEcrireLn (hFicOrigOuDest, "\"\"");
				uFicEcrireLn (hFicOrigOuDest, "TUPLES");
				
				StrCopy (lng_tampon1, "0,");
				StrLONGToStr (lng_tampon2, nb_enr_source);
				StrConcat (lng_tampon1,lng_tampon2);
				uFicEcrireLn (hFicOrigOuDest, lng_tampon1);
				
				uFicEcrireLn (hFicOrigOuDest, "\"\"");
				uFicEcrireLn (hFicOrigOuDest, "VECTORS");
				
				StrCopy (lng_tampon1, "0,");
				StrLONGToStr (lng_tampon2, nb_rubrique);
				StrConcat (lng_tampon1,lng_tampon2);
				uFicEcrireLn (hFicOrigOuDest, lng_tampon1);
				
				uFicEcrireLn (hFicOrigOuDest, "\"\"");
				uFicEcrireLn (hFicOrigOuDest, "DATA");
				uFicEcrireLn (hFicOrigOuDest, "0,0");
				uFicEcrireLn (hFicOrigOuDest, "\"\"");
				for (nb_enr_cour=1;nb_enr_cour<=nb_enr_source;nb_enr_cour++)
					{
					ads_courante = ads_courante_deb;
					
					uFicLireTout(hdl_local,ads_courante, 1);
					ads_fichier = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,num_fichier);
					i = ads_fichier->pos_debut_var - 1;
					uFicEcrireLn (hFicOrigOuDest, "-1,0");
					uFicEcrireLn (hFicOrigOuDest, "BOT");
					while (i < ads_fichier->pos_fin_var)
						{
						i= i + 1;
						enr_cour = i;
						ads_var = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq,i);
						if (ads_var->taille != 0)
							{
							for (j=1;j<=ads_var->taille;j++)
								{
								switch (ads_var->genre)
									{
									case c_res_logique   :
										ads_fen_log = (BOOL*) ads_courante;
										if ((*ads_fen_log))
											{
											uFicEcrireLn (hFicOrigOuDest, "0,1");
											}
										else
											{
											uFicEcrireLn (hFicOrigOuDest, "0,0");
											}
										StrBOOLToStr (lng_tampon1, *ads_fen_log, "FALSE","TRUE");
										uFicEcrireLn (hFicOrigOuDest, lng_tampon1);
										ads_courante = (PFLOAT)inc_ptr (ads_courante,2);
										break;
										
									case c_res_numerique   :
										ads_fen_num = (FLOAT*) ads_courante;
										StrCopy (lng_tampon1, "0,");
										StrPrintFormat (lng_tampon2, "%.4f", *ads_courante);
										StrConcat (lng_tampon1,lng_tampon2);
										uFicEcrireLn (hFicOrigOuDest, lng_tampon1);
										uFicEcrireLn (hFicOrigOuDest,"V");
										ads_courante = (PFLOAT)inc_ptr (ads_courante,4);
										break;
										
									case c_res_message   :
										ads_fen_mes = (char*) ads_courante;
										uFicEcrireLn (hFicOrigOuDest,"1,0");
										StrCopy (lng_tampon1, "\"");
										StrPascalToC (lng_tampon2, ads_fen_mes);
										StrConcat (lng_tampon1,lng_tampon2);
										StrConcat (lng_tampon1, "\"");
										uFicEcrireLn (hFicOrigOuDest, lng_tampon1);
										ads_courante = (PFLOAT)inc_ptr (ads_courante,82);
										break;
										
									default:
										break;
										
									} // switch (ads_var.genre
								}// for (
							}
						else
							{
							switch (ads_var->genre)
								{
								case c_res_logique   :
									ads_fen_log = (BOOL*) ads_courante;
									if (*ads_fen_log)
										{
										uFicEcrireLn (hFicOrigOuDest,"0,1");
										}
									else
										{
										uFicEcrireLn (hFicOrigOuDest,"0,0");
										}
									StrBOOLToStr (lng_tampon1, *ads_fen_log, "FALSE","TRUE");
									uFicEcrireLn (hFicOrigOuDest, lng_tampon1);
									ads_courante = (PFLOAT)inc_ptr (ads_courante,2);
									break;
									
								case c_res_numerique   :
									ads_fen_num = ads_courante;
									StrCopy (lng_tampon1, "0,");
									StrPrintFormat (lng_tampon2, "%.4f", *ads_courante);
									StrConcat (lng_tampon1,lng_tampon2);
									uFicEcrireLn (hFicOrigOuDest, lng_tampon1);
									uFicEcrireLn (hFicOrigOuDest,"V");
									ads_courante = (PFLOAT)inc_ptr (ads_courante,4);
									break;
									
								case c_res_message   :
									ads_fen_mes = (char*) ads_courante;
									uFicEcrireLn (hFicOrigOuDest,"1,0");
									StrCopy (lng_tampon1, "\"");
									StrPascalToC (lng_tampon2, ads_fen_mes);
									StrConcat (lng_tampon1,lng_tampon2);
									StrConcat (lng_tampon1, "\"");
									uFicEcrireLn (hFicOrigOuDest, lng_tampon1);
									ads_courante = (PFLOAT)inc_ptr (ads_courante,82);
									break;
									
								default:
									break;
									
								} // switch (ads_var.genre
							} // taille == 0
						} // while i < ads_fichier->pos_fin_var
					} // for (
				uFicEcrireLn (hFicOrigOuDest,"-1,0");
				uFicEcrireLn (hFicOrigOuDest,"EOD");
				uFicFerme (&hFicOrigOuDest);
				} // if (uFicTexteOuvre  (&hFicOrigOuDest, fic_destination
			else
				numero_err = 16;
			uFicFerme (&hdl_local);
			}
		}
	else
		{
		numero_err = 256;
		}
	return numero_err;
	} // trans_pcs_dif

//----------------------------------------------------------------------------
static DWORD mes_erreur(DWORD erreur,DWORD position)
  {
  DWORD numero_err;

  numero_err = 0x8000 | ( erreur * 1024);
  uFicFerme (&hFicOrigOuDest);
  uFicFerme (&hdl_local);
  return(numero_err);
  }


//---------------------------------------------------------------------------
//  ERREURS :
//   1 'TABLE non trouv� ou position incorrecte dans fichier source ';
//   2 '0,1 non trouv� apr�s TABLE dans fichier source ';
//   3 'mauvais pointeur �criture ';
//   4 'VECTORS non trouv� ou position incorrecte dans fichier source ';
//   5 'Nombre enregistrements non trouv� dans fichier source ';
//   6 '"" non trouv� apr�s nombre enregistrements dans fichier source ';
//   7 'TUPLES non trouv� ou position incorrecte dans fichier source ';
//   8 'Nombre de rubriques non trouv� dans fichier source ';
//   9 'Nombre de rubriques different entre les deux fichiers ';
//  10 '"" non trouv� apr�s nombre de rubriques dans fichier source ';
//  11 'DATA non trouv� ou position incorrecte dans fichier source ';
//  12 '0,0 non trouv� apr�s DATA dans fichier source ';
//  13 '"" non trouv� apr�s 0,0 qui suit DATA dans fichier source ';
//  14 '-1,0 non trouv� avant les donn�es dans fichier source ';
//  15 'BOT non trouv� avant les donn�es ou position incorrecte dans fichier source ';
//  16 '-1,0 non trouv� en fin de fichier dans fichier source ';
//  17 'EOD non trouv� en fin de fichier dans fichier source ';
//  18 'La rubrique doit etre un logique ';
//  19 'La rubrique doit etre un num�rique ';
//  20 'La rubrique doit etre un message '
//  21 'Numero premier enregistrement superieur au nombre enregistrements ';
/* $$ en cours
typedef enum
=01, ERRDIF_TABLE non trouv� ou position incorrecte dans fichier source
=02, ERRDIF_0,1 non trouv� apr�s TABLE dans fichier source
=03, ERRDIF_mauvais pointeur �criture
=04, ERRDIF_VECTORS non trouv� ou position incorrecte dans fichier source
=05, ERRDIF_Nombre enregistrements non trouv� dans fichier source
=06, ERRDIF_DEUX_GUILLEMETS non trouv� apr�s nombre enregistrements dans fichier source
=07, ERRDIF_non trouv� ou position incorrecte dans fichier source
=08, ERRDIF_Nombre de rubriques non trouv� dans fichier source
=09, ERRDIF_Nombre de rubriques different entre les deux fichiers
=10, ERRDIF_DEUX_GUILLEMETS non trouv� apr�s nombre de rubriques dans fichier source
=11, ERRDIF_DATA non trouv� ou position incorrecte dans fichier source
=12, ERRDIF_0,0 non trouv� apr�s DATA dans fichier source
=13, ERRDIF_DEUX_GUILLEMETS non trouv� apr�s 0,0 qui suit DATA dans fichier source
=14, ERRDIF_-1,0 non trouv� avant les donn�es dans fichier source
=15, BOT non trouv� avant les donn�es ou position incorrecte dans fichier source
=16, -1,0 non trouv� en fin de fichier dans fichier source
=17, EOD non trouv� en fin de fichier dans fichier source
=18, La rubrique doit etre un logique
=19, La rubrique doit etre un num�rique
=20, La rubrique doit etre un message
=21, Numero premier enregistrement superieur au nombre enregistrements
*/
//----------------------------------------------------------------------------
static DWORD trans_dif_pcs( char *fic_source, char *fic_destination,
	DWORD num_fichier, BOOL reinit, DWORD enr_depart)
  {
  //*********************************************************************
  #define err(code1,code2) return (mes_erreur(code1,code2))  //Sale Sioux
  //*********************************************************************

  PFICHIER_DQ ads_fichier;
  t_tableau_vars_fichiers*ads_var;
  PBOOL ads_fen_log;
  PFLOAT ads_courante_deb;
  PFLOAT ads_courante;
  PFLOAT ads_num;
  PFLOAT ads_fen_num;
  char *ads_fen_mes;
  char mes[TAILLE_MESS];
  DWORD nb_bot;
  DWORD nb_enr_source;
  HFDQ n_fichier;
  DWORD nb_rubrique;
  DWORD nb_rub;
  FLOAT val_num;
  DWORD taille_enr;
  DWORD numero_err;
  HFDQ hBidon;
  DWORD i,j;
  DWORD nb_enr_cour;
  DWORD nb_enr_fic;
  DWORD err_fic;


  numero_err  = 0;
  ads_fichier = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,num_fichier);
  nb_rubrique = ads_fichier->pos_fin_var - ads_fichier->pos_debut_var + 1;
  i = ads_fichier->pos_debut_var - 1;
  taille_enr = 0;
  while (i < ads_fichier->pos_fin_var)
    {
    i = i + 1;
    ads_var = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq,i);
    if (ads_var->taille != 0)
      {
      switch (ads_var->genre)
        {
        case c_res_logique :
          taille_enr =  taille_enr + (2 * ads_var->taille);
          break;
        case c_res_numerique :
          taille_enr =  taille_enr + (4 * ads_var->taille);
          break;
        case c_res_message :
          taille_enr =  taille_enr + (82 * ads_var->taille);
          break;
        default:
          break;
        } // End Switch
      }
    else
      {
      switch (ads_var->genre)
        {
        case c_res_logique :
          taille_enr = taille_enr + 2;
          break;
        case c_res_numerique :
          taille_enr = taille_enr + 4;
          break;
        case c_res_message :
          taille_enr = taille_enr + 82;
          break;
        default:
          break;
        } // Fin Switch
      } // Fin If
    } // Fin While

  if (!bFicOuvert(&hBidon, fic_destination))
    {
    if (reinit)
      {
      uFicEfface (fic_destination);
      }
		if (uFicOuvre(&n_fichier, fic_destination,taille_enr) == 0)
			{
			// Reservation Buffer Ecriture (pour remplacer le poinre_fenetre)
			ads_courante_deb = (PFLOAT)pMemAlloue(taille_enr);

			if (uFicTexteOuvre  (&hFicOrigOuDest, fic_source) == 0)
				{
				err_fic = uFicLireLn (hFicOrigOuDest, mes, TAILLE_MESS);
				if (!bStrEgales(mes,"TABLE"))
					{
					err(1,0);
					}
				err_fic = uFicLireLn (hFicOrigOuDest, mes, TAILLE_MESS);
				if (!bStrEgales(mes,"0,1")) err(2,0);

				uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
		    BOOL bCasExcel = bStrEgales (mes, "\"EXCEL\"");
				uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);

				if ((!bStrEgales(mes,"VECTORS")) && (!bStrEgales(mes,"TUPLES")))
					{
					err(4,0);
					}
				else
					{
					if (bStrEgales(mes,"TUPLES"))
						{
						uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
						StrDelete (mes,0,2);
						if (bCasExcel)
							{
							if (!StrToDWORD(&nb_rub,mes)) err(8,0);
							if (nb_rub != nb_rubrique) err (9,0);
							}
						else
							{
							if (!StrToDWORD(&nb_enr_source,mes)) err(5,0);
							}
						}
					else
						{
						uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
						StrDelete (mes,0,2);
						if (bCasExcel)
							{
							if (!StrToDWORD(&nb_enr_source,mes)) err(5,0);
							}
						else
							{
							if (!StrToDWORD(&nb_rub,mes)) err(8,0);
							if (nb_rub != nb_rubrique) err(9,0);
							}
						}
					}
				uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);

				if (!bStrEgales(mes,"\"\"")) err(6,0);
				uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);

				if ((!bStrEgales(mes,"VECTORS")) && (!bStrEgales(mes,"TUPLES")))
					{
					err(7,0);
					}
				else
					{
					if (bStrEgales(mes,"TUPLES"))
						{
						uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
						StrDelete (mes,0,2);
						if (bCasExcel)
							{
							if (!StrToDWORD(&nb_rub,mes)) err(8,0);
							if (nb_rub != nb_rubrique) mes_erreur(9,0);
							}
						else
							{
							if (!StrToDWORD(&nb_enr_source,mes)) err(5,0);
							}
						}
					else
						{
						uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
						StrDelete (mes,0,2);
						if (bCasExcel)
							{
							if (!StrToDWORD(&nb_enr_source,mes)) err(5,0);
							}
						else
							{
							if (!StrToDWORD(&nb_rub,mes)) err(8,0);
							if (nb_rub != nb_rubrique) mes_erreur(9,0);
							}
						}
					}
				if (enr_depart > nb_enr_source) err(21,0);
				nb_enr_t = nb_enr_source;
				uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);

				if (!bStrEgales(mes,"\"\"")) err(10,0);
				uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
				if (!bStrEgales(mes,"DATA")) err(11,0);
				uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
				if (!bStrEgales(mes,"0,0")) err(12,0);
				uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
				if (!bStrEgales(mes,"\"\"")) err(13,0);
				nb_bot = 0;
				nb_enr_fic = 0;
				//--------------------------------------------------------
				while ((nb_enr_fic==0) && (nb_bot != enr_depart))
					{
					nb_enr_fic = uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
					if (bStrEgales(mes,"BOT")) nb_bot = nb_bot + 1;
					}
				if (nb_enr_fic == 0)
					{
					for (nb_enr_cour=enr_depart;nb_enr_cour<=nb_enr_source;nb_enr_cour++)
						{
						DWORD	dwNbEnr;

						ads_courante = ads_courante_deb;
						enr_cour = nb_enr_cour;
						if (nb_enr_cour > enr_depart)
							{
							uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
							if (!bStrEgales(mes,"-1,0")) err(14,0);
							uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
							if (!bStrEgales(mes,"BOT")) err(15,0);
							}

						ads_fichier = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq, num_fichier);

						i = ads_fichier->pos_debut_var;
						while (i <= ads_fichier->pos_fin_var)
							{
							ads_var = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq,i);
							if (ads_var->taille != 0)
								{
								for (j=1;j<=ads_var->taille;j++)
									{
									switch (ads_var->genre)
										{
										case c_res_logique :
											uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
											StrDelete (mes,0,2);
											if ((!bStrEgales(mes,"0")) && (!bStrEgales(mes,"1"))) err(18,i-ads_fichier->pos_debut_var+1);
											uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);

											ads_fen_log = (BOOL*) ads_courante;
											if (bStrIEgales(mes,"FALSE"))
												{
												*ads_fen_log = FALSE;
												}
											else
												{
												if (bStrIEgales(mes,"TRUE"))
													{
													*ads_fen_log = TRUE;
													}
												else
													{
													err(18,i-ads_fichier->pos_debut_var+1);
													}
												}
											ads_courante = (PFLOAT)inc_ptr (ads_courante,2);
											break;

										case c_res_numerique :
											uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
											StrDelete (mes,0,2);
											if (!StrToFLOAT(&val_num,mes)) err(19,i-ads_fichier->pos_debut_var+1);
											uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
											if (!bStrEgales(mes,"V")) err(19,i-ads_fichier->pos_debut_var+1);
											ads_fen_num = ads_courante;
											*ads_fen_num = val_num;
											ads_courante = (PFLOAT)inc_ptr (ads_courante,4);
											break;

										case c_res_message :
											uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
											if (!bStrEgales(mes,"1,0")) err(20,i-ads_fichier->pos_debut_var+1);
											uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
											StrDelete (mes,0,1);
											StrDelete (mes,StrLength(mes)-1,1);
											ads_fen_mes = (char*) ads_courante;
											StrCToPascal (ads_fen_mes,mes);
											ads_courante = (PFLOAT)inc_ptr (ads_courante,82);
											break;

										default:
											break;

										} // switch (ads_var->genre)
									} // for (j=1;j<=ads_var->taille;j++)
								} // if (ads_var->taille != 0)
							else
								{
								switch (ads_var->genre)
									{
									case c_res_logique :
										uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
										StrDelete (mes,0,2);
										if ((!bStrEgales(mes,"0")) && (!bStrEgales(mes,"1"))) err(18,i-ads_fichier->pos_debut_var+1);
										uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
										ads_fen_log = (BOOL*) ads_courante;
										if (bStrIEgales(mes,"FALSE"))
											{
											*ads_fen_log = FALSE;
											}
										else
											{
											if (bStrIEgales(mes,"TRUE"))
												{
												*ads_fen_log = TRUE;
												}
											else
												{
												err(18,i-ads_fichier->pos_debut_var+1);
												}
											}
										ads_courante = (PFLOAT)inc_ptr (ads_courante,2);
										break;

									case c_res_numerique :
										uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
										StrDelete (mes,0,2);
										if (!StrToFLOAT(&val_num,mes)) err(19,i-ads_fichier->pos_debut_var+1);
										uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
										if (!bStrEgales(mes,"V")) err(19,i-ads_fichier->pos_debut_var+1);
										ads_fen_num = ads_courante;
										*ads_fen_num = val_num;
										ads_courante = (PFLOAT)inc_ptr (ads_courante,4);
										break;

									case c_res_message :
										uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
										if (!bStrEgales(mes,"1,0")) err(20,i-ads_fichier->pos_debut_var+1);
										uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
										StrDelete (mes,0,1);
										StrDelete (mes,StrLength(mes)-1,1);
										ads_fen_mes = (char*) ads_courante;
										StrCToPascal (ads_fen_mes,mes);
										ads_courante = (PFLOAT)inc_ptr (ads_courante,82);
										break;

									default:
										break;

									} // switch (ads_var->genre)
								} /* taille tableau == 0 */
							i = i + 1;
							} /* while i <= ads_fichier->pos_fin_var */

						uFicNbEnr(n_fichier, &dwNbEnr);
						uFicPointeEcriture (n_fichier, dwNbEnr + 1);
						uFicEcrireTout (n_fichier,ads_courante_deb, 1);
						} // for
					ads_fichier = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq, num_fichier);
					if (ads_fichier->dwPosESTaille != 0)
						{ 
						DWORD	dwNbEnr;
						
						// existe var taille
						ads_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ads_fichier->dwPosESTaille);
						uFicNbEnr (n_fichier, &dwNbEnr);
						*ads_num = (FLOAT) (dwNbEnr);
						} /* existe var taille */
					if (nb_enr_source > enr_depart)
						{
						uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
						if (!bStrEgales(mes,"-1,0")) err(16,0);
						uFicLireLn (hFicOrigOuDest,mes, TAILLE_MESS);
						if (!bStrEgales(mes,"EOD")) err(17,0);
						}
					}
				else
					{
					err (21,0);
					}
				uFicFerme (&hFicOrigOuDest);
				}
			else
				numero_err = 128;
			uFicFerme (&n_fichier);
			}
		else
			numero_err = 16;
    }
  else
    {
    numero_err = 256;
    }
  return (numero_err);
  } // trans_dif_pcs

//----------------------------------------------------------------------------
static BOOL valide_nom_fichier (char* nom_fichier, DWORD* num_fichier )
  {
  DWORD index;
  PFICHIER_DQ ads_fichier;
  BOOL fini;
  BOOL erreur;

  erreur = FALSE;
  if (existe_repere (bx_disq))
    {
    index = 0;
    fini = FALSE;
    while ((index < nb_enregistrements (szVERIFSource, __LINE__, bx_disq)) && (!fini))
      {
      index++;
      ads_fichier = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,index);
      if ( bStrIEgales(nom_fichier, ads_fichier->nom_fichier))
        {
        erreur = (BOOL) (index != 0);
        *num_fichier = index;
        fini = TRUE;
        }
      }
    }
  return (erreur);
  }

//----------------------------------------------------------------------------
static DWORD decode_parametre (char sens,
                       char * fichier_source,
                       char * fichier_destination,
                       DWORD* num_fichier,
		       DWORD enr_depart)

  {
  DWORD erreur;
  DWORD lg_nom_fic;
	char szNomFicTemp [MAX_PATH];

  erreur=0;
  lg_nom_fic = StrLength(fichier_source);
  // test fichier source
  if ((lg_nom_fic == 0 ) || (lg_nom_fic > MAX_PATH))
    {
    erreur = erreur | 2 ; // longueur fichier source invalide
    }
  // test fichier destination
  lg_nom_fic = StrLength(fichier_destination);
  if ((lg_nom_fic == 0 ) | (lg_nom_fic > MAX_PATH))
    {
    erreur = erreur | 16;
    }

	StrCopy (szNomFicTemp, fichier_source);
	pszPathNameAbsoluAuDoc (szNomFicTemp);
  if (!bFicPresent(szNomFicTemp))
    {
    erreur = erreur | 4;
    }


  if (fichier_destination == fichier_source)
    {
    erreur = erreur | 32;
    }

  if (enr_depart = 0)
    {
    erreur = erreur | 512;
    }

  switch (sens)
    {
    case '1': //PCS ---> DIF
      if (!valide_nom_fichier(fichier_source,num_fichier))
        {
        erreur = erreur | 8;
        }
      break;

    case '2': //Dif ---> PCS
      if (!valide_nom_fichier(fichier_destination,num_fichier))
        {
        erreur = erreur | 64;
        }
      break;
    } /* fin switch (*/

  return (erreur);
  }

//--------------------------------------------------------------------
DWORD fonction_dif (char sens, BOOL reinit, char* fichier_source,
										char* fichier_destination, DWORD enr_depart)

  {
  DWORD num_fichier;
  DWORD status_dif = decode_parametre(sens,fichier_source,fichier_destination,&num_fichier,enr_depart);
  if (status_dif == 0)
    {
		char szNomFicSourceTemp[MAX_PATH];
		char szNomFicDestinationTemp[MAX_PATH];

		StrCopy (szNomFicSourceTemp, fichier_source);
		pszPathNameAbsoluAuDoc (szNomFicSourceTemp);
		StrCopy (szNomFicDestinationTemp, fichier_destination);
		pszPathNameAbsoluAuDoc (szNomFicDestinationTemp);
    switch (sens)
      {
      case '1':// conversion PCS ----> DIF
        status_dif=trans_pcs_dif(szNomFicSourceTemp, szNomFicDestinationTemp, num_fichier);
        break;

      case '2':// conversion Dif (----> PCS
        status_dif=trans_dif_pcs(szNomFicSourceTemp, szNomFicDestinationTemp, num_fichier, reinit, enr_depart );
        break;

      }
    }
  return status_dif;
  }
// --------------------------------- fin Driv_dif.cpp --------------------------------------


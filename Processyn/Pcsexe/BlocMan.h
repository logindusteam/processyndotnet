//---------------------------------------------------------------------------
// Gestion de tableaux d'enregistrements appel�s blocs.
//---------------------------------------------------------------------------

#ifndef   BLOCMAN_H
  #define BLOCMAN_H

  //--------------- Handle blocs
	DECLARE_HANDLE (HBLOC);
	typedef HBLOC * PHBLOC;

// ----------- actions sur les blocs ----------------------
  // Cr�e un bloc
	HBLOC			hBlocCree				(DWORD dwTailleEnr, DWORD dwNbEnr);

  // lib�re et d�truit un bloc
  BOOL			bBlocFerme			(PHBLOC phbloc);

  
	// Renvoie le nombre d'enregistrements dans ce bloc
  DWORD			dwBlocNbEnr			(HBLOC hbloc);

	// Renvoie la taille de l'enregistrement de base dans ce bloc dans cette base de donn�e 
  DWORD			dwBlocTailleEnrDeBase	(HBLOC hbloc);


	// Ins�re de 0 � N enregistrements dans ce bloc dans cette base de donn�e 
  PVOID			pBlocInsereEnr		(HBLOC hbloc, DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr);

	// Supprime de 0 � N enregistrements dans ce bloc
  void			BlocEnleveEnr		(HBLOC hbloc, DWORD dwNEnr, DWORD dwNbEnr);


	// Change la taille de 0 � N enregistrements dans ce bloc
  PVOID			pBlocChangeTailleEnr		(HBLOC hbloc, DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr);

	// Renvoie la taille d'un enregistrement dans ce bloc
  DWORD			dwBlocTailleEnr  (HBLOC hbloc, DWORD dwNEnr);


	// Renvoie l'adresse d'un enregistrement dans ce bloc
  PVOID			pBlocPointeEnr		(HBLOC hbloc, DWORD dwNEnr);
	

	// Copie de 0 � N enregistrements entre deux blocs
  void			BlocCopieEnr			(HBLOC hblocDst, DWORD wRecIdDst, HBLOC hblocSrc, DWORD wRecIdSrc, DWORD dwNbEnr);

	// Echange de 0 � N enregistrements entre deux blocs
  void			BlocEchangeEnr		(HBLOC hbloc1, DWORD wRecId1, HBLOC hbloc2, DWORD wRecId2, DWORD dwNbEnr);


#endif //BLOCMAN_H




// --------------------------------------------------------------------------
BOOL TpsLireDateHeure   (DWORD *Jour, DWORD *Mois, DWORD *Annee, DWORD *JourDeSemaine,
                            DWORD *Heures, DWORD *Minutes, DWORD *Secondes, DWORD *Centiemes);
BOOL TpsForcerDateHeure (DWORD Jour, DWORD Mois, DWORD Annee, // pas de JourDeSemaine,
                            DWORD Heures, DWORD Minutes, DWORD Secondes, DWORD Centiemes);

// --------------------------------------------------------------------------
BOOL TpsLireDate   (DWORD *Jour, DWORD *Mois, DWORD *Annee, DWORD *JourDeSemaine);
BOOL TpsForcerDate (DWORD Jour, DWORD Mois, DWORD Annee);

// --------------------------------------------------------------------------
BOOL TpsLireHeure   (DWORD *Heures, DWORD *Minutes, DWORD *Secondes, DWORD *Centiemes);
BOOL TpsForcerHeure (DWORD Heures, DWORD Minutes, DWORD Secondes, DWORD Centiemes);


// --------------------------------------------------------------------------
// Partie asynchrone de l'animation de synoptique
// Elle fonctionne en liaison avec scrut_gr.cpp
// Version Win32 du 20/2/97 par JS
// --------------------------------------------------------------------------

#include "stdafx.h"
#include "MemMan.h"
#include "threads.h"
#include "USem.h"
#include "ITC.h"
#include "tipe_mbx.h"          // Noms (MBX) du canal PROCESSYN
#include "G_Objets.h"
#include "UChrono.h"
#include "g_sys.h"
#include "BdGr.h"
#include "lng_res.h"
#include "Tipe_gr.h"
#include "WAnmGr.h"           // Fenetres PM.................: PmAnmGrxxx ()
#include "Verif.h"
#include "Appli.h"
#include "WExe.h"
#include "DebugDumpVie.h"
#include "CliAnmGr.h"
VerifInit;

// Handles communication avec PcsExe (devraient appartenir � la session)
static  CITC ITCClientReceptionAnmGr;
static  CITC ITCClientEmissionAnmGr;

// Handle de thread de l'animateur
static 	HTHREAD	hthreadCliAnmGr = NULL;

// Handle de la fen�tre au comportement "boite de dialogue" active
HWND	hDlgGrActive = NULL;

// ---------------------------------------------------------------
// Boucle de message : traite un message re�u
// Renvoie TRUE si message != WM_QUIT
static BOOL bTraiteMessage (MSG * pmsg)
	{
	// boite de dialogue active ?
	if (hDlgGrActive)
		{
		// acc�l�rateur ?
		if (!TranslateAccelerator (Appli.hwnd, hAccelTouchesFonctions, pmsg))
			{
			// non => Message autre que Message dialogue ?
			if (!IsDialogMessage (hDlgGrActive, pmsg))
				{
				// oui => traite le
				TranslateMessage(pmsg);
				DispatchMessage (pmsg);
				}
			}
		}
	else
		{
		// non => traite le
		TranslateMessage(pmsg);
		DispatchMessage (pmsg);
		}

	return pmsg->message != WM_QUIT;
	} // bTraiteMessage

// -----------------------------------------------------------------------------
// Thread client animation de synoptique g�rant la communication et l'affichage
//------------------------------------------------------------------------------
static UINT __stdcall uThreadCliAnmGr (void *hThrd)
  {
	DumpVie(uThreadCliAnmGr);
  UINT   wIdMessage = 0;

	// Initialise la boucle de message thread 
  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
  PVOID	pParamIn = NULL;

  while (bThreadAttenteMessage ((HTHREAD)hThrd, &pParamIn, NULL, NULL, &wIdMessage))
    {
    switch (wIdMessage)
      {
      case THRD_FCT_START:
				{
				BOOL	bEnd = FALSE;
				// R�cup�re les ITC de communication serveurs
				ITC_TX_RX ITCServeurAnmGr = *(PITC_TX_RX)pParamIn;

				// Connexion avec le serveur
        if (ITCClientEmissionAnmGr.bCreeClientTX (szTopicReceptionAnmGr, ITCServeurAnmGr.pITCRX) &&
	        ITCClientReceptionAnmGr.bCreeClientRX (szTopicEmissionAnmGr, ITCServeurAnmGr.pITCTX))
					{
		      if (bInitWanmGr ())
						{
						// r�cup�re le s�maphore d'attente de r�ception
						HSEM	hsemRx = ITCClientReceptionAnmGr.hsemAttenteReception ();

						while (!bEnd)
							{
							// Attente de l'arriv�e d'un message dans la queue ou en r�ception
							DWORD dwCause = MsgWaitForMultipleObjects (1, (PHANDLE)&hsemRx, FALSE, INFINITE, QS_ALLINPUT);

							// traitement selon la cause de la fin de l'attente
							switch (dwCause)
								{
								// �v�nement li� � la queue de message
								case WAIT_OBJECT_0+1: 
									{
									MSG	msg;

									// Y a t'il des messages dans la queue de message
									while (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
										{
										// oui => traite le
										if (!bTraiteMessage (&msg))
											bEnd = TRUE;
										}
									}
									break;

								// �v�nement li� � la r�ception d'un message de communication
								case WAIT_OBJECT_0:
									{
									// R�cup�re le buffer de message re�u
									CITC::PMESSAGE_ITC	pMessageRecu = NULL;
									if (ITCClientReceptionAnmGr.dwLecture ((PVOID*)&pMessageRecu, TRUE, FALSE) == 0)
										{
										Verif(pMessageRecu);
										switch (pMessageRecu->Entete.IdMessage)
											{ // pas de MBX_MSG_OUVRE puisque connexion � l'initiative du client
											case ITC_MSG_USER:
												{
												MSG	msg;

												// Traitement des donn�es re�ues
												t_anm_gr_data_service * ptrServices = (t_anm_gr_data_service *) &pMessageRecu->aDonnees[0];
												BYTE  * pbBufferServices = &(ptrServices->bBufferServices [0]);

												//$$ r�entrance interdite pour cause d'�crasement des buffers de r�ception
												//$$ devrait r�cup�rer les blocs m�moires transf�r�s dans la queue et les d�truire apr�s
												Verif (ptrServices->NbServices >= 0);

												// Tant qu'il y a des services � traiter
												while (ptrServices->NbServices)
													{
													// Traite le service courant
													ptrServices->NbServices--;
													ExecService (&pbBufferServices);
													}
												// Vide la queue de message
												while (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
													{
													if (!bTraiteMessage (&msg))
														bEnd = TRUE;
													}
												}
												break;
											default:
												//case MBX_MSG_FERME:
												bEnd = TRUE;
												break;
											} // switch ((pMessageRecu->Entete).IdMessage)

										// Lib�re le buffer du message re�u
										if (pMessageRecu)
											MemLibere ((PVOID *)(&pMessageRecu));
										} // if (dwMbxLecture (ITCClientReceptionAnmGr
									}
									break;
								case WAIT_TIMEOUT:			// time out (impossible car INFINITE)
								case WAIT_ABANDONED_0:	// Erreur en attente de synchro r�ception
								default:
									bEnd = TRUE;
									break;

								} // switch (dwCause)
							} // while (!bEnd)
						} // if (bInitWanmGr ())
					FermeWanmGr (); // $$ V�rifier si bInitWanmGr n'a pas bien fonctionn�
					} // if ((dwMbxAppelServeur (&ITCClientReceptionAnmGr, 

				// Fermeture des ressources
        ITCClientReceptionAnmGr.Ferme ();
        ITCClientEmissionAnmGr.Ferme ();
				} // case THRD_FCT_START:
				break;
      } // switch (wIdMessage)
    } // while (bThreadAttenteMessage ((HTHREAD)hThrd, ...

  return uThreadTermine ((HTHREAD)hThrd, 0);
  } // uThreadCliAnmGr


// --------------------------------------------------------------------------
//											FONCTIONS EXPORTEES
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
BOOL bCliAnmGrEnvoiAR (void)
  {
  return ITCClientReceptionAnmGr.dwEnvoiAR () == 0;
  }

// --------------------------------------------------------------------------
// Envoi au serveur d'un message d'animation graphique
BOOL bCliAnmGrEnvoiMessage (DWORD wSizeMsg, PVOID pMsg)
  {
  BOOL					bOk = FALSE;
  CITC::PMESSAGE_ITC	pMessageEmission;
  DWORD         wSizeMsgEmission;

  if (ITCClientEmissionAnmGr.bAUnCorrespondant(0))
    {
    wSizeMsgEmission = sizeof (CITC::ITC_HEADER_MESSAGE) + wSizeMsg;
    pMessageEmission = (CITC::PMESSAGE_ITC)pMemAlloue (wSizeMsgEmission);
    if (pMessageEmission != NULL)
      {
      memcpy (&(pMessageEmission->aDonnees[0]), pMsg, wSizeMsg);
      if (ITCClientEmissionAnmGr.dwEcriture (&pMessageEmission->Entete, wSizeMsgEmission, FALSE) == 0)
        bOk = TRUE;
      MemLibere ((PVOID *)(&pMessageEmission));
      }
    }

  return bOk;
  }


// --------------------------------------------------------------------------
// cr�ation du thread d'animation du graphisme
BOOL bLanceCliAnmGr (PITC_TX_RX pITCServeurAnmGr)
	{
	BOOL		bRes = FALSE;
	#define STACK_THREAD_ANIMATION_GR   2 * 4096

	// cr�ation du Thread d'animation Alarme
	if (bThreadCree (&hthreadCliAnmGr, uThreadCliAnmGr, STACK_THREAD_ANIMATION_GR))
		{
		ThreadEnvoiMessage (hthreadCliAnmGr, THRD_FCT_START, pITCServeurAnmGr, NULL, NULL);
		bRes = TRUE;
		}
	return bRes;
  } // bLanceCliAnmGr

// --------------------------------------------------------------------------
// fermeture du thread d'animation du graphisme
BOOL bFermeCliAnmGr (void)
	{
	BOOL bRes = FALSE;

	// fermeture du Thread d'animation Alarme
	if (hthreadCliAnmGr)
		{
		bRes = TRUE;
		bThreadFerme (&hthreadCliAnmGr, INFINITE);
		}

	return bRes;
  } // bFermeCliAnmGr


// ---------------------- fin CliAnmGr.c ----------------------------------

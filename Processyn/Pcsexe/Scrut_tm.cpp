// --------------------------------------------------------------------------
// Ex�cution Processyn :
// Gestion du Terminal Monochrome (Terminal Texte)
// --------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"         // Types Standards
#include "UStr.h"      // Fcts Standards
#include "lng_res.h"
#include "tipe.h"        // Types + Constantes Processyn
#include "mem.h"         // Memory Manager
#include "MemMan.h"
#include "inpuex.h"      // cmd_menu()
#include "WScrExe.h"				// Touches de Fonctions TOUCHE_F1...
#include "WExe.h"     // commande main menu
#include "Driv_sys.h"     // commande main menu
#include "pcs_sys.h"
#include "PcsVarEx.h"
#include "pcsdlgex.h"     //t_buff_syn_ex
#include "scrut_tm.h"
#include "Verif.h"
VerifInit;

static BOOL force_arret_ex = FALSE;
static BOOL force_retour_sys = FALSE;

//-----------------------
typedef struct
  {
  ID_VAR_SYS var_sys_logique;
  DWORD cst_touche;
  } t_touche_logiques;

static t_touche_logiques td_touches_logiques [NB_TOUCHES] =
  {
   { ff1    , TOUCHE_F1        },
   { ff2    , TOUCHE_F2        },
   { ff3    , TOUCHE_F3        },
   { ff4    , TOUCHE_F4        },
   { ff5    , TOUCHE_F5        },
   { ff6    , TOUCHE_F6        },
   { ff7    , TOUCHE_F7        },
   { ff8    , TOUCHE_F8        },
   { ff9    , TOUCHE_F9        },
   { ff10   , TOUCHE_F10       },
   { ff11   , TOUCHE_SHIFT_F1  },
   { ff12   , TOUCHE_SHIFT_F2  },
   { ff13   , TOUCHE_SHIFT_F3  },
   { ff14   , TOUCHE_SHIFT_F4  },
   { ff15   , TOUCHE_SHIFT_F5  },
   { ff16   , TOUCHE_SHIFT_F6  },
   { ff17   , TOUCHE_SHIFT_F7  },
   { ff18   , TOUCHE_SHIFT_F8  },
   { ff19   , TOUCHE_SHIFT_F9  },
   { ff20   , TOUCHE_SHIFT_F10 },
   { fha    , TOUCHE_UP        },
   { fba    , TOUCHE_DOWN      },
   { fdr    , TOUCHE_RIGHT     },
   { fga    , TOUCHE_LEFT      },
   { pg_up  , TOUCHE_PAGE_UP   },
   { pg_down, TOUCHE_PAGE_DOWN },
   { retour , TOUCHE_RETURN    }
  };

// --------------------------------------------------------------------------
void init_tm (void)
  {
  // Effacement de la fenetre texte
  ScrAcces ();
  ScrClear ();
  ScrFinAcces ();
  ScrRefresh  ();

  // Reset Variables syst�mes Touches
  for (DWORD w = 0; w < NB_TOUCHES; w++)
    {
		CPcsVarEx::SetValVarSysLogEx (td_touches_logiques [w].var_sys_logique, FALSE);
    bScrRAZToucheAppuyee (td_touches_logiques [w].cst_touche);
    }

  force_retour_sys = FALSE;
  force_arret_ex = FALSE;
  }

// --------------------------------------------------------------------------
void recoit_tm (void)
	{
	PX_VAR_SYS pwPosition;
	BOOL   *		pBool;
	PFLOAT    	pNum;
	PSTR      	pMes;
	BOOL   *		ptr_tab_log;
	DWORD      *ptr_tab_num;
	char      *ptr_tab_mes;
	DWORD      taille_tab,x;
	t_buff_syn_ex *ptr_tab_syn;
	
	// Reset/Set Touches
	for (DWORD w = 0; w < NB_TOUCHES; w++)
		{
		pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, td_touches_logiques [w].var_sys_logique);
		pBool = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx));
		(*pBool) = bScrRAZToucheAppuyee (td_touches_logiques [w].cst_touche);
		}
	if (ExisteMenu())
		{
		for (DWORD w = 0; w < NB_VAR; w++)
			{
			if (bVarSysExeEstModifiee (w))
				{
				pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme,dwPosVarSysExe (w));
				switch (dwVarSysExeGetType (w))
					{
					case c_res_logique :
						if (dwVarSysExeGetSens (w) == c_res_variable_ta_es) //tableau
							{
							taille_tab = dwVarSysExeReadPtr(w,(PVOID *)&ptr_tab_log);
							if (dwPosVarSysExe (w) == c_sys_fen_visible)
								{
								ptr_tab_syn = (t_buff_syn_ex *)ptr_tab_log;
								for (x=1;x<=taille_tab;x++)
									{
									pBool = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx)+((ptr_tab_syn->no_synoptique)-1));
									(*pBool) = ptr_tab_syn->etat;
									ptr_tab_syn++;
									}
								}
							else
								{
								for (x=1;x<=taille_tab;x++)
									{
									pBool = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx)+(x-1));
									(*pBool) = *ptr_tab_log;
									ptr_tab_log++;
									}
								}
							MemLibere((PVOID *)(&ptr_tab_log));
							}
						else
							{
							pBool = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx));
							(*pBool) = bVarSysExeReadLog (w);
							
							if ((dwPosVarSysExe (w) == arret_exploitation) && (*pBool))
								{
								force_arret_ex = TRUE;
								}
							if ((dwPosVarSysExe (w) == retour_systeme) && (*pBool))
								{
								force_retour_sys = TRUE;
								}
							}
						break;
						
						
					case c_res_numerique :
						if (dwVarSysExeGetSens (w) == c_res_variable_ta_es) //tableau
							{
							taille_tab = dwVarSysExeReadPtr (w,(PVOID *)&ptr_tab_num);
							if (dwPosVarSysExe (w) == c_sys_fen_impression)
								{
								ptr_tab_syn = (t_buff_syn_ex *)ptr_tab_num;
								for (x=1;x<=taille_tab;x++)
									{
									pNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pwPosition->dwPosCourEx)+((ptr_tab_syn->no_synoptique)-1));
									(*pNum) = (FLOAT)ptr_tab_syn->valeur;
									ptr_tab_syn++;
									}
								}
							else
								{
								for (x=1;x<=taille_tab;x++)
									{
									pNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pwPosition->dwPosCourEx)+(x-1));
									(*pNum) = (FLOAT)*ptr_tab_num;
									ptr_tab_num++;
									}
								}
							MemLibere((PVOID *)(&ptr_tab_num));
							}
						else
							{
							pNum = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pwPosition->dwPosCourEx));
							(*pNum) = (FLOAT) dwVarSysExeReadNum (w);
							}
						break;
						
						
					case c_res_message :
						if (dwVarSysExeGetSens (w) == c_res_variable_ta_es) //tableau
							{
							taille_tab = dwVarSysExeReadPtr(w,(PVOID *)&ptr_tab_mes);
							if (dwPosVarSysExe (w) == c_sys_fen_nom_document)
								{
								ptr_tab_syn = (t_buff_syn_ex *)ptr_tab_mes;
								for (x=1;x<=taille_tab;x++)
									{
									pMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, (pwPosition->dwPosCourEx)+((ptr_tab_syn->no_synoptique)-1));
									StrCopy (pMes,ptr_tab_syn->message);
									ptr_tab_syn++;
									}
								}
							else
								{
								for (x=1;x<=taille_tab;x++)
									{
									pMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, (pwPosition->dwPosCourEx)+(x-1));
									StrCopy (pMes,ptr_tab_mes);
									ptr_tab_mes++;
									}
								}
							MemLibere((PVOID *)(&ptr_tab_mes));
							}
						else
							{
							pMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, (pwPosition->dwPosCourEx));
							StrCopy (pMes,pszVarSysExeReadMess (w));
							}
						break;
						
					default :
						break;
					}
				}
			}
		}
	} // recoit_tm

// --------------------------------------------------------------------------
void emet_tm (void)
  {
  PX_VAR_SYS	pwPosition = (X_VAR_SYS*)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_etat_menu);
  PBOOL    pBool = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx));
  PBOOL    pBool1 = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, (pwPosition->dwPosCourEx));

  if ((*pBool) && (!*pBool1))
    {
    MnuEtatToutGris();
    }
  if ((!*pBool) && (*pBool1))
    {
    MnuEtatLance();
    }
  if (force_arret_ex)
    {
    pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, arret_exploitation);
    pBool      = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx));
    (*pBool) = TRUE ;
    }
  if (force_retour_sys)
    {
    pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, retour_systeme);
    pBool      = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx));
    (*pBool) = TRUE ;
    }

  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme,arret_exploitation);
  pBool1     = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx));
  if (*pBool1)
    {
    MnuEtatArrete();
    }

  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, efface_visu);
  pBool      = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pwPosition->dwPosCourEx));
  if ((*pBool)||(*pBool1))
    {
    (*pBool) = FALSE;
    // ------------------------ Effacement de la fenetre texte
    ScrAcces ();
    ScrClear ();
    ScrFinAcces ();
    ScrRefresh ();
    }
  }

// --------------------------------------------------------------------------
void affiche_message_texte (char *pszMessage)
  {
  int         iPosX = (int) CPcsVarEx::fGetValVarSysNumEx(pos_x);
  int         iPosY = (int) CPcsVarEx::fGetValVarSysNumEx(pos_y);

  // Ecriture sur la fenetre texte
  ScrAcces ();
  ScrGotoLgnCol    (iPosY, iPosX);
  ScrWriteLn       (pszMessage);
  ScrGetLgnCol     (&iPosY, &iPosX);
  ScrFinAcces ();
  ScrRefresh ();

   if (!CPcsVarEx::bGetValVarSysLogEx(tabule_x))
		CPcsVarEx::SetValVarSysNumEx(pos_x, (FLOAT) iPosX);

   if (!CPcsVarEx::bGetValVarSysLogEx(tabule_y))
		CPcsVarEx::SetValVarSysNumEx(pos_y, (FLOAT) iPosY);
  }

// --------------------------------------------------------------------------
void ouvre_qhelp (BOOL *pbOuverture)
  {
  if (*pbOuverture)
    {
    ScrQhelpOpen (*pbOuverture);
    (*pbOuverture) = FALSE;
    }
  else
    {
    ScrQhelpOpen (*pbOuverture);
    }
  }

// --------------------------------------------------------------------------
void affiche_qhelp (char *pszNomFic)
  {
  ScrQhelpLoadTxt (pszNomFic, TRUE);
  }

// --------------------------------------------------------------------------
void init_qhelp (char *pszNomFic, char *pszTitre)
  {
  ScrQhelpSetIdx (pszNomFic, pszTitre);
  }

// --------------------------------------------------------------------------
void finit_tm (void)
  {
  //
  }
// --------------------------------------------------------------------------

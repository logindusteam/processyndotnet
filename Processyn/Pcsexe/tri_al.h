//-------------------------------- Fonctions Exportées
// Renvoie une valeur caractéristique de la date respectant la relation d'ordre
LONG CalculDate (DWORD wAnnee, DWORD wMois, DWORD wJour);

// Renvoie une valeur caractéristique de l'heure respectant la relation d'ordre
LONG CalculHeure (DWORD wHeure, DWORD wMinute, DWORD wSeconde, DWORD wMilliSeconde);

//-------------------------------------------------------------------------
// Convertis une chaine de caractère au format date JJxMMxAAAA en date sur un LONG et renvoie TRUE
// si format "?..." renvoie TRUE et le LONG vaut 0
BOOL bValideCritereDateAlarme (PCSTR pszDate, PLONG plDate);
//-------------------------------------------------------------------------
// Convertis une chaine de caractère au format HHxMMxSS en heure sur un LONG et renvoie TRUE
// si format "?..." renvoie TRUE et le LONG vaut 0 ou 23:59:59+1 si bHeureDebut TRUE ou FALSE
BOOL bValideCritereHeureAlarme (PCSTR pszHeure, BOOL bHeureDebut, LONG * plHeure);

//-------------------------------------------------------------------------
// Convertis une chaine de caractère au format numérique en niveau de priorité et renvoie TRUE
// si format "?..." renvoie TRUE
BOOL bValideCriterePrioriteAlarme (PCSTR pszPriorite, PBYTE pbyPriorite);

//-------------------------------------------------------------------------
// Convertis une chaine de caractère au format numérique en évenement et renvoie TRUE
// si format "?..." renvoie TRUE
BOOL bValideCritereEvenementsAlarme (PCSTR pszEvenement, PBYTE pbyEvenement);

//-------------------------------------------------------------------------
BOOL bAlConsulteArchive (LONG date_debut, LONG date_fin, LONG heure_debut, LONG heure_fin,
	PCSTR pszFicTri, char *nom_groupe, int priorite, int evenement);


void OuvreFicCons (char *pszNomFicArch, PHFIC phficFichierArch6, BOOL *bFicConsOuvert);

void FermeFicCons (PHFIC phficFichierArch6, BOOL *bFicConsOuvert);

BOOL lire_mess_rtat_consult (HFIC hficFichierArch6, DWORD position, char *tampon_mes);

void ArchivageRtatConsult (PHFIC phficFichierArch6, BOOL *bFicConsOuvert, char *pszFicTri, char *pszFicArch);

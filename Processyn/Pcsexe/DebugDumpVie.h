// DebugDumpVie.h: interface for the CDebugDumpVie class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEBUGDUMPVIE_H__E0C04E44_767E_4B88_8B8D_0C6EB32F3BD6__INCLUDED_)
#define AFX_DEBUGDUMPVIE_H__E0C04E44_767E_4B88_8B8D_0C6EB32F3BD6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Outil de debug
// Une instance d'un CDebugDumpVie provoque un message d'audit lors de sa construction
// et un autre lors de sa destruction.
// La Macro DumpVie(Nom), invoqu�e apr�s #include "Verif.h" permet une d�claration simple d'un tel objet
// Exemple : DumpVie(WinMain);
// NOTA cette macro est conditionelle et d�pend de la d�finition ou pas de DEBUG_DUMP_VIE

//#define DEBUG_DUMP_VIE

#ifdef DEBUG_DUMP_VIE


class CDebugDumpVie  
	{
	public:
		CDebugDumpVie(PCSTR pszNom, PCSTR pszSource, DWORD dwLigne);
		virtual ~CDebugDumpVie();
	private:
		void LogEvenement(BOOL bLancement);

		PCSTR m_pszSource;
		PCSTR m_pszNom;
		DWORD m_dwLigne;
	};

// Macro provoquant l'instanciation d'un DebugDumpVie DEBUGDUMPNom identifi� par son Nom
// ou rien (si DEBUG_DUMP_VIE n'est pas d�fini)
	#define DumpVie(Nom) CDebugDumpVie DEBUGDUMP##Nom (#Nom ,szVERIFSource,__LINE__)
#else
	#define DumpVie(Nom)
#endif
#endif // !defined(AFX_DEBUGDUMPVIE_H__E0C04E44_767E_4B88_8B8D_0C6EB32F3BD6__INCLUDED_)

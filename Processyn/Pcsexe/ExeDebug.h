//-------------------------------------------------------
// ExeDebug.h
// ex�cution Pcs : debugger
// JS Win32 11/06/97
//-------------------------------------------------------
#define LG_RECHERCHE_MAX 200
typedef struct
	{
	BOOL On;			// TRUE si fen�tre debugger hwndCode op�rationelle
	HWND hwndCode;
	HWND hwndChoixVariable;
	HWND hwndTraceVariable;
	HWND hwndForceVariable;
	HWND hwndGoLigne;
	HWND hwndTraceVaCycle;
	HSEM hSemDeb; // Pris <=> ex�cution du code suspendue
	DWORD point_arret;
	DWORD nb_lignes;
	DWORD a_executer;
	BOOL pas;
	char	szRecherche[LG_RECHERCHE_MAX];
	} CONTEXTE_DEBUGGER;


extern CONTEXTE_DEBUGGER CtxtDebugger;

DWORD PointArret(void);
void AttentePArret(void);
void AttentePasAPas(DWORD ligne);

// Traitement de la commande menu Execution Pas � Pas
void DebuggerExecutionPasAPas (HWND hwnd);

// Renvoie TRUE si le fichier d'infos de debug est pr�sent
BOOL bInfoDebugPresente (void);


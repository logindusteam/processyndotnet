/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------*/
// scrut_pg.h
// appel de pgm externe pour pcs exe
// WIN32 10/11/98

#define	NB_MAX_PROGRAMMES_LANCES_SIMULTANNES 10
class CExecuteProgrammes1
	{
	protected:
		typedef struct
			{
			CHAR		szNomProgramme [MAX_PATH];
			PROCESS_INFORMATION	ProcessInformation;//HANDLE	hProcess;
			} PROGRAMME_LANCE, *PPROGRAMME_LANCE;

	// fonctions
	public:
		CExecuteProgrammes1();
		~CExecuteProgrammes1();
	public:
		// Lance un programme externe
		DWORD dwLancePgmExt (PCSTR pszNomPgm, PCSTR pszParam); // renvoie une erreur OS

		// Test etat execution d'un programme externe 
		BOOL bExecutionPgmEnCours (PCSTR pszNomPgm);

		// Passage en avant plan d'un programme externe
		DWORD dwPassePgmExtDevant (PCSTR pszNomPgm); // renvoie une erreur OS

		// Arret d'un programme externe
		DWORD dwArretePgmExt (PCSTR pszNomPgm); // renvoie une erreur OS

	// membres locaux
	protected:
		DWORD PremierIndexLibre (void) const;
		DWORD dwIndexPgm (PCSTR pszNomPgm)	;
	
	//membres
	protected:
		PROGRAMME_LANCE m_aProgrammesLances [NB_MAX_PROGRAMMES_LANCES_SIMULTANNES];
	};


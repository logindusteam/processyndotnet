// DebugDumpVie.cpp: implementation of the CDebugDumpVie class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Audit.h"
#include "DebugDumpVie.h"

#ifdef DEBUG_DUMP_VIE

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDebugDumpVie::CDebugDumpVie(PCSTR pszNom, PCSTR pszSource, DWORD dwLigne)
	{
	m_pszSource = pszSource;
	m_pszNom = pszNom;
	m_dwLigne = dwLigne;
	LogEvenement (TRUE);
	}

CDebugDumpVie::~CDebugDumpVie()
	{
	LogEvenement (FALSE);
	}

// 
void CDebugDumpVie::LogEvenement(BOOL bLancement)
	{
	char szTemp[1000];
	if (bLancement)
		wsprintf(szTemp, "DumpVie;Start;%s",m_pszNom);
	else
		wsprintf(szTemp, "DumpVie;Stop;%s",m_pszNom);
	Audit.AuditInfo(m_pszSource,m_dwLigne,szTemp);
	}

#endif

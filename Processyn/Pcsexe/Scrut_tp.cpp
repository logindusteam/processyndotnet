/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : scrut_tp.C                                               |
 |   Auteur  : AC					        	|
 |   Date    : 7/7/93 					        	|
 |   Version : 4.00							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "UChrono.h"
#include "tipe_tp.h"
#include "Temps.h"
#include "mem.h"
#include "pcs_sys.h"

#include "scrut_tp.h"
#include "Verif.h"
VerifInit;

//--------------------------------------------------------------------------
//                      rafraichissement horloge
//--------------------------------------------------------------------------
void scrute_horo (void)
  {
  PX_VAR_SYS pdwPosition;
  PFLOAT pfTemp;
  DWORD v_heure;
  DWORD v_minute;
  DWORD v_seconde;
  DWORD v_centieme;
  DWORD v_jour;
  DWORD v_quantieme;
  DWORD v_mois;
  DWORD v_annee;

  if (existe_repere (b_va_systeme) )
    {
		TpsLireDateHeure (&v_quantieme, &v_mois, &v_annee, &v_jour, &v_heure, &v_minute, &v_seconde, &v_centieme);
    pdwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, heure);
    pfTemp = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pdwPosition->dwPosCourEx));
    (*pfTemp) = (FLOAT) v_heure;
    pdwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, minute);
    pfTemp = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pdwPosition->dwPosCourEx));
    (*pfTemp) = (FLOAT) v_minute;
    pdwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, seconde);
    pfTemp = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pdwPosition->dwPosCourEx));
    (*pfTemp) = (FLOAT) v_seconde;
    pdwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, quantieme);
    pfTemp = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pdwPosition->dwPosCourEx));
    (*pfTemp) = (FLOAT) v_quantieme;
    pdwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, mois);
    pfTemp = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pdwPosition->dwPosCourEx));
    (*pfTemp) = (FLOAT) v_mois;
    pdwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, jour);
    pfTemp = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pdwPosition->dwPosCourEx));
    (*pfTemp) = (FLOAT) v_jour;
    pdwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, annee);
    pfTemp = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pdwPosition->dwPosCourEx));
    (*pfTemp) = (FLOAT) v_annee;
    }
  }

//---------------------------------------------------------------------------
void raz_chronometre (DWORD index_cn)
  {
  PX_CHRONOMETRE	ads_chrono = (PX_CHRONOMETRE)pointe_enr (szVERIFSource, __LINE__, bx_chronometre, index_cn);
  FLOAT *ads_re;

  ChronoInitSuspendu (&(ads_chrono->chrono));
  ads_re = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ads_chrono->index_dans_bd);
  (*ads_re) = (FLOAT) 0;
  }

//---------------------------------------------------------------------------
void lance_chronometre (DWORD index_cn)
  {
  PX_CHRONOMETRE	ads_chrono = (PX_CHRONOMETRE)pointe_enr (szVERIFSource, __LINE__, bx_chronometre, index_cn);

  ChronoLanceEch (&(ads_chrono->chrono));
  }

//---------------------------------------------------------------------------
void arrete_chronometre (DWORD index_cn)
  {
  PX_CHRONOMETRE ads_chrono = (PX_CHRONOMETRE)pointe_enr (szVERIFSource, __LINE__, bx_chronometre, index_cn);

  ChronoSuspendsEch (&(ads_chrono->chrono));
  }

//---------------------------------------------------------------------------
void relance_chronometre (DWORD index_cn)
  {
  PX_CHRONOMETRE ads_chrono = (PX_CHRONOMETRE)pointe_enr (szVERIFSource, __LINE__, bx_chronometre, index_cn);

  ChronoReprendsEch (&(ads_chrono->chrono));
  }

//---------------------------------------------------------------------------
void lance_metronome (DWORD index_cn)
  {
  PX_METRONOME ads_metro = (PX_METRONOME)pointe_enr (szVERIFSource, __LINE__, bx_metronome, index_cn);

  MinuterieLanceMsEch (&(ads_metro->tempo), NB_MS_PAR_CENTIEME (ads_metro->val_duree));
  }

//---------------------------------------------------------------------------
void arrete_metronome (DWORD index_cn)
  {
  PX_METRONOME	ads_metro = (PX_METRONOME)pointe_enr (szVERIFSource, __LINE__, bx_metronome, index_cn);

  MinuterieInitSuspendue (&ads_metro->tempo); // avant : = 0x80000000;
  }

//---------------------------------------------------------------------------
//                           mise a jour  BD
//---------------------------------------------------------------------------
void  scrute_cn  (BOOL bPremier)
  {
  PX_METRONOME	ads_metro;
  PX_CHRONOMETRE	ads_chrono;
  PBOOL pbTemp;
  FLOAT *ads_re;
  __int64 interm;
  DWORD i;
  DWORD nbr_enr;

  if (existe_repere (bx_metronome))
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_metronome);
    for (i = 1; (i <= nbr_enr); i++)
      {
      ads_metro = (PX_METRONOME)pointe_enr (szVERIFSource, __LINE__, bx_metronome, i);
      pbTemp = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, ads_metro->index_dans_bd);

			// Minuterie suspendue ?
			if (bMinuterieSuspendueEch(ads_metro->tempo))
				// oui => pas d'�ch�ance
				*pbTemp = FALSE;
			else
				{
				// non => rattrape les �ch�ances perdues et relance
				(*pbTemp) = bMinuterieRelanceSiEcheanceEch (&(ads_metro->tempo), NB_MS_PAR_CENTIEME(ads_metro->val_duree));
				if (*pbTemp)
					{
					while (bMinuterieRelanceSiEcheanceEch (&(ads_metro->tempo), NB_MS_PAR_CENTIEME(ads_metro->val_duree)))
						{
						// rien
						}
					}
				}
      } // for
    } // if (existe_repere (bx_metronome))

  if (existe_repere (bx_chronometre))
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_chronometre);
    for (i = 1; (i <= nbr_enr); i++)
      {
      ads_chrono = (PX_CHRONOMETRE)pointe_enr (szVERIFSource, __LINE__, bx_chronometre, i);
      interm = NB_CENTIEME_PAR_MS (i64ValChronoMsEch (&(ads_chrono->chrono)));
      if (interm >= 0)
        {
        ads_re = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ads_chrono->index_dans_bd);
        (*ads_re) = (FLOAT)interm;
        }
      } /* for */
    } /* existe metronome */
  }


//---------------------------------------------------------------------------
//                  Gestion index pour Scrut_co et Gener_co
//---------------------------------------------------------------------------
DWORD index_spec_metro (DWORD index_dans_bd)
  {
  PX_METRONOME	ads_metro;
  BOOL trouve;
  DWORD i;
  DWORD nbr_enr;

  trouve = FALSE;
  i = 0;
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_metronome);
  while ((!trouve) && (i < nbr_enr))
    {
    i++;
    ads_metro = (PX_METRONOME)pointe_enr (szVERIFSource, __LINE__, bx_metronome, i);
    trouve = (BOOL)(ads_metro->index_dans_bd == index_dans_bd);
    } /* while */
  if (trouve)
    {
    return (i);
    }
  else
    {
    return (0);
    }
  }

//---------------------------------------------------------------------------
DWORD index_spec_chrono (DWORD index_dans_bd)
  {
  PX_CHRONOMETRE	ads_chrono;
  BOOL trouve;
  DWORD i;
  DWORD nbr_enr;

  trouve = FALSE;
  i = 0;
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_chronometre);
  while ((!trouve) && (i < nbr_enr))
    {
    i++;
    ads_chrono = (PX_CHRONOMETRE)pointe_enr (szVERIFSource, __LINE__, bx_chronometre, i);
    trouve = (BOOL)(ads_chrono->index_dans_bd == index_dans_bd);
    } /* while */
  if (trouve)
    {
    return (i);
    }
  else
    {
    return (0);
    }
  }

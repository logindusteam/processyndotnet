// UDCOM.cpp
// Gestion DCOM Client
#include "stdafx.h"
#include <OLECTL.h>
#include <OLE2.h>
#include "UDCOM.h"

#ifdef ACCES_FONCTIONS_COM_DYNAMIQUE
	#include "DLLMan.h"
	#include "USignale.h"
	#include "Verif.h"
VerifInit;
#endif

//---------------------------------------------------------
// Variable globale 
// Interface sur le gestionnaire m�moire de COM
IMalloc * pDCOMMem = NULL;

#ifdef ACCES_FONCTIONS_COM_DYNAMIQUE

#define NB_LIENS_OLE32 7
#define NB_LIENS_OLEAUT32 5

// D�claration de pointeurs de fonctions DCOM utilis�es
typedef struct
	{
	HRESULT (STDAPICALLTYPE * pfnCLSIDFromProgID) (LPCOLESTR lpszProgID, LPCLSID lpclsid);
	HRESULT (STDAPICALLTYPE * pfnCoGetClassObject)(REFCLSID rclsid, DWORD dwClsContext, LPVOID pvReserved, REFIID riid, LPVOID FAR* ppv);
	HRESULT (STDAPICALLTYPE * pfnCoGetMalloc)(DWORD dwMemContext, LPMALLOC FAR* ppMalloc);
	HRESULT (STDAPICALLTYPE * pfnCoInitialize)(LPVOID pvReserved);
	void    (STDAPICALLTYPE * pfnCoUninitialize)(void);
	HRESULT (STDAPICALLTYPE * pfnCoCreateInstanceEx)(REFCLSID Clsid, IUnknown * punkOuter, // only relevant locally
		DWORD dwClsCtx, COSERVERINFO * pServerInfo, DWORD dwCount, MULTI_QI * pResults);
	HRESULT (STDAPICALLTYPE * pfnCoInitializeSecurity)(PSECURITY_DESCRIPTOR pSecDesc, LONG cAuthSvc,
		SOLE_AUTHENTICATION_SERVICE *asAuthSvc, void *pReserved1, DWORD dwAuthnLevel,
		DWORD dwImpLevel, void *pReserved2, DWORD dwCapabilities, void *pReserved3);
	} FONCTIONS_OLE32;

// D�claration de pointeurs de fonctions DCOM utilis�es
typedef struct
	{
	UINT (STDAPICALLTYPE * pfnSysStringLen) (BSTR bstrSource);
	void (STDAPICALLTYPE * pfnVariantInit)(VARIANTARG * pvarg);
	HRESULT (STDAPICALLTYPE * pfnVariantClear)(VARIANTARG * pvarg);
	HRESULT (STDAPICALLTYPE * pfnVariantCopy)(VARIANTARG * pvargDest, VARIANTARG * pvargSrc);
	HRESULT (STDAPICALLTYPE * pfnVariantChangeType)(VARIANTARG * pvargDest, VARIANTARG * pvarSrc, USHORT wFlags, VARTYPE vt);
	} FONCTIONS_OLEAUT32;

// instanciation globale de ces pointeurs de fonctions DCOM
static FONCTIONS_OLE32 LiensOLE32;

// instanciation globale de ces pointeurs de fonctions DCOM
static FONCTIONS_OLEAUT32 LiensOLEAUT32;

// Table de lien � l'ex�cution avec la DLL OLE32
static LIEN_PFONCTION aLiensOLE32 [NB_LIENS_OLE32] =
	{
	{(FARPROC *)&LiensOLE32.pfnCoGetMalloc, "CoGetMalloc"},
	{(FARPROC *)&LiensOLE32.pfnCLSIDFromProgID, "CLSIDFromProgID"},
	{(FARPROC *)&LiensOLE32.pfnCoGetClassObject, "CoGetClassObject"},
	{(FARPROC *)&LiensOLE32.pfnCoInitialize, "CoInitialize"},
	{(FARPROC *)&LiensOLE32.pfnCoUninitialize, "CoUninitialize"},
	{(FARPROC *)&LiensOLE32.pfnCoCreateInstanceEx, "CoCreateInstanceEx"},
	{(FARPROC *)&LiensOLE32.pfnCoInitializeSecurity, "CoInitializeSecurity"},
	};

// Table de lien � l'ex�cution avec la DLL OLEAUT32
static LIEN_PFONCTION aLiensOLEAUT32 [NB_LIENS_OLEAUT32] =
	{
	{(FARPROC *)&LiensOLEAUT32.pfnSysStringLen, "SysStringLen"},
	{(FARPROC *)&LiensOLEAUT32.pfnVariantInit, "VariantInit"},
	{(FARPROC *)&LiensOLEAUT32.pfnVariantClear, "VariantClear"},
	{(FARPROC *)&LiensOLEAUT32.pfnVariantCopy, "VariantCopy"},
	{(FARPROC *)&LiensOLEAUT32.pfnVariantChangeType, "VariantChangeType"},
	};

static HINSTANCE hmodOle32 = NULL;
static PCSTR pszNomOle32 = "OLE32.dll";

static HINSTANCE hmodOleAUT32 = NULL;
static PCSTR pszNomOleAUT32 = "OLEAUT32.dll";

#undef CLSIDFromProgID
#undef CoGetClassObject
#undef CoGetMalloc
#undef CoInitialize
#undef CoUninitialize
#undef CoCreateInstanceEx
#undef CoInitializeSecurity

#undef SysStringLen
#undef VariantInit
#undef VariantClear
#undef VariantCopy
#undef VariantChangeType

// Liens blind� � chaque fonction DCOM �ventuellement charg�e
HRESULT _CLSIDFromProgID (LPCOLESTR lpszProgID, LPCLSID lpclsid)
	{
	return LiensOLE32.pfnCLSIDFromProgID ? LiensOLE32.pfnCLSIDFromProgID (lpszProgID, lpclsid) : E_FAIL;
	}

HRESULT _CoGetClassObject (REFCLSID rclsid, DWORD dwClsContext, LPVOID pvReserved, REFIID riid, LPVOID FAR* ppv)
	{
	return LiensOLE32.pfnCoGetClassObject? LiensOLE32.pfnCoGetClassObject (rclsid, dwClsContext, pvReserved, riid, ppv): E_FAIL;
	}

HRESULT _CoGetMalloc (DWORD dwMemContext, LPMALLOC FAR* ppMalloc)
	{
	return LiensOLE32.pfnCoGetMalloc? LiensOLE32.pfnCoGetMalloc(dwMemContext, ppMalloc): E_FAIL;
	}

HRESULT _CoInitialize (LPVOID pvReserved)
	{
	return LiensOLE32.pfnCoInitialize? LiensOLE32.pfnCoInitialize (pvReserved): E_FAIL;
	}

void _CoUninitialize (void)
	{
	if (LiensOLE32.pfnCoUninitialize)
		LiensOLE32.pfnCoUninitialize ();
	}

// partie DCOM (DCOM doit �tre install� sp�cifiquement sur Windows 95
HRESULT _CoCreateInstanceEx (REFCLSID Clsid, IUnknown * punkOuter, // only relevant locally
	DWORD dwClsCtx, COSERVERINFO * pServerInfo, DWORD dwCount, MULTI_QI * pResults)
	{
	return LiensOLE32.pfnCoCreateInstanceEx ? LiensOLE32.pfnCoCreateInstanceEx (Clsid, punkOuter, dwClsCtx, pServerInfo, dwCount, pResults) : E_FAIL;
	}

HRESULT _CoInitializeSecurity (PSECURITY_DESCRIPTOR pSecDesc, LONG cAuthSvc,
	SOLE_AUTHENTICATION_SERVICE *asAuthSvc, void *pReserved1, DWORD dwAuthnLevel,
	DWORD dwImpLevel, void *pReserved2, DWORD dwCapabilities, void *pReserved3)
	{
	return LiensOLE32.pfnCoInitializeSecurity ? LiensOLE32.pfnCoInitializeSecurity (pSecDesc, cAuthSvc, asAuthSvc, pReserved1,
			dwAuthnLevel, dwImpLevel, pReserved2, dwCapabilities, pReserved3) : E_FAIL;
	}

UINT _SysStringLen (BSTR bstrSource)
	{
	return LiensOLEAUT32.pfnSysStringLen ? LiensOLEAUT32.pfnSysStringLen(bstrSource) : E_FAIL;
	}

void _VariantInit(VARIANTARG * pvarg)
	{
	if(LiensOLEAUT32.pfnVariantInit)
		LiensOLEAUT32.pfnVariantInit(pvarg);
	}

HRESULT _VariantClear (VARIANTARG * pvarg)
	{
	return LiensOLEAUT32.pfnVariantClear ? LiensOLEAUT32.pfnVariantClear(pvarg) : E_FAIL;
	}

HRESULT _VariantCopy (VARIANTARG * pvargDest, VARIANTARG * pvargSrc)
	{
	return LiensOLEAUT32.pfnVariantCopy ? LiensOLEAUT32.pfnVariantCopy(pvargDest, pvargSrc) : E_FAIL;
	}

HRESULT _VariantChangeType (VARIANTARG * pvargDest, VARIANTARG * pvarSrc, USHORT wFlags, VARTYPE vt)
	{
	return LiensOLEAUT32.pfnVariantChangeType ? LiensOLEAUT32.pfnVariantChangeType(pvargDest, pvarSrc, wFlags, vt) : E_FAIL;
	}

#define CLSIDFromProgID _CLSIDFromProgID
#define CoGetClassObject _CoGetClassObject
#define CoGetMalloc _CoGetMalloc
#define CoInitialize _CoInitialize
#define CoUninitialize _CoUninitialize
#define CoCreateInstanceEx _CoCreateInstanceEx 
#define CoInitializeSecurity _CoInitializeSecurity

#define SysStringLen _SysStringLen
#define VariantInit _VariantInit
#define VariantClear _VariantClear
#define VariantCopy _VariantCopy
#define VariantChangeType _VariantChangeType
//-----------------------------------------------------
// Libere la DLL DCOM (tableau local aLiens mis � jour)
static BOOL bLibereDllsDCOM (void)
	{
	BOOL bRes = TRUE;

	// DLL OLE32 charg�e ?
	if (hmodOle32)
		{
		// oui => essaye de la d�charger
		bRes = bLibereLienDLL(&hmodOle32, aLiensOLE32, NB_LIENS_OLE32);
		VerifWarning(bRes);
		}

	// DLL OLEAUT32 charg�e ?
	if (hmodOleAUT32)
		{
		// oui => essaye de la d�charger
		bRes = bLibereLienDLL(&hmodOleAUT32, aLiensOLEAUT32, NB_LIENS_OLEAUT32);
		VerifWarning(bRes);
		}
	return bRes;
	} // bLibereDllsDCOM

//-----------------------------------------------------
// Lie l'application aux DLLs DCOM (tableau local aLiens mis � jour)
static BOOL bLieDllsDCOM (void)
	{
	BOOL bLienDCOM = bLieDLL(&hmodOle32, pszNomOle32, aLiensOLE32, NB_LIENS_OLE32);

	if (!bLienDCOM)
		{
		SignaleWarnExit ("Invalid or unreachable OLE32.dll\nVerify your DCOM installation.");
		bLibereDllsDCOM();
		}
	else
		{
		bLienDCOM = bLieDLL(&hmodOleAUT32, pszNomOleAUT32, aLiensOLEAUT32, NB_LIENS_OLEAUT32);
		if (!bLienDCOM)
			{
			SignaleWarnExit ("Invalid or unreachable OLEAUT32.dll\nVerify your DCOM installation.");
			bLibereDllsDCOM();
			}
		}

	return bLienDCOM;
	} // bLieDllsDCOM

#endif

//---------------------------------------------------------
// Initialisation d'une t�che utilisant DCOM
BOOL bDCOMInitialiseLocal (BOOL bInitialiseCOM)
	{
#ifdef ACCES_FONCTIONS_COM_DYNAMIQUE
	bLieDllsDCOM();
#endif
	BOOL	bRes = TRUE;

	if (bInitialiseCOM)
		{
		bRes = FALSE;

		// initialisation COM en mode appartement thread Ok ?
		HRESULT	hr = CoInitialize(NULL); // $$ appeler CoInitializeEx

		if (SUCCEEDED(hr)) 
			{
			// oui => initialisation DCOM Ok ?
			hr = CoInitializeSecurity(
				NULL,   // Droits d'acc�s par d�faut
				-1,     // Nombre d'entr�es dans asAuthSvc 
				NULL,   // Noms � enregistrer
				NULL,   // Reserv�
				RPC_C_AUTHN_LEVEL_NONE,    // authentication pour les proxy
				RPC_C_IMP_LEVEL_IMPERSONATE,// impersonation par d�faut pour les proxy
				NULL,                      // r�serv�
				EOAC_NONE,                 // pas de capacit� particuli�re
				NULL                       // r�serv�
				); 
			if (SUCCEEDED(hr))
				// oui => r�cup�ration du gestionnaire m�moire de COM Ok ?
				bRes = TRUE;
			else
				CoUninitialize();
			}
		} // if (bInitialiseCOM)

	if (bRes)
		{
		// oui => r�cup�ration du gestionnaire m�moire de COM Ok ?
		HRESULT hr = CoGetMalloc(MEMCTX_TASK, &pDCOMMem);
		if (FAILED(hr))
			bRes = FALSE;
		}

	return bRes;
	} // bDCOMInitialiseLocal

//---------------------------------------------------------
// Fermeture DCOM pour une t�che.
void DCOMFermeLocal (BOOL bLibereCOM)
	{
	// Libere le gestionnaire m�moire et COM
	if (pDCOMMem)
		pDCOMMem->Release();

	if (bLibereCOM)
		CoUninitialize();
	}

//---------------------------------------------------------
// Alloue et duplique une chaine WCHAR
//---------------------------------------------------------
WCHAR * WSTRClone (const WCHAR *oldstr, IMalloc *pmem)
	{
	WCHAR *newstr;
	
	if (pmem)
		newstr = (WCHAR*)pmem->Alloc(sizeof(WCHAR) * (wcslen(oldstr) + 1));
	else
		newstr = new WCHAR[wcslen(oldstr) + 1];
#pragma warning(disable : 4996)
	
	if(newstr)
		wcscpy(newstr, oldstr);

	return newstr;
	}

//---------------------------------------------------------
// Lib�re une chaine WCHAR
//---------------------------------------------------------
void WSTRFree (WCHAR * c, IMalloc *pmem)
	{
	if(c != NULL)
		{
		if(pmem)
			pmem->Free(c);
		else
			delete c;
		}
	}

//---------------------------------------------------------
// Lib�re une chaine ANSII
//---------------------------------------------------------
void SBCSFree (CHAR * c, IMalloc *pmem)
	{
	if (c != NULL)
		{
		if(pmem)
			pmem->Free(c);
		else
			delete c;
		}
	}


//---------------------------------------------------------
// Alloue et duplique une chaine WCHAR dans une chaine ANSII
//---------------------------------------------------------
CHAR * pSTRFromWSTR (const WCHAR *wcbuf, IMalloc * pmem)
	{
	int	length = wcslen(wcbuf) + 1;
	CHAR * pszRes;
	
	if(pmem)
		pszRes = (CHAR *) pmem->Alloc( length );
	else
		pszRes = new CHAR [length];
	
	if (pszRes)
		{
		for (int i=0; i<length; i++)
			pszRes[i] = (CHAR) wcbuf[i];
		}

	return pszRes;
	}

//---------------------------------------------------------
// Alloue et duplique une chaine ANSII dans une chaine WCHAR
//---------------------------------------------------------
WCHAR * pWSTRFromSTR (const CHAR *buf, IMalloc * pmem)
	{
	int	length = strlen(buf) + 1;
	WCHAR	*pszRes;
	
	if (pmem)
		pszRes = (WCHAR*)pmem->Alloc(sizeof(WCHAR) * (strlen(buf) + 1));
	else
		pszRes = new WCHAR[strlen(buf) + 1];
	
	if(pszRes)
		{
		for(int i=0; i<length; i++)
			pszRes[i] = (WCHAR) buf[i];
		}
	return pszRes;
	}

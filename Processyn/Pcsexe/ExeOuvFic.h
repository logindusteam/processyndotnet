// --------------------------------------------------------------------------------------
// Pcsexe exclusivement - code utilisateur (EXCUTE_F38): 
// Gestion d'une boite de dialogue d'ouverture de fichier
//------------------------------------------------------------------------------------------

// R�sultats de wDlgNomFichierPcs (selon bOuvreLaBoite)
#define OUVERTURE_BOITE_OUV_FIC_OK 0
#define OUVERTURE_BOITE_OUV_FIC_IMPOSSIBLE 1

#define BOITE_OUV_FIC_OUVERTE 0
#define BOITE_OUV_FIC_TERMINEE_OK 1
#define BOITE_OUV_FIC_TERMINEE_ABANDON 2

//------------------------------------------------------------------------------------------
// Demande soit l'ouverture de la boite de s�lection d'un fichier, soit son �tat
// Remarque : une seule boite peut �tre ouverte � la fois
DWORD wDlgNomFichierPcs (BOOL bOuvreLaBoite, PSTR pszPathDeRecherche, PCSTR pszTypeFichierDeRecherche);

//------------------------------------------------------------------------------------------
// Appel effectif de la boite de dialogue d'ouverture de fichier par Wexe
// (les param�tres sont globaux car une seule instance � la fois de cette boite de dialogue)
void DlgOuvreBoiteSelectFic (void);


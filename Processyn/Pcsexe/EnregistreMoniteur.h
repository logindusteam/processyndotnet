// EnregistreMoniteur.h: interface for the CEnregistreMoniteur class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENREGISTREMONITEUR_H__5A7C2C1A_0B37_4A62_934C_5160FD0B582F__INCLUDED_)
#define AFX_ENREGISTREMONITEUR_H__5A7C2C1A_0B37_4A62_934C_5160FD0B582F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FichierF.h"

class CEnregistreMoniteur  
	{
	public:
		// Constructeur / destructeur
		CEnregistreMoniteur();
		virtual ~CEnregistreMoniteur();

		// Donne un nom au fichier d'enregistrement
		void NommeFichier(PCSTR pszNomFichier);

		// D�marre ou arr�te l'enregistrement
		void SetEnregistrement(BOOL bOn);

		// Enregistre la donn�e si le fichier est en cours d'enregistrement (la donn�e doit contenir Cr Lf en fin)
		void EnregistreDonnees(PCSTR pszDonnees);

		BOOL bEnregistrement(){return m_bEnregistre;}

	private:
		CFichierF m_FichierF; // Fichier d'enregistrement
		BOOL m_bEnregistre; // Etat courant de l'enregistrement
		char m_szNomFichier[MAX_PATH]; // Nom courant du fichier
		BOOL m_bErreurEnregistrement; // Une erreur est intervenue depuis la derni�re ouverture
		DWORD m_dwErrCodeEnregistrement; // Code de l'erreur intervenue depuis la derni�re ouverture
		DWORD m_dwNbCarMaxFichier;	// Limite en taille � ne pas d�passer (1G par d�faut)
		DWORD m_dwNbCarDansFichier; // d�compte des caract�res �crits dans le fichier courant
		CRITICAL_SECTION m_cs; // Section critique pour permettre un acc�s multithread
	};

#endif // !defined(AFX_ENREGISTREMONITEUR_H__5A7C2C1A_0B37_4A62_934C_5160FD0B582F__INCLUDED_)

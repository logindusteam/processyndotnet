// --------------------------------------------------------------------------
// driv_im.h Gestion de "Line Printer" (impression sur un port serie ou // sans formatage).
// L'acces aux fonctions de driv_im se fait par un numero (wImp) compris entre 1 et NB_MAX_PRN
// A l'ouverture on r�alise l'association N� imprimante - index Port (type ci-dessous)
// --------------------------------------------------------------------------

// Nombre max d'imprimantes utilisables . 
#define NB_MAX_PRN  6

// --------------------------------------------------------------------------
// Ports s�ries & parall�les des imprimantes
// --------------------------------------------------------------------------
typedef enum
	{
	PRN_PORT_NONE   = -1,
	PRN_PORT_LPT1, 
	PRN_PORT_LPT2,   
	PRN_PORT_COM1, 
	PRN_PORT_COM2, 
	PRN_PORT_COM3, 
	PRN_PORT_COM4, 
	PRN_PORT_COM5,   
	PRN_PORT_COM6, 
	PRN_PORT_COM7, 
	PRN_PORT_COM8, 
	PRN_PORT_COM9, 
	PRN_PORT_COM10,
	PRN_PORT_COM11, 
	PRN_PORT_COM12, 
	PRN_PORT_COM13, 
	PRN_PORT_COM14, 
	PRN_PORT_COM15,   
	PRN_PORT_COM16, 
	PRN_PORT_COM17, 
	PRN_PORT_COM18, 
	PRN_PORT_COM19, 
	PRN_PORT_COM20,
	PRN_PORT_COM21, 
	PRN_PORT_COM22, 
	PRN_PORT_COM23, 
	PRN_PORT_COM24, 
	PRN_PORT_COM25,   
	PRN_PORT_COM26, 
	PRN_PORT_COM27, 
	PRN_PORT_COM28, 
	PRN_PORT_COM29, 
	PRN_PORT_COM30,
	PRN_PORT_COM31, 
	PRN_PORT_COM32, 
	PRN_PORT_COM33, 
	PRN_PORT_COM34, 
	PRN_PORT_COM35,   
	PRN_PORT_COM36
  } LPRN_PORT_ID;

#define PRN_PORT_MAX  (PRN_PORT_COM36 +1)


// --------------------------------------------------------------------------
// Status Imprimantes
// --------------------------------------------------------------------------
#define PRN_SERIAL_STATUS_OK                    0x0000
// -------------------------------------------- Imprimante s�rie
#define PRN_SERIAL_STATUS_QUEUE_OVERRUN         0x0001
#define PRN_SERIAL_STATUS_HARD_OVERRUN          0x0002
#define PRN_SERIAL_STATUS_PARITY_ERROR          0x0004
#define PRN_SERIAL_STATUS_FRAMING_ERROR         0x0008
#define PRN_SERIAL_STATUS_OTHER_ERROR						0x0020


// --------------------------------------------------------------------------
typedef struct
  {
  DWORD  wBaudRate;     //Vitesse: 110, 150, 300, 600, 1200, 2400, 4800, 9600, 19200
  DWORD  wDataBits;     //Nb Bits de donn�es: 5, 6, 7, 8
  DWORD  wParity;       //Parit�: 0 (Sans), 1 (Impaire), 2 (Paire), 3 (Mark), 4 (Space)
  DWORD  wStopBits;     //Nb Bits de Stop: 1, 2
  char  cXOn;          //Caract�re XON  ([CTRL]+[Q] = '\021' = chr (17))
  char  cXOff;         //Caract�re XOFF ([CTRL]+[S] = '\023' = chr (19))
  } LPRN_CONFIG_COM, *PLPRN_COM_INFO;

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// interpreter les param�tres d'une ligne de configuration par d�faut de Line printer
// --------------------------------------------------------------------------
#define MAX_UL  30
#define SIZE_UL STR_MAX_CHAR_ARRAY
BOOL bImprimanteInterpreteLigneIni (DWORD wNbUl, char tab_ul [] [SIZE_UL]);

// --------------------------------------------------------------------------
// R�cup�rer les infos de configuration de buffer 
BOOL definition_buffer_imprimante 
	(DWORD wPrn,
	DWORD *pwSizeBuf,
	DWORD *pwLinePerCycle);

// --------------------------------------------------------------------------
// R�cup�rer les infos de configuration du port
BOOL definition_port_imprimante 
	(DWORD wPrn,
	DWORD *pwPort,
	DWORD *pwBaudeRate,
	DWORD *pwDataBits,
	DWORD *pwParity,
	DWORD *pwStopBits,
	BOOL *pbIsSerial);


// --------------------------------------------------------------------------
// Ouverture d'une Imprimante Line Printer
CLpt::ERREUR_LPT LImprimanteOuvre (UINT wPrn, DWORD wPort, PLPRN_COM_INFO pComInfo);

// Fermeture d'une Imprimante Line Printer
CLpt::ERREUR_LPT LImprimanteFerme (UINT wPrn);

// Envoi une sequence d'init � l'imprimante
CLpt::ERREUR_LPT LImprimanteInit (UINT wPrn);
//
// Force en ms la valeur du timeout pour les fonctions d'attente de retour d'impression
CLpt::ERREUR_LPT LImprimanteTimeOut (UINT wPrn, DWORD lwTimeOut);
//
// Demande l'impression d'un buffer de caractere. La fonction retourne immediatement
// Attention la demande d'impression ecrase syst�matiquement le buffer d'impression.
// il est indispensable d'attendre la fin de l'impression pr�c�dente avant de re-appeller la fonction
CLpt::ERREUR_LPT LImprimanteWriteAsync (UINT wPrn, DWORD dwSizeBuf, void *pvBuf, DWORD *pdwErreurSysteme, BOOL bAvecConversion);

// Attend la fin de l'impression jusqu'au d�lai timeout et renvoi le nombre de caract�re ecrit
CLpt::ERREUR_LPT LImprimanteWaitWrite (UINT wPrn, DWORD *pdwErreurSysteme, DWORD *pdwSizeBufWritten);

// Demande l'impression d'un buffer de caractere. La fonction bloque jusqu'a fin emission (attention blocage total possible)
CLpt::ERREUR_LPT LImprimanteWrite (UINT wPrn, DWORD dwSizeBuf, void *pvBuf, DWORD *pdwErreurSysteme, DWORD *pdwSizeBufWritten, BOOL bAvecConversion);

// -------------------------- fin driv_im.h ----------------------------

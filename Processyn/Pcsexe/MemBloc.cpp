// MemBloc.cpp: implementation of the CMemBloc class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MemMan.h"
#include "UExcept.h"
#include "MemBloc.h"

#define BLK_BLOCK_NONE                  0xFFFFFFFF
#define BLK_RECORD_NONE                 0xFFFFFFFF
#define BLK_COUNT_NONE                  0xFFFFFFFF

// Erreurs
#define BLK_ERR_MEMORY                   0
#define BLK_ERR_ALREADY_EXIST            1
#define BLK_ERR_NOT_EXIST                2
#define BLK_ERR_RECORD_OVER_END          3
#define BLK_ERR_RECORD_OVERFLOW          4
#define BLK_ERR_RECORD_SIZE              5
#define BLK_ERR_RECORD_OVERLAP           6

#define BLK_ERR_READ_FILE                7
#define BLK_ERR_INVALID_FILE             8
#define BLK_ERR_VERSION_FILE             9
#define BLK_ERR_WRITE_FILE              10
#define BLK_ERR_OPEN_FILE               11
#define BLK_ERR_CLOSE_FILE              12
#define BLK_ERR_RECORD_SIZE_NOT_CONST   13
#define BLK_ERR_BAD_SIZE                14


// Descriptifs d'erreur DANS LE MEME ORDRE QUE LES ERREURS CI-DESSUS
static PCSTR pszErrorMessage [] = 
	{
	"System memory allocation failure",
	"User is attempting to create an existing block",
	"User is attempting to reference a non-existant block",
	"User is attempting to access over the block end",
	"User has reached block limits",
	"User \"record size\" is smaler than \"block base size\"",
	"User is attempting to exchange overlaped regions",
	"Unable to read data-base file",
	"User file is not a data-base",
	"User is attempting to load a bad version of data-base",
	"Unable to write in data-base file",
	"Unable to open data-base file",
	"Unable to close data-base file",
	"User is attempting to save a block with non-constant \"record size\"",
	"User is attempting to load a block with a bad \"record size\""
	};

static PCSTR pszBlkManagerCreate = "hBlkCreeBDVierge";
static PCSTR pszBlkCreate        = "pBlkCreeBloc";
static PCSTR pszBlkDelete        = "BlkFermeBloc";
static PCSTR pszBlkRecCount      = "dwBlkNbEnr";
static PCSTR pszBlkRecBaseSize   = "dwBlkTailleEnrDeBase";
static PCSTR pszBlkRecSize       = "dwBlkTailleEnr";
static PCSTR pszBlkRecInsert     = "pBlkInsereEnr";
static PCSTR pszBlkRecDelete     = "BlkEnleveEnr";
static PCSTR pszBlkRecSetSize    = "pBlkChangeTailleEnr";
static PCSTR pszBlkRecPointer    = "pBlkPointeEnr";
static PCSTR pszBlkRecCopy       = "BlkCopieEnr";
//static PCSTR pszBlkRecXChange    = "BlkEchangeEnr";
static PCSTR pszLoad             = "Load function";
static PCSTR pszSave             = "Save function";

//---------------------------------------------------------------------------
// Gestion d'erreur
void CMemBloc::BlkError 
	(
	//HBLK_BD hbd,
	DWORD   dwIdBloc,
	DWORD   dwNEnr,
	DWORD   dwNbEnr,
	DWORD   wError,
	PCSTR  pszFuncName,
	PCSTR  pszManagerName,
	DWORD dwNLine, PCSTR pszSource)
  {
//$$  PBD pbd = (PBD) hbd;
	char szErr[5000] = "";

//	if (pbd)
// $$		pszManagerName = pbd->szNomBD;

	if (pszSource)
		{
		wsprintf (szErr, "Error %d: %s\n"\
			"Data base = %s; Function= %s ()\n"\
			"Block   = %d; Record  = %d; Count   = %d\n",
			"Line %d (%s)",
			wError, pszErrorMessage [wError],
			pszManagerName, pszFuncName,
			(dwIdBloc+1), dwNEnr, dwNbEnr,
			dwNLine, pszSource);
		}
	else
		{
		wsprintf (szErr, "Error %d: %s\n"\
			"Data base = %s; Function= %s ()\n"\
			"Block   = %d; Record  = %d; Count   = %d",
			wError, pszErrorMessage [wError],
			pszManagerName, pszFuncName,
			(dwIdBloc+1), dwNEnr, dwNbEnr);
		}

	// Envoie une exception avec le texte d'explication
	ExcepContexte (EXCEPTION_BLKMAN, szErr);
  }


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMemBloc::CMemBloc(DWORD dwIdBloc)
	{
	m_bExist = FALSE;             // Indicateur de cr�ation $$ remplacer par signature
	m_dwTailleEnrDeBase = 0;  // Taille de base d'un enregistrement
	m_dwNbEnr = 0;						// Nombre d'enregistrements
	m_pHdrEnr = (CMemBloc::PBLOC_ENR)NULL;						// Pointeur sur Tableau d'ent�te d'un enregistrement
	m_dwIdBloc = dwIdBloc;
	}

CMemBloc::CMemBloc(DWORD dwIdBloc, DWORD dwTailleEnr, DWORD dwNbEnr)
	{
	m_dwIdBloc = dwIdBloc;
	Ouvre(dwTailleEnr,dwNbEnr);
	}

void CMemBloc::Ouvre(DWORD dwTailleEnr, DWORD dwNbEnr)
	{
	Ferme();
  m_dwTailleEnrDeBase  = dwTailleEnr;
  m_dwNbEnr     = 0;
  m_pHdrEnr     = NULL;

  if (dwNbEnr > 0)
    {
    dwNbEnr = dwNbEnr;
    m_pHdrEnr = (PBLOC_ENR)pMemAlloue (dwNbEnr * sizeof (*m_pHdrEnr));
    if (m_pHdrEnr == NULL)
      {
      //$$BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
      BlkError (m_dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
      }
    for (DWORD wIdx = 0; wIdx < dwNbEnr; wIdx++)
      {
      m_pHdrEnr [wIdx].dwTailleEnr = dwTailleEnr;
      m_pHdrEnr [wIdx].pEnr     = (PBYTE)pMemAlloue (dwTailleEnr);
      if (m_pHdrEnr [wIdx].pEnr == NULL)
        {
        //$$BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
        BlkError (m_dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
        }
      }
    //$$ptr = m_pHdrEnr [0].pEnr;
    }
  m_bExist = TRUE;
	}


CMemBloc::~CMemBloc()
	{
	Ferme();
	m_dwIdBloc = BLK_BLOCK_NONE;
	}

void CMemBloc::Ferme()
	{
	m_bExist = FALSE;             // Indicateur de cr�ation $$ remplacer par signature
  if (m_dwNbEnr > 0)
    {
    for (DWORD wIdxRec = 0; wIdxRec < m_dwNbEnr; wIdxRec++)
      {
      MemLibere ((PVOID*)(&m_pHdrEnr [wIdxRec].pEnr));
      }
    MemLibere ((PVOID*)(&m_pHdrEnr));
    }
	m_dwTailleEnrDeBase = 0;  // Taille de base d'un enregistrement
	m_dwNbEnr = 0;						// Nombre d'enregistrements
	m_pHdrEnr = (CMemBloc::PBLOC_ENR)NULL;						// Pointeur sur Tableau d'ent�te d'un enregistrement
	}

//---------------------------------------------------------------------------
// Renvoie la taille totale, en octets, du bloc
DWORD CMemBloc::dwTailleTotale ()
  {
  DWORD wSize = 0;

	if (m_dwNbEnr > 0)
		{
		for (DWORD wIdxRec = 0; wIdxRec < m_dwNbEnr; wIdxRec++)
			{
			wSize = wSize + m_pHdrEnr [wIdxRec].dwTailleEnr;
			}
		}

  return wSize;
  }

//---------------------------------------------------------------------------
// Renvoie la taille, en octets, d'un Enr
DWORD CMemBloc::dwTailleEnr (DWORD dwNEnr)
  {
  if (!m_bExist)
    //$$BlkError (hbd, dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecSize, "");
    BlkError (m_dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecSize, "");

  if (dwNEnr >= m_dwNbEnr)
    //$$BlkError (hbd, dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_RECORD_OVER_END, pszBlkRecSize, "");
    BlkError (m_dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_RECORD_OVER_END, pszBlkRecSize, "");

  return (m_pHdrEnr [dwNEnr].dwTailleEnr);
  }

//---------------------------------------------------------------------------
// Ins�re dwNbEnr Enregistrements, de taille dwTailleEnr, � la position dwNEnr
PVOID CMemBloc::pBlkInsereEnr (DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr, PCSTR pszSource, DWORD dwNLine)
  {
  if (!m_bExist)
    {
    BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecInsert, "", dwNLine, pszSource);
    }

  //-------------------------------------
  DWORD wRealRecId;
  if (dwNEnr == BLK_INSERT_END)
    {
    wRealRecId = m_dwNbEnr;
    }
  else
    {
    wRealRecId = dwNEnr;
    if (wRealRecId > m_dwNbEnr)
      {
      BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecInsert, "", dwNLine, pszSource);
      }
    }

  DWORD wNewRecCount = m_dwNbEnr + dwNbEnr;
  //-------------------------------------
  if ((wNewRecCount < m_dwNbEnr) || (wNewRecCount < dwNbEnr))
    {
    BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVERFLOW, pszBlkRecInsert, "", dwNLine, pszSource);
    }

  //-------------------------------------
  DWORD wRealRecSize;
  if (dwTailleEnr == BLK_REC_BASE_SIZE)
    {
    wRealRecSize = m_dwTailleEnrDeBase;
    }
  else
    {
    wRealRecSize = dwTailleEnr;
    if (wRealRecSize < m_dwTailleEnrDeBase)
      {
      BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_SIZE, pszBlkRecInsert, "", dwNLine, pszSource);
      }
    }

  //-------------------------------------
  if (dwNbEnr > 0)
    {
    if (wRealRecId == m_dwNbEnr)
      MemRealloue ((PVOID *)(&m_pHdrEnr), wNewRecCount * sizeof (*m_pHdrEnr));
    else
      MemInsere ((PVOID *)(&m_pHdrEnr), wRealRecId * sizeof (*m_pHdrEnr), dwNbEnr * sizeof (*m_pHdrEnr));

    m_dwNbEnr = wNewRecCount;
    //
		DWORD wLastRecId = wRealRecId + dwNbEnr;
    for (DWORD wIdx = wRealRecId; wIdx < wLastRecId; wIdx++)
      {
			CMemBloc::PBLOC_ENR pHdrEnr = &m_pHdrEnr [wIdx];
      pHdrEnr->dwTailleEnr = wRealRecSize;
      pHdrEnr->pEnr     = (PBYTE)pMemAlloue (wRealRecSize);
      if (pHdrEnr->pEnr == NULL)
        {
        BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_MEMORY, pszBlkRecInsert, "", dwNLine, pszSource);
        }
      }
    }

  return m_pHdrEnr [wRealRecId].pEnr;
  }

void CMemBloc::BlkEnleveEnr (DWORD dwNEnr, DWORD dwNbEnr, PCSTR pszSource, DWORD dwNLine)
  {
  if (!m_bExist)
    {
    BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecDelete, "", dwNLine, pszSource);
    }

  //-------------------------------------
  if (dwNEnr >= m_dwNbEnr)
    {
    BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecDelete, "", dwNLine, pszSource);
    }

  DWORD wLastRecId = dwNEnr + dwNbEnr;
  //-------------------------------------
  if ((wLastRecId < dwNEnr) || (wLastRecId < dwNbEnr))
    {
    BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecDelete, "");
    }

  //-------------------------------------
  if (wLastRecId > m_dwNbEnr)
    {
    BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecDelete, "", dwNLine, pszSource);
    }

  //-------------------------------------
  if (dwNbEnr > 0)
    {
    for (DWORD wIdx = dwNEnr; wIdx < wLastRecId; wIdx++)
      {
      MemLibere ((PVOID *)(&m_pHdrEnr [wIdx].pEnr));
      }
                              
    m_dwNbEnr = m_dwNbEnr - dwNbEnr;
    if (m_dwNbEnr == 0)
      {
      MemLibere ((PVOID *)(&m_pHdrEnr));
      m_pHdrEnr = NULL;
      }
    else
      {
      MemEnleve ((PVOID *)(&m_pHdrEnr), dwNEnr * sizeof (*m_pHdrEnr), dwNbEnr * sizeof (*m_pHdrEnr));
      }
    }
  }

PVOID CMemBloc::pBlkChangeTailleEnr (DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr)
  {
  //-------------------------------------
  if (!m_bExist)
    {
    BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecSetSize, "");
    }

  //-------------------------------------
  DWORD wLastRecId = dwNEnr + dwNbEnr;
  if (wLastRecId > m_dwNbEnr)
    {
    BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecSetSize, "");
    }

  //-------------------------------------
  DWORD wRealRecSize;
  if (dwTailleEnr == BLK_REC_BASE_SIZE)
    {
    wRealRecSize = m_dwTailleEnrDeBase;
    }
  else
    {
    wRealRecSize = dwTailleEnr;
    if (wRealRecSize < m_dwTailleEnrDeBase)
      {
      BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_SIZE, pszBlkRecSetSize, "");
      }
    }

  //-------------------------------------
  for (DWORD wIdx = dwNEnr; wIdx < wLastRecId; wIdx++)
    {
    CMemBloc::PBLOC_ENR pHdrEnr = &m_pHdrEnr [wIdx];
    pHdrEnr->dwTailleEnr = wRealRecSize;
    pHdrEnr->pEnr     = (PBYTE)pMemAlloue (wRealRecSize);
    if (pHdrEnr->pEnr == NULL)
      {
      BlkError (m_dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_MEMORY, pszBlkRecSetSize, "");
      }
    }

  return (m_pHdrEnr [dwNEnr].pEnr);
  }

PVOID CMemBloc::pBlkPointeEnr (DWORD dwNEnr, PCSTR pszSource, DWORD dwNLine)
  {
  if (!m_bExist)
    {
    BlkError (m_dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecPointer, "", dwNLine, pszSource);
    }

  if (dwNEnr >= m_dwNbEnr)
    {
    BlkError (m_dwIdBloc, dwNEnr, BLK_COUNT_NONE,BLK_ERR_RECORD_OVER_END, pszBlkRecPointer, "", dwNLine, pszSource);
    }

  return (m_pHdrEnr[dwNEnr].pEnr);
  }

// Renvoie l'adresse d'un enregistrement dans ce bloc dans cette base de donn�e 
PVOID CMemBloc::pBlkPointeEnrSansErr (DWORD dwNEnr)
  {
	PVOID pRet = NULL;
	if (m_bExist)
		{
		if (dwNEnr < m_dwNbEnr)
			pRet = m_pHdrEnr[dwNEnr].pEnr;
		}
  return pRet;
  }

void CMemBloc::BlkCopieEnr (CMemBloc * pBlocDest, DWORD wRecIdDst, DWORD wRecIdSrc, DWORD dwNbEnr)
  {
  //-------------------------------------
  // V�rification des param�tres concernant la destination :
  // Validit� bloc Dest
  if (pBlocDest == NULL)
    {
    //$$ BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecCopy, "");
    BlkError (0, wRecIdDst, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecCopy, "");
    }

  // Bloc existe
  if (!pBlocDest->m_bExist)
    {
    pBlocDest->BlkError (pBlocDest->m_dwIdBloc, wRecIdDst, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecCopy, "");
    }

  // Enr Dest
  if (wRecIdDst >= pBlocDest->m_dwNbEnr)
    {
    pBlocDest->BlkError (pBlocDest->m_dwIdBloc, wRecIdDst, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }

  DWORD wLastRecIdDst = wRecIdDst + dwNbEnr;

  // Nb d'enr dest
  if ((wLastRecIdDst < wRecIdDst) || (wLastRecIdDst < dwNbEnr))
    {
    pBlocDest->BlkError (pBlocDest->m_dwIdBloc, wRecIdDst, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }

  // Nb d'enr dest
  if (wLastRecIdDst > pBlocDest->m_dwNbEnr)
    {
    pBlocDest->BlkError (pBlocDest->m_dwIdBloc, wRecIdDst, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }


  //-------------------------------------
  // V�rification des param�tres concernant la source :
  // Bloc existe
  if (!m_bExist)
    {
    BlkError (m_dwIdBloc, wRecIdSrc, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecCopy, "");
    }

  //-------------------------------------
  if (wRecIdSrc >= m_dwNbEnr)
    {
    BlkError (m_dwIdBloc, wRecIdSrc, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }

  DWORD wLastRecIdSrc = wRecIdSrc + dwNbEnr;
  //-------------------------------------
  if ((wLastRecIdSrc < wRecIdSrc) || (wLastRecIdSrc < dwNbEnr))
    {
    BlkError (m_dwIdBloc, wRecIdSrc, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }

  //-------------------------------------
  if (wLastRecIdSrc > m_dwNbEnr)
    {
    BlkError (m_dwIdBloc, wRecIdSrc, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }


  //-------------------------------------
  if (dwNbEnr > 0)
    {
		DWORD           wIdx;
		DWORD           wIdxStart;
		DWORD           wIdxEnd;
		INT        iIdxInc;
    if (wRecIdSrc >= wRecIdDst)
      {
      wIdxStart = 0;
      wIdxEnd   = dwNbEnr;
      iIdxInc   = 1;
      }
    else
      {
      wIdxStart = dwNbEnr - 1;
      wIdxEnd   = (DWORD) -1;
      iIdxInc   = -1;
      }
		CMemBloc::PBLOC_ENR pRecHdrDst = NULL;
		//
		CMemBloc::PBLOC_ENR pRecHdrSrc = NULL;
		//
    if (m_dwTailleEnrDeBase >=  pBlocDest->m_dwTailleEnrDeBase)
      {
      for (wIdx = wIdxStart; wIdx != wIdxEnd; wIdx = wIdx + iIdxInc)
        {
        pRecHdrDst = &pBlocDest->m_pHdrEnr [wRecIdDst + wIdx];
        pRecHdrSrc = &m_pHdrEnr [wRecIdSrc + wIdx];
        if (pRecHdrDst->dwTailleEnr != pRecHdrSrc->dwTailleEnr)
          {
          pRecHdrDst->dwTailleEnr = pRecHdrSrc->dwTailleEnr;
          MemLibere ((PVOID *)(&pRecHdrDst->pEnr));
          pRecHdrDst->pEnr = (PBYTE)pMemAlloue (pRecHdrDst->dwTailleEnr);
          if (pRecHdrDst->pEnr == NULL)
            {
            pBlocDest->BlkError (m_dwIdBloc, wRecIdDst, dwNbEnr, BLK_ERR_MEMORY, pszBlkRecCopy, "");
            }
          }
        MemCopy (pRecHdrDst->pEnr, pRecHdrSrc->pEnr, pRecHdrSrc->dwTailleEnr);
        }
      }
    else
      {
      for (wIdx = wIdxStart; wIdx != wIdxEnd; wIdx = wIdx + iIdxInc)
        {
        pRecHdrDst = &pBlocDest->m_pHdrEnr [wRecIdDst + wIdx];
        pRecHdrSrc = &m_pHdrEnr [wRecIdSrc + wIdx];
        if (pRecHdrSrc->dwTailleEnr < pBlocDest->m_dwTailleEnrDeBase)
          {
          pBlocDest->BlkError (m_dwIdBloc, wRecIdDst, dwNbEnr, BLK_ERR_RECORD_SIZE, pszBlkRecCopy, "");
          }
        if (pRecHdrDst->dwTailleEnr != pRecHdrSrc->dwTailleEnr)
          {
          pRecHdrDst->dwTailleEnr = pRecHdrSrc->dwTailleEnr;
          MemLibere ((PVOID *)(&pRecHdrDst->pEnr));
          pRecHdrDst->pEnr = (PBYTE)pMemAlloue (pRecHdrDst->dwTailleEnr);
          if (pRecHdrDst->pEnr == NULL)
            {
            pBlocDest->BlkError (m_dwIdBloc, wRecIdDst, dwNbEnr, BLK_ERR_MEMORY, pszBlkRecCopy, "");
            }
          }
        MemCopy (pRecHdrDst->pEnr, pRecHdrSrc->pEnr, pRecHdrSrc->dwTailleEnr);
        }
      }
    }
  }

// ExecuteProgrammes.cpp: implementation of the CExecuteProgrammes class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Appli.h"
#include "UStr.h"
#include "DocMan.h"
#include "ProgMan.h"
#include "ExecuteProgrammes.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------
// Constructeur
CExecuteProgrammes::CExecuteProgrammes(void)
	{
  for (DWORD wIndex = 0; (wIndex < NB_MAX_PROGRAMMES_LANCES_SIMULTANNES); wIndex++)
    {
    StrSetNull (m_aProgrammes [wIndex].szNom);
    m_aProgrammes [wIndex].ProcessInformation = PROCESS_INFORMATION_NULL;
    }
	}

//---------------------------------------------------------------
// Destructeur
CExecuteProgrammes::~CExecuteProgrammes(void)
	{
	// lib�re les �ventules handles ouverts de chaque �l�ment du tableau
  for (DWORD wIndex = 0; (wIndex < NB_MAX_PROGRAMMES_LANCES_SIMULTANNES); wIndex++)
    {
		bProgLibereProcessInformation (&m_aProgrammes [wIndex].ProcessInformation);
    }
	}

//---------------------------------------------------------------
// Renvoie le premier programme libre (non utilis�) ou NULL si plus de programme libre
CExecuteProgrammes::PPROGRAMME CExecuteProgrammes::pProgrammeLibre (void)
  {
	PPROGRAMME pProgrammeRes = NULL;
	for (DWORD wIndex = 0; wIndex < NB_MAX_PROGRAMMES_LANCES_SIMULTANNES; wIndex++)
		{
		if (!bProgUtilise (&m_aProgrammes[wIndex].ProcessInformation))
			{
			pProgrammeRes = &m_aProgrammes[wIndex];
			break;
			}
		}
  return pProgrammeRes;
  }

//---------------------------------------------------------------
// Renvoie l'adresse d'un programme en cours d'ex�cution ou NULL sinon
CExecuteProgrammes::PPROGRAMME CExecuteProgrammes::pProgrammeTrouve (PCSTR pszNomPgm)
  {
	PPROGRAMME pProgrammeRes = NULL;
  if (!StrIsNull(pszNomPgm))
    {
		for (DWORD wIndex = 0; wIndex < NB_MAX_PROGRAMMES_LANCES_SIMULTANNES; wIndex++)
      {
      if ((bProgUtilise (&m_aProgrammes[wIndex].ProcessInformation)) &&
        (bStrEgales (m_aProgrammes[wIndex].szNom, pszNomPgm)))
				{
				pProgrammeRes = &m_aProgrammes[wIndex];
				break;
				}
      }
    }
  return pProgrammeRes;
  }

//--------------------------------------------------------
// Test l'�tat execution d'un programme externe
BOOL CExecuteProgrammes::bExecutionEnCours (PCSTR pszNomPgm)
  {
	// programme d�ja lanc� ?
  PPROGRAMME pProgramme = pProgrammeTrouve (pszNomPgm);
  BOOL bTrouve = pProgramme != NULL;
  if (bTrouve)
    {
		// oui => ex�cution en cours si pas encore arr�t�
		bProgFini (&pProgramme->ProcessInformation, FALSE);
    bTrouve = bProgUtilise (&pProgramme->ProcessInformation);
    }
  return bTrouve;
  }

//--------------------------------------------------------
// Lance un programme externe
DWORD CExecuteProgrammes::dwLance (PCSTR pszNomPgm, PCSTR pszParam) // TRUE si Ok
  {
	BOOL	dwRes = ERROR_ALREADY_EXISTS;

	// Programme d�ja en cours d'ex�cution ?
	if (!bExecutionEnCours (pszNomPgm))
    {
		// non => essaye de l'ex�cuter
    PPROGRAMME pProgramme = pProgrammeLibre();
    if (pProgramme)
      {
      StrCopy (pProgramme->szNom, pszNomPgm);
			if (bProgExecute (&pProgramme->ProcessInformation, FALSE, pProgramme->szNom, pszParam, NULL, 0, pszPathDoc()))
				dwRes = NO_ERROR;
			else
				dwRes = GetLastError();
      }
    else
      dwRes = ERROR_OUTOFMEMORY;
    }

	return dwRes;
  }

//--------------------------------------------------------
DWORD CExecuteProgrammes::dwActive (PCSTR pszNomPgm)
  {
	DWORD dwRes = NO_ERROR;

	// Nom programme autre que "" ?
	if (pszNomPgm[0])
		{
		// oui => essaye d'activer le programme sp�cifi�
	  PPROGRAMME pProgramme = pProgrammeTrouve (pszNomPgm);
		if (pProgramme)
			{
			bProgFini (&pProgramme->ProcessInformation, FALSE);
			if (bProgUtilise (&pProgramme->ProcessInformation))
				{
				if (!bProgActive (&pProgramme->ProcessInformation))
					dwRes = GetLastError();
				}
			else
				dwRes = ERROR_MOD_NOT_FOUND;
			}
		else
			dwRes = ERROR_FILE_NOT_FOUND;
		}
  else
    {
		// non => Auto activation
    SetForegroundWindow (Appli.hwnd);
    }
	return dwRes;
  }

//--------------------------------------------------------
// Arret d'un programme externe
DWORD CExecuteProgrammes::dwArrete (PCSTR pszNomPgm)
  {
	DWORD dwRes = NO_ERROR;
  PPROGRAMME pProgramme = pProgrammeTrouve (pszNomPgm);
  if (pProgramme)
    {
    if (bProgUtilise (&pProgramme->ProcessInformation))
      {
      if (!bProgArrete (&pProgramme->ProcessInformation, TIME_OUT_ABORT_STANDARD))
				dwRes = GetLastError();
      }
    }
	return dwRes;
  }

//--------------------------------------------------------
// Arr�te la prise en compte d'un programme sans l'arr�ter
DWORD CExecuteProgrammes::dwLibere (PCSTR pszNomPgm) // renvoie une erreur OS
  {
	DWORD dwRes = NO_ERROR;
  PPROGRAMME pProgramme = pProgrammeTrouve (pszNomPgm);
  if (pProgramme)
    {
		if (bProgUtilise (&pProgramme->ProcessInformation))
			{
			if (bProgLibereProcessInformation (&pProgramme->ProcessInformation))
				{
				if (!bProgLibereProcessInformation (&pProgramme->ProcessInformation))
					dwRes = GetLastError();
				}
			}
    }
	return dwRes;
  }


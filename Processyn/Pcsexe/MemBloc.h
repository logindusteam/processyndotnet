// MemBloc.h: interface for the CMemBloc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MEMBLOC_H__1FCCA8F9_AD2D_4BA5_A8C4_813ABBC724E7__INCLUDED_)
#define AFX_MEMBLOC_H__1FCCA8F9_AD2D_4BA5_A8C4_813ABBC724E7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define BLK_INSERT_END            0xffffffff
#define BLK_REC_BASE_SIZE         0xffffffff

class CMemBloc  
	{
	public:
		// Cr�e un bloc avec bExiste = FALSE
		CMemBloc(DWORD dwIdBloc);
		// Cr�e un bloc complet
		CMemBloc(DWORD dwIdBloc, DWORD dwTailleEnr, DWORD dwNbEnr); 
		virtual ~CMemBloc();

		// Initialise le bloc complet
		void Ouvre(DWORD dwTailleEnr, DWORD dwNbEnr); 
		// Lib�re le bloc et le marque comme non existant
		void Ferme();

	// Ent�te d'un enregistrement
	typedef struct
		{
		DWORD		dwTailleEnr;						// Taille d'un enregistrement
		PBYTE	  pEnr;                   // Pointeur sur enregistrement
		} BLOC_ENR,*PBLOC_ENR;

	public:
		// Propri�t�s
		
		// TRUE si Bloc couramment utilisable
		BOOL bExiste (){return m_bExist;}
		// Nb d'enregistrements si le bloc existe, 0 sinon
		DWORD dwNbEnr () {if (m_bExist) return m_dwNbEnr; else return 0;}
		// Taille enr de base
		DWORD dwTailleEnrDeBase () {if (m_bExist) return m_dwTailleEnrDeBase; else return 0;}
		// Taille d'un Enr particulier
		DWORD dwTailleEnr (DWORD dwNEnr);
		// Taille totale, en octets, de tous les enrs du bloc
		DWORD dwTailleTotale ();

		// M�thodes

		// Ins�re dwNbEnr Enregistrements, de taille dwTailleEnr, � la position dwNEnr
		// Renvoie un pointeur sur le premier enr ins�r� (ou NULL)
		PVOID pBlkInsereEnr (DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr, PCSTR pszSource, DWORD dwNLine);
		void BlkEnleveEnr (DWORD dwNEnr, DWORD dwNbEnr, PCSTR pszSource, DWORD dwNLine);
		PVOID pBlkChangeTailleEnr (DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr);
		PVOID pBlkPointeEnr (DWORD dwNEnr, PCSTR pszSource, DWORD dwNLine);
		// Renvoie l'adresse d'un enregistrement dans ce bloc
		PVOID pBlkPointeEnrSansErr (DWORD dwNEnr);
		void BlkCopieEnr (CMemBloc * pBlocDest, DWORD wRecIdDst, DWORD wRecIdSrc, DWORD dwNbEnr);

	private:
		// Propri�t�s
		BOOL				m_bExist;             // Indicateur de cr�ation $$ remplacer par signature
		DWORD				m_dwIdBloc;         // Id du Bloc
		DWORD				m_dwTailleEnrDeBase;  // Taille de base d'un enregistrement
		DWORD				m_dwNbEnr;						// Nombre d'enregistrements
		CMemBloc::PBLOC_ENR m_pHdrEnr;						// Pointeur sur Tableau d'ent�te d'un enregistrement
		// M�thodes
		void BlkError 
			(
			//HBLK_BD hbd,
			DWORD   dwIdBloc,
			DWORD   dwNEnr,
			DWORD   dwNbEnr,
			DWORD   wError,
			PCSTR  pszFuncName,
			PCSTR  pszManagerName,
			DWORD dwNLine = 0, PCSTR pszSource = NULL);

	};
typedef CMemBloc * pCMemBloc;
#endif // !defined(AFX_MEMBLOC_H__1FCCA8F9_AD2D_4BA5_A8C4_813ABBC724E7__INCLUDED_)

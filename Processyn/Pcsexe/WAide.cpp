//-----------------------------------------------------------
// pcsqhelp.c
//
// Gestion d'une fen�tre d'aide, avec sommaire et recherche
// WIN32 7/1/98 JS
// Remarque
// Travail en cours : un objet instanci� globalement nomm� Aide
// contient tout le contexte d'aide.
// A chaque ouverture et fermeture de l'aide (ancien montre et cache)
// on enregistre et restaure ce contexte.
//----------------------------------------------------------

#include "stdafx.h"
#include <stdlib.h>

#include "std.h"
#include "Appli.h"
#include "couleurs.h"
#include "WBarOuti.h"
#include "UStr.h"
#include "PathMan.h"
#include "DocMan.h"
#include "BufMan.h"
#include "FMan.h"
#include "MemMan.h"
#include "UEnv.h"
#include "LireLng.h"

#if !defined( c_cf ) && !defined( c_ex )
    #error Compilation S�lective: Constantes c_cf OU c_ex NON DEFINIE
#else
    #if defined( c_cf ) && defined( c_ex )
        #error Compilation S�lective: Constantes c_cf ET c_ex SONT EXCLUSIVES
    #endif
#endif
//
#if defined( c_cf )
#include "IdConfLng.h" // ressources PcsConf
#endif
#if defined( c_ex )
#include "IdExeLng.h" // ressources Execution
#endif
#include "ULangues.h"

#include "Verif.h"
#include "WAide.h"

VerifInit;

#define QHLP_CMD_FERME		11
#define QHLP_CMD_COPIE		12
#define QHLP_CMD_CHERCHE  13
#define QHLP_CMD_IMPRIME  14
#define QHLP_CMD_OUVRE		15

#define QHLP_CMD_SUIVANT  16

#define QHELP_MAXIDX   16
#define LG_CHERCHE_MAX 256

#define QHELP_TEXT_NAME "Aide : "
#define QHELP_EXT ".txt"

// Type d'aide affich�e
typedef enum
	{
	AUCUNE_AIDE,
	AIDE_UTILISATEUR,
	AIDE_PCS
	} TYPE_AIDE;

// Boite de dialogue d'aide
typedef struct 
	{
	HBO			hBarreOutilVueAide;
	}ENV_VUE_AIDE, *PENV_VUE_AIDE;

typedef struct
	{
	char			szRecherche[LG_CHERCHE_MAX];
	char			szDerniereRecherche[LG_CHERCHE_MAX];
	DWORD			dwDebutCherche;
	BOOL			bMinMaj;
	} RECHERCHE;

typedef struct
	{
	int		nNbEntrees;
	char	aszPathNames [MAX_PATH][QHELP_MAXIDX];// Index de l'aide : une entr�e par th�me d'aide
	char	aszNoms [32][QHELP_MAXIDX];
	} INDEX;

typedef struct
	{
	int		nDebutSel;
	int		nFinSel;
	} TEXTE_VU;

// Contexte $$ contient le sujet d'aide

typedef struct
	{
	HWND						hwnd;
	INDEX						Index;
	TYPE_AIDE				nTypeAide; // Type d'aide ou num�ro d'index courant
	char						szNom [MAX_PATH];
	char						szFichier[MAX_PATH];
	RECHERCHE				Recherche;
	TEXTE_VU				TexteVu;
	WINDOWPLACEMENT WindowPlacement;
	char *					pBufTexte;
	DWORD						dwTailleTexte;
	} AIDE;

static AIDE Aide;

//-------------------------------------------------------------------------------
// Boite de dialogue cherche
static BOOL CALLBACK dlgprocCherche (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL mres = TRUE;

  switch (msg)
    {
		case WM_INITDIALOG:
			{
			HWND hwndTexteAide;
			DWORD dwFin = 0;
			char *ptr;
			DWORD i;

			Aide.Recherche.dwDebutCherche = 0;

			// m�morise la derni�re recherche
			StrCopy (Aide.Recherche.szDerniereRecherche, Aide.Recherche.szRecherche);

			// r�cup�ration du texte s�lectionn� dans la fen�tre d'aide
			hwndTexteAide = ::GetDlgItem (Aide.hwnd, QHELP_TEXTE_AIDE);
			::SendMessage (hwndTexteAide,EM_GETSEL, (WPARAM) &Aide.Recherche.dwDebutCherche, (LPARAM) &dwFin);
			Verif (dwFin <= Aide.dwTailleTexte);

			if ((Aide.Recherche.dwDebutCherche + LG_CHERCHE_MAX) <= dwFin)
				dwFin = Aide.Recherche.dwDebutCherche + LG_CHERCHE_MAX;

			// recopie de la s�lection juqu'� la fin ou CR ou LF 
			CopyMemory (Aide.Recherche.szRecherche, Aide.pBufTexte + Aide.Recherche.dwDebutCherche, dwFin - Aide.Recherche.dwDebutCherche);
			ptr = Aide.Recherche.szRecherche;
			for (i = 0; (i < (dwFin - Aide.Recherche.dwDebutCherche)) && (*ptr != 0) && (*ptr != 13) && (*ptr != 10); i++)
				{
				ptr++;
				}
			*ptr = 0;

			// Affichage de la chaine recherch�e
			SetDlgItemText (hwnd, QHELP_SRCH_CHAINE,Aide.Recherche.szRecherche);
			}
			break;

    case WM_COMMAND:
      if (HIWORD(mp1) == BN_CLICKED)
        {
				switch (LOWORD(mp1))
					{
					case IDCANCEL:
	          EndDialog(hwnd,0);
						break;

					case IDOK:
						::GetWindowText(::GetDlgItem(hwnd,QHELP_SRCH_CHAINE), Aide.Recherche.szRecherche, LG_CHERCHE_MAX);
						Aide.Recherche.bMinMaj = IsDlgButtonChecked (hwnd, QHELP_SRCH_MAJMIN);
						EndDialog(hwnd,1);
						break;
          }
        }
      break;

    default:
       mres = FALSE;
       break;
    }

  return (mres);
  } // dlgprocCherche

//--------------------------------------------------------------------------
static BOOL CALLBACK dlgprocIndexAide (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL mres = TRUE;
  LRESULT query_sel;
  int     i;
  HWND    hwnd_liste;

  switch (msg)
    {
    case WM_INITDIALOG:
      hwnd_liste = ::GetDlgItem(hwnd,QHELP_LB_CHOIX_INDEX);
			// remplis la liste des indexs
      for (i = 0; i < Aide.Index.nNbEntrees; i++)
        {
        ::SendMessage(hwnd_liste,LB_ADDSTRING,0,(LPARAM)(Aide.Index.aszNoms[i]));
        }
      break;

    case WM_COMMAND:
			switch (LOWORD(mp1))
				{
				case IDOK:
					query_sel = SendDlgItemMessage(hwnd,QHELP_LB_CHOIX_INDEX, LB_GETCURSEL, 0, 0);
          if (query_sel != LB_ERR)
            {
            EndDialog(hwnd, query_sel + 1);
            }
          else
            {
            EndDialog(hwnd,0);
            }
					break;

				case IDCANCEL:
          EndDialog(hwnd,0);
					break;

				case QHELP_LB_CHOIX_INDEX:
					if  (HIWORD(mp1) == LBN_DBLCLK)
						{
						// Double click sur une des entr�es d'index
						query_sel = SendDlgItemMessage (hwnd,QHELP_LB_CHOIX_INDEX, LB_GETCURSEL, 0, 0);
						if (query_sel != LB_ERR)
							{
							EndDialog (hwnd, query_sel + 1);
							}
						else
							{
							EndDialog (hwnd, 0);
							}
						}
					break;
				}
      break;

    default:
       mres = FALSE;
       break;
    }

  return (mres);
  } // dlgprocIndexAide

// -----------------------------------------------------------
// Recherche de l'occurence d'une chaine dans un buffer texte
// ATTENTION impl�ment� avec les fonctions C
static PSTR pBufChercheStr (PSTR pBuf, DWORD dwLgBuf, const char * pszCible, BOOL case_sens)
  {
	PSTR pRes = 0;

	// Quelque chose � chercher ?
	if ((*pszCible) && dwLgBuf)
		{
		char	cPremierCarRecherche = pszCible[0];
		char	cAutreCarRecherche;
		DWORD	dwOffsetRecherche = 0;
		DWORD dwLgRecherche = (DWORD)lstrlen(pszCible);
		PSTR pszTrouve;
		PSTR pszAutreTrouve;

		// insensibilit� � la casse ?
		if (!case_sens)
			{
			// oui => on recherche avec un car en min et l'autre en maj (le cas �ch�ant)
			cAutreCarRecherche =  (char)toupper (cPremierCarRecherche);
			cPremierCarRecherche = (char)tolower (cPremierCarRecherche);
			}

		// recherche du premier caract�re de la chaine recherch�e
		while ((dwOffsetRecherche + dwLgRecherche) <= dwLgBuf)
			{
			pszTrouve = (PSTR)memchr (&pBuf[dwOffsetRecherche], cPremierCarRecherche, 1 + dwLgBuf - (dwOffsetRecherche + dwLgRecherche));

		// insensibilit� � la casse ?
			if (!case_sens)
				{
				// oui => recherche de l'autre car
				pszAutreTrouve = (PSTR)memchr (&pBuf[dwOffsetRecherche], cAutreCarRecherche, 1 + dwLgBuf - (dwOffsetRecherche + dwLgRecherche));

				// prend le premier trouv�
				if (pszAutreTrouve < pszTrouve)
					pszTrouve = pszAutreTrouve;
				}

			// car identique trouv� ?
			if (pszTrouve)
				{
				// oui => v�rification chaine identique
				BOOL bTrouve;

				if (case_sens)
					bTrouve = (memcmp (pszTrouve, pszCible, dwLgRecherche) == 0);
				else
					bTrouve = (_memicmp (pszTrouve, pszCible, dwLgRecherche) == 0);

				// Chaine identique ?
				if (bTrouve)
					{
					// oui => note le r�sultat recherche termin�e
					pRes = pszTrouve;
					break;
					}

				// non => suite recherche = imm�diatement apr�s le caract�re trouv�
				dwOffsetRecherche += (pszTrouve - &pBuf[dwOffsetRecherche]) + 1;
				} // if (pszTrouve)
			else
				{
				// non => recherche termin�e
				break;
				}
			} // while ((dwOffsetRecherche + dwLgRecherche) <= dwLgBuf)
		} // 	if ((*pszCible) && dwLgBuf)

	return pRes;
  } // dwBufChercheStr

// -----------------------------------------------------------
// S�lectionne la prochaine occurence du texte recherch� dans le texte d'aide
static void TrouveEtMontre (void)
  {
	PSTR pTrouve = NULL;

	// D�but de recherche avant la fin du texte ?
	if ((Aide.Recherche.dwDebutCherche + 1) < Aide.dwTailleTexte)
		{
		// oui => cherche la prochaine occurence
		pTrouve = pBufChercheStr (Aide.pBufTexte + (Aide.Recherche.dwDebutCherche + 1), 
			Aide.dwTailleTexte - (Aide.Recherche.dwDebutCherche + 1), Aide.Recherche.szRecherche, Aide.Recherche.bMinMaj);
		}
	// pas d'occurence trouv�e dans la fin de fichier ?
	if (!pTrouve)
		{
		// oui => trouve une occurence � partir du d�but ?
		pTrouve = pBufChercheStr (Aide.pBufTexte, 
			Aide.dwTailleTexte - (Aide.Recherche.dwDebutCherche + 1), Aide.Recherche.szRecherche, Aide.Recherche.bMinMaj);
		if (pTrouve)
			// oui => fait un Beep pour signaler � l'utilisateur qu'un "tour" a �t� fait
			Beep(200,100);
		}

	if (pTrouve)
		{
		// oui => s�lectionne la et positionne le caret
		Aide.Recherche.dwDebutCherche = pTrouve - Aide.pBufTexte;
		
		SendDlgItemMessage (Aide.hwnd, QHELP_TEXTE_AIDE, EM_SETSEL, 
			(WPARAM)Aide.Recherche.dwDebutCherche, (LPARAM)(Aide.Recherche.dwDebutCherche + StrLength (Aide.Recherche.szRecherche)));

		// montre le caret
		SendDlgItemMessage (Aide.hwnd, QHELP_TEXTE_AIDE, EM_SCROLLCARET, 0, 0);
		}
	else
		// non => Pouet
    Beep(200,100);
  } // TrouveEtMontre



//--------------------------------------------------------------------------
static void ChoixNouvelIndex(void)
  {
	// choix par l'utilisateur d'un index de l'aide
  DWORD idx = LangueDialogBoxParam (MAKEINTRESOURCE(DLG_QHELP_INDEX), Aide.hwnd, dlgprocIndexAide, 0);

	// Nouveau fichier d'aide demand�
  if (idx)
    {
		// oui => on le charge
    ChargeFichierAide (Aide.Index.aszPathNames[idx - 1], FALSE);
    }
  }

//--------------------------------------------------------------------------
// Disposition :
// Outils................................Item cherch�
// ..................................................
// ............. texte aide..........................
// ..................................................
//
static void ArrangeVueAide (HWND hwnd, BOOL raf)
  {
	PENV_VUE_AIDE pEnv = (PENV_VUE_AIDE)pGetEnv (hwnd);
	int cxBord = GetSystemMetrics(SM_CXBORDER);
	int cyBord = GetSystemMetrics(SM_CYBORDER);
  RECT rcOutils, rcTemp, rcClient;
  HWND hwnd_itm;

  // positionnement de la barre d'outil en haut � gauche
  BODeplace (pEnv->hBarreOutilVueAide,0,0,BO_POS_GAUCHE | BO_POS_HAUTE);
  BOGetRect (pEnv->hBarreOutilVueAide ,&rcOutils);
  rcOutils.left += cxBord;
  rcOutils.top += cyBord;
  BODeplace (pEnv->hBarreOutilVueAide, rcOutils.left, rcOutils.top,0);
  BOMontre (pEnv->hBarreOutilVueAide,1);

  // positionnement du champ texte recherch� � droite de la barre d'outils
	::GetClientRect (hwnd, &rcClient);
  hwnd_itm = ::GetDlgItem (hwnd,QHELP_RCH_TXT);
	::GetWindowRect (hwnd_itm, &rcTemp);
  ::MoveWindow (hwnd_itm, rcOutils.right + (2 * cxBord), rcOutils.top, 
		rcClient.right - rcOutils.right - (3 * cxBord),	rcOutils.bottom, FALSE);

  // positionnement du texte de l'aide en dessous de la barre d'outil 
  hwnd_itm = ::GetDlgItem(hwnd,QHELP_TEXTE_AIDE);
  ::MoveWindow (hwnd_itm, rcClient.left + cxBord, rcOutils.bottom + cyBord,
		rcClient.right - rcClient.left - 2 * cxBord, rcClient.bottom - rcOutils.bottom - 2 * cyBord, FALSE);

  if (raf)
    {
    ::InvalidateRect(hwnd,NULL,TRUE);
    }
  } // ArrangeVueAide

//--------------------------------------------------------------------------
static void CopieSelectionAide(void)
  {
  SendDlgItemMessage (Aide.hwnd, QHELP_TEXTE_AIDE, WM_COPY, 0, 0);
  }

//-------------------------------------------------------
// Enregistre et affiche dans la barre de titre de l'aide le nouveau nom de l'aide
static void NouveauNomAide (PCSTR pszNom)
  {
  char szTemp[MAX_PATH];

  StrCopy(szTemp,QHELP_TEXT_NAME);
  StrConcat(szTemp,pszNom);
	StrCopy (Aide.szNom, pszNom);

  ::SetWindowText (Aide.hwnd, szTemp);
  }

//-------------------------------------------------------
// Pas d'aide couramment s�lectionn�e
static void LibereTexteAide (void)
  {
  if (Aide.pBufTexte)
    {
    MemLibere ((PVOID *)(&Aide.pBufTexte));
    Aide.pBufTexte = NULL;
    }
  }

//-------------------------------------------------------
// Pas d'aide couramment s�lectionn�e
static void VideTexteAide (void)
  {
  if (Aide.pBufTexte)
    {
		SetDlgItemText(Aide.hwnd, QHELP_TEXTE_AIDE, "");

		LibereTexteAide();
		Aide.dwTailleTexte = 0;
		Aide.Recherche.dwDebutCherche = 0;
		Aide.nTypeAide = AUCUNE_AIDE;
    NouveauNomAide("");
    }
  }

//-------------------------------------------------------------------------------
// Boite de dialogue modeless de visualisation de l'aide
static BOOL CALLBACK dlgprocAide (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL mres = TRUE;
  
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			PENV_VUE_AIDE pEnv = (PENV_VUE_AIDE)pCreeEnv (hwnd, sizeof (ENV_VUE_AIDE));

			Aide.hwnd = hwnd;

			// Infos disposition initialis�es ?
			if (Aide.WindowPlacement.length != 0)
				{
				// oui => restaure les
				::SetWindowPlacement (hwnd, &Aide.WindowPlacement);
				}
			else
				{
				// non => lis les et enregistre les
				Aide.WindowPlacement.length = sizeof(Aide.WindowPlacement);
				::GetWindowPlacement (hwnd, &Aide.WindowPlacement);
				}

			// cr�ation des barres d'outil
      pEnv->hBarreOutilVueAide = hBOCree (NULL, FALSE, FALSE, FALSE, 0,0);
      //bBOAjouteBouton (pEnv->hBarreOutilVueAide,"IDB_CMD_QHLP_QUIT",BO_COL_SUIVANTE,BO_BOUTON,QHLP_CMD_FERME);
      bBOAjouteBouton (pEnv->hBarreOutilVueAide,"IDB_CMD_COPIER",BO_COL_SUIVANTE,BO_BOUTON,QHLP_CMD_COPIE);
      bBOAjouteBouton (pEnv->hBarreOutilVueAide,"IDB_CMD_CHERCHER",BO_COL_SUIVANTE,BO_BOUTON,QHLP_CMD_CHERCHE);
      bBOAjouteBouton (pEnv->hBarreOutilVueAide,"IDB_CMD_QHLP_RESR",BO_COL_SUIVANTE,BO_BOUTON,QHLP_CMD_SUIVANT);
      bBOAjouteBouton (pEnv->hBarreOutilVueAide,"IDB_CMD_QHLP_OPEN",BO_COL_SUIVANTE,BO_BOUTON,QHLP_CMD_OUVRE);

      bBOOuvre(pEnv->hBarreOutilVueAide,FALSE,hwnd,0,0, BO_POS_DROITE | BO_POS_HAUTE, TRUE);

			ArrangeVueAide (hwnd, FALSE);

			// restaure le texte vu : s�lection
			SendDlgItemMessage (hwnd, QHELP_TEXTE_AIDE, EM_SETSEL, 
				(WPARAM) (&Aide.TexteVu.nDebutSel), (LPARAM) (&Aide.TexteVu.nFinSel));

			// montre le caret
			SendDlgItemMessage (hwnd, QHELP_TEXTE_AIDE, EM_SCROLLCARET, 0, 0);

			// Affichage de la chaine recherch�e
			SetDlgItemText (hwnd, QHELP_RCH_TXT, Aide.Recherche.szRecherche);

			// Recharge le fichier d'aide courant
			switch (Aide.nTypeAide)
				{
				case AIDE_UTILISATEUR:
				case AIDE_PCS:
					ChargeFichierAide (Aide.szFichier, Aide.nTypeAide == AIDE_UTILISATEUR);
					break;
				}
			}
      break;

		case WM_DESTROY:
			{
			PENV_VUE_AIDE pEnv = (PENV_VUE_AIDE)pGetEnv (hwnd);

			// Enregistre l'�tat actuel de la fen�tre
			::GetWindowPlacement (hwnd, &Aide.WindowPlacement);

			// et de la s�lection par l'utilisateur
			SendDlgItemMessage (hwnd, QHELP_TEXTE_AIDE, EM_GETSEL, (WPARAM) (&Aide.TexteVu.nDebutSel),
				(LPARAM) (&Aide.TexteVu.nFinSel));

			// lib�re les ressources
			LibereTexteAide();
			BODetruit (&pEnv->hBarreOutilVueAide);
			bLibereEnv (hwnd);
			Aide.hwnd = NULL;
			}
			break;

		case WM_SIZE:
			if (mp1 != SIZE_MINIMIZED)
				ArrangeVueAide (hwnd, TRUE);
			break;

    case WM_CLOSE:
      CacheAide();
			// pas de traitement par d�faut
      mres = FALSE;
      break;


    case WM_COMMAND:
      switch (LOWORD(mp1))
        { 
				// commandes envoy�es par la barre d'outil
				case IDCANCEL:
        case QHLP_CMD_FERME :
          CacheAide();
          break;
        case QHLP_CMD_COPIE :
          CopieSelectionAide();
          break;
        case QHLP_CMD_CHERCHE :
					{
					// param�trage de la recherche valid� ?
					if (LangueDialogBoxParam (MAKEINTRESOURCE(DLG_QHELP_CHERCHE), hwnd, dlgprocCherche, 0) != 0)
						{
						// lance la recherche � partir du curseur
						SendDlgItemMessage (hwnd, QHELP_TEXTE_AIDE, EM_GETSEL, (WPARAM)(&Aide.Recherche.dwDebutCherche), (LPARAM) 0);
						TrouveEtMontre ();
						}
					else
						{
						// Annulation : on restaure l'ancienne recherche
						StrCopy(Aide.Recherche.szRecherche,Aide.Recherche.szDerniereRecherche);
						}

					// Mise � jour de la chaine en cours de recherche
					SetDlgItemText(hwnd, QHELP_RCH_TXT, Aide.Recherche.szRecherche);
					}
          break;
        case QHLP_CMD_IMPRIME :
          break;
        case QHLP_CMD_OUVRE :
          ChoixNouvelIndex();
          break;

        case QHLP_CMD_SUIVANT :
					// relance la recherche � partir du curseur
					SendDlgItemMessage (hwnd, QHELP_TEXTE_AIDE, EM_GETSEL, (WPARAM)(&Aide.Recherche.dwDebutCherche), (LPARAM) 0);
          TrouveEtMontre ();
          break;
        default :
					mres = FALSE;
          break;
        }
      break; // WM_COMMAND

    default:
       mres = FALSE;
       break;
    }

  return (mres);
  } // dlgprocAide


//-------------------------------------------------------------------
static void AfficheErreur (char *pszFic, HWND hwnd)
  {
  char err_msg[MAX_PATH+80]="";

  if (!bLngLireErreur (425, err_msg))
    {
    StrCopy (err_msg, "Help File not found or too long: "); //$$ mettre � jour message
    }
  StrConcatChar(err_msg, ' ');
  StrConcat(err_msg, pszFic);
  //
  ::MessageBox (hwnd, err_msg, NULL, MB_OK | MB_ICONEXCLAMATION | MB_APPLMODAL);
  }

//-------------------------------------------------------------------
//						FONCTIONS EXPORTEES
// -----------------------------------------------------------

// -----------------------------------------------------------
void CacheAide (void)
  {
  ::ShowWindow (Aide.hwnd,SW_HIDE);
  }

//-------------------------------------------------------
void VideAide (void)
  {
  VideTexteAide();
  }

//-------------------------------------------------------------------
// Charge un fichier d'aide
void ChargeFichierAide 
	(
	PCSTR szNomFic,						// Nom du fichier
	BOOL bFichierUtilisateur	// TRUE : le fichier dans document; FALSE fichier avec EXEs
	)
  {
	char	szNomAide [MAX_PATH];

	// change si n�cessaire le path du fichier � afficher et ajoute l'extension
	StrCopy (szNomAide, szNomFic);
	if (bFichierUtilisateur)
		{
		pszPathNameAbsoluAuDoc (szNomAide);
		pszNomAjouteExt (szNomAide, MAX_PATH, QHELP_EXT);
		}
	else
		pszCreePathNameExt (szNomAide, MAX_PATH, Appli.szPathExe, szNomFic,QHELP_EXT);

	CFman::HF hFile = 0;
	if (CFman::bFOuvre (&hFile, szNomAide, CFman::OF_OUVRE_LECTURE_SEULE))
    {
    // effacement de l'ancien texte et buffer
		DWORD size = 0;
		if (CFman::bFLongueur (hFile, &size))
			{
			if (size >= 0xfffe)
				{
				// $$ limitation contr�le
				size = 0xfffe;
				AfficheErreur (szNomAide, Aide.hwnd); //$$ message fichier d'aide coup�
				}

			VideTexteAide();
			StrCopy (Aide.szFichier, szNomFic);
			if (bFichierUtilisateur)
				Aide.nTypeAide = AIDE_UTILISATEUR;
			else
				Aide.nTypeAide = AIDE_PCS;

			// changement du texte 
			NouveauNomAide(szNomFic);

			// mise � jour du nouveau contenu :
			// Alloue au moins un octet pour le \0 terminateur du texte d'aide
			Aide.pBufTexte = (char *)pMemAlloue (size+1);
			CFman::bFLireTout (hFile, Aide.pBufTexte, size);
			Aide.pBufTexte [size] = (char)0;
			Aide.dwTailleTexte = size;
			SetDlgItemText(Aide.hwnd, QHELP_TEXTE_AIDE, Aide.pBufTexte);
			CFman::bFFerme(&hFile);
			}
		else
			{
		  AfficheErreur (szNomAide, Aide.hwnd);
			}
    }
  else
    {
    AfficheErreur (szNomAide, Aide.hwnd);
    }
  }

//--------------------------------------------------------------------------
void AjouteIndexAide (char *mnemo, char *titre)
  {
  int i;
  BOOL trouve;

	//$$ V�rifier longueurs
  trouve = FALSE;
  for (i = 0; (i < Aide.Index.nNbEntrees) && (!trouve); i++)
    {
    trouve = bStrEgales(titre, Aide.Index.aszNoms[i]);
    }
  if (trouve)
    {
    StrCopy (Aide.Index.aszPathNames[i-1],mnemo);
    }
  else
    {
    if (Aide.Index.nNbEntrees < QHELP_MAXIDX)
      {
      StrCopy(Aide.Index.aszPathNames[Aide.Index.nNbEntrees],mnemo);
      StrCopy(Aide.Index.aszNoms[Aide.Index.nNbEntrees],titre);
      Aide.Index.nNbEntrees++;
      }
    }
  }

//--------------------------------------------------------------------------
void MontreAide (void)
  {
  if (!Aide.hwnd)
		Aide.hwnd = LangueCreateDialogParam (MAKEINTRESOURCE(DLG_QHELP_VIEW), Appli.hwnd, dlgprocAide, 0);
  ::ShowWindow (Aide.hwnd,SW_SHOW);
  }

//--------------------------------------------------------------------------
// Fermeture et lib�ration de l'aide
void FermeAide (void)
	{
	if (Aide.hwnd && IsWindow (Aide.hwnd))
		{
		::ShowWindow (Aide.hwnd,SW_SHOW);
		::DestroyWindow (Aide.hwnd);
		}
	Aide.hwnd = NULL;
	}

//--------------------------------------------------------------------------
void InitAide (void)
  {
	// initialisation des variables
	Aide.Recherche.szRecherche[0] = 0;
	Aide.Recherche.szDerniereRecherche[0] = 0;
	Aide.Recherche.bMinMaj = FALSE;
	Aide.Index.nNbEntrees = 0;
	Aide.WindowPlacement.length = 0; // signifie non initialis�
	Aide.pBufTexte = NULL;
	Aide.dwTailleTexte = 0;
	Aide.nTypeAide = AUCUNE_AIDE;
	Aide.szNom[0] = 0;
  }



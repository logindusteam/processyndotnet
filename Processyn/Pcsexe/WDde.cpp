//-------------------------------------------------------
// WDde.c
// ex�cution Pcs : Fen�tres de gestion DDE
// JS Win32 08/09/97
//-------------------------------------------------------

#include "stdafx.h"
#include "USem.h"
#include "Appli.h"
#include "DocMan.h"
#include "UEnv.h"
#include "MemMan.h"
#include "UStr.h"
#include "UDde.h"
#include "lng_res.h"
#include "tipe.h"        // Types + Constantes Processyn
#include "mem.h"         // Memory Manager
#include "pcsdlg.h"
#include "appliex.h"
#include "pcsdlgex.h"
#include "IdExeLng.h"
#include "ULangues.h"
#include "tipe_de.h"
#include "scrut_de.h"
#include "Wexe.h"
#include "Driv_sys.h"
#include "PathMan.h"
#include "FMan.h"
#include "FileMan.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "ClipBMan.h"
#include "Verif.h"
#include "USignale.h"
#include "ExeDde.h"
#include "WDde.h"
VerifInit;

typedef struct
	{
	BOOL ok;
	DWORD index;
	} DLG_LIEN_DDE_EX, *PDLG_LIEN_DDE_EX;

typedef struct
	{
	BOOL selection;
	DWORD no_item;
	} ENV_SELECTION_VARIABLE, *PENV_SELECTION_VARIABLE;

static HWND		hwndCopierLienDDE = NULL ;// Handle Boite de dialogue modeless
static HWND		hwndCollerLiaisonDDE = NULL ;// Handle Boite de dialogue modeless

//------------------------------------------------------------
//     renvoie applic topic et item pour un lien donn�
//------------------------------------------------------------
static DWORD DdeGetLinkNames (HCONV hEnvConv, UINT wNumItem, PSTR pszApplic, PSTR pszTopic, PSTR pszItem)
  {
  DWORD              wCodeRet = 0;
/*
  PLIEN_DDE       pLien;
  PITEM_DDE				ptDdeItem;
  PLISTE_LIENS_DDE paListeLiens;

  if (ptrDdeListeLien != NULL)
    {
    paListeLiens = ptrDdeListeLien;
    if (paListeLiens->pLienDde != NULL)
      {
      pLien = paListeLiens->pLienDde;
      StrCopy (pszApplic, pLien->pszApplic);
      StrCopy (pszTopic, pLien->pszTopic);
      if ((wNumItem <= pLien->wNbItems) && (pLien->ptrItems != NULL))
        {
        ptDdeItem = (PITEM_DDE) pLien->ptrItems;
        ptDdeItem += wNumItem - 1;
        StrCopy (pszItem, ptDdeItem->pszItem);
        }
      else
        {
        wCodeRet = DDE_ERR_NO_ITEM;
        }
      }
    else
      {
      wCodeRet = DDE_ERR_NO_LINK;
      }
    }
  else
    {
    wCodeRet = DDE_ERR_NO_LINK;
    }
*/
  return wCodeRet;
  }


// --------------------------------------------------------------------------
// r�cup�re les param�tres d'un lien DDE
static BOOL GetLienDde (DWORD index,char *pszApplic,char *pszTopic,char *pszItem)
  {
  DWORD            erreur;
  PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;
  BOOL         bOk;

  erreur = 0;
  bOk = FALSE;
  if (existe_repere(bx_var_dde_client_dyn))
    {
    if (index <= nb_enregistrements (szVERIFSource, __LINE__, bx_var_dde_client_dyn))
      {
      pVarClientDyn= (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn,index);
      if ((pVarClientDyn->bLienOk) && (!pVarClientDyn->bLienDetruit))
        {
        erreur = DdeGetLinkNames (pVarClientDyn->hconvClient,1,
                                  pszApplic, pszTopic, pszItem);
        if (erreur == 0)
          {
          bOk =TRUE;
          }
        }
      else
        {
        bOk =TRUE;
        StrSetNull (pszApplic);
        StrSetNull (pszTopic);
        StrSetNull (pszItem);
        }
      }
    }
  return bOk;
  }

// -----------------------------------------------------------------------
// Remplis la list box des liens DDE
static void ListBoxEditionLiensDde (HWND hdlg, PDLG_LIEN_DDE_EX data)
  {
  DWORD w;
  DWORD item;
  char chaine[256];
  char pszApplic [80];
  char pszTopic [80];
  char pszItem  [80];


  SendDlgItemMessage (hdlg, ID_DDE_LIST, LB_RESETCONTENT,0,0);
  w = 1;
  while (GetLienDde (w,pszApplic,pszTopic,pszItem))
    {
    if (!StrIsNull(pszApplic))
      {
      StrDWordToStr (chaine, w);
      StrConcat (chaine," - ");
      StrConcat (chaine,pszApplic);
      StrConcatChar (chaine, ':');
      StrConcat (chaine,pszTopic);
      StrConcatChar (chaine, ':');
      StrConcat (chaine,pszItem);
      item = LOWORD (SendDlgItemMessage (hdlg,ID_DDE_LIST, LB_INSERTSTRING,
				(WPARAM)-1, (LPARAM) chaine));
      }
    w ++;
    }
  }

// -----------------------------------------------------------------
// Renvoie le format presse papier pour un lien DDE ou 0 si probl�me
static UINT uFormatLienDDEPressePapier (void)
	{
	static UINT uLinkFormat = 0;

	if (!uLinkFormat)	
		uLinkFormat = RegisterClipboardFormat ("Link");

	return uLinkFormat;
	}


//----------------------------------------------
// Copie d'un lien DDE sur le presse papier :
static BOOL bDdeCopieLienServeur (PCSTR pszAppli, PCSTR pszTopic, PCSTR pszItem, PCSTR pszTexteValeurVar)
	{
	// enregistrement du format presse papier "Link"Ok ?
	BOOL	bRes = FALSE;
	UINT uLinkFormat = uFormatLienDDEPressePapier();

	if (uLinkFormat)
		{
		// oui => mise en forme des param�tres
		DWORD	dwNbCharsAppli = StrLength (pszAppli);
		DWORD	dwNbCharsTopic = StrLength (pszTopic);
		DWORD	dwNbCharsItem = StrLength (pszItem);
		DWORD dwTailleBuf = 0;
		PSTR pBuf = (PSTR)pMemAlloueInit0 (dwNbCharsAppli + dwNbCharsTopic + dwNbCharsItem + 4);
		
		// oui => copie du nom du lien sur le presse papier
		// Mise au format "APPLI\0TOPIC\0ITEM\0\0" des param�tres de la conversation
		StrCopy (pBuf, pszAppli);
		dwTailleBuf = dwNbCharsAppli + 1;
		StrCopy (pBuf + dwTailleBuf, pszTopic);
		dwTailleBuf += dwNbCharsTopic + 1;
		StrCopy (pBuf + dwTailleBuf, pszItem);
		dwTailleBuf += dwNbCharsItem + 2;
		
		// Place la donn�e sp�cifi�e dans le presse papier (au format sp�cifi�)
		bRes = (bClipBEcritFormat (uLinkFormat, pBuf, dwTailleBuf) && bClipBAjouteTexte(pszTexteValeurVar));
		MemLibere ((PVOID *)(&pBuf));
		}

	return bRes;
	} // bDdeCopieLienServeur



// ---------------------------------------------------
// Boite de dialogue variables dde serveur
static BOOL CALLBACK dlgprocCopierLienServeurDDE (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
  {
  LRESULT	mres = FALSE;		   // Valeur de retour

  switch (msg)
    {
    case WM_INITDIALOG:
      {
			char    szNomVar [NB_MAX_CAR];
			DWORD w;
			DWORD item;
			szNomVar [0] = '\0';
			w = 1;
			while (bNomVarDdeDyn (w, szNomVar, TRUE))
				{
				item = LOWORD (SendDlgItemMessage (hwnd, ID_DDE_LIST, LB_INSERTSTRING,
					(WPARAM)-1, (LPARAM) szNomVar));
				w ++;
				}
      mres = TRUE;
			}
      break;

    case WM_DESTROY:
			hwndCopierLienDDE = NULL;
      break;

    case WM_CLOSE:
      ::DestroyWindow(hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(wParam))
        {
				case ID_DDE_LIST:
					{
					if (HIWORD(wParam) == LBN_SELCHANGE)
						{
						DWORD	item = (DWORD)  SendDlgItemMessage (hwnd, ID_DDE_LIST, LB_GETCURSEL, 0, 0);

						if (item != LB_ERR)
							{
							// S�lection d'un nouvelle variable serveur
							PX_VAR_SERVEUR_DDE	pVarServeurDde = (PX_VAR_SERVEUR_DDE)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, item+1);
							PX_E_S				pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pVarServeurDde->wPosBxEs);

							// copie les infos de lien correspondant
							SetDlgItemText (hwnd, IDS_APPLIC, "PCSEXE");
							//SetDlgItemText (hwnd, IDS_TOPIC, pszPathNameDoc());
							// $$ac
							SetDlgItemText (hwnd, IDS_TOPIC, pszNameDoc());
							SetDlgItemText (hwnd, IDS_ITEM, pxES->pszNomVar);
							// Valide le bouton copier
							::EnableWindow (::GetDlgItem (hwnd, IDOK), TRUE);
							}
						else
							{
							// pas de s�lection : vide les infos de lien
							SetDlgItemText (hwnd, IDS_APPLIC, "");
							SetDlgItemText (hwnd, IDS_TOPIC, "");
							SetDlgItemText (hwnd, IDS_ITEM, "");
							// Inhibe le bouton copier
							::EnableWindow (::GetDlgItem (hwnd, IDOK), FALSE);
							}
						}
					}
					break;

				case IDOK:
					{
					// Copier le lien vers la variable serveur s�lectionn�e
					DWORD	item = (DWORD)  SendDlgItemMessage (hwnd, ID_DDE_LIST, LB_GETCURSEL, 0, 0);

					if (item != LB_ERR)
						{
						DWORD						wNumItem;
						DWORD						wErreur;
						char						pszApplic[DDE_MAX_NAME_SIZE];
						char			      pszTopic[DDE_MAX_NAME_SIZE];
						char				    pszItem[DDE_MAX_ITEM_SIZE];
						char						pszTexteValeurVar[256];
						PX_VAR_SERVEUR_DDE	pVarServeurDde;
						PX_E_S					pxES;
						UINT						wSizeData;
						BOOL						bTest;

						SendDlgItemMessage(hwnd, ID_DDE_LIST,LB_SETSEL, FALSE, item);
						wNumItem = item + 1;
						//
						StrCopy (pszApplic, "PCSEXE");
						//StrCopy (pszTopic, pszPathNameDoc());
						//$$ac
						StrCopy (pszTopic, pszNameDoc());

						// r�cup�re le nom de variable serveur
						pVarServeurDde = (PX_VAR_SERVEUR_DDE)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, wNumItem);
						pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pVarServeurDde->wPosBxEs);
						StrCopy (pszItem, pxES->pszNomVar);

						// r�cup�re sa valeur courante
						Verif ((wErreur = GetValeurBD (pxES->wPosEx, pxES->wTaille, pxES->wBlocGenre,
							FALSE, 0, pszTexteValeurVar, &wSizeData, FALSE, &bTest)) == 0);

						// Copie lien et valeur courante sur le presse papier
						if (!bDdeCopieLienServeur (pszApplic, pszTopic, pszItem, pszTexteValeurVar))
							SignaleWarnExit ("Erreur lors de la copie du lien");
						}
					} // IDOK
					break;

				case IDCANCEL:
					// Fermeture de la boite
          ::DestroyWindow(hwnd);
					break;
        }
      break;
    }
  return mres;
  } // dlgprocCopierLienServeurDDE

	
// --------------------------------------------------------------------------
// Supprime tous les liens DDE dans la liste des liens DDE $$ ??
static BOOL bSupprimeLienDde (DWORD item)
  {
  BOOL					bOk;
  PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;
  DWORD         wNumItem;
  DWORD         wNumItemOk;

  bOk = FALSE;
  wNumItemOk =0;
  if (existe_repere(bx_var_dde_client_dyn))
    {
    if (item <= nb_enregistrements (szVERIFSource, __LINE__, bx_var_dde_client_dyn))
      {
      for (wNumItem=1; wNumItem<=nb_enregistrements (szVERIFSource, __LINE__, bx_var_dde_client_dyn); wNumItem++)
        {
        pVarClientDyn= (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn,wNumItem);
        if ((pVarClientDyn->bLienOk) && (!pVarClientDyn->bLienDetruit))
          {
          wNumItemOk++;
          if (item == wNumItemOk)
            {
            uDDEFermeConversationClient (&pVarClientDyn->hconvClient);
            pVarClientDyn->bLienOk = FALSE;
            pVarClientDyn->bLienDetruit = TRUE;
            bOk = TRUE;
            break; //c est fini
            }
          }
        }
      }
    }
  return bOk;
  }

//--------------------------------------------------------------------------
// un serveur vient d'arreter le lien, demander s'il faut le sauvegarder
// DdeDelLien sera fait ds la phase des entrees
//--------------------------------------------------------------------------
static void TerminateLienEDynRecu (LPARAM lParam)
  {
  PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;

  pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, (DWORD) lParam);
  pVarClientDyn->bLienOk = FALSE;
  }

//--------------------------------------------------------------------------
// indication reception de datas en mode advise
static void DataEDynRecue (LPARAM lParam, DWORD wEvent, void *pData, DWORD wSizeData)
  {
  PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;

  switch (wEvent)
    {
    case DDE_DATA_RECU :
      pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, (DWORD)lParam);
      pVarClientDyn->bDataRecue = TRUE;
      break;
    }
  }

// --------------------------------------------------------------------------
// R�tablis tous les liens DDE
static BOOL  bRetablisLienDde (void)
  {
  BOOL	bOk = FALSE;
  // DWORD	erreur = 0;

  if (existe_repere(bx_var_dde_client_dyn))
    {
		char	pszFile[MAX_PATH];
		HFIC hfile;

    // Fichier des liens  = "Fichier Appli".DDE
    StrCopy (pszFile, pszPathNameDoc());
		pszNomAjouteExt (pszFile, MAX_PATH,"DDE");

		// Ouverture Ok ?
    if ((uFileOuvre  (&hfile, pszFile, sizeof(t_sav_dde_e_dyn), CFman::OF_OUVRE_LECTURE_SEULE)==0))
      {
			DWORD	dwNbEnr;

      if ((uFileNbEnr(hfile, &dwNbEnr) == NO_ERROR))
        {
				// oui => lecture des liens l'un apr�s l'autre
				DWORD  dwNEnr;
        bOk = TRUE;

        for (dwNEnr=0; dwNEnr < dwNbEnr; dwNEnr++)
          {
					PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;
					t_sav_dde_e_dyn SauveLienE;
					char            pszTxt[256];
					UINT           wSizepszTxt;

          uFileLireTout (hfile,&SauveLienE, 1); //$$ Err ?
          pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, SauveLienE.wPosItem);

          if ((!pVarClientDyn->bLienOk) && (pVarClientDyn->bLienDetruit))
            {
            if ((uDDEOuvreConversationClient (SauveLienE.pszApplic, SauveLienE.pszTopic, 1, TerminateLienEDynRecu,
							(LPARAM)(SauveLienE.wPosItem), &pVarClientDyn->hconvClient)) == 0)
              {
              pVarClientDyn->bLienOk = TRUE;
              pVarClientDyn->bLienDetruit = FALSE;

              if (uDdeClientRequestItem (SauveLienE.pszItem, 1, NULL, 0, pVarClientDyn->hconvClient, TIMEOUTDDE) == 0)
                {
                if (uDdeClientGetItemValeur (pVarClientDyn->hconvClient,1, &pszTxt, &wSizepszTxt) == 0)
                  {
                  pVarClientDyn->bDataRecue = FALSE;

                  if (uDdeClientAdviseItem (SauveLienE.pszItem, 1, (void *)pszTxt, wSizepszTxt, 
										DataEDynRecue, (LPARAM)(SauveLienE.wPosItem), pVarClientDyn->hconvClient, TIMEOUTDDE) == 0)
                    {
                    pVarClientDyn->bDataRecue = TRUE;
                    bOk = TRUE;
                    }
                  }
                }
              }
            } // lien pas ok
          }
        }
      uFileFerme (&hfile);
      } // if ((uFileOuvre  (&hfile, 
    } // if (existe_repere(bx_var_dde_client_dyn))
  return bOk;
  }


// -------------------------------------------------------
// Boite de dialogue �dition lien dde
static BOOL CALLBACK dlgprocEditionLiensDde (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL							mres = FALSE;		   // Valeur de retour
  PDLG_LIEN_DDE_EX pEnvLiens;

  switch (msg)
    {
    case WM_INITDIALOG:
      pEnvLiens = (PDLG_LIEN_DDE_EX)pSetEnvOnInitDialogParam (hwnd, mp2);
		  ListBoxEditionLiensDde (hwnd, pEnvLiens); // $$ v�rifier
      mres = TRUE;
      break;

    case WM_CLOSE:
      EndDialog(hwnd, TRUE);
      break;

    case WM_COMMAND:
      pEnvLiens = (PDLG_LIEN_DDE_EX)pGetEnv(hwnd);
      switch (LOWORD(mp1))
        {
				case ID_DDE_SUP:
					{
					DWORD	item = SendDlgItemMessage (hwnd, ID_DDE_LIST, LB_GETCURSEL, 0, 0);

					if (SendDlgItemMessage (hwnd, ID_DDE_LIST, LB_GETSEL, (WPARAM)item, 0) == 0)
						break;
          item++;
          bSupprimeLienDde (item);
          ListBoxEditionLiensDde(hwnd,pEnvLiens);
					}
          break;

				case ID_DDE_RETABLIR:
          bRetablisLienDde ();
          ListBoxEditionLiensDde(hwnd,pEnvLiens);
          break;

				case IDCANCEL:
          EndDialog(hwnd, TRUE);
          break;
        }
      break;
    }
  return mres;
  } // dlgprocEditionLiensDde

//-------------------------------------------------------------------------------------------------
// Alloue *pBuf et initialise avec les infos de lien sur le presse papier et met � jour *ppszApplic etc...
// ATTENTION : Apr�s usage lib�rer *ppBuf
static BOOL bLisLienDDEPressePapier (PSTR	* ppBuf, PSTR * ppszApplic, PSTR * ppszTopic, PSTR * ppszItem)
	{
	BOOL	bRes = FALSE;
	UINT uLinkFormat = uFormatLienDDEPressePapier();

	if (uLinkFormat)
		{
		// oui => copie des infos de lien � partir du presse papier Ok ?
		DWORD dwTailleBuf;

		if (bClipBLitFormat (uLinkFormat, (PVOID *)ppBuf, &dwTailleBuf))
			{
			// oui => analyse du format "APPLI\0TOPIC\0ITEM\0\0" des param�tres de la conversation
			*ppszApplic = *ppBuf;
			*ppszTopic = (*ppBuf) + 1 + StrLength (*ppszApplic);
			*ppszItem = (*ppszTopic) + 1 + StrLength (*ppszTopic);
			bRes = TRUE;
			}
		}
	return bRes;
	}

//------------------------------------------------------------
// Rafraichis les infos sur le Lien Du PressePapier � coller
static void RafraichisLienDuPressePapier (HWND hwnd)
	{
	PSTR	pBuf;
	PSTR	pszApplic;
	PSTR	pszTopic;
	PSTR	pszItem;

	if (bLisLienDDEPressePapier (&pBuf, &pszApplic, &pszTopic, &pszItem))
		{
		// copie les infos de lien correspondant
		SetDlgItemText (hwnd, IDS_APPLIC, pszApplic);
		SetDlgItemText (hwnd, IDS_TOPIC, pszTopic);
		SetDlgItemText (hwnd, IDS_ITEM, pszItem);
		MemLibere((PVOID *)(&pBuf));
		::EnableWindow (::GetDlgItem (hwnd, IDOK), TRUE);
		}
	else
		{
		// Inhibe le bouton coller
		::EnableWindow (::GetDlgItem (hwnd, IDOK), FALSE);
		SetDlgItemText (hwnd, IDS_APPLIC, "");
		SetDlgItemText (hwnd, IDS_TOPIC, "");
		SetDlgItemText (hwnd, IDS_ITEM, "");
		}
	} // RafraichisLienDuPressePapier


// ----------------------------------------------------
//Boite de dialogue variables dde client
static BOOL CALLBACK dlgprocCollerLienClientDDE (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
  {
  BOOL	mres = FALSE;		   // Valeur de retour

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			char    szNomVar [NB_MAX_CAR];
			DWORD w = 1;
			DWORD item;
			szNomVar [0] = '\0';

			while (bNomVarDdeDyn (w, szNomVar, FALSE))
				{
				item = LOWORD (SendDlgItemMessage (hwnd, ID_DDE_LIST, LB_INSERTSTRING,
					(WPARAM)-1, (LPARAM) szNomVar));
				w ++;
				}

			// Mise � jour de l'affichage des donn�es du lien sur le presse papier
			RafraichisLienDuPressePapier (hwnd);
			}
      mres = TRUE;
      break;

    case WM_DESTROY:
			hwndCollerLiaisonDDE = NULL;
      break;

    case WM_CLOSE:
      ::DestroyWindow(hwnd);
      break;

		case WM_ACTIVATE:
			// Mise � jour de l'affichage des donn�es du lien sur le presse papier
			RafraichisLienDuPressePapier (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(wParam))
        {
				case IDOK:
					{
					// oui => un item est s�lectionn� ?
					DWORD	item = (DWORD)  SendDlgItemMessage (hwnd, ID_DDE_LIST, LB_GETCURSEL, 0, 0);

					if (item != LB_ERR)
						{
						// oui => faire le lien avec la variable :
						PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;
						DWORD					wNumItem;

						// d�s�lectionner l'item
						SendDlgItemMessage (hwnd, ID_DDE_LIST, LB_SETCURSEL, -1 , 0);
						wNumItem = item + 1;

						// recuperer le numero de variable ds la liste client
						pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, wNumItem);
						
						// D�ja 1 lien ?
						if ((pVarClientDyn->bLienOk) && (!pVarClientDyn->bLienDetruit))
							{
							// oui => il faut le d�truire
							uDDEFermeConversationClient (&pVarClientDyn->hconvClient);
							pVarClientDyn->bLienOk = FALSE;
							pVarClientDyn->bLienDetruit = TRUE;
							}

						// Cr�ation du nouveau lien sur la variable
							{
							char	pszApplic[DDE_MAX_NAME_SIZE];
							char	pszTopic[DDE_MAX_NAME_SIZE];
							char	pszItem[DDE_MAX_ITEM_SIZE];
							BOOL	bRes = FALSE;

							// r�cup�ration des param�tres du lien
							GetDlgItemText (hwnd, IDS_APPLIC, pszApplic, DDE_MAX_NAME_SIZE);
							GetDlgItemText (hwnd, IDS_TOPIC, pszTopic, DDE_MAX_NAME_SIZE);
							GetDlgItemText (hwnd, IDS_ITEM, pszItem, DDE_MAX_ITEM_SIZE);

							// cr�ation du lien Ok ?
							if (uDDEOuvreConversationClient (pszApplic, pszTopic, 1, TerminateLienEDynRecu, (LPARAM)wNumItem, &pVarClientDyn->hconvClient) == 0)
								{
								// Lance un AdviseItem
								char    pszTxt[256]; //$$ Const ?
								LPARAM	lParam = 0;// Normalement NumItem

								if (uDdeClientAdviseItem (pszItem, 1, (void *)pszTxt, 256, //$$ V�rifier 256 et DataEDynRecue
									DataEDynRecue, lParam, pVarClientDyn->hconvClient, TIMEOUTDDE) == 0)
									{
									// Lien et rafraichissement de la variable Ok
									pVarClientDyn->bLienOk = TRUE;
									pVarClientDyn->bLienDetruit = FALSE;
									pVarClientDyn->bDataRecue = TRUE; // mettre ds BD ds la phase des entrees
									bRes = TRUE;
									}
								else
									uDDEFermeConversationClient (&pVarClientDyn->hconvClient);
								}
							}
						} // if (item != LB_ERR)
					}
					break;

				case IDCANCEL:
          ::DestroyWindow(hwnd);
          break;
        }
      break;

    }
  return mres;
  } // dlgprocCollerLienClientDDE


// --------------------------------------------------------------------------
// Traitement de la commande menu Connexion Edition lien DDE
void EditionLienDDE (HWND hwnd)
	{
	DLG_LIEN_DDE_EX retour_dde_lien = {FALSE,0};
	
  LangueDialogBoxParam (MAKEINTRESOURCE(ID_DLG_DDE_LIEN_EX), hwnd, 
		dlgprocEditionLiensDde, (LPARAM)&retour_dde_lien);
	}

// --------------------------------------------------------------------------
// Commande menu Edition copier lien DDE : ouverture de la boite de dialogue modeless si n�cessaire
void EditionCopierLienServeurDDE (HWND hwnd)
	{
	// Ouvre la boite Modeless CopierLienServeurDDE si n�cessaire
  if (hwndCopierLienDDE == NULL)
    hwndCopierLienDDE = LangueCreateDialogParam (MAKEINTRESOURCE (ID_DLG_DDE_SRV_EX), hwnd, dlgprocCopierLienServeurDDE, 0);
	}

// --------------------------------------------------------------------------
// Commande menu Edition Coller avec liaison DDE : ouverture de la boite de dialogue modeless si n�cessaire
void EditionCollerLienClientDDE (HWND hwnd)
	{
	// Ouvre la boite Modeless CollerLienClientDDE si n�cessaire
	if (hwndCollerLiaisonDDE == NULL)
		{
		ENV_SELECTION_VARIABLE retour_dde_choisi = {FALSE,0};
		
		hwndCollerLiaisonDDE = LangueCreateDialogParam (MAKEINTRESOURCE (ID_DLG_DDE_CLI_EX), hwnd,
			dlgprocCollerLienClientDDE, (LPARAM)(&retour_dde_choisi));
		}
	}

// --------------------------------------------------------------------------
// Enregistre les liaisons DDE en cours dans un fichier PathAppli.DDE
void SauveLienDde (PCSTR pszNom)
  {
	char	pszFile[MAX_PATH];

	// Cr�e le nom de fichier
  StrCopy (pszFile,pszNom);
	pszNomAjouteExt (pszFile, MAX_PATH,"DDE");

	// Liens existent ?
  if (existe_repere(bx_var_dde_client_dyn))
    {
		DWORD           erreur = 0;
		DWORD           wNumItem;
		PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;
		t_sav_dde_e_dyn SauveLienE;
		char            pszApplic[MAX_PATH];
		char            pszTopic[MAX_PATH];
		char            pszItem[MAX_PATH];
		HFIC						hfile;

		// oui => Cr�ation fichier Ok ?
    if (uFileOuvre  (&hfile, pszFile,sizeof(SauveLienE), CFman::OF_CREE)==0)
      {
			DWORD	dwNbEcrits = 0;

			// Lien par lien
      for (wNumItem=1; wNumItem <= nb_enregistrements (szVERIFSource, __LINE__, bx_var_dde_client_dyn); wNumItem++)
        {
				// Lien en cours ?
        pVarClientDyn= (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, wNumItem);
        if ((pVarClientDyn->bLienOk) && (!pVarClientDyn->bLienDetruit))
          {
					// oui => r�cup�re param�tres du lien
          erreur = DdeGetLinkNames (pVarClientDyn->hconvClient,1, pszApplic, pszTopic, pszItem);
          if (erreur == 0)
            {
						// Enregistre les
						dwNbEcrits++;
            StrCopy (SauveLienE.pszApplic,pszApplic);
            StrCopy (SauveLienE.pszTopic,pszTopic);
            StrCopy (SauveLienE.pszItem,pszItem);
            SauveLienE.wPosItem = wNumItem;
            uFileEcrireTout (hfile, &SauveLienE, 1);
            }
          }
        }
      uFileFerme  (&hfile);
			// rien �crit ?
      if (dwNbEcrits == 0)
				// oui => efface le fichier des liens
        uFileEfface (pszFile);
      } // if (uFileOuvre  (&hfile, 
    } // if (existe_repere(bx_var_dde_client_dyn))
  else
		// pas de lien => efface le fichier des liens
    uFileEfface (pszFile);
  } // SauveLienDde

// --------------------------------------------------------------------------
// Demande
static BOOL bDemandeConfirmDde (char *pszMes)
  {
  return dlg_confirmation (Appli.hwnd, pszMes) == IDYES;
  }

// --------------------------------------------------------------------------
// Etablis les liens clients DDE enregistr�s dans le fichier PathAppli".DDE"
BOOL ChargeLiensDde (void)
  {
  BOOL	bOk = TRUE;
  char	pszFile[MAX_PATH];

	// reset du syst�me DDE
  //$$ termine_de();
  //$$ initialise_de();

	// Fichier de liens DDE existe ?
  StrCopy (pszFile, pszPathNameDoc());
	pszNomAjouteExt (pszFile, MAX_PATH,"DDE");

	if (bFileExiste(pszFile))
    {
		// oui => Confirmation �ventuelle de son utilisation ?
		BOOL aCharger = TRUE;

    if (ExisteMenu())
      {
			char pszMess[80];

      bLngLireInformation (c_inf_confir_dde, pszMess);
      if (bDemandeConfirmDde(pszMess))
        aCharger = FALSE;
      }

    if (aCharger)
      {
			// oui => ouverture du fichier de liens DDE
			HFIC	hfile;
			DWORD	dwNbEnr;

      bOk = FALSE;
      if ((uFileOuvre  (&hfile, pszFile, sizeof(t_sav_dde_e_dyn), CFman::OF_OUVRE_LECTURE_SEULE)==0) && 
				(uFileNbEnr(hfile, &dwNbEnr) == 0))
        {
				DWORD wNEnr;

        bOk = TRUE;

				// Lecture de chaque Enr
        for (wNEnr = 1; wNEnr <= dwNbEnr; wNEnr++)
          {
					PX_VAR_DDE_CLIENT_DYN	pVarClientDyn;
					t_sav_dde_e_dyn SauveLienE;
					char            pszTxt[256]; //$$ Const ?
					UINT            wSizepszTxt;

          uFileLireTout (hfile, &SauveLienE, 1);
					pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, SauveLienE.wPosItem);

					// ouverture du lien avec un autre serveur ?
          if ((uDDEOuvreConversationClient (SauveLienE.pszApplic, SauveLienE.pszTopic, 1,
						TerminateLienEDynRecu, (LPARAM)(SauveLienE.wPosItem), &pVarClientDyn->hconvClient)) == 0)
            {
						// oui => initialise le lien 
            pVarClientDyn->bLienOk = bDDEOuvert();
            pVarClientDyn->bLienDetruit = FALSE;

            if (pVarClientDyn->bLienOk && uDdeClientRequestItem (SauveLienE.pszItem, 1, NULL, 0, pVarClientDyn->hconvClient, TIMEOUTDDE) == 0)
              {
              if (uDdeClientGetItemValeur (pVarClientDyn->hconvClient,1, &pszTxt, &wSizepszTxt) == 0)
                {
                pVarClientDyn->bDataRecue = FALSE;

                if (uDdeClientAdviseItem (SauveLienE.pszItem, 1, (void *)pszTxt, wSizepszTxt,
                  DataEDynRecue, (LPARAM)(SauveLienE.wPosItem), pVarClientDyn->hconvClient, TIMEOUTDDE) == 0)
                  {
                  pVarClientDyn->bDataRecue = TRUE;
                  bOk = TRUE; // $$ �crasement de la valeur de bOk si erreur d�tect�e
                  }
                else
                  {
                  bOk = FALSE;
                  }
                }
              }
            }
           else
            {
            bOk = FALSE;
            }
          } //for
        uFileFerme  (&hfile);
        }
      else
        {
        bOk = FALSE;
        }
      if (!bOk)
        {
        if (ExisteMenu())
          {
					char pszMess[80];

          bLngLireInformation (c_inf_confir_dde1,pszMess);
          if (bDemandeConfirmDde(pszMess))
            bOk=TRUE;
          }
        }
      } // if (aCharger)
    } // if (bFileExiste(pszFile))
  return bOk;
  } // ChargeLiensDde




/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Initialisations par lecture du fichier texte PCS.INI	|
 |   Auteur  : LM							|
 |   Date    : 26/10/92  						|
 |   Version : 3.30							|
 |   Remarques :                                                        |
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include "std.h"                // Types standard
#include "UStr.h"             // Strxxx ()
#include "PathMan.h"             
#include "DocMan.h"             
#include "driv_dq.h"
#include "ULpt.h"
#include "driv_im.h"         // Constantes sur Imprimantes PRN_xxx
#include "init_txt.h"

// Nom du fichier � interpreter
static const char PSZ_INIT_FILE_NAME [] = "pcs.ini";

#define SIZE_RECORD_FILE       255

// Variables globales pour les tabulations d'affichage des alarmes
LONG nNbAlarmTabsFormat=0; // Nombre de tabulations dans le format
LONG alPosXTab[20]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // Position en X (pixels) de chaque tabulation



// --------------------------------- Variables pour les alarmes
// $$ Infos alarmes devraient d�pendre de l'application utilisateur
#define PSZ_KEY_ALARM           "ALARM"
#define NB_UL_ALARM              2
typedef struct
  {
  DWORD wNbCarMaxMesAl;
  DWORD wNbCarMaxFicArch;
  DWORD wNbCarMaxLineImp;
  } t_def_alarm;

static t_def_alarm td_def_alarm = {40, 80, 80};

// --------------------------------------------------------------------------
//Configuration dans le fichier PCS.INI
//Exemple : ALARM_TABS 400 500 780
//Cet exemple d�clare 3 "Tabs" d�marrant en 400,500 et 780 pixels, ce qui d�finit 4 colonnes d�marrant en 0,400,500 et 780 pixels.
//Chaque messages d'alarmes contenant des caract�res '|'(pipe) sera ainsi r�parti colonne par colonne, chaque occurence de | d�signant la s�paration.
//Remarques :
//Les d�passement de colonnes sont tronqu�s. 
//Au plus 20 Tabs peuvent �tre ainsi d�finis. Au dela, ils sont ignor�s. 
//Ces formats sont utilis�s dans les fen�tres d�alarmes dynamiques et archiv�es. 
//Si le message contient plus de Tabs que configur�s, alors les Tabs surnum�raires sont affich�s tels quel dans la derni�re colonne d�finie, sous forme d�un caract�re ressemblant � �|�. 
//
// Renvoie True si la ligne commence par "ALARM_TABS". Met � jour les param�tres.
// --------------------------------------------------------------------------
static BOOL bInterpreteLigneAlarmTabs (DWORD dwNbUl, char tab_ul [] [SIZE_UL])
  {
  BOOL bRes = FALSE;
  LONG lNbTabs = 0;

	// Ligne de description des tabulationns d'alarme ?
  if (bStrEgales (tab_ul [0], "ALARM_TABS"))
    {
		// Oui => Bon type de ligne
    bRes = TRUE;
		// Limite le nombre max de tabs
		if (dwNbUl>NB_MAX_ALARM_TABS+1)
			{
			dwNbUl = NB_MAX_ALARM_TABS+1;
			}
		// Traite valeur de Tab par valeur de Tab
		DWORD  dwTailleTab = 0;
		for (DWORD dwNUl=1; dwNUl<dwNbUl; dwNUl++)
			{
			// R�cup�re la valeur du Tab (garde la m�me si format invalide)
      StrToDWORD (&dwTailleTab, tab_ul [dwNUl]);
			alPosXTab[dwNUl-1]=dwTailleTab;
			// Un tab de plus
			lNbTabs += 1;
			}
		// Enregistre le nouveau nombre de Tabs
		nNbAlarmTabsFormat=lNbTabs;
    }

  return bRes;
  }

// --------------------------------------------------------------------------
// initialiser le contexte des alarmes
// --------------------------------------------------------------------------
static BOOL interprete_ligne_alarme (DWORD wNbUl, char tab_ul [] [SIZE_UL])
	{
	BOOL bIsAlarm;
	DWORD    wValue;

	if (bStrEgales (tab_ul [0], PSZ_KEY_ALARM))
		{
		bIsAlarm = TRUE;
		//
		if (wNbUl >= NB_UL_ALARM)
			{
			//--------------- ATTENTION: switch sans BREAK.
			//                Si wNbUl vaut Xx, alors tout le code C
			//                du switch est execut� � partir du case Xx:
			switch (wNbUl)
				{
				case 4:
					if (StrToDWORD (&wValue, tab_ul [3]))
						{
						if ((wValue <= 130) && (wValue >= 80))
							{
							td_def_alarm.wNbCarMaxLineImp = wValue;
							}
						}
				case 3:
					if (StrToDWORD (&wValue, tab_ul [2]))
						{
						if ((wValue <= 253) && (wValue >= 80))
							{
							td_def_alarm.wNbCarMaxFicArch = wValue;
							}
						}

				case 2:
					if (StrToDWORD (&wValue, tab_ul [1]))
						{
						if ((wValue <= 79) && (wValue >= 40))
							{
							td_def_alarm.wNbCarMaxMesAl = wValue;
							}
						}

				default:
					break;
				} //------------ Switch Nombre d'UL !!!
			} //---------------- Le Nombre de champs est valide
		}
	else //----------------- La ligne n'est pas la description d'une imprimante
		{
		bIsAlarm = FALSE;
		}

	return (bIsAlarm);
	}


// --------------------------------------------------------------------------
// Initialiser un contexte en fonction d'une ligne de commande
// La ligne de commande est mise au format: MOT_1 MOT_2 MOT_3....
// --------------------------------------------------------------------------
static void interprete_ligne (char *pszLine)
  {
  DWORD wNbUl;
  char tab_ul [MAX_UL] [SIZE_UL];

  StrUpperCase (pszLine);
  //
  wNbUl = StrToStrArray (tab_ul, MAX_UL, pszLine);
  if (wNbUl > 0)
    {
    if (!bImprimanteInterpreteLigneIni (wNbUl, tab_ul))
      {
			if (!bInterpreteLigneAlarmTabs (wNbUl, tab_ul))
				{
				interprete_ligne_alarme (wNbUl, tab_ul);
				}
      }
    }
  }

// --------------------------------------------------------------------------
// Charger les infos de configurations du fichier  PCS.INI
void InterpreteFichierIni (void)
  {
  HFDQ whFile;
  char pszLine [SIZE_RECORD_FILE];
	char szIni [MAX_PATH];

  pszCreePathName (szIni, MAX_PATH, pszPathDoc(),  PSZ_INIT_FILE_NAME);
  if (bFicPresent (szIni))
		{
		uFicTexteOuvre (&whFile, szIni);
		//
		while (uFicLireLn (whFile, pszLine, SIZE_RECORD_FILE) == 0)
			{
			interprete_ligne (pszLine);
			}
		//
		uFicFerme (&whFile);
		}
  }

// --------------------------------------------------------------------------
// Retourner les parametres utilises pour les alarmes
// --------------------------------------------------------------------------
BOOL definition_alarme (DWORD *pwNbCarMaxMesAl,
                           DWORD *pwNbCarMaxFicArch,
                           DWORD *pwNbCarMaxLineImp)
  {
  BOOL bOk;

  (*pwNbCarMaxMesAl) = td_def_alarm.wNbCarMaxMesAl;
  (*pwNbCarMaxFicArch) = td_def_alarm.wNbCarMaxFicArch;
  (*pwNbCarMaxLineImp) = td_def_alarm.wNbCarMaxLineImp;
  bOk = TRUE;
  return (bOk);
  }


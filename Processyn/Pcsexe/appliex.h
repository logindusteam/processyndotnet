// --------------------------------------------------------------------------
// AppliEx.h
// Gestion de l'application utilisateur pour pcsexe
// Win32 22/7/97 JS
// --------------------------------------------------------------------------
BOOL ChargeApplication (char *pszNom,BOOL aChaud);
void SauveApplication (char *pszNom,BOOL aChaud);
BOOL DecodeApplicSuivante (char *pszApplicationSuivante, BOOL *bAChaud);
void    InitRessourcesAppli(void);

// --------------------------------------------------------------------------

/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de																				    |
 |		    Societe LOGIQUE INDUSTRIE																					|
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3											|
 | Il est et demeure sa propriete exclusive et est confidentiel.								|
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/
// DllMuxEx.h (Dynamic Link Librarie Multiplexeur en Exploitation)
// Acc�s aux services Processyn en exploitation (utilis�s par les DLLs utilisateurs)

//-------------------------------------------------------
// Structure d'un message pass� au multiplexeur
typedef struct
	{
	DWORD	dwTaille;			// taille globale du message (en octets)
	DWORD	uIdService;		// ID_MX_SERVICE
	DWORD	uIdFonction;	// ID_FONCTION_MXxx
	} HEADER_MESSAGE_MUX, *PHEADER_MESSAGE_MUX; // suivi d'�ventuels param�tres

// ------------------------------------------------------
// Type du point d'entr�e multiplexeur
typedef UINT (*FONCTION_MUX_EX) (PHEADER_MESSAGE_MUX pMes);

//-------------------------------------------------------
// Liste des services support�s
typedef enum
	{
	ID_SERVICE_MX_VERSIONS = 'MXVR',	// Service Version (MXVR)
	ID_SERVICE_MX_DB_PCS	 = 'MXDB'		// Service base de donn�e (MXBD)
																		// ...
	} ID_MX_SERVICE;

//-------------------------------------------------------
// Service Version : liste des fonctions support�es
typedef enum
	{
	ID_FONCTION_MXVR_VERSION_MUX = 0			// renvoie la version courante du multiplexeur
	} ID_FONCTION_MXVR;

typedef struct
	{
	HEADER_MESSAGE_MUX	header;
	UINT								uVersionCourante;
	} PARAM_MXVR_VERSION_MUX, *PPARAM_MXVR_VERSION_MUX;

// Version de multiplexeur courante (concerne le m�canisme de passage des param�tres - pas les services)
#define VERSION_MUX_COURANTE 100

//-------------------------------------------------------
// Service Base de donn�es ID_SERVICE_MX_DB_PCS : liste des fonctions support�es
typedef enum
	{
	ID_FONCTION_MXDB_COPIE_VAR_LOG = 0,			// lit/�crit la valeur d'une var logique
	ID_FONCTION_MXDB_COPIE_VAR_NUM = 1,			// lit/�crit la valeur d'une var num�rique
	ID_FONCTION_MXDB_COPIE_VAR_MES = 2,			// lit/�crit la valeur d'une var message
	ID_FONCTION_MXDB_COPIE_VAR_SYS_LOG = 3,	// lit/�crit la valeur d'une var syst�me logique
	ID_FONCTION_MXDB_COPIE_VAR_SYS_NUM = 4,	// lit/�crit la valeur d'une var syst�me num�rique
	ID_FONCTION_MXDB_COPIE_VAR_SYS_MES = 5,	// lit/�crit la valeur d'une var syst�me message
	ID_FONCTION_MXDB_INFO_VAR = 6						// lit des infos d'une var
	} ID_FONCTION_MXDB;

typedef struct // ID_FONCTION_MXDB_COPIE_VAR_LOG
	{
	HEADER_MESSAGE_MUX	header;
	BOOL	bAffecte;										// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	DWORD	dwIndex;										// 0 ou index de tableau (1..n)
	char	szNomVar[c_nb_car_es];			// nom de la variable
	BOOL	bValeur;										// valeur transf�r�e
	} PARAM_MXDB_COPIE_VAR_LOG, *PPARAM_MXDB_COPIE_VAR_LOG;

typedef struct // ID_FONCTION_MXDB_COPIE_VAR_NUM
	{	
	HEADER_MESSAGE_MUX	header;
	BOOL	bAffecte;									// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	DWORD	dwIndex;										// 0 ou index de tableau (1..n)
	char	szNomVar[c_nb_car_es];			// nom de la variable
	FLOAT	fValeur;										// valeur transf�r�e
	} PARAM_MXDB_COPIE_VAR_NUM, *PPARAM_MXDB_COPIE_VAR_NUM;

typedef struct // ID_FONCTION_MXDB_COPIE_VAR_MES
	{
	HEADER_MESSAGE_MUX	header;
	BOOL	bAffecte;										// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	DWORD	dwIndex;										// 0 ou index de tableau (1..n)
	char	szNomVar[c_nb_car_es];			// nom de la variable
	char	szValeur[c_nb_car_ex_mes];	// valeur transf�r�e
	} PARAM_MXDB_COPIE_VAR_MES, *PPARAM_MXDB_COPIE_VAR_MES;

typedef struct // ID_FONCTION_MXDB_COPIE_VAR_SYS_LOG
	{
	HEADER_MESSAGE_MUX	header;
	BOOL	bAffecte;										// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	DWORD	dwIndex;										// 0 ou index de tableau (1..n)
	BOOL	bValeur;										// valeur transf�r�e
	DWORD	dwIdVarSys;									// id de la variable syst�me
	} PARAM_MXDB_COPIE_VAR_SYS_LOG, *PPARAM_MXDB_COPIE_VAR_SYS_LOG;

typedef struct // ID_FONCTION_MXDB_COPIE_VAR_SYS_NUM
	{	
	HEADER_MESSAGE_MUX	header;
	BOOL	bAffecte;									// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	DWORD	dwIndex;										// 0 ou index de tableau (1..n)
	FLOAT	fValeur;										// valeur transf�r�e
	DWORD	dwIdVarSys;									// id de la variable syst�me
	} PARAM_MXDB_COPIE_VAR_SYS_NUM, *PPARAM_MXDB_COPIE_VAR_SYS_NUM;

typedef struct // ID_FONCTION_MXDB_COPIE_VAR_SYS_MES
	{
	HEADER_MESSAGE_MUX	header;
	BOOL	bAffecte;										// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	DWORD	dwIndex;										// 0 ou index de tableau (1..n)
	char	szValeur[c_nb_car_ex_mes];	// valeur transf�r�e
	DWORD	dwIdVarSys;									// id de la variable syst�me
	} PARAM_MXDB_COPIE_VAR_SYS_MES, *PPARAM_MXDB_COPIE_VAR_SYS_MES;

typedef struct // ID_FONCTION_MXDB_INFO_VAR
	{
	HEADER_MESSAGE_MUX	header;
	char	szNomVar[c_nb_car_es];			// nom de la variable
	DWORD	dwGenre;										// c_res_message, c_res_numerique ou c_res_logique
	DWORD	dwIndexMax;									// 0 = var simple ou index max de tableau (1..n)
	} PARAM_MXDB_INFO_VAR, *PPARAM_MXDB_INFO_VAR;

// Erreurs renvoy�es par le multiplexeur
typedef enum 
	{
	MUX_ERR_OK = 0,
	MUX_ERR_HANDLE_INVALIDE = 0,
	MUX_ERR_PARAMETRES_INVALIDES = 1,
	MUX_ERR_ID_SERVICE = 2,
	MUX_ERR_ID_FONCTION = 3,
	MUX_ERR_TAILLE_PARAMETRES = 4,
	MUX_ERR_RESULTAT_FONCTION = 5		// Erreur renvoy�e par la fonction
	} MUX_ERR;

// --------------------------------------------------------------------------
// Renvoie un handle sur le point d'entr�e des services Processyn en exploitation
HANDLE hMuxExProcessyn (void);

// --------------------------------------------------------------------------
// Appel du multiplexeur : renvoie TRUE si l'appel du multiplexeur s'est bien pass�
__inline BOOL bAppelMuxOk 
	(
	HANDLE hProcessyn,					// doit �tre NULL ou l'adresse de la fonction Mux
	PHEADER_MESSAGE_MUX pMes		// Adresse du header �ventuellement suivi de ses param�tres
	)
	{
	// Renvoie TRUE si le handle du multiplexeur est non null et que le Mux renvoie MUX_ERR_OK
	return (hProcessyn != NULL) && (((FONCTION_MUX_EX)hProcessyn)(pMes) == MUX_ERR_OK);
	}


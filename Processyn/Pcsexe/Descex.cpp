/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de                                           |
 |                                                                          |
 |                  Societe LOGIQUE INDUSTRIE                               |
 |           Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |                                                                          |
 | Il est demeure sa propriete exclusive et est confidentiel.               |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |                                                                          |
 |   Titre   : DescEx.C  Gestion des Descripteurs pr�sents en Execution PCS |
 |   Auteur  : Jean                                                         |
 |   Date    : 15/03/93                                                     |
 |                                                                          |
 |  Remarques: Fichier s�lectif;                                            |
 |             Permet de r�cuperer les fonctions d'execution de chaque      |
 |             descripteur pr�sent dans la collection en execution.         |
 |                                                                          |
 +--------------------------------------------------------------------------*/

#include "stdafx.h"
#include "col.h"           // Compilation selective
#include "std.h"           // Definition des types standard
#include "lng_res.h"
#include "tipe.h"          // Constantes d_xxx
#include "Descripteur.h"
#include "mem.h"           // Gestion memoire
#include "USem.h"
#include "Threads.h"
#include "ITC.h"

#include "UChrono.h"            // pour tipe_al    
#include "scrut_sy.h"          // Systeme
#include "scrut_co.h"          // Code
#include "scrut_dq.h"          // Code
#include "scrut_tp.h"          // Temps
#include "scrut_de.h"          // Dde
#include "scrut_gr.h"	       // GR
#include "tipe_al.h"	       // AL
#include "srvanmal.h"	       // AL
#include "scrut_al.h"	       // AL

#ifdef c_modbus
  #include "scrut_jb.h"        // JBus
#endif

#ifdef c_ap
  #include "scrut_ap.h"        // Applicom Bd
#endif

#ifdef c_opc
  #include "scrut_opc.h"        // Applicom Bd
#endif

#include "DescEx.h"
#include "Verif.h"
VerifInit;


//----------------------------------------------------------------------------
// Renvoie TRUE et met �jour les infos d'ex�cution du descripteur demand� s'il existe,
// FALSE sinon
BOOL bGetDescripteurEx (DWORD num_descripteur, PDESCRIPTEUR_EX pDescripteurEx)
	{
	BOOL bOk = TRUE;

	// Par d�faut pas de points d'entr�e
	pDescripteurEx->fnInit = NULL;
	pDescripteurEx->fnTermine = NULL;
	pDescripteurEx->fnEntrees = NULL;
	pDescripteurEx->fnTraite = NULL;
	pDescripteurEx->fnSorties = NULL;
	pDescripteurEx->bEntreesDeclarees = FALSE;
	pDescripteurEx->bSortiesVersPeripheriques = FALSE;
	pDescripteurEx->bSortiesDeclarees = FALSE;

	// Met � jour l'existence d'entr�es d�clar�es
  //DWORD dwNPeriph = 1;
  DWORD wNbPeriph = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_periph_sortie);
	for (DWORD dwNPeriph = 1; dwNPeriph <= wNbPeriph; dwNPeriph++)
		{
		PDWORD pwDriver = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_periph_sortie, dwNPeriph);
    if ((*pwDriver) == num_descripteur)
			{
			pDescripteurEx->bSortiesDeclarees = TRUE;
			break;
			}
		}

	// Met � jour l'existence de sorties d�clar�es
  wNbPeriph = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_periph_entree);
	for (DWORD dwNPeriph = 1; dwNPeriph <= wNbPeriph; dwNPeriph++)
		{
		PDWORD pwDriver = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_periph_entree, dwNPeriph);
    if ((*pwDriver) == num_descripteur)
			{
			pDescripteurEx->bEntreesDeclarees = TRUE;
			break;
			}
		}

	// Met � jour les donn�es selon le descripteur
	switch (num_descripteur)
    {
    case d_systeme:
			{
      pDescripteurEx->fnInit = init_sy;
			pDescripteurEx->fnTermine = fini_sy;
			pDescripteurEx->fnEntrees = recoit_sy;
			// Traite = FALSE;
			pDescripteurEx->fnSorties = emet_sy;
			}
      break;

    case d_code:
			{
      pDescripteurEx->fnInit = init_co;
			pDescripteurEx->fnTermine = termine_co;
			// Entrees = FALSE;
			pDescripteurEx->fnTraite = traitement_co;
			// Sortie = FALSE;
			}
      break;

    case d_mem:
			{
			// Init = FALSE;
			// Fin = FALSE;
			// Entrees = FALSE;
			// Traite = FALSE;
			// Sortie = FALSE;
			}
			break;

    case d_chrono:
			{
			// Init = FALSE;
			// Fin = FALSE;
			pDescripteurEx->fnEntrees = scrute_cn;
			// Traite = FALSE;
			// Sortie = FALSE;
			}
			break;

    case d_disq:
			{
			pDescripteurEx->fnInit = initialise_dq;
			pDescripteurEx->fnTermine = ferme_disc;
			// Entrees = FALSE;
			// Traite = FALSE;
			// Sortie = FALSE;
			}
			break;

    case d_repete:
			{
			// init = FALSE;
			// fin = FALSE;
			// Entrees = FALSE;
			// Traite = FALSE;
			// Sortie = FALSE;
			}
			break;

    case d_dde:
			{
      pDescripteurEx->fnInit = initialise_de;
			pDescripteurEx->fnTermine = termine_de;
			pDescripteurEx->fnEntrees = recoit_de;
			// Traite = FALSE;
			pDescripteurEx->fnSorties = emet_de;
			pDescripteurEx->bSortiesVersPeripheriques = TRUE;
			}
      break;

    case d_graf:
			{
			pDescripteurEx->fnInit = initialise_gr;
			pDescripteurEx->fnTermine = termine_gr;
			pDescripteurEx->fnEntrees = recoit_gr;
			// Traite = FALSE;
			pDescripteurEx->fnSorties = sorties_gr;
			}
			break;

    case d_al:
			{
			pDescripteurEx->fnInit = initialise_al;
			pDescripteurEx->fnTermine = termine_al;
			pDescripteurEx->fnEntrees = recoit_al;
			// Traite = FALSE;
			pDescripteurEx->fnSorties = sorties_al;
			}
			break;

#ifdef c_modbus
    case d_jbus:
			{
			pDescripteurEx->fnInit = initialise_jbus;
			// fin = FALSE;
			pDescripteurEx->fnEntrees = recoit_jb;
			// Traite = FALSE;
			pDescripteurEx->fnSorties = emet_jb;
			pDescripteurEx->bSortiesVersPeripheriques = TRUE;
			}
			break;
#endif

#ifdef c_ap
    case d_ap:
			{
			pDescripteurEx->fnInit = initialise_ap;
			pDescripteurEx->fnTermine = ferme_ap;
			pDescripteurEx->fnEntrees = recoit_ap;
			// Traite = FALSE;
			pDescripteurEx->fnSorties = emet_ap;
			pDescripteurEx->bSortiesVersPeripheriques = TRUE;
			}
			break;
#endif

#ifdef c_opc
    case d_opc:
			{
			pDescripteurEx->fnInit = initialise_opc;
			pDescripteurEx->fnTermine = ferme_opc;
			pDescripteurEx->fnEntrees = recoit_opc;
			// Traite = FALSE;
			pDescripteurEx->fnSorties = emet_opc;
			pDescripteurEx->bSortiesVersPeripheriques = TRUE;
			}
			break;
#endif

		default:
			bOk = FALSE;
			break;
    }

	// R�gles de gestion suppl�mentaires si descripteur trouv�: 
	if (bOk)
		{
		// Pas de variables en entr�e => pas d'entr�e
		if (!pDescripteurEx->bEntreesDeclarees)
			pDescripteurEx->fnEntrees = NULL;

		// Pas de variables en sortie => pas de sortie
		if (!pDescripteurEx->bSortiesDeclarees)
			pDescripteurEx->fnSorties = NULL;

		// Descripteur autre que fichier ?
		if (num_descripteur != d_disq)
			{
			// oui => Ni entr�e Ni traitement Ni sortie => Ni init Ni Fermeture : pas de descripteur
			if ((pDescripteurEx->fnEntrees == NULL) && (pDescripteurEx->fnTraite == NULL) && 
				(pDescripteurEx->fnSorties == NULL))
				{
				pDescripteurEx->fnInit = NULL;
				pDescripteurEx->fnTermine = NULL;
				bOk = FALSE;
				}
			}
		}
	return bOk;
	}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------

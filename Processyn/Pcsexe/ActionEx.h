/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il demeure sa propriete exclusive et est confidentiel.             |
 |   Aucune diffusion n'est possible sans accord ecrit.                 |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : ACTIONS - EXE                                            |
 |   Auteur  : JB                                                       |
 |   Date    : 12/02/93                                                 |
 +----------------------------------------------------------------------*/

// -------------- Actions Processyn exe ----------
#define  action_rien                         0
#define  action_initialisation               1
#define  action_finalisation                 2
#define  action_lance_application            3
#define  action_relance_application          4
#define  action_trace_application            5
#define  action_examine_produit              6
#define  action_quitte                       7
#define  action_aide                         8
#define  action_programme_dongle             9
#define  action_charge_applic_froide        10
#define  action_charge_applic_chaude        11
#define  action_initialise_menu             12
#define  action_charge_applic_froide_mnu    13
#define  action_charge_applic_chaude_mnu    14
#define  action_lance_application_mnu       15
#define  action_relance_application_mnu     16
#define  action_sauve_application_mnu       17
#define  action_init_ressources             18
#define  action_sauve_lien_dde              19
#define  action_charge_lien_dde             20
#define  action_copier_lien_dde             21
#define  action_coller_lien_dde             22

// structures associ�es aux actions
typedef struct
  {
  int  entier;
	DWORD dwMot;
  char chaine [MAX_PATH];
  }
  t_param_action;

// Retour Action
typedef struct
  {
  int  entier;
  }
  t_retour_action;

// fonctions associ�e aux actions
void CreerAction (void);

// ajoute une action dans la pile
void ajouter_action  (DWORD action, t_param_action *param);

// ex�cute la plus ancienne des actions dans la pile d'actions et renvoie son identifiant
int executer_action (t_retour_action * pRetourAction);


/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : control ligne saisie texte                               |
 |   Auteur  : AC                                                       |
 |   Date    : 22/02/93 						|
 |   Version :                                                          |
 |                                                                      |
 +----------------------------------------------------------------------*/


/*
Le module permet la cr�ation d'une fen�tre de saisie texte de EDIT
Il g�re la couleur de fond, la couleur du texte, la taille, la police, le style.
Le curseur et le cadre optionnel sont tjrs noirs sauf sur fond noir o� ils
sont blancs.
La fen�tre de saisie g�re un scroll si n�cessaire.
Une fonction masque permet de remplacer les caract�res saisis par des
caract�res '*' � l'affichage.
En saisie on dispose des fonctions ins�re et modifie, des touches fl�ches
droite et gauche, home et end, del et backspace.
La souris permet le positionnement du curseur. La s�lection n'est pas g�r�e.
La fen�tre capture la souris si elle a le focus. Elle peut mettre la fenetre en
video inverse si elle a le focus.
Sur option elle efface la fen�tre de saisie d�s la prise de focus.
Un mode particulier permet la saise de numerique avec filtre.
Sur action ENTER, NEWLINE et ESC un WM_COMMAND est post� au owner avec mp1
contenant l'id et le type d'action et mp2 contenant le hwnd du controle (standard).
Les coordonn�es de la fenetre doivent �tre donn�es en pixels.
*/
// Utiser ::SetWindowText pour assigner un nouveau texte � ce contr�le

// Constantes de notification (envoy�es au parent en param�tre de WM_COMMAND)
#define INLINE_VALIDATION 0x0001
#define INLINE_ABANDON 0x0002

// Cr�ation du contr�le (fermeture par ::DestroyWindow) 
HWND InLineCreate (HWND hwndParent, DWORD id,
                    LONG xFen, LONG yFen, LONG cxFen, LONG cyFen,
                    G_COULEUR backcolor, G_COULEUR textcolor,
                    int nbr_car_max, DWORD police, DWORD style_police, DWORD taille_police,
                    PCSTR texte_depart, BOOL cadre, BOOL masque,
                    BOOL bRazSurFocus, BOOL focus_video_inv);

// Passage en mode �dition de nombre
BOOL bInLineSetModeNum (HWND hdl_inline, FLOAT num_min, FLOAT num_max, LONG NbDecimales, DWORD err_num);

// Assigne les couleurs de ce contr�le
BOOL bInLineSetColor		(HWND hdl_inline, G_COULEUR backcolor, G_COULEUR textcolor);

// Assigne un nouveau nombre � ce contr�le
BOOL bInLineNumSet	(HWND hdl_inline, FLOAT val);


// ------------ fin -----------------------

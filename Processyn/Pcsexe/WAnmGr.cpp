//---------------------------------------------------
// Gestion de l'affichage de l'animation de synoptique de processyn
// Traite les services demand�s par le serveur Scrut_gr de PcsExe
// Win32 5/2/97
//---------------------------------------------------
#include "stdafx.h"
#include "std.h"            // Types Standards
#include "USem.h"
#include "Appli.h"
#include "UStr.h"         // Chaines de Caract�res            Strxxx ()
#include "UEnv.h"
#include "mem.h"            // Memory Manager
#include "MemMan.h"
#include "MenuMan.h"
#include "threads.h"        // Threads                          thrd_xxx ()
#include "LireLng.h"        // Definition extension sysmenu
#include "IdLngLng.h"        //
#include "pcsdlg.h"         //  Saisie facteur zoom
#include "lng_res.h"
#include "tipe.h"           // Types + Constantes Processyn
#include "Descripteur.h"
#include "G_Objets.h"
#include "DevMan.h"
#include "Space.h"          // Espace G�n�ralis�
#include "PcsSpace.h"       // Espace G�n�ralis�
#include "G_Sys.h"          // Dessin en g�n�ralis�             g_xxx ()
#include "BdGr.h"           // Base de donn�e graphique         bdgr_xxx ()
#include "bdanmgr.h"
#include "BdPageGr.h"       // Base de donn�e graphique - propri�t�s de page
#include "BdElemGr.h"       // Base de donn�e graphique - �l�ments
#include "PcsFont.h"        // Fonts PM                         fnt_xxx ()
#include "UChrono.h"
#include "tipe_gr.h"        // Types des Blocs
#include "ITC.h"
#include "CliAnmGr.h"       // Partie client de l'animateur               MbxAnmGrxxx ()
#include "WInLine.h"				// Entrees Messages et Numeriques
#include "IdExeLng.h"
#include "WEntLog.h"
#include "WScrExe.h"				// Touches de Fonctions
#include "WParPop.h"
#include "CheckMan.h"
#include "Verif.h"

#include "WAnmGr.h"
VerifInit;

// Position de la touche fonction F1 dans le bloc des entr�es logiques pour scrut_gr
#define GR_TOUCHES_FCT 1

// Commandes du Menu syst�me
#define CMD_SYSMENU_GROSSIR 1
#define CMD_SYSMENU_MAIGRIR 2
#define CMD_SYSMENU_FACTEUR 3

// Pas de scroll
#define FACTEUR_SCROLL_X 50
#define FACTEUR_SCROLL_Y 50

//
#define INTERVALLE_CLIGNOTTEMENT 500

// Classe pour les fen�tres d'animation de synoptique
static const char szClasseAnmGr [] = "ClasseAnmGr";

// Ids des contr�les enfants d'une page d'animation
#define	PREMIER_ID_VALIDE	100
#define NB_IDS_VALIDES 10000

// Environnement d'une fen�tre d'animation de synoptique
typedef struct
  {
  PDWORD	padwNEnrEntree;
  DWORD		wId;
	HWND		hwndDernierControleActif;
  } ENV_ANM_GR;

typedef ENV_ANM_GR * PENV_ANM_GR;

// Fen�tre parente de toutes les pages de synoptique
static HWND hwndParentPage = NULL;

// Liste des elements clignotants
typedef struct
  {
	DWORD Page;
  DWORD Element;
  G_COULEUR Couleur;
  } X_ANM_ELT_LISTE_ELTS_CLI, *PX_ANM_ELT_LISTE_ELTS_CLI;

// --------------------------------------------------------------------------
// Variables pour les Courbes...
// Contexte Animateur Graphique
static DWORD wCtxtPage;
static HSPACE hSpcFen = NULL;

// --------------------------------------------------------------------------
// Variables pour les services
static char titre_saisie_facteur[c_nb_car_message_inf];
static char mnemo_maigrir [c_nb_car_message_inf];
static char mnemo_grossir [c_nb_car_message_inf];
static char mnemo_facteur [c_nb_car_message_inf];

// --------------------------------------------------------------------------
static void MakeProportional (LONG icxPage, LONG icyPage, LONG icxDessin, LONG icyDessin, LONG *picxDraw, LONG *picyDraw)
  {
  double dPentePage;
  double dPenteBmp;
  double dRapport;

  if ((icxPage > 0L) && (icyPage > 0L))
    {
    dPentePage = ((double) icyPage) / ((double) icxPage);
    if ((icxDessin > 0L) && (icyDessin > 0L))
      {
      dPenteBmp = ((double) icyDessin) / ((double) icxDessin);
      if (dPenteBmp < dPentePage)
        {
        dRapport = ((double) icxPage) / ((double) icxDessin);
        (*picxDraw) = icxPage;
        (*picyDraw) = (LONG) (((double) icyDessin) * dRapport);
        }
      else
        {
        dRapport = ((double) icyPage) / ((double) icyDessin);
        (*picxDraw) = (LONG) (((double) icxDessin) * dRapport);
        (*picyDraw) = icyPage;
        }
      }
    }
  }

// --------------------------------------------------------------------------
static void ajuste_entrees_page (DWORD numero_page)
  {
  DWORD  dwNEntree;
  LONG   x,y,cx,cy;
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, numero_page);

  for (dwNEntree = 0;dwNEntree < pFenAnimGr->nbr_entree; dwNEntree++)
    {
    PX_ANM_E_VI	pt_e_vi = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi,pFenAnimGr->prem_entree+dwNEntree);
    switch (pt_e_vi->n_bloc)
      {
      case bx_anm_e_log_vi :
				{
			  PX_ANM_E_LOG_VI	pt_e_log = (PX_ANM_E_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        if (pt_e_log->hwnd != NULL)
          {
          x =  pt_e_log->x;
          y =  pt_e_log->y;
          cx =  pt_e_log->cx;
          cy =  pt_e_log->cy;
          g_convert_point_page_to_dev (pFenAnimGr->hGsys ,&x, &y);
          g_convert_delta_page_to_dev (pFenAnimGr->hGsys ,&cx, &cy);
          ::SetWindowPos (pt_e_log->hwnd, NULL, x, y, cx, cy, SWP_NOACTIVATE | SWP_NOZORDER);
          }
				}
        break;

      case bx_anm_e_num_vi :
				{
        PX_ANM_E_NUM_VI	pt_e_num = (PX_ANM_E_NUM_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        if (pt_e_num->hdl_inline != NULL)
          {
          x =  pt_e_num->x1;
          y =  pt_e_num->y1;
          cx =  pt_e_num->cx;
          cy =  pt_e_num->cy;
          g_convert_point_page_to_dev (pFenAnimGr->hGsys ,&x, &y);
          g_convert_delta_page_to_dev (pFenAnimGr->hGsys ,&cx, &cy);
          ::SetWindowPos (pt_e_num->hdl_inline, NULL, x, y, cx, cy, SWP_NOACTIVATE | SWP_NOZORDER);
          }
				}
        break;

      case bx_anm_e_mes_vi :
				{
        PX_ANM_E_MES_VI	pt_e_mes = (PX_ANM_E_MES_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        if (pt_e_mes->hdl_inline != NULL)
          {
          x =  pt_e_mes->x1;
          y =  pt_e_mes->y1;
          cx =  pt_e_mes->cx;
          cy =  pt_e_mes->cy;
          g_convert_point_page_to_dev (pFenAnimGr->hGsys ,&x, &y);
          g_convert_delta_page_to_dev (pFenAnimGr->hGsys ,&cx, &cy);
          ::SetWindowPos (pt_e_mes->hdl_inline, NULL, x, y, cx, cy, SWP_NOACTIVATE | SWP_NOZORDER);
          }
				}
        break;
      default :
        break;
      }
    }
  }

// --------------------------------------------------------------------------
// Renvoie un pointeur sur l'�l�ment num�ro n_elt de la page courante ou NULL
static PELEMENT_PAGE_GR pElementDansPage (DWORD dwIdPage, DWORD n_elt)
	{
	PELEMENT_PAGE_GR pElementPageRet = NULL; 
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, dwIdPage);
	PPAGE_GR	pPage = ppageHBDGR (pFenAnimGr->hBdGr);

  if (pPage)
    {
    if ((n_elt > 0) && (n_elt <= pPage->NbElements))
      {
			pElementPageRet = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, pPage->PremierElement + n_elt - 1);
			}
		}
	return pElementPageRet;
	}

// --------------------------------------------------------------------------
// Cr�e les contr�les de la page autres que les contr�les Windows
// Change l'Id des contr�les standards
static void cre_entrees_page (DWORD numero_page, PENV_ANM_GR pEnvAnmGr)
  {
  DWORD  dwNEntree;
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, numero_page);
  
	for (dwNEntree = 0; dwNEntree < pFenAnimGr->nbr_entree; dwNEntree++)
    {
		LONG   x,y,cx,cy;
		PX_ANM_E_VI	pt_e_vi = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pFenAnimGr->prem_entree+dwNEntree);

    pEnvAnmGr->padwNEnrEntree [dwNEntree] = pFenAnimGr->prem_entree+dwNEntree;
    switch (pt_e_vi->n_bloc)
      {
      case bx_anm_e_log_vi :
				{
				// entr�e logique : cr�e une entr�e logique
				PX_ANM_E_LOG_VI	pt_e_log = (PX_ANM_E_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        x =  pt_e_log->x;
        y =  pt_e_log->y;
        cx =  pt_e_log->cx;
        cy =  pt_e_log->cy;
        g_convert_point_page_to_dev (pFenAnimGr->hGsys ,&x, &y);
        g_convert_delta_page_to_dev (pFenAnimGr->hGsys ,&cx, &cy);

        pt_e_log->hwnd = hwndCreeFenetreEntLog (pFenAnimGr->hwndVi, x, y, cx, cy, dwNEntree + PREMIER_ID_VALIDE);
				}
        break;

			case bx_anm_e_control_log:
				{
     		PX_ANM_E_CONTROL_LOG_VI	donneex_anm_e_control_log = (PX_ANM_E_CONTROL_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

				// dwNEntree devient l'id de la fen�tre correspondante
				ChangeIdFenetreElementGR (pFenAnimGr->hBdGr, donneex_anm_e_control_log->n_elt, dwNEntree + PREMIER_ID_VALIDE);
				}
				break;

      case bx_anm_e_num_vi :
				{
				PX_ANM_E_NUM_VI	pt_e_num = (PX_ANM_E_NUM_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        x = pt_e_num->x1;
        y = pt_e_num->y1;
        cx = pt_e_num->cx;
        cy = pt_e_num->cy;
        g_convert_point_page_to_dev (pFenAnimGr->hGsys ,&x, &y);
        g_convert_delta_page_to_dev (pFenAnimGr->hGsys ,&cx, &cy);
        pt_e_num->hdl_inline = InLineCreate(pFenAnimGr->hwndVi,
                                            dwNEntree + PREMIER_ID_VALIDE, x, y, cx, cy,
                                             pt_e_num->couleur_fond, pt_e_num->couleur,
                                             pt_e_num->nbr_car,
                                             pt_e_num->police, pt_e_num->style_police, cy,
                                             "", pt_e_num->cadre , pt_e_num->masque, TRUE, pt_e_num->video_focus);
        VerifWarning(bInLineSetModeNum (pt_e_num->hdl_inline, pt_e_num->min, pt_e_num->max, pt_e_num->NbDecimales, IDSERR_LA_SAISIE_DU_NUMERIQUE_EST_INVALIDE ));
        VerifWarning(bInLineNumSet (pt_e_num->hdl_inline, pt_e_num->val_init));
				}
        break;

      case bx_anm_e_mes_vi :
				{
				PX_ANM_E_MES_VI	pt_e_mes = (PX_ANM_E_MES_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        x = pt_e_mes->x1;
        y = pt_e_mes->y1;
        cx = pt_e_mes->cx;
        cy = pt_e_mes->cy;
        g_convert_point_page_to_dev (pFenAnimGr->hGsys ,&x, &y);
        g_convert_delta_page_to_dev (pFenAnimGr->hGsys ,&cx, &cy);
        pt_e_mes->hdl_inline = InLineCreate(pFenAnimGr->hwndVi, dwNEntree + PREMIER_ID_VALIDE, x, y, cx, cy,
				pt_e_mes->couleur_fond, pt_e_mes->couleur, pt_e_mes->nbr_car, pt_e_mes->police, pt_e_mes->style_police, cy,
				pt_e_mes->val_init, pt_e_mes->cadre , pt_e_mes->masque, TRUE, pt_e_mes->video_focus);
				}
        break;

			case bx_anm_e_radio:
				{
     		PX_ANM_E_RADIO	donneex_anm_e_radio = (PX_ANM_E_RADIO)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

				// dwNEntree devient l'id de la fen�tre correspondante
				ChangeIdFenetreElementGR (pFenAnimGr->hBdGr, donneex_anm_e_radio->n_elt, dwNEntree + PREMIER_ID_VALIDE);
				}
				break;

			case bx_anm_e_liste:
				{
				PX_ANM_E_LISTE	donneex_anm_e_liste = (PX_ANM_E_LISTE)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

				// dwNEntree devient l'id de la fen�tre correspondante
				ChangeIdFenetreElementGR (pFenAnimGr->hBdGr, donneex_anm_e_liste->n_elt, dwNEntree + PREMIER_ID_VALIDE);
				}
				break;

			case bx_anm_e_combobox:
				{
				PX_ANM_E_COMBOBOX	donneex_anm_e_combobox = (PX_ANM_E_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

				// dwNEntree devient l'id de la fen�tre correspondante
				ChangeIdFenetreElementGR (pFenAnimGr->hBdGr, donneex_anm_e_combobox->n_elt, dwNEntree + PREMIER_ID_VALIDE);
				}
				break;

      default :
        break;
      }
    } // for (dwNEntree = 0; dwNEntree < pFenAnimGr->nbr_entree; dwNEntree++)
  } // cre_entrees_page

// --------------------------------------------------------------------------
// Ajoute les autres contr�les d'entr�e de la page
// Cr�e les contr�les de la page autres que les contr�les Windows
// Change l'Id des contr�les standards
static void ajoute_entrees_page (DWORD n_page)
  {
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, n_page);

  if (pFenAnimGr->nbr_entree != 0)
    {
		PENV_ANM_GR pEnvAnmGr = (PENV_ANM_GR)pGetEnv(pFenAnimGr->hwndVi);

    if (pEnvAnmGr->padwNEnrEntree == NULL)
      {
      pEnvAnmGr->padwNEnrEntree = (PDWORD)pMemAlloue(pFenAnimGr->nbr_entree * sizeof(DWORD));
      }
    cre_entrees_page(n_page, pEnvAnmGr);
    }
  }

// --------------------------------------------------------------------------
// prise en compte de la destruction de tous les objets d'entr�e d'une page
static void enleve_entrees_page (DWORD n_page)
  {
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, n_page);
  PENV_ANM_GR pEnvAnmGr;
  DWORD dwNEntree;

  for (dwNEntree = 0;dwNEntree < pFenAnimGr->nbr_entree; dwNEntree++)
    {
		PX_ANM_E_VI	pt_e_vi = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi,pFenAnimGr->prem_entree+dwNEntree);

    switch (pt_e_vi->n_bloc)
      {
      case bx_anm_e_log_vi :
				{
				PX_ANM_E_LOG_VI pt_e_log = (PX_ANM_E_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        pt_e_log->hwnd = NULL;
				}
        break;
      case bx_anm_e_num_vi :
				{
				PX_ANM_E_NUM_VI	pt_e_num = (PX_ANM_E_NUM_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        pt_e_num->hdl_inline = NULL;
				}
        break;
      case bx_anm_e_mes_vi :
				{
				PX_ANM_E_MES_VI	pt_e_mes = (PX_ANM_E_MES_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

        pt_e_mes->hdl_inline = NULL;
				}
        break;

/* utiliser si besoin
			case bx_anm_e_control_log:
			case bx_anm_e_liste:
			case bx_anm_e_radio:
			case bx_anm_e_combobox:
				break;
*/
			default:
				break;
      }
    }
  pEnvAnmGr = (PENV_ANM_GR)pGetEnv(pFenAnimGr->hwndVi);
  MemLibere((PVOID *)(&pEnvAnmGr->padwNEnrEntree));
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			Fonctions pour les animations couleurs avec gestion couleurs clignottantes
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Forcage en couleur des �l�ments anim�s clignottants
static void AnimationCouleurElementClignottant (HWND hwnd, BOOL bEtatVisible)
  {
	G_COULEUR CouleurCour;
	DWORD dwNbEnr;
	DWORD dwNEnr;
	PENV_ANM_GR pEnvAnmGr = (PENV_ANM_GR)pGetEnv (hwnd);
	PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);

	dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_liste_elts_cli);
	dwNEnr = 0;
	CouleurCour = G_COULEUR_TRANSPARENTE;
	while ((dwNEnr < dwNbEnr))
		{
		PX_ANM_ELT_LISTE_ELTS_CLI	pEltListeEltsClignottant;
		dwNEnr++;
		pEltListeEltsClignottant = (PX_ANM_ELT_LISTE_ELTS_CLI)pointe_enr (szVERIFSource, __LINE__, bx_anm_liste_elts_cli, dwNEnr);
		if (pEltListeEltsClignottant->Page == pEnvAnmGr->wId)
			{
			if (bEtatVisible)
				{
				CouleurCour = pEltListeEltsClignottant->Couleur;
				}
			if ((CouleurCour >= G_N_COULEUR_MIN) && (CouleurCour <= G_N_COULEUR_MAX))
				{
				bdgr_couleur_element (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pEltListeEltsClignottant->Element, CouleurCour);
				}
			else
				{
				// faire monter statut
				}
			}
		}
	}

// --------------------------------------------------------------------------
// Recherche de l'element dans la liste des elements clignottants
static BOOL bEltPresentListeEltsClignottants (DWORD Page, DWORD Element, DWORD *pdwNEnr)
  {
	BOOL bTrouve = FALSE;
	DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_liste_elts_cli);

	(*pdwNEnr) = 0;
	while ((*pdwNEnr < dwNbEnr) && (!bTrouve))
		{
		PX_ANM_ELT_LISTE_ELTS_CLI	pEltListeEltsClignottant;

		(*pdwNEnr)++;
		pEltListeEltsClignottant = (PX_ANM_ELT_LISTE_ELTS_CLI)pointe_enr (szVERIFSource, __LINE__, bx_anm_liste_elts_cli, (*pdwNEnr));
		bTrouve = ((pEltListeEltsClignottant->Page == Page) && (pEltListeEltsClignottant->Element == Element));
		}
	return (bTrouve);
  }

// --------------------------------------------------------------------------
// insere dans la liste des elements ayant une couleur clignotante. Ignore si deja present
// La liste contient la couleur de l'element en etat visible. 
static void AddEltListeEltsClignottants (DWORD Page, DWORD Element, G_COULEUR Couleur)
  {
	PX_ANM_ELT_LISTE_ELTS_CLI	pEltListeEltsClignottant;
	DWORD dwNEnr;

	if (!bEltPresentListeEltsClignottants (Page, Element, &dwNEnr))
		{
		dwNEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_liste_elts_cli) + 1;
		insere_enr (szVERIFSource, __LINE__, 1, bx_anm_liste_elts_cli, dwNEnr);
		}
	pEltListeEltsClignottant = (PX_ANM_ELT_LISTE_ELTS_CLI)pointe_enr (szVERIFSource, __LINE__, bx_anm_liste_elts_cli, dwNEnr);
	pEltListeEltsClignottant->Page = Page;
	pEltListeEltsClignottant->Element = Element;
	pEltListeEltsClignottant->Couleur = Couleur;
	}

// --------------------------------------------------------------------------
// Suppression de l'element dans la liste des elements clignottants si il existe
static void SupEltListeEltsClignottants (DWORD Page, DWORD Element)
  {
	DWORD dwNEnr;

	if (bEltPresentListeEltsClignottants (Page, Element, &dwNEnr))
		{
		enleve_enr (szVERIFSource, __LINE__, 1, bx_anm_liste_elts_cli, dwNEnr);
		}
	}

// --------------------------------------------------------------------------
// Suppression de tous les elements clignottants d'une page dans la liste des elements clignottants 
static void SupEltPageListeEltsClignottants (DWORD Page)
  {
	DWORD dwNEnr = 1;

	while (dwNEnr <= nb_enregistrements (szVERIFSource, __LINE__, bx_anm_liste_elts_cli)) 
		{
		PX_ANM_ELT_LISTE_ELTS_CLI	pEltListeEltsClignottant = (PX_ANM_ELT_LISTE_ELTS_CLI)pointe_enr (szVERIFSource, __LINE__, bx_anm_liste_elts_cli, dwNEnr);
		if (pEltListeEltsClignottant->Page == Page)
			{
			enleve_enr (szVERIFSource, __LINE__, 1, bx_anm_liste_elts_cli, dwNEnr);
			}
		else
			{
			dwNEnr++;
			}
		}
	}

// --------------------------------------------------------------------------
// Forcage en couleur des �l�ments anim�s : gere les couleurs clignottantes
// si la couleur est clignottante, l'animation se fera au prochain message clignottement recu par la fenetre 
// ceci pour synchroniser tous les clignottements de toutes les fenetres
static void AnimationCouleurElement (DWORD Page, DWORD ModeTravail, DWORD Element, G_COULEUR Couleur)
  {
	if (Couleur > G_N_COULEUR_MAX) 
		{
		if (Couleur >= G_OFFSET_CLIGNOTANT) // Couleur clignottante
			{
			LONG CouleurCour = Couleur;

			CouleurCour = CouleurCour - G_OFFSET_CLIGNOTANT;
			AddEltListeEltsClignottants (Page, Element, (G_COULEUR)CouleurCour);
			}
		else
			{
			// faire monter statut
			}
		}
	else
		{
		SupEltListeEltsClignottants (Page, Element); // au cas ou l'ancienne couleur est clignottante
		if ((Couleur >= G_N_COULEUR_MIN) && (Couleur <= G_N_COULEUR_MAX))
			{
			PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, Page);

			G_COULEUR OldCouleurLigne;
			G_COULEUR OldCouleurRemplissage;

			bdgr_lire_couleur_element (pFenAnimGr->hBdGr, Element, &OldCouleurLigne, &OldCouleurRemplissage);

			// on ne teste pas la couleur de remplissage car ignor� pour les textes et identique pour les autres
			if (Couleur != OldCouleurLigne) 
				{
				bdgr_couleur_element (pFenAnimGr->hBdGr, ModeTravail, Element, Couleur);
				}
			}
		else
			{
			// faire monter statut
			}
		}
	}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			Fonctions pour les Courbes
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Initialisations Courbes
// --------------------------------------------------------------------------
static void InitCourbes (void)
  {
  DWORD             wNbCourbes;
  DWORD             wNumeroCourbe;
  PX_ANM_COURBE_VI	pCourbe;
  DWORD             wIndexPoint;
  PPOINT						pPoints;
  LONG               x_generalise;

  PX_ANM_COURBE_FIC_VI	pCourbeArchive;

  if (existe_repere (bx_anm_courbe_vi))
    {
    wNbCourbes = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_courbe_vi);
    for (wNumeroCourbe = 1; wNumeroCourbe <= wNbCourbes; wNumeroCourbe++)
      {
      pCourbe = (PX_ANM_COURBE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_vi, wNumeroCourbe);
      pCourbe->nb_points = 0;
      pCourbe->old_nb_points = 0;
      pCourbe->paPoints = (PPOINT)pMemAlloue ((size_t) (pCourbe->nb_points_max * sizeof (POINT)));
      //
      pPoints = pCourbe->paPoints;
      x_generalise = pCourbe->x_org;
      for (wIndexPoint = 0; wIndexPoint < pCourbe->nb_points_max; wIndexPoint++)
        {
        pPoints [wIndexPoint].x = HIWORD(x_generalise);
        pPoints [wIndexPoint].y = 0;
        //
        x_generalise = x_generalise + pCourbe->pas_x;
        }

      }
    }
  if (existe_repere (bx_anm_courbe_archive_vi))
    {
    wNbCourbes = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi);
    for (wNumeroCourbe = 1; wNumeroCourbe <= wNbCourbes; wNumeroCourbe++)
      {
      pCourbeArchive = (PX_ANM_COURBE_FIC_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi, wNumeroCourbe);
      pCourbeArchive->paPoints = NULL;
      pCourbeArchive->nb_points_max = 0;
      }
    }
  }

// --------------------------------------------------------------------------
static void FiniCourbes (void)
  {
  if (existe_repere (bx_anm_courbe_vi))
    {
    DWORD wNbCourbes = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_courbe_vi);
    for (DWORD wNumeroCourbe = 1; wNumeroCourbe <= wNbCourbes; wNumeroCourbe++)
      {
			PX_ANM_COURBE_VI	pCourbe = (PX_ANM_COURBE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_vi, wNumeroCourbe);
      MemLibere ((PVOID *)(&pCourbe->paPoints));
      }
    }

  if (existe_repere (bx_anm_courbe_archive_vi))
    {
    DWORD wNbCourbes = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi);
    for (DWORD wNumeroCourbe = 1; wNumeroCourbe <= wNbCourbes; wNumeroCourbe++)
      {
      PX_ANM_COURBE_FIC_VI	pCourbeArchive = (PX_ANM_COURBE_FIC_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi, wNumeroCourbe);
      if (pCourbeArchive->paPoints)
        {
        MemLibere ((PVOID *)(&pCourbeArchive->paPoints));
        }
      }
    }
  }

// --------------------------------------------------------------------------
// Flush m�moire de la courbe
// --------------------------------------------------------------------------
static void FlushCourbe (PX_ANM_COURBE_VI	pCourbe)
  {
  pCourbe->nb_points = 0;
  pCourbe->old_nb_points = 0;
  }

// --------------------------------------------------------------------------
// Trace une courbe par des segments ou des escaliers (trac�s __|) entre chaque points
// --------------------------------------------------------------------------
static void TracePolyLignes (HGSYS hGsys, G_COULEUR couleur, G_STYLE_LIGNE style, DWORD type_courbe, DWORD nb_points, POINT * paPoints)
  {
  switch (type_courbe)
    {
    case LINEAIRE:
			{
      for (DWORD wPoint = 1; wPoint < nb_points; wPoint++)
        {
        LONG x1 = paPoints [wPoint - 1].x;
        LONG y1 = paPoints [wPoint - 1].y;
        LONG x2 = paPoints [wPoint].x;
        LONG y2 = paPoints [wPoint].y;
        if ((y1 != MAXSHORT) && (y1 != -MAXSHORT) && (y2 != MAXSHORT) && (y2 != -MAXSHORT))
          {
          g_line (hGsys, couleur, style, x1, y1, x2, y2);
          }
        }
			}
      break;

    case ESCALIER:
			{
      for (DWORD wPoint = 1; wPoint < nb_points; wPoint++)
        {
        LONG x1 = paPoints [wPoint - 1].x;
        LONG y1 = paPoints [wPoint - 1].y;
        LONG x2 = paPoints [wPoint].x;
        LONG y2 = paPoints [wPoint].y;
        if ((y1 != MAXSHORT) && (y1 != -MAXSHORT) && (y2 != MAXSHORT) && (y2 != -MAXSHORT))
          {
          if (y1 != y2)
            {
            g_line (hGsys, couleur, style, x1, y1, x2, y1);
            g_line (hGsys, couleur, style, x2, y1, x2, y2);
            }
          else
            {
            g_line (hGsys, couleur, style, x1, y1, x2, y2);
            }
           }
        }
			}
      break;
    }
  }

// --------------------------------------------------------------------------
// Dessine une courbe par des bargraphes pour chaque point
// --------------------------------------------------------------------------
static void TracePolyBars (HGSYS hGsys, G_COULEUR couleur, G_STYLE_REMPLISSAGE style, LONG yBas, DWORD nb_points, POINT * paPoints)
  {
  DWORD wPoint;
  LONG  x1;
  LONG  x2;
  LONG  y1;

  for (wPoint = 1; wPoint < nb_points; wPoint++)
    {
    x1 = paPoints [wPoint - 1].x;
    y1 = paPoints [wPoint - 1].y;
    x2 = paPoints [wPoint].x;
    if ((yBas != MAXSHORT) && (yBas != -MAXSHORT) && (y1 != MAXSHORT) && (y1 != -MAXSHORT))
      {
      g_fill_rectangle (hGsys, couleur, style, x1, yBas, x2, y1);
      }
    }
  }

// --------------------------------------------------------------------------
// Dessin de toutes les Courbes d'une page (WM_PAINT)
// --------------------------------------------------------------------------
static void DessineCourbesPage (HGSYS hGsys, DWORD wNumeroPage)
  {
  DWORD wNumeroCourbe = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_anm_courbe_vi);
  while (wNumeroCourbe > 0)
    {
		PX_ANM_COURBE_VI	pCourbe = (PX_ANM_COURBE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_vi, wNumeroCourbe);
    if (pCourbe->numero_page == wNumeroPage)
      {
      TracePolyLignes (hGsys, pCourbe->couleur, (G_STYLE_LIGNE)pCourbe->style, pCourbe->type_courbe, pCourbe->nb_points, pCourbe->paPoints);
      }
    wNumeroCourbe--;
    }

  wNumeroCourbe = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi);
  while (wNumeroCourbe > 0)
    {
		PX_ANM_COURBE_FIC_VI	pCourbeArchive = (PX_ANM_COURBE_FIC_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi, wNumeroCourbe);
    if (pCourbeArchive->numero_page == wNumeroPage)
      {
      switch (pCourbeArchive->modele)
        {
        case LINEAIRE :
          TracePolyLignes (hGsys, pCourbeArchive->couleur, (G_STYLE_LIGNE)pCourbeArchive->style, LINEAIRE, pCourbeArchive->nb_points_max, pCourbeArchive->paPoints);
          break;
        case ESCALIER :
          TracePolyLignes (hGsys, pCourbeArchive->couleur, (G_STYLE_LIGNE)pCourbeArchive->style, ESCALIER, pCourbeArchive->nb_points_max, pCourbeArchive->paPoints);
          break;
        case HISTOGRAMME :
          TracePolyBars (hGsys, pCourbeArchive->couleur, (G_STYLE_REMPLISSAGE)pCourbeArchive->style, (LONG)pCourbeArchive->yBas, pCourbeArchive->nb_points_max, pCourbeArchive->paPoints);
          break;
        default:
          break;
        }
      }
    wNumeroCourbe--;
    }
  }

// --------------------------------------------------------------------------
// Dessine le nouveau morceau de Courbe qui s'est rajout�
// --------------------------------------------------------------------------
static void DessineNewMorceauCourbe (HGSYS hGsys, PX_ANM_COURBE_VI	pCourbe)
  {
  POINT *	pPoints = pCourbe->paPoints;
  DWORD		wNbPoints = pCourbe->nb_points - pCourbe->old_nb_points;

  if (wNbPoints > 1)
    {
    TracePolyLignes (hGsys, pCourbe->couleur, (G_STYLE_LIGNE)pCourbe->style, pCourbe->type_courbe, wNbPoints, &pPoints [pCourbe->old_nb_points]);
    pCourbe->old_nb_points = pCourbe->nb_points - 1;
    }
  }

// --------------------------------------------------------------------------
// Efface Morceau Courbe pr�sent pour Scroll
// --------------------------------------------------------------------------
static void EffaceOldMorceauCourbe (HGSYS hGsys, PX_ANM_COURBE_VI	pCourbe)
  {
	RECT			rect;

	// calcule la zone � redessinner
	rect.top = pCourbe->yHaut;
	rect.bottom = pCourbe->yBas;
	rect.left = (LONG)HIWORD (pCourbe->x_org);
	rect.right = (LONG)HIWORD(pCourbe->x_org + pCourbe->nb_points_max * pCourbe->pas_x);

	// Invalide la
	g_InvalidateRect (hGsys, &rect);
  }

// --------------------------------------------------------------------------
// Ajout un point sur le nouveau morceau de courbe
// --------------------------------------------------------------------------
static void AjoutPointCourbe (LONG valeur_y, HGSYS hGsys, PX_ANM_COURBE_VI	pCourbe)
  {
	// Buffer points plein ?
  if (pCourbe->nb_points >= pCourbe->nb_points_max)
    {
		// oui => scroll de la courbe :

		// efface le morceau de courbe existant
		EffaceOldMorceauCourbe (hGsys, pCourbe);

		// d�calage du buffer en M�moire
	  pCourbe->nb_points = pCourbe->nb_points_max - pCourbe->nb_points_scroll;
		pCourbe->old_nb_points = 0;

		POINT *	pPoints = pCourbe->paPoints;
		for (DWORD wIndexPoint = 0; wIndexPoint < pCourbe->nb_points; wIndexPoint++)
			{
			pPoints [wIndexPoint].y = pPoints [wIndexPoint + pCourbe->nb_points_scroll].y;
			}
    }

	// ajout du point au buffer
  pCourbe->paPoints [pCourbe->nb_points].y = valeur_y;
  pCourbe->nb_points++;
  }

// --------------------------------------------------------------------------
// calcul y affichage point en fonction de la valeur de l'�chantillon (mise � l'echelle)
// --------------------------------------------------------------------------
static LONG YCourbe (FLOAT rValeur, PX_ANM_COURBE_VI	pCourbe)
  {
  LONG y_valeur = (LONG) ((rValeur * pCourbe->homothetie) + pCourbe->translation);

  if (y_valeur <= pCourbe->yHaut)
    {
    y_valeur = pCourbe->yHaut;
    }
  else
    {
    if (y_valeur >= pCourbe->yBas)
      {
      y_valeur = pCourbe->yBas;
      }
    }

  return y_valeur;
  }

// --------------------------------------------------------------------------
// calculs parametres courbes archivages
// --------------------------------------------------------------------------
static void CalculParametresCbArchive (PX_ANM_COURBE_FIC_VI	pCourbe, DWORD wNombre, FLOAT *rHomothetie, FLOAT *rTranslation, FLOAT *PasX, LONG *PasEch)
  {
  (*rHomothetie)  = (FLOAT)(pCourbe->yHaut - pCourbe->yBas) / (pCourbe->max - pCourbe->min);
  (*rTranslation) = (FLOAT)(pCourbe->yBas) - (pCourbe->min * (*rHomothetie));
  DWORD wNbPixelX = HIWORD (pCourbe->coef_en_x);
  if (wNombre <= 1)
    {
    (*PasX) = (FLOAT) wNbPixelX;
    (*PasEch) = 1;
    }
  else
    {
    if (wNombre > wNbPixelX)
      { // echantillonnage
      (*PasEch) = (LONG) (wNombre / wNbPixelX);
      if ((wNombre % wNbPixelX) != 0)
        {
        (*PasEch)++;
        }
      (*PasX) = ((FLOAT) wNbPixelX) / (((FLOAT) wNombre) / ((FLOAT) *PasEch));
      }
    else
      { // cas normal
      (*PasX) = ((FLOAT) wNbPixelX) / ((FLOAT) (wNombre - 1));
      (*PasEch) = 1;
      }
    }
  }

// --------------------------------------------------------------------------
// calcul y en fonction de la valeur du r�el (mise � l'echelle)
// --------------------------------------------------------------------------
static LONG YCourbeArchive (FLOAT rValeur, PX_ANM_COURBE_FIC_VI	pCourbe, FLOAT rHomothetie, FLOAT rTranslation)
  {
  LONG y_valeur;

  if (rValeur < pCourbe->min)
    {
		// valeur limite pour les courbes archives
    y_valeur = -MAXSHORT;
    }
  else
    {
    if (rValeur > pCourbe->max)
      {
			// valeur limite pour les courbes archives
      y_valeur = MAXSHORT;
      }
    else
      {
      y_valeur = (LONG) ((rValeur * rHomothetie) + rTranslation);
      }
    }

  return y_valeur;
  }

// --------------------------------------------------------------------------
// Ajout un point sur la courbe archive
// --------------------------------------------------------------------------
static void AjoutPointCourbeArchive (LONG valeur_x, LONG valeur_y, PX_ANM_COURBE_FIC_VI	pCourbe)
  {
  POINT *pPoints = pCourbe->paPoints;

  pPoints [pCourbe->nb_points_max].x = valeur_x;
  pPoints [pCourbe->nb_points_max].y = valeur_y;
  pCourbe->nb_points_max++;
  }

// --------------------------------------------------------------------------
// Flush m�moire de la courbe archive
// --------------------------------------------------------------------------
static void FlushCourbeArchive (PX_ANM_COURBE_FIC_VI	pCourbe)
  {
  pCourbe->nb_points_max = 0;
  }

// --------------------------------------------------------------------------
// Dessine une Courbe d'Archivage
// --------------------------------------------------------------------------
static void DessineCourbeArchive (HGSYS hGsys, PX_ANM_COURBE_FIC_VI pCourbe)
  {
  POINT *	pPoints = pCourbe->paPoints;
  DWORD   wNbPoints = pCourbe->nb_points_max;

  if (wNbPoints > 1)
    {
    switch (pCourbe->modele)
      {
      case LINEAIRE :
        TracePolyLignes (hGsys, pCourbe->couleur, (G_STYLE_LIGNE)pCourbe->style, LINEAIRE, wNbPoints, pCourbe->paPoints);
        break;
      case ESCALIER :
        TracePolyLignes (hGsys, pCourbe->couleur, (G_STYLE_LIGNE)pCourbe->style, ESCALIER, wNbPoints, pCourbe->paPoints);
        break;
      case HISTOGRAMME :
        TracePolyBars (hGsys, pCourbe->couleur, (G_STYLE_REMPLISSAGE)pCourbe->style, (LONG)pCourbe->yBas, wNbPoints, pCourbe->paPoints);
        break;
      default:
        break;
      }
    }
  }

// --------------------------------------------------------------------------
// Efface une Courbe d'Archivage
// --------------------------------------------------------------------------
static void EffaceCourbeArchive (HGSYS hGsys, PX_ANM_COURBE_FIC_VI	pCourbe)
  {
	//$$  DessineCourbeArchive (hGsys, pCourbe);
	RECT	rect;
	DWORD wNbPixelX = HIWORD (pCourbe->coef_en_x);

	// calcule la zone � redessinner
	rect.top = pCourbe->yHaut;
	rect.bottom = pCourbe->yBas;
	rect.left = (LONG)(HIWORD(pCourbe->x_min));
	rect.right = rect.left + wNbPixelX;

	g_InvalidateRect (hGsys, &rect);
  }

// --------------------------------------------------------------------------
// Flush m�moire de toutes les courbes archive d'une page
// --------------------------------------------------------------------------
static void FlushCourbeArchivePage (DWORD wPage)
  {
  DWORD wNumeroCourbe = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi);
  while (wNumeroCourbe > 0)
    {
		PX_ANM_COURBE_FIC_VI	pCourbe = (PX_ANM_COURBE_FIC_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi, wNumeroCourbe);
    if (pCourbe->numero_page == wPage)
      {
      FlushCourbeArchive (pCourbe);
      }
    wNumeroCourbe--;
    }
  }


// --------------------------------------------------------------------------
// Fonctions d'Ecritures
// --------------------------------------------------------------------------
static BOOL NotificationTrame (DWORD ValType)
  {
  t_anm_gr_notification MsgNotification;

  MsgNotification.CodeRetour = 0;
  MsgNotification.Valeur.ValType = ValType;

  return bCliAnmGrEnvoiMessage (sizeof (DWORD) + sizeof (ValType), &MsgNotification);
  }

// --------------------------------------------------------------------------
static BOOL NotificationBoolean (DWORD CodeRet, BOOL Val)
  {
  t_anm_gr_notification MsgNotification;

  MsgNotification.CodeRetour = CodeRet;
  MsgNotification.Valeur.ValLogique = Val;

  return bCliAnmGrEnvoiMessage (sizeof (CodeRet) + sizeof (Val), &MsgNotification);
  }

// --------------------------------------------------------------------------
static BOOL NotificationReel (DWORD CodeRet, FLOAT Val)
  {
  t_anm_gr_notification MsgNotification;

  MsgNotification.CodeRetour = CodeRet;
  MsgNotification.Valeur.ValNumerique = Val;

  return bCliAnmGrEnvoiMessage (sizeof (CodeRet) + sizeof (Val), &MsgNotification);
  }

// --------------------------------------------------------------------------
static BOOL NotificationPsz (DWORD CodeRet, const char *Val)
  {
  t_anm_gr_notification MsgNotification;

  MsgNotification.CodeRetour = CodeRet;
  StrCopy (MsgNotification.Valeur.ValMessage, Val);

  return bCliAnmGrEnvoiMessage (sizeof (CodeRet) + StrLength (Val) + 1, &MsgNotification);
  }

// --------------------------------------------------------------------------
// Fonctions d'Extraction de parametres
// --------------------------------------------------------------------------
static HEADER_SERVICE_ANM_GR ExtraireHeaderServiceAnmGr (BYTE **pBuff)
  {
  HEADER_SERVICE_ANM_GR Valeur =  *((HEADER_SERVICE_ANM_GR *) *pBuff);
  *pBuff += sizeof (Valeur);

  return Valeur;
  }

// --------------------------------------------------------------------------
static BOOL ExtraireBoolean (BYTE **pBuff)
  {
  BOOL Valeur = *((BOOL *) *pBuff);

  *pBuff += sizeof (Valeur);

  return Valeur;
  }

// --------------------------------------------------------------------------
static DWORD ExtraireWord (BYTE **pBuff)
  {
  DWORD Valeur = *((DWORD *) *pBuff);

  *pBuff += sizeof (Valeur);

  return Valeur;
  }

// --------------------------------------------------------------------------
static FLOAT ExtraireReel (BYTE **pBuff)
  {
  FLOAT Valeur = *((FLOAT *) *pBuff);

  *pBuff += sizeof (Valeur);

  return Valeur;
  }

// --------------------------------------------------------------------------
static char *ExtrairePsz (BYTE **pBuff, char *PszChaine)
  {
  char *Valeur = (char *) (*pBuff);

  StrCopy (PszChaine, Valeur);
  *pBuff += StrLength (Valeur) + 1;

  return PszChaine;
  }


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//					Service d'Animation
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Service : ne fait rien
static void AnimNull (BYTE **pBuff, DWORD Position)
  {
  }

// --------------------------------------------------------------------------
// Service : envoie un AR au serveur
static void AnimAck (BYTE **pBuff, DWORD Position)
  {
  bCliAnmGrEnvoiAR ();
  }

// -----------------------------------------------------------------------
// Envoie au serveur les dimensions externes virtuelles de la fen�tre
static void renvoie_coordonnees_etat (PX_ANM_FENETRE_VI	pFenAnimGr)
  {
	RECT rect;
  LONG lx;
  LONG ly;

  NotificationTrame (DEBUT_TRAME);
  NotificationReel (pFenAnimGr->code_ret_dimension , (FLOAT) pFenAnimGr->dimension);
  if (pFenAnimGr->dimension == DIM_NORMALE)
    {
		::GetWindowRect (pFenAnimGr->hwndVi, &rect);

		// largeur et hauteur
    lx = rect.right - rect.left;
    ly = rect.bottom - rect.top;
    Spc_CvtSizeEnVirtuel (hSpcFen, &lx,  &ly);
    NotificationReel (pFenAnimGr->code_ret_cx , (FLOAT) lx);
    NotificationReel (pFenAnimGr->code_ret_cy , (FLOAT) ly);
		
		// origine du coin haut gauche
    lx = rect.left;
    ly = rect.top;
    Spc_CvtPtEnVirtuel (hSpcFen, &lx,  &ly);
    NotificationReel (pFenAnimGr->code_ret_x , (FLOAT) lx);
    NotificationReel (pFenAnimGr->code_ret_y , (FLOAT) ly);
    }
  NotificationTrame (FIN_TRAME);
  }

// --------------------------------------------------------------------------
// Service : ferme une fen�tre si elle est ouverte
static void AnimFenEfface (BYTE **pBuff, DWORD Position)
  {
  PX_ANM_FENETRE_VI pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);

  if (pFenAnimGr->visible)
    {
    ::DestroyWindow (pFenAnimGr->hwndVi);
    }
  }

// --------------------------------------------------------------------------
static void AnimFenTaille (BYTE **pBuff, DWORD Position)
  {
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
  LONG               xFen = (LONG)  ExtraireReel (pBuff);
  LONG               yFen = (LONG)  ExtraireReel (pBuff);
  LONG               cxFen = (LONG)  ExtraireReel (pBuff);
  LONG               cyFen = (LONG)  ExtraireReel (pBuff);
  DWORD             wNewDim = (DWORD) ExtraireReel (pBuff);

  if (pFenAnimGr->visible)
    {
		int	 fs;

    switch (wNewDim)
      {
			case DIM_NORMALE:
				fs = SW_SHOWNORMAL;	// Activation et visualisation normale
				break;
			case DIM_ICONE:
				fs = SW_MINIMIZE;		// Mise en icone
				break;
			case DIM_MAX:
			fs = SW_SHOWMAXIMIZED;	// activation et affichage en plein �cran
				break;
			default:
				fs = 0;
				break;
      }

    if ((pFenAnimGr->dimension == DIM_NORMALE) || (pFenAnimGr->dimension != wNewDim))
      {
		  //$$SWP	swp;

      pFenAnimGr->dimension = wNewDim;
      pFenAnimGr->pas_forcage = FALSE;
      //$$WinQueryWindowPos (HWND_DESKTOP, &swp);
      g_set_page_units (pFenAnimGr->hGsys, c_G_NB_X, c_G_NB_Y, Appli.sizEcran.cx, Appli.sizEcran.cy);
      g_convert_point_page_to_dev (pFenAnimGr->hGsys, &xFen, &yFen);
      g_convert_delta_page_to_dev (pFenAnimGr->hGsys, &cxFen, &cyFen);
      
			// Affiche la fen�tre dans son �tat
			::MoveWindow (pFenAnimGr->hwndVi, xFen, yFen, cxFen, cyFen, TRUE);
			::ShowWindow (pFenAnimGr->hwndVi, fs);
			//::SetWindowPos (pFenAnimGr->hwndVi, HWND_TOP, lxFen, lyFen, lcxFen, lcyFen, fs | SWP_NOACTIVATE |SWP_NOZORDER); // $$V�rifier
      if (pFenAnimGr->mode_aff == MODE_ZOOM)
        {
				RECT rect;
     		LONG  lcx;
				LONG  lcy;

				//$$WinQueryWindowPos (pFenAnimGr->hwndVi, &swp);
				::GetClientRect (pFenAnimGr->hwndVi, &rect);
        lcx = (LONG) ((FLOAT)rect.right * pFenAnimGr->coeff_zoom);
        lcy = (LONG) ((FLOAT)rect.bottom * pFenAnimGr->coeff_zoom);
        g_set_page_units (pFenAnimGr->hGsys, pFenAnimGr->cx_client_ref, pFenAnimGr->cy_client_ref, lcx, lcy);
        }
      pFenAnimGr->pas_forcage = TRUE;
      }
    }
  }

// --------------------------------------------------------------------------
static void AnimSelectPage (BYTE **pBuff, DWORD Position)
  {
  DWORD n_page = ExtraireWord (pBuff);

  if (n_page <= nb_enregistrements (szVERIFSource, __LINE__, bx_anm_fenetre_vi) && (bdgr_existe_page (n_page)))
    wCtxtPage = n_page;
  }

// --------------------------------------------------------------------------
static void AnimImpression (BYTE **pBuff, DWORD Position)
  {
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, Position);
  DWORD             wTypeImpression = (DWORD) ExtraireReel (pBuff);
  char              pszNomDoc [256];

  ExtrairePsz (pBuff, pszNomDoc);

  if (pFenAnimGr->visible)
    {
		void               *hdl;
		LONG                 x0Page;
		LONG                 y0Page;
		LONG                 cxPage;
		LONG                 cyPage;
		LONG                 x0Dessin;
		LONG                 y0Dessin;
		LONG                 cxDessin;
		LONG                 cyDessin;
		LONG                 x1Draw;
		LONG                 y1Draw;
		LONG                 x2Draw;
		LONG                 y2Draw;

    g_set_document_name (pFenAnimGr->hGsys, pszNomDoc, c_DEVICE_DOC_TYPE_UNCHANGED);
    //
    switch (wTypeImpression)
      {
      case 0: //Pas d'impression
        break;
      case 1: //Impression Synoptique en Vectoriel Via Device Imprimante
				VerifWarningExit;
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_PRINTER);
        bdgr_dessiner_page (pFenAnimGr->hBdGr, MODE_DESSIN_ELEMENT);
        DessineCourbesPage (pFenAnimGr->hGsys, Position);
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        break;
      case 2: //Impression Synoptique Via Meta File
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_METAFILE);
        //
        bdgr_dessiner_page (pFenAnimGr->hBdGr, MODE_DESSIN_ELEMENT);
        DessineCourbesPage (pFenAnimGr->hGsys, Position);
        //
        hdl = g_select_device (pFenAnimGr->hGsys, c_DEVICE_PRINTER);
        g_draw_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_METAFILE, c_DEVICE_ADAPT_ADJUST, 0, 0, 0, 0);
        //
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        break;
      case 3: //Impression Fenetre Client Via Bit-Map
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_BITMAP_CLIENT);
        //
        g_get_device_limits (pFenAnimGr->hGsys, &x0Dessin, &y0Dessin, &cxDessin, &cyDessin);
        //
        hdl = g_select_device (pFenAnimGr->hGsys, c_DEVICE_PRINTER);
        g_get_device_limits (pFenAnimGr->hGsys, &x0Page, &y0Page, &cxPage, &cyPage);
        MakeProportional (cxPage, cyPage, cxDessin, cyDessin, &x2Draw, &y2Draw);
        x1Draw = x0Page;
        y1Draw = y0Page;
        x2Draw = x1Draw + x2Draw;
        y2Draw = y1Draw + y2Draw;
        g_draw_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP, c_DEVICE_ADAPT_ADJUST, x1Draw, y1Draw, x2Draw, y2Draw);
        //
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        break;
      case 4: //Impression Fenetre Parent (Frame) Via Bit-Map
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_BITMAP_FRAME);
        //
        g_get_device_limits (pFenAnimGr->hGsys, &x0Dessin, &y0Dessin, &cxDessin, &cyDessin);
        //
        hdl = g_select_device (pFenAnimGr->hGsys, c_DEVICE_PRINTER);
        g_get_device_limits (pFenAnimGr->hGsys, &x0Page, &y0Page, &cxPage, &cyPage);
        MakeProportional (cxPage, cyPage, cxDessin, cyDessin, &x2Draw, &y2Draw);
        x1Draw = x0Page;
        y1Draw = y0Page;
        x2Draw = x1Draw + x2Draw;
        y2Draw = y1Draw + y2Draw;
        g_draw_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP, c_DEVICE_ADAPT_ADJUST, x1Draw, y1Draw, x2Draw, y2Draw);
        //
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        break;
      case 5: //Impression Ecran Via Bit-Map
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_BITMAP_SCREEN);
        //
        g_get_device_limits (pFenAnimGr->hGsys, &x0Dessin, &y0Dessin, &cxDessin, &cyDessin);
        //
        hdl = g_select_device (pFenAnimGr->hGsys, c_DEVICE_PRINTER);
        g_get_device_limits (pFenAnimGr->hGsys, &x0Page, &y0Page, &cxPage, &cyPage);
        MakeProportional (cxPage, cyPage, cxDessin, cyDessin, &x2Draw, &y2Draw);
        x1Draw = x0Page;
        y1Draw = y0Page;
        x2Draw = x1Draw + x2Draw;
        y2Draw = y1Draw + y2Draw;
        g_draw_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP, c_DEVICE_ADAPT_ADJUST, x1Draw, y1Draw, x2Draw, y2Draw);
        //
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        break;
      case 6: //Sortie Synoptique sur Fichier Meta File
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_METAFILE);
        bdgr_dessiner_page (pFenAnimGr->hBdGr, MODE_DESSIN_ELEMENT);
        DessineCourbesPage (pFenAnimGr->hGsys, Position);
        hdl = g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        g_save_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_METAFILE);
        g_delete_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_METAFILE);
        break;
      case 7: //Sortie Fenetre Client sur Fichier Bit-Map
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_BITMAP_CLIENT);
        hdl = g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        g_save_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP);
        g_delete_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP);
        break;
      case 8: //Sortie Fenetre Parent (Frame) sur Fichier Bit-Map
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_BITMAP_FRAME);
        hdl = g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        g_save_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP);
        g_delete_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP);
        break;
      case 9: //Sortie Ecran sur Fichier Bit-Map
        g_select_device (pFenAnimGr->hGsys, c_DEVICE_BITMAP_SCREEN);
        hdl = g_select_device (pFenAnimGr->hGsys, c_DEVICE_WINDOW);
        g_save_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP);
        g_delete_handle (pFenAnimGr->hGsys, hdl, c_DEVICE_HDL_BITMAP);
        break;
      }
    }
  } // AnimImpression

// -------------------------------------------------------------------------------
// Service : animation d'un �l�ment graphique selon l'�tat d'une variable logique
static void AnimLogique2 (BYTE **pBuff, DWORD Position)
  {
  BOOL							bEtat = ExtraireBoolean (pBuff);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);

  if (pFenAnimGr->visible)
    {
		PX_ANM_R_LOG_VI pLogique2 = (PX_ANM_R_LOG_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_log_vi, Position);
		LONG			      nValeur = bEtat ? pLogique2->color_m_n: pLogique2->color_a_n;

    switch (pLogique2->tipe_anim)
      {
      case STYLE_COULEUR:
				AnimationCouleurElement (wCtxtPage, MODE_GOMME_ELEMENT, pLogique2->numero_gr, (G_COULEUR)nValeur);
				break;
      case STYLE_REMPLISSAGE:
				bdgr_style_remplissage_element (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pLogique2->numero_gr, (G_STYLE_REMPLISSAGE)nValeur);
				break;
      case STYLE_LIGNE:
				bdgr_style_ligne_element (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pLogique2->numero_gr, (G_STYLE_LIGNE)nValeur);
				break;
      }
    }
  } // AnimLogique2

// ----------------------------------------------------------------------------------
// Service : animation d'un �l�ment graphique selon l'�tat de deux variables logiques
static void AnimLogique4 (BYTE **pBuff, DWORD Position)
  {
  BOOL              bEtat1 = ExtraireBoolean (pBuff);
  BOOL              bEtat2 = ExtraireBoolean (pBuff);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
  LONG              nValeur;

  if (pFenAnimGr->visible)
    {
		PX_ANM_R_LOG_4_ETATS	pLogique4 = (PX_ANM_R_LOG_4_ETATS)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_log_4etats_vi, Position);
    if (bEtat1)
      {
      if (bEtat2)
        nValeur = pLogique4->color_h_h;
      else
        nValeur = pLogique4->color_h_b;
      }
    else
      {
      if (bEtat2)
        nValeur = pLogique4->color_b_h;
      else
        nValeur = pLogique4->color_b_b;
      }

    switch (pLogique4->tipe_anim)
      {
      case STYLE_COULEUR:
				AnimationCouleurElement (wCtxtPage, MODE_GOMME_ELEMENT, pLogique4->numero_gr, (G_COULEUR)nValeur);
				break;
      case STYLE_REMPLISSAGE:
				bdgr_style_remplissage_element (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pLogique4->numero_gr, (G_STYLE_REMPLISSAGE)nValeur);
				break;
      case STYLE_LIGNE:
				bdgr_style_ligne_element (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pLogique4->numero_gr, (G_STYLE_LIGNE)nValeur);
				break;
      }
    }
  }

// -----------------------------------------------------------------------------------
// Service : animation d'un bargraphe selon une variable num�rique (et ses min et max)
static void AnimBaragraphe (BYTE **pBuff, DWORD Position)
  {
  FLOAT             rValeur = ExtraireReel (pBuff);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
  PX_ANM_R_NUM	pBar;
  LONG                  NewVal;
  DWORD                wTypeBar;

  if (pFenAnimGr->visible)
    {
    pBar = (PX_ANM_R_NUM)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_num_vi, Position);
    if (rValeur > pBar->max)
      {
      rValeur = pBar->max; //(FLOAT)(LONG) ; //$$ arrondi n�cessaire ?
      }
    else
      {
      if (rValeur < pBar->min)
        {
        rValeur = pBar->min; //(FLOAT)(LONG)  //$$ arrondi n�cessaire ?
        }
      }

    NewVal = (LONG) ((rValeur * pBar->homothetie) + pBar->translation + 0.5);

    switch (pBar->modele)
      {
      case BAR_UP:
				wTypeBar = c_BAR_HAUT;
				break;
      case BAR_DOWN:
				wTypeBar = c_BAR_BAS;
				break;
			case BAR_LEFT:
				wTypeBar = c_BAR_GAUCHE;
				break;
			case BAR_RIGHT:
				wTypeBar = c_BAR_DROITE;
				break;
      }
    bdgr_maj_bargraphe (pFenAnimGr->hBdGr, pBar->numero_gr, wTypeBar, NewVal);
    }
  } // AnimBaragraphe

// --------------------------------------------------------------------------
// Si un style de remplissage est invalide, force le � G_STYLE_REMPLISSAGE_INDETERMINE et renvoie TRUE
static BOOL bLimiteStyleRemplissage (G_STYLE_REMPLISSAGE * pnStyleRemplissage)
	{
	BOOL bRes = (* pnStyleRemplissage < G_N_STYLE_REMPLISSAGE_MIN) || 
		(* pnStyleRemplissage > G_N_STYLE_REMPLISSAGE_MAX);

	if (bRes)
		* pnStyleRemplissage = G_STYLE_REMPLISSAGE_INDETERMINE;

	return bRes;
	}

// --------------------------------------------------------------------------
// Si un style de ligne est invalide, force le � G_STYLE_LIGNE_INDETERMINE et renvoie TRUE
static BOOL bLimiteStyleLigne (G_STYLE_LIGNE * pnStyleLigne)
	{
	BOOL bRes = (* pnStyleLigne < G_N_STYLE_LIGNE_MIN) || 
		(* pnStyleLigne > G_N_STYLE_LIGNE_MAX);

	if (bRes)
		* pnStyleLigne = G_STYLE_LIGNE_INDETERMINE;

	return bRes;
	}

// --------------------------------------------------------------------------
// Service : animation d'un objet graphique selon deux � trois variable num�rique
// d�placement en x et en y + �ventuelle animation en couleur ou style
static void AnimDeplacement (BYTE **pBuff, DWORD Position)
  {
  LONG                 iDx = (LONG) ExtraireReel (pBuff);
  LONG                 iDy = (LONG) ExtraireReel (pBuff);
	BOOL								bDeplacement = (iDx != 0) || (iDy != 0);
  LONG									nValeur;
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
  PX_ANM_R_NUM_DEPLACE	pDepNum = (PX_ANM_R_NUM_DEPLACE)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_num_dep_vi, Position);

  if (pFenAnimGr->visible)
    {
    switch (pDepNum->tipe_anim)
      {
      case STYLE_COULEUR:
        nValeur = (LONG) ExtraireReel (pBuff);
				// Change la valeur
				AnimationCouleurElement (wCtxtPage, MODE_GOMME_ELEMENT, pDepNum->numero_gr, (G_COULEUR)nValeur) ;
        break;

      case STYLE_REMPLISSAGE:
        nValeur = (LONG) ExtraireReel (pBuff);

				// Limite la valeur admissible
				bLimiteStyleRemplissage ((PG_STYLE_REMPLISSAGE)&nValeur);// $$Faire monter statut

				// Choisis la valeur ind�termin�e
				if (nValeur == G_STYLE_REMPLISSAGE_INDETERMINE)
					nValeur = G_STYLE_REMPLISSAGE_HACHURE; // $$ Remettre la valeur d'origine

				// Change la valeur
        bdgr_style_remplissage_element (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pDepNum->numero_gr, (G_STYLE_REMPLISSAGE)nValeur);
        break;

      case STYLE_LIGNE:
        nValeur = (LONG) ExtraireReel (pBuff);

				// Limite la valeur admissible
				bLimiteStyleLigne ((PG_STYLE_LIGNE)&nValeur);// $$Faire monter statut

				// Choisis la valeur ind�termin�e
				if (nValeur == G_STYLE_LIGNE_INDETERMINE)
					nValeur = G_STYLE_LIGNE_TIRETS_2_POINTS; // $$ Remettre la valeur d'origine

				// Change la valeur
        bdgr_style_ligne_element (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pDepNum->numero_gr, (G_STYLE_LIGNE)nValeur);
        break;

      default:
				// force le redessin (couleur constante)
        AnimationCouleurElement (wCtxtPage, MODE_GOMME_ELEMENT, pDepNum->numero_gr, G_COULEUR_INDETERMINEE);
        break;
      } // switch (pDepNum->tipe_anim)

		// D�placement ?
		if (TRUE)//bDeplacement) // $$ V�rifier
			{
			// D�placement et invalide la nouvelle position
			bdgr_offset_element (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pDepNum->numero_gr, pDepNum->pt0.x, pDepNum->pt0.y, pDepNum->pt1.x, pDepNum->pt1.y, iDx, iDy);
			}
    } // if (pFenAnimGr->visible)
  else
    {
    switch (pDepNum->tipe_anim)
      {
      case STYLE_COULEUR:
      case STYLE_REMPLISSAGE:
      case STYLE_LIGNE:
        nValeur = (LONG) ExtraireReel (pBuff);
        break;
      }
    }
  } // AnimDeplacement

// --------------------------------------------------------------------------
// Service : animation reflet d'une variable message
static void AnimMessage (BYTE **pBuff, DWORD Position)
  {
  char                szTexte [256];
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);

  ExtrairePsz (pBuff, szTexte);

  if (pFenAnimGr->visible)
    {
		PX_ANM_R_MES pMess = (PX_ANM_R_MES)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_mes_vi, Position);

    bTexteElementGRDansPage (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pMess->numero_gr, szTexte);
    }
  } // AnimMessage

// --------------------------------------------------------------------------
// Service : animation reflet d'une variable message et animation en couleur
static void AnimMessageClr (BYTE **pBuff, DWORD Position)
  {
  G_COULEUR           nCouleur = (G_COULEUR) (DWORD)ExtraireReel (pBuff);
  char                szTexte [256];
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);

  ExtrairePsz (pBuff, szTexte);

  if (pFenAnimGr->visible)
    {
		PX_ANM_R_MES pMess = (PX_ANM_R_MES)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_mes_vi, Position);

    //AnimationCouleurElement (wCtxtPage, MODE_VIRTUEL, pMess->numero_gr, nCouleur);
    AnimationCouleurElement (wCtxtPage, MODE_GOMME_ELEMENT, pMess->numero_gr, nCouleur);
    bTexteElementGRDansPage   (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pMess->numero_gr, szTexte);
    }
  } // AnimMessageClr

// --------------------------------------------------------------------------
// Service : animation reflet en texte d'une variable num�rique
static void AnimMesNum (BYTE **pBuff, DWORD Position)
  {
  FLOAT               rValeur = ExtraireReel (pBuff);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);

  if (pFenAnimGr->visible)
    {
		PX_ANM_R_NUM_ALPHA	pMesNum = (PX_ANM_R_NUM_ALPHA)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_num_alpha_vi, Position);
		char									szTexte [256];

    StrPrintFormat (szTexte, pMesNum->pszformat, rValeur);
    bTexteElementGRDansPage (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pMesNum->numero_gr, szTexte);
    }
  } // AnimMesNum

// --------------------------------------------------------------------------
// Service : animation reflet en texte d'une variable num�rique + animation couleur
static void AnimMesNumClr (BYTE **pBuff, DWORD Position)
  {
  G_COULEUR           nCouleur = (G_COULEUR)(LONG) ExtraireReel (pBuff);
  FLOAT               rValeur = ExtraireReel (pBuff);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);

  if (pFenAnimGr->visible)
    {
		PX_ANM_R_NUM_ALPHA	pMesNum = (PX_ANM_R_NUM_ALPHA)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_num_alpha_vi, Position);
		char                szTexte [256];

    StrPrintFormat (szTexte, pMesNum->pszformat, rValeur);
    //AnimationCouleurElement (wCtxtPage, MODE_VIRTUEL, pMesNum->numero_gr, nCouleur);
    AnimationCouleurElement (wCtxtPage, MODE_GOMME_ELEMENT, pMesNum->numero_gr, nCouleur);
    bTexteElementGRDansPage (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, pMesNum->numero_gr, szTexte);
    }
  } // AnimMesNumClr

// --------------------------------------------------------------------------
static void AnimCourbe (BYTE **pBuff, DWORD Position)
  {
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
  PX_ANM_COURBE_VI	pCourbe;
  DWORD wSousFonction;
  DWORD wNombre;
  FLOAT rValeur;
  LONG YValeur;

  pCourbe = (PX_ANM_COURBE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_vi, Position);
  do
    {
    wSousFonction = ExtraireWord (pBuff);
    switch (wSousFonction)
      {
      case ANM_GR_COURBES_FIN:
        if (pFenAnimGr->visible)
          {
          DessineNewMorceauCourbe (pFenAnimGr->hGsys, pCourbe);
          }
        break;

      case ANM_GR_COURBES_ERASE:
        if (pFenAnimGr->visible)
          {
					if ((pCourbe->nb_points != 0) || (pCourbe->old_nb_points != 0))
						{
						EffaceOldMorceauCourbe (pFenAnimGr->hGsys, pCourbe);
						FlushCourbe (pCourbe);
						}
          }
        break;

      case ANM_GR_COURBES_BUFFER:
        wNombre = ExtraireWord (pBuff);
        while (wNombre)
          {
          rValeur = ExtraireReel (pBuff);
          YValeur = YCourbe (rValeur, pCourbe);
          if (pFenAnimGr->visible)
            {
            AjoutPointCourbe (YValeur, pFenAnimGr->hGsys, pCourbe);
            }
          wNombre--;
          }
        break;

      case ANM_GR_COURBES_VALEUR:
        wNombre = ExtraireWord (pBuff);
        rValeur = ExtraireReel (pBuff);
        if (pFenAnimGr->visible)
          {
          YValeur = YCourbe (rValeur, pCourbe);
          while (wNombre)
            {
            AjoutPointCourbe (YValeur, pFenAnimGr->hGsys, pCourbe);
            wNombre--;
            }
          }
        break;

      default:
        break;
      }
    } while (wSousFonction != ANM_GR_COURBES_FIN);
  } // AnimCourbe

// --------------------------------------------------------------------------
static void AnimCourbeArchive (BYTE **pBuff, DWORD Position)
  {
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
  PX_ANM_COURBE_FIC_VI	pCourbe;
  DWORD                      wSousFonction;
  DWORD                      wNombre;
  FLOAT                       rValeur;
  LONG                       YValeur;
  FLOAT                       rHomothetie;
  FLOAT                       rTranslation;
  FLOAT                       PasX;
  LONG                       PasEch;
  FLOAT                       XValeur;
  LONG                       index;
  DWORD                      wExtr;

  pCourbe = (PX_ANM_COURBE_FIC_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi, Position);
  if (pFenAnimGr->visible)
    {
    //Effacer Courbe Archive
    EffaceCourbeArchive (pFenAnimGr->hGsys, pCourbe);

    do
      {
      wSousFonction = ExtraireWord (pBuff);
      switch (wSousFonction)
        {
        case ANM_GR_ARCHIVE_FIN:
          //dessiner courbe
          DessineCourbeArchive (pFenAnimGr->hGsys, pCourbe);
          break;

        case ANM_GR_ARCHIVE_ERASE:
          FlushCourbeArchive (pCourbe);
          break;

        case ANM_GR_ARCHIVE_COULEUR: // maj couleur
          pCourbe->couleur = (G_COULEUR)ExtraireWord (pBuff);
          break;

        case ANM_GR_ARCHIVE_STYLE: // maj style
          pCourbe->style = ExtraireWord (pBuff);
          break;

        case ANM_GR_ARCHIVE_MODELE: // maj modele
          pCourbe->modele = ExtraireWord (pBuff);
          break;

        case ANM_GR_ARCHIVE_MIN: // maj min
          pCourbe->min = ExtraireReel (pBuff);
          break;

        case ANM_GR_ARCHIVE_MAX: // maj max
          pCourbe->max = ExtraireReel (pBuff);
          break;

        case ANM_GR_ARCHIVE_BUFFER:
					// R�ception des points de la courbe � mettre en forme
          wNombre = ExtraireWord (pBuff);
          CalculParametresCbArchive (pCourbe, wNombre, &rHomothetie, &rTranslation, &PasX, &PasEch);
          if (pCourbe->paPoints)
            {
            MemLibere ((PVOID *)(&pCourbe->paPoints));
            }
          pCourbe->paPoints = (PPOINT)pMemAlloue ((size_t) (wNombre * sizeof (POINT)));
          FlushCourbeArchive (pCourbe);
          XValeur = (FLOAT)(HIWORD(pCourbe->x_min));
          while (wNombre)
            {
            index = 0;
            while ((index < PasEch) && wNombre)
              { // g�re le cas o� il y plus de points que de pixels
              rValeur = ExtraireReel (pBuff);
              wNombre--;
              index++;
              }
            YValeur = YCourbeArchive (rValeur, pCourbe, rHomothetie, rTranslation);
            AjoutPointCourbeArchive ((LONG)XValeur, YValeur, pCourbe);
            XValeur = XValeur + PasX;
            }
          // Bidon pour debug
          XValeur = (FLOAT)0;
          break;

        default:
          break;
        }
      } while (wSousFonction != ANM_GR_COURBES_FIN);
    } // if (pFenAnimGr->visible)
  else // extraction des parametres
    {
    do
      {
      wSousFonction = ExtraireWord (pBuff);
      switch (wSousFonction)
        {
        case ANM_GR_ARCHIVE_COULEUR: // maj couleur
        case ANM_GR_ARCHIVE_STYLE: // maj style
        case ANM_GR_ARCHIVE_MODELE: // maj modele
          wExtr = ExtraireWord (pBuff);
          break;

        case ANM_GR_ARCHIVE_MIN: // maj min
        case ANM_GR_ARCHIVE_MAX: // maj max
          rValeur = ExtraireReel (pBuff);
          break;

        case ANM_GR_ARCHIVE_BUFFER: // mettre les points dans les blocs
          wNombre = ExtraireWord (pBuff);
          while (wNombre)
            {
            rValeur = ExtraireReel (pBuff);
            wNombre--;
            }
          break;

        default:
          break;
        }
      } while (wSousFonction != ANM_GR_COURBES_FIN);
    }
  } // AnimCourbeArchive

// --------------------------------------------------------------------------
static void AnimForceNum (BYTE **pBuff, DWORD Position)
  {
  PX_ANM_E_NUM_VI	pt_e_num = (PX_ANM_E_NUM_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_num_vi,Position);
  FLOAT           rValeur = ExtraireReel (pBuff);

	if (pt_e_num->hdl_inline)
		{
		pt_e_num->val_init = rValeur;
		VerifWarning(bInLineNumSet (pt_e_num->hdl_inline, rValeur));
		}
  }

// --------------------------------------------------------------------------
static void AnimForceMes (BYTE **pBuff, DWORD Position)
  {
  PX_ANM_E_MES_VI	pt_e_mes = (PX_ANM_E_MES_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_mes_vi,Position);
  char            pszValeur [256];

  ExtrairePsz (pBuff, pszValeur);

  StrCopy(pt_e_mes->val_init,pszValeur);
		{
		if (pt_e_mes->hdl_inline)
			::SetWindowText (pt_e_mes->hdl_inline, pszValeur);
		}
  }

// --------------------------------------------------------------------------
static void AnimControlText (BYTE **pBuff, DWORD n_elt)
  {
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
  char              pszValeur [256];

  ExtrairePsz (pBuff, pszValeur);
  bTexteElementGRDansPage   (pFenAnimGr->hBdGr, MODE_GOMME_ELEMENT, n_elt, pszValeur);
  }

// --------------------------------------------------------------------------
// service SERVICE_ANM_GR_ANM_FORCAGE_CHECK
static void AnimCheck (BYTE **pBuff, DWORD n_elt)
  {
	BOOL								bEtat = ExtraireBoolean (pBuff);
  PX_ANM_FENETRE_VI pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
	PPAGE_GR						pPage = ppageHBDGR (pFenAnimGr->hBdGr);

	if ((pPage) && (n_elt > 0) && (n_elt <= pPage->NbElements))
		{
		DWORD							wPositionElement = pPage->PremierElement + n_elt - 1;
		PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

		Verif (pElement->nAction == G_ACTION_CONT_CHECK);

		// mettre � jour le controle n_elt de pFenAnimGr->hBdGr avec bEtat
		if (pElement->hwndControle)
			{
			// enregistre la nouvelle valeur courante :
			PENV_ANM_GR		pEnvAnmGr = (PENV_ANM_GR)pGetEnv(::GetParent (pElement->hwndControle));
			DWORD					dwId = ::GetDlgCtrlID (pElement->hwndControle);
			PX_ANM_E_VI		pt_e_vi = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pEnvAnmGr->padwNEnrEntree[dwId - PREMIER_ID_VALIDE]);
			PX_ANM_E_CONTROL_LOG_VI pt_e_control_log;

			// Verifie le type d'animation
			Verif (pt_e_vi->n_bloc == bx_anm_e_control_log); // Contr�les boutons � impulsion et check box

			// bascule l'�tat et notifie
			pt_e_control_log = (PX_ANM_E_CONTROL_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
			pt_e_control_log->bValeurCourante = bEtat;

			// et affiche le nouvel �tat
			::SendMessage (pElement->hwndControle, BM_SETCHECK, (bEtat ? BST_CHECKED: BST_UNCHECKED), 0);
			}
		}
  } // AnimCheck

// --------------------------------------------------------------------------
// service SERVICE_ANM_GR_ANM_FORCAGE_BOUTON_PULSE
static void AnimBoutonPulse (BYTE **pBuff, DWORD n_elt)
  {
	BOOL bEtat = ExtraireBoolean (pBuff);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
	PPAGE_GR	pPage = ppageHBDGR (pFenAnimGr->hBdGr);

	if ((pPage) && (n_elt > 0) && (n_elt <= pPage->NbElements))
		{
		DWORD							wPositionElement = pPage->PremierElement + n_elt - 1;
		PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

		Verif (pElement->nAction == G_ACTION_CONT_BOUTON);

		// mettre � jour le control n_elt de pFenAnimGr->hBdGr avec bEtat
		if (pElement->hwndControle)
			{
			// enregistre la nouvelle valeur courante :
			PENV_ANM_GR		pEnvAnmGr = (PENV_ANM_GR)pGetEnv(::GetParent (pElement->hwndControle));
			DWORD					dwId = ::GetDlgCtrlID (pElement->hwndControle);
			PX_ANM_E_VI		pt_e_vi = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pEnvAnmGr->padwNEnrEntree[dwId - PREMIER_ID_VALIDE]);
			PX_ANM_E_CONTROL_LOG_VI pt_e_control_log;

			// Verifie le type d'animation
			Verif (pt_e_vi->n_bloc == bx_anm_e_control_log); // Contr�les boutons � impulsion et check box

			// bascule l'�tat et notifie
			pt_e_control_log = (PX_ANM_E_CONTROL_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
			pt_e_control_log->bValeurCourante = bEtat;

			// pas de nouvel �tat � afficher
			}
		}
  } // AnimBoutonPulse

// --------------------------------------------------------------------------
// Coche un radio bouton.
// n_elt est le premier bouton radio du groupe. 
// rreel est le num�ro du bouton � cocher (de 1 � n)
static void AnimRadio (BYTE **pBuff, DWORD n_elt)
  {
	FLOAT     rReel = ExtraireReel (pBuff);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
	PPAGE_GR	pPage = ppageHBDGR (pFenAnimGr->hBdGr);

	if (pPage)
		{
		if ((n_elt > 0) && (n_elt <= pPage->NbElements))
			{
			DWORD           wPositionElement = pPage->PremierElement + n_elt - 1;
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

			switch (pElement->nAction)
				{
				case G_ACTION_CONT_RADIO:
					// mettre � jour le control radio n_elt de pFenAnimGr->hBdGr avec rReel
					if (pElement->hwndControle)
						{
						bCheckRadioBouton	(pFenAnimGr->hwndVi, pElement->hwndControle, (LONG) rReel);
						}
					break;
				default:
					VerifWarningExit;
					break;
				}
			}
		}
  }

// --------------------------------------------------------------------------
static void AnimControlListeCombobox (BYTE **pBuff, DWORD n_elt)
  {
	PELEMENT_PAGE_GR	pElement = pElementDansPage (wCtxtPage, n_elt);
	DWORD							dwNbrMessageListe;
	DWORD							dwIndiceSelectionListe;
	DWORD							dwIndexMessage;
	HWND							hwnd = NULL;
	G_ACTION					nAction = G_ACTION_NOP;

	// r�cup�re si possible le handle de la combo
	if (pElement)
		{
		hwnd = pElement->hwndControle;
		nAction = pElement->nAction;

		// Vide la combo ou la liste
		if (hwnd)
			{
			switch (nAction)
				{
				case G_ACTION_CONT_COMBO:
					::SendMessage (hwnd, CB_RESETCONTENT, 0, 0);
					break;
				case G_ACTION_CONT_LIST:
					::SendMessage (hwnd, LB_RESETCONTENT, 0, 0);
					break;
				}
			}
		}

  // extrait les donn�es et met � jour la combo ou la liste
	dwNbrMessageListe = (DWORD) ExtraireReel (pBuff);
	for (dwIndexMessage = 1; (dwIndexMessage <= dwNbrMessageListe); dwIndexMessage++)
		{
		char	szTemp [256];

		ExtrairePsz (pBuff, szTemp);

		// remplis la combo ou la liste
		if (hwnd)
			{
			switch (nAction)
				{
				case G_ACTION_CONT_COMBO:
					::SendMessage (hwnd, CB_ADDSTRING, 0, (LPARAM)szTemp);
					break;
				case G_ACTION_CONT_LIST:
					::SendMessage (hwnd, LB_ADDSTRING, 0, (LPARAM)szTemp);
					break;
				}
			}
		}

	dwIndiceSelectionListe = (DWORD) ExtraireReel (pBuff);

	// S�lectionne le bon item
	if (hwnd)
		{
		switch (nAction)
			{
			case G_ACTION_CONT_COMBO:
				{
				char	szTemp [256];
				HWND  hwndParent = ::GetParent (hwnd);
				PENV_ANM_GR		pEnvAnmGr = (PENV_ANM_GR)pGetEnv(hwndParent);
				DWORD dwId = (DWORD)::GetDlgCtrlID(hwnd);
				PX_ANM_E_VI	pt_e_vi = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pEnvAnmGr->padwNEnrEntree[dwId - PREMIER_ID_VALIDE]);
				PX_ANM_E_COMBOBOX pt_e_combobox = (PX_ANM_E_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);

				::SendMessage (hwnd, CB_SETCURSEL, (WPARAM)(dwIndiceSelectionListe-1), 0);
				// Et notification du texte s�lectionn�
				::SendMessage(hwnd, CB_GETLBTEXT, (dwIndiceSelectionListe-1),(LPARAM) (LPCSTR) szTemp);
				NotificationPsz (pt_e_combobox->pos_ret_saisie, szTemp);
				}
				break;
			case G_ACTION_CONT_LIST:
				::SendMessage (hwnd, LB_SETCURSEL, (WPARAM)(dwIndiceSelectionListe-1), 0);
				break;
			}
		}
	} // AnimControlListeCombobox

// --------------------------------------------------------------------------
static void AnimLogTransparent (BYTE **pBuff, DWORD Position)
  {
  BOOL            bValeur = ExtraireBoolean (pBuff);
	PX_ANM_E_LOG_VI pt_e_log = (PX_ANM_E_LOG_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_log_vi, Position);

	pt_e_log->bValeurCourante = bValeur;
	
	// pas d'affichage de la valeur courante
  }


// -----------------------------------------------------------------------
static void calcule_taille_fen_relative (HWND hwnd, LONG *pcx, LONG *pcy)
  {
  PENV_ANM_GR pEnvAnmGr = (PENV_ANM_GR)pGetEnv (hwnd);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);

  if (pFenAnimGr->mode_aff == MODE_ZOOM)
    {
    *pcx = (LONG)(pFenAnimGr->cx_client_ref / pFenAnimGr->coeff_zoom);
    *pcy = (LONG)(pFenAnimGr->cy_client_ref / pFenAnimGr->coeff_zoom);
    }
  else
    {
		RECT rect;

    ::GetClientRect (hwnd, &rect);
    *pcx = rect.right;
    *pcy = rect.bottom;
    Spc_CvtSizeEnVirtuel (hSpcFen, pcx,  pcy);
    }
  }

// -----------------------------------------------------------------------
static void maj_thumbsize_scroll (HWND hwnd)
  {
/* $$ porter
  HWND hwndVi;
  HWND hwndScroll;
  LONG lcx;
  LONG lcy;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  hwndVi = ::GetParent (hwnd);
  hwndScroll = ::GetDlgItem (hwndVi, FID_HORZSCROLL);
  if (hwndScroll != NULL)
    {
    ::SendMessage (hwndScroll, SBM_SETTHUMBSIZE, MAKEWPARAM ((SHORT) lcx, c_G_MAX_X - c_G_MIN_X), 0);
    }
  hwndScroll = ::GetDlgItem (hwndVi, FID_VERTSCROLL);
  if (hwndScroll != NULL)
    {
    ::SendMessage (hwndScroll, SBM_SETTHUMBSIZE, MAKEWPARAM ((SHORT) lcy, c_G_MAX_Y - c_G_MIN_Y), 0);
    }
*/
  }

// -----------------------------------------------------------------------
static void maj_espace_scroll_bar_h (HWND hwnd, HGSYS hGsys)
  {
/* $$ porter
  HWND hwndScroll;
  HWND hwndVi;
  LONG lcx;
  LONG lcy;
  LONG xOrigine;
  LONG yOrigine;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  g_get_origin (hGsys, &xOrigine, &yOrigine);
  hwndVi = ::GetParent (hwnd);
  hwndScroll = ::GetDlgItem (hwndVi, FID_HORZSCROLL);
  if (hwndScroll != NULL)
    {
    ::SendMessage (hwndScroll, SBM_SETSCROLLBAR, xOrigine, MAKELPARAM (c_G_MIN_X, c_G_MAX_X - lcx));
    }
*/  }

// -----------------------------------------------------------------------
static void maj_espace_scroll_bar_v (HWND hwnd, HGSYS hGsys)
  {
/* $$ porter
  HWND hwndScroll;
  HWND hwndVi;
  LONG lcx;
  LONG lcy;
  LONG xOrigine;
  LONG yOrigine;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  g_get_origin (hGsys, &xOrigine, &yOrigine);
  hwndVi = ::GetParent (hwnd);
  hwndScroll = ::GetDlgItem (hwndVi, FID_VERTSCROLL);
  if (hwndScroll != NULL)
    {
    ::SendMessage (hwndScroll, SBM_SETSCROLLBAR, (c_G_MAX_Y-lcy) - yOrigine, MAKELPARAM (c_G_MIN_Y, (c_G_MAX_Y-lcy)));
    }
*/  }

// -----------------------------------------------------------------------
static void bornage (LONG *val, LONG petit, LONG grand)
  {
  if ((*val) < petit)
    (*val) = petit;
  else
    {
    if ((*val) > grand)
      (*val) = grand;
    }
  }

// -----------------------------------------------------------------------
static void ajuste_origine_fen_x (HWND hwnd, HGSYS hGsys, LONG *dx0)
  {
/* $$ porter
  LONG   sMin;
  LONG   sMax;
  HWND  hwndScroll;
  HWND  hwndVi;
  LONG   lcx;
  LONG   lcy;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  sMin = c_G_MIN_X;
  sMax = c_G_MAX_X - lcx;
  bornage (dx0, sMin, sMax);
  hwndVi = ::GetParent (hwnd);
  hwndScroll = ::GetDlgItem (hwndVi, FID_HORZSCROLL);
  if (hwndScroll != NULL)
    {
    ::SendMessage (hwndScroll, SBM_SETPOS, *dx0, 0);
    }
  g_set_x_origine (hGsys, *dx0);
*/  }

// -----------------------------------------------------------------------
static void ajuste_origine_fen_y (HWND hwnd, HGSYS hGsys, LONG *dy0)
  {
/* $$ porter
  LONG   sMin;
  LONG   sMax;
  HWND  hwndScroll;
  LRESULT mresQuery;
  HWND  hwndVi;
  LONG   lcx;
  LONG   lcy;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  sMin = c_G_MIN_Y;
  sMax = c_G_MAX_Y - lcy;
  bornage (dy0, sMin, sMax);
  hwndVi = ::GetParent (hwnd);
  hwndScroll = ::GetDlgItem (hwndVi, FID_VERTSCROLL);
  if (hwndScroll != NULL)
    {
    mresQuery = ::SendMessage (hwndScroll, SBM_QUERYRANGE, 0, 0);
    sMax = (LONG)HIWORD (mresQuery);
    ::SendMessage (hwndScroll, SBM_SETPOS, sMax-(*dy0), 0);
    }
  g_set_y_origine (hGsys, *dy0);
*/  }

// -----------------------------------------------------------------------
static void param_fenetre (PX_ANM_FENETRE_VI	pFenAnimGr, LONG *CentrexDev, LONG *CentreyDev,
                           LONG *NewTaillexDev, LONG *NewTailleyDev)
  {
  //$$SWP swp;
  RECT rcl;
	RECT rect;

  
	::GetClientRect (pFenAnimGr->hwndVi, &rect); //WinQueryWindowPos (pFenAnimGr->hwndVi, &swp);
  ::GetWindowRect (pFenAnimGr->hwndVi, &rcl);

  (*CentrexDev) = (rcl.right + rcl.left) / 2;
  (*CentreyDev) = (rcl.top + rcl.bottom) / 2;
  (*NewTaillexDev) = (LONG) ((FLOAT)rect.right * pFenAnimGr->coeff_zoom);
  (*NewTailleyDev) = (LONG) ((FLOAT)rect.bottom * pFenAnimGr->coeff_zoom);
  if ((*NewTaillexDev) < 1)
    {
    (*NewTaillexDev) = 1;
    }
  if ((*NewTailleyDev) < 1)
    {
    (*NewTailleyDev) = 1;
    }
  }

// --------------------------------------------------------------------------
// Service : ouvre une fen�tre
static void AnimFenAffiche (BYTE **pBuff, DWORD Position)
  {
  BOOL		bOk = FALSE;
  int     fs_dim = 0;
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, wCtxtPage);
	// r�cup�re les dimensions de la fen�tre
  LONG     xFen = (LONG)  ExtraireReel (pBuff);
  LONG     yFen = (LONG)  ExtraireReel (pBuff);
  LONG     cxFen = (LONG)  ExtraireReel (pBuff);
  LONG     cyFen = (LONG)  ExtraireReel (pBuff);
  DWORD   wNewDim = (DWORD) ExtraireReel (pBuff);
  LONG     xOrigine;
  LONG     yOrigine;

	// conversion �tat d�sir� de la fen�tre
  switch (wNewDim)
    {
    case DIM_ICONE:
			fs_dim = SW_MINIMIZE;
			break;
    case DIM_MAX:
			fs_dim = SW_SHOWMAXIMIZED;
			break;
    case DIM_NORMALE:
			fs_dim = SW_SHOWNORMAL;
			break;
    }

  pFenAnimGr->pas_forcage = FALSE;
  if (!pFenAnimGr->visible)     // Creation fenetre
    {
	  DWORD   dwStyle = WS_POPUP; //Styles Fenetre
		DWORD		dwStyleEx = WS_EX_WINDOWEDGE|WS_EX_CONTROLPARENT;
		DWORD   facteur_zoom;
		DWORD   mode_deform;
		BOOL    sizeborder;
		BOOL    titre;
		char    texte_titre[c_NB_CAR_TITRE_PAGE_GR+1];
		DWORD   grille_x;
		DWORD   grille_y;
		BOOL    bFenModale;
		BOOL    bFenDialog;
		BOOL    un_sysmenu;
		BOOL    un_minimize;
		BOOL    un_maximize;
		BOOL    scroll_bar_h;
		BOOL    scroll_bar_v;
	  DWORD   mode_aff;

    // Pas de Notifications au moment de la cr�ation & de l'initialisation des fen�tres (==>FORCAGE).
    pFenAnimGr->pas_forcage = FALSE;
    pFenAnimGr->dimension = wNewDim;

		// Cr�e ou ouvre un objet page de synoptique
    pFenAnimGr->hBdGr = hbdgr_ouvrir_page (wCtxtPage, FALSE, FALSE);
    bdgr_lire_styles_page  (pFenAnimGr->hBdGr, &bFenModale, &bFenDialog, &mode_aff, &facteur_zoom, &mode_deform,
                            &sizeborder, &titre, texte_titre, &un_sysmenu, &un_minimize,
                            &un_maximize, &scroll_bar_h, &scroll_bar_v,
                            &grille_x, &grille_y);
    //bdgr_lire_coord_page  (pFenAnimGr->hBdGr, &xFen, &yFen, &cxFen, &cyFen);

		// conversion en pixel des coordonn�es de la fen�tre
    Spc_CvtPtEnPixel (hSpcFen, &xFen,  &yFen);
    Spc_CvtSizeEnPixel (hSpcFen, &cxFen,  &cyFen);
    bdgr_lire_origine_page  (pFenAnimGr->hBdGr, &xOrigine, &yOrigine);

		pFenAnimGr->mode_aff = mode_aff;
    if (mode_aff == MODE_ZOOM)
      {
      pFenAnimGr->coeff_zoom = (FLOAT)facteur_zoom / (FLOAT)100.0;
      pFenAnimGr->facteur_zoom = facteur_zoom;
      }

		// synth�se du style de la fen�tre � cr�er
    if (sizeborder)
      dwStyle |= WS_THICKFRAME;
		else
			dwStyle |= WS_DLGFRAME;
    if (titre)
      dwStyle |= WS_CAPTION;
    if (un_minimize)
      dwStyle |= WS_MINIMIZEBOX;
    if (un_maximize)
      dwStyle |= WS_MAXIMIZEBOX;

	// Ajoute un menu syst�me si n�cessaire
		if (un_sysmenu || un_minimize || un_maximize)
			dwStyle |= WS_SYSMENU;

		// Rajoute une barre de titre si n�cessaire
		if (titre || un_minimize || un_maximize || un_sysmenu)
			dwStyle |= WS_CAPTION;

		// La fen�tre est toujours rajout�e dans la barre de t�che
		dwStyleEx |= WS_EX_APPWINDOW;


		if (scroll_bar_h)
				dwStyle |= WS_HSCROLL;
    if (scroll_bar_v)
      dwStyle |= WS_VSCROLL;

		// cr�ation de la fen�tre
    pFenAnimGr->hwndVi = NULL;
		CreateWindowEx (
			dwStyleEx,
			szClasseAnmGr,	// class name
			texte_titre,	// window name
			dwStyle,			//  window style
			xFen,	// x // $$ rendre �ventuellement invisible, � la bonne taille etc...
			yFen,	// y
			cxFen,	// cx
			cyFen,	// cy
			hwndParentPage,	// handle parent window
			NULL,	// handle to menu or child-window identifier
			Appli.hinst,	// handle to application instance
			&wCtxtPage);	// pointer to window-creation data

    bOk = (pFenAnimGr->hwndVi != NULL);
    if (bOk)
      {
			// Affiche la fen�tre dans son �tat
			::ShowWindow (pFenAnimGr->hwndVi, fs_dim);

      if (un_sysmenu)
        {
				HMENU	hwndsysmenu = ::GetSystemMenu (pFenAnimGr->hwndVi, FALSE);

				MenuAppendSeparateur (hwndsysmenu);
				MenuAppendMI (hwndsysmenu, c_inf_grossir, CMD_SYSMENU_GROSSIR);
				MenuGriseItem (hwndsysmenu, CMD_SYSMENU_GROSSIR, mode_aff != MODE_ZOOM);

				MenuAppendMI (hwndsysmenu, c_inf_maigrir, CMD_SYSMENU_MAIGRIR);
				MenuGriseItem (hwndsysmenu, CMD_SYSMENU_MAIGRIR, mode_aff != MODE_ZOOM);

				MenuAppendMI (hwndsysmenu, c_inf_facteur_zoom, CMD_SYSMENU_FACTEUR);
				MenuGriseItem (hwndsysmenu, CMD_SYSMENU_FACTEUR, mode_aff != MODE_ZOOM);
        }
			}

    // Notifications si Evenement PM (==>PAS de FORCAGE par PROGRAMME).
    pFenAnimGr->pas_forcage = TRUE;
    }
  } // AnimFenAffiche


// -----------------------------------------------------------------------
static BOOL origine_x_modifie (PENV_ANM_GR pEnvAnmGr, LPARAM mp2, LONG *dx0)
  {
  BOOL bScroller;
/* $$ � porter
  RECT rcl;
  SWP swp;
  PX_ANM_FENETRE_VI	pFenAnimGr;
  LONG dx;
  LONG pas_scroll;

  bScroller = FALSE;
  pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
  WinQueryWindowPos (pFenAnimGr->hwndVi, &swp);
  pas_scroll = g_convert_dx_dev_to_page (pFenAnimGr->hGsys, swp.cx) / FACTEUR_SCROLL_X;
  if (pas_scroll <= 0)
    {
    pas_scroll = g_convert_dx_dev_to_page (pFenAnimGr->hGsys, swp.cx) / 2; //1/2 fenetre
    }
  switch (HIWORD (mp2))
    {
    case SB_LINELEFT:
         //Action sur Fl�che Gauche
         (*dx0) = g_add_dx_page (pFenAnimGr->hGsys, (*dx0), -pas_scroll);
         bScroller = TRUE;
         break;
    case SB_LINERIGHT:
         //Action sur Fl�che Droite
         (*dx0) = g_add_dx_page (pFenAnimGr->hGsys, (*dx0), pas_scroll);
         bScroller = TRUE;
         break;
    case SB_PAGELEFT:
         //Action sur Page Gauche
         ::GetWindowRect (pFenAnimGr->hwndVi, &rcl);
         dx = g_convert_dx_dev_to_page (pFenAnimGr->hGsys, -(rcl.right - rcl.left));
         (*dx0) = g_add_dx_page (pFenAnimGr->hGsys, (*dx0), dx);
         bScroller = TRUE;
         break;
    case SB_PAGERIGHT:
         //Action sur Page Droite
         ::GetWindowRect (pFenAnimGr->hwndVi, &rcl);
         dx = g_convert_dx_dev_to_page (pFenAnimGr->hGsys, rcl.right - rcl.left);
         (*dx0) = g_add_dx_page (pFenAnimGr->hGsys, (*dx0), dx);
         bScroller = TRUE;
         break;
    case SB_THUMBPOSITION:
         (*dx0) = LOWORD (mp2);
         bScroller = TRUE;
         break;
    case SB_THUMBTRACK:
         //Ne rien faire (scroll dynamique... (dessin trop lent))
         break;
    case SB_ENDSCROLL:
         //Ne rien faire (la souris quitte le scroll bar)
         break;
    default:
         break;
    }
		*/
  return bScroller;
  }

// -----------------------------------------------------------------------
static BOOL origine_y_modifie (PENV_ANM_GR pEnvAnmGr, LPARAM mp2, LONG *dy0)
  {
  BOOL bScroller;
/* $$ A porter
  SWP swp;
  RECT rcl;
  PX_ANM_FENETRE_VI	pFenAnimGr;
  LONG dy;
  LONG   sMax;
  LONG pas_scroll;
  HWND  hwndScroll;
  LRESULT mresQuery;
  HWND  hwndVi;

  bScroller = FALSE;
  pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
  WinQueryWindowPos (pFenAnimGr->hwndVi, &swp);
  pas_scroll = g_convert_dy_dev_to_page (pFenAnimGr->hGsys, swp.cy) / FACTEUR_SCROLL_Y;
  if (pas_scroll <= 0)
    {
    pas_scroll = g_convert_dy_dev_to_page (pFenAnimGr->hGsys, swp.cy) / 2; //1/2 fenetre
    }
  switch (HIWORD (mp2))
    {
    case SB_LINEUP:
         //Action sur Fl�che Haut
         (*dy0) = g_add_dy_page (pFenAnimGr->hGsys, (*dy0), pas_scroll);
         bScroller = TRUE;
         break;
    case SB_LINEDOWN:
         //Action sur Fl�che Bas
         (*dy0) = g_add_dy_page (pFenAnimGr->hGsys, (*dy0), -pas_scroll);
         bScroller = TRUE;
         break;
    case SB_PAGEUP:
         //Action sur Page Haut
         ::GetWindowRect (pFenAnimGr->hwndVi, &rcl);
         dy = g_convert_dy_dev_to_page (pFenAnimGr->hGsys, (rcl.top - rcl.bottom));
         (*dy0) = g_add_dy_page (pFenAnimGr->hGsys, (*dy0), dy);
         bScroller = TRUE;
         break;
    case SB_PAGEDOWN:
         //Action sur Page Bas
         ::GetWindowRect (pFenAnimGr->hwndVi, &rcl);
         dy = g_convert_dy_dev_to_page (pFenAnimGr->hGsys, -(rcl.top - rcl.bottom));
         (*dy0) = g_add_dy_page (pFenAnimGr->hGsys, (*dy0), dy);
         bScroller = TRUE;
         break;
    case SB_THUMBPOSITION:
         hwndVi = ::GetParent (pFenAnimGr->hwndVi);
         hwndScroll = ::GetDlgItem (hwndVi, FID_VERTSCROLL);
         mresQuery = ::SendMessage (hwndScroll, SBM_QUERYRANGE, 0, 0);
         sMax = (LONG)HIWORD (mresQuery);
         (*dy0) = sMax - (LONG)LOWORD (mp2);
         bScroller = TRUE;
         break;
    case SB_THUMBTRACK:
         //Ne rien faire (scroll dynamique... (dessin trop lent))
         break;
    case SB_ENDSCROLL:
         //Ne rien faire (la souris quitte le scroll bar)
         break;
    default:
         break;
    }
		*/
  return bScroller;
  }

// -----------------------------------------------------------------------
static void ajuste_espace_fen (HWND hwnd, DWORD id_fen)
  {

  if (existe_repere (bx_anm_fenetre_vi) && (nb_enregistrements (szVERIFSource, __LINE__, bx_anm_fenetre_vi)!= 0))
    {
		PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, id_fen);

    if (pFenAnimGr->mode_aff == MODE_ZOOM)
      {
		  //SWP swp;
			RECT rect;
			LONG lx1;
			LONG ly1;
			LONG new_cx_client;
			LONG new_cy_client;

      ::GetClientRect (hwnd, &rect); // $$WinQueryWindowPos (hwnd, &swp);
      new_cx_client = rect.right;
      new_cy_client = rect.bottom;
      if (pFenAnimGr->cx_client_ref == 0)  // creation fenetre
        {
        lx1 = (LONG)new_cx_client;
        ly1 = (LONG)new_cy_client;
        Spc_CvtSizeEnVirtuel (hSpcFen, &lx1, &ly1);
        pFenAnimGr->cx_client_ref = (LONG)lx1;
        pFenAnimGr->cy_client_ref = (LONG)ly1;
        }
      lx1 = (LONG) ((FLOAT)new_cx_client * pFenAnimGr->coeff_zoom);
      ly1 = (LONG) ((FLOAT)new_cy_client * pFenAnimGr->coeff_zoom);
      g_set_page_units (pFenAnimGr->hGsys, pFenAnimGr->cx_client_ref, pFenAnimGr->cy_client_ref, lx1, ly1);
      }
    else
      {
      g_set_page_units (pFenAnimGr->hGsys, c_G_NB_X, c_G_NB_Y, Appli.sizEcran.cx, Appli.sizEcran.cy);
      }
    }
  }

//--------------------------------------------------------------------
// WM_SYSCOMMAND de ExecutionGr
static BOOL bOnSysCommand (HWND hwnd, WPARAM mp1, LPARAM mp2)
	{
	BOOL	bTraite = TRUE;
  PARAM_DLG_SAISIE_NUM retour_saisie_num;
  PENV_ANM_GR pEnvAnmGr = (PENV_ANM_GR)pGetEnv(hwnd);
  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
  LONG NewTaillexDev;
  LONG NewTailleyDev;
  LONG Originex;
  LONG Originey;
  LONG CentrexDev;
  LONG CentreyDev;
  LONG OldCentrex;
  LONG OldCentrey;
  LONG NewCentrex;
  LONG NewCentrey;
  LONG dx;
  LONG dy;

	switch (mp1 & 0xFFF0)
		{
		case CMD_SYSMENU_GROSSIR:
			if (pFenAnimGr->facteur_zoom < 100)
				{
				pFenAnimGr->coeff_zoom = pFenAnimGr->coeff_zoom / (FLOAT)pFenAnimGr->facteur_zoom * (FLOAT)100.0;
				}
			else
				{
				pFenAnimGr->coeff_zoom = pFenAnimGr->coeff_zoom * (FLOAT)pFenAnimGr->facteur_zoom / (FLOAT)100.0;
				}
			param_fenetre (pFenAnimGr, &CentrexDev, &CentreyDev, &NewTaillexDev, &NewTailleyDev);

			OldCentrex = g_convert_x_dev_to_page (pFenAnimGr->hGsys, CentrexDev);
			OldCentrey = g_convert_y_dev_to_page (pFenAnimGr->hGsys, CentreyDev);

			g_set_page_units (pFenAnimGr->hGsys, pFenAnimGr->cx_client_ref, pFenAnimGr->cy_client_ref, NewTaillexDev, NewTailleyDev);

			NewCentrex = g_convert_x_dev_to_page (pFenAnimGr->hGsys, CentrexDev);
			NewCentrey = g_convert_y_dev_to_page (pFenAnimGr->hGsys, CentreyDev);
			dx = OldCentrex - NewCentrex;
			dy = OldCentrey - NewCentrey;
			g_get_origin (pFenAnimGr->hGsys, &Originex, &Originey);
			g_add_deltas_page (pFenAnimGr->hGsys, &Originex, &Originey, dx, dy);
			ajuste_origine_fen_x (pFenAnimGr->hwndVi, pFenAnimGr->hGsys, &Originex);
			ajuste_origine_fen_y (pFenAnimGr->hwndVi, pFenAnimGr->hGsys, &Originey);
			ajuste_entrees_page (pEnvAnmGr->wId);

			::InvalidateRect (hwnd, NULL, TRUE);
			maj_espace_scroll_bar_h (pFenAnimGr->hwndVi, pFenAnimGr->hGsys);
			maj_espace_scroll_bar_v (pFenAnimGr->hwndVi, pFenAnimGr->hGsys);
			maj_thumbsize_scroll (pFenAnimGr->hwndVi);
			break;

		case CMD_SYSMENU_MAIGRIR:
			if (pFenAnimGr->facteur_zoom < 100)
				{
				pFenAnimGr->coeff_zoom = pFenAnimGr->coeff_zoom * (FLOAT)pFenAnimGr->facteur_zoom / (FLOAT)100.0;
				}
			else
				{
				pFenAnimGr->coeff_zoom = pFenAnimGr->coeff_zoom / (FLOAT)pFenAnimGr->facteur_zoom * (FLOAT)100.0;
				}
			param_fenetre (pFenAnimGr, &CentrexDev, &CentreyDev, &NewTaillexDev, &NewTailleyDev);

			OldCentrex = g_convert_x_dev_to_page (pFenAnimGr->hGsys, CentrexDev);
			OldCentrey = g_convert_y_dev_to_page (pFenAnimGr->hGsys, CentreyDev);

			g_set_page_units (pFenAnimGr->hGsys, pFenAnimGr->cx_client_ref, pFenAnimGr->cy_client_ref, NewTaillexDev, NewTailleyDev);

			NewCentrex = g_convert_x_dev_to_page (pFenAnimGr->hGsys, CentrexDev);
			NewCentrey = g_convert_y_dev_to_page (pFenAnimGr->hGsys, CentreyDev);
			dx = OldCentrex - NewCentrex;
			dy = OldCentrey - NewCentrey;
			g_get_origin (pFenAnimGr->hGsys, &Originex, &Originey);
			g_add_deltas_page (pFenAnimGr->hGsys, &Originex, &Originey, dx, dy);
			ajuste_origine_fen_x (pFenAnimGr->hwndVi, pFenAnimGr->hGsys, &Originex);
			ajuste_origine_fen_y (pFenAnimGr->hwndVi, pFenAnimGr->hGsys, &Originey);
			ajuste_entrees_page (pEnvAnmGr->wId);

			::InvalidateRect (hwnd, NULL, TRUE);
			maj_thumbsize_scroll (pFenAnimGr->hwndVi);
			maj_espace_scroll_bar_h (pFenAnimGr->hwndVi, pFenAnimGr->hGsys);
			maj_espace_scroll_bar_v (pFenAnimGr->hwndVi, pFenAnimGr->hGsys);
			break;

		case CMD_SYSMENU_FACTEUR:
			StrSetNull (retour_saisie_num.titre);
			StrCopy (retour_saisie_num.titre, titre_saisie_facteur);
			retour_saisie_num.saisie = (LONG)(pFenAnimGr->facteur_zoom);
			if (dlg_saisie_num (hwnd, &retour_saisie_num))
				{
				if (retour_saisie_num.saisie > 0)
					{
					pFenAnimGr->facteur_zoom = (DWORD)retour_saisie_num.saisie;
					pFenAnimGr->coeff_zoom = (FLOAT)pFenAnimGr->facteur_zoom / (FLOAT)100.0;
					param_fenetre (pFenAnimGr, &CentrexDev, &CentreyDev, &NewTaillexDev, &NewTailleyDev);

					OldCentrex = g_convert_x_dev_to_page (pFenAnimGr->hGsys, CentrexDev);
					OldCentrey = g_convert_y_dev_to_page (pFenAnimGr->hGsys, CentreyDev);

					g_set_page_units (pFenAnimGr->hGsys, pFenAnimGr->cx_client_ref, pFenAnimGr->cy_client_ref, NewTaillexDev, NewTailleyDev);

					NewCentrex = g_convert_x_dev_to_page (pFenAnimGr->hGsys, CentrexDev);
					NewCentrey = g_convert_y_dev_to_page (pFenAnimGr->hGsys, CentreyDev);
					dx = OldCentrex - NewCentrex;
					dy = OldCentrey - NewCentrey;
					g_get_origin (pFenAnimGr->hGsys, &Originex, &Originey);
					g_add_deltas_page (pFenAnimGr->hGsys, &Originex, &Originey, dx, dy);
					ajuste_origine_fen_x (pFenAnimGr->hwndVi, pFenAnimGr->hGsys, &Originex);
					ajuste_origine_fen_y (pFenAnimGr->hwndVi, pFenAnimGr->hGsys, &Originey);
					ajuste_entrees_page (pEnvAnmGr->wId);

					::InvalidateRect (pFenAnimGr->hwndVi , NULL, TRUE);
					maj_espace_scroll_bar_h (pFenAnimGr->hwndVi, pFenAnimGr->hGsys);
					maj_espace_scroll_bar_v (pFenAnimGr->hwndVi, pFenAnimGr->hGsys);
					maj_thumbsize_scroll (pFenAnimGr->hwndVi);
					}
				else
					{
					Beep (300, 300);
					}
				}
			break;

		default:
			bTraite = FALSE;
			break;
		} // switch (mp1 & 0xFFF0)

	return bTraite;
	} // bOnSysCommand

//--------------------------------------------------------------------
// Notification de l'�tat d'une entr�e logique
static void Notifie_bx_anm_e_log_vi (PX_ANM_E_VI pt_e_vi)
	{
	PX_ANM_E_LOG_VI pt_e_log = (PX_ANM_E_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

	NotificationBoolean (pt_e_log->pos_ret, pt_e_log->bValeurCourante);
	}

//--------------------------------------------------------------------
// Notification de l'�tat d'un bouton � impulsion ou d'un check box
static void Notifie_bx_anm_e_control_log (PX_ANM_E_VI pt_e_vi)
	{
	PX_ANM_E_CONTROL_LOG_VI pt_e_control_log = (PX_ANM_E_CONTROL_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc,pt_e_vi->n_enr);

	NotificationBoolean (pt_e_control_log->pos_ret, pt_e_control_log->bValeurCourante);
	}

//--------------------------------------------------------------------
// Notification de l'�tat d'un bouton radio
static void Notifie_bx_anm_e_radio (PX_ANM_FENETRE_VI	pFenAnimGr, PX_ANM_E_VI pt_e_vi, int nCheck)
	{
	PX_ANM_E_RADIO pt_e_radio = (PX_ANM_E_RADIO)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);

	if (nCheck >= 0)
		NotificationReel (pt_e_radio->pos_ret, (FLOAT) nCheck);
	}

//--------------------------------------------------------------------
// Notification de l'�tat d'une entr�e num�rique
static void Notifie_bx_anm_e_num_vi (PX_ANM_E_VI pt_e_vi)
	{
	PX_ANM_E_NUM_VI	pt_e_num = (PX_ANM_E_NUM_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
	FLOAT rReel = (FLOAT)0;
	char	szTemp [256];

	::GetWindowText (pt_e_num->hdl_inline, szTemp, 256);
	StrToFLOAT (&rReel, szTemp);
	NotificationReel (pt_e_num->pos_ret, rReel);
	}

//--------------------------------------------------------------------
// Notification de l'�tat d'une entr�e message
static void Notifie_bx_anm_e_mes_vi (PX_ANM_E_VI pt_e_vi)
	{
	PX_ANM_E_MES_VI	pt_e_mes = (PX_ANM_E_MES_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
	char	szTemp [256];

	::GetWindowText (pt_e_mes->hdl_inline, szTemp, 256);
	NotificationPsz (pt_e_mes->pos_ret, szTemp);
	}

//--------------------------------------------------------------------
// Notification de l'�tat de la s�lection d'une combo box
static void Notifie_bx_anm_e_comboboxSelection (PX_ANM_FENETRE_VI pFenAnimGr, PX_ANM_E_VI pt_e_vi)
	{
	PX_ANM_E_COMBOBOX pt_e_combobox = (PX_ANM_E_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
	HWND	hwndControle = hwndElementGR (pFenAnimGr->hBdGr, pt_e_combobox->n_elt);
	DWORD dwSel = ::SendMessage (hwndControle, CB_GETCURSEL, 0, 0);

	if (dwSel != CB_ERR)
		{
		char	szTemp [256];

		NotificationReel (pt_e_combobox->pos_ret_indice, (FLOAT)(dwSel+1));

		// Et notification du texte s�lectionn�
		::SendMessage(hwndControle, CB_GETLBTEXT, dwSel,(LPARAM) (LPCSTR) szTemp);
		NotificationPsz (pt_e_combobox->pos_ret_saisie, szTemp);
		}
	}

//--------------------------------------------------------------------
// Notification de l'�tat de la saisie d'une combo box
static void Notifie_bx_anm_e_comboboxSaisie (PX_ANM_FENETRE_VI pFenAnimGr, PX_ANM_E_VI pt_e_vi)
	{
	PX_ANM_E_COMBOBOX pt_e_combobox = (PX_ANM_E_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
	HWND	hwndControle = hwndElementGR (pFenAnimGr->hBdGr, pt_e_combobox->n_elt);
	char	szTemp [256];

	::GetWindowText (hwndControle, szTemp, 256);
	NotificationPsz (pt_e_combobox->pos_ret_saisie, szTemp);
	}

//--------------------------------------------------------------------
// Notification de l'�tat de la saisie d'une combo box
static void Notifie_bx_anm_e_liste (PX_ANM_FENETRE_VI pFenAnimGr, PX_ANM_E_VI pt_e_vi)
	{
	PX_ANM_E_LISTE	pt_e_liste = (PX_ANM_E_LISTE)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
	HWND	hwndControle = hwndElementGR (pFenAnimGr->hBdGr, pt_e_liste->n_elt);

	DWORD dwSel = ::SendMessage (hwndControle, LB_GETCURSEL, 0, 0);

	if (dwSel != CB_ERR)
		NotificationReel (pt_e_liste->pos_ret, (FLOAT)(dwSel+1));
	}
	
//--------------------------------------------------------------------
// Notifie l'�tat de toutes les entr�es de la page (fin de boite de dialogue)
static void NotificationEntreesDialogue (DWORD n_page)
	{
  PX_ANM_FENETRE_VI pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, n_page);
  DWORD dwNEntree;

  for (dwNEntree = 0;dwNEntree < pFenAnimGr->nbr_entree; dwNEntree++)
    {
		PX_ANM_E_VI pt_e_vi = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pFenAnimGr->prem_entree+dwNEntree);

    switch (pt_e_vi->n_bloc)
      {
      case bx_anm_e_log_vi:
				Notifie_bx_anm_e_log_vi (pt_e_vi);
        break;

			case bx_anm_e_control_log:
				Notifie_bx_anm_e_control_log (pt_e_vi);
				break;

			case bx_anm_e_radio:
				{
				PX_ANM_E_RADIO	pt_e_radio = (PX_ANM_E_RADIO)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
				HWND	hwndControle = hwndElementGR (pFenAnimGr->hBdGr, pt_e_radio->n_elt);
				INT dwIdPremierRadio;

				// Recherche l'Id du premier Item du groupe
				if (bIdPremierRadio (pFenAnimGr->hwndVi, hwndControle, &dwIdPremierRadio))
					{
					// r�cup�re le num�ro de radio bouton check�
					int	nCheck = nCheckIndex (pFenAnimGr->hwndVi, dwIdPremierRadio);

					// Notifie
					Notifie_bx_anm_e_radio (pFenAnimGr, pt_e_vi, nCheck);
					}
				}
				break;

      case bx_anm_e_num_vi:
				Notifie_bx_anm_e_num_vi (pt_e_vi);
        break;

      case bx_anm_e_mes_vi:
				Notifie_bx_anm_e_mes_vi (pt_e_vi);
        break;

			case bx_anm_e_combobox:
				Notifie_bx_anm_e_comboboxSelection (pFenAnimGr, pt_e_vi);
				Notifie_bx_anm_e_comboboxSaisie (pFenAnimGr, pt_e_vi);
				break;

			case bx_anm_e_liste:
				Notifie_bx_anm_e_liste (pFenAnimGr, pt_e_vi);
				break;

			default:
				VerifWarningExit;
				break;
      }
    }
	} // NotificationEntreesDialogue

//--------------------------------------------------------------------
// WM_COMMAND
static void OnCommand (HWND hwnd, WPARAM mp1, LPARAM mp2)
	{
	DWORD dwId = LOWORD(mp1);

	switch (dwId)
		{
		case IDOK:
			{
			HWND	hwndBoutonVALOk = ::GetDlgItem(hwnd, IDOK);
			// Ce message est envoy� soit par un BOUTON_VAL, 
			// soit parce que l'utilisateur a appuy� sur Entr�e et qu'il n'y a pas de bouton par d�faut
			// Le bouton VAL existe et est � l'origine du message ?
			if (hwndBoutonVALOk && (hwndBoutonVALOk == GetFocus()))
				{
				PENV_ANM_GR	pEnvAnmGr = (PENV_ANM_GR)pGetEnv(hwnd);
				PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
				BOOL								bDialogue = bPageGrDialog (pFenAnimGr->hBdGr);

				// Notification de l'appui du bouton
				NotificationReel (pFenAnimGr->code_ret_user, (FLOAT)ANM_BOUTON_VAL_OK);

				// Boite de dialogue ?
				if (bDialogue)
					// oui => notification de toutes les entr�es 
					NotificationEntreesDialogue (pEnvAnmGr->wId);

				// Fermeture de la fen�tre
				::DestroyWindow (hwnd);
				}
			}
			break;

		case IDCANCEL:
			{
			HWND	hwndBoutonVALEsc = ::GetDlgItem(hwnd, IDCANCEL);
			// Ce message est envoy� soit par un BOUTON_VAL, 
			// soit parce que l'utilisateur a appuy� sur Esc
			// Le bouton VAL existe et est � l'origine du message ?
			if (hwndBoutonVALEsc && (hwndBoutonVALEsc == GetFocus()))
				{
				PENV_ANM_GR					pEnvAnmGr = (PENV_ANM_GR)pGetEnv(hwnd);
				PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);

				// Notification de l'appui du bouton
				NotificationReel (pFenAnimGr->code_ret_user, (FLOAT)ANM_BOUTON_VAL_ANNULER);

				// Fermeture de la fen�tre
				::DestroyWindow (hwnd);
				}
			}
			break;

		case IDAPPLY:
			{ // BOUTON_VAL
			// Le bouton existe ?
			if (::GetDlgItem(hwnd, IDAPPLY))
				{
				PENV_ANM_GR					pEnvAnmGr = (PENV_ANM_GR)pGetEnv(hwnd);
				PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
				BOOL								bDialogue = bPageGrDialog (pFenAnimGr->hBdGr);

				// Notification de l'appui du bouton
				NotificationReel (pFenAnimGr->code_ret_user, (FLOAT)ANM_BOUTON_VAL_APPLIQUER);

				// Boite de dialogue ?
				if (bDialogue)
					// oui => notification de toutes les entr�es 
					NotificationEntreesDialogue (pEnvAnmGr->wId);
				}
			}
			break;

		default:
			{
			// Id d'un contr�le avec animation ?
			if ((dwId >= PREMIER_ID_VALIDE) && (dwId < (PREMIER_ID_VALIDE + NB_IDS_VALIDES)))
				{
				// oui => r�cup�re ses param�tres...
				PENV_ANM_GR		pEnvAnmGr = (PENV_ANM_GR)pGetEnv(hwnd);
				PX_ANM_E_VI	pt_e_vi = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pEnvAnmGr->padwNEnrEntree[dwId - PREMIER_ID_VALIDE]);
				DWORD					dwNotify = HIWORD(mp1);
				HWND					hwndControle = (HWND) mp2;
				PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);

				// ... et traite la notification re�ue selon le type d'animation
				switch (pt_e_vi->n_bloc)
					{
					case bx_anm_e_log_vi: // Entr�e logique
						{
						switch (dwNotify)
							{
							case BN_CLICKED:
								// bascule l'�tat de l'entr�e logique
								{
								PX_ANM_E_LOG_VI pt_e_log = (PX_ANM_E_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);

								pt_e_log->bValeurCourante = !pt_e_log->bValeurCourante;

								// Notifie si l'on n'est pas dans une boite de dialogue
								if (!bPageGrDialog (pFenAnimGr->hBdGr))
									Notifie_bx_anm_e_log_vi(pt_e_vi);
								}
								break;
							}
						}
						break;

					case bx_anm_e_control_log: // Contr�les boutons � impulsion et check box
						{
						switch (dwNotify)
							{
							case BN_CLICKED:
								{
								// Bascule l'�tat de la variable
								PX_ANM_E_CONTROL_LOG_VI pt_e_control_log = (PX_ANM_E_CONTROL_LOG_VI)pointe_enr (szVERIFSource, __LINE__, pt_e_vi->n_bloc, pt_e_vi->n_enr);
								
								pt_e_control_log->bValeurCourante = !pt_e_control_log->bValeurCourante;

								// Dans une boite de dialogue ?
								if (bPageGrDialog (pFenAnimGr->hBdGr))
									// oui => autocheck
									::SendMessage(hwndControle, BM_SETCHECK, (pt_e_control_log->bValeurCourante ? BST_CHECKED: BST_UNCHECKED), 0);
								else
									// non => notifie
									Notifie_bx_anm_e_control_log (pt_e_vi);
								}
								break;
							}
						}
						break;

					case bx_anm_e_radio: // bouton radio
						{
						if (dwNotify == BN_CLICKED)
							{
							// Trouve la position du bouton radio dans son groupe ?
							int	nPositionDansGroupe;

							if (bPositionRadioDansGroupe (hwnd, hwndControle, &nPositionDansGroupe))
								{
								// oui => dans une boite de dialogue ?
								if (bPageGrDialog (pFenAnimGr->hBdGr))
									// oui => autocheck : 
									bCheckRadioBouton	(hwnd, hwndControle, nPositionDansGroupe);
								else
									// non => notifie
									Notifie_bx_anm_e_radio (pFenAnimGr, pt_e_vi, nPositionDansGroupe);
								}
							}
						}
						break;

					case bx_anm_e_num_vi: // Edition d'une valeur num�rique
						{
						if (dwNotify == INLINE_VALIDATION)
							{
							// Pas dans une boite de dialogue ?
							if (!bPageGrDialog (pFenAnimGr->hBdGr))
								// oui => notifie la valeur
								Notifie_bx_anm_e_num_vi (pt_e_vi);
							}
						}
						break;

					case bx_anm_e_mes_vi: // Edition d'un message
						{  
						if (dwNotify == INLINE_VALIDATION)
							{
							// Pas dans une boite de dialogue ?
							if (!bPageGrDialog (pFenAnimGr->hBdGr))
								// oui => r�cup�re le texte et notifie
								Notifie_bx_anm_e_mes_vi (pt_e_vi);
							}
						}
						break;

					case bx_anm_e_combobox: // combobox
						{
						switch (dwNotify)
							{
							case CBN_SELCHANGE:
								{
								// Pas dans une boite de dialogue ?
								if (!bPageGrDialog (pFenAnimGr->hBdGr))
									// oui => notification de l'indice de 1 � n de l'�l�ment s�lectionn�
									Notifie_bx_anm_e_comboboxSelection (pFenAnimGr, pt_e_vi);
								}								
								break;
							case CBN_EDITCHANGE:
								{
								// Pas dans une boite de dialogue ?
								if (!bPageGrDialog (pFenAnimGr->hBdGr))
									// oui => notification du texte �dit�
									Notifie_bx_anm_e_comboboxSaisie (pFenAnimGr, pt_e_vi);
								}
							} // switch (HIWORD(mp1))
						}
						break;

					case bx_anm_e_liste: // ListBox
						{
						switch (dwNotify)
							{
							case LBN_SELCHANGE:
								{
								// Pas dans une boite de dialogue ?
								if (!bPageGrDialog (pFenAnimGr->hBdGr))
									// oui => notification de l'indice de 1 � n de l'�l�ment s�lectionn�
									Notifie_bx_anm_e_liste (pFenAnimGr, pt_e_vi);
								}
								break;
							} // switch (HIWORD(mp1))
						} 
						break; // case bx_anm_e_liste:
					} // switch (pt_e_vi->n_bloc)
				} // if ((dwId >= PREMIER_ID_VALIDE) && (dwId < (PREMIER_ID_VALIDE + NB_IDS_VALIDES)))
			} // default:
			break;
		} // switch (dwId)
	} // OnCommand

// --------------------------------------------------------------------------
// Fen�tres d'affichage d'animation de synoptique
// --------------------------------------------------------------------------
static LRESULT CALLBACK wndprocAnimGr (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT mres = 0;              // Valeur de retour
  BOOL    bTraite = TRUE;        // Indique commande trait�e

  switch (msg)
    {
    case WM_CREATE:
			{
			PENV_ANM_GR pEnvAnmGr = (PENV_ANM_GR)pCreeEnv	(hwnd, sizeof (ENV_ANM_GR));

			if (pEnvAnmGr)
				{
				PX_ANM_FENETRE_VI	pFenAnimGr;

				// Init tableau des entr�es
				pEnvAnmGr->padwNEnrEntree =  NULL;

				// par d�faut, pas encore de contr�le ayant eu le Focus
				pEnvAnmGr->hwndDernierControleActif = NULL;

				// r�cup�re l'Id de la fen�tre pass� en param�tre
				pEnvAnmGr->wId = *(DWORD *)(((LPCREATESTRUCT)mp2)->lpCreateParams);

				// Initialisation des donn�es associ�es � cette fen�tre
			  pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);

				pFenAnimGr->hwndVi = hwnd;
				pFenAnimGr->page = pEnvAnmGr->wId;
				pFenAnimGr->visible = TRUE;

				pFenAnimGr->hGsys = g_open (hwnd);
				g_init (pFenAnimGr->hGsys);
				bdgr_associer_espace (pFenAnimGr->hBdGr, pFenAnimGr->hGsys);
				g_set_page_units (pFenAnimGr->hGsys, c_G_NB_X, c_G_NB_Y, Appli.sizEcran.cx, Appli.sizEcran.cy);
				g_set_x_origine (pFenAnimGr->hGsys, 0); //$$ xOrigine);
				g_set_y_origine (pFenAnimGr->hGsys, 0); //$$ yOrigine);

				// BmpFond
				bdgr_charger_bmp_fond_page (pFenAnimGr->hBdGr);

				// cr�e les contr�les Windows de la page
				VerifWarning (bCreerFenetresElementGr (pFenAnimGr->hBdGr, PREMIER_ID_VALIDE + NB_IDS_VALIDES));

				// Ajoute les autres contr�les d'entr�e de la page
				ajoute_entrees_page (wCtxtPage);

				OnCreatePopUp (hwndParentPage, hwnd);
				}
			else
				mres = -1;
			}
			break;

    case WM_DESTROY:
			{
			PENV_ANM_GR pEnvAnmGr = (PENV_ANM_GR)pGetEnv (hwnd);

			OnDestroyPopUp (hwndParentPage, hwnd);
		  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);

			// Fermetures diverse
			FlushCourbeArchivePage (pEnvAnmGr->wId);
			pFenAnimGr->visible = FALSE;
			NotificationBoolean (pFenAnimGr->code_ret_visible , pFenAnimGr->visible);
			renvoie_coordonnees_etat (pFenAnimGr);
			enleve_entrees_page (pEnvAnmGr->wId); // $$ oui pour les objets, NON pour les hwnd
			bdgr_supprimer_bmp_fond_page (pFenAnimGr->hBdGr);
			VerifWarning (bFermerFenetresElementGr (pFenAnimGr->hBdGr));
			bdgr_fermer_page (&pFenAnimGr->hBdGr, FALSE);
			SupEltPageListeEltsClignottants (pEnvAnmGr->wId);
			g_close (&pFenAnimGr->hGsys);
			pFenAnimGr->hwndVi = NULL;
			bLibereEnv (hwnd);
			}
			break;

		case WM_GET_MODALITE_POPUP:
			{
			PENV_ANM_GR pEnvAnmGr = (PENV_ANM_GR)pGetEnv (hwnd);
		  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
			if (bPageGrModale (pFenAnimGr->hBdGr))
				mres = POPUP_MODAL;
			else
				mres = POPUP_NON_MODAL;
			}
			break;

		case WM_TIMER_PARPOP:
			// Gestion clignottement
			{
			AnimationCouleurElementClignottant (hwnd, (BOOL)mp1);
			}
			break;

		case WM_ACTIVATE:
			if (LOWORD(mp1) != WA_INACTIVE)
				hDlgGrActive = hwnd;
			else
				hDlgGrActive = NULL;

			OnActivatePopUp (hwndParentPage, hwnd, mp1);
			break;

    case WM_COMMAND:
			OnCommand (hwnd, mp1, mp2);
			break;

    case WM_SYSCOMMAND:
			bTraite = bOnSysCommand (hwnd, mp1, mp2);
			mres = bTraite;
      break;

    case WM_KEYDOWN:
			{
			DWORD	dwTouche;

			bTraite = bScrOnKeyDownTouchePcs (mp1, &dwTouche);
			if (bTraite)
				bTraite = (dwTouche <= TOUCHE_SHIFT_F10);

			if (bTraite)
				{
				// oui => Notifie Pcs de l'appui
				NotificationBoolean (dwTouche + GR_TOUCHES_FCT , TRUE);
				}
			mres = (LRESULT) !bTraite;
			}
			break;


    case WM_NEXTDLGCTL:
			{
			PENV_ANM_GR pEnvAnmGr = (PENV_ANM_GR)pGetEnv (hwnd);
			HWND	hwndNouveau;

			// Demande d'activation d'un controle sp�cifique ?
			if ((BOOL) LOWORD(mp2))
				{
				// oui => r�cup�re le
				hwndNouveau = (HWND)mp1;
				}
			else
				{
				// non => Le focus vient t'il d'une fen�tre fille de la boite de dialogue ?
				HWND	hwndCourant = GetFocus ();

				if (::IsChild (hwnd, hwndCourant))
					//r�cup�re le pr�c�dent ou le suivant du courant
					hwndNouveau = GetNextDlgTabItem (hwnd, hwndCourant, (BOOL)mp1);
				else
					//r�cup�re le pr�c�dent ou le suivant 
					hwndNouveau = GetNextDlgTabItem (hwnd, pEnvAnmGr->hwndDernierControleActif, (BOOL)mp1);
				}
			// Handle � activer Ok ?
			if (hwndNouveau)
				{
				// oui => active le
				pEnvAnmGr->hwndDernierControleActif = hwndNouveau;
				SetFocus (pEnvAnmGr->hwndDernierControleActif);
				}
			}
			break;

		case WM_SIZE:
			if (mp1 != SIZE_MINIMIZED)
				{
				PENV_ANM_GR					pEnvAnmGr = (PENV_ANM_GR)pGetEnv (hwnd);
			  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
				LONG dx0;
				LONG dy0;

				g_get_origin (pFenAnimGr->hGsys, &dx0, &dy0);
				ajuste_espace_fen (hwnd, pEnvAnmGr->wId);
				ajuste_origine_fen_x (hwnd, pFenAnimGr->hGsys, &dx0);
				ajuste_origine_fen_y (hwnd, pFenAnimGr->hGsys, &dy0);
				ajuste_entrees_page (pEnvAnmGr->wId);
				maj_thumbsize_scroll (hwnd);
				maj_espace_scroll_bar_h (hwnd, pFenAnimGr->hGsys);
				maj_espace_scroll_bar_v (hwnd, pFenAnimGr->hGsys);
				}
			break;

    case WM_MOVE:
			bTraite = FALSE;
			break;

    case WM_ERASEBKGND:
			// effacement du fond int�gr� au WM_PAINT => effacement effectu�
			mres = TRUE;
			break;

    case WM_PAINT:
			{
			PENV_ANM_GR					pEnvAnmGr = (PENV_ANM_GR)pGetEnv (hwnd);
		  PX_ANM_FENETRE_VI	pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
			PAINTSTRUCT					ps;

			// r�cup�re les infos pour dessiner

			::BeginPaint (hwnd, &ps);

			// dessin complet de la page
			bdgr_dessiner_page (pFenAnimGr->hBdGr, MODE_DESSIN_COULEUR_FOND | MODE_DESSIN_ELEMENT);

			// dessin des courbes
			DessineCourbesPage (pFenAnimGr->hGsys, pFenAnimGr->page);

			// dessin termin�
			::EndPaint (hwnd, &ps);
			}
			break;

/* $$ � porter
    case WM_MINMAXFRAME:
			//------------------------------------------------------------------
			pEnvAnmGr = pGetEnv (hwnd);
			pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
			if (pFenAnimGr->pas_forcage)
				{
				pswpNew = (PSWP) (PVOID)(mp1);
				// ---------------------------------------------------- dim
				if (pswpNew->fs & SW_MINIMIZE)
					pFenAnimGr->dimension = DIM_ICONE;
				else
					{
					if (pswpNew->fs & SW_MAXIMIZE)
						pFenAnimGr->dimension = DIM_MAX;
					else
						pFenAnimGr->dimension = DIM_NORMALE;
					}
				}
			bTraite = FALSE;
			break;
*/
    case WM_HSCROLL:
			{
			/* $$ � porter
		  LONG dx0;
		  LONG dy0;

			if (LOWORD (mp1) == FID_HORZSCROLL)
				{
				pEnvAnmGr = pGetEnv (hwnd);
				pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
				g_get_origin (pFenAnimGr->hGsys, &dx0, &dy0);
				if (origine_x_modifie (pEnvAnmGr, mp2, &dx0))
					{
					ajuste_origine_fen_x (hwnd, pFenAnimGr->hGsys, &dx0);
					ajuste_entrees_page (pEnvAnmGr->wId);
					::InvalidateRect (hwnd, NULL, TRUE);
					}
				}
			*/
			}
			break;

    case WM_VSCROLL:
			{
			/* $$ � porter
		  LONG  dx0;
		  LONG  dy0;

			if (LOWORD (mp1) == FID_VERTSCROLL)
				{
				pEnvAnmGr = pGetEnv (hwnd);
				pFenAnimGr = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, pEnvAnmGr->wId);
				g_get_origin (pFenAnimGr->hGsys, &dx0, &dy0);
				if (origine_y_modifie (pEnvAnmGr, mp2, &dy0))
					{
					ajuste_origine_fen_y (hwnd, pFenAnimGr->hGsys, &dy0);
					ajuste_entrees_page (pEnvAnmGr->wId);
					::InvalidateRect (hwnd, NULL, TRUE);
					}
				}
			*/
			}
			break;

    default:
			bTraite = FALSE;
			break;
    } // switch (msg)

  if (!bTraite)
    {
    mres = DefWindowProc (hwnd, msg, mp1, mp2);
    }

  return mres;
  } // wndprocAnimGr


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//								FONCTIONS EXPORTEES
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Initialisation de WanmGr
// --------------------------------------------------------------------------
BOOL bInitWanmGr (void)
	{
	BOOL	bRes = FALSE;

	// Initialisation de la base de donn�e graphique avec des valeurs par d�faut
	// pour les donn�es inexistantes
  bdgr_creer ();
  StrSetNull (titre_saisie_facteur);
  StrSetNull (mnemo_facteur);
  StrSetNull (mnemo_maigrir);
  StrSetNull (mnemo_grossir);
  bLngLireInformation (c_inf_titre_facteur_zoom, titre_saisie_facteur);
  bLngLireInformation (c_inf_facteur_zoom, mnemo_facteur);
  bLngLireInformation (c_inf_maigrir, mnemo_maigrir);
  bLngLireInformation (c_inf_grossir, mnemo_grossir);

  if (existe_repere (bx_anm_fenetre_vi) && (nb_enregistrements (szVERIFSource, __LINE__, bx_anm_fenetre_vi) != 0))
    {
		// enregistrement si n�cessaire de la classe ex�cution GR
		static BOOL	bClasseEnregistree = FALSE;

		if (!bClasseEnregistree)
			{
			WNDCLASS wc;

			wc.style					= CS_OWNDC; // $$ v�rifier | CS_HREDRAW | CS_VREDRAW; // un DC est allou� en permanence pour la fen�tre
			wc.lpfnWndProc		= wndprocAnimGr; 
			wc.cbClsExtra			= 0; 
			wc.cbWndExtra			= 0; 
			wc.hInstance			= Appli.hinst; 
			wc.hIcon					= LoadIcon (Appli.hinstDllLng, MAKEINTRESOURCE(IDICO_DOC_EXE));
			wc.hCursor				= LoadCursor (NULL, IDC_ARROW);
			wc.hbrBackground	= NULL; // pas de brosse d'effacement par d�faut
			wc.lpszMenuName		= NULL; 
			wc.lpszClassName	= szClasseAnmGr; 
			bClasseEnregistree = RegisterClass (&wc);
			}
    if (bClasseEnregistree)
      {
      // Espace de travail de la fen�tre ex�cution GR
      hSpcFen = hSpcCree (Appli.sizEcran.cx, Appli.sizEcran.cy, c_G_NB_X, c_G_NB_Y);

      // Alloue m�moire pour points courbes
      InitCourbes ();

			// Cr�e une fen�tre parente pour toutes les pages
			hwndParentPage = hwndCreeParentPopUp ();
      if (hwndParentPage != NULL)
				{
				cree_bloc (bx_anm_liste_elts_cli, sizeof (X_ANM_ELT_LISTE_ELTS_CLI), 0);
			  if (bLanceDiffusionEvtTimerPopUp (hwndParentPage, INTERVALLE_CLIGNOTTEMENT))
					bRes = TRUE;
				}
      }
    }

	return bRes;
	} // bInitWanmGr

// --------------------------------------------------------------------------
// Fermeture de WanmGr
// --------------------------------------------------------------------------
void FermeWanmGr (void)
	{
	BOOL	bRes = FALSE;

  // DestroyWindowsGr ();
	// Fen�tre parente des pages existe ?
	if (hwndParentPage)
		{
		// oui => ferme la (ce qui ferme les fen�tres filles)
		::DestroyWindow (hwndParentPage);
		hwndParentPage = NULL;
		enleve_bloc (bx_anm_liste_elts_cli);
		}
  if (existe_repere (bx_anm_fenetre_vi))
    {
    // Lib�re m�moire pour points courbes
	  FiniCourbes ();
		SpcFerme (&hSpcFen);
		}

  bdgr_supprimer_blocs_marques ();
  } // FermeWanmGr

// --------------------------------------------------------------------------
// Ex�cution � la demande du serveur PcsExe d'un service WanmGr
void ExecService (BYTE ** ppBuff)
  {
	// Tableau des Services de WAnmGr
	typedef void (* PFN_SERVICE_GR) (BYTE ** ppBuff, DWORD Position);
	static PFN_SERVICE_GR tdGrSrv [ANM_GR_NOMBRE_SERVICES] =
		{
		AnimAck        , // SERVICE_ANM_GR_ACK
		AnimFenAffiche , // SERVICE_ANM_GR_FEN_AFFICHE
		AnimFenEfface  , // SERVICE_ANM_GR_FEN_EFFACE
		AnimFenTaille  , // SERVICE_ANM_GR_FEN_TAILLE
		AnimSelectPage , // SERVICE_ANM_GR_SELECT_PAGE
		AnimImpression , // SERVICE_ANM_GR_PAGE_IMPRESSION
		AnimLogique2   , // SERVICE_ANM_GR_ANM_LOG_2_ETATS
		AnimLogique4   , // SERVICE_ANM_GR_ANM_LOG_4_ETATS
		AnimBaragraphe , // SERVICE_ANM_GR_ANM_NUM_BARAGRAPHE
		AnimDeplacement, // SERVICE_ANM_GR_ANM_NUM_DEPLACEMENT
		AnimMessage    , // SERVICE_ANM_GR_ANM_MES_SIMPLE
		AnimMessageClr , // SERVICE_ANM_GR_ANM_MES_COULEUR
		AnimMesNum     , // SERVICE_ANM_GR_ANM_MES_NUM_SIMPLE
		AnimMesNumClr  , // SERVICE_ANM_GR_ANM_MES_NUM_COULEUR
		AnimCourbe     , // SERVICE_ANM_GR_ANM_COURBES
		AnimCourbe     , // SERVICE_ANM_GR_ANM_ESCALIERS
		AnimCourbeArchive  , // SERVICE_ANM_GR_ANM_ARCHIVES
		AnimForceNum   , // SERVICE_ANM_GR_ANM_FORCAGE_NUM
		AnimForceMes   , // SERVICE_ANM_GR_ANM_FORCAGE_MES
		AnimControlListeCombobox,   // SERVICE_ANM_GR_ANM_LISTE_COMBOBOX				       
		AnimRadio,       // SERVICE_ANM_GR_ANM_FORCAGE_RADIO        
		AnimCheck,			 // SERVICE_ANM_GR_ANM_FORCAGE_CHECK        
		AnimControlText, // SERVICE_ANM_GR_ANM_FORCAGE_TEXT
		AnimLogTransparent, // SERVICE_ANM_GR_ANM_FORCAGE_LOG_TRANSPARENT
		AnimBoutonPulse	 // SERVICE_ANM_GR_ANM_FORCAGE_BOUTON_PULSE
		};
  HEADER_SERVICE_ANM_GR	HeaderServiceAnmGr = ExtraireHeaderServiceAnmGr (ppBuff);

	Verif (HeaderServiceAnmGr.Service < ANM_GR_NOMBRE_SERVICES);
  tdGrSrv [HeaderServiceAnmGr.Service] (ppBuff, HeaderServiceAnmGr.Element);
  }

// --------------------------------------------------------------------------
DWORD dwTailleInfosServicesAnmGr (void)
  {
  return sizeof (t_anm_gr_data_service);
  }


/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |---------------------------------------------------------------------*/
// scrut_ap.h
// Unite de Scrutation sur Carte Applicom
// Win32 23/7/97/JS

//----------------------------------------------
// Initialise et termine l'ex�cution d'Applicom
//----------------------------------------------
void  initialise_ap (void);
void	ferme_ap (void);

// ------------------------------------
// interface scrutation Carte Applicom
// ------------------------------------
void  recoit_ap (BOOL bPremier);
void  emet_ap   (BOOL bPremier);





	

//-------------------------------------------------------------------------
// Module de gestion DDE
//-------------------------------------------------------------------------
#include "stdafx.h"
#include "UDde.h"

// Instanciation des variables DDE
DWORD	hInstDDE = 0;

// M�morisation de l'environnement des conversations
int				nNbEnvConversations = 0;
ENV_CONV	EnvConvs [NB_MAX_CONVERSATIONS];

static const ENV_CONV ENV_CONV_NULL = {NULL, NULL, NULL, NULL, 0, 0, NULL, FALSE};

static PCSTR pszErreursDDE[18] =
	{
	"DDE Advise Ack time out",
	"DDE Busy",
	"DDE Data Ack time out",
	"DDE Dll not initialized",
	"DDE DLL usage",
	"DDE Exec ack time out",
	"DDE invalid parameter",
	"DDE Low memory",
	"DDE memory_error",
	"DDE not processed",
	"DDE no conversation established",
	"DDE poke ack time out",
	"DDE post msg failed",
	"DDE reentrancy",
	"DDE server died",
	"DDE sys error",
	"DDE unadvise ack time out",
	"DDE unfound queue id"
	};

static PCSTR	szErreurDDEInconnue = "Erreur DDE Inconnue";
static PCSTR	szErreurDDEOk = "DDE Ok";

//-------------------------------------------------------------------------
// R�cup�ration de la chaine descriptive d'une erreur DDE
static PCSTR pszErreurDDE (UINT wErrDDE)
	{
	PCSTR pszRes = szErreurDDEInconnue;

	if (wErrDDE)
		{
		if ((wErrDDE >= DMLERR_FIRST) && (wErrDDE <= DMLERR_LAST))
			pszRes = pszErreursDDE[wErrDDE - DMLERR_FIRST];
		}
	else
		pszRes = szErreurDDEOk;

	return pszRes;
	} // pszErreurDDE

//----------------------------------------------------------------------
// Derni�re erreur DDE
UINT uDerniereErreurDDE (void)
	{
	return DdeGetLastError(hInstDDE);
	}

//----------------------------------------------------------------------
// Message Box / Derni�re erreur DDE circonstanci�e par son contexte
void SignaleDerniereErreurDDE (HWND hWndParent, PSTR	pszContexte)
	{
	::MessageBox (hWndParent, pszErreurDDE (DdeGetLastError(hInstDDE)), pszContexte, MB_OK|MB_ICONSTOP);
	}

//-------------------------------------------------------------------------
// cr�ation d'un environnement li� � une conversation client / serveur
PENV_CONV pEnvConvCree(HCONV hConv, BOOL bCreateurServeur)
	{
	PENV_CONV	pEnvConv = NULL;
	
	// peut on encore cr�er un environnement ?
	if (nNbEnvConversations < NB_MAX_CONVERSATIONS)
		{
		// oui => le dernier est libre : initialise le
		pEnvConv = &EnvConvs [nNbEnvConversations];
		*pEnvConv = ENV_CONV_NULL;
		pEnvConv->bCreateurServeur = bCreateurServeur;
		
		// Une conversation de plus
		nNbEnvConversations++;
		}

	return pEnvConv;
	} // pEnvConvCree

//-------------------------------------------------------------------------	
// r�cup�ration d'un environnement d�ja li� � une conversation client / serveur
PENV_CONV pEnvConvTrouve (HCONV hConv)
	{
	int
		nNConversation = 0;
	PENV_CONV
		pEnvConv;
	
	// recherche de l'environnement de la conversation
	pEnvConv  = &EnvConvs [0];
	for (nNConversation = 0; nNConversation < nNbEnvConversations; nNConversation++)
		{
		if (pEnvConv->hConv  == hConv)
			{
			return pEnvConv;
			break;
			}
		else
			pEnvConv++;
		}
	return NULL;
	} // pEnvConvTrouve

//-------------------------------------------------------------------------	
// lib�ration d'un environnement d�ja li� � une conversation client / serveur
void FermeEnvConversation (PENV_CONV * ppEnvConv)
	{
	int	nNConversation;
	PENV_CONV pEnvConv = *ppEnvConv;
	
	// Environnement valide ?
	if (pEnvConv)
		{
		// oui => on tasse les environnements de conversation suivants
		for (nNConversation = pEnvConv - &EnvConvs[0]; nNConversation+1 < nNbEnvConversations; nNConversation++)
			{
			*pEnvConv  = pEnvConv[1];
			pEnvConv++;
			}
		// une conversation en moins
		nNbEnvConversations--;

		// mise � 0 du pointeur pass�
		*ppEnvConv = NULL;
		}
	} // FermeEnvConversation

//---------------------------------------------------------------------------
// Demande de d�connection de toutes les conversations ouvertes par un serveur
void FermeConversationsDDEServeur (HSZ hszAppli)
	{
	int
		nNConversation;
	
	// parcours des conversations en cours
	for (nNConversation = 0; nNConversation < nNbEnvConversations; nNConversation++)
		{
		// Origine de la conversation examin�e = serveur � d�connecter ?
		if ((EnvConvs[nNConversation].bCreateurServeur) && (EnvConvs[nNConversation].hszAppli == hszAppli))
			// oui => on demande la d�connection de la liaison
			if (DdeDisconnect (EnvConvs[nNConversation].hConv))
				EnvConvs[nNConversation].hszAppli = NULL;
			else
				SignaleDerniereErreurDDE (NULL, "Fermeture conversation serveur restante");
		}
	} // FermeConversationsDDEServeur

//---------------------------------------------------------------------------
// Fonction appel�e par DDEML sur tout �v�nement DDE (client comme serveur)
static HDDEDATA CALLBACK  hDDECallBackCommune
	(
	UINT wType,			// type de transaction
	UINT wFormat,		// format "clipboard" des donn�es
	HCONV hConv,		// handle de la conversation
	HSZ hsz1,				// handle string1
	HSZ hsz2,				// handle string2
	HDDEDATA hData,	// handle objet m�moire global
	DWORD dwData1,	// donn�e sp�cifique de transaction
	DWORD dwData2		// donn�e sp�cifique de transaction
	)
	{
	HDDEDATA hDataRes = NULL;	// handle r�sultat NULL par d�faut
	
	// traitement selon le type de message
	switch (wType)
		{
		case XTYP_ERROR:			// Client ou Serveur
			::MessageBox (NULL, "Syst�me DDE en difficult�", NULL, MB_ICONHAND | MB_OK);
			break;
			
			// case XTYP_REGISTER: // client ou serveur case XTYP_UNREGISTER: return (HDDEDATA) NULL;
			
		case XTYP_CONNECT:			// Serveur seulement
		case XTYP_WILDCONNECT:	// Serveur seulement
			// callback du serveur effectivement implant�e ?
			if ((ServeurDde.hszAppli == hsz2) && ServeurDde.pfnDDE)
				hDataRes = ServeurDde.pfnDDE (NULL, wType, wFormat, hConv, hsz1, hsz2, hData, dwData1, dwData2);
			break;
			
		case XTYP_CONNECT_CONFIRM:	// Serveur seulement
			// callback du serveur effectivement implant�e ?
			if ((ServeurDde.hszAppli == hsz2) && ServeurDde.pfnDDE)
				{
				// oui => cr�e un environnement de conversation initialis� par d�faut...
				PENV_CONV	pEnvConv = pEnvConvCree(hConv, TRUE);

				pEnvConv->hszAppli = hsz2;
				pEnvConv->hszTopic = hsz1;
				pEnvConv->hConv = hConv;
				pEnvConv->pfnDDE = ServeurDde.pfnDDE;

				// ... et notifie la fonction serveur (pas de valeur de retour)
				ServeurDde.pfnDDE (pEnvConv, wType, wFormat, hConv, hsz1, hsz2, hData, dwData1, dwData2);
				}
			else
				::MessageBox (NULL, "Connexion � un serveur invalide", NULL, MB_ICONHAND | MB_OK);			
			break;
			
		case XTYP_ADVDATA: 	// Client seulement
		case XTYP_ADVREQ:		// Serveur seulement
		case XTYP_ADVSTART:	// Serveur seulement
		case XTYP_ADVSTOP:	// Serveur seulement
		case XTYP_EXECUTE:	// Serveur seulement
		case XTYP_POKE:			// Serveur seulement
		case XTYP_REQUEST:	// Serveur seulement
			{
			// retrouve le contexte li� � la conversation ?
			PENV_CONV	pEnvConv = pEnvConvTrouve(hConv);

			if (pEnvConv && pEnvConv->pfnDDE)
				// oui => on s'adresse � la fonction DDE concern�e
				hDataRes = pEnvConv->pfnDDE (pEnvConv, wType, wFormat, hConv, hsz1, hsz2, hData, dwData1, dwData2);
			else
				// non => comportement par d�faut (n'arrive jamais $$)
				::MessageBox (NULL, "Environnement conversation invalide", NULL, MB_ICONHAND | MB_OK);			
			}
			break;

		case XTYP_DISCONNECT:	// Client ou Serveur
			{
			// retrouve le contexte li� � la conversation ?
			PENV_CONV	pEnvConv = pEnvConvTrouve(hConv);

			if (pEnvConv)
				{
				// oui => on s'adresse � la fonction DDE concern�e
				if (pEnvConv->pfnDDE)
					hDataRes = pEnvConv->pfnDDE (pEnvConv, wType, wFormat, hConv, hsz1, hsz2, hData, dwData1, dwData2);

				// puis on lib�re l'environnement associ�
				FermeEnvConversation (&pEnvConv);
				}
			else
				// non => comportement par d�faut (n'arrive jamais $$)
				::MessageBox (NULL, "Environnement conversation invalide (Fermeture conversation)", NULL, MB_ICONHAND | MB_OK);
			}
			break;
		} // switch (wType)
	return hDataRes;
	} // hDDECallBackCommune

//-------------------------------------------------------------------------------
// Initialisation du syst�me DDEML
BOOL bInitDDE (HINSTANCE hInst, BOOL	bClient, BOOL bServeur)
	{
	UINT wErr = DMLERR_NO_ERROR;
#define F_COMMUN APPCLASS_STANDARD | CBF_SKIP_REGISTRATIONS | CBF_SKIP_UNREGISTRATIONS
	
	// Initialisation DDEML selon les caract�ristiques demand�es
	if (bServeur)
		// Initialisation avec serveur
		wErr = DdeInitialize(&hInstDDE, hDDECallBackCommune, F_COMMUN, 0L);
	else
		{
		if (bClient)
			// Initialisation pour client seulement
      wErr = DdeInitialize(&hInstDDE, hDDECallBackCommune, APPCMD_CLIENTONLY | F_COMMUN, 0L);
		} // else (bServeur)
	
	// Analyse du r�sultat de l'initialisation
	return (wErr == DMLERR_NO_ERROR);
	} // bInitDDE

//---------------------------------------------------------------------
// Lib�ration du syst�me DDEML
void FermeDDE (void)
	{
	// Lib�ration DDEML
	if (hInstDDE)
		{
		DdeUninitialize (hInstDDE);
		hInstDDE = 0;
		}
	} // FermeDDE

//---------------------------------------------------------------------
// Renvoie TRUE si DDEML est actif
BOOL bDDEOuvert (void)
	{
	return hInstDDE != 0;
	}

//%%%%%%%%%%%%%%%%%%%%%% Sp�cifique SERVEURS %%%%%%%%%%%%%%%%%%%%%%%%%%

SERVEUR_DDE	ServeurDde = {0, 0, 0};

//---------------------------------------------------------------------
// D�claration d'un nouveau service DDE
BOOL bAjouteServeurDDE (PCSTR pszService, PFNDDE pfnDDE, LPARAM lParam)
	{
	HSZ		hszAppli;
	
	// Un service existe d�ja ?
	if (ServeurDde.hszAppli)
		{
		// oui => erreur
		::MessageBox (NULL, "Un seul serveur est autoris�", NULL, MB_ICONHAND | MB_OK);
		return FALSE;
		}
	else
		{
		// cr�ation du handle de string pour le service (code page = WP_ANSI)
		hszAppli = DdeCreateStringHandle (hInstDDE, pszService, 0);
		
		// On garde cette string pour plus tard
		// $$DdeKeepStringHandle(hInstDDE, hszAppli);
		
		// Informe les autres clients de la pr�sence de ce nouveau serveur
		if ((BOOL)(DWORD)DdeNameService(hInstDDE, hszAppli, NULL, DNS_REGISTER | DNS_FILTERON))
			{
			ServeurDde.hszAppli = hszAppli;
			ServeurDde.pfnDDE = pfnDDE;
			ServeurDde.lParam = lParam;
			return TRUE;
			}
		else
			{
			DdeFreeStringHandle(hInstDDE, hszAppli);
			return FALSE;
			}
		} // else (ServeurDde.hszAppli)
	} // bAjouteServeurDDE

//---------------------------------------------------------------------
// Fermeture d'un service DDE
BOOL bFermeServeurDDE (void)
	{
	BOOL
		bRes = FALSE;
	
	// $$ cr�ation du handle de string pour le service (code page = WP_ANSI)
	// $$ hszAppli = DdeCreateStringHandle (hInstDDE, pszService, 0);
	
	// ServeurDde d�ja ouvert ?
	if (ServeurDde.hszAppli)
		{
		// Le service n'est plus support�
		bRes = (BOOL)(DWORD)DdeNameService(hInstDDE, ServeurDde.hszAppli, NULL, DNS_UNREGISTER);
		
		// fermeture des conversations en cours sur le serveur
		FermeConversationsDDEServeur(ServeurDde.hszAppli);
		
		// lib�ration des donn�es du serveur
		DdeFreeStringHandle(hInstDDE, ServeurDde.hszAppli);
		ServeurDde.pfnDDE = NULL;
		ServeurDde.lParam = 0;
		ServeurDde.hszAppli =	NULL;
		}
	
	// Renvoie le r�sultat de la connection
	return bRes;
	} // bFermeServeurDDE

//---------------------------------------------------------------------
// Un Item du serveur, sujet d'un ou plusieurs Advise en cours, vient de changer de valeur.
// L'appel de cette fonction met � jour tous les clients concern�s
// (DDEML enverra au serveur un XTYP_REQUEST pour chaque conversation concern�e).
BOOL bChangementItemServeurDansAdvise (PSTR pszTopic, PSTR pszItem)
	{
	// cr�ation du handle de string pour l'item (code page = WP_ANSI)
	HSZ	hszItem = DdeCreateStringHandle (hInstDDE, pszItem, 0);
	HSZ hszTopic = DdeCreateStringHandle (hInstDDE, pszTopic, 0);

	// $$ Destruction de ces HSZ n�c�ssaire ?
	return DdePostAdvise (hInstDDE, hszTopic, hszItem);
	} // bChangementItemServeurDansAdvise


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Sp�cifique CLIENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------
// Lancement d'une conversation
PENV_CONV pInitConversationDDE (PSTR pszService, PSTR pszTopic, PFNDDE pfnClient, LPARAM lParam1, LPARAM lParam2)
	{
	PENV_CONV	pEnvConv = NULL;
	HCONV			hConv;
	// cr�ation des handles de string pour applic et topic de la conversation (code page = WP_ANSI)
	HSZ				hszAppli = DdeCreateStringHandle (hInstDDE, pszService, 0);
	HSZ				hszTopic = DdeCreateStringHandle (hInstDDE, pszTopic, 0);
	
	// Connexion au serveur
	hConv = DdeConnect(hInstDDE, hszAppli, hszTopic, NULL);
	
	// Connexion Ok ?
	if (hConv)
		{
		// On cr�e un environnement  pour la conversation cliente
		pEnvConv = pEnvConvCree(hConv, FALSE);
		
		// Environnement Ok ?
		if (pEnvConv)
			{
			// oui => On enregistre dans l'environnement tous les param�tres utilisateur
			pEnvConv->hszAppli = hszAppli;
			pEnvConv->hszTopic = hszTopic;
			pEnvConv->lParam1 = lParam1;
			pEnvConv->lParam2 = lParam2;
			pEnvConv->pfnDDE = pfnClient;
			pEnvConv->hConv = hConv;	// Handle de conversation d�ja ouverte
			}
		else
			{
			// non => On d�connecte la conversation $$ Code d'erreur
			DdeDisconnect(hConv);
			}
		} // if (hConv)
	return pEnvConv;
	} // pInitConversationDDE

//---------------------------------------------------------------------
// Fin d'une conversation (d�connection du serveur)
BOOL bFermeConversationDDE (PENV_CONV * ppEnvConv)
	{
	BOOL bRes = FALSE;
	PENV_CONV pEnvConv = * ppEnvConv;

	// Environnement conversation valide ?
	if (pEnvConv)
		{
		// oui => D�connection Ok ?
		if (DdeDisconnect(pEnvConv->hConv))
			{
			// oui => lib�ration des ressources de l'environnement de cette conversation
			DdeFreeStringHandle (hInstDDE, pEnvConv->hszAppli);
			DdeFreeStringHandle (hInstDDE, pEnvConv->hszTopic);
			if (pEnvConv->hszItem)
				DdeFreeStringHandle (hInstDDE, pEnvConv->hszItem);
			*pEnvConv = ENV_CONV_NULL;
			
			// Lib�ration de cet environnement
			FermeEnvConversation (ppEnvConv);
			
			// Termin� Ok
			bRes = TRUE;
			} // if (DdeDisconnect(pEnvConv->hConv))
		} // if (pEnvConv)

	return bRes;
	} // bFermeConversationDDE

//---------------------------------------------------------------------
// demande par un client de donn�es 
HDDEDATA hdataRequeteDonneeDDE (PENV_CONV pEnvConv, PSTR pszItem, UINT wCBFormat, DWORD dwMsTimeOut)
	{
	// cr�ation du handle de string pour l'item (code page = WP_ANSI)
	pEnvConv->hszItem = DdeCreateStringHandle (hInstDDE, pszItem, 0);
	
	// renvoie le r�sultat de la requete
	return DdeClientTransaction(
		NULL,	// pas de donn�es � passer dans une requ�te au serveur
		0,		// longueur de donn�es pass�e au serveur nulle
		pEnvConv->hConv,	// Handle de conversation d�ja ouverte
		pEnvConv->hszItem,	// Handle Nom Item
		wCBFormat,	// Les donn�es sont demand�es au format sp�cifi�
		XTYP_REQUEST,	// on effectue une requete
		dwMsTimeOut,	// transaction synchrone, Time out de la requete en milliseconde
		NULL	// Pas d'Id transaction en mode synchrone
		);
	} // hdataRequeteDonneeDDE 

//---------------------------------------------------------------------
// demande par un client de surveillance automatique de donn�es
HDDEDATA hdataRequeteAutoDonneeDDE (PENV_CONV pEnvConv, PSTR pszItem, UINT wCBFormat, DWORD dwMsTimeOut)
	{
	HDDEDATA hdataRes = NULL;
	// cr�ation du handle de string pour l'item (code page = WP_ANSI)
	pEnvConv->hszItem = DdeCreateStringHandle (hInstDDE, pszItem, 0);
	
	// On renvoie le r�sultat de la requ�te (Bool�en)
	if ((DWORD)DdeClientTransaction(
		NULL,	// pas de donn�es � passer dans une requ�te au serveur
		0,		// longueur de donn�es pass�e au serveur nulle
		pEnvConv->hConv,	// Handle de conversation d�ja ouverte
		pEnvConv->hszItem,	// Handle Nom Item
		wCBFormat,	// Les donn�es sont demand�es au format texte
		XTYP_ADVSTART | XTYPF_ACKREQ,	// d�but de boucle de surveillance avec attente lecture
		dwMsTimeOut,	// transaction synchrone, Time out de la requete en milliseconde
		// TIMEOUT_ASYNC
		NULL	// Id transaction ignor� $$
		))
		{
		// Lecture de la valeur de la donn�e
		hdataRes = DdeClientTransaction(
			NULL,	// pas de donn�es � passer dans une requ�te au serveur
			0,		// longueur de donn�es pass�e au serveur nulle
			pEnvConv->hConv,	// Handle de conversation d�ja ouverte
			pEnvConv->hszItem,	// Handle Nom Item
			wCBFormat,	// Les donn�es sont demand�es au format sp�cifi�
			XTYP_REQUEST,	// on effectue une requete
			dwMsTimeOut,	// transaction synchrone, Time out de la requete en milliseconde
			NULL	// Pas d'Id transaction en mode synchrone
			);
		}
	return hdataRes;
	} // hdataRequeteAutoDonneeDDE

//---------------------------------------------------------------------
// envoi par un client de donn�es
BOOL bPokeDonneeDDE (PENV_CONV pEnvConv, PSTR pszItem, UINT wCBFormat, PSTR pszBuf, DWORD dwTailleBuf, DWORD dwMsTimeOut)
	{
	HDDEDATA hReponse;
	UINT	wErr;
		
	// cr�ation du handle de string pour l'item (code page = WP_ANSI)
	pEnvConv->hszItem = DdeCreateStringHandle (hInstDDE, pszItem, 0);
	
	// poke ok ?
	hReponse = DdeClientTransaction(
		(PBYTE)pszBuf,				// donn�es � passer au serveur
		dwTailleBuf,		// longueur des donn�es � passer
		pEnvConv->hConv,	// Handle de conversation d�ja ouverte
		pEnvConv->hszItem,				// Handle Nom Item
		wCBFormat,	// Les donn�es sont envoy�es au format sp�cifi�
		XTYP_POKE,	// on effectue un "poke"
		dwMsTimeOut,	// transaction synchrone, Time out du poke en milliseconde
		NULL	// Pas d'Id transaction en mode synchrone
		);
	if ((BOOL)(DWORD)hReponse)
		{
		// oui => Ok
		return TRUE;
		}
	else
		{
		// non => on r�cup�re l'erreur
		wErr = DdeGetLastError(hInstDDE);
		// termin�
		return FALSE;
		}
	} // bPokeDonneeDDE

//---------------------------------------------------------------------
// Transaction dans une conversation : demande par un client de donn�es texte 
BOOL bRequeteTexteDDE (PENV_CONV pEnvConv, PSTR pszItem, PSTR pszBuf, DWORD dwTailleBuf, DWORD dwMsTimeOut)
	{
	BOOL			bRes = FALSE;
	HDDEDATA	hReponse;
		
	// cr�ation du handle de string pour l'item (code page = WP_ANSI)
	pEnvConv->hszItem = DdeCreateStringHandle (hInstDDE, pszItem, CP_WINANSI);
	
	// requete ok ?
	hReponse = DdeClientTransaction(
		NULL,	// pas de donn�es � passer dans une requ�te au serveur
		0,		// longueur de donn�es pass�e au serveur nulle
		pEnvConv->hConv,	// Handle de conversation d�ja ouverte
		pEnvConv->hszItem,	// Handle Nom Item
		CF_TEXT,	// Les donn�es sont demand�es au format texte
		XTYP_REQUEST,	// on effectue une requete
		dwMsTimeOut,	// transaction synchrone, Time out de la requete en milliseconde
		NULL	// Pas d'Id transaction en mode synchrone
		);
	if (hReponse)
		{
		// oui => on r�cup�re le r�sultat en coupant si n�cessaire le texte en trop
		DdeGetData (hReponse, (PBYTE)pszBuf, dwTailleBuf-1, 0);
		pszBuf[dwTailleBuf-1]= '\0';
		// On lib�re le r�sultat
		DdeFreeDataHandle(hReponse);
		bRes = TRUE;
		}

	return bRes;
	} // bRequeteTexteDDE

//---------------------------------------------------------------------
// Transaction dans une conversation : demande par un client de surveillance
// automatique de donn�es texte 
BOOL bRequeteAutoTexteDDE (PENV_CONV pEnvConv, PSTR pszItem, PSTR pszBuf, DWORD dwTailleBuf, DWORD dwMsTimeOut)
	{
	BOOL	bRes = FALSE;

	// cr�ation du handle de string pour l'item (code page = WP_ANSI)
	pEnvConv->hszItem = DdeCreateStringHandle (hInstDDE, pszItem, 0);
	
	// demande surveillance ok ?
	if ((DWORD) DdeClientTransaction(
		NULL,	// pas de donn�es � passer dans une requ�te au serveur
		0,		// longueur de donn�es pass�e au serveur nulle
		pEnvConv->hConv,	// Handle de conversation d�ja ouverte
		pEnvConv->hszItem,	// Handle Nom Item
		CF_TEXT,	// Les donn�es sont demand�es au format texte
		XTYP_ADVSTART | XTYPF_ACKREQ,	// d�but de boucle de surveillance avec attente lecture
		dwMsTimeOut,	// transaction synchrone, Time out de la requete en milliseconde
		// TIMEOUT_ASYNC
		NULL	// Id transaction ignor� $$
		))
		{
		// oui => lecture de la premi�re donn�e ok ?
		HDDEDATA	hReponse = DdeClientTransaction(
			NULL,	// pas de donn�es � passer dans une requ�te au serveur
			0,		// longueur de donn�es pass�e au serveur nulle
			pEnvConv->hConv,	// Handle de conversation d�ja ouverte
			pEnvConv->hszItem,	// Handle Nom Item
			CF_TEXT,	// Les donn�es sont demand�es au format texte
			XTYP_REQUEST,	// on effectue une requete
			dwMsTimeOut,	// transaction synchrone, Time out de la requete en milliseconde
			NULL	// Pas d'Id transaction en mode synchrone
			);
		if (hReponse)
			{
			// oui => on r�cup�re le r�sultat
			DdeGetData (hReponse, (PBYTE)pszBuf, dwTailleBuf, 0);

			// On lib�re le r�sultat
			DdeFreeDataHandle(hReponse);
			bRes = TRUE;
			}
		}
	return bRes;
	} // bRequeteAutoTexteDDE

//---------------------------------------------------------------------
// Transaction dans une conversation : envoi par un client de donn�es texte 
BOOL bPokeTexteDDE (PENV_CONV pEnvConv, PSTR pszItem, PSTR pszTexteEnvoi, DWORD dwMsTimeOut)
	{
	// cr�ation du handle de string pour l'item (code page = WP_ANSI)
	pEnvConv->hszItem = DdeCreateStringHandle (hInstDDE, pszItem, 0);
	
	// poke ok ?
	return (BOOL)(DWORD) DdeClientTransaction(
		(PBYTE)pszTexteEnvoi,	// donn�es � passer au serveur
		(DWORD)lstrlen (pszTexteEnvoi)+1,		// longueur des donn�es � passer
		pEnvConv->hConv,	// Handle de conversation d�ja ouverte
		pEnvConv->hszItem,	// Handle Nom Item
		CF_TEXT,	// Les donn�es sont envoy�es au format texte
		XTYP_POKE,	// on effectue un "poke"
		dwMsTimeOut,	// transaction synchrone, Time out du poke en milliseconde
		NULL	// Pas d'Id transaction en mode synchrone
		);
	} // bPokeTexteDDE

//---------------------------------------------------------------------
// Transaction dans une conversation : envoi par un client d'une commande texte 
BOOL bExecuteCommandeTexteDDE (PENV_CONV pEnvConv, PSTR pszTexteCommande, DWORD dwMsTimeOut)
	{
	// Execute ok ?
	return (BOOL)(DWORD) DdeClientTransaction(
		(PBYTE)pszTexteCommande,	// donn�es � passer au serveur
		(DWORD)lstrlen (pszTexteCommande)+1,		// longueur des donn�es � passer
		pEnvConv->hConv,	// Handle de conversation d�ja ouverte
		NULL,	// pas de nom d'item pour un ex�cute
		0,	// Format texte unique pour un execute
		XTYP_EXECUTE,	// on effectue un "execute"
		dwMsTimeOut,	// transaction synchrone, Time out du poke en milliseconde
		NULL	// Pas d'Id transaction en mode synchrone
		);
	} // bExecuteCommandeTexteDDE


//---------------------------- fin UDde.c -----------------------------

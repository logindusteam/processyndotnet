#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "LireLng.h"
#include "Appli.h"
#include "MenuMan.h"


// --------------------------------------------------------------------
// ajout d'un message information � un menu
void MenuAppendMI (HMENU hMenu, int IdMI, UINT IdMenu)
	{
  char szMI [c_nb_car_message_inf];

  bLngLireInformation (IdMI, szMI);
	AppendMenu (hMenu, MF_STRING, IdMenu, szMI);
	}

// --------------------------------------------------------------------
// ajout d'un menu pop up message information � un menu
void MenuAppendPopUpMI (HMENU hMenu, int IdMI, HMENU hmenuPopUp)
	{
  char szMI [c_nb_car_message_inf];

  bLngLireInformation (IdMI, szMI);
 	AppendMenu (hMenu, MF_STRING | MF_POPUP, (UINT)hmenuPopUp, szMI);
	}


// --------------------------------------------------------------------
// Insertion d'un menu pop up message information � un menu (insertion avant uPosItemInsert)
void MenuInsertPopUpMI (HMENU hMenu, UINT uPosItemInsert, int IdMI, HMENU hmenuPopUp)
	{
  char szMI [c_nb_car_message_inf];

  bLngLireInformation (IdMI, szMI);
	//InsertMenu(hMenu,	IdItemInsert,	MF_BYCOMMAND | MF_STRING | MF_POPUP,(UINT)hmenuPopUp, szMI);	
	InsertMenu(hMenu,	uPosItemInsert,	MF_BYPOSITION | MF_STRING | MF_POPUP,(UINT)hmenuPopUp, szMI);	
	}

// --------------------------------------------------------------------
// ajout d'un s�parateur � un menu
void MenuAppendSeparateur (HMENU hMenu)
	{
	AppendMenu (hMenu, MF_SEPARATOR, 0, NULL);
	}

// -----------------------------------------------------------------------
// Grise ou valide un item du menu
// -----------------------------------------------------------------------
void MenuGriseItem (HMENU hmenu, UINT uIdItem, BOOL bGrise)
  {
	EnableMenuItem (hmenu, uIdItem, bGrise ? MF_BYCOMMAND | MF_GRAYED : MF_BYCOMMAND | MF_ENABLED);
  }


// -----------------------------------------------------------------------
// Grise ou valide un sous menu du menu de l'application (Cf Appli.h)
void MenuAppliGriseSubMenu (UINT uPosition, BOOL bGrise)
	{
	HMENU hmenuMain = ::GetMenu (Appli.hwnd);


	EnableMenuItem (hmenuMain, uPosition, bGrise ? MF_BYPOSITION | MF_GRAYED : MF_BYPOSITION | MF_ENABLED);
	DrawMenuBar(Appli.hwnd);
	}

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
// Met ou Enl�ve une coche sur un item menu
// -----------------------------------------------------------------------
void MenuCocheItem (HMENU hmenu, UINT uIdItem, BOOL bAvecCoche)
  {
	CheckMenuItem (hmenu, uIdItem, bAvecCoche ? MF_BYCOMMAND | MF_CHECKED : MF_BYCOMMAND | MF_UNCHECKED);
  }

// -----------------------------------------------------------------------
// Grise ou valide un item du menu de l'application (Cf Appli.h)
void MenuAppliGriseItem (UINT uIdItem, BOOL bGrise)
	{
	EnableMenuItem (::GetMenu (Appli.hwnd), uIdItem, bGrise ? MF_BYCOMMAND | MF_GRAYED : MF_BYCOMMAND | MF_ENABLED);
	}

// -----------------------------------------------------------------------
// Met ou Enl�ve une coche sur un item du menu de l'application (Cf Appli.h)
void MenuAppliCocheItem (UINT uIdItem, BOOL bAvecCoche)
  {
	CheckMenuItem (::GetMenu (Appli.hwnd), uIdItem, bAvecCoche ? MF_BYCOMMAND | MF_CHECKED : MF_BYCOMMAND | MF_UNCHECKED);
  }

// -----------------------------------------------------------------------
// ins�re automatiquement des raccourcis � un tableau d'items ??
void insere_raccourci (char *titre_desc, char *tab_caractere_raccourci)
  {
  #define ACCELERATEUR '&'
  BOOL trouve;
  UINT    index_titre;
  UINT    longueur_titre;
  char    car_raccourci;

  if (StrLength (tab_caractere_raccourci) < 26)
    {
    trouve = FALSE;
    longueur_titre = StrLength (titre_desc);
    for (index_titre = 0; ((index_titre < longueur_titre) && (!trouve)) ; index_titre++)
      {
      car_raccourci = StrUpperCaseChar (StrGetChar (titre_desc, index_titre));
      if ((car_raccourci>= 'A') && (car_raccourci <= 'Z'))
        {
        if (!StrCharIsInStr (car_raccourci, tab_caractere_raccourci))
          {
          trouve = TRUE;
          }
        }
      }
    if (trouve)
      {
      StrConcatChar (tab_caractere_raccourci, car_raccourci);
      StrInsertChar (titre_desc, ACCELERATEUR, index_titre - 1);
      }
    }
  }


// ITC.h: interface for the CITC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITC_H__336A9662_22AE_41A8_9CDB_092A64B9AEBF__INCLUDED_)
#define AFX_ITC_H__336A9662_22AE_41A8_9CDB_092A64B9AEBF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "QueueMan.h"
#include "QueueLI.h"

//--------------------------------------------------
// Codes d'erreurs renvoy�es
#define ITC_ERR_ITC_IS_IN_USE      1
#define ITC_ERR_NO_MORE_HDL        2
#define ITC_ERR_ITC_UNKOWN_STATE   3
#define ITC_ERR_ITC_NOT_IN_USE     4
#define ITC_ERR_USR_BUSY           5
#define ITC_ERR_TIMEOUT            6
#define ITC_ERR_WAIT_EVENT         7
#define ITC_ERR_ITC_OPEN_WAIT      8
#define ITC_ERR_ITC_ALREADY_CLOSED 9
#define ITC_ERR_ITC_NOT_CONNECTED  10
#define ITC_ERR_PUT_MSG            11
#define ITC_ERR_ITC_NOT_COMM_STATE 12
#define ITC_NO_MSG                 13
#define ITC_ERR_ITC_READ_MSG       14
#define ITC_ERR_ITC_DISCONNECTED   15
#define ITC_ERR_SEND_RQU           16
#define ITC_ERR_ALLOC              17
#define ITC_ERR_INVALID_PARAM      18

#define ITC_INDEFINITE_WAIT        INFINITE
#define ITC_DEF_ACK_TIMEOUT        120000 //$$6400 // timeout d'attente d'accuse reception par defaut

// Ids messages
#define ITC_MSG_OUVRE    1
#define ITC_MSG_FERME    2
#define ITC_MSG_USER     3


// type utilisateur
#define ITC_USR_MAX_NAME 32


class CITC  
	{
	public:
		// �tats d'une connexion
		typedef enum
			{
			CNX_INVALIDE,									// Connexion invalide (non initialis�e)
			CNX_EN_ATTENTE_CONNEXION,			// Connexion en attente d'�tablissement d'une communication
			CNX_CONNEXION_EN_COURS,				// Connexion avec un correspondant en cours
			CNX_FIN_CONNEXION							// Connexion ferm�e par le correspondant
			} ETAT;

		// entete d'un message transmis
		typedef struct
			{
			DWORD IdMessage;				// Ids messages
			DWORD dwTailleDonnees;	// Taille en octets du message, ce Header non compris
			} ITC_HEADER_MESSAGE, *PITC_HEADER_MESSAGE;		

		// Messages transmis
		typedef struct
			{
			ITC_HEADER_MESSAGE  Entete;
			BYTE								aDonnees[1]; // partie utilisateur de 0 � n octets
			} MESSAGE_ITC, *PMESSAGE_ITC;

		// Constructeur / destructeur
		CITC();
		virtual ~CITC();

		// Cr�ation d'un serveur ITC en �mission (TX) ou en r�ception (RX).
		// Si un client est en attente de connexion on ouvre la communication avec lui.
		// Sinon, on attend au plus dwTimeOutAttente qu'il soit connect�.
		// Si la connexion n'a pas pu �tre �tablie pendant cette la dur�e, un appel ulterieur � la fonction
		// bCnxAUnCorrespondant pourra determiner si l'appel a abouti.
		// L'attente d'appel court jusqu' � l'�tablissement de la connexion ou un appel � dwFerme
		// Codes de retour :                                                      
		//    0                         : la fonction s'est bien deroulee         
		// ITC_ERR_SEND_RQU             : erreur dans l'envoie de la demande      
		// ITC_ERR_ITC_OPEN_WAIT        : canal est deja en attente d'ouverture   
		// ITC_ERR_ITC_ALREADY_OPEN     : canal est deja ouvert                   
		// ITC_ERR_ITC_NOT_CONNECTED    : canal non enregistre                    
		// ITC_ERR_ITC_UNKOWN_STATE     : canal en derangement                    
		// ITC_ERR_WAIT_EVENT           : erreur attente d'ouverture              
		BOOL bCreeServeurRX (const char * pszNomTopic);		// Nom du service support�.
		BOOL bCreeServeurTX (const char * pszNomTopic);		// Nom du service support�.

		// Cr�ation d'une client ITC avec essai de connexion � un serveur ITC de m�me nom.
		BOOL bCreeClientRX
			(
			const char * pszNomTopic,		// Nom du service auquel on veut se connecter
			CITC* pITCServeur						// ITC du serveur auquel se connecter
			);
		
		BOOL bCreeClientTX
			(
			const char * pszNomTopic,		// Nom du service auquel on veut se connecter
			CITC* pITCServeur						// ITC du serveur auquel se connecter
			);

		// Savoir si une connexion est reli�e � un correspondant
		// Renvoie TRUE si c'est le cas, FALSE sinon
		BOOL bAUnCorrespondant
			(
			DWORD dwTimeOutAttente		// Time out max d'attente de la connexion en ms ou ITC_INDEFINITE_WAIT
			);

		// Ferme le client ou le serveur ITC.
		// Si une connexion � un correspondant est en cours, signale lui cette fermeture.
		void Ferme ();
		
		// Envoi d'un message � un correspondant.
		// Attention ! Le buffer de donn�es doit commencer par une structure ITC_HEADER_MESSAGE
		// (la valeur de ses champs sera mise � jour par cette fonction)
		DWORD dwEcriture
			(
			PITC_HEADER_MESSAGE	pHeaderMessage,	// adresse du header contenant les donn�es a envoyer suivies par les donn�es
			DWORD			dwTailleMessage,	// taille du buffer 
			BOOL			bDemandeAR				// active l'attente d'Accus� de R�ception si TRUE
			);

		// Envoi d'un message quelconque � un correspondant.
		// Le buffer de donn�es NE NECESSITE PAS de commencer par une structure ITC_HEADER_MESSAGE
		DWORD dwEcritureBuffer
			(
			PVOID			pBufMessage,	// adresse du header contenant les donn�es a envoyer suivies par les donn�es
			DWORD			dwTailleMessage,	// taille du buffer 
			BOOL			bDemandeAR				// active l'attente d'Accus� de R�ception si TRUE
			);

		// R�ception d'un message syst�me ou issu du correspondant.
		DWORD dwLecture 
			(
			PVOID * ppBufDest,			// adresse du pointeur sur le buffer recevant les donn�es lues
			BOOL	bAvecAttente,	// Attente avec time out courant si TRUE, pas d'attente sinon
			BOOL	bCopieBuffer	// Si TRUE recopie le message initial, sinon donne le message initial � lib�rer
			);

		// Renvoie le s�maphore d'attente de r�ception de la connexion, NULL si erreur
		HSEM hsemAttenteReception ();

		// Envoi d'un accus� de reception. 
		DWORD dwEnvoiAR ();

		// Attente d'un accus� de reception sur une connexion pendant au plus timeout ms.
		BOOL bAttenteAR 
			(
			DWORD dwTimeOutAttente	// Time out max d'attente de l'accus� de r�ception (ms) ou ITC_INDEFINITE_WAIT
			);


	private:
		BOOL bCreeServeur (PCSTR pszNomTopic, BOOL bEnReception);
		BOOL bCreeClient (PCSTR pszNomTopic, CITC* pITCServeur, BOOL bEnReception);
		BOOL bAccesExclusif ();
		void FinAccesExclusif ();
		void FermeNonExclusif ();
		BOOL bEnvoiMessageAuCorrespondant (PITC_HEADER_MESSAGE pEntete, PVOID pvDonnees, BOOL bDemandeAR);
		void LibereQueue ();
		BOOL bAddQueueRX (DWORD dwTailleEnvoi, PVOID pBufEnvoi);
		BOOL bAccepteConnexion (CITC* pCorrespondant);

	private:
		ETAT			m_nEtat;													// Etat courant de la connexion
		HSEM			m_hsemAccesExclusif;							// s�maphore auto pris d'acc�s exclusif � cette connexion
		CQueueLI *	m_pQueueRX;												// queue receptrice des messages partag�e entre TX et RX
		//HQUEUE		m_hQueueRX;												// queue receptrice des messages 
		//HSEM			m_hsemAttenteAR;									// s�maphore d'attente de l'arriv�e d'un Accus� de R�ception
		char			m_szNomTopic [ITC_USR_MAX_NAME];	// nom du service g�r� par cette connexion
//		CITC *		m_pCITCCorrespondant;							// handle Connexion du correspondant ou null
		BOOL			m_bServeur;												// TRUE : Cet ITC accepte une connexion client ou FALSE : se connecte comme client
		BOOL			m_bEnReception;											// TRUE : Re�oit des messages ou FALSE : �met des messages
		BOOL			m_bDansListeAttente;							// $$ Obsolete Vrai si le handle de cette connexion est actuellement dans hListeHcnxAConnecter
	};

typedef CITC* PCITC;

typedef struct
	{
	PCITC pITCRX;
	PCITC pITCTX;
	} ITC_TX_RX, *PITC_TX_RX;


#endif // !defined(AFX_ITC_H__336A9662_22AE_41A8_9CDB_092A64B9AEBF__INCLUDED_)

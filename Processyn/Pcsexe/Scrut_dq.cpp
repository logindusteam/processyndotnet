// ----------------------------------------------------
// Ce fichier est la propriete de LOGIQUE INDUSTRIE
// Il est demeure sa propriete exclusive et est confidentiel.
// Aucune diffusion n'est possible sans accord ecrit
// Titre   : scrut_dq.cpp
// Auteur  : AC
// Date    : 7/7/93
// Version : 4.00
// NOTA les fichiers sont identifi�s dans bx_disq sous le nom du code Processyn
// mais utilis�s avec des path partant du dossier document s'ils �taient relatifs
// exemple : fic.dat sera ouvert sur c:\Mon Application\fic.dat
// ----------------------------------------------------------------------
#include "stdafx.h"
#include <math.h>

#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "MemMan.h"
#include "DocMan.h"
#include "trap.h"
#include "driv_dq.h"
#include "Descripteur.h"
#include "Pcs_Sys.h"
#include "PcsVarEx.h"
#include "tipe_dq.h"
#include "Appli.h"
#include "scrut_dq.h"
#include "Verif.h"
VerifInit;

#define MIN(A,B) ((A) <= (B) ? (A) : (B))

#define TAILLE_MESSAGE_DANS_FICHIER 82

#define CSV_TAILLE_MAX_CHAMP_LOGIQUE 21
#define CSV_TAILLE_MAX_CHAMP_NUMERIQUE 51
#define CSV_TAILLE_MAX_CHAMP_MESSAGE 1001
typedef enum
	{
	FORMAT_MIN = 0,
	FORMAT_MS = 0,
	FORMAT_BASIC = 1,
	FORMAT_TURBO = 2,
	FORMAT_ASCII = 3,
	FORMAT_CSV = 4, // NOUVEAU JS 2001 07 06
	FORMAT_BINAIRE_8_BITS = 5, // NOUVEAU JS 2005 03 10
	MAX_FORMAT = 5
	} FORMAT_FICHIER;

#define c_pointeur_min ((FLOAT)1.0)
#define c_pointeur_max ((FLOAT)16777216.0) // Limite au dela de laquelle un +1 est perdue par cause de manque de pr�cision

#define c_car_cr ((char)13)
#define c_car_lf ((char)10)
typedef struct
  {
  BYTE exposant;
  BYTE mantisse1;
  BYTE mantisse2;
  BYTE mantisse3;
  BYTE mantisse4;
  BYTE mantisse5;
  } t_turbo_r32;


//-------------------------------------------------------
static void BufferInit (char *buffer, char char_init, int nombre_init)
  {
  for (int i = 0; (i < nombre_init); i++)
    {
    buffer[i] = char_init;
    }
  }

//--------------------------------------------------------------
// Recherche le fichier sp�cifi� parmi les fichiers g�r�s
// renvoie son pointeur si trouv�, NULL sinon
static PFICHIER_DQ pTrouveFichierDQ (PCSTR pszNomFichier)
  {
  PFICHIER_DQ pRet = NULL;
	DWORD dwNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_disq);
	for (DWORD dwNEnr = 1; dwNEnr <= dwNbEnr; dwNEnr++)
		{
		PFICHIER_DQ pFichierDQTemp = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq, dwNEnr);
    if (bStrIEgales (pFichierDQTemp->nom_fichier, pszNomFichier))
			{
			pRet = pFichierDQTemp;
			break;
			}
		}

  return pRet;
  }

//--------------------------------------------------------------
// Recherche le fichier sp�cifi� parmi les fichiers g�r�s et ferme le s'il est ouvert
// renvoie TRUE si trouv�, FALSE sinon
static BOOL bTrouveEtFermeFichierDQ(PCSTR pszNomFichier, DWORD & erreur_sys)
	{
	BOOL bRes = FALSE;

	PFICHIER_DQ pFichierDQ = pTrouveFichierDQ(pszNomFichier);
	if (pFichierDQ)
    {
    if (pFichierDQ->bOuvert)
      {
      erreur_sys = uFicFerme (&pFichierDQ->f_handle);
      MemLibere (&pFichierDQ->pBuffer);
      pFichierDQ->bOuvert = FALSE;
      }
		bRes = TRUE;
    }

	return bRes;
	}
//-------------------------------------------------------------------
static void maj_status (DWORD erreur, char *nom_fichier)
  {
  CPcsVarEx::SetBitsValVarSysNumEx (VA_SYS_STATUS_DISQUE, erreur);
  CPcsVarEx::SetValVarSysMesEx (VA_SYS_NOM_DE_FICHIER, nom_fichier);
  }

//-------------------------------------------------------------------
static void maj_status_systeme (DWORD erreur)
  {
  PX_VAR_SYS	position = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, status_systeme);
  PFLOAT ptr_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (position->dwPosCourEx));

  (*ptr_num) = (FLOAT) (erreur);
  }


//----------voids de conversion de format MS <---->  TURBO -------
static t_turbo_r32 ms2turbo (FLOAT *vr, char *nom_fichier)
  {
  BYTE t1, t2;
  BYTE *btravail;
  t_turbo_r32 turbo_r32;

  // ---------------------------------------------------------------
  //  CALCUL DE L'EXPOSANT
  //----------------------------------------------------------------
	//$$ !!! Il semble qu'il y ait un bug dans l'�criture orginale V�rifier
  //$$btravail = (BYTE *)vr;
	btravail = (BYTE *)&vr;
  t1 = (BYTE)(btravail[4] * 2);
  t2 = (BYTE)(btravail[2] / 0x80);
  t1 = t1 + t2;
  if (t1 < 254)
    {
    if (t1 != 0)
      {
      turbo_r32.exposant = (BYTE)(t1 + 2);
      }
    else
      {
      turbo_r32 .exposant = t1;
      }
    }
  else
    {
    maj_status (STATUT_DQ_ERR_NUMERIQUE_INVALIDE, nom_fichier);
    turbo_r32.exposant = 0;
    }

  // ---------------------------------------------------------------
  //  CALCUL DU SIGNE
  //----------------------------------------------------------------
  t1 = (BYTE)(btravail[3] & 0x80);
  turbo_r32 .mantisse5 = t1;
  // ---------------------------------------------------------------
  //  CALCUL DE LA MANTISSE
  //----------------------------------------------------------------
  t1 = (BYTE)(btravail[2] & 0x7F);
  turbo_r32 .mantisse5 = turbo_r32 .mantisse5 | t1;
  turbo_r32 .mantisse4 = btravail[1];
  turbo_r32 .mantisse3 = btravail[0];
  turbo_r32 .mantisse2 = 0;
  turbo_r32 .mantisse1 = 0;
  return (turbo_r32);
  }

//-------------------------------------------------------------------
static FLOAT turbo2ms (t_turbo_r32 *turbo_r32, char *nom_fichier)
  {
  BYTE t1, t2;
  BYTE *btravail;
  FLOAT vr;

  // Calcul de l'exposant
  btravail = (BYTE *)&vr;
  if (turbo_r32->exposant > 2 )
    {
    t1 = (BYTE)(turbo_r32->exposant - 2);
    t2 = (BYTE)(t1 % 2);
    t2 = (BYTE)(t2 * 0x80);
    t1 = (BYTE)(t1 / 2);
    }
  else
    {
    if (turbo_r32->exposant != 0 )
      {
      maj_status (STATUT_DQ_ERR_NUMERIQUE_INVALIDE, nom_fichier);
      }
    t1 = 0;
    t2 = 0;
    }
  btravail [3] = t1;
  btravail [2] = t2;

  // Calcul du signe
  t1 = (BYTE)(turbo_r32->mantisse5 & 0x80);
  btravail [3] = btravail [3] | t1;

  // Calcul de la mantisse
  t1 = (BYTE)(turbo_r32->mantisse5 & 0x7F);
  btravail [2] = btravail [2] | t1;
  btravail [1] = turbo_r32->mantisse4;
  btravail [0] = turbo_r32->mantisse3;
  return (vr);
  }


//-------------------------------------------------------------------
static BOOL bGetFormatFichierCourant(FORMAT_FICHIER & nFormatFichierCourant, BOOL & bFormatFixe)
  {
	BOOL bRes = FALSE;
  nFormatFichierCourant = (FORMAT_FICHIER)(DWORD)CPcsVarEx::fGetValVarSysNumEx(VA_SYS_FORMAT_FICHIER);
	if ((((DWORD)nFormatFichierCourant) >= FORMAT_MIN) && (((DWORD)nFormatFichierCourant) <= MAX_FORMAT))
		{
		bFormatFixe = nFormatFichierCourant != FORMAT_CSV;
		bRes = TRUE;
		}
	return bRes;
  }

//--------------------------------------
// Ecriture sur disque
//--------------------------------------
void scrute_ecrit_dq (char *nom_fichier)
  {
  DWORD erreur_sys = 0;

	// Fichier sp�cifi� existe ?
	PFICHIER_DQ pFichierDQ = pTrouveFichierDQ(nom_fichier);
  if (pFichierDQ)
    {
		// oui => fichier ouvert ?
    if (pFichierDQ->bOuvert)
      {
			// oui : r�cup�re le pointeur d'�criture
			HFDQ hdl = pFichierDQ->f_handle;
			PBYTE	pbBufFic = (PBYTE)pFichierDQ->pBuffer;
			DWORD	dwNbEnr;
			uFicNbEnr (hdl, &dwNbEnr); //$$ err
			FLOAT taille_plus1 = (FLOAT) (dwNbEnr + 1);
			BOOL bPointeurEcritureExiste = (pFichierDQ->dwPosESEcriture != 0);
			FLOAT fNEnrEcriture;
			FLOAT * ptr_ecr = 0;
      if (bPointeurEcritureExiste)
        { // acc�s direct
				ptr_ecr = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pFichierDQ->dwPosESEcriture);
        fNEnrEcriture = (*ptr_ecr);
      	}
      else
        { 
				// acc�s s�quentiel
				DWORD	dwNEnr = 0;

				uFicPointeurEcriture (hdl, &dwNEnr);
        fNEnrEcriture = (FLOAT) dwNEnr;
        }

			// Pointeur d'�criture ok ?
      if ((fNEnrEcriture >= c_pointeur_min) && (fNEnrEcriture <= c_pointeur_max) && (fNEnrEcriture <= taille_plus1))
        {
				// oui : format courant de fichier Ok ?
				FORMAT_FICHIER nFormatFichierCourant;
				BOOL bFormatFixe = FALSE;
				if (bGetFormatFichierCourant(nFormatFichierCourant, bFormatFixe))
					{
					t_tableau_vars_fichiers*ptr_var;
					FLOAT *ptr_fen_num;
					FLOAT *ptr_num;
					t_turbo_r32 *ptr_fen_turbo;
					PBOOL ptr_log;
					PSTR ptr_mes;
					char mes_tempo[20];

					for (DWORD i = pFichierDQ->pos_debut_var; (i <= pFichierDQ->pos_fin_var); i++)
						{
						ptr_var = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq, i);
						DWORD taille_tab = 1;
						if (ptr_var->taille != 0)
							{
							taille_tab = ptr_var->taille;
							}
						for (DWORD pos_in_tab = ptr_var->pos; (pos_in_tab <= (ptr_var->pos + taille_tab - 1)); pos_in_tab++)
							{
							switch (ptr_var->genre)
								{
								case c_res_logique :
									{
									ptr_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pos_in_tab);
									switch (nFormatFichierCourant)
										{
										case FORMAT_ASCII:
											{
											*pbBufFic = (BYTE) ' ';
											pbBufFic++;
											if (*ptr_log)
												{
												*pbBufFic = (BYTE) '1';
												}
											else
												{
												*pbBufFic = (BYTE) '0';
												}
											pbBufFic ++; // espace + 0 ou 1
											}
											break;

										case FORMAT_CSV:
											{
											if (*ptr_log)
												*pbBufFic = (BYTE)'1';
											else
												*pbBufFic = (BYTE)'0';
											pbBufFic += 1; // 0 ou 1
											}
											break;

										case FORMAT_BINAIRE_8_BITS:
											// pas de logiques en format binaire
							        maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
											break;

										default:
											{
											//pour garder compatibilite fichier un BOOL est sur 2 octets (BOOL sur un octet puis 0 sur un octet)
											*pbBufFic = (BYTE)(*ptr_log);
											pbBufFic++;
											*pbBufFic = (BYTE)0;
											pbBufFic++;
											}
											break;
										} // switch (nFormatFichierCourant)
									}
									break; // c_res_logique

								case c_res_numerique :
									{
									ptr_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_in_tab);
									switch (nFormatFichierCourant)
										{
										case FORMAT_BASIC :
											// $$fonctions de conversions non support�es par VC 4.0 suppos� conforme IEEE
											//ptr_fen_num = (FLOAT *)pbBufFic;
											//fieeetomsbin (ptr_num, ptr_fen_num) ;
											//pbBufFic += sizeof(FLOAT);
											//break;
										case FORMAT_MS :
											{
											ptr_fen_num = (FLOAT *)pbBufFic;
											(*ptr_fen_num) = (*ptr_num);
											pbBufFic += sizeof(FLOAT);
											}
											break;
										case FORMAT_TURBO : // $$ �ventuellement obsolete ?
											{
											ptr_fen_turbo = (t_turbo_r32 *)pbBufFic;
											(*ptr_fen_turbo) = ms2turbo (ptr_num, nom_fichier);
											pbBufFic += sizeof(t_turbo_r32);
											}
											break;

										case FORMAT_ASCII :
											{
											BufferInit ((char *)pbBufFic, ' ', 15);
											StrPrintFormat (mes_tempo, "%15.2f", (*ptr_num));
											StrCopySans0Terminateur((char *)pbBufFic, mes_tempo);
											pbBufFic += 15;
											}
											break;

										case FORMAT_CSV:
											{
											StrFLOATToStr (mes_tempo, (*ptr_num));
											StrCopySans0Terminateur((char *)pbBufFic, mes_tempo);
											pbBufFic += StrLength (mes_tempo);
											}
											break;

										case FORMAT_BINAIRE_8_BITS:
											{
											FLOAT fVal = *ptr_num;
											if ((fVal >= 0) && (fVal <= 255))
												{
												*(PBYTE)pbBufFic = (BYTE)fVal;
												pbBufFic += sizeof(BYTE);
												}
											else
								        maj_status (STATUT_DQ_ERR_NUMERIQUE_INVALIDE, nom_fichier);
											}
											break;

										} // switch format
									}
									break; // c_res_numerique

								case c_res_message :
									{
									ptr_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, pos_in_tab);
									switch (nFormatFichierCourant)
										{
										case FORMAT_ASCII:
											{
											BufferInit ((char *)pbBufFic, ' ', TAILLE_MESSAGE_DANS_FICHIER);
											StrCopySans0Terminateur((char *)pbBufFic, ptr_mes);
											pbBufFic += (TAILLE_MESSAGE_DANS_FICHIER - 2);
											}
											break;
										case FORMAT_BASIC:
											{
											BufferInit ((char *)pbBufFic, ' ', TAILLE_MESSAGE_DANS_FICHIER);
											StrCopySans0Terminateur((char *)pbBufFic, ptr_mes);
											pbBufFic += TAILLE_MESSAGE_DANS_FICHIER;
											}
											break;
										case FORMAT_TURBO :
										case FORMAT_MS :
											{
											BufferInit ((char *)pbBufFic, ' ', TAILLE_MESSAGE_DANS_FICHIER);
											((char *)pbBufFic)[0] = (char)StrLength (ptr_mes);
											StrCopySans0Terminateur((char *)(pbBufFic+1), ptr_mes);
											pbBufFic += TAILLE_MESSAGE_DANS_FICHIER;
											}
											break;
										case FORMAT_CSV:
											{
											StrCopySans0Terminateur((char *)pbBufFic, ptr_mes);
											pbBufFic += StrLength (ptr_mes);
											}
											break;

										case FORMAT_BINAIRE_8_BITS:
											// Pas de messages en format binaire
							        maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
											break;
										} // switch format cour
									}
									break; // c_res_message
								} // switch ptr_var.genre

							// Ajout du s�parateur
							switch (nFormatFichierCourant)
								{
								case FORMAT_ASCII:
									{
									*pbBufFic = (BYTE)c_car_cr;
									pbBufFic++;
									*pbBufFic = (BYTE)c_car_lf;
									pbBufFic++;
									}
									break;

								case FORMAT_CSV:
									{
									//ajoute s�parateur ou Cr Lf
									if (i == pFichierDQ->pos_fin_var)
										{
										*pbBufFic = (BYTE)c_car_cr;
										pbBufFic++;
										*pbBufFic = (BYTE)c_car_lf;
										pbBufFic++;
										}
									else
										{
										*pbBufFic = (BYTE)Appli.cSeparateurListe;
										pbBufFic++;
										}
									}
								}
							}
						} // for i <= pFichierDQ->pos_fin_var

					// Calcul de la taille de cet enregistrement
					DWORD dwNbEnrDansEcriture = 1;
					if (!bFormatFixe)
						dwNbEnrDansEcriture = (PCSTR)pbBufFic - (PCSTR)pFichierDQ->pBuffer;

					// Mise � jour du pointeur d'�criture
					if (bPointeurEcritureExiste)
						{
						uFicPointeEcriture (hdl, (DWORD) (*ptr_ecr)); // $$ err
						(*ptr_ecr) += dwNbEnrDansEcriture;
						}

					// �criture
					erreur_sys = uFicEcrireTout (hdl, pFichierDQ->pBuffer, dwNbEnrDansEcriture);

					// Variable de taille existe ?
					if (pFichierDQ->dwPosESTaille != 0 )
						{
						// oui => mise � jour de la taille
						uFicNbEnr (hdl, &dwNbEnr);
						CPcsVarEx::SetValVarNumEx (pFichierDQ->dwPosESTaille,(FLOAT) dwNbEnr);
						} // existe var taille
					} // format ok
				else
					{
	        maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
					}
      	} // fNEnrEcriture ecriture ok
      else
        { // mauvais fNEnrEcriture ecriture
        maj_status (STATUT_DQ_ERR_N_ENR_ECR, nom_fichier);
        }
      } // fichier ouvert
    else
      { // fichier ferme
      maj_status (STATUT_DQ_ERR_OUVERTURE, nom_fichier);
      } // fichier ferme
    }
  else
    {
    maj_status (STATUT_DQ_ERR_NOM_FIC, nom_fichier);
    }
  if (erreur_sys != 0)
    {
    maj_status ((DWORD)STATUT_DQ_ERR_OS, nom_fichier);
    maj_status_systeme (erreur_sys);
    }
  }


//--------------------------------------
// lecture sur disque
void scrute_lit_dq (char *nom_fichier)
  {
  DWORD erreur_sys = 0;

	// Fichier sp�cifi� existe ?
	PFICHIER_DQ pFichierDQ = pTrouveFichierDQ(nom_fichier);
  if (pFichierDQ)
    {
		// oui => fichier ouvert ?
    if (pFichierDQ->bOuvert)
      {
			// oui : fichier non vide ?
			HFDQ hdl = pFichierDQ->f_handle ;
			DWORD	dwNbEnr;
			uFicNbEnr (hdl, &dwNbEnr);
			FLOAT fNEnrFinFichier = (FLOAT) dwNbEnr; 
      if (fNEnrFinFichier >= c_pointeur_min)
        { 
				// R�cup�re le Num�ro d'Enregistrement � lire
				t_tableau_vars_fichiers *ptr_var = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq, pFichierDQ->pos_debut_var);
				FLOAT *ptr_lec;
				FLOAT fNEnrLecture;
				BOOL bPointeurLectureExiste = (pFichierDQ->dwPosESLecture != 0);
        if (bPointeurLectureExiste )
          {
					// acc�s  direct
          ptr_lec = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pFichierDQ->dwPosESLecture);
          fNEnrLecture = (*ptr_lec);
          }
        else
          { 
					// acc�s s�quentiel
					DWORD	dwNEnr;
					uFicPointeurLecture (hdl, &dwNEnr);
          fNEnrLecture = (FLOAT) dwNEnr;
          }

				// Num�ro d'enregistrement � lire Ok ?
        if ((fNEnrLecture >= c_pointeur_min) && (fNEnrLecture <= c_pointeur_max) && (fNEnrLecture <= fNEnrFinFichier) )
          { 
					// oui => format courant de fichier Ok ?
					FORMAT_FICHIER nFormatFichierCourant;
					BOOL bFormatFixe;
					if (bGetFormatFichierCourant(nFormatFichierCourant, bFormatFixe))
						{
						// oui => pointe et lit (format fixe ou variable)
						if (bPointeurLectureExiste)
							{
							uFicPointeLecture (hdl, (DWORD)(*ptr_lec));
							}
						if (bFormatFixe)
							erreur_sys = uFicLireTout (hdl, pFichierDQ->pBuffer, 1);
						else
							erreur_sys = uFicLireLn (hdl, (PSTR)pFichierDQ->pBuffer, uMemTaille(pFichierDQ->pBuffer));

						// Lecture Ok ?
						if (erreur_sys == 0)
							{
							// oui => note la nouvelle position du pointeur de lecture
							if (bPointeurLectureExiste)
								{
								if (bFormatFixe)
									(*ptr_lec)++;
								else
									{
									DWORD dwNOctet = 0;
									uFicPointeurLecture (hdl, &dwNOctet);
									*ptr_lec = (FLOAT)dwNOctet;
									}
								}
							// Analyse l'enregistrement lu, �l�ment de variable apr�s �l�ment de variable
							PBYTE pbBufFic = (PBYTE)pFichierDQ->pBuffer;
							for (DWORD i = pFichierDQ->pos_debut_var; (i <= pFichierDQ->pos_fin_var); i++)
								{
								ptr_var = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq, i);
								DWORD taille_tab = 1;
								if (ptr_var->taille != 0)
									{
									taille_tab = ptr_var->taille;
									}

								for (DWORD pos_in_tab = ptr_var->pos; (pos_in_tab <= (ptr_var->pos + taille_tab - 1)); pos_in_tab++ )
									{
									DWORD dwNbCarsChamp = 0;

									// Format variable ?
									if (!bFormatFixe)
										{
										// oui => d�termine la longueur du champ en cours d'analyse (0 si plus de champ)
										// Reste des caract�res � lire ?
										if ((PSTR)pbBufFic < ((PSTR)pFichierDQ->pBuffer + StrLength((PSTR)pFichierDQ->pBuffer)))
											{
											// oui => cherche le s�parateur de champ
											dwNbCarsChamp = StrSearchChar((PSTR)pbBufFic, Appli.cSeparateurListe);
											if (dwNbCarsChamp == STR_NOT_FOUND)
												dwNbCarsChamp = StrLength((PSTR)pbBufFic);
											}
										else
											{
									    maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
											erreur_sys = ERROR_INVALID_DATA;
											break;
											}
										}

									char *ptr_fen_mes;
									switch (ptr_var->genre)
										{
										case c_res_logique :
											{
											BOOL *ptr_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, pos_in_tab);
											switch (nFormatFichierCourant)
												{
												case FORMAT_ASCII:
													{
													ptr_fen_mes = (char *)pbBufFic + 1; // le premier octet est un espace. On accede au deuxieme
													if ((*ptr_fen_mes) == '1')
														{
														(*ptr_log) = TRUE;
														}
													else
														{
														(*ptr_log) = FALSE;
														}
													pbBufFic += 2;
													pbBufFic += 2;
													}
													break;

												case FORMAT_CSV:
													{
													// R�cup�re le champ, enl�ve espaces et tabulations devant et derri�re...
													char szTemp[CSV_TAILLE_MAX_CHAMP_MESSAGE];
													StrCopyUpToLength (szTemp, (PSTR)pbBufFic, MIN(dwNbCarsChamp, CSV_TAILLE_MAX_CHAMP_MESSAGE-1));
													StrDeleteFirstAndLastSpacesAndTabs(szTemp);
							            //bMotReserve (c_res_zero, szB);
							            //bMotReserve (c_res_un, szH);

													// d�code le champ ou erreur
													if (bStrIEgales(szTemp, "0") || bStrIEgales(szTemp, "false") || bStrIEgales(szTemp, "faux"))
														{
														(*ptr_log) = FALSE;
														}
													else
														{
														if (bStrIEgales(szTemp, "1") || bStrIEgales(szTemp, "-1") || bStrIEgales(szTemp, "true") || bStrIEgales(szTemp, "vrai"))
															{
															(*ptr_log) = TRUE;
															}
														else
															{
															maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
															erreur_sys = ERROR_INVALID_DATA;
															}
														}

													pbBufFic += (dwNbCarsChamp + 1);
													}
													break;

												case FORMAT_BINAIRE_8_BITS:
													{
													erreur_sys = ERROR_INVALID_DATA;
												  maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
													}
													break;

												default:
													{
													BYTE *ptr_fen_byte = pbBufFic; // valeur sur le premier octet 
													if ((*ptr_fen_byte) == 1)
														{
														(*ptr_log) = TRUE;
														}
													else
														{
														(*ptr_log) = FALSE;
														}
													pbBufFic += 2;
													}
													break;
												}
											}
											break; // c_res_logique

										case c_res_numerique :
											{
											FLOAT * ptr_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_in_tab);
											switch (nFormatFichierCourant)
												{
												case FORMAT_BASIC :
													// $$fonctions de conversions non support�es par VC 4.0 suppos� conforme IEEE
													//ptr_fen_num = (FLOAT *)pbBufFic;
													//fmsbintoieee (ptr_fen_num, ptr_num);
													//pbBufFic += sizeof(FLOAT);
													//break;
												case FORMAT_MS :
													{
													FLOAT * ptr_fen_num = (PFLOAT)pbBufFic;
													(*ptr_num) = (*ptr_fen_num);
													pbBufFic += sizeof (FLOAT);
													}
													break;

												case FORMAT_TURBO :
													{
													t_turbo_r32 *ptr_fen_turbo = (t_turbo_r32 *)pbBufFic;
													(*ptr_num) = turbo2ms (ptr_fen_turbo, nom_fichier);
													pbBufFic += sizeof(t_turbo_r32);
													}
													break;

												case FORMAT_ASCII :
													{
													ptr_fen_mes = (char *)pbBufFic;
													char mes_tempo[20];
													DWORD j = 0;
													for (j = 0 ; (j < 15); j++, ptr_fen_mes++)
														{
														mes_tempo[j] = (*ptr_fen_mes);
														}
													StrSetLength (mes_tempo, j);
													(*ptr_num) = (FLOAT) 0;
													if (!StrToFLOAT(ptr_num, mes_tempo))
														{
												    maj_status (STATUT_DQ_ERR_NUMERIQUE_INVALIDE, nom_fichier);
														}
													pbBufFic += 2;
													pbBufFic += 15;
													}
													break;
												case FORMAT_CSV:
													{
													// R�cup�re le champ, enl�ve espaces et tabulations devant et derri�re puis convertid
													char szTemp[CSV_TAILLE_MAX_CHAMP_MESSAGE];
													StrCopyUpToLength (szTemp, (PSTR)pbBufFic, MIN(dwNbCarsChamp, CSV_TAILLE_MAX_CHAMP_MESSAGE-1));
													StrDeleteFirstAndLastSpacesAndTabs(szTemp);
													(*ptr_num) = (FLOAT) 0;
													if (!StrToFLOAT(ptr_num, szTemp))
														{
														erreur_sys = ERROR_INVALID_DATA;
												    maj_status (STATUT_DQ_ERR_NUMERIQUE_INVALIDE, nom_fichier);
														}

													pbBufFic += (dwNbCarsChamp + 1);
													}
													break;
												case FORMAT_BINAIRE_8_BITS:
													{
													BYTE * pByte = (PBYTE)pbBufFic;
													(*ptr_num) = *pByte;
													pbBufFic += sizeof (BYTE);
													}
													break;
												} // switch format
											}
											break; // c_res_numerique

										case c_res_message :
											{
											PSTR ptr_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, pos_in_tab);
											ptr_fen_mes = (PSTR)pbBufFic;
											switch (nFormatFichierCourant)
												{
												case FORMAT_MS:
												case FORMAT_TURBO :
													{
													for (DWORD j = 0 ; (j < 81); j++)
														{
														ptr_mes[j] = ptr_fen_mes[j+1];
														}
													StrSetLength (ptr_mes, StrGetChar (ptr_fen_mes, 0));
													pbBufFic += TAILLE_MESSAGE_DANS_FICHIER;
													}
													break;
												case FORMAT_BASIC :
												case FORMAT_ASCII :
													{
													// recopie la chaine de caract�re lue sauf les espaces de fin
													// $$ avant, laissait un espace en fin de chaine
													int nNbCars=0;
													for (nNbCars = (TAILLE_MESSAGE_DANS_FICHIER - 2); nNbCars > 0; nNbCars--)
														{
														if (ptr_fen_mes [nNbCars-1] != ' ' )
															break;
														}
													for (int j = 0; j < nNbCars; j++)
														{
														ptr_mes[j] = ptr_fen_mes[j];
														}
													ptr_mes[nNbCars] = '\0';
													pbBufFic += TAILLE_MESSAGE_DANS_FICHIER;
													}
													break;
												case FORMAT_CSV:
													{
													// Copie le champ � concurrence de la taille maxi autoris�e
													StrCopyUpToLength (ptr_mes, (PSTR)pbBufFic, MIN(dwNbCarsChamp, c_nb_car_ex_mes-1));
													pbBufFic += (dwNbCarsChamp + 1);
													}
													break;
												case FORMAT_BINAIRE_8_BITS:
													{
													erreur_sys = ERROR_INVALID_DATA;
												  maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
													}
													break;
												} // switch format_courant
											}
											break; // c_res_message
										} // switch ptr_var.genre
									} // for pos_in_tab
								if (erreur_sys != 0)
									break;
								} // for i <= pFichierDQ->pos_fin_var
							}
						} // if (bGetFormatFichierCourant...
					else
						{
	          maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
						}
          } // fNEnrLecture lecture ok
        else
          { // mauvais fNEnrLecture
	        maj_status (STATUT_DQ_ERR_N_ENR_LEC, nom_fichier);
          }
      	} // fichier non vide
      else
        { // fichier vide
        maj_status (STATUT_DQ_ERR_FICHIER_VIDE, nom_fichier);
        } // fichier vide
      } // fichier ouvert
    else
      { // fichier non ouvert
      maj_status (STATUT_DQ_ERR_OUVERTURE, nom_fichier);
      } // fichier non ouvert
    }
  else
    { // nom de fichier inexistant
    maj_status (STATUT_DQ_ERR_NOM_FIC, nom_fichier);
    }
  if (erreur_sys != 0 )
    {
    maj_status ((DWORD)STATUT_DQ_ERR_OS, nom_fichier);
    maj_status_systeme (erreur_sys);
    }
  }

//-----------------------------------------
// Ouverture d'un fichier
void scrute_ouvre_dq (char *nom_fichier)
  {
  DWORD erreur_sys = 0;

	// Fichier sp�cifi� existe ?
	PFICHIER_DQ pFichierDQ = pTrouveFichierDQ(nom_fichier);
  if (pFichierDQ)
    {
		// format courant de fichier Ok ?
		FORMAT_FICHIER nFormatFichierCourant;
		BOOL bFormatFixe;
		if (bGetFormatFichierCourant(nFormatFichierCourant, bFormatFixe))
			{
			// oui => fichier pas encore ouvert ?
			if (!pFichierDQ->bOuvert)
				{
				DWORD dwTailleEnr = 0;

				// D�termine la taille d'un enregistrement (= taille totale des vars lues ou ecrites en une op�ration)
				for (DWORD pointeur_var = pFichierDQ->pos_debut_var; (pointeur_var <= pFichierDQ->pos_fin_var); pointeur_var++)
					{
					t_tableau_vars_fichiers *vars_fichiers = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq, pointeur_var);
					DWORD taille_tab = 1;
					if (vars_fichiers->taille != 0)
						{
						taille_tab = vars_fichiers->taille;
						}

					switch (vars_fichiers->genre)
						{
						case c_res_logique :
							{
							switch (nFormatFichierCourant)
								{
								case FORMAT_MS:
								case FORMAT_BASIC:
								case FORMAT_TURBO :
									dwTailleEnr += (2 * taille_tab);
									break;
								case FORMAT_ASCII :
									dwTailleEnr += (4 * taille_tab);
									break;
								case FORMAT_CSV :
									dwTailleEnr += (CSV_TAILLE_MAX_CHAMP_LOGIQUE * taille_tab);
									break;
								case FORMAT_BINAIRE_8_BITS:
									// Pas de messages en format binaire
							    maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
									break;
								default:
									maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
									break;
								} // switch format
							}
							break;

						case c_res_numerique:
							{
							switch (nFormatFichierCourant)
								{
								case FORMAT_MS:
								case FORMAT_BASIC :
									dwTailleEnr += (4 * taille_tab);
									break;
								case FORMAT_TURBO :
									dwTailleEnr += (6 * taille_tab);
									break;
								case FORMAT_ASCII :
									dwTailleEnr += (17 * taille_tab);
									break;
								case FORMAT_CSV :
									dwTailleEnr += (CSV_TAILLE_MAX_CHAMP_NUMERIQUE * taille_tab);
									break;
								case FORMAT_BINAIRE_8_BITS:
									dwTailleEnr += (sizeof(BYTE) * taille_tab);
									break;
								default:
									maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
									break;
								} // switch format
							}
							break;

						case c_res_message :
							{
							switch (nFormatFichierCourant)
								{
								case FORMAT_MS:
								case FORMAT_BASIC:
								case FORMAT_TURBO:
								case FORMAT_ASCII :
									dwTailleEnr += (TAILLE_MESSAGE_DANS_FICHIER * taille_tab);
									break;
								case FORMAT_CSV :
									dwTailleEnr += (CSV_TAILLE_MAX_CHAMP_MESSAGE * taille_tab);
									break;
								case FORMAT_BINAIRE_8_BITS:
									// Pas de messages en format binaire
							    maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
									break;

								default:
									maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
									break;
								} // switch format
							}
							break;
						} //switch
					} // for pointeur_var

				// La taille d'un enr est connue : ouverture ou cr�ation du fichier
				char szNomFicTemp[MAX_PATH];
				CopiePathNameAbsoluAuDoc (szNomFicTemp, nom_fichier);

				HFDQ hdl = 0;
				if (bFormatFixe)
					erreur_sys = uFicOuvre (&hdl, szNomFicTemp, dwTailleEnr);
				else
					erreur_sys = uFicTexteOuvre (&hdl, szNomFicTemp);

				if (erreur_sys != 0)
					{
					pFichierDQ->bOuvert = FALSE;
					}
				else
					{
					pFichierDQ->bOuvert = TRUE;
					pFichierDQ->f_handle = hdl;
					pFichierDQ->pBuffer = pMemAlloue (dwTailleEnr);
					if (pFichierDQ->dwPosESTaille != 0)
						{
						DWORD	dwNbEnr;
						uFicNbEnr (hdl, &dwNbEnr);
						CPcsVarEx::SetValVarNumEx(pFichierDQ->dwPosESTaille,(FLOAT)dwNbEnr);
						CPcsVarEx::SetValMemVarNumEx(pFichierDQ->dwPosESTaille,(FLOAT)dwNbEnr);
/*
						PFLOAT pfloat = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pFichierDQ->dwPosESTaille);
						(*pfloat) = (FLOAT)dwNbEnr;
						pfloat = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pFichierDQ->dwPosESTaille);
						(*pfloat) = (FLOAT)dwNbEnr;
*/
						}
					}
				}
			}
		else
			{
	    maj_status (STATUT_DQ_ERR_FORMAT, nom_fichier);
			}
    }
  else
    { // nom de fichier inexistant
    maj_status (STATUT_DQ_ERR_NOM_FIC, nom_fichier);
    }
  if (erreur_sys != 0)
    {
    maj_status ((DWORD)STATUT_DQ_ERR_OS, nom_fichier);
    maj_status_systeme (erreur_sys);
    }
  }

//---------------------------------------
// fermeture fichier
void scrute_ferme_dq (char *nom_fichier)
  {
  DWORD erreur_sys = 0;

	// Fichier sp�cifi� n'existe pas (avec fermeture) ?
	if (!bTrouveEtFermeFichierDQ(nom_fichier, erreur_sys))
    { 
		// non => erreur
    maj_status (STATUT_DQ_ERR_NOM_FIC, nom_fichier);
    }
  if (erreur_sys != 0)
    {
    maj_status ((DWORD)STATUT_DQ_ERR_OS, nom_fichier);
    maj_status_systeme (erreur_sys);
    }
  }

// --------------------------------------------------------------------------
// effacement fichier
// --------------------------------------------------------------------------
void efface_dq (char *nom_fichier)
  {
  DWORD erreur_sys = 0;

	// Nom fichier valide ?
  if (bFicNomValide (nom_fichier))
    {
		// oui => ferme le fichier si n�cessaire ...
		bTrouveEtFermeFichierDQ(nom_fichier, erreur_sys);

		// puis efface le
		char szNomFicTemp[MAX_PATH];
		CopiePathNameAbsoluAuDoc (szNomFicTemp, nom_fichier);
    erreur_sys = uFicEfface (szNomFicTemp);
    }
  else
    {
    maj_status (STATUT_DQ_ERR_NOM_FIC, nom_fichier);
    }
  if (erreur_sys != 0)
    {
    maj_status ((DWORD)STATUT_DQ_ERR_OS, nom_fichier);
    maj_status_systeme (erreur_sys);
    }
  }

// --------------------------------------------------------------------------
// copie fichier
// --------------------------------------------------------------------------
void copie_dq (char * fichier_source, char *fichier_destination)
  {
  DWORD erreur_sys = 0;

	// fichier source valide ?
  if (bFicNomValide (fichier_source))
    {
		char szNomFicSourceTemp[MAX_PATH];
		CopiePathNameAbsoluAuDoc (szNomFicSourceTemp, fichier_source);

		// oui => fichier source existe ?
    if (bFicPresent (szNomFicSourceTemp))
      {
			// oui => fichier dest valide ?
      if (bFicNomValide (fichier_destination))
        {
				// oui => ferme si n�cessaire source et destination...
				bTrouveEtFermeFichierDQ(fichier_source, erreur_sys);
				bTrouveEtFermeFichierDQ(fichier_destination, erreur_sys);

				// et copie le fichiers
				char szNomFicDestinationTemp[MAX_PATH];
				CopiePathNameAbsoluAuDoc (szNomFicDestinationTemp, fichier_destination);

        erreur_sys = uFicCopie (szNomFicSourceTemp, szNomFicDestinationTemp, FALSE);
        }
      else
        {
        maj_status (STATUT_DQ_ERR_NOM_FIC | STATUT_DQ_ERR_COPIE, fichier_destination);
        }
      }
    else
      {
      maj_status (STATUT_DQ_ERR_COPIE, fichier_source);
      }
    }
  else
    {
    maj_status (STATUT_DQ_ERR_NOM_FIC | STATUT_DQ_ERR_COPIE, fichier_source);
    }

  if (erreur_sys != 0)
    {
		char nom_fichier[(2 * 82) + 3];  // Nom_Fic_Src / Nom_Fic_Dest $$
    StrCopy (nom_fichier, fichier_source);
    StrConcat (nom_fichier, " / ");
    StrConcat (nom_fichier, fichier_destination);
    StrSetLength (nom_fichier, 80);
    maj_status ((DWORD)STATUT_DQ_ERR_OS, nom_fichier);
    maj_status_systeme (erreur_sys);
    }
  }

// --------------------------------------------------------------------------
// renomme fichier
// --------------------------------------------------------------------------
void renomme_dq (char *fichier_source, char *fichier_destination)
  {
  DWORD erreur_sys = 0;

	// Fichier source valide ?
	if (bFicNomValide (fichier_source))
    {
		// oui => fichier source pr�sent ?
		char szNomFicSourceTemp[MAX_PATH];
		CopiePathNameAbsoluAuDoc (szNomFicSourceTemp, fichier_source);
    if (bFicPresent (szNomFicSourceTemp))
      {
			// oui => nom fichier dest valide ?
      if (bFicNomValide (fichier_destination))
        {
				// oui => ferme source et dest si n�cessaire...
				bTrouveEtFermeFichierDQ(fichier_source, erreur_sys);
				bTrouveEtFermeFichierDQ(fichier_destination, erreur_sys);

				// renomme source en dest
				char szNomFicDestinationTemp[MAX_PATH];
				CopiePathNameAbsoluAuDoc (szNomFicDestinationTemp, fichier_destination);
        erreur_sys = uFicRenomme (szNomFicSourceTemp, szNomFicDestinationTemp);
        }
      else
        {
        maj_status (STATUT_DQ_ERR_NOM_FIC | STATUT_DQ_ERR_RENOMME, fichier_destination);
        }
      }
    else
      {
      maj_status (STATUT_DQ_ERR_RENOMME, fichier_source);
      }
    }
  else
    {
    maj_status (STATUT_DQ_ERR_NOM_FIC | STATUT_DQ_ERR_RENOMME, fichier_source);
    }

  if (erreur_sys != 0 )
    {
		char nom_fichier[(2 * 82) + 3];  // Nom_Fic_Src / Nom_Fic_Dest
    StrCopy (nom_fichier, fichier_source);
    StrConcat (nom_fichier, " / ");
    StrConcat (nom_fichier, fichier_destination);
    StrSetLength (nom_fichier, 80);
    maj_status ((DWORD)STATUT_DQ_ERR_OS, nom_fichier);
    maj_status_systeme (erreur_sys);
    }
  }

// ---------------------------------------------------------------------------
void initialise_dq (void)
  {
  lance_trap ();
  }

// ---------------------------------------------------------------------------
void ferme_disc (void)
  {
  DWORD erreur_sys = 0;
  if (existe_repere (bx_disq))
    {
		DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_disq);
    for (DWORD index = 1; (index <= nbr_enr); index++)
      {
			PFICHIER_DQ pFichierDQ = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq, index);
      if (pFichierDQ->bOuvert)
        {
        erreur_sys = uFicFerme (&pFichierDQ->f_handle);
        if (erreur_sys == 0)
          {
          MemLibere (&pFichierDQ->pBuffer);
          pFichierDQ->bOuvert = FALSE;
          }
        }
      }
    reinit_driv_dq_c ();
    }
  arrete_trap ();
  }

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il demeure sa propriete exclusive et est confidentiel.             |
 |   Aucune diffusion n'est possible sans accord ecrit.                 |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Driv_sys.c  acc�s en modification de quelques variables syst�me
 |   Permet la relation entre les variables systemes de la base de donnees
 |   et le menu utilisateur. Toute action du menu est stock�e dans une 
 |   variable statique. Dans recoit_tm de scrut_tm la variable statique est 
 |   parcourue et les va systemes correspondantes misent a jour. 
 |   JS WIN32 12/10/97
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "MemMan.h"
#include "UStr.h"
#include "lng_res.h"
#include "tipe.h"        // Types + Constantes Processyn
#include "mem.h"         // Memory Manager
#include "pcs_sys.h"
#include "G_Objets.h"
#include "g_sys.h"
#include "BdGr.h"
#include "UChrono.h"
#include "tipe_gr.h"

#include "Driv_sys.h"
#include "Verif.h"
VerifInit;

// objet de modification des variables syst�mes � l'ex�cution
typedef struct
  {
  DWORD  pos_var [NB_VAR];
  DWORD  type [NB_VAR];
  DWORD  sens [NB_VAR];
  BOOL   change [NB_VAR];
  void   *ptr_zone[NB_VAR];
  DWORD  taille_zone [NB_VAR];
  BOOL   bLog [NB_VAR];        // var logiques
  DWORD  bNum [NB_VAR];        // var numeriques
  char   bMes [82][NB_VAR];    // var Message
  } EXE_MODIFIE_VAR_SYS;

static EXE_MODIFIE_VAR_SYS ExeModifieVarSys;

// --------------------------------------------------------------------------
void VarSysExeEcritLog (DWORD dwIdVarSys, BOOL SysValue)
  {
  ExeModifieVarSys.change [dwIdVarSys] = TRUE;
  ExeModifieVarSys.bLog  [dwIdVarSys] = SysValue;
  }

// --------------------------------------------------------------------------
void VarSysExeEcritZone (DWORD dwIdVarSys, void *ptr_zone,DWORD taille)
  {
  ExeModifieVarSys.change [dwIdVarSys] = TRUE;
  ExeModifieVarSys.taille_zone[dwIdVarSys] = taille;
  ExeModifieVarSys.ptr_zone[dwIdVarSys] = ptr_zone;
  }

// --------------------------------------------------------------------------
BOOL bVarSysExeEstModifiee (DWORD dwIdVarSys)
  {
  return ExeModifieVarSys.change[dwIdVarSys];
  }

// --------------------------------------------------------------------------
DWORD dwPosVarSysExe (DWORD dwIdVarSys)
  {
  return ExeModifieVarSys.pos_var[dwIdVarSys];
  }

// --------------------------------------------------------------------------
DWORD dwVarSysExeGetType (DWORD dwIdVarSys)
  {
  return ExeModifieVarSys.type[dwIdVarSys];
  }

// --------------------------------------------------------------------------
DWORD dwVarSysExeGetSens (DWORD dwIdVarSys)
  {
  return ExeModifieVarSys.sens[dwIdVarSys];
  }

// --------------------------------------------------------------------------
BOOL bVarSysExeReadLog (DWORD dwIdVarSys)
  {
  ExeModifieVarSys.change [dwIdVarSys] = FALSE;

  return ExeModifieVarSys.bLog [dwIdVarSys];
  }

// --------------------------------------------------------------------------
DWORD dwVarSysExeReadNum (DWORD dwIdVarSys)
  {
  ExeModifieVarSys.change [dwIdVarSys] = FALSE;

  return ExeModifieVarSys.bNum [dwIdVarSys];
  }

// --------------------------------------------------------------------------
PCSTR pszVarSysExeReadMess (DWORD dwIdVarSys)
  {
  ExeModifieVarSys.change [dwIdVarSys] = FALSE;

  return ExeModifieVarSys.bMes [dwIdVarSys];
  }

// --------------------------------------------------------------------------
DWORD dwVarSysExeReadPtr (DWORD dwIdVarSys,void **ptr_zone)
  {
  ExeModifieVarSys.change [dwIdVarSys] = FALSE;
  (*ptr_zone) = ExeModifieVarSys.ptr_zone[dwIdVarSys];
  return ExeModifieVarSys.taille_zone[dwIdVarSys];
  }

// ---------------------------------------------------------------------------
// Parcours des pages de synoptique � l'ex�cution (de 1 � n): 
// R�cup�re les infos de la la premi�re page existante, � partir de la page sp�cifi�e
// *pdwNPage vaut le num�ro de page suivante ou 0 s'il n'y en a pas
void  initialise_num_page_dlg (DWORD * pdwNPage, BOOL * pbPageVisible, PSTR pszTitrePage)
  {
  DWORD		dwNbPages = 0;
  BOOL		bPageTrouvee = FALSE;

  if (existe_repere (bx_vdi_nom_page))
    dwNbPages = nb_enregistrements (szVERIFSource, __LINE__, bx_vdi_nom_page);

	// Trouve, � partir de la page sp�cifi�e, la premi�re page existante
  while  (((*pdwNPage) <= dwNbPages) && (!bPageTrouvee))
    {
		// r�cup�re les infos de la page examin�e
    tx_vdi_nom_page* ptr_donnees_page = (tx_vdi_nom_page*)pointe_enr (szVERIFSource, __LINE__, bx_vdi_nom_page, *pdwNPage);

		// page existe ?
    if (ptr_donnees_page->existe)
      {
			// oui => r�cup�re ses infos
			PX_VAR_SYS	pxva_sys = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme,c_sys_fen_visible);
			PBOOL	ptr_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pxva_sys->dwPosCourEx) + (*pdwNPage) - 1);

      *pbPageVisible = *ptr_log;
      StrCopy (pszTitrePage, ptr_donnees_page->titre_page);
      bPageTrouvee = TRUE;
      }
		// page suivante
    (*pdwNPage)++;
    } // while  (((*pdwNPage) <= dwNbPages) && (!bPageTrouvee))

	// page trouv�e ?
  if (!bPageTrouvee)
		// non => note l'�chec
    *pdwNPage = 0;

  } // initialise_num_page_dlg


// -----------------------------------------------------------------------
// Initialisation des structures de modification des variables syst�me
void VarSysExeInit (void)
  {
  DWORD w;

  for (w = 0; w < NB_VAR; w++)
    {
    ExeModifieVarSys.change[w] = FALSE;
    }

	// Position
  ExeModifieVarSys.pos_var[IdxVarSysExeArretExploitation    ]=arret_exploitation;
  ExeModifieVarSys.pos_var[IdxVarSysExeRetourSysteme        ]=retour_systeme;
  ExeModifieVarSys.pos_var[IdxVarSysExeApplicSuivante       ]=applic_suivante;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenVisible     ]=c_sys_fen_visible;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenX           ]=c_sys_fen_x;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenY           ]=c_sys_fen_y;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenCx          ]=c_sys_fen_cx;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenCy          ]=c_sys_fen_cy;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenEtat        ]=c_sys_fen_etat;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenImpression  ]=c_sys_fen_impression;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenNomDocument]=c_sys_fen_nom_document;
  ExeModifieVarSys.pos_var[IdxVarSysExeEffaceVisu           ]=efface_visu;
  ExeModifieVarSys.pos_var[IdxVarSysExeAlarmeVisible            ]=al_visible;
  ExeModifieVarSys.pos_var[IdxVarSysExeFenUser        ]=c_sys_fen_user;

	// Type
  ExeModifieVarSys.type[IdxVarSysExeArretExploitation    ]= c_res_logique;
  ExeModifieVarSys.type[IdxVarSysExeRetourSysteme        ]= c_res_logique;
  ExeModifieVarSys.type[IdxVarSysExeApplicSuivante       ]= c_res_message;
  ExeModifieVarSys.type[IdxVarSysExeFenVisible     ]= c_res_logique;
  ExeModifieVarSys.type[IdxVarSysExeFenX           ]= c_res_numerique;
  ExeModifieVarSys.type[IdxVarSysExeFenY           ]= c_res_numerique;
  ExeModifieVarSys.type[IdxVarSysExeFenCx          ]= c_res_numerique;
  ExeModifieVarSys.type[IdxVarSysExeFenCy          ]= c_res_numerique;
  ExeModifieVarSys.type[IdxVarSysExeFenEtat        ]= c_res_numerique;
  ExeModifieVarSys.type[IdxVarSysExeFenImpression  ]= c_res_numerique;
  ExeModifieVarSys.type[IdxVarSysExeFenNomDocument]= c_res_message;
  ExeModifieVarSys.type[IdxVarSysExeEffaceVisu           ]= c_res_logique;
  ExeModifieVarSys.type[IdxVarSysExeAlarmeVisible            ]= c_res_logique;
  ExeModifieVarSys.type[IdxVarSysExeFenUser     ]= c_res_numerique;

	// sens
  ExeModifieVarSys.sens[IdxVarSysExeArretExploitation			]= c_res_e_et_s;
  ExeModifieVarSys.sens[IdxVarSysExeRetourSysteme					]= c_res_e_et_s;
  ExeModifieVarSys.sens[IdxVarSysExeApplicSuivante	]= c_res_e_et_s;
  ExeModifieVarSys.sens[IdxVarSysExeEffaceVisu							]= c_res_e_et_s;
  ExeModifieVarSys.sens[IdxVarSysExeAlarmeVisible							]= c_res_e_et_s;
  ExeModifieVarSys.sens[IdxVarSysExeFenVisible       ]= c_res_variable_ta_es;
  ExeModifieVarSys.sens[IdxVarSysExeFenImpression    ]= c_res_variable_ta_es;
  ExeModifieVarSys.sens[IdxVarSysExeFenNomDocument  ]= c_res_variable_ta_es;
  ExeModifieVarSys.sens[IdxVarSysExeFenX            ]= c_res_variable_ta_es;
  ExeModifieVarSys.sens[IdxVarSysExeFenY            ]= c_res_variable_ta_es;
  ExeModifieVarSys.sens[IdxVarSysExeFenCx           ]= c_res_variable_ta_es;
  ExeModifieVarSys.sens[IdxVarSysExeFenCy           ]= c_res_variable_ta_es;
  ExeModifieVarSys.sens[IdxVarSysExeFenEtat         ]= c_res_variable_ta_es;
  ExeModifieVarSys.sens[IdxVarSysExeFenUser         ]= c_res_variable_ta_e;
  } // VarSysExeInit

// ------------------------ fin Driv_sys.c -----------------------------




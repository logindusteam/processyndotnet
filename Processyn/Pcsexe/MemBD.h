// MemBD.h: interface for the CMemBD class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MEMBD_H__74962B3B_6DA4_4496_9B35_78AEE328D822__INCLUDED_)
#define AFX_MEMBD_H__74962B3B_6DA4_4496_9B35_78AEE328D822__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MemBloc.h"

//----------------------- Constantes ----------------------------------------
#define BLK_MAX_CHAR_NOM_BD 256

#define BLK_INSERT_END            0xffffffff
#define BLK_REC_BASE_SIZE         0xffffffff
// Taille de la table des blocs (�vite r�alloc en multi-thread)
#define NB_BLOCS_MIN 1000


class CMemBD  
	{
	public:
		CMemBD();
		CMemBD(PCSTR pszNomBD);// cr�e une base de donn�es vierge
		virtual ~CMemBD();

		//------------ bases de donn�es -----------------------------
		// cr�e une base de donn�es vierge
		//HBLK_BD hBlkCreeBDVierge (PCSTR pszNomBD);

		// lib�re et d�truit une base de donn�e (met � NULL *phbd)
		//void BlkFermeBD (HBLK_BD *phbd);
		// lib�re une base de donn�es
		void Ferme();

		// Renvoie la taille en octets d'une base de donn�es
		DWORD dwBlkTailleBD ();

		// Cr�e un bloc dans la base de donn�e
		PVOID pBlkCreeBloc (DWORD dwIdBloc, DWORD dwTailleEnr, DWORD dwNbEnr);

		// transferts bases de donn�es <-> fichiers :

		// Cr�e une base de donn�e � partir d'un fichier base de donn�es
		//HBLK_BD	hBlkChargeBD (PCSTR pszFile, PCSTR pszNomBD);
		void bBlkChargeBD (PCSTR pszFile, PCSTR pszNomBD);

		// Enregistre une base de donn�e sur un fichier base de donn�es
		//void BlkSauveBD (HBLK_BD hbd, PCSTR pszFile);
		void BlkSauveBD (PCSTR pszFile);

	private:
		// Propri�t�s
	  CHAR	m_szNomBD [BLK_MAX_CHAR_NOM_BD];// Nom de la base de donn�es
		DWORD	m_dwNbBlocsBD;									// Nombre de blocs dans apBlocsBD
		pCMemBloc m_apBlocsBD[NB_BLOCS_MIN+1];	// Tableau de 0 ou pointeur sur MemBlocs

		// M�thodes
		void Init(PCSTR pszNomBD);
		void BlkError 
			(
			//HBLK_BD hbd,
			DWORD   dwIdBloc,
			DWORD   dwNEnr,
			DWORD   dwNbEnr,
			DWORD   wError,
			PCSTR  pszFuncName,
			PCSTR  pszManagerName,
			DWORD dwNLine = 0, PCSTR pszSource = NULL);

	};

typedef CMemBD * pCMemBD;

#endif // !defined(AFX_MEMBD_H__74962B3B_6DA4_4496_9B35_78AEE328D822__INCLUDED_)

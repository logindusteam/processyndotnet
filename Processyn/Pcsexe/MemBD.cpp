// MemBD.cpp: implementation of the CMemBD class.
// Doit remplacer BlkMan (A FAIRE)
// Remarque : Les pCMemBlocs sont allou�s / lib�r�s en dynamique
// alors que dans la version initiale, ils sont allou�s en permanence
// avec bExiste = FALSE
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MemMan.h"
#include "UExcept.h"
#include "MemBD.h"

//----------------------- Erreurs -------------------------------------------
#define BLK_ERR_MEMORY                   0
#define BLK_ERR_ALREADY_EXIST            1
#define BLK_ERR_NOT_EXIST                2
#define BLK_ERR_RECORD_OVER_END          3
#define BLK_ERR_RECORD_OVERFLOW          4
#define BLK_ERR_RECORD_SIZE              5
#define BLK_ERR_RECORD_OVERLAP           6

#define BLK_ERR_READ_FILE                7
#define BLK_ERR_INVALID_FILE             8
#define BLK_ERR_VERSION_FILE             9
#define BLK_ERR_WRITE_FILE              10
#define BLK_ERR_OPEN_FILE               11
#define BLK_ERR_CLOSE_FILE              12
#define BLK_ERR_RECORD_SIZE_NOT_CONST   13
#define BLK_ERR_BAD_SIZE                14
//
#define BLK_MANAGER_NONE                NULL
#define BLK_BLOCK_NONE                  0xFFFFFFFF
#define BLK_RECORD_NONE                 0xFFFFFFFF

// Structures de fichiers bases de donn�es
#define BLK_MAX_CHAR_COPY_RIGHT        256
#define BLK_MEMORY_MANAGER_COPY_RIGHT "(c) Logique Industrie 1994"
#define BLK_MEMORY_MANAGER_VERSION_0   0x302E3130                    //'01.0'
#define BLK_MEMORY_MANAGER_VERSION_1   0x302E3131                    //'01.1'
#define BLK_MEMORY_MANAGER_VERSION     BLK_MEMORY_MANAGER_VERSION_0
//
#define BLK_TYPE_FILE_MANAGER_ARRAY    0x414D4654                    //'TFMA'
#define BLK_TYPE_FILE_MANAGER          0x204D4654                    //'TFM '
#define BLK_TYPE_FILE_BLOCK            0x20424654                    //'TFB '
#define BLK_TYPE_FILE_DATA             0x20444654                    //'TFD '

static PCSTR pszBlkCopyRight = BLK_MEMORY_MANAGER_COPY_RIGHT;
//
static PCSTR pszBlkManagerCreate = "hBlkCreeBDVierge";
static PCSTR pszBlkCreate        = "pBlkCreeBloc";
static PCSTR pszBlkDelete        = "BlkFermeBloc";
static PCSTR pszBlkRecCount      = "dwBlkNbEnr";
static PCSTR pszBlkRecBaseSize   = "dwBlkTailleEnrDeBase";
static PCSTR pszBlkRecSize       = "dwBlkTailleEnr";
static PCSTR pszBlkRecInsert     = "pBlkInsereEnr";
static PCSTR pszBlkRecDelete     = "BlkEnleveEnr";
static PCSTR pszBlkRecSetSize    = "pBlkChangeTailleEnr";
static PCSTR pszBlkRecPointer    = "pBlkPointeEnr";
static PCSTR pszBlkRecCopy       = "BlkCopieEnr";
//static PCSTR pszBlkRecXChange    = "BlkEchangeEnr";
static PCSTR pszLoad             = "Load function";
static PCSTR pszSave             = "Save function";

static PCSTR pszErrorMessage [] = 
	{
	"System memory allocation failure",
	"User is attempting to create an existing block",
	"User is attempting to reference a non-existant block",
	"User is attempting to access over the block end",
	"User has reached block limits",
	"User \"record size\" is smaler than \"block base size\"",
	"User is attempting to exchange overlaped regions",
	"Unable to read data-base file",
	"User file is not a data-base",
	"User is attempting to load a bad version of data-base",
	"Unable to write in data-base file",
	"Unable to open data-base file",
	"Unable to close data-base file",
	"User is attempting to save a block with non-constant \"record size\"",
	"User is attempting to load a block with a bad \"record size\""
	};


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMemBD::CMemBD()
	{
	Init("");
	}

CMemBD::CMemBD(PCSTR pszNomBD) // cr�e une base de donn�es vierge
	{
	Init(pszNomBD);
	}

CMemBD::~CMemBD()
	{
	// Lib�re la table de blocs
  if (m_dwNbBlocsBD > 0)
    {
    for (DWORD wIdxBlk = 0; wIdxBlk < m_dwNbBlocsBD; wIdxBlk++)
      {
			delete m_apBlocsBD [wIdxBlk];
			m_apBlocsBD [wIdxBlk]=NULL;
      }
    }
  m_dwNbBlocsBD=0;
	}

void CMemBD::Init(PCSTR pszNomBD)
	{
	pMemCopyToByte (m_szNomBD, pszNomBD, '\0', BLK_MAX_CHAR_NOM_BD);
	m_dwNbBlocsBD    = NB_BLOCS_MIN;
  //m_apBlocsBD      = (pCMemBloc)pMemAlloue ((m_dwNbBlocsBD + 1) * sizeof (pBloc));

	// Initialise la table des blocs
	// (pour �viter des redimensionnements ult�rieurs probl�matiques en multithread)
	for (DWORD dwNBloc = 0; dwNBloc <= NB_BLOCS_MIN; dwNBloc++)
		{
		m_apBlocsBD[dwNBloc]= new CMemBloc(dwNBloc);

		//pCMemBloc pBlk							= &pbd->apBlocsBD [dwNBloc];

		//pBlk->bExist						= FALSE;
		//pBlk->dwTailleEnrDeBase = 0;
		//pBlk->dwNbEnr						= 0;
		//pBlk->pHdrEnr						= NULL;
		}
	}

void CMemBD::Ferme()
	{
  if (m_dwNbBlocsBD > 0)
    {
    for (DWORD wIdxBlk = 0; wIdxBlk < m_dwNbBlocsBD; wIdxBlk++)
      {
			m_apBlocsBD [wIdxBlk]->Ferme();
      }
    //MemLibere ((PVOID*)(&pbd->apBlocsBD));
    }
	}

DWORD CMemBD::dwBlkTailleBD ()
	{
	DWORD dwRes = 0;

	for (DWORD dwNBloc = 0; dwNBloc <m_dwNbBlocsBD; dwNBloc++)
		{
		dwRes += m_apBlocsBD[dwNBloc]->dwTailleTotale();
		}
	return dwRes;
	}

// Cr�e un bloc dans la base de donn�e
PVOID CMemBD::pBlkCreeBloc (DWORD dwIdBloc, DWORD dwTailleEnr, DWORD dwNbEnr)
  {
  //-------------------------------------
  if (dwIdBloc >= m_dwNbBlocsBD)
    {
		// $$ Fixer au moins � la cr�ation de la BDD le Nb de blocs max
    BlkError (dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, m_szNomBD);
/* $$ interdit pour �viter pb acc�s multi-thread
    MemRealloue (&pbd->apBlocsBD, (dwIdBloc + 1) * sizeof (*pbd->apBlocsBD));
    do
      {
      pBlk = &pbd->apBlocsBD [pbd->dwNbBlocsBD];
      pBlk->bExist        = FALSE;
      pBlk->dwTailleEnrDeBase  = 0;
      pBlk->dwNbEnr				= 0;
      pBlk->pHdrEnr       = NULL;
      pbd->dwNbBlocsBD++;
      } while (pbd->dwNbBlocsBD <= dwIdBloc);
*/
    }

  pCMemBloc pBlk = m_apBlocsBD [dwIdBloc];

	//-------------------------------------
  if (pBlk->bExiste())
    {
    BlkError (dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_ALREADY_EXIST, pszBlkCreate, m_szNomBD);
    }

	pBlk->Ouvre(dwTailleEnr,dwNbEnr);
/*  pBlk->bExist        = TRUE;
  pBlk->dwTailleEnrDeBase  = dwTailleEnr;
  pBlk->dwNbEnr     = 0;
  pBlk->pHdrEnr     = NULL;

  PVOID			ptr = NULL;
  if (dwNbEnr > 0)
    {
    pBlk->dwNbEnr = dwNbEnr;
    pBlk->pHdrEnr = (PHEADER_ENR)pMemAlloue (dwNbEnr * sizeof (*pBlk->pHdrEnr));
    if (pBlk->pHdrEnr == NULL)
      {
      BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
      }
    for (DWORD wIdx = 0; wIdx < dwNbEnr; wIdx++)
      {
      pBlk->pHdrEnr [wIdx].dwTailleEnr = dwTailleEnr;
      pBlk->pHdrEnr [wIdx].pEnr     = (PBYTE)pMemAlloue (dwTailleEnr);
      if (pBlk->pHdrEnr [wIdx].pEnr == NULL)
        {
        BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
        }
      }
    ptr = pBlk->pHdrEnr [0].pEnr;
    }
*/
  return pBlk->pBlkPointeEnrSansErr(0);
  }

// Gestion d'erreur
void CMemBD::BlkError 
	(
	//HBLK_BD hbd,
	DWORD   dwIdBloc,
	DWORD   dwNEnr,
	DWORD   dwNbEnr,
	DWORD   wError,
	PCSTR  pszFuncName,
	PCSTR  pszManagerName,
	DWORD dwNLine, PCSTR pszSource)
  {
//$$  PBD pbd = (PBD) hbd;
	char szErr[5000] = "";

//	if (pbd)
// $$		pszManagerName = pbd->szNomBD;

	if (pszSource)
		{
		wsprintf (szErr, "Error %d: %s\n"\
			"Data base = %s; Function= %s ()\n"\
			"Block   = %d; Record  = %d; Count   = %d\n",
			"Line %d (%s)",
			wError, pszErrorMessage [wError],
			pszManagerName, pszFuncName,
			(dwIdBloc+1), dwNEnr, dwNbEnr,
			dwNLine, pszSource);
		}
	else
		{
		wsprintf (szErr, "Error %d: %s\n"\
			"Data base = %s; Function= %s ()\n"\
			"Block   = %d; Record  = %d; Count   = %d",
			wError, pszErrorMessage [wError],
			pszManagerName, pszFuncName,
			(dwIdBloc+1), dwNEnr, dwNbEnr);
		}

	// Envoie une exception avec le texte d'explication
	ExcepContexte (EXCEPTION_BLKMAN, szErr);
  }


/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : gestion abort retry ignore                               |
 |   Auteur  : LM                                                       |
 |   Date    : 09/06/92 						|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "trap.h"

//-------------------------------------------------------------------------
void lance_trap (void)
  {
  SetErrorMode (SEM_FAILCRITICALERRORS);
  }

//-------------------------------------------------------------------------
void arrete_trap (void)
  {
  SetErrorMode (0);
  }
//-------------------------------------------------------------------------
DWORD lire_erreur_trap (void)
  {
  return (0);
  }
//-------------------------------------------------------------------------

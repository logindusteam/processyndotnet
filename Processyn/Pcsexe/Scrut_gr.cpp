/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :                                           		|
 |	       								|
 |   Auteur  : LM							|
 |   Date    : 20/04/93 						|
 |   Remarques : gestion des entrees et des sorties graphiques de PcsExe|
 |                                                                      |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_tb.h"
#include "pcs_sys.h"
#include "UStr.h"
#include "USem.h"
#include "MemMan.h"
#include "ITC.h"
#include "tipe_mbx.h"
#include "mem.h"
#include "threads.h"
#include "Descripteur.h"
#include "PcsVarEx.h"
#include "G_Objets.h"
#include "g_sys.h"
#include "BdGr.h"
#include "BdPageGr.h"
#include "BdAnmGr.h"
#include "UChrono.h"
#include "CliAnmGr.h"
#include "tipe_gr.h"
#include "driv_dq.h"            // courbes archivage
#include "scrut_gr.h"
#include "DebugDumpVie.h"
#include "Verif.h"
VerifInit;

// Constantes
#define ACTION_CREATION_FENETRE    1
#define ACTION_ANIMATION_FENETRE   2
#define ACTION_SUPPRESSION_FENETRE 3

// Variables Globales
typedef struct
  {
  DWORD page;
  BOOL page_selectionnee;
  } t_contexte_vi;

// Messages re�us par PCS_EXE et �mis par l'animateur
typedef struct
  {
  CITC::ITC_HEADER_MESSAGE     Entete;
  t_anm_gr_notification  Donnee;
  } t_anm_gr_msg_event;

// Variables
static t_contexte_vi contexte_vi;
static CITC ITCServeurReceptionAnmGr;
static CITC ITCServeurEmissionAnmGr;
static t_anm_gr_data_service* pEmissionServicesAnmGr = NULL; // Messages re�us par l'animateur et �mis par PCS-EXE

static BOOL ok_init_gr = FALSE;
static HTHREAD  hThrdAckGr = NULL;

#define TIMEOUT_DEMARRAGE_GR 30000L
#define GR_INIT_ERR_MBX_CALL           1
#define GR_INIT_ERR_ACK_THREAD         2
#define GR_INIT_ERR_NO_SPACE           3
#define GR_INIT_ERR_MBX_LISTEN         4
#define GR_INIT_ERR_ANM                5

#define GR_THR_SIZE 4096
//-------------------------------------------------------------------------
// thread
static UINT __stdcall attend_ack_gr (void *hThrd)
  {
	DumpVie(attend_ack_gr);
  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
  while (bThreadAttenteMessage ((HTHREAD)hThrd, NULL, NULL, NULL, NULL))
    {
    ITCServeurEmissionAnmGr.bAttenteAR (INFINITE);
    ThreadFinTraiteMessage ((HTHREAD)hThrd);
    }
  return uThreadTermine ((HTHREAD)hThrd, 0);
  }

//-------------------------------------------------------------------------
// Monte les bits du statut vdi
static void MajStatutGr (DWORD statut_vi)
  {
	CPcsVarEx::SetBitsValVarSysNumEx (VA_SYS_STATUS_VDI, statut_vi);
  }

//--------------------------------------------------------------------------
void initialise_gr (void)
  {
  DWORD	dwCodeEchec = 0;

	// Initialisation des infos de courbe temporelle locale
  DWORD dwNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_vdi_courbe_temps);
  for (DWORD dwNEnr = 1; (dwNEnr <= dwNbEnr); dwNEnr++)
    {
		PXGR_COURBE_TEMPS ptr_courbe_temps = (PXGR_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_temps, dwNEnr);
    CPcsVarEx::SetValMemVarLogEx (ptr_courbe_temps->pos_es_enclanche, FALSE);
    ptr_courbe_temps->pfBufferEchantillons = (FLOAT *)pMemAlloue (ptr_courbe_temps->nbr_echantillon * sizeof (*ptr_courbe_temps->pfBufferEchantillons));
    ptr_courbe_temps->courant_tampon = 0;
    ptr_courbe_temps->tampon_plein = FALSE;
    ptr_courbe_temps->nbr_stocke = 0;
    }

	// Initialisation des infos de courbe sur commande locale
  dwNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_vdi_courbe_cmd);
  for (DWORD dwNEnr = 1; (dwNEnr <= dwNbEnr); dwNEnr++)
    {
		PXGR_COURBE_CMD   pXGrCourbeCmd = (PXGR_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_cmd, dwNEnr);
    pXGrCourbeCmd->pfBufferEchantillons = (FLOAT *)pMemAlloue (pXGrCourbeCmd->nbr_echantillon * sizeof (*pXGrCourbeCmd->pfBufferEchantillons));
    pXGrCourbeCmd->courant_tampon = 0;
    pXGrCourbeCmd->tampon_plein = FALSE;
    }

	// lancement de l'animateur
  pEmissionServicesAnmGr = (t_anm_gr_data_service*)pMemAlloue (sizeof(t_anm_gr_data_service));

  ok_init_gr = FALSE;
	// Init des ITC pour discuter avec le thread de CliAnmGr
	ITC_TX_RX ITCServeurAnmGr;
	ITCServeurAnmGr.pITCRX = &ITCServeurReceptionAnmGr;
	ITCServeurAnmGr.pITCTX = &ITCServeurEmissionAnmGr;

  if (ITCServeurAnmGr.pITCRX->bCreeServeurRX (szTopicReceptionAnmGr))
    {
    // Ouverture de la communication avec la reception de l'autre cote
    if (ITCServeurAnmGr.pITCTX->bCreeServeurTX (szTopicEmissionAnmGr))
			{
			// lancement de l'animateur graphique Ok ?
			if (bLanceCliAnmGr (&ITCServeurAnmGr))
				{
				// oui => : attente de l'�tablissement de la connexion
				if (ITCServeurAnmGr.pITCRX->bAUnCorrespondant (TIMEOUT_DEMARRAGE_GR) && 
					ITCServeurAnmGr.pITCTX->bAUnCorrespondant (TIMEOUT_DEMARRAGE_GR))
					{
					if (bThreadCree (&hThrdAckGr, attend_ack_gr, GR_THR_SIZE)&& 
						bThreadSetPriorite (hThrdAckGr, PRIORITE_TIMECRITICAL))
						dwCodeEchec = 0;
					else
						dwCodeEchec = GR_INIT_ERR_ACK_THREAD;
					}
				else
					dwCodeEchec = GR_INIT_ERR_ANM;
				}
			else
				dwCodeEchec = GR_INIT_ERR_ANM;
			}
		else
			dwCodeEchec = GR_INIT_ERR_MBX_LISTEN;
		}
	else
		dwCodeEchec = GR_INIT_ERR_MBX_CALL;
  
	// erreur rencontr�e ?
	if (dwCodeEchec)
		// oui => lib�ration des ressources ouvertes
		termine_gr ();
  } // initialise_gr

//--------------------------------------------------------------------------
// fermeture de la tache d'animation graphique
void termine_gr (void)
  {
	// fermeture des objets courbe temporelles
  DWORD dwNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_vdi_courbe_temps);
  for (DWORD dwNEnr = 1; (dwNEnr <= dwNbEnr); dwNEnr++)
    {
    PXGR_COURBE_TEMPS ptr_courbe_temps = (PXGR_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_temps, dwNEnr);
    MemLibere ((PVOID *)&ptr_courbe_temps->pfBufferEchantillons);
    }

	// fermeture des objets courbe sur commande
  dwNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_vdi_courbe_cmd);
  for (DWORD dwNEnr = 1; (dwNEnr <= dwNbEnr); dwNEnr++)
    {
    PXGR_COURBE_CMD pXGrCourbeCmd = (PXGR_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_cmd, dwNEnr);
    MemLibere ((PVOID *)&pXGrCourbeCmd->pfBufferEchantillons);
    }

  // ferme les communications
	if (hThrdAckGr)
		bThreadFerme (&hThrdAckGr, 1000);   // Attente 1s : "ne pas faire destroy car possible blocage dans mbx_receive ack"

  ITCServeurEmissionAnmGr.Ferme ();
  
  ITCServeurReceptionAnmGr.Ferme ();
  
	if (pEmissionServicesAnmGr)
		MemLibere ((PVOID *)(&pEmissionServicesAnmGr));

	// ferme la t�che d'animation graphique
	bFermeCliAnmGr();
  }

//-------------------------------------------------------------------------
// Fonctions locales entr�es graphiques
//-------------------------------------------------------------------------

// --------------------------------------------------------------------------
static void get_buffer_courbe_tps (PXGR_COURBE_TEMPS ptr_courbe, DWORD *nb_octets_buffer, void **ptr_buffer, BOOL bForcageEmission)
  {
  DWORD	taille_message = 0;
  void *debut_message = NULL;
  PBYTE pbSuiteMessage = NULL;
  PFLOAT	tab_echantillons;
  DWORD	index_echantillon;
  DWORD	position_valeur;
  PBOOL	ptr_enclenche = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, ptr_courbe->pos_es_enclanche);
  //
  if (!bForcageEmission)
    {                                 //-- Courbe D�j� Initialis�e
    if (*ptr_enclenche)
      {                               //-- Historique Enclench�
      if (ptr_courbe->nbr_stocke > 0)
        {                             //-- NB Echantillons Identiques En Plus
        // Emission Factoris�e de: nbr_stocke x [courant_tampon]
        taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_VALEUR
                         sizeof (DWORD)+       // ptr_courbe->nbr_stocke
                         sizeof (FLOAT) +       // [ptr_courbe->courant_tampon - 1]
                         sizeof (DWORD);       // ANM_GR_COURBES_FIN
        //
        if (ptr_courbe->courant_tampon == 0)
          {
          position_valeur = ptr_courbe->nbr_echantillon - 1;
          }
        else
          {
          position_valeur = ptr_courbe->courant_tampon - 1;
          }
        //
        debut_message = pMemAlloue (taille_message);
        pbSuiteMessage = (PBYTE)debut_message;
        // $$ codage nul! cr�er une structure
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_VALEUR;
        pbSuiteMessage += sizeof (DWORD);
        (*(DWORD *)pbSuiteMessage) = ptr_courbe->nbr_stocke; //$$ modifi�
        pbSuiteMessage += sizeof (DWORD);
        tab_echantillons = ptr_courbe->pfBufferEchantillons;
        (*(FLOAT *)pbSuiteMessage) = tab_echantillons [position_valeur];
        pbSuiteMessage += sizeof (FLOAT);
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
        pbSuiteMessage += sizeof (DWORD);
        }
      }
    }
  else
    {                                 //-- Courbe NON Initialis�e
    if (*ptr_enclenche)
      {                               //-- Historique Enclench�
      if (ptr_courbe->tampon_plein)
        {                             //-- Buf. Historique Plein
        if (ptr_courbe->nbr_stocke > ptr_courbe->nbr_echantillon)
          {                           //-- On a fait le tour du Buf. Histo
                                      //   Echantillons Identiques
          //
          taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                           sizeof (DWORD)+       // ANM_GR_COURBES_VALEUR
                           sizeof (DWORD)+       // ptr_courbe->nbr_stocke
                           sizeof (FLOAT) +       // [ptr_courbe->0]
                           sizeof (DWORD);       // ANM_GR_COURBES_FIN
          //
          debut_message = pMemAlloue (taille_message);
          pbSuiteMessage = (PBYTE)debut_message;
          //
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_VALEUR;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ptr_courbe->nbr_stocke;
          pbSuiteMessage += sizeof (DWORD);
          tab_echantillons = ptr_courbe->pfBufferEchantillons;
          (*(FLOAT *)pbSuiteMessage) = tab_echantillons [0];
          pbSuiteMessage += sizeof (FLOAT);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
          pbSuiteMessage += sizeof (DWORD);
          }
        else
          {                           //-- Echantillons Diff�rents

          taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                           sizeof (DWORD)+       // ANM_GR_COURBES_BUFFER
                           sizeof (DWORD)+       // nbr_echantillon
                          (ptr_courbe->nbr_echantillon * sizeof (FLOAT))+ // [ptr_courbe->i]
                           sizeof (DWORD);       // ANM_GR_COURBES_FIN
          //
          debut_message = pMemAlloue (taille_message);
          pbSuiteMessage = (PBYTE)debut_message;
          //
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_BUFFER;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ptr_courbe->nbr_echantillon;
          pbSuiteMessage += sizeof (DWORD);
          tab_echantillons = ptr_courbe->pfBufferEchantillons;
          for (index_echantillon = ptr_courbe->courant_tampon; index_echantillon < ptr_courbe->nbr_echantillon; index_echantillon++)
            {
            (*(FLOAT *)pbSuiteMessage) = tab_echantillons [index_echantillon];
            pbSuiteMessage += sizeof (FLOAT);
            }
          for (index_echantillon = 0; index_echantillon < ptr_courbe->courant_tampon; index_echantillon++)
            {
            (*(FLOAT *)pbSuiteMessage) = tab_echantillons [index_echantillon];
            pbSuiteMessage += sizeof (FLOAT);
            }
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
          pbSuiteMessage += sizeof (DWORD);
          }
        }
      else
        {                             //-- Buf. Historique Non Plein
        if (ptr_courbe->courant_tampon != 0)
          {                           //-- Buf. Historique Non Vide
          taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                           sizeof (DWORD)+       // ANM_GR_COURBES_BUFFER
                           sizeof (DWORD)+       // taille buffer
                          (ptr_courbe->courant_tampon * sizeof (FLOAT))+ // [ptr_courbe->i]
                           sizeof (DWORD);       // ANM_GR_COURBES_FIN
          //
          debut_message = pMemAlloue (taille_message);
          pbSuiteMessage = (PBYTE)debut_message;
          //
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_BUFFER;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ptr_courbe->courant_tampon;
          pbSuiteMessage += sizeof (DWORD);
          tab_echantillons = ptr_courbe->pfBufferEchantillons;
          for (index_echantillon = 0; index_echantillon < ptr_courbe->courant_tampon; index_echantillon++)
            {
            (*(FLOAT *)pbSuiteMessage) = tab_echantillons [index_echantillon];
            pbSuiteMessage += sizeof (FLOAT);
            }
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
          pbSuiteMessage += sizeof (DWORD);
          }
        else
          {                             //-- Buf. Historique Vide
          taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                           sizeof (DWORD);       // ANM_GR_COURBES_FIN
          //
          debut_message = pMemAlloue (taille_message);
          pbSuiteMessage = (PBYTE)debut_message;
          //
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
          pbSuiteMessage += sizeof (DWORD);
          }
        }
      }
    else
      {                               //-- Historique Arr�t�
      taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                       sizeof (DWORD);       // ANM_GR_COURBES_FIN
      //
      debut_message = pMemAlloue (taille_message);
      pbSuiteMessage = (PBYTE)debut_message;
      //
      (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
      pbSuiteMessage += sizeof (DWORD);
      (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
      pbSuiteMessage += sizeof (DWORD);
      }
    }

  (*nb_octets_buffer) = taille_message;
  (*ptr_buffer) = debut_message;
  } // get_buffer_courbe_tps

// --------------------------------------------------------------------------
static void release_buffer_courbe (void *ptr_buffer)
  {
  if (ptr_buffer)
    {
    MemLibere (&ptr_buffer);
    }
  }

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
static void get_buffer_courbe_cmd (PXGR_COURBE_CMD ptr_courbe, PBOOL pbEffaceCourbe, DWORD *nb_octets_buffer, void **ptr_buffer, BOOL bForcageEmission)
  {
  DWORD taille_message = 0;
  PVOID	debut_message = NULL;
  PBYTE	pbSuiteMessage = NULL;
  PFLOAT tab_echantillons;
  DWORD index_echantillon;

	// par d�faut : pas de courbe � effacer
	*pbEffaceCourbe = FALSE; 

	// Emission des �l�ments dans tous les cas
  if (!bForcageEmission)
    {
		// Pas de forcage en �mission (Courbe D�j� Initialis�e)
		*pbEffaceCourbe = CPcsVarEx::bGetValVarLogEx (ptr_courbe->pos_es_reset_buffer);

		BOOL bEchantillonage = CPcsVarEx::bGetValVarLogEx(ptr_courbe->pos_es_echantillonnage);
    if (bEchantillonage)
      {
      //-- Emission de 1 �l�ment
      taille_message = sizeof (DWORD)+  // Sous message ANM_GR_COURBES_VALEUR
                       sizeof (DWORD)+  // Nombre 1
                       sizeof (FLOAT)+	// Valeurs : 1 �chantillon
                       sizeof (DWORD);  // Sous Message ANM_GR_COURBES_FIN
			if (taille_message)
				{
				debut_message = pMemAlloue (taille_message);
				pbSuiteMessage = (PBYTE)debut_message;
				}

      //-- Une Valeur Echantillonn�e
      if (ptr_courbe->courant_tampon > 0)
        { //-- On n'a Pas fait Le Tour Du Buffer
        index_echantillon = ptr_courbe->courant_tampon - 1;
        }
      else
        {//-- On a fait Le Tour Du Buffer
        index_echantillon = ptr_courbe->nbr_echantillon - 1;
        }

      (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_VALEUR;
      pbSuiteMessage += sizeof (DWORD);
      (*(DWORD *)pbSuiteMessage) = 1;
      pbSuiteMessage += sizeof (DWORD);
      tab_echantillons = ptr_courbe->pfBufferEchantillons;
      (*(FLOAT *)pbSuiteMessage) = tab_echantillons [index_echantillon];
      pbSuiteMessage += sizeof (FLOAT);
      (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
      pbSuiteMessage += sizeof (DWORD);
      } // if (bEchantillonage)
    } // if (!bForcageEmission)
  else
    {
		//Emission forc�e => Courbe NON Initialis�e
    if (ptr_courbe->tampon_plein)
      {                             //-- Buf. Historique Plein
      if (ptr_courbe->courant_tampon > 0)
        {                           //-- On a fait le tour du Buf. Histo
                                    //   [Cour]-->[Fin] + [Debut]-->[Cour-1]
        //
        taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                         sizeof (DWORD)+       // ANM_GR_COURBES_BUFFER
                         sizeof (DWORD)+       // ptr_courbe->nbr_echantillon
                         (ptr_courbe->nbr_echantillon  * sizeof (FLOAT))+ // [i]
                         sizeof (DWORD);       // ANM_GR_COURBES_FIN
        //
        debut_message = pMemAlloue (taille_message);
        pbSuiteMessage = (PBYTE)debut_message;
        //
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
        pbSuiteMessage += sizeof (DWORD);
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_BUFFER;
        pbSuiteMessage += sizeof (DWORD);
        (*(DWORD *)pbSuiteMessage) = ptr_courbe->nbr_echantillon;
        pbSuiteMessage += sizeof (DWORD);
        tab_echantillons = ptr_courbe->pfBufferEchantillons;
        for (index_echantillon = ptr_courbe->courant_tampon; index_echantillon < ptr_courbe->nbr_echantillon; index_echantillon++)
          {
          (*(FLOAT *)pbSuiteMessage) = tab_echantillons [index_echantillon];
          pbSuiteMessage += sizeof (FLOAT);
          }
        for (index_echantillon = 0; index_echantillon < ptr_courbe->courant_tampon; index_echantillon++)
          {
          (*(FLOAT *)pbSuiteMessage) = tab_echantillons [index_echantillon];
          pbSuiteMessage += sizeof (FLOAT);
          }
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
        pbSuiteMessage += sizeof (DWORD);
        }
      else
        {                           //-- Tout Le Buff. Histo Est Plein
                                    //   [Debut]-->[Fin]
        //
        taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                         sizeof (DWORD)+       // ANM_GR_COURBES_BUFFER
                         sizeof (DWORD)+       // nbr_echantillon
                        (ptr_courbe->nbr_echantillon * sizeof (FLOAT))+ // [i]
                         sizeof (DWORD);       // ANM_GR_COURBES_FIN
        //
        debut_message = pMemAlloue (taille_message);
        pbSuiteMessage = (PBYTE)debut_message;
        //
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
        pbSuiteMessage += sizeof (DWORD);
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_VALEUR;
        pbSuiteMessage += sizeof (DWORD);
        (*(DWORD *)pbSuiteMessage) = ptr_courbe->nbr_echantillon;
        pbSuiteMessage += sizeof (DWORD);
        tab_echantillons = ptr_courbe->pfBufferEchantillons;
        for (index_echantillon = 0; index_echantillon < ptr_courbe->nbr_echantillon; index_echantillon++)
          {
          (*(FLOAT *)pbSuiteMessage) = tab_echantillons [index_echantillon];
          pbSuiteMessage += sizeof (FLOAT);
          }
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
        pbSuiteMessage += sizeof (DWORD);
        }
      }
    else
      {                             //-- Buf. Historique Non Plein
      if (ptr_courbe->courant_tampon > 0)
        {                           //-- Buf. Historique Non Vide
        taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                         sizeof (DWORD)+       // ANM_GR_COURBES_BUFFER
                         sizeof (DWORD)+       // ptr_courbe->courant_tampon
                        (ptr_courbe->courant_tampon * sizeof (FLOAT))+ // [i]
                         sizeof (DWORD);       // ANM_GR_COURBES_FIN
        //
        debut_message = pMemAlloue (taille_message);
        pbSuiteMessage = (PBYTE)debut_message;
        //
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
        pbSuiteMessage += sizeof (DWORD);
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_BUFFER;
        pbSuiteMessage += sizeof (DWORD);
        (*(DWORD *)pbSuiteMessage) = ptr_courbe->courant_tampon;
        pbSuiteMessage += sizeof (DWORD);
        tab_echantillons = ptr_courbe->pfBufferEchantillons;
        for (index_echantillon = 0; index_echantillon < ptr_courbe->courant_tampon; index_echantillon++)
          {
          (*(FLOAT *)pbSuiteMessage) = tab_echantillons [index_echantillon];
          pbSuiteMessage += sizeof (FLOAT);
          }
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
        pbSuiteMessage += sizeof (DWORD);
        }
      else
        {                             //-- Buf. Historique Vide
        taille_message = sizeof (DWORD)+       // ANM_GR_COURBES_ERASE
                         sizeof (DWORD);       // ANM_GR_COURBES_FIN
        //
        debut_message = pMemAlloue (taille_message);
        pbSuiteMessage = (PBYTE)debut_message;
        //
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_ERASE;
        pbSuiteMessage += sizeof (DWORD);
        (*(DWORD *)pbSuiteMessage) = ANM_GR_COURBES_FIN;
        pbSuiteMessage += sizeof (DWORD);
        }
      }
    }

	(*nb_octets_buffer) = taille_message;
  (*ptr_buffer) = debut_message;
  }

// --------------------------------------------------------------------------
// gestion des courbes g�n�r�es a partir de fichiers
// --------------------------------------------------------------------------
static void get_valeur_fichier_archive (HFDQ repere_fichier, DWORD num_enr, FLOAT *valeur)
  {
  BYTE *pbyte = (BYTE *)valeur;
  for (DWORD index = 0; index <4; index++)
    {
    uFicLireDirect (repere_fichier, num_enr + index, pbyte);
    pbyte++;
    }
  }

// --------------------------------------------------------------------------
static BOOL valide_fichier_archive (PHFDQ repere_fichier, char *cour_fichier,
                                       FLOAT cour_enr, FLOAT cour_nb_enr, FLOAT cour_taille)
  {
  BOOL	fic_valide = FALSE;

  if (!bFicOuvert (repere_fichier, cour_fichier))
    {
    if (bFicPresent (cour_fichier))
      { // le fichier existe
      if ((uFicOuvre (repere_fichier, cour_fichier, sizeof(char))) == 0)
        {
				DWORD	dwNbEnr;
				DWORD num_enr = (DWORD) ((cour_enr + cour_nb_enr - 1) * cour_taille);
				uFicNbEnr (*repere_fichier, &dwNbEnr);
        if (dwNbEnr >= num_enr)
          fic_valide = TRUE;
        }
      }
    }
  return fic_valide;
  }

// --------------------------------------------------------------------------
static void get_buffer_courbe_archive (tx_vdi_courbe_archive *ptr_courbe, DWORD *nb_octets_buffer,
                                       void **ptr_buffer, BOOL bForcageEmission)
  {
  DWORD taille_message = 0;
  void *debut_message = NULL;
  PBYTE	pbSuiteMessage = NULL;
  BOOL   affichage;
	BOOL  bModifie = FALSE;
	BOOL  *cour_affichage;
  FLOAT *cour_couleur, *cour_style,  *cour_modele, *cour_min, *cour_max;
  FLOAT      *cour_taille, *cour_offset, *cour_enr, *cour_nb_enr;
  PSTR psz_cour_fic;
  PSTR	psz_mem_fic;
  PBOOL  mem_affichage;
  PFLOAT mem_couleur, mem_style, mem_modele, mem_min, mem_max;
  FLOAT  *mem_taille, *mem_offset, *mem_enr, *mem_nb_enr;
  DWORD index;
  HFDQ repere_fichier;
  DWORD num_enr;
  FLOAT valeur;

  cour_affichage = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, ptr_courbe->pos_spec_affichage);
  mem_affichage  = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, ptr_courbe->pos_spec_affichage);
  if (*cour_affichage)
    { // affichage
    cour_couleur = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_couleur);
    mem_couleur  = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_couleur);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_couleur));

    cour_style   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_style);
    mem_style    = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_style);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_style));

    cour_modele  = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_modele);
    mem_modele   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_modele);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_modele));

    cour_min     = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_min);
    mem_min      = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_min);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_min));

    cour_max     = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_max);
    mem_max      = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_max);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_max));

    psz_cour_fic = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, ptr_courbe->pos_spec_fichier);
    psz_mem_fic  = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes, ptr_courbe->pos_spec_fichier);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourMesExValeurModifie(ptr_courbe->pos_spec_fichier));

    cour_taille  = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_taille);
    mem_taille   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_taille);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_taille));

    cour_offset  = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_offset);
    mem_offset   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_offset);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_offset));

    cour_enr     = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_enr);
    mem_enr      = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_enr);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_enr));

    cour_nb_enr  = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe->pos_spec_nb_enr);
    mem_nb_enr   = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, ptr_courbe->pos_spec_nb_enr);
		bModifie = bModifie || (CPcsVarEx::bGetBlocCourNumExValeurModifie(ptr_courbe->pos_spec_nb_enr));

    affichage = FALSE;
    if (!*mem_affichage)
      { // affichage : 0 1
      affichage = TRUE;
      }
    else
      { // affichage : 1 1
      if ((bForcageEmission)                               ||
				  (bModifie)															||
          (*cour_couleur != *mem_couleur)         ||
          (*cour_style   != *mem_style)           ||
          (*cour_modele  != *mem_modele)          ||
          (*cour_min     != *mem_min)             ||
          (*cour_max     != *mem_max)             ||
          !(bStrEgales (psz_cour_fic, psz_mem_fic)) ||
          (*cour_taille  != *mem_taille)          ||
          (*cour_offset  != *mem_offset)          ||
          (*cour_enr     != *mem_enr)             ||
          (*cour_nb_enr  != *mem_nb_enr))
        {
        affichage = TRUE;
        }
      } // affichage : 1 1
    if (affichage)
      { // envoyer tout
      //
      if ((*cour_couleur    >= 0) &&
          (*cour_style      >= 0) &&
          (*cour_min        <  *cour_max) &&
          (*cour_taille     >= 4) &&
          (*cour_enr        >= 1) &&
          (*cour_nb_enr     >  1) &&
          (*cour_offset     >= 1) &&
          (*cour_offset + 3 <= *cour_taille))
        { // parametres courbes valides
        //
        if (valide_fichier_archive (&repere_fichier, psz_cour_fic, *cour_enr, *cour_nb_enr, *cour_taille))
          { // fichier valide
          taille_message = sizeof (DWORD)+       // ANM_GR_ARCHIVE_ERASE
                           2*(sizeof (DWORD))+   // ANM_GR_ARCHIVE_COULEUR + valeur
                           2*(sizeof (DWORD))+   // ANM_GR_ARCHIVE_STYLE   + valeur
                           2*(sizeof (DWORD))+   // ANM_GR_ARCHIVE_MODELE  + valeur
                           sizeof (DWORD)+       // ANM_GR_ARCHIVE_MIN
                           sizeof (FLOAT)+        // valeur min
                           sizeof (DWORD)+       // ANM_GR_ARCHIVE_MAX
                           sizeof (FLOAT)+        // valeur max
                           sizeof (DWORD)+       // ANM_GR_ARCHIVE_BUFFER
                           sizeof (DWORD)+       // nbre de valeurs
                           (DWORD)(*cour_nb_enr) * (sizeof (FLOAT))+  // valeurs
                           sizeof (DWORD);       // ANM_GR_ARCHIVE_FIN
          //
          debut_message = pMemAlloue (taille_message);
          pbSuiteMessage = (PBYTE)debut_message;
          //
          (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_ERASE;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_COULEUR;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = (DWORD)(*cour_couleur);
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_STYLE;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = (DWORD)(*cour_style);
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_MODELE;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = (DWORD)(*cour_modele);
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_MIN;
          pbSuiteMessage += sizeof (DWORD);
          (*(FLOAT  *)pbSuiteMessage) = (*cour_min);
          pbSuiteMessage += sizeof(FLOAT);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_MAX;
          pbSuiteMessage += sizeof (DWORD);
          (*(FLOAT  *)pbSuiteMessage) = (*cour_max);
          pbSuiteMessage += sizeof(FLOAT);
          (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_BUFFER;
          pbSuiteMessage += sizeof (DWORD);
          (*(DWORD *)pbSuiteMessage) = (DWORD)(*cour_nb_enr);
          pbSuiteMessage += sizeof (DWORD);

          num_enr = (DWORD) (((*cour_enr) - 1) * (*cour_taille) + (*cour_offset));
          for (index = 0; index < (DWORD)(*cour_nb_enr); index++)
            {
            get_valeur_fichier_archive (repere_fichier, num_enr, &valeur);
            (*(FLOAT *)pbSuiteMessage) = valeur;
            pbSuiteMessage += sizeof (FLOAT);
            num_enr = num_enr + (DWORD)(*cour_taille);
            }
          uFicFerme (&repere_fichier);

          (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_FIN;
          pbSuiteMessage += sizeof (DWORD);
          }
        else
          { // fichier invalide
          MajStatutGr (VI_PARAM_CB_ARCHIVE); // parametre courbe archive invalide
          }
        } // parametres courbes corrects
      else
        { // parametres courbes invalides
        MajStatutGr (VI_PARAM_CB_ARCHIVE); // parametre courbe archive invalide
        }
      } // affichage
    } // cour_affichage
  else
    { // cour_affichage : 0
    if (*mem_affichage)
      { // affichage : 1 0
      taille_message = sizeof (DWORD)+       // ANM_GR_ARCHIVE_ERASE
                       sizeof (DWORD);       // ANM_GR_ARCHIVE_FIN
      //
      debut_message = pMemAlloue (taille_message);
      pbSuiteMessage = (PBYTE)debut_message;
      //
      (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_ERASE;
      pbSuiteMessage += sizeof (DWORD);
      (*(DWORD *)pbSuiteMessage) = ANM_GR_ARCHIVE_FIN;
      pbSuiteMessage += sizeof (DWORD);

      } // affichage : 1 0
    } // cour_affichage : 0

  // 
  (*nb_octets_buffer) = taille_message;
  (*ptr_buffer) = debut_message;
  } // get_buffer_courbe_archive

// --------------------------------------------------------------------------
static DWORD traite_message_recu (t_anm_gr_notification *pnotif)
  {
  tx_entrees_vi *donnees_entrees;
  DWORD    type_message;

  if (pnotif->CodeRetour != 0)
    {
		// R�ception d'une notification de la valeur d'une entr�e
    type_message = CONTENU_TRAME;
    donnees_entrees = (tx_entrees_vi *)pointe_enr (szVERIFSource, __LINE__, bx_entrees_vi, pnotif->CodeRetour);
    switch (donnees_entrees->bloc_genre)
      {
      case b_cour_log :
				CPcsVarEx::SetValVarLogEx (donnees_entrees->pos_es, (pnotif->Valeur).ValLogique);
        //plog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, donnees_entrees->pos_es);
        //(*plog) = (BOOL)(!(*plog)); $$ modif 13/6/97 JS et AC
        break;
      case b_cour_num :
				CPcsVarEx::SetValVarNumEx (donnees_entrees->pos_es, (pnotif->Valeur).ValNumerique);
        break;
      case b_cour_mes :
				CPcsVarEx::SetValVarMesEx (donnees_entrees->pos_es, (pnotif->Valeur).ValMessage);
        break;
      default :
        break;
      }
    }
  else
    {
    type_message = (pnotif->Valeur).ValType;
    }
  return type_message;
  }

//-------------------------------------------------------------------------
// Fonctions locales sorties graphiques
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
static void reset_buff_vi (DWORD *pos_buff)
  {
  pEmissionServicesAnmGr->NbServices = 0;
  (*pos_buff) = 0;
  }

//-----------------------------------------------------------------------------------
// Envoie le message de commande � l'animateur et met � jour le statut si n�cessaire
static BOOL envoi_message_mbx (DWORD *pos_buff, BOOL avec_ack)
  {
  BOOL ok_envoi = FALSE;
  DWORD code_ret= ITCServeurEmissionAnmGr.dwEcritureBuffer (pEmissionServicesAnmGr,
    sizeof(DWORD) +                  //NbServices
    (*pos_buff),                     // buffer
    avec_ack);

  if (code_ret == 0)
    {
    ok_envoi   = TRUE;
    ok_init_gr = TRUE;
    }
  else
    {
    if (code_ret == ITC_ERR_ITC_NOT_COMM_STATE)
      {
      MajStatutGr (VI_MBX_ERR_CLOSE);
      }
    else
      {
      MajStatutGr (VI_SEND_ERR_PUT_MBX);
      }
    }
  return ok_envoi;
  }

//-------------------------------------------------------------------------
//
static BOOL bEnvoieBufExistantSiNecessaire (DWORD *pos_buff, DWORD taille)
  {
  taille = taille + sizeof(HEADER_SERVICE_ANM_GR) + sizeof(DWORD);
  if (((*pos_buff) + taille) >= ANM_GR_MAX_BUFF_SERVICES)
    {
    envoi_message_mbx (pos_buff,FALSE);
    reset_buff_vi (pos_buff);
    }
  return (TRUE);
  }

//-------------------------------------------------------------------------
//
static BOOL add_buff_HeaderServiceAnmGr (DWORD *pos_buff, HEADER_SERVICE_ANM_GR HeaderServiceAnmGr)
  {
  PBYTE ptr_buff = pEmissionServicesAnmGr->bBufferServices + (*pos_buff);

  (*(HEADER_SERVICE_ANM_GR*)ptr_buff) = HeaderServiceAnmGr;
  (*pos_buff) = (*pos_buff) + sizeof (HeaderServiceAnmGr);
  pEmissionServicesAnmGr->NbServices++;
  return(TRUE);
  }

//-------------------------------------------------------------------------
//
static BOOL add_buff_select_page (DWORD *pos_buff, DWORD numero_page)
  {
  PBYTE ptr_buff;
  tx_services_speciaux_vi*donneex_bx_srv_spec;

  bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HEADER_SERVICE_ANM_GR) + sizeof(DWORD)); //$$ Taille Ok ?
  donneex_bx_srv_spec = (tx_services_speciaux_vi*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_vi, PTR_SRV_SPEC_VI_PAGE);
  add_buff_HeaderServiceAnmGr (pos_buff, donneex_bx_srv_spec->handle);
  ptr_buff = pEmissionServicesAnmGr->bBufferServices + (*pos_buff);
  (*(DWORD *)ptr_buff) = numero_page;
  (*pos_buff) = (*pos_buff) + sizeof(DWORD);
  return(TRUE);
  }

//-------------------------------------------------------------------------
//
static BOOL test_add_contexte (DWORD *pos_buff)
  {
  if (!contexte_vi.page_selectionnee)
    {
    bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HEADER_SERVICE_ANM_GR) + sizeof(DWORD));
    add_buff_select_page (pos_buff, contexte_vi.page);
    contexte_vi.page_selectionnee = TRUE;
    }
  return (TRUE);
  }

//-------------------------------------------------------------------------
//
static BOOL add_buff_taille (DWORD *pos_buff, FLOAT x, FLOAT y, FLOAT cx, FLOAT cy,
                         FLOAT dimension)
  {
  PBYTE ptr_buff = pEmissionServicesAnmGr->bBufferServices + (*pos_buff);

  (*(FLOAT *)ptr_buff) = x;
  ptr_buff = ptr_buff + sizeof(FLOAT);
  (*(FLOAT *)ptr_buff) = y;
  ptr_buff = ptr_buff + sizeof(FLOAT);
  (*(FLOAT *)ptr_buff) = cx;
  ptr_buff = ptr_buff + sizeof(FLOAT);
  (*(FLOAT *)ptr_buff) = cy;
  ptr_buff = ptr_buff + sizeof(FLOAT);
  (*(FLOAT *)ptr_buff) = dimension;
  (*pos_buff) = (*pos_buff) + 5*(sizeof(FLOAT));
  return(TRUE);
  }

//-------------------------------------------------------------------------
//
static void test_add_buff_taille (DWORD *pos_buff, HEADER_SERVICE_ANM_GR anm_hdl,
	DWORD pos_es_x, DWORD pos_es_y, DWORD pos_es_cx, DWORD pos_es_cy, DWORD pos_es_dimension)
  {
  FLOAT *cour_num_x, *cour_num_y, *cour_num_cx, *cour_num_cy, *cour_num_dimension;
  FLOAT *mem_num_x, *mem_num_y, *mem_num_cx, *mem_num_cy, *mem_num_dimension;
  BOOL a_emettre = FALSE;

  cour_num_x = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_x);
	a_emettre = a_emettre || CPcsVarEx::bGetBlocCourNumExValeurModifie(pos_es_x);
  cour_num_y = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_y);
	a_emettre = a_emettre || CPcsVarEx::bGetBlocCourNumExValeurModifie(pos_es_y);
  cour_num_cx = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_cx);
	a_emettre = a_emettre || CPcsVarEx::bGetBlocCourNumExValeurModifie(pos_es_cx);
  cour_num_cy = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_cy);
	a_emettre = a_emettre || CPcsVarEx::bGetBlocCourNumExValeurModifie(pos_es_cy);
  cour_num_dimension = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_dimension);
	a_emettre = a_emettre || CPcsVarEx::bGetBlocCourNumExValeurModifie(pos_es_dimension);
  mem_num_x = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pos_es_x);
  mem_num_y = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pos_es_y);
  mem_num_cx = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pos_es_cx);
  mem_num_cy = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pos_es_cy);
  mem_num_dimension = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, pos_es_dimension);
  a_emettre = a_emettre || ((*cour_num_x) != (*mem_num_x))   ||
                           ((*cour_num_y) != (*mem_num_y))   ||
                           ((*cour_num_cx) != (*mem_num_cx)) ||
                           ((*cour_num_cy) != (*mem_num_cy)) ||
                           ((*cour_num_dimension) != (*mem_num_dimension));
  if (a_emettre)
    {
    test_add_contexte (pos_buff);
    bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HEADER_SERVICE_ANM_GR) + 5*(sizeof(FLOAT)));
    add_buff_HeaderServiceAnmGr  (pos_buff, anm_hdl);
    add_buff_taille (pos_buff, (*cour_num_x), (*cour_num_y), (*cour_num_cx), (*cour_num_cy), (*cour_num_dimension));
    }
  }

//-------------------------------------------------------------------------
//
static BOOL add_buff_sous_message (DWORD *pos_buff, DWORD dwSousMessage)
  {
  PBYTE ptr_buff = pEmissionServicesAnmGr->bBufferServices + (*pos_buff);

  (*(DWORD *)ptr_buff) = dwSousMessage;
  (*pos_buff) = (*pos_buff) + sizeof(DWORD);
  return TRUE;
  }

//-------------------------------------------------------------------------
//
static BOOL add_buff_message_vi (DWORD *pos_buff, char *message)
  {
  PBYTE ptr_buff = pEmissionServicesAnmGr->bBufferServices + (*pos_buff);

  StrCopy ((PSTR)ptr_buff, message);
  (*pos_buff) = (*pos_buff) + StrLength (message) + 1;
  return TRUE;
  }

//-------------------------------------------------------------------------
//
static BOOL add_buff_numerique_vi (DWORD *pos_buff, FLOAT num)
  {
  PBYTE ptr_buff = pEmissionServicesAnmGr->bBufferServices + (*pos_buff);

  (*(FLOAT *)ptr_buff) = num;
  (*pos_buff) = (*pos_buff) + sizeof(FLOAT);
  return TRUE;
  }

//-------------------------------------------------------------------------
//
static void test_add_buff_impression (DWORD *pos_buff, HEADER_SERVICE_ANM_GR anm_hdl,
                                      DWORD pos_es_impression, DWORD pos_es_nom_doc)
  {
  FLOAT * impression;
  char * psz_nom_doc;

  impression = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_impression);
  if ((*impression) != 0)
    {
    psz_nom_doc = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, pos_es_nom_doc);
    test_add_contexte     (pos_buff);
    bEnvoieBufExistantSiNecessaire   (pos_buff, sizeof(HEADER_SERVICE_ANM_GR) + sizeof(FLOAT) + StrLength (psz_nom_doc));
    add_buff_HeaderServiceAnmGr    (pos_buff, anm_hdl);
    add_buff_numerique_vi (pos_buff, (*impression));
    add_buff_message_vi   (pos_buff, psz_nom_doc);
    *impression = (FLOAT)0;
    }
  }


//-------------------------------------------------------------------------
//
static BOOL add_buff_fen_affiche (DWORD *pos_buff, HEADER_SERVICE_ANM_GR anm_hdl,
                              DWORD pos_es_x, DWORD pos_es_y, DWORD pos_es_cx,
                              DWORD pos_es_cy, DWORD pos_es_dimension)
  {
  FLOAT *cour_num_x, *cour_num_y, *cour_num_cx, *cour_num_cy, *cour_num_dimension;

  cour_num_x = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_x);
  cour_num_y = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_y);
  cour_num_cx = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_cx);
  cour_num_cy = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_cy);
  cour_num_dimension = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pos_es_dimension);

  test_add_contexte (pos_buff);
  bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HEADER_SERVICE_ANM_GR) + 5*(sizeof(FLOAT)));
  add_buff_HeaderServiceAnmGr  (pos_buff, anm_hdl);
  add_buff_taille (pos_buff, (*cour_num_x), (*cour_num_y), (*cour_num_cx), (*cour_num_cy), (*cour_num_dimension));
  return TRUE;
  }

//-------------------------------------------------------------------------
//
static BOOL add_buff_fen_efface (DWORD *pos_buff, HEADER_SERVICE_ANM_GR anm_hdl)
  {
  test_add_contexte (pos_buff);
  bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HEADER_SERVICE_ANM_GR));
  add_buff_HeaderServiceAnmGr  (pos_buff, anm_hdl);
  return TRUE;
  }

//-------------------------------------------------------------------------
// Ajoute un logique dans le buffer � �mettre
static BOOL add_buff_logique_vi (DWORD *pos_buff, BOOL log)
  {
  PBYTE	ptr_buff = pEmissionServicesAnmGr->bBufferServices + (*pos_buff);

  (*(BOOL *)ptr_buff) = log;
  (*pos_buff) = (*pos_buff) + sizeof(BOOL);
  return TRUE;
  }

//-------------------------------------------------------------------------
// Ajoute un buffer dans le buffer � �mettre
static BOOL add_buff_buff_vi (DWORD *pos_buff, void *ptr_buff_vi, DWORD taille_buff_vi)
  {
  PBYTE ptr_buff = pEmissionServicesAnmGr->bBufferServices + (*pos_buff);

  memcpy (ptr_buff, ptr_buff_vi, taille_buff_vi);
  (*pos_buff) = (*pos_buff) + taille_buff_vi;

  return TRUE;
  }


//-------------------------------------------------------------------------
//
static BOOL test_add_buff_numerique_vi (DWORD *pos_buff, HEADER_SERVICE_ANM_GR anm_hdl, DWORD pos_es, BOOL bForcageEmission)
  {
  BOOL ok_add = FALSE;

  FLOAT fNum = CPcsVarEx::fGetValVarNumEx (pos_es);
  if (!bForcageEmission)
    {
    ok_add = (fNum != CPcsVarEx::fGetValMemVarNumEx (pos_es)) || (CPcsVarEx::bGetBlocCourNumExValeurModifie(pos_es));
    }
  else
    {
    ok_add = TRUE;
    }
  if (ok_add)
    {
    test_add_contexte (pos_buff);
    bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HEADER_SERVICE_ANM_GR) + sizeof(FLOAT));
    add_buff_HeaderServiceAnmGr  (pos_buff, anm_hdl);
    add_buff_numerique_vi (pos_buff, fNum);
    }
  return ok_add;
  }

//-------------------------------------------------------------------------
//
static BOOL add_buff_ack_vi (DWORD *pos_buff)
  {
  tx_services_speciaux_vi *donneex_bx_srv_spec;

  bEnvoieBufExistantSiNecessaire (pos_buff, sizeof(HEADER_SERVICE_ANM_GR));
  donneex_bx_srv_spec = (tx_services_speciaux_vi*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_vi, PTR_SRV_SPEC_VI_ACK);
  add_buff_HeaderServiceAnmGr (pos_buff, donneex_bx_srv_spec->handle);
  return TRUE;
  }

//-------------------------------------------------------------------
// gestion des reflets graphiques : ajout au buffer d'envoi des infos pour les reflets graphiques
static BOOL test_envoi_reflets_graphiques_fenetre (DWORD *pos_buff, DWORD dwId_bx_fenetre, BOOL bForcageEmission)
  {
  tx_fenetre_vi*	donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, dwId_bx_fenetre);

	// Parcours des �l�ments d'animation de la fen�tre
  for (DWORD index_element = donnees_bx_fenetre->prem_elt;
       index_element < donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
       index_element++)
    {
		// Ajoute si n�cessaire les infos d'animation � �mettre pour cet �l�ment :
		tx_element_vi* donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, index_element);

    // Teste s'il y a quelque chose � envoyer pour cet �l�ment :
		DWORD taille_buff_courbe_tps		= 0;
		PVOID ptr_buff_courbe_tps				= NULL;
		DWORD taille_buff_courbe_cmd		= 0;
		PVOID ptr_buff_courbe_cmd				= NULL;
		BOOL	bEffaceCourbe							= FALSE;
		DWORD taille_buff_courbe_archive= 0;
		PVOID	ptr_buff_courbe_archive		= NULL;

		tx_variable_vi*donnees_bx_variable;
		BOOL									*cour_log, *mem_log;
		FLOAT                 *cour_num, *mem_num;
		PXGR_COURBE_TEMPS			ptr_courbe_temps;
		PXGR_COURBE_CMD				pXGrCourbeCmd;
		tx_vdi_courbe_archive *ptr_courbe_fic;
		PSTR									psz_cour ;
		char                  *psz_mem  ;
		PX_TABLEAU						ptr_tableau;
		DWORD									index_tab;
		DWORD									ptr_ex_mes;

		BOOL	a_envoyer = bForcageEmission;

		// Parcours les 'variables' composant l'�l�ment d'animation jusqu'� trouver quelque chose � envoyer
		DWORD index_variable = donnees_bx_element->prem_variable;
		DWORD	index_fin_variable = donnees_bx_element->prem_variable + donnees_bx_element->nbr_variable;
    while ((index_variable < index_fin_variable) && (!a_envoyer))
      {
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, index_variable);
      switch (donnees_bx_variable->bloc_genre)
        {
        case b_cour_log:
          cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
          mem_log  = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, donnees_bx_variable->pos_es);
          a_envoyer = ((*cour_log) != (*mem_log)) || (CPcsVarEx::bGetBlocCourLogExValeurModifie(donnees_bx_variable->pos_es));
          break;

        case b_cour_num:
          cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
          mem_num  = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, donnees_bx_variable->pos_es);
          a_envoyer =  ((*cour_num) != (*mem_num)) || (CPcsVarEx::bGetBlocCourNumExValeurModifie(donnees_bx_variable->pos_es));
          break;

        case b_cour_mes:
          psz_cour = (PSTR)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
          psz_mem  = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes, donnees_bx_variable->pos_es);
          a_envoyer =  (!bStrEgales (psz_cour, psz_mem)) || (CPcsVarEx::bGetBlocCourMesExValeurModifie(donnees_bx_variable->pos_es));
          break;

        case bx_vdi_courbe_temps:
          ptr_courbe_temps = (PXGR_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
          get_buffer_courbe_tps (ptr_courbe_temps, &taille_buff_courbe_tps, &ptr_buff_courbe_tps, bForcageEmission);
          a_envoyer =  (taille_buff_courbe_tps != 0);
          break;

        case bx_vdi_courbe_cmd:
          pXGrCourbeCmd = (PXGR_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
          get_buffer_courbe_cmd (pXGrCourbeCmd, &bEffaceCourbe, &taille_buff_courbe_cmd, &ptr_buff_courbe_cmd, bForcageEmission);
          a_envoyer = (bEffaceCourbe ||(taille_buff_courbe_cmd != 0));
          break;

        case bx_vdi_courbe_archive:
          ptr_courbe_fic = (tx_vdi_courbe_archive *)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
          get_buffer_courbe_archive (ptr_courbe_fic, &taille_buff_courbe_archive, &ptr_buff_courbe_archive, bForcageEmission);
          a_envoyer =  (taille_buff_courbe_archive != 0);
          break;

				case  bx_vdi_dlg_combo_liste:
					ptr_tableau	= (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau, donnees_bx_variable->pos_es);
					index_tab = 1;
					ptr_ex_mes = ptr_tableau->pos_x_es;
					while ((!a_envoyer) && (index_tab <= ptr_tableau->taille)) 
						{
						psz_cour = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, ptr_ex_mes);
						psz_mem  = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes, ptr_ex_mes);
						a_envoyer =  (!bStrEgales (psz_cour, psz_mem)) || (CPcsVarEx::bGetBlocCourMesExValeurModifie(ptr_ex_mes));
						index_tab++;
						ptr_ex_mes++;
						}
					break;

        default:
          break;
        } // switch (donnees_bx_variable->bloc_genre)
      index_variable++;
      } // while ((index_variable < index_fin_variable) && (!a_envoyer))

    // Y a t'il quelque chose � envoyer ?
    if (a_envoyer)
      {
			// oui => Fait l'envoi :
      test_add_contexte (pos_buff);

			// calcule la taille de ce qu'il y a � envoyer
   		DWORD	taille = 0;

			// Parcours les 'variables' composant l'�l�ment d'animation jusqu'� trouver quelque chose � envoyer
      for (index_variable = donnees_bx_element->prem_variable; index_variable < index_fin_variable; index_variable++)
        {
        donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, index_variable);
        switch (donnees_bx_variable->bloc_genre)
          {
          case b_cour_log:
            taille += sizeof(BOOL);
            break;

          case b_cour_num:
            taille += sizeof(FLOAT);
            break;

          case b_cour_mes:
            taille += 82; //$$
            break;

          case bx_vdi_courbe_temps:
            if (taille_buff_courbe_tps == 0)
              {
              ptr_courbe_temps = (PXGR_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
              get_buffer_courbe_tps (ptr_courbe_temps, &taille_buff_courbe_tps, &ptr_buff_courbe_tps, bForcageEmission);
              }
            taille += taille_buff_courbe_tps;
            break;

          case bx_vdi_courbe_cmd:
						{
            if (taille_buff_courbe_cmd == 0)
              {
              pXGrCourbeCmd = (PXGR_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
              get_buffer_courbe_cmd (pXGrCourbeCmd, &bEffaceCourbe, &taille_buff_courbe_cmd, &ptr_buff_courbe_cmd, bForcageEmission);
              }
            taille += taille_buff_courbe_cmd;
						}
            break;

          case bx_vdi_courbe_archive:
            if (taille_buff_courbe_archive == 0)
              {
              ptr_courbe_fic = (tx_vdi_courbe_archive *)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
              get_buffer_courbe_archive (ptr_courbe_fic, &taille_buff_courbe_archive, &ptr_buff_courbe_archive, bForcageEmission);
              }
            taille += taille_buff_courbe_archive;
            break;

					case bx_vdi_dlg_combo_liste:
						ptr_tableau	= (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau, donnees_bx_variable->pos_es);
						taille += sizeof(FLOAT) + (82 * ptr_tableau->taille);	// taille tab + messages du tab  
						break;

          default:
            break;
          }
        } // for (index_variable = donnees_bx_element->prem_variable; index_variable < index_fin_variable; index_variable++)

			// Effacement courbe � faire ?
			if (bEffaceCourbe)
				{
				// oui => �mission du service de l'�l�ment
        bEnvoieBufExistantSiNecessaire (pos_buff, sizeof (HEADER_SERVICE_ANM_GR) + sizeof(DWORD) + sizeof(DWORD));
        add_buff_HeaderServiceAnmGr (pos_buff, donnees_bx_element->HeaderServiceAnmGr);

				// Ajout sous message ANM_GR_COURBES_ERASE
				add_buff_sous_message(pos_buff, ANM_GR_COURBES_ERASE);

				// Ajout sous message ANM_GR_COURBES_FIN
				add_buff_sous_message(pos_buff, ANM_GR_COURBES_FIN);

				// Effacement courbe pris en compte
				bEffaceCourbe = FALSE;
				}

      // Quelque chose � envoyer ?
      if (taille != 0)            // si param�tres du handle message non nuls
        {
				// oui => �mission du service de l'�l�ment :
				// son header...
        taille = taille + sizeof (HEADER_SERVICE_ANM_GR);
        bEnvoieBufExistantSiNecessaire (pos_buff, taille);
        add_buff_HeaderServiceAnmGr (pos_buff, donnees_bx_element->HeaderServiceAnmGr);

				// ... et ses donn�es
        for (index_variable = donnees_bx_element->prem_variable; index_variable < index_fin_variable; index_variable++)
          {
          donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, index_variable);
          switch (donnees_bx_variable->bloc_genre)
            {
            case b_cour_log:
              cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
              add_buff_logique_vi (pos_buff, (*cour_log));
              break;

            case b_cour_num:
              cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
              add_buff_numerique_vi (pos_buff, (*cour_num));
              break;

            case b_cour_mes:
              psz_cour = (PSTR)pointe_enr (szVERIFSource, __LINE__, donnees_bx_variable->bloc_genre, donnees_bx_variable->pos_es);
              add_buff_message_vi (pos_buff, psz_cour);
              break;

            case bx_vdi_courbe_temps:
              add_buff_buff_vi (pos_buff, ptr_buff_courbe_tps, taille_buff_courbe_tps);
              release_buffer_courbe (ptr_buff_courbe_tps);
              break;

            case bx_vdi_courbe_cmd:
							{
              add_buff_buff_vi (pos_buff, ptr_buff_courbe_cmd, taille_buff_courbe_cmd);
              release_buffer_courbe (ptr_buff_courbe_cmd);
							}
              break;

            case bx_vdi_courbe_archive:
              add_buff_buff_vi (pos_buff, ptr_buff_courbe_archive, taille_buff_courbe_archive);
              release_buffer_courbe (ptr_buff_courbe_archive);
              break;

						case  bx_vdi_dlg_combo_liste:
							ptr_tableau	= (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau, donnees_bx_variable->pos_es);
              add_buff_numerique_vi (pos_buff, (FLOAT)(ptr_tableau->taille));
							index_tab = 1;
							ptr_ex_mes = ptr_tableau->pos_x_es;
							while (index_tab <= ptr_tableau->taille)
								{
								psz_cour = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, ptr_ex_mes);
	              add_buff_message_vi (pos_buff, psz_cour);
								index_tab++;
								ptr_ex_mes++;
								}
							break;
            } // switch (donnees_bx_variable->bloc_genre)
          } // boucle 1� var a nbr_var
        } // param�tres du handle message non nuls
      } // a_envoyer
    }  // for (index_element = donnees_bx_fenetre->prem_elt;

  return TRUE;
  } // test_envoi_reflets_graphiques_fenetre

//-------------------------------------------------------------------
// envoi de tous les messages de sorties graphiques d'une fen�tre
//-------------------------------------------------------------------
static void AjouteBufferSortiesGraphiquesFenetre (DWORD *pos_buff, DWORD dwId_bx_fenetre, DWORD action_a_faire)
  {
  tx_fenetre_vi *donnees_bx_fenetre;

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, dwId_bx_fenetre);
  contexte_vi.page = dwId_bx_fenetre;
  contexte_vi.page_selectionnee = FALSE;
  switch (action_a_faire)
    {
    case ACTION_CREATION_FENETRE:
			{
      add_buff_fen_affiche (pos_buff,
                            donnees_bx_fenetre->visible.handle,
                            donnees_bx_fenetre->taille.pos_es_x,
                            donnees_bx_fenetre->taille.pos_es_y,
                            donnees_bx_fenetre->taille.pos_es_cx,
                            donnees_bx_fenetre->taille.pos_es_cy,
                            donnees_bx_fenetre->taille.pos_es_dimension);
      test_add_buff_impression (pos_buff,
                                donnees_bx_fenetre->impression.handle,
                                donnees_bx_fenetre->impression.pos_es_impression,
                                donnees_bx_fenetre->impression.pos_es_nom_doc);
      test_envoi_reflets_graphiques_fenetre (pos_buff, dwId_bx_fenetre, TRUE);
			}
      break;

    case ACTION_ANIMATION_FENETRE:
			{
      test_add_buff_taille (pos_buff,
                            donnees_bx_fenetre->taille.handle,
                            donnees_bx_fenetre->taille.pos_es_x,
                            donnees_bx_fenetre->taille.pos_es_y,
                            donnees_bx_fenetre->taille.pos_es_cx,
                            donnees_bx_fenetre->taille.pos_es_cy,
                            donnees_bx_fenetre->taille.pos_es_dimension);
      test_add_buff_impression (pos_buff,
          donnees_bx_fenetre->impression.handle,
          donnees_bx_fenetre->impression.pos_es_impression,
          donnees_bx_fenetre->impression.pos_es_nom_doc);
      test_envoi_reflets_graphiques_fenetre (pos_buff, dwId_bx_fenetre, FALSE);
			}
      break;

    case ACTION_SUPPRESSION_FENETRE:
      add_buff_fen_efface (pos_buff, donnees_bx_fenetre->invisible.handle);
      break;
    }
  } // AjouteBufferSortiesGraphiquesFenetre

// --------------------------------------------------------------------------
//													Fonctions export�es
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// traitement des entr�es graphiques
void recoit_gr (BOOL bPremierPassage)
  {
  PX_VAR_SYS pos_es_user = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_user);
  DWORD nb_page = bdgr_nombre_pages ();
  t_anm_gr_msg_event MessageRecu;
  BOOL fini = FALSE;
  BOOL attente = FALSE;
  DWORD n_page;
  FLOAT *num_exe;

	// Boucle de remise � 0 de tous les elements tableaux de fen_user
  for (n_page = 1; (n_page <= nb_page); n_page++)
    {
    num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pos_es_user->dwPosCourEx) + n_page - 1);
    (*num_exe) = (FLOAT) (0);
		}
	// Boucle de r�ception des messages du client AnmGr
  while (!fini)
    {
		PVOID pMessageRecu = &MessageRecu;
    if (ITCServeurReceptionAnmGr.dwLecture (&pMessageRecu, FALSE, TRUE) == 0)
      {
      switch ((MessageRecu.Entete).IdMessage)
        {
        case ITC_MSG_OUVRE:
					fini = TRUE;
					break;
        case ITC_MSG_FERME:
					MajStatutGr (VI_REC_ERR_GET_MBX); // animateur arrete
					fini = TRUE;
					break;
        case ITC_MSG_USER:
					switch (traite_message_recu (&(MessageRecu.Donnee)))
						{
						case DEBUT_TRAME:
							attente = TRUE;
							break;
						case CONTENU_TRAME:
							break;
						case FIN_TRAME:
							fini = TRUE;
						}
					break;
        }
      }
    else
      fini = !attente;
    }
  } // recoit_gr


//-------------------------------------------------------------------------
// emission des sorties graphiques
//-------------------------------------------------------------------------
void sorties_gr (BOOL bPremierPassage)
  {
  // Canal Ouvert ?
  if (ITCServeurEmissionAnmGr.bAUnCorrespondant(0))
    {
		DWORD nbr_enr_bx_fen = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_fenetre_vi);

		// Fen�tre existe ?
    if (nbr_enr_bx_fen != 0)
      {
			DWORD wError;
			DWORD pos_buff;
			DWORD dwId_bx_fenetre;
			tx_fenetre_vi* donnees_bx_fenetre;

			// prise en compte des courbes � �chantillonage temporel
			DWORD dwNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_vdi_courbe_temps);
			for (DWORD dwNEnr = 1; (dwNEnr <= dwNbEnr); dwNEnr++)
				{
				PXGR_COURBE_TEMPS ptr_courbe_temps = (PXGR_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_temps, dwNEnr);
				ptr_courbe_temps->nbr_stocke = 0;
				BOOL bEchantillonageEnCours = CPcsVarEx::bGetValVarLogEx(ptr_courbe_temps->pos_es_enclanche);
				// lancer l'�chantillonage ?
				if ((bEchantillonageEnCours) && (!CPcsVarEx::bGetValMemVarLogEx(ptr_courbe_temps->pos_es_enclanche)))
					{
					MinuterieLanceMsEch (&(ptr_courbe_temps->periode_ech_tampon), NB_MS_PAR_CENTIEME (ptr_courbe_temps->periode_ech_duree));
					ptr_courbe_temps->nbr_stocke = 1;
					}
				else
					{
					if (bEchantillonageEnCours)
						{
						while (bMinuterieRelanceSiEcheanceEch (&(ptr_courbe_temps->periode_ech_tampon), NB_MS_PAR_CENTIEME(ptr_courbe_temps->periode_ech_duree)))
							{
							ptr_courbe_temps->nbr_stocke++;
							}
						}
					else
						{
						ptr_courbe_temps->tampon_plein = FALSE;
						ptr_courbe_temps->courant_tampon = 0;
						}
					}
				PFLOAT ptr_num_cour = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, ptr_courbe_temps->pos_es_stocke);
				PFLOAT tab_num = ptr_courbe_temps->pfBufferEchantillons;
				for (DWORD j = 1; (j <= ptr_courbe_temps->nbr_stocke); j++)
					{
					tab_num [ptr_courbe_temps->courant_tampon] = (*ptr_num_cour);
					ptr_courbe_temps->courant_tampon++;
					if (ptr_courbe_temps->courant_tampon >= ptr_courbe_temps->nbr_echantillon)
						{
						ptr_courbe_temps->courant_tampon = 0;
						ptr_courbe_temps->tampon_plein = TRUE;
						}
					}
				}

			// prise en compte des courbes avec �chantillonage sur commande
			dwNbEnr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bx_vdi_courbe_cmd);
			for (DWORD dwNEnr = 1; (dwNEnr <= dwNbEnr); dwNEnr++)
				{
				PXGR_COURBE_CMD		pXGrCourbeCmd = (PXGR_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_cmd, dwNEnr);
				if (CPcsVarEx::bGetValVarLogEx (pXGrCourbeCmd->pos_es_reset_buffer))
					{
					pXGrCourbeCmd->tampon_plein = FALSE;
					pXGrCourbeCmd->courant_tampon = 0;
					}
				if (CPcsVarEx::bGetValVarLogEx(pXGrCourbeCmd->pos_es_echantillonnage))
					{
					PFLOAT ptr_num_cour = (FLOAT *)pointe_enr (szVERIFSource, __LINE__, b_cour_num, pXGrCourbeCmd->pos_es_stocke);
					pXGrCourbeCmd->pfBufferEchantillons [pXGrCourbeCmd->courant_tampon] = (*ptr_num_cour);
					pXGrCourbeCmd->courant_tampon++;
					if (pXGrCourbeCmd->courant_tampon >= pXGrCourbeCmd->nbr_echantillon)
						{
						pXGrCourbeCmd->courant_tampon = 0;
						pXGrCourbeCmd->tampon_plein = TRUE;
						}
					}
				}

      // 1� Passage
      if ((bPremierPassage) || (!(ok_init_gr)))
        {
        reset_buff_vi (&pos_buff);
        
				// Boucle sur les Fenetres
        for (dwId_bx_fenetre = 1; dwId_bx_fenetre <= nbr_enr_bx_fen; dwId_bx_fenetre++)
          {
          donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, dwId_bx_fenetre);
          if (donnees_bx_fenetre->existe)
            {
            if (CPcsVarEx::bGetValVarLogEx(donnees_bx_fenetre->visible.pos_es))
              {
              AjouteBufferSortiesGraphiquesFenetre (&pos_buff, dwId_bx_fenetre, ACTION_CREATION_FENETRE);
              }
            }
          }

        // Existe Message � Emettre ?
        if (pos_buff != 0)
          {
          // oui => attente ack pour un eventuel forcage de premier passage dans code
          // Au vrai premier passage le wait sort de suite car pas de fct async en cours
          bThreadAttenteFinTraiteMessage (hThrdAckGr, VI_TIMEOUT_ACK);

          add_buff_ack_vi (&pos_buff);
          if (envoi_message_mbx (&pos_buff,TRUE))
            {
            // On lance la reception ack, qui sera teste aux cycles suivants...
            ThreadEnvoiMessage (hThrdAckGr, THRD_FCT_EXEC, NULL, NULL, &wError);
            }
          } // if (pos_buff != 0)
        } // if ((bPremierPassage) || (!(ok_init_gr)))
      else
        {
				// Pas 1� Passage :
        // Attente Acquittement cycle pr�c�dent
        if (bThreadAttenteFinTraiteMessage (hThrdAckGr, VI_TIMEOUT_ACK))
          {
          // On a recu l'acquittement : effacement du buffer de message possible
          reset_buff_vi (&pos_buff);

          // Boucle sur les Fenetres
          for (dwId_bx_fenetre = 1; dwId_bx_fenetre <= nbr_enr_bx_fen; dwId_bx_fenetre++)
            {
            donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, dwId_bx_fenetre);
            if (donnees_bx_fenetre->existe)
              {
              BOOL bMemFenetreVisible = CPcsVarEx::bGetValMemVarLogEx (donnees_bx_fenetre->visible.pos_es);
         			BOOL bFenetreVisible = CPcsVarEx::bGetValVarLogEx (donnees_bx_fenetre->visible.pos_es);

              //-------------------------------------------------------------------------------------
              //  bMemFenetreVisible  |   bFenetreVisible   |   Messages � envoyer                  |
              //------------------------------------------------------------------------------------|
              //     0								|      0							| aucun                                 |
              //     0								|      1							| visible,taille,page,calque,impression |
              //     1								|      0							| invisible                             |
              //     1								|      1							| ce qui a chang�                       |
              //-------------------------------------------------------------------
              if (bMemFenetreVisible)
                {
                if (bFenetreVisible)
                  {
                  // Fen�tre reste visible
                  AjouteBufferSortiesGraphiquesFenetre (&pos_buff, dwId_bx_fenetre, ACTION_ANIMATION_FENETRE);
                  }
                else
                  {
                  // Fen�tre disparait
                  AjouteBufferSortiesGraphiquesFenetre (&pos_buff, dwId_bx_fenetre, ACTION_SUPPRESSION_FENETRE);
                  }
                }
              else
                {
                if (bFenetreVisible)
                  {
									// Apparition fen�tre
                  AjouteBufferSortiesGraphiquesFenetre (&pos_buff, dwId_bx_fenetre, ACTION_CREATION_FENETRE);
                  }
                else
                  {
                  // Fen�tre reste invisible: pas d'envoi
                  }
                }
              }
            } // fin Boucle sur les Fenetres

          // Message � �mettre ?
          if (pos_buff != 0)
            {
						// oui => envoie buffer � l'animateur
            add_buff_ack_vi (&pos_buff);
            if (envoi_message_mbx (&pos_buff,TRUE))
              {
              // On lance la reception ack, qui sera teste aux cycles suivants...
              ThreadEnvoiMessage (hThrdAckGr, THRD_FCT_EXEC, NULL, NULL, &wError);
              }
            else
              {
              MajStatutGr (VI_SEND_ERR_PUT_MBX);
              }
            } // pos_buff != 0
          } // acquittement
        else
          { // time_out ack d�pass�
          MajStatutGr (VI_SEND_TIMEOUT);
          }
        } // pas 1� passage
      } // existe bx_fenetre_vi
    } // le canal est ouvert
  } // sorties_gr

	//-------------------------- fin Scrut_gr.c -----------------------------

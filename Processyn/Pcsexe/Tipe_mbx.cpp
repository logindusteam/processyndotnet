// déclaration du serveur PcsExe et de ses services
#include "stdafx.h"
#include "std.h"
#include "USem.h"
#include "ITC.h"
#include "Tipe_mbx.h"

// Nom des services d'animation des alarmes
const char szTopicReceptionAnmAl [ITC_USR_MAX_NAME] = "Reception Anim Alarmes";
const char szTopicEmissionAnmAl  [ITC_USR_MAX_NAME] = "Emission Anim Alarmes";

// Nom des services d'animation graphique
const char szTopicReceptionAnmGr [ITC_USR_MAX_NAME] = "Reception Anim Graphique";
const char szTopicEmissionAnmGr  [ITC_USR_MAX_NAME] = "Emission Anim Graphique";

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : scrut_sn.C                                               |
 |   Auteur  : AC					        	|
 |   Date    : 7/7/93 					        	|
 |   Version : 4.00							|
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "son.h"
#include "pcs_sys.h"
#include "scrut_sn.h"
#include "Verif.h"
VerifInit;

#define max_note 70

// --------------------------------------------------------------------------
// interface driver SON
// --------------------------------------------------------------------------
void  scrute_son (void)
  {
  PX_VAR_SYS	index = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, son);
  PBOOL ads_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (index->dwPosCourEx));
  PFLOAT ads_num;
  DWORD note_w;
  DWORD duree_w;

  if (*ads_log)
    {
    index = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, note);
    ads_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (index->dwPosCourEx));
    note_w = (DWORD)(*ads_num);
    if (note_w < max_note)
      {
      index = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, duree);
      ads_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (index->dwPosCourEx));
      duree_w = (DWORD)(*ads_num);
      Sonne (note_w, duree_w);
      }
    }
  }

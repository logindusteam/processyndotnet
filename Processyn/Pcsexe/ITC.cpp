//--------------------------------------------------------
// ITC.cpp: implementation of the CITC class.
// syst�me de communication client - serveur processyn.
//
// Remarques : 
//	Impl�mentation multi-thread en intra-process.
//--------------------------------------------------------
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "std.h"
#include "USem.h"
#include "UStr.h"
#include "threads.h"
#include "MemMan.h"
#include "UListeH.h"
#include "Verif.h"

#include "ITC.h"

VerifInit;

#define SRV_MAX_BUFF_SIZE 32767	// taille maximale de donn�es que l'on peut envoyer $$


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CITC::CITC()
	{
	m_nEtat = CNX_INVALIDE;													// Etat courant de la connexion
	m_hsemAccesExclusif  = hSemCree (FALSE);				// s�maphore auto pris d'acc�s exclusif � cette connexion
	m_pQueueRX = NULL;
	m_szNomTopic [0]= 0;	// nom du service g�r� par cette connexion
	m_bServeur = FALSE;												// Connexion de type serveur (TRUE) ou client (FALSE)
	m_bDansListeAttente = FALSE;							// Vrai si le handle de cette connexion est actuellement dans hListeHcnxAConnecter
	}

CITC::~CITC()
	{
	// Fermeture des ressources allou�es
	FermeNonExclusif();
	if (m_hsemAccesExclusif)
		bSemFerme (&m_hsemAccesExclusif);
	m_hsemAccesExclusif = NULL;
	}


//-----------------------------------------------------------------
// Demande d'un acc�s excusif � une connexion (Attente infinie si n�cessaire)
BOOL CITC::bAccesExclusif ()
	{
	BOOL bRes = FALSE;
	if (m_hsemAccesExclusif)
		{
		if (dwSemAttenteLibre (m_hsemAccesExclusif, INFINITE) == WAIT_OBJECT_0)
			bRes = TRUE;
		}
	return bRes;
	}

//-----------------------------------------------------------------
// Fin d'un acc�s excusif � une connexion
void CITC::FinAccesExclusif ()
	{
	Verif (bSemLibere (m_hsemAccesExclusif));
	}


//-----------------------------------------------------------------
// Lib�re les ressources d'une connexion
//-----------------------------------------------------------------
void CITC::LibereQueue ()
  {
	// Queue ouvert ?
	if (m_pQueueRX != NULL)
		{
		// Oui => ferme la
		m_nEtat = CNX_INVALIDE;
		if (m_pQueueRX->dwSupprimeReference()==0)
			{
			delete m_pQueueRX;
			}
		m_pQueueRX = NULL;
		}
  }

// Ferme une connexion et d�connecte l'�ventuel correspondant Sans AccesExclusif
void CITC::FermeNonExclusif ()
	{
	switch (m_nEtat)
		{
		case CNX_EN_ATTENTE_CONNEXION :  // ligne en attente d'ouverture 
		case CNX_CONNEXION_EN_COURS :
			{
			// Correspondant connect� ?
			if (m_pQueueRX)
				{
				// Oui => note le nouvel �tat
				m_nEtat = CNX_FIN_CONNEXION;

				// d�connecte le
				ITC_HEADER_MESSAGE headerFermeture = {ITC_MSG_FERME, 0};

				// envoi du message de fermeture au correspondant
				if (!m_bEnReception)
					Verif (bEnvoiMessageAuCorrespondant (&headerFermeture, NULL, FALSE));
				}

			// fermeture de ce CITC et �ventuellement destruction de la queue
			LibereQueue();
			}
			break;

		default:
			//case CNX_INVALIDE, CNX_FIN_CONNEXION
			LibereQueue ();
			break;
		}
	}

//--------------------------------------------------
// Envoi d'un message au correspondant
// Si Ok, alloue un buffer compos� du header suivi �ventuellement par des donn�es
// Renvoie TRUE si envoi Ok, FALSE sinon.       
//--------------------------------------------------
BOOL CITC::bEnvoiMessageAuCorrespondant 
	(
	PITC_HEADER_MESSAGE pEntete, // doit �tre rempli
	PVOID pvDonnees, // peut �tre NULL si pEntete.dwTailleDonnees = 0
	BOOL bDemandeAR)
  {
	BOOL bRet = FALSE;

	if (m_pQueueRX && m_pQueueRX->bOuverte())
		{
		// enregistre l'�ventuelle attente d'AR
		if (bDemandeAR)
			{
			m_pQueueRX->bDemandeAR();
			}

		// Message compatible avec l'�tat actuel ?
		if (pEntete &&
			(((pEntete->IdMessage == ITC_MSG_OUVRE) && (m_nEtat == CNX_EN_ATTENTE_CONNEXION)) ||
			(m_nEtat == CNX_CONNEXION_EN_COURS) || ((pEntete->IdMessage == ITC_MSG_FERME) && (m_nEtat == CNX_FIN_CONNEXION))))
			{
			// oui => transfert des donn�es au correspondant :
			// recopie les donn�es dans un buffer allou� dynamiquement
			DWORD	dwTailleEnvoi = sizeof (ITC_HEADER_MESSAGE) + pEntete->dwTailleDonnees;
			PVOID	pBufEnvoi = pMemAlloue (dwTailleEnvoi);

			// Copie de l'ent�te
			CopyMemory (pBufEnvoi, pEntete, sizeof (*pEntete));

			// Copie �ventuelle des donn�es
			if (pEntete->dwTailleDonnees)
				CopyMemory (&((PMESSAGE_ITC)pBufEnvoi)->aDonnees, pvDonnees, pEntete->dwTailleDonnees);

			// Depot du nouveau message chez le correspondant Ok ?
			if (bAddQueueRX(dwTailleEnvoi, pBufEnvoi))
				bRet = TRUE;

			// lib�re le buffer si quelque chose n'a pas fonctionn�
			if ((!bRet) && pBufEnvoi)
				MemLibere (&pBufEnvoi);
			}
		// Lib�re le s�maphore d'attente AR si n�cessaire
		if (bDemandeAR && (!bRet))
			{
			m_pQueueRX->bEnvoiAR();
			}
		} // if (m_pQueueRX)
  return bRet;
  } // bEnvoiMessageAuCorrespondant

//-----------------------------------------------------------------------------
// Permet � un correspondant d'�crire directement le message dans la queue de r�ception
BOOL CITC::bAddQueueRX (DWORD dwTailleEnvoi, PVOID pBufEnvoi)
	{
	BOOL bRet = FALSE;
	Verif(!m_bEnReception);

	// �tat compatible avec �criture dans la queue de r�ception ?
	if (pBufEnvoi &&
		(m_nEtat == CNX_CONNEXION_EN_COURS) ||
		(m_nEtat == CNX_FIN_CONNEXION))
		{
		// oui => ajout du message dans la queue du correspondant
		//if (bEcritQueue (m_hQueueRX, 0, dwTailleEnvoi, pBufEnvoi))
		if (m_pQueueRX && m_pQueueRX->bEcrit(0, dwTailleEnvoi, pBufEnvoi))
			bRet = TRUE;
		}
	return bRet;
	}


//-----------------------------------------------------------------------------
// Renvoie TRUE si pCorrespondant est autoris� � se connecter
BOOL CITC::bAccepteConnexion (CITC* pCorrespondant)
	{
	BOOL	bRes = FALSE;
	if (bAccesExclusif ())
		{
		// correspondant en attente de connexion, l'un est client et l'autre serveur, m�me nom de service ?
		if ((m_nEtat == CNX_EN_ATTENTE_CONNEXION) &&
			(m_bServeur ^ pCorrespondant->m_bServeur) &&
			(m_bEnReception ^ pCorrespondant->m_bEnReception) &&
			(StrICompare (m_szNomTopic, pCorrespondant->m_szNomTopic) == 0))
			{
			// oui => �tablissement de la communication
			m_nEtat = CNX_CONNEXION_EN_COURS;
			m_pQueueRX = pCorrespondant->m_pQueueRX;
			}

		// lib�re le s�maphore d'acc�s exclusif
		FinAccesExclusif ();
		}

	return bRes;
	}


//--------------------------------------------------------------------------
// Ouverture en tant que serveur (connection au client faite ult�rieurement par le client).
//--------------------------------------------------------------------------
BOOL CITC::bCreeServeur (PCSTR pszNomTopic, BOOL bEnReception)
  {
  BOOL bRes = FALSE;

	// Acc�s exclusif � cette connexion ?
	if (bAccesExclusif ())
		{
		// Oui => lib�re l'�ventuelle connexion en cours
		FermeNonExclusif();

		// recopie du nom du service
		StrCopy (m_szNomTopic, pszNomTopic);

		m_nEtat = CNX_EN_ATTENTE_CONNEXION;

		m_bServeur = TRUE;
		m_bEnReception = bEnReception;
		m_bDansListeAttente = FALSE;

		// Cr�e la queue commune
		m_pQueueRX = new CQueueLI;
		if (m_pQueueRX->bCree())
			{
			bRes = TRUE;
			}
		else
			{
			// non => on lib�re tout
			LibereQueue ();
			}
		FinAccesExclusif();
		}
	return bRes;
  } // bCreeServeur

//--------------------------------------------------------------------------
// Ouverture en tant que Client (avec connection au serveur).
//--------------------------------------------------------------------------
BOOL CITC::bCreeClient (PCSTR pszNomTopic, CITC* pITCServeur, BOOL bEnReception)
  {
	// Lib�re l'�ventuelle connexion en cours
  BOOL bRes = FALSE;

	// Acc�s exclusif � cette connexion ?
	if (bAccesExclusif ())
		{
		// Oui => lib�re l'�ventuelle connexion en cours
		FermeNonExclusif();

		// recopie du nom du service
		StrCopy (m_szNomTopic, pszNomTopic);
		m_nEtat = CNX_EN_ATTENTE_CONNEXION;
		m_bServeur = FALSE;
		m_bEnReception = bEnReception;
		m_bDansListeAttente = FALSE;

		// correspondant en attente de connexion, l'un est client et l'autre serveur, m�me nom de service ?
		if ((pITCServeur && pITCServeur->m_pQueueRX) &&
			(m_nEtat == CNX_EN_ATTENTE_CONNEXION) &&
			(m_bServeur ^ pITCServeur->m_bServeur) &&
			(m_bEnReception ^ pITCServeur->m_bEnReception) &&
			(StrICompare (m_szNomTopic, pITCServeur->m_szNomTopic) == 0))
			{
			// oui => �tablissement de la communication : note la connexion
			m_nEtat = CNX_CONNEXION_EN_COURS;
			m_pQueueRX = pITCServeur->m_pQueueRX;
			m_pQueueRX->AjouteReference();
			bRes = TRUE;
			}
		}

	// oui => termin�
	FinAccesExclusif ();
 
	return bRes;
  } // bCreeClient


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//						Fonctions export�es
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//-------------------------------------------------------------------------
// Cr�ation d'une connexion en attente d'un appel d'un client.
//------------------------------------------------------------------------
BOOL CITC::bCreeServeurRX
	(
	const char * pszNomTopic		// Nom du service support� ou NULL.
	)
  {
  return bCreeServeur (pszNomTopic, TRUE);
  } // bCreeServeurRX

BOOL CITC::bCreeServeurTX 
	(
	const char * pszNomTopic		// Nom du service support� ou NULL.
	)
  {
  return bCreeServeur (pszNomTopic, FALSE);
  } // bCreeServeurTX

//-------------------------------------------------------------------------
// Cr�ation d'une connexion en communication avec un service d'un serveur ITC.
// -----------------------------------------------------------------------
BOOL CITC::bCreeClientRX 
	(
	const char * pszNomTopic,		// Nom du service auquel on veut se connecter
	CITC* pITCServeur						// ITC du serveur auquel se connecter
	)
  {
  return bCreeClient (pszNomTopic, pITCServeur, TRUE);
  } // dwCreeClientRX

BOOL CITC::bCreeClientTX 
	(
	const char * pszNomTopic,		// Nom du service auquel on veut se connecter
	CITC* pITCServeur						// ITC du serveur auquel se connecter
	)
  {
  return bCreeClient (pszNomTopic, pITCServeur, FALSE);
  } // bCreeClientRX

//------------------------------------------------------------------------
// Ferme une connexion et d�connecte l'�ventuel correspondant
//------------------------------------------------------------------------
void CITC::Ferme ()
  {
	if (bAccesExclusif ())
		{
		FermeNonExclusif();
		FinAccesExclusif();
		}
  } // Ferme

//----------------------------------------------------------------------
// Envoi d'un message � un correspondant.
// $$ Le buffer de donn�es doit commencer par une structure ITC_HEADER_MESSAGE
// (la valeur de ses champs sera mise � jour par cette fonction)
//-----------------------------------------------------------------------
DWORD CITC::dwEcriture
	(
	PITC_HEADER_MESSAGE	pHeaderMessage,	// adresse du header contenant les donn�es a envoyer suivies par les donn�es
	DWORD			dwTailleMessage,	// taille du buffer 
	BOOL			bDemandeAR				// active l'attente d'Accus� de R�ception si TRUE
	)
  {
  DWORD				dwRet = ITC_ERR_ITC_UNKOWN_STATE;
  
	if (bAccesExclusif ())
		{
		if (m_nEtat == CNX_CONNEXION_EN_COURS)
			{
			// Mise � jour du header de message
			pHeaderMessage->IdMessage = ITC_MSG_USER;
			pHeaderMessage->dwTailleDonnees = dwTailleMessage - sizeof(ITC_HEADER_MESSAGE);

			// Envoi du message utilisateur au correspondant
			if (bEnvoiMessageAuCorrespondant (pHeaderMessage, &((PMESSAGE_ITC)pHeaderMessage)->aDonnees, bDemandeAR))
				dwRet = 0;
			else
				dwRet = ITC_ERR_PUT_MSG;
			}
		else
			dwRet = ITC_ERR_ITC_NOT_COMM_STATE;
		FinAccesExclusif ();
		}
  return dwRet;
  } // dwITCEcriture

//----------------------------------------------------------------------
// Envoi d'un message Quelconque � un correspondant.
// Le buffer de donn�es NE NECESSITE PAS de commencer par une structure ITC_HEADER_MESSAGE
//-----------------------------------------------------------------------
DWORD CITC::dwEcritureBuffer
	(
	PVOID			pBufMessage,	// adresse du header contenant les donn�es a envoyer suivies par les donn�es
	DWORD			dwTailleMessage,	// taille du buffer 
	BOOL			bDemandeAR				// active l'attente d'Accus� de R�ception si TRUE
	)
  {
  DWORD				dwRet = ITC_ERR_ITC_UNKOWN_STATE;
  
	if(dwTailleMessage)
		Verif (pBufMessage);
	if (bAccesExclusif ())
		{
		if (m_nEtat == CNX_CONNEXION_EN_COURS)
			{
			// Mise � jour du header de message
			ITC_HEADER_MESSAGE HeaderMessage;
			HeaderMessage.IdMessage = ITC_MSG_USER;
			HeaderMessage.dwTailleDonnees = dwTailleMessage;

			// Envoi du message utilisateur au correspondant
			if (bEnvoiMessageAuCorrespondant (&HeaderMessage, pBufMessage, bDemandeAR))
				dwRet = 0;
			else
				dwRet = ITC_ERR_PUT_MSG;
			}
		else
			dwRet = ITC_ERR_ITC_NOT_COMM_STATE;
		FinAccesExclusif ();
		}
  return dwRet;
  } // dwITCEcriture

//--------------------------------------------------------------------------
// R�ception d'un message syst�me ou issu du correspondant.
//--------------------------------------------------------------------------
DWORD CITC::dwLecture 
	(
	PVOID * ppBufDest,	// adresse du pointeur sur le buffer recevant les donn�es lues
	BOOL	bAvecAttente,	// Attente avec time out courant si TRUE, pas d'attente sinon
	BOOL	bCopieBuffer	// Si TRUE recopie le message initial, sinon donne le message initial � lib�rer
	)
  {
	DWORD	dwRet = ITC_ERR_ITC_DISCONNECTED;
  DWORD code_ret_sem = 0;
  PVOID	pBufRX = NULL;

	if (bAccesExclusif ())
		{
		// lit la queue de r�ception
		DWORD	dwTailleMessageRecu = 0;
		HSEM	hsemQueueRX = NULL;
		if (m_pQueueRX)
			{
			dwRet = m_pQueueRX->uLit (NULL, &dwTailleMessageRecu, &pBufRX, FALSE, &hsemQueueRX);

			// pas de message re�u ?
			if (dwRet != 0)
				{
				// oui => attente d'un message n�cessaire ?
				if (bAvecAttente)
					{
					// oui => attente avec time out de l'arriv�e d'un message
					FinAccesExclusif ();
					dwRet = dwSemAttenteLibre (hsemQueueRX, ITC_DEF_ACK_TIMEOUT);
					if (bAccesExclusif ())
						{
						// traite selon le r�sultat de l'attente
						switch (dwRet)
							{
							case 0:
								//dwRet = uLitQueue (m_hQueueRX, NULL, &dwTailleMessageRecu, &pBufRX, FALSE, NULL);
								dwRet = m_pQueueRX->uLit (NULL, &dwTailleMessageRecu, &pBufRX, FALSE, NULL);
								if (dwRet != 0)
									dwRet = ITC_NO_MSG;
								break;
							case SEM_TIMEOUT:
								dwRet = ITC_ERR_TIMEOUT;
								break;
							default:
								dwRet = ITC_ERR_WAIT_EVENT;
							}
						FinAccesExclusif ();
						} // if (bAccesExclusif ())
					else
						dwRet = ITC_ERR_ITC_DISCONNECTED;
					}
				else
					dwRet = ITC_NO_MSG;
				}
			FinAccesExclusif ();
			}

		// un message a �t� re�u ?
		if (dwRet == 0)
			{
			// oui => prise en compte du message re�u
			if (bCopieBuffer)
				{
				// on le recopie dans le buffer de l'appelant et on le lib�re
				memcpy (*ppBufDest, pBufRX, dwTailleMessageRecu);
				MemLibere (&pBufRX);
				}
			else
				{
				// on renvoie directement l'adresse du buffer message sans le liberer
				*ppBufDest = pBufRX;
				}
			}
		}

  return dwRet;
  } // dwLecture

//--------------------------------------------------------------------------
// Renvoie le s�maphore d'attente de r�ception de la connexion, NULL si erreur
//--------------------------------------------------------------------------
HSEM CITC::hsemAttenteReception ()			// handle de connexion � utiliser
	{
	HSEM	hsemRes = NULL;

	// r�cup�re les infos de de la connexion courante
	if (bAccesExclusif ())
		{
		if (m_pQueueRX)
			Verif (m_pQueueRX->bGetSemAttenteElement (&hsemRes));
		FinAccesExclusif ();
		}

	return hsemRes;
  } // hsemCnxAttenteReceptionITC

//--------------------------------------------------------------------------
// Savoir si une connexion serveur est reli�e � un correspondant
//--------------------------------------------------------------------------
BOOL CITC::bAUnCorrespondant
	(
	DWORD dwTimeOutAttente		// Time out max d'attente de la connexion en ms ou ITC_INDEFINITE_WAIT
	)
  {
	BOOL bRes = FALSE;

	// Selon l'�tat de l'ITC
	switch (m_nEtat)
		{
		case CNX_CONNEXION_EN_COURS:
			{
			// un correspondant
			bRes = TRUE;
			}
			break;

		case CNX_EN_ATTENTE_CONNEXION: // attente connexion possible ?
			{
			// oui => attente connexion : r�cup�re le s�maphore d'attente de connexion
			if (bAccesExclusif ())
				{
				HSEM hsemAttenteConnexion = NULL;
				if (m_pQueueRX)
					Verif (m_pQueueRX->bGetSemAttenteConnexion (&hsemAttenteConnexion));
				FinAccesExclusif ();
				if (dwSemAttenteLibre (hsemAttenteConnexion, dwTimeOutAttente) == 0)
					{
					bRes = TRUE;
					m_nEtat = CNX_CONNEXION_EN_COURS;
					}
				}
			}
			break;
		} // switch (m_nEtat)

	return bRes;
  } // bAUnCorrespondant

//------------------------------------------------------------------------
// Envoi d'un accus� de reception. 
//------------------------------------------------------------------------
DWORD CITC::dwEnvoiAR 
	(
	)
  {
	DWORD	dwRes = ITC_ERR_ITC_NOT_COMM_STATE;
	if (bAccesExclusif ())
		{
		// Action compatible avec l'�tat actuel du CNX ?
		if ((m_nEtat == CNX_CONNEXION_EN_COURS)&&
				(m_pQueueRX))
			{
			// oui => EnvoiAR possible ?
			if (m_pQueueRX->bEnvoiAR())
				{
				// oui => Ok
				dwRes = 0;
				}
			}

		FinAccesExclusif ();
		}
	return dwRes;
  } // dwITCEnvoiAR

//-------------------------------------------------------------------------
// Attente d'un accus� de reception sur une connexion pendant au plus timeout ms.
//-------------------------------------------------------------------------
BOOL CITC::bAttenteAR 
	(
	DWORD dwTimeOutAttente	// Time out max d'attente de l'accus� de r�ception (ms) ou ITC_INDEFINITE_WAIT
	)
  {
  BOOL bRes = FALSE;
	if (bAccesExclusif ())
		{
		// attente AR possible ?
		if (m_nEtat == CNX_CONNEXION_EN_COURS && m_pQueueRX)
			{
			CQueueLI * pQueueRX = m_pQueueRX;

			// attente de l'arriv�e de l'AR
			FinAccesExclusif ();
			if (pQueueRX->bAttenteAR(dwTimeOutAttente))
				{
				bRes = TRUE;
				}
			}
		else
			FinAccesExclusif ();
		}

	return bRes;
  } // bAttenteAR

// ----------------------------- fin ITC.cpp -------------------------------



//-------------------------------------------------------
// ExeDde.c
// ex�cution Pcs : Interface DDE
// JS Win32 11/06/97
//-------------------------------------------------------

#include "stdafx.h"
#include "USem.h"
#include "Appli.h"
#include "DocMan.h"
#include "UEnv.h"
#include "MemMan.h"
#include "UStr.h"
#include "UDde.h"
#include "lng_res.h"
#include "tipe.h"        // Types + Constantes Processyn
#include "mem.h"         // Memory Manager
#include "pcsdlg.h"
#include "appliex.h"
#include "pcsdlgex.h"
#include "IdExeLng.h"
#include "tipe_de.h"
#include "scrut_de.h"
#include "Wexe.h"
#include "Driv_sys.h"
#include "PathMan.h"
#include "FileMan.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "ClipBMan.h"
#include "Verif.h"
#include "USignale.h"

#include "ExeDde.h"
VerifInit;

// ---------------------------------------------------------------
//--------------------------------------------------------------------------
// Gestion dynamique des liens DDE clients
//--------------------------------------------------------------------------
// --------------------------------------------------------------------------
// R�cup�re le nom d'une variable serveur ou cliente non pr�d�finie
BOOL bNomVarDdeDyn (DWORD index, PSTR pszNomVar, BOOL bServeur)
  {
  BOOL									trouve = FALSE;
  PX_VAR_DDE_CLIENT_DYN pVarClientDyn;
  PX_VAR_SERVEUR_DDE		pVarServeurDde;
  PX_E_S								pxES;

  if (bServeur)
    {
    if (existe_repere(bx_dde_var_serveur))
      {
      if (index <= nb_enregistrements (szVERIFSource, __LINE__, bx_dde_var_serveur))
        {
        pVarServeurDde = (PX_VAR_SERVEUR_DDE)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur,index);
        pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pVarServeurDde->wPosBxEs);
        StrCopy (pszNomVar, pxES->pszNomVar);
        trouve = TRUE;
        }
      }
    }
  else
    {
    if (existe_repere(bx_var_dde_client_dyn))
      {
      if (index <= nb_enregistrements (szVERIFSource, __LINE__, bx_var_dde_client_dyn))
        {
        pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn,index);
        pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pVarClientDyn->wPosBxEs);
        StrCopy (pszNomVar, pxES->pszNomVar);
        trouve = TRUE;
        }
      }
    }

  return trouve;
  } // bNomVarDdeDyn

//----------------------------------------------------------
// creation d'un lien DDE client avec un serveur externe
//----------------------------------------------------------
UINT uDDEOuvreConversationClient (PSTR pszApplic, PSTR pszTopic, UINT wNbItems,
	t_func_terminate FctTerminate, LPARAM lParam, HCONV * phConv)
  {
  UINT 	uRes = 0;

	// $$ DDEML ouvert ?
	if (bDDEOuvert ())
		{
		// oui => Ouverture d'une conversation
		PENV_CONV pEnvConv = pInitConversationDDE (pszApplic, pszTopic, NULL, //PFNDDE pfnClient, 
			lParam, (LPARAM) FctTerminate);

		// Erreur d'ouverture ?
		if (pEnvConv)
			*phConv = pEnvConv->hConv;
		else
			{
			*phConv = NULL;
			uRes = uDerniereErreurDDE();
			}
		}
/*
  DWORD             wIndex;
  PLIEN_DDE					pLien;
  PITEM_DDE					ptDdeItem;
  PLISTE_LIENS_DDE	paListeLiens;

  *phEnvConv = NULL;
  if (wNbItems > 0)
    {
    if ((paListeLiens = pMemAlloue (sizeof(LISTE_LIENS_DDE))) != NULL)
      {
      *phEnvConv = paListeLiens;
      if ((pLien = pMemAlloue (sizeof(LIEN_DDE))) != NULL)
        {
        paListeLiens->pLienDde = pLien;

        StrCopy (pLien->pszApplic, pszApplic);
        StrCopy (pLien->pszTopic, pszTopic);
        pLien->wNbItems = wNbItems;
        pLien->FctTerminate = FctTerminate;
        pLien->Param = Param;
        pLien->wStatus = DDE_ST_WAIT_INIT;

        // cree et set semaphore
        DdeCreateSem (phSemDde);
        pLien->handleSem = *phSemDde;

        pLien->hwndClient = NULL;
        //
        if ((pLien->ptrItems = pMemAlloue (pLien->wNbItems * sizeof(ITEM_DDE))) != NULL)
          {
          ptDdeItem = (PITEM_DDE) pLien->ptrItems;
          for (wIndex = 0; wIndex < pLien->wNbItems; wIndex++)
            {
            ptDdeItem->pszItem[0] = '\0';
            ptDdeItem->pData = NULL;
            ptDdeItem->wSizeData = 0;
            ptDdeItem->FctNotif = NULL;
            ptDdeItem->Param = NULL;
            ptDdeItem->wStatus = DDE_ST_NOT_DEFINE;
            ptDdeItem->pDdeStructPost = NULL;
            // creer semaphores
            //
            DdeCreateSem (&ptDdeItem->handleSem);
            //
            ptDdeItem++;
            }
            //
          if (!::PostMessage (hwndClientThrd, WM_DEMANDE_INITIATE,
                           (WPARAM)(paListeLiens), 0L))
            {
            // penser au del des semaphores
            MemLibere (&pLien);
            MemLibere (&paListeLiens);
            wCodeRet = DDE_ERR_POST_MSG;
            }
          }
        else
          {
          MemLibere (&pLien);
          MemLibere (&paListeLiens);
          wCodeRet = DDE_ERR_NO_MEM;
          }
        }
      else
        {
        wCodeRet = DDE_ERR_NO_MEM;
        MemLibere (&paListeLiens);
        }
      }
    else
      {
      wCodeRet = DDE_ERR_NO_MEM;
      }
    }
  else
    {
    wCodeRet = DDE_ERR_NO_ITEM;
    }
*/
  return uRes;
  } // uDDEOuvreConversationClient

//----------------------------------------------------------
//                 suppression d'un lien DDE
// on suppose que toutes les transactions advise sont arretees
//----------------------------------------------------------
UINT uDDEFermeConversationClient (HCONV * phConv)
  {
  UINT	uRes = 0;
	PENV_CONV pEnvConv = pEnvConvTrouve(*phConv);

	// Fin d'une conversation (d�connection du serveur)
	if (pEnvConv && bFermeConversationDDE (&pEnvConv))
		*phConv = NULL;
	else
		uRes = uDerniereErreurDDE();

/*
  PLISTE_LIENS_DDE	paListeLiens;
  PLIEN_DDE					pLien;

  if (ptrDdeListeLien != NULL)
    {
    paListeLiens = ptrDdeListeLien;
    if (paListeLiens->pLienDde != NULL)
      {
      pLien = paListeLiens->pLienDde;
      pLien->wStatus = DDE_ST_WAIT_TERM;
      //
      if (!::PostMessage (hwndClientThrd, WM_DEMANDE_TERMINATE,
                       (WPARAM)(paListeLiens->pLienDde), 0L))
        {
        wCodeRet = DDE_ERR_POST_MSG;
        }
      }
    }
*/
  return uRes;
  } // uDDEFermeConversationClient

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//											PARTIE SERVEUR
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------------
// Renvoie le num�ro d'item serveur correspondant au hszItem sp�cifi� ou 0
static DWORD dwTrouveVariableServeur (HSZ hszItem)
	{
	DWORD	dwRes = 0;
	char	szItem [NB_MAX_CAR];

	// r�cup�re le nom de l'item fourni ?
	if (DdeQueryString (hInstDDE, hszItem, szItem, NB_MAX_CAR, CP_WINANSI))
		{
		// oui => recherche parmi les Items serveurs
		char  szNomVar [NB_MAX_CAR];
		DWORD wNumItem = 1;
		while (bNomVarDdeDyn (wNumItem, szNomVar, TRUE))
			{
			if (StrICompare (szNomVar, szItem) == 0)
				{
				// recherche termin�e
				dwRes = wNumItem;
				break;
				}
			wNumItem ++;
			}
		}
	return dwRes;
	}

//----------------------------------------------------------
// Fonction Serveur DDE appel�e par DDE � chaque r�ception d'�v�nement
static HDDEDATA hddedataServeurExeDde (ENV_CONV * pEnvConv, UINT wType, UINT wFmt, HCONV hConv,
	HSZ hsz1, HSZ hsz2, HDDEDATA hData, DWORD dwData1, DWORD dwData2)
	{
	HDDEDATA hRes = NULL;

	// traitement slon le type d'�v�nement
	switch (wType)
		{
		// case XTYP_REGISTER: case XTYP_UNREGISTER: return (HDDEDATA) NULL;
		
		case XTYP_CONNECT:
			{
			// Demande client "d�but de conversation (Appli, topic)"
			// R�cup�ration du nom du topic = Nom du document = param�tre du serveur
			HSZ		hszTopic = DdeCreateStringHandle (hInstDDE, (PSTR)(ServeurDde.lParam), 0);
			
			// On renvoie TRUE si le topic est support�
			DdeFreeStringHandle(hInstDDE, hszTopic);
			hRes = (HDDEDATA)(hszTopic == hsz1);
			} // XTYP_CONNECT
			break;
			
		//case XTYP_CONNECT_CONFIRM: break; // Non implant� car rien de sp�cial � faire
			
		case XTYP_DISCONNECT:
			// $$ D�compte des variables en advise dans cette conversation
			// pas de valeur de retour
			break;
			
		case XTYP_ADVREQ:
			// n�cessit� de rafraichir des donn�es client dans une boucle Advise
		case XTYP_REQUEST:
			// demande client de lecture de donn�es durant la conversation
			{
			// Format valide ?
			if (wFmt == CF_TEXT)
				{
				// oui => item demand� = nom de variable serveur ?
				DWORD wNumItem = dwTrouveVariableServeur (hsz2);

				if (wNumItem)
					{
					// oui => renvoie la valeur de la variable au format texte :
					// r�cup�re les infos de la variable serveur
					PX_VAR_SERVEUR_DDE	pVarServeurDde = (X_VAR_SERVEUR_DDE*)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, wNumItem);
					PX_E_S					pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pVarServeurDde->wPosBxEs);
					char						szTexteValeurVar[256];
					UINT						wSizeData;
					BOOL						bTest;

					// r�cup�re sa valeur courante ?
					if (GetValeurBD (pxES->wPosEx, pxES->wTaille, pxES->wBlocGenre,
						FALSE, 0, szTexteValeurVar, &wSizeData, FALSE, &bTest) == 0)
						// oui => Renvoie la
						hRes = (HDDEDATA)DdeCreateDataHandle(hInstDDE, (PBYTE)szTexteValeurVar, StrLength (szTexteValeurVar)+1, 0, hsz2, CF_TEXT, 0 );
					}
				}
			}
			break;
			
		case XTYP_POKE:
			// �criture client -> serveur dans une conversation
			{
			// Donn�es refus�es par d�faut
			hRes = (HDDEDATA)DDE_FNOTPROCESSED;

			// Donn�es valides ?
			if (hData && (wFmt == CF_TEXT))
				{
				// oui => item demand� = nom de variable serveur ?
				DWORD wNumItem = dwTrouveVariableServeur (hsz2);

				if (wNumItem)
					{
					// oui => renvoie la valeur de la variable au format texte
					char						szTexteValeurVar[256+1];
					PX_VAR_SERVEUR_DDE	pVarServeurDde;
					PX_E_S					pxES;

					// R�cup�ration des donn�es transmises
					DdeGetData (hData, (PBYTE)szTexteValeurVar, 256+1, 0);

					// s�curit� si overflow
					szTexteValeurVar [256] = '\0';

					// r�cup�re les infos de la variable serveur
					pVarServeurDde = (PX_VAR_SERVEUR_DDE)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, wNumItem); // $$ devrait pointer sur bx_var_dde_client_dyn ?
					pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, pVarServeurDde->wPosBxEs);

					// mise � jour de la variable du serveur Ok ?
          if (PutValeurBD (pxES->wPosEx, pxES->wTaille, pxES->wBlocGenre, szTexteValeurVar) == 0)
            {
						// oui => donn�es accept�es
						hRes = (HDDEDATA)DDE_FACK;

						// On lib�re ces donn�es $$ n�cessaire dans ce cas ou dans tous les cas ?
						DdeFreeDataHandle(hData);

						// $$ Provoquer si n�cessaire un rafraichissement d'�ventuel advise sur la variable
            }
					}
				
				}
			} // XTYP_POKE
			break;
		
		case XTYP_ADVSTART:
			// demande client de cr�ation d'une boucle Advise (surveillance de donn�es)
			{
			// Demande au format texte ?
			if (wFmt == CF_TEXT)
				{
				// oui => item demand� = nom de variable serveur ?
				DWORD wNumItem = dwTrouveVariableServeur (hsz2);

				if (wNumItem)
					{
					// oui => note que cette variable est soumise � une surveillance Advise :
					// Quand elle changera, il faudra appeler DdePostAdvise (Topic, Item)
					PX_VAR_SERVEUR_DDE	pVarServeurDde = (X_VAR_SERVEUR_DDE*)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, wNumItem); // $$ devrait pointer sur bx_var_dde_client_dyn ?

					// Une surveillance de plus sur cette variable
					pVarServeurDde->wNbClientsEnAdvise ++;
				  //$$ Nota : pVarServeurDde->ptrClients n'est pas utilis�
						
					// acceptation de la boucle advise
					hRes = (HDDEDATA)TRUE;
					} // if (!pEnvConv->lParam2)
				} // if (wFmt == CF_TEXT)
			} // XTYP_ADVSTART
			break;
			
		case XTYP_ADVSTOP:
			// demande client de terminer une boucle de surveillance de donn�es
			{
			// Fin de la boucle advise
			// Demande au format texte ?
			if (wFmt == CF_TEXT)
				{
				// oui => item demand� = nom de variable serveur ?
				DWORD wNumItem = dwTrouveVariableServeur (hsz2);

				if (wNumItem)
					{
					// oui => note que cette surveillance Advise est termin�e:
					// Quand elle changera, il ne faudra plus appeler DdePostAdvise (Topic, Item)
					PX_VAR_SERVEUR_DDE	pVarServeurDde = (X_VAR_SERVEUR_DDE*)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, wNumItem); // $$ devrait pointer sur bx_var_dde_client_dyn ?

					// Une surveillance de moins sur cette variable
					VerifWarning (pVarServeurDde->wNbClientsEnAdvise > 0);
					pVarServeurDde->wNbClientsEnAdvise --;
				  //$$ Nota : pVarServeurDde->ptrClients n'est pas utilis�
						
					} // if (!pEnvConv->lParam2)
				} // if (wFmt == CF_TEXT)			// pas de retour
			}
			break;
		} // switch (wType)
	return hRes;
	} // hddedataServeurExeDde

//----------------------------------------------------------
// initialisation serveur DDE
BOOL bDdeExeInitServeur (PCSTR pszApplic, PCSTR pszTopic)
	{
  static char szTopic[DDE_MAX_NAME_SIZE];

	// Param�tre du serveur = adresse chaine Topic
	StrCopy (szTopic, pszTopic);

	return bAjouteServeurDDE (pszApplic, hddedataServeurExeDde, (LPARAM) szTopic);
	} // bDdeExeInitServeur


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//											PARTIE CLIENT
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/*
//----------------------------------------------------------
// envoyer une valeur qui a change a un client : WM_DDE_DATA
//----------------------------------------------------------
DWORD DdeDataItem (void *handleClient, void *handleServeur,
                  char *pszItem,
                  void *pData, UINT wSizeData)
  {
  DWORD            wCodeRet = 0;
  t_dde_lien_srv *pLien;

  if (wSizeData)
    { // il y a des datas a envoyer
    pLien = pGetEnv (handleServeur);
    if (TRUE)//$$(pLien->pDdeStructPost = MakeDDEObject (pszItem, DDE_FRESPONSE, CF_TEXT, pData, wSizeData)) == NULL)
      {
      wCodeRet = DDE_ERR_SEG_ALLOC;
      }
    else
      {
      if (!::PostMessage ((HWND)handleServeur, WM_ENVOI_DATA, (WPARAM)(pLien), 0L))
        {
        MemLibere (&pLien->pDdeStructPost);
        wCodeRet = DDE_ERR_POST_MSG;
        }
      }
    }

  return wCodeRet;
  }
*/

//----------------------------------------------------------
// ecrire la valeur d'un item dans un lien : WM_DDE_POKE
//----------------------------------------------------------
DWORD uDdeClientPokeItem (char *pszItem, UINT wNumItem,
                  void *pData, UINT wSizeData,
                  HCONV hConv, DWORD dwTimeOut)
  {
  DWORD             wCodeRet = 0;
/*
  PLIEN_DDE					pLien;
  PITEM_DDE					ptDdeItem;
  PLISTE_LIENS_DDE paListeLiens;

  if (wSizeData)
    { // il y a des datas a envoyer
    paListeLiens = ptrDdeListeLien;
    if (paListeLiens != NULL)
      {
      pLien = paListeLiens->pLienDde;
      if (pLien != NULL)
        {
        if ((wNumItem <= pLien->wNbItems) &&
            (pLien->ptrItems != NULL) &&
            (pLien->wStatus == DDE_ST_LINK_OK))
          {
          ptDdeItem = (PITEM_DDE) pLien->ptrItems;
          ptDdeItem += wNumItem - 1;
          if (ptDdeItem->wStatus == DDE_ST_NOT_DEFINE)
            {
            StrCopy (ptDdeItem->pszItem, pszItem);
            if ((ptDdeItem->pDdeStructPost = MakeDDEObject (pszItem, DDE_FACKREQ,
                                             CF_TEXT, pData, wSizeData)) == NULL)
              {
              wCodeRet = DDE_ERR_SEG_ALLOC;
              }
            else
              {
              ptDdeItem->wStatus = DDE_ST_WAIT_POKE;

              //--------------------- cree et set semaphore
              //
              bSemPrends (ptDdeItem->handleSem);
              *phSemDde = ptDdeItem->handleSem;
              //
              if (!::PostMessage (hwndClientThrd, WM_DEMANDE_POKE,
                               (WPARAM)(pLien), (LPARAM)(ptDdeItem)))
                {
                ptDdeItem->wStatus = DDE_ST_NOT_DEFINE;
                MemLibere (&ptDdeItem->pDdeStructPost);
                wCodeRet = DDE_ERR_POST_MSG;
                }
              }
            }
          else
            {
            wCodeRet = DDE_ERR_NOT_INDET_STATE;
            }
          }
        else
          {
          wCodeRet = DDE_ERR_NO_LINK;
          }
        }
      else
        {
        wCodeRet = DDE_ERR_NO_LINK;
        }
      }
    else
      {
      wCodeRet = DDE_ERR_NO_LINK;
      }
    }
*/
  return wCodeRet;
  } // uDdeClientPokeItem

//----------------------------------------------------------
// initialiser une transaction en lecture : WM_DDE_ADVISE
// on peut initialiser la zone data de reception des donnees
// et (ou) specifier une fct de retour
//----------------------------------------------------------
DWORD uDdeClientAdviseItem (PCSTR pszItem, UINT wNumItem,
                    void *pData, UINT wSizeData,
                    t_func_notif FctNotif, LPARAM lParam,
                    HCONV hConv, DWORD dwTimeOut)
  {
  DWORD             wCodeRet = 0;
/*
  PITEM_DDE					ptDdeItem;
  PLIEN_DDE					pLien;
  PLISTE_LIENS_DDE	paListeLiens;

  if (hEnvConv != NULL)
    {
    paListeLiens = hEnvConv;
    if (paListeLiens->pLienDde != NULL)
      {
      pLien = paListeLiens->pLienDde;
      if ((wNumItem <= pLien->wNbItems) && (pLien->ptrItems != NULL))
        {
        ptDdeItem = (PITEM_DDE) pLien->ptrItems;
        ptDdeItem += wNumItem - 1;
        if ((pLien->wStatus == DDE_ST_LINK_OK) &&
            (ptDdeItem->wStatus == DDE_ST_NOT_DEFINE))
          {
          if ((ptDdeItem->pDdeStructPost = MakeDDEObject (pszItem, DDE_FACKREQ,
                                           CF_TEXT, NULL, 0)) != NULL)
            {
            if (ptDdeItem->pData == NULL)
              { // c'est la 1� fois qu'on fait advise sur cet item
              if ((ptDdeItem->pData = pMemAlloue (wSizeData)) == NULL)
                {
                wCodeRet = DDE_ERR_NO_MEM;
                MemLibere (&ptDdeItem->pDdeStructPost);
                }
              }
            else
              { // ce n'est pas la 1� fois qu'on fait advise sur cet item
              if ((pMemRealloue (&ptDdeItem->pData, wSizeData)) == NULL)
                {
                wCodeRet = DDE_ERR_NO_MEM;
                MemLibere (&ptDdeItem->pDdeStructPost);
                }
              }
            if (wCodeRet == 0)
              {
              StrCopy (ptDdeItem->pszItem, pszItem);

              memcpy (ptDdeItem->pData, pData, wSizeData);
              ptDdeItem->wSizeData = wSizeData;
              ptDdeItem->FctNotif = FctNotif;
              ptDdeItem->Param = Param;
              ptDdeItem->wStatus = DDE_ST_WAIT_ADVISE;

              //--------------------- cree et set semaphore
              //
              bSemPrends (ptDdeItem->handleSem);
              *phSemDde = ptDdeItem->handleSem;
              //
              if (!::PostMessage (hwndClientThrd, WM_DEMANDE_ADVISE,
                               (WPARAM)(pLien), (LPARAM)(ptDdeItem)))
                {
                ptDdeItem->wStatus = DDE_ST_NOT_DEFINE;
                MemLibere (&ptDdeItem->pDdeStructPost);
                wCodeRet = DDE_ERR_POST_MSG;
                }
              }
            }
          else
            {
            wCodeRet = DDE_ERR_SEG_ALLOC;
            }
          }
        else
          {
          wCodeRet = DDE_ERR_NOT_INDET_STATE;
          }
        }
      else
        {
        wCodeRet = DDE_ERR_NO_ITEM;
        }
      }
    else
      {
      wCodeRet = DDE_ERR_NO_LINK;
      }
    }
  else
    {
    wCodeRet = DDE_ERR_NO_LINK;
    }
*/
  return wCodeRet;
  }

//----------------------------------------------------------
//          lire la valeur d'un item dans un lien
//----------------------------------------------------------
DWORD uDdeClientGetItemValeur (HCONV hConv, UINT wNumItem,
                       void *pData, UINT *wSizeData)
  {
  DWORD             wCodeRet = 0;
/*
  PLIEN_DDE					pLien;
  PITEM_DDE					ptDdeItem;
  PLISTE_LIENS_DDE	paListeLiens;

  if (hEnvConv != NULL)
    {
    paListeLiens = hEnvConv;
    if (paListeLiens->pLienDde != NULL)
      {
      pLien = paListeLiens->pLienDde;
      if ((wNumItem <= pLien->wNbItems) && (pLien->ptrItems != NULL))
        {
        ptDdeItem = (PITEM_DDE) pLien->ptrItems;
        ptDdeItem += wNumItem - 1;
        if (pLien->wStatus == DDE_ST_LINK_OK)
          {
          (*wSizeData) = ptDdeItem->wSizeData;
          if (ptDdeItem->pData != NULL)
            {
            memcpy (pData, ptDdeItem->pData, ptDdeItem->wSizeData);
            }
          else
            {
            wCodeRet = DDE_ERR_NO_DATA;
            }
          }
        else
          {
          wCodeRet = DDE_ERR_NO_LINK;
          }
        }
      else
        {
        wCodeRet = DDE_ERR_NO_ITEM;
        }
      }
    else
      {
      wCodeRet = DDE_ERR_NO_LINK;
      }
    }
  else
    {
    wCodeRet = DDE_ERR_NO_LINK;
    }
*/
  return wCodeRet;
  }

//----------------------------------------------------------
// lire la valeur d'un item dans un lien : WM_DDE_REQUEST
//----------------------------------------------------------
DWORD uDdeClientRequestItem (char *pszItem, UINT wNumItem, t_func_notif FctNotif, LPARAM lParam,
	HCONV hConv, DWORD dwTimeOut) //, PHSEM phSemDde)
  {
  DWORD              wCodeRet = 0;
/*
  PLIEN_DDE       pLien;
  PITEM_DDE				ptDdeItem;
  PLISTE_LIENS_DDE paListeLiens;

  paListeLiens = hEnvConv;
  if (paListeLiens != NULL)
    {
    pLien = paListeLiens->pLienDde;
    if (pLien != NULL)
      {
      if ((wNumItem <= pLien->wNbItems) &&
          (pLien->ptrItems != NULL) &&
          (pLien->wStatus == DDE_ST_LINK_OK))
        {
        ptDdeItem = (PITEM_DDE) pLien->ptrItems;
        ptDdeItem += wNumItem - 1;
        if (ptDdeItem->wStatus == DDE_ST_NOT_DEFINE)
          {
          StrCopy (ptDdeItem->pszItem, pszItem);
          if ((ptDdeItem->pDdeStructPost = MakeDDEObject (pszItem, DDE_FACKREQ,
                                           CF_TEXT, NULL, 0)) == NULL)
            {
            wCodeRet = DDE_ERR_SEG_ALLOC;
            }
          else
            {
            ptDdeItem->FctNotif = FctNotif;
            ptDdeItem->Param = Param;
            ptDdeItem->wStatus = DDE_ST_WAIT_REQUEST;

            //--------------------- cree et set semaphore
            //
            bSemPrends (ptDdeItem->handleSem);
            *phSemDde = ptDdeItem->handleSem;
            //
            if (!::PostMessage (hwndClientThrd, WM_DEMANDE_REQUEST,
                             (WPARAM)(pLien), (LPARAM)(ptDdeItem)))
              {
              ptDdeItem->wStatus = DDE_ST_NOT_DEFINE;
              MemLibere (&ptDdeItem->pDdeStructPost);
              wCodeRet = DDE_ERR_POST_MSG;
              }
            }
          }
        else
          {
          wCodeRet = DDE_ERR_NOT_INDET_STATE;
          }
        }
      else
        {
        wCodeRet = DDE_ERR_NO_LINK;
        }
      }
    else
      {
      wCodeRet = DDE_ERR_NO_LINK;
      }
    }
  else
    {
    wCodeRet = DDE_ERR_NO_LINK;
    }
*/
  return wCodeRet;
  }

//----------------------------------------------------------------
// terminer toutes les transactions advise d'un lien en meme temps
//----------------------------------------------------------------
DWORD DdeUnadviseLink (HCONV hConv, DWORD dwTimeOut)
  {
  DWORD              wCodeRet = 0;
/*
  PLIEN_DDE					pLien;
  PLISTE_LIENS_DDE paListeLiens;

  paListeLiens = hEnvConv;
  if (paListeLiens != NULL)
    {
    pLien = paListeLiens->pLienDde;
    if (pLien != NULL)
      {
      if ((pLien->wNbItems > 0) &&
          (pLien->ptrItems != NULL) &&
          (pLien->wStatus == DDE_ST_LINK_OK))
        {
        if ((pLien->pDdeStructPost = MakeDDEObject ("", DDE_FACKREQ,
                                         CF_TEXT, NULL, 0)) == NULL)
          {
          wCodeRet = DDE_ERR_SEG_ALLOC;
          }
        else
          {
          pLien->wStatus = DDE_ST_WAIT_UNADVISE_LIEN;

          //--------------------- cree et set semaphore
          //
          bSemPrends (pLien->handleSem);
          *phSemDde = pLien->handleSem;
          //
          if (!::PostMessage (hwndClientThrd, WM_DEMANDE_UNADVISE,
                           (WPARAM)(pLien), 0))
            {
            pLien->wStatus = DDE_ST_LINK_OK;
            MemLibere (&pLien->pDdeStructPost);
            wCodeRet = DDE_ERR_POST_MSG;
            }
          }
        }
      else
        {
        wCodeRet = DDE_ERR_NO_LINK;
        }
      }
    else
      {
      wCodeRet = DDE_ERR_NO_LINK;
      }
    }
  else
    {
    wCodeRet = DDE_ERR_NO_LINK;
    }
*/
  return wCodeRet;
  }




// ------------------------ fin ExeDde.c -----------------------------

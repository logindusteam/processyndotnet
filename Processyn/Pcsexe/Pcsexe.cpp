//----------------------------------------------------------
//|   Ce fichier est la propriete de                        
//|              Societe LOGIQUE INDUSTRIE                  
//|       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3     
//|   Il demeure sa propriete exclusive et est confidentiel.
//|   Aucune diffusion n'est possible sans accord ecrit.    
//|---------------------------------------------------------
//|
//|   Titre   : PROGRAMME EXECUTION PROCESSYN: VERSION  Win32
//|   Auteur  : JB
//|   Date    : 12/02/93
//|   Version :	5.00
//----------------------------------------------------------

#include "stdafx.h"
#include "std.h"         // Types Standards
#include "Appli.h"
#include "DocMan.h"
#include "UStr.h"      // Chaines de Caracteres: Strxxx ()

#include "InpuEx.h"      // Lire_Commandes  ()
#include "ActionEx.h"    // Executer_Action ()

#include "UChrono.h"       // TpsLanceHorloge ()
#include "threads.h"     // thrd_xxx ()
#include "ProgMan.h"
#include "VersMan.h"
#include "fileman.h"
#include "USem.h"
#include "trap.h"        // lance_trap (), arrete_trap ()
#include "UCom.h"     // nComFerme (), raccroche_tache ()
#include "mem.h"
#include "WExe.h"				// InitPm ()
#include "lance.h"      // adresse procedure finalisation
#include "dongle.h"
#include "ULpt.h"
#include "LireLng.h"
#include "driv_im.h"
#include "Driv_sys.h"   
#include "UExcept.h"
#include "Audit.h"
#include "DebugDumpVie.h"
#include "Verif.h"
VerifInit;

// Instancie la classe principale d'Audit
CAudit Audit("c:\\LogPcsexe.csv", CAudit::AUDIT_AUCUN);
static HTHREAD	hThrdFenEx = NULL;

// --------------------------------------------------------------------------
// protoypes
static void FermerRessources (void);

// --------------------------------------------------------------------------
static void FermeTout (void)
  {
  FermeDescripteursEx ();
  FermerRessources ();
  }

// --------------------------------------------------------------------------
static BOOL bOuvrirRessources (void)
  {
  BOOL	bOk = FALSE;
  DWORD	wError;
  
	creer_bd_vierge();

  lance_trap ();
  InitChronosEtMinuteries ();
  if (ouvrir_dongle (&FermeTout) == 0)
    {
    // $ac init_test_dongle ();

		// Initialise le buffer de modification des variables syst�mes
		VarSysExeInit();

    // Cr�ation de la t�che Fenetre Pcsexe (pour Ecran Texte (ecrire_ecran()))
    if (bThreadCree (&hThrdFenEx, uThreadFenEx, (DWORD) 34048)) // $$ Valeur ?
      {
      // Phase de cr�ation des ressources de la fen�tre
      dwThreadExecuteMessage (hThrdFenEx, THRD_FCT_CREATE, NULL, NULL, &wError);
      if (wError == 0)
        {
        // Boucle de message
        ThreadEnvoiMessage (hThrdFenEx, THRD_FCT_EXEC, NULL, NULL, NULL);
        bOk = TRUE;
        }
      }
    }
  return bOk;
  }

// --------------------------------------------------------------------------
static void FermerRessources (void)
  {
  DWORD wCanal;

  // Fermeture des COMs en cas d'oubli ou d'ABORT
  for (wCanal = 1; wCanal <= NB_MAX_VOIE; wCanal++)
    {
    nComFerme (wCanal);
    }

	// Thread fen�tre principale existe ?
  if (hThrdFenEx)
    {
    // oui => fermeture de la fen�tre principale de l'application
    bPostDestroyWExe ();

		// attente de la fermeture effective
    bThreadAttenteFinTraiteMessage (hThrdFenEx, INFINITE);

		// fermeture du thread Fen�tre principale
    bThreadFerme (&hThrdFenEx, INFINITE);
    }

  fermer_dongle ();
  arrete_trap ();

	fermer_bd();
  }

static int BodyMain (
	HINSTANCE hInstance,			// handle de cette instance de l'application
	HINSTANCE hPrevInstance,	// 0 sur Win32 actuel
	LPSTR lpCmdLine,					// param�tres de la ligne de commande
	int nCmdShow 							// show state de la fen�tre principale
	)
	{
		t_retour_action  retour_action={0};
	t_param_lecture  param_lecture = {0,0,""};
  int              error_level = 0;

	// Prise en charge des exceptions
	__try 
		{
		// Initialisation de l'application
		CHAR szVersionComplete [MAX_PATH] = "";

		Appli.Init (hInstance, "Processyn (Ex�cution)", GetVersionComplete(szVersionComplete));
		bOuvrirLng ();

		InitPathNameDoc ();

		// Initialisations
		CreerAction ();
		if (bOuvrirRessources ())
			{
			param_lecture.mode_lecture = MODE_INITIALISATION;

			StrCopy (param_lecture.chaine, lpCmdLine);
			lire_commandes (&param_lecture);
			if (executer_action (&retour_action) != action_quitte)
				{
				// Lectures Commandes Demarrage
				param_lecture.mode_lecture = MODE_DEMARAGE;
				lire_commandes (&param_lecture);

				// Boucle: Executer-Action / Lire-Commandes
				while (executer_action (&retour_action) != action_quitte)
					{
					param_lecture.mode_lecture = MODE_ENCHAINEMENT;
					lire_commandes (&param_lecture);
					}
				error_level = retour_action.entier;

				// Fermeture de l'application
				param_lecture.mode_lecture = MODE_FINALISATION;
				lire_commandes (&param_lecture);
				executer_action (&retour_action);
				}
			else
				{
				error_level = retour_action.entier;
				}

			// fermeture des ressources
			FermerRessources ();
			}
		} // __try 
	__except (nSignaleExceptionUser(GetExceptionInformation()))
		{
		ExitProcess((UINT)-1);// handler des exceptions choisies par le filtre
		}

	// Ferme tous les threads en cours $$ devraient d�ja �tre ferm�s
	//ExitProcess(error_level);
	return error_level;
	}

// -------------------------------------------------------------------------
//                   Programme principal de PcsExe, ex�cution de Processyn
// -------------------------------------------------------------------------

int WINAPI WinMain
	(
	HINSTANCE hInstance,			// handle de cette instance de l'application
	HINSTANCE hPrevInstance,	// 0 sur Win32 actuel
	LPSTR lpCmdLine,					// param�tres de la ligne de commande
	int nCmdShow 							// show state de la fen�tre principale
	)
  {
	DumpVie(WinMain);
	return BodyMain(hInstance,hPrevInstance,lpCmdLine,nCmdShow);
	}

// ------------------------------- fin pcsexe ------------------------------------

//------------------------------------------------------------------------
// ExecuteF.h 
// Fonctions ExecuteF du code de l'application utilisateur (PCSEXE)
// Win32 JS 15/5/98
//------------------------------------------------------------------------

//----------------------------------------------------------------------------
// 1     numero fonction invalide (< 0 ou > MAXINT4)
// 2     numero de drive invalide sur fonction 1
// 4     la valeur ecrite dans STATUS 7552 )it etre comprise entre 0 et 65535
// 8     la valeur ecrite dans la sortie digitale du 7552 )it etre 1 ou 2
// 16
// 32
// 64
// 128
// 256
// 512
// 1024
// 2048
// 4096
// 8192
// 16384
// 32768
void maj_status_fct (DWORD erreur);

//----------------------------------------------------------------------------
// Ouverture fermeture de l'environnement d'exploitation des execute_f
//----------------------------------------------------------------------------

// Initialisation des contextes d'ex�cution des execute_f
void InitExecuteF (void);

// Fermeture des contextes d'ex�cution des execute_f
void TermineExecuteF (void);

//----------------------------------------------------------------------------
// Execute_f
//----------------------------------------------------------------------------

// Fonction place libre sur un disque
void ExecuteF1 (void);

// Fonction Initialisation imprimante
void ExecuteF9 (void);

// Fonction Place libre dans tampon imprimante
void ExecuteF10 (void);

// Fonction test d'�criture sur unite en gerant abort retry ignore
//void ExecuteF11 (void);

// fonction PCS <----> DIf
void ExecuteF12 (void);

// Fonction gestion time-out imprimante
void ExecuteF13 (void);

// Fonction mise � jour horloge : date
void ExecuteF16 (void);

// Fonction mise � jour horloge : heure
void ExecuteF17 (void);

// Fonction longueur d'une chaine
void ExecuteF18 (void);

// Fonction Recherche d'un caract�re avec codage ascii
void ExecuteF19 (void);

// Fonction Recherche d'une chaine
void ExecuteF20 (void);

// Fonction Insertion d'une chaine
void ExecuteF21 (void);

// Fonction Suppression d'une partie de cha�ne
void ExecuteF22 (void);

// Fonction Substitution dans une cha�ne
void ExecuteF23 (void);

// Fonction Extrait une partie de cha�ne � droite
void ExecuteF24 (void);

// Fonction Extrait une partie de cha�ne � gauche
void ExecuteF25 (void);

// Fonction Extrait une partie de cha�ne
void ExecuteF26 (void);

// Fonction envoi message ASCII
void ExecuteF31 (void);

// Fonction reception message ASCII
void ExecuteF32 (void);

// Affichage Qhelp
void ExecuteF34 (void);

// Affichage dans l'aide d'un fichier
void ExecuteF35 (void);

// init index Qhelp
void ExecuteF36 (void);

// vide buffer imprimante
void ExecuteF37 (void);

// Recupere un nom de fichier via une dlg
void ExecuteF38 (void);

// Appel dll user
void ExecuteF39 (void);

// Appel d'un programme externe
void ExecuteF40 (void);

// Test etat execution d'un programme externe
void ExecuteF41 (void);

// Passage en avant plan d'un programme externe
void ExecuteF42 (void);

// Arret d'un programme externe
void ExecuteF43 (void);

// Lib�ration d'un programme externe
void ExecuteF44 (void);

// Joue un son  (si un autre process n'est pas en train de jouer)
void ExecuteF45 (void);

// R�cup�re le texte d'une erreur OS
// num�ro d'erreur OS (dans REG_NUM1)
// r�sultat mis dans REG_LOG1 : si TRUE :  => message mis dans REG_MES1 (termin� par ... si coup�)
void ExecuteF46 (void);

// Ex�cution synchrone DDE : ouverture / fermeture de conversation avec un serveur
// en entr�e
// REG_MES1 contient le nom du serveur
// REG_MES2 contient le nom du topic
// REG_NUM1 time out des commandes en millisecondes
// REG_NUM2 mode de fonctionnement : 
//	0 ouverture / reconnexion / fermeture auto au fil de l'eau
//	1 ouverture connexion imm�diate
//	2 fermeture connexion imm�diate
// en sortie
// REG_LOG1 : si TRUE odre pris en compte FALSE sinon
// REG_NUM1 : num�ro d'erreur OS
// Remarque : il n'est possible d'avoir qu'une seule conversation 
// avec un serveur, � un instant donn�. En mode auto, l'ouverture d'une nouvelle conversation
// ferme le cas �ch�ant celle en cours
void ExecuteF50 (void);

// Ex�cution synchrone imm�diate de la commande DDE Peek
// sur la conversation courante.
// en entr�e
// REG_MES1 : contient le nom de l'item � lire
// en sortie
// REG_LOG1 : si TRUE lecture de l'item effectu�e FALSE sinon
// REG_NUM1 : num�ro d'erreur OS
// REG_MES2 : contient le texte lu
// Remarque : l'item est demand� sur le serveur et le topic de la conversation ouverte en cours
void ExecuteF51 (void);

// Ex�cution synchrone imm�diate de la commande DDE Poke
// sur la conversation courante.
// en entr�e :
// REG_MES1 : contient le nom de l'item � lire
// REG_MES2 : contient le texte � envoyer
// en sortie :
// REG_LOG1 : si TRUE texte envoy� pris en compte par le serveur, FALSE sinon
// REG_NUM1 : num�ro d'erreur OS
// Remarque : l'item est envoy� au serveur et topic de la conversation ouverte en cours
void ExecuteF52 (void);

// Ex�cution synchrone imm�diate de la commande Execute DDE
// sur la conversation courante.
// commande prise dans REG_MES1 accept� par le serveur
// en entr�e :
// REG_MES1 : contient la commande � faire ex�cuter
// en sortie :
// REG_LOG1 : si TRUE commande envoy�e prise en compte par le serveur, FALSE sinon
// REG_NUM1 : num�ro d'erreur OS
// Remarque : la commande est envoy� au serveur et topic de la conversation ouverte en cours
void ExecuteF53 (void);

// Inhibition/restauration des actions effectu�es
// par appui des touches CRTL+ESC, ALT+ESC, ALT+TAB, Touche"windows", ALT+F4
// en entr�e :
// REG_LOG1 : si true, inhibition des touches CRTL+ESC, ALT+ESC, ALT+TAB, Touche"windows"
// REG_LOG2 : si true, inhibition ALT+F4
// REG_LOG1 et REG_LOG2 : False simultan�ment, restauration action sur touche
// en sortie :
// REG_NUM1 : 0 si Ok 0xFF si Dll non trouv�
void ExecuteF54 (void);

//-----------------------------------------------------------------------------------
// Tri de tous les �l�ments d'une variable tableau (quelque soit son type)
// en entr�e :
// REG_MES1 = Nom du tableau � trier (TOR, num�rique ou message)
// REG_LOG1 = TRUE => tri par ordre croissant, FALSE = d�croissant
// REG_LOG2 = Respect de la casse pour les variables message (ignor� pour les autres types):
//						TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM2 = Nombre d'�l�ments du tableau modifi�s par le tri
void ExecuteF60 (void);

//-----------------------------------------------------------------------------------
// Tri d'une partie des �l�ments d'une variable tableau
// en entr�e :
// REG_MES1 = Nom du tableau � trier (TOR, num�rique ou message)
// REG_NUM1 = Num�ro du premier �l�ment du tableau � trier (1 � nb �l�ments max du tableau)
// REG_NUM2 = Nombre d'�l�ments dans le tableau � trier (de REG_NUM1 REG_NUM1 - 1 + nb �l�ments max du tableau)
// REG_LOG1 = TRUE => tri par ordre croissant, FALSE = d�croissant
// REG_LOG2 = Respect de la casse pour les variables message (ignor� pour les autres types):
//						TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM2 = Nombre d'�l�ments du tableau modifi�s par le tri
void ExecuteF61 (void);

//-----------------------------------------------------------------------------------
// Tri de tous les �l�ments d'une variable tableau (quelque soit son type)
// Avec mise � jour d'un Index num�rique
// en entr�e :
// REG_MES1 = Nom du tableau � trier (TOR, num�rique ou message)
// REG_MES2 = Nom du tableau num�rique devant contenir les indexs du tableau tri�
// ex : Si TableauTri[2] se retrouve en TableauTri[5] apr�s tri, alors TableauIndex[5] = 2
// remarque : la taille de TableauIndex doit �tre sup�rieure ou �gale � celle de TableauTri
// REG_LOG1 = TRUE => tri par ordre croissant, FALSE = d�croissant
// REG_LOG2 = Respect de la casse pour les variables message (ignor� pour les autres types):
//						TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM2 = Nombre d'�l�ments du tableau modifi�s par le tri
void ExecuteF62 (void);

//-----------------------------------------------------------------------------------
// Tri d'une partie des �l�ments d'une variable tableau
// en entr�e :
// REG_MES1 = Nom du tableau � trier (TOR, num�rique ou message)
// REG_MES2 = Nom du tableau num�rique devant contenir les indexs du tableau tri�
// ex : Si TableauTri[2] se retrouve en TableauTri[5] apr�s tri, alors TableauIndex[5] = 2
// remarques : 
// la taille de TableauIndex doit �tre sup�rieure ou �gale � celle de TableauTri
// seuls les index correspondants aux �l�ments � trier sont mis � jour dans le tableau d'index,
// les autres n'�tant pas touch�s
// REG_NUM1 = Num�ro du premier �l�ment du tableau � trier (1 � nb �l�ments max du tableau)
// REG_NUM2 = Nombre d'�l�ments dans le tableau � trier (de REG_NUM1 REG_NUM1 - 1 + nb �l�ments max du tableau)
// REG_LOG1 = TRUE => tri par ordre croissant, FALSE = d�croissant
// REG_LOG2 = Respect de la casse pour les variables message (ignor� pour les autres types):
//						TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM2 = Nombre d'�l�ments du tableau modifi�s par le tri
void ExecuteF63 (void);

// E-MAILS

//-----------------------------------------------------------------------------------
// Init Envoi de mail
// en entr�e :
// REG_MES1 = Nom du serveur SMTP
// REG_MES2 = Nom de l'�metteur
// REG_NUM1 = 
// REG_NUM2 = 
// REG_LOG1 = Envoi actif : TRUE => oui , FALSE => non
// REG_LOG2 = Secure TRUE => oui , FALSE => non
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_MES3 = R�sultat de l'appel ("Ok" ou descriptif d'erreur)
void ExecuteF64 (void);
//-----------------------------------------------------------------------------------
// Envoi de mail
// en entr�e :
// REG_MES1 = Nom du tableau texte Destinataire(s) (A), Destinataire(s) (Cc), Destinataire(s) (Ci)
// REG_MES2 = Objet
// REG_MES3 = Nom du tableau texte Corps du message (1 ligne par �l�ment du tableau)
// REG_NUM2 = 
// REG_LOG1 = 
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur pour la mise en queue de l'envoi de mail (nom ou nb �l�ments invalide, etc...), FALSE sinon
// REG_NUM1 = 
// REG_MES3 = R�sultat de l'appel ("Ok" ou descriptif d'erreur)
void ExecuteF65 (void);
//-----------------------------------------------------------------------------------
// Surveillance Envoi de mail
// en entr�e :
// en sortie :
// REG_LOG2 = TRUE si pas d'erreur d'envoi, FALSE sinon
// REG_NUM1 = Nb de mails en attente d'envoi
// REG_NUM2 = Nb de mails envoy�s Ok
// REG_NUM3 = Nb de mails Non envoy�s/erreur
// REG_MES3 = Nb de mails envoy�s Nok Derni�re erreur envoi mail
void ExecuteF66 (void);

// MU : ensemble de fonctions permettant de traiter des grands nombres dans Processyn
// Utilise deux r�els pour repr�senter un grand nombre avec moins de perte de pr�cision comme suit
// U = r�el1 consid�r� comme unit�.
// M = r�el2 consid�r� en million = 10exp6
// Cette convention permet de traiter des nombres entiers de plus de 6 chiffres significatifs (mais moins de 12) sans perte de pr�cision avec les r�els 32 bits de Processyn
// les deux r�els 32 bits du code processyn (M,U) sont trait�s en interne comme un r�el double pr�cision D comme suit;
// Les r�gles de conversion sont D=(double)M*1000000+(double)U ; U=(REAL32)(D modulo 1000000) puis M=(REAL32)((D-U)/1000000);
// NOTA : en entr�e M comme U comme peuvent d�passer 1000000, une conversion en D �tant syst�matiquement effectu�e

// ExecuteF70 MU1=MU1+MU2
// ExecuteF71 MU1=MU1-MU2
// ExecuteF72 MU1=MU1xMU2
// ExecuteF73 MU1=MU1/MU2
// ExecuteF74 MU1=MU1 Modulo MU2 (division enti�re)
// en entr�e :
// REG_NUM1 = U1
// REG_NUM2 = M1
// REG_NUM3 = U2
// REG_NUM4 = M2
// en sortie :
// REG_NUM1 = U r�sultat
// REG_NUM2 = M r�sultat
// REG_LOG1 = diviseur <>0 
void ExecuteF70 (void);
void ExecuteF71 (void);
void ExecuteF72 (void);
void ExecuteF73 (void);
void ExecuteF74 (void);

// ExecuteF75 Conversion de MU1 en un double mot non sign� 32 bits, r�parti sur WL, WH avec WL = partie basse 16 bit du double mot et WH partie haute 16 bit du double mot
// en entr�e :
// REG_NUM1 = U1
// REG_NUM2 = M1
// en sortie :
// REG_NUM1 = WL
// REG_NUM2 = WH
void ExecuteF75 (void);

// ExecuteF76 Conversion d'un Double mot DW 32 bit en MU, avec MU = 65536x(WH Mot 16 bit) + WL (Mot 16 bit)
// en entr�e :
// REG_NUM1 = WL
// REG_NUM2 = WH
// en sortie :
// REG_NUM1 = U r�sultat
// REG_NUM2 = M r�sultat
void ExecuteF76 (void);

// ExecuteF77 Conversion de MU1 en chaine de caract�re
// en entr�e :
// REG_NUM1 = U1
// REG_NUM2 = M1
// REG_NUM3 = Nb de caract�res min
// REG_NUM4 = Nb de d�cimales
// en sortie :
// REG_MES1 = conversion chaine de caract�res
void ExecuteF77 (void);

// ExecuteF78 R�sultat MU1=Conversion Chaine de caract�re num�rique
// en entr�e :
// REG_MES1 = Chaine num�rique � convertir
// en sortie :
// REG_LOG1 = format valide (conversion effectu�e)
// REG_NUM1 = U1 r�sultat
// REG_NUM2 = M1 r�sultat
void ExecuteF78 (void);



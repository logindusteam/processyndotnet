// --------------------------------------------------------------------------
// SCRUT_SYsteme  scrutation (Entrees/Sorties) pour les drivers qui n'ont
//                pas de descripteur, et qui sont associ�s � des variables
//                systemes.
// --------------------------------------------------------------------------

#include "stdafx.h"
#include "mem.h"       // Memory Manager
#include "lng_res.h"
#include "tipe.h"      // Constantes + Types Processyn
#include "scrut_tm.h"  // emet_tm () + recoit_tm (): CLAVIER + ECRAN TEXTE
#include "scrut_tp.h"  // scrute_horo ()           : DATE + HEURE
#include "scrut_sn.h"  // scrute_son  ()           : SON
#include "pcs_sys.h"
#include "scrut_im.h"  // scruter_imprimantes ()   : Imprimante
#include "PcsVarEx.h"
#include "scrut_sy.h"

// --------------------------------------------------------------------------
void recoit_sy (BOOL bPremier)
  {
  // CLAVIER + MENU
  recoit_tm ();

  // DATE + HEURE
  scrute_horo ();
  }

// --------------------------------------------------------------------------
void emet_sy (BOOL bPremier)
  {
  // PREMIER_PASSAGE := FALSE
	CPcsVarEx::SetValVarSysLogEx (premier_passage, FALSE);

  // ECRAN TEXTE (effacement si necessaire)
  emet_tm ();

  // SON
  scrute_son ();

  // Imprimantes
  scruter_imprimantes ();
  }

// --------------------------------------------------------------------------
void  init_sy (void)
  {
  // Initialisation variable PREMIER_PASSAGE
  CPcsVarEx::SetValVarSysLogEx (premier_passage, TRUE);

  // Initialisation CLAVIER + ECRAN TEXTE
  init_tm ();

  // Initialisation DATE, HEURE
  scrute_horo ();

  // Initialisation Imprimantes
  ouvrir_imprimantes ();
  }

// --------------------------------------------------------------------------
void  fini_sy (void)
  {
  // Fermeture CLAVIER + ECRAN TEXTE
  finit_tm ();

  // Fermeture Imprimante
  fermer_imprimantes ();
  }

// -------------------------- fin scrut_sy.c --------------------------------

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : scrut_dq.h                                               |
 |   Auteur  : AC					        	|
 |   Date    : 7/7/93 					        	|
 |   Version : 4.00							|
 |   Mise a jours:                                                      |
 |                                                                      |
 |   +--------+--------+---+--------------------------------------+     |
 |   | date   | qui    |no | raisons                              |     |
 |   +--------+--------+---+--------------------------------------+     |
 |   |        |        |   |                                      |     |
 |   +--------+--------+---+--------------------------------------+     |
 |                                                                      |
 +----------------------------------------------------------------------*/

void  initialise_dq (void);
void  ferme_disc    (void);

void scrute_ecrit_dq (char *nom_fichier);
void scrute_lit_dq (char *nom_fichier);
void scrute_ouvre_dq (char *nom_fichier);
void scrute_ferme_dq (char *nom_fichier);
void efface_dq (char *nom_fichier);
void copie_dq (char *fichier_source, char *fichier_destination);
void renomme_dq (char *fichier_source, char *fichier_destination);

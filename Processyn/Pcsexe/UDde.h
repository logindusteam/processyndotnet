//----------------------------------------------------------
// UDde.h
// gestion DDE (compl�mentaire � DDEML)
// Win32 22/7/97 JS
//----------------------------------------------------------

typedef struct tagENV_CONV
	{
	HSZ	hszAppli;
	HSZ	hszTopic;
	HSZ	hszItem;
	HCONV		hConv;
	LPARAM	lParam1;
	LPARAM	lParam2;
	HDDEDATA (* pfnDDE) (struct tagENV_CONV * pEnvConv, UINT wType, UINT wFmt, HCONV hConv,
		HSZ hsz1, HSZ hsz2, HDDEDATA hData, DWORD dwData1, DWORD dwData2);
	BOOL bCreateurServeur;
	} ENV_CONV, *PENV_CONV;


// Type de fonction client et serveur appel�e en CALLBACK sur �v�nement DDE
typedef	HDDEDATA (*PFNDDE)(ENV_CONV * pEnvConv, UINT wType, UINT wFmt, HCONV hConv,
		HSZ hsz1, HSZ hsz2, HDDEDATA hData, DWORD dwData1, DWORD dwData2);

// Export de variables
extern DWORD hInstDDE; // Identifiant de l'instance DDE

// M�morisation de l'environnement des conversations
#define NB_MAX_CONVERSATIONS	200
extern int nNbEnvConversations;

extern ENV_CONV	EnvConvs[NB_MAX_CONVERSATIONS];

// Valeur de la derni�re erreur DDE
extern UINT uDerniereErreurDDE (void);

// R�cup�ration de la chaine descriptive d'une erreur DDE
extern PCSTR lpszErreurDDE (UINT wErrDDE);

// Message Box / Derni�re erreur DDE circonstanci�e par son contexte
extern void SignaleDerniereErreurDDE (HWND hWndParent, PSTR	pszContexte);


//-------------------------------------------------------------------------	
// Initialisation du syst�me DDEML
extern BOOL bInitDDE (HINSTANCE hInst, BOOL	bClient, BOOL bServeur);

// Lib�ration du syst�me DDEML
extern void FermeDDE (void);

// Renvoie TRUE si DDEML est actif
extern BOOL bDDEOuvert (void);

//-------------------------------------------------------------------------
// r�cup�ration d'un environnement d�ja li� � une conversation client / serveur
extern PENV_CONV pEnvConvTrouve (HCONV hConv);

// lib�ration d'un environnement d�ja li� � une conversation client / serveur
extern void FermeEnvConversation (PENV_CONV	* ppEnvConv);

// Demande de d�connection de toutes les conversations ouvertes par un serveur
extern void FermeConversationsDDEServeur (HSZ hszAppli);


//%%%%%%%%%%%%%%%%%%%%%% Sp�cifique SERVEURS %%%%%%%%%%%%%%%%%%%%%%%%%%

typedef struct
	{
	HSZ			hszAppli;
	PFNDDE	pfnDDE;
	LPARAM	lParam;
	} SERVEUR_DDE;

extern SERVEUR_DDE
	ServeurDde; // Environnement du service courant

// D�claration d'un nouveau service DDE
extern BOOL bAjouteServeurDDE (PCSTR pszService,	PFNDDE pfnDDE, LPARAM lParam);

// Fermeture du service DDE en cours
extern BOOL bFermeServeurDDE (void);

// Un Item du serveur, sujet d'un ou plusieurs Advise en cours, vient de changer de valeur.
// L'appel de cette fonction met � jour tous les clients concern�s
// (DDEML enverra au serveur un XTYP_ADVREQ pour chaque conversation concern�e).
extern BOOL bChangementItemServeurDansAdvise (PSTR pszTopic, PSTR pszItem);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Sp�cifique CLIENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Lancement d'une conversation (connection � un serveur)
extern PENV_CONV pInitConversationDDE (PSTR pszService, PSTR pszTopic, PFNDDE pfnClient, LPARAM lParam1, LPARAM lParam2);

// Fin d'une conversation (d�connection du serveur)
extern BOOL bFermeConversationDDE (PENV_CONV * ppEnvConv);

// Transactions synchrones possibles au cours d'une conversation en format quelconque
// demande par un client de donn�es 
extern HDDEDATA hdataRequeteDonneeDDE (PENV_CONV pEnvConv, PSTR pszItem, UINT wCBFormat,
	DWORD dwMsTimeOut);

// envoi par un client de donn�es
extern BOOL bPokeDonneeDDE (PENV_CONV pEnvConv, PSTR pszItem, UINT wCBFormat,
	PSTR pszBuf, DWORD dwTailleBuf, DWORD dwMsTimeOut);

// demande par un client de surveillance automatique de donn�es (renvoie la donn�e ou NULL)
extern HDDEDATA hdataRequeteAutoDonneeDDE (PENV_CONV pEnvConv, PSTR pszItem, UINT wCBFormat,
	DWORD dwMsTimeOut);


// Transactions synchrones possibles au cours d'une conversation en format texte
// demande par un client de donn�es texte 
extern BOOL bRequeteTexteDDE (PENV_CONV pEnvConv, PSTR pszItem, PSTR pszBuf, DWORD dwTailleBuf, DWORD dwMsTimeOut);

// envoi par un client de donn�es texte 
extern BOOL bPokeTexteDDE (PENV_CONV pEnvConv, PSTR pszItem, PSTR pszTexteEnvoi, DWORD dwMsTimeOut);

// envoi par un client de donn�es texte 
extern BOOL bExecuteCommandeTexteDDE (PENV_CONV pEnvConv, PSTR pszTexteCommande, DWORD dwMsTimeOut);

// demande par un client de surveillance automatique de donn�es texte (r�cup�re les donn�es initiales)
extern BOOL bRequeteAutoTexteDDE (PENV_CONV pEnvConv, PSTR pszItem, PSTR pszBuf, DWORD dwTailleBuf, DWORD dwMsTimeOut);


//---------------------------------- fin UDde.h  ---------------------------------

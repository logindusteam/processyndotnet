/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il demeure sa propriete exclusive et est confidentiel.             |
 |   Aucune diffusion n'est possible sans accord ecrit.                 |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : WScrExe  Fenetre Windows, Ecran Texte pour PCSEXE    |
 |   Auteur  : JB                                                       |
 |   Date    : 12/02/93                                                 |
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include <stdlib.h>

#include "std.h"
#include "USem.h"
#include "Appli.h"
#include "MemMan.h"
#include "UStr.h"
#include "UEnv.h"
#include "Couleurs.h"
#include "WAide.h"
#include "Verif.h"

#include "WScrExe.h"

VerifInit;

static char szClasseScrExe [] = "ClasseScrExe";


#define WM_RAFRAICHIS   (WM_USER+2)
#define WM_QHELP_OPEN   (WM_USER+3)
#define WM_QHELP_CLOSE  (WM_USER+4)
#define WM_QHELP_LOAD   (WM_USER+5)
#define WM_QHELP_INIT   (WM_USER+6)
#define WM_QHELP_FLUSH  (WM_USER+7)

// Police utilis�e pour la fen�tre ScrExe
static  CHAR szFont [] = "Courier";
static  int  sSizeFont  = 10; //$$ 8

//fichier d'aide par defaut
#define FQHELP  "F_HELP1"   

// Caractere d'effacement
#define CLEAR_CHAR ' '

// Couleurs
#define CLR_FORE_CHAR   CLR_BLACK
#define CLR_BACK_CHAR   CLR_WHITE

// ------------------------------------
#define NB_LGN 25
#define NB_COL 80
typedef struct
  {
  HWND		hwnd;
	HFONT		hfont;
  int			cxChar;                     // Taille Caractere
  int		  cyChar;
  HSEM		hsemAccesEtat;                   // Semaphore d'acces au buffer ecran
  HSEM		hsemRefresh;                  // Semaphore Synchro rafraichissement �cran
  int			nLigneEcriture;                        // Position courante
  int			nColonneEcriture;
  CHAR		cBuffer [NB_LGN][NB_COL];    // Buffer ecran
  int		  nPremierLigneModifiee;                     // Limites des lignes modifiees
  int		  nDerniereLigneModifiee;

  BOOL		abTouchesAppuyees [NB_TOUCHES];        // Touches du clavier
  char		szNomFicQhelp [MAX_PATH];               // Qhelp : Taille mes approx : sale
  char		szTitreQhelp [MAX_PATH];                // Qhelp : Taille mes approx : sale
  } ETAT_ECRAN, *PETAT_ECRAN;

// Adresse d'un objet ETAT_ECRAN global
static PETAT_ECRAN pEtatEcran = NULL;


// --------------------------------------------------------------------------
static void InitFont (HDC hdc, char *pszFaceName, int sSize, LOGFONT *pLogFont)
  {
	// remplissage des caract�ristiques de la police
	pLogFont->lfHeight = -MulDiv (sSize, GetDeviceCaps(hdc, LOGPIXELSY), 72);
	pLogFont->lfWidth = 0; 
	pLogFont->lfEscapement = 0; 
	pLogFont->lfOrientation = 0;
	pLogFont->lfWeight = FW_NORMAL;
	pLogFont->lfItalic = FALSE;
	pLogFont->lfUnderline = FALSE;
	pLogFont->lfStrikeOut = FALSE;
	pLogFont->lfCharSet = ANSI_CHARSET;
	pLogFont->lfOutPrecision = OUT_DEVICE_PRECIS;
	pLogFont->lfClipPrecision = CLIP_DEFAULT_PRECIS;
	pLogFont->lfQuality = DRAFT_QUALITY; //$$ ?
	pLogFont->lfPitchAndFamily = FIXED_PITCH | FF_MODERN;
	StrCopy (pLogFont->lfFaceName, pszFaceName);
  }


// --------------------------------------------------------------------------
static void ScrPaint (HDC hdc)
  {
  RECT	rcl;
  INT		iLgn;
  HFONT	hfontAncien = (HFONT)SelectObject (hdc, pEtatEcran->hfont);

  ::GetClientRect (pEtatEcran->hwnd, &rcl);
  rcl.bottom = rcl.top + pEtatEcran->cyChar;
  for (iLgn = 0; iLgn < NB_LGN; iLgn++)
    {
    SetTextColor(hdc, CLR_FORE_CHAR); //$$ restaurer
		SetBkColor (hdc, CLR_BACK_CHAR); //$$ restaurer
    //DrawText (hdc, &(pEtatEcran->cBuffer[iLgn][0]), NB_COL, &rcl, DT_LEFT | DT_VCENTER | DT_NOPREFIX); //$$DT_ERASERECT);
		TextOut (hdc, rcl.left, rcl.top, &(pEtatEcran->cBuffer[iLgn][0]), NB_COL);
    rcl.top    += pEtatEcran->cyChar;
    rcl.bottom += pEtatEcran->cyChar;
    }
 	SelectObject (hdc, hfontAncien);
  return;
  }

// --------------------------------------------------------------------------
// Fonctions Globales et Export�es pouvant �tre appel�es de taches non PM
// Seule la fonction ::PostMessage est autoris�e dans ces proc�dures
// --------------------------------------------------------------------------
// demande d'acc�s au buffer Scr
void ScrAcces (void)
  {
	dwSemAttenteLibre (pEtatEcran->hsemAccesEtat, INFINITE);
  }

// --------------------------------------------------------------------------
// notification de fin d'acc�s au buffer Scr
void ScrFinAcces (void)
  {
  bSemLibere (pEtatEcran->hsemAccesEtat);
  }

// --------------------------------------------------------------------------
// Efface la fen�tre
void ScrClear (void)
  {
  INT           iLgn;
  INT           iCol;

  for (iLgn = 0; iLgn < NB_LGN; iLgn++)
    {
    for (iCol = 0; iCol < NB_COL; iCol++)
      {
      pEtatEcran->cBuffer [iLgn] [iCol] = CLEAR_CHAR;
      }
    }
  pEtatEcran->nPremierLigneModifiee = 0;
  pEtatEcran->nDerniereLigneModifiee = NB_LGN;
  }

// --------------------------------------------------------------------------
void ScrGotoLgnCol (INT iLgn, INT iCol)
  {
  pEtatEcran->nLigneEcriture = iLgn % NB_LGN;
  pEtatEcran->nColonneEcriture = iCol % NB_COL;
  }

// --------------------------------------------------------------------------
void ScrGetLgnCol (INT * pnLigne, INT *pnColonne)
  {
  (*pnLigne) = pEtatEcran->nLigneEcriture;
  (*pnColonne) = pEtatEcran->nColonneEcriture;
  }

// --------------------------------------------------------------------------
void ScrWrite (CHAR *pszString)
  {
  int         nDerniereLigneModifiee;

  if (*pszString)
    {
    if (pEtatEcran->nLigneEcriture < pEtatEcran->nPremierLigneModifiee)
      {
      pEtatEcran->nPremierLigneModifiee = pEtatEcran->nLigneEcriture;
      }

    while (*pszString)
      {
      pEtatEcran->cBuffer [pEtatEcran->nLigneEcriture] [pEtatEcran->nColonneEcriture] = (*pszString);
      pszString++;
      pEtatEcran->nColonneEcriture++;
      pEtatEcran->nColonneEcriture %= NB_COL;
      if (pEtatEcran->nColonneEcriture == 0)
        {
        pEtatEcran->nLigneEcriture++;
        pEtatEcran->nLigneEcriture %= NB_LGN;
        }
      }
    if (pEtatEcran->nColonneEcriture == 0)
      {
      nDerniereLigneModifiee = pEtatEcran->nLigneEcriture;
      }
    else
      {
      nDerniereLigneModifiee = pEtatEcran->nLigneEcriture + 1;
      }

    if (nDerniereLigneModifiee > pEtatEcran->nDerniereLigneModifiee)
      {
      pEtatEcran->nDerniereLigneModifiee = nDerniereLigneModifiee;
      }
    }
  }

// --------------------------------------------------------------------------
void ScrWriteLn (CHAR *pszString)
  {
  INT iLgn;
  INT iCol;

  ScrWrite (pszString);
  ScrGetLgnCol (&iLgn, &iCol);
  ScrGotoLgnCol (iLgn + 1, 0);
  }

// --------------------------------------------------------------------------
void ScrRefresh (void)
  {
  bSemPrends (pEtatEcran->hsemRefresh);
  ::PostMessage (pEtatEcran->hwnd, WM_RAFRAICHIS, 0, 0);

  // Attente rafraichissement �cran pour synchro visualisation
  dwSemAttenteLibre (pEtatEcran->hsemRefresh, INFINITE);
  }

// --------------------------------------------------------------------------
BOOL bScrRAZToucheAppuyee (INT iKey)
  {
  BOOL bKeyState;

	Verif ((iKey >=0) && (iKey < NB_TOUCHES));
  bKeyState = pEtatEcran->abTouchesAppuyees [iKey];
  if (bKeyState)
    {
    pEtatEcran->abTouchesAppuyees [iKey] = FALSE;
    }

  return (bKeyState);
  }

// --------------------------------------------------------------------------
void ScrQhelpOpen (BOOL ouverture)
  {
  if (ouverture)
    {
    ::PostMessage (pEtatEcran->hwnd, WM_QHELP_OPEN, 0, 0);
    }
  else
    {
    ::PostMessage (pEtatEcran->hwnd, WM_QHELP_CLOSE, 0, 0);
    ::PostMessage (pEtatEcran->hwnd, WM_QHELP_FLUSH, 0, 0);
    }
  }

// --------------------------------------------------------------------------
// Enregistre un nouveau fichier d'aide courant
void ScrQhelpLoadTxt (char *pszNomFic, BOOL bUtilisateur)
  {
  ScrAcces ();
		StrCopy (pEtatEcran->szNomFicQhelp, pszNomFic);
  ScrFinAcces ();
  ::PostMessage (pEtatEcran->hwnd, WM_QHELP_LOAD, bUtilisateur, 0);
  }

// --------------------------------------------------------------------------
// Enregistre un nouveau fichier d'aide et son titre courant
void ScrQhelpSetIdx (char *pszNomFic, char *pszTitre)
  {
  ScrAcces ();
    StrCopy (pEtatEcran->szNomFicQhelp, pszNomFic);
    StrCopy (pEtatEcran->szTitreQhelp, pszTitre);
  ScrFinAcces ();
  ::PostMessage (pEtatEcran->hwnd, WM_QHELP_INIT, 0, 0);
  }


// --------------------------------------------------------------------------
//                 PROCEDURES APPELEES PAR L'Interface utilisateur
// --------------------------------------------------------------------------

// ----------------------------------------------------------------------- 
// Traitement du message WM_CHAR (pour touches Ansii)
// renvoie TRUE si la touche a �t� trait�e
// -----------------------------------------------------------------------
BOOL ScrOnChar (WPARAM mp1)
  {
  BOOL          bOk = TRUE;
  INT           iTouche;

	char c = LOBYTE (mp1);

	switch (c)
		{
		case VK_RETURN: // VK_NEWLINE
			iTouche = TOUCHE_RETURN;
      break;

		default:
			bOk = FALSE;
			break;
		}

  if (bOk)
    {
    pEtatEcran->abTouchesAppuyees [iTouche] = TRUE;
    }
	
  return bOk;
  } // OnChar

//--------------------------------------------------------------------
// Prise en compte de l'appui d'une touche (selon la nomenclature ci dessus)
void ScrAppuiTouche   (DWORD dwCodePcsTouche)
	{
	Verif (dwCodePcsTouche < NB_TOUCHES);
	//protection acc�s multi-thread
  ScrAcces ();
  pEtatEcran->abTouchesAppuyees [dwCodePcsTouche] = TRUE;
	ScrFinAcces ();
	}

//--------------------------------------------------------------------
// message WM_KEYDOWN : d�tecte et renvoie une touche g�r�e par les applications PCSEXE
BOOL bScrOnKeyDownTouchePcs
	(
	WPARAM mp1,
	DWORD * pdwCodeTouchePcs	// Code mis � jour s'il s'agit d'une touche g�r�e par PCS
	) // Renvoie TRUE s'il s'agit d'une touche g�r�e par PCS
  {
  BOOL	bToucheTraitee = TRUE;
  DWORD dwTouche = LOBYTE (mp1);

	switch (dwTouche)
		{
		case VK_LEFT: // fl�che vers la gauche
			dwTouche = TOUCHE_LEFT;
			break;

		case VK_RIGHT: // fl�che vers la droite
			dwTouche = TOUCHE_RIGHT;
			break;

		case VK_UP: // fl�che vers le haut
			dwTouche = TOUCHE_UP;
			break;

		case VK_DOWN: // fl�che vers le bas
			dwTouche = TOUCHE_DOWN;
			break;

		case VK_PRIOR: // VK_PAGEUP:
			dwTouche = TOUCHE_PAGE_UP;
			break;

		case VK_NEXT: // VK_PAGEDOWN:
			dwTouche = TOUCHE_PAGE_DOWN;
			break;

		default:
			// Appui d'une touche fonction ?
			if ((dwTouche >= VK_F1) && (dwTouche <= VK_F20)) // VK_F10))
				{
				// oui => r�cup�re le code Pcs de la touche de fonction
				BOOL	bShift = ((0x8000 & GetKeyState (VK_SHIFT))!= 0);

				if (bShift)
					dwTouche = TOUCHE_SHIFT_F1 + dwTouche - VK_F1;
				else
					dwTouche = TOUCHE_F1 + dwTouche - VK_F1;

				// Touche de fonction g�r�e par Pcs ?
				if (dwTouche >= TOUCHE_SHIFT_F10)
					bToucheTraitee = FALSE;
				}
			else
				bToucheTraitee = FALSE;
			break;
		}
  if (bToucheTraitee)
    {
		Verif (dwTouche < NB_TOUCHES);
		* pdwCodeTouchePcs = dwTouche;
    }

  return bToucheTraitee;
  } // bScrOnKeyDownTouchePcs


// --------------------------------------------------------------------------
void ScrReSize (void)
  {
  RECT rcl;

//$$ restreindre � la taille max de la zone d'affichage
  ::GetClientRect (::GetParent (pEtatEcran->hwnd), &rcl);
	::MoveWindow (pEtatEcran->hwnd, 0, 0, rcl.right, rcl.bottom, TRUE);
  }


// --------------------------------------------------------------------------
// Fen�tre Screen (moniteur monochrome)
// --------------------------------------------------------------------------
static LRESULT CALLBACK wndprocScrExe (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT      mres = 0;              // Valeur de retour
  BOOL         bTraite = TRUE;        // Indique commande trait�e

  switch (msg)
    {
    case WM_CREATE:
			{
			HDC					hdc;
			TEXTMETRIC	fm;
			HFONT				hOldfont;
			LOGFONT			lf;	// Attributs de la police

			// Initialisation de pEtatEcran
			pEtatEcran = (PETAT_ECRAN)pMemAlloue (sizeof (ETAT_ECRAN));
			pEtatEcran->hsemAccesEtat  = hSemCree (TRUE);
			pEtatEcran->hsemRefresh = hSemCreeAttenteSeule (TRUE);
			pEtatEcran->hwnd = hwnd;
			//
			pEtatEcran->szNomFicQhelp [0] = '\0';
			pEtatEcran->szTitreQhelp  [0] = '\0';

			// initialise la police courante
			hdc = ::GetDC (hwnd);
			InitFont (hdc, szFont, sSizeFont, &lf);
			pEtatEcran->hfont = CreateFontIndirect (&lf);
			hOldfont = (HFONT)SelectObject(hdc, pEtatEcran->hfont);
			GetTextMetrics (hdc, &fm);
			SelectObject(hdc, hOldfont);
			::ReleaseDC (pEtatEcran->hwnd, hdc);

			pEtatEcran->cxChar = fm.tmAveCharWidth;
			pEtatEcran->cyChar = fm.tmHeight;
			ScrFinAcces();
			}
			break;

    case WM_DESTROY:
			DeleteObject (pEtatEcran->hfont); 

			ScrAcces ();		// Pour r�veiller les taches �ventuellement endormies.
			ScrFinAcces ();
			bSemFerme (&pEtatEcran->hsemAccesEtat);
			bSemFerme (&pEtatEcran->hsemRefresh);

			MemLibere ((PVOID *)(&pEtatEcran));
			break;

    //$$case WM_TRANSLATEACCEL:         //Pas de table de touche d'acc�leration.
    //     mres = (LRESULT) FALSE;
    //     break;

    case WM_CHAR:
			bTraite = ScrOnChar (mp1);
			mres = (LRESULT) bTraite;
			break;

    case WM_KEYDOWN:
			{
			DWORD	dwTouche;

			bTraite = bScrOnKeyDownTouchePcs (mp1, &dwTouche);
			if (bTraite)
				// Prise en compte de l'appui d'une touche (selon la nomenclature ci dessus)
				ScrAppuiTouche   (dwTouche);
			mres = (LRESULT) !bTraite;
			}
			break;


    case WM_ERASEBKGND:
			mres = (LRESULT) TRUE;  // Pas d'effacement
			break;

    case WM_PAINT:
			{
			PAINTSTRUCT ps;

      ::BeginPaint (hwnd, &ps);
			ScrAcces ();
			ScrPaint (ps.hdc);
			ScrFinAcces ();
			::EndPaint (hwnd, &ps);
			mres = 0;
			}
     break;

    case WM_RAFRAICHIS:
			if (pEtatEcran->nPremierLigneModifiee < pEtatEcran->nDerniereLigneModifiee)
				{
				RECT rclInvalid;

				// 
				::GetClientRect (pEtatEcran->hwnd, &rclInvalid);
				rclInvalid.top += (pEtatEcran->nPremierLigneModifiee * pEtatEcran->cyChar);
				rclInvalid.bottom = rclInvalid.top + ((pEtatEcran->nDerniereLigneModifiee - pEtatEcran->nPremierLigneModifiee) * pEtatEcran->cyChar);
				ScrAcces ();
				pEtatEcran->nPremierLigneModifiee = NB_LGN;
				pEtatEcran->nDerniereLigneModifiee = 0;
				ScrFinAcces ();
				::InvalidateRect (pEtatEcran->hwnd, &rclInvalid, TRUE);
				::UpdateWindow (hwnd);
				}
			bSemLibere (pEtatEcran->hsemRefresh);
			mres = 0;
			break;

    case WM_QHELP_OPEN:
			MontreAide ();
			mres = 0;
			break;

    case WM_QHELP_CLOSE:
			CacheAide ();
			mres = 0;
			break;

    case WM_QHELP_FLUSH:
			VideAide ();
			mres = 0;
			break;

    case WM_QHELP_LOAD:
			// charge un fichier d'aide utilisateur ou systeme (selon mp1)
			ChargeFichierAide (pEtatEcran->szNomFicQhelp, (BOOL) mp1);
			mres = 0;
			break;

    case WM_QHELP_INIT:
			AjouteIndexAide (pEtatEcran->szNomFicQhelp, pEtatEcran->szTitreQhelp);
			mres = 0;
			break;

    default:
			bTraite = FALSE;
			break;
    }

  if (!bTraite)
    {
    mres = DefWindowProc (hwnd, msg, mp1, mp2);
    }

  return (mres);
  }

// --------------------------------------------------------------------------
// cr�ation de la fen�tre fille ScrExe
BOOL bScrCreeFenetre (HWND hwndParent, UINT uId)
  {
	static BOOL	bClasseEnregistree = FALSE;
	BOOL				bOk = FALSE;

	// Cr�ation de la fen�tre
	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= 0; // CS_HREDRAW | CS_VREDRAW; initiles: alignement en haut � gauche du texte
		wc.lpfnWndProc		= wndprocScrExe; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= NULL; //(HBRUSH)(COLOR_APPWORKSPACE+1); pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseScrExe; 
 		bClasseEnregistree = RegisterClass (&wc);
		}

	if (bClasseEnregistree)
		{
		// cr�ation de la fen�tre ScrExe (g�re l'objet global pEtatEcran);
		CreateWindow (szClasseScrExe, NULL, WS_VISIBLE |WS_CHILD,
			0, 0, CW_USEDEFAULT, CW_USEDEFAULT, hwndParent, (HMENU) uId, Appli.hinst, NULL);

		// cr�ation Ok ?
		if (pEtatEcran->hwnd)
			{
			// oui => fin initialisation
			bOk = TRUE;

			ScrAcces ();
			ScrGotoLgnCol (0, 0);
			ScrClear ();
			ScrFinAcces ();

			// chargement d'un fichier d'aide par d�faut
			ScrQhelpLoadTxt (FQHELP, FALSE);
			}
		}
  return bOk;
  }



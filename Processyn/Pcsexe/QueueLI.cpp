// QueueLI.cpp: implementation of the CQueueLI class.
//  JS 23/9/2003  Tir� de QueueMan
//  NON IMPLANTE : D'autres peuvent ouvrir cette queue principale pour y �crire
//  ils r�cup�rent un handle de queue "secondaire" qu'ils peuvent fermer
//  sans impact sur la queue principale.
//  la fermeture de la queue principale ferme toutes les queues secondaires correspondantes
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Std.h"
#include "MemMan.h"
#include "BufMan.h"
#include "USem.h"
#include "UStr.h"
#include "UListeH.h"
#include "Verif.h"
#include "QueueLI.h"
VerifInit;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CQueueLI::CQueueLI()
	{
	m_dwNbElementsUtilisateurs = 0;	// Nombre d'�l�ments utilisateurs dans la queue
	m_dwTailleBufElements = 0;			// Taille en octets du buffer d'�l�ments utilisateur
	m_paElements = NULL;			// Adresse du tableau d'�l�ments utilisateur
	m_dwNbReferences = 0;
	// Cr�ation de la queue : en attente de connexion
	m_hsemAttenteConnexion = hSemCreeAttenteSeule(TRUE);
	m_hsemAttenteAR = hSemCreeAttenteSeule (TRUE);
	// s�maphore d'acc�s mutellement exclusif � cette queue
	m_hMutexAcces = CreateMutex (NULL, FALSE, NULL); // s�curit�, signaled, nom
	// s�maphore d'attente "au moins un �l�ment dans cette queue
	m_hsemAttenteRX = hSemCreeAttenteSeule (TRUE); // Pris tant qu'il n'y a pas de messages dans la queue
	}

CQueueLI::~CQueueLI()
	{
	// Lib�re les ressources de la queue
	Vide();
	if (m_hsemAttenteConnexion)
		{
		bSemLibere (m_hsemAttenteConnexion);
		Verif(bSemFerme (&m_hsemAttenteConnexion));
		}
	if (m_hsemAttenteAR)
		{
		bSemLibere (m_hsemAttenteAR);
		Verif (bSemFerme (&m_hsemAttenteAR));
		}
	if (m_hsemAttenteRX)
		{
		bSemLibere (m_hsemAttenteRX);
		Verif (bSemFerme (&m_hsemAttenteRX));
		}
	if (m_hMutexAcces)
		{
		ReleaseMutex (m_hMutexAcces);
		CloseHandle (m_hMutexAcces);
		m_hMutexAcces=NULL;
		}
	}

// -----------------------------------------------------------------------
// Renvoie TRUE si la demande d'AR a pu �tre prise, FALSE sinon
BOOL CQueueLI::bDemandeAR ()
  {
	BOOL bRes = FALSE;
	if (bAccesExclusif ())
		{
		// oui => envoi du message dans sa queue
		if (bSemPrends(m_hsemAttenteAR))
			bRes = TRUE;

		FinAccesExclusif ();
		}
	return bRes;
  } // bSignaleAttenteAR

// -----------------------------------------------------------------------
// Renvoie TRUE si l'envoi d'AR a pu �tre honor�e, FALSE sinon
BOOL CQueueLI::bEnvoiAR ()
  {
	BOOL bRes = FALSE;
	if (bAccesExclusif ())
		{
		// oui => envoi du message dans sa queue
		if (bSemLibere (m_hsemAttenteAR))
			bRes = TRUE;

		FinAccesExclusif ();
		}
	return bRes;
  } // bSignaleAttenteAR

//-------------------------------------------------------------------------
// Attente d'un accus� de reception sur une connexion pendant au plus timeout ms.
BOOL CQueueLI::bAttenteAR (DWORD dwTimeOutAttente)	// Time out max d'attente de l'accus� de r�ception (ms) ou ITC_INDEFINITE_WAIT
  {
  BOOL	bRes =  FALSE;
	if (bAccesExclusif ())
		{
		HSEM	hsemAttenteARTemp = m_hsemAttenteAR;

		// attente de l'arriv�e de l'AR
		FinAccesExclusif ();
		if (dwSemAttenteLibre (hsemAttenteARTemp, dwTimeOutAttente)== SEM_TIMEOUT)
			bRes = TRUE;
    }

	return bRes;
  } // bAttenteAR


// -----------------------------------------------
// supprime tous les �l�ments d'une queue
void	CQueueLI::Vide ()
	{
	DWORD dwNElement;
	PELEMENT_QUEUE_LI pelement = m_paElements;

	// parcours des �l�ments de la queue
	for (dwNElement = 0; dwNElement < m_dwNbElementsUtilisateurs; dwNElement++)
		{
		// donn�es associ�es � l'�l�ment ?
		if (pelement->cbBuf && pelement->pbBuf)
			// oui => lib�re les
			MemLibere (&pelement->pbBuf);

		// �l�ment suivant
		pelement++;
		}

	// supprime tous les �l�ments de la queue
	MemLibere ((PVOID *)(&m_paElements));
	m_paElements = NULL;
	m_dwTailleBufElements = 0;

	// plus d'�l�ments
	m_dwNbElementsUtilisateurs = 0;

	// s�maphore mis en en attente d'arriv�e d'�l�ments dans la queue
	if (m_hsemAttenteRX)
		Verif (bSemPrends (m_hsemAttenteRX));
	}

//------------------------------------------------------------------------------
// Cr�ation de la queue
BOOL CQueueLI::bCree()
	{
	BOOL bRes = FALSE;

	// Acc�s Ok ?
	if (bAccesExclusif ())
		{
		// Oui => Supprime tous les �l�ments de la queue
		Vide ();
		// Ajoute une r�f�rence � la queue $$ Queue marqu�e LISTENER
		AjouteReference();
		bRes = TRUE;
		FinAccesExclusif();
		}
		// renvoie TRUE si queue Ok
	return bRes;
	} // bCree


//------------------------------------------------------------------------------
void CQueueLI::Ferme ()
	{
	if (dwSupprimeReference()<1)
		bVide();
	}

//------------------------------------------------------------------------------
void CQueueLI::AjouteReference()
	{
	DWORD dwRet = 0;
	if (bAccesExclusif())
		{
		m_dwNbReferences += 1;

		// Au moins 2 r�f�rences ?
		if (m_dwNbReferences > 1)
			{
			// oui => Fin de l'attente connexion
			Verif (bSemLibere(m_hsemAttenteConnexion));
			}
		FinAccesExclusif();
		}
	}

//------------------------------------------------------------------------------
DWORD CQueueLI::dwSupprimeReference()
	{
	DWORD dwRet = 0;
	if (bAccesExclusif())
		{
		m_dwNbReferences -= 1;
		dwRet = m_dwNbReferences;

		// moins 2 r�f�rences ?
		if (dwRet <= 1)
			{
			// oui => Repasse en attente connexion
			Verif (bSemPrends(m_hsemAttenteConnexion));
			}
		FinAccesExclusif();
		}
	return dwRet;
	}

//------------------------------------------------------------------------------
BOOL CQueueLI::bVide ()
	{
	BOOL bRes = FALSE;

	if (bAccesExclusif())
		{
		Vide ();

		// Termin� Ok
		FinAccesExclusif ();
		bRes = TRUE;
		}

	return bRes;
	} // bVide

//------------------------------------------------------------------------------
// Ecriture d'un �lement dans la queue sp�cifi�e.
// Apr�s que l'�l�ment sp�cifi� soit �crit, le process propri�taire de la queue
// peut lire l'�l�ment � l'aide de uLit
BOOL CQueueLI::bEcrit
	(
	UINT	uiUser,			// valeur 32 bit associ�e � l'�l�ment
	DWORD cbBuf,      // Taille en octets de l'�l�ment � ajouter � la pile
	PVOID pbBuf       // Adresse du buffer contenant l'�l�ment � �crire
	)
	{
	BOOL bRes = FALSE;
	if (bAccesExclusif())
		{
		ELEMENT_QUEUE_LI element;

		// enregistrement des param�tres utilisateurs
		element.pbBuf = pbBuf;
		element.uiUser = uiUser;
		element.cbBuf = cbBuf;

		// ajout de l'�l�ment � la fin de la liste des �l�ments de cette queue Ok ?
		if (bBufAjouteFin ((PSTR *)&m_paElements, &m_dwTailleBufElements, (PSTR)&element, sizeof (element)))
			{
			// oui => mise � jour du s�maphore d'attente
			Verif (bSemLibere (m_hsemAttenteRX));
			m_dwNbElementsUtilisateurs ++;

			// un �l�ment en plus
			bRes = TRUE;
			}

		FinAccesExclusif ();
		}
	return bRes;
	} // bEcrit

//------------------------------------------------------------------------------
// Renvoie le plus ancien des �l�ments restants dans la queue.
// Si la queue est vide, retourne imm�diatement ou attend l'arriv�e 
// du prochain �l�ment selon bAttente.
// Le buffer r�cup�r� sera lib�r� (free) par le lecteur.
// Seul le process qui a cr�� la queue utilisera uLit.
// Renvoie 0 si Ok Sinon ERROR_HANDLE_EOF, ERROR_INVALID_HANDLE
UINT CQueueLI::uLit
	(
	UINT *	puiUser,	// adresse d'une variable pour r�cup�rer la valeur 32 bit associ�e � l'�l�ment ou NULL
	DWORD * pcbData,	// adresse de la variable pour r�cup�rer la longueur de l'�l�ment
	PVOID * ppbuf,		// Adresse d'un pointeur pour r�cup�rer l'�lement
	BOOL		bAttente,	// Attente illimit�e de l'arriv�e d'un �l�ment si TRUE, pas d'attente sinon
	PHSEM		phsem			// adresse pour r�cup�rer un handle de s�maphore libre d�s l'arriv�e d'un �l�ment ou NULL
	)
	{
	UINT wErr = ERROR_INVALID_HANDLE;

	// Queue ouverte ?
	if (bAccesExclusif())
		{
		// oui => y a t'il au moins un �l�ment dans la queue ?
		if (!m_dwNbElementsUtilisateurs)
			{
			// non => Doit on attendre qu'il y en ait un ?
			if (bAttente)
				{
				// oui => attente qu'il y ait au moins un �v�nement � lire
				FinAccesExclusif ();
				dwSemAttenteLibre (m_hsemAttenteRX, INFINITE);
				}
			else
				{
				// non => termin� : rien � lire
				wErr = ERROR_HANDLE_EOF;

				// r�cup�ration �ventuelle du s�maphore de synchro
				if (phsem)
					*phsem = m_hsemAttenteRX;
				FinAccesExclusif ();
				}
			}
		else
			FinAccesExclusif ();
		} // if (bAccesExclusif())

	// queue ouverte
	if (bAccesExclusif())
		{
		// elle contient au moins un �l�ment ?
		if (m_dwNbElementsUtilisateurs)
			{
			// oui => on r�cup�re les donn�es de cet �l�ment
			PELEMENT_QUEUE_LI pelement = m_paElements;

			if (puiUser)
				*puiUser = pelement->uiUser;
			if (pcbData)
				*pcbData = pelement->cbBuf;
			if (ppbuf)
				*ppbuf = pelement->pbBuf;
			// on le supprime de la queue
			if (bBufSupprimeDebut ((PSTR *)&m_paElements, &m_dwTailleBufElements, sizeof (*pelement)))
				{
				// un �l�ment de moins
				m_dwNbElementsUtilisateurs --;
				// plus d'�l�ment ?
				if (!m_dwNbElementsUtilisateurs)

					// oui => Attente de nouveaux �l�ments
					Verif (bSemPrends (m_hsemAttenteRX));

				wErr = 0;
				}
			else
				wErr = ERROR_INVALID_HANDLE;
			}
		FinAccesExclusif ();
		} // if (bAccesExclusif)
	return wErr;
	} // uLit

//------------------------------------------------------------------------------
// R�cup�re le s�maphore d'attente d'au moins un �l�ment de la queue
// Renvoie TRUE si le handle de s�maphore est recopi�, FALSE sinon
BOOL CQueueLI::bGetSemAttenteElement (PHSEM phsemDest)	// adresse pour recopie du s�maphore de pr�sence d'un �l�ment dans la queue
	{
	BOOL bRes = FALSE;

	// pointeur s�maphore valide
	if (phsemDest)
		{
		if (bAccesExclusif())
			{
			*phsemDest = m_hsemAttenteRX;
			FinAccesExclusif ();
			bRes = TRUE;
			}
		}
	return bRes;
	} // bGetSemAttenteElement

//------------------------------------------------------------------------------
// R�cup�re le s�maphore d'attente de connexion
// Renvoie TRUE si le handle de s�maphore est recopi�, FALSE sinon
BOOL CQueueLI::bGetSemAttenteConnexion (PHSEM phsemDest)	// adresse pour recopie du s�maphore de pr�sence d'un �l�ment dans la queue
	{
	BOOL bRes = FALSE;

	// pointeur s�maphore valide
	if (phsemDest)
		{
		if (bAccesExclusif())
			{
			*phsemDest = m_hsemAttenteConnexion;
			FinAccesExclusif ();
			bRes = TRUE;
			}
		}
	return bRes;
	} // bGetSemAttenteElement

//------------------------------------------------------------------------------
// Acc�s exclusif � la queue $$ �viter si non multithread ???
BOOL CQueueLI::bAccesExclusif ()
	{
	BOOL bRes = FALSE;
	if (m_hMutexAcces != NULL)
		{
		bRes = WaitForSingleObject (m_hMutexAcces, INFINITE)==WAIT_OBJECT_0;
		}
	return bRes;
	}

//------------------------------------------------------------------------------
// Fin Acc�s exclusif � la queue
void CQueueLI::FinAccesExclusif ()
	{
	if (m_hMutexAcces != NULL)
		ReleaseMutex (m_hMutexAcces);
	}

// Gestion des infos de version stock�es en resssource
#include "stdafx.h"
#include "Appli.h"
#include "Docman.h"
#include "UStr.h"
#include "IdStrProcessyn.h"
#include "VersMan.h"

//static PCSTR pszCommments = "";
//static PCSTR pszCompanyName = "Logique Industrie";
//static PCSTR pszFileDescription = "";
//static PCSTR pszFileVersion = "";
//static PCSTR pszInternalName = "Processyn";
static PCSTR pszLegalCopyright = ID_STR_COPYRIGHT_PROCESSYN;
//static PCSTR pszLegalTradeMarks = "";
//static PCSTR pszOriginalFileName = "";
static PCSTR pszPrivateBuild = ID_STR_RELEASE_PROCESSYN;
static PCSTR pszProductName = ID_STR_NOM_PRODUIT;
static PCSTR pszProductVersion = ID_STR_NUM_VERSION_PROCESSYN;
//static PCSTR pszSpecialBuild = "";

//----------------------------------------------------------
// renvoie une chaine compos� de la version et du num�ro de release
PSTR GetVersionComplete (PSTR pszVersionComplete)
	{
	StrCopy (pszVersionComplete, ID_STR_VERSION_RELEASE_PROCESSYN);
	return pszVersionComplete;
	}

//----------------------------------------------------------
// Affiche les infos de Version couramment charg�s
BOOL bAfficheInfoVersion (void)
	{
  //MSGBOXPARAMS	mp;
	char szTexte [2000];

	// R�cup�re des infos sur la version de l'OS
	char szVersionOS[1000] ="?";
	Appli.GetNomOS(szVersionOS);
	wsprintf
		(szTexte, 
		"%s Version %s %s\n"
		"%s\n\n"
		"%s\n"
		"Code page %lu (ansi %lu)\n\n"
		"Syst�me d'exploitation : %s\n"
		"%s\n\n"
		"Ex�cutable : %s\n"
		"sur : %s\n\n"
		"Application utilisateur : %s\n"
		"sur %s\n",

		pszProductName, pszProductVersion, pszPrivateBuild,
		pszLegalCopyright,
		Appli.szLangueUtilisateur,
		Appli.dwCodePage, (DWORD)Appli.byCharSetFont,
		szVersionOS,
		Appli.OSVersionInfoEx.szCSDVersion,
		Appli.szNomExe,
		Appli.szPathExe,
		pszNameDoc(),
		pszPathDoc()
		);
	::MessageBox (Appli.hwnd, szTexte, Appli.szNom, MB_OK|MB_ICONINFORMATION);
	return TRUE;
	}

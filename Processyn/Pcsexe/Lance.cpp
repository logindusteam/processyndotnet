#include "stdafx.h"
#include "std.h"
#include "PathMan.h"
#include "UStr.h"
#include "mem.h"
#include "lng_res.h"
#include "tipe.h"          // Constantes...
#include "Descripteur.h"
#include "UChrono.h"         // ChronoEchantilloneMsMaintenant ()
#include "DescEx.h"        // get_fct_ ()
#include "pcs_sys.h"
#include "USem.h"                
#include "dongle.h"
#include "appliex.h"       // SauveApplication()   ChargeApplication()
#include "PcsVarEx.h"
#include "DocMan.h"
#include "Tipe_de.h"
#include "WDde.h"
#include "WExe.h"      // Menu
#include "cmdpcs.h"        // Suffixe
#include "Appli.h"
#include "USignale.h"
#include "lance.h"
#include "Verif.h"
VerifInit;

// ----------------------------- Bloc des Fonctions des Drivers Pr�sents ----
#define B_DESCRIPTEUR_EX			  640

// --------------------------------------------------------------------------
static void PartageRessources (PCHRONO pchronoCycleCourant, __int64 * pTempsDebutCycle)
  {
	// Attente de ATTENTE_CYCLE (en ms)
  LONG	TimeSleep = (LONG)CPcsVarEx::fGetValVarSysNumEx(c_sys_attente_cycle);
  if (TimeSleep >= 0)
    Sleep (TimeSleep);

	// Attente compl�mentaire � DUREE_CYCLE_DESIREE (en ms) si n�cessaire
	DWORD dwDureeCycleDesiree  = (DWORD)CPcsVarEx::fGetValVarSysNumEx(c_sys_duree_cycle_desiree);  // en centi�mes
	if (((int)dwDureeCycleDesiree) < 0)
		dwDureeCycleDesiree = 0;

	DWORD dwTempsCycle = (DWORD) (i64ValChronoMs (pchronoCycleCourant) - (*pTempsDebutCycle));
	if (dwTempsCycle < dwDureeCycleDesiree)
		{
		Sleep (dwDureeCycleDesiree - dwTempsCycle);  // en ms
		dwTempsCycle = dwDureeCycleDesiree;
		}

	// DUREE_DERNIER_CYCLE
	CPcsVarEx::SetValVarSysNumEx (c_sys_duree_dernier_cycle, (FLOAT) dwTempsCycle); // en ms
	}


// --------------------------------------------------------------------------
static void MajParametresEnchainement (char *pszApplicationSuivante, BOOL *bAChaud, BOOL *bEnchaine, BOOL *FinPcs, int *NumErreur)
  {
  PX_VAR_SYS pPosition = (X_VAR_SYS*)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, retour_systeme);

  BOOL * pValLog = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pPosition->dwPosCourEx));
  (*FinPcs)     = (BOOL) (*pValLog);

  pPosition     = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, errorlevel);
  FLOAT *pValReel = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pPosition->dwPosCourEx));
  (*NumErreur)  = (int) (*pValReel);
  //
  pPosition     = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, applic_suivante);
  char * pValMes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, (pPosition->dwPosCourEx));
  StrCopy (pszApplicationSuivante,pValMes);
  (*bEnchaine)  = DecodeApplicSuivante (pszApplicationSuivante, bAChaud);
  }

// --------------------------------------------------------------------------
void FermeDescripteursEx (void)
	{
	// Effectue les fermetures de chaque driver le permettant
  for (DWORD dwNDriver = nb_enregistrements (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX); dwNDriver > 0; dwNDriver--)
    {
		PDESCRIPTEUR_EX pDescripteurEx = (PDESCRIPTEUR_EX) pointe_enr (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX, dwNDriver);
		if (pDescripteurEx->fnTermine)
			{
			pDescripteurEx->fnTermine();
			}
    }
	}

// --------------------------------------------------------------------------
static void ExecuteUneApplicationUser (char *pszApplicationSuivante, BOOL *bAChaud, BOOL *bEnchaine, BOOL *FinPcs, int *NumErreur)
  {
  limiter_memoire ();
  limiter_temps ();
  limiter_nb_variables();

	//-------------------
	// INITIALISATION
	//-------------------
  // Ouverture des drivers
	// Mise a jour de la variable systeme ligne de commande
  char	szValLigneCommandePcs [MAX_PATH] = "";
	CopieParametresLigneCommandePcs (szValLigneCommandePcs);
	CPcsVarEx::SetValVarSysMesEx (c_sys_param_commande, szValLigneCommandePcs);

	// Initialise chaque driver le supportant
  for (DWORD dwNDriver = nb_enregistrements (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX); dwNDriver > 0; dwNDriver--)
    {
		PDESCRIPTEUR_EX pDescripteurEx = (PDESCRIPTEUR_EX) pointe_enr (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX, dwNDriver);
		if (pDescripteurEx->fnInit)
			{
			pDescripteurEx->fnInit();
			}
    }

	CHRONO chronoCycleCourant;
	ChronoLance (&chronoCycleCourant);
	BOOL bAlarmeCode = FALSE;

  do
    {
    // Mise � jour des param�tres du cycle
		BOOL bPremierPassage = CPcsVarEx::bGetValVarSysLogEx (premier_passage);
		__int64 dwTempsDebutCycle = i64ValChronoMs (&chronoCycleCourant);

		//-------------------
		// ENTREES
		//-------------------
    // Les drivers actifs effectuent leurs entr�es
		ChronoEchantilloneMsMaintenant ();

		// Effectue les entr�es de chaque driver le permettant
		for (DWORD	dwNDriver = nb_enregistrements (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX); dwNDriver > 0; dwNDriver--)
			{
			PDESCRIPTEUR_EX pDescripteurEx = (PDESCRIPTEUR_EX) pointe_enr (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX, dwNDriver);
			if (pDescripteurEx->fnEntrees)
				{
				pDescripteurEx->fnEntrees(bPremierPassage);
				}
			}

		//-------------------
		// TRAITEMENT
		//-------------------
		// Appel des fonctions traitement de chaque descripteur le permettant
		for (DWORD dwNDriver = nb_enregistrements (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX); dwNDriver > 0; dwNDriver--)
			{
			PDESCRIPTEUR_EX pDescripteurEx = (PDESCRIPTEUR_EX) pointe_enr (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX, dwNDriver);
			if (pDescripteurEx->fnTraite)
				{
				pDescripteurEx->fnTraite();
				}
			}

		FLOAT	StatusCode = CPcsVarEx::fGetValVarSysNumEx (VA_SYS_STATUS_CODE);
		FLOAT	MasqueStatusCode = CPcsVarEx::fGetValVarSysNumEx (VA_SYS_MASQUE_STATUS_CODE); // Correction Bug JS 17/5/2000
		DWORD	StatusCodeMasque = ((DWORD)StatusCode) & ((DWORD)MasqueStatusCode);
		BOOL	MemArretExploit = CPcsVarEx::bGetValVarSysLogEx(arret_exploitation);
		BOOL	MemRetourSysteme = CPcsVarEx::bGetValVarSysLogEx(retour_systeme);

		bAlarmeCode = (StatusCodeMasque != 0);
		if (bAlarmeCode)
			{
			// Si StatusCode, on restore l'ancienne BDD (servait anciennement pour inhiber les sorties...
			trans_enr (szVERIFSource, __LINE__, nb_enregistrements (szVERIFSource, __LINE__, b_cour_log), bva_mem_log, b_cour_log, 1, 1);
			trans_enr (szVERIFSource, __LINE__, nb_enregistrements (szVERIFSource, __LINE__, b_cour_num), bva_mem_num, b_cour_num, 1, 1);
			trans_enr (szVERIFSource, __LINE__, nb_enregistrements (szVERIFSource, __LINE__, b_cour_mes), bva_mem_mes, b_cour_mes, 1, 1);

			// Remet malgr� tout les nouvelles valeurs
			CPcsVarEx::SetValVarSysNumEx (VA_SYS_STATUS_CODE, StatusCode);
			CPcsVarEx::SetValVarSysLogEx (arret_exploitation, MemArretExploit);
			CPcsVarEx::SetValVarSysLogEx (retour_systeme, MemRetourSysteme);
			}

		// Signale si n�cessaire l'apparition ou la disparition de l'alarme code
		SetAlarmeCode (bAlarmeCode);

		// un peu de protection
    test_dongle ();
		suit_cailloux ();

		//-------------------
		// SORTIES
		//-------------------
		// Appel des fonctions sorties de chaque descripteur le permettant
		for (DWORD dwNDriver = nb_enregistrements (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX); dwNDriver > 0; dwNDriver--)
			{
			PDESCRIPTEUR_EX pDescripteurEx = (PDESCRIPTEUR_EX) pointe_enr (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX, dwNDriver);

			if (pDescripteurEx->fnSorties)
				{
				// Pas d'alarme ou alors : alarme avec sortie sur autre que p�riph�rique ?
				if ((!bAlarmeCode) || (!pDescripteurEx->bSortiesVersPeripheriques))
					// Oui => effectue la sortie
					pDescripteurEx->fnSorties(bPremierPassage);
				}
			}

		// Backup BDD
		trans_enr (szVERIFSource, __LINE__, nb_enregistrements (szVERIFSource, __LINE__, b_cour_log), b_cour_log, bva_mem_log, 1, 1);
		trans_enr (szVERIFSource, __LINE__, nb_enregistrements (szVERIFSource, __LINE__, b_cour_num), b_cour_num, bva_mem_num, 1, 1);
		trans_enr (szVERIFSource, __LINE__, nb_enregistrements (szVERIFSource, __LINE__, b_cour_mes), b_cour_mes, bva_mem_mes, 1, 1);

		CPcsVarEx::ResetBlocsExValeurModifie ();

    // rend la main au syst�me le temps demand�
    PartageRessources (&chronoCycleCourant, &dwTempsDebutCycle);

		// Tant que arret_exploitation ou retour systeme non demand� et que la dur�e d'utilisation est valide
    } while ((!CPcsVarEx::bGetValVarSysLogEx(arret_exploitation)) && (!CPcsVarEx::bGetValVarSysLogEx(retour_systeme)) && (!limite_temps_ecoule ()));
  
	//-------------------
	// FERMETURE
	//-------------------
	FermeDescripteursEx();

	// Pr�pare l'enchainement
  MajParametresEnchainement (pszApplicationSuivante, bAChaud, bEnchaine, FinPcs, NumErreur);
  } // ExecuteUneApplicationUser

// --------------------------------------------------------------------------------
//				maj du bloc des procedures qui seront appel�es en cours de cycle
// --------------------------------------------------------------------------------
static void MajFctDrivers (DWORD dwNDriver)
	{
	// r�cup�re la fonctions d'entr�e des variables
	DESCRIPTEUR_EX DescripteurExTemp;

  if (bGetDescripteurEx (dwNDriver, &DescripteurExTemp))
    {
    insere_enr (szVERIFSource, __LINE__, 1, B_DESCRIPTEUR_EX, 1);
		PDESCRIPTEUR_EX pDescripteurEx = (PDESCRIPTEUR_EX) pointe_enr (szVERIFSource, __LINE__, B_DESCRIPTEUR_EX, 1);
    *pDescripteurEx = DescripteurExTemp;
    }
	}

// --------------------------------------------------------------------------
//								fonctions export�es
// --------------------------------------------------------------------------

// Initialise l'acc�s � l'ex�cution des descripteurs
void InitFctDrivers (void)
  {
  // Vide le bloc fonctions Inits
  if (existe_repere (B_DESCRIPTEUR_EX))
    vide_bloc (B_DESCRIPTEUR_EX);
  else
    cree_bloc (B_DESCRIPTEUR_EX, sizeof (DESCRIPTEUR_EX), 0);

  // boucle pour chaque driver
  for (DWORD dwNDriver = 0; dwNDriver <= dernier_driver; dwNDriver++)
    {
		if (dwNDriver != d_graf)
			{
			MajFctDrivers (dwNDriver);
			}
    }
	
	// l'appel des traitements (entr�e et sortie) associ�s au graphisme sera effectu� en dernier
	MajFctDrivers (d_graf);  
  } // InitFctDrivers 

// --------------------------------------------------------------------------
BOOL RetourSysteme (void)
  {
  BOOL  bFin = FALSE;

  if (existe_repere (b_va_systeme))
    {
		PX_VAR_SYS pPosition = (X_VAR_SYS*)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, retour_systeme);
		BOOL *pValLog  = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pPosition->dwPosCourEx));

    bFin = *pValLog;
    }
  return bFin;
  }

// --------------------------------------------------------------------------
BOOL ExecuteApplicationsUtilisateur (char *pszNomApplication, BOOL bDemarreAChaud, BOOL *FinPcs, int *NumErreur)
  {
  #define chaude TRUE

  BOOL bOk = TRUE;
  char    pszApplication         [MAX_PATH];
  char    pszApplicationSuivante [MAX_PATH];
  BOOL bTemperature = bDemarreAChaud;
  BOOL bEnchaine;

  StrCopy (pszApplication, pszNomApplication);
	// Le syst�me a suffisament de couleurs pour permettre un affichage de synoptique valide ?
	if (Appli.nNbBitsParPixel < 16)
		SignaleWarnExit ("\nYour graphic card has currently not enough colors.\n"
		"Minimum required is 16 bits per pixel (65536 colors); \n"
		"improper color rendering may occur with this configuration\n"
		);

  init_test_dongle ();

  do
    {
    if (bOk)
      {
			if (bTemperature)
				{
				if (existe_repere (b_va_systeme))
					{
					CPcsVarEx::SetValVarSysLogEx(arret_exploitation, FALSE);
					CPcsVarEx::SetValVarSysLogEx(retour_systeme, FALSE);
					}
				}
      ExecuteUneApplicationUser (pszApplicationSuivante, &bTemperature, &bEnchaine, FinPcs, NumErreur);

      StrCopy (pszApplication, pszPathNameDoc());
      SauveApplication (pszApplication, TRUE);
      SauveLienDde (pszApplication);

      if (!(*FinPcs))
        {
        if (bEnchaine)
          {
          StrCopy (pszApplication, pszApplicationSuivante);
          limiter_enchainement ();
          bOk = ChargeApplication (pszApplication,bTemperature);
					MnuTitreAJour();
					InitRessourcesAppli();
          }
        }
      }
    } while ( (!(*FinPcs)) && (bEnchaine) && (!ExisteMenu()) && (bOk) );

  return bOk;
  } // ExecuteApplicationsUtilisateur

// --------------------------------------------------------------------------

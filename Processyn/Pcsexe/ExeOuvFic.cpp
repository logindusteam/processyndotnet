//-------------------------------------------------------
// ExeOuvFic.c
// Pcsexe : boite de dialogue d'ouverture de fichier appel�e par le code utilisateur (EXCUTE_F38)
// JS Win32 11/06/97
//-------------------------------------------------------
#include "stdafx.h"
#include "Appli.h"
#include "UStr.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "pcsdlg.h"
#include "DocMan.h"
#include "PathMan.h"
#include "Wexe.h"

#include "ExeOuvFic.h"

// Variables de la boite de dialogue
typedef struct
	{
	DWORD wBoiteSelectFicOuverte;
	char szPathRecherche [MAX_PATH];
	char szTypeDeFichier [MAX_PATH];
	char szSelectFic [MAX_PATH];
	} EXE_OUV_FIC_PARAM;

static EXE_OUV_FIC_PARAM ExeOuvFicParam = {BOITE_OUV_FIC_TERMINEE_ABANDON, "","",""};

//-----------------------------------------------------------------------------------------------
// Demande soit l'ouverture de la boite de s�lection d'un fichier, soit son �tat
// Remarque : une seule boite peut �tre ouverte � la fois
DWORD wDlgNomFichierPcs (BOOL bOuvreLaBoite, PSTR pszPathDeRecherche, PCSTR pszTypeFichierDeRecherche)
  {
  DWORD wCodeRet;

	// Demande l'ouverture de la boite ?
  if (bOuvreLaBoite)
    {
    // Oui => boite ouvrable ?
    if (ExeOuvFicParam.wBoiteSelectFicOuverte != BOITE_OUV_FIC_OUVERTE)
      {
			// oui => pr�pare les param�tres :
			// R�cup�re l'extension (par d�faut *)
      if (StrIsNull (pszTypeFichierDeRecherche))
				StrCopy (ExeOuvFicParam.szTypeDeFichier, "*.*");
			else
				{
	      StrCopy (ExeOuvFicParam.szTypeDeFichier, pszTypeFichierDeRecherche);
				}

			// path de recherche par d�faut = r�pertoire Doc
      if (StrIsNull (pszPathDeRecherche))
				StrCopy (ExeOuvFicParam.szPathRecherche, pszPathDoc());
			else
				StrCopy (ExeOuvFicParam.szPathRecherche, pszPathDeRecherche);

			// Associe le type de fichier recherch� et le path 
			pszCreePathName (ExeOuvFicParam.szPathRecherche, MAX_PATH, ExeOuvFicParam.szPathRecherche, ExeOuvFicParam.szTypeDeFichier);

			// transforme le path de recherche en extension
			if ((pszTypeFichierDeRecherche[0] == '*') && (pszTypeFichierDeRecherche[1] == '.'))
				StrCopy (ExeOuvFicParam.szTypeDeFichier, pszTypeFichierDeRecherche+2);

			// Pas de fichier s�lectionn� pour l'instant
      StrSetNull (ExeOuvFicParam.szSelectFic);

			// boite ouverte
      ExeOuvFicParam.wBoiteSelectFicOuverte = BOITE_OUV_FIC_OUVERTE;

			// ordonne au thread Wexe de demander l'ouverture de la boite de dialogue
      ::PostMessage(Appli.hwnd, WM_SET_DLG_SELECT_FIC, 0, 0);

			// pas d'erreur
      wCodeRet = OUVERTURE_BOITE_OUV_FIC_OK;
      }
    else
      {
			// non => erreur boite d�ja ouverte
      wCodeRet = OUVERTURE_BOITE_OUV_FIC_IMPOSSIBLE;
      }
    } // if (bOuvreLaBoite)
  else
    {
    // non => demande l'�tat de la boite et son r�sultat
    StrCopy (pszPathDeRecherche, ExeOuvFicParam.szSelectFic);
    wCodeRet = ExeOuvFicParam.wBoiteSelectFicOuverte;
    }

  return wCodeRet;
  } // wDlgNomFichierPcs

// --------------------------------------------------------------
// Appel effectif de la boite de dialogue d'ouverture de fichier
void DlgOuvreBoiteSelectFic ()
  {
  char pszTitre[MAX_PATH];
  char pszAction[30];

  bLngLireInformation (c_inf_tdlg_ouvrir, pszTitre);
  bLngLireInformation (c_inf_action_ouvre, pszAction);

  if (bDlgFichierOuvrir (Appli.hwnd, ExeOuvFicParam.szPathRecherche, pszTitre, ExeOuvFicParam.szTypeDeFichier, pszAction))
    {
		StrCopy (ExeOuvFicParam.szSelectFic, ExeOuvFicParam.szPathRecherche);
    ExeOuvFicParam.wBoiteSelectFicOuverte = BOITE_OUV_FIC_TERMINEE_OK;
    }
  else
    {
    ExeOuvFicParam.wBoiteSelectFicOuverte = BOITE_OUV_FIC_TERMINEE_ABANDON;
    }
  } // DlgOuvreBoiteSelectFic


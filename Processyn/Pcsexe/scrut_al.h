/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Auteur  : LM							|
 |   Date    : 27/07/93 						|
 +----------------------------------------------------------------------*/


//---------------------------- codes erreurs ----------------------------
#define AL_REC_ERR_SIZE_DATA              0X0001
#define AL_REC_ERR_GET_MBX                0X0002
#define AL_REC_ERR_DATA_TYPE              0X0004

#define AL_SEND_ERR_PUT_MBX               0X0008
#define AL_SEND_ERR_DATA_TYPE             0X0010
#define AL_SEND_ERR_SIG_WAIT              0X0020
#define AL_SEND_ERR_WAIT_ACK              0X0040
#define AL_SEND_ERR_RQU_SEMA              0X0080
#define AL_SEND_ERR_ANIM                  0X0100
#define AL_SEND_ERR_PUT_ACK               0X0200
#define AL_SEND_TIMEOUT                   0X0400

#define AL_MBX_ERR_CLOSE                  0X0800

#define AL_TIMEOUT_ACK                    10000L


//---------------------------- Fonctions Export�es --------------------
void sorties_al (BOOL first_pass);
void recoit_al (BOOL first_pass);
void  initialise_al(void);
void  termine_al(void);

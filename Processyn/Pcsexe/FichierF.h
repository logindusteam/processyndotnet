// FichierF.h: interface for the CFichierF class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FICHIERF_H__74BE59CF_2441_4D6E_A968_7EC19C89BE99__INCLUDED_)
#define AFX_FICHIERF_H__74BE59CF_2441_4D6E_A968_7EC19C89BE99__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CFichierF  
	{
	public:
	enum OpenFlags 
		{
		modeRead =          0x0000,	// Opens the file for reading only.
		modeWrite =         0x0001,	// Opens the file for reading and writing.
		modeReadWrite =     0x0002,	// Opens the file for writing only.
		shareCompat =       0x0000,	// This flag is not available in 32 bit MFC. This flag maps to CFile::shareExclusive when used in CFile::Open.
		shareExclusive =    0x0010,	// Opens the file with exclusive mode, denying other processes both read and write access to the file. Construction fails if the file has been opened in any other mode for read or write access, even by the current process.
		shareDenyWrite =    0x0020,	// Opens the file and denies other processes write access to the file. Create fails if the file has been opened in compatibility mode or for write access by any other process.
		shareDenyRead =     0x0030,	// Opens the file and denies other processes read access to the file. Create fails if the file has been opened in compatibility mode or for read access by any other process.
		shareDenyNone =     0x0040,	// Opens the file without denying other processes read or write access to the file. Create fails if the file has been opened in compatibility mode by any other process.
		modeNoInherit =     0x0080,	// Prevents the file from being inherited by child processes.
		modeCreate =        0x1000,	// Directs the constructor to create a new file. If the file exists already, it is truncated to 0 length.
		modeNoTruncate =    0x2000,	// Combine this value with modeCreate. If the file being created already exists, it is not truncated to 0 length. Thus the file is guaranteed to open, either as a newly created file or as an existing file. This might be useful, for example, when opening a settings file that may or may not exist already. This option applies to CStdioFile as well.
		typeText =          0x4000, // Sets text mode with special processing for carriage return�linefeed pairs (used in derived classes only).
		typeBinary =   (int)0x8000	// Sets binary mode (used in derived classes only).
		};
		CFichierF();
		virtual ~CFichierF();

		// -----------------------------------------------
		// Ouverture d'un fichier
		BOOL bOuvre (PCSTR pszName, DWORD wFlags);

		// Fermeture d'un fichier ()
		BOOL bFerme ();

		// Renvoie la taille courante d'un fichier
		BOOL bLongueur (DWORD * pwSize);

		// -----------------------------------------------
		// Pointeur de lecture - �criture
		// -----------------------------------------------

		// Sp�cifie l'offset depuis le d�but de fichier du pointeur de lecture/�criture
		// Permet de positionner le pointeur AU DELA de la fin de fichier !
		BOOL bPointe (DWORD dwOffset);

		// R�cup�re l'offset depuis le d�but de fichier du pointeur de lecture/�criture
		BOOL bValPointe (DWORD * pdwOffset);

		// Pointe � la fin du fichier
		BOOL bPointeFin ();

		// -----------------------------------------------
		// Ecriture fichier
		// renvoie TRUE en cas d'�criture partielle
		BOOL bEcrire (const void * pBuff, DWORD dwTailleAEcrire, DWORD * pdwTailleEcrite);

		// Ecriture s�quentielle
		// renvoie FALSE si tout n'a pas pu �tre �crit
		BOOL bEcrireTout (const void * pBuff, DWORD dwTailleAEcrire);

		// Ecriture s�quentielle d'une chaine de caract�re
		BOOL bEcrireSz (PCSTR pszBufferLn);

		// Ecriture s�quentielle d'une chaine de caract�re puis d'un Cr Lf
		BOOL bEcrireSzLn (PCSTR pszBufferLn);

		// -----------------------------------------------
		// Lecture s�quentielle
		// renvoie TRUE MEME SI LA LECTURE N'EST QUE PARTIELLE - FALSE si erreur.
		BOOL bLire (PVOID pBuff, DWORD dwTailleALire, DWORD * pdwTailleLue);

		// Lecture s�quentielle
		// renvoie FALSE si tout n'a pas pu �tre lu
		BOOL bLireTout (PVOID pBuff, DWORD dwTailleALire);

		// Lecture s�quentielle d'une ligne de texte
		BOOL bLireLn (PSTR pszDest, DWORD dwTailleDest, PDWORD pdwNbOctetsLus = NULL);

		// ---------------------------------------------------------------------
		// Teste la possibilit�s d'ouverture d'un fichier selon l'acc�s sp�cifi�
		BOOL bAcces (PCSTR pszName, DWORD wFlags);

		// Efface un fichier
		BOOL bEfface (PCSTR pszName);

		BOOL bOuvert() const {return m_hFile != INVALID_HANDLE_VALUE;}

		private:
			HANDLE m_hFile; //INVALID_HANDLE_VALUE
			char m_szPathName [MAX_PATH];
	};

// -----------------------------------------------
//					Ouverture / fermeture de fichiers
// Flags : (piqu�s aux MFC)
// Sharing and access mode. Specifies the action to take when opening the file. 
// You can combine options listed below by using the bitwise-OR (|) operator. 
// One access permission and one share option are required; 
// the modeCreate and modeNoInherit modes are optional.
#define F_OF_DRIV_DQ (CFichierF::modeCreate|CFichierF::modeReadWrite|CFichierF::shareDenyNone|CFichierF::modeNoTruncate) // Mode d'ouverture de Driv_dq
#define F_OF_OUVRE_LECTURE_SEULEF (CFichierF::modeRead|CFichierF::shareDenyNone|CFichierF::modeNoTruncate) // Lecture non exclusive d'un fichier devant exister
#define F_OF_OUVRE_LECTURE_SEULE_EXCLUSIF (CFichierF::modeRead|CFichierF::shareDenyWrite|CFichierF::modeNoTruncate) // Lecture exclusive d'un fichier devant exister
#define F_OF_OUVRE (CFichierF::modeReadWrite|CFichierF::shareDenyNone|CFichierF::modeNoTruncate) // Lecture exclusive d'un fichier devant exister
#define F_OF_OUVRE_EXCLUSIF (CFichierF::modeReadWrite|shareDenyRead|CFichierF::shareDenyWrite|CFichierF::modeNoTruncate) // Lecture exclusive d'un fichier devant exister
#define F_OF_CREE (CFichierF::modeCreate|CFichierF::modeReadWrite|CFichierF::shareDenyNone) // Lecture �criture non exclusive d'un fichier cr��
#define F_OF_CREE_EXCLUSIF (CFichierF::modeCreate|CFichierF::modeReadWrite|CFichierF::shareDenyRead|CFichierF::shareDenyWrite) // Lecture �criture exclusive d'un fichier cr��
#define F_OF_OUVRE_OU_CREE (CFichierF::modeCreate|CFichierF::modeReadWrite|CFichierF::shareDenyNone|CFichierF::modeNoTruncate) // Lecture �criture non exclusive d'un fichier cr�� s'il n'existe pas
#define F_OF_OUVRE_OU_CREE_EXCLUSIF (CFichierF::modeCreate|CFichierF::modeReadWrite|CFichierF::shareDenyRead|CFichierF::shareDenyWrite|CFichierF::modeNoTruncate) // Lecture �criture exclusive d'un fichier cr�� s'il n'existe pas

#endif // !defined(AFX_FICHIERF_H__74BE59CF_2441_4D6E_A968_7EC19C89BE99__INCLUDED_)



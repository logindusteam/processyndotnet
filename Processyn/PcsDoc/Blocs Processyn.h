//------------------------------------------------------
// Blocs Processyn.h
// Liste des blocs utilis�s dans Processyn
// Derni�re mise � jour :
// 18/5/98 
// Historique :
//	Cr�ation : 18/5/98
//------------------------------------------------------
/*----------------------------------------------------------------------
#define libre 0

  b_e_s = 1;                                    b_cour_log = 11;
  b_identif = 2;                                b_cour_num = 12;
  b_constant = 3;                               b_cour_mes = 13;
  bx_anm_e_radio = 4;                           bva_mem_log = 14;
  b_pt_constant = 5;                            bva_mem_num = 15;
  b_geo_jbus = 6;                               bva_mem_mes = 16;
  b_jbus_e = 7;                                 bx_code = 17;
  b_cour_log_modifie = 8;                       bx_jbus_e_log = 18;
  b_cour_num_modifie = 9;                       bx_jbus_e_num_entier = 19;
  b_organise_es = 10;                           bx_jbus_s_log = 20;

  bx_jbus_s_num_entier = 21;                    b_spec_titre_paragraf_tess = libre;
  b_limite_driver = 22;                         bx_tess_e_log = libre;
  b_cour_mes_modifie = 23;                      bx_tess_e_num = libre;
  b_periph_entree = 24;                         bx_tess_fenetre = 34;
  b_periph_sortie = 25;                         b_titre_paragraf_ca = 35;
  b_organise_cle = 26;                          b_tess_synop = libre;
  bx_para_jbus_log = 27;                        b_fifo_tess_envoit = libre;
  bx_para_jbus_num = 28;                        b_fifo_tess_recoit = libre;
  b_id_auto_pcs = 29;                        	b_geo_mem = 39;
  b_tess_e_num = libre;                         b_code = 40;

  b_geo_code = 41;                              bx_clavier = 51;
  b_tess_courbe = libre;                        bx_imprimantes = 52;
  bx_tess_courbe = libre;                       b_spec_titre_paragraf_ca = 53;
  b_mot_res = 44;                               b_spec_titre_paragraf_dq = 54;
  b_titre_paragraf_jb = 45;                     b_titre_paragraf_dq = 55;
  b_jbus_s = 46;                                b_geo_ancien_dq = 56;
  bx_num = 47;                                  b_va_systeme = 57;
  bx_mes = 48;                                  b_geo_vdi                 = 58;
  b_spec_titre_paragraf_jb = 49;                b_titre_paragraf_vdi      = 59;
  b_geo_syst = 50;                              b_spec_titre_paragraf_vdi = 60;

  b_vdi_r_log = 61;                             bx_tess_r_num = 71;
  b_vdi_r_num = 62;                             bx_tess_r_mes = 72;
  bx_anm_r_log_vi = 63;                         b_geo_ca = 73;
  bx_anm_r_num_vi = 64;                         bx_disq = 74;
  bx_anm_r_mes_vi = 65;                         bx_var_disq = 75;
  b_vdi_r_mes = 66;                             b_spec_s_ca = 76;
  b_tess_r_mes = 67;                            bx_tess_e_mes = 77;
  b_tess_r_num = 68;                            b_tess_e_mes = 78;
  b_tess_thermometre = 69;                      bx_para_jbus_com = 79;
  bx_tess_thermometre = 70;                     b_titre_paragraf_chrono = 80;

  b_metronome = 81;                             bx_trame_ca = 91;
  b_geo_chrono = 82;                            bx_e_ca = 92;
  bx_chronometre = 83;                          bx_s_ca = 93;
  bx_metronome = 84;                            bx_anm_courbe_archive_vi = 94;
  bx_anm_cour_mes_al = 85;                      bx_vdi_courbe_cmd = 95;
  b_vdi_courbe_cmd = 86;                        b_vdi_r_num_alpha = 96;
  bx_anm_groupe_al   = 87;                      bx_anm_r_num_alpha_vi = 97;
  buffer_anm_surv_al = 88;                      b_geo_tcs = 98;
  buffer_anm_visu_al = 89;                      b_titre_paragraf_tcs = 99;
  bx_anm_glob_al     = 90;                      b_spec_titre_paragraf_tcs = 100;

  b_tcs = 101;                                  b_bloc_note_cz = 111;
  bx_anm_consultation_al = 102;                 b_lecture_cz = 112;
  bx_para_tcs_com = 103;                        bx_anm_tempo_al = 113;
  bx_tcs_e = 104;                               b_geo_dde       = 114;
  bx_anm_e_vi    = 105;                         bx_cz_s = 115;
  b_spec_titre_paragraf_de = 106;               bx_cz_e = 116;
  b_titre_paragraf_de      = 107;               bx_para_cz_com = 117;
  b_geo_cz = 108;                               bx_para_cz = 118;
  b_titre_paragraf_cz = 109;                    b_vdi_e_log = 119;
  b_spec_titre_paragraf_cz = 110;               b_vdi_e_num = 120;

  b_vdi_courbe_temps = 121;                     b_spec_titre_paragraf_om = 131;
  bx_vdi_courbe_temps = 122;                    b_titre_paragraf_om = 132;
  b_dde_e = 123;                                b_om_e = 133;
  b_dde_r = 124;                                b_om_s = 134;
  bx_dde_lien_e = 125;                          bx_para_om_com = 135;
  bx_dde_lien_r = 126;                          bx_para_om = 136;
  b_vdi_e_mes = 127;                            bx_om_s_num = 137;
  bx_tcs_s = 128;                               bx_om_s_log = 138;
  bx_anm_courbe_vi = 129;                       bx_om_e = 139;
  b_geo_om = 130;                               bx_table_transfert_jb = 140;

  bx_table_transfert_om = 141;                  bx_table_transfert_km = 151;
  b_geo_km = 142;                               b_geo_re = 152;
  b_spec_titre_paragraf_km = 143;               b_titre_paragraf_re = 153;
  b_titre_paragraf_km = 144;                    b_spec_titre_paragraf_re = 154;
  b_km_e = 145;                                 b_liste = 155;
  b_km_s = 146;                                 bx_liste = 156;
  bx_para_km_com = 147;                         b_temp_nom_structure = 157;
  bx_km_e = 148;                                b_temp_genc_genx = 158;
  bx_km_s_num = 149;                            b_geo_ad = 159;
  bx_para_km = 150;                             b_spec_titre_paragraf_ad = 160;

  b_titre_paragraf_ad = 161;                    b_spec_titre_paragraf_fp = 171;
  b_ad_e = 162;                                 b_titre_paragraf_fp = 172;
  b_ad_s = 163;                                 b_fp_e = 173;
  bx_table_transfert_ad = 164;                  b_fp_s = 174;
  bx_para_ad = 165;                             b_geo_opto = 175;
  bx_ad_e = 166;                                b_spec_titre_paragraf_opto = 176;
  bx_ad_s_num = 167;                            b_titre_paragraf_opto = 177;
  bx_ad_s_mess = 168;                           b_opto_es_log = 178;
  bx_para_ad_com = 169;                         b_opto_es_num = 179;
  b_geo_fp = 170;                               bx_opto_entree = 180;

  bx_opto_sortie = 181;                         b_spec_titre_paragraf_eth = 191;
  bx_param_opto = 182;                          b_eth = 192;
  bx_param_opto_com = 183;                      bx_para_eth_com = 193;
  bx_table_transfert_fp = 184;                  bx_eth_e = 194;
  bx_para_fp = 185;                             bx_eth_s = 195;
  bx_fp_e = 186;                                b_geo_rt = 196;
  bx_fp_s = 187;                                bx_rt = 197;
  bx_para_fp_com = 188;                         b_geo_ss = 198;
  b_geo_eth = 189;                              b_titre_paragraf_ss = 199;
  b_titre_paragraf_eth = 190;                   b_spec_titre_paragraf_ss = 200;

  b_ss_e = 201;                                 bx_dde_e_advise = 211;
  b_ss_s = 202;                                 bx_dde_e_request = 212;
  bx_ss_e = 203;                                bx_dde_r = 213;
  bx_ss_s_log = 204;                            b_geo_sa = 214;
  bx_ss_s_num = 205;                            b_spec_titre_paragraf_sa = 215;
  bx_para_ss_com = 206;                         b_titre_paragraf_sa = 216;
  bx_para_ss = 207;                             b_sa_e = 217;
  b_dde_e_dyn = 208;                            b_sa_s = 218;
  b_dde_r_dyn = 209;                            bx_table_transfert_sa = 219;
  bx_var_dde_client_dyn = 210;                  bx_para_sa = 220;

  bx_sa_e = 221;                                bx_mo_e = 231;
  bx_sa_s_num = 222;                            bx_mo_s = 232;
  bx_sa_s_log = 223;                            bx_param_trame_mo_e = 233;
  bx_para_sa_com = 224;                         bx_param_trame_mo_s = 234;
  bx_pile_sa1 = 225 ;                           bx_mo_init = 235;
  bx_pile_sa2 = 226 ;                           bx_param_init_mo = 236;
  b_geo_mo = 227;                               bx_para_mo_com = 237;
  b_spec_titre_paragraf_mo =228  ;              b_param_tactil = 238;
  b_titre_paragraf_mo = 229 ;                   bx_pile_tactil = 239;
  b_mo_es = 230 ;                               b_geo_l1 = 240;

  b_spec_titre_paragraf_l1 = 241;               b_spec_titre_paragraf_sp = 251;
  b_titre_paragraf_l1 = 242;                    b_titre_paragraf_sp = 252;
  b_l1_e = 243;                                 b_sp_e = 253;
  b_l1_s = 244;                                 b_sp_s = 254;
  bx_para_e_l1 = 245;                           bx_esclave_sp = 255;
  bx_l1_e = 246;                                bx_es_sp = 256 ;
  bx_para_s_l1 = 247;                           bx_transfert_log = 257 ;
  bx_l1_s = 248;                                bx_transfert_num = 258 ;
  bx_para_l1_com = 249;                         bx_transfert_mes = 259 ;
  b_geo_sp = 250;                               bx_transfert_plog = 260 ;

  bx_transfert_pnum = 261 ;                     bx_lac_e_surv = 271;
  bx_transfert_pmes = 262 ;                     bx_lac_s_log = 272;
  bx_dde_var_serveur = 263;                     bx_lac_s_num = 273;
  b_geo_lac = 264;                              bx_para_lac = 274;
  b_spec_titre_paragraf_la = 265;               bx_para_lac_surv = 275;
  b_titre_paragraf_la = 266;                    bx_para_lac_com = 276;
  b_lac_e = 267;                                bx_table_transfert_la = 277;
  b_lac_s = 268;                                bx_trame_lac = 278;
  b_lac_e_surv = 269;                           b_geo_factor = 279;
  bx_lac_e = 270;                               b_spec_titre_paragraf_fa = 280;

  b_titre_paragraf_fa = 281;                    bx_dqs = 291;
  b_factor_e = 282;                             bx_factor_dqs = 292;
  b_factor_s = 283;                             bx_para_factor_com = 293;
  b_factor_e_surv = 284;                        bx_table_transfert_fa = 294;
  bx_factor_e = 285;                            bx_station_lac = 295;
  bx_factor_e_surv = 286;                       bx_station_smba = 296;
  bx_factor_s_log = 287;                        bx_e_s          = 297;
  bx_factor_s_num = 288;                        bx_e_s_cour_log = 298;
  bx_para_factor = 289;                         bx_vdi_courbe_archive      = 299;
  bx_para_factor_surv = 290;                    b_vdi_courbe_archivage     = 300;

  bx_rc = 301;                                  bx_para_kmlac = 311;
  bx_para_rc = 302;                             bx_table_transfert_kmlac = 312;
  b_geo_kmlac = 303;                            b_factor_e_mes = 313 ;
  b_spec_titre_paragraf_kmlac = 304;            b_factor_s_mes = 314 ;
  b_titre_paragraf_kmlac = 305;                 bx_factor_e_mes = 315 ;
  b_kmlac_e = 306;                              bx_factor_s_mes = 316 ;
  b_kmlac_s = 307;                              bx_para_factor_mes = 317 ;
  bx_para_kmlac_com = 308;                      b_jbus_e_mes = 318;
  bx_kmlac_e = 309;                             b_jbus_s_mes = 319;
  bx_kmlac_s_num = 310;                         bx_jbus_e_mes = 320;

  bx_jbus_s_mes = 321;                          b_rc_es = 331;
  bx_para_jbus_mes = 322;                       b_rc_cmd = 332;
  repere_e,b_tempo_ppgr = 323; (dispo)          bg_esclave_rc = 333;
  bg_fac,bg_ce,b_tempo_ppgr = 324; (dispo)      bg_rc = 334;
  bg_fac,bg_c,eb_tempo_ppgr = 325; (dispo)      bx_para_rc = 335;
  b_represente_syst = 326;                      bx_esclave_rc = 336;
  bx_km_s_log = 327;                            bx_rc = 337;
  b_geo_rc = 328;                               b_geo_jbusrc = 338;
  b_spec_titre_paragraf_rc = 329;               b_spec_titre_paragraf_jbrc = 339;
  b_titre_paragraf_rc = 330;                    b_titre_paragraf_jbrc = 340;

  b_jbusrc_e = 341;                             bx_jbusrc_es_num = 351;
  b_jbusrc_s = 342;                             bx_para_jbusrc_log = 352;
  b_jbusrc_es = 343;                            bx_para_jbusrc_num = 353;
  bg_jbusrc_e_log = 344;                        bx_para_jbusrc_com = 354;
  bg_jbusrc_e_num = 345;                        bx_para_jbusrc_es = 355;
  bx_jbusrc_e_log = 346;
  bx_jbusrc_e_num = 347;                        b_geo_celduc = 351;
  bx_jbusrc_s_log = 348;                        b_spec_titre_paragraf_celduc = 352;
  bx_jbusrc_s_num = 349;                        b_titre_paragraf_celduc = 353;
  bx_jbusrc_es_log = 350;                       b_celduc_ad_s = 354;
                                                b_celduc_d_sl_specif = 355;
                                                b_celduc_d_creneau = 356;
                                                b_celduc_d_el = 357;
                                                b_celduc_d_en = 358;
                                                b_celduc_a_en = 359;
                                                b_celduc_a_el = 360;



  b_celduc_a_signaux = 361;                     bx_celduc_s2 = 371;
  b_celduc_lcd = 362;                           bx_para_celduc_com = 372;
  bx_para_e_celduc = 363;                       bx_init_carte_celduc = 373;
  bx_para_s_celduc = 364;                       b_geo_h1 = 374;
  bx_celduc_e = 365;                            b_spec_titre_paragraf_h1 = 375;
  bx_celduc_e2 = 366;                           b_titre_paragraf_h1 = 376;
  bx_celduc_e3 = 367;                           b_h1_e = 377;
  bx_celduc_e4 = 368;                           b_h1_s = 378;
  bx_celduc_s = 369;                            bx_para_e_h1 = 379;
  bx_celduc_s1 = 370;                           bx_h1_e = 380;

  bx_para_s_h1 = 381;                           b_ex_el_ai = 391;
  bx_h1_s = 382;                                b_ex_al_ao = 392;
  bx_e_s_cour_num = 383;                        b_ex_en = 393;
  b_tableau = 384;                              b_ex_n_ds = 394;
  bx_tableau = 385;                             b_ex_sn = 395;
  b_geo_ex = 386;                               bx_ex_slog = 396;
  b_spec_titre_paragraf_ex = 387;               bx_trame_ex_slog = 397;
  b_titre_paragraf_ex = 388;                    bx_ex_snum = 398;
  b_ex_e = 389;                                 bx_trame_ex_snum = 399;
  b_ex_l = 390;                                 bx_ex_sds = 400;

  bx_trame_ex_sds = 401;                        b_titre_paragraf_ccm = 411;
  bx_trame_e = 402;                             b_ccm_es = 412;
  bx_ex_e_aio = 403;                            bx_para_e_ccm = 413;
  bx_ex_e_ndat = 404;                           bx_ccm_e = 414;
  bx_ex_e_ldat = 405;                           bx_para_s_ccm = 415;
  bx_ex_eds = 406;                              bx_ccm_s = 416;
  bx_trame_eds_ldat = 407;                      bx_para_ccm_com = 417;
  bx_para_ex_com = 408;                         b_param_tactil2 = 418;
  b_geo_ccm = 409;                              bx_pile_tactil2 = 419;
  b_spec_titre_paragraf_ccm = 410;              bn_constant = 420;

  b_cst_nom = 421;                              b_opn_num = 431;
  bn_identif = 422;                             bx_opn_elog = 432;
  b_es_nom = 423;                               bx_opn_slog = 433;
  b_pt_constant_vi = 424; <- libre              bx_opn_enum = 434;
  bn_constant_vi = 425; <- libre                bx_opn_snum = 435;
  b_cst_nom_vi = 426; <- libre                  bx_opn_param_num = 436;
  b_jbusrc_es_tab = 427;                        bx_para_opn_com = 437;
  b_geo_opn = 428;                              b_param_crayon = 438;
  b_titre_paragraf_opn = 429;                   bx_pile_crayon = 439;
  b_spec_titre_paragraf_opn = 430;              b_vdi_r_log_tab = 440;

  b_vdi_r_num_tab = 441;                        bx_jbus_s_log_tab = 451;
  b_vdi_r_mes_tab = 442;                        bx_jbus_s_num_entier_tab = 452;
  b_vdi_r_num_alpha_tab = 443;                  b_info_applic            = 453;
  bg_jbus_e_log = 444;                          bx_e_s_cour_mes          = 454;
  bg_jbus_e_num_entier = 445;                   b_frontal                = 455;
  bg_jbus_e_mes = 446;                          b_wi_e = 456 ;
  b_jbus_e_tab = 447;                           b_wi_s = 457 ;
  b_jbus_s_tab = 448;                           b_wi_param = 458 ;
  bx_jbus_e_log_tab = 449;                      b_wi_journal = 459 ;
  bx_jbus_e_num_entier_tab = 450;               bx_wi_e_cst = 460 ;

  bx_wi_e_var = 461 ;                           b_ab_s = 471;
  bx_wi_s_cst = 462 ;                           b_ab_e_tab = 472;
  bx_wi_s_var = 463 ;                           b_ab_s_tab = 473;
  bx_wi_journal = 464 ;                         bx_ab_e_log = 474;
  bx_wi_journal_var = 465 ;                     bx_ab_e_num = 475;
  bx_wi_para_com = 466 ;                        bx_ab_s_log = 476;
  b_geo_ab = 467;                               bx_ab_s_num = 477;
  b_spec_titre_paragraf_ab = 468;               bx_ab_e_log_tab = 478;
  b_titre_paragraf_ab = 469;                    bx_ab_e_num_tab = 479;
  b_ab_e = 470;                                 bx_ab_s_log_tab = 480;

  bx_ab_s_num_tab = 481;                        b_titre_paragraf_ti = 491;
  bx_para_ab_log = 482;                         b_ti_e = 492;
  bx_para_ab_num = 483;                         b_ti_s = 493;
  bx_para_ab_com = 484;                         b_ti_e_tab = 494;
  bx_table_transfert_ab = 485;                  b_ti_e_mes = 495;
  bg_ab_e_log = 486;                            b_ti_s_tab = 496;
  bg_ab_e_num = 487;                            b_ti_s_mes = 497;
  bx_table_api_ab = 488;                        bx_ti_e_log = 498;
  b_geo_ti = 489;                               bx_ti_e_num = 499;
  b_spec_titre_paragraf_ti = 490;               bx_ti_e_mes = 500;

  bx_ti_s_log = 501;                            bx_para_ti_com = 511;
  bx_ti_s_num = 502;                            bx_table_api_ti = 512;
  bx_ti_s_mes = 503;                            bx_table_transfert_ti = 513;
  bx_ti_e_log_tab = 504;                        bg_ti_e_log = 514;
  bx_ti_e_num_tab = 505;                        bg_ti_e_num = 515;
  bx_ti_s_log_tab = 506;                        bg_ti_e_mes = 516;
  bx_ti_s_num_tab = 507;                        b_geo_ma = 517;
  bx_para_ti_log = 508;                         b_spec_titre_paragraf_ma = 518;
  bx_para_ti_num = 509;                         b_titre_paragraf_ma = 519;
  bx_para_ti_mes = 510;                         b_ma_e_num = 520;

  b_ma_s_num = 521;                             bx_jbus_s_num_mot = 531;
  b_ma_e_log = 522;                             bx_jbus_s_num_reel = 532;
  b_ma_s_log = 523;                             bg_jbus_e_num_mot = 533;
  bx_ma_e_num = 524;                            bg_jbus_e_num_reel = 534;
  bx_ma_s_num = 525;                            bx_jbus_e_num_mot_tab = 535;
  bx_ma_e_log = 526;                            bx_jbus_e_num_reel_tab = 536;
  bx_ma_s_log = 527;                            bx_jbus_s_num_mot_tab = 537;
  bx_seg_offset_ma = 528;                       bx_jbus_s_num_reel_tab = 538;
  bx_jbus_e_num_mot = 529;                      bx_para_jbus_num_mot = 539;
  bx_jbus_e_num_reel = 530;                     bx_para_jbus_num_reel = 540;

  b_geo_wi = 541;                               bx_et_s_para = 551;
  b_spec_titre_paragraf_wi = 542;               bx_et_e_num = 552;
  b_titre_paragraf_wi = 543;                    bx_et_s_num = 553;
  b_geo_et = 544;                               bx_et_e_log = 554;
  b_titre_paragraf_et = 545;                    bx_et_s_log = 555;
  b_et_s_para = 546;                            b_geo_eu                 = 556;
  b_et_e_num = 547;                             b_spec_titre_paragraf_eu = 557;
  b_et_s_num = 548;                             b_titre_paragraf_eu      = 558;
  b_et_e_log = 549;                             b_eu_e_log               = 559;
  b_et_s_log = 550;                             b_eu_e_num               = 560;

  b_eu_s_log               = 561;               b_eu_pgm                 = 571;
  b_eu_s_num               = 562;               bx_eu_pgm                = 572;
  b_eu_adresse             = 563;               b_vdi_r_log_4etats       = 573;
  bx_eu_e_num              = 564;               bx_anm_r_log_4etats_vi   = 574;
  bx_eu_e_log              = 565;               b_vdi_r_num_dep          = 575;
  bx_eu_s_num              = 566;               bx_anm_r_num_dep_vi      = 576;
  bx_eu_s_log              = 567;               bx_pile_tactil3          = 577;
  bx_para_eu_com           = 568;               b_param_tactil3          = 578;
  b_libelle_saisie         = 569;               b_anm_r_mes_nv1          = 579;
  b_geo_disq               = 570;               b_vdi_r_mes_tab_nv1      = 580;

  b_vdi_r_num_alpha_nb1    = 581;		b_mitsu_e_tab		 = 591;
  b_vdi_r_num_alpha_tab_nv1= 582;		b_mitsu_s_tab		 = 592;
  b_vdi_r_log_4etats_tab   = 583;    modif 710	b_mitsu_com		 = 593;
  b_vdi_r_num_dep_tab      = 584;    modif 711	bx_mitsu_station	 = 594;
  b_para_ip		   = 585;		bx_mitsu		 = 595;
  b_geo_mitsu		   = 586;		bx_mitsu_adr		 = 596;
  b_spec_titre_paragraf_mi = 587;		bx_mitsu_s		 = 597;
  b_titre_paragraf_mi	   = 588;		bg_mitsu_e_num		 = 598;
  b_mitsu_e		   = 589;		bg_mitsu_e_log		 = 599;
  b_mitsu_s		   = 590;		bg_mitsu_e_num_tab	 = 600;

  bg_mitsu_e_log_tab	    = 601;		b_efi_s 		 = 611;
  bg_mitsu_adr		    = 602;		b_efi_e_mes		 = 612;
  bg_mitsu_s_log	    = 603;		b_efi_s_mes		 = 613;
  bg_mitsu_s_num	    = 604;		bx_efi_e_log		 = 614;
  bg_mitsu_s_num_tab	    = 605;		bx_efi_e_num		 = 615;
  bg_mitsu_s_log_tab	    = 606;		bx_efi_e_mes		 = 616;
  b_geo_efi		    = 607;		bx_efi_s_log		 = 617;
  b_spec_titre_paragraf_efi = 608;		bx_efi_s_num		 = 618;
  b_titre_paragraf_efi	    = 609;		bx_efi_s_mes		 = 619;
  b_efi_e		    = 610;		b_efi_e_tab		 = 620;

  b_efi_s_tab		    = 621;              b_va_fenetre_gr    = 631;
  bx_efi_e_log_tab	    = 622;              bx_dlg_ress        = libre;
  bx_efi_e_num_tab	    = 623;              bx_dlg_itm         = libre;
  bx_efi_s_log_tab	    = 624;              bx_dlg_data        = libre;
  bx_efi_s_num_tab	    = 625;              bx_dlg_ctrl_s      = libre;
  bx_param_efi		    = 626;              bx_anm_data        = libre;
  b_commande                = 627;              b_gener_canal_ap   = libre;
  b_fenetre_ctxtgr          = 628;              b_fct_inits        = 638;
  b_text_commande           = 629;              b_fct_entrees      = 639;
  b_fenetre_gr              = 630;              b_fct_traites      = 640;

  b_point (poly.asm)        = 641;              bx_mos_e_mes              = 651;
  b_geo_mos                 = 642;              bx_mos_s_log              = 652;
  b_spec_titre_paragraf_mos = 643;              bx_mos_s_num              = 653;
  b_titre_paragraf_mos      = 644;              bx_mos_s_mes              = 654;
  b_mos_e                   = 645;              b_mos_e_tab               = 655;
  b_mos_s                   = 646;              b_mos_s_tab               = 656;
  b_mos_e_mes               = 647;              bx_mos_e_log_tab          = 657;
  b_mos_s_mes               = 648;              bx_mos_e_num_tab          = 658;
  bx_mos_e_log              = 649;              bx_mos_s_log_tab          = 659;
  bx_mos_e_num              = 650;              bx_mos_s_num_tab          = 660;

  bx_param_mos              = 661;              bx_eb_s_num               = 671;
  b_geo_eb                  = 662;              bx_eb_e_log               = 672;
  b_titre_paragraf_eb       = 663;              bx_eb_s_log               = 673;
  b_eb_e_num                = 664;              b_geo_ut                  = 674;
  b_eb_s_num                = 665;              b_spec_titre_paragraf_ut  = 675;
  b_eb_e_log                = 666;              b_titre_paragraf_ut       = 676;
  b_eb_s_log                = 667;              b_ut_e                    = 677;
  b_eb_e_tab                = 668;              b_ut_s                    = 678;
  b_eb_s_tab                = 669;              b_ut_e_tor                = 679;
  bx_eb_e_num               = 670;              b_ut_s_tor                = 680;

  b_ut_e_mes                = 681;              bx_ut_e_num_entier        = 691;
  b_ut_s_mes                = 682;              bx_ut_e_num_double        = 692;
  b_ut_e_tor_tab            = 683;              bx_ut_e_mes               = 693;
  b_ut_s_tor_tab            = 684;              bx_ut_s_log_interne       = 694;
  b_ut_e_tab                = 685;              bx_ut_s_log_interne_tab   = 695;
  b_ut_s_tab                = 686;              bx_ut_s_log_tor           = 696;
  b_ut_cyc_e                = 687;              bx_ut_s_log_tor_tab       = 697;
  b_ut_cyc_e_tab            = 688;              bx_ut_s_num_entier        = 698;
  bx_ut_e_log_interne       = 689;              bx_ut_s_num_entier_tab    = 699;
  bx_ut_e_log_tor           = 690;              bx_ut_s_num_double        = 700;

  bx_ut_s_num_double_tab    = 701;              bx_ip                    = 711;
  bx_ut_s_mes               = 702;              b_geo_t3                 = 712;
  bx_ut_cyc                 = 703;              b_spec_titre_paragraf_t3 = 713;
  libre			    = 704;              b_titre_paragraf_t3      = 714;  												  ????						b_t3_e                   = 715;
  b_celduc_a_en_creneau     = 706;              b_t3_s                   = 716;
  b_celduc_a_en_moyenne     = 707;              b_t3_e_tab               = 717;
  bx_celduc_e1              = 708;              b_t3_s_tab               = 718;
  b_celduc_d_el_memo        = 709;              bx_t3_e_log              = 719;
  b_geo_ip	            = 710;              bx_t3_e_num_mot      	 = 720;

  bx_t3_e_num_double        = 721;              bx_para_t3_com           = 731;
  bx_t3_e_log_tab           = 722;              bx_table_transfert_t3    = 732;
  bx_t3_e_num_mot_tab       = 723;              b_geo_nb                 = 733;
  bx_t3_e_num_double_tab    = 724;              b_titre_paragraf_nb      = 734;
  bx_t3_s_log	            = 725;              b_spec_titre_paragraf_nb = 735;
  bx_t3_s_num_mot	    = 726;              b_nb_e                   = 736;
  bx_t3_s_num_double        = 727;              b_nb_s                   = 737;
  bx_t3_s_log_tab	    = 728;              bx_nb_e_log              = 738;
  bx_t3_s_num_mot_tab       = 729;              bx_nb_e_num              = 739;
  bx_t3_s_num_double_tab    = 730;              bx_nb_e_mes              = 740;

  bx_nb_s_log		    = 741;		b_geo_ov1		 	= 751;
  bx_nb_s_num		    = 742;		b_titre_paragraf_ov1	 	= 752;
  bx_nb_s_mes		    = 743;		b_spec_titre_paragraf_ov1  	= 753;
  bx_session_nb_e	    = 744;		b_ov1_init		        = 754;
  bx_tampon_efi 	    = 745;		b_ov1_e 		        = 755;
  bx_session_nb_s	    = 746;		b_ov1_s 		        = 756;
  b_factor_tab		    = 747;		b_ov1_trait		        = 757;
  b_factor_surv_tab	    = 748;		b_geo_ov2                       = 758;
  bx_factor_tab_log	    = 749;		b_titre_paragraf_ov2	        = 759;
  bx_factor_tab_num	    = 750;		b_spec_titre_paragraf_ov2       = 760;

  b_ov2_init		    = 761;              bx_param_ov1_mes         = 771;
  b_ov2_e		    = 762;              bx_var_ov1_log           = 772;
  b_ov2_s		    = 763;              bx_var_ov1_num           = 773;
  b_ov2_trait		    = 764;              bx_var_ov1_mes           = 774;
  bx_module_ov1_init	    = 765;              bx_module_ov2_init       = 775;
  bx_module_ov1_e	    = 766;              bx_module_ov2_e          = 776;
  bx_module_ov1_trait	    = 767;              bx_module_ov2_trait      = 777;
  bx_module_ov1_s	    = 768;              bx_module_ov2_s          = 778;
  bx_param_ov1_log	    = 769;              bx_param_ov2_log         = 779;
  bx_param_ov1_num	    = 770;              bx_param_ov2_num         = 780;

  bx_param_ov2_mes	    = 781;		b_spec_titre_paragraf_al      = 791;
  bx_var_ov2_log	    = 782;		b_alarme		      = 792;
  bx_var_ov2_num	    = 783;		b_pt_libre		      = 793;
  bx_var_ov2_mes	    = 784;		bx_geo_al		      = 794;
  b_pt_constant_ov	    = 785;		bx_glob_al		      = 795;
  bn_constant_ov	    = 786;		bx_services_speciaux_al	      = 796;
  b_cst_nom_ov		    = 787;		bx_groupe_al		      = 797;
  b_geo_fr 		    = 788;		bx_priorite_al		      = 798;
  b_geo_al		    = 789;		bx_element_al     	      = 799;
  b_titre_paragraf_al	    = 790;		bx_variable_al  	      = 800;

  bx_anm_geo_al		    = 801;              b_commentaires                = 811;
  bx_anm_element_al	    = 802;              bx_anm_liste_elts_cli         = 812;
  bx_anm_variable_al	    = 803;              b_vdi_control_radio           = 813;                      
  bx_anm_cour_log_al	    = 804;              b_vdi_control_static          = 814;        
  bx_anm_cour_num_al	    = 805;              b_vdi_control_liste           = 815;    
  b_module_ov1_trait	    = 806;              b_vdi_control_combobox        = 816;                     
  b_module_ov2_trait	    = 807;              B_GEO_PAGES_GR                = 817;
  b_vdi_r_mes_nv2           = 808;              B_PAGES_GR                    = 818;
  b_vdi_r_mes_tab_nv2       = 809;              B_ELEMENTS_PAGES_GR           = 819;
  b_vdi_e_log_nv1	    = 810;              B_GEO_ELEMENTS_LISTES_GR      = 820;

  B_ELEMENTS_LISTES_GR        = 821;            bx_anm_e_control_log        = 831;
  B_ESPACEMENT_GR             = 822;            bx_anm_e_liste              = 832;
  B_MEM_ANIMATION_GR          = 823;            B_GEO_POINTS_SAUVEGARDES_GR = 833;
  B_GEO_TEXTES_GR             = 824;            B_POINTS_SAUVEGARDES_GR     = 834;
  B_TEXTES_GR                 = 825;            b_fct_sorties               = 835;
  B_GEO_POINTS_POLY_GR        = 826;            b_fct_fins                  = 836;
  B_POINTS_POLY_GR            = 827;            bx_fenetre_vi               = 837;
  B_MARQUES_GR                = 828;            bx_anm_fenetre_vi           = 838;
  B_GEO_MARQUES_GR            = 829;            bx_page_vi                  = 839;
  bx_vdi_dlg_combo_liste      = 830;            bx_element_vi               = 840;

  bx_variable_vi                = 841;          BLOC_COMMANDES            = 851;
  bx_services_speciaux_vi       = 842;          bx_page_gr_pcsexe         = 852;
  bx_entrees_vi                 = 843;          b_geo_ap                  = 853;
  bx_anm_page_vi                = 844;          b_spec_titre_paragraf_ap  = 854;
  bx_anm_e_log_vi               = 845;          b_titre_paragraf_ap       = 855;
  bx_anm_e_num_vi               = 846;          b_ap_e                    = 856;
  bx_anm_e_mes_vi               = 847;          b_ap_e_mes                = 857;
  bx_anm_e_combobox             = 848;          b_ap_s                    = 858;
  bx_vdi_nom_page               = 849;          b_ap_s_mes                = 859;
  b_anmgr_n_desc_libre          = 850;          b_ap_e_tab                = 860;

  b_ap_s_tab 	           = 861;               bx_ap_e_num_mot           = 871;
  b_ap_s_reflet	           = 862;               bx_ap_e_num_mot_tab       = 872;
  b_ap_s_reflet_mes        = 863;               bx_ap_e_num_reel          = 873;
  b_ap_s_tab_reflet        = 864;               bx_ap_e_num_reel_tab      = 874;
  b_ap_s_tab_reflet_mes    = 865;               bx_ap_e_mes               = 875;
  b_ap_fct_cyc             = 866;               bx_ap_s_log               = 876;
  bx_ap_e_log              = 867;               bx_ap_s_log_tab           = 877;
  bx_ap_e_log_tab          = 868;               bx_ap_s_num_entier        = 878;
  bx_ap_e_num_entier       = 869;               bx_ap_s_num_entier_tab    = 879;
  bx_ap_e_num_entier_tab   = 870;               bx_ap_s_num_mot           = 880;

  bx_ap_s_num_mot_tab           = 881;          bx_applicom_adresse_max   = 891;
  bx_ap_s_num_reel              = 882;          bx_debug_code             = 892;
  bx_ap_s_num_reel_tab          = 883;          b_var_deb                 = 893;
  bx_ap_s_mes                   = 884;          bx_log_forcage            = 894;
  bx_ap_s_log_tab_reflet        = 885;          bx_num_forcage            = 895;
  bx_ap_s_num_entier_tab_reflet = 886;          bx_mes_forcage            = 896;
  bx_ap_s_num_mot_tab_reflet    = 887;          b_libelle                 = 897;
  bx_ap_s_num_reel_tab_reflet   = 888;          bx_main_nb                = 898;
  bx_ap_s_tab_mes_reflet        = 889;          bx_buffer_horodate_al     = 899;
  bx_ap_fct_cyc                 = 890;		b_geo_opc                   900  

  b_spec_titre_paragraf_opc      901		?????			    911
  b_titre_paragraf_opc           902		?????			    912
  ?????				 903 		b_opc_item                  913
  ?????				 904		bx_opc_serveur              914
  ?????				 905		bx_opc_groupe               915
  ?????				 906		bx_opc_item                 916
  ?????				 907
  ?????				 908
  ?????				 909
  ?????				 910
  
  ??????????  opc : 900 � bx_opc_adresse_max = 937   ??????????????













  repere_e = 323;  a utiliser pour tout les cas tampons
				  deja utilise dans :
				   - gener_vi
				   - gener_km
				   - scrut_dq
				   - gener_ce	modif commentaire 16/11/90



--------------------------------
  AMBIGUITE
--------------------------------
bx_pile_sa3 = 227 ;	     bx_jbusrc_es_num = 351;
bx_pile_sa4 = 228 ;	     bx_para_jbusrc_log = 352;
bx_pile_sa5 = 229 ;      bx_para_jbusrc_num = 353;
bx_pile_sa6 = 230 ;      bx_para_jbusrc_com = 354;
                         bx_para_jbusrc_es = 355;

 ---------------------------------------------------------------------- */


@echo off
echo Effacement du contenu du r�pertoire d�mo
del d�mo\*.*
echo *** Copie des Exes
copy pcsbin\debug\Pcsconf.exe d�mo
copy pcsbin\debug\Pcsgen.exe d�mo
copy pcsbin\debug\Pcsgr.exe d�mo
copy pcsbin\debug\Pcsexe.exe d�mo

echo *** Copie des DLL de traductions
copy pcsbin\debug\Pcsconf*.dll d�mo
copy pcsbin\debug\Pcsgen*.dll d�mo
copy pcsbin\debug\Pcsgr*.dll d�mo
copy pcsbin\debug\Pcsexe*.dll d�mo
copy pcsbin\debug\PcsLng*.dll d�mo


echo *** Copie des fichiers necessaires aux EXE
copy Sources\FR\*.doc  d�mo
attrib -r d�mo\*.doc

echo *** Copie des elements pour la DLL pcsusrnt
copy Sources\PcsNTDB.h d�mo
attrib -r d�mo\PcsNTDB.h
copy pcsbin\debug\PcsNTDB.lib d�mo
attrib -r d�mo\PcsNTDB.lib
copy pcsbin\debug\PcsNTDB.dll d�mo

copy Sources\UCom.h d�mo
attrib -r d�mo\UCom.h
copy pcsbin\debug\LICom.lib d�mo
attrib -r d�mo\LICom.lib

copy Sources\PcsUsrNT.cpp d�mo
attrib -r d�mo\PcsUsrNT.cpp

echo *** Copie des elements pour la DLL Liclock
copy ..\Liclock\debug\Liclock.dll d�mo

echo *** Copie de la documentation de la version
copy Sources\Fr\LisezMoi.doc  d�mo
attrib -r d�mo\LisezMoi.doc
pause


﻿Imports System.Runtime.InteropServices
'Dll compatible COM prenant en charge l'envoi de mails sur un serveur SMTP.

'Interface sur les éléments exportés par ce composant
<GuidAttribute("B5C63C55-DA58-4abd-81D7-9BA8E292A228")> _
Public Interface MonTest
	Property x() As Integer
	Property y() As Integer
	Function somme(ByVal a As Integer, ByVal b As Integer) As Integer
End Interface

'Classe implémentant cet interface
<GuidAttribute("0D0F7000-788D-4b7f-AB96-178A2BCA9C66")> _
Public Class PcsMail
	Implements MonTest
	Private mx As Int32 = 0
	Private my As Int32 = 0

	Public Function somme(ByVal a As Int32, ByVal b As Int32) As Int32 Implements MonTest.somme
		Return a + b
	End Function

	Public Property x() As Int32 Implements MonTest.x
		Get
			Return mx
		End Get
		Set(ByVal value As Int32)
			mx = value
		End Set
	End Property

	Public Property y() As Int32 Implements MonTest.y
		Get
			Return my
		End Get
		Set(ByVal value As Int32)
			my = value
		End Set
	End Property
End Class

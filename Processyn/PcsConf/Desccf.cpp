/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de                                           |
 |                                                                          |
 |                  Societe LOGIQUE INDUSTRIE                               |
 |           Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |                                                                          |
 | Il est demeure sa propriete exclusive et est confidentiel.               |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/
// Desccf.cpp
// DESCCF.C     Init selective adresse interpreteur
// AC 18/03/94
#include "stdafx.h"
#include "col.h"               //Compilation selective
#include "std.h"               //Definition des types standard pour PCS
#include "lng_res.h"
#include "tipe.h"
#include "G_Objets.h"
#include "Descripteur.h"
#include "inter_sy.h"
#include "inter_gr.h"
#include "inter_al.h"
#include "inter_de.h"
#include "inter_me.h"
#include "inter_dq.h"
#include "inter_tp.h"
#include "inter_re.h"
#include "inter_co.h"

#ifdef c_modbus
  #include "inter_jb.h"
#endif

#ifdef c_ap
  #include "inter_ap.h"
#endif

#ifdef c_opc
  #include "inter_opc.h"
#endif

#include "desccf.h"

// --------------------------------------------------------------------------
// R�cup�re les adresses de proc d'un interpreteur s'il existe
// --------------------------------------------------------------------------
void RecupereDescripteurConfiguration (ID_DESCRIPTEUR IdDescripteur, PDESCRIPTEUR_CF pDescripteurCf)
  {
  switch (IdDescripteur)
    {
    case d_systeme :
      LisDescripteurSy (pDescripteurCf);
      break;

    case d_code :
			LisDescripteurCo (pDescripteurCf);
      break;

    case d_mem:
			LisDescripteurMe (pDescripteurCf);
      break;

    case d_dde:
			LisDescripteurDDE (pDescripteurCf);
      break;

    case d_al:
			LisDescripteurAl (pDescripteurCf);
      break;

    case d_repete:
			LisDescripteurRe (pDescripteurCf);
      break;

    case d_graf:
			LisDescripteurVDI (pDescripteurCf);
      break;

    case d_disq:
			LisDescripteurDq (pDescripteurCf);
      break;

    case d_chrono:
      LisDescripteurTp (pDescripteurCf);
      break;

#ifdef c_modbus
    case d_jbus:
			LisDescripteurJb (pDescripteurCf);
      break;
#endif

#ifdef c_ap
    case d_ap:
      LisDescripteurAp (pDescripteurCf);
      break;
#endif
#ifdef c_opc
    case d_opc:
			LisDescripteurOPC (pDescripteurCf);
      break;
#endif
    }
  }

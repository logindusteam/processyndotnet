/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : ppcf.h                                                   |
 |   Auteur  : AC  					        	|
 |   Date    : 25/6/92 					        	|
 |   Version : 3.20							|
 |                                                                      |
 |   Remarques : uniquement pour import/export presse-papier            |
 |               fonctionnement sequentiel et texte                     |
 |                                                                      |
 +----------------------------------------------------------------------*/


// ---------------------------------------------------------------- //

#define NOM_PP_VIRTUEL "TEMP"
#define CARACTERE_META_PP '@'
#define APPLICATION_PP "@APPLICATION: "
#define DESCRIPTEUR_PP "@DESCRIPTOR: "
#define FIN_PP "@END"

#define LIGNE_PP_QUELCONQUE 0
#define LIGNE_META_PP_APPLIC 1
#define LIGNE_META_PP_DESCRIPTEUR 2
#define LIGNE_META_PP_FIN 3
#define LIGNE_PP_STANDARD 4
#define LIGNE_PP_VIERGE 5
#define LIGNE_META_PP_ANIMATION 6

typedef enum
  {
  pp_en_lecture = 0,
  pp_en_ecriture
  } t_ouverture_pp;


BOOL bOuvrir_presse_papier (char *nom_fic, t_ouverture_pp type_ouverture);

DWORD ecrire_ligne_presse_papier (char *texte);

void consulter_ligne_presse_papier (char *texte, BOOL *possible);

void vider_presse_papier (void);

void analyse_ligne_presse_papier (char *texte, DWORD *numero_desc, DWORD *type_ligne_pp);

void fermer_presse_papier (void);

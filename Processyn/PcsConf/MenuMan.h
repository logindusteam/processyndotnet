// outil Win32 d'aide � la gestion de menus pour Processyn

#ifndef   MENUMAN_H
  #define MENUMAN_H

//------------- Cr�ation de menus -----------------------
// ajout d'un message information � un menu
void MenuAppendMI (HMENU hMenu, int IdMI, UINT IdMenu);

// ajout d'un menu pop up message information � un menu
void MenuAppendPopUpMI (HMENU hMenu, int IdMI, HMENU hmenuPopUp);

// Insertion d'un menu pop up message information � un menu (insertion avant uPosItemInsert)
void MenuInsertPopUpMI (HMENU hMenu, UINT uPosItemInsert, int IdMI, HMENU hmenuPopUp);

// ajout d'un s�parateur � un menu
void MenuAppendSeparateur (HMENU hMenu);

//------------------ Etats d'un sous menu complet----------------
// Grise ou valide un sous menu du menu main de l'application (Cf Appli.h)
void MenuAppliGriseSubMenu (UINT uPosition, BOOL bGrise);

//------------------ Etats d'un item menu ----------------
// Grise ou valide un item du menu
void MenuGriseItem (HMENU hmenu, UINT uIdItem, BOOL bGrise);

// Met ou Enl�ve une coche sur un item menu
void MenuCocheItem (HMENU hmenu, UINT uIdItem, BOOL bAvecCoche);

// Grise ou valide un item du menu de l'application (Cf Appli.h)
void MenuAppliGriseItem (UINT uIdItem, BOOL bGrise);

// Met ou Enl�ve une coche sur un item du menu de l'application (Cf Appli.h)
void MenuAppliCocheItem (UINT uIdItem, BOOL bAvecCoche);

//--------------- Raccourcis clavier ---------------------
// Aide � l'insertion automatique de raccourcis $$ ??
void insere_raccourci (char *titre_desc, char *tab_caractere_raccourci);

#endif //MENUMAN_H

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgme.h                                               |
 |   Auteur  :	LM							|
 |   Date    :	18/01/93						|
 |   Remarques : gestion des boites de dialogue dans le descripteur     |
 |               memoire                                                |
 +----------------------------------------------------------------------*/

void LIS_PARAM_DLG_ME (int index_boite, UINT *iddlg, FARPROC * adr_winproc);

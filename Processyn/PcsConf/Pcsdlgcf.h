/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : pcsdlgcf.h                                                |
 |   Auteur  : LM                                                       |
 |   Date    : 01/09/92 						|
 |   Version :                                                          |
 |                                                                      |
 |   Remarques : Gestion des boites de dialogue recherche et imprime
 |								dans Pcsconf            |
 +----------------------------------------------------------------------*/



// -----------------------------------------------------------------------
// Structures renvoy�es aux boites de dialogue
// -----------------------------------------------------------------------

typedef struct
	{
	char titre[20];
	DWORD selecteur;
	void *pteur_param;
	} t_dlg_imprime;

typedef struct
	{
	char titre[20];
	char saisie[255];
	int  sens;
	void *pteur_param;
	} t_dlg_recherche;

// --------------------------------------------------------------------------

BOOL  dlg_recherche_mot (HWND hwnd, t_dlg_recherche *retour_recherche);
BOOL  dlg_imprime (HWND hwnd, t_dlg_imprime *retour_imprime);
//DWORD     INIT_PMDLGCF (void);

//------------------------------ Fonctions des T�ches Structur�es -
// Ids Fonctions threads standardis�s
#define THRD_FCT_NOP          0   //Ne rien faire !- R�serv�e -!
#define THRD_FCT_START        1   //Lancement de la t�che (param�trage)
#define THRD_FCT_CREATE       2   //Cr�ation des ressources de la t�che
#define THRD_FCT_INIT         3   //Initialisation des ressources de la t�che
#define THRD_FCT_UNINIT       4   //Terminaison des ressources de la t�che
#define THRD_FCT_DESTROY      5   //Destruction des ressources de la t�che
#define THRD_FCT_END          6   //Terminer la t�che
#define THRD_FCT_RESET        7   //Re-initialisation de la t�che
#define THRD_FCT_STOP         8   //Arret de boucle (boucle non blocante)
#define THRD_FCT_EXEC         9   //Execution du corps principal de la t�che
#define THRD_FCT_USER        10   //Execution d'une partie du code de la t�che
                                  //Idem THRD_FCT_USER+1, THRD_FCT_USER+2, ...

// ----------------------------- Niveaux de priorit� des T�ches -
#ifndef NB_PRIORITE // red�finition constantes Progman.h
// Remarque : il existe d'autres niveaux de priorit�s. Ces niveaux sont interpr�t�es
// selon la classe de priorit� du process (lui m�me soumis aux boosts de priorit�)

#define PRIORITE_IDLETIME     THREAD_PRIORITY_IDLE
#define PRIORITE_BELOW_NORMAL THREAD_PRIORITY_BELOW_NORMAL
#define PRIORITE_REGULAR      THREAD_PRIORITY_NORMAL
#define PRIORITE_TIMECRITICAL THREAD_PRIORITY_HIGHEST //THREAD_PRIORITY_TIME_CRITICAL 
#endif

DECLARE_HANDLE (HTHREAD);
typedef HTHREAD * PHTHREAD;

// type fonction thread
typedef unsigned (__stdcall * PFNTHREAD)(void *);

// --------------------------------------------------------------------------
// Fonctions � l'usage excusif des threads cr��s -
//---------------------------------------------------------------------------

// thread : sp�cifie le time out courant d'attente d'un message
void ThreadSetDureeAttenteMessage
	(HTHREAD hThread, DWORD dwTimeOut);

// thread : attente d'un message (avec time out courant)
// Renvoie TRUE si le message est autre que THRD_FCT_END, FALSE sinon
BOOL bThreadAttenteMessage
	(
	HTHREAD hThread, 
	PVOID * ppParamIn, PVOID * ppParamOut, DWORD ** ppdwError, UINT * pdwIdMessage // NULLs ou adresse pour recopie des param�tres
	);

// thread : signale au gestionnaire que le traitement du message est termin�e
void ThreadFinTraiteMessage
	(HTHREAD hThread);

// thread : signale au gestionnaire que le thread s'arr�te
// remarque : les ressources de l'objet thread ne sont lib�r�es que par bThreadFerme
UINT uThreadTermine 
	(HTHREAD hThread, 
	UINT uRetCode);// renvoie uRetCode ou -1 s'il y a une erreur

//--------------------------------------------------------
// Fonctions de pilotages de thread interdites aux threads
//--------------------------------------------------------

// cr�ation d'un objet thread et lancement de la t�che (time out attente message = 0)
// le HTHREAD cr�� est syt�matiquement pass� au thread
BOOL bThreadCree
	(PHTHREAD phThread, PFNTHREAD pfnThread, DWORD size_stack);

// Envoi du message THRD_FCT_END � la t�che et attente de la fin de la t�che,
// puis lib�ration et remise � 0 de *phThread
void bThreadFerme
	(PHTHREAD phThread, DWORD dwTimeOut);

// Envoi d'un message au thread
// (fait sortir le thread du bThreadAttenteMessage)
void ThreadEnvoiMessage
	(HTHREAD hThread, 
	UINT dwIdMessage, PVOID pParamIn, PVOID pParamOut, DWORD * pwError); // param�tres transmis au thread

// Attends au plus dwTimeOut ms que le thread ai fini de traiter un message
BOOL bThreadAttenteFinTraiteMessage
	(HTHREAD hThread, DWORD dwTimeOut);

// ThreadEnvoiMessage + bThreadAttenteFinTraiteMessage (INFINITE)
DWORD dwThreadExecuteMessage
	(HTHREAD hThread, 
	UINT IdMessage, void *pParamIn, void *pParamOut, DWORD *pwError);  // param�tres transmis au thread

//---------------------------------------------------------------------------
//  Fonctions pour tous
//---------------------------------------------------------------------------
// fixe le niveau de priorit� du thread
BOOL bThreadSetPriorite	(HTHREAD hThread, int thread_prty);

//---------------------------------------------------------------------------

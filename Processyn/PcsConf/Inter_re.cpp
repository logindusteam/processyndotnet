/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_re.hpr                                             |
 |   Auteur  : RP                                                       |
 |   Date    : 11/08/94                                                 |
 |   Version : 4.05                                                     |
 ----------------------------------------------------------------------*/

// interpreteur repete
// --------------------------
#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "cvt_ul.h"
#include "tipe_re.h"
#include "mem.h"
#include "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "IdLngLng.h"
#include "inter_re.h"
#include "Verif.h"
VerifInit;

//-------------------------- Variables ---------------------
static char contexte_re[82];
static DWORD ligne_depart_repete;
static DWORD ligne_courante_repete;
static BOOL re_deja_initialise;

//--------------------------------------------------------------------------
//                             DIVERS
//--------------------------------------------------------------------------
static void maj_pointeur_dans_geo (DWORD numero_repere,DWORD limite, int nbr_ote, BOOL spec)
  {
  PGEO_REPETE	ptr_val_geo;
  DWORD nbr_enr;
  DWORD i;

  if (!existe_repere (b_geo_re))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_re);
    }
  i = 0;
  while (i < nbr_enr)
    {
    i ++;
    ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,i);
    if (ptr_val_geo->numero_repere == numero_repere)
      {
      switch (ptr_val_geo->numero_repere)
        {
        case b_titre_paragraf_re:
          if (spec)
            {
            if (ptr_val_geo->pos_specif_paragraf >= limite)
              {
              ptr_val_geo->pos_specif_paragraf --;
              }
            }
          else
            {
            if (ptr_val_geo->pos_paragraf >= limite)
              {
              ptr_val_geo->pos_paragraf --;
              }
            }
          break;

        case b_liste:
          if (spec)
            {
            if (ptr_val_geo->pos_donnees >= limite)
              {
              ptr_val_geo->pos_donnees = ptr_val_geo->pos_donnees - (DWORD)(nbr_ote);
              }
            }
          else
            {
            if (ptr_val_geo->pos_donnees > limite)
              {
              ptr_val_geo->pos_donnees = ptr_val_geo->pos_donnees - (DWORD)(nbr_ote);
              }
            }
          break;

        default:
          break;

        }
      }
    }
  }


//----------------------------------------------------------------------
static DWORD nb_colonne_liste (DWORD ligne , REQUETE_EDIT action)
  {
  PTITRE_PARAGRAPHE titre_paragraf;
  DWORD pos_paragraf;
  PGEO_REPETE	donnees_geo;
  DWORD nbr_va;
  DWORD i;
  BOOL fin_generique;

  nbr_va = 0;
  if (bLigneDansParagraphe (b_titre_paragraf_re,ligne,structure_re,action,&pos_paragraf))
    {
    titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,pos_paragraf);
    i = titre_paragraf->pos_geo_debut;
    fin_generique = FALSE;
    while ((i < titre_paragraf->pos_geo_debut + titre_paragraf->nbr_ligne)
          && (!fin_generique))
      {
      i++;
      donnees_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,i);
      switch (donnees_geo->numero_repere)
        {
        case b_e_s :
          nbr_va++;
          break;

        case b_titre_paragraf_re :
          fin_generique = TRUE;
          break;

        default:
          break;
        }
      }
    }
  return(nbr_va);
  }

//----------------------------------------------------------------------
//renvoie l'index "nieme ligne significative decrite" (sans compter les commentaires)
static DWORD pos_geo_significative (DWORD ligne, REQUETE_EDIT action)
  {
  PTITRE_PARAGRAPHE titre_paragraf;
  DWORD pos_paragraf;
  PGEO_REPETE	donnees_geo;
  DWORD i;
  DWORD nbr_va;

  nbr_va = 0;
  if (bLigneDansParagraphe (b_titre_paragraf_re,ligne,structure_re,action,&pos_paragraf))
    {
    titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,pos_paragraf);
    i = titre_paragraf->pos_geo_debut;
    while ((i < titre_paragraf->pos_geo_debut + titre_paragraf->nbr_ligne) && (i < (DWORD)(ligne-1)))
      {
      i++;
      donnees_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,i);
      if (donnees_geo->numero_repere == b_e_s)
        {
        nbr_va++;
        }
      }
    }
  nbr_va++;
  return (nbr_va);
  }


//----------------------------------------------------------------------
static BOOL va_generique_correspondante (DWORD ligne, DWORD numero,
                                            ID_MOT_RESERVE *tipe_gen, ID_MOT_RESERVE *genre_gen,
                                            REQUETE_EDIT action)
  {
  DWORD nbr_va;
  DWORD i;
  DWORD pos_paragraf;
  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_REPETE	donnees_geo;
  BOOL trouve;
  PENTREE_SORTIE es;

  trouve = FALSE;
  if (bLigneDansParagraphe (b_titre_paragraf_re,ligne,structure_re,action,&pos_paragraf))
    {
    titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,pos_paragraf);
    i = titre_paragraf->pos_geo_debut;
    nbr_va = 0;
    while ((i < (titre_paragraf->pos_geo_debut + titre_paragraf->nbr_ligne)) && (!trouve))
      {
      i++;
      donnees_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,i);
      if (donnees_geo->numero_repere == b_e_s)
        {
        nbr_va++;
        }
      trouve = (BOOL)(nbr_va == numero);
      }
    if (trouve)
      {
      es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,donnees_geo->pos_es);
      (*genre_gen) = es->genre;
      (*tipe_gen) = es->sens;
      }
    }
  return (trouve);
  }

//--------------------------------------------------------------------------
//                             EDITION
//--------------------------------------------------------------------------

// Génération vecteur UL
static void genere_vect_ul (DWORD ligne, PUL vect_ul, int *nb_ul)
  {
  PGEO_REPETE	ptr_val_geo;
  PTITRE_PARAGRAPHE titre_paragraf;
  char *spec_titre_paragraf;
  t_liste_re *ptr_liste;
  DWORD pos_liste;
  int i;
 
  ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,ligne);
  switch (ptr_val_geo->numero_repere)
    {
    case b_pt_constant:
       (*nb_ul) = 1;
       init_vect_ul (vect_ul, (*nb_ul));
       consulte_cst_bd_c (ptr_val_geo->pos_donnees,vect_ul[0].show);
       break;

    case b_titre_paragraf_re :
     titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,ptr_val_geo->pos_paragraf);
     switch (titre_paragraf->IdParagraphe)
       {
       case structure_re :
         (*nb_ul) = 2;
         init_vect_ul (vect_ul, (*nb_ul));
         bMotReserve (c_res_structure, vect_ul[0].show);
         spec_titre_paragraf = (char *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_re,ptr_val_geo->pos_specif_paragraf);
         StrCopy (vect_ul[1].show,spec_titre_paragraf);
        break;

       case liste_re :
         (*nb_ul) = 1;
         init_vect_ul (vect_ul, (*nb_ul));
         bMotReserve (c_res_liste, vect_ul[0].show);
         break;

       default:
         break;

       }
      break;

    case b_e_s :
      (*nb_ul) = 3;
      init_vect_ul (vect_ul, (*nb_ul));
      consulte_es_bd_c (ptr_val_geo->pos_es,vect_ul[0].show, vect_ul[1].show, vect_ul[2].show);
      break;

    case b_liste:
      (*nb_ul) = (int)(nb_colonne_liste (ligne,CONSULTE_LIGNE));
      init_vect_ul (vect_ul, (*nb_ul));
      pos_liste = ptr_val_geo->pos_donnees;
      for (i = 0; i < (*nb_ul); i++)
        {
        ptr_liste = (t_liste_re *)pointe_enr (szVERIFSource, __LINE__, b_liste,pos_liste);
        switch (ptr_liste->numero_repere)
          {
          case b_e_s:
            NomVarES (vect_ul[i].show, ptr_liste->position);
            break;

          case b_pt_constant:
            consulte_cst_bd_c (ptr_liste->position,vect_ul[i].show);
            break;

          default:
            StrCopy (vect_ul[i].show, "?");
            break;

          }
        pos_liste++;
        }
      break;

    default :
      break;

    }
  }

//''''''''''''''''''''''''valide vecteur UL''''''''''''''''''''''''''''''''''

//---------------------------------------------------------------------------
static BOOL valide_vect_ul (PUL vect_ul, int nb_ul, DWORD ligne,
                               REQUETE_EDIT action, DWORD *profondeur, DWORD *numero_repere,
                               PID_PARAGRAPHE tipe_paragraf,
                               DWORD *position_ident,
                               ID_MOT_RESERVE *genre, ID_MOT_RESERVE *sens,
                               DWORD *pos_init, BOOL *exist_init,
                               DWORD *erreur)

  {
  BOOL exist_ident;
  BOOL val_log;
  BOOL un_log;
  BOOL un_num;
  BOOL trouve;
  FLOAT val_num;
  char message_reserve[c_nb_car_message_res];
  char old_sens [c_nb_car_message_res];
  char old_genre [c_nb_car_message_res];
  char old_ident [c_nb_car_es];
  PGEO_REPETE	ptr_val_geo;
  DWORD num_ul;
  ID_MOT_RESERVE n_fonction;
  DWORD nbr_imbrication;
  ID_MOT_RESERVE tipe_gen;
  ID_MOT_RESERVE genre_gen;
  DWORD pos_paragraf;
  DWORD pos_debut;
  DWORD pos_fin;
  DWORD pos_ligne;
  DWORD pos_es;
  PENTREE_SORTIE es;
  PTITRE_PARAGRAPHE titre_paragraf;


  //***********************************************
  #define err(code) (*erreur) = code;return (FALSE)  //Sale Sioux
  //***********************************************

  (*erreur) = 0;
  if (nb_ul < 1)
    {
    err(IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
    }
  if (!bReconnaitESValide ((vect_ul+0),&exist_ident,position_ident))
    {
    if (!bReconnaitMotReserve ((vect_ul+0)->show,&n_fonction))
      {
      if (!bReconnaitConstante ((vect_ul+0),&val_log,&val_num,
                             &un_log,&un_num,exist_init,pos_init))
        {
        err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if ((un_log) || (un_num))
        {
        err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      (*numero_repere) = b_pt_constant;
      }
    else
      {
      switch (n_fonction)
        {
        case c_res_structure:
         if (nb_ul != 2)
           {
           err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
           }
         if ((!bReconnaitConstante ((vect_ul+1),&val_log,&val_num,&un_log,&un_num,exist_init,pos_init)) ||
            (un_log) || (un_num) || (StrLength (vect_ul[1].show) > 20))
           {
           err (155);
           }
         (*profondeur) = 1;
         nbr_imbrication = 0;
         (*numero_repere) = b_titre_paragraf_re;
         (*tipe_paragraf) = structure_re;
         break;

        case c_res_liste:
         if (nb_ul != 1)
           {
           err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
           }
         if ((action != MODIFIE_LIGNE))
           {
           bLigneDansParagraphe (b_titre_paragraf_re,ligne,structure_re,INSERE_LIGNE,&pos_paragraf);
           if ((pos_paragraf == 0))
             {
             err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
             }
           titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,pos_paragraf);
           pos_debut = titre_paragraf->pos_geo_debut + 1;
           pos_fin = titre_paragraf->pos_geo_debut + titre_paragraf->nbr_ligne;
           pos_ligne = pos_debut - 1;
           trouve = FALSE;
           while ((pos_ligne < pos_fin) && (!trouve))
             {
             pos_ligne++;
             ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,pos_ligne);
             if (ptr_val_geo->numero_repere == b_titre_paragraf_re)
               {
               titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,ptr_val_geo->pos_paragraf);
               trouve = (BOOL)(titre_paragraf->IdParagraphe == liste_re);
               }
             }
           if (trouve)
             {
             err(201);
             }
           }
         (*profondeur) = 2;
         nbr_imbrication = 1;
         (*numero_repere) = b_titre_paragraf_re;
         (*tipe_paragraf) = liste_re;
         break;

       default:
         err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
         break;

       }
      if (!bValideInsereLigneParagraphe (b_titre_paragraf_re,ligne,
                                           (*profondeur),nbr_imbrication,erreur))
        {
        err((*erreur));
        }
      }
    }
  else
    {
    if (StrLength (vect_ul[0].show) > 20)
      {
      err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
      }
    if (bLigneDansParagraphe (b_titre_paragraf_re,ligne,liste_re,INSERE_LIGNE,&pos_paragraf))
      {
      if (action != MODIFIE_LIGNE)
        {
        if (nb_ul != (int)(nb_colonne_liste (ligne,INSERE_LIGNE)))
          {
          err (156);
          }
        }
      else
        {
        if (nb_ul != (int)(nb_colonne_liste (ligne,CONSULTE_LIGNE)))
          {
          err (156);
          }
        }
      num_ul = 0;
      while ((num_ul < (DWORD)(nb_ul)) && ((*erreur) == 0))
        {
        num_ul++;
        if (action != MODIFIE_LIGNE)
          {
          if (!va_generique_correspondante (ligne,num_ul,&tipe_gen,&genre_gen,INSERE_LIGNE))
            {
            (*erreur) = 157;
            }
          }
        else
          {
          if (!va_generique_correspondante (ligne,num_ul,&tipe_gen,&genre_gen,CONSULTE_LIGNE))
            {
            (*erreur) = 157;
            }
          }
        if ((*erreur) == 0)
          {
          switch (tipe_gen)
            {
            case c_res_variable_re:
              if (bReconnaitESExistante ((vect_ul + num_ul - 1),&pos_es))
                {
                es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pos_es);
                if (es->genre != genre_gen)
                  {
                  (*erreur) = 159;
                  }
                /* Interdiction des noms de tableaux */
                if ((es->sens == c_res_variable_ta_es) || (es->sens == c_res_variable_ta_e) ||
                   (es->sens == c_res_variable_ta_s))
                  {
                  err(198);
                  }
                }
              else
                {
                (*erreur) = 158;
                }
              break;

            case c_res_cste_re:
              if (!bReconnaitConstante ((vect_ul + num_ul - 1),&val_log,&val_num,&un_log,&un_num,exist_init,pos_init))
                {
                (*erreur) = 160;
                }
              if (((genre_gen == c_res_logique) && (!un_log)) ||
                 ((genre_gen == c_res_numerique) && (!un_num)) ||
                 ((genre_gen == c_res_message) && (un_num || un_log)))
                {
                (*erreur) = 159;
                }
              break;

            case c_res_tab_re:
              break;

            default:
              break;
            }
          }
        }
      if ((*erreur) != 0)
        {
        err((*erreur));
        }
      (*numero_repere) = b_liste;
      }
    else
      {
      if (nb_ul != 3)
        {
        err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if (!bReconnaitMotReserve ((vect_ul+1)->show,sens))
        {
        err (196);
        }
      if (((*sens) != c_res_cste_re) && ((*sens) != c_res_variable_re) && ((*sens) != c_res_tab_re))
        {
        err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if ((pos_geo_significative (ligne,INSERE_LIGNE) == 1) && ((*sens) == c_res_cste_re))
        {
        err (202);
        }
      if (!bReconnaitMotReserve ((vect_ul+2)->show,genre))
        {
        err (197);
        }
      if (((*genre) != c_res_logique) && ((*genre) != c_res_numerique) && ((*genre) != c_res_message))
        {
        err (198);
        }
      if (action != MODIFIE_LIGNE)
        {
        if (exist_ident)
          {
          err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
          }
        }
      else
        {
        ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,ligne);
        if (ptr_val_geo->numero_repere == b_e_s)
          {
          consulte_es_bd_c (ptr_val_geo->pos_es,old_ident,old_sens,old_genre);
          if ((exist_ident) && (!bStrEgales (old_ident, vect_ul[0].show)))
            {
            err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
            }
          bMotReserve ((*genre), message_reserve);
          if (!bStrEgales (message_reserve, old_genre))
            {
            err (199);
            }
          message_reserve[0] = '\0';
          bMotReserve ((*sens), message_reserve);
          if (!bStrEgales (message_reserve, old_sens))
            {
            err (200);
            }
          }
        else
          {
          if (exist_ident)
            {
            err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
            }
          }
        }
      (*numero_repere) = b_e_s;
      }
    }
  return ((BOOL)((*erreur) == 0));
  }

//''''''''''''''''''''''''''''''valide ligne''''''''''''''''''''''''''''''''''''
void  valide_ligne_repete (REQUETE_EDIT action, DWORD ligne,
                                PUL vect_ul, int *nb_ul,
                                BOOL *supprime_ligne, BOOL *accepte_ds_bd,
                                DWORD *erreur_val, int *niveau, int *indentation)
  {
  BOOL booleen;
  BOOL un_log;
  BOOL un_num;
  BOOL trouve;
  BOOL val_log;
  BOOL toute_seule;
  BOOL un_paragraphe;
  BOOL gen_suivant_variable;
  FLOAT reel;
  FLOAT val_num;
  GEO_REPETE val_geo;
  PGEO_REPETE	ptr_val_geo;
  ENTREE_SORTIE es;
  PENTREE_SORTIE	ptr_es;
  PTITRE_PARAGRAPHE titre_paragraf;
  PTITRE_PARAGRAPHE pTitreParagraphe;
  char *ptr_spec_titre_paragraf; //ts20
  DWORD mot;
  t_liste_re *ptr_liste;
  t_liste_re val_liste;
  DWORD erreur;
  DWORD numero_repere;
  DWORD pos_paragraf;
  DWORD profondeur = 1;
  DWORD pos_colonne_insertion;
  DWORD pos_a_enlever;
  DWORD pos_colonne;
  DWORD pos_enr;
  DWORD pos_init;
  DWORD position_ident;
  DWORD pos_geo_liste;
  DWORD pos_fin_liste;
  DWORD nbr_colonne_liste;
  DWORD pos_cour;
  DWORD pos_debut;
  DWORD pos_fin;
  ID_MOT_RESERVE tipe_gen;
  ID_MOT_RESERVE genre_gen;
  DWORD pos_ligne;
  DWORD pos_es;
  BOOL exist_init;
  BOOL exist_ident;
  UL v_ul;
  ID_PARAGRAPHE tipe_paragraf;
  DWORD num_ul;
  DWORD i;
  DWORD index;

 
  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;

  switch (action)
    {
    case MODIFIE_LIGNE  :
      if (valide_vect_ul(vect_ul,(*nb_ul),ligne,MODIFIE_LIGNE,&profondeur,&numero_repere,
                         &tipe_paragraf,&es.i_identif,&es.genre,&es.sens,&pos_init,
                         &exist_init,&erreur))
        {
        ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,ligne);
        val_geo = (*ptr_val_geo);
        if (val_geo.numero_repere == numero_repere)
          {
          switch (numero_repere)
            {
            case b_liste :
              for (num_ul = 0; num_ul < (DWORD)(*nb_ul); num_ul++)
                {
                va_generique_correspondante (ligne, (DWORD)(num_ul+1),&tipe_gen,&genre_gen,CONSULTE_LIGNE);
                ptr_liste = (t_liste_re *)pointe_enr (szVERIFSource, __LINE__, b_liste,val_geo.pos_donnees+num_ul);
                val_liste = (*ptr_liste);
                switch (tipe_gen)
                  {
                  case c_res_variable_re:
                    bReconnaitESValide((vect_ul+num_ul),&exist_ident,&position_ident);
                    pos_es = dwIdVarESFromPosIdentif(position_ident);
                    if (val_liste.numero_repere == b_e_s)
                      {
                      if (val_liste.position != pos_es)
                        {
                        DeReferenceES (val_liste.position);
                        ReferenceES (pos_es);
                        val_liste.position = pos_es;
                        }
                      }
                    else
                      {
                      val_liste.numero_repere = b_e_s;
                      ReferenceES (pos_es);
                      val_liste.position = pos_es;
                      }
                    break;

                  case c_res_cste_re:
                    if (val_liste.numero_repere == b_pt_constant)
                      {
                      mot = val_liste.position;
                      modifie_cst_bd_c (vect_ul[num_ul].show,&mot);
                      val_liste.position = mot;
                      }
                    else
                      {
                      val_liste.numero_repere = b_pt_constant;
                      bReconnaitConstante ((vect_ul+num_ul),&val_log,&val_num,&un_log,&un_num,&exist_init,&pos_init);
                      AjouteConstanteBd (pos_init,exist_init,vect_ul[num_ul].show,&mot);
                      val_liste.position = mot;
                      }
                    break;

                  case c_res_tab_re:
                    break;

                  default:
                    break;

                  }
                ptr_liste = (t_liste_re *)pointe_enr (szVERIFSource, __LINE__, b_liste,val_geo.pos_donnees+num_ul);
                (*ptr_liste) = val_liste;
                }
              break;

            case b_e_s :
              if (!bModifieDeclarationES (&es,vect_ul[0].show,val_geo.pos_es))
                {
                erreur = 170;
                }
              break;

            case b_pt_constant :
              modifie_cst_bd_c (vect_ul[0].show,&val_geo.pos_donnees);
              break;

            case b_titre_paragraf_re:
              titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,val_geo.pos_paragraf);
              if (tipe_paragraf == titre_paragraf->IdParagraphe)
                {
                if (titre_paragraf->IdParagraphe == structure_re)
                  {
                  ptr_spec_titre_paragraf = (char *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_re,val_geo.pos_specif_paragraf);
                  StrCopy(ptr_spec_titre_paragraf,vect_ul[1].show);
                  }
                }
              else
                {
                erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
                }
              break;

            default :
              break;

            }
          }
        else
          {
          erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
          }
        }
      if (erreur != 0)
        {
        genere_vect_ul (ligne,vect_ul,nb_ul);
        (*erreur_val) = erreur;
        }
      else
        {
        ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,ligne);
        (*ptr_val_geo) = val_geo;
        }
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne)=FALSE;
      break;

    case INSERE_LIGNE   :
      if (valide_vect_ul(vect_ul,(*nb_ul),ligne,INSERE_LIGNE,&profondeur,&numero_repere,
                        &tipe_paragraf,&es.i_identif,&es.genre,&es.sens,&pos_init,
                        &exist_init,&erreur))
        {
        pos_paragraf = 0;
        val_geo.numero_repere = numero_repere;
        switch (numero_repere)
          {
          case b_pt_constant :
            AjouteConstanteBd (pos_init,exist_init,vect_ul[0].show,&val_geo.pos_donnees);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            break;

          case b_e_s :
            es.n_desc = d_repete;
            es.taille = 0;
            InsereVarES (&es,vect_ul[0].show,&val_geo.pos_es);
            switch (es.genre)
              {
              case c_res_logique :
                bMotReserve (c_res_zero,v_ul.show);
                v_ul.genre = g_id;
                break;

              case c_res_numerique :
                StrCopy (v_ul.show, "0");
                v_ul.genre = g_num;
                break;

              case c_res_message :
                StrSetNull (v_ul.show);
                v_ul.genre = g_id;
                break;

              default:
                break;
              }
            bReconnaitConstante (&v_ul,&booleen,&reel,&un_log,&un_num,&exist_init,&pos_init);
            AjouteConstanteBd (pos_init,exist_init,v_ul.show,&val_geo.pos_initial);
            bLigneDansParagraphe (b_titre_paragraf_re,ligne,structure_re,action,&mot);
            titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,mot);
            pos_geo_liste = titre_paragraf->pos_geo_debut;
            trouve = FALSE;
            while ((pos_geo_liste < titre_paragraf->pos_geo_debut + titre_paragraf->nbr_ligne) && (!trouve))
              {
              pos_geo_liste++;
              ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,pos_geo_liste);
              if (ptr_val_geo->numero_repere == b_titre_paragraf_re)
                {
                pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,ptr_val_geo->pos_paragraf);
                trouve = (BOOL)(pTitreParagraphe->IdParagraphe == liste_re);
                }
              }
            if (trouve)
              {
              pos_fin_liste = pTitreParagraphe->pos_geo_debut + pTitreParagraphe->nbr_ligne;
              }
            else
              {
              pos_fin_liste = 0;
              }
            pos_colonne_insertion = pos_geo_significative (ligne,INSERE_LIGNE);
            for (i = (pos_geo_liste + 1); i <= pos_fin_liste; i++)
              {
              ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,i);
              if (ptr_val_geo->numero_repere == b_liste)
                {
                pos_enr = pos_colonne_insertion + ptr_val_geo->pos_donnees - 1;
                insere_enr (szVERIFSource, __LINE__, 1,b_liste,pos_enr);
                ptr_liste = (t_liste_re *)pointe_enr (szVERIFSource, __LINE__, b_liste,pos_enr);
                ptr_liste->numero_repere = MAXWORD;
                if (pos_colonne_insertion != 1)
                  {
                  maj_pointeur_dans_geo (b_liste,pos_enr,-1,TRUE);
                  }
                else
                  {
                  maj_pointeur_dans_geo (b_liste,pos_enr,-1,FALSE);
                  }
                }
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            break;

          case b_liste:
            if (!existe_repere (b_liste))
              {
              val_geo.pos_donnees = 1;
              cree_bloc (b_liste,sizeof (*ptr_liste),(DWORD)(*nb_ul));
              }
            else
              {
              val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, b_liste) + 1;
              insere_enr (szVERIFSource, __LINE__, (DWORD)(*nb_ul),b_liste,val_geo.pos_donnees);
              }
            for (num_ul = 0; num_ul < (DWORD)(*nb_ul); num_ul++)
              {
              va_generique_correspondante (ligne, (DWORD)(num_ul+1),&tipe_gen,&genre_gen,INSERE_LIGNE);
              ptr_liste = (t_liste_re *)pointe_enr (szVERIFSource, __LINE__, b_liste,val_geo.pos_donnees+num_ul);
              switch (tipe_gen)
                {
                case c_res_variable_re:
                  ptr_liste->numero_repere = b_e_s;
                  bReconnaitESValide((vect_ul+num_ul),&exist_ident,&position_ident);
                  pos_es = dwIdVarESFromPosIdentif(position_ident);
                  ReferenceES (pos_es);
                  ptr_liste->position = pos_es;
                  break;

                case c_res_cste_re:
                  ptr_liste->numero_repere = b_pt_constant;
                  bReconnaitConstante ((vect_ul+num_ul),&val_log,&val_num,&un_log,&un_num,&exist_init,&pos_init);
                  AjouteConstanteBd (pos_init,exist_init,vect_ul[num_ul].show,&mot);
                  ptr_liste = (t_liste_re *)pointe_enr (szVERIFSource, __LINE__, b_liste,val_geo.pos_donnees+num_ul);
                  ptr_liste->position = mot;
                  break;

                case c_res_tab_re:
                   break;

                default:
                  break;
                }
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            break;

          case b_titre_paragraf_re :
            InsereTitreParagraphe (b_titre_paragraf_re,ligne,profondeur,tipe_paragraf,&val_geo.pos_paragraf);
            pos_paragraf = val_geo.pos_paragraf;
            if (tipe_paragraf == structure_re)
              {
              if (!existe_repere (b_spec_titre_paragraf_re))
                {
                val_geo.pos_specif_paragraf = 1;
                cree_bloc (b_spec_titre_paragraf_re,Taille_Bloc_Titre_Paragraf,0);
                }
              else
                {
                val_geo.pos_specif_paragraf = nb_enregistrements (szVERIFSource, __LINE__, b_spec_titre_paragraf_re) + 1;
                }
              insere_enr (szVERIFSource, __LINE__, 1,b_spec_titre_paragraf_re,val_geo.pos_specif_paragraf);
              ptr_spec_titre_paragraf = (char *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_re,val_geo.pos_specif_paragraf);
              StrCopy(ptr_spec_titre_paragraf,vect_ul[1].show);
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            break;

          default:
            break;

          }
        if (numero_repere != 0)
          {
          if (!existe_repere (b_geo_re))
            {
            cree_bloc (b_geo_re,sizeof (val_geo),0);
            }
          insere_enr (szVERIFSource, __LINE__, 1,b_geo_re,ligne);
          ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,ligne);
          (*ptr_val_geo) = val_geo;
          un_paragraphe = (BOOL)(numero_repere == b_titre_paragraf_re);
          MajDonneesParagraphe (b_titre_paragraf_re,ligne,INSERE_LIGNE,
                                un_paragraphe,profondeur,pos_paragraf);
          }
        }
      else
        {
        (*accepte_ds_bd) = FALSE;
        (*erreur_val) = erreur;
        }
      break;

    case SUPPRIME_LIGNE :
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,ligne);
      val_geo = (*ptr_val_geo);
      switch (val_geo.numero_repere)
        {
        case b_pt_constant :
            supprime_cst_bd_c (val_geo.pos_donnees);
            break;

        case b_liste:
            nbr_colonne_liste = nb_colonne_liste (ligne,CONSULTE_LIGNE);
            for (index = val_geo.pos_donnees; index <= (val_geo.pos_donnees+nbr_colonne_liste-1); index++)
              {
              ptr_liste = (t_liste_re *)pointe_enr (szVERIFSource, __LINE__, b_liste,index);
              switch (ptr_liste->numero_repere)
                {
                case b_e_s:
                  DeReferenceES (ptr_liste->position);
                  break;

                case b_pt_constant:
                  supprime_cst_bd_c(ptr_liste->position);
                  break;

                default:
                  break;

                }
              }
            enleve_enr (szVERIFSource, __LINE__, nbr_colonne_liste,b_liste,val_geo.pos_donnees);
            maj_pointeur_dans_geo (b_liste,val_geo.pos_donnees,(int)(nbr_colonne_liste),TRUE);
            break;

        case b_e_s :
          pos_colonne = pos_geo_significative (ligne,CONSULTE_LIGNE);
          bLigneDansParagraphe (b_titre_paragraf_re,ligne,structure_re,CONSULTE_LIGNE,&pos_paragraf);
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,pos_paragraf);
          pos_debut = titre_paragraf->pos_geo_debut + 1;
          pos_fin = titre_paragraf->pos_geo_debut + titre_paragraf->nbr_ligne;
          if (pos_colonne == 1)
            {
            pos_ligne = pos_debut - 1;
            pos_cour = 0;
            while ((pos_ligne < pos_fin) && (pos_cour < 2))
              {
              pos_ligne++;
              ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,pos_ligne);
              if (ptr_val_geo->numero_repere == b_e_s)
                {
                pos_cour++;
                }
              }
            if (pos_cour == 2)
              {
              ptr_es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,ptr_val_geo->pos_es);
              gen_suivant_variable = (BOOL)(ptr_es->sens == c_res_variable_re);
              toute_seule = FALSE;
              }
            else
              {
              toute_seule = TRUE;
              gen_suivant_variable = TRUE;
              }
            }
          else
            {
            gen_suivant_variable = TRUE;
            }
          if (gen_suivant_variable)
            {
            if (!bSupprimeES (val_geo.pos_es))
              {
              (*supprime_ligne) = FALSE;
              (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
              }
            else
              {
              supprime_cst_bd_c (val_geo.pos_initial);
              for (i = pos_debut; i <= pos_fin; i++)
                {
                ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,i);
                if (ptr_val_geo->numero_repere == b_liste)
                  {
                  pos_a_enlever = ptr_val_geo->pos_donnees + pos_colonne - 1;
                  ptr_liste = (t_liste_re *)pointe_enr (szVERIFSource, __LINE__, b_liste,pos_a_enlever);

                  switch (ptr_liste->numero_repere)
                    {
                    case b_e_s:
                      DeReferenceES (ptr_liste->position);
                      break;

                    case b_pt_constant:
                      supprime_cst_bd_c(ptr_liste->position);
                      break;

                    case MAXWORD:
                      break;

                    default:
                      break;

                    }
                  enleve_enr (szVERIFSource, __LINE__, 1,b_liste,pos_a_enlever);
                  if (pos_colonne != 1)
                    {
                    maj_pointeur_dans_geo (b_liste,pos_a_enlever,1,TRUE);
                    }
                  else
                    {
                    maj_pointeur_dans_geo (b_liste,pos_a_enlever,1,FALSE);
                    }
                  }
                }
              for (i = pos_fin; i >= pos_debut; i--)
                {
                ptr_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,i);
                if (ptr_val_geo->numero_repere == b_liste)
                  {
                  if ((pos_colonne == 1) && (toute_seule))
                    {
                    MajDonneesParagraphe (b_titre_paragraf_re,i,SUPPRIME_LIGNE,
											0, profondeur,pos_paragraf); // un_paragraphe,profondeur,pos_paragraf);
                    enleve_enr (szVERIFSource, __LINE__, 1,b_geo_re,i);
                    }
                  }
                }
              }
            }
          else
            {
            (*supprime_ligne) = FALSE;
            (*erreur_val) = 202;
            }
          break;

        case b_titre_paragraf_re:
          un_paragraphe = TRUE;
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,val_geo.pos_paragraf);
          profondeur = titre_paragraf->profondeur;
          tipe_paragraf = titre_paragraf->IdParagraphe;
          if (!bSupprimeTitreParagraphe (b_titre_paragraf_re,val_geo.pos_paragraf))
            {
            (*supprime_ligne) = FALSE;
            (*erreur_val) = IDSERR_SUPPRESSION_DU_PARAGRAPHE_IMPOSSIBLE_A_CETTE_POSITION;
            }
          else
            {
            maj_pointeur_dans_geo (b_titre_paragraf_re,val_geo.pos_paragraf,1,FALSE);
            if (tipe_paragraf == structure_re)
              {
              enleve_enr (szVERIFSource, __LINE__, 1,b_spec_titre_paragraf_re,val_geo.pos_specif_paragraf);
              maj_pointeur_dans_geo(b_titre_paragraf_re,val_geo.pos_specif_paragraf,1,TRUE);
              }
            }
          break;

        default:
          break;
        }
      if ((*supprime_ligne))
        {
        MajDonneesParagraphe (b_titre_paragraf_re,ligne,SUPPRIME_LIGNE,
                                un_paragraphe,profondeur,pos_paragraf);
        enleve_enr (szVERIFSource, __LINE__, 1,b_geo_re,ligne);
        }
      break;

    case CONSULTE_LIGNE :
      genere_vect_ul (ligne,vect_ul,nb_ul);
      break;

    default :
      break;

    }
  }

// ---------------------------------------------------------------------------
void  init_repete (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  if (!re_deja_initialise)
    {
    ligne_depart_repete = 1;
    ligne_courante_repete = ligne_depart_repete;
    re_deja_initialise=TRUE;
    }
  (*depart) = ligne_depart_repete;
  (*courant) = ligne_courante_repete;
  }

// ---------------------------------------------------------------------------
DWORD  lis_nombre_de_ligne_repete (void)
  {
  DWORD  nombre_de_ligne_repete;

  if (existe_repere (b_geo_re))
    {
    nombre_de_ligne_repete = nb_enregistrements (szVERIFSource, __LINE__, b_geo_re);
    }
  else
    {
    nombre_de_ligne_repete = 0;
    }
  return (nombre_de_ligne_repete);
  }


/*-------------------------------------------------------------------------*/
/*                        GESTION CONTEXTE                                 */
/*-------------------------------------------------------------------------*/
static void consulte_titre_paragraf_re (DWORD ligne)
  {
  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_REPETE	donnees_geo;
  char *spec_titre_paragraf;
  CONTEXTE_PARAGRAPHE_LIGNE   table_contexte_ligne [2];
  DWORD profondeur_ligne;
  DWORD i;

  if (bDonneContexteParagrapheLigne (b_titre_paragraf_re, ligne, &profondeur_ligne, table_contexte_ligne))
    {
    for (i = 0; i < profondeur_ligne; i++)
      {
      switch (table_contexte_ligne [i].IdParagraphe)
        {
        case structure_re:
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re, table_contexte_ligne [i]. position);
          donnees_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re, titre_paragraf->pos_geo_debut);
          spec_titre_paragraf = (char *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_re,donnees_geo->pos_specif_paragraf);
          StrCopy(contexte_re,spec_titre_paragraf);
          break;

        case liste_re:
          break;

        default:
          StrSetNull (contexte_re);
          break;

        }
      }
    }
  else
    {
    StrSetNull (contexte_re);
    }
  }





// ---------------------------------------------------------------------------
void  set_tdb_repete (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_res[c_nb_car_message_res];

  StrSetNull (tdb);
  if ((existe_repere (b_titre_paragraf_re)) && (nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf_re) != 0))
    {
    if (nouvelle_ligne)
      {
      ligne --;
      }
    consulte_titre_paragraf_re (ligne);
    //tdb_curseur_droite (tdb); ????
    StrConcatChar (tdb, ' ');
    bMotReserve (c_res_structure, mot_res);
    StrConcat (tdb, mot_res);
    StrConcatChar (tdb, ' ');
    StrConcat (tdb, contexte_re);
    }
  }
// ---------------------------------------------------------------------------
void  fin_repete (BOOL quitte, DWORD courant)
  {
  if (!quitte)
    {
    ligne_courante_repete = courant;
    }
  }
// ---------------------------------------------------------------------------
void  creer_repete (void)
  {
  re_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
// Interface du descripteur de listes
void LisDescripteurRe (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_repete;
	pDescripteurCf->pfnInitDesc = init_repete;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_repete;
	pDescripteurCf->pfnValideLigne = valide_ligne_repete;
	pDescripteurCf->pfnSetTdb = set_tdb_repete;
	pDescripteurCf->pfnFin = fin_repete;
	}

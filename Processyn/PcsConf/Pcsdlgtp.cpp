/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgtp.c                                               |
 |   Auteur  :	LM							|
 |   Date    :	20/01/93						|
 |   Remarques : gestion des boites de dialogue dans le descripteur     |
 |               chrono                                                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "appli.h"
#include "MemMan.h"
#include "CheckMan.h"
#include "UStr.h"
#include "UEnv.h"
#include "IdConfLng.h"
#include "ActionCf.h"
#include "lng_res.h"
#include "Tipe.h"
#include "Descripteur.h"
#include "pcsdlgtp.h"

typedef struct
	{
	t_dlg_desc echange_data;
	DWORD index_type;
	char nom_var[80];
	char duree[80];
	} t_param_temps;

static char mot_res[40];
static char tab_champs[6][STR_MAX_CHAR_ARRAY];
static DWORD nb_champs;
static char buffer[30];
static LONG  index;


// -----------------------------------------------------------------------
// Nom......:
// Objet....: permet de griser les textes et bloquer les entry_field
// Entr�es..:
// Sorties..:
// Retour...:
// -----------------------------------------------------------------------
static void normal_ou_grise (HWND hwnd, BOOL bool1, BOOL bool2, BOOL bool3, BOOL bool4)
  {
  ::EnableWindow(::GetDlgItem(hwnd, ID_TP1_TEXTE1), bool1);
  ::EnableWindow(::GetDlgItem(hwnd, ID_TP1_NOM_VAR), bool2);
  ::EnableWindow(::GetDlgItem(hwnd, ID_TP1_TEXTE2), bool3);
  ::EnableWindow(::GetDlgItem(hwnd, ID_TP1_DUREE), bool4);
  }

// -----------------------------------------------------------------------
// Nom......:
// Objet....: fonctions relatives a la wndproc tp1
// Entr�es..: HWND hwnd
// Sorties..:
// Retour...:
// Remarques:
// -----------------------------------------------------------------------
void retablir_temps (HWND hwnd, t_param_temps *param_temps) /* parametres sauvegardes */
  {
  switch (param_temps->index_type)
    {
    case 1: /* CHRONOMETRE */
      normal_ou_grise (hwnd, FALSE, FALSE, FALSE, FALSE);
      StrSetNull (param_temps->nom_var);
      StrSetNull (param_temps->duree);
      SendDlgItemMessage (hwnd, ID_TP1_CHRONO, BM_SETCHECK, TRUE, 0);
    break;

    case 2: /* METRONOME */
      normal_ou_grise (hwnd, FALSE, FALSE, FALSE, FALSE);
      StrSetNull (param_temps->nom_var);
      StrSetNull (param_temps->duree);
      SendDlgItemMessage (hwnd, ID_TP1_METRO, BM_SETCHECK, TRUE, 0);
    break;

    case 3: /* variable chrono */
      normal_ou_grise (hwnd, TRUE, TRUE, FALSE, FALSE);
      StrSetNull (param_temps->duree);
      SendDlgItemMessage (hwnd, ID_TP1_VA_CHRONO, BM_SETCHECK, TRUE, 0);
    break;

    case 4: /* variable metro */
      normal_ou_grise (hwnd, TRUE, TRUE, TRUE, TRUE);
      SendDlgItemMessage (hwnd, ID_TP1_VA_METRO, BM_SETCHECK, TRUE, 0);
    break;

    default:
      break;

    } /* switch */

  SetDlgItemText (hwnd, ID_TP1_NOM_VAR, param_temps->nom_var);
  SetDlgItemText (hwnd, ID_TP1_DUREE, param_temps->duree);
  }

// -----------------------------------------------------------------------
void defaut_temps (HWND hwnd, t_param_temps *param_temps,
                   BOOL init) /* parametres par defaut */
  {
  if (init)
    {
    param_temps->index_type = 1;
    StrSetNull (param_temps->nom_var);
    StrSetNull (param_temps->duree);
    }
  SetDlgItemText (hwnd, ID_TP1_NOM_VAR, "");
  SetDlgItemText (hwnd, ID_TP1_DUREE, "");
  SendDlgItemMessage (hwnd, ID_TP1_CHRONO, BM_SETCHECK, TRUE, 0);
  normal_ou_grise (hwnd, FALSE, FALSE, FALSE, FALSE);
  }

// -----------------------------------------------------------------------
void fabrication_ul_temps (HWND hwnd, char *chaine,
                             t_param_temps *param_temps)

  {
  /* FABRICATION de la CHAINE � partir des champs de la boite */
  StrSetNull (chaine);

  /* r�cup�ration du type */
  index = nCheckIndex (hwnd, ID_TP1_CHRONO); //$$SendDlgItemMessage (hwnd, ID_TP1_CHRONO,BM_QUERYCHECKINDEX, 0, 0);
  param_temps->index_type = (DWORD)index;
  switch(index)
    {
    case 1: /* CHRONO */
      //(*param_temps->echange_data.ads_proc_message_mot_reserve) (c_res_chronometre, mot_res);
      bMotReserve (c_res_chronometre, mot_res);
      StrConcat (chaine, mot_res);
      break;

    case 2: /* METRO */
      //(*param_temps->echange_data.ads_proc_message_mot_reserve) (c_res_metronome, mot_res);
      bMotReserve (c_res_metronome, mot_res);
      StrConcat (chaine, mot_res);
      break;

    case 3: /* variable chrono */
      /* r�cup�ration du nom de la variable */
      GetDlgItemText (hwnd, ID_TP1_NOM_VAR, buffer, 30);
      StrCopy (param_temps->nom_var, buffer);
      StrConcat (chaine, buffer);
      break;

    case 4: /* variable metro */
      /* r�cup�ration du nom de la variable */
      GetDlgItemText (hwnd, ID_TP1_NOM_VAR, buffer, 30);
      StrCopy (param_temps->nom_var, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      GetDlgItemText (hwnd, ID_TP1_DUREE, buffer, 30);
      StrCopy (param_temps->duree, buffer);
      StrConcat (chaine, buffer);
      break;

    default:
      break;
    }

  /* TRACE */
  //SetDlgItemText(hwnd, ID_TRACE, chaine);
  // fin traces
  }

// -----------------------------------------------------------------------
// Nom......: fnwp_tp1
// Objet....: boite temps
// Entr�es..: HWND     hwnd
//	      USHORT   msg
//	      WPARAM   mp1
//	      LPARAM   mp2
// Sorties..:
// Retour...: LRESULT: r�sultat du traitement du message
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgTemps (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  mres = TRUE;			   // Valeur de retour
  BOOL  commande_a_executer = FALSE;
  t_param_temps *pctxt;
  t_param_action  param_action;
  t_retour_action retour_action;

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc *pechange_data = (t_dlg_desc *) mp2;

			// allocation de memoire pour le pteur sur la structure
			pctxt = (t_param_temps  *)pCreeEnv (hwnd, sizeof (t_param_temps));
			pctxt->echange_data = (*pechange_data);

      /* initialisation des champs de la boite */
      normal_ou_grise (hwnd, FALSE, FALSE, FALSE, FALSE);
      if (StrIsNull (pctxt->echange_data.chaine))
        { /* pas double clique : on veut inserer */
        defaut_temps (hwnd, pctxt, TRUE);
        }
      else
        { /* double clique : on veut modifier, il faut r�afficher la ligne */
        StrDeleteFirstSpaces (pctxt->echange_data.chaine);
        nb_champs = StrToStrArray (tab_champs, 6, pctxt->echange_data.chaine);
        if (tab_champs[0][0] != '"')
          {
          //(*pctxt->echange_data.ads_proc_message_mot_reserve) (c_res_chronometre, mot_res);
          bMotReserve (c_res_chronometre, mot_res);
          if (bStrEgales (tab_champs[0], mot_res))
            { // CHRONO
            pctxt->index_type = 1;
            }
          else
            {
            //(*pctxt->echange_data.ads_proc_message_mot_reserve) (c_res_metronome, mot_res);
            bMotReserve (c_res_metronome, mot_res);
            if (bStrEgales (tab_champs[0], mot_res))
              { // METRO
              pctxt->index_type = 2;
              }
            else
              { // variable
              StrCopy (pctxt->nom_var, tab_champs[0]);
              if (nb_champs > 1)
                { // variable metronome
                pctxt->index_type = 4;
                StrCopy (pctxt->duree, tab_champs[1]);
                }
              else
                { // variable chrono
                pctxt->index_type = 3;
                }
              }
            }
          }
        else // ligne commentaire
          {
          defaut_temps (hwnd, pctxt, TRUE);
          }
        retablir_temps(hwnd, pctxt);
        }
      mres = (LRESULT)FALSE;
			}
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      pctxt = (t_param_temps  *)pGetEnv (hwnd);
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_TP1_INSERER:
          pctxt = (t_param_temps  *)pGetEnv (hwnd);
          pctxt->echange_data.n_ligne_depart++;
          param_action.entier = pctxt->echange_data.n_ligne_depart;
          bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);
          fabrication_ul_temps (hwnd, param_action.chaine, pctxt);
          param_action.entier = pctxt->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_TP1_MODIFIER:
          pctxt = (t_param_temps  *)pGetEnv (hwnd);
          fabrication_ul_temps (hwnd, param_action.chaine, pctxt);
          param_action.entier = pctxt->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_TP1_DEFAUT:
          pctxt = (t_param_temps  *)pGetEnv (hwnd);
          defaut_temps(hwnd, pctxt, FALSE);
          break;

        case ID_TP1_RETABLIR:
          pctxt = (t_param_temps  *)pGetEnv (hwnd);
          retablir_temps(hwnd, pctxt);
          break;

				case ID_TP1_FERMER:
          pctxt = (t_param_temps  *)pGetEnv (hwnd);
          EndDialog(hwnd, FALSE);
          break;

				case IDCANCEL:
					pctxt = (t_param_temps  *)pGetEnv (hwnd);
					EndDialog(hwnd, FALSE);
					break;

	default:
      if( HIWORD( mp1 ) == BN_CLICKED )
        switch( (USHORT)( mp1 ) )
          {
          case ID_TP1_CHRONO: /* CHRONOMETRE */
          case ID_TP1_METRO:  /* METRONOME */
          normal_ou_grise (hwnd, FALSE, FALSE, FALSE, FALSE);
          break;

          case ID_TP1_VA_CHRONO: /* variable chrono */
          normal_ou_grise (hwnd, TRUE, TRUE, FALSE, FALSE);
          break;

          case ID_TP1_VA_METRO: /* variable metro */
          normal_ou_grise (hwnd, TRUE, TRUE, TRUE, TRUE);
          break;
          }
          break;
        }

			if (commande_a_executer)
				{
				pctxt = (t_param_temps  *)pGetEnv (hwnd);
				bExecuteActionCf (&retour_action);
				pctxt->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
				StrCopy (pctxt->echange_data.chaine, retour_action.chaine);
				}
      break;

    default:
      mres = FALSE;
      break;
    }

  return (mres);
  }


/* -------------------------------------------------------------------
fonction qui r�cup�re l'identificateur et l'adresse de la Window Proc
de la bo�te a afficher. En entr�e, on passe le num�ro de la bo�te parmi
les bo�tes du descripteur.
--------------------------------------------------------------------*/
void LIS_PARAM_DLG_TP (int index_boite, UINT *iddlg, FARPROC * adr_winproc)
  {
	*iddlg = DLG_INTERPRETE_TP1;
  (*adr_winproc) = (FARPROC) dlgTemps;
  }


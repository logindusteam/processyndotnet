/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_re.h                                               |
 |   Auteur  : RP                                                       |
 |   Date    : 11/08/94                                                 |
 |   Version : 4.05                                                     |
 +----------------------------------------------------------------------*/

// Interface du descripteur de listes
void LisDescripteurRe (PDESCRIPTEUR_CF pDescripteurCf);

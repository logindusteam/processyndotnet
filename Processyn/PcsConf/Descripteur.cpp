// Descripteur.cpp: implementation of the Descripteur class.
// Classe virtuelle vide.
// NON UTILISEE pour l'instant
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDescripteur::CDescripteur()
	{
	
	}

CDescripteur::~CDescripteur()
	{
	
	}

BOOL CDescripteur::bExiste() // TRUE si descripteur existe 
	{
	return FALSE;
	}

void CDescripteur::Cree (void)
	{
	}

void CDescripteur::InitDesc (DWORD *depart, DWORD *courant, char *nom_applic)
	{
	}

DWORD CDescripteur::LisNbLignes (void)
	{
	return 0;
	}

void CDescripteur::ValideLigne (REQUETE_EDIT action, DWORD ligne, PUL vect_ul, int *nb_ul,
	BOOL *supprime_ligne, BOOL *accepte_ds_bd, DWORD *erreur_val, int *niveau, int *indentation)
	{
	}

void CDescripteur::SetTdb (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
	{
	}

void CDescripteur::LisParamDlg (int index_boite, UINT *iddlg, FARPROC * adr_winproc)
	{
	}

void CDescripteur::Fin (BOOL quitte, DWORD courant)
	{
	}

void CDescripteur::Satellite (BOOL *bExecutionSynchrone, char *nom, char *param, char *env, char *chemin)
	{
	}

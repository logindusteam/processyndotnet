/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : TIPE_SY.H                                                |
 |   Auteur  : AC                                                       |
 +----------------------------------------------------------------------*/

typedef struct
    {
    DWORD pos_es;
    DWORD pos_init;
    DWORD taille_tableau;
    } GEO_SYST, *PGEO_SYST;

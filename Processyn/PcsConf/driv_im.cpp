// --------------------------------------------------------------------------
// driv_im.c Gestion de "Line Printer" sortie sur un port serie ou // sans formatage
// Version WIN32 25/7/97
// --------------------------------------------------------------------------
#include "stdafx.h"
#include "MemMan.h"
#include "ULpt.h"
#include "UStr.h"
#include "Verif.h"

#include "driv_im.h"
VerifInit;

// --------------------------------------------------------------------------
//#define PRN_THRD_SIZE_STACK 4096
#define TAILLE_BUF_COM_RX 1024	//$$ tailles � pr�ciser
#define TAILLE_BUF_COM_TX 8000 

// --------------------------------------------------------------------------
// Types d'imprimantes g�r�es
// --------------------------------------------------------------------------
#define PRN_TYPE_PARALLELE  0
#define PRN_TYPE_SERIAL     1

// --------------------------------------------------------------------------
// Valeurs par d�faut
// --------------------------------------------------------------------------
#define PRN_TIMEOUT_WRITE   1000	  // Time-out �criture, en mili-seconde
//
#define PRN_BAUDE_RATE      1200   //
#define PRN_DATA_BITS          8   //
#define PRN_PARITY             0   //
#define PRN_STOP_BITS          1   //
#define PRN_CAR_XON        '\021'  // Valeur en octal: [CTRL]+[Q] = chr (17)
#define PRN_CAR_XOFF       '\023'  // Valeur en octal: [CTRL]+[S] = chr (19)


// --------------------------------------------------------------------------
// Tableau des Line printers Processyn (contient leur num port index sur aPorts[] ou PRN_PORT_NONE)
static int adwNPortImprimante [NB_MAX_PRN] =
	{
	PRN_PORT_NONE, PRN_PORT_NONE, PRN_PORT_NONE, PRN_PORT_NONE, PRN_PORT_NONE, PRN_PORT_NONE
	};

// --------------------------------------------------------------------------
typedef struct
	{
	DWORD      dwSizeBufMax;
	DWORD      dwSizeBuf;
	DWORD      dwSizeBufWritten;
	BYTE *     pbBuf;
	HANDLE     hFile;
	OVERLAPPED ovl;
	} DONNEES_A_ECRIRE, *PDONNEES_A_ECRIRE;

// Infos propres � un port
typedef struct
	{
	PCSTR pszName;
	BOOL	bImprimanteSerie;
	CLpt::HLPT	hlpt;
	PDONNEES_A_ECRIRE	pData;
	PLPRN_COM_INFO		pComInfo;
	} PORT, * PPORT;


static PORT aPorts [PRN_PORT_MAX] = 
	{
		{"LPT1" , FALSE, NULL, NULL, NULL},  //PRN_PORT_LPT1, 
		{"LPT2" , FALSE, NULL, NULL, NULL},  //PRN_PORT_LPT2, 
		{"COM1" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM1, 
		{"COM2" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM2, 
		{"COM3" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM3, 
		{"COM4" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM4, 
		{"COM5" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM5, 
		{"COM6" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM6, 
		{"COM7" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM7, 
		{"COM8" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM8, 
		{"COM9" , TRUE , NULL, NULL, NULL},  //PRN_PORT_COM9, 
		{"COM10", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM10,
		{"COM11", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM11,
		{"COM12", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM12,
		{"COM13", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM13,
		{"COM14", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM14,
		{"COM15", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM15,
		{"COM16", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM16,
		{"COM17", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM17,
		{"COM18", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM18,
		{"COM19", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM19,
		{"COM20", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM20,
		{"COM21", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM21,
		{"COM22", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM22,
		{"COM23", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM23,
		{"COM24", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM24,
		{"COM25", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM25,
		{"COM26", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM26,
		{"COM27", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM27,
		{"COM28", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM28,
		{"COM29", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM29,
		{"COM30", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM30,
		{"COM31", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM31,
		{"COM32", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM32,
		{"COM33", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM33,
		{"COM34", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM34,
		{"COM35", TRUE , NULL, NULL, NULL},  //PRN_PORT_COM35,
		{"COM36", TRUE , NULL, NULL, NULL}   //PRN_PORT_COM36
	};

// --------------------------------------------------------------------------
// Configuration des Line Printers du syt�me
// --------------------------------------------------------------------------
// --------------------------------- Variables pour les imprimantes
//
#define PSZ_KEY_PRN           "PRN"
#define NB_UL_PRN                2
#define MIN_BUF_PRN              1
#define MAX_BUF_PRN              1024
#define MIN_LINE_PER_CYCLE_PRN   1
#define MAX_LINE_PER_CYCLE_PRN  99

typedef struct
	{
	DWORD wPort;						// Port Imprimante
	DWORD wSizeBuf;         // Taille Buffer Tampon en Ko
	DWORD wLinePerCycle;    // Nb de lignes imprim�es par cycle
	DWORD wBaudeRate;       // Vitesse pour imprimante s�rie
	DWORD wDataBits;        // Nb Bits
	DWORD wParity;          // Parit�
	DWORD wStopBits;        // Nb Bits Stop
	BOOL	bImprimanteSerie; // True si imprimante serie
	} CONFIG_LPRN_INITIALE;

static CONFIG_LPRN_INITIALE aConfigsLPrnInitiales [NB_MAX_PRN] =
	{
		{PRN_PORT_LPT1, 100, 1,    0, 0, 0, 0, FALSE},
		{PRN_PORT_LPT2, 100, 0,    0, 0, 0, 0, FALSE},
		{PRN_PORT_COM1, 1, 0, 1200, 8, 0, 1, TRUE },
		{PRN_PORT_COM2, 1, 0, 1200, 8, 0, 1, TRUE },
		{PRN_PORT_COM3, 1, 0, 1200, 8, 0, 1, TRUE },
		{PRN_PORT_COM4, 1, 0, 1200, 8, 0, 1, TRUE }
	};



// --------------------------------------------------------------------------
// interpreter les param�tres d'une ligne de configuration de Lines printer
// --------------------------------------------------------------------------
BOOL bImprimanteInterpreteLigneIni (DWORD wNbUl, char tab_ul [] [SIZE_UL])
	{
	BOOL bIsPrn = FALSE;
	DWORD    wPrn;
	DWORD    wValue;
	BOOL bTrouve;

	if (bStrEgales (tab_ul [0], PSZ_KEY_PRN))
		{
		bIsPrn = TRUE;
		if (wNbUl >= NB_UL_PRN)
			{
			if (!StrToDWORD (&wPrn, tab_ul [1]))
				{
				wPrn = 0;
				}
			wPrn--;
			if (wPrn < NB_MAX_PRN)
				{
				// ATTENTION: switch sans BREAK.
				switch (wNbUl)
					{
					case 9:
						if (StrToDWORD (&wValue, tab_ul [8]))
							aConfigsLPrnInitiales [wPrn].wStopBits = wValue;

					case 8:
						if (StrToDWORD (&wValue, tab_ul [7]))
							aConfigsLPrnInitiales [wPrn].wParity = wValue;

					case 7:
						if (StrToDWORD (&wValue, tab_ul [6]))
							aConfigsLPrnInitiales [wPrn].wDataBits = wValue;

					case 6:
						if (StrToDWORD (&wValue, tab_ul [5]))
							aConfigsLPrnInitiales [wPrn].wBaudeRate = wValue;

					case 5:
						if (StrToDWORD (&wValue, tab_ul [4]))
							{
							if ((wValue >= MIN_LINE_PER_CYCLE_PRN) && (wValue <= MAX_LINE_PER_CYCLE_PRN))
								aConfigsLPrnInitiales [wPrn].wLinePerCycle = wValue;
							}

					case 4:
						if (StrToDWORD (&wValue, tab_ul [3]))
							{
							if ((wValue >= MIN_BUF_PRN) && (wValue <= MAX_BUF_PRN))
								aConfigsLPrnInitiales [wPrn].wSizeBuf = wValue;
							}

					case 3:
						bTrouve = FALSE;
						wValue = 0;
						while ((wValue < PRN_PORT_MAX) && (!bTrouve))
							{
							bTrouve = bStrEgales (tab_ul [2], aPorts [wValue].pszName);
							wValue++;
							}
						if (bTrouve)
							{
							aConfigsLPrnInitiales [wPrn].wPort = wValue - 1;
							aConfigsLPrnInitiales [wPrn].bImprimanteSerie = aPorts [wValue - 1].bImprimanteSerie;
							}
						else
							{
							aConfigsLPrnInitiales [wPrn].wPort = PRN_PORT_NONE;
							}
					} // switch (wNbUl)
				} //if (wPrn < NB_MAX_PRN)
			} //if (wNbUl >= NB_UL_PRN)
		}

	return bIsPrn;
	}


// --------------------------------------------------------------------------
// Retourner les param�tres du buffer souhait� pour une imprimante donn�e
// --------------------------------------------------------------------------
BOOL definition_buffer_imprimante (DWORD wPrn, DWORD *pwSizeBuf, DWORD *pwLinePerCycle)
	{
	BOOL bOk = FALSE;

	wPrn--;
	if (wPrn < NB_MAX_PRN)
		{
		*pwSizeBuf      = aConfigsLPrnInitiales [wPrn].wSizeBuf;
		*pwLinePerCycle = aConfigsLPrnInitiales [wPrn].wLinePerCycle;
		bOk = TRUE;
		}

	return bOk;
	}

// --------------------------------------------------------------------------
// Retourner les param�tres du port d'une imprimante donn�e
// --------------------------------------------------------------------------
BOOL definition_port_imprimante (DWORD wPrn, DWORD *pwPort, DWORD *pwBaudeRate, DWORD *pwDataBits,DWORD *pwParity,DWORD *pwStopBits,BOOL *pbIsSerial)
	{
	BOOL bOk = FALSE;

	wPrn--;
	if (wPrn < NB_MAX_PRN)
		{
		*pwPort					= aConfigsLPrnInitiales [wPrn].wPort;
		*pwBaudeRate    = aConfigsLPrnInitiales [wPrn].wBaudeRate;
		*pwDataBits     = aConfigsLPrnInitiales [wPrn].wDataBits;
		*pwParity       = aConfigsLPrnInitiales [wPrn].wParity;
		*pwStopBits     = aConfigsLPrnInitiales [wPrn].wStopBits;
		*pbIsSerial     = aConfigsLPrnInitiales [wPrn].bImprimanteSerie;
		bOk = TRUE;
		}

	return bOk;
	}

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------
static CLpt::ERREUR_LPT PrnFctSerialInit (PORT *pPort)
	{
	CLpt::ERREUR_LPT           LError;
	//  USHORT             usfCommErr;
	//  LINECONTROL        LineControl;
	DCB            dcb;
	//  MODEMSTATUS        ModemStatus;
	LPRN_CONFIG_COM *pSerialData;

	// $$ time out ?
	pSerialData = pPort->pComInfo;
	if (SetupComm(pPort->pData->hFile, TAILLE_BUF_COM_RX,	TAILLE_BUF_COM_TX))
		{
		if (GetCommState(pPort->pData->hFile, &dcb))
			{
			dcb.BaudRate	= pSerialData->wBaudRate;
			dcb.fBinary		= TRUE;
			dcb.fParity		= TRUE;		// teste la parit�
			dcb.fOutxCtsFlow = FALSE; // pas d'arr�t d'�mission selon CTS
			dcb.fOutxDsrFlow = FALSE; // pas d'arr�t d'�mission selon DSR
			dcb.fDtrControl = DTR_CONTROL_ENABLE;	// DTR ON - pas de handshake
			dcb.fDsrSensitivity = FALSE; // pas de r�ception de caract�re ignor�e selon l'�tat de DSR
			dcb.fTXContinueOnXoff = TRUE;
			dcb.fOutX = TRUE; // Xon Xoff � l'�mission
			dcb.fInX = FALSE;
			dcb.fErrorChar = FALSE; // pas de remplacement des caract�res re�us avec erreur de parit�
			dcb.fNull = FALSE;	// r�ception de caract�res NULL
			dcb.fRtsControl = RTS_CONTROL_DISABLE;	// RTS OFF - pas de handshake
			dcb.fAbortOnError = FALSE; // pas d'abort transmission sur erreur
			dcb.fDummy2 = 0;
			dcb.wReserved = 0;
			dcb.XonLim = 2;	//$$ faute de mieux
			dcb.XoffLim = 2;	//$$ faute de mieux
			dcb.ByteSize	= (BYTE)pSerialData->wDataBits;
			dcb.Parity		= (BYTE)pSerialData->wParity;
			dcb.StopBits	= (BYTE)pSerialData->wStopBits;// $$ v�rifier
			dcb.XonChar = pSerialData->cXOn;
			dcb.XoffChar = pSerialData->cXOff;
			dcb.ErrorChar = '\0';
			dcb.EvtChar = '\0';
			dcb.wReserved1 = 0;
			if (SetCommState(pPort->pData->hFile, &dcb))
				{
				if (TRUE)
					LError = CLpt::ERREUR_LPT_NONE;
				}
			else
				{
				LError = CLpt::ERREUR_LPT_INIT_DEVICE_CTRL;
				}
			}
		} // if (SetupComm
	else
		LError = CLpt::ERREUR_LPT_INIT_LINE_CTRL; // $$ � documenter

	/*
	LineControl.bDataBits   = (BYTE) pSerialData->wDataBits;
	LineControl.bParity     = (BYTE) pSerialData->wParity;
	LineControl.bStopBits   = (BYTE) ((pSerialData->wStopBits - 1) * 2);
	LineControl.fTransBreak = 0;
	if (DosDevIOCtl (NULL, &LineControl, ASYNC_SETLINECTRL, IOCTL_ASYNC, pPort->pData->hFile) == 0)
	{
	if (DosDevIOCtl (NULL, &, ASYNC_SETBAUDRATE, IOCTL_ASYNC, pPort->pData->hFile) == 0)
	{
	dcb.usWriteTimeout        = 0;
	dcb.usReadTimeout         = 0;
	dcb.fbCtlHndShake         = 0;                        // pas de handshake
	dcb.fbFlowReplace         = MODE_AUTO_TRANSMIT;       // handshake Xon/Xoff
	dcb.fbTimeout             = MODE_NOWAIT_READ_TIMEOUT;
	dcb.bErrorReplacementChar = 0;
	dcb.bBreakReplacementChar = 0;
	dcb.bXONChar              = pSerialData->cXOn;
	dcb.bXOFFChar             = pSerialData->cXOff;
	if (DosDevIOCtl (NULL, &dcb, ASYNC_SETDCBINFO, IOCTL_ASYNC, pPort->pData->hFile) == 0)
	{
	ModemStatus.fbModemOn  = 0x01;   // ARME   #00 =aucun; #01 =DTR; #02 =RTS
	ModemStatus.fbModemOff = 0xFD;   // MASQUE #FF =aucun; #FE =DTR; #FD =RTS
	if (DosDevIOCtl (&usfCommErr, &ModemStatus, ASYNC_SETMODEMCTRL, IOCTL_ASYNC, pPort->pData->hFile) == 0)
	{
	LError = ERREUR_LPT_NONE;
	}
	else
	{
	LError = ERREUR_LPT_INIT_MODEM_CTRL;
	}
	}
	else
	{
	LError = ERREUR_LPT_INIT_DEVICE_CTRL;
	}
	}
	else
	{
	LError = ERREUR_LPT_INIT_BAUDE_RATE;
	}
	}
	else
	{
	LError = ERREUR_LPT_INIT_LINE_CTRL;
	}
	*/

	return (LError);
	}

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------
static CLpt::ERREUR_LPT PrnFctSerialStatus (PORT *pPort, DWORD *pwStatus)
	{
	CLpt::ERREUR_LPT   LError = CLpt::ERREUR_LPT_NONE;
	DWORD   wStatus = PRN_SERIAL_STATUS_OK;
	DWORD dwCommErr;
	COMSTAT	comstat;


	if (ClearCommError(pPort->pData->hFile,	&dwCommErr, &comstat))
		//if (DosDevIOCtl (&usCommErr, NULL, ASYNC_GETCOMMERROR, IOCTL_ASYNC, pPort->pData->hFile) == 0)
		{
		if (dwCommErr & CE_RXOVER)
			wStatus = wStatus | PRN_SERIAL_STATUS_QUEUE_OVERRUN;

		if (dwCommErr & CE_OVERRUN)
			wStatus = wStatus | PRN_SERIAL_STATUS_HARD_OVERRUN;

		if (dwCommErr & CE_RXPARITY)
			wStatus = wStatus | PRN_SERIAL_STATUS_PARITY_ERROR;

		if (dwCommErr & CE_FRAME)
			wStatus = wStatus | PRN_SERIAL_STATUS_FRAMING_ERROR;

		if (dwCommErr & (CE_BREAK|CE_TXFULL|CE_MODE)) // $$ documenter
			wStatus = wStatus | PRN_SERIAL_STATUS_OTHER_ERROR;
		}
	else
		{
		LError = CLpt::ERREUR_LPT_STATUS;
		}

	(*pwStatus) = wStatus;

	return LError;
	}

// --------------------------------------------------------------------------
// Ouverture d'une Imprimante Line Printer
CLpt::ERREUR_LPT LImprimanteOuvre (UINT wPrn, DWORD wPort, PLPRN_COM_INFO pComInfo)
	{
	CLpt::ERREUR_LPT LError = CLpt::ERREUR_LPT_NONE;
	PPORT						pPort;
	LPRN_CONFIG_COM * pSerialData;

	if (wPort != PRN_PORT_NONE)
		{
		wPrn--;
		if (adwNPortImprimante [wPrn] == PRN_PORT_NONE)
			{
			pPort = &aPorts [wPort];
			//
			pPort->pData                  = (PDONNEES_A_ECRIRE)pMemAlloueInit0 (sizeof (DONNEES_A_ECRIRE));
			pPort->pData->hFile           = INVALID_HANDLE_VALUE;
			//
			if (pPort->bImprimanteSerie)
				{
				pSerialData  = (PLPRN_COM_INFO)pMemAlloue (sizeof (LPRN_CONFIG_COM));
				if (pComInfo == NULL)
					{
					pSerialData->wBaudRate  = PRN_BAUDE_RATE;
					pSerialData->wDataBits  = PRN_DATA_BITS;
					pSerialData->wParity    = PRN_PARITY;
					pSerialData->wStopBits  = PRN_STOP_BITS;
					pSerialData->cXOn       = PRN_CAR_XON;
					pSerialData->cXOff      = PRN_CAR_XOFF;
					}
				else
					{
					*pSerialData = (* (LPRN_CONFIG_COM *) pComInfo);
					}
				pPort->pComInfo = pSerialData;
				LError = CLpt::ERREUR_LPT_OPEN; // $$ac non implante
				}
			else // imprimante parallele
				{
				pPort->pComInfo = NULL;
				if (!CLpt::bLptOuvre (&pPort->hlpt, pPort->pszName))
					{
					LError = CLpt::ERREUR_LPT_OPEN;
					MemLibere ((PVOID *)(&pPort->pData));
					}
				}
			}
		else
			LError = CLpt::ERREUR_LPT_PORT_REDEFINITION;

		if (LError == CLpt::ERREUR_LPT_NONE)
			{
			adwNPortImprimante [wPrn] = wPort;
			}
		}
	else // mauvaise init dans pcs.ini
		{
		LError = CLpt::ERREUR_LPT_PORT_UNDEFINE;
		}
	return LError;
	} // dwLImprimanteOuvre

// --------------------------------------------------------------------------
// Fermeture d'une Imprimante Line Printer
CLpt::ERREUR_LPT LImprimanteFerme (UINT wPrn)
	{
	CLpt::ERREUR_LPT  LError = CLpt::ERREUR_LPT_NONE;

	wPrn--;
	if (adwNPortImprimante [wPrn] != PRN_PORT_NONE)
		{
		PORT * pPort = &aPorts [adwNPortImprimante [wPrn]];

		if (pPort->bImprimanteSerie)
			{
			LError = CLpt::ERREUR_LPT_OPEN;
			}
		else
			{
			if (!CLpt::bFermeLpt (&pPort->hlpt))
				LError = CLpt::ERREUR_LPT_OPEN;
			}

		if (pPort->pData->pbBuf != NULL)
			{
			MemLibere ((PVOID *)(&pPort->pData->pbBuf));
			}

		MemLibere ((PVOID *)(&pPort->pData));

		if (pPort->pComInfo != NULL)
			{
			MemLibere ((PVOID *)(&pPort->pComInfo));
			}

		adwNPortImprimante [wPrn] = PRN_PORT_NONE;
		}
	else
		LError = CLpt::ERREUR_LPT_PORT_UNDEFINE;

	return LError;
	} // dwLImprimanteFerme

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------
CLpt::ERREUR_LPT LImprimanteInit (UINT wPrn)
	{
	CLpt::ERREUR_LPT   LError = CLpt::ERREUR_LPT_NONE;

	wPrn--;
	if (adwNPortImprimante [wPrn] != PRN_PORT_NONE)
		{
		PORT *	pPort = &aPorts [adwNPortImprimante [wPrn]];

		if (pPort->bImprimanteSerie)
			LError = PrnFctSerialInit (pPort);
		}
	else
		LError = CLpt::ERREUR_LPT_PORT_UNDEFINE;

	return LError;
	}

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------
CLpt::ERREUR_LPT LImprimanteTimeOut (UINT wPrn, DWORD lwTimeOut)
	{
	CLpt::ERREUR_LPT   LError = CLpt::ERREUR_LPT_NONE;

	wPrn--;
	if (adwNPortImprimante [wPrn] != PRN_PORT_NONE)
		{
		PORT *	pPort = &aPorts [adwNPortImprimante [wPrn]];
		if (pPort->bImprimanteSerie)
			{
			//
			}
		else
			{
			CLpt::LptSetTimeOut (pPort->hlpt, lwTimeOut);
			}
		}
	//
	else
		LError = CLpt::ERREUR_LPT_PORT_UNDEFINE;

	return LError;
	}

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------
CLpt::ERREUR_LPT LImprimanteWrite (UINT wPrn, DWORD dwSizeBuf, void *pvBuf, DWORD *pdwErreurSysteme, DWORD *pdwSizeBufWritten, BOOL bAvecConversion)
	{
	CLpt::ERREUR_LPT  LError;

	if ((LError = LImprimanteWaitWrite (wPrn, pdwErreurSysteme, pdwSizeBufWritten)) == CLpt::ERREUR_LPT_NONE)
		{
		if ((LError = LImprimanteWriteAsync (wPrn, dwSizeBuf, pvBuf, pdwErreurSysteme, bAvecConversion)) ==  CLpt::ERREUR_LPT_NONE)
			{
			do
				{
				LError = LImprimanteWaitWrite (wPrn, pdwErreurSysteme, pdwSizeBufWritten);
				}
				while ((*pdwSizeBufWritten) != dwSizeBuf);
			}
		}
	return LError;
	}

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------
CLpt::ERREUR_LPT LImprimanteWriteAsync (UINT wPrn, DWORD dwSizeBuf, void *pvBuf, DWORD *pdwErreurSysteme, BOOL bAvecConversion)
	{
	CLpt::ERREUR_LPT LError = CLpt::ERREUR_LPT_NONE;

	wPrn--;
	if (adwNPortImprimante [wPrn] != PRN_PORT_NONE)
		{
		PORT *pPort = &aPorts [adwNPortImprimante [wPrn]];
		// Augmente la taille du buffer d'impression si n�cessaire
		if (dwSizeBuf > pPort->pData->dwSizeBufMax)
			{
			MemRealloue ((LPVOID *)(&pPort->pData->pbBuf), dwSizeBuf);
			pPort->pData->dwSizeBufMax = dwSizeBuf;
			}
		// Recopie les donn�es
		pPort->pData->dwSizeBuf = dwSizeBuf;
		MemCopy (pPort->pData->pbBuf, pvBuf, dwSizeBuf);

		// Conversion ansi - ascii pr�sum�e utilis� sur les imprimantes
		if (bAvecConversion)
			CharToOemBuff ((PCSTR)pPort->pData->pbBuf, (PSTR)pPort->pData->pbBuf, dwSizeBuf);

		pPort->pData->dwSizeBufWritten = 0;

		if (pPort->bImprimanteSerie)
			{
			// Ecriture sur imprimante s�rie
			LError = PrnFctSerialStatus (pPort, pdwErreurSysteme);
			//$$ Manque le write
			if (LError != CLpt::ERREUR_LPT_NONE)
				{
				if (!PurgeComm(pPort->pData->hFile,PURGE_TXABORT))// | PURGE_RXABORT
					{
					//PURGE_TXCLEAR  PURGE_RXCLEAR 
					//$$ statut
					//DosDevIOCtl (0L, &bCommand, DEV_FLUSHINPUT,  IOCTL_GENERAL, pPort->pData->hFile);
					//DosDevIOCtl (0L, &bCommand, DEV_FLUSHOUTPUT, IOCTL_GENERAL, pPort->pData->hFile);
					}
				}
			}
		else
			{
			LError = CLpt::LptEcrire (pPort->hlpt, pPort->pData->dwSizeBuf, pPort->pData->pbBuf, pdwErreurSysteme);
			}
		}
	else
		LError = CLpt::ERREUR_LPT_PORT_UNDEFINE;

	return LError;
	}

// --------------------------------------------------------------------------
//
// --------------------------------------------------------------------------
CLpt::ERREUR_LPT LImprimanteWaitWrite (UINT wPrn, DWORD *pdwErreurSysteme, DWORD *pdwSizeBufWritten)
	{
	CLpt::ERREUR_LPT	LError = CLpt::ERREUR_LPT_NONE;

	wPrn--;
	if (adwNPortImprimante [wPrn] != PRN_PORT_NONE)
		{
		PORT *pPort = &aPorts [adwNPortImprimante [wPrn]];
		//
		if (pPort->bImprimanteSerie)
			{
			//
			}
		else
			{
			LError = CLpt::LptAttenteFinEcriture (pPort->hlpt, pdwErreurSysteme, pdwSizeBufWritten);
			}
		}
	else
		LError = CLpt::ERREUR_LPT_PORT_UNDEFINE;

	return LError;
	}





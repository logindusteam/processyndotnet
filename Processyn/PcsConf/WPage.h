// -----------------------------------------------------------------------
// Module...: WPage.h
// Objet....: Gestion d'une Fen�tre editeur de texte
// -----------------------------------------------------------------------


HWND creer_wnd_page (HWND hwndParent, HWND hwndOwner, UINT id,
		     int pos_x_init, int pos_y_init,
		     int taille_x, int taille_y);

void detruire_wnd_page (HWND hwndPage);

void effacer_wnd_page (HWND hwndPage);

void ecrire_wnd_page (HWND hwndPage, char *texte, DWORD etat_ligne);

DWORD nombre_ligne_wnd_page (HWND hwndPage);

void position_curseur_wnd_page (HWND hwndPage, DWORD *ligne, DWORD *colonne);

void texte_cour_wnd_page (HWND hwndPage, char *texte);

void scroller_ligne_wnd_page (HWND hwndPage, int decalage);

void scroller_colonne_wnd_page (HWND hwndPage, int decalage);

void aller_debut_ligne_wnd_page (HWND hwndPage);

void aller_fin_ligne_wnd_page (HWND hwndPage);

void monter_curseur_wnd_page (HWND hwndPage, DWORD *decalage);

void descendre_curseur_wnd_page (HWND hwndPage, DWORD &decalage);

void droiter_curseur_wnd_page (HWND hwndPage, DWORD decalage);

void gaucher_curseur_wnd_page (HWND hwndPage, DWORD decalage);

void inserer_ligne_wnd_page (HWND hwndPage);

void inserer_car_wnd_page (HWND hwndPage, char caractere);

void modifier_car_wnd_page (HWND hwndPage, char caractere);

void supprimer_car_wnd_page (HWND hwndPage);

void aller_haut_wnd_page (HWND hwndPage);
void aller_bas_wnd_page  (HWND hwndPage);
void droiter_mot_curseur_wnd_page (HWND hwndPage);
void gaucher_mot_curseur_wnd_page (HWND hwndPage);

void reajuster_ascenseur_wnd_page (HWND hwndPage, DWORD pos_relative_ligne, DWORD taille_relative);

void memoriser_curseur_wnd_page (HWND hwndPage);
void restaurer_curseur_wnd_page (HWND hwndPage);




void sauver_marque_wnd_page    (HWND hwndPage);
void restaurer_marque_wnd_page (HWND hwndPage);

void afficher_select_ligne_wnd_page (HWND hwndPage, BOOL mode_selection);
void abandon_selection_wnd_page     (HWND hwndPage);

void curseur_invisible_wnd_page (HWND hwndPage);
void curseur_visible_wnd_page   (HWND hwndPage);
void PageChangePolice (HWND hwnd, DWORD dwNFont, DWORD dwTailleFont, DWORD dwConfBitsStyle, BYTE byCharSetFont);


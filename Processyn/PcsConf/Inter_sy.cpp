/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_sy.c                                               |
 |   Auteur  : AC					        	|
 |   Date    : 29/12/93					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "cvt_ul.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "tipe_sy.h"
#include "inter_sy.h"
#include "Verif.h"
VerifInit;

//-------------------------- Variables ---------------------
static DWORD ligne_depart_systeme;
static DWORD ligne_courante_systeme;
static BOOL systeme_deja_initialise;

//------------------------------------------------------------------------
static void genere_vect_ul (DWORD ligne, PUL vect_ul, int *nb_ul)
  {
  if (ligne <= dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_represente_syst))
    {
		PDWORD position = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_represente_syst, ligne);
		PGEO_SYST	ptr_geo = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, (*position));

    (*nb_ul) = 2;
		init_vect_ul (vect_ul, *nb_ul);
    NomVarES (vect_ul[0].show, ptr_geo->pos_es);
    consulte_cst_bd_c (ptr_geo->pos_init,vect_ul[1].show);
    }
  else
    {
    (*nb_ul) = 0;
    }
  }

//-------------------------------------------------------------------------
static BOOL valide_vect_ul (PUL vect_ul, DWORD ligne, int *nb_ul, DWORD *erreur)
  {
  ID_MOT_RESERVE numero_fonction;
  DWORD pos_ident;
  DWORD pos_init;
  BOOL exist_ident;
  BOOL exist_init;
  BOOL un_log;
  BOOL un_num;
  BOOL val_log;
  FLOAT val_num;
  PGEO_SYST	ptr_geo;
  DWORD *position;
  char message_reserve[c_nb_car_message_res];
  char old_genre[c_nb_car_message_res];
  char old_sens[c_nb_car_message_res];
  char old_ident[c_nb_car_es];

  //***********************************************
  #define err(code) (*erreur) = code;return (FALSE)  //Sale Sioux
  //***********************************************

  (*erreur) = 0;
  if ((*nb_ul) <= 1)
    {
    err (IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
    }
  if ((*nb_ul) > 2)
    {
    err (31);
    }
  if (StrLength (vect_ul[0].show) > 20)
    {
    err(IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, &pos_ident))
    {
    err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!exist_ident)
    {
    err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
    }
  position = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_represente_syst, ligne);
  ptr_geo = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, (*position));
  consulte_es_bd_c (ptr_geo->pos_es, old_ident, old_sens, old_genre);
  if (!bStrEgales (old_ident, vect_ul[0].show))
    {
    err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
    }
  if (!bReconnaitConstante ((vect_ul+1), &val_log, &val_num,
                            &un_log, &un_num, &exist_init, &pos_init))
    {
    err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
    }
  if (un_log)
    {
    numero_fonction = c_res_logique;
    }
  else
    {
    if (un_num)
      {
      numero_fonction = c_res_numerique;
      }
    else
      {
      numero_fonction = c_res_message;
      }
    }
  bMotReserve (numero_fonction, message_reserve);
  if (!bStrEgales (message_reserve, old_genre))
    {
    err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
    }
  return (TRUE);
  }

//---------------------------------------------------------------------
static void valide_ligne_systeme (REQUETE_EDIT action, DWORD ligne,
                                  PUL vect_ul, int *nb_ul,
                                  BOOL *supprime_ligne, BOOL *accepte_ds_bd,
                                  DWORD *erreur_val, int *niveau, int *indentation)
  {
  DWORD erreur;
  DWORD *position;
  GEO_SYST val_geo;
  PGEO_SYST	ptr_val_geo;

  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;
  switch (action)
    {
    case MODIFIE_LIGNE :
      if (valide_vect_ul (vect_ul, ligne, nb_ul, &erreur))
         {
         position = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_represente_syst, ligne);
         ptr_val_geo = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, (*position));
         val_geo = (*ptr_val_geo);
         modifie_cst_bd_c (vect_ul[1].show, &val_geo.pos_init);
         position = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_represente_syst, ligne);
         ptr_val_geo = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, (*position));
         (*ptr_val_geo) = val_geo;
         }
       else
         {
         genere_vect_ul (ligne, vect_ul, nb_ul);
         (*erreur_val) = erreur;
         }
       (*accepte_ds_bd) = TRUE;
       (*supprime_ligne) = FALSE;
       break;

    case INSERE_LIGNE :
      (*erreur_val) = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
      break;

    case SUPPRIME_LIGNE :
      (*erreur_val) = 54;
      break;

    case CONSULTE_LIGNE :
      genere_vect_ul (ligne, vect_ul, nb_ul);
      break;

    default:
      break;
    }
  }

// ---------------------------------------------------------------------------
static void creer_systeme (void)
  {
  systeme_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
static void init_systeme (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  if (!systeme_deja_initialise)
    {
    ligne_depart_systeme = 1;
    ligne_courante_systeme = ligne_depart_systeme;
    systeme_deja_initialise = TRUE;
    }
  (*depart) = ligne_depart_systeme;
  (*courant) = ligne_courante_systeme;
  }

// ---------------------------------------------------------------------------
static DWORD lis_nombre_de_ligne_systeme (void)
  {
  DWORD nombre_de_ligne_systeme;

  if (existe_repere (b_geo_syst))
    {
    nombre_de_ligne_systeme = nb_enregistrements (szVERIFSource, __LINE__, b_represente_syst);
    }
  else
    {
    nombre_de_ligne_systeme = 0;
    }
  return (nombre_de_ligne_systeme);
  }

// ---------------------------------------------------------------------------
static void set_tdb_systeme (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_com[c_nb_car_message_inf];
  char chaine_long [80];

  tdb[0] = '\0';
  mot_com [0] = '\0';
  chaine_long [0] = '\0';
  bLngLireInformation (c_inf_occupation_memoire, mot_com);
  StrCopy (tdb, mot_com);
  StrConcat (tdb, ": ");
  StrDWordToStr (chaine_long, long_user());
  StrConcat (tdb, chaine_long);
  StrConcatChar (tdb, ' ');
  bLngLireInformation (c_inf_octets, mot_com);
  StrConcat (tdb, mot_com);
  }

// ---------------------------------------------------------------------------
static void fin_systeme (BOOL quitte, DWORD courant)
  {
  if (!quitte)
    {
    ligne_courante_systeme = courant;
    }
  }

// ---------------------------------------------------------------------------
// Interface du descripteur Variables syst�me
void LisDescripteurSy (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_systeme;
	pDescripteurCf->pfnInitDesc = init_systeme;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_systeme;
	pDescripteurCf->pfnValideLigne = valide_ligne_systeme;
	pDescripteurCf->pfnSetTdb = set_tdb_systeme;
	pDescripteurCf->pfnFin = fin_systeme;
	}

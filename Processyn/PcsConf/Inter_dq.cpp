/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_dq.hpr                                             |
 |   Auteur  : RP                                                       |
 |   Date    : 21/07/94                                                 |
 |   Version : 4.0							|
 |                                                                      |
 +----------------------------------------------------------------------*/
// Interpreteur disque
// -------------------

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "cvt_ul.h"
#include "driv_dq.h"
#include "tipe_dq.h"
#include "mem.h"
#include "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "pcsdlgdq.h"
#include "IdLngLng.h"
#include "inter_dq.h"
#include "Verif.h"
VerifInit;

//-------------------------- Variables ---------------------
static char contexte_dq[82];
static DWORD ligne_depart_disq;
static DWORD ligne_courante_disq;
static BOOL disque_deja_initialise;


/*-------------------------------------------------------------------------*/
/*                        FONCTION UTILITAIRE                              */
/*-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/*                        GESTION CONTEXTE                                 */
/*-------------------------------------------------------------------------*/
static void consulte_titre_paragraf_dq (DWORD ligne)
  {

  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_DISQUE	donnees_geo;
  PSPEC_TITRE_PARAGRAPHE_DISQUE	spec_titre_paragraf;
  CONTEXTE_PARAGRAPHE_LIGNE   table_contexte_ligne [2];
  DWORD profondeur_ligne;
  DWORD i;

  if (bDonneContexteParagrapheLigne (b_titre_paragraf_dq, ligne, &profondeur_ligne, table_contexte_ligne))
    {
    for (i = 0; (i < profondeur_ligne); i++)
      {
      switch (table_contexte_ligne [i].IdParagraphe)
        {
        case fichier_dq:
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,table_contexte_ligne [i].position);
          donnees_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq, titre_paragraf->pos_geo_debut);
          spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,donnees_geo->pos_specif_paragraf);
          consulte_cst_bd_c (spec_titre_paragraf->position_fichier[0], contexte_dq);
          break;

        case ecrit_dq:
          break;

        case lit_dq:
          break;

        case enr_dq:
          break;

        default:
          StrSetNull (contexte_dq);
          break;

        }
      }
    }
  else
    {
    StrSetNull (contexte_dq);
    }
  }

/*--------------------------------------------------------------------------*/
/*                             DIVERS                                       */
/*--------------------------------------------------------------------------*/

static void maj_pointeur_spec_dans_geo (DWORD limite)
  {
  PGEO_DISQUE	ptr_val_geo;
  DWORD nbr_enr;
  DWORD i;

  if (!existe_repere (b_geo_disq))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_disq);
    }
  i = 0;
  while (i < nbr_enr)
    {
    i++;
    ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,i);
    if (ptr_val_geo->numero_repere == b_titre_paragraf_dq)
      {
      if (ptr_val_geo->pos_specif_paragraf >= limite)
        {
        ptr_val_geo->pos_specif_paragraf--;
        }
      }
    }
  }

//---------------------------------------------------------------------------

static void maj_pointeur_paragraf_dans_geo (DWORD limite)
  {

  PGEO_DISQUE	ptr_val_geo;
  DWORD nbr_enr;
  DWORD i;

  if (!existe_repere (b_geo_disq))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_disq);
    }
  i = 0;
  while (i < nbr_enr)
    {
    i++;
    ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,i);
    if (ptr_val_geo->numero_repere == b_titre_paragraf_dq)
      {
      if (ptr_val_geo->pos_paragraf >= limite)
        {
        ptr_val_geo->pos_paragraf--;
        }
      }
    }
  }

/*--------------------------------------------------------------------------*/
void static traitement_num (PUL ul, DWORD num)
  {
  StrDWordToStr (ul->show, num);
  ul->erreur = 0;
  }

/*--------------------------------------------------------------------------*/
/*                             EDITION                                      */
/*--------------------------------------------------------------------------*/

//'''''''''''''''''''''''''''generation vecteur UL'''''''''''''''''''''''''''''
static void genere_vect_ul (DWORD ligne, PUL vect_ul, int *nb_ul)

  {
  PGEO_DISQUE	ptr_val_geo;
  PTITRE_PARAGRAPHE titre_paragraf;
  PSPEC_TITRE_PARAGRAPHE_DISQUE	spec_titre_paragraf;
  int i;

  ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,ligne);
  switch (ptr_val_geo->numero_repere)
    {

    case b_pt_constant:
     (*nb_ul) = 1;
     init_vect_ul (vect_ul, (*nb_ul));
     consulte_cst_bd_c (ptr_val_geo->pos_donnees,vect_ul[0].show);
     break;

    case b_titre_paragraf_dq :
     titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,ptr_val_geo->pos_paragraf);
     switch (titre_paragraf->IdParagraphe)
       {

       case fichier_dq:
         spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,ptr_val_geo->pos_specif_paragraf);
         (*nb_ul) = (int)(spec_titre_paragraf->nb_fichier + 1);
         init_vect_ul (vect_ul, (*nb_ul));
         bMotReserve (c_res_fichier, vect_ul[0].show);
         for (i = 0; i < (int)(spec_titre_paragraf->nb_fichier); i++)
           {
           consulte_cst_bd_c (spec_titre_paragraf->position_fichier[i],vect_ul[i+1].show);
           }
         break;

       case ecrit_dq :
         (*nb_ul) = 2;
         init_vect_ul (vect_ul, (*nb_ul));
         bMotReserve (c_res_ecriture, vect_ul[0].show);
         spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,ptr_val_geo->pos_specif_paragraf);
         NomVarES (vect_ul[1].show, spec_titre_paragraf->pos_es);
         break;

       case lit_dq :
         (*nb_ul) = 2;
         init_vect_ul (vect_ul, (*nb_ul));
         bMotReserve (c_res_lecture, vect_ul[0].show);
         spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,ptr_val_geo->pos_specif_paragraf);
         NomVarES (vect_ul[1].show, spec_titre_paragraf->pos_es);
         break;

       case enr_dq :
         (*nb_ul) = 2;
         init_vect_ul (vect_ul, (*nb_ul));
         bMotReserve (c_res_enregistrements, vect_ul[0].show);
         spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,ptr_val_geo->pos_specif_paragraf);
         NomVarES (vect_ul[1].show, spec_titre_paragraf->pos_es);
         break;

       default:
         break;

       }
     break;

    case b_e_s :
      if (ptr_val_geo->longueur == 0)
        {
        (*nb_ul) = 2;
        }
      else
        {
        (*nb_ul) = 3 ;
        }
      init_vect_ul (vect_ul, (*nb_ul));
      NomVarES (vect_ul[0].show, ptr_val_geo->pos_es);

      if (*nb_ul == 3)
        {
        traitement_num(&(vect_ul[1]),ptr_val_geo->longueur);
        }
      consulte_cst_bd_c (ptr_val_geo->pos_initial,vect_ul[(*nb_ul)-1].show);
      break;

    default:
      break;

    }
  }

//''''''''''''''''''''''''valide vecteur UL''''''''''''''''''''''''''''''''''

//------------------------------------------------------------------------
static DWORD reconnait_var_controle (PUL ul, DWORD *position,REQUETE_EDIT *action, DWORD ligne)
  {
  //***********************************************
  #define err_rvc(code) return (code)  //Sale Sioux
  //***********************************************

  DWORD pos_specif;
  BOOL exist_var_controle;
  PSPEC_TITRE_PARAGRAPHE_DISQUE	spec_titre_paragraf_dq;
  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_DISQUE	ptr_val_geo;

  if (!bReconnaitESValide(ul,&exist_var_controle,position))
    {
    err_rvc(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (StrLength (ul->show) > 20)
    {
    err_rvc(IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if ((*action) != MODIFIE_LIGNE)
    {
    if (exist_var_controle)
      {
      err_rvc(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
      }
    }
  else
    {
    ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,ligne);
    if (ptr_val_geo->numero_repere != b_titre_paragraf_dq)
      {
      err_rvc(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,ptr_val_geo->pos_paragraf);
    if ((titre_paragraf->IdParagraphe == fichier_dq))
      {
      err_rvc(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    spec_titre_paragraf_dq = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,ptr_val_geo->pos_specif_paragraf);
    pos_specif = spec_titre_paragraf_dq->pos_es;
    if ((exist_var_controle) && (!bStrEgaleANomVarES (ul->show, pos_specif)))
      {
      err_rvc(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
      }
    }
  err_rvc (0);
  }

//------------------------------------------------------------------------

static BOOL paragraf_controle_existe (ID_PARAGRAPHE tipe_paragraf, DWORD ligne)
  {

  PTITRE_PARAGRAPHE ptr1_titre_paragraf;
  PTITRE_PARAGRAPHE ptr2_titre_paragraf;
  DWORD pos_paragraf;
  PGEO_DISQUE	ptr_val_geo;
  BOOL trouve;
  DWORD index;


  if (bLigneDansParagraphe (b_titre_paragraf_dq,ligne,fichier_dq,INSERE_LIGNE,&pos_paragraf))
    {
    ptr1_titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,pos_paragraf);
    trouve = FALSE;
    index = ptr1_titre_paragraf->pos_geo_debut - 1;
    while ((index <= ptr1_titre_paragraf->pos_geo_debut+ptr1_titre_paragraf->nbr_ligne-1) && (!trouve))
      {
      index++;
      ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,index);
      if (ptr_val_geo->numero_repere == b_titre_paragraf_dq)
        {
        ptr2_titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,ptr_val_geo->pos_paragraf);
        trouve = (BOOL) (ptr2_titre_paragraf->IdParagraphe == tipe_paragraf);
        }
      }
    }
  else
    {
    trouve = FALSE;
    }
  return(trouve);
  }

//---------------------------------------------------------------------------
static BOOL valide_vect_ul (PUL vect_ul, int nb_ul, DWORD ligne,
	REQUETE_EDIT action, DWORD *val_longueur,	DWORD *profondeur, DWORD *numero_repere,
	PID_PARAGRAPHE tipe_paragraf, DWORD *position_ident, DWORD *pos_init, BOOL *exist_init,
	ID_MOT_RESERVE *numero_fonction, DWORD *erreur)
	{
  BOOL exist_ident;
  BOOL val_log;
  BOOL un_log;
  BOOL un_num;
  BOOL booleen;
  FLOAT val_num;
  FLOAT reel;
  char old_contexte[82];

  PGEO_DISQUE	ptr_val_geo;
  ID_MOT_RESERVE n_fonction;
  DWORD nbr_imbrication;
  DWORD mot;
  int  i;
  DWORD err_rvc;
  char chaine_temp[82];

  //***********************************************
  #define err(code) (*erreur) = code;return (FALSE)  //Sale Sioux
  //***********************************************

  //------------------------------------------------------------------------
  StrCopy (old_contexte, contexte_dq);
  (*erreur) = 0;
  if (nb_ul < 1)
    {
    err(IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
    }
  if (!bReconnaitESValide((vect_ul+0),&exist_ident,position_ident))
    {
    if (!bReconnaitMotReserve ((vect_ul+0)->show,&n_fonction))
      {
      if (!bReconnaitConstante ((vect_ul+0),&val_log,&val_num,
                             &un_log,&un_num,exist_init,pos_init))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if ((un_log) || (un_num))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      (*numero_repere) = b_pt_constant;
      }
    else //paragraf
      {
      switch (n_fonction)
        {
        case  c_res_fichier:
          if ((nb_ul < 2) || (nb_ul > c_NB_FIC_MAX+1))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          for (i = 1; i < nb_ul; i++)
            {
            if (!bReconnaitConstante ((vect_ul+i),&val_log,&val_num,
                                  &un_log,&un_num,exist_init,pos_init))
              {
              err(4);
              }
            if ((un_log) || (un_num))
              {
              err(4);
              }
            StrSetNull (chaine_temp);
            StrCopy (chaine_temp, vect_ul[i].show);
            StrDeleteDoubleQuotation (chaine_temp);
            if ((StrLength (vect_ul[i].show) > 80) || (!bFicNomValide (chaine_temp)))
              {
              err(4);
              }
            }
          (*profondeur) = 1;
          nbr_imbrication = 0;
          StrCopy (contexte_dq, vect_ul[1].show) ;
          (*numero_repere) = b_titre_paragraf_dq;
          (*tipe_paragraf) = fichier_dq;
          break;

        case c_res_ecriture:
          if (nb_ul != 2)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (action != MODIFIE_LIGNE)
            {
            if (paragraf_controle_existe (ecrit_dq,ligne))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            }
          if ((err_rvc = reconnait_var_controle((vect_ul+1), position_ident,&action,ligne)) != 0)
            {
            err (err_rvc);
            }
          (*profondeur) = 2;
          nbr_imbrication = 1;
          (*numero_repere) = b_titre_paragraf_dq;
          (*tipe_paragraf) = ecrit_dq;
          break;

        case c_res_lecture:
          if (nb_ul != 2)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (action != MODIFIE_LIGNE)
            {
            if (paragraf_controle_existe (lit_dq,ligne))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            }
          if ((err_rvc = reconnait_var_controle((vect_ul+1), position_ident,&action,ligne)) != 0)
            {
            err (err_rvc);
            }
          (*profondeur) = 2;
          nbr_imbrication = 1;
          (*numero_repere) = b_titre_paragraf_dq;
          (*tipe_paragraf) = lit_dq;
          break;

        case c_res_enregistrements:
          if (nb_ul != 2)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (action != MODIFIE_LIGNE)
            {
            if (paragraf_controle_existe (enr_dq,ligne))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            }
          if ((err_rvc = reconnait_var_controle((vect_ul+1), position_ident,&action,ligne)) != 0)
            {
            err (err_rvc);
            }
          (*profondeur) = 2;
          nbr_imbrication = 1;
          (*numero_repere) = b_titre_paragraf_dq;
          (*tipe_paragraf) = enr_dq;
          break;

        default:
          err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          break;

        }
      if (!bValideInsereLigneParagraphe (b_titre_paragraf_dq,ligne,
                                           (*profondeur),nbr_imbrication,erreur))
        {
        StrCopy (contexte_dq, old_contexte);
        err((*erreur));
        }
      } //mot res
    } // non identificateur
  else
    {
    if (StrLength (vect_ul[0].show) > 20)
      {
      err(IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
      }
    if ((nb_ul < 2) || (nb_ul > 3))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    if (StrIsNull (contexte_dq))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }

    if (nb_ul == 3)
      {
      if (!bReconnaitConstanteNum ((vect_ul+1),&booleen,&reel,&mot))
        {
        err (69);
        }
      (*val_longueur) = (DWORD)(reel);
      }
    else
      {
      (*val_longueur) = 0;
      }


    if (action != MODIFIE_LIGNE)
      {
      if (exist_ident)
        {
        err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    else
      {
      ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,ligne);
      if (ptr_val_geo->numero_repere == b_e_s)
        {
        if ((ptr_val_geo->longueur != (*val_longueur)) &&
          ((ptr_val_geo->longueur == 0) || ((*val_longueur) == 0)))
          {
          err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        else
          {
          if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, ptr_val_geo->pos_es)))
            {
            err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
            }
          if (nb_ul == 3)
            {
            if (!bReconnaitConstanteNum ((vect_ul+1),&booleen,&reel,&mot))
              {
              err (69);
              }
            (*val_longueur) = (DWORD) (reel);
            }
          }
        }
      else
        {
        if (exist_ident)
          {
          err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
          }
        }
      }
    if (!bReconnaitConstante((vect_ul+(nb_ul-1)),&val_log,&val_num,
                         &un_log,&un_num,exist_init,pos_init))
      {
      err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
      }
    if (un_log)
      {
      (*numero_fonction) = c_res_logique;
      }
    else
      {
      if (un_num)
        {
        (*numero_fonction) = c_res_numerique;
        }
      else
        {
        (*numero_fonction) = c_res_message;
        }
      }
    (*numero_repere) = b_e_s;
    }
  return ((BOOL)((*erreur) == 0));
  }

//-------------------------------------------------------------------------------
static void valide_ligne_disq
	(REQUETE_EDIT action, DWORD ligne, PUL vect_ul, int *nb_ul,
	BOOL *supprime_ligne, BOOL *accepte_ds_bd, DWORD *erreur_val, int *niveau, int *indentation)
  {
  BOOL un_log;
  BOOL un_num;
  BOOL val_log;
  BOOL un_paragraphe;
  FLOAT val_num;
  GEO_DISQUE val_geo;
  PGEO_DISQUE	ptr_val_geo;
  ENTREE_SORTIE es;
  PENTREE_SORTIE	ptr_es;
  DWORD erreur;
  DWORD numero_repere;
  ID_MOT_RESERVE numero_fonction;
  DWORD nv_nb_fic;
  DWORD pos_paragraf;
  DWORD profondeur;
  PTITRE_PARAGRAPHE titre_paragraf;
  SPEC_TITRE_PARAGRAPHE_DISQUE spec_titre_paragraf;
  PSPEC_TITRE_PARAGRAPHE_DISQUE	ptr_spec_titre_paragraf;

  DWORD position_ident;
  DWORD pos_init;
  DWORD pos_init_var_controle;
  DWORD longueur;
  BOOL exist_init;
  BOOL exist_init_var_controle;
  UL ul;
  ID_PARAGRAPHE tipe_paragraf;
  int  i;

  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;

  switch (action)
    {
    case MODIFIE_LIGNE  :
      if (valide_vect_ul(vect_ul,(*nb_ul),ligne,MODIFIE_LIGNE,
                         &longueur,&profondeur,&numero_repere,
                         &tipe_paragraf,&position_ident ,&pos_init,&exist_init,
                         &numero_fonction,&erreur)  )
        {
        ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,ligne);
        val_geo = (*ptr_val_geo);
        if (val_geo.numero_repere == numero_repere)
          {
          switch (numero_repere)
            {
            case b_e_s :
              if ((*nb_ul) == 3)
                {
                val_geo.longueur = longueur;
                es.sens = c_res_variable_ta_es;
                }
              else
                {
                val_geo.longueur = 0;
                es.sens = c_res_e_et_s ;
                }
              es.genre = numero_fonction ;
              es.i_identif = position_ident ;
              es.taille = val_geo.longueur;
              if (bModifieDeclarationES (&es,vect_ul[0].show,val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[(*nb_ul)-1].show,&(val_geo.pos_initial));
                }
               else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_pt_constant :
              modifie_cst_bd_c (vect_ul[0].show,&(val_geo.pos_donnees));
              break;

            case b_titre_paragraf_dq:
              titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,val_geo.pos_paragraf);
              ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
              spec_titre_paragraf = (*ptr_spec_titre_paragraf);
              if (tipe_paragraf == titre_paragraf->IdParagraphe)
                {
                switch (titre_paragraf->IdParagraphe)
                  {
                  case fichier_dq :
                    nv_nb_fic = (DWORD)((*nb_ul) - 1);
                    if (spec_titre_paragraf.nb_fichier <= nv_nb_fic)
                      {
                      for (i = 0; i < (int)(spec_titre_paragraf.nb_fichier); i++)
                        {
                        modifie_cst_bd_c (vect_ul[i+1].show, &(spec_titre_paragraf.position_fichier[i]));
                        }
                      if (spec_titre_paragraf.nb_fichier < nv_nb_fic)
                        {
                        for (i = (int)(spec_titre_paragraf.nb_fichier); i < (int)(nv_nb_fic); i++)
                          {
                          bReconnaitConstante ((vect_ul+i+1),&val_log,&val_num,
                                &un_log,&un_num,&exist_init,&pos_init);
                          AjouteConstanteBd (pos_init,exist_init,vect_ul[i+1].show,&(spec_titre_paragraf.position_fichier[i]));
                          }
                        }
                      }
                    else
                      {
                      for (i = 0; i < (int)(nv_nb_fic); i++)
                        {
                        modifie_cst_bd_c (vect_ul[i+1].show,&(spec_titre_paragraf.position_fichier[i]));
                        }
                      for (i = ((*nb_ul)-1); i < (int)(spec_titre_paragraf.nb_fichier); i++)
                        {
                        supprime_cst_bd_c (spec_titre_paragraf.position_fichier[i]);
                        }
                      }
                    spec_titre_paragraf.nb_fichier = nv_nb_fic;
                    break;

                  case ecrit_dq:
                  case lit_dq :
                    es.sens = c_res_s ;
                    es.genre = c_res_numerique;
                    es.i_identif = position_ident;
                    if (!bModifieDeclarationES (&es,vect_ul[1].show,spec_titre_paragraf.pos_es))
                      {
                      erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                      }
                    break;

                  case enr_dq :
                    es.sens = c_res_e ;
                    es.genre = c_res_numerique;
                    es.i_identif = position_ident;
                    if (!bModifieDeclarationES (&es,vect_ul[1].show,spec_titre_paragraf.pos_es))
                      {
                      erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                      }
                    break;

                  default:
                    break;

                  }
                ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
                (*ptr_spec_titre_paragraf) = spec_titre_paragraf;
                }
              else
                {
                erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
                }
              break;

            default:
              break;

            }
          }
        else
          {
          erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
          }
        }
      if (erreur != 0)
        {
        genere_vect_ul (ligne,vect_ul,nb_ul);
        (*erreur_val) = erreur;
        }
      else
        {
        ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,ligne);
        (*ptr_val_geo) = val_geo;
        }
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne)=FALSE;
      break;

    case INSERE_LIGNE   :
      if (valide_vect_ul(vect_ul,(*nb_ul),ligne,INSERE_LIGNE,
                         &longueur,&profondeur,&numero_repere,
                         &tipe_paragraf,&position_ident,&pos_init,&exist_init,
                         &numero_fonction ,&erreur)  )
         {
         pos_paragraf = 0; // init pour mise a jour donnees paragraf
         val_geo.numero_repere = numero_repere;
         switch (numero_repere)
           {
           case b_pt_constant :
             AjouteConstanteBd (pos_init,exist_init,vect_ul[0].show,
                            &(val_geo.pos_donnees));
             (*accepte_ds_bd) = TRUE;
             (*supprime_ligne)= FALSE;
             break;

           case b_e_s:
             if ((*nb_ul) == 3)
               {
               val_geo.longueur = longueur;
               es.sens = c_res_variable_ta_es;
               }
             else
               {
               val_geo.longueur = 0;
               es.sens = c_res_e_et_s ;
               }
             es.genre = numero_fonction ;
             es.i_identif = position_ident ;
             es.n_desc = d_disq;
             es.taille = val_geo.longueur;
             InsereVarES (&es,vect_ul[0].show,&(val_geo.pos_es));
             AjouteConstanteBd (pos_init,exist_init,vect_ul[(*nb_ul)-1].show,&(val_geo.pos_initial));
             (*accepte_ds_bd) = TRUE;
             (*supprime_ligne)= FALSE;
             break;

           case b_titre_paragraf_dq :
             InsereTitreParagraphe (b_titre_paragraf_dq,ligne,profondeur,
                                     tipe_paragraf,&(val_geo.pos_paragraf));
             pos_paragraf = val_geo.pos_paragraf;
             if (!existe_repere (b_spec_titre_paragraf_dq))
               {
               val_geo.pos_specif_paragraf = 1;
               cree_bloc (b_spec_titre_paragraf_dq,sizeof (spec_titre_paragraf),0);
               }
             else
               {
               val_geo.pos_specif_paragraf = nb_enregistrements (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq) + 1;
               }
             switch (tipe_paragraf)
               {
               case fichier_dq :
                 spec_titre_paragraf.nb_fichier = (DWORD)((*nb_ul) - 1);
                 for (i = 0; i < (int)(spec_titre_paragraf.nb_fichier); i++)
                   {
                   bReconnaitConstante ((vect_ul+i+1),&val_log,&val_num,
                               &un_log,&un_num,&exist_init,&pos_init);
                   AjouteConstanteBd (pos_init,exist_init,vect_ul[i+1].show,&(spec_titre_paragraf.position_fichier[i]));
                   }
                 break;

               case ecrit_dq:
               case lit_dq :
                 es.sens = c_res_s ;
                 es.genre = c_res_numerique;
                 es.i_identif = position_ident;
                 es.n_desc = d_disq;
                 es.taille = 0;
                 StrCopy (ul.show, "1");
                 ul.genre = g_num;
                 InsereVarES (&es,vect_ul[1].show,&(spec_titre_paragraf.pos_es));


                 bReconnaitConstante(&ul,&val_log,&val_num,&un_log,&un_num,
                                &exist_init_var_controle,&pos_init_var_controle);
                 AjouteConstanteBd (pos_init_var_controle,exist_init_var_controle,ul.show,&(spec_titre_paragraf.pos_init));
                 break;

               case enr_dq :
                 es.sens = c_res_e ;
                 es.genre = c_res_numerique;
                 es.i_identif = position_ident;
                 es.n_desc = d_disq;
                 es.taille = 0;
                 StrCopy (ul.show, "1");
                 ul.genre = g_num;
                 InsereVarES (&es,vect_ul[1].show,&(spec_titre_paragraf.pos_es));
                 bReconnaitConstante(&ul,&val_log,&val_num,&un_log,&un_num,
                                &exist_init_var_controle,&pos_init_var_controle);
                 AjouteConstanteBd (pos_init_var_controle,exist_init_var_controle,ul.show,&(spec_titre_paragraf.pos_init));
                 break;

               default:
                 break;

               }
             insere_enr (szVERIFSource, __LINE__, 1,b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
             ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
             (*ptr_spec_titre_paragraf) = spec_titre_paragraf;
             (*accepte_ds_bd) = TRUE;
             (*supprime_ligne) = FALSE;
             break;


           default:
             break;

           }
         if (numero_repere != 0)
           {
           if (!existe_repere (b_geo_disq))
             {
             cree_bloc (b_geo_disq,sizeof (val_geo),0);
             }
           insere_enr (szVERIFSource, __LINE__, 1,b_geo_disq,ligne);
           ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,ligne);
           (*ptr_val_geo) = val_geo;
           un_paragraphe = (BOOL)(numero_repere == b_titre_paragraf_dq);

           MajDonneesParagraphe (b_titre_paragraf_dq,ligne,INSERE_LIGNE,
                                 un_paragraphe,profondeur,pos_paragraf);
           }
         }
       else
         {
         (*accepte_ds_bd) = FALSE;
         (*erreur_val) = erreur;
         }
       break;

    case SUPPRIME_LIGNE :
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      ptr_val_geo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,ligne);
      val_geo = (*ptr_val_geo);
      switch (val_geo.numero_repere)
        {

        case b_pt_constant :
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;

        case  b_e_s:
          if (!bSupprimeES (val_geo.pos_es))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.pos_initial);
            }
          break;

        case b_titre_paragraf_dq:
          un_paragraphe = TRUE;
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq, val_geo.pos_paragraf);
          profondeur = titre_paragraf->profondeur;
          tipe_paragraf = titre_paragraf->IdParagraphe;
          if ((tipe_paragraf == ecrit_dq) ||
          (tipe_paragraf == lit_dq) || (tipe_paragraf == enr_dq))
            {
            ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
            spec_titre_paragraf = (*ptr_spec_titre_paragraf) ;
            ptr_es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,spec_titre_paragraf.pos_es);
            if (ptr_es->ref != 0)
              {
              (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
              (*accepte_ds_bd) = TRUE;
              (*supprime_ligne) = FALSE;
              }
            }
          if ((*supprime_ligne))
            {
            if (!bSupprimeTitreParagraphe (b_titre_paragraf_dq,val_geo.pos_paragraf))
              {
              (*accepte_ds_bd) = TRUE;
              (*supprime_ligne) = FALSE;
              (*erreur_val) = IDSERR_SUPPRESSION_DU_PARAGRAPHE_IMPOSSIBLE_A_CETTE_POSITION;
              }
            else
              {
              maj_pointeur_paragraf_dans_geo (val_geo.pos_paragraf);
              ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
              spec_titre_paragraf = (*ptr_spec_titre_paragraf) ;
              switch (tipe_paragraf)
                {
                case fichier_dq :
                  for (i = 0; i < (int)(spec_titre_paragraf.nb_fichier); i++)
                    {
                    supprime_cst_bd_c (spec_titre_paragraf.position_fichier[i]);
                    }
                  break;

                case ecrit_dq :
                case lit_dq :
                case enr_dq :
                  bSupprimeES (spec_titre_paragraf.pos_es);
                  supprime_cst_bd_c (spec_titre_paragraf.pos_init);
                  break;

                default:
                  break;
                }
              enleve_enr (szVERIFSource, __LINE__, 1,b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
              maj_pointeur_spec_dans_geo(val_geo.pos_specif_paragraf);
              }
            }
            break;

        default:
          break;
        }
        if ((*supprime_ligne))
          {
          MajDonneesParagraphe (b_titre_paragraf_dq,ligne,SUPPRIME_LIGNE,
						un_paragraphe,profondeur,0); //pos_paragraf);
          enleve_enr (szVERIFSource, __LINE__, 1,b_geo_disq,ligne);
          }

      break;


    case CONSULTE_LIGNE :
      genere_vect_ul (ligne,vect_ul,nb_ul);
      break;

    default:
      break;

    }
  }


// ---------------------------------------------------------------------------
static void init_disq (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  if (!disque_deja_initialise)
    {
    ligne_depart_disq = 1;
    ligne_courante_disq = ligne_depart_disq;
    disque_deja_initialise=TRUE;
    }
  (*depart) = ligne_depart_disq;
  (*courant) = ligne_courante_disq;
  }

// ---------------------------------------------------------------------------
DWORD lis_nombre_de_ligne_disq (void)
  {
  DWORD  nombre_de_ligne_disq;

  if (existe_repere (b_geo_disq))
    {
    nombre_de_ligne_disq = nb_enregistrements (szVERIFSource, __LINE__, b_geo_disq);
    }
  else
    {
    nombre_de_ligne_disq = 0;
    }
  return (nombre_de_ligne_disq);
  }

// ---------------------------------------------------------------------------
static void set_tdb_disq (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_res[c_nb_car_message_res];

  StrSetNull (tdb);
  if ((existe_repere (b_titre_paragraf_dq)) && (nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf_dq) != 0))
    {
    if (nouvelle_ligne)
      {
      ligne--;
      }
    consulte_titre_paragraf_dq (ligne);
    if (!StrIsNull (contexte_dq))
      {
      bMotReserve (c_res_fichier, mot_res);
      StrConcat (tdb, mot_res);
      StrConcatChar (tdb, ' ');
      StrConcat (tdb, contexte_dq);
      }
    }
  }
// ---------------------------------------------------------------------------
static void fin_disq (BOOL quitte, DWORD courant)
  {
  if (!quitte)
    {
    ligne_courante_disq = courant;
    }
  }
// ---------------------------------------------------------------------------
static void creer_disq (void)
  {
  disque_deja_initialise = FALSE;
  }

// Interface du descripteur fichiers
void LisDescripteurDq (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_disq;
	pDescripteurCf->pfnInitDesc = init_disq;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_disq;
	pDescripteurCf->pfnValideLigne = valide_ligne_disq;
	pDescripteurCf->pfnSetTdb = set_tdb_disq;
	pDescripteurCf->pfnLisParamDlg = LIS_PARAM_DLG_DQ;
	pDescripteurCf->pfnFin = fin_disq;
	}


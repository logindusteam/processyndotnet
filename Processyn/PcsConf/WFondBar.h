
// WFondBar.h
// Gestion de Barres d'outils
  typedef struct
    {
    char *FileName;
    int  PositionInMenu;
    int  Mode;
    int   Id;
    } DESCRIPTION_BOUTON_BARRE_OUTIL, *PDESCRIPTION_BOUTON_BARRE_OUTIL;

  typedef struct
    {
    DWORD	NbBoutons;
    BOOL  bAvecBordure;
    DWORD  XSpaceBetweenItemBmp;
    DWORD  YSpaceBetweenItemBmp;
    //
    int  RelativePositionBmpMnu;
    DWORD  xOrigine;
    DWORD  yOrigine;
    //
    DESCRIPTION_BOUTON_BARRE_OUTIL *ItemBmpMnu;
    } DESCRIPTION_BARRE_OUTIL, *PDESCRIPTION_BARRE_OUTIL;



// ------------------------------------------------- Déclaration de fonctions
   HWND hwndCreeFondBarreOutil    
		(
		HWND      hwndParent,
		int     x,	// origine
		int     y,
		int     cx,	// taille
		int     cy,
		UINT    id,	// Identifiant
		PDESCRIPTION_BARRE_OUTIL pDescriptionBarreOutil
		);

   void FermeFondBarreOutil    (HWND hwndBarreOutil);

   
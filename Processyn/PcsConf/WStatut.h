
// ------------------------------------------------- 
// WStatut.h
// Gestion d'une fen�tre fille de statut (une ligne de texte)
// On sp�cifie le texte � l'aide de ::SetWindowText
// -------------------------------------------------

HWND hwndStatutInit 
	(HWND hwndParent,
	int     x,
	int     y,
	int     cx,
	int     cy,
	UINT    id
	);

void StatutDestroy (HWND hwndStatut);


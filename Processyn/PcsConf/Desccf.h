/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Interface de init interpreteur				|
 |	       								|
 |   Auteur  : AC							|
 |   Date    : 15/3/94 							|
 +----------------------------------------------------------------------*/

// --------------------------------------------------------------------------
// R�cup�re les adresses de proc d'un interpreteur s'il existe
// --------------------------------------------------------------------------
void RecupereDescripteurConfiguration (ID_DESCRIPTEUR IdDescripteur, PDESCRIPTEUR_CF pDescripteurCf);

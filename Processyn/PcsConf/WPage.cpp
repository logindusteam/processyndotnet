// PMPagecf.c
// Objet....: Gestion d'une Fen�tre editeur de texte
//            Initialisation/Vie/Finalisation

#include "stdafx.h"
#include "std.h"
#include "appli.h"
#include "MemMan.h"
#include "pmnucf.h"
#include "UEnv.h"
#include "WLigne.h"

#include "WPage.h"

// Police par d�faut
#define N_FONT_LIGNE 0
#define TAILLE_FONT_LIGNE 12
#define TAILLE_FONT_INTERNAL_LEADING 3
#define BITS_STYLE_FONT_LIGNE 0

// ------------------ Structure pour enregistrer les donn�es de wnd_page
typedef struct
  {
  USHORT	PageId;
  DWORD   NbLignesPage;

  DWORD	  LigneCourantePage;							// Il s'agit d'indexs
  DWORD	  ColonneCourantePage;
  DWORD   PremiereColonnePage;

  DWORD	  LigneMemoriseePage;
  DWORD	  ColonneMemoriseePage;
  DWORD   PremiereColonneMemoriseePage;

  DWORD   LigneMarqueePage;								// n� de la ligne marquee
  DWORD   ColonneMarqueePage;							// n� de la colonne marquee
  DWORD   PremiereColonneMarqueePage;

  HWND    hHScrollPage;										// Handle du Horizontal-Scroll
  HWND    hVScrollPage;

  BOOL		bLBoutonDownPage;								// �tat du bouton 1 ($$ ???)
  BOOL		bLBoutonDebutGlissePage;				// �tat mode gliss�

	BOOL		bCaretVisiblePage;							// visibilit� du curseur

	DWORD	dwNFont; // N� FONT (voir PCSFONT) utilis�e dans les lignes de la page
	DWORD dwTailleFont; // Taille en pixel de la police (voir PCSFONT) utilis�e dans les lignes de la page
	DWORD dwBitsStyleFont;
	DWORD wHauteurLigne; // Taille en pixel d'une ligne (contient InternalLeading en plus de dwTailleFont)
	BYTE byCharSetFont;
  } ENV_PAGE, *PENV_PAGE;

// -----------------------------------------------------------------------
// Contr�le et for�age du n� de la ligne courante d'une wnd_page
// -----------------------------------------------------------------------
static DWORD SetLigneCourantePage (PENV_PAGE pEnv, DWORD ln_cour)
  {
  DWORD derniere_ln;

  if (((int) ln_cour) <= 0)
    pEnv->LigneCourantePage = 0;
  else
    {
    derniere_ln = pEnv->NbLignesPage - 1;
    if (ln_cour >= derniere_ln)
      pEnv->LigneCourantePage = derniere_ln;
    else
      pEnv->LigneCourantePage = ln_cour;
    }

  return (pEnv->LigneCourantePage);
  }

// -----------------------------------------------------------------------
// Contr�le et for�age du n� de la colonne courante d'une wnd_page
// -----------------------------------------------------------------------
static DWORD SetColonneCourantePage (PENV_PAGE pEnv, DWORD col_cour)
  {
  pEnv->ColonneCourantePage = col_cour;

	// Positionnement de l'ascenseur
	SetScrollPos(pEnv->hHScrollPage, SB_HORZ, pEnv->ColonneCourantePage, FALSE);
	SetScrollRange (pEnv->hHScrollPage, SB_HORZ, 1, 255, TRUE);
  //::SendMessage (pEnv->hHScrollPage,SBM_SETSCROLLBAR,(WPARAM) ((SHORT) pEnv->ColonneCourantePage),MAKELONG (0, 255));
//  ::SendMessage (pEnv->hHScrollPage,SBM_SETTHUMBSIZE,MAKELONG ((USHORT) 1, (USHORT) 255),0L);

  return (pEnv->ColonneCourantePage);
  }

// -----------------------------------------------------------------------
// Determine le deplacement a effectuer sur le curseur pour acceder
// a la ligne, colonne
// -----------------------------------------------------------------------
static void CalculeDeplacementPage (PENV_PAGE pEnv, DWORD ligne, DWORD colonne, int *deplacement_ligne, int *deplacement_colonne)
  {
  *deplacement_ligne   = (int) ligne   - (int) pEnv->LigneCourantePage;
  *deplacement_colonne = (int) colonne - (int) pEnv->ColonneCourantePage;
  return;
  }

//// -----------------------------------------------------------------------
//// Renvoie la hauteur de la fonte utilis�e
//// -----------------------------------------------------------------------
//static DWORD GetHauteurCarPage (HWND hwnd)
//  {
//  HDC hdc = ::GetDC (hwnd);
//  TEXTMETRIC fm;
//  GetTextMetrics (hdc, &fm);
//  ::ReleaseDC (hwnd, hdc);
//
//  return ((DWORD) fm.tmHeight);
//  }


// -----------------------------------------------------------------------
// Init des donn�es d'une wnd_page
// -----------------------------------------------------------------------
static void InitEnvPage (PENV_PAGE pEnv)
  {
	// $$ v�rification pEnv
  pEnv->NbLignesPage = 0;
  //
  pEnv->LigneCourantePage = 0;
  pEnv->ColonneCourantePage = 0;
  pEnv->PremiereColonnePage = 0;
  //
  pEnv->LigneMemoriseePage = 0;
  pEnv->ColonneMemoriseePage = 0;
  pEnv->PremiereColonneMemoriseePage = 0;
  //
  pEnv->LigneMarqueePage = 0;
  pEnv->ColonneMarqueePage = 0;
  pEnv->PremiereColonneMarqueePage = 0;
  //
  pEnv->bLBoutonDownPage = FALSE;
  pEnv->bLBoutonDebutGlissePage = FALSE;
  pEnv->bCaretVisiblePage = TRUE;

	pEnv->dwNFont = N_FONT_LIGNE;
	pEnv->dwTailleFont = TAILLE_FONT_LIGNE;
	pEnv->dwBitsStyleFont = BITS_STYLE_FONT_LIGNE;
	pEnv->wHauteurLigne = TAILLE_FONT_LIGNE + TAILLE_FONT_INTERNAL_LEADING;
  return;
  }


// -----------------------------------------------------------------------
// Nom......: OnCreate
// Objet....: Traitement du message WM_CREATE de la page de travail
// Remarques: pCreateStruct = (CREATESTRUCT *) *pmp2
//            pointe sur une structure CREATESTRUCT
//$$ virer les param�tres et indirections inutiles pour tous les OnX
// -----------------------------------------------------------------------
static BOOL OnCreate (HWND hwnd, LPARAM mp2, LRESULT * pmres)
  {
	// Allocation du contexte de la fen�tre wnd_page
  PENV_PAGE	pEnv = (PENV_PAGE)pCreeEnv (hwnd, sizeof (ENV_PAGE));
  

  // Calcul de la taille de la page
  CREATESTRUCT * pCreateStruct = (CREATESTRUCT *) mp2;

  int cx_fen = pCreateStruct->cx;
  int cy_fen = pCreateStruct->cy;

	// Handles scroll bar = Handle de cette page
  pEnv->hHScrollPage = hwnd;
  pEnv->hVScrollPage = hwnd;

  // ----------------------- Init. m�moire pour info de la fen�tre � cr�er
  InitEnvPage (pEnv);
  pEnv->PageId = (UINT) pCreateStruct->hMenu;

  // -- Calcul du nombre de lignes pouvant �tre cr��es dans la fen�tre ---
  DWORD nb_lignes = ((DWORD) (cy_fen / pEnv->wHauteurLigne));

  // ----------------------- Init. m�moire pour info de la fen�tre � cr�er
  pEnv->NbLignesPage = nb_lignes;

  // ------------------ Cr�ation de nb_lignes Wnd_Line -------------------
  if (nb_lignes > 0)
    {
		int pos_y = cy_fen;
		DWORD i = 0;

    while (i < nb_lignes)
      {
      pos_y = pos_y - pEnv->wHauteurLigne;
      creer_wnd_line (hwnd, i, 0, pos_y, cx_fen, pEnv->wHauteurLigne,
				pEnv->dwNFont, pEnv->dwTailleFont, pEnv->dwBitsStyleFont, pEnv->byCharSetFont);
      i++;
      }
    if (pEnv->bCaretVisiblePage)
      donner_focus_wnd_line (::GetDlgItem (hwnd, pEnv->LigneCourantePage));
    }

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = (LRESULT) FALSE;
  return (FALSE);
  }

// -----------------------------------------------------------------------
// Traitement du message WM_DESTROY
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// -----------------------------------------------------------------------
static BOOL OnDestroy (HWND hwnd, LRESULT * pmres)
  {
  // Lib�ration m�moire pour info fen�tre
	bLibereEnv(hwnd);
  
  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return (TRUE);
  }

// -----------------------------------------------------------------------
// Ajuste la zone client de la page,
// en cr�ant, supprimant, redimensionnant les sous fen�tres de ligne
// et en changeant la ligne courante d'affichage si n�cessaire
// -----------------------------------------------------------------------
static void RedimensionnePage(HWND hwnd, // hwnd Page
												 int cxNew, // Largeur zone cliente page
												 int cyNew) // Hauteur zone cliente page
	{
	PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwnd);

	int old_nb_lignes = pEnv->NbLignesPage;

	// Calcul du nombre de lignes pouvant �tre affich�es dans la fen�tre
	int new_nb_lignes = ((DWORD) (cyNew / pEnv->wHauteurLigne));

	// cr�ation des nouvelles lignes ou destruction des lignes surnum�raires ----
	if (new_nb_lignes != old_nb_lignes)
		{
		if (new_nb_lignes > old_nb_lignes)
			{
			for (int i = old_nb_lignes; i < new_nb_lignes; i++)
				{
				creer_wnd_line (hwnd, i, 0, pEnv->wHauteurLigne * i, cxNew, pEnv->wHauteurLigne,
					pEnv->dwNFont, pEnv->dwTailleFont, pEnv->dwBitsStyleFont, pEnv->byCharSetFont);
				}
			}
		else
			{
			for (int i = new_nb_lignes; i < old_nb_lignes; i++)
				{
				detruire_wnd_line (::GetDlgItem (hwnd, i));
				}
			}
		}

	// Init. m�moire pour info de la fen�tre � cr�er
	pEnv->NbLignesPage = new_nb_lignes;
	DWORD old_ligne_cour = pEnv->LigneCourantePage;
	if ((int)old_ligne_cour >= new_nb_lignes)
		{
		SetLigneCourantePage (pEnv, new_nb_lignes - 1);
		}
	DWORD new_ligne_cour = pEnv->LigneCourantePage;

	// Positionnement des lignes par rapport � la fenetre
	if (new_nb_lignes > 0)
		{
		for (int i = 0; i < new_nb_lignes; i++)
			{ // $$ faire un d�placement global des fen�tres
			::MoveWindow (::GetDlgItem (hwnd, i), 0, pEnv->wHauteurLigne * i,
				cxNew, pEnv->wHauteurLigne,TRUE);
			}
		}

	notification_user (Appli.hwnd,  pEnv->PageId, NU0_WND_PAGE_SIZE,
		LOWORD(new_ligne_cour - old_ligne_cour), 0); //decalage eventuel
	}

// -----------------------------------------------------------------------
// Traitement du message WM_SIZE
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// Remarques: cxNew = (USHORT) (mp2);                     new width
//            cyNew = HIWORD (mp2);                     new height
// -----------------------------------------------------------------------
static BOOL OnSize (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
	if (mp1 != SIZE_MINIMIZED)
		{
		// Nouvelle taille de la Fen�tre
		int	cxNew = LOWORD (mp2);
		int	cyNew = HIWORD (mp2);

		// Redimensionne la page
		RedimensionnePage (hwnd, cxNew, cyNew);
		}

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return (TRUE);
  }

// -----------------------------------------------------------------------
// Nom......: OnHScroll
// Objet....: Traitement du message WM_HSCROLL
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// Remarques: celui ci est rerout� sur Appli.hwnd
// Notification :id = (USHORT) (mp1);         control-window identifier
//            sPos = (USHORT) (mp2);                 slider position
//            usCmd = HIWORD (mp2);                        command
// -----------------------------------------------------------------------
static BOOL OnHScroll (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwnd);

  ::SendMessage (Appli.hwnd, msg, (WPARAM) (pEnv->PageId), mp1);

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return (TRUE);
  }

// -----------------------------------------------------------------------
// Nom......: OnVScroll
// Objet....: Traitement du message WM_VSCROLL
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// Remarques: notification : id = (USHORT) (mp1); control-window ID
//            sPos = (USHORT) (mp2);               slider position
//            usCmd = HIWORD (mp2);              command
// -----------------------------------------------------------------------
static BOOL OnVScroll (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwnd);

  ::SendMessage (Appli.hwnd, msg, (WPARAM) (pEnv->PageId), mp1);

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return (TRUE);
  }

// -----------------------------------------------------------------------
// Nom......: OnUser
// Objet....: Traitement du message WM_USER
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// Remarques: notif  = (USHORT) (*pmp1);   window notification
//            id     = HIWORD (*pmp1);   window ID
//            param1 = (USHORT) (*pmp2);
//            param2 = HIWORD (*pmp2);
// -----------------------------------------------------------------------
static BOOL OnUser (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  BOOL     bTraite = FALSE;
  PENV_PAGE	pEnv;
  int      delta_ln;
  int      delta_col;
  HWND     hwnd_line;
  int    sNbLigneDeplacement;
  DWORD    nb_line;
  RECT     rcl_line;
	LONG		 lHeightLine;

  switch ((USHORT) (mp1))
		{
		case NU0_WND_LINE_BN_1_DOWN:
			pEnv = (PENV_PAGE)pGetEnv(hwnd);
			pEnv->bLBoutonDownPage = TRUE;
			pEnv->bLBoutonDebutGlissePage = FALSE;
			SetCapture (::GetDlgItem (hwnd, HIWORD (mp1)));
			CalculeDeplacementPage (pEnv, HIWORD (mp1), LOWORD (mp2), &delta_ln, &delta_col);
			notification_user (Appli.hwnd, pEnv->PageId, NU0_WND_PAGE_FIN_GLISSE, 0, 0);
			notification_user (Appli.hwnd, pEnv->PageId, NU0_WND_PAGE_DEPLACE_CURSEUR, (WORD)delta_ln, (WORD)delta_col);
			bTraite = TRUE;
			break;

		case NU0_WND_LINE_BN_1_UP:
			pEnv = (PENV_PAGE)pGetEnv(hwnd);
			pEnv->bLBoutonDownPage = FALSE;
			pEnv->bLBoutonDebutGlissePage = FALSE;
			ReleaseCapture ();
			bTraite = TRUE;
			break;

		case NU0_WND_LINE_BN_1_DBL_CLICK:
			{
			pEnv = (PENV_PAGE)pGetEnv(hwnd);
			pEnv->bLBoutonDownPage = FALSE;
			pEnv->bLBoutonDebutGlissePage = FALSE;
			ReleaseCapture ();
			notification_user (Appli.hwnd, pEnv->PageId, NU0_WND_PAGE_BN_1_DBL_CLICK, 0, 0);
			bTraite = TRUE;
			}
			break;

		case NU0_WND_LINE_LOSE_MOUSE:
			pEnv = (PENV_PAGE)pGetEnv(hwnd);
			if (pEnv->bLBoutonDownPage)
				{

				if (!pEnv->bLBoutonDebutGlissePage)
					{
					pEnv->bLBoutonDebutGlissePage = TRUE;
					notification_user (Appli.hwnd, pEnv->PageId, NU0_WND_PAGE_DEBUT_GLISSE, 0, 0);
					}

				// Recherche Id Ligne ou se trouve la souris maintenant	
				nb_line = pEnv->NbLignesPage;
				sNbLigneDeplacement = 0;
				hwnd_line = ::GetDlgItem (hwnd, 0); // premiere ligne
				::GetWindowRect (hwnd_line, &rcl_line);
				lHeightLine = rcl_line.bottom - rcl_line.top;
				sNbLigneDeplacement = (SHORT) HIWORD (mp2) / lHeightLine;
				if (sNbLigneDeplacement >= 0)
					{ 
					// en dessous
				  if ((DWORD)(HIWORD (mp1) + sNbLigneDeplacement + 1) > nb_line) 
					  {
					  // hors de la page 
					  sNbLigneDeplacement = (int)(nb_line - HIWORD (mp1) - 1);
						}
  				}
				else
					{
					// en dessus
					if ((HIWORD (mp1) + sNbLigneDeplacement) < 0)
						{
						// hors de la page
						sNbLigneDeplacement = - (HIWORD (mp1));
						}
					}
				// $$ attention risque d'overflow si d�placement > 32767
			  notification_user (Appli.hwnd, pEnv->PageId, NU0_WND_PAGE_DEPLACE_CURSEUR, (SHORT)sNbLigneDeplacement, 0);
			  hwnd_line = ::GetDlgItem (hwnd, HIWORD (mp1)+sNbLigneDeplacement);
			  SetCapture (hwnd_line);
				}
			bTraite = TRUE;
			break;

		default:
			break;
		}

  *pmres = (LRESULT) NULL;
  return (bTraite);
  }



// -----------------------------------------------------------------------
// Nom......: wndprocPage
// Objet....: Traitement des messages de la fen�tre client
// Retour...: LRESULT: r�sultat du traitement du message
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocPage (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;                         // Valeur de retour
  BOOL     bTraite = TRUE;                  // Indique commande non trait�e

  // ------------ Switch message re�u.... --------------------------------
  switch (msg)
    {
    case WM_CREATE:
			bTraite = OnCreate (hwnd, mp2, &mres);
			break;
			
    case WM_DESTROY:
			bTraite = OnDestroy (hwnd, &mres);
			break;
			// $$ manque WM_SETFOCUS
    case WM_USER:
			bTraite = OnUser (hwnd, mp1, mp2, &mres);
			break;
			
    case WM_HSCROLL:
			bTraite = OnHScroll (hwnd, msg, mp1, mp2, &mres);
			break;
			
    case WM_VSCROLL:
			bTraite = OnVScroll (hwnd, msg, mp1, mp2, &mres);
			break;
			
    case WM_SIZE:
			bTraite = OnSize (hwnd, mp1, mp2, &mres);
			break;
			
    default:
			bTraite = FALSE;
			break;
    } // Switch

  // -------- Si message non trait�, traitement par d�faut ---------------
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  } // wndprocPage


// -----------------------------------------------------------------------
// Cr�ation et initialisation de la fen�tre Wnd_Page
// Renvoie NULL si la fen�tre n'a pas �t� cr��e et initialis�e correctement.
// -----------------------------------------------------------------------
HWND creer_wnd_page (HWND hwndParent, HWND hwndOwner, UINT id,
                   int pos_x_init, int pos_y_init, int taille_x, int taille_y)
  {
  HWND     hwndPage;
	static BOOL bClasseWPageEnregistree = FALSE;
	static char szClassePage[] = "Page";


  // ------------------ Cr�ation d'une Class de type Wnd_Page ------------
  if (!bClasseWPageEnregistree)
    {
		WNDCLASS wc;

		wc.style					= 0; //CS_HREDRAW | CS_VREDRAW; 
    wc.lpfnWndProc		= wndprocPage; 
    wc.cbClsExtra			= 0; 
    wc.cbWndExtra			= 0; 
    wc.hInstance			= Appli.hinst; 
    wc.hIcon					= NULL;
    wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
    wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
    wc.lpszMenuName		= NULL; 
    wc.lpszClassName	= szClassePage; 

     bClasseWPageEnregistree = (BOOL) RegisterClass (&wc);
    }

  // ------------------ Cr�ation de la Wnd_Page de l'�diteur --------------------------
  if (bClasseWPageEnregistree)
    {
    hwndPage = CreateWindowEx 
			(WS_EX_CLIENTEDGE, szClassePage,"",
			WS_VISIBLE | WS_CHILD | WS_HSCROLL | WS_VSCROLL,
			pos_x_init, pos_y_init,	taille_x, taille_y,
			hwndParent, (HMENU)id, Appli.hinst, NULL);
    }

  // ------------------ Retour de la fonction ----------------------------
  return (hwndPage);
  }


// -----------------------------------------------------------------------
// Nom......: detruire_wnd_page
// Objet....: Suppression de la fen�tre Wnd_Page
// Entr�es..:
// Sorties..:
// Retour...:
// Remarques:
// -----------------------------------------------------------------------
void detruire_wnd_page (HWND hwndPage)
  {
  ::DestroyWindow (hwndPage);
  return;
  }

// -----------------------------------------------------------------------
// Nom......: effacer_wnd_page
// Objet....: Effacement de la fen�tre Wnd_Page
// Entr�es..:
// Sorties..:
// Retour...:
// Remarques: curseur en (0,0) + scroll_col <- 0 + txt_ln <- "" + reaffiche
// -----------------------------------------------------------------------
void effacer_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     i;
  DWORD     nb_lignes_wnd_page;

  SetLigneCourantePage  (pEnv, 0);
  SetColonneCourantePage (pEnv, 0);
  pEnv->PremiereColonnePage = 0;

  nb_lignes_wnd_page = pEnv->NbLignesPage;
  if (nb_lignes_wnd_page > 0)
    {
    for (i = 0; i < nb_lignes_wnd_page; i++)
      {
      initialiser_wnd_line (::GetDlgItem (hwndPage, (USHORT) i));
      //il faut tout r�initialiser, m�me l'�tat, la position du curseur...
      //effacer_wnd_line (::GetDlgItem (hwndPage, (USHORT) i));
      }

    if (pEnv->bCaretVisiblePage)
      {
      donner_focus_wnd_line (::GetDlgItem (hwndPage, pEnv->LigneCourantePage));
      }
    }

  return;
  }

// -----------------------------------------------------------------------
// Nom......: ecrire_wnd_page
// Objet....: Ecriture d'un texte � la ligne courante d'une fen�tre Wnd_Page
// Entr�es..:
// Sorties..:
// Retour...:
// Remarques: reaffiche
// -----------------------------------------------------------------------
void ecrire_wnd_page (HWND hwndPage, char *texte, DWORD etat_ligne)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  if (pEnv->NbLignesPage > 0)
    {
    ecrire_wnd_line (::GetDlgItem (hwndPage, pEnv->LigneCourantePage), texte, etat_ligne);
    }
  return;
  }

// -----------------------------------------------------------------------
// R�cuperer le nombre de lignes d'une fen�tre Wnd_Page
// -----------------------------------------------------------------------
DWORD nombre_ligne_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);

  return (pEnv->NbLignesPage);
  }

// -----------------------------------------------------------------------
// R�cuperer les coordonn�es du curseur d'une fen�tre Wnd_Page
// -----------------------------------------------------------------------
void position_curseur_wnd_page (HWND hwndPage, DWORD *ligne, DWORD *colonne)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);

  if (pEnv->NbLignesPage > 0)
    {
    *ligne = pEnv->LigneCourantePage;
    *colonne = position_curseur_wnd_line (::GetDlgItem (hwndPage, *ligne));
    (*ligne)++;                 //Conversion index --> position
    }
  else
    {
    *ligne = 0;
    *colonne = 0;
    }
  }

// -----------------------------------------------------------------------
// R�cuperer le texte de la ligne courante d'une fen�tre Wnd_Page
// -----------------------------------------------------------------------
void texte_cour_wnd_page (HWND hwndPage, char *texte)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     ln_courante;

  if (pEnv->NbLignesPage > 0)
    {
    ln_courante = pEnv->LigneCourantePage;
    texte_wnd_line (::GetDlgItem (hwndPage, (USHORT) ln_courante), texte);
    }
  return;
  }

// -----------------------------------------------------------------------
// D�placement vertical du texte
// Remarques: d�calage > 0 ==> d�placement du texte du bas vers le haut
//            les nouvelles lignes visualis�es sont effac�e + r�affiche
// -----------------------------------------------------------------------
void scroller_ligne_wnd_page (HWND hwndPage, int decalage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     nb_lignes_wnd_page;
  DWORD     i_destination;
  DWORD     i_fin;
  DWORD     i;
  HWND     hwnd_wnd_line_source;
  HWND     hwnd_wnd_line_destination;

  nb_lignes_wnd_page = pEnv->NbLignesPage;

  if ((nb_lignes_wnd_page != 0) && (decalage != 0))
    {
    if (decalage > 0)
      {
      i_destination = 0;
      i_fin = nb_lignes_wnd_page - ((DWORD) decalage);

      for (i = i_destination; i < i_fin; i++)
        {
        hwnd_wnd_line_destination = ::GetDlgItem (hwndPage, i);
        hwnd_wnd_line_source = ::GetDlgItem (hwndPage, (DWORD) (i + decalage));
        copier_wnd_line (hwnd_wnd_line_destination, hwnd_wnd_line_source);
        }
      for (i = i_fin; i < nb_lignes_wnd_page; i++)
        {
        hwnd_wnd_line_destination = ::GetDlgItem (hwndPage, i);
        effacer_wnd_line (hwnd_wnd_line_destination);
        }
      }
    else
      {
      i_destination = nb_lignes_wnd_page - 1;
      i_fin = ((DWORD) - decalage) - 1;            // Le decalage est n�gatif
      for (i = i_destination; i > i_fin; i--)
        {
        hwnd_wnd_line_destination = ::GetDlgItem (hwndPage, i);
        hwnd_wnd_line_source = ::GetDlgItem (hwndPage, (DWORD) (i + decalage));
        copier_wnd_line (hwnd_wnd_line_destination, hwnd_wnd_line_source);
        }
      for (i = 0; i <= i_fin; i++)
        {
        hwnd_wnd_line_destination = ::GetDlgItem (hwndPage, i);
        effacer_wnd_line (hwnd_wnd_line_destination);
        }
      }
    }

  return;
  }


// -----------------------------------------------------------------------
// Nom......: scroller_colonne_wnd_page
// Objet....: D�placement vertical du texte
// Remarques: d�calage > 0 ==> d�placement du texte de la droite vers la gauche
//            reaffiche
// -----------------------------------------------------------------------
void scroller_colonne_wnd_page (HWND hwndPage, int decalage)
  {
  HWND     hwnd_wnd_line_courante;
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     ligne_courante;
  DWORD     premiere_colonne_visible;
  DWORD     nb_lignes_wnd_page;
  DWORD     i;

  if (pEnv->NbLignesPage > 0)
    {
    ligne_courante = pEnv->LigneCourantePage;
    hwnd_wnd_line_courante = ::GetDlgItem (hwndPage, ligne_courante);

    premiere_colonne_visible = pEnv->PremiereColonnePage;
    premiere_colonne_visible = (DWORD) (((int) premiere_colonne_visible) + decalage);
    premiere_colonne_visible = position_car_visible_wnd_line (hwnd_wnd_line_courante, premiere_colonne_visible);
    pEnv->PremiereColonnePage = premiere_colonne_visible;

    nb_lignes_wnd_page = pEnv->NbLignesPage;
    for (i = 0; i < nb_lignes_wnd_page; i++)
      {
      position_car_visible_wnd_line (::GetDlgItem (hwndPage, i), premiere_colonne_visible);
      }
    }
  }

// -----------------------------------------------------------------------
// Nom......: aller_debut_ligne_wnd_page
// Objet....: Positionner le curseur en d�but de la ligne courante d'un Wnd_Page
// Remarques: + scroll en colonnes de toutes les lignes
//            reaffiche
// -----------------------------------------------------------------------
void aller_debut_ligne_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     i;
  DWORD     nb_lignes_wnd_page;

  nb_lignes_wnd_page = pEnv->NbLignesPage;
  if (nb_lignes_wnd_page > 0)
    {
    pEnv->PremiereColonnePage = 0;
    SetColonneCourantePage (pEnv, 0);

    for (i = 0; i < nb_lignes_wnd_page; i++)
      {
      position_car_visible_wnd_line (::GetDlgItem (hwndPage, i), 0);
      position_car_courant_wnd_line (::GetDlgItem (hwndPage, i), 0);
      }
    }
  }

// -----------------------------------------------------------------------
// Positionner le curseur en d�but de la ligne courante d'un Wnd_Page
// Remarques: + scroll en colonnes de toutes les lignes
//            reaffiche
// -----------------------------------------------------------------------
void aller_fin_ligne_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  HWND     hwnd_wnd_line_courante;
  DWORD     ligne_courante;
  DWORD     colonne_courante;
  int      decalage_col_a_faire;
  DWORD     i;
  DWORD     nb_lignes_wnd_page;

  nb_lignes_wnd_page = pEnv->NbLignesPage;
  if (nb_lignes_wnd_page > 0)
    {
    ligne_courante = pEnv->LigneCourantePage;
    hwnd_wnd_line_courante = ::GetDlgItem (hwndPage, ligne_courante);

    colonne_courante = longueur_texte_wnd_line (hwnd_wnd_line_courante);
    colonne_courante = position_car_courant_wnd_line (hwnd_wnd_line_courante, colonne_courante);
    SetColonneCourantePage (pEnv, colonne_courante);

    for (i = 0; i < nb_lignes_wnd_page; i++)
      {
      position_car_courant_wnd_line (::GetDlgItem (hwndPage, i), colonne_courante);
      }

    decalage_col_a_faire = scroll_curs_visible_wnd_line (hwnd_wnd_line_courante);
    if (decalage_col_a_faire != 0)
      {
      scroller_colonne_wnd_page (hwndPage, decalage_col_a_faire);
      }
    }
  }

// -----------------------------------------------------------------------
// Objet....: D�placer le curseur
// Remarques: reaffiche
// -----------------------------------------------------------------------
void monter_curseur_wnd_page (HWND hwndPage, DWORD *decalage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     old_ln_courante;
  DWORD     new_ln_courante;

  if (pEnv->NbLignesPage > 0)
    {
    old_ln_courante = pEnv->LigneCourantePage;
    new_ln_courante = (int) SetLigneCourantePage (pEnv, old_ln_courante - *decalage);
    *decalage = old_ln_courante - new_ln_courante;
    if (pEnv->bCaretVisiblePage)
      {
      enlever_focus_wnd_line (::GetDlgItem (hwndPage, old_ln_courante));
      donner_focus_wnd_line (::GetDlgItem (hwndPage, new_ln_courante));
      }
    }
  return;
  }

// -----------------------------------------------------------------------
// Objet....: D�placer le curseur
// Remarques: reaffiche
// -----------------------------------------------------------------------
void descendre_curseur_wnd_page (HWND hwndPage, DWORD & decalage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);

  if (pEnv->NbLignesPage > 0)
    {
    DWORD old_ln_courante = pEnv->LigneCourantePage;
    DWORD new_ln_courante = (int) SetLigneCourantePage (pEnv, old_ln_courante + decalage);
    decalage = new_ln_courante - old_ln_courante;
    if (pEnv->bCaretVisiblePage)
      {
      enlever_focus_wnd_line (::GetDlgItem (hwndPage, old_ln_courante));
      donner_focus_wnd_line (::GetDlgItem (hwndPage, new_ln_courante));
      }
    }
  }


// -----------------------------------------------------------------------
// Objet....: D�placer le curseur
// Remarques: scroll si necessaire
//            reaffiche
// -----------------------------------------------------------------------
void droiter_curseur_wnd_page (HWND hwndPage, DWORD decalage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  HWND     hwnd_wnd_line_courante;
  DWORD     ligne_courante;
  DWORD     colonne_courante;
  DWORD     nb_lignes_wnd_page;
  DWORD     i;
  int      decalage_col_a_faire;

  nb_lignes_wnd_page = pEnv->NbLignesPage;
  if (nb_lignes_wnd_page > 0)
    {
    ligne_courante = pEnv->LigneCourantePage;
    hwnd_wnd_line_courante = ::GetDlgItem (hwndPage, ligne_courante);
    colonne_courante = pEnv->ColonneCourantePage;
    colonne_courante = colonne_courante + (DWORD)decalage;
    colonne_courante = position_car_courant_wnd_line (hwnd_wnd_line_courante, colonne_courante);
    SetColonneCourantePage (pEnv, colonne_courante);

    for (i = 0; i < nb_lignes_wnd_page; i++)
      {
      position_car_courant_wnd_line (::GetDlgItem (hwndPage, i), colonne_courante);
      }

    decalage_col_a_faire = scroll_curs_visible_wnd_line (hwnd_wnd_line_courante);
    if (decalage_col_a_faire != 0)
      {
      scroller_colonne_wnd_page (hwndPage, decalage_col_a_faire);
      }
    }
  }

// -----------------------------------------------------------------------
// Objet....: D�placer le curseur
// Remarques: scroll si necessaire
//            reaffiche
// -----------------------------------------------------------------------
void gaucher_curseur_wnd_page (HWND hwndPage, DWORD decalage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  HWND     hwnd_wnd_line_courante;
  DWORD     ligne_courante;
  DWORD     colonne_courante;
  DWORD     nb_lignes_wnd_page;
  DWORD     i;
  int      decalage_col_a_faire;

  ligne_courante = pEnv->LigneCourantePage;
  hwnd_wnd_line_courante = ::GetDlgItem (hwndPage, ligne_courante);
  colonne_courante = pEnv->ColonneCourantePage;
  colonne_courante = colonne_courante - decalage;
  colonne_courante = position_car_courant_wnd_line (hwnd_wnd_line_courante, colonne_courante);
  SetColonneCourantePage (pEnv, colonne_courante);

  nb_lignes_wnd_page = pEnv->NbLignesPage;
  for (i = 0; i < nb_lignes_wnd_page; i++)
    {
    position_car_courant_wnd_line (::GetDlgItem (hwndPage, i), colonne_courante);
    }

  decalage_col_a_faire = scroll_curs_visible_wnd_line (hwnd_wnd_line_courante);
  if (decalage_col_a_faire != 0)
    {
    scroller_colonne_wnd_page (hwndPage, decalage_col_a_faire);
    }
  }


// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void inserer_ligne_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     ligne_courante = pEnv->LigneCourantePage;
  DWORD     colonne_courante = pEnv->ColonneCourantePage;
  DWORD     nb_lignes_wnd_page = pEnv->NbLignesPage;
  DWORD     derniere_ligne_wnd_page = nb_lignes_wnd_page - 1;
  DWORD     i;
  DWORD     decalage;
  HWND     hwnd_wnd_line_source;
  HWND     hwnd_wnd_line_destination;

  if (ligne_courante == derniere_ligne_wnd_page)
    {
    if (colonne_courante == 0)
      {
      effacer_wnd_line (::GetDlgItem (hwndPage, ligne_courante));
      }
    else
      {
      scroller_ligne_wnd_page (hwndPage, 1);
      effacer_wnd_line (::GetDlgItem (hwndPage, ligne_courante));
      SetColonneCourantePage (pEnv, 0);
      //aller_debut_ligne_fenetre (repere);
      }
    }
  else
    {
    if (colonne_courante != 0)
      {
      decalage = 1;
      descendre_curseur_wnd_page (hwndPage, decalage);
      SetColonneCourantePage (pEnv, 0);
      //aller_debut_ligne_fenetre (repere);
      }
    pEnv = (PENV_PAGE)pGetEnv(hwndPage);
    ligne_courante = pEnv->LigneCourantePage;

    if (nb_lignes_wnd_page > 0)
      {
      for (i = derniere_ligne_wnd_page; i > ligne_courante; i--)
        {
        hwnd_wnd_line_destination = ::GetDlgItem (hwndPage, i);
        hwnd_wnd_line_source = ::GetDlgItem (hwndPage, (DWORD) (i - 1));
        copier_wnd_line (hwnd_wnd_line_destination, hwnd_wnd_line_source);
        }
      effacer_wnd_line (::GetDlgItem (hwndPage, ligne_courante));
      }
    }
  }

// -----------------------------------------------------------------------
// Objet....: insere un caractere a la position courante du curseur
// Remarques: droiter_curseur + reaffiche
// -----------------------------------------------------------------------
void inserer_car_wnd_page (HWND hwndPage, char caractere)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     ligne_courante = pEnv->LigneCourantePage;

  inserer_car_wnd_line (::GetDlgItem (hwndPage, ligne_courante), caractere);
  droiter_curseur_wnd_page (hwndPage, 1);
  }

// -----------------------------------------------------------------------
// Objet....: remplacer un caractere a la position courante du curseur
// Remarques: droiter_curseur + reaffiche
// -----------------------------------------------------------------------
void modifier_car_wnd_page (HWND hwndPage, char caractere)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     ligne_courante;

  ligne_courante = pEnv->LigneCourantePage;
  modifier_car_wnd_line (::GetDlgItem (hwndPage, ligne_courante), caractere);
  droiter_curseur_wnd_page (hwndPage, 1);
  }

// -----------------------------------------------------------------------
// Objet....: supprimer le caractere a la position courante du curseur
// Remarques: reaffiche
// -----------------------------------------------------------------------
void supprimer_car_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     ligne_courante = pEnv->LigneCourantePage;

  supprimer_car_wnd_line (::GetDlgItem (hwndPage, ligne_courante));
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void aller_haut_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD       old_ln_courante = pEnv->LigneCourantePage;
  DWORD       new_ln_courante = SetLigneCourantePage (pEnv, 0);

  if (pEnv->bCaretVisiblePage)
    {
    enlever_focus_wnd_line (::GetDlgItem (hwndPage, old_ln_courante));
    donner_focus_wnd_line (::GetDlgItem (hwndPage, new_ln_courante));
    }
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void aller_bas_wnd_page  (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD       old_ln_courante = pEnv->LigneCourantePage;
  DWORD       new_ln_courante = SetLigneCourantePage (pEnv, pEnv->NbLignesPage - 1);

  if (pEnv->bCaretVisiblePage)
    {
    enlever_focus_wnd_line (::GetDlgItem (hwndPage, old_ln_courante));
    donner_focus_wnd_line (::GetDlgItem (hwndPage, new_ln_courante));
    }
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void droiter_mot_curseur_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD       ln_courante = pEnv->LigneCourantePage;
  DWORD       old_colonne = pEnv->ColonneCourantePage;
  DWORD       new_colonne = droiter_mot_curseur_wnd_line (::GetDlgItem (hwndPage, (USHORT) ln_courante));

  droiter_curseur_wnd_page (hwndPage, new_colonne - old_colonne);
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void gaucher_mot_curseur_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD       ln_courante = pEnv->LigneCourantePage;
  DWORD       old_colonne = pEnv->ColonneCourantePage;
  DWORD       new_colonne = gaucher_mot_curseur_wnd_line (::GetDlgItem (hwndPage, (USHORT) ln_courante));

  gaucher_curseur_wnd_page (hwndPage, old_colonne - new_colonne);
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void reajuster_ascenseur_wnd_page (HWND hwndPage, DWORD pos_relative_ligne, DWORD taille_relative)
  {
	SetScrollPos (hwndPage, SB_VERT, pos_relative_ligne, FALSE);
	SetScrollRange (hwndPage, SB_VERT, 1, taille_relative, TRUE);
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void memoriser_curseur_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);

  pEnv->PremiereColonneMemoriseePage = pEnv->PremiereColonnePage;
  pEnv->ColonneMemoriseePage = pEnv->ColonneCourantePage;
  pEnv->LigneMemoriseePage = pEnv->LigneCourantePage;
  }
// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void restaurer_curseur_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD       nb_lignes_wnd_page = pEnv->NbLignesPage;
  DWORD       i;
  DWORD       old_ln_cour;

  if (nb_lignes_wnd_page > 0)
    {
    old_ln_cour = pEnv->LigneCourantePage;
    if (old_ln_cour > pEnv->LigneMemoriseePage)
      {
      SetLigneCourantePage (pEnv, pEnv->LigneMemoriseePage);
      }
    SetColonneCourantePage (pEnv, pEnv->ColonneMemoriseePage);
    pEnv->PremiereColonnePage = pEnv->PremiereColonneMemoriseePage;

    for (i = 0; i < nb_lignes_wnd_page; i++)
      {
      position_car_visible_wnd_line (::GetDlgItem (hwndPage, i), pEnv->PremiereColonnePage);
      position_car_courant_wnd_line (::GetDlgItem (hwndPage, i), pEnv->ColonneCourantePage);
      }

    if (pEnv->bCaretVisiblePage)
      {
      enlever_focus_wnd_line (::GetDlgItem (hwndPage, old_ln_cour));
      donner_focus_wnd_line (::GetDlgItem (hwndPage, pEnv->LigneCourantePage));
      }
    }
  }

// -----------------------------------------------------------------------
void sauver_marque_wnd_page    (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);

  pEnv->PremiereColonneMarqueePage = pEnv->PremiereColonnePage;
  pEnv->ColonneMarqueePage = pEnv->ColonneCourantePage;
  pEnv->LigneMarqueePage = pEnv->LigneCourantePage;
  }

// -----------------------------------------------------------------------
void restaurer_marque_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD       nb_lignes_wnd_page = pEnv->NbLignesPage;

  if (nb_lignes_wnd_page > 0)
    {
		DWORD       i;
		DWORD       old_ln_cour;

    old_ln_cour = pEnv->LigneCourantePage;
    SetLigneCourantePage  (pEnv, pEnv->LigneMarqueePage);
    SetColonneCourantePage (pEnv, pEnv->ColonneMarqueePage);
    pEnv->PremiereColonnePage = pEnv->PremiereColonneMarqueePage;

    for (i = 0; i < nb_lignes_wnd_page; i++)
      {
      position_car_visible_wnd_line (::GetDlgItem (hwndPage, i), pEnv->PremiereColonnePage);
      position_car_courant_wnd_line (::GetDlgItem (hwndPage, i), pEnv->ColonneCourantePage);
      }

    if (pEnv->bCaretVisiblePage)
      {
      enlever_focus_wnd_line (::GetDlgItem (hwndPage, old_ln_cour));
      donner_focus_wnd_line (::GetDlgItem (hwndPage, pEnv->LigneCourantePage));
      }
    }
  }

// -----------------------------------------------------------------------
void afficher_select_ligne_wnd_page (HWND hwndPage, BOOL mode_selection)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD     ln_courante = pEnv->LigneCourantePage;

  afficher_select_ligne_wnd_line (::GetDlgItem (hwndPage, (USHORT) ln_courante), mode_selection);
  }

// -----------------------------------------------------------------------
void abandon_selection_wnd_page     (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);
  DWORD       nb_lignes_wnd_page = pEnv->NbLignesPage;

  while (nb_lignes_wnd_page > 0)
    {
    nb_lignes_wnd_page--;
    afficher_select_ligne_wnd_line (::GetDlgItem (hwndPage, (USHORT) nb_lignes_wnd_page), FALSE);
    }
  }

// -----------------------------------------------------------------------
void curseur_invisible_wnd_page (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);

  pEnv->bCaretVisiblePage = FALSE;
  enlever_focus_wnd_line (::GetDlgItem (hwndPage, pEnv->LigneCourantePage));
  }

// -----------------------------------------------------------------------
void curseur_visible_wnd_page   (HWND hwndPage)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwndPage);

  pEnv->bCaretVisiblePage = TRUE;
  donner_focus_wnd_line (::GetDlgItem (hwndPage, pEnv->LigneCourantePage));
  }

// -----------------------------------------------------------------------
// Modifie la police des lignes
// Remarques: raffraichit l'�cran
// -----------------------------------------------------------------------
void PageChangePolice (HWND hwnd, DWORD dwNFont, DWORD dwTailleFont, DWORD dwConfBitsStyle, BYTE byConfCharSetFont)
  {
  PENV_PAGE	pEnv = (PENV_PAGE)pGetEnv(hwnd);
	pEnv->dwTailleFont = dwTailleFont;
	pEnv->dwNFont = dwNFont;
	pEnv->dwBitsStyleFont = dwConfBitsStyle;
	pEnv->byCharSetFont = byConfCharSetFont;

	// Modification des polices des lignes existantes
	for (DWORD i = 0; i < pEnv->NbLignesPage; i++)
		{
		LigneChangePolice	(::GetDlgItem (hwnd, (int)i),pEnv->dwNFont,pEnv->dwTailleFont, pEnv->dwBitsStyleFont, pEnv->byCharSetFont);
		}
	if (pEnv->NbLignesPage > 0)
		{
		pEnv->wHauteurLigne = LigneHauteur (::GetDlgItem (hwnd, (int)0));
		}

	// R�organise la page si n�cessaire
	RECT ClientRect;
	::GetClientRect(hwnd, &ClientRect);

	// Redimensionne la page
	RedimensionnePage (hwnd, ClientRect.right-ClientRect.left, ClientRect.bottom-ClientRect.top);
  }


//----------------- fin WPage --------------------------------------------
/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : TIPE_ME.H   Declaration des types du driver MEMOIRE      |
 |   Auteur  : ??                                                       |
 |   Date    : ??                                                       |
 |   Version : ??                                                       |
 |                    Auto Bonne Moyenne Impossible                     |
 |   Portabilite :                                                      |
 |                                                                      |
 |   Mise a jours:                                                      |
 |                                                                      |
 |                                                                      |
 |   Remarques :                                                        |
 |                                                                      |
 +----------------------------------------------------------------------*/

// ------------------------------- Const -------------------------------------
#define b_geo_mem      39

#define mem_commentaire     1
#define mem_interne_simple  2
#define mem_interne_tableau 3
// ------------------------------- Type ------------------------------

typedef struct
  {
  DWORD type_memoire;
  DWORD position_es;
  DWORD position_constante;
  DWORD taille;
  } GEO_MEM, *PGEO_MEM;

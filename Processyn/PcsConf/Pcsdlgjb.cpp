/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgjb.c                                               |
 |   Auteur  :	LM							|
 |   Date    :	02/11/92						|
 |   Remarques : gestion des boites de dialogue dans le descripteur     |
 |               modbus                                                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "appli.h"
#include "UStr.h"
#include "IdConfLng.h"
#include "ActionCf.h"
#include "lng_res.h"
#include "Tipe.h"
#include "Descripteur.h"
#include "CheckMan.h"
#include "UEnv.h"
#include "Verif.h"
#include "pcsdlgjb.h"

VerifInit;

typedef struct
   {
   t_dlg_desc echange_data;
   } t_param_menu;

typedef struct
   {
   t_dlg_desc echange_data;
   char numero_canal[80];
   DWORD index_vitesse;
   DWORD index_nb_bits;
   DWORD index_parite;
   DWORD index_bits_stop;
   } t_param_canal;

typedef struct
   {
   t_dlg_desc echange_data;
   char numero_esclave[80];
   DWORD index_type;
   DWORD index_nb_retry;
   DWORD index_time_out;
   } t_param_esclave;

typedef struct
   {
   t_dlg_desc echange_data;
   DWORD index_type;
   } t_param_type_var;

typedef struct
   {
   t_dlg_desc echange_data;
   DWORD index_type;
   char nom_var[80];
   char adresse_jbus[80];
   char val_init[80];
   char raf[80];
   char taille[80];
   } PARAM_LECTURE_JB;

typedef struct
   {
   t_dlg_desc echange_data;
   DWORD index_type;
   char nom_var[80];
   char adresse_jbus[80];
   char val_init[80];
   char activation[80];
   char taille[80];
   } PARAM_ECRITURE_JB;

static char mot_res[40];
static char tab_champs[6][STR_MAX_CHAR_ARRAY];
static DWORD nb_champs;
static char buffer[30];
static LONG index;
static char taille_tab[30];
static char activation[30];
static DWORD texte_normal = ES_RIGHT | WS_GROUP | WS_VISIBLE | WS_CHILD;
static DWORD texte_grise  = ES_RIGHT | WS_GROUP | WS_VISIBLE | WS_CHILD; // $$ | DT_HALFTONE; WS_DISABLED?
static BOOL tab_bool[4] = {FALSE, FALSE, FALSE, FALSE};

#define nb_canaux 14
#define nb_vitesses 9
static char canal_texte[nb_canaux][10];// = {"CANAL 1","CANAL 2","CANAL 3","CANAL 4","CANAL 5","CANAL 6","CANAL 7","CANAL 8","CANAL 9","CANAL 10","CANAL 11","CANAL 12","CANAL 13","CANAL 14"};
static char canal_jbus[nb_canaux][3] = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14"};
static char vitesse[nb_vitesses][6] = {"50","75","300","600","1200","2400","4800","9600","19200"};
static char nb_bits[2][2] = {"7","8"};
static char parite[3][2] = {"0","1","2"};
static char bits_stop[2][2] = {"1","2"};

#define nb_esclaves 8
#define nb_retry 6
#define nb_time_out 10
static char esclave_jbus[nb_esclaves][15] = {"ALSPA","F15_F16","GOULD","PB","SIEMENS","SMC","TELEMECANIQUE","SMCX000"};
static char retry[nb_retry][3] = {"1","2","3","4","5","6"};
static char time_out_texte[nb_time_out][10] = {"1 (0.5s)","2 (1s)","3 (1.5s)","4 (2s)","5 (2.5s)","6 (3s)","7 (3.5s)","8 (4s)","9 (4.5s)","10 (5s)"};
static char time_out[nb_time_out][3] = {"1","2","3","4","5","6","7","8","9","10"};

#define nb_type 10
static char type_var[nb_type][15]; // = {"E L","E N ENTIER","E N MOT","E N REEL","E M","S L","S N ENTIER","S N MOT","S N REEL","S M"};

// -----------------------------------------------------------------------
// Objet....: permet de griser les textes et bloquer les entry_field
// Remarques: ferme la bo�te quand double clique en haut a gauche
// -----------------------------------------------------------------------
void init_normal_ou_grise (HWND hwnd)
  {
  SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_grise);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), NULL, TRUE);

  ::EnableWindow(::GetDlgItem(hwnd, ID_JB6_TAILLE), FALSE);

  SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE2), GWL_STYLE, texte_grise);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE2), NULL, TRUE);

  ::EnableWindow(::GetDlgItem(hwnd, ID_JB6_ACTIVATION), FALSE);
  }

// -----------------------------------------------------------------------
// Objet....: permet de griser les textes et bloquer les entry_field
// Remarques: ferme la bo�te quand double clique en haut a gauche
// -----------------------------------------------------------------------
static void normal_ou_grise (HWND hwnd, BOOL bool1, BOOL bool2, BOOL bool3, BOOL bool4)
  {
  RECT rcl;

  if (bool1 != tab_bool[0])
    {
    if (bool1)
      SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_normal);
    else
      SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_grise);
    ::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), NULL, TRUE);
    }

  if (bool2 != tab_bool[1])
    {
    ::EnableWindow(::GetDlgItem(hwnd, ID_JB6_TAILLE), bool2);
    if (bool2)
      { // on vient de cliquer sur var chrono
      SetDlgItemText(hwnd, ID_JB6_TAILLE, taille_tab);
      }
    else
      {
      GetDlgItemText (hwnd, ID_JB6_TAILLE, taille_tab, 30);
      SetDlgItemText(hwnd, ID_JB6_TAILLE, "");
      }
    }

  if (bool3 != tab_bool[2])
    {
    if (bool3)
      SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE2), GWL_STYLE, texte_normal);
    else
      SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE2), GWL_STYLE, texte_grise);
    ::GetWindowRect(::GetDlgItem(hwnd, ID_JB_TEXTE2), &rcl);
    ::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE2), &rcl, TRUE);
    }

  if (bool4 != tab_bool[3])
    {
    ::EnableWindow(::GetDlgItem(hwnd, ID_JB6_ACTIVATION), bool4);
    if (bool4)
      {
      SetDlgItemText(hwnd, ID_JB6_ACTIVATION, activation);
      }
    else
      {
      GetDlgItemText (hwnd, ID_JB6_ACTIVATION, activation, 30);
      SetDlgItemText(hwnd, ID_JB6_ACTIVATION, "");
      }
    }

  tab_bool[0]  = bool1;
  tab_bool[1]  = bool2;
  tab_bool[2]  = bool3;
  tab_bool[3]  = bool4;
  }

// -----------------------------------------------------------------------
// Objet....: fonctions relatives a la wndproc canal
// -----------------------------------------------------------------------
void retablir_canal(HWND hwnd, t_param_canal *param_canal) /* parametres sauvegardes */

  {
  SetDlgItemText (hwnd, ID_JB2_NUMERO_CANAL, param_canal->numero_canal);
  SendDlgItemMessage (hwnd, ID_JB2_VITESSE, CB_SETCURSEL,
                     (WPARAM) param_canal->index_vitesse, 0);

  /* initialisation des radio boutons */
  if (param_canal->index_nb_bits == 0) /* nb bits = 7 */
    {
    SendDlgItemMessage (hwnd, ID_JB2_7BITS, BM_SETCHECK, TRUE, 0);
    }
  else
    {
    SendDlgItemMessage (hwnd, ID_JB2_8BITS, BM_SETCHECK, TRUE, 0);
    }

  switch (param_canal->index_parite) /* parite */
    {
    case 0: /* sans parite */
      SendDlgItemMessage (hwnd, ID_JB2_SANS, BM_SETCHECK, TRUE, 0);
      break;
    case 1: /* parite impaire */
      SendDlgItemMessage (hwnd, ID_JB2_IMPAIRE, BM_SETCHECK, TRUE, 0);
      break;
    case 2: /* parite paire */
      SendDlgItemMessage (hwnd, ID_JB2_PAIRE, BM_SETCHECK, TRUE, 0);
      break;

    default:
      break;
    }

    if (param_canal->index_bits_stop == 0) /* bits stop */
      {
      SendDlgItemMessage (hwnd, ID_JB2_1STOP, BM_SETCHECK, TRUE, 0);
      }
    else
      {
      SendDlgItemMessage (hwnd, ID_JB2_2STOP, BM_SETCHECK, TRUE, 0);
      }
  }

// -----------------------------------------------------------------------
void defaut_canal (HWND hwnd, t_param_canal *param_canal,
                  BOOL init) /* parametres par defaut */

  {
  if (init)
    {
    StrCopy (param_canal->numero_canal, "1");
    param_canal->index_vitesse   = 7;
    param_canal->index_nb_bits   = 1;
    param_canal->index_parite    = 0;
    param_canal->index_bits_stop = 0;
    }

  SetDlgItemText (hwnd, ID_JB2_NUMERO_CANAL, "1");
  SendDlgItemMessage (hwnd, ID_JB2_VITESSE, CB_SETCURSEL, 7, 0);
  SendDlgItemMessage (hwnd, ID_JB2_8BITS, BM_SETCHECK, TRUE ,0);
  SendDlgItemMessage (hwnd, ID_JB2_SANS, BM_SETCHECK, TRUE ,0);
  SendDlgItemMessage (hwnd, ID_JB2_1STOP, BM_SETCHECK, TRUE ,0);
  }

// -----------------------------------------------------------------------
void fabrication_ul_canal (HWND hwnd, char *chaine,
                          t_param_canal *param_canal)

  {
  // FABRICATION de la CHAINE � partir des champs de la boite
  StrSetNull (chaine);
  //(*param_canal->echange_data.ads_proc_message_mot_reserve) (c_res_canal, mot_res);
  bMotReserve (c_res_canal, mot_res);
  StrConcat (chaine, mot_res);
  StrConcatChar (chaine, ' ');

  // r�cup�ration du num�ro de canal
  GetDlgItemText (hwnd, ID_JB2_NUMERO_CANAL, buffer, 10);
  StrCopy (param_canal->numero_canal, buffer);
  StrConcat (chaine, buffer);
  StrConcatChar (chaine, ' ');

  // r�cup�ration de la vitesse de communication
  index = (LONG)SendDlgItemMessage (hwnd, ID_JB2_VITESSE, CB_GETCURSEL, 0, 0);
  StrConcat (chaine, vitesse[index]);
  StrConcatChar (chaine, ' ');
  param_canal->index_vitesse = (DWORD)index;

  // r�cup�ration du nbre de bits
  index = nCheckIndex (hwnd, ID_JB2_7BITS);  //$$(LONG)SendDlgItemMessage (hwnd, ID_JB2_7BITS, BM_QUERYCHECKINDEX, 0, 0);
  StrConcat (chaine, nb_bits[index-1]);
  StrConcatChar (chaine, ' ');
  param_canal->index_nb_bits = (DWORD)(index-1);

  // r�cup�ration de la parit�
  index = nCheckIndex (hwnd, ID_JB2_SANS);//$$(LONG)SendDlgItemMessage (hwnd, ID_JB2_SANS, BM_QUERYCHECKINDEX, 0, 0);
  StrConcat (chaine, parite[index-1]);
  StrConcatChar (chaine, ' ');
  param_canal->index_parite = (DWORD)(index-1);

  // r�cup�ration des bits de stop
  index = nCheckIndex (hwnd, ID_JB2_1STOP);// $$(LONG)SendDlgItemMessage (hwnd, ID_JB2_1STOP, BM_QUERYCHECKINDEX, 0, 0);
  StrConcat (chaine, bits_stop[index-1]);
  param_canal->index_bits_stop = (DWORD)(index-1);

  // TRACE
  //SetDlgItemText(hwnd, ID_TRACE, chaine);
  // fin traces
  }
// -----------------------------------------------------------------------
// Objet....: fonctions relatives a la wndproc esclave
// -----------------------------------------------------------------------
void retablir_esclave(HWND hwnd, t_param_esclave *param_esclave) /* parametres sauvegardes */

  {
  SetDlgItemText (hwnd, ID_JB3_NUMERO, param_esclave->numero_esclave);
  SendDlgItemMessage (hwnd, ID_JB3_ESCLAVE, CB_SETCURSEL,
                     (WPARAM) param_esclave->index_type, 0);
  SendDlgItemMessage (hwnd, ID_JB3_TIME_OUT, CB_SETCURSEL,
                     (WPARAM) param_esclave->index_time_out, 0);
  SendDlgItemMessage (hwnd, ID_JB3_RETRY, CB_SETCURSEL,
                     (WPARAM) param_esclave->index_nb_retry, 0);
  }

// -----------------------------------------------------------------------
void defaut_esclave (HWND hwnd, t_param_esclave *param_esclave,
                     BOOL init) /* parametres par defaut */
  {
  if (init)
    {
    StrCopy (param_esclave->numero_esclave, "1");
    param_esclave->index_type     = 5;
    param_esclave->index_nb_retry = 2;
    param_esclave->index_time_out = 0;
    }

  SetDlgItemText (hwnd, ID_JB3_NUMERO, "1");
  SendDlgItemMessage (hwnd, ID_JB3_ESCLAVE, CB_SETCURSEL, 5, 0);
  SendDlgItemMessage (hwnd, ID_JB3_TIME_OUT, CB_SETCURSEL, 0, 0);
  SendDlgItemMessage (hwnd, ID_JB3_RETRY, CB_SETCURSEL, 2, 0);
  }
// -----------------------------------------------------------------------
void fabrication_ul_esclave (HWND hwnd, char *chaine,
                             t_param_esclave *param_esclave)

  {
  StrSetNull (chaine);
  //(*param_esclave->echange_data.ads_proc_message_mot_reserve) (c_res_esclave, mot_res);
  bMotReserve (c_res_esclave, mot_res);
  StrConcat (chaine, mot_res);
  StrConcatChar (chaine, ' ');

  /* r�cup�ration du num�ro d'esclave */
  GetDlgItemText (hwnd, ID_JB3_NUMERO, buffer, 10);
  StrCopy (param_esclave->numero_esclave, buffer);
  StrConcat (chaine, buffer);
  StrConcatChar (chaine, ' ');

  /* r�cup�ration du type d'esclave */
  index = (LONG)SendDlgItemMessage (hwnd, ID_JB3_ESCLAVE, CB_GETCURSEL, 0, 0);
  param_esclave->index_type = (DWORD)index;
  StrConcat (chaine, esclave_jbus[index]);
  StrConcatChar (chaine, ' ');

  /* r�cup�ration du time_out */
  index = (LONG)SendDlgItemMessage (hwnd, ID_JB3_TIME_OUT, CB_GETCURSEL, 0, 0);
  param_esclave->index_time_out = (DWORD)index;
  StrConcat (chaine, time_out[index]);
  StrConcatChar (chaine, ' ');

  /* r�cup�ration du retry */
  index = (LONG)SendDlgItemMessage (hwnd, ID_JB3_RETRY, CB_GETCURSEL, 0, 0);
  param_esclave->index_nb_retry = (DWORD)index;
  StrConcat (chaine, retry[index]);

  /* TRACE */
  //SetDlgItemText(hwnd, ID_TRACE, chaine);
  // fin traces
  }

// -----------------------------------------------------------------------
// Objet....: fonctions relatives a la wndproc type variable
// -----------------------------------------------------------------------
void initialise_type_var (t_param_type_var *pEnv)
  {
  char chaine[20];

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_e, chaine); /* E L */
  bMotReserve (c_res_e, chaine); /* E L */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_logique, mot_res);
  bMotReserve (c_res_logique, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[0], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_e, chaine); /* E N ENTIER */
  bMotReserve (c_res_e, chaine); /* E N ENTIER */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_numerique, mot_res);
  bMotReserve (c_res_numerique, mot_res);
  StrConcat (chaine, mot_res);
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_entier, mot_res);
  bMotReserve (c_res_entier, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[1], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_e, chaine); /* E N MOT */
  bMotReserve (c_res_e, chaine); /* E N MOT */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_numerique, mot_res);
  bMotReserve (c_res_numerique, mot_res);
  StrConcat (chaine, mot_res);
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_mots, mot_res);
  bMotReserve (c_res_mots, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[2], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_e, chaine); /* E N REEL */
  bMotReserve (c_res_e, chaine); /* E N REEL */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_numerique, mot_res);
  bMotReserve (c_res_numerique, mot_res);
  StrConcat (chaine, mot_res);
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_reels, mot_res);
  bMotReserve (c_res_reels, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[3], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_e, chaine); /* E M */
  bMotReserve (c_res_e, chaine); /* E M */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_message, mot_res);
  bMotReserve (c_res_message, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[4], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_s, chaine); /* S L */
  bMotReserve (c_res_s, chaine); /* S L */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_logique, mot_res);
  bMotReserve (c_res_logique, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[5], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_s, chaine); /* S N ENTIER */
  bMotReserve (c_res_s, chaine); /* S N ENTIER */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_numerique, mot_res);
  bMotReserve (c_res_numerique, mot_res);
  StrConcat (chaine, mot_res);
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_entier, mot_res);
  bMotReserve (c_res_entier, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[6], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_s, chaine); /* S N MOT */
  bMotReserve (c_res_s, chaine); /* S N MOT */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_numerique, mot_res);
  bMotReserve (c_res_numerique, mot_res);
  StrConcat (chaine, mot_res);
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_mots, mot_res);
  bMotReserve (c_res_mots, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[7], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_s, chaine); /* S N REEL */
  bMotReserve (c_res_s, chaine); /* S N REEL */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_numerique, mot_res);
  bMotReserve (c_res_numerique, mot_res);
  StrConcat (chaine, mot_res);
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_reels, mot_res);
  bMotReserve (c_res_reels, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[8], chaine);

  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_s, chaine); /* S M */
  bMotReserve (c_res_s, chaine); /* S M */
  StrConcatChar (chaine, ' ');
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_message, mot_res);
  bMotReserve (c_res_message, mot_res);
  StrConcat (chaine, mot_res);
  StrCopy (type_var[9], chaine);
  }

// -----------------------------------------------------------------------
void defaut_type_var (HWND hwnd, t_param_type_var *param_type_var,
                     BOOL init) /* parametres par defaut */
  {
  if (init)
    {
    param_type_var->index_type = 0;
    }
  SendDlgItemMessage (hwnd, ID_JB4_ELOG, BM_SETCHECK, TRUE, 0);
  }

// -----------------------------------------------------------------------
void fabrication_ul_type_var (HWND hwnd, char *chaine,
                              t_param_type_var *param_type_var)

  {
  StrSetNull (chaine);

  /* r�cup�ration du Sens */
  index = nCheckIndex (hwnd, ID_JB4_ELOG);//$$(LONG)SendDlgItemMessage (hwnd, ID_JB4_ELOG, BM_QUERYCHECKINDEX, 0, 0);
  StrConcat (chaine, type_var[index-1]);
  param_type_var->index_type = (DWORD)index-1;

  /* TRACE A ENLEVER */
  //SetDlgItemText(hwnd, ID_TRACE, chaine);
  // fin traces
  }

// -----------------------------------------------------------------------
void retablir_type_var(HWND hwnd, t_param_type_var *param_type_var) /* parametres sauvegardes */

  {
  switch (param_type_var->index_type)
    {
    case 0: /* E L */
    SendDlgItemMessage (hwnd, ID_JB4_ELOG, BM_SETCHECK, TRUE, 0);
    break;

    case 1: /* E N ENTIER */
    SendDlgItemMessage (hwnd, ID_JB4_ENENTIER, BM_SETCHECK, TRUE, 0);
    break;

    case 2: /* E N MOT */
    SendDlgItemMessage (hwnd, ID_JB4_ENMOT, BM_SETCHECK, TRUE, 0);
    break;

    case 3: /* E N REEL */
    SendDlgItemMessage (hwnd, ID_JB4_ENREEL, BM_SETCHECK, TRUE, 0);
    break;

    case 4: /* E M */
    SendDlgItemMessage (hwnd, ID_JB4_EMESSAGE, BM_SETCHECK, TRUE, 0);
    break;

    case 5: /* S L */
    SendDlgItemMessage (hwnd, ID_JB4_SLOG, BM_SETCHECK, TRUE, 0);
    break;

    case 6: /* S N ENTIER */
    SendDlgItemMessage (hwnd, ID_JB4_SNENTIER, BM_SETCHECK, TRUE, 0);
    break;

    case 7: /* S N MOT */
    SendDlgItemMessage (hwnd, ID_JB4_SNMOT, BM_SETCHECK, TRUE, 0);
    break;

    case 8: /* S N REEL */
    SendDlgItemMessage (hwnd, ID_JB4_SNREEL, BM_SETCHECK, TRUE, 0);
    break;

    case 9: /* S M */
    SendDlgItemMessage (hwnd, ID_JB4_SMESSAGE, BM_SETCHECK, TRUE, 0);
    break;

    default:
      break;

    } /* switch */

  }

// -----------------------------------------------------------------------
// Objet....: fonctions relatives a la wndproc lecture modbus
// -----------------------------------------------------------------------
void retablir_lecture(HWND hwnd, PARAM_LECTURE_JB *param_lecture) /* parametres sauvegardes */

  {
  RECT rcl;

  switch (param_lecture->index_type)
    {
    case 1: /* simple */
      StrSetNull (taille_tab);
      SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_grise);
      ::GetWindowRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_JB5_TAILLE), FALSE);
      SendDlgItemMessage (hwnd, ID_JB5_SIMPLE, BM_SETCHECK, TRUE, 0);
      break;
    case 2: /* tableau */
      SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_normal);
      ::GetWindowRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_JB5_TAILLE), TRUE);
      SendDlgItemMessage (hwnd, ID_JB5_TABLEAU, BM_SETCHECK,TRUE, 0);
      break;
    case 3: /* message */
      SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_normal);
      ::GetWindowRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_JB5_TAILLE), TRUE);
      SendDlgItemMessage (hwnd, ID_JB5_MESSAGE, BM_SETCHECK, TRUE, 0);
      break;

    default:
      break;
    }

  SetDlgItemText (hwnd, ID_JB5_NOM, param_lecture->nom_var);
  SetDlgItemText (hwnd, ID_JB5_ADRESSE, param_lecture->adresse_jbus);
  SetDlgItemText (hwnd, ID_JB5_VAL_INIT, param_lecture->val_init);
  SetDlgItemText (hwnd, ID_JB5_RAF, param_lecture->raf);
  SetDlgItemText (hwnd, ID_JB5_TAILLE, param_lecture->taille);

  }

// -----------------------------------------------------------------------
void defaut_lecture (HWND hwnd, PARAM_LECTURE_JB *param_lecture,
                    BOOL init) /* parametres par defaut */
  {
  RECT rcl;

  if (init)
    {
    param_lecture->index_type = 1;
    StrSetNull (param_lecture->nom_var);
    StrSetNull (param_lecture->adresse_jbus);
    StrSetNull (param_lecture->val_init);
    StrSetNull (param_lecture->raf);
    StrSetNull (param_lecture->taille);
    }

  SendDlgItemMessage (hwnd, ID_JB5_SIMPLE, BM_SETCHECK, TRUE, 0);
  SetDlgItemText (hwnd, ID_JB5_NOM, "");
  SetDlgItemText (hwnd, ID_JB5_ADRESSE, "");
  SetDlgItemText (hwnd, ID_JB5_VAL_INIT, "");
  SetDlgItemText (hwnd, ID_JB5_RAF, "");
  SetDlgItemText (hwnd, ID_JB5_TAILLE, "");

  StrSetNull (taille_tab);
  SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_grise);
  ::GetWindowRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl, TRUE);
  ::EnableWindow(::GetDlgItem(hwnd, ID_JB5_TAILLE), FALSE);
  }

// -----------------------------------------------------------------------
void fabrication_ul_lecture (HWND hwnd, char *chaine,
                             PARAM_LECTURE_JB *param_lecture)

  {
  StrSetNull (chaine);

  /* r�cup�ration du nom de variable */
  GetDlgItemText (hwnd, ID_JB5_NOM, buffer, 30);
  StrCopy (param_lecture->nom_var, buffer);
  StrConcat (chaine, buffer);
  StrConcatChar (chaine, ' ');

  /* r�cup�ration de la Taille */
  index = nCheckIndex (hwnd, ID_JB5_SIMPLE);//$$(LONG)SendDlgItemMessage (hwnd, ID_JB5_SIMPLE, BM_QUERYCHECKINDEX, 0, 0);
  param_lecture->index_type = (DWORD)index-1;
  switch(index)
    {
    case 1: /* variable simple pas message (RAF) */
      /* r�cup�ration de l'adresse */
      GetDlgItemText (hwnd, ID_JB5_ADRESSE, buffer, 30);
      StrCopy (param_lecture->adresse_jbus, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      StrSetNull (param_lecture->taille);
      break;

    case 2: /* variable tableau pas message (RAF, TAILLE) */
      /* r�cup�ration de la Taille */
      GetDlgItemText (hwnd, ID_JB5_TAILLE, buffer, 30);
      StrCopy (param_lecture->taille, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      /* r�cup�ration de l'adresse */
      GetDlgItemText (hwnd, ID_JB5_ADRESSE, buffer, 30);
      StrCopy (param_lecture->adresse_jbus, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      break;

    case 3: /* variable message (RAF, TAILLE) */
      /* r�cup�ration de l'adresse */
      GetDlgItemText (hwnd, ID_JB5_ADRESSE, buffer, 30);
      StrCopy (param_lecture->adresse_jbus, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      /* r�cup�ration de la Taille */
      GetDlgItemText (hwnd, ID_JB5_TAILLE, buffer, 30);
      StrCopy (param_lecture->taille, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      break;

    default:
      break;
    }
  /* r�cup�ration de la Raf */
  GetDlgItemText (hwnd, ID_JB5_RAF, buffer, 30);
  StrCopy (param_lecture->raf, buffer);
  StrConcat (chaine, buffer);
  StrConcatChar (chaine, ' ');

  /* r�cup�ration de la Valeur Initiale */
  GetDlgItemText (hwnd, ID_JB5_VAL_INIT, buffer, 30);
  StrCopy (param_lecture->val_init, buffer);
  StrConcat (chaine, buffer);

  /* TRACE */
  //SetDlgItemText(hwnd, ID_TRACE, chaine);
  // fin traces
  }

// -----------------------------------------------------------------------
// Objet....: fonctions relatives a la wndproc ecriture modbus
// -----------------------------------------------------------------------
void retablir_ecriture(HWND hwnd, PARAM_ECRITURE_JB *param_ecriture) /* parametres sauvegardes */

  {
  switch (param_ecriture->index_type)
    {
    case 1: /* simple */
      normal_ou_grise (hwnd, FALSE, FALSE, FALSE, FALSE);
      StrSetNull (param_ecriture->taille);
      StrSetNull (param_ecriture->activation);
      StrCopy(taille_tab, param_ecriture->taille);
      StrCopy(activation, param_ecriture->activation);
      SendDlgItemMessage (hwnd, ID_JB6_SIMPLE, BM_SETCHECK, TRUE, 0);
      break;
    case 2: /* tableau */
      normal_ou_grise (hwnd, TRUE, TRUE, TRUE, TRUE);
      StrCopy(taille_tab, param_ecriture->taille);
      StrCopy(activation, param_ecriture->activation);
      SendDlgItemMessage (hwnd, ID_JB6_TABLEAU, BM_SETCHECK, TRUE, 0);
      break;
    case 3: /* message */
      normal_ou_grise (hwnd, TRUE, TRUE, FALSE, FALSE);
      StrSetNull (param_ecriture->activation);
      StrCopy(taille_tab, param_ecriture->taille);
      StrCopy(activation, param_ecriture->activation);
      SendDlgItemMessage (hwnd, ID_JB6_MESSAGE, BM_SETCHECK, TRUE, 0);
      break;

    default:
      break;
    }

  SetDlgItemText (hwnd, ID_JB6_NOM, param_ecriture->nom_var);
  SetDlgItemText (hwnd, ID_JB6_ADRESSE, param_ecriture->adresse_jbus);
  SetDlgItemText (hwnd, ID_JB6_VAL_INIT, param_ecriture->val_init);
  SetDlgItemText (hwnd, ID_JB6_ACTIVATION, param_ecriture->activation);
  SetDlgItemText (hwnd, ID_JB6_TAILLE, param_ecriture->taille);

  }

// -----------------------------------------------------------------------
void defaut_ecriture (HWND hwnd, PARAM_ECRITURE_JB *param_ecriture,
                      BOOL init) /* parametres par defaut */
  {
  if (init)
    {
    param_ecriture->index_type       = 1;
    StrSetNull (param_ecriture->nom_var);
    StrSetNull (param_ecriture->adresse_jbus);
    StrSetNull (param_ecriture->val_init);
    StrSetNull (param_ecriture->activation);
    StrSetNull (param_ecriture->taille);
    }

  SendDlgItemMessage (hwnd, ID_JB6_SIMPLE, BM_SETCHECK, TRUE, 0);
  SetDlgItemText (hwnd, ID_JB6_NOM, "");
  SetDlgItemText (hwnd, ID_JB6_ADRESSE, "");
  SetDlgItemText (hwnd, ID_JB6_VAL_INIT, "");
  SetDlgItemText (hwnd, ID_JB6_ACTIVATION, "");
  SetDlgItemText (hwnd, ID_JB6_TAILLE, "");
  StrSetNull (taille_tab);
  StrSetNull (activation);
  normal_ou_grise (hwnd, FALSE, FALSE, FALSE, FALSE);
  }

// -----------------------------------------------------------------------
void fabrication_ul_ecriture (HWND hwnd, char *chaine,
                              PARAM_ECRITURE_JB *param_ecriture)
  {
  StrSetNull (chaine);

  /* r�cup�ration du nom de variable */
  GetDlgItemText (hwnd, ID_JB6_NOM, buffer, 30);
  StrCopy (param_ecriture->nom_var, buffer);
  StrConcat (chaine, buffer);
  StrConcatChar (chaine, ' ');

  /* r�cup�ration du type */
  index = nCheckIndex (hwnd, ID_JB6_SIMPLE);//(LONG)SendDlgItemMessage (hwnd, ID_JB6_SIMPLE, BM_QUERYCHECKINDEX, 0, 0);
  param_ecriture->index_type = (DWORD)index-1;
  switch(index)
    {
    case 1: /* variable simple pas message */
      /* r�cup�ration de l'adresse */
      GetDlgItemText (hwnd, ID_JB6_ADRESSE, buffer, 30);
      StrCopy (param_ecriture->adresse_jbus, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      StrSetNull (param_ecriture->taille);
      StrSetNull (param_ecriture->activation);
      break;

    case 2: /* variable tableau pas message (ACT, TAILLE) */
      /* r�cup�ration de la Taille */
      GetDlgItemText (hwnd, ID_JB6_TAILLE, buffer, 30);
      StrCopy (param_ecriture->taille, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');

      /* r�cup�ration de l'adresse */
      GetDlgItemText (hwnd, ID_JB6_ADRESSE, buffer, 30);
      StrCopy (param_ecriture->adresse_jbus, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');

      /* r�cup�ration de la var d'activation */
      GetDlgItemText (hwnd, ID_JB6_ACTIVATION, buffer, 30);
      StrCopy (param_ecriture->activation, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      break;

    case 3: /* variable message (TAILLE) */
      /* r�cup�ration de l'adresse */
      GetDlgItemText (hwnd, ID_JB6_ADRESSE, buffer, 30);
      StrCopy (param_ecriture->adresse_jbus, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');

      /* r�cup�ration de la Taille */
      GetDlgItemText (hwnd, ID_JB6_TAILLE, buffer, 30);
      StrCopy (param_ecriture->taille, buffer);
      StrConcat (chaine, buffer);
      StrConcatChar (chaine, ' ');
      StrSetNull (param_ecriture->activation);
      break;

    default:
      break;
    }
  /* r�cup�ration de la Valeur Initiale */
  GetDlgItemText (hwnd, ID_JB6_VAL_INIT, buffer, 30);
  StrCopy (param_ecriture->val_init, buffer);
  StrConcat (chaine, buffer);

  /* TRACE A ENLEVER */
  //SetDlgItemText(hwnd, ID_TRACE, chaine);
  // fin traces
  }
// -----------------------------------------------------------------------




// -----------------------------------------------------------------------
// Objet....: boite g�n�rale modbus : canal, esclave, type var, variables
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgJB1 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  bRes = TRUE;			   // Valeur de retour
  t_param_action  param_action;
  t_param_menu *	pEnv;

  // ------------ Switch message re�u.... --------------------------------
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			t_dlg_desc * pechange_data = (t_dlg_desc *) mp2;

			pEnv = (t_param_menu *)pCreeEnv (hwnd, sizeof (t_param_menu));
			pEnv->echange_data = (*pechange_data);
			}
      break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
        case ID_JB1_CANAL:
          pEnv = (t_param_menu *)pGetEnv (hwnd);
          param_action.entier = 1; /* numero de boite */
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

        case ID_JB1_ESCLAVE:
          pEnv = (t_param_menu *)pGetEnv (hwnd);
          param_action.entier = 2;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

        case ID_JB1_TYPE:
          pEnv = (t_param_menu *)pGetEnv (hwnd);
          param_action.entier = 3;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

        case ID_JB1_ENTREES:
          pEnv = (t_param_menu *)pGetEnv (hwnd);
          param_action.entier = 4;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

        case ID_JB1_SORTIES:
          pEnv = (t_param_menu *)pGetEnv (hwnd);
          param_action.entier = 5;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

				case ID_JB1_FERMER:
          EndDialog(hwnd, FALSE);
          pEnv = (t_param_menu *)pGetEnv (hwnd);
          break;

				case IDCANCEL:
					EndDialog(hwnd, FALSE);
					pEnv = (t_param_menu *)pGetEnv (hwnd);
					break;

        } // switch(LOWORD(mp1))
			} // WM_COMMAND
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      pEnv = (t_param_menu *)pGetEnv (hwnd);
      break;

    case WM_DESTROY:
			bLibereEnv(hwnd);
      break;

    default:
      bRes = FALSE;
      break;
    }

  return (bRes);
  }


// -----------------------------------------------------------------------
// Boite de dialogue param�tres de communication
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgJB2 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  bRes = TRUE;			   // Valeur de retour
  BOOL  commande_a_executer = FALSE;
  t_param_canal* pEnv;
  t_param_action  param_action;
  t_retour_action retour_action;

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc * pechange_data = (t_dlg_desc *) mp2;
			pEnv = (t_param_canal*)pCreeEnv (hwnd, sizeof (t_param_canal));
			pEnv->echange_data = (*pechange_data);

      /* initialisation de la combo_box vitesse */
      for (index=0; (index<=nb_vitesses-1); index++)
        {
        SendDlgItemMessage (hwnd, ID_JB2_VITESSE, CB_ADDSTRING,
                           0, (LPARAM)(PSTR) vitesse[index]);
        }

      /* initialisation des champs de la boite */
      if (StrIsNull (pEnv->echange_data.chaine))
        { /* pas double clique : on veut inserer */
        defaut_canal(hwnd, pEnv, TRUE);
        }
      else
        { /* double clique : on veut modifier, il faut r�afficher la ligne */
        StrDeleteFirstSpaces (pEnv->echange_data.chaine);
        nb_champs = StrToStrArray (tab_champs, 6, pEnv->echange_data.chaine);
        StrCopy (pEnv->numero_canal, tab_champs[1]);
        index = 0; /* vitesse */
        while (!bStrEgales (tab_champs[2], vitesse[index])
               && (index < (nb_vitesses-1)))
          {
          index++;
          }
        pEnv->index_vitesse = (DWORD)(index);

        /* initialisation des radio boutons */
        if (tab_champs[3][0] == '7') /* nb bits = 7 */
          {
          pEnv->index_nb_bits = 0;
          }
        else
          {
          pEnv->index_nb_bits = 1;
          }

        switch (tab_champs[4][0]) /* parite */
          {
          case '0': /* sans parite */
            pEnv->index_parite = 0;
          break;
          case '1': /* parite impaire */
            pEnv->index_parite = 1;
          break;
          case '2': /* parite paire */
            pEnv->index_parite = 2;
          break;
          default:
          break;
          }

        if (tab_champs[5][0] == '1') /* bits stop */
          { /* 1 bit stop */
          pEnv->index_bits_stop = 0;
          }
        else
          { /* 2 bits stop */
          pEnv->index_bits_stop = 1;
          }

        retablir_canal(hwnd, pEnv);
        } // double clique
			}
      break; // WM_INIT_DIALOG

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_JB2_INSERER:
          pEnv = (t_param_canal*)pGetEnv (hwnd);
          pEnv->echange_data.n_ligne_depart++;
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);
          fabrication_ul_canal (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_JB2_MODIFIER:
          pEnv = (t_param_canal*)pGetEnv (hwnd);
          fabrication_ul_canal (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_JB2_DEFAUT:
          pEnv = (t_param_canal*)pGetEnv (hwnd);
          defaut_canal(hwnd, pEnv, FALSE);
          break;

        case ID_JB2_RETABLIR:
          pEnv = (t_param_canal*)pGetEnv (hwnd);
          retablir_canal(hwnd, pEnv);
          break;

				case ID_JB2_AUTRES:
          pEnv = (t_param_canal*)pGetEnv (hwnd);
          param_action.entier = 0;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

				case ID_JB2_FERMER:
          pEnv = (t_param_canal*)pGetEnv (hwnd);
          EndDialog(hwnd, FALSE);
          break;

				case IDCANCEL:
					EndDialog(hwnd, FALSE);
					pEnv = (t_param_canal*)pGetEnv (hwnd);
					break;
        }
			if (commande_a_executer)
				{
				pEnv = (t_param_canal*)pGetEnv (hwnd);
				bExecuteActionCf (&retour_action);
				pEnv->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
				StrCopy (pEnv->echange_data.chaine, retour_action.chaine);
				}
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      pEnv = (t_param_canal*)pGetEnv (hwnd);
      break;

    case WM_DESTROY:
			bLibereEnv(hwnd);
      break;

    default:
      bRes = FALSE;
      break;
    }


  return (bRes);

  }

// -----------------------------------------------------------------------
// Nom......: fnwp_jb3
// Objet....: esclave
// Entr�es..: HWND     hwnd
//	      USHORT   msg
//	      WPARAM   mp1
//	      LPARAM   mp2
// Sorties..:
// Retour...: LRESULT: r�sultat du traitement du message
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgJB3 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  bRes = TRUE;			   // Valeur de retour
  BOOL  commande_a_executer = FALSE;
  t_param_esclave *pEnv;
  t_param_action  param_action;
  t_retour_action retour_action;

  // ------------ Switch message re�u.... --------------------------------
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc * pechange_data = (t_dlg_desc *) mp2;
			pEnv = (t_param_esclave*)pCreeEnv (hwnd, sizeof (t_param_esclave));
			pEnv->echange_data = (*pechange_data);

      /* initialisation des types d'esclave */
      for (index=0; index<=(nb_esclaves-1); index++)
        {
        SendDlgItemMessage (hwnd, ID_JB3_ESCLAVE, CB_ADDSTRING,
                           0,(LPARAM)((PSTR) esclave_jbus[index]));
        }
      /* initialisation du retry */
      for (index=0; (index<=nb_retry-1); index++)
        {
        SendDlgItemMessage (hwnd, ID_JB3_RETRY, CB_ADDSTRING,
                           0, (LPARAM)((PSTR) retry[index]));
        }
      /* initialisation du time_out */
      for (index=0; (index<=nb_time_out-1); index++)
        {
        SendDlgItemMessage (hwnd, ID_JB3_TIME_OUT, CB_ADDSTRING,
                           0, (LPARAM)((PSTR) time_out_texte[index]));
        }

      /* initialisation des champs de la boite */
      if (StrIsNull (pEnv->echange_data.chaine))
        { /* pas double clique : on veut inserer */
        defaut_esclave (hwnd, pEnv, TRUE);
        }
      else
        { /* double clique : on veut modifier, il faut r�afficher la ligne */
        StrDeleteFirstSpaces (pEnv->echange_data.chaine);
        nb_champs = StrToStrArray (tab_champs, 6, pEnv->echange_data.chaine);
        StrCopy (pEnv->numero_esclave, tab_champs[1]);

        index = 0; /* type */
        while (!bStrEgales (tab_champs[2], esclave_jbus[index])
               && (index < (nb_esclaves-1)))
          {
          index++;
          }
        pEnv->index_type = (DWORD)(index);

        index = 0; /* time_out */
        while (!bStrEgales (tab_champs[3], time_out[index])
               && (index < (nb_time_out-1)))
          {
          index++;
          }
        pEnv->index_time_out = (DWORD)(index);

        index = 0; /* nb_retry */
        while (!bStrEgales (tab_champs[4], retry[index])
               && (index < (nb_retry-1)))
          {
          index++;
          }
        pEnv->index_nb_retry = (DWORD)(index);

        retablir_esclave(hwnd, pEnv);
        }
			}
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_JB3_INSERER:
          pEnv = (t_param_esclave*)pGetEnv (hwnd);
          pEnv->echange_data.n_ligne_depart++;
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);
          fabrication_ul_esclave (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_JB3_MODIFIER:
          pEnv = (t_param_esclave*)pGetEnv (hwnd);
          fabrication_ul_esclave (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_JB3_DEFAUT:
          pEnv = (t_param_esclave*)pGetEnv (hwnd);
          defaut_esclave (hwnd, pEnv, FALSE);
          break;

        case ID_JB3_RETABLIR:
          pEnv = (t_param_esclave*)pGetEnv (hwnd);
          retablir_esclave(hwnd, pEnv);
          break;

				case ID_JB3_AUTRES:
          pEnv = (t_param_esclave*)pGetEnv (hwnd);
          param_action.entier = 0;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

				case ID_JB3_FERMER:
          pEnv = (t_param_esclave*)pGetEnv (hwnd);
          EndDialog(hwnd, FALSE);
          break;

		    case IDCANCEL:
					EndDialog(hwnd, FALSE);
					pEnv = (t_param_esclave*)pGetEnv (hwnd);
					break;
        }
			if (commande_a_executer)
				{
				pEnv = (t_param_esclave*)pGetEnv (hwnd);
				bExecuteActionCf (&retour_action);
				pEnv->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
				StrCopy (pEnv->echange_data.chaine, retour_action.chaine);
				}
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      pEnv = (t_param_esclave*)pGetEnv (hwnd);
      break;


    case WM_DESTROY:
			bLibereEnv(hwnd);
      break;

    default:
      bRes = FALSE;
      break;
    }

  return (bRes);
  }

// -----------------------------------------------------------------------
// Nom......: fnwp_jb4
// Objet....: type de variable
// Entr�es..: HWND     hwnd
//	      USHORT   msg
//	      WPARAM   mp1
//	      LPARAM   mp2
// Sorties..:
// Retour...: LRESULT: r�sultat du traitement du message
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgJB4 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  bRes = TRUE;			   // Valeur de retour
  BOOL  commande_a_executer = FALSE;
  t_param_type_var *pEnv;
  t_param_action  param_action;
  t_retour_action retour_action;


  // ------------ Switch message re�u.... --------------------------------
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc * pechange_data = (t_dlg_desc *) mp2;
			pEnv = (t_param_type_var*)pCreeEnv (hwnd, sizeof (t_param_type_var));
			pEnv->echange_data = (*pechange_data);

      initialise_type_var(pEnv); /* fabrique les types de variables */

      /* initialisation des champs de la boite */
      if (StrIsNull (pEnv->echange_data.chaine))
        { /* pas double clique : on veut inserer */
        defaut_type_var (hwnd, pEnv, TRUE);
        }
      else
        { /* double clique : on veut modifier, il faut r�afficher la ligne */
        StrDeleteFirstSpaces (pEnv->echange_data.chaine);
        index = 0; /* type */
        while (!bStrEgales (pEnv->echange_data.chaine, type_var[index])
               && (index < (nb_type-1)))
          {
          index++;
          }
        pEnv->index_type = (DWORD)index;
        retablir_type_var(hwnd, pEnv);
        }
			}
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_JB4_INSERER:
          pEnv = (t_param_type_var*)pGetEnv (hwnd);
          pEnv->echange_data.n_ligne_depart++;
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);
          fabrication_ul_type_var (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_JB4_MODIFIER:
          pEnv = (t_param_type_var*)pGetEnv (hwnd);
          fabrication_ul_type_var (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_JB4_DEFAUT:
          pEnv = (t_param_type_var*)pGetEnv (hwnd);
          defaut_type_var(hwnd, pEnv, FALSE);
          break;

        case ID_JB4_RETABLIR:
          pEnv = (t_param_type_var*)pGetEnv (hwnd);
          retablir_type_var(hwnd, pEnv);
          break;

				case ID_JB4_AUTRES:
          pEnv = (t_param_type_var*)pGetEnv (hwnd);
          param_action.entier = 0;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

				case ID_JB4_FERMER:
          pEnv = (t_param_type_var*)pGetEnv (hwnd);
          EndDialog(hwnd, FALSE);
          break;

				case IDCANCEL:
					EndDialog(hwnd, FALSE);
					pEnv = (t_param_type_var*)pGetEnv (hwnd);
					break;
        }

			if (commande_a_executer)
				{
				pEnv = (t_param_type_var*)pGetEnv (hwnd);
				bExecuteActionCf (&retour_action);
				pEnv->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
				StrCopy (pEnv->echange_data.chaine, retour_action.chaine);
				}
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      pEnv = (t_param_type_var*)pGetEnv (hwnd);
      break;

    case WM_DESTROY:
			bLibereEnv(hwnd);
      break;

    default:
      bRes = FALSE;
      break;
    }

  return (bRes);
  }

// -----------------------------------------------------------------------
// Dlgproc lectures modbus
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgJB5 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  bRes = TRUE;			   // Valeur de retour
  BOOL  commande_a_executer = FALSE;
  PARAM_LECTURE_JB *pEnv;
  t_param_action  param_action;
  t_retour_action retour_action;
  RECT rcl;

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc * pechange_data = (t_dlg_desc *) mp2;
			pEnv = (PARAM_LECTURE_JB*)pCreeEnv (hwnd, sizeof (PARAM_LECTURE_JB));
			pEnv->echange_data = (*pechange_data);

      /* initialisation des champs de la boite */
      if (StrIsNull (pEnv->echange_data.chaine))
        { /* pas double clique : on veut inserer */
        defaut_lecture (hwnd, pEnv, TRUE);
        }
      else
        { /* double clique : on veut modifier, il faut r�afficher la ligne */
        StrDeleteFirstSpaces (pEnv->echange_data.chaine);
        nb_champs = StrToStrArray (tab_champs, 6, pEnv->echange_data.chaine);
        StrCopy (pEnv->nom_var, tab_champs[0]);
        StrCopy (pEnv->val_init, tab_champs[nb_champs-1]);

        if (nb_champs == 4)
          { /* entr�e simple log ou num */
          StrSetNull (pEnv->taille);
          pEnv->index_type = 1;
          StrCopy (pEnv->adresse_jbus, tab_champs[1]);
          StrCopy (pEnv->raf, tab_champs[2]);
          } /* entr�e simple log ou num */
        else
          { /* entr�e tab log ou num ou mess */
          if (tab_champs[4][0] == '"')
            { /* entr�e message */
            pEnv->index_type = 3;
            StrCopy (pEnv->adresse_jbus, tab_champs[1]);
            StrCopy (pEnv->taille, tab_champs[2]);
            StrCopy (pEnv->raf, tab_champs[3]);
            } /* entr�e simple log */
          else
            { /* entr�e tab log ou num */
            pEnv->index_type = 2;
            StrCopy (pEnv->taille, tab_champs[1]);
            StrCopy (pEnv->adresse_jbus, tab_champs[2]);
            StrCopy (pEnv->raf, tab_champs[3]);
            } /* entr�e tab log ou num */
          } /* entr�e tab log ou num ou mess */

        retablir_lecture(hwnd, pEnv);
        }
			}
      break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
        case ID_JB5_INSERER:
          pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
          pEnv->echange_data.n_ligne_depart++;
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);
          fabrication_ul_lecture (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_JB5_MODIFIER:
          pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
          fabrication_ul_lecture (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_JB5_DEFAUT:
          pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
          defaut_lecture(hwnd, pEnv, FALSE);
          break;

        case ID_JB5_RETABLIR:
          pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
          retablir_lecture(hwnd, pEnv);
          break;

				case ID_JB5_AUTRES:
          pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
          param_action.entier = 0;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;

				case ID_JB5_FERMER:
          pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
          EndDialog(hwnd, FALSE);
          break;

				case IDCANCEL:
					EndDialog(hwnd, FALSE);
					pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
					break;

				default:
					{
					if (HIWORD (mp1) == BN_CLICKED)
						{
						switch (LOWORD (mp1))
							{
							case ID_JB5_SIMPLE:
								SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_grise);
								::GetWindowRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl);
								::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl, TRUE);
								::EnableWindow(::GetDlgItem(hwnd, ID_JB5_TAILLE), FALSE);
								GetDlgItemText (hwnd, ID_JB5_TAILLE, taille_tab, 30);
								SetDlgItemText(hwnd, ID_JB5_TAILLE, "");
							break;

							case ID_JB5_TABLEAU:
							case ID_JB5_MESSAGE:
								SetWindowLong(::GetDlgItem(hwnd, ID_JB_TEXTE1), GWL_STYLE, texte_normal);
								::GetWindowRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl);
								::InvalidateRect(::GetDlgItem(hwnd, ID_JB_TEXTE1), &rcl, TRUE);
								::EnableWindow(::GetDlgItem(hwnd, ID_JB5_TAILLE), TRUE);
								SetDlgItemText(hwnd, ID_JB5_TAILLE, taille_tab);
							break;
							} //switch (LOWORD (mp1))
						}
					}
					break;
				} // switch(LOWORD(mp1))
			if (commande_a_executer)
				{
				pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
				bExecuteActionCf (&retour_action);
				pEnv->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
				StrCopy (pEnv->echange_data.chaine, retour_action.chaine);
				}
			} // WM_COMMAND
     break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      pEnv = (PARAM_LECTURE_JB*)pGetEnv (hwnd);
      break;


    case WM_DESTROY:
			bLibereEnv(hwnd);
      break;

    default:
      bRes = FALSE;
      break;
    }

  return bRes;
  } // dlgJB5

// -----------------------------------------------------------------------
// Dlgproc �critures modbus
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgJB6 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  bRes = TRUE;			   // Valeur de retour
  BOOL  commande_a_executer = FALSE;
  PARAM_ECRITURE_JB *pEnv;
  t_param_action  param_action;
  t_retour_action retour_action;

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc * pechange_data = (t_dlg_desc *) mp2;
			pEnv = (PARAM_ECRITURE_JB*)pCreeEnv (hwnd, sizeof (PARAM_ECRITURE_JB));
			pEnv->echange_data = (*pechange_data);

      /* initialisation des champs de la boite */
      init_normal_ou_grise (hwnd);
      if (StrIsNull (pEnv->echange_data.chaine))
        { /* pas double clique : on veut inserer */
        defaut_ecriture (hwnd, pEnv, TRUE);
        }
      else
        { /* double clique : on veut modifier, il faut r�afficher la ligne */
        StrDeleteFirstSpaces (pEnv->echange_data.chaine);
        nb_champs = StrToStrArray (tab_champs, 6, pEnv->echange_data.chaine);
        StrCopy (pEnv->nom_var, tab_champs[0]);
        StrCopy (pEnv->val_init, tab_champs[nb_champs-1]);

        switch (nb_champs)
          {
          case 3: /* sortie simple log ou num */
            StrSetNull (pEnv->taille);
            StrSetNull (pEnv->activation);
            pEnv->index_type = 1;
            StrCopy (pEnv->adresse_jbus, tab_champs[1]);
            break;

          case 4: /* sortie mess */
            pEnv->index_type = 3;
            StrSetNull (pEnv->activation);
            StrCopy (pEnv->adresse_jbus, tab_champs[1]);
            StrCopy (pEnv->taille, tab_champs[2]);
            break;

          case 5: /* sortie tab log ou num */
            pEnv->index_type = 2;
            StrCopy (pEnv->taille, tab_champs[1]);
            StrCopy (pEnv->adresse_jbus, tab_champs[2]);
            StrCopy (pEnv->activation, tab_champs[3]);

          default:
            break;

          } /* switch */

        retablir_ecriture(hwnd, pEnv);
        }
			}
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_JB6_INSERER:
          pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
          pEnv->echange_data.n_ligne_depart++;
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);
          fabrication_ul_ecriture (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;
					
        case ID_JB6_MODIFIER:
          pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
          fabrication_ul_ecriture (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;
					
        case ID_JB6_DEFAUT:
          pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
          defaut_ecriture(hwnd, pEnv, FALSE);
          break;
					
        case ID_JB6_RETABLIR:
          pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
          retablir_ecriture(hwnd, pEnv);
          break;
					
				case ID_JB6_AUTRES:
          pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
          param_action.entier = 0;
          param_action.mot = (DWORD)pEnv->echange_data.n_ligne_depart; /* numero de ligne */
          param_action.booleen = BOITE_DIRECTE;
          bAjouterActionCf (action_enchaine_boite, &param_action);
          EndDialog(hwnd, FALSE);
          break;
					
				case ID_JB6_FERMER:
          pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
          EndDialog(hwnd, FALSE);
          break;
					
				case IDCANCEL:
					EndDialog(hwnd, FALSE);
					pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
					break;
					
				default:
					if( HIWORD( mp1 ) == BN_CLICKED )
						{
						switch (LOWORD (mp1))
							{
							case ID_JB6_SIMPLE:
								normal_ou_grise (hwnd, FALSE, FALSE, FALSE, FALSE);
								break;
								
							case ID_JB6_TABLEAU:
								normal_ou_grise (hwnd, TRUE, TRUE, TRUE, TRUE);
								break;
								
							case ID_JB6_MESSAGE:
								normal_ou_grise (hwnd, TRUE, TRUE, FALSE, FALSE);
								break;
							} // switch mp1
						} // BN_CLICKED
          break; // default
				} // switch LOWORD (mp1)
			if (commande_a_executer)
				{
				pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
				bExecuteActionCf (&retour_action);
				pEnv->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
				StrCopy (pEnv->echange_data.chaine, retour_action.chaine);
				}
      break; // WM_COMMAND

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      pEnv = (PARAM_ECRITURE_JB*)pGetEnv (hwnd);
      break;

    case WM_DESTROY:
			bLibereEnv(hwnd);
      break;

    default:
      bRes = FALSE;
      break;
    }

  return bRes;
  } // dlgJB6


/* -------------------------------------------------------------------
fonction qui r�cup�re l'identificateur et l'adresse de la Window Proc
de la bo�te a afficher. En entr�e, on passe le num�ro de la bo�te parmi
les bo�tes du descripteur.
--------------------------------------------------------------------*/
void LIS_PARAM_DLG_JB (int index_boite, UINT *iddlg, FARPROC * adr_winproc)
  {
  switch (index_boite)
    {
    case 0 : // canal, esclave, type variables, variables
      *iddlg = DLG_INTERPRETE_JB1;
      (*adr_winproc) = (FARPROC) dlgJB1;
      break;

    case 1 : // param�tres de communication : canal ...
      *iddlg = DLG_INTERPRETE_JB2;
      (*adr_winproc) = (FARPROC) dlgJB2;
      break;

    case 2 : // type d'esclave
      *iddlg = DLG_INTERPRETE_JB3;
      (*adr_winproc) = (FARPROC) dlgJB3;
      break;

    case 3 : // type de variables
      *iddlg = DLG_INTERPRETE_JB4;
      (*adr_winproc) = (FARPROC) dlgJB4;
      break;

    case 4 : // lectures
      *iddlg = DLG_INTERPRETE_JB5;
      (*adr_winproc) = (FARPROC) dlgJB5;
      break;

    case 5 : // ecritures
      *iddlg = DLG_INTERPRETE_JB6;
      (*adr_winproc) = (FARPROC) dlgJB6;
      break;

    default:
			VerifWarningExit;
      break;
    }
  }


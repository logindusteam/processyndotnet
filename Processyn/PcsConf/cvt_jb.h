/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |									    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 |									    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |									    |
 |   Titre   : CV_JB.H   						    |
 |   Auteur  : AC							    |
 |   Date    : 31/12/93 						    |
 +--------------------------------------------------------------------------*/


DWORD dwConversionAdresseModbus
	(
	MODBUS_AUTOMATE nAutomate, 
	DWORD sens, DWORD genre, PCSTR pszAdresse, 
	DWORD * pwAdresseModbus,
	DWORD * pwSensOuNombreSurAutomate
	);

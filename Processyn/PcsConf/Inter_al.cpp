/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_al.c                                               |
 |   Auteur  : lm					        	|
 |   Date    : 14/04/94					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "mem.h"
#include "init_txt.h"
#include "UStr.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "driv_dq.h"    // bFicNomValide
#include "DocMan.h"       
#include "PathMan.h"       
#include "UChrono.h"
#include "tipe_al.h"
#include "IdLngLng.h"
#include "cvt_ul.h"
#include "inter_al.h"
#include "Verif.h"
VerifInit;

//-------------------------- Procedures locales ---------------------
static void traite_message_effaces (void);
static BOOL initialiser_al (void);
static void fini_al (void);
static BOOL insere_message_alarme (char *message_alarme, DWORD *pointeur_sur_fichier);
static void enleve_message_alarme (DWORD pointeur_sur_fichier);
static BOOL lire_message_alarme (char *message_alarme, DWORD pointeur_sur_fichier);
static void consulte_titre_paragraf_al (DWORD ligne);
static void maj_pointeur_dans_geo (DWORD numero_repere, DWORD limite, BOOL spec);
static void traitement_num (PUL ul, DWORD num);
static void erreur_encode_r (PUL ul, FLOAT rValeur);
static BOOL va_deja_declaree (PUL ul, ID_MOT_RESERVE genre_va,
                                 DWORD *position_es, DWORD *wErreur);
static void genere_vect_ul (DWORD ligne, PUL vect_ul,
                            int *nb_ul, int *niveau, int *indentation);
static BOOL valide_vect_ul
	(PUL vect_ul, int nb_ul, DWORD ligne,
	REQUETE_EDIT action, DWORD *profondeur,
	PID_PARAGRAPHE tipe_paragraf,
	DWORD *numero_repere,
	DWORD *pos_init, BOOL *exist_init,
	PC_ALARME	spec_alarme,
	char *message_alarme,
	PSPEC_TITRE_PARAGRAPHE_ALARME	spec_titre_paragraf,
	DWORD *erreur);

static BOOL test_groupe (char *nom_groupe, DWORD pos_specif_paragraf);

//-------------------------- Variables ---------------------
static t_contexte_al contexte_al;
static DWORD ligne_depart_al;
static DWORD ligne_courante_al;
static BOOL al_deja_initialise;
static HFDQ repere_fichier_ascii;


//-------------------------------------------------------------------------
//                        GESTION CONTEXTE
//-------------------------------------------------------------------------
static void consulte_titre_paragraf_al (DWORD ligne)
  {
  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_ALARME	donnees_geo;
  PSPEC_TITRE_PARAGRAPHE_ALARME	spec_titre_paragraf;
  CONTEXTE_PARAGRAPHE_LIGNE table_contexte_ligne[6];
  DWORD profondeur_ligne;
  DWORD i;

  if (bDonneContexteParagrapheLigne (b_titre_paragraf_al, ligne, &profondeur_ligne, table_contexte_ligne))
    { // donne_contexte_ok
    for (i = 0; (i < profondeur_ligne); i++)
      {
      switch (table_contexte_ligne [i].IdParagraphe)
        {
        case surveillance_ala:
					titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_al, table_contexte_ligne [i].position);
					donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, titre_paragraf->pos_geo_debut);
					spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
          contexte_al.type_horodatage = spec_titre_paragraf->type_horodatage;
          StrSetNull (contexte_al.nom_groupe);
					contexte_al.numero_priorite = 0;
          break;

				case visualisation_ala:
				case archivage_ala:
				case impression_ala:
          break;

				case groupe_ala:
					titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_al, table_contexte_ligne [i].position);
					donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, titre_paragraf->pos_geo_debut);
					spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
          StrCopy (contexte_al.nom_groupe, spec_titre_paragraf->nom_groupe);
          break;

        case priorite_ala:
					titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_al, table_contexte_ligne [i].position);
					donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, titre_paragraf->pos_geo_debut);
					spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
					contexte_al.numero_priorite = spec_titre_paragraf->numero_priorite;
          break;

        default :
          StrSetNull (contexte_al.nom_groupe);
					contexte_al.numero_priorite = 0;
          contexte_al.type_horodatage = HORODATAGE_PC;
          break;
        } // switch
      } // for i
    } // donne_contexte_ok
  else
    {
    StrSetNull (contexte_al.nom_groupe);
    contexte_al.numero_priorite = 0;
    contexte_al.type_horodatage = HORODATAGE_PC;
    }
  }

/*--------------------------------------------------------------------------*/
/*                             DIVERS                                       */
/*--------------------------------------------------------------------------*/
void ResetFichierMessagesAlarme (void)
	{
  char nom_fic_pa1[MAX_PATH];

	StrSetNull (nom_fic_pa1);
	pszCopiePathNameDocExt (nom_fic_pa1, MAX_PATH, EXTENSION_PA1);
  if (bFicPresent (nom_fic_pa1))
    {
    uFicEfface (nom_fic_pa1);
    }
	}


// -----------------------------------------------------------------------
//
void DupliqueFichierMessagesAlarme (PCSTR pszNomSource)
  {
  char nom_depart[MAX_PATH];
  char nom_arrivee[MAX_PATH];

	pszCreeNameExt (nom_depart, MAX_PATH, pszNomSource, EXTENSION_PA1);
  if (bFicPresent (nom_depart))  
		{
		pszCreeNameExt (nom_arrivee, MAX_PATH, pszPathNameDoc(), EXTENSION_PA1);
		uFicCopie (nom_depart, nom_arrivee, FALSE);
		}
  }


// -----------------------------------------------------------------------
//
static void traite_message_effaces (void)
  {
  PC_ALARME	ads_tc_alarme;
  DWORD       position;

  enleve_bloc ( b_pt_libre); // $$ n'existe pas si BD vierge 
	// $$ ancien commentaire ligne au dessus : ca repart "propre"
  if (existe_repere(b_alarme))
    {
    position = 0;
    while (position < nb_enregistrements (szVERIFSource, __LINE__, b_alarme))
      { // on remet tous les numeros_enregistrements a zero
      position++;
      ads_tc_alarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, position);
      ads_tc_alarme->numero_enregistrement = 0;
      }
    }
  }

// -----------------------------------------------------------------------
//
static BOOL initialiser_al (void)
  {
  BOOL valide;
  char fic_messages[MAX_PATH];

  valide = FALSE;

	pszCopiePathNameDocExt (fic_messages, MAX_PATH, EXTENSION_PA1);
  if (!bFicPresent (fic_messages))
    { // le fichier a ete efface
    traite_message_effaces();
    }
  if ((uFicOuvre (&repere_fichier_ascii, fic_messages, TAILLE_MAX_MESSAGE_AL)) == 0)
    {
    valide = TRUE;
    } // ouvre_fichier ok

  return (valide);
  }

// -----------------------------------------------------------------------
//
static void fini_al (void)
  {
  BOOL	bDelete = FALSE;
  char	fic_messages[MAX_PATH];
	DWORD	dwNbEnr;

	uFicNbEnr (repere_fichier_ascii, &dwNbEnr); // $$ err
  if (dwNbEnr == 0)
    {
    bDelete = TRUE;
    }
  //
  uFicFerme (&repere_fichier_ascii);
  //
  if (bDelete)
    {
		pszCopiePathNameDocExt (fic_messages, MAX_PATH, EXTENSION_PA1);
    uFicEfface (fic_messages);
    }
  }

// -----------------------------------------------------------------------
//
static BOOL insere_message_alarme (char *message_alarme, DWORD *pointeur_sur_fichier)
  {
  BOOL valide;
  BOOL place_libre;
  DWORD    wIndex;
  DWORD    nbr_blanc;
  DWORD    nbr_enr;
  PDWORD	ads_word;
  char    message_tamp[TAILLE_MAX_MESSAGE_AL + 1];

  valide = FALSE; // erreur d'insertion ds le fichier

  place_libre = FALSE;
  if (existe_repere (b_pt_libre))
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_pt_libre);
    if (nbr_enr != 0)
      {
      place_libre = TRUE;
      }
    }

  if (place_libre)
    { // place libre dans le fichier ascii
    ads_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_pt_libre, nbr_enr); // pointe le dernier
    (*pointeur_sur_fichier) = (*ads_word);
    enleve_enr (szVERIFSource, __LINE__, 1, b_pt_libre, nbr_enr); // la place n'est plus libre
    }
  else
    { // pas de place libre ds le fichier ascii
    uFicNbEnr (repere_fichier_ascii, pointeur_sur_fichier);
		(*pointeur_sur_fichier) ++;
    }

  StrCopy (message_tamp, message_alarme);

  nbr_blanc = TAILLE_MAX_MESSAGE_AL - 2 - StrLength (message_tamp);
  for (wIndex = 1; wIndex <= nbr_blanc; wIndex++)
    {
    StrConcatChar (message_tamp, ' ');
    }
  message_tamp[TAILLE_MAX_MESSAGE_AL - 2] = CAR_CR;
  message_tamp[TAILLE_MAX_MESSAGE_AL - 1] = CAR_LF;
  message_tamp[TAILLE_MAX_MESSAGE_AL] = '\0';   // inutile au sens du fichier

  if (uFicEcrireDirect (repere_fichier_ascii, (*pointeur_sur_fichier), message_tamp) == 0)
    {
    valide = TRUE;
    }
  return (valide);
  }

// -----------------------------------------------------------------------
//
static void enleve_message_alarme (DWORD pointeur_sur_fichier)
  {
  DWORD    nbr_enr;
  DWORD   *ads_word;
  DWORD    position;

  if (pointeur_sur_fichier != 0)
    { // le fichier n'a pas ete efface
    if (!existe_repere (b_pt_libre))
      {
      cree_bloc (b_pt_libre, sizeof(DWORD), 0);
      nbr_enr = 0;
      }
    else
      {
      nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_pt_libre);
      }
    position = nbr_enr + 1;
    insere_enr (szVERIFSource, __LINE__, 1, b_pt_libre, position);
    ads_word = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_pt_libre, position);
    (*ads_word) = pointeur_sur_fichier;
    } // test pointeur_sur_fichier <> 0
  }

// -----------------------------------------------------------------------
//
static BOOL lire_message_alarme (char *message_alarme, DWORD pointeur_sur_fichier)
  {
  BOOL	valide = FALSE;
  char  message_tamp[TAILLE_MAX_MESSAGE_AL];

  if (pointeur_sur_fichier > 0)
    { 
		// le fichier n'a pas ete efface
		DWORD nbr_enr;

    uFicNbEnr (repere_fichier_ascii, &nbr_enr);
    if (pointeur_sur_fichier <= nbr_enr)
      {
      if (uFicLireDirect (repere_fichier_ascii, pointeur_sur_fichier, message_tamp) == 0)
        {
        StrSetLength (message_tamp, TAILLE_MAX_MESSAGE_AL - 2); // positionne fin chaine a la place de CR
        StrDeleteLastSpaces (message_tamp);
        valide = TRUE;
        }
      }
    } // pointeur sur fichier > 0

  if (!valide)
    {
    StrCopy (message_tamp, "error message");
    }
  StrCopy (message_alarme, message_tamp);
  return valide;
  }

//--------------------------------------------------------------------------
//
static void maj_pointeur_dans_geo (DWORD numero_repere, DWORD limite, BOOL spec)
  {
  PGEO_ALARME	donnees_geo;
  DWORD nbr_enr;
  DWORD i;

  if (!existe_repere (b_geo_al))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_al);
    }
  for (i = 1; (i <= nbr_enr); i++)
    {
    donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, i);
    if (donnees_geo->numero_repere == numero_repere)
      {
      switch (numero_repere)
        {
        case b_alarme :
	  if (donnees_geo->pos_donnees >= limite)
            {
	    donnees_geo->pos_donnees--;
            }
          break;

        case b_titre_paragraf_al:
          if (spec)
            {
            if (donnees_geo->pos_specif_paragraf >= limite)
              {
              donnees_geo->pos_specif_paragraf--;
              }
            }
          else
            {
            if (donnees_geo->pos_paragraf >= limite)
              {
              donnees_geo->pos_paragraf--;
              }
            }
          break;

        default:
          break;
       
        } // switch
      }
    } // for
  }

/*--------------------------------------------------------------------------*/
static void traitement_wnum (PUL ul, DWORD num)
  {
  StrDWordToStr (ul->show, num);
  ul->erreur = 0;
  }

/*--------------------------------------------------------------------------*/
static void traitement_lnum (PUL ul, LONG num)
  {
  StrLONGToStr (ul->show, num);
  ul->erreur = 0;
  }

/*--------------------------------------------------------------------------*/
static void traitement_inum (PUL ul, int num)
  {
  StrINTToStr (ul->show, num);
  ul->erreur = 0;
  }

/*--------------------------------------------------------------------------*/
static void erreur_encode_r (PUL ul, FLOAT rValeur)
  {
  StrFLOATToStr (ul->show, rValeur);
  ul->erreur = 0;
  }

/*--------------------------------------------------------------------------*/
static BOOL va_deja_declaree (PUL ul, ID_MOT_RESERVE genre_va, DWORD *position_es, DWORD *wErreur)
  {
  PENTREE_SORTIE es;
  BOOL          bRetour = FALSE;

  (*wErreur) = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
  if (bReconnaitESExistante (ul, position_es))
    {
    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, (*position_es));
    if (es->genre == genre_va)
      {
      bRetour = TRUE;
      }
    else
      {
      (*wErreur) = 12;
      }
    }

  return (bRetour);
  }

/*--------------------------------------------------------------------------*/
/*                             EDITION                                      */
/*--------------------------------------------------------------------------*/

//************************* generation vecteur UL ****************************

static void genere_vect_ul (DWORD ligne, PUL vect_ul,
                            int *nb_ul, int *niveau, int *indentation)
  {
  PGEO_ALARME	donnees_geo;
  PC_ALARME	al_alarme;
  int indice_courant;
  char mess_80[TAILLE_MAX_MESSAGE_AL - 2 + 1];
  PTITRE_PARAGRAPHE titre_paragraf;
  PSPEC_TITRE_PARAGRAPHE_ALARME	spec_titre_paragraf;

  (*niveau) = 0;
  (*indentation) = 0;
  donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, ligne);
  switch (donnees_geo->numero_repere)
    {
    case b_pt_constant:
      (*nb_ul) = 1;
      vect_ul[0].erreur = 0;
      StrSetNull (vect_ul[0].show);
      consulte_cst_bd_c (donnees_geo->pos_donnees, vect_ul[0].show);
      break;

    case b_titre_paragraf_al :
      titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_al, donnees_geo->pos_paragraf);
      switch (titre_paragraf->IdParagraphe)
        {
        case surveillance_ala:
          (*niveau) = 1;
          (*indentation) = 0;
          (*nb_ul) = 8;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_surveillance, vect_ul[0].show);
     	  spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
     	  if (spec_titre_paragraf->val_init_surv)
     	    {
            bMotReserve (c_res_un, vect_ul[1].show);
            }
     	  else
     	    {
            bMotReserve (c_res_zero, vect_ul[1].show);
     	    }
          traitement_inum (&(vect_ul[2]), spec_titre_paragraf->fen_al_x);
          traitement_inum (&(vect_ul[3]), spec_titre_paragraf->fen_al_y);
          traitement_inum (&(vect_ul[4]), spec_titre_paragraf->fen_al_cx);
          traitement_inum (&(vect_ul[5]), spec_titre_paragraf->fen_al_cy);
          traitement_inum (&(vect_ul[6]), spec_titre_paragraf->type_horodatage);
          traitement_lnum (&(vect_ul[7]), spec_titre_paragraf->tempo_stabilite_evenement);
          break;

        case visualisation_ala:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 3;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_visualisation, vect_ul[0].show);
     	  spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
          traitement_wnum (&(vect_ul[2]), spec_titre_paragraf->nbre_al_maxi);
     	  if (spec_titre_paragraf->val_init_vis)
     	    {
            bMotReserve (c_res_un, vect_ul[1].show);
            }
     	  else
     	    {
            bMotReserve (c_res_zero, vect_ul[1].show);
     	    }
          break;

        case archivage_ala:
          (*niveau) = 3;
          (*indentation) = 2;
          (*nb_ul) = 4;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_archivage, vect_ul[0].show);
     	  spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
          StrCopy (vect_ul[2].show, spec_titre_paragraf->nom_fichier_alarme);
     	  if (spec_titre_paragraf->val_init_arch)
     	    {
            bMotReserve (c_res_un, vect_ul[1].show);
            }
     	  else
     	    {
            bMotReserve (c_res_zero, vect_ul[1].show);
     	    }
          traitement_wnum (&(vect_ul[3]), spec_titre_paragraf->taille_fic_max);
          break;

        case impression_ala :
          (*niveau) = 4;
          (*indentation) = 3;
          (*nb_ul) = 2;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_impression, vect_ul[0].show);
     			spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
     			if (spec_titre_paragraf->val_init_imp)
     				{
            bMotReserve (c_res_un, vect_ul[1].show);
            }
     			else
     				{
            bMotReserve (c_res_zero, vect_ul[1].show);
     				}
          break;

        case groupe_ala:
          (*niveau) = 5;
          (*indentation) = 4;
          (*nb_ul) = 6;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_groupe, vect_ul[0].show);
     			spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
          StrCopy (vect_ul[1].show, spec_titre_paragraf->nom_groupe);
          traitement_wnum (&(vect_ul[2]), spec_titre_paragraf->coul_al_pres_non_ack);
          traitement_wnum (&(vect_ul[3]), spec_titre_paragraf->coul_al_pres_ack);
          traitement_wnum (&(vect_ul[4]), spec_titre_paragraf->coul_al_non_pres_non_ack);
          traitement_wnum (&(vect_ul[5]), spec_titre_paragraf->type_ack);
          break;

        case priorite_ala:
          (*niveau) = 6;
          (*indentation) = 5;
          (*nb_ul) = 2;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_priorite, vect_ul[0].show);
     			spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, donnees_geo->pos_specif_paragraf);
          traitement_wnum (&(vect_ul[1]), spec_titre_paragraf->numero_priorite);
          break;

        default :
          break;

        } // switch (titre_paragraf->IdParagraphe)
      break;

    case b_alarme :
      (*niveau) = 7;
      (*indentation) = 6;
      al_alarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, donnees_geo->pos_donnees);

      if (al_alarme->var_logique) // alarme logique
        {
        (*nb_ul) = 5; // au maximum
				init_vect_ul (vect_ul, *nb_ul);
        NomVarES (vect_ul[0].show, al_alarme->pos_var);
        if (al_alarme->indice_tableau == 0)
          { // c'est une variable simple
					indice_courant = 2;
          }
        else
          { // c'est une variable indicee (tableau)
          traitement_wnum(&(vect_ul[1]), al_alarme->indice_tableau);
					indice_courant = 3;
          }
        bMotReserve (al_alarme->test_alarme, vect_ul[indice_courant - 1].show);
        indice_courant++;
        }
      else
        { // alarme numerique
        (*nb_ul) = 7; // au maximum
				init_vect_ul (vect_ul, *nb_ul);
        NomVarES (vect_ul[0].show, al_alarme->pos_var);

        if (al_alarme->indice_tableau == 0)
          { // c'est une variable simple
					indice_courant = 2;
          }
        else
          { // c'est une variable indicee (tableau)
          traitement_wnum(&(vect_ul[1]), al_alarme->indice_tableau);
					indice_courant = 3;
          }
        bMotReserve (al_alarme->test_alarme, vect_ul[indice_courant - 1].show);
        switch (al_alarme->test_alarme)
          {
          case c_res_change :
          case c_res_stable :
						indice_courant++; // pointe sur le mess alarme
            break;

					case c_res_egal :
          case c_res_different :
          case c_res_superieur :
          case c_res_superieur_egal :
          case c_res_inferieur :
          case c_res_inferieur_egal :
					case c_res_passe_valeur :
          case c_res_passe_valeur_vers_h :
          case c_res_passe_valeur_vers_b :
          case c_res_prend_valeur :
					case c_res_prend_valeur_vers_h :
          case c_res_prend_valeur_vers_b :
          case c_res_quitte_valeur :
					case c_res_quitte_valeur_vers_b :
          case c_res_quitte_valeur_vers_h :
          case c_res_testbit_h :
          case c_res_testbit_b :
						if (al_alarme->une_variable1)
							{
              NomVarES (vect_ul[indice_courant].show, al_alarme->pos_var1);
              }
						else
							{ // une constante
							//erreur_encode_r((indice_courant + 1),al_alarme->cste1_alarme);
							erreur_encode_r (&(vect_ul[indice_courant]), al_alarme->cste1_alarme);
							}
						indice_courant = indice_courant + 2; // pointe sur le mess alarme
            break;

          case c_res_entre_alarme :
          case c_res_pavhd :
          case c_res_pavbd :
					 if (al_alarme->une_variable1)
						 {
             NomVarES (vect_ul[indice_courant].show, al_alarme->pos_var1);
             }
					 else
						 { // une constante
						 erreur_encode_r (&(vect_ul[indice_courant]), al_alarme->cste1_alarme);
						 }

					 if (al_alarme->une_variable2)
						 {
             NomVarES (vect_ul[indice_courant + 1].show, al_alarme->pos_var2);
             }
					 else
						 { // une constante
						 erreur_encode_r (&(vect_ul[indice_courant + 1]), al_alarme->cste2_alarme);
						 }

					 indice_courant = indice_courant + 3; // pointe sur le mess alarme
           break;

					default :
            break;
          } // fin switch
        } // fin alarmes numeriques

      lire_message_alarme (mess_80, al_alarme->numero_enregistrement);
      StrConcatChar (vect_ul[indice_courant - 1].show, '\"');
      StrConcat (vect_ul[indice_courant - 1].show, mess_80);
      StrConcatChar (vect_ul[indice_courant - 1].show, '\"');

      if (al_alarme->pos_message != 0)
        { // il y a une variable message
        indice_courant++; // pointe sur variable message
        NomVarES (vect_ul[indice_courant - 1].show, al_alarme->pos_message);
        }
      (*nb_ul) = indice_courant;
      break;

    default:
      break;

    } // switch
  } // genere_vect_ul

//---------------------------------------------------------------------------
static BOOL valide_vect_ul (PUL vect_ul, int nb_ul, DWORD ligne,
	REQUETE_EDIT action, DWORD *profondeur,
	PID_PARAGRAPHE tipe_paragraf,
	DWORD *numero_repere,
	DWORD *pos_init, BOOL *exist_init,
	PC_ALARME	spec_alarme,
	char *message_alarme,
	PSPEC_TITRE_PARAGRAPHE_ALARME	spec_titre_paragraf,
	DWORD *erreur)
  {
  DWORD nbr_imbrication;
  ID_MOT_RESERVE n_fonction;
  DWORD position_es;
  DWORD mot;
  DWORD wErreur;
  BOOL arrive_au_message;
  BOOL var_tab;
  BOOL booleen;
  BOOL exist_ident;
  BOOL un_log;
  BOOL un_num;
  FLOAT reel;
  t_contexte_al old_contexte;
  char tampon_str[82];
  int indice_courant;
  PENTREE_SORTIE es;
  DWORD position_ident;
  DWORD wNbCarMaxMesAl;
  DWORD wNbCarMaxFicArch;
  DWORD wNbCarMaxLineImp;

  //***********************************************
  #define err(code) (*erreur) = code;return (FALSE)  //Sale Sioux
  //***********************************************

  old_contexte = contexte_al;
  (*erreur) = 0;
  if (nb_ul < 1)
    {
    err(IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
    }
  if (nb_ul > 8)
    {
    err(31);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, &position_ident))
    {
    if (!bReconnaitMotReserve ((vect_ul+0)->show, &n_fonction))
      {
      if (!bReconnaitConstante ((vect_ul+0), &booleen, &reel, &un_log, &un_num, exist_init, pos_init))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if ((un_log) || (un_num))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      (*numero_repere) = b_pt_constant;
      }
    else
      {
      switch (n_fonction)
        {
        case c_res_surveillance :
					if (ligne > 1)
            {
            err(63);
            }
					if (nb_ul == 2)
						{
            if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                                   &un_log, &un_num, exist_init,pos_init))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
				    if (!un_log )
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            spec_titre_paragraf->fen_al_x  = -1;
            spec_titre_paragraf->fen_al_y  = -1;
            spec_titre_paragraf->fen_al_cx = -1;
            spec_titre_paragraf->fen_al_cy = -1;
            spec_titre_paragraf->type_horodatage = HORODATAGE_PC;
            spec_titre_paragraf->tempo_stabilite_evenement = 0;
            }
					else
            {
            if (nb_ul >= 6)
              {
              if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                                     &un_log, &un_num, exist_init,pos_init))
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              if (!un_log )
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              spec_titre_paragraf->fen_al_x  = (int)reel;
              if (!bReconnaitConstanteNum ((vect_ul+3), &booleen, &reel, &mot))
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              spec_titre_paragraf->fen_al_y  = (int)reel;
              if (!bReconnaitConstanteNum ((vect_ul+4), &booleen, &reel, &mot))
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              spec_titre_paragraf->fen_al_cx  = (int)reel;
              if (!bReconnaitConstanteNum ((vect_ul+5), &booleen, &reel, &mot))
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              spec_titre_paragraf->fen_al_cy  = (int)reel;
              if (nb_ul == 6)
                {
                spec_titre_paragraf->type_horodatage = HORODATAGE_PC;
                spec_titre_paragraf->tempo_stabilite_evenement = 0;
                }
              else
                {
                if (nb_ul == 8)
                  {
                  if (!bReconnaitConstanteNum ((vect_ul+6), &booleen, &reel, &mot))
                    {
                    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                    }
                  spec_titre_paragraf->type_horodatage  = (int)reel;
                  if ((spec_titre_paragraf->type_horodatage != HORODATAGE_PC) && (spec_titre_paragraf->type_horodatage != HORODATAGE_SOURCE))
                    {
                    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                    }
                  if (!bReconnaitConstanteNum ((vect_ul+7), &booleen, &reel, &mot))
                    {
                    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                    }
                  spec_titre_paragraf->tempo_stabilite_evenement  = (LONG)reel;
                  }
                else
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                }
              }
            else  // < 6 et != 2
              {
              if (nb_ul != 1)
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              bMotReserve (c_res_zero, vect_ul[1].show);
              spec_titre_paragraf->fen_al_x  = -1;
              spec_titre_paragraf->fen_al_y  = -1;
              spec_titre_paragraf->fen_al_cx = -1;
              spec_titre_paragraf->fen_al_cy = -1;
              spec_titre_paragraf->type_horodatage = HORODATAGE_PC;
              spec_titre_paragraf->tempo_stabilite_evenement = 0;
              }
						}
          contexte_al.type_horodatage = spec_titre_paragraf->type_horodatage;
          (*profondeur) = 1;
					nbr_imbrication = 0;
					(*numero_repere) = b_titre_paragraf_al;
					(*tipe_paragraf) = surveillance_ala;
          break;
 
        case c_res_visualisation :
				  if (ligne > 2)
            {
            err(63);
            }
					if (nb_ul == 3)
						{
            if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                                  &un_log, &un_num, exist_init, pos_init))
              {
              err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						if (!un_log)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }

            if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						spec_titre_paragraf->nbre_al_maxi = (DWORD)reel;
            }
					else
						{
						if (nb_ul != 1)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            bMotReserve (c_res_zero, vect_ul[1].show);
						spec_titre_paragraf->nbre_al_maxi = 500;
						}
					(*profondeur) = 2;
					nbr_imbrication = 1;
					(*numero_repere) = b_titre_paragraf_al;
					(*tipe_paragraf) = visualisation_ala;
          break;
 
        case c_res_archivage :
				  if (ligne > 4)
            {
            err(63);
            }
					if (nb_ul == 4)
						{
            if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                                  &un_log, &un_num, exist_init, pos_init))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
				    if (!un_log)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }

            if (!bReconnaitConstante ((vect_ul+2), &booleen, &reel,
                                  &un_log, &un_num, exist_init, pos_init))
              {
              err(4);
              }
						if (un_log || un_num)
              {
              err(4);
              }

            StrCopy (tampon_str, vect_ul[2].show);
            StrDelete (vect_ul[2].show, 0, 1); // enleve "
            StrSetLength (vect_ul[2].show, StrLength (vect_ul[2].show) - 1); // enleve "
            if ((StrLength (vect_ul[2].show) > 40) ||
                (StrCharIsInStr ('.', vect_ul[2].show)))
              {
              err (4);
              }
            if (!bFicNomValide (vect_ul[2].show))
              {
              err(4);
              }
            StrCopy (vect_ul[2].show, tampon_str);

            if (!bReconnaitConstanteNum ((vect_ul+3), &booleen, &reel, &mot))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						spec_titre_paragraf->taille_fic_max = (DWORD)reel;
            }
					else
						{
						if (nb_ul != 1)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            bMotReserve (c_res_zero, vect_ul[1].show);
            StrCopy (vect_ul[2].show, "\"archive\"");
						spec_titre_paragraf->taille_fic_max = 1000;
						}
					(*profondeur) = 3;
					nbr_imbrication = 2;
					(*numero_repere) = b_titre_paragraf_al;
					(*tipe_paragraf) = archivage_ala;
          break;
 
        case c_res_impression :
					if (ligne > 4)
            {
            err(63);
            }
					if ((nb_ul > 1) && (nb_ul < 4)) // 2 ou 3 champs pour compatibilite
						{
            if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                                  &un_log, &un_num, exist_init, pos_init))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						if (!(un_log))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            }
					else
						{
						if (nb_ul != 1)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            bMotReserve (c_res_zero, vect_ul[1].show);
						}
					(*profondeur) = 4;
					nbr_imbrication = 3;
					(*numero_repere) = b_titre_paragraf_al;
					(*tipe_paragraf) = impression_ala;
          break;

				case c_res_res_groupe_alarme: // compatibilit� ajout�e pour probl�me mot r�serv� Anglais en conflit
        case c_res_groupe :
          if ((nb_ul < 2) || (nb_ul > 6))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                                &un_log, &un_num, exist_init, pos_init))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
					if (un_log || un_num)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }

          if ((vect_ul[1].show[StrLength (vect_ul[1].show) - 1] != '"'))
						{
            StrConcatChar (vect_ul[1].show, '\"');
						}
          if (StrLength (vect_ul[1].show) > 12)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrCopy (contexte_al.nom_groupe, vect_ul[1].show);
          if (nb_ul < 3)   //Syntaxe < 4.20
            {
            spec_titre_paragraf->coul_al_pres_non_ack = 19; //Noir sur Rouge
            spec_titre_paragraf->coul_al_pres_ack = 16; //Noir sur Blanc
            spec_titre_paragraf->coul_al_non_pres_non_ack = 21; //Noir sur Vert
            spec_titre_paragraf->type_ack = 0;
            }
          else
            {
            if (nb_ul != 6)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            else
              {
              if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
                {
                err(414);
                }
              spec_titre_paragraf->coul_al_pres_non_ack = (DWORD)reel;
              if (spec_titre_paragraf->coul_al_pres_non_ack > 255)
                {
                err(43);
                }
              if (!bReconnaitConstanteNum ((vect_ul+3), &booleen, &reel, &mot))
                {
                err(414);
                }
              spec_titre_paragraf->coul_al_pres_ack = (DWORD)reel;
              if (spec_titre_paragraf->coul_al_pres_ack > 255)
                {
                err(43);
                }
              if (!bReconnaitConstanteNum ((vect_ul+4), &booleen, &reel, &mot))
                {
                err(414);
                }
              spec_titre_paragraf->coul_al_non_pres_non_ack = (DWORD)reel;
              if (spec_titre_paragraf->coul_al_non_pres_non_ack > 255)
                {
                err(43);
                }
              if (!bReconnaitConstanteNum ((vect_ul+5), &booleen, &reel, &mot))
                {
                err(443);
                }
              spec_titre_paragraf->type_ack = (DWORD)reel;
              if (spec_titre_paragraf->type_ack > 2)
                {
                err(443);
                }
              }
            }
					(*profondeur) = 5;
					nbr_imbrication = 4;
					(*numero_repere) = b_titre_paragraf_al;
					(*tipe_paragraf) = groupe_ala;
          break;

        case c_res_priorite :
					if (nb_ul != 2)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }

          if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel, &un_log, &un_num, exist_init, pos_init))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
					if (!un_num)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
					if ((reel < 1) || (reel > 10))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
					contexte_al.numero_priorite = (DWORD)reel;
					spec_titre_paragraf->numero_priorite = contexte_al.numero_priorite;
					(*profondeur) = 6;
					nbr_imbrication = 5;
					(*numero_repere) = b_titre_paragraf_al;
					(*tipe_paragraf) = priorite_ala;
          break;

        default :
          err (32);
          break;

        } // switch n_fonction
      if (!bValideInsereLigneParagraphe (b_titre_paragraf_al, ligne, 
				(*profondeur), nbr_imbrication, erreur))
        {
        contexte_al = old_contexte;
        err((*erreur));
        }
      } // mot res
    }// non identificateur
  else
    {
    if (bReconnaitESExistante ((vect_ul+0), &position_es))
      { 
			// variable deja declaree
      es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
      var_tab = (BOOL)((es->sens == c_res_variable_ta_e) ||
		          (es->sens == c_res_variable_ta_s) ||
		          (es->sens == c_res_variable_ta_es));

      if (contexte_al.numero_priorite == 0)
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      switch (es->genre)
        {
        case c_res_logique :
					if ((nb_ul < 3) || (nb_ul > 5))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (contexte_al.type_horodatage == HORODATAGE_SOURCE)
            {
            err(447);
            }
					spec_alarme->var_logique = TRUE;
					spec_alarme->pos_var = position_es;
          if (!bReconnaitMotReserve ((vect_ul+1)->show, &n_fonction))
						{ // ce n'est pas un mot reserve donc c'est un indice tableau
            if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                                  &un_log, &un_num, exist_init, pos_init))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						if (!un_num)
              {
              err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
					 // c'est un indice de tableau
						if (!var_tab)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						spec_alarme->indice_tableau = (DWORD)reel;
						indice_courant = 3; // pointe sur mot reserve
            }
					else
						{	// c'est un mot reserve donc pas un indice tableau
						if (var_tab)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						indice_courant = 2; // pointe sur mot reserve
						spec_alarme->indice_tableau = 0;
						}
          if (!bReconnaitMotReserve ((vect_ul + indice_courant - 1)->show, &n_fonction))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
					if ((n_fonction != c_res_un) && (n_fonction != c_res_zero) &&
							(n_fonction != c_res_change) && (n_fonction != c_res_stable))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
					spec_alarme->test_alarme = n_fonction;
					indice_courant++; // pointe sur "message"
          StrCopy (tampon_str, vect_ul[indice_courant - 1].show);
          if (tampon_str [StrLength (tampon_str) - 1] != '"')
            {
            StrConcatChar (tampon_str, '\"');
            }
          definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
          if ((StrLength (tampon_str) > wNbCarMaxMesAl + 2))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrDelete (tampon_str, 0, 1); // enleve "
          StrSetLength (tampon_str, StrLength (tampon_str) - 1); // enleve "
          StrCopy (message_alarme, tampon_str);
					indice_courant++; // pointe sur variable message (s'il y en a une)

					if (nb_ul > indice_courant)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
					if (nb_ul == indice_courant)
            { // il y a une variable message
						if (!va_deja_declaree (&(vect_ul[indice_courant-1]), c_res_message,
                                   &spec_alarme->pos_message, &wErreur))
              {
              err (wErreur);
              }
            }
          else
            { // il n'y a pas de variable message
            spec_alarme->pos_message = 0;
            }

					(*numero_repere) = b_alarme;
          break;

        case c_res_numerique :
					spec_alarme->une_variable1 = FALSE; // initialisations
					spec_alarme->une_variable2 = FALSE; //	      "
					if ((nb_ul < 3) || (nb_ul > 7))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
					spec_alarme->var_logique = FALSE;
					spec_alarme->pos_var = position_es;
          if (!bReconnaitMotReserve ((vect_ul + 1)->show, &n_fonction))
						{ // ce n'est pas un mot reserve donc c'est un indice tableau
            if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                                  &un_log, &un_num, exist_init, pos_init))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						if (!un_num)
              {
              err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						// c'est un indice de tableau
						if (!var_tab)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						spec_alarme->indice_tableau = (DWORD)reel;
						indice_courant = 4; // pointe sur mess alarme
            if (!bReconnaitMotReserve ((vect_ul + 2)->show, &n_fonction))
              {
							err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            }
					else
						{	// c'est un mot reserve donc pas un indice tableau
						if (var_tab)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						indice_courant = 3; // pointe sur mess alarme
						spec_alarme->indice_tableau = 0;
						}

					switch (n_fonction)
            {
						case c_res_change :
            case c_res_stable :
							if (var_tab)
								{
								if ((nb_ul < 4) || (nb_ul > 5))
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                }
							else
								{
								if ((nb_ul < 3) || (nb_ul > 4))
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
								}
              break;

						case c_res_egal :
            case c_res_different :
            case c_res_superieur :
            case c_res_superieur_egal :
            case c_res_inferieur :
            case c_res_inferieur_egal :
						case c_res_passe_valeur :
            case c_res_passe_valeur_vers_h :
            case c_res_passe_valeur_vers_b :
            case c_res_prend_valeur :
						case c_res_prend_valeur_vers_h :
            case c_res_prend_valeur_vers_b :
            case c_res_quitte_valeur :
            case c_res_quitte_valeur_vers_b :
						case c_res_quitte_valeur_vers_h :
            case c_res_testbit_h :
            case c_res_testbit_b :
							if (var_tab)
								{
								if ((nb_ul < 5) || (nb_ul > 6))
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                }
							else
								{
								if ((nb_ul < 4) || (nb_ul > 5))
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
								}
              break;

            case c_res_entre_alarme :
            case c_res_pavhd :
            case c_res_pavbd :
							if (var_tab)
								{
								if ((nb_ul < 6) || (nb_ul > 7))
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                }
							else
								{
								if ((nb_ul < 5) || (nb_ul > 6))
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
 								}
              break;

						default :
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              break;
						} // fin switch
					spec_alarme->test_alarme = n_fonction;

					//nouvelle version

					arrive_au_message = FALSE;
          if (bReconnaitConstante ((vect_ul+indice_courant-1), &booleen, &reel,
                               &un_log, &un_num, exist_init, pos_init))
						{
						if (un_log)
              {
              err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
						if (un_num) // c'est la cste num 1
							{
							spec_alarme->cste1_alarme = reel;
              }
            else
     					{ // c'est le message
     					arrive_au_message = TRUE;
     					}
            }
     	  else
     	    { // c'est la variable 1
     	    if (!va_deja_declaree (&(vect_ul[indice_courant-1]), c_res_numerique,
                                   &spec_alarme->pos_var1, &wErreur))
              {
              err (wErreur);
              }
     	    /* test si la var utilisee est compatible */
     	    spec_alarme->une_variable1 = TRUE;
     	    }

          if (!arrive_au_message)
						{
						indice_courant++; // pointe sur mess alarme ou 2� num

            if (bReconnaitConstante ((vect_ul+indice_courant-1), &booleen, &reel,
                                 &un_log, &un_num, exist_init, pos_init))
							{
							if (un_log)
                {
                err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
							if (un_num) // c'est la constante num�rique 2
								{
								spec_alarme->cste2_alarme = reel;
                }
							else
								{ // c'est le message
								arrive_au_message = TRUE;
								}
              }
						else
							{ // c'est la variable 2
							if (!va_deja_declaree (&(vect_ul[indice_courant-1]), c_res_numerique,
																					 &spec_alarme->pos_var2, &wErreur))
                {
                err (wErreur);
                }
              /* test si la var utilisee est compatible */
   						spec_alarme->une_variable2 = TRUE;
   						}
   					if (!arrive_au_message)
   						{
   						indice_courant++; // pointe sur mess alarme
   						}
   					}

          StrCopy (tampon_str, vect_ul[indice_courant-1].show);
          if (tampon_str [StrLength (tampon_str) - 1] != '"' )
            {
            StrConcatChar (tampon_str, '\"');
            }
          definition_alarme (&wNbCarMaxMesAl, &wNbCarMaxFicArch, &wNbCarMaxLineImp);
          if (StrLength (tampon_str) > wNbCarMaxMesAl+2)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrDelete (tampon_str, 0, 1); // enleve "
          StrSetLength (tampon_str, StrLength (tampon_str) - 1); // enleve "
          StrCopy (message_alarme, tampon_str);
          indice_courant++; // pointe sur variable message facultative

    	  if (nb_ul > indice_courant)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
    	  if (nb_ul == indice_courant)
            { // il y a une variable message
    	    if (!va_deja_declaree (&(vect_ul[indice_courant-1]), c_res_message,
                                   &spec_alarme->pos_message, &wErreur))
              {
              err (wErreur);
              }
            }
          else
            { // il n'y a pas de variable message
            spec_alarme->pos_message = 0;
            }

   				(*numero_repere) = b_alarme;
          break;

        default :
          err(IDSERR_LA_DESCRIPTION_EST_INVALIDE); // le genre est ni logique ni numerique
          break;

        } // fin switch genre
      (*profondeur) = 6;
      nbr_imbrication = 5;
      }
    else // la variable n'est pas deja declaree (n'existe pas)
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    }
  return ((BOOL)((*erreur) == 0));
  } // valide_vect_ul

//**************************************************************************
// renvoie vrai s'il y a moins de 21 groupes
static BOOL test_groupe (char *nom_groupe, DWORD pos_specif_paragraf)
  {
  PGEO_ALARME	geo_al;
  GEO_ALARME val_geo;
  BOOL test;
  DWORD ligne_cour;
  DWORD nb_groupe;
  PTITRE_PARAGRAPHE ads_titre_paragraf;
  PSPEC_TITRE_PARAGRAPHE_ALARME	spec_titre_paragraf;

  test = TRUE;
  if (existe_repere (b_geo_al) && (nb_enregistrements (szVERIFSource, __LINE__, b_geo_al) != 0))
    {
    ligne_cour = 0;
    nb_groupe = 0;
    while ((ligne_cour < nb_enregistrements (szVERIFSource, __LINE__, b_geo_al)) && test)
      {
      ligne_cour++;
      geo_al = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, ligne_cour);
      val_geo = (*geo_al);
      if (val_geo.numero_repere == b_titre_paragraf_al)
        { // paragraf al
        ads_titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_al, val_geo.pos_paragraf);
        if (ads_titre_paragraf->IdParagraphe == groupe_ala)
          { // paragraf groupe
          nb_groupe++;
          test = (BOOL)(nb_groupe < NB_GROUPE_MAX_AL);
          if (test && (pos_specif_paragraf != val_geo.pos_specif_paragraf))
            { // tester si un autre groupe a le meme nom sauf si c'est le meme
            spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, val_geo.pos_specif_paragraf);
            test = !bStrEgales (nom_groupe, spec_titre_paragraf->nom_groupe);
            }
          } // paragraphe groupe
        } // paragraf al
      } // while
    } // existe_repere
  return (test);
  } // fin de test_groupe

//-------------------------------------------------------------
static void  valide_ligne_al 
	(REQUETE_EDIT action, DWORD ligne,
	 PUL vect_ul, int *nb_ul,
	 BOOL *supprime_ligne, BOOL *accepte_ds_bd,
	 DWORD *erreur_val, int *niveau, int *indentation)
  {
  PGEO_ALARME	donnees_geo;
  GEO_ALARME val_geo;
  C_ALARME spec_alarme;
  SPEC_TITRE_PARAGRAPHE_ALARME spec_titre_paragraf;
  DWORD erreur;
  PC_ALARME	ads_alarme;
  char message_alarme[TAILLE_MAX_MESSAGE_AL-1];
  char chaine[82];
  PSPEC_TITRE_PARAGRAPHE_ALARME	ads_spec_titre_paragraf;
  PTITRE_PARAGRAPHE titre_paragraf;
  DWORD profondeur;
  DWORD pos_paragraf;
  DWORD pos_init;
  DWORD numero_repere;
  BOOL exist_init;
  BOOL un_paragraphe;
  ID_PARAGRAPHE tipe_paragraf;

  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;
  switch (action)
    {
    case MODIFIE_LIGNE:
      consulte_titre_paragraf_al (ligne);
      if (valide_vect_ul(vect_ul, (*nb_ul), ligne, MODIFIE_LIGNE,
                         &profondeur, &tipe_paragraf, &numero_repere,
                         &pos_init, &exist_init,
                         &spec_alarme, message_alarme,
                         &spec_titre_paragraf, &erreur))
        {
        donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, ligne);
        val_geo = (*donnees_geo);
        if (val_geo.numero_repere == numero_repere)
          {
          switch (numero_repere)
            {
	    case b_alarme :
	      ads_alarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, val_geo.pos_donnees);
	      if (ads_alarme->pos_var != spec_alarme.pos_var)
	        {
		ReferenceES (spec_alarme.pos_var);
		DeReferenceES (ads_alarme->pos_var);
	        }

	      //nouvelle version
	      switch (ads_alarme->test_alarme) // switch (ancien
                {
	        case c_res_egal :
                case c_res_different :
                case c_res_superieur :
                case c_res_superieur_egal :
                case c_res_inferieur :
                case c_res_inferieur_egal :
	        case c_res_passe_valeur :
                case c_res_passe_valeur_vers_h :
                case c_res_passe_valeur_vers_b :
                case c_res_prend_valeur :
	        case c_res_prend_valeur_vers_h :
                case c_res_prend_valeur_vers_b :
                case c_res_quitte_valeur :
	        case c_res_quitte_valeur_vers_b :
                case c_res_quitte_valeur_vers_h :
                case c_res_testbit_h :
                case c_res_testbit_b :
		  if (ads_alarme->une_variable1)
		    {
		    DeReferenceES (ads_alarme->pos_var1);
		    }
                  break;

                case c_res_entre_alarme :
                case c_res_pavhd :
                case c_res_pavbd :
		  if (ads_alarme->une_variable1)
		    {
		    DeReferenceES (ads_alarme->pos_var1);
		    }
		  if (ads_alarme->une_variable2)
		    {
		    DeReferenceES (ads_alarme->pos_var2);
		    }
                  break;

	        default :
                  break;
	        } // fin switch

	      switch (spec_alarme.test_alarme) // switch (nouveau
                {
	        case c_res_egal :
                case c_res_different :
                case c_res_superieur :
                case c_res_superieur_egal :
                case c_res_inferieur :
                case c_res_inferieur_egal :
	        case c_res_passe_valeur :
                case c_res_passe_valeur_vers_h :
                case c_res_passe_valeur_vers_b :
                case c_res_prend_valeur :
	        case c_res_prend_valeur_vers_h :
                case c_res_prend_valeur_vers_b :
                case c_res_quitte_valeur :
	        case c_res_quitte_valeur_vers_b :
                case c_res_quitte_valeur_vers_h :
                case c_res_testbit_h :
                case c_res_testbit_b :
		  if (spec_alarme.une_variable1)
		    {
		    ReferenceES (spec_alarme.pos_var1);
		    }
                  break;

                case c_res_entre_alarme :
                case c_res_pavhd :
                case c_res_pavbd :
		  if (spec_alarme.une_variable1)
		    {
		    ReferenceES (spec_alarme.pos_var1);
		    }
		  if (spec_alarme.une_variable2)
		    {
		    ReferenceES (spec_alarme.pos_var2);
		    }
                  break;

	        default :
                  break;
	        } // fin switch

              if (ads_alarme->pos_message != 0)
                {
                DeReferenceES (ads_alarme->pos_message);
                }
              if (spec_alarme.pos_message != 0)
                {
                ReferenceES (spec_alarme.pos_message);
                }

	      enleve_message_alarme (ads_alarme->numero_enregistrement);
	      insere_message_alarme (message_alarme, &spec_alarme.numero_enregistrement);
	      (*ads_alarme) = spec_alarme;
              break;

            case b_pt_constant :
              modifie_cst_bd_c (vect_ul[0].show, &(val_geo.pos_donnees));
              break;

	    case b_titre_paragraf_al :
	      titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_al, val_geo.pos_paragraf);
              if (titre_paragraf->IdParagraphe == tipe_paragraf)
                {
		if ((tipe_paragraf == surveillance_ala)  ||
		    (tipe_paragraf == visualisation_ala) ||
		    (tipe_paragraf == archivage_ala)     ||
		    (tipe_paragraf == impression_ala)    ||
		    (tipe_paragraf == groupe_ala)        ||
		    (tipe_paragraf == priorite_ala)      )
		  {
		  ads_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, val_geo.pos_specif_paragraf);
                  switch (tipe_paragraf)
                    {
		    case surveillance_ala :
                      bMotReserve (c_res_un, chaine);
                      if (bStrEgales (vect_ul[1].show, chaine))
		        {
		        spec_titre_paragraf.val_init_surv = TRUE;
                        }
		      else
		        {
		        spec_titre_paragraf.val_init_surv = FALSE;
		        }
                      break;

		    case visualisation_ala :
                      bMotReserve (c_res_un, chaine);
                      if (bStrEgales (vect_ul[1].show, chaine))
		        {
		        spec_titre_paragraf.val_init_vis = TRUE;
                        }
		      else
		        {
		        spec_titre_paragraf.val_init_vis = FALSE;
		        }
                      break;

		    case archivage_ala :
                      StrCopy (spec_titre_paragraf.nom_fichier_alarme, vect_ul[2].show);
                      bMotReserve (c_res_un, chaine);
                      if (bStrEgales (vect_ul[1].show, chaine))
		        {
		        spec_titre_paragraf.val_init_arch = TRUE;
                        }
		      else
		        {
		        spec_titre_paragraf.val_init_arch = FALSE;
		        }
                      break;

		    case impression_ala :
                      bMotReserve (c_res_un, chaine);
                      if (bStrEgales (vect_ul[1].show, chaine))
		        {
		        spec_titre_paragraf.val_init_imp = TRUE;
                        }
		      else
		        {
		        spec_titre_paragraf.val_init_imp = FALSE;
		        }
                      break;

		    case groupe_ala :
		      if (test_groupe (vect_ul[1].show, val_geo.pos_specif_paragraf)) // moins de 21 paragraphes groupe
		        {
                        StrCopy (spec_titre_paragraf.nom_groupe, vect_ul[1].show);
                        }
		      else
		        {
		        erreur = 63; //il ne peut pas y avoir plus de 20 paragraphes groupes
		        }
                      break;

		    case priorite_ala :
                      break;

		    default :
                      break;
		    } // fin switch

                  if (erreur == 0)
                    {
									   (*ads_spec_titre_paragraf) = spec_titre_paragraf;
                    }
                  }
                }
              else
                {
                erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
                }
              break;

            default:
              break;

            } // switch numero repere
          }// meme repere
        else
          {
          erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
          }
        } // valide vect_ul Ok
      if (erreur != 0 )
        {
        genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
        (*erreur_val) = erreur;
        }
      else
        {
        donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, ligne);
        (*donnees_geo) = val_geo;
        }
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = FALSE;
      break;

    case INSERE_LIGNE :
      consulte_titre_paragraf_al (ligne - 1);
      if (valide_vect_ul(vect_ul, (*nb_ul), ligne, INSERE_LIGNE,
                         &profondeur, &tipe_paragraf, &numero_repere,
                         &pos_init, &exist_init,
                         &spec_alarme,
			 message_alarme,
                         &spec_titre_paragraf, &erreur))
        {
        pos_paragraf = 0; // init pour mise a jour donnees paragraf
        val_geo.numero_repere = numero_repere;
        switch (numero_repere)
          {
          case b_pt_constant :
            AjouteConstanteBd (pos_init, exist_init, vect_ul[0].show, &(val_geo.pos_donnees));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

	  case b_alarme :
	    if (!existe_repere (b_alarme))
              {
	      cree_bloc (b_alarme, sizeof (spec_alarme), 0);
              }
	    val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, b_alarme) + 1;
	    insere_enr (szVERIFSource, __LINE__, 1, b_alarme, val_geo.pos_donnees);
	    insere_message_alarme (message_alarme, &spec_alarme.numero_enregistrement);
	    ads_alarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, val_geo.pos_donnees);
	    (*ads_alarme) = spec_alarme;
	    ReferenceES (spec_alarme.pos_var);

	    // nouvelle version
	    switch (spec_alarme.test_alarme) // switch (sur le nouveau
              {
	      case c_res_egal :
              case c_res_different :
              case c_res_superieur :
              case c_res_superieur_egal :
              case c_res_inferieur :
              case c_res_inferieur_egal :
	      case c_res_passe_valeur :
              case c_res_passe_valeur_vers_h :
              case c_res_passe_valeur_vers_b :
              case c_res_prend_valeur :
	      case c_res_prend_valeur_vers_h :
              case c_res_prend_valeur_vers_b :
              case c_res_quitte_valeur :
	      case c_res_quitte_valeur_vers_b :
              case c_res_quitte_valeur_vers_h :
              case c_res_testbit_h :
              case c_res_testbit_b :
	        if (spec_alarme.une_variable1)
	          {
		  ReferenceES (spec_alarme.pos_var1);
	          }
                break;

              case c_res_entre_alarme :
              case c_res_pavhd :
              case c_res_pavbd :
	        if (spec_alarme.une_variable1)
	          {
		  ReferenceES (spec_alarme.pos_var1);
	          }

	        if (spec_alarme.une_variable2)
	          {
		  ReferenceES (spec_alarme.pos_var2);
	          }
                break;

	      default :
                break;
	      } // fin switch

            if (spec_alarme.pos_message != 0)
              {
              ReferenceES (spec_alarme.pos_message);
              }

            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            break;

	  case b_titre_paragraf_al :
            if ((tipe_paragraf == surveillance_ala)  ||
	        (tipe_paragraf == visualisation_ala) ||
                (tipe_paragraf == archivage_ala)     ||
                (tipe_paragraf == impression_ala)    ||
                (tipe_paragraf == groupe_ala)        ||
                (tipe_paragraf == priorite_ala))
              {
	      if (!existe_repere (b_spec_titre_paragraf_al))
                {
                val_geo.pos_specif_paragraf = 1;
		cree_bloc (b_spec_titre_paragraf_al, sizeof (spec_titre_paragraf), 0);
                }
              else
                {
		val_geo.pos_specif_paragraf = nb_enregistrements (szVERIFSource, __LINE__, b_spec_titre_paragraf_al) + 1;
	        }

	      switch (tipe_paragraf)
                {
	        case surveillance_ala :
                  bMotReserve (c_res_un, chaine);
                  if (bStrEgales (vect_ul[1].show, chaine))
		    {
		    spec_titre_paragraf.val_init_surv = TRUE;
                    }
                  else
		    {
		    spec_titre_paragraf.val_init_surv = FALSE;
		    }
                  break;

	        case visualisation_ala :
                  bMotReserve (c_res_un, chaine);
                  if (bStrEgales (vect_ul[1].show, chaine))
		    {
		    spec_titre_paragraf.val_init_vis = TRUE;
                    }
		  else
		    {
		    spec_titre_paragraf.val_init_vis = FALSE;
		    }
                  break;

	        case archivage_ala :
                  bMotReserve (c_res_un, chaine);
                  if (bStrEgales (vect_ul[1].show, chaine))
		    {
		    spec_titre_paragraf.val_init_arch = TRUE;
                    }
		  else
		    {
		    spec_titre_paragraf.val_init_arch = FALSE;
		    }
                  StrCopy (spec_titre_paragraf.nom_fichier_alarme, vect_ul[2].show);
                  break;

	        case impression_ala :
                  bMotReserve (c_res_un, chaine);
                  if (bStrEgales (vect_ul[1].show, chaine))
		    {
		    spec_titre_paragraf.val_init_imp = TRUE;
                    }
		  else
		    {
		    spec_titre_paragraf.val_init_imp = FALSE;
		    }
                  break;

	        case groupe_ala :
		  if (test_groupe (vect_ul[1].show, val_geo.pos_specif_paragraf))
		    { // moins de 21 paragraphes groupe
                    StrCopy (spec_titre_paragraf.nom_groupe, vect_ul[1].show);
                    }
		  else
		    {
		    erreur = 63; //il ne peut pas y avoir plus de 20 paragraphes groupes
		    }
                  break;

	        case priorite_ala :
                  break;

	        default :
                  break;
	        } // fin switch

	      if (erreur == 0)
	        {
		insere_enr (szVERIFSource, __LINE__, 1, b_spec_titre_paragraf_al, val_geo.pos_specif_paragraf);
		ads_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, val_geo.pos_specif_paragraf);
		(*ads_spec_titre_paragraf) = spec_titre_paragraf;
	        }
              }
	    if (erreur == 0)
	      {
              (*accepte_ds_bd) = TRUE;
              (*supprime_ligne) = FALSE;
	      InsereTitreParagraphe (b_titre_paragraf_al, ligne, profondeur,
                                       tipe_paragraf, &(val_geo.pos_paragraf));
	      pos_paragraf = val_geo.pos_paragraf;
	      }
            break;

	  default :
            break;
          } //switch (donnees_specif
	if (erreur == 0)
	  {
	  if (numero_repere != 0)
	    {
	    if (!existe_repere (b_geo_al))
	      {
	      cree_bloc (b_geo_al, sizeof (val_geo), 0);
	      }
	    insere_enr (szVERIFSource, __LINE__, 1, b_geo_al, ligne);
	    donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, ligne);

	    (*donnees_geo) = val_geo;
	    un_paragraphe = (BOOL)(numero_repere == b_titre_paragraf_al);
	    MajDonneesParagraphe (b_titre_paragraf_al, ligne, INSERE_LIGNE,
				    un_paragraphe, profondeur, pos_paragraf);
            }
          }
	else
	  { // erreur
          (*accepte_ds_bd) = FALSE;
          (*erreur_val) = erreur;
          }
        }
      else
        {
        (*accepte_ds_bd) = FALSE;
        (*erreur_val) = erreur;
        }
      break;

    case SUPPRIME_LIGNE :
      consulte_titre_paragraf_al (ligne);
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      donnees_geo = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, ligne);
      val_geo = (*donnees_geo);
      switch (val_geo.numero_repere)
        {
        case b_pt_constant :
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;

        case b_alarme :
          ads_alarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, val_geo.pos_donnees);
          DeReferenceES (ads_alarme->pos_var);

          //nouvelle version
          switch (ads_alarme->test_alarme) // switch (ancien
            {
	    case c_res_egal :
            case c_res_different :
            case c_res_superieur :
            case c_res_superieur_egal :
            case c_res_inferieur :
            case c_res_inferieur_egal :
	    case c_res_passe_valeur :
            case c_res_passe_valeur_vers_h :
            case c_res_passe_valeur_vers_b :
            case c_res_prend_valeur :
	    case c_res_prend_valeur_vers_h :
            case c_res_prend_valeur_vers_b :
            case c_res_quitte_valeur :
	    case c_res_quitte_valeur_vers_b :
            case c_res_quitte_valeur_vers_h :
            case c_res_testbit_h :
            case c_res_testbit_b :
              if (ads_alarme->une_variable1)
                {
                DeReferenceES (ads_alarme->pos_var1);
                }
              break;

            case c_res_entre_alarme :
            case c_res_pavhd :
            case c_res_pavbd :
              if (ads_alarme->une_variable1)
                {
                DeReferenceES (ads_alarme->pos_var1);
                }

              if (ads_alarme->une_variable2)
                {
                DeReferenceES (ads_alarme->pos_var2);
                }
              break;

            default :
              break;
            } // fin switch

          if (ads_alarme->pos_message != 0)
            {
            DeReferenceES (ads_alarme->pos_message);
            }

          enleve_message_alarme (ads_alarme->numero_enregistrement);
          enleve_enr (szVERIFSource, __LINE__, 1, val_geo.numero_repere, val_geo.pos_donnees);
          maj_pointeur_dans_geo (val_geo.numero_repere,
                                 val_geo.pos_donnees, TRUE);
          break; // fin b_alarme

        case b_titre_paragraf_al :
          un_paragraphe = TRUE;
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_al, val_geo.pos_paragraf);
          tipe_paragraf = titre_paragraf->IdParagraphe;
          profondeur = titre_paragraf->profondeur;
          if (!bSupprimeTitreParagraphe (b_titre_paragraf_al, val_geo.pos_paragraf))
            {
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            (*erreur_val) = IDSERR_SUPPRESSION_DU_PARAGRAPHE_IMPOSSIBLE_A_CETTE_POSITION;
            }
          else
            {
            maj_pointeur_dans_geo (b_titre_paragraf_al, val_geo.pos_paragraf, FALSE);
            if ((tipe_paragraf == surveillance_ala)  ||
	        (tipe_paragraf == visualisation_ala) ||
                (tipe_paragraf == archivage_ala)     ||
                (tipe_paragraf == impression_ala)    ||
                (tipe_paragraf == groupe_ala)        ||
                (tipe_paragraf == priorite_ala))
              {
              enleve_enr (szVERIFSource, __LINE__, 1, b_spec_titre_paragraf_al, val_geo.pos_specif_paragraf);
              maj_pointeur_dans_geo (val_geo.numero_repere, val_geo.pos_specif_paragraf, TRUE);
              }
            }
          break;

        default :
          break;
        } // switch (numero_repere

      if (*supprime_ligne)
        {
        MajDonneesParagraphe (b_titre_paragraf_al, ligne, SUPPRIME_LIGNE,
					un_paragraphe, profondeur, 0); //pos_paragraf);
        enleve_enr (szVERIFSource, __LINE__, 1, b_geo_al, ligne);
        }
      break;

    case CONSULTE_LIGNE :
       genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
       break;

    default :
      break;
    } // switch
  } // valide_ligne

// ---------------------------------------------------------------------------
static void  creer_al (void)
  {
  al_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
static void  init_inter_al (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  initialiser_al ();

  if (!al_deja_initialise)
    {
    ligne_depart_al = 1;
    ligne_courante_al = ligne_depart_al;
    al_deja_initialise = TRUE;
    }
  (*depart) = ligne_depart_al;
  (*courant) = ligne_courante_al;
  }

//----------------------------------------------------------------------------
static DWORD  lis_nombre_de_ligne_al (void)
  {
  DWORD wretour;

  if (existe_repere (b_geo_al))
    {
    wretour = nb_enregistrements (szVERIFSource, __LINE__, b_geo_al);
    }
  else
    {
    wretour = 0;
    }
  return (wretour);
  }

// ---------------------------------------------------------------------------
static void  set_tdb_al (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_res[c_nb_car_message_res];
  char chaine_mot [10];

  StrSetNull (tdb);
  StrSetNull (chaine_mot);
  if ((existe_repere (b_titre_paragraf_al)) && (nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf_al) != 0))
    {
   if (nouvelle_ligne)
      {
      ligne--;
      }
    consulte_titre_paragraf_al (ligne);
    if (!StrIsNull (contexte_al.nom_groupe))
      { // nom de groupe
      bMotReserve (c_res_groupe, mot_res);
      StrConcat (tdb, mot_res);
      StrConcatChar (tdb, ' ');
      StrConcat (tdb, contexte_al.nom_groupe);
      if (contexte_al.numero_priorite != 0)
        { // numero de priorite
        StrConcatChar (tdb, ' ');
        bMotReserve (c_res_priorite, mot_res);
        StrConcat (tdb, mot_res);
        StrConcatChar (tdb, ' ');
        StrDWordToStr (chaine_mot, contexte_al.numero_priorite);
        StrConcat (tdb, chaine_mot);
        }
      }
    } // existe paragraphe
  }

// ---------------------------------------------------------------------------
static void  fin_alarme (BOOL quitte, DWORD courant)
  {
  if (!quitte)
    {
    ligne_courante_al = courant;
    }
  fini_al ();
  }

// ---------------------------------------------------------------------------
// Interface du descripteur alarmes
void LisDescripteurAl (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_al;
	pDescripteurCf->pfnInitDesc = init_inter_al;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_al;
	pDescripteurCf->pfnValideLigne = valide_ligne_al;
	pDescripteurCf->pfnSetTdb = set_tdb_al;
	pDescripteurCf->pfnFin = fin_alarme;
	}

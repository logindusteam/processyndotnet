/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_jb.c                                               |
 |   Auteur  : AC					        	|
 |   Date    : 29/12/93					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include <stdlib.h>

#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "modbus.h"
#include "tipe_jb.h"
#include "cvt_jb.h"
#include "pcsdlgjb.h"
#include "IdLngLng.h"
#include "cvt_ul.h"
#include "inter_jb.h"
#include "Verif.h"
VerifInit;

//-------------------------- Variables ---------------------
#define COEFF_TIME_OUT_JB_MIN 0
#define COEFF_TIME_OUT_JB_MAX 130
#define COEFF_TIME_OUT_JB_DEFAUT 1
#define NB_RETRY_JB_MIN 0
#define NB_RETRY_JB_MAX 6
#define NB_RETRY_JB_DEFAUT 3

typedef struct
  {
  DWORD numero_canal;
  DWORD CanalVarOuCste;
  DWORD numero_esclave;
  DWORD EsclVarOuCste;
  char nom_esclave [c_nb_car_es];
  char nom_canal [c_nb_car_es];
  MODBUS_AUTOMATE sorte_auto;
  ID_MOT_RESERVE sens;
  ID_MOT_RESERVE genre;
  ID_MOT_RESERVE genre_num;
  } t_contexte_jb;

static t_contexte_jb contexte_jb;

static DWORD ligne_depart_jbus;
static DWORD ligne_courante_jbus;
static BOOL jbus_deja_initialise;




/*-------------------------------------------------------------------------*/
/*                        GESTION CONTEXTE                                 */
/*-------------------------------------------------------------------------*/

static void consulte_titre_paragraf_jb (DWORD ligne)
  {
  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_MODBUS	donnees_geo;
  t_spec_titre_paragraf_jb *spec_titre_paragraf;
  CONTEXTE_PARAGRAPHE_LIGNE table_contexte_ligne[3];
  DWORD profondeur_ligne;
  DWORD i;

  if (bDonneContexteParagrapheLigne (b_titre_paragraf_jb, ligne, &profondeur_ligne, table_contexte_ligne))
    { // donne_contexte_ok
    for (i = 0; (i < profondeur_ligne); i++)
      {
      switch (table_contexte_ligne [i].IdParagraphe)
        {
        case canal_jb:
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_jb, table_contexte_ligne [i].position);
          donnees_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, titre_paragraf->pos_geo_debut);
          spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, donnees_geo->pos_specif_paragraf);
          contexte_jb.CanalVarOuCste = spec_titre_paragraf->CanalVarOuCste;
          if (contexte_jb.CanalVarOuCste == ID_CHAMP_VAR)
            {
            NomVarES (contexte_jb.nom_canal , spec_titre_paragraf->numero_canal);
            }
          else
            {
	          contexte_jb.numero_canal = spec_titre_paragraf->numero_canal;
						}
          contexte_jb.sorte_auto = MODBUS_AUTOMATE_SMC;
          contexte_jb.numero_esclave = 0;
          contexte_jb.EsclVarOuCste = 0;
          contexte_jb.sens  = libre;
          contexte_jb.genre = libre;
          contexte_jb.genre_num = libre;
          break;
        case esclave_jb:
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_jb, table_contexte_ligne [i].position);
          donnees_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, titre_paragraf->pos_geo_debut);
          spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, donnees_geo->pos_specif_paragraf);
          contexte_jb.EsclVarOuCste = spec_titre_paragraf->EsclVarOuCste;
          if (contexte_jb.EsclVarOuCste == ID_CHAMP_VAR)
            {
            NomVarES (contexte_jb.nom_esclave , spec_titre_paragraf->numero_esclave);
            }
          else
            {
            contexte_jb.numero_esclave = spec_titre_paragraf->numero_esclave;
            }
          contexte_jb.sorte_auto = (MODBUS_AUTOMATE)spec_titre_paragraf->tipe;
          break;
        case entre_num :
          contexte_jb.sens = c_res_e;
          contexte_jb.genre = c_res_numerique;
          contexte_jb.genre_num = c_res_entier;
          break;
        case entre_num_mot :
          contexte_jb.sens = c_res_e;
          contexte_jb.genre = c_res_numerique;
          contexte_jb.genre_num = c_res_mots;
          break;
        case entre_num_reel :
          contexte_jb.sens  = c_res_e;
          contexte_jb.genre = c_res_numerique;
          contexte_jb.genre_num = c_res_reels;
          break;
        case entre_log:
          contexte_jb.sens  = c_res_e;
          contexte_jb.genre = c_res_logique;
          break;
        case entre_mes:
          contexte_jb.sens  = c_res_e;
          contexte_jb.genre = c_res_message;
          break;
        case sorti_num :
          contexte_jb.sens  = c_res_s;
          contexte_jb.genre = c_res_numerique;
          contexte_jb.genre_num = c_res_entier;
          break;
        case sorti_num_mot :
          contexte_jb.sens  = c_res_s;
          contexte_jb.genre = c_res_numerique;
          contexte_jb.genre_num = c_res_mots;
          break;
        case sorti_num_reel :
          contexte_jb.sens  = c_res_s;
          contexte_jb.genre = c_res_numerique;
          contexte_jb.genre_num = c_res_reels;
          break;
        case sorti_log:
          contexte_jb.sens  = c_res_s;
          contexte_jb.genre = c_res_logique;
          break;
        case sorti_mes:
          contexte_jb.sens  = c_res_s;
          contexte_jb.genre = c_res_message;
          break;
        default :
          contexte_jb.numero_canal = 0;
          contexte_jb.sorte_auto = MODBUS_AUTOMATE_SMC;
          contexte_jb.numero_esclave = 0;
          break;
        } // switch
      } // for i
    } // donne_contexte_ok
  else
    {
    contexte_jb.numero_canal = 0;
    contexte_jb.sorte_auto = MODBUS_AUTOMATE_SMC;
    contexte_jb.numero_esclave = 0;
    }
  }

/*--------------------------------------------------------------------------*/
/*                             DIVERS                                       */
/*--------------------------------------------------------------------------*/

static void maj_pointeur_dans_geo (DWORD numero_repere, DWORD limite,
                                   BOOL spec)
  {
  PGEO_MODBUS	donnees_geo;
  DWORD nbr_enr;
  DWORD i;

  if (!existe_repere (b_geo_jbus))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_jbus);
    }
  for (i = 1; (i <= nbr_enr); i++)
    {
    donnees_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, i);
    if (donnees_geo->numero_repere == numero_repere)
      {
      switch (numero_repere)
        {
        case b_jbus_e:
        case b_jbus_s:
        case b_jbus_e_mes:
        case b_jbus_s_mes:
        case b_jbus_e_tab:
        case b_jbus_s_tab:
          if (donnees_geo->pos_specif_es >= limite)
            {
            donnees_geo->pos_specif_es--;
            }
          break;

        case b_titre_paragraf_jb:
          if (spec)
            {
            if (donnees_geo->pos_specif_paragraf >= limite)
              {
              donnees_geo->pos_specif_paragraf--;
              }
            }
          else
            {
            if (donnees_geo->pos_paragraf >= limite)
              {
              donnees_geo->pos_paragraf--;
              }
            }
          break;

        default:
          break;
       
        } // switch
      }
    } // for
  }


/*--------------------------------------------------------------------------*/
void static traitement_num (PUL ul, DWORD num)
  {
  StrDWordToStr (ul->show, num);
  ul->erreur = 0;
  }

void static traitement_reel (PUL ul, FLOAT fReel)
  {
  StrFLOATToStr (ul->show, fReel);
  ul->erreur = 0;
  }

/*--------------------------------------------------------------------------*/
/*                             EDITION                                      */
/*--------------------------------------------------------------------------*/

//'''''''''''''''''''''''''''generation vecteur UL'''''''''''''''''''''''''''''

static void genere_vect_ul (DWORD ligne, PUL vect_ul,
                            int *nb_ul, int *niveau, int *indentation)
  {
  PGEO_MODBUS	donnees_geo;
  tc_jbus_e *jbus_e;
  tc_jbus_s *jbus_s;
  tc_jbus_tab *jbus_e_tab;
  tc_jbus_tab *jbus_s_tab;
  tc_jbus_e_mes *jbus_e_mes;
  tc_jbus_s_mes *jbus_s_mes;
  PTITRE_PARAGRAPHE titre_paragraf;
  t_spec_titre_paragraf_jb *spec_titre_paragraf;
  
  (*niveau) = 0;
  (*indentation) = 0;
  donnees_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, ligne);
  switch (donnees_geo->numero_repere)
    {
    case b_pt_constant:
      (*nb_ul) = 1;
      vect_ul[0].erreur = 0;
      vect_ul[0].show[0] = '\0';
      consulte_cst_bd_c (donnees_geo->pos_donnees, vect_ul[0].show);
      break;

    case b_titre_paragraf_jb :
     titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_jb, donnees_geo->pos_paragraf);
     switch (titre_paragraf->IdParagraphe)
       {
       case canal_jb:
         (*niveau) = 1;
         (*indentation) = 0;
         (*nb_ul) = 6;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_canal, vect_ul[0].show);
         spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, donnees_geo->pos_specif_paragraf);
         if (spec_titre_paragraf->CanalVarOuCste == ID_CHAMP_VAR)
           {
           NomVarES (vect_ul[1].show, spec_titre_paragraf->numero_canal);
           }
         else
           {
	         traitement_num  (&(vect_ul[1]), spec_titre_paragraf->numero_canal);
					 }
         traitement_num  (&(vect_ul[2]), spec_titre_paragraf->vitesse);
         traitement_num  (&(vect_ul[3]), spec_titre_paragraf->nb_de_bits);
         traitement_num  (&(vect_ul[4]), spec_titre_paragraf->parite);
         traitement_num  (&(vect_ul[5]), spec_titre_paragraf->encadrement);
         break;
         
       case esclave_jb:
         (*niveau) = 2;
         (*indentation) = 1;
         (*nb_ul) = 5;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_esclave, vect_ul[0].show);
         spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, donnees_geo->pos_specif_paragraf);
         if (spec_titre_paragraf->EsclVarOuCste == ID_CHAMP_VAR)
           {
           NomVarES (vect_ul[1].show, spec_titre_paragraf->numero_esclave);
           }
         else
           {
           traitement_num (&(vect_ul[1]), spec_titre_paragraf->numero_esclave);
           }
         if (spec_titre_paragraf->tipe > (NB_MAX_SORTE_AUTO - 1))
           {
           StrCopy (vect_ul[2].show, "?");
           vect_ul[2].erreur = 23;
           }
         else
           {
           StrCopy (vect_ul[2].show, sorte_auto[spec_titre_paragraf->tipe]);
           }

         if ((spec_titre_paragraf->fNbUnitesTimeOut < COEFF_TIME_OUT_JB_MIN) || (spec_titre_paragraf->fNbUnitesTimeOut > COEFF_TIME_OUT_JB_MAX))
           {
           spec_titre_paragraf->fNbUnitesTimeOut = COEFF_TIME_OUT_JB_DEFAUT;
           }
         traitement_reel (&(vect_ul[3]), spec_titre_paragraf->fNbUnitesTimeOut);

         if ((spec_titre_paragraf->retry < NB_RETRY_JB_MIN) || (spec_titre_paragraf->retry > NB_RETRY_JB_MAX))
           {
           spec_titre_paragraf->retry = NB_RETRY_JB_DEFAUT;
           }
         traitement_num (&(vect_ul[4]), spec_titre_paragraf->retry);
         break;

       case entre_num:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 3;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_e, vect_ul[0].show);
         bMotReserve (c_res_numerique, vect_ul[1].show);
         bMotReserve (c_res_entier, vect_ul[2].show);
         break;

       case entre_num_mot:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 3;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_e, vect_ul[0].show);
         bMotReserve (c_res_numerique, vect_ul[1].show);
         bMotReserve (c_res_mots, vect_ul[2].show);
         break;

       case entre_num_reel:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 3;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_e, vect_ul[0].show);
         bMotReserve (c_res_numerique, vect_ul[1].show);
         bMotReserve (c_res_reels, vect_ul[2].show);
         break;

       case entre_log:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 2;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_e, vect_ul[0].show);
         bMotReserve (c_res_logique, vect_ul[1].show);
         break;

       case entre_mes:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 2;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_e, vect_ul[0].show);
         bMotReserve (c_res_message, vect_ul[1].show);
         break;

       case sorti_num:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 3;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_s, vect_ul[0].show);
         bMotReserve (c_res_numerique, vect_ul[1].show);
         bMotReserve (c_res_entier, vect_ul[2].show);
         break;

       case sorti_num_mot:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 3;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_s, vect_ul[0].show);
         bMotReserve (c_res_numerique, vect_ul[1].show);
         bMotReserve (c_res_mots, vect_ul[2].show);
         break;

       case sorti_num_reel:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 3;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_s, vect_ul[0].show);
         bMotReserve (c_res_numerique, vect_ul[1].show);
         bMotReserve (c_res_reels, vect_ul[2].show);
         break;

       case sorti_log:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 2;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_s, vect_ul[0].show);
         bMotReserve (c_res_logique, vect_ul[1].show);
         break;

       case sorti_mes:
         (*niveau) = 3;
         (*indentation) = 2;
         (*nb_ul) = 2;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_s, vect_ul[0].show);
         bMotReserve (c_res_message, vect_ul[1].show);
         break;

       default:
         break;

       } // switch (titre_paragraf->tipe)
     break;

    case b_jbus_e:
      (*niveau) = 4;
      (*indentation) = 3;
      (*nb_ul) = 4;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      jbus_e = (tc_jbus_e *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e, donnees_geo->pos_specif_es);
      StrCopy (vect_ul[1].show, jbus_e->adresse_automate);
      NomVarES (vect_ul[2].show, jbus_e->i_cmd_rafraich);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[3].show);
      break;

    case b_jbus_e_tab :
      (*niveau) = 4;
      (*indentation) = 3;
      (*nb_ul) = 5;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      jbus_e_tab = (tc_jbus_tab *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_tab, donnees_geo->pos_specif_es);
      if (jbus_e_tab->LongVarOuCste == ID_CHAMP_CTE)
        {
        traitement_num(&(vect_ul[1]), jbus_e_tab->longueur);
        }
      else
        {
        NomVarES (vect_ul[1].show, jbus_e_tab->longueur);
        }
      if (jbus_e_tab->AdrVarOuCste == ID_CHAMP_CTE)
        {
        StrCopy (vect_ul[2].show, jbus_e_tab->adresse_automate);
        }
      else
        {
        NomVarES (vect_ul[2].show, jbus_e_tab->PosVarAdresse);
        }
      NomVarES (vect_ul[3].show, jbus_e_tab->i_cmd_rafraich);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[4].show);
      break;

    case b_jbus_e_mes:
      (*niveau) = 4;
      (*indentation) = 3;
      (*nb_ul) = 5;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      jbus_e_mes = (tc_jbus_e_mes *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_mes, donnees_geo->pos_specif_es);
      StrCopy (vect_ul[1].show, jbus_e_mes->adresse_automate);
      traitement_num (&(vect_ul[2]), jbus_e_mes->longueur);
      NomVarES (vect_ul[3].show, jbus_e_mes->i_cmd_rafraich);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[4].show);
      break;

    case b_jbus_s:
      (*niveau) = 5;
      (*indentation) = 3;
      (*nb_ul) = 3;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      jbus_s = (tc_jbus_s *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s, donnees_geo->pos_specif_es);
      StrCopy (vect_ul[1].show, jbus_s->adresse_automate);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[2].show);
      break;

    case b_jbus_s_tab :
      (*niveau) = 5;
      (*indentation) = 3;
      (*nb_ul) = 5;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      jbus_s_tab = (tc_jbus_tab *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_tab, donnees_geo->pos_specif_es);
      if (jbus_s_tab->LongVarOuCste == ID_CHAMP_CTE)
        {
        traitement_num(&(vect_ul[1]), jbus_s_tab->longueur);
        }
      else
        {
        NomVarES (vect_ul[1].show, jbus_s_tab->longueur);
        }
      if (jbus_s_tab->AdrVarOuCste == ID_CHAMP_CTE)
        {
        StrCopy (vect_ul[2].show, jbus_s_tab->adresse_automate);
        }
      else
        {
        NomVarES (vect_ul[2].show, jbus_s_tab->PosVarAdresse);
        }
      NomVarES (vect_ul[3].show, jbus_s_tab->i_cmd_rafraich);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[4].show);
      break;

    case b_jbus_s_mes:
      (*niveau) = 5;
      (*indentation) = 3;
      (*nb_ul) = 4;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      jbus_s_mes = (tc_jbus_s_mes *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_mes, donnees_geo->pos_specif_es);
      StrCopy (vect_ul[1].show, jbus_s_mes->adresse_automate);
      traitement_num (&(vect_ul[2]), jbus_s_mes->longueur);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[3].show);
      break;

    default:
      break;

    } // switch
  } // genere_vect_ul

//''''''''''''''''''''''''valide vecteur UL''''''''''''''''''''''''''''''''''
 
//-------------------------------
static BOOL valide_vect_ul (PUL vect_ul, int nb_ul, DWORD ligne,
                               REQUETE_EDIT action, DWORD *profondeur,
                               PID_PARAGRAPHE tipe_paragraf,
                               DWORD *numero_repere, DWORD *position_ident,
                               DWORD *pos_init, BOOL *exist_init,
                               tc_jbus_e *spec_e, tc_jbus_s *spec_s,
                               tc_jbus_tab *spec_e_tab, tc_jbus_tab *spec_s_tab,
                               tc_jbus_e_mes *spec_e_mes, tc_jbus_s_mes *spec_s_mes,
                               t_spec_titre_paragraf_jb *spec_titre_paragraf,
                               DWORD *erreur)

  {
  PENTREE_SORTIE es;
  DWORD sens_sur_auto;
  DWORD nbr_imbrication;
  ID_MOT_RESERVE n_fonction;
  ID_MOT_RESERVE n_fonction2;
  ID_MOT_RESERVE n_fonction3;
  DWORD pos_param_ident;
  DWORD mot;
  DWORD position_es;
  BOOL trouve;
  BOOL exist_ident;
  BOOL booleen;
  BOOL un_log;
  BOOL un_num;
  FLOAT reel;
  t_contexte_jb old_contexte;
  PGEO_MODBUS	val_geo;
  DWORD w;
  int pos_va_init;


  //***********************************************
  #define err(code) (*erreur) = code;return (FALSE)  //Sale Sioux
  //***********************************************

 
  old_contexte = contexte_jb;
  (*erreur) = 0;
  if (nb_ul < 1)
    {
    err(IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
    }
  if (nb_ul > 6)
    {
    err(31);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    if (!bReconnaitMotReserve ((vect_ul+0)->show, &n_fonction))
      {
      if (!bReconnaitConstante ((vect_ul+0), &booleen, &reel,
                            &un_log, &un_num, exist_init, pos_init))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if ((un_log) || (un_num))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      (*numero_repere) = b_pt_constant;
      }
    else
      {
      switch (n_fonction)
        {
        case c_res_e:
        case c_res_s:
          if (!bReconnaitMotReserve ((vect_ul+1)->show, &n_fonction2))
            {
            err(32);
            }
          (*profondeur) = 3;
          nbr_imbrication = 2;
          switch (n_fonction2)
            {
            case c_res_numerique :
              if (nb_ul == 2)
                {
                n_fonction3 = c_res_entier;
                }
              else
                {
                if (nb_ul != 3)
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                if (!bReconnaitMotReserve ((vect_ul+2)->show, &n_fonction3))
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                }
              contexte_jb.genre = c_res_numerique;
              if (n_fonction == c_res_e)
                {
                switch (n_fonction3)
                  {
                  case c_res_entier :
                    (*tipe_paragraf) = entre_num;
                    contexte_jb.genre_num = c_res_entier;
                    break;
                  case c_res_mots :
                    (*tipe_paragraf) = entre_num_mot;
                    contexte_jb.genre_num = c_res_mots;
                    break;
                  case c_res_reels :
                    (*tipe_paragraf) = entre_num_reel;
                    contexte_jb.genre_num = c_res_reels;
                    break;
                  default:
                    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                    break;
                  }
                }
              else
                {
                switch (n_fonction3)
                  {
                  case c_res_entier:
                    (*tipe_paragraf) = sorti_num;
                    contexte_jb.genre_num = c_res_entier;
                    break;
                  case c_res_mots:
                    (*tipe_paragraf) = sorti_num_mot;
                    contexte_jb.genre_num = c_res_mots;
                    break;
                  case c_res_reels:
                    (*tipe_paragraf) = sorti_num_reel;
                    contexte_jb.genre_num = c_res_reels;
                    break;
                  default:
                    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                    break;
                  }
                }
              break;

            case c_res_logique :
              contexte_jb.genre = c_res_logique;
              if (n_fonction == c_res_e)
                {
                (*tipe_paragraf) = entre_log;
                }
              else
                {
                (*tipe_paragraf) = sorti_log;
                }
              break;

            case c_res_message :
              contexte_jb.genre = c_res_message;
              if (n_fonction == c_res_e)
                {
                (*tipe_paragraf) = entre_mes;
                }
              else
                {
                (*tipe_paragraf) = sorti_mes;
                }
              break;

            default:
              err (32);
              break;
            } // switch n_fonction2
          contexte_jb.sens = n_fonction;
          (*numero_repere) = b_titre_paragraf_jb;
          break;
    
        case c_res_canal:
          (*profondeur) = 1;
          nbr_imbrication = 0;
          if (bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
						{
						if ((StrLength (vect_ul[1].show) > 2) || (StrLength (vect_ul[1].show) < 1) || (reel > 36) || (reel < 0))
							{
							err(61);
							}
						else
							{
		          contexte_jb.numero_canal = (DWORD)(reel);
              spec_titre_paragraf->CanalVarOuCste = ID_CHAMP_CTE;
		          spec_titre_paragraf->numero_canal = contexte_jb.numero_canal;
							}
						}
					else
						{
            if ((bReconnaitESValide ((vect_ul+1), &exist_ident,&pos_param_ident))&&(exist_ident))
              {
              position_es = dwIdVarESFromPosIdentif (pos_param_ident);
              es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
              if ((es->genre != c_res_numerique))
                {
                err(61);
                }
              contexte_jb.numero_canal = 0;
              spec_titre_paragraf->numero_canal = (DWORD)position_es;//$$???
              spec_titre_paragraf->CanalVarOuCste = ID_CHAMP_VAR;
              }
            else
              {
              err(17);
              }
						}
          if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
            {
            err(69);
            }
          spec_titre_paragraf->vitesse = (DWORD)(reel);
          if ((!bReconnaitConstanteNum ((vect_ul+3), &booleen, &reel, &mot)) ||
              (StrLength (vect_ul[3].show) != 1) ||
              ((vect_ul[3].show[0] != '8') &&
               (vect_ul[3].show[0] != '7')))
            {
            err(70);
            }
          spec_titre_paragraf->nb_de_bits = (DWORD)(reel);
          if ((!bReconnaitConstanteNum ((vect_ul+4), &booleen, &reel, &mot)) ||
              (StrLength (vect_ul[4].show) != 1) ||
              (( vect_ul[4].show[0] != '0') &&
               (vect_ul[4].show[0] != '1') &&
               (vect_ul[4].show[0] != '2')))
            {
            err(71);
            }
          spec_titre_paragraf->parite = (DWORD)(reel);
          if ((!bReconnaitConstanteNum ((vect_ul+5), &booleen, &reel, &mot)) ||
              (StrLength (vect_ul[5].show) != 1) ||
              ((vect_ul[5].show[0] != '0') &&
               (vect_ul[5].show[0] != '1') &&
               (vect_ul[5].show[0] != '2')))
            {
            err(72);
            }
          spec_titre_paragraf->encadrement = (DWORD)(reel);
          (*numero_repere) = b_titre_paragraf_jb;
          (*tipe_paragraf) = canal_jb;
          break;
 
        case c_res_esclave:
          if ((nb_ul != 3) && (nb_ul != 5))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
            {
            if ((bReconnaitESValide ((vect_ul+1), &exist_ident,&pos_param_ident))&&(exist_ident))
              {
              position_es = dwIdVarESFromPosIdentif (pos_param_ident);
              es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
              if ((es->genre != c_res_numerique))
                {
                err(17);
                }
              contexte_jb.numero_esclave = 0;
              spec_titre_paragraf->numero_esclave = (DWORD)position_es;//$$???
              spec_titre_paragraf->EsclVarOuCste = ID_CHAMP_VAR;
              }
            else
              {
              err(17);
              }
            }
          else
            {
            if ((reel < 0) || (reel > 255))
              {
              err(17);
              }
            contexte_jb.numero_esclave = (DWORD)(reel);
            spec_titre_paragraf->numero_esclave = contexte_jb.numero_esclave;
            spec_titre_paragraf->EsclVarOuCste = ID_CHAMP_CTE;
            }
          trouve = FALSE;
          for (w = 0; ( (w < NB_MAX_SORTE_AUTO) && (!trouve)); w++)
            {
            trouve = bStrEgales (vect_ul[2].show, sorte_auto [w]);
            }
          if (!trouve)
            {
            err(62);
            }
          else
            {
            w--;
            }
          contexte_jb.sorte_auto = (MODBUS_AUTOMATE)w;
          spec_titre_paragraf->tipe = contexte_jb.sorte_auto;
          if (nb_ul == 5)
            {
            if (!bReconnaitConstanteNum ((vect_ul+3), &booleen, &reel, &mot))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            if ((reel < COEFF_TIME_OUT_JB_MIN) || (reel > COEFF_TIME_OUT_JB_MAX))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            spec_titre_paragraf->fNbUnitesTimeOut = reel;
            if (!bReconnaitConstanteNum ((vect_ul+4), &booleen, &reel, &mot))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            if ((reel < NB_RETRY_JB_MIN) || (reel > NB_RETRY_JB_MAX))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            spec_titre_paragraf->retry = (DWORD)(reel);
            }
          else
            {
            spec_titre_paragraf->fNbUnitesTimeOut = COEFF_TIME_OUT_JB_DEFAUT;  // coef multiplicatif
            spec_titre_paragraf->retry = NB_RETRY_JB_DEFAUT;
            }
          (*profondeur) = 2;
          nbr_imbrication = 1;
          (*numero_repere) = b_titre_paragraf_jb;
          (*tipe_paragraf) = esclave_jb;
          break;

        default:
          err(32);
          break;
        } // switch n_fonction
      if (!bValideInsereLigneParagraphe (b_titre_paragraf_jb, ligne,
                                           (*profondeur), nbr_imbrication,
                                           erreur))
        {
        contexte_jb = old_contexte;
        err((*erreur));
        }
      } // mot res
    }// non identificateur
  else
    {
    if (action != MODIFIE_LIGNE)
      {
      if (exist_ident)
        {
        err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    else
      { // modification
      val_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, ligne);
      if ((val_geo->numero_repere == b_jbus_e) ||
          (val_geo->numero_repere == b_jbus_e_mes) ||
          (val_geo->numero_repere == b_jbus_s) ||
          (val_geo->numero_repere == b_jbus_s_mes) ||
          (val_geo->numero_repere == b_jbus_e_tab) ||
          (val_geo->numero_repere == b_jbus_s_tab))
        {
        if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
          {
          err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
          }
        } // deja description donnees
      else
        {
        if (exist_ident)
          {
          err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
          }
        }
      }
    if (nb_ul < 3)
      {
      err(IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
      }

    switch (contexte_jb.sens)
      {
      case c_res_e :
        if ((nb_ul != 4) && (nb_ul != 5))
          {
          err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if ((nb_ul == 4) || ((nb_ul == 5) && (contexte_jb.genre == c_res_message)))
          { // pas tableau
          if ((vect_ul[1].genre == g_bizarre) || (StrLength (vect_ul[1].show) > 6))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          (*erreur) = dwConversionAdresseModbus (contexte_jb.sorte_auto,
                                                contexte_jb.sens, contexte_jb.genre,
                                                vect_ul[1].show, &w, &sens_sur_auto);
          if ((*erreur) != 0)
            {
            err((*erreur));
            }
          if (contexte_jb.genre == c_res_message)
            {     // message
            StrCopy (spec_e_mes->adresse_automate, vect_ul[1].show);
            if (!bReconnaitConstante ((vect_ul+2), &booleen, &reel,
                                   &un_log, &un_num, exist_init,pos_init))
              {
              err(271);
              }
            if ((!un_num) || (reel < 1) || (reel > 80))
              {
              err(271);
              }
            spec_e_mes->longueur = (DWORD)(reel);
            pos_va_init = 4;
            (*numero_repere) = b_jbus_e_mes;
            }
          else      // pas message pas tableau
            {
            StrCopy (spec_e->adresse_automate, vect_ul[1].show);
            pos_va_init = 3;
            (*numero_repere) = b_jbus_e;
            }
          }      // tableau
        else
          {
          if ((bReconnaitESValide ((vect_ul+2), &exist_ident,&pos_param_ident))&&(exist_ident))
            {
            position_es = dwIdVarESFromPosIdentif (pos_param_ident);
            es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
            if ((es->genre != c_res_message))
              {
              err(28);
              }
            spec_e_tab->PosVarAdresse = position_es;
            spec_e_tab->AdrVarOuCste = ID_CHAMP_VAR;
            }
          else
            {
            if ((vect_ul[2].genre == g_bizarre) || (StrLength (vect_ul[2].show) > 6))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            (*erreur) = dwConversionAdresseModbus (contexte_jb.sorte_auto,
                                                  contexte_jb.sens, contexte_jb.genre,
                                                  vect_ul[2].show, &w, &sens_sur_auto);
            if ((*erreur) != 0)
              {
              err((*erreur));
              }
            spec_e_tab->AdrVarOuCste = ID_CHAMP_CTE;
            StrCopy (spec_e_tab->adresse_automate, vect_ul[2].show);
            }
          if ((bReconnaitESValide ((vect_ul+1), &exist_ident, &pos_param_ident))&&(exist_ident))
            {
            position_es = dwIdVarESFromPosIdentif ((pos_param_ident));
            es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
            if ((es->genre != c_res_numerique))
              {
              err(28);
              }
            spec_e_tab->longueur = position_es;
            spec_e_tab->LongVarOuCste = ID_CHAMP_VAR;
            }
          else
            {
            if (!bReconnaitConstanteNum((vect_ul+1), &booleen, &reel, &mot))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            if ( ((reel<1) || (reel > NB_MAX_ELEMENTS_VAR_TABLEAU)) && (contexte_jb.genre == c_res_logique))
              {
              err(IDSERR_LA_TAILLE_DU_TABLEAU_EST_COMPRISE_ENTRE_1_ET_10000);
              }
            if (contexte_jb.genre == c_res_numerique)
              {
              switch (contexte_jb.genre_num)
                {
                case c_res_entier:
                case c_res_mots :
                  if ((reel<1) || (reel > 128))
                    {
                    err(323);
                    }
                  break;
                case c_res_reels :
                  if ((reel<1) || (reel > 64))
                    {
                    err(325);
                    }
                  break;
                default:
                  break;
                }
              }
            spec_e_tab->longueur = (DWORD)(reel);
            spec_e_tab->LongVarOuCste = ID_CHAMP_CTE;
            }

          pos_va_init = 4;
          (*numero_repere) = b_jbus_e_tab;
          }
        if (bReconnaitESExistante ((vect_ul+pos_va_init-1), &position_es))
          {
          es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
          if (es->genre != c_res_logique)
            {
            err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE);
            }
          if (contexte_jb.genre == c_res_message)
            {
            spec_e_mes->i_cmd_rafraich = position_es;
            }
          else
            {
            if (nb_ul == 4)
              {     // pas tableau
              spec_e->i_cmd_rafraich = position_es;
              }
            else      // tableau
              {
              spec_e_tab->i_cmd_rafraich = position_es;
              }
            }
          }
        else
          {
          err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE);
          }
        if (!bReconnaitConstante ((vect_ul+pos_va_init), &booleen, &reel,
                              &un_log, &un_num, exist_init, pos_init))
          {
          err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if ((contexte_jb.genre == c_res_logique) && (!un_log))
          {
          err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
          }
        if ((contexte_jb.genre == c_res_numerique) && (!un_num))
          {
          err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
          }
        if ((contexte_jb.genre == c_res_message) && ((un_num) || (un_log)))
          {
          err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
          }
        break;

      case c_res_s :
        if ((nb_ul != 3) && (nb_ul != 4) && (nb_ul != 5))
          {
          err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (nb_ul != 5)
          {             // pas tableau
          if ((vect_ul[1].genre == g_bizarre) || (StrLength (vect_ul[1].show) > 6))
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          (*erreur) = dwConversionAdresseModbus (contexte_jb.sorte_auto,
                                                contexte_jb.sens, contexte_jb.genre,
                                                vect_ul[1].show, &w, &sens_sur_auto);
          if ((*erreur) != 0)
            {
            err((*erreur));
            }
          if (contexte_jb.genre == c_res_message)
            {     // message
            StrCopy (spec_s_mes->adresse_automate, vect_ul[1].show);
            if (!bReconnaitConstante ((vect_ul+2), &booleen, &reel,
                                  &un_log, &un_num, exist_init, pos_init))
              {
              err(271);
              }
            if ((!un_num) || (reel < 1) || (reel > 80))
              {
              err(271);
              }
            spec_s_mes->longueur = (DWORD)(reel);

            pos_va_init = 3;
            (*numero_repere) = b_jbus_s_mes;
            }
          else      // pas message pas tableau
            {
            StrCopy (spec_s->adresse_automate, vect_ul[1].show);
            pos_va_init = 2;
            (*numero_repere) = b_jbus_s;
            }
          }      // tableau
        else
          {
          if ((bReconnaitESValide ((vect_ul+2), &exist_ident,&pos_param_ident))&&(exist_ident))
            {
            position_es = dwIdVarESFromPosIdentif (pos_param_ident);
            es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
            if ((es->genre != c_res_message))
              {
              err(28);
              }
            spec_s_tab->PosVarAdresse = position_es;
            spec_s_tab->AdrVarOuCste = ID_CHAMP_VAR;
            }
          else
            {
            if ((vect_ul[2].genre == g_bizarre) || (StrLength (vect_ul[2].show) > 6))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            (*erreur) = dwConversionAdresseModbus (contexte_jb.sorte_auto,
                                                  contexte_jb.sens, contexte_jb.genre,
                                                  vect_ul[2].show, &w, &sens_sur_auto);
            if ((*erreur) != 0)
              {
              err((*erreur));
              }
            spec_s_tab->AdrVarOuCste = ID_CHAMP_CTE;
            StrCopy (spec_s_tab->adresse_automate, vect_ul[2].show);
            }
          if ((bReconnaitESValide ((vect_ul+1), &exist_ident,&pos_param_ident))&&(exist_ident))
            {
            position_es = dwIdVarESFromPosIdentif (pos_param_ident);
            es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
            if ((es->genre != c_res_numerique))
              {
              err(28);
              }
            spec_s_tab->longueur = position_es;
            spec_s_tab->LongVarOuCste = ID_CHAMP_VAR;
            }
          else
            {
            if (!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            if (((reel<1) || (reel > NB_MAX_ELEMENTS_VAR_TABLEAU)) && (contexte_jb.genre == c_res_logique))
              {
              err(IDSERR_LA_TAILLE_DU_TABLEAU_EST_COMPRISE_ENTRE_1_ET_10000);
              }
            if (contexte_jb.genre == c_res_numerique)
              {
              switch (contexte_jb.genre_num)
                {
                case c_res_entier:
                case c_res_mots :
                  if ((reel<1) || (reel > 128))
                    {
                    err(323);
                    }
                  break;
                case c_res_reels :
                  if ((reel<1) || (reel > 64))
                    {
                    err(334);
                    }
                  break;
                default:
                  break;
                }
              }
            spec_s_tab->longueur = (DWORD)(reel);
            spec_s_tab->LongVarOuCste = ID_CHAMP_CTE;
            }
          pos_va_init = 4;
          (*numero_repere) = b_jbus_s_tab;
          if (bReconnaitESExistante ((vect_ul+pos_va_init-1), &position_es))
            {
            es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
            if (es->genre != c_res_logique)
              {
              err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE);
              }
            spec_s_tab->i_cmd_rafraich = position_es;
            }
          else
            {
            err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE);
            }
          }
        if (!bReconnaitConstante ((vect_ul+pos_va_init), &booleen, &reel,
                              &un_log, &un_num, exist_init, pos_init))
          {
          err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if ((contexte_jb.genre == c_res_logique) && (!un_log))
          {
          err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
          }
        if ((contexte_jb.genre == c_res_numerique) && (!un_num))
          {
          err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
          }
        if ((contexte_jb.genre == c_res_message) && ((un_num) || (un_log)))
          {
          err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
          }
        break;

      default :
        err (IDSERR_LE_SENS_EST_INVALIDE);
        break;

      } // switch
    }
  return ((BOOL)((*erreur) == 0));
  } // valide_vect_ul

//---------------------------------------------------------------
static void  valide_ligne_jbus 
	(REQUETE_EDIT action, DWORD ligne, PUL vect_ul, int *nb_ul,
	BOOL *supprime_ligne, BOOL *accepte_ds_bd, DWORD *erreur_val, int *niveau, int *indentation)
  {
  DWORD erreur;
  DWORD pos_init;
  BOOL un_paragraphe;
  BOOL exist_init;
  tc_jbus_e spec_e;
  tc_jbus_e *ptr_spec_e;
  tc_jbus_tab spec_e_tab;
  tc_jbus_tab *ptr_spec_e_tab;
  tc_jbus_e_mes spec_e_mes;
  tc_jbus_e_mes *ptr_spec_e_mes;
  tc_jbus_s spec_s;
  tc_jbus_s *ptr_spec_s;
  tc_jbus_tab spec_s_tab;
  tc_jbus_tab *ptr_spec_s_tab;
  tc_jbus_s_mes spec_s_mes;
  tc_jbus_s_mes *ptr_spec_s_mes;
  GEO_MODBUS val_geo;
  PGEO_MODBUS	donnees_geo;
  ENTREE_SORTIE es;
  DWORD numero_repere;
  DWORD pos_paragraf;
  DWORD profondeur;
  ID_PARAGRAPHE tipe_paragraf;
  PTITRE_PARAGRAPHE titre_paragraf;
  t_spec_titre_paragraf_jb spec_titre_paragraf;
  t_spec_titre_paragraf_jb *ptr_spec_titre_paragraf;

  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;
  switch (action)
    {
    case MODIFIE_LIGNE:
      consulte_titre_paragraf_jb (ligne);
      if (valide_vect_ul(vect_ul, (*nb_ul), ligne, MODIFIE_LIGNE,
                         &profondeur, &tipe_paragraf, &numero_repere,
                         &(es.i_identif), &pos_init, &exist_init,
                         &spec_e, &spec_s,
                         &spec_e_tab, &spec_s_tab,
                         &spec_e_mes, &spec_s_mes,
                         &spec_titre_paragraf, &erreur))

        {
        donnees_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, ligne);
        val_geo = (*donnees_geo);
        if (val_geo.numero_repere == numero_repere)
          {
          switch (numero_repere)
            {
            case b_jbus_e :
              es.genre = contexte_jb.genre;
              es.sens = contexte_jb.sens;
					    es.taille = 0;
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[3].show, &(val_geo.pos_initial));
                ptr_spec_e = (tc_jbus_e *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e, val_geo.pos_specif_es);
                if (ptr_spec_e->i_cmd_rafraich != spec_e.i_cmd_rafraich)
                  {
                  DeReferenceES (ptr_spec_e->i_cmd_rafraich);
                  ReferenceES (spec_e.i_cmd_rafraich);
                  }
                (*ptr_spec_e) = spec_e;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_jbus_e_tab :
              es.genre = contexte_jb.genre;
              es.sens = c_res_variable_ta_e;
              if (spec_e_tab.LongVarOuCste == ID_CHAMP_VAR)
                {
                if (contexte_jb.genre == c_res_numerique)
                  {
                  switch (contexte_jb.genre_num)
                    {
                    case c_res_entier:
                    case c_res_mots :
  	                  es.taille = 128;
                      break;
                    case c_res_reels :
                    es.taille = 64;
                      break;
                    default:
                      break;
                    }
                  }
                else
                  {
		              es.taille = 1024;
                  }
                }
              else
                {
                es.taille = spec_e_tab.longueur;
                }
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[4].show, &(val_geo.pos_initial));
                ptr_spec_e_tab = (tc_jbus_tab *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_tab, val_geo.pos_specif_es);
                if (ptr_spec_e_tab->i_cmd_rafraich != spec_e_tab.i_cmd_rafraich)
                  {
                  DeReferenceES (ptr_spec_e_tab->i_cmd_rafraich);
                  ReferenceES (spec_e_tab.i_cmd_rafraich);
                  }
                if (ptr_spec_e_tab->AdrVarOuCste == ID_CHAMP_VAR)
                  {
                  if (ptr_spec_e_tab->PosVarAdresse != spec_e_tab.PosVarAdresse)
                    {
                    DeReferenceES (ptr_spec_e_tab->PosVarAdresse);
                    ReferenceES (spec_e_tab.PosVarAdresse);
                    }
                  }
                if (ptr_spec_e_tab->LongVarOuCste == ID_CHAMP_VAR)
                  {
                  if (ptr_spec_e_tab->longueur != spec_e_tab.longueur)
                    {
                    DeReferenceES (ptr_spec_e_tab->longueur);
                    ReferenceES (spec_e_tab.longueur);


                    }
                  }
                (*ptr_spec_e_tab) = spec_e_tab;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_jbus_e_mes :
              es.genre = contexte_jb.genre;
              es.sens = contexte_jb.sens;
					    es.taille = 0;
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[4].show, &(val_geo.pos_initial));
                ptr_spec_e_mes = (tc_jbus_e_mes *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_mes, val_geo.pos_specif_es);
                if (ptr_spec_e_mes->i_cmd_rafraich != spec_e_mes.i_cmd_rafraich)
                  {
                  DeReferenceES (ptr_spec_e_mes->i_cmd_rafraich);
                  ReferenceES (spec_e_mes.i_cmd_rafraich);
                  }
                (*ptr_spec_e_mes) = spec_e_mes;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_jbus_s :
              es.genre = contexte_jb.genre;
              es.sens = contexte_jb.sens;
					    es.taille = 0;
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[2].show, &(val_geo.pos_initial));
                ptr_spec_s = (tc_jbus_s *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s,val_geo.pos_specif_es);
                (*ptr_spec_s) = spec_s;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_jbus_s_tab :
              es.genre = contexte_jb.genre;
              es.sens = c_res_variable_ta_s;
              if (spec_s_tab.LongVarOuCste == ID_CHAMP_VAR)
                {
                if (contexte_jb.genre == c_res_numerique)
                  {
                  switch (contexte_jb.genre_num)
                    {
                    case c_res_entier:
                    case c_res_mots :
  	                  es.taille = 128;
                      break;
                    case c_res_reels :
                    es.taille = 64;
                      break;
                    default:
                      break;
                    }
                  }
                else
                  {
		              es.taille = 1024;
                  }
                }
              else
                {
                es.taille = spec_s_tab.longueur;
                }
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[4].show, &(val_geo.pos_initial));
                ptr_spec_s_tab = (tc_jbus_tab *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_tab, val_geo.pos_specif_es);
                if (ptr_spec_s_tab->i_cmd_rafraich != spec_s_tab.i_cmd_rafraich)
                  {
                  DeReferenceES (ptr_spec_s_tab->i_cmd_rafraich);
                  ReferenceES (spec_s_tab.i_cmd_rafraich);
                  }
                if (ptr_spec_s_tab->AdrVarOuCste == ID_CHAMP_VAR)
                  {
                  if (ptr_spec_s_tab->PosVarAdresse != spec_s_tab.PosVarAdresse)
                    {
                    DeReferenceES (ptr_spec_s_tab->PosVarAdresse);
                    ReferenceES (spec_s_tab.PosVarAdresse);
                    }
                  }
                if (ptr_spec_s_tab->LongVarOuCste == ID_CHAMP_VAR)
                  {
                  if (ptr_spec_s_tab->longueur != spec_s_tab.longueur)
                    {
                    DeReferenceES (ptr_spec_s_tab->longueur);
                    ReferenceES (spec_s_tab.longueur);
                    }
                  }
                (*ptr_spec_s_tab) = spec_s_tab;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_jbus_s_mes :
              es.genre = contexte_jb.genre;
              es.sens = contexte_jb.sens;
					    es.taille = 0;
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[3].show, &(val_geo.pos_initial));
                ptr_spec_s_mes = (tc_jbus_s_mes *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_mes, val_geo.pos_specif_es);
                (*ptr_spec_s_mes) = spec_s_mes;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_pt_constant :
              modifie_cst_bd_c (vect_ul[0].show, &(val_geo.pos_donnees));
              break;

            case b_titre_paragraf_jb:
              titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_jb, val_geo.pos_paragraf);
              if (titre_paragraf->IdParagraphe == tipe_paragraf)
                {
                if (tipe_paragraf == esclave_jb)
                  {
                  ptr_spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
                  if (ptr_spec_titre_paragraf->EsclVarOuCste == ID_CHAMP_VAR)
                    {
                    DeReferenceES (ptr_spec_titre_paragraf->numero_esclave);
										}
                  if (spec_titre_paragraf.EsclVarOuCste == ID_CHAMP_VAR)
                    {
                    ReferenceES (spec_titre_paragraf.numero_esclave);
                    }
									(*ptr_spec_titre_paragraf) = spec_titre_paragraf;
                  }
                if (tipe_paragraf == canal_jb)
                  {
                  ptr_spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
                  if (ptr_spec_titre_paragraf->CanalVarOuCste == ID_CHAMP_VAR)
                    {
                    DeReferenceES (ptr_spec_titre_paragraf->numero_canal);
										}
                  if (spec_titre_paragraf.CanalVarOuCste == ID_CHAMP_VAR)
                    {
                    ReferenceES (spec_titre_paragraf.numero_canal);
										}
									(*ptr_spec_titre_paragraf) = spec_titre_paragraf;
                  }
                } // if (titre_paragraf->tipe == tipe_paragraf)
              else
                {
                erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
                }
              break;

            default:
              break;

            } // switch numero repere
          }// meme repere
        else
          {
          erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
          }
        } // valide vect_ul Ok
      if (erreur != 0)
        {
        genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
        (*erreur_val) = erreur;
        }
      else
        {
        donnees_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, ligne);
        (*donnees_geo) = val_geo;
        }
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne)=FALSE;
      break;

    case INSERE_LIGNE:
      consulte_titre_paragraf_jb (ligne - 1);
      if (valide_vect_ul(vect_ul, (*nb_ul), ligne, INSERE_LIGNE,
                         &profondeur, &tipe_paragraf, &numero_repere,
                         &(es.i_identif), &pos_init, &exist_init,
                         &spec_e, &spec_s,
                         &spec_e_tab, &spec_s_tab,
                         &spec_e_mes, &spec_s_mes,
                         &spec_titre_paragraf, &erreur))
        {
        pos_paragraf = 0; // init pour mise a jour donnees paragraf
        val_geo.numero_repere = numero_repere;
        switch (numero_repere)
          {
          case b_pt_constant :
            AjouteConstanteBd (pos_init, exist_init, vect_ul[0].show, &(val_geo.pos_donnees));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_jbus_e:
            if (!existe_repere (b_jbus_e))
              {
              cree_bloc (b_jbus_e, sizeof (spec_e), 0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_jbus_e) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_jbus_e, val_geo.pos_specif_es);
            ptr_spec_e = (tc_jbus_e *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e, val_geo.pos_specif_es);
            (*ptr_spec_e) = spec_e;
            ReferenceES (spec_e.i_cmd_rafraich);
            es.genre = contexte_jb.genre;
            es.sens = contexte_jb.sens;
            es.n_desc = d_jbus;
				    es.taille = 0;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.pos_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[3].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_jbus_e_tab:
            if (!existe_repere (b_jbus_e_tab))
              {
              cree_bloc (b_jbus_e_tab, sizeof (spec_e_tab), 0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_jbus_e_tab) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_jbus_e_tab, val_geo.pos_specif_es);
            ptr_spec_e_tab = (tc_jbus_tab *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_tab, val_geo.pos_specif_es);
            (*ptr_spec_e_tab) = spec_e_tab;
            if (ptr_spec_e_tab->AdrVarOuCste == ID_CHAMP_VAR)
              {
              ReferenceES (spec_e_tab.PosVarAdresse);
              }
            if (ptr_spec_e_tab->LongVarOuCste == ID_CHAMP_VAR)
              {
              ReferenceES (spec_e_tab.longueur);
              if (contexte_jb.genre == c_res_numerique)
                {
                switch (contexte_jb.genre_num)
                  {
                  case c_res_entier:
                  case c_res_mots :
	    	            es.taille = 128;
                    break;
                  case c_res_reels :
				            es.taille = 64;
                    break;
                  default:
                    break;
                  }
                }
              else
                {
  				      es.taille = 1024;
                }
              }
            else
              {
  						es.taille = spec_e_tab.longueur;
              }
            ReferenceES (spec_e_tab.i_cmd_rafraich);
            es.genre = contexte_jb.genre;
            es.sens = c_res_variable_ta_e;
            es.n_desc = d_jbus;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.pos_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[4].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_jbus_e_mes:
            if (!existe_repere (b_jbus_e_mes))
              {
              cree_bloc (b_jbus_e_mes, sizeof (spec_e_mes), 0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_jbus_e_mes) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_jbus_e_mes, val_geo.pos_specif_es);
            ptr_spec_e_mes = (tc_jbus_e_mes *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_mes, val_geo.pos_specif_es);
            (*ptr_spec_e_mes) = spec_e_mes;
            ReferenceES (spec_e_mes.i_cmd_rafraich);
            es.genre = contexte_jb.genre;
            es.sens = contexte_jb.sens;
            es.n_desc = d_jbus;
						es.taille = 0;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.pos_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[4].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_jbus_s:
            if (!existe_repere (b_jbus_s))
              {
              cree_bloc (b_jbus_s, sizeof (spec_s), 0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_jbus_s) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_jbus_s, val_geo.pos_specif_es);
            ptr_spec_s = (tc_jbus_s *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s, val_geo.pos_specif_es);
            (*ptr_spec_s) = spec_s;
            es.genre = contexte_jb.genre;
            es.sens = contexte_jb.sens;
            es.n_desc = d_jbus;
						es.taille = 0;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.pos_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[2].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_jbus_s_tab:
            if (!existe_repere (b_jbus_s_tab))
              {
              cree_bloc (b_jbus_s_tab, sizeof (spec_s_tab), 0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_jbus_s_tab) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_jbus_s_tab, val_geo.pos_specif_es);
            ptr_spec_s_tab = (tc_jbus_tab *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_tab, val_geo.pos_specif_es);
            (*ptr_spec_s_tab) = spec_s_tab;
            if (ptr_spec_s_tab->AdrVarOuCste == ID_CHAMP_VAR)
              {
              ReferenceES (spec_s_tab.PosVarAdresse);
              }
            if (ptr_spec_s_tab->LongVarOuCste == ID_CHAMP_VAR)
              {
              ReferenceES (spec_s_tab.longueur);
              if (contexte_jb.genre == c_res_numerique)
                {
                switch (contexte_jb.genre_num)
                  {
                  case c_res_entier:
                  case c_res_mots :
    					      es.taille = 128;
                    break;
                  case c_res_reels :
  								  es.taille = 64;
                    break;
                  default:
                    break;
                  }
                }
              else
                {
		  	        es.taille = 1024;
                }
              }
            else
              {
  			      es.taille = spec_s_tab.longueur;
              }
            ReferenceES (spec_s_tab.i_cmd_rafraich);
            es.genre = contexte_jb.genre;
            es.sens = c_res_variable_ta_s;
            es.n_desc = d_jbus;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.pos_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[4].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_jbus_s_mes:
            if (!existe_repere (b_jbus_s_mes))
              {
              cree_bloc (b_jbus_s_mes, sizeof (spec_s_mes), 0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_jbus_s_mes) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_jbus_s_mes, val_geo.pos_specif_es);
            ptr_spec_s_mes = (tc_jbus_s_mes *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_mes, val_geo.pos_specif_es);
            (*ptr_spec_s_mes) = spec_s_mes;
            es.genre = contexte_jb.genre;
            es.sens = contexte_jb.sens;
            es.n_desc = d_jbus;
				    es.taille = 0;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.pos_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[3].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_titre_paragraf_jb :
            InsereTitreParagraphe (b_titre_paragraf_jb, ligne, profondeur,
                                     tipe_paragraf, &(val_geo.pos_paragraf));
            pos_paragraf = val_geo.pos_paragraf;
            if ((tipe_paragraf == canal_jb) || (tipe_paragraf == esclave_jb))
              {
              if (!existe_repere (b_spec_titre_paragraf_jb))
                {
                val_geo.pos_specif_paragraf = 1;
                cree_bloc (b_spec_titre_paragraf_jb, sizeof (spec_titre_paragraf), 0);
                }
              else
                {
                val_geo.pos_specif_paragraf = nb_enregistrements (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb) + 1;
                }
              insere_enr (szVERIFSource, __LINE__, 1, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
              ptr_spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
              if ((tipe_paragraf == canal_jb) && (spec_titre_paragraf.CanalVarOuCste == ID_CHAMP_VAR))
                {
                ReferenceES (spec_titre_paragraf.numero_canal);
                }
              if ((tipe_paragraf == esclave_jb) && (spec_titre_paragraf.EsclVarOuCste == ID_CHAMP_VAR))
                {
                ReferenceES (spec_titre_paragraf.numero_esclave);
                }
              (*ptr_spec_titre_paragraf) = spec_titre_paragraf;
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            break;

          default:
            break;

          } //switch numero_repere
        if (!existe_repere (b_geo_jbus))
           {
           cree_bloc (b_geo_jbus, sizeof (val_geo), 0);
           }
        insere_enr (szVERIFSource, __LINE__, 1, b_geo_jbus, ligne);
        donnees_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, ligne);
        (*donnees_geo) = val_geo;
        un_paragraphe = (BOOL)(numero_repere == b_titre_paragraf_jb);
        MajDonneesParagraphe (b_titre_paragraf_jb, ligne, INSERE_LIGNE,
                                un_paragraphe, profondeur, pos_paragraf);
        } // valide vect_ul Ok
      else
        {
        (*accepte_ds_bd) = FALSE;
        (*erreur_val) = erreur;
        }
      break;

    case SUPPRIME_LIGNE:
      consulte_titre_paragraf_jb (ligne);
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      donnees_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, ligne);
      val_geo = (*donnees_geo);
      switch (val_geo.numero_repere)
        {
        case b_pt_constant:
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;

        case b_jbus_e:
        case b_jbus_s:
        case b_jbus_e_tab:
        case b_jbus_s_tab:
        case b_jbus_e_mes:
        case b_jbus_s_mes:
          if (!bSupprimeES (val_geo.pos_es))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.pos_initial);
            switch (val_geo.numero_repere)
              {
              case b_jbus_e:
                ptr_spec_e = (tc_jbus_e *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e, val_geo.pos_specif_es);
                DeReferenceES (ptr_spec_e->i_cmd_rafraich);
                break;
              case b_jbus_e_mes:
                ptr_spec_e_mes = (tc_jbus_e_mes *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_mes, val_geo.pos_specif_es);
                DeReferenceES (ptr_spec_e_mes->i_cmd_rafraich);
                break;
              case b_jbus_e_tab:
                ptr_spec_e_tab = (tc_jbus_tab *)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_tab, val_geo.pos_specif_es);
                DeReferenceES (ptr_spec_e_tab->i_cmd_rafraich);
                if (ptr_spec_e_tab->AdrVarOuCste == ID_CHAMP_VAR)
                  {
                  DeReferenceES (ptr_spec_e_tab->PosVarAdresse);
                  }
                if (ptr_spec_e_tab->LongVarOuCste == ID_CHAMP_VAR)
                  {
                  DeReferenceES (ptr_spec_e_tab->longueur);
                  }
                break;
              case b_jbus_s_tab:
                ptr_spec_s_tab = (tc_jbus_tab *)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_tab, val_geo.pos_specif_es);
                DeReferenceES (ptr_spec_s_tab->i_cmd_rafraich);
                if (ptr_spec_s_tab->AdrVarOuCste == ID_CHAMP_VAR)
                  {
                  DeReferenceES (ptr_spec_s_tab->PosVarAdresse);
                  }
                if (ptr_spec_s_tab->LongVarOuCste == ID_CHAMP_VAR)
                  {
                  DeReferenceES (ptr_spec_s_tab->longueur);
                  }
                break;
              default:
                break;
              }
            enleve_enr (szVERIFSource, __LINE__, 1, val_geo.numero_repere, val_geo.pos_specif_es);
            maj_pointeur_dans_geo (val_geo.numero_repere, val_geo.pos_specif_es, TRUE);
            }
          break;

        case b_titre_paragraf_jb:
          un_paragraphe = TRUE;
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_jb, val_geo.pos_paragraf);
          tipe_paragraf = titre_paragraf->IdParagraphe;
          profondeur = titre_paragraf->profondeur;
          if (!bSupprimeTitreParagraphe (b_titre_paragraf_jb, val_geo.pos_paragraf))
            {
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            (*erreur_val) = IDSERR_SUPPRESSION_DU_PARAGRAPHE_IMPOSSIBLE_A_CETTE_POSITION;
            }
          else
            {
            maj_pointeur_dans_geo (b_titre_paragraf_jb, val_geo.pos_paragraf, FALSE);
            if (tipe_paragraf == canal_jb)
              {
							ptr_spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
              if (ptr_spec_titre_paragraf->CanalVarOuCste == ID_CHAMP_VAR)
                {
                DeReferenceES (ptr_spec_titre_paragraf->numero_canal);
                }
							enleve_enr (szVERIFSource, __LINE__, 1, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
							maj_pointeur_dans_geo(val_geo.numero_repere, val_geo.pos_specif_paragraf, TRUE);
							}
            if (tipe_paragraf == esclave_jb)
              {
							ptr_spec_titre_paragraf = (t_spec_titre_paragraf_jb *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
              if (ptr_spec_titre_paragraf->EsclVarOuCste == ID_CHAMP_VAR)
                {
                DeReferenceES (ptr_spec_titre_paragraf->numero_esclave);
                }
							enleve_enr (szVERIFSource, __LINE__, 1, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
							maj_pointeur_dans_geo(val_geo.numero_repere, val_geo.pos_specif_paragraf, TRUE);
							}
            }
          break;

        default:
          break;
        } // switch numero_repere
      if ((*supprime_ligne))
        {
        MajDonneesParagraphe (b_titre_paragraf_jb, ligne, SUPPRIME_LIGNE,
					un_paragraphe, profondeur, 0); //pos_paragraf);
        enleve_enr (szVERIFSource, __LINE__, 1, b_geo_jbus, ligne);
        }
      break;

    case CONSULTE_LIGNE:
       genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
       break;

    default:
      break;

    } // switch action
  } // valide_ligne
// ---------------------------------------------------------------------------
static void  creer_jbus (void)
  {
  jbus_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
static void  init_jbus (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  if (!jbus_deja_initialise)
    {
    ligne_depart_jbus = 1;
    ligne_courante_jbus = ligne_depart_jbus;
    jbus_deja_initialise = TRUE;
    }
  (*depart) = ligne_depart_jbus;
  (*courant) = ligne_courante_jbus;
  }

// ---------------------------------------------------------------------------
static DWORD  lis_nombre_de_ligne_jbus (void)
  {
  DWORD wretour;

  if (existe_repere (b_geo_jbus))
    {
    wretour = nb_enregistrements (szVERIFSource, __LINE__, b_geo_jbus);
    }
  else
    {
    wretour = 0;
    }
  return (wretour);
  }

// ---------------------------------------------------------------------------
static void  set_tdb_jbus (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_res[c_nb_car_message_res];
  char chaine_mot [20];

  tdb[0] = '\0';
  chaine_mot [0] = '\0';
  if ((existe_repere (b_titre_paragraf_jb)) && (nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf_jb) != 0))
    {
    if (nouvelle_ligne)
      {
      ligne--;
      }
    consulte_titre_paragraf_jb (ligne);
    if ((contexte_jb.numero_canal != 0) || (contexte_jb.CanalVarOuCste==ID_CHAMP_VAR))
      {
      bMotReserve (c_res_canal, mot_res);
      StrConcat (tdb, mot_res);
      StrConcatChar (tdb, ' ');
      if (contexte_jb.CanalVarOuCste==ID_CHAMP_VAR)
        {
        StrCopy (chaine_mot, contexte_jb.nom_canal);
        }
      else
        {
	      StrDWordToStr (chaine_mot, contexte_jb.numero_canal);
				}
      StrConcat (tdb, chaine_mot);
      if ((contexte_jb.numero_esclave != 0)||(contexte_jb.EsclVarOuCste==ID_CHAMP_VAR))
        {
        StrConcatChar (tdb, ' ');
        bMotReserve (c_res_esclave, mot_res);
        StrConcat (tdb, mot_res);
        StrConcatChar (tdb, ' ');
        StrConcat (tdb, sorte_auto [contexte_jb.sorte_auto]);
        StrConcatChar (tdb, ' ');
        if (contexte_jb.EsclVarOuCste==ID_CHAMP_VAR)
          {
          StrCopy (chaine_mot, contexte_jb.nom_esclave);
          }
        else
          {
          StrDWordToStr (chaine_mot, contexte_jb.numero_esclave);
          }
        StrConcat (tdb, chaine_mot);
        if ((contexte_jb.sens != 0) && (contexte_jb.genre != 0))
          {
          StrConcatChar (tdb, ' ');
          bMotReserve (contexte_jb.sens, mot_res);
          StrConcat (tdb, mot_res);
          StrConcatChar (tdb, ' ');
          bMotReserve (contexte_jb.genre, mot_res);
          StrConcat (tdb, mot_res);
          if (contexte_jb.genre == c_res_numerique)
            {
            StrConcatChar (tdb, ' ');
            bMotReserve (contexte_jb.genre_num, mot_res);
            StrConcat (tdb, mot_res);
            }
          }
        }
      }
    }
  }

// ---------------------------------------------------------------------------
static void  fin_jbus (BOOL quitte, DWORD courant)
  {
  if ((!quitte))
    {
    ligne_courante_jbus = courant;
    }
  }

// Interface du descripteur modbus
void LisDescripteurJb (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_jbus;
	pDescripteurCf->pfnInitDesc = init_jbus;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_jbus;
	pDescripteurCf->pfnValideLigne = valide_ligne_jbus;
	pDescripteurCf->pfnSetTdb = set_tdb_jbus;
	pDescripteurCf->pfnLisParamDlg = LIS_PARAM_DLG_JB;
	pDescripteurCf->pfnFin = fin_jbus;
	}

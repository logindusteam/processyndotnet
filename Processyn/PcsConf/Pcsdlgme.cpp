/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgme.c                                             |
 |   Auteur  :	LM							|
 |   Date    :	18/01/93						|
 |   Remarques : gestion des boites de dialogue dans le descripteur     |
 |               memoire                                                |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "appli.h"
#include "std.h"
#include "MemMan.h"
#include "UStr.h"
#include "UEnv.h"
#include "IdConfLng.h"
#include "ActionCf.h"
#include "CheckMan.h"
#include "lng_res.h"
#include "Tipe.h"
#include "Descripteur.h"
#include "pcsdlg.h"
#include "Verif.h"
#include "IdLngLng.h"
#include "pcsdlgme.h"

VerifInit;

typedef struct
	{
	t_dlg_desc echange_data;
	DWORD index_type;
	char nom_var[80];
	char taille_tab[80];
	char val_init[80];
	} t_param_memoire;

static char mot_res[40];
static char mot_res1[40];
static char tab_champs[6][STR_MAX_CHAR_ARRAY];
static DWORD nb_champs;
static char buffer[100];
static char taille_tab[30];
static DWORD texte_normal = ES_RIGHT | WS_GROUP | WS_VISIBLE | WS_CHILD;
static DWORD texte_grise  = ES_RIGHT | WS_GROUP | WS_VISIBLE | WS_CHILD;// $$ | DT_HALFTONE; WS_DISABLED?


// -----------------------------------------------------------------------
// Objet....: fonctions relatives a la wndproc me1
// -----------------------------------------------------------------------
static void retablir_memoire(HWND hwnd, t_param_memoire *param_memoire) // parametres sauvegardes 

  {
  switch (param_memoire->index_type)
    {
    case 1: // LOG SIMPLE
      SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_grise);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), FALSE);
      StrSetNull (taille_tab);
      SendDlgItemMessage (hwnd, ID_ME1_LOG_SIMPLE, BM_SETCHECK, TRUE, 0);
    break;
    case 2: // NUM SIMPLE
      SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_grise);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), FALSE);
      StrSetNull (taille_tab);
      SendDlgItemMessage (hwnd, ID_ME1_NUM_SIMPLE, BM_SETCHECK, TRUE, 0);
    break;
    case 3: // MESS SIMPLE
      SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_grise);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), FALSE);
      StrSetNull (taille_tab);
      SendDlgItemMessage (hwnd, ID_ME1_MESS_SIMPLE, BM_SETCHECK, TRUE, 0);
    break;

    case 4: // LOG TABLEAU
      SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_normal);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), TRUE);
      SendDlgItemMessage (hwnd, ID_ME1_LOG_TABLEAU, BM_SETCHECK, TRUE, 0);
    break;
    case 5: // NUM TABLEAU
      SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_normal);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), TRUE);
      SendDlgItemMessage (hwnd, ID_ME1_NUM_TABLEAU, BM_SETCHECK, TRUE, 0);
    break;
    case 6: // MESS TABLEAU
      SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_normal);
      ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), TRUE);
      SendDlgItemMessage (hwnd, ID_ME1_MESS_TABLEAU, BM_SETCHECK, TRUE, 0);
    break;

    default:
      break;

    } // switch 

  SetDlgItemText (hwnd, ID_ME1_NOM_VAR, param_memoire->nom_var);
  SetDlgItemText (hwnd, ID_ME1_TAILLE_TAB, param_memoire->taille_tab);
  SetDlgItemText (hwnd, ID_ME1_VAL_INIT, param_memoire->val_init);
  }

// -----------------------------------------------------------------------
static void defaut_memoire (HWND hwnd, t_param_memoire *param_memoire,
                     BOOL init) // parametres par defaut

  {
  //RECT rcl;

  if (init)
    {
    param_memoire->index_type    = 1;
    StrSetNull (param_memoire->nom_var);
    StrSetNull (param_memoire->taille_tab);
    StrSetNull (param_memoire->val_init);
    }
  StrSetNull (taille_tab);
  SetDlgItemText(hwnd, ID_ME1_NOM_VAR, "");
  SetDlgItemText(hwnd, ID_ME1_VAL_INIT, "");
  SetDlgItemText(hwnd, ID_ME1_TAILLE_TAB, "");
  SendDlgItemMessage (hwnd, ID_ME1_LOG_SIMPLE, BM_SETCHECK, TRUE, 0);
  SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_grise);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);
  ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), FALSE);
  }

// -----------------------------------------------------------------------
static void type_constante (t_param_memoire *pEnv, char *champ,
                     BOOL *un_log, BOOL *un_num, BOOL *un_mess)
  {
  char mot_res1[40];
  char mot_res2[40];

  *un_log  = FALSE;
  *un_num  = FALSE;
  *un_mess = FALSE;
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_zero, mot_res1);
  bMotReserve (c_res_zero, mot_res1);
  //(*pEnv->echange_data.ads_proc_message_mot_reserve) (c_res_un, mot_res2);
  bMotReserve (c_res_un, mot_res2);
  if ((bStrEgales (champ, mot_res1)) |
     (bStrEgales (champ, mot_res2)))
    { // va log
    *un_log = TRUE;
    }
  else
    {
    if (champ[0] == '"')
      { // va mess
      *un_mess = TRUE;
      }
    else
      { // va num
      *un_num = TRUE;
      }
    }
  }
// -----------------------------------------------------------------------
static DWORD fabrication_ul_memoire (HWND hwnd, char *chaine,
                             t_param_memoire *param_memoire)
  {
  BOOL un_log, un_num, un_mess;
  DWORD retour = 0;
  LONG index;

  // FABRICATION de la CHAINE � partir des champs de la boite
  StrSetNull (chaine);
  // r�cup�ration du nom de la variable
  GetDlgItemText (hwnd, ID_ME1_NOM_VAR, buffer, 30);
  StrCopy (param_memoire->nom_var, buffer);
  StrConcat (chaine, buffer);
  StrConcatChar (chaine, ' ');

  // r�cup�ration du type
  index = nCheckIndex (hwnd, ID_ME1_LOG_SIMPLE); //$$(LONG)SendDlgItemMessage (hwnd, ID_ME1_LOG_SIMPLE, BM_QUERYCHECKINDEX, 0, 0);
  param_memoire->index_type = (DWORD)index;
  if (index > 3)
    { // variable champ tableau
    // r�cup�ration de la Taille 
    GetDlgItemText (hwnd, ID_ME1_TAILLE_TAB, taille_tab, 30);
    if (StrIsNull (taille_tab))
      {
      retour = IDSERR_LA_TAILLE_DU_TABLEAU_EST_INVALIDE;
      }
    StrCopy (param_memoire->taille_tab, taille_tab);
    StrConcat (chaine, taille_tab);
    StrConcatChar (chaine, ' ');
    }

  // r�cup�ration de la Valeur Initiale 
  GetDlgItemText (hwnd, ID_ME1_VAL_INIT, buffer, 100);
  StrCopy (param_memoire->val_init, buffer);
  StrConcat (chaine, buffer);

  type_constante (param_memoire, buffer, &un_log, &un_num, &un_mess);

  switch (index)
    {
    case 1: // logique 
    case 4:
      if (!un_log)
        retour = IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE;
      break;

    case 2: // numerique 
    case 5:
      if (!un_num)
        retour = IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE;
      break;

    case 3: // message 
    case 6:
      if (!un_mess)
        retour = IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE;
      break;
    }

  return (retour);
  }

// -----------------------------------------------------------------------
// Boite de dialogue memoire
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgMem (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  bRes = TRUE;			   // Valeur de retour
  BOOL  commande_a_executer = FALSE;
  t_param_memoire *pEnv;
  t_param_action  param_action;
  t_retour_action retour_action;
  BOOL un_log, un_num, un_mess;
  DWORD erreur;

  // ------------ Switch message re�u.... --------------------------------
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc* pechange_data = (t_dlg_desc*) mp2;

			pEnv = (t_param_memoire *)pCreeEnv (hwnd, sizeof (t_param_memoire));
			pEnv->echange_data = (*pechange_data);

      // initialisation des champs de la boite 
      if (StrIsNull (pEnv->echange_data.chaine))
        { // pas double clique : on veut inserer 
        defaut_memoire (hwnd, pEnv, TRUE);
        }
      else
        { // double clique : on veut modifier, il faut r�afficher la ligne 
        StrDeleteFirstSpaces (pEnv->echange_data.chaine);
        nb_champs = StrToStrArray (tab_champs, 6, pEnv->echange_data.chaine);
        StrCopy (pEnv->nom_var, tab_champs[0]);
        StrCopy (pEnv->val_init, tab_champs[nb_champs-1]);

        if (nb_champs > 1)
          {
          StrCopy (pEnv->nom_var, tab_champs[0]);
          StrCopy (pEnv->val_init, tab_champs[nb_champs-1]);

          type_constante (pEnv, tab_champs[nb_champs-1],
                          &un_log, &un_num, &un_mess);
          if (un_log)
            { // va log 
            pEnv->index_type = 1;
            }
          else
            {
            if (un_num)
              { // va num 
              pEnv->index_type = 2;
              }
            else
              { // va mess 
              pEnv->index_type = 3;
              }
            }
          if (nb_champs > 2)
            { // variable tableau
            pEnv->index_type = pEnv->index_type + 3;
            StrCopy (pEnv->taille_tab, tab_champs[1]);
            }
          else
            { // variable simple
            StrSetNull (pEnv->taille_tab);
            }
          retablir_memoire(hwnd, pEnv);
          }
        else // ligne commentaire
          {
          defaut_memoire (hwnd, pEnv, TRUE);
          }
        }
      bRes = (LRESULT)FALSE;
			}
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      pEnv = (t_param_memoire *)pGetEnv (hwnd);
      break;

    case WM_DESTROY:
			bLibereEnv(hwnd);
      break;


    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_ME1_INSERER:
          pEnv = (t_param_memoire *)pGetEnv (hwnd);
          pEnv->echange_data.n_ligne_depart++;
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);
          fabrication_ul_memoire (hwnd, param_action.chaine, pEnv);
          param_action.entier = pEnv->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_ME1_MODIFIER:
          pEnv = (t_param_memoire *)pGetEnv (hwnd);
          if ((erreur = fabrication_ul_memoire (hwnd, param_action.chaine, pEnv)) == 0)
            {
            param_action.entier = pEnv->echange_data.n_ligne_depart;
            bAjouterActionCf (action_validation_dlg, &param_action);
            commande_a_executer = TRUE;
            }
          else
            {
            dlg_num_erreur (erreur);
            }
          break;

        case ID_ME1_DEFAUT:
          pEnv = (t_param_memoire *)pGetEnv (hwnd);
          defaut_memoire(hwnd, pEnv, FALSE);
          break;

        case ID_ME1_RETABLIR:
          pEnv = (t_param_memoire *)pGetEnv (hwnd);
          retablir_memoire(hwnd, pEnv);
          break;

				case ID_ME1_FERMER:
          pEnv = (t_param_memoire *)pGetEnv (hwnd);
          EndDialog(hwnd, FALSE);
          break;

				case IDCANCEL:
					pEnv = (t_param_memoire *)pGetEnv (hwnd);
					EndDialog(hwnd, FALSE);
					break;

	default:
      if( HIWORD( mp1 ) == BN_CLICKED )
        switch( (USHORT)( mp1 ) )
        {
        case ID_ME1_LOG_SIMPLE:
        case ID_ME1_NUM_SIMPLE:
        case ID_ME1_MESS_SIMPLE:
          SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_grise);
          ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);

          ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), FALSE);
          GetDlgItemText (hwnd, ID_ME1_TAILLE_TAB, taille_tab, 30);
          SetDlgItemText(hwnd, ID_ME1_TAILLE_TAB, "");
          break;

        case ID_ME1_LOG_TABLEAU:
        case ID_ME1_NUM_TABLEAU:
        case ID_ME1_MESS_TABLEAU:
          SetWindowLong(::GetDlgItem(hwnd, ID_ME1_TEXTE3), GWL_STYLE, texte_normal);
          ::InvalidateRect(::GetDlgItem(hwnd, ID_ME1_TEXTE3), NULL, TRUE);

          ::EnableWindow(::GetDlgItem(hwnd, ID_ME1_TAILLE_TAB), TRUE);
          SetDlgItemText(hwnd, ID_ME1_TAILLE_TAB, taille_tab);
          break;
        }
        break;
        }
		 if (commande_a_executer)
			{
			pEnv = (t_param_memoire *)pGetEnv (hwnd);
			bExecuteActionCf (&retour_action);
			pEnv->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
			StrCopy (pEnv->echange_data.chaine, retour_action.chaine);
			}
     break; // case WM_COMMAND:

    default:
      bRes = FALSE;
      break;
    }

  return (bRes);
  }


/* -------------------------------------------------------------------
fonction qui r�cup�re l'identificateur et l'adresse de la Window Proc
de la bo�te a afficher. En entr�e, on passe le num�ro de la bo�te parmi
les bo�tes du descripteur.
--------------------------------------------------------------------*/
void LIS_PARAM_DLG_ME (int index_boite, UINT *iddlg, FARPROC * adr_winproc)
  {
  switch (index_boite)
    {
    case 0 :
      *iddlg = DLG_INTERPRETE_ME1;
      (*adr_winproc) = (FARPROC) dlgMem;
      break;
		default:
			VerifWarningExit;
			break;
    }
  }


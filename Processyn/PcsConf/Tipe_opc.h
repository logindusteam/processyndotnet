// ----------------------------------------------------------------
// Tipe_opc.cpp
// declaration des types du driver client OPC
// ----------------------------------------------------------------
#ifndef Tipe_opc_h
#define Tipe_opc_h


//-------------------------- blocs
#define  b_geo_opc                     900
#define  b_spec_titre_paragraf_opc      901
#define  b_titre_paragraf_opc           902
#define  b_opc_item                  913

// ---------------------------------- Blocs pour l'ex�cution
#define  bx_opc_serveur                 914
#define  bx_opc_groupe                  915
#define  bx_opc_item                    916

// ----------------------------------
typedef struct
  {
  char szNomServeur[c_nb_car_ex_mes];// Nom du serveur
	DWORD dwIdESStatut;								// Var statut
	ID_MOT_RESERVE dwIdAcces;					// Mode d'acc�s au serveur OPC
	DWORD dwIdESExecution;						// 0 ou Id Var d'ex�cution du client $$ nouveau
  char szNomPoste[c_nb_car_ex_mes];	// Nom du poste si acces remote
  } SERVEUR_OPC_CF, *PSERVEUR_OPC_CF;

typedef struct
  {
  char szNomGroupe[c_nb_car_ex_mes];	// Nom du groupe
	DWORD dwIdESStatut;
	DWORD dwIdESRaf;
	ID_MOT_RESERVE IdSens;				// le groupe comport des entr�es ou des sorties
	ID_MOT_RESERVE dwIdOperation; // mode de lecture ou d'�criture
	DWORD dwCadenceMS;						// acc�s cache : fr�quence de rafraichissement
  } GROUPE_OPC_CF, *PGROUPE_OPC_CF;

typedef union
  {
	SERVEUR_OPC_CF Serveur;
	GROUPE_OPC_CF Groupe;
  } SPEC_TITRE_PARAGRAPHE_OPC, *PSPEC_TITRE_PARAGRAPHE_OPC;

// ----------------------------------
typedef struct
  {
  DWORD numero_repere; // identifiant du type de ligne interpr�teur
  union
    {
    DWORD pos_donnees;	// ligne de type commentaire : le num�ro enr du commentaire
    struct							// ligne de type paragraphe : infos de paragraphe
      {
      DWORD pos_paragraf;					// Num�ro d'enr bloc de paragraphe g�n�rique
      DWORD pos_specif_paragraf;	// Num�ro d'enr d'un bloc de paragraphe sp�cifique compl�mentaire
      };
    struct							// ligne de type entr�e / sortie
      {
      DWORD dwIdES;					// num�ro d'enr de la variable dans le bloc des entr�es sorties (b_e_s) 
      DWORD pos_initial;		// 0 (reflet) ou num�ro d'enr de la valeur initiale de la variable dans le bloc des constante
      DWORD pos_specif_es;	// num�ro d'enr de donn�es compl�mentaires � la variable
      };
    };
  } GEO_OPC, *PGEO_OPC;

typedef struct // bloc b_opc_item
  {
  char szAdresseItem[c_nb_car_ex_mes];// Adresse de l'item sur le serveur
  char szAdresseItemExt1[c_nb_car_ex_mes];// V6.06 : Extension 1 de l'adresse de l'item sur le serveur (si adresse>80 car)
  char szAdresseItemExt2[c_nb_car_ex_mes];// V6.06 : Extension 2 de l'adresse de l'item sur le serveur (si adresse>80 car)
  DWORD dwIdESRaf;
  DWORD dwIdESStatut;
	ID_MOT_RESERVE dwIdTypeVar;
  } ITEM_OPC_CF, *PITEM_OPC_CF;

typedef struct // Bloc  bx_opc_serveur
  {
  char szNomServeur[c_nb_car_ex_mes];// Nom du serveur
	DWORD dwIdESStatut;
	DWORD dwIdESExecution;						// 0 ou Id Var d'ex�cution du client $$ nouveau
	ID_MOT_RESERVE dwIdAcces;					// Mode d'acc�s au serveur OPC
  char szNomPoste[c_nb_car_ex_mes];	// Nom du poste si acces remote
	COPCClient * pCOPCClientEx;				// pointeur sur objet Client en ex�cution
	DWORD dwNPremierGroupeEx;					// Num�ro d'enr du premier X_GROUPE_OPC de ce serveur
	DWORD dwNbGroupesEx;							// Nb d'enrs X_GROUPE_OPC pour ce serveur
  } X_CLIENT_OPC, *PX_CLIENT_OPC;

typedef struct // Bloc  bx_opc_groupe
  {
  char szNomGroupe[c_nb_car_ex_mes];	// Nom du groupe
	DWORD dwIdESStatut;
	DWORD dwIdESRaf;							// Sortie : TRUE = r�p�titif FALSE = changement d'�tat 
																// lecture_directe : ?
	ID_MOT_RESERVE IdSens;				// le groupe comporte soit des entr�es ou des sorties
	ID_MOT_RESERVE dwIdOperation; // mode de lecture ou d'�criture
	DWORD dwCadenceMS;						// acc�s cache : fr�quence de rafraichissement
	COPCGroupe * pCOPCGroupeEx;		// pointeur sur objet groupe en ex�cution
	INT nIdGroupeEx;							// Identifiant du groupe pour le client
	DWORD dwNPremierItemEx;				// Num�ro d'enr du premier X_ITEM_OPC de ce groupe
	DWORD dwNbItemsEx;						// Nb d'enrs X_ITEM_OPC pour ce groupe
  } X_GROUPE_OPC, *PX_GROUPE_OPC;

typedef struct // Bloc bx_opc_item
  {
  char szAdresseItem[c_nb_car_ex_mes];// Adresse de l'item sur le serveur
  char szAdresseItemExt1[c_nb_car_ex_mes];// V6.06 : Extension 1 de l'adresse de l'item sur le serveur (si adresse>80 car)
  char szAdresseItemExt2[c_nb_car_ex_mes];// V6.06 : Extension 2 de l'adresse de l'item sur le serveur (si adresse>80 car)
	DWORD dwIdES;
  DWORD dwIdESRaf;
  DWORD dwIdESStatut;
	ID_MOT_RESERVE IdGenre;			// c_res_logique, c_res_numerique ou c_res_message
	DWORD dwTaille;             // taille != 0 si tableau
	VARTYPE VarType;						// uniquement pour les types variants
	INT nIdItemEx;							// Identifiant de l'item pour le client
	COPCItem * pCOPCItemEx;							// pointeur sur objet groupe en ex�cution
  } X_ITEM_OPC, *PX_ITEM_OPC;

#endif

// ---------------------------------------------------------------------------

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgjb.h						|
 |   Auteur  :	LM							|
 |   Date    :	08/10/92							|
 +----------------------------------------------------------------------*/

void    LIS_PARAM_DLG_JB (int index_boite, UINT *iddlg, FARPROC * adr_winproc);

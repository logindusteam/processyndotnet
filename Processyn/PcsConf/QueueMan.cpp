/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : QueueMan.c                                               |
 |   Auteur  : JS					        																			|
 |   Date    : 17/7/95 					        	|
//  NON IMPLANTE : D'autres peuvent ouvrir cette queue principale pour y �crire
//  ils r�cup�rent un handle de queue "secondaire" qu'ils peuvent fermer
//  sans impact sur la queue principale.
//  la fermeture de la queue principale ferme toutes les queues secondaires correspondantes

 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "Std.h"
#include "MemMan.h"
#include "BufMan.h"
#include "USem.h"
#include "UStr.h"
#include "UListeH.h"
#include "Verif.h"
//
#include "QueueMan.h"
VerifInit;

// �l�ments utilisateurs d'une queue
typedef struct
	{
	PVOID	pbBuf;		// pointer to buffer with element to write
	UINT	uiUser;		// request/identification data
	DWORD	cbBuf;		// number of bytes to write
	} ELEMENT_QUEUE_LI;

typedef ELEMENT_QUEUE_LI * PELEMENT_QUEUE_LI;


// Objet interne de contr�le d'une queue
typedef struct
	{
	HANDLE	hMutexAcces;							// s�maphore d'acc�s mutellement exclusif � cette queue
	HSEM		hsemQueueNonVide;					// s�maphore d'attente "au moins un �l�ment dans cette queue
	char		szNom [OFS_MAXPATHNAME];	// nom de la queue
	DWORD		dwNbElementsUtilisateurs;	// Nombre d'�l�ments utilisateurs dans la queue
	DWORD		dwTailleBufElements;			// Taille en octets du buffer d'�l�ments utilisateur
	PELEMENT_QUEUE_LI paElements;			// Adresse du tableau d'�l�ments utilisateur
	} QUEUE_LI;
typedef QUEUE_LI * PQUEUE_LI;


// instanciation du gestionnaire de handles
static HLISTE_H hListeHQueues;

//------------------------------------------------------------------------------
// conversions HQUEUE <-> PQUEUE_LI
static _inline HQUEUE hFromP (PQUEUE_LI pqueue) {return (HQUEUE)pqueue;}
static _inline PQUEUE_LI pFromH (HQUEUE hQueue)	{return (PQUEUE_LI)hQueue;}

//------------------------------------------------------------------------------
// Initialisation "� la vol�e" du bloc de handle
static _inline BOOL bInitAuto (void)
	{
	static BOOL bInitAFaire = TRUE;

	if (bInitAFaire && bCreeListeH(&hListeHQueues, INVALID_HQUEUE, TRUE))
		bInitAFaire = FALSE;
	return !bInitAFaire;
	}

//------------------------------------------------------------------------------
// Acc�s exclusif � la queue $$ �viter si non multithread ???
static _inline void AccesQueue (PQUEUE_LI pqueue)
	{
	WaitForSingleObject (pqueue->hMutexAcces, INFINITE);
	}

// Fin Acc�s exclusif � la queue
static _inline void FinAccesQueue (PQUEUE_LI pqueue)
	{
	ReleaseMutex (pqueue->hMutexAcces);
	}
	
//------------------------------------------------------------------------------
// recherche du nom d'une queue dans la liste des queues actuelles
static HQUEUE hChercheNomQueue (PCSTR pszNomRecherche)
	{
	DWORD dwNHandle;
	HQUEUE hQueue = NULL;
	DWORD	dwNbHandles = dwNbHDansListeH (hListeHQueues);

	// recherche du nom dans le tableau des queues
	for (dwNHandle = 0; dwNHandle < dwNbHandles; dwNHandle++)
		{
		PQUEUE_LI pQueueTemp = pFromH((HQUEUE)hDansListeH (hListeHQueues, dwNHandle));

		// Nom trouv� ?
		if (pQueueTemp && StrICompare (pszNomRecherche, pQueueTemp->szNom) == 0)
			{
			// oui => renvoie le handle touv�, recherche termin�e
			hQueue = hFromP(pQueueTemp);
			break;
			}
		}

	return hQueue;
	} // hChercheNomQueue

// -----------------------------------------------
// supprime tous les �l�ments d'une queue
static void	VideQueue (PQUEUE_LI pqueue)
	{
	DWORD dwNElement;
	PELEMENT_QUEUE_LI pelement = pqueue->paElements;

	// parcours des �l�ments de la queue
	for (dwNElement = 0; dwNElement < pqueue->dwNbElementsUtilisateurs; dwNElement++)
		{
		// donn�es associ�es � l'�l�ment ?
		if (pelement->cbBuf && pelement->pbBuf)
			// oui => lib�re les
			MemLibere (&pelement->pbBuf);

		// �l�ment suivant
		pelement++;
		}

	// supprime tous les �l�ments de la queue
	MemLibere ((PVOID *)(&pqueue->paElements));
	pqueue->paElements = NULL;
	pqueue->dwTailleBufElements = 0;

	// s�maphore mis en en attente d'arriv�e d'�l�ments dans la queue
	Verif (bSemPrends (pqueue->hsemQueueNonVide));

	// plus d'�l�ments
	pqueue->dwNbElementsUtilisateurs = 0;
	}

// -----------------------------------------------------------------------------
// d�truit une queue (sans supprimer son handle dans le tableau des handles)
static BOOL	bDetruitQueue (PHQUEUE phqueue)
	{
	BOOL	bRet = FALSE;

	if (phqueue)
		{
		PQUEUE_LI pqueue;

		if (*phqueue)
			{
			pqueue = pFromH (*phqueue);
			if (!IsBadReadPtr (pFromH (*phqueue), sizeof (QUEUE_LI)))
				{
				// attend la fin des autres acc�s � la queue
				AccesQueue (pqueue);
				//FinAccesQueue (pqueue) inutile

				// Supprime tous les �l�ments de la queue
				VideQueue (pqueue);

				// Lib�re les ressources de la queue
				if (pqueue->hMutexAcces)
					CloseHandle (pqueue->hMutexAcces);
				if (pqueue->hsemQueueNonVide)
					Verif (bSemFerme (&pqueue->hsemQueueNonVide));
				MemLibere ((PVOID *)(phqueue));

				// termin� Ok
				bRet = TRUE;
				}
			}
		}
	return bRet;
	} // bDetruitQueue

//------------------------------------------------------------------------------
// Recherche un handle de queue, y r�serve l'acc�s et renvoie son adresse si elle est trouv�e,
// renvoie NULL sinon
static PQUEUE_LI pRechercheEtAccesQueue (HQUEUE hqueue)
	{
	PHQUEUE phqueue;
	PQUEUE_LI pqueueRet;

	// lecture des handles
	AccesLectureListeH (hListeHQueues);

	// hQueue trouv� ?
	phqueue  = (PHQUEUE)phTrouveDansListeH (hListeHQueues, hqueue);
	pqueueRet = pFromH (*phqueue);
	if (pqueueRet)
		{
		// oui => r�serve l'acc�s � cette queue
		AccesQueue (pqueueRet);
		}
	// fin de l'acc�s � la table des handles
	FinAccesLectureListeH (hListeHQueues);
	return pqueueRet;
	}

//------------------------------------------------------------------------------
// Cr�ation d'une queue principale
HQUEUE hCreeQueue(
	PCSTR pszQueueName		// pointer to queue name
	)
	{
	HQUEUE hNouvelleQueue = NULL;

	// v�rification param�tres
	if (pszQueueName && (StrLength (pszQueueName) < MAX_PATH) && bInitAuto())
		{
		// acc�s en modification des handles
		AccesModifListeH (hListeHQueues);

		// v�rifie si le nom existe d�ja
		if (hChercheNomQueue (pszQueueName))
			SetLastError (ERROR_ALREADY_EXISTS);
		else
			{
			// Tout est bon, on alloue et initialise une nouvelle queue principale
			PQUEUE_LI	pqueue = (PQUEUE_LI)pMemAlloue (sizeof(QUEUE_LI));

			if (pqueue)
				{
				StrCopy (pqueue->szNom, pszQueueName);
				pqueue->dwNbElementsUtilisateurs = 0;
				pqueue->dwTailleBufElements = 0;
				pqueue->paElements = NULL;
				pqueue->hsemQueueNonVide = NULL;
				pqueue->hMutexAcces = CreateMutex (NULL, FALSE, NULL); // s�curit�, signaled, nom
				pqueue->hsemQueueNonVide = hSemCreeAttenteSeule (TRUE); // Pris tant qu'il n'y a pas d'�l�ment
				hNouvelleQueue = hFromP(pqueue);

				// ajout � la liste impossible ou erreurs sur les ressources ?
				if ((!bAjouteDansListeH (hListeHQueues, hNouvelleQueue)) || (!pqueue->hMutexAcces) || (!pqueue->hsemQueueNonVide))
					// oui => destruction de la nouvelle queue
					bDetruitQueue (&hNouvelleQueue);
				}
			}

		// fin des modifications
		FinAccesModifListeH (hListeHQueues);
		}

	// renvoie le handle de queue cr��
	return hNouvelleQueue;
	} // hCreeQueue


//------------------------------------------------------------------------------
BOOL bFermeQueue (PHQUEUE phqueue)
	{
	// Recherche du handle dans le tableau
	PHQUEUE phqueueLocal;
	BOOL bRes = FALSE;

	// init hListeHQueues
	if (bInitAuto())
		{
		// acc�s en modification des handles
		AccesModifListeH (hListeHQueues);

		// handle trouv� ?
		phqueueLocal  = (PHQUEUE)phTrouveDansListeH (hListeHQueues, *phqueue);
		if (phqueueLocal)
			{
			// oui => supprime handle et queue
			Verif (bDetruitQueue (phqueueLocal) && bSupprimeDansListeH (hListeHQueues, *phqueueLocal));
			*phqueue = NULL;
			bRes = TRUE;
			}

		// fin modification des handles
		FinAccesModifListeH (hListeHQueues);
		}

	return bRes;
	} // bFermeQueue


//------------------------------------------------------------------------------
BOOL bVideQueue (HQUEUE hqueue)
	{
	PQUEUE_LI pqueue;
	BOOL bRes = FALSE;

	// init hListeHQueues
	if (bInitAuto())
		{
		// recherche et acc�de la queue
		pqueue = pRechercheEtAccesQueue(hqueue);

		// queue trouv�e
		if (pqueue)
			{
			VideQueue (pqueue);

			// Termin� Ok
			FinAccesQueue (pqueue);
			bRes = TRUE;
			}
		}
	return bRes;
	} // bVideQueue

//------------------------------------------------------------------------------
// Ecriture d'un �lement dans la queue sp�cifi�e.
// Apr�s que l'�l�ment sp�cifi� soit �crit, le process propri�taire de la queue
// peut lire l'�l�ment � l'aide de uLitQueue
BOOL bEcritQueue
	(
	HQUEUE hqueue,    // target-queue handle
	UINT	uiUser,			// valeur 32 bit associ�e � l'�l�ment
	DWORD cbBuf,      // Taille en octets de l'�l�ment � ajouter � la pile
	PVOID pbBuf       // Adresse du buffer contenant l'�l�ment � �crire
	)
	{
	PQUEUE_LI pqueue;
	BOOL bRes = FALSE;

	// init hListeHQueues
	if (!bInitAuto())
		return ERROR_GEN_FAILURE;

	// recherche et acc�de la queue
	pqueue = pRechercheEtAccesQueue(hqueue);

	if (pqueue)
		{
		ELEMENT_QUEUE_LI element;

		// enregistrement des param�tres utilisateurs
		element.pbBuf = pbBuf;
		element.uiUser = uiUser;
		element.cbBuf = cbBuf;

		// ajout de l'�l�ment � la fin de la liste des �l�ments de cette queue Ok ?
		if (bBufAjouteFin ((PSTR *)&pqueue->paElements, &pqueue->dwTailleBufElements, (PSTR)&element, sizeof (element)))
			{
			// oui => mise � jour du s�maphore d'attente
			Verif (bSemLibere (pqueue->hsemQueueNonVide));
			pqueue->dwNbElementsUtilisateurs ++;

			// un �l�ment en plus
			bRes = TRUE;
			}

		FinAccesQueue (pqueue);
		}
	return bRes;
	} // bEcritQueue

//------------------------------------------------------------------------------
// Renvoie le plus ancien des �l�ments restants dans la queue.
// Si la queue est vide, retourne imm�diatement ou attend l'arriv�e 
// du prochain �l�ment selon bAttente.
// Le buffer r�cup�r� sera lib�r� (free) par le lecteur.
// Seul le process qui a cr�� la queue utilisera uLitQueue.
// Renvoie 0 si Ok Sinon ERROR_HANDLE_EOF, ERROR_INVALID_HANDLE
UINT uLitQueue
	(
	HQUEUE	hqueue,				// handle de la queue cible
	UINT *	puiUser,	// adresse d'une variable pour r�cup�rer la valeur 32 bit associ�e � l'�l�ment ou NULL
	DWORD * pcbData,	// adresse de la variable pour r�cup�rer la longueur de l'�l�ment
	PVOID * ppbuf,		// Adresse d'un pointeur pour r�cup�rer l'�lement
	BOOL		bAttente,	// Attente illimit�e de l'arriv�e d'un �l�ment si TRUE, pas d'attente sinon
	PHSEM		phsem			// adresse pour r�cup�rer un handle de s�maphore libre d�s l'arriv�e d'un �l�ment ou NULL
	)
	{
	UINT wErr = ERROR_INVALID_HANDLE;
	PQUEUE_LI pqueue;

	// init hListeHQueues
	if (!bInitAuto())
		return ERROR_GEN_FAILURE;

	pqueue = pRechercheEtAccesQueue(hqueue);

	// hQueue retrouv� ?
	if (pqueue)
		{
		// oui => y a t'il au moins un �l�ment dans la queue ?
		if (!pqueue->dwNbElementsUtilisateurs)
			{
			// non : Doit on attendre qu'il y en ait un ?
			if (bAttente)
				{
				HSEM hsemQueueNonVide = pqueue->hsemQueueNonVide;

				// oui => attente qu'il y ait au moins un �v�nement � lire
				FinAccesQueue (pqueue);
				dwSemAttenteLibre (hsemQueueNonVide, INFINITE);

				// recherche et acc�de la queue
				pqueue = pRechercheEtAccesQueue(hqueue);
				}
			else
				{
				// non => termin� : rien � lire
				wErr = ERROR_HANDLE_EOF;

				// r�cup�ration �ventuelle du s�maphore de synchro
				if (phsem)
					*phsem = pqueue->hsemQueueNonVide;
				}
			}
		} // if (pqueue)
	// queue existe et acc�d�e ?
	if (pqueue)
		{
		// elle contient au moins un �l�ment ?
		if (pqueue->dwNbElementsUtilisateurs)
			{
			// oui => on r�cup�re les donn�es de cet �l�ment
			PELEMENT_QUEUE_LI pelement = pqueue->paElements;

			if (puiUser)
				*puiUser = pelement->uiUser;
			if (pcbData)
				*pcbData = pelement->cbBuf;
			if (ppbuf)
				*ppbuf = pelement->pbBuf;
			// on le supprime de la queue
			if (bBufSupprimeDebut ((PSTR *)&pqueue->paElements, &pqueue->dwTailleBufElements, sizeof (*pelement)))
				{
				// un �l�ment de moins
				pqueue->dwNbElementsUtilisateurs --;
				// plus d'�l�ment ?
				if (!pqueue->dwNbElementsUtilisateurs)

					// oui => Attente de nouveaux �l�ments
					Verif (bSemPrends (pqueue->hsemQueueNonVide));

				wErr = 0;
				}
			else
				wErr = ERROR_INVALID_HANDLE;
			}
		FinAccesQueue (pqueue);
		} // if (pqueue)
	return wErr;
	} // uLitQueue

//------------------------------------------------------------------------------
// R�cup�re le s�maphore d'attente d'au moins un �l�ment de la queue
BOOL bQueueGetSemAttenteElement
	(HQUEUE hqueue,		// handle de la queue voulue
	PHSEM phsemDest)	// adresse pour recopie du s�maphore de pr�sence d'un �l�ment dans la queue
										// Renvoie TRUE si le handle de s�maphore est recopi�, FALSE sinon
	{
	BOOL bRes = FALSE;

	// init hListeHQueues
	if (bInitAuto())
		{
		PQUEUE_LI	pqueue = pRechercheEtAccesQueue(hqueue);

		// Queue trouv�e et pointeur s�maphore valide
		if (pqueue && phsemDest)
			{
			*phsemDest = pqueue->hsemQueueNonVide;
			FinAccesQueue (pqueue);
			bRes = TRUE;
			}
		}
	return bRes;
	} // uLitQueue

//------------------- fin QueueMan.c --------------------------------------

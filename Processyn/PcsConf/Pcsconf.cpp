//-------------------------------------------------------------
//|   Ce fichier est la propriete de Societe LOGIQUE INDUSTRIE
//|       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3
//|   Il est et demeure sa propriete exclusive et est confidentiel
//|   Aucune diffusion n'est possible sans accord ecrit        
//|------------------------------------------------------------
//|   Titre   : CONFIGURATION Processyn Win32
//| Historique
//| .	31/01/92|AC
//------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "UChrono.h"
#include "FileMan.h"
#include "Mem.h"
#include "Trap.h"
#include "Temps.h"
#include "pcsdlg.h"
#include "LireLng.h"
#include "actioncf.h"
#include "WConf.h"
#include "VersMan.h"
#include "PathMan.h"
#include "DocMan.h"
#include "init_txt.h"
#include "bdcf.h"
#include "UExcept.h"
#include "Appli.h"
#include "USignale.h"
#include "lng_res.h"
#include "tipe.h"
#include "Verif.h"
VerifInit;

// -----------------------------------------
// fermeture des ressources materielles
static void fermer_ressources (void)
  {
	// fermer les imprimantes �ventuellement ouvertes $$
  ::DestroyWindow (Appli.hwnd);
	fermer_bd();
  arrete_trap ();
  }

//------------------------------------------------------------------------
//                    Programme principal 
//-------------------------------------------------------------------------
int WINAPI WinMain
	(
	HINSTANCE  hInstance,			// handle instance courante
	HINSTANCE  hPrevInstance,	// handle instance pr�c�dente = 0 (WIN32)
	LPSTR  lpCmdLine,					// ligne de commande
	int  nShowCmd 						// Mode d'affichage de la fen�tre ::ShowWindow(nShowCmd)
  )
  {
	__try 
		{
		CHAR szVersionComplete [MAX_PATH];

		Appli.Init (hInstance, "Processyn configuration", GetVersionComplete(szVersionComplete));
		CHAR szPathAide [MAX_PATH]="";
		StrConcat(szPathAide,Appli.szPathExe);
		StrConcat(szPathAide,"DocPCS.chm");
		Appli.m_Aide.SetNomFichier(szPathAide);
		InitPathNameDocVierge (EXTENSION_SOURCE);

		// charge dll langue
		if (!bOuvrirLng ())
			SignaleExit ("Missing language DLL");
		// trap disque
		lance_trap ();
		// init temps
		InitChronosEtMinuteries ();

		// Bd par d�faut vierge
		creer_bd_vierge();

		// Cr�ation de la fen�tre bureau de PcsConf Obligatoire
		Verif (bCreeFenetreConf ());
		
		// �cran splash
		hwndCreeSplash (Appli.hwnd);

		// montre la fen�tre principale de l'application
		::ShowWindow (Appli.hwnd, nShowCmd);

		//init_patience (TRUE);
		InitContexteConf ();

		// La base de donn�es par d�faut - vierge est initialis� avec une appli par d�faut
		initialiser_ctxtbd_general ();

		// lecture du fichier Pcs.ini pour la config imprimante et alarme
		InterpreteFichierIni ();

		// ajout de la commande : traite ligne de commande
		t_param_action param_action;

		StrSetNull (param_action.chaine);
		if (lpCmdLine)
			StrCopy (param_action.chaine, lpCmdLine);
      
		bAjouterActionCf (action_traite_ligne_de_commande, &param_action);

		// Cr�ation de toutes les sous fen�tres du bureau
		bCreerSousFenetresBureau ();

		// Boucle de message principale
		MSG msg;
		while (GetMessage (&msg, NULL, 0, 0))
			{
			TranslateMessage(&msg);
			DispatchMessage (&msg);
			}

		/* $$ commentaris� car modif actions sur queue de message
		typedef struct
			{
			int mode_lecture;
			BOOL attente;
			DWORD n_ligne_depart;
			char chaine[255];
			char ligne_commande[MAX_PATH];
			void *ptr_wnd_proc_boite;
			DWORD id_dlg_boite;
			} PARAMS_CHERCHE_ACTION;


		#define mode_demarrage 0
		#define mode_interactif 1
		#define mode_boite 2

		t_retour_action retour_action;
		PARAMS_CHERCHE_ACTION pParamsChercheAction;

		InitPileAction();
		pParamsChercheAction.mode_lecture = mode_demarrage;
		pParamsChercheAction.attente = sans_attente;
		StrSetNull (pParamsChercheAction.ligne_commande);
		if (lpCmdLine)
			StrConcat (pParamsChercheAction.ligne_commande, lpCmdLine);
		ChercheAction (&pParamsChercheAction); // $$ interaction Messages / actions � revoir
		pParamsChercheAction.attente = avec_attente;
		retour_action.mode_lecture = mode_interactif;

		//$$ tant que mode interactif non pass� �a peut planter si une commande interactive arrive
		// tant que non re�u action quitte
		while (bExecuteActionCf (&retour_action))
			{
			pParamsChercheAction.mode_lecture = retour_action.mode_lecture;
			pParamsChercheAction.n_ligne_depart = retour_action.n_ligne_depart;
			StrCopy (pParamsChercheAction.chaine, retour_action.chaine);
			pParamsChercheAction.ptr_wnd_proc_boite = retour_action.ptr_wnd_proc_boite;
			pParamsChercheAction.id_dlg_boite = retour_action.id_dlg_boite;
			ChercheAction (&pParamsChercheAction);
			}
*/

		// fermeture des outils, des fen�tres, de la base de donn�es etc...
		fermer_ressources ();
		}	
	__except (nSignaleExceptionUser(GetExceptionInformation()))
		{
		ExitProcess((UINT)-1);// handler des exceptions choisies par le filtre
		}

	return 0; // $$ � voir
  }


// ----------------------------------------------------------------------


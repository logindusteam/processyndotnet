/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inpucf.h                                                 |
 |   Auteur  : AC					        	|
 |   Date    : 24/2/92 					        	|
 |   Version : 3.20							|
 +----------------------------------------------------------------------*/

typedef struct
	{
	int mode_lecture;
	BOOL attente;
	DWORD n_ligne_depart;
	char chaine[255];
	char ligne_commande[MAX_PATH];
	void *ptr_wnd_proc_boite;
	DWORD id_dlg_boite;
	} PARAMS_CHERCHE_ACTION;


#define mode_demarrage 0
#define mode_interactif 1
#define mode_boite 2
#define mode_macro 3

#define avec_attente TRUE
#define sans_attente FALSE

void ChercheAction (PARAMS_CHERCHE_ACTION *pParamsChercheAction);

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : pcsdlgcf.c                                                 |
 |   Auteur  : LM                                                       |
 |   Date    : 01/09/92 						|
 |   Version :                                                          |
 |   Remarques : Gestion des boites de dialogue recherche et imprime 
 |								dans Pcsconf            |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include <stdlib.h>

#include "std.h"
#include "Appli.h"
#include "MemMan.h"
#include "UEnv.h"
#include "IdConfLng.h"
#include "ULangues.h"
#include "pcsdlgcf.h"

// -----------------------------------------------------------------------
// Boite de dialogue de saisie d'un texte et d'un choix entre 2 radio_boutons
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocRecherche (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = TRUE;			   // Valeur de retour
  t_dlg_recherche * pEnvRecherche;

  switch (msg)
    {
    case WM_INITDIALOG:
			// Enregistre les param�tres d'Init
			pEnvRecherche = (t_dlg_recherche *)pSetEnvOnInitDialogParam (hwnd, mp2);

			// sens initialise vers le bas
			pEnvRecherche->sens = 1;
			SendDlgItemMessage (hwnd, ID_RECHERCHE_BAS, BM_SETCHECK, TRUE, 0);
			SetDlgItemText (hwnd, ID_SAISIE_RECHERCHE, pEnvRecherche->saisie);
      break;

    case WM_COMMAND:
			{
      switch( LOWORD(mp1))
        {
				case ID_RECHERCHE_HAUT:
					if (HIWORD (mp1) == BN_CLICKED)
						{
						pEnvRecherche = (t_dlg_recherche *)pGetEnv (hwnd);
						pEnvRecherche->sens = -1;
						}
					break;

				case ID_RECHERCHE_BAS:
					if (HIWORD (mp1) == BN_CLICKED)
						{
						pEnvRecherche = (t_dlg_recherche *)pGetEnv (hwnd);
						pEnvRecherche->sens = 1;
						}
					break;

				case IDOK:
					{
				  BOOL	recherche_ok;

					pEnvRecherche = (t_dlg_recherche *)pGetEnv (hwnd);
					recherche_ok = (GetDlgItemText (hwnd, ID_SAISIE_RECHERCHE, pEnvRecherche->saisie, 255) != 0);
					EndDialog (hwnd, recherche_ok);
					}
					break;

				case IDCANCEL:
					EndDialog(hwnd, FALSE);
				break;
        }
			}
      break; // WM_COMMAND

    default:
      mres = FALSE;
      break;
    }
  return mres;
  } // dlgprocRecherche


// -----------------------------------------------------------------------
// boite de dialogue pour la recherche
// Entr�es..: titre, noms des boutons
// Sorties..: retour_recherche contient le sens et le texte de la recherche
// Retour...: retour_b indique qu'on a bien recupere le texte a chercher
// -----------------------------------------------------------------------
BOOL dlg_recherche_mot (HWND hwnd, t_dlg_recherche * pretour_recherche)
  {
  return LangueDialogBoxParam(MAKEINTRESOURCE(DLG_RECHERCHE), hwnd, dlgprocRecherche, (LPARAM) pretour_recherche);
  }


// -----------------------------------------------------------------------
// Boite de dialogue d'impression
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgImprime (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  mres = TRUE;			   // Valeur de retour
  t_dlg_imprime** ptr_sur_data_a_init;
  
  switch (msg)
    {
    case WM_INITDIALOG:
			// allocation de memoire pour le pteur sur la structure
			ptr_sur_data_a_init = (t_dlg_imprime**)pMemAlloue (sizeof (t_dlg_imprime *));
			// la structure se trouve a l'adresse donnee par mp2
			(*ptr_sur_data_a_init) = (t_dlg_imprime *) mp2;
			// sens initialise vers le bas
			(*ptr_sur_data_a_init)->selecteur = 1;
			// place le pteur ds la memoire reservee correspondant a la window hwnd
  	  // initialise selection avec imprime_tout (rond noir)
			SendDlgItemMessage (hwnd, ID_IMPRIME_TOUT, BM_SETCHECK, TRUE, 0);

      SetWindowLong (hwnd, GWL_USERDATA, (LONG)ptr_sur_data_a_init); //$$ utiliser les fonctions d'environnement
      mres = FALSE;
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_VALIDE_IMPRIME:
					EndDialog(hwnd, TRUE);
					break;
				case ID_ABANDON_IMPRIME:
					EndDialog(hwnd, FALSE);
					break;
				default:
					if (HIWORD (mp1) == BN_CLICKED)
						{
						switch( LOWORD (mp1) )
							{
							case ID_IMPRIME_TOUT:
								ptr_sur_data_a_init = (t_dlg_imprime**)GetWindowLong (hwnd, GWL_USERDATA);
								(*ptr_sur_data_a_init)->selecteur = 1;
								break;
							case ID_IMPRIME_FENETRE:
								ptr_sur_data_a_init = (t_dlg_imprime**)GetWindowLong (hwnd, GWL_USERDATA);
								(*ptr_sur_data_a_init)->selecteur = 2;
								break;
							case ID_IMPRIME_SELECTION:
								ptr_sur_data_a_init = (t_dlg_imprime**)GetWindowLong (hwnd, GWL_USERDATA);
								(*ptr_sur_data_a_init)->selecteur = 3;
								break;
							}
						}
        } // switch(LOWORD(mp1))
      break;

    case WM_CHAR:
      if ((TCHAR)mp1 == VK_ESCAPE)
        EndDialog(hwnd, FALSE);
			else
        mres = FALSE;
      break;

    case WM_DESTROY:
			//$$ utiliser Envman
      ptr_sur_data_a_init = (t_dlg_imprime**)GetWindowLong (hwnd, GWL_USERDATA);
      MemLibere ((PVOID *)&ptr_sur_data_a_init); // libere la place allouee pour le pteur sur la structure
      break;

    default:
      mres = FALSE;
      break;
    }
  return (mres);
  }


// -----------------------------------------------------------------------
// Nom......: dlg_imprime
// Objet....: boite de dialogue pour l'impression
// Entr�es..: titre
// Sorties..: retour_imprime contient le selecteur
// Retour...: retour_b indique qu'on a bien demande l'impression
// Remarques: l'adresse de retour_imprime est passee a la WndProc lorsqu'on recoit le message
//            WM_INITDIALOG, l'adresse se trouve dans mp2
// -----------------------------------------------------------------------
BOOL dlg_imprime (HWND hwnd, t_dlg_imprime *retour_imprime)
  {
  return (BOOL) LangueDialogBoxParam (MAKEINTRESOURCE(DLG_IMPRIME), hwnd, dlgImprime, (LPARAM)retour_imprime);
  }



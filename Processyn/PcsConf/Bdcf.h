/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : interface action sur base de donn�es      		|
 |   Auteur  : AC							|
 |   Date    : 5/2/92 							|
 +----------------------------------------------------------------------*/

#define NBR_INDENTATION_MAX 60
#define NBR_CAR_LIGNE_MAX   255

// types de lignes : permet de gerer des couleurs differentes suivant les types
typedef enum
	{
	ligne_standard = 0,
	ligne_invalide = 1,
	ligne_commentaire = 2,
	ligne_contexte = 3
	} ETAT_LIGNE_BD;

void initialiser_ctxtbd_general (void);

void raz_bd (void);

void init_application (PCSTR chaine, PCSTR pszExtension);

// Charge une application
BOOL monter_application (PCSTR pszPathNameSansExtension, PCSTR pszExtension);

// Enregistre l'application
int sauver_application (PCSTR pszPathNameSansExtension, PCSTR pszExtension);

PCSTR pszNomDescripteurCourant (void); // appel� une fois pour charger l'aide

void initialiser_ctxtbd_desc (DWORD numero_descripteur);
BOOL lancer_satellite_bd (BOOL *pbExecutionSynchrone, char *nom, char *param,
			     char *env, char *chemin);
BOOL satellite_existe_bd (void);
void finir_desc_bd (BOOL quitte);

void monter_segment_bd (DWORD *decalage);
// renvoi le decalage effectue. renvoi 0 si pas de decalage ce qui permet le test

void descendre_segment_bd (DWORD *decalage);
// renvoi le decalage effectue. renvoi 0 si pas de decalage ce qui permet le test
// descend en fait jusqu'a taille bd + 1 pour permettre insertion derniere ligne

void positionner_segment_bd_100 (UINT pourcentage);

void positionner_segment_bd (DWORD segment_relatif);

void rechercher_mot_bd (DWORD offset, int type_recherche, char *mot_recherche, DWORD *decalage);
// type = 1 recherche vers le bas de mot
// type = 2 recherche vers le bas de invalide
// type < 0 recherche vers le haut de mot
// les lignes blanches sont ignor�es

void lire_contexte_bd (DWORD offset, char *nom_application, char *nom_descripteur,
											 char *contexte_descripteur, DWORD *pos_relative_ligne_bd, DWORD *taille_relative_bd);

BOOL fin_bd (DWORD offset);

void consulter_bd (DWORD offset, char *texte, ETAT_LIGNE_BD *type,
                   BOOL *possible, int *niveau, int *indentation, DWORD dwTailleTexte);

#define FORCAGE TRUE
#define SANS_FORCAGE FALSE

// insere une ligne dans le descripteur courant
void inserer_bd (DWORD offset, char *texte, DWORD *erreur_bd, BOOL forcage);

void supprimer_bd (DWORD offset, DWORD *erreur_bd, BOOL *possible);

void modifier_bd (DWORD offset, char *texte, DWORD *type, BOOL *possible);

void sauver_marque_bd (DWORD offset);

void restaurer_marque_bd (void);

void debut_selection_bd (DWORD offset);

void selectionner_tout_bd (void);

void abandon_selection_bd (void);

void typer_selection_bd (DWORD offset, BOOL *mode_selection);

void descendre_selection_bd (DWORD decalage);

void monter_selection_bd (DWORD decalage);

void positionner_selection_bd (DWORD numero_ligne);

void positionner_selection_bd_100 (UINT pourcentage);

void consulterln_selection_bd (char *texte, BOOL *possible, DWORD dwTailleTexte);

void supprimer_selection_bd (DWORD *erreur_bd);

void selection_item_bd (DWORD offset);

BOOL lis_param_boite_dlg (int index_boite, UINT *id_dlg_boite, FARPROC * ptr_wnd_proc_boite);

int lis_niveau_ligne_bd (DWORD ligne, char *chaine, int *indentation, DWORD dwTailleTexte);


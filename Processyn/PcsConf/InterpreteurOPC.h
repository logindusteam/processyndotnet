// InterpreteurOPC.h: interface for the CInterpreteurOPC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTERPRETEUROPC_H__82BA65C5_416C_11D2_A145_0000E8D90704__INCLUDED_)
#define AFX_INTERPRETEUROPC_H__82BA65C5_416C_11D2_A145_0000E8D90704__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Interpreteur.h"

class CInterpreteurOPC : public CInterpreteur  
{
public:
	CInterpreteurOPC();
	virtual ~CInterpreteurOPC();

};

#endif // !defined(AFX_INTERPRETEUROPC_H__82BA65C5_416C_11D2_A145_0000E8D90704__INCLUDED_)

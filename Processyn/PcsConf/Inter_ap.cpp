/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_ap.c                                               |
 |   Auteur  :   					        	|
 |   Date    :   					        	|
 |   Version : 4.0							|
 |                                                                      |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include <stdlib.h>
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "tipe_ap.h"
#include "cvt_ul.h"
#include "IdLngLng.h"
#include "inter_ap.h"
#include "Verif.h"
VerifInit;

#define  reflet_elmt_tab   1
#define  simple_non_reflet 2

// #define  max_taille_bit_ap 0x0FC00	// taille maxi de la zone bit utilisateur / carte
// #define  max_taille_mot_ap 0x04E20	// taille maxi de la zone mot utilisateur / carte
#define  max_taille_bit_ap 0x0FC00	// taille maxi de la zone bit utilisateur / carte
#define  max_taille_mot_ap 0x07FFF	// taille maxi de la zone mot utilisateur / carte

//-------------------------- Variables ---------------------
typedef struct
  {
  DWORD numero_carte;
  ID_MOT_RESERVE sens;
  ID_MOT_RESERVE genre;
  ID_MOT_RESERVE genre_donnees;
  } CONTEXTE_APPLICOM;

static CONTEXTE_APPLICOM contexte_ap;

static DWORD ligne_depart_ap;
static DWORD ligne_courante_ap;
static BOOL ap_deja_initialise;



/*-------------------------------------------------------------------------*/
static BOOL pas_un_word (FLOAT reel)
  {
  DOUBLE double_reel = (DOUBLE)reel;
  return ((BOOL)((double_reel < 0.0) || (double_reel > 65535.0)));
  }

/*-------------------------------------------------------------------------*/
static DWORD nombre_de_mot (int longueur)
  {
  DWORD nombre;

  if ((longueur % 2) != 0)
    {
    nombre = (DWORD)((longueur / 2) + 1);
    }
  else
    {
    nombre = (DWORD)(longueur / 2);
    }
  return (nombre);
  }

/*-------------------------------------------------------------------------*/
/*                        GESTION CONTEXTE                                 */
/*-------------------------------------------------------------------------*/

static void consulte_titre_paragraf_ap (DWORD ligne)
  {
  PTITRE_PARAGRAPHE pTitreParagraphe;
  PGEO_APPLICOM	donnees_geo;
  PSPEC_TITRE_PARAGRAPHE_APPLICOM	ptr_spec_titre_paragraf;
  CONTEXTE_PARAGRAPHE_LIGNE table_contexte_ligne[3];
  DWORD profondeur_ligne;
  DWORD i;
	
  if (bDonneContexteParagrapheLigne (b_titre_paragraf_ap, ligne, &profondeur_ligne, table_contexte_ligne))
    { // donne_contexte_ok
    for (i = 0; (i < profondeur_ligne); i++)
      {
      switch (table_contexte_ligne [i].IdParagraphe)
        {
        case carte_ap :
          pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_ap, table_contexte_ligne [i].position);
          donnees_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, pTitreParagraphe->pos_geo_debut);
          ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_ap, donnees_geo->pos_specif_paragraf);
          contexte_ap .numero_carte = ptr_spec_titre_paragraf-> numero_carte;
          contexte_ap.sens  = libre;
          break;
					
        case entre_num :
          contexte_ap .sens  = c_res_e;
          contexte_ap.genre = c_res_numerique;
          contexte_ap.genre_donnees = c_res_entier;
          break;
					
				case entre_num_mot:
          contexte_ap.sens  = c_res_e;
          contexte_ap.genre = c_res_numerique;
          contexte_ap.genre_donnees = c_res_mots;
          break;
					
				case entre_num_reel:
          contexte_ap.sens  = c_res_e;
          contexte_ap.genre = c_res_numerique;
          contexte_ap.genre_donnees = c_res_reels;
          break;
					
        case entre_log:
          contexte_ap.sens  = c_res_e;
          contexte_ap.genre = c_res_logique;
          break;
					
        case entre_mes:
          contexte_ap.sens  = c_res_e;
          contexte_ap.genre = c_res_message;
          break;
					
        case sorti_num :
          contexte_ap .sens  = c_res_s;
          contexte_ap.genre = c_res_numerique;
          contexte_ap.genre_donnees = c_res_entier;
          break;
					
				case sorti_num_mot:
          contexte_ap.sens  = c_res_s;
          contexte_ap.genre = c_res_numerique;
          contexte_ap.genre_donnees = c_res_mots;
          break;
					
				case sorti_num_reel:
          contexte_ap.sens  = c_res_s;
          contexte_ap.genre = c_res_numerique;
          contexte_ap.genre_donnees = c_res_reels;
          break;
					
        case sorti_log:
          contexte_ap.sens  = c_res_s;
          contexte_ap.genre = c_res_logique;
          break;
					
        case sorti_mes:
          contexte_ap.sens  = c_res_s;
          contexte_ap.genre = c_res_message;
          break;
					
        case fct_cyclique_ap:
          contexte_ap.numero_carte = 65535;
          contexte_ap.genre        = c_res_logique;
          contexte_ap.sens         = c_res_s;
          break;
					
				default:
          contexte_ap .numero_carte = 0;
          break;
        } // switch
      } // for i
    } // donne_contexte_ok
  else
    {
    contexte_ap .numero_carte = 0;
    }
  }

/*--------------------------------------------------------------------------*/
/*                             DIVERS                                       */
/*--------------------------------------------------------------------------*/

static void maj_pointeur_dans_geo (DWORD numero_repere, DWORD limite,
                                   BOOL spec)
  {
  PGEO_APPLICOM	donnees_geo;
  DWORD nbr_enr;
  DWORD i;
	
	if (!existe_repere (b_geo_ap))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_ap);
    }
  for (i = 1; (i <= nbr_enr); i++)
    {
    donnees_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap,i);
    if (donnees_geo->numero_repere == numero_repere)
      {
      switch (numero_repere)
        {
        case b_ap_e:
        case b_ap_e_mes:
        case b_ap_s:
        case b_ap_s_mes:
        case b_ap_e_tab:
        case b_ap_s_tab:
        case b_ap_s_reflet:
        case b_ap_s_tab_reflet:
        case b_ap_s_tab_reflet_mes:
        case b_ap_fct_cyc:
          if (donnees_geo->pos_specif_es >= limite)
						{
            donnees_geo->pos_specif_es--;
						}
          break;
					
        case b_titre_paragraf_ap:
					if (spec)
						{
            if (donnees_geo->pos_specif_paragraf >= limite)
							{
              donnees_geo->pos_specif_paragraf--;
							}
            }
					else
						{
						if (donnees_geo->pos_paragraf >= limite)
							{
							donnees_geo->pos_paragraf--;
							}
						}
          break;
					
				default:
          break;
					
        } //switch
      } //repere trouv�
    } /* for */
  }


/*--------------------------------------------------------------------------*/
void static traitement_num (PUL ul, DWORD num)
  {
  StrDWordToStr (ul->show, num);
  ul->erreur = 0;
  }

/*--------------------------------------------------------------------------*/
/*                             EDITION                                      */
/*--------------------------------------------------------------------------*/

//'''''''''''''''''''''''''''generation vecteur UL'''''''''''''''''''''''''''''

static void genere_vect_ul (DWORD ligne, PUL vect_ul,
                            int *nb_ul, int *niveau, int *indentation)
  {
  PGEO_APPLICOM	donnees_geo;
  C_APPLICOM_E *ap_e;
  C_APPLICOM_S *ap_s;
  C_APPLICOM_TAB *ap_e_tab;
  C_APPLICOM_TAB *ap_s_tab;
  C_APPLICOM_E_MES *ap_e_mes;
  C_APPLICOM_S_MES *ap_s_mes;
  C_APPLICOM_TAB_S_REFLET *ap_s_tab_reflet;
  C_APPLICOM_TAB_S_REFLET_MES *ap_s_tab_reflet_mes;
  C_APPLICOM_FCT_CYC *ap_fct_cyc;
  PTITRE_PARAGRAPHE pTitreParagraphe;
  PSPEC_TITRE_PARAGRAPHE_APPLICOM	ptr_spec_titre_paragraf;
  
  (*niveau) = 0;
  (*indentation) = 0;
  donnees_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap,ligne);
  switch (donnees_geo->numero_repere)
    {
    case b_pt_constant:
      (*nb_ul) = 1;
      vect_ul[0].erreur = 0;
      vect_ul[0].show[0] = '\0';
      consulte_cst_bd_c (donnees_geo->pos_donnees, vect_ul[0].show);
      break;
			
    case b_titre_paragraf_ap :
      pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_ap,donnees_geo->pos_paragraf);
      switch (pTitreParagraphe->IdParagraphe)
        {
        case carte_ap:
          (*niveau) = 1;
          (*indentation) = 0;
          (*nb_ul) = 2;
					init_vect_ul (vect_ul, *nb_ul);
          ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_ap,donnees_geo->pos_specif_paragraf);
          bMotReserve (c_res_carte, vect_ul[0].show);
          traitement_num(&(vect_ul[1]), ptr_spec_titre_paragraf->numero_carte);
          break;
					
        case entre_num :
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 3;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_e, vect_ul[0].show);
          bMotReserve (c_res_numerique, vect_ul[1].show);
          bMotReserve (c_res_entier, vect_ul[2].show);
					break;
					
        case entre_num_mot:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 3;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_e, vect_ul[0].show);
          bMotReserve (c_res_numerique, vect_ul[1].show);
          bMotReserve (c_res_mots, vect_ul[2].show);
          break;
					
        case entre_num_reel:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 3;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_e, vect_ul[0].show);
          bMotReserve (c_res_numerique, vect_ul[1].show);
          bMotReserve (c_res_reels, vect_ul[2].show);
          break;
					
        case entre_log:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 2;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_e, vect_ul[0].show);
          bMotReserve (c_res_logique, vect_ul[1].show);
          break;
					
        case entre_mes:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 2;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_e, vect_ul[0].show);
          bMotReserve (c_res_message, vect_ul[1].show);
          break;
					
        case sorti_num :
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 3;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_s, vect_ul[0].show);
          bMotReserve (c_res_numerique, vect_ul[1].show);
          bMotReserve (c_res_entier, vect_ul[2].show);
					break;
					
        case sorti_num_mot:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 3;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_s, vect_ul[0].show);
          bMotReserve (c_res_numerique, vect_ul[1].show);
          bMotReserve (c_res_mots, vect_ul[2].show);
					break;
					
        case sorti_num_reel:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 3;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_s, vect_ul[0].show);
          bMotReserve (c_res_numerique, vect_ul[1].show);
          bMotReserve (c_res_reels, vect_ul[2].show);
					break;
					
        case sorti_log:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 2;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_s, vect_ul[0].show);
          bMotReserve (c_res_logique, vect_ul[1].show);
          break;
					
        case sorti_mes:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 2;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_s, vect_ul[0].show);
          bMotReserve (c_res_message, vect_ul[1].show);
          break;
					
        case fct_cyclique_ap:
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 1;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_fonctions_cycliques, vect_ul[0].show);
          break;
					
				default:
          break;
        } //switch IdParagraphe
			break; //b_titre_paragraf_ap
				
    case b_ap_e :
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 4;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_e = (C_APPLICOM_E *)pointe_enr (szVERIFSource, __LINE__, b_ap_e, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_e->adresse);
      NomVarES (vect_ul[2].show, ap_e->i_cmd_rafraich);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[3].show);
      break;
			
    case b_ap_e_tab:
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 5;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_e_tab = (C_APPLICOM_TAB *)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_e_tab->taille);
      traitement_num(&(vect_ul[2]), ap_e_tab->adresse);
      NomVarES (vect_ul[3].show, ap_e_tab->i_cmd_rafraich);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[4].show);
      break;
			
    case b_ap_e_mes:
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 5;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_e_mes = (C_APPLICOM_E_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_e_mes, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_e_mes->longueur);
      traitement_num(&(vect_ul[2]), ap_e_mes->adresse);
      NomVarES (vect_ul[3].show, ap_e_mes->i_cmd_rafraich);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[4].show);
      break;
			
		case b_ap_s :
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 3;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_s = (C_APPLICOM_S *)pointe_enr (szVERIFSource, __LINE__, b_ap_s, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_s->adresse);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[2].show);
      break;
			
		case b_ap_s_reflet :
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 2;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_s = (C_APPLICOM_S *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_reflet, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_s->adresse);
      break;
			
		case b_ap_s_tab:
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 5;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_s_tab = (C_APPLICOM_TAB *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab,donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_s_tab->taille);
      traitement_num(&(vect_ul[2]), ap_s_tab->adresse);
      NomVarES (vect_ul[3].show, ap_s_tab->i_cmd_rafraich);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[4].show);
      break;
			
		case b_ap_s_tab_reflet:
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 3;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_s_tab_reflet = (C_APPLICOM_TAB_S_REFLET *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab_reflet, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_s_tab_reflet->indice);
      traitement_num(&(vect_ul[2]), ap_s_tab_reflet->adresse);
      break;
			
		case b_ap_s_mes :
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 4;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_s_mes = (C_APPLICOM_S_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_mes, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_s_mes->longueur);
      traitement_num(&(vect_ul[2]), ap_s_mes->adresse);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[3].show);
      break;
			
		case b_ap_s_reflet_mes :
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 2;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_s_mes = (C_APPLICOM_S_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_reflet_mes, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_s_mes->adresse);
      break;
			
		case b_ap_s_tab_reflet_mes:
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 3;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_s_tab_reflet_mes = (C_APPLICOM_TAB_S_REFLET_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab_reflet_mes, donnees_geo->pos_specif_es);
      traitement_num(&(vect_ul[1]), ap_s_tab_reflet_mes->indice);
      traitement_num(&(vect_ul[2]), ap_s_tab_reflet_mes->adresse);
      break;
			
		case b_ap_fct_cyc:
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 4;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      ap_fct_cyc = (C_APPLICOM_FCT_CYC *)pointe_enr (szVERIFSource, __LINE__, b_ap_fct_cyc, donnees_geo->pos_specif_es);
      traitement_num (&(vect_ul[1]), ap_fct_cyc->numero_canal);
      traitement_num (&(vect_ul[2]), ap_fct_cyc->numero_fonction_cyclique);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[3].show);
      break;
			
    default:
      break;
    } // switch numero repere
		
  } // genere_vect_ul


	typedef struct
		{
		C_APPLICOM_E *spec_e;
		C_APPLICOM_E_MES *spec_e_mes;
		C_APPLICOM_S *spec_s;
		C_APPLICOM_S_MES *spec_s_mes;
		C_APPLICOM_TAB *spec_e_tab;
		C_APPLICOM_TAB *spec_s_tab;
		C_APPLICOM_S *spec_s_reflet;
		C_APPLICOM_S_MES *spec_s_reflet_mes;
		C_APPLICOM_TAB_S_REFLET *spec_s_tab_reflet;
		C_APPLICOM_TAB_S_REFLET_MES *spec_s_tab_reflet_mes;
		C_APPLICOM_FCT_CYC *spec_fct_cyc;
		PSPEC_TITRE_PARAGRAPHE_APPLICOM	spec_titre_paragraf;
		} PARAM_VALIDE, *PPARAM_VALIDE;
 
//-------------------------------
// valide vecteur UL
//-------------------------------
static BOOL valide_vect_ul 
	(PUL vect_ul, int nb_ul, DWORD ligne,
	REQUETE_EDIT nRequete, DWORD *profondeur,
	PID_PARAGRAPHE tipe_paragraf,
	DWORD *numero_repere, DWORD *position_ident,
	DWORD *pos_init, BOOL *exist_init,
	PPARAM_VALIDE pPV,
	/*
	C_APPLICOM_E *spec_e,
	C_APPLICOM_E_MES *spec_e_mes,
	C_APPLICOM_S *spec_s,
	C_APPLICOM_S_MES *spec_s_mes,
	C_APPLICOM_TAB *spec_e_tab,
	C_APPLICOM_TAB *spec_s_tab,
	C_APPLICOM_S *spec_s_reflet,
	C_APPLICOM_S_MES *spec_s_reflet_mes,
	C_APPLICOM_TAB_S_REFLET *spec_s_tab_reflet,
	C_APPLICOM_TAB_S_REFLET_MES *spec_s_tab_reflet_mes,
	C_APPLICOM_FCT_CYC *spec_fct_cyc,
	PSPEC_TITRE_PARAGRAPHE_APPLICOM	spec_titre_paragraf,
	*/
	DWORD *erreur)

  {
  DWORD nbr_imbrication;
  ID_MOT_RESERVE n_fonction;
  ID_MOT_RESERVE n_fonction2;
  ID_MOT_RESERVE n_fonction3;
  DWORD mot;
  DWORD position_es;
  DWORD cas_sortie;
  BOOL exist_ident;
  BOOL booleen;
  BOOL un_log;
  BOOL un_num;
  FLOAT reel;
  CONTEXTE_APPLICOM old_contexte;
  PENTREE_SORTIE es;
  PGEO_APPLICOM	val_geo;
	
  //***********************************************
#define err(code) (*erreur) = code;return (FALSE)  //Sale Sioux
  //***********************************************
	
  old_contexte = contexte_ap;
  (*erreur) = 0;
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    if (!bReconnaitMotReserve ((vect_ul+0)->show, &n_fonction))
      {
      if (!bReconnaitConstante ((vect_ul+0), &booleen, &reel,
				&un_log, &un_num, exist_init, pos_init))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if (un_log || un_num)
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      (*numero_repere) = b_pt_constant;
      }
    else
      {
      switch (n_fonction)
        {
				
        case c_res_carte :
					if (nb_ul != 2)
            {
            err (IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
            }
          (*profondeur) = 1;
          nbr_imbrication = 0;
          if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
						(StrLength (vect_ul[1].show) != 1) ||
						(reel > 4) || (reel < 1))
            {
            err(61);
            }
          contexte_ap.numero_carte = (DWORD)(reel);
          pPV->spec_titre_paragraf->numero_carte = contexte_ap .numero_carte;
          (*numero_repere) = b_titre_paragraf_ap;
          (*tipe_paragraf) = carte_ap;
          break;
					
					
				case c_res_e :
				case c_res_s :
          if (!bReconnaitMotReserve ((vect_ul+1)->show, &n_fonction2))
            {
            err(32);
            }
          switch (n_fonction2)
            {
            case c_res_numerique:
              if (nb_ul == 2)
                {
                n_fonction3 = c_res_entier;
                }
              else
                {
                if (nb_ul != 3)
                  {
                  err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                if (!bReconnaitMotReserve ((vect_ul+2)->show, &n_fonction3))
                  {
									err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                }
              contexte_ap.genre = c_res_numerique;
              if (n_fonction == c_res_e)
                {
                switch (n_fonction3)
                  {
                  case c_res_entier:
                    (*tipe_paragraf) = entre_num;
                    contexte_ap.genre_donnees = c_res_entier;
                    break;
                  case c_res_mots:
                    (*tipe_paragraf) = entre_num_mot;
                    contexte_ap.genre_donnees = c_res_mots;
                    break; // mots
                  case c_res_reels:
                    (*tipe_paragraf) = entre_num_reel;
                    contexte_ap.genre_donnees = c_res_reels;
                    break;
									default:
                    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                    break;
                  } // switch n_fonction3
                } // fonction = e
              else
                {  // sorties
                switch (n_fonction3)
                  {
                  case c_res_entier:
                    (*tipe_paragraf) = sorti_num;
                    contexte_ap.genre_donnees = c_res_entier;
                    break;
									case c_res_mots:
										(*tipe_paragraf) = sorti_num_mot;
                    contexte_ap.genre_donnees = c_res_mots;
                    break;
									case c_res_reels:
										(*tipe_paragraf) = sorti_num_reel;
                    contexte_ap.genre_donnees = c_res_reels;
                    break;
									default:
                    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                    break;
                  } // switch n_fonction3
                } // n_fonction = s
              break; // numerique
							
            case c_res_logique:
							if (nb_ul != 2)
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              contexte_ap.genre = c_res_logique;
              if (n_fonction == c_res_e)
                {
								(*tipe_paragraf) = entre_log;
                } // n_fonction = e
              else
                { //sorties
								(*tipe_paragraf) = sorti_log;
                } // n_fonction = s
							break; // logique
							
						case c_res_message:
							if (nb_ul != 2)
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              contexte_ap.genre = c_res_message;
							if (n_fonction == c_res_e)
								{
								(*tipe_paragraf) = entre_mes;
                }
							else
								{
								(*tipe_paragraf) = sorti_mes;
								} // sortie
							break; // message
							
						default:
              err(32);
              break;
            } // switch n_fonction2
          (*profondeur) = 2;
          nbr_imbrication = 1;
          contexte_ap.sens = n_fonction;
          (*numero_repere) = b_titre_paragraf_ap;
          break;
					
        case c_res_fonctions_cycliques:
					if (nb_ul != 1)
            {
            err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          contexte_ap.numero_carte = 65535;
          contexte_ap.sens = c_res_s;
          contexte_ap.genre = c_res_logique;
          (*profondeur) = 1;
          nbr_imbrication = 0;
          (*numero_repere) = b_titre_paragraf_ap;
          (*tipe_paragraf) = fct_cyclique_ap;
          break; // fonctions_cycliques
					
				default:
          err (IDSERR_LA_DESCRIPTION_EST_INVALIDE); //err mot reserv� inconnu
          break;
        } //switch fonction mot reserv�
				if (!bValideInsereLigneParagraphe (b_titre_paragraf_ap,ligne,
					(*profondeur),nbr_imbrication,erreur) )
					{
					contexte_ap = old_contexte;
					err((*erreur));
					}
      } //mot reserv�
    }//pas identificateur
		else
			{ //identificateur
			switch (contexte_ap.sens)
				{
				case c_res_e :
					if (nRequete != MODIFIE_LIGNE)
						{
						if (exist_ident)
							{
							err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
							}
						} // nRequete != MODIFIE_LIGNE
					else
						{
						val_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne);
						if ((val_geo->numero_repere == b_ap_e) ||
              (val_geo->numero_repere == b_ap_e_mes) ||
              (val_geo->numero_repere == b_ap_e_tab))
							{
							if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
								{
								err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
								}
							} // deja description donnees
						}
					switch (contexte_ap.genre)
						{
						case c_res_logique:
							switch (nb_ul)
								{
								case 4:  // pas tableaux
									if (!bReconnaitConstanteNum((vect_ul+1), &booleen, &reel, &mot))
										{
										err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
										}
									if (pas_un_word(reel))
										{
										err (IDSERR_L_ADRESSE_EST_INVALIDE);
										}
									pPV->spec_e->adresse = (DWORD)reel;
									if (pPV->spec_e->adresse >= max_taille_bit_ap)
										{
										err(IDSERR_L_ADRESSE_EST_INVALIDE);
										}
									if (bReconnaitESExistante ((vect_ul+2), &position_es))
										{
										es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_es);
										if (es->genre != c_res_logique)
											{
											err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE); //la RAF doit �tre logique
											}
										}
									else
										{
										err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE); //la RAF n'existe pas dans le bloc des ESs
										}
									pPV->spec_e->i_cmd_rafraich = position_es;
									if (!bReconnaitConstante ((vect_ul+3), &booleen, &reel,
										&un_log, &un_num, exist_init, pos_init))
										{
										err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
										}
									if (!un_log)
										{
										err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre logique
										}
									(*numero_repere) = b_ap_e;
									break; // entree/logique/variables
									
								case 5:  //---------------- nb param�tres 5 (tableaux)
									if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
                    (StrLength (vect_ul[1].show) < 1) ||
                    (StrLength (vect_ul[1].show) > 4) ||
                    (reel < 1) ||
                    (reel > NB_MAX_ELEMENTS_VAR_TABLEAU))
										{
										err(IDSERR_LA_TAILLE_DU_TABLEAU_EST_COMPRISE_ENTRE_1_ET_10000);
										}
									pPV->spec_e_tab->taille = (DWORD)reel;
									if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
										{
										err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
										}
									if (pas_un_word(reel))
										{
										err (IDSERR_L_ADRESSE_EST_INVALIDE);
										}
									pPV->spec_e_tab->adresse = (DWORD)reel;
									if ((pPV->spec_e_tab->adresse + pPV->spec_e_tab->taille) >= max_taille_bit_ap)
										{
										err(IDSERR_L_ADRESSE_EST_INVALIDE);
										}
									if (bReconnaitESExistante((vect_ul+3), &position_es))
										{
										es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
										if (es->genre != c_res_logique)
											{
											err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE); //la RAF doit �tre logique
											}
										}
									else
										{
										err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE); //la RAF n'existe pas dans le bloc des ESs
										}
									pPV->spec_e_tab->i_cmd_rafraich = position_es;
									if (!bReconnaitConstante ((vect_ul+4), &booleen, &reel,
										&un_log, &un_num, exist_init, pos_init))
										{
										err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
										}
									if (!un_log)
										{
										err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre logique
										}
									(*numero_repere) = b_ap_e_tab;
									break; // entree/logique/tableaux
									
								default:
									err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
									break;
								} //switch nb_ul
							break; // entree/logique
							
								case c_res_numerique:
									switch (nb_ul)
										{
										case 4:  // pas tableaux
											if (!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
												{
												err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
												}
											if (pas_un_word(reel))
												{
												err (IDSERR_L_ADRESSE_EST_INVALIDE);
												}
											pPV->spec_e->adresse = (DWORD)reel;
											switch (contexte_ap.genre_donnees)
												{
												case c_res_entier:
												case c_res_mots:
													if (pPV->spec_e->adresse >= max_taille_mot_ap)
														{
														err(IDSERR_L_ADRESSE_EST_INVALIDE);
														}
													break;
												case c_res_reels :
													if ((pPV->spec_e->adresse+1) >= max_taille_mot_ap)
														{
														err(IDSERR_L_ADRESSE_EST_INVALIDE);
														}
													break;
												} // switch genre_donnees
											if (bReconnaitESExistante ((vect_ul+2), &position_es))
												{
												es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
												if (es->genre != c_res_logique)
													{
													err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE); //la RAF doit �tre logique
													}
												}
											else
												{
												err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE); //la RAF n'existe pas dans le bloc des ESs
												}
											pPV->spec_e->i_cmd_rafraich = position_es;
											if (!bReconnaitConstante ((vect_ul+3), &booleen, &reel,
												&un_log, &un_num, exist_init,pos_init))
												{
												err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
												}
											if (!un_num)
												{
												err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre numerique
												}
											(*numero_repere) = b_ap_e;
											break; // entree/numerique/variables
											
												case 5:  // tableaux
													if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
														{
														err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
														}
													if (pas_un_word(reel))
														{
														err (IDSERR_L_ADRESSE_EST_INVALIDE);
														}
													pPV->spec_e_tab->adresse = (DWORD)reel;
													switch (contexte_ap.genre_donnees)
														{
														case c_res_entier:
														case c_res_mots:
															if ((!bReconnaitConstanteNum ((vect_ul+1),&booleen, &reel, &mot)) ||
																(StrLength (vect_ul[1].show) < 1) ||
																(StrLength (vect_ul[1].show) > 3) ||
																(reel < 1) ||
																(reel > 128))
																{
																err(323);
																}
															pPV->spec_e_tab->taille = (DWORD)reel;
															if ((pPV->spec_e_tab->adresse + pPV->spec_e_tab->taille) >= max_taille_mot_ap)
																{
																err(IDSERR_L_ADRESSE_EST_INVALIDE);
																}
															break;
														case c_res_reels :
															if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel,& mot)) ||
																(StrLength (vect_ul[1].show) < 1) ||
																(StrLength (vect_ul[1].show) > 3) ||
																(reel < 1) ||
																(reel > 64))
																{
																err(334);
																}
															pPV->spec_e_tab->taille = (DWORD)reel;
															if ((pPV->spec_e_tab->adresse + (pPV->spec_e_tab->taille*2)) >= max_taille_mot_ap)
																{
																err(IDSERR_L_ADRESSE_EST_INVALIDE);
																}
															break;
														default:
															break;
														} // switch genre_donnees
													if (bReconnaitESExistante( (vect_ul+3), &position_es))
														{
														es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
														if (es->genre != c_res_logique)
															{
															err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE); //la RAF doit �tre logique
															}
														}
													else
														{
														err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE); //la RAF n'existe pas dans le bloc des ESs
														}
													pPV->spec_e_tab->i_cmd_rafraich = position_es;
													if (!bReconnaitConstante ((vect_ul+4), &booleen, &reel,
														&un_log, &un_num, exist_init, pos_init))
														{
														err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
														}
													if (!un_num)
														{
														err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre numerique
														}
													(*numero_repere) = b_ap_e_tab;
													break; // entree/numerique/tableaux
														default:
															err(IDSERR_LA_DESCRIPTION_EST_INVALIDE); //----------- nb param�tres INVALIDE
															break;
              } // switch nb_ul
							break; // entree/numerique
							
          case c_res_message :
            if (nb_ul != 5)
              {
              err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
							(StrLength (vect_ul[1].show) < 1) ||
							(StrLength (vect_ul[1].show) > 2) ||
							(reel < 1 ) ||
							(reel > 80))
              {
              err(IDSERR_LE_NOMBRE_DE_CARACTERES_EST_INVALIDE);
              }
            pPV->spec_e_mes->longueur = (DWORD)reel;
            if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
              {
              err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            if (pas_un_word(reel))
              {
              err (IDSERR_L_ADRESSE_EST_INVALIDE);
              }
            pPV->spec_e_mes->adresse = (DWORD)reel;
            if ((pPV->spec_e_mes->adresse + (nombre_de_mot((int)(pPV->spec_e_mes->longueur)))) >= max_taille_mot_ap)
              {
              err(IDSERR_L_ADRESSE_EST_INVALIDE);
              }
            if (bReconnaitESExistante ((vect_ul+3), &position_es))
              {
              es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
              if (es->genre != c_res_logique)
                {
                err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE); //la RAF doit �tre logique
                }
              }
            else
              {
              err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE); //la RAF n'existe pas dans le bloc des ESs
              }
            pPV->spec_e_mes->i_cmd_rafraich = position_es;
            if (!bReconnaitConstante ((vect_ul+4), &booleen, &reel,
							&un_log, &un_num, exist_init, pos_init))
              {
              err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
              }
            if ((un_num) || (un_log))
              {
              err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre message
              }
            (*numero_repere) = b_ap_e_mes;
            break;
          } // switch genre
					break; //e
					
      case c_res_s :
        switch (contexte_ap.genre)
          {
          case c_res_logique :
            if (contexte_ap.numero_carte == 65535)
              { // Gestion des Fonctions_cycliques
              if (nRequete != MODIFIE_LIGNE)
                {
                if (exist_ident)
                  {
                  err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
                  }
                } //nRequete != MODIFIE_LIGNE
              else
                {
                val_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne);
                if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
                  {
                  err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
                  }
                }
              if (nb_ul != 4)
                {
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
								(StrLength (vect_ul[1].show) < 1) ||
								(StrLength (vect_ul[1].show) > 2) ||
								(reel < 0) ||
								(reel > 15))
                {
                err(61);
                }
              pPV->spec_fct_cyc->numero_canal = (DWORD)reel;
              if ((!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot)) ||
								(StrLength (vect_ul[2].show) < 1) ||
								(StrLength (vect_ul[2].show) > 3) ||
								(reel < 0) ||
								(reel > 510))
                {
                err(327);
                }
              pPV->spec_fct_cyc->numero_fonction_cyclique = (DWORD)reel;
              if (!bReconnaitConstante ((vect_ul+3), &booleen, &reel,
								&un_log, &un_num, exist_init, pos_init))
                {
                err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                }
              if (!un_log)
                {
                err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre logique
                }
              (*numero_repere) = b_ap_fct_cyc;
              } //Gestion des Fonctions_cycliques
            else
              { // !Gestion des Fonctions_cycliques
              switch (nb_ul)
                {
                case 2 : // reflet sur va simple
                  if (!exist_ident)
                    {
                    err (IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
                    }
                  if (!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
                    {
                    err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                    }
                  if (pas_un_word(reel))
                    {
                    err(IDSERR_L_ADRESSE_EST_INVALIDE);
                    }
                  pPV->spec_s_reflet->adresse = (DWORD)reel;
                  if (pPV->spec_s_reflet->adresse >= max_taille_bit_ap)
                    {
                    err(IDSERR_L_ADRESSE_EST_INVALIDE);
                    }
                  position_es = dwIdVarESFromPosIdentif ((*position_ident));
                  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
                  if ((es->genre != c_res_logique))
                    {
                    err(IDSERR_LE_TYPE_DE_LA_VARIABLE_EST_DIFFERENT_DU_TYPE_DE_PARAGRAPHE);
                    }
                  (*numero_repere) = b_ap_s_reflet;
                  break; // sortie/logique reflet
									
                case 3 :  // reflet sur el tab ou declaration va simple
                  if (nRequete != MODIFIE_LIGNE)  // insertion
                    {
                    if (exist_ident)
                      {
                      cas_sortie = reflet_elmt_tab;
                      }
                    else
                      {
                      cas_sortie = simple_non_reflet;
                      } //nouvel identificateur
                    }//nRequete != MODIFIE_LIGNE
                  else
                    {  // modif
                    position_es = dwIdVarESFromPosIdentif ((*position_ident));
                    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
                    if ((es->sens == c_res_variable_ta_s) ||
											(es->sens == c_res_variable_ta_e) ||
											(es->sens == c_res_variable_ta_es) )
                      {
                      if (!exist_ident)
                        {
                        err (IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
                        }
                      cas_sortie = reflet_elmt_tab;
                      }//reflet tab
                    else
                      {
                      cas_sortie = simple_non_reflet;
                      val_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne);
                      if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
                        {
                        err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
                        }
                      } //nouvel identificateur
                    }
									
                  switch (cas_sortie)
                    {
                    case simple_non_reflet :
                      if (!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
                        {
                        err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                        }
                      if (pas_un_word(reel))
                        {
                        err (IDSERR_L_ADRESSE_EST_INVALIDE);
                        }
                      pPV->spec_s->adresse = (DWORD)reel;
                      if (pPV->spec_s->adresse > max_taille_bit_ap)
                        {
                        err(IDSERR_L_ADRESSE_EST_INVALIDE);
                        }
                      if (!bReconnaitConstante ((vect_ul+2), &booleen, &reel,
												&un_log, &un_num, exist_init, pos_init))
                        {
                        err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                        }
                      if (!un_log)
                        {
                        err (IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
                        }
                      (*numero_repere) = b_ap_s;
                      break; //cas sortie simple
											
                    case reflet_elmt_tab :
                      position_es = dwIdVarESFromPosIdentif ((*position_ident));
                      es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
                      if ((es->sens != c_res_variable_ta_s) &&
												(es->sens != c_res_variable_ta_e) &&
												(es->sens != c_res_variable_ta_es))
                        {
                        err(IDSERR_LA_VARIABLE_DOIT_ETRE_DE_TYPE_TABLEAU);
                        }
                      if (es->genre != c_res_logique)
                        {
                        err(IDSERR_LE_TYPE_DE_LA_VARIABLE_EST_DIFFERENT_DU_TYPE_DE_PARAGRAPHE);
                        }
                      if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
												(StrLength (vect_ul[1].show) < 1) ||
												(StrLength (vect_ul[1].show) > 4) ||
												(reel < 1) ||
												(reel > (FLOAT)(65535.0)))
                        {
                        err(IDSERR_INDICE_TABLEAU_INVALIDE);
                        }
                      pPV->spec_s_tab_reflet->indice = (DWORD)reel;
                      if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
                        {
                        err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                        }
                      if (pas_un_word(reel))
                        {
                        err (IDSERR_L_ADRESSE_EST_INVALIDE);
                        }
                      pPV->spec_s_tab_reflet->adresse = (DWORD)reel;
                      if (pPV->spec_s_tab_reflet->adresse > max_taille_bit_ap)
                        {
                        err(IDSERR_L_ADRESSE_EST_INVALIDE);
                        }
                      (*numero_repere) = b_ap_s_tab_reflet;
                      break; //cas reflet sur 1 elmt d'un tab
											
                    default:
                      break;
                    } //switch (cas_sortie
                  break; // reflet sur el tab ou declaration va simple
									
										case 5 : // d�claration Tableau
											if (nRequete != MODIFIE_LIGNE)
												{
												if (exist_ident)
													{
													err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
													}
												} //nRequete != MODIFIE_LIGNE
											else
												{
												val_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne);
												if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
													{
													err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
													}
												}
											if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
												(StrLength (vect_ul[1].show) < 1) ||
												(StrLength (vect_ul[1].show) > 4) ||
												(reel < 1) ||
												(reel > NB_MAX_ELEMENTS_VAR_TABLEAU))
												{
												err(IDSERR_LA_TAILLE_DU_TABLEAU_EST_COMPRISE_ENTRE_1_ET_10000);
												}
											pPV->spec_s_tab->taille = (DWORD)reel;
											if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
												{
												err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
												}
											if (pas_un_word(reel))
												{
												err (IDSERR_L_ADRESSE_EST_INVALIDE);
												}
											pPV->spec_s_tab->adresse = (DWORD)reel;
											if ((pPV->spec_s_tab->adresse + pPV->spec_s_tab->taille) >= max_taille_bit_ap)
												{
												err(IDSERR_L_ADRESSE_EST_INVALIDE);
												}
											if (bReconnaitESExistante ((vect_ul+3), &position_es))
												{
												es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
												if (es->genre != c_res_logique)
													{
													err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE); //la RAF doit �tre logique
													}
												}
											else
												{
												err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE); //la RAF n'existe pas dans le bloc des ESs
												}
											pPV->spec_s_tab->i_cmd_rafraich = position_es;
											if (!bReconnaitConstante ((vect_ul+4), &booleen, &reel,
												&un_log, &un_num, exist_init, pos_init))
												{
												err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
												}
											if (!un_log)
												{
												err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre logique
												}
											(*numero_repere) = b_ap_s_tab;
											break; // sortie/logique/tableaux
											
										default:
											err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
											break;
											
                } //switch (nb_ul
              } // !Gestion des Fonctions_cycliques
							break; // sortie/logique
							
          case c_res_numerique :
            switch (nb_ul)
              {
              case 2 : // reflet sur va simple
                if (!exist_ident)
                  {
                  err (IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
                  }
                if (!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
                  {
                  err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                if (pas_un_word(reel))
                  {
                  err(IDSERR_L_ADRESSE_EST_INVALIDE);
                  }
                pPV->spec_s_reflet->adresse = (DWORD)reel;
                switch (contexte_ap.genre_donnees)
                  {
                  case c_res_entier:
                  case c_res_mots:
                    if (pPV->spec_s_reflet->adresse >= max_taille_mot_ap)
                      {
                      err(IDSERR_L_ADRESSE_EST_INVALIDE);
                      }
                    break;
                  case c_res_reels :
                    if ((pPV->spec_s_reflet->adresse+1) >= max_taille_mot_ap)
                      {
                      err(IDSERR_L_ADRESSE_EST_INVALIDE);
                      }
                    break;
                  } // switch (genre_donnees
                position_es = dwIdVarESFromPosIdentif ((*position_ident));
                es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
                if ((es->genre != c_res_numerique))
                  {
                  err(IDSERR_LE_TYPE_DE_LA_VARIABLE_EST_DIFFERENT_DU_TYPE_DE_PARAGRAPHE);
                  }
                (*numero_repere) = b_ap_s_reflet;
                break; // sortie/logique reflet
								
									case 3 :  // reflet sur el tab ou declaration va simple
										if (nRequete != MODIFIE_LIGNE)  // insertion
											{
											if (exist_ident)
												{
												cas_sortie = reflet_elmt_tab;
												}
											else
												{
												cas_sortie = simple_non_reflet;
												} //nouvel identificateur
											}//nRequete != MODIFIE_LIGNE
										else
											{  // modif
											position_es = dwIdVarESFromPosIdentif ((*position_ident));
											es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
											if ((es->sens == c_res_variable_ta_s) ||
												(es->sens == c_res_variable_ta_e) ||
												(es->sens == c_res_variable_ta_es))
												{
												if (!exist_ident)
													{
													err (IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
													}
												cas_sortie = reflet_elmt_tab;
												}//reflet tab
											else
												{
												cas_sortie = simple_non_reflet;
												val_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne);
												if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
													{
													err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
													}
												} //nouvel identificateur
											}
										
										switch (cas_sortie)
											{
											case simple_non_reflet :
												if (!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
													{
													err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
													}
												if (pas_un_word(reel))
													{
													err (IDSERR_L_ADRESSE_EST_INVALIDE);
													}
												pPV->spec_s->adresse = (DWORD)reel;
												switch (contexte_ap.genre_donnees)
													{
													case c_res_entier:
													case c_res_mots:
														if (pPV->spec_s->adresse >= max_taille_mot_ap)
															{
															err(IDSERR_L_ADRESSE_EST_INVALIDE);
															}
														break;
													case c_res_reels :
														if ((pPV->spec_s->adresse+1) >= max_taille_mot_ap )
															{
															err(IDSERR_L_ADRESSE_EST_INVALIDE);
															}
														break;
													} // switch (genre_donnees
												if (!bReconnaitConstante ((vect_ul+2), &booleen, &reel,
													&un_log, &un_num, exist_init, pos_init))
													{
													err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
													}
												if (!un_num)
													{
													err (IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
													}
												(*numero_repere) = b_ap_s;
												break; //cas sortie simple
												
													case reflet_elmt_tab :
														position_es = dwIdVarESFromPosIdentif ((*position_ident));
														es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_es);
														if ((es->sens != c_res_variable_ta_s) &&
															(es->sens != c_res_variable_ta_e) &&
															(es->sens != c_res_variable_ta_es))
															{
															err(IDSERR_LA_VARIABLE_DOIT_ETRE_DE_TYPE_TABLEAU);
															}
														if ((es->genre != c_res_numerique))
															{
															err(IDSERR_LE_TYPE_DE_LA_VARIABLE_EST_DIFFERENT_DU_TYPE_DE_PARAGRAPHE);
															}
														if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
															(StrLength (vect_ul[1].show) < 1) ||
															(StrLength (vect_ul[1].show) > 3) ||
															(reel < 1) ||
															(reel > (FLOAT)(65535.0)))
															{
															err(IDSERR_INDICE_TABLEAU_INVALIDE);
															}
														pPV->spec_s_tab_reflet->indice = (DWORD)reel;
														if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
															{
															err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
															}
														if (pas_un_word(reel))
															{
															err (IDSERR_L_ADRESSE_EST_INVALIDE);
															}
														pPV->spec_s_tab_reflet->adresse = (DWORD)reel;
														switch (contexte_ap.genre_donnees)
															{
															case c_res_entier:
															case c_res_mots:
																if ((pPV->spec_s_tab_reflet->adresse >= max_taille_mot_ap))
																	{
																	err(IDSERR_L_ADRESSE_EST_INVALIDE);
																	}
																break;
															case c_res_reels :
																if ((pPV->spec_s_tab_reflet->adresse+1) >= max_taille_mot_ap)
																	{
																	err(IDSERR_L_ADRESSE_EST_INVALIDE);
																	}
																break;
															} // switch (genre_donnees
														(*numero_repere) = b_ap_s_tab_reflet;
														break; //cas reflet sur 1 elmt d'un tab
															default:
																break;
											} //switch (cas_sortie
										break; // reflet sur el tab ou declaration va simple
										
											case 5 : //
												if (nRequete != MODIFIE_LIGNE)
													{
													if (exist_ident)
														{
														err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
														}
													} //nRequete != MODIFIE_LIGNE
												else
													{
													val_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne);
													if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
														{
														err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
														}
													}
												if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
													{
													err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
													}
												if (pas_un_word(reel))
													{
													err (IDSERR_L_ADRESSE_EST_INVALIDE);
													}
												pPV->spec_s_tab->adresse = (DWORD)reel;
												switch (contexte_ap.genre_donnees)
													{
													case c_res_entier:
													case c_res_mots:
														if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
															(StrLength (vect_ul[1].show) < 1) ||
															(StrLength (vect_ul[1].show) > 3) ||
															(reel < 1) ||
															(reel > 128))
															{
															err(323);
															}
														pPV->spec_s_tab->taille = (DWORD)reel;
														if ((pPV->spec_s_tab->adresse + pPV->spec_s_tab->taille) >= max_taille_mot_ap)
															{
															err(IDSERR_L_ADRESSE_EST_INVALIDE);
															}
														break;
													case c_res_reels :
														if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
															(StrLength (vect_ul[1].show) < 1) ||
															(StrLength (vect_ul[1].show) > 3) ||
															(reel < 1) ||
															(reel > 64))
															{
															err(334);
															}
														pPV->spec_s_tab->taille = (DWORD)reel;
														if ((pPV->spec_s_tab->adresse + (pPV->spec_s_tab->taille*2)) >= max_taille_mot_ap)
															{
															err(IDSERR_L_ADRESSE_EST_INVALIDE);
															}
														break;
													} // switch (genre_donnees
												if (bReconnaitESExistante((vect_ul+3), &position_es))
													{
													es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
													if (es->genre != c_res_logique)
														{
														err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE); //la RAF doit �tre logique
														}
													}
												else
													{
													err(IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE); //la RAF n'existe pas dans le bloc des ESs
													}
												pPV->spec_s_tab->i_cmd_rafraich = position_es;
												if (!bReconnaitConstante ((vect_ul+4), &booleen, &reel,
													&un_log, &un_num, exist_init, pos_init))
													{
													err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
													}
												if (!un_num)
													{
													err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre numerique
													}
												(*numero_repere) = b_ap_s_tab;
												break; // sortie/numerique/tableaux
													default:
														err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
														break;
              } // switch nb_ul
						break; // sortie/numerique
							
          case c_res_message :
            switch (nb_ul)
              {
							
              case 2 : // reflet d'une var simple
                if (!exist_ident)
                  {
                  err (IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
                  }
                if (!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot))
                  {
                  err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                if (pas_un_word(reel))
                  {
                  err(IDSERR_L_ADRESSE_EST_INVALIDE);
                  }
                pPV->spec_s_reflet_mes->adresse = (DWORD)reel;
                if (pPV->spec_s_reflet_mes->adresse >= max_taille_mot_ap)
                  {
                  err(IDSERR_L_ADRESSE_EST_INVALIDE);
                  }
                position_es = dwIdVarESFromPosIdentif ((*position_ident));
                es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_es);
                if (es->genre != c_res_message)
                  {
                  err(IDSERR_LE_TYPE_DE_LA_VARIABLE_EST_DIFFERENT_DU_TYPE_DE_PARAGRAPHE);
                  }
                (*numero_repere) = b_ap_s_reflet_mes;
                break; // reflet d'une var simple
								
              case 3 : // reflet d'un element tableau
                if (!exist_ident)
                  {
                  err (IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
                  }
                position_es = dwIdVarESFromPosIdentif ((*position_ident));
                es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_es);
                if ((es->sens != c_res_variable_ta_s) &&
									(es->sens != c_res_variable_ta_e) &&
									(es->sens != c_res_variable_ta_es))
                  {
                  err(IDSERR_LA_VARIABLE_DOIT_ETRE_DE_TYPE_TABLEAU);
                  }
                if ((es->genre != c_res_message))
                  {
                  err(IDSERR_LE_TYPE_DE_LA_VARIABLE_EST_DIFFERENT_DU_TYPE_DE_PARAGRAPHE);
                  }
                if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
									(StrLength (vect_ul[1].show) < 1) ||
									(StrLength (vect_ul[1].show) > 3) ||
									(reel < 1) ||
									(reel > (FLOAT)(65535.0)))
                  {
                  err(IDSERR_INDICE_TABLEAU_INVALIDE);
                  }
                pPV->spec_s_tab_reflet_mes->indice = (DWORD)reel;
                if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
                  {
                  err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                if (pas_un_word(reel))
                  {
                  err (IDSERR_L_ADRESSE_EST_INVALIDE);
                  }
                pPV->spec_s_tab_reflet_mes->adresse = (DWORD)reel;
                if (pPV->spec_s_tab_reflet_mes->adresse >= max_taille_mot_ap)
                  {
                  err(IDSERR_L_ADRESSE_EST_INVALIDE);
                  }
                (*numero_repere) = b_ap_s_tab_reflet_mes;
                break; // reflet d'un element tableau
								
              case 4 : // declaration d'une var
                if (nRequete != MODIFIE_LIGNE)
                  {
                  if (exist_ident)
                    {
                    err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
                    }
                  } //nRequete != MODIFIE_LIGNE
                else
                  {
                  val_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne);
                  if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
                    {
                    err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
                    }
                  }
                if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
									(StrLength (vect_ul[1].show) < 1) ||
									(StrLength (vect_ul[1].show) > 2) ||
									(reel < 1) ||
									(reel > 80))
                  {
                  err(IDSERR_LE_NOMBRE_DE_CARACTERES_EST_INVALIDE);
                  }
                pPV->spec_s_mes->longueur = (DWORD)reel;
                if (!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot))
                  {
                  err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                if (pas_un_word(reel))
                  {
                  err (IDSERR_L_ADRESSE_EST_INVALIDE);
                  }
                pPV->spec_s_mes->adresse = (DWORD)reel;
                if ((pPV->spec_s_mes->adresse + (nombre_de_mot((int)(pPV->spec_s_mes->longueur)))) >= max_taille_mot_ap)
                  {
                  err(IDSERR_L_ADRESSE_EST_INVALIDE);
                  }
                if (!bReconnaitConstante ((vect_ul+3), &booleen, &reel,
									&un_log, &un_num, exist_init, pos_init))
                  {
                  err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                  }
                if ((un_num) || (un_log))
                  {
                  err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);//la constante d'init doit �tre message
                  }
                (*numero_repere) = b_ap_s_mes;
                break; // declaration d'une var
								
              default :
                err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
                break;
              } // switch nb_ul
						break; // sortie/message
							
          default:
            err (IDSERR_LE_SENS_EST_INVALIDE);
            break;
          } // genre donnees
					break; // sortie
					
      default:
        err (IDSERR_LE_SENS_EST_INVALIDE);
        break;
      } //switch sens
    } //identificateur
		
		return ((BOOL)((*erreur) == 0));
  } // valide_vect_ul

//-------------------------------------------------------------
static void  valide_ligne_ap 
	(REQUETE_EDIT nRequete, DWORD ligne,
	PUL vect_ul, int *nb_ul,
	BOOL *supprime_ligne, BOOL *accepte_ds_bd,
	DWORD *erreur_val, int *niveau, int *indentation)

  {
  DWORD erreur;
	DWORD pos_init;
  BOOL un_paragraphe;
  BOOL exist_init;
  C_APPLICOM_E spec_e;
  C_APPLICOM_E_MES spec_e_mes;
  C_APPLICOM_S spec_s;
  C_APPLICOM_S_MES spec_s_mes;
  C_APPLICOM_TAB spec_e_tab;
  C_APPLICOM_TAB spec_s_tab;
  C_APPLICOM_S spec_s_reflet;
  C_APPLICOM_S_MES spec_s_reflet_mes;
  C_APPLICOM_TAB_S_REFLET spec_s_tab_reflet;
  C_APPLICOM_TAB_S_REFLET_MES spec_s_tab_reflet_mes;
  C_APPLICOM_FCT_CYC spec_fct_cyc;
  C_APPLICOM_E *ptr_spec_e;
  C_APPLICOM_E_MES *ptr_spec_e_mes;
  C_APPLICOM_S *ptr_spec_s;
  C_APPLICOM_S_MES *ptr_spec_s_mes;
  C_APPLICOM_TAB *ptr_spec_e_tab;
  C_APPLICOM_TAB *ptr_spec_s_tab;
  C_APPLICOM_S *ptr_spec_s_reflet;
  C_APPLICOM_S_MES *ptr_spec_s_reflet_mes;
  C_APPLICOM_TAB_S_REFLET *ptr_spec_s_tab_reflet;
  C_APPLICOM_TAB_S_REFLET_MES *ptr_spec_s_tab_reflet_mes;
  C_APPLICOM_FCT_CYC *ptr_spec_fct_cyc;
  ENTREE_SORTIE es;
  DWORD numero_repere;
  DWORD pos_paragraf;
  DWORD profondeur;
  DWORD position_es;
  ID_PARAGRAPHE tipe_paragraf;
  SPEC_TITRE_PARAGRAPHE_APPLICOM spec_titre_paragraf;
  PSPEC_TITRE_PARAGRAPHE_APPLICOM	ptr_spec_titre_paragraf;
  PTITRE_PARAGRAPHE pTitreParagraphe;
  GEO_APPLICOM val_geo;
  PGEO_APPLICOM	donnees_geo;
	
  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;
  switch (nRequete)
    {
    case MODIFIE_LIGNE:
			{
			PARAM_VALIDE param;

      consulte_titre_paragraf_ap (ligne);
			param.spec_e = &spec_e;
			param.spec_e_mes = &spec_e_mes;
			param.spec_s = &spec_s;
			param.spec_s_mes = &spec_s_mes;
			param.spec_e_tab = &spec_e_tab;
			param.spec_s_tab = &spec_s_tab;
			param.spec_s_reflet = &spec_s_reflet;
			param.spec_s_reflet_mes = &spec_s_reflet_mes;
			param.spec_s_tab_reflet = &spec_s_tab_reflet;
			param.spec_s_tab_reflet_mes = &spec_s_tab_reflet_mes;
			param.spec_fct_cyc = &spec_fct_cyc;
			param.spec_titre_paragraf = &spec_titre_paragraf;

      if (valide_vect_ul(vect_ul, (*nb_ul), ligne, MODIFIE_LIGNE,
				&profondeur, &tipe_paragraf, &numero_repere,
				&(es.i_identif), &pos_init, &exist_init, &param, &erreur))
        {
        donnees_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap,ligne);
				val_geo = (*donnees_geo);
				if (val_geo .numero_repere == numero_repere)
					{
					switch (numero_repere)
            {
						case b_pt_constant :
							modifie_cst_bd_c (vect_ul[0].show, &(val_geo.pos_donnees));
							break; //b_pt_constant
							
            case b_titre_paragraf_ap:
              pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_ap,val_geo .pos_paragraf);
							if (pTitreParagraphe->IdParagraphe == tipe_paragraf)
								{
                if (tipe_paragraf == carte_ap)
                  {
                  ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_ap,val_geo.pos_specif_paragraf);
                  (*ptr_spec_titre_paragraf) = spec_titre_paragraf;
                  }
								}//m�me nom paragraphe
							else
                {
                erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
								} //modif nom paragraf impossible
              break; //b_titre_paragraf_ap
							
            case b_ap_e :
              es .sens  = contexte_ap .sens;
              es .genre = contexte_ap .genre;
							es .taille = 0;
              if (bModifieDeclarationES (&es,vect_ul[0].show, val_geo .pos_es))
                {
                modifie_cst_bd_c (vect_ul[3] .show, &(val_geo .pos_initial));
                ptr_spec_e = (C_APPLICOM_E *)pointe_enr (szVERIFSource, __LINE__, b_ap_e,val_geo .pos_specif_es);
                if ((ptr_spec_e->i_cmd_rafraich != spec_e.i_cmd_rafraich) )
                  {
                  DeReferenceES (ptr_spec_e->i_cmd_rafraich);
                  ReferenceES (spec_e.i_cmd_rafraich);
                  }
                (*ptr_spec_e) = spec_e;
                }//modif possible
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                } //modif impossible
              break; //b_ap_e
							
            case b_ap_e_tab:
              es.genre  = contexte_ap.genre;
              es.sens   = c_res_variable_ta_e;
              es.taille = spec_e_tab.taille;
              if (bModifieDeclarationES (&es,vect_ul[0].show,val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[4].show, &(val_geo.pos_initial));
                ptr_spec_e_tab = (C_APPLICOM_TAB *)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab,val_geo.pos_specif_es);
                if ((ptr_spec_e_tab->i_cmd_rafraich != spec_e_tab.i_cmd_rafraich) )
                  {
                  DeReferenceES (ptr_spec_e_tab->i_cmd_rafraich);
                  ReferenceES (spec_e_tab.i_cmd_rafraich);
                  }
                (*ptr_spec_e_tab) = spec_e_tab;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;
							
            case b_ap_e_mes:
              es.genre = contexte_ap.genre;
              es.sens = contexte_ap.sens;
							es .taille = 0;
              if (bModifieDeclarationES (&es,vect_ul[0].show,val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[4].show, &(val_geo.pos_initial));
                ptr_spec_e_mes = (C_APPLICOM_E_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_e_mes,val_geo.pos_specif_es);
                if ((ptr_spec_e_mes->i_cmd_rafraich != spec_e_mes.i_cmd_rafraich) )
                  {
                  DeReferenceES (ptr_spec_e_mes->i_cmd_rafraich);
                  ReferenceES (spec_e_mes.i_cmd_rafraich);
                  }
                (*ptr_spec_e_mes) = spec_e_mes;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;
							
            case b_ap_s :
              es.genre = contexte_ap.genre;
              es.sens = contexte_ap.sens;
							es .taille = 0;
              if (bModifieDeclarationES (&es,vect_ul[0].show,val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[2].show,&(val_geo.pos_initial));
                ptr_spec_s = (C_APPLICOM_S *)pointe_enr (szVERIFSource, __LINE__, b_ap_s,val_geo.pos_specif_es);
                (*ptr_spec_s) = spec_s;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;
							
            case b_ap_s_tab :
              es.genre = contexte_ap.genre;
              es.sens = c_res_variable_ta_s;
              es.taille = spec_s_tab.taille;
              if (bModifieDeclarationES (&es,vect_ul[0].show,val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[4].show,&(val_geo.pos_initial));
                ptr_spec_s_tab = (C_APPLICOM_TAB *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab,val_geo.pos_specif_es);
								if ((ptr_spec_s_tab->i_cmd_rafraich != spec_s_tab.i_cmd_rafraich) )
                  {
                  DeReferenceES (ptr_spec_s_tab->i_cmd_rafraich);
                  ReferenceES (spec_s_tab.i_cmd_rafraich);
                  }
                (*ptr_spec_s_tab) = spec_s_tab;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;
							
            case b_ap_s_mes:
              es.genre = contexte_ap.genre;
              es.sens = contexte_ap.sens;
							es .taille = 0;
              if (bModifieDeclarationES (&es,vect_ul[0].show,val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[3].show, &(val_geo.pos_initial));
                ptr_spec_s_mes = (C_APPLICOM_S_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_mes,val_geo.pos_specif_es);
                (*ptr_spec_s_mes) = spec_s_mes;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;
							
            case b_ap_s_reflet :
              position_es = dwIdVarESFromPosIdentif (es. i_identif);
              if (val_geo .pos_es != position_es )
                {
                ReferenceES (position_es);
                DeReferenceES (val_geo .pos_es);
                val_geo .pos_es = position_es;
                }
              ptr_spec_s_reflet = (C_APPLICOM_S *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_reflet,val_geo .pos_specif_es);
              (*ptr_spec_s_reflet) = spec_s_reflet;
              break;
							
            case b_ap_s_tab_reflet :
              position_es = dwIdVarESFromPosIdentif (es. i_identif);
              if (val_geo .pos_es != position_es )
                {
                ReferenceES (position_es);
                DeReferenceES (val_geo .pos_es);
                val_geo .pos_es = position_es;
                }
              ptr_spec_s_tab_reflet  = (C_APPLICOM_TAB_S_REFLET *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab_reflet,val_geo .pos_specif_es);
              (*ptr_spec_s_tab_reflet) = spec_s_tab_reflet;
              break;
							
            case b_ap_s_reflet_mes :
              position_es = dwIdVarESFromPosIdentif (es. i_identif);
              if (val_geo .pos_es != position_es )
                {
                ReferenceES (position_es);
                DeReferenceES (val_geo .pos_es);
                val_geo .pos_es = position_es;
                }
              ptr_spec_s_reflet_mes  = (C_APPLICOM_S_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_reflet_mes,val_geo .pos_specif_es);
              (*ptr_spec_s_reflet_mes) = spec_s_reflet_mes;
              break;
							
            case b_ap_s_tab_reflet_mes :
              position_es = dwIdVarESFromPosIdentif (es. i_identif);
              if (val_geo .pos_es != position_es )
                {
                ReferenceES (position_es);
                DeReferenceES (val_geo .pos_es);
                val_geo .pos_es = position_es;
                }
              ptr_spec_s_tab_reflet_mes  = (C_APPLICOM_TAB_S_REFLET_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab_reflet_mes,val_geo .pos_specif_es);
              (*ptr_spec_s_tab_reflet_mes) = spec_s_tab_reflet_mes;
              break;
							
            case b_ap_fct_cyc :
              es.genre = c_res_logique;
              es.sens  = c_res_s;
							es .taille = 0;
              if (bModifieDeclarationES (&es,vect_ul[0].show,val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[3].show,&(val_geo.pos_initial));
                ptr_spec_fct_cyc = (C_APPLICOM_FCT_CYC *)pointe_enr (szVERIFSource, __LINE__, b_ap_fct_cyc,val_geo.pos_specif_es);
                (*ptr_spec_fct_cyc) = spec_fct_cyc;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;
							
						default:
              break;
							
						} //switch numero repere
					}//meme repere : m�me type de ligne
				else
					{
					erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
					} //modif du type de la ligne impossible
				} // valide vect_ul Ok
			if (erreur != 0)
				{
				genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
				(*erreur_val) = erreur;
				}
			else
				{
				donnees_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne);
				(*donnees_geo) = val_geo;
				}
			(*accepte_ds_bd) = TRUE;
			(*supprime_ligne)=FALSE;
			}
			break;
				
    case INSERE_LIGNE:
			{
			PARAM_VALIDE param;

      consulte_titre_paragraf_ap (ligne - 1);

			param.spec_e = &spec_e;
			param.spec_e_mes = &spec_e_mes;
			param.spec_s = &spec_s;
			param.spec_s_mes = &spec_s_mes;
			param.spec_e_tab = &spec_e_tab;
			param.spec_s_tab = &spec_s_tab;
			param.spec_s_reflet = &spec_s_reflet;
			param.spec_s_reflet_mes = &spec_s_reflet_mes;
			param.spec_s_tab_reflet = &spec_s_tab_reflet;
			param.spec_s_tab_reflet_mes = &spec_s_tab_reflet_mes;
			param.spec_fct_cyc = &spec_fct_cyc;
			param.spec_titre_paragraf = &spec_titre_paragraf;

      if (valide_vect_ul(vect_ul, (*nb_ul), ligne, INSERE_LIGNE,
				&profondeur, &tipe_paragraf, &numero_repere,
				&(es.i_identif), &pos_init, &exist_init, &param, &erreur))
        {
				pos_paragraf = 0; // init pour mise a jour donnees paragraf
				val_geo .numero_repere = numero_repere;
				switch (numero_repere)
          {
					case b_pt_constant :
						AjouteConstanteBd (pos_init, exist_init, vect_ul[0].show,
							&(val_geo .pos_donnees));
						(*accepte_ds_bd) = TRUE;
						(*supprime_ligne) = FALSE;
						break;
						
          case b_titre_paragraf_ap :
            InsereTitreParagraphe (b_titre_paragraf_ap, ligne, profondeur,
							tipe_paragraf, &(val_geo .pos_paragraf));
						pos_paragraf = val_geo .pos_paragraf;
            if (tipe_paragraf == carte_ap)
              {
              if (!existe_repere (b_spec_titre_paragraf_ap))
                {
                val_geo .pos_specif_paragraf = 1;
                cree_bloc (b_spec_titre_paragraf_ap,sizeof (spec_titre_paragraf),0);
                }
              else
                {
                val_geo .pos_specif_paragraf = nb_enregistrements (szVERIFSource, __LINE__, b_spec_titre_paragraf_ap) + 1;
                }
              insere_enr (szVERIFSource, __LINE__, 1,b_spec_titre_paragraf_ap,val_geo.pos_specif_paragraf);
              ptr_spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_ap,val_geo.pos_specif_paragraf);
              (*ptr_spec_titre_paragraf) = spec_titre_paragraf;
              }
						(*accepte_ds_bd) = TRUE;
						(*supprime_ligne)= FALSE;
            break;  //b_titre_paragraf_ap
						
          case b_ap_e :
            if (!existe_repere (b_ap_e))
							{
              cree_bloc (b_ap_e,sizeof (spec_e),0);
							}
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_e) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_e,val_geo .pos_specif_es);
            ptr_spec_e = (C_APPLICOM_E *)pointe_enr (szVERIFSource, __LINE__, b_ap_e,val_geo.pos_specif_es);
            (*ptr_spec_e) = spec_e;
            ReferenceES (spec_e.i_cmd_rafraich);
            es.sens = contexte_ap.sens;
            es.genre = contexte_ap.genre;
            es.taille = 0;
            InsereVarES (&es,vect_ul[0] .show,&(val_geo.pos_es));
            AjouteConstanteBd (pos_init,exist_init,vect_ul[3].show,&(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_e
						
          case b_ap_e_tab :
            if (!existe_repere (b_ap_e_tab))
							{
              cree_bloc (b_ap_e_tab,sizeof (spec_e_tab),0);
							}
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_e_tab) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_e_tab,val_geo .pos_specif_es);
            ptr_spec_e_tab = (C_APPLICOM_TAB *)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab,val_geo.pos_specif_es);
            (*ptr_spec_e_tab) = spec_e_tab;
            ReferenceES (spec_e_tab.i_cmd_rafraich);
            es.sens = c_res_variable_ta_e;
            es.genre = contexte_ap.genre;
            es.taille = spec_e_tab.taille;
            InsereVarES (&es, vect_ul[0] .show,&(val_geo.pos_es));
            AjouteConstanteBd (pos_init,exist_init,vect_ul[4].show,&(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_e_tab
						
          case b_ap_e_mes :
            if (!existe_repere (b_ap_e_mes))
							{
              cree_bloc (b_ap_e_mes,sizeof (spec_e_mes),0);
							}
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_e_mes) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_e_mes,val_geo .pos_specif_es);
            ptr_spec_e_mes = (C_APPLICOM_E_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_e_mes,val_geo.pos_specif_es);
            (*ptr_spec_e_mes) = spec_e_mes;
            ReferenceES (spec_e_mes.i_cmd_rafraich);
            es.sens = contexte_ap.sens;
            es.genre = contexte_ap.genre;
            es.taille = 0;
            InsereVarES (&es,vect_ul[0].show,&(val_geo.pos_es));
            AjouteConstanteBd (pos_init,exist_init,vect_ul[4].show,&(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_e_mes
						
          case b_ap_s :
            if (!existe_repere (b_ap_s))
							{
              cree_bloc (b_ap_s ,sizeof (spec_s),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_s) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_s,val_geo.pos_specif_es);
            ptr_spec_s = (C_APPLICOM_S *)pointe_enr (szVERIFSource, __LINE__, b_ap_s,val_geo.pos_specif_es);
            (*ptr_spec_s) = spec_s;
            es.genre = contexte_ap.genre;
            es.sens = contexte_ap.sens;
            es.taille = 0;
            InsereVarES (&es,vect_ul[0].show,&(val_geo.pos_es));
            AjouteConstanteBd (pos_init,exist_init,vect_ul[2].show,&(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_s
						
          case b_ap_s_tab :
						if (!existe_repere (b_ap_s_tab))
              {
              cree_bloc (b_ap_s_tab,sizeof (spec_s_tab),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_s_tab) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_s_tab,val_geo.pos_specif_es);
            ptr_spec_s_tab = (C_APPLICOM_TAB *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab,val_geo.pos_specif_es);
            (*ptr_spec_s_tab) = spec_s_tab;
            ReferenceES (spec_s_tab.i_cmd_rafraich);
            es.genre = contexte_ap.genre;
            es.sens = c_res_variable_ta_s;
            es.taille = spec_s_tab.taille;
            InsereVarES (&es,vect_ul[0].show,&(val_geo.pos_es));
            AjouteConstanteBd (pos_init,exist_init,vect_ul[4].show,&(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_s_tab
						
          case b_ap_s_mes:
            if (!existe_repere (b_ap_s_mes))
              {
              cree_bloc (b_ap_s_mes,sizeof (spec_s_mes),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_s_mes) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_s_mes,val_geo.pos_specif_es);
            ptr_spec_s_mes = (C_APPLICOM_S_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_mes,val_geo.pos_specif_es);
            (*ptr_spec_s_mes) = spec_s_mes;
            es.genre = contexte_ap.genre;
            es.sens = contexte_ap.sens;
            es.taille = 0;
            InsereVarES (&es,vect_ul[0].show,&(val_geo.pos_es));
            AjouteConstanteBd (pos_init,exist_init,vect_ul[3].show,&(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;
						
          case b_ap_s_reflet :
						if (!existe_repere (b_ap_s_reflet))
              {
              cree_bloc (b_ap_s_reflet,sizeof (spec_s_reflet),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_s_reflet) + 1;
            val_geo.pos_es = dwIdVarESFromPosIdentif (es.i_identif);
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_s_reflet ,val_geo.pos_specif_es);
            ptr_spec_s_reflet = (C_APPLICOM_S *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_reflet ,val_geo.pos_specif_es);
            (*ptr_spec_s_reflet) = spec_s_reflet;
            ReferenceES (val_geo.pos_es);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_s_reflet
						
          case b_ap_s_tab_reflet :
						if (!existe_repere (b_ap_s_tab_reflet))
              {
              cree_bloc (b_ap_s_tab_reflet,sizeof (spec_s_tab_reflet),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_s_tab_reflet) + 1;
            val_geo.pos_es = dwIdVarESFromPosIdentif (es.i_identif);
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_s_tab_reflet ,val_geo.pos_specif_es);
            ptr_spec_s_tab_reflet = (C_APPLICOM_TAB_S_REFLET *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab_reflet ,val_geo.pos_specif_es);
            (*ptr_spec_s_tab_reflet) = spec_s_tab_reflet;
            ReferenceES (val_geo.pos_es);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_s_tab_reflet
						
          case b_ap_s_reflet_mes :
            if (!existe_repere (b_ap_s_reflet_mes))
              {
              cree_bloc (b_ap_s_reflet_mes,sizeof (spec_s_reflet_mes),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_s_reflet_mes) + 1;
            val_geo.pos_es = dwIdVarESFromPosIdentif (es.i_identif);
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_s_reflet_mes ,val_geo.pos_specif_es);
            ptr_spec_s_reflet_mes = (C_APPLICOM_S_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_reflet_mes ,val_geo.pos_specif_es);
            (*ptr_spec_s_reflet_mes) = spec_s_reflet_mes;
            ReferenceES (val_geo.pos_es);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_s_reflet_mes
						
          case b_ap_s_tab_reflet_mes :
            if (!existe_repere (b_ap_s_tab_reflet_mes) )
              {
              cree_bloc (b_ap_s_tab_reflet_mes,sizeof (spec_s_tab_reflet_mes),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_s_tab_reflet_mes) + 1;
            val_geo.pos_es = dwIdVarESFromPosIdentif (es.i_identif);
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_s_tab_reflet_mes ,val_geo.pos_specif_es);
            ptr_spec_s_tab_reflet_mes = (C_APPLICOM_TAB_S_REFLET_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab_reflet_mes ,val_geo.pos_specif_es);
            (*ptr_spec_s_tab_reflet_mes) = spec_s_tab_reflet_mes;
            ReferenceES (val_geo.pos_es);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_s_tab_reflet
						
          case b_ap_fct_cyc :
            if (!existe_repere (b_ap_fct_cyc))
              {
              cree_bloc (b_ap_fct_cyc,sizeof (spec_fct_cyc),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_ap_fct_cyc) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_ap_fct_cyc,val_geo.pos_specif_es);
            ptr_spec_fct_cyc = (C_APPLICOM_FCT_CYC *)pointe_enr (szVERIFSource, __LINE__, b_ap_fct_cyc,val_geo.pos_specif_es);
            (*ptr_spec_fct_cyc) = spec_fct_cyc;
            es.genre = c_res_logique;
            es.sens = c_res_s;
            es.taille = 0;
            InsereVarES (&es,vect_ul[0].show,&(val_geo.pos_es));
            AjouteConstanteBd (pos_init,exist_init,vect_ul[3].show,&(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break; // b_ap_s_tab
						
					default:
            break;
					} //switch (numero_repere
				if (!existe_repere (b_geo_ap))
					{
					cree_bloc (b_geo_ap,sizeof (val_geo),0);
					}
				insere_enr (szVERIFSource, __LINE__, 1,b_geo_ap,ligne);
				donnees_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap,ligne);
				(*donnees_geo) = val_geo;
				un_paragraphe = (BOOL)(numero_repere == b_titre_paragraf_ap);
				MajDonneesParagraphe (b_titre_paragraf_ap, ligne, INSERE_LIGNE,
				un_paragraphe, profondeur, pos_paragraf);
        }//vect_ul Ok
			else
				{
				(*accepte_ds_bd) = FALSE;
				(*erreur_val) = erreur;
				}
			}
			break;
				
    case SUPPRIME_LIGNE:
      consulte_titre_paragraf_ap (ligne);
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      donnees_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap,ligne);
      val_geo = (*donnees_geo);
      switch (val_geo .numero_repere)
        {
				case b_pt_constant :
					supprime_cst_bd_c (val_geo .pos_donnees);
          break;
					
        case b_titre_paragraf_ap :
					un_paragraphe = TRUE;
          pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_ap,val_geo .pos_paragraf);
					tipe_paragraf = pTitreParagraphe->IdParagraphe;
					profondeur = pTitreParagraphe->profondeur;
          if (!bSupprimeTitreParagraphe (b_titre_paragraf_ap,val_geo .pos_paragraf) )
						{
						(*accepte_ds_bd) = TRUE;
						(*supprime_ligne) = FALSE;
            (*erreur_val) = IDSERR_SUPPRESSION_DU_PARAGRAPHE_IMPOSSIBLE_A_CETTE_POSITION;
						}
					else
						{
            maj_pointeur_dans_geo (b_titre_paragraf_ap,val_geo .pos_paragraf,FALSE);
            if (tipe_paragraf == carte_ap)
              {
              enleve_enr (szVERIFSource, __LINE__, 1,b_spec_titre_paragraf_ap,val_geo.pos_specif_paragraf);
              maj_pointeur_dans_geo (val_geo .numero_repere,val_geo.pos_specif_paragraf,TRUE);
              }
						} //suppression possible
          break; //b_titre_paragraf_ap
					
        case b_ap_e:
        case b_ap_s:
        case b_ap_e_tab:
        case b_ap_s_tab:
        case b_ap_e_mes:
        case b_ap_s_mes:
        case b_ap_fct_cyc :
          if (!bSupprimeES (val_geo.pos_es))
						{
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
						}
					else
						{
            supprime_cst_bd_c (val_geo.pos_initial);
            switch (val_geo.numero_repere)
              {
              case b_ap_e:
                ptr_spec_e = (C_APPLICOM_E *)pointe_enr (szVERIFSource, __LINE__, b_ap_e,val_geo.pos_specif_es);
                DeReferenceES (ptr_spec_e->i_cmd_rafraich);
                break;
              case b_ap_s:
                break;
              case b_ap_e_tab:
                ptr_spec_e_tab = (C_APPLICOM_TAB *)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab,val_geo.pos_specif_es);
                DeReferenceES (ptr_spec_e_tab->i_cmd_rafraich);
                break;
              case b_ap_s_tab:
                ptr_spec_s_tab = (C_APPLICOM_TAB *)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab,val_geo.pos_specif_es);
                DeReferenceES (ptr_spec_s_tab->i_cmd_rafraich);
                break;
              case b_ap_e_mes:
                ptr_spec_e_mes = (C_APPLICOM_E_MES *)pointe_enr (szVERIFSource, __LINE__, b_ap_e_mes,val_geo.pos_specif_es);
                DeReferenceES (ptr_spec_e_mes->i_cmd_rafraich);
                break;
              case b_ap_s_mes:
                break;
              case b_ap_fct_cyc:
                break;
              }
            enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_specif_es);
            maj_pointeur_dans_geo (val_geo.numero_repere,val_geo.pos_specif_es,TRUE);
            }
          break; // non reflets
					
        case b_ap_s_reflet:
        case b_ap_s_tab_reflet:
        case b_ap_s_reflet_mes:
        case b_ap_s_tab_reflet_mes :
          DeReferenceES (val_geo.pos_es);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_specif_es);
          maj_pointeur_dans_geo (val_geo.numero_repere,val_geo.pos_specif_es,TRUE);
          break;
					
				default:
          break;
        } //switch val_geo .numero_repere
			if ((*supprime_ligne))
        {
        MajDonneesParagraphe (b_titre_paragraf_ap,ligne,SUPPRIME_LIGNE,
					un_paragraphe,profondeur, 0); // pos_paragraf
        enleve_enr (szVERIFSource, __LINE__, 1,b_geo_ap,ligne);
        }
      break;
			
		case CONSULTE_LIGNE:
			genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
			break;
			
		default:
			break;
					
    } // switch nRequete
  } // valide_ligne

// ---------------------------------------------------------------------------
static void  creer_ap (void)
  {
  ap_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
static void  init_ap (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  if (!ap_deja_initialise)
    {
    ligne_depart_ap = 1;
    ligne_courante_ap = ligne_depart_ap;
    ap_deja_initialise = TRUE;
    }
  (*depart) = ligne_depart_ap;
  (*courant) = ligne_courante_ap;
  }

// ---------------------------------------------------------------------------
static DWORD  lis_nombre_de_ligne_ap (void)
  {
  DWORD wretour;

  if (existe_repere (b_geo_ap))
    {
    wretour = nb_enregistrements (szVERIFSource, __LINE__, b_geo_ap);
    }
  else
    {
    wretour = 0;
    }
  return (wretour);
  }

// ---------------------------------------------------------------------------
static void  set_tdb_ap (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_res[c_nb_car_message_res];
  char chaine_mot [10];

  tdb[0] = '\0';
  chaine_mot [0] = '\0';
  if ((existe_repere (b_titre_paragraf_ap)) && (nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf_ap) != 0))
    {
    if (nouvelle_ligne)
      {
      ligne--;
      }
    consulte_titre_paragraf_ap (ligne);
    if ((contexte_ap .numero_carte != 0) &&
        (contexte_ap .numero_carte != 65535))
      {
      bMotReserve (c_res_carte, mot_res);
      StrConcat (tdb, mot_res);
      StrConcatChar (tdb, ' ');
      StrDWordToStr (chaine_mot, contexte_ap.numero_carte);
      StrConcat (tdb, chaine_mot);
      if (contexte_ap .sens != 0)
        {
        StrConcatChar (tdb, ' ');
        bMotReserve (contexte_ap.sens, mot_res);
        StrConcat (tdb, mot_res);
        StrConcatChar (tdb, ' ');
        bMotReserve (contexte_ap.genre, mot_res);
        StrConcat (tdb, mot_res);
        StrConcatChar (tdb, ' ');
        if (contexte_ap.genre == c_res_numerique)
          {
          bMotReserve (contexte_ap.genre_donnees, mot_res);
          StrConcat (tdb, mot_res);
          StrConcatChar (tdb, ' ');
          }
        } //* sens != 0 *//
      } //numero_carte != 0,65535
    if ((contexte_ap .numero_carte == 65535))
      {
      bMotReserve (c_res_fonctions_cycliques, mot_res);
      StrConcat (tdb, mot_res);
      }
    }
  }

// ---------------------------------------------------------------------------
static void  fin_ap (BOOL quitte, DWORD courant)
  {
  if ((!quitte))
    {
    ligne_courante_ap = courant;
    }
  }

// ---------------------------------------------------------------------------
// Interface du descripteur Applicom
void LisDescripteurAp (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_ap;
	pDescripteurCf->pfnInitDesc = init_ap;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_ap;
	pDescripteurCf->pfnValideLigne = valide_ligne_ap;
	pDescripteurCf->pfnSetTdb = set_tdb_ap;
	pDescripteurCf->pfnFin = fin_ap;
	}


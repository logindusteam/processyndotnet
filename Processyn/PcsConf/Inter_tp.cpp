/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_tp.c                                               |
 |   Auteur  : LM					        	|
 |   Date    : 13/04/94					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include <stdlib.h>
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "UChrono.h"
#include "tipe_tp.h"
#include "pcsdlgtp.h"
#include "cvt_ul.h"
#include "IdLngLng.h"
#include "inter_tp.h"
#include "Verif.h"
VerifInit;

//-------------------------- Procedures locales
//
static void consulte_titre_paragraf_cn (DWORD ligne);
static void maj_pointeur_dans_geo (DWORD numero_repere, DWORD limite);
void static traitement_num (PUL ul, DWORD num);
static void genere_vect_ul (DWORD ligne, PUL vect_ul,
                            int *nb_ul, int *niveau, int *indentation);
static BOOL valide_vect_ul (PUL vect_ul, int nb_ul, DWORD ligne,
                               REQUETE_EDIT action, DWORD *profondeur,
                               PID_PARAGRAPHE tipe_paragraf,
                               DWORD *numero_repere, DWORD *position_ident,
                               DWORD *pos_init, BOOL *exist_init,
                               LONG *spec_metro,
                               DWORD *erreur);

//-------------------------- Variables
//
static ID_MOT_RESERVE contexte_cn;
static DWORD ligne_depart_chrono;
static DWORD ligne_courante_chrono;
static BOOL chrono_deja_initialise;

/*-------------------------------------------------------------------------*/
/*                        GESTION CONTEXTE                                 */
/*-------------------------------------------------------------------------*/
static void consulte_titre_paragraf_cn (DWORD ligne)
  {
  CONTEXTE_PARAGRAPHE_LIGNE table_contexte_ligne[1];
  DWORD profondeur_ligne;
  DWORD i;

  if (bDonneContexteParagrapheLigne (b_titre_paragraf_chrono, ligne, &profondeur_ligne, table_contexte_ligne))
    { // donne_contexte_ok
    for (i = 0; (i < profondeur_ligne); i++)
      {
      switch (table_contexte_ligne [i].IdParagraphe)
        {
        case chronometre_cn:
          contexte_cn = c_res_chronometre;
          break;

        case metronome_cn:
          contexte_cn = c_res_metronome;
          break;

        default :
          contexte_cn = libre;
          break;
        }  // switch
      } // for
    } // donne_contexte_ok
  else
    {
    contexte_cn = libre;
    }
  }

/*--------------------------------------------------------------------------*/
/*                             DIVERS                                       */
/*--------------------------------------------------------------------------*/

static void maj_pointeur_dans_geo (DWORD numero_repere, DWORD limite)
  {
  PGEO_CHRONO	donnees_geo;
  DWORD nbr_enr;
  DWORD i;

  if (!existe_repere (b_geo_chrono))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_chrono);
    }
  for (i = 1; (i <= nbr_enr); i++)
    {
    donnees_geo = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono, i);
    if (donnees_geo->numero_repere == numero_repere )
      {
      switch (numero_repere)
        {
        case b_metronome:
          if (donnees_geo->pos_specif_es >= limite)
            {
            donnees_geo->pos_specif_es--;
            }
          break;

        case b_titre_paragraf_chrono:
          if (donnees_geo->pos_paragraf >= limite)
            {
            donnees_geo->pos_paragraf--;
            }
          break;

        default :
          break;
        } // switch
      } // if
    } // for
  }

/*--------------------------------------------------------------------------*/
/*                             EDITION                                      */
/*--------------------------------------------------------------------------*/

//'''''''''''''''''''''''''''generation vecteur UL'''''''''''''''''''''''''''''

static void genere_vect_ul (DWORD ligne, PUL vect_ul,
                            int *nb_ul, int *niveau, int *indentation)
  {
  PGEO_CHRONO	donnees_geo;
  PLONG ads_metronome; // $$ Donner un type explicite
  PTITRE_PARAGRAPHE titre_paragraf;
  
  (*niveau) = 0;
  (*indentation) = 0;
  donnees_geo = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono, ligne);
  switch (donnees_geo->numero_repere)
    {
    case b_pt_constant:
      (*nb_ul) = 1;
      vect_ul[0].erreur = 0;
      StrSetNull (vect_ul[0].show);
      consulte_cst_bd_c (donnees_geo->pos_donnees, vect_ul[0].show);
      break;

    case b_titre_paragraf_chrono :
      (*niveau) = 1;
      (*indentation) = 0;
      (*nb_ul) = 1;
      vect_ul[0].erreur = 0;
      StrSetNull (vect_ul[0].show);
      titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_chrono, donnees_geo->pos_paragraf);
      switch (titre_paragraf->IdParagraphe)
        {
        case chronometre_cn :
          bMotReserve (c_res_chronometre, vect_ul[0].show);
          break;

        case metronome_cn :
          bMotReserve (c_res_metronome, vect_ul[0].show);
          break;

        default :
          break;
        } // switch
      break;

    case b_metronome:
      (*niveau) = 2;
      (*indentation) = 1;
      (*nb_ul) = 2;
			init_vect_ul (vect_ul, *nb_ul);
			NomVarES (vect_ul[0].show, donnees_geo->pos_es);
			ads_metronome = (PLONG)pointe_enr (szVERIFSource, __LINE__, b_metronome, donnees_geo->pos_specif_es);
			StrLONGToStr (vect_ul[1].show, (*ads_metronome));
			break;

    case b_e_s :
      (*niveau) = 2;
      (*indentation) = 1;
      (*nb_ul) = 1;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->pos_es);
      break;

    default:
      break;
    } // switch
  } // genere_vect_ul

//''''''''''''''''''''''''valide vecteur UL''''''''''''''''''''''''''''''''''
 
//-------------------------------
static BOOL valide_vect_ul (PUL vect_ul, int nb_ul, DWORD ligne,
                               REQUETE_EDIT action, DWORD *profondeur,
                               PID_PARAGRAPHE tipe_paragraf,
                               DWORD *numero_repere, DWORD *position_ident,
                               DWORD *pos_init, BOOL *exist_init,
                               LONG *spec_metro,
                               DWORD *erreur)
  {
  DWORD nbr_imbrication;
  ID_MOT_RESERVE n_fonction;
  BOOL exist_ident;
  BOOL booleen;
  BOOL un_log;
  BOOL un_num;
  FLOAT reel;
  PGEO_CHRONO	val_geo;

  //***********************************************
  #define err(code) (*erreur) = code;return (FALSE)  //$$Sale Sioux
  //***********************************************


  (*erreur) = 0;
  if (nb_ul < 1)
    {
    err (IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
    }
  if (nb_ul > 2)
    {
    err (31);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    if (!bReconnaitMotReserve ((vect_ul+0)->show, &n_fonction))
      {
      if (!bReconnaitConstante ((vect_ul+0), &booleen, &reel,
                            &un_log, &un_num, exist_init, pos_init))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if ((un_log) || (un_num))
        {
        err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      (*numero_repere) = b_pt_constant;
      } // pas mot res
    else
      { // paragraf
      (*profondeur) = 1;
      nbr_imbrication = 0;
      (*numero_repere) = b_titre_paragraf_chrono;
      switch (n_fonction)
        {
        case c_res_metronome :
          (*tipe_paragraf) = metronome_cn;
          break;
 
        case c_res_chronometre :
          (*tipe_paragraf) = chronometre_cn;
          break;

        default :
          err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          break;
        } // switch

     if (!bValideInsereLigneParagraphe (b_titre_paragraf_chrono, ligne,
                                          (*profondeur), nbr_imbrication,
                                          erreur))
        {
        err((*erreur));
        }
      } // mot res
    } // non identificateur
  else
    {
    if (action != MODIFIE_LIGNE)
      {
      if (exist_ident)
        {
        err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    else
      { // modification
      val_geo = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono, ligne);
      if ((val_geo->numero_repere == b_e_s) ||
          (val_geo->numero_repere == b_metronome))
        {
        if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
          {
          err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
          }
        } // deja description donnees
      else
        {
        if (exist_ident)
          {
          err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
          }
        }
      }
    switch (contexte_cn)
      {
      case c_res_metronome :
        if (nb_ul != 2)
          {
          err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (!bReconnaitConstante ((vect_ul+1), &booleen, &reel,
                               &un_log, &un_num, exist_init,pos_init))
          {
          err(73);
          }
        if (!un_num)
          {
          err (73);
          }
        (*spec_metro) = (LONG)reel;
        (*numero_repere) = b_metronome;
        break;

      case c_res_chronometre :
        if (nb_ul != 1)
          {
          err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        (*numero_repere) = b_e_s;
        break;

      default :
        err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        break;
      } // switch
    }
  return ((BOOL)((*erreur) == 0));
  } // valide_vect_ul

//------------------------------------------------------------------
static void  valide_ligne_chrono
	(REQUETE_EDIT action, DWORD ligne, PUL vect_ul, int *nb_ul, 
	BOOL *supprime_ligne, BOOL *accepte_ds_bd, DWORD *erreur_val, int *niveau, int *indentation)
  {
  DWORD erreur;
  DWORD pos_init;
  DWORD pos_init_var_metro;
  BOOL un_paragraphe;
  BOOL exist_init;
  BOOL exist_init_var_metro;
  LONG spec_metronome;
  LONG *ads_spec_metronome;
  GEO_CHRONO val_geo;
  PGEO_CHRONO	donnees_geo;
  ENTREE_SORTIE es;
  DWORD numero_repere;
  DWORD pos_paragraf;
  DWORD profondeur;
  ID_PARAGRAPHE tipe_paragraf;
  PTITRE_PARAGRAPHE titre_paragraf;
  UL ul;
  BOOL val_log;
  BOOL un_log;
  BOOL un_num;
  FLOAT val_num;
    
  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;

  switch (action)
    {
    case MODIFIE_LIGNE  :
      consulte_titre_paragraf_cn (ligne);
      if (valide_vect_ul(vect_ul, (*nb_ul), ligne, MODIFIE_LIGNE,
                         &profondeur, &tipe_paragraf, &numero_repere,
                         &(es.i_identif), &pos_init, &exist_init,
                         &spec_metronome,
                         &erreur))
        { // vect_ul Ok
        donnees_geo = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono, ligne);
        val_geo = (*donnees_geo);
        if (val_geo.numero_repere == numero_repere)
          {
          switch (numero_repere)
            {
            case b_metronome :
              es.genre = c_res_logique;
              es.sens = c_res_e;
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.pos_es))
                {
                ads_spec_metronome = (PLONG)pointe_enr (szVERIFSource, __LINE__, b_metronome, val_geo.pos_specif_es);
                (*ads_spec_metronome) = spec_metronome;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_e_s :
              es.genre = c_res_numerique;
              es.sens = c_res_e;
              if (!bModifieDeclarationES (&es, vect_ul[0].show, val_geo.pos_es))
                {
                erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
                }
              break;

            case b_pt_constant :
              modifie_cst_bd_c (vect_ul[0].show, &(val_geo.pos_donnees));
              break;

            case b_titre_paragraf_chrono :
              erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
              break;

            default :
              break;
            } // switch
          } // meme repere
        else
          {
          erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
          }
        } // vect_ul Ok
      if (erreur != 0 )
         {
        genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
        (*erreur_val) = erreur;
         } // erreur vect_ul
       else
         {
         donnees_geo = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono, ligne);
         (*donnees_geo) = val_geo;
         }
       (*accepte_ds_bd) = TRUE;
       (*supprime_ligne)=FALSE;
      break;

    case INSERE_LIGNE :
      consulte_titre_paragraf_cn ((DWORD)(ligne - 1));
      if (valide_vect_ul(vect_ul, (*nb_ul), ligne, INSERE_LIGNE,
                         &profondeur, &tipe_paragraf, &numero_repere,
                         &(es.i_identif), &pos_init, &exist_init,
                         &spec_metronome,
                         &erreur))
        {
        pos_paragraf = 0; // init pour mise a jour donnees paragraf
        val_geo.numero_repere = numero_repere;
        switch (numero_repere)
          {
          case b_pt_constant :
            AjouteConstanteBd (pos_init, exist_init, vect_ul[0].show, &(val_geo.pos_donnees));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_metronome:
            if (!existe_repere (b_metronome))
              {
              cree_bloc (b_metronome, sizeof (spec_metronome), 0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_metronome) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_metronome, val_geo.pos_specif_es);
            ads_spec_metronome = (PLONG)pointe_enr (szVERIFSource, __LINE__, b_metronome, val_geo.pos_specif_es);
            (*ads_spec_metronome) = spec_metronome;
            es.genre = c_res_logique;
            es.sens = c_res_e;
            es.n_desc = d_chrono;
	    es.taille = 0;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.pos_es));
            bMotReserve (c_res_zero, ul.show);
            ul.genre = g_id;
            bReconnaitConstante (&ul, &val_log, &val_num,
                             &un_log, &un_num,
                             &exist_init_var_metro, &pos_init_var_metro);
            AjouteConstanteBd (pos_init_var_metro, exist_init_var_metro,
                             ul.show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_e_s :
            es.genre = c_res_numerique;
            es.sens = c_res_e;
            es.n_desc = d_chrono;
	    es.taille = 0;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.pos_es));
            StrCopy (ul.show, "0");
            ul.genre = g_num;
            bReconnaitConstante (&ul, &val_log, &val_num,
                             &un_log, &un_num,
                             &exist_init_var_metro, &pos_init_var_metro);
            AjouteConstanteBd (pos_init_var_metro, exist_init_var_metro,
                             ul.show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_titre_paragraf_chrono :
            InsereTitreParagraphe (b_titre_paragraf_chrono, ligne, profondeur,
                                     tipe_paragraf, &(val_geo.pos_paragraf));
            pos_paragraf = val_geo.pos_paragraf;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;
           
          default :
            break;

          } // switch
        if (!existe_repere (b_geo_chrono))
          {
          cree_bloc (b_geo_chrono, sizeof (val_geo), 0);
          }
        insere_enr (szVERIFSource, __LINE__, 1, b_geo_chrono, ligne);
        donnees_geo = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono, ligne);
        (*donnees_geo) = val_geo;
        un_paragraphe = (BOOL)(numero_repere == b_titre_paragraf_chrono);
        MajDonneesParagraphe (b_titre_paragraf_chrono, ligne, INSERE_LIGNE,
                                un_paragraphe, profondeur, pos_paragraf);
        } // vect_ul Ok
      else
        { // erreur vect_ul
        (*accepte_ds_bd) = FALSE;
        (*erreur_val) = erreur;
        } // erreur vect_ul
      break;

    case SUPPRIME_LIGNE :
      consulte_titre_paragraf_cn (ligne);
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      donnees_geo = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono, ligne);
      val_geo = (*donnees_geo);
      switch (val_geo.numero_repere)
        {
        case b_pt_constant:
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;

        case b_metronome :
          if (!bSupprimeES (val_geo.pos_es))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.pos_initial);
            enleve_enr (szVERIFSource, __LINE__, 1, val_geo.numero_repere, val_geo.pos_specif_es);
            maj_pointeur_dans_geo (val_geo.numero_repere, val_geo.pos_specif_es);
            }
          break;

        case b_e_s :
          if (!bSupprimeES (val_geo.pos_es))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.pos_initial);
            }
          break;

        case b_titre_paragraf_chrono :
          un_paragraphe = TRUE;
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_chrono, val_geo.pos_paragraf);
          tipe_paragraf = titre_paragraf->IdParagraphe;
          profondeur = titre_paragraf->profondeur;
          if (!bSupprimeTitreParagraphe (b_titre_paragraf_chrono, val_geo.pos_paragraf))
            {
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            (*erreur_val) = IDSERR_SUPPRESSION_DU_PARAGRAPHE_IMPOSSIBLE_A_CETTE_POSITION;
            }
          else
            {
            maj_pointeur_dans_geo (b_titre_paragraf_chrono, val_geo.pos_paragraf);
            }
          break;

        default:
          break;
        } // switch
      if (*supprime_ligne)
        {
        MajDonneesParagraphe (b_titre_paragraf_chrono, ligne, SUPPRIME_LIGNE,
					un_paragraphe, profondeur, 0); // pos_paragraf);
        enleve_enr (szVERIFSource, __LINE__, 1, b_geo_chrono, ligne);
        }
      break;

    case CONSULTE_LIGNE:
      genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
      break;

    default:
      break;
    } // switch
  } // valide_ligne

// ---------------------------------------------------------------------------
static void  creer_chrono (void)
  {
  chrono_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
static void  init_chrono (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  if (!chrono_deja_initialise)
    {
    ligne_depart_chrono = 1;
    ligne_courante_chrono = ligne_depart_chrono;
    chrono_deja_initialise = TRUE;
    }
  (*depart) = ligne_depart_chrono;
  (*courant) = ligne_courante_chrono;
  }

// ---------------------------------------------------------------------------
static DWORD  lis_nombre_de_ligne_chrono (void)
  {
  DWORD wretour;

  if (existe_repere (b_geo_chrono))
    {
    wretour = nb_enregistrements (szVERIFSource, __LINE__, b_geo_chrono);
    }
  else
    {
    wretour = 0;
    }

  return (wretour);
  }

// ---------------------------------------------------------------------------
static void  set_tdb_chrono (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_res[c_nb_car_message_res];
  char chaine_mot [10];

  StrSetNull (tdb);
  StrSetNull (chaine_mot);
  if (existe_repere (b_titre_paragraf_chrono) &&
     nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf_chrono) != 0)
    {
    if (nouvelle_ligne)
      {
      ligne--;
      }
    consulte_titre_paragraf_cn (ligne);
    if (contexte_cn != 0)
      {
      bMotReserve (contexte_cn, mot_res);
      StrConcat (tdb, mot_res);
      }
    }
  }

// ---------------------------------------------------------------------------
static void  fin_chrono (BOOL quitte, DWORD courant)
  {
 if (!quitte)
    {
    ligne_courante_chrono = courant;
    }
  }

// ---------------------------------------------------------------------------
// Interface du descripteur chronomètres et métronomes
void LisDescripteurTp (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_chrono;
	pDescripteurCf->pfnInitDesc = init_chrono;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_chrono;
	pDescripteurCf->pfnValideLigne = valide_ligne_chrono;
	pDescripteurCf->pfnSetTdb = set_tdb_chrono;
	pDescripteurCf->pfnLisParamDlg = LIS_PARAM_DLG_TP;
	pDescripteurCf->pfnFin = fin_chrono;
	}

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------*/

// Import export synoptiques et symboles
// 7/7/97 Win32

#define  IMPORT_SYNOP_REMPLACEMENT 1
#define  IMPORT_SYNOP_AJOUT        2

BOOL bExporteSynoptique (PCSTR pszNomFic, BOOL bAvecIdentifiant);
DWORD dwImporteSynoptique (PCSTR pszNomFic, DWORD *n_ligne, DWORD action, DWORD offset_ligne_anim, BOOL bAvecIdentifiant);

BOOL bExporteSymbole (PCSTR pszNomFic, DWORD page, BOOL bAvecIdentifiant);
DWORD dwImporteSymbole (PCSTR pszNomFic, HBDGR hbdgr, DWORD *n_ligne, BOOL bAvecIdentifiant);

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgco.h						|
 |   Auteur  :	LM							|
 |   Date    :	01/02/92						|
 +----------------------------------------------------------------------*/

void    LIS_PARAM_DLG_CO (int index_boite, UINT *iddlg, FARPROC * adr_winproc);

// ------------------------------ WFondBar.C --------------------------
//      Gestion des barres d'outils de PcsConf
// ---------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "appli.h"
#include "WBarOuti.h"
#include "UEnv.h"
#include "Verif.h"

#include "WFondBar.h"

VerifInit;

// ---------------------------- Types local pour chaque fenetre ---
typedef struct
  {
  HBO			hmnu_bmp;
  int			RelativePos;
  DWORD   XPos;
  DWORD   YPos;
  }
  ENV_FOND_BARRE_OUTIL, *PENV_FOND_BARRE_OUTIL;

// -----------------------------------------------------
static char szClasseFondBarreOutil[] = "ClasseFondBarreOutil";

// ---------------------------------------------------------------------------------------
// Cr�ation d'une barre outil fille de hwnd telle que d�crite dans *pDescriptionBO
// ---------------------------------------------------------------------------------------
static HBO creation_bmp_mnu (HWND hwnd, PDESCRIPTION_BARRE_OUTIL pDescriptionBO)
  {
  HBO		hboCree;
  DWORD nBouton;

	// cr�ation de l'objet barre d'outil
  hboCree = hBOCree 
		(NULL, // pszTitre
		pDescriptionBO->bAvecBordure,FALSE, FALSE, // cadre, deplacement,fermeture
		pDescriptionBO->XSpaceBetweenItemBmp, pDescriptionBO->YSpaceBetweenItemBmp);// cx_bord, cy_bord

  if (hboCree)
    {
    for (nBouton = 0; nBouton < pDescriptionBO->NbBoutons; nBouton++)
      {
      VerifWarning (bBOAjouteBouton 
				(hboCree,
				pDescriptionBO->ItemBmpMnu [nBouton].FileName,
				pDescriptionBO->ItemBmpMnu [nBouton].PositionInMenu,
				pDescriptionBO->ItemBmpMnu [nBouton].Mode,
				pDescriptionBO->ItemBmpMnu [nBouton].Id));
      }

    if (pDescriptionBO->RelativePositionBmpMnu)
      bBOOuvre (hboCree,FALSE,hwnd, 0, 0,pDescriptionBO->RelativePositionBmpMnu, TRUE);
    else
      bBOOuvre (hboCree,FALSE,hwnd, pDescriptionBO->xOrigine,pDescriptionBO->yOrigine,0, TRUE);
    //BOMontre (hboCree, TRUE);
    }

  return hboCree;
  }

// -----------------------------------------------------------------
//      WndProc de la fen�tre FondBarreOutil - fond des barres d'outil
// ---------------------------------------------------------------
static LRESULT CALLBACK wndprocFondBarreOutil (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT           mres = 0;             // Valeur de retour
  BOOL              bTraite = TRUE;       // Indique commande trait�e
	PAINTSTRUCT				ps;
  HDC               hdc;
  RECT             rect;
  PENV_FOND_BARRE_OUTIL pEnvBarreOutil;
  PDESCRIPTION_BARRE_OUTIL pDescriptionBO;

  switch (msg)
    {
    case WM_CREATE:
			pEnvBarreOutil = (PENV_FOND_BARRE_OUTIL)pCreeEnv (hwnd, sizeof (ENV_FOND_BARRE_OUTIL));
			pDescriptionBO = (PDESCRIPTION_BARRE_OUTIL)((LPCREATESTRUCT)mp2)->lpCreateParams;

			pEnvBarreOutil->hmnu_bmp    = creation_bmp_mnu (hwnd, pDescriptionBO);
			pEnvBarreOutil->XPos        = pDescriptionBO->xOrigine;
			pEnvBarreOutil->YPos        = pDescriptionBO->yOrigine;
			pEnvBarreOutil->RelativePos = pDescriptionBO->RelativePositionBmpMnu;
			if (!pEnvBarreOutil->hmnu_bmp)
				{
				bLibereEnv (hwnd);
				mres = -1;  // Pour arreter la creation
				}
			break;

     case WM_DESTROY:
			pEnvBarreOutil = (PENV_FOND_BARRE_OUTIL)pGetEnv (hwnd);
			BODetruit (&pEnvBarreOutil->hmnu_bmp);
			bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
			::SendMessage (::GetParent(hwnd), msg, mp1, mp2);//$$WinQueryWindow (hwnd, QW_OWNER, FALSE)
			break;

    case WM_PAINT:
			pEnvBarreOutil = (PENV_FOND_BARRE_OUTIL)pGetEnv (hwnd);
			hdc = ::BeginPaint (hwnd, &ps);
			::GetClientRect (hwnd, &rect);
			DrawEdge (hdc, &rect, EDGE_ETCHED, BF_RECT);
			::EndPaint (hwnd, &ps);
			mres = 0;
			break;

    case WM_SIZE:
			pEnvBarreOutil = (PENV_FOND_BARRE_OUTIL)pGetEnv (hwnd);
			if (mp1 != SIZE_MINIMIZED)
				{
				BODeplace (pEnvBarreOutil->hmnu_bmp,
					pEnvBarreOutil->XPos,
					pEnvBarreOutil->YPos,
					pEnvBarreOutil->RelativePos);
				}
			mres = 0;
			break;

    default:
			bTraite = FALSE;
			break;
    }

  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  } // wndprocFondBarreOutil

// ---------------------------- hwndCreeFondBarreOutil --------------------
//      Cr�ation et initialisation de la fen�tre FondBarreOutil
// ---------------------------------------------------------------
HWND hwndCreeFondBarreOutil 
	(HWND hwndParent, int x, int y, int cx, int cy,
	UINT id, PDESCRIPTION_BARRE_OUTIL pDescriptionBO)
  {
	static BOOL bClasseEnregistree = FALSE;
  HWND hwnd = NULL;

  // ------------------------------------------------------------
  // D�claration de la classe de la fen�tre Support_Menu_Bit_Map
  // ------------------------------------------------------------
  if (!bClasseEnregistree)
    {
		WNDCLASS wc;

		wc.style					= CS_HREDRAW | CS_VREDRAW; 
    wc.lpfnWndProc		= wndprocFondBarreOutil; 
    wc.cbClsExtra			= 0; 
    wc.cbWndExtra			= 0; 
    wc.hInstance			= Appli.hinst; 
    wc.hIcon					= NULL;
    wc.hCursor				= LoadCursor (NULL, IDC_ARROW);
    wc.hbrBackground	= (HBRUSH)(COLOR_MENU+1);
    wc.lpszMenuName		= NULL;
    wc.lpszClassName	= szClasseFondBarreOutil; 

    bClasseEnregistree = (BOOL) RegisterClass (&wc);
    }

  // Cr�ation de la fen�tre barre d'outil
  if (bClasseEnregistree)
    {
    hwnd = CreateWindow 
			(
			szClasseFondBarreOutil, NULL,
			WS_VISIBLE|WS_CHILD|WS_CLIPCHILDREN,
			x, y, cx, cy,	hwndParent,
			(HMENU)id, Appli.hinst, (PVOID) pDescriptionBO
			);
    }

  return (hwnd);
  }


// ---------------------------- FermeFondBarreOutil --------------------
//      Finalisation de la fen�tre SprtBmpMnu
// ----------------------------------------------------------------
void FermeFondBarreOutil (HWND hwndBarreOutil)
  {
  ::DestroyWindow (hwndBarreOutil);
  }



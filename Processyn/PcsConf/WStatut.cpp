// ------------------------------- PM - LnTxt.C ------------------
//      Gestion des fen�tres LnTxt (statut de l'�diteur)
// ---------------------------------------------------------------

#include "stdafx.h"
#include "appli.h"
//#include "couleurs.h"

#include "std.h"
#include "UStr.h"
#include "MemMan.h"
#include "UEnv.h"
#include "WStatut.h"

// ------------------------------------------------- Constantes
static char szClasseStatut [] = "LigneStatut";

// ---------------------------- Types locaux ----------------------
typedef struct
  {
  char *LineText;
  } ENV_STATUT, *PENV_STATUT;

// ---------------------------- Fonction locale -------------------
// -------------------------- wndprocStatut --------------------
//      WndProc de la fen�tre d'affichage du statut
// ------------------------------------------------------------
static LRESULT CALLBACK wndprocStatut (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
  LRESULT      lres = 0;             // Valeur de retour
  ENV_STATUT * pEnvStatut;

  switch (msg)
    {
    case WM_CREATE:
			pEnvStatut = (PENV_STATUT)pCreeEnv (hwnd, sizeof (ENV_STATUT));
			pEnvStatut->LineText = (char *) pMemAlloue (sizeof (char));
			StrSetNull (pEnvStatut->LineText);
			break;

    case WM_SETTEXT:
			pEnvStatut = (PENV_STATUT)pGetEnv (hwnd);
			MemRealloue ((PVOID *)&pEnvStatut->LineText, StrLength ((PSTR)lparam) + 1);
			StrCopy (pEnvStatut->LineText, (PSTR)lparam);
			::InvalidateRect (hwnd, NULL, TRUE);
			lres = (LRESULT) TRUE;
			break;

    case WM_PAINT:
			{
			HDC             hdc;
			PAINTSTRUCT			ps;
			RECT            rect;
			HFONT						hfAnsii,hfAncien;

			// Dessin de la barre de statut
			pEnvStatut = (PENV_STATUT)pGetEnv (hwnd);
			hdc = ::BeginPaint (hwnd, &ps);

			// mesure de la zone cliente
			::GetClientRect (hwnd, &rect);

			// cadre autour
			DrawEdge (hdc, &rect, EDGE_ETCHED, BF_RECT);

			// restriction de la zone de trac� � l'int�rieur du  cadre
			InflateRect(&rect, -2, -2);

			// Dessin du texte avec fond transparent
			SetBkMode (hdc, TRANSPARENT);
			hfAnsii =  (HFONT)GetStockObject(ANSI_VAR_FONT);
			hfAncien = (HFONT)SelectObject (hdc, hfAnsii);
			DrawText (hdc, pEnvStatut->LineText, -1, &rect,
				DT_LEFT | DT_VCENTER | DT_SINGLELINE | DT_NOPREFIX);
			SelectObject (hdc, hfAncien);

			// fin dessin
			::EndPaint (hwnd, &ps);
			}
			break;

    case WM_DESTROY:
			pEnvStatut = (PENV_STATUT)pGetEnv (hwnd);
			MemLibere ((PVOID *)(&pEnvStatut->LineText));
			bLibereEnv (hwnd);
			break;

    default:
			lres = DefWindowProc (hwnd, msg, wparam, lparam);
			break;
    }

  return lres;
  } // wndprocStatut


// ---------------------------- LnTxtInit ------------
//      Cr�ation et initialisation de la fen�tre LnTxt
// ---------------------------------------------------
HWND hwndStatutInit (HWND      hwndParent,
                int     x,
                int     y,
                int     cx,
                int     cy,
                UINT    id
               )
  {
	static BOOL bClasseEnregistree = FALSE;
  HWND hwnd = NULL;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		// --------------------------------------------
		// D�claration de la classe de la fen�tre LnTxt
		// --------------------------------------------
		wc.style					= CS_HREDRAW | CS_VREDRAW; 
		wc.lpfnWndProc		= wndprocStatut; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= (HBRUSH)(COLOR_MENU+1);
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseStatut; 

		bClasseEnregistree = RegisterClass (&wc);
		}

  // ----------------------------
  // Cr�ation de la fen�tre Statut
  // ----------------------------
	if (bClasseEnregistree)
		{
    hwnd = CreateWindow (
			szClasseStatut,
			(PSTR) NULL,
			WS_VISIBLE | WS_CHILD,
			x, y, cx, cy,
			hwndParent,
			(HMENU)id,
			Appli.hinst, NULL);
		}

  return (hwnd);
  }


// ---------------------------- LnTxtFini --------------------
//      Finalisation de la fen�tre LnTxt
// -----------------------------------------------------------
void StatutDestroy (HWND hwndStatut)
  {
  ::DestroyWindow (hwndStatut);
  }



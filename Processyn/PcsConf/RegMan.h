// RegMan.h
// Classe de Gestion de la base de registres
// JS WIN32 12/2007

#if !defined(AFX_TEMPAPPLI_H__4982EE24_38DD_11D2_A131_0000E8D90705__INCLUDED_)
#define AFX_TEMPAPPLI_H__4982EE24_38DD_11D2_A131_0000E8D90705__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CRegMan  
	{
	public:
	// constructeur / destructeur
		CRegMan();
		virtual ~CRegMan();

	// m�thodes
	public:
		// Lecture de la valeur de la cl� String pszNomCle dans HKEY_CURRENT_USER\pszPath.
		// Si la cl� n'existe pas ou n'est pas lisible, pszValeurCle vaut pszValDefaut et renvoie FALSE
		// Si la cl� existe, sa valeur est lue dans pszValeurCle et renvoie TRUE
		BOOL bUserLireString (PSTR pszValeurCle, DWORD dwTailleMaxValeuCle, PCSTR pszPathCle, PCSTR pszNomCle, PCSTR pszValDefaut);

		// Lecture de la valeur de la cl� String pszNomCle dans HKEY_CURRENT_USER\pszPath.
		// Si la cl� n'existe pas ou n'est pas lisible, dwValeurCle vaut dwValDefaut et renvoie FALSE
		// Si la cl� existe, sa valeur est lue dans dwValeurCle et renvoie TRUE
		BOOL bUserLireDWORD (DWORD & dwValeurCle, PCSTR pszPathCle, PCSTR pszNomCle, DWORD dwValDefaut);
		//Idem ci-dessus pour LONG
		BOOL bUserLireLONG (LONG & lValeurCle, PCSTR pszPathCle, PCSTR pszNomCle, LONG lValDefaut);
		//Idem ci-dessus pour BOOL
		BOOL bUserLireBOOL (BOOL & bValeurCle, PCSTR pszPathCle, PCSTR pszNomCle, BOOL bValDefaut);

		// Ecriture de la valeur pszValeurCle de la cl� String pszNomCle dans HKEY_CURRENT_USER\pszPath.
		// Renvoie TRUE Si la cl� a �t� �crite, FALSE sinon 
		BOOL bUserEcrireString (PCSTR pszValeurCle, PCSTR pszPathCle, PCSTR pszNomCle);

		// Ecriture de la valeur dwValeurCle de la cl� DWORD pszNomCle dans HKEY_CURRENT_USER\pszPath.
		// Renvoie TRUE Si la cl� a �t� �crite, FALSE sinon 
		BOOL bUserEcrireDWORD (DWORD dwValeurCle, PCSTR pszPathCle, PCSTR pszNomCle);
		//Idem ci-dessus pour LONG
		BOOL bUserEcrireLONG (LONG lValeurCle, PCSTR pszPathCle, PCSTR pszNomCle);
		//Idem ci-dessus pour BOOL
		BOOL bUserEcrireBOOL (BOOL bValeurCle, PCSTR pszPathCle, PCSTR pszNomCle);
	private:
		// Ouvre ou cr�e un chemin compos� d'1 � N sous-cl�s
		BOOL bOuvreOuCreeChemin (HKEY hKeyOuverte, PCSTR pszPath, PHKEY phKey);
	};

#endif // !defined(AFX_TEMPAPPLI_H__4982EE24_38DD_11D2_A131_0000E8D90705__INCLUDED_)

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_me.c                                               |
 |   Auteur  : AC					        	|
 |   Date    : 29/12/93					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include <stdlib.h>

#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "cvt_ul.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "tipe_me.h"
#include "pcsdlgme.h"
#include "IdLngLng.h"
#include "inter_me.h"
#include "Verif.h"
VerifInit;

//-------------------------- Variables ---------------------
static DWORD ligne_depart_memoire;
static DWORD ligne_courante_memoire;
static BOOL memoire_deja_initialise;


/*--------------------------------------------------------------------------*/
void static traitement_num (PUL ul, DWORD num)
  {
  StrDWordToStr (ul->show, num);
  ul->erreur = 0;
  }

//------------------------------------------------------------------------
static void genere_vect_ul (DWORD ligne, PUL vect_ul, int *nb_ul)
  {
  PGEO_MEM	ptr_geo = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem, ligne);

  switch (ptr_geo->type_memoire)
    {
    case mem_interne_simple:
      (*nb_ul) = 2;
      init_vect_ul (vect_ul, 2);
      NomVarES (vect_ul[0].show, ptr_geo->position_es);
      consulte_cst_bd_c (ptr_geo->position_constante,vect_ul[1].show);
      break;

    case mem_interne_tableau:
      (*nb_ul) = 3;
      init_vect_ul (vect_ul, 3);
      NomVarES (vect_ul[0].show, ptr_geo->position_es);
      traitement_num  (&(vect_ul[1]),ptr_geo->taille);
      consulte_cst_bd_c (ptr_geo->position_constante,vect_ul[2].show);
      break;

    case mem_commentaire:
      (*nb_ul) = 1;
      vect_ul[0].erreur = 0;
      vect_ul[0].show [0] = '\0';
      consulte_cst_bd_c (ptr_geo->position_constante,vect_ul[0].show);
      break;

    default:
      break;

    } // switch
  }

//-------------------------------------------------------------------------
static BOOL valide_vect_ul (PUL vect_ul, REQUETE_EDIT action, DWORD ligne,
                               PENTREE_SORTIE es, int *nb_ul,
                               DWORD *pos_init, DWORD *erreur,
                               DWORD *type_memoire, DWORD *taille,
                               BOOL *exist_init)

	{
	DWORD pos_va_init;
	DWORD pos_ident;
	BOOL exist_ident;
	BOOL un_log;
	BOOL un_num;
	BOOL val_log;
	FLOAT val_num;
	PGEO_MEM	ptr_geo;


	//***********************************************
#define err(code) (*erreur) = code;return (FALSE)  //Sale Sioux
	//***********************************************

	(*erreur) = 0;
	if ((*nb_ul) < 1)
		{
		err (IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
		}
	if ((*nb_ul) > 3)
		{
		err (31);
		}
	if ((*nb_ul) == 1)
		{
		if (!bReconnaitConstante ((vect_ul+0), &val_log, &val_num,
			&un_log, &un_num, exist_init, pos_init))
			{
			err (53);
			}
		if ((un_log) || (un_num))
			{
			err (53);
			}
		(*type_memoire) = mem_commentaire;
		} // commentaire
	else
		{
		if (StrLength (vect_ul[0].show) > 20)
			{
			err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
			}
		if (!bReconnaitESValide ((vect_ul+0), &exist_ident, &pos_ident))
			{
			err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
			}

		switch (*nb_ul)
			{
			case 2 :
				(*type_memoire) = mem_interne_simple;
				es->sens = c_res_e_et_s;
				pos_va_init = 1;
				break;
			case 3 :
				(*type_memoire) = mem_interne_tableau;
				es->sens = c_res_variable_ta_es;
				pos_va_init = 2;
				break;
			} // switch

		if ((*type_memoire) == mem_interne_tableau)
			{
			if (!bReconnaitConstante ((vect_ul+1), &val_log, &val_num,
				&un_log, &un_num, exist_init, pos_init))
				{
				err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
				}

			if (un_num)
				{
				(*taille) = (DWORD)(val_num);
				if (((*taille) < 1) || ((*taille) > NB_MAX_ELEMENTS_VAR_TABLEAU))
					{
					err(IDSERR_LA_TAILLE_DU_TABLEAU_EST_COMPRISE_ENTRE_1_ET_10000);
					}
				}
			else
				{
				err(IDSERR_LA_TAILLE_DU_TABLEAU_EST_COMPRISE_ENTRE_1_ET_10000);
				}
			}

		if (action != MODIFIE_LIGNE)
			{
			if (exist_ident)
				{
				err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
				}
			}
		else
			{
			ptr_geo = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem, ligne);
			if ((ptr_geo->type_memoire == mem_interne_simple) ||
				(ptr_geo->type_memoire == mem_interne_tableau))
				{
				if (ptr_geo->type_memoire == (*type_memoire))
					{
					if ((exist_ident) && (!bStrEgaleANomVarES (vect_ul[0].show, ptr_geo->position_es)))
						{
						err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
						}
					}
				else
					{
					err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
					}
				}
			else
				{
				if (exist_ident)
					{
					err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
					}
				}
			}
		if (!bReconnaitConstante ((vect_ul+pos_va_init), &val_log, &val_num,
			&un_log, &un_num, exist_init, pos_init))
			{
			err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
			}
		if (un_log)
			{
			es->genre = c_res_logique;
			}
		else
			{
			if (un_num)
				{
				es->genre = c_res_numerique;
				}
			else
				{
				es->genre = c_res_message;
				}
			}
		es->i_identif = pos_ident;
		}
	return (TRUE);
	}

//---------------------------------------------------------------------
void  valide_ligne_memoire (REQUETE_EDIT action, DWORD ligne,
                                  PUL vect_ul, int *nb_ul,
                                  BOOL *supprime_ligne, BOOL *accepte_ds_bd,
                                  DWORD *erreur_val, int *niveau, int *indentation)
  {
  DWORD erreur;
  ENTREE_SORTIE es;
  DWORD pos_init;
  DWORD type_memoire;
  DWORD taille;
  BOOL exist_init;
  GEO_MEM val_geo;
  PGEO_MEM	ptr_val_geo;

  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;
  switch (action)
    {
    case MODIFIE_LIGNE :
      if (valide_vect_ul (vect_ul, MODIFIE_LIGNE, ligne, &es, nb_ul, &pos_init,
                          &erreur, &type_memoire, &taille, &exist_init))
        {
        ptr_val_geo = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem, ligne);
        val_geo = (*ptr_val_geo);
        if (val_geo.type_memoire == type_memoire)
          {
          switch (type_memoire)
            {
            case mem_interne_simple :
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.position_es))
                {
                modifie_cst_bd_c (vect_ul[1].show, &(val_geo.position_constante));
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                genere_vect_ul (ligne, vect_ul, nb_ul);
                (*erreur_val) = erreur;
                }
              break;

            case mem_interne_tableau :
              es.taille = taille;
              if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.position_es))
                {
                modifie_cst_bd_c (vect_ul[2].show, &(val_geo.position_constante));
                val_geo.taille = taille;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                genere_vect_ul (ligne, vect_ul, nb_ul);
                (*erreur_val) = erreur;
                }
              break;

            case mem_commentaire :
              modifie_cst_bd_c (vect_ul[0].show, &(val_geo.position_constante));
              break;

            default:
              break;

            } // switch type_memoire
          ptr_val_geo = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem, ligne);
          (*ptr_val_geo) = val_geo;
          }
        else
          {
          genere_vect_ul (ligne, vect_ul, nb_ul);
          (*erreur_val) = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
          }
        } // vect_ul Ok
      else
        {
        genere_vect_ul (ligne, vect_ul, nb_ul);
        (*erreur_val) = erreur;
        }
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = FALSE;
      break;

    case INSERE_LIGNE :
      if (valide_vect_ul (vect_ul, INSERE_LIGNE, ligne, &es, nb_ul, &pos_init,
                          &erreur, &(val_geo.type_memoire), &(val_geo.taille), &exist_init))
        {
        switch (val_geo.type_memoire)
          {
          case mem_commentaire :
            AjouteConstanteBd (pos_init, exist_init, vect_ul[0].show, &(val_geo.position_constante));
            break;

          case mem_interne_simple:
            es.n_desc = d_mem;
	    es.taille = 0;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.position_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[1].show, &(val_geo.position_constante));
            break;

          case mem_interne_tableau :
            es.n_desc = d_mem;
	    es.taille = val_geo.taille;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.position_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[2].show, &(val_geo.position_constante));
            break;

          default:
            break;

          } // switch
        if (!existe_repere (b_geo_mem))
          {
          cree_bloc (b_geo_mem, sizeof (GEO_MEM), 0);
          }
        insere_enr (szVERIFSource, __LINE__, 1, b_geo_mem, ligne);
        ptr_val_geo = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem, ligne);
        (*ptr_val_geo) = val_geo;
        (*accepte_ds_bd) = TRUE;
        (*supprime_ligne) = FALSE;
        } // vect_ul Ok
      else
        {
        (*accepte_ds_bd) = FALSE;
        (*erreur_val) = erreur;
        } // erreur vect_ul
      break; // INSERE_LIGNE

    case SUPPRIME_LIGNE :
      ptr_val_geo = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem,ligne);
      val_geo = (*ptr_val_geo);
      if ((val_geo.type_memoire == mem_interne_simple) ||
	  (val_geo.type_memoire == mem_interne_tableau))
        {
        if (bSupprimeES (val_geo.position_es))
          {
          supprime_cst_bd_c (val_geo.position_constante);
          enleve_enr (szVERIFSource, __LINE__, 1, b_geo_mem, ligne);
          (*accepte_ds_bd) = TRUE;
          (*supprime_ligne) = TRUE;
          } // suppression possible
        else
          {
          genere_vect_ul (ligne, vect_ul, nb_ul);
          (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
          (*accepte_ds_bd) = TRUE;
          (*supprime_ligne) = FALSE;
          }
        } // description
      else
        {
        supprime_cst_bd_c (val_geo.position_constante);
        enleve_enr (szVERIFSource, __LINE__, 1, b_geo_mem, ligne);
        (*accepte_ds_bd) = TRUE;
        (*supprime_ligne)= TRUE;
        }
      break;

    case CONSULTE_LIGNE :
      genere_vect_ul (ligne, vect_ul, nb_ul);
      break;

    default:
      break;

    } // switch action
  } // valide_ligne

// ---------------------------------------------------------------------------
void  creer_memoire (void)
  {
  memoire_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
void  init_memoire (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  if (!memoire_deja_initialise)
    {
    ligne_depart_memoire = 1;
    ligne_courante_memoire = ligne_depart_memoire;
    memoire_deja_initialise = TRUE;
    }
  (*depart) = ligne_depart_memoire;
  (*courant) = ligne_courante_memoire;
  }

// ---------------------------------------------------------------------------
DWORD  lis_nombre_de_ligne_memoire (void)
  {
  DWORD nb_ligne;

  if (existe_repere (b_geo_mem))
    {
    nb_ligne = nb_enregistrements (szVERIFSource, __LINE__, b_geo_mem);
    }
  else
    {
    nb_ligne = 0;
    }
  return (nb_ligne);
  }

// ---------------------------------------------------------------------------
void  set_tdb_memoire (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  tdb[0] = '\0';
  }

// ---------------------------------------------------------------------------
void  fin_memoire (BOOL quitte, DWORD courant)
  {
  if (!quitte)
    {
    ligne_courante_memoire = courant;
    }
  }

// Interface du descripteur variables m�moire
void LisDescripteurMe (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_memoire;
	pDescripteurCf->pfnInitDesc = init_memoire;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_memoire;
	pDescripteurCf->pfnValideLigne = valide_ligne_memoire;
	pDescripteurCf->pfnSetTdb = set_tdb_memoire;
	pDescripteurCf->pfnLisParamDlg = LIS_PARAM_DLG_ME;
	pDescripteurCf->pfnFin = fin_memoire;
	}

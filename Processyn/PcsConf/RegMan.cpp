// RegMan.c
// Gestion de la base de registres
// Lecture/�criture de valeurs dans la base de registre
// JS WIN32 

#include "stdafx.h"
#include "UStr.h"
#include "RegMan.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRegMan::CRegMan()
	{
	}
CRegMan::~CRegMan()
	{
	}

//--------------------------------------------------------
// Ouvre ou cr�e un chemin compos� d'1 � N sous-cl�s
BOOL CRegMan::bOuvreOuCreeChemin (HKEY hKeyOuverte, PCSTR pszPath, PHKEY phKey)
	{
	BOOL	bOk = FALSE;
	BOOL	bCheminComplexe = FALSE;
	int		nNChar;
	DWORD dwAction;
	DWORD dwErr;
	HKEY	hKeyCreee;

	// Recherche si le path contient plusieurs sous-cl�
	for (nNChar = 0; nNChar < lstrlen (pszPath); nNChar++)
		{
		if (pszPath [nNChar] =='\\')
			{
			// Le path contient plusieurs sous-cl�s
			bCheminComplexe = TRUE;
			break;
			}
		}

	// Le path contient plusieurs sous-cl� ?
	if (bCheminComplexe)
		{
		// oui => cr�e ou ouvre une cl� sur la premi�re cl� trouv�e
		char szSousCle [MAX_PATH];

		// R�cup�re la sous cl�
		lstrcpy (szSousCle, pszPath);
		szSousCle [nNChar] = '\0';

		// Sous cl� ouverte ou cr��e ?
		dwErr = RegCreateKeyEx (hKeyOuverte, szSousCle, 0, NULL, 
			REG_OPTION_NON_VOLATILE, KEY_CREATE_SUB_KEY, NULL, &hKeyCreee, &dwAction);

		if (dwErr == NO_ERROR)
			{
			// oui => r�entre pour continuer la cr�ation de l'arborescence
			bOk = bOuvreOuCreeChemin (hKeyCreee, &pszPath[nNChar+1], phKey);
			RegCloseKey (hKeyCreee);
			}
		}
	else
		{
		// non => cr�e ou ouvre la cl� sp�cifi�e
		// Sous cl� ouverte ou cr��e ?
		dwErr = RegCreateKeyEx (hKeyOuverte, pszPath, 0, NULL, 
			REG_OPTION_NON_VOLATILE, KEY_SET_VALUE, NULL, &hKeyCreee, &dwAction);

		if (dwErr == NO_ERROR)
			{
			// oui => Ok, renvoie le handle de cl� ouvert
			bOk = TRUE;
			*phKey = hKeyCreee;
			}
		}

	return bOk;
	} // bOuvreOuCreeChemin

// Lecture de la valeur de la cl� String pszNomCle dans HKEY_CURRENT_USER\pszPath.
// Si la cl� n'existe pas ou n'est pas lisible, pszValeurCle vaut pszValDefaut et renvoie FALSE
// Si la cl� existe, sa valeur est lue dans pszValeurCle et renvoie TRUE
BOOL CRegMan::bUserLireString (PSTR pszValeurCle, DWORD dwTailleMaxValeurCle, PCSTR pszPathCle, PCSTR pszNomCle, PCSTR pszValDefaut)
	{
	BOOL bOk = FALSE;
	HKEY	hKey = NULL;

	// Essai de lecture de la valeur de la cl�
	if (ERROR_SUCCESS == RegOpenKeyEx (HKEY_CURRENT_USER,	pszPathCle, 0, KEY_QUERY_VALUE, &hKey))
		{
		DWORD dwTypeCle = 0;
		DWORD	dwErr = RegQueryValueEx (hKey, pszNomCle, NULL, &dwTypeCle, (LPBYTE)pszValeurCle, &dwTailleMaxValeurCle);

		// Lecture de la valeur de cl� Ok ?
		if ((dwErr == NO_ERROR) && (dwTypeCle == REG_SZ))
			{
			bOk = TRUE;
			}

		// Fermeture de la cl�
		RegCloseKey (hKey);
		}
	// Erreur de lecture de la valeur de la cl� ?
	if (!bOk)
		{
		// Oui = > renvoie la valeur par d�faut
		StrCopyUpToLength(pszValeurCle, pszValDefaut,dwTailleMaxValeurCle);
		}
	return bOk;
	}

// Ecriture de la valeur pszValeurCle de la cl� String pszNomCle dans HKEY_CURRENT_USER\pszPath.
// Renvoie TRUE Si la cl� a �t� �crite, FALSE sinon 
BOOL CRegMan::bUserEcrireString (PCSTR pszValeurCle, PCSTR pszPathCle, PCSTR pszNomCle)
	{
	BOOL bOk = FALSE;
	HKEY	hKey = NULL;

	// Lecture de la valeur de cl� Ok ?
	if (bOuvreOuCreeChemin (HKEY_CURRENT_USER, pszPathCle, &hKey))
		{
		// Enregistrement de la valeur de cl� Ok ?
		if (ERROR_SUCCESS == RegSetValueEx(
			hKey,	// handle of key to query 
			pszNomCle,	// address of name of value to query 
			0,	// reserved 
			REG_SZ,	// address of buffer for value type 
			(LPBYTE)pszValeurCle,	// address of data buffer 
			StrLength(pszValeurCle)+1 	// address of data buffer size 
		 ))
			{
			// oui => Ok
			bOk = TRUE;
			}

		// Fermeture de la cl�
		RegCloseKey (hKey);
		}
	return bOk;
	}

// Lecture de la valeur de la cl� String pszNomCle dans HKEY_CURRENT_USER\pszPath.
// Si la cl� n'existe pas ou n'est pas lisible, dwValeurCle vaut dwValDefaut et renvoie FALSE
// Si la cl� existe, sa valeur est lue dans dwValeurCle et renvoie TRUE
BOOL CRegMan::bUserLireDWORD (DWORD & dwValeurCle, PCSTR pszPathCle, PCSTR pszNomCle, DWORD dwValDefaut)
	{
	BOOL bOk = FALSE;
	HKEY	hKey = NULL;
	DWORD dwTailleMaxValeurCle = sizeof(dwValeurCle);

	// Essai de lecture de la valeur de la cl�
	if (ERROR_SUCCESS == RegOpenKeyEx (HKEY_CURRENT_USER,	pszPathCle, 0, KEY_QUERY_VALUE, &hKey))
		{
		DWORD dwTypeCle = 0;
		DWORD	dwErr = RegQueryValueEx (hKey, pszNomCle, NULL, &dwTypeCle, (LPBYTE)&dwValeurCle, &dwTailleMaxValeurCle);

		// Lecture de la valeur de cl� Ok ?
		if ((dwErr == NO_ERROR) && (dwTypeCle == REG_DWORD))
			{
			bOk = TRUE;
			}

		// Fermeture de la cl�
		RegCloseKey (hKey);
		}
	// Erreur de lecture de la valeur de la cl� ?
	if (!bOk)
		{
		// Oui = > renvoie la valeur par d�faut
		dwValeurCle = dwValDefaut;
		}
	return bOk;
	}

//Idem ci-dessus pour LONG
BOOL CRegMan::bUserLireLONG (LONG & lValeurCle, PCSTR pszPathCle, PCSTR pszNomCle, LONG lValDefaut)
	{
	DWORD dwTemp = (DWORD)lValeurCle;
	BOOL bRes = bUserLireDWORD (dwTemp, pszPathCle, pszNomCle, (DWORD)lValDefaut);
	lValeurCle = (LONG)dwTemp;
	return bRes;
	}

//Idem ci-dessus pour BOOL
BOOL CRegMan::bUserLireBOOL (BOOL & bValeurCle, PCSTR pszPathCle, PCSTR pszNomCle, BOOL bValDefaut)
	{
	DWORD dwTemp = (DWORD)bValeurCle;
	BOOL bRes = bUserLireDWORD (dwTemp, pszPathCle, pszNomCle, (DWORD)bValDefaut);
	bValeurCle = dwTemp != 0;
	return bRes;
	}

// Ecriture de la valeur dwValeurCle de la cl� DWORD pszNomCle dans HKEY_CURRENT_USER\pszPath.
// Renvoie TRUE Si la cl� a �t� �crite, FALSE sinon 
BOOL CRegMan::bUserEcrireDWORD (DWORD dwValeurCle, PCSTR pszPathCle, PCSTR pszNomCle)
	{
	BOOL bOk = FALSE;
	HKEY	hKey = NULL;

	// Lecture de la valeur de cl� Ok ?
	if (bOuvreOuCreeChemin (HKEY_CURRENT_USER, pszPathCle, &hKey))
		{
		// Enregistrement de la valeur de cl� Ok ?
		if (ERROR_SUCCESS == RegSetValueEx(
			hKey,	// handle of key to query 
			pszNomCle,	// address of name of value to query 
			0,	// reserved 
			REG_DWORD,	// address of buffer for value type 
			(LPBYTE)&dwValeurCle,	// address of data buffer 
			sizeof(dwValeurCle) 	// address of data buffer size 
		 ))
			{
			// oui => Ok
			bOk = TRUE;
			}

		// Fermeture de la cl�
		RegCloseKey (hKey);
		}
	return bOk;
	}

//Idem ci-dessus pour LONG
BOOL CRegMan::bUserEcrireLONG (LONG lValeurCle, PCSTR pszPathCle, PCSTR pszNomCle)
	{
	return bUserEcrireDWORD ((DWORD)lValeurCle, pszPathCle, pszNomCle);
	}
//Idem ci-dessus pour BOOL
BOOL CRegMan::bUserEcrireBOOL (BOOL bValeurCle, PCSTR pszPathCle, PCSTR pszNomCle)
	{
	return bUserEcrireDWORD ((BOOL)bValeurCle, pszPathCle, pszNomCle);
	}

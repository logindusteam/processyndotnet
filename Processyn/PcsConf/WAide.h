// initialisation du syst�me d'aide processyn
void InitAide (void);
void AjouteIndexAide 
	(
	char *mnemo, // Nom court de l'index d'aide
	char *titre		// Titre d'aide
	);

// affichage de l'aide
void MontreAide (void);

// cache l'aide
void CacheAide (void);

// Charge un fichier d'aide
void ChargeFichierAide 
	(
	PCSTR szNomFic,						// Nom du fichier
	BOOL bFichierUtilisateur	// TRUE : le fichier dans document; FALSE fichier avec EXEs
	);

// Vide le fichier d'aide couramment charg�
void VideAide (void);

// Fermeture et lib�ration de l'aide
void FermeAide (void);

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Initialisations par lecture du fichier texte PCS.INI	|
 |   Auteur  : LM							|
 |   Date    : 26/10/92 						|
 |   Version : 4.00							|
 |                                                                      |
 +----------------------------------------------------------------------*/

// --------------------------------------------------------------------------
// Objet: Interpreter le fichier de commande PCS.INI
// --------------------------------------------------------------------------
void InterpreteFichierIni (void);

BOOL definition_alarme 
	(DWORD *pwNbCarMaxMesAl,
	DWORD *pwNbCarMaxFicArch,
	DWORD *pwNbCarMaxLineImp);

#define NB_MAX_ALARM_TABS 20
extern LONG nNbAlarmTabsFormat; // Nombre de tabulations dans le format
extern LONG alPosXTab[NB_MAX_ALARM_TABS]; // Position en X (pixels) de chaque tabulation


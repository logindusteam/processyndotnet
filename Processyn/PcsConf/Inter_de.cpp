/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_de.c                                               |
 |   Auteur  : LM					        	|
 |   Date    : 02/02/94					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include <stdlib.h>

#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "tipe_de.h"
#include "IdLngLng.h"
#include "cvt_ul.h"
#include "inter_de.h"
#include "Verif.h"
VerifInit;

//-------------------------- Variables ---------------------
typedef struct
  {
  DWORD wParagraf;
  DWORD wSens;
  char pszLien[80];
  char pszApplic[80];
  char pszTopic[80];
  } t_contexte_de;

static t_contexte_de contexte_de;

static DWORD ligne_depart_dde;
static DWORD ligne_courante_dde;
static BOOL dde_deja_initialise;


/*-------------------------------------------------------------------------*/
/*                        GESTION CONTEXTE                                 */
/*-------------------------------------------------------------------------*/

static void consulte_titre_paragraf_de (DWORD ligne)
  {
  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_DDE	donnees_geo;
  t_spec_titre_paragraf_de *spec_titre_paragraf;
  CONTEXTE_PARAGRAPHE_LIGNE table_contexte_ligne[2];
  DWORD profondeur_ligne;
  DWORD i;

  if (bDonneContexteParagrapheLigne (b_titre_paragraf_de, ligne, &profondeur_ligne, table_contexte_ligne))
    { // donne_contexte_ok
    for (i = 0; (i < profondeur_ligne); i++)
      {
      switch (table_contexte_ligne [i].IdParagraphe)
        {
        case liens_predefinis_de:
          contexte_de.wParagraf    = liens_predefinis_de;
          contexte_de.wSens        = 0;
          StrSetNull (contexte_de.pszLien);
          StrSetNull (contexte_de.pszApplic);
          StrSetNull (contexte_de.pszTopic);
          break;

        case variables_importees_de:
          contexte_de.wParagraf    = variables_importees_de;
          contexte_de.wSens        = c_res_client;
          StrSetNull (contexte_de.pszLien);
          StrSetNull (contexte_de.pszApplic);
          StrSetNull (contexte_de.pszTopic);
          break;

        case variables_exportees_de:
          contexte_de.wParagraf    = variables_exportees_de;
          contexte_de.wSens        = c_res_serveur;
          StrSetNull (contexte_de.pszLien);
          StrSetNull (contexte_de.pszApplic);
          StrSetNull (contexte_de.pszTopic);
          break;

        case entree :
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_de, table_contexte_ligne [i].position);
          donnees_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, titre_paragraf->pos_geo_debut);
          spec_titre_paragraf = (t_spec_titre_paragraf_de *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, donnees_geo->wPosSpecifParagraf);
          //
          contexte_de.wSens = c_res_e;
          StrCopy (contexte_de.pszLien, spec_titre_paragraf->pszLien);
          StrCopy (contexte_de.pszApplic, spec_titre_paragraf->pszApplic);
          StrCopy (contexte_de.pszTopic, spec_titre_paragraf->pszTopic);
          break;

        case sortie :
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_de, table_contexte_ligne [i].position);
          donnees_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, titre_paragraf->pos_geo_debut);
          spec_titre_paragraf = (t_spec_titre_paragraf_de *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, donnees_geo->wPosSpecifParagraf);
          //
          contexte_de.wSens = c_res_reflet;
          StrCopy (contexte_de.pszLien, spec_titre_paragraf->pszLien);
          StrCopy (contexte_de.pszApplic, spec_titre_paragraf->pszApplic);
          StrCopy (contexte_de.pszTopic, spec_titre_paragraf->pszTopic);
          break;

        default :
          contexte_de.wParagraf    = 0;
          contexte_de.wSens        = 0;
          StrSetNull (contexte_de.pszLien);
          StrSetNull (contexte_de.pszApplic);
          StrSetNull (contexte_de.pszTopic);
          break;
        } // switch
      } // for i
    } // donne_contexte_ok
  else
    {
    contexte_de.wParagraf    = 0;
    contexte_de.wSens        = 0;
    StrSetNull (contexte_de.pszLien);
    StrSetNull (contexte_de.pszApplic);
    StrSetNull (contexte_de.pszTopic);
    }
  }

/*--------------------------------------------------------------------------*/
/*                             DIVERS                                       */
/*--------------------------------------------------------------------------*/

static void maj_pointeur_dans_geo (DWORD wNumeroRepere, DWORD limite,
                                   BOOL spec)
  {
  PGEO_DDE	donnees_geo;
  DWORD nbr_enr;
  DWORD i;

  if (!existe_repere (b_geo_dde))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_dde);
    }
  for (i = 1; (i <= nbr_enr); i++)
    {
    donnees_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, i);
    if (donnees_geo->wNumeroRepere == wNumeroRepere)
      {
      switch (wNumeroRepere)
        {
        case b_dde_e:
        case b_dde_e_dyn:
          if (donnees_geo->wPosSpecifEs >= limite)
            {
            donnees_geo->wPosSpecifEs--;
            }
          break;

        case b_dde_r:
        case b_dde_r_dyn:
          if (donnees_geo->wPosDonnees >= limite)
            {
            donnees_geo->wPosDonnees--;
            }
          break;

        case b_titre_paragraf_de:
          if (spec)
            {
            if (donnees_geo->wPosSpecifParagraf >= limite)
              {
              donnees_geo->wPosSpecifParagraf--;
              }
            }
          else
            {
            if (donnees_geo->wPosParagraf >= limite)
              {
              donnees_geo->wPosParagraf--;
              }
            }
          break;

        default:
          break;
       
        } // switch
      }
    } // for
  }

/*--------------------------------------------------------------------------*/
void static traitement_num (PUL ul, DWORD num)
  {
  StrDWordToStr (ul->show, num);
  ul->erreur = 0;
  }

//--------------------------------------------------------------------------
//                             EDITION
//--------------------------------------------------------------------------

// Generation vecteur UL
static void genere_vect_ul (DWORD ligne, PUL vect_ul,
                            int *nb_ul, int *niveau, int *indentation)
  {
  PGEO_DDE	donnees_geo;
  tc_dde_e *dde_e;
  tc_dde_r *dde_r;
  tc_dde_e_dyn *dde_e_dyn;
  tc_dde_r_dyn *dde_r_dyn;
  PTITRE_PARAGRAPHE titre_paragraf;
  t_spec_titre_paragraf_de *spec_titre_paragraf;
  DWORD wNumChamp;

  (*niveau) = 0;
  (*indentation) = 0;
  donnees_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, ligne);
  switch (donnees_geo->wNumeroRepere)
    {
    case b_pt_constant:
      (*nb_ul) = 1;
      vect_ul[0].erreur = 0;
      StrSetNull (vect_ul[0].show);
      consulte_cst_bd_c (donnees_geo->wPosDonnees, vect_ul[0].show);
      break;

    case b_titre_paragraf_de :
     titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_de, donnees_geo->wPosParagraf);
     switch (titre_paragraf->IdParagraphe)
       {
       case liens_predefinis_de:
         (*niveau) = 1;
         (*indentation) = 0;
         (*nb_ul) = 1;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_liens_predefinis, vect_ul[0].show);
         break;

       case variables_importees_de:
         (*niveau) = 1;
         (*indentation) = 0;
         (*nb_ul) = 1;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_client, vect_ul[0].show);
         break;

       case variables_exportees_de:
         (*niveau) = 1;
         (*indentation) = 0;
         (*nb_ul) = 1;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_serveur, vect_ul[0].show);
         break;

       case entree:
         (*niveau) = 2;
         (*indentation) = 1;
         (*nb_ul) = 7;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_e, vect_ul[0].show);
         spec_titre_paragraf = (t_spec_titre_paragraf_de *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, donnees_geo->wPosSpecifParagraf);
         //
         StrCopy (vect_ul[1].show, spec_titre_paragraf->pszLien);
         StrCopy (vect_ul[2].show, spec_titre_paragraf->pszApplic);
         StrCopy (vect_ul[3].show, spec_titre_paragraf->pszTopic);
         NomVarES (vect_ul[4].show, spec_titre_paragraf->wPosEsLien);
         traitement_num (&(vect_ul[5]), spec_titre_paragraf->wTimeOut);
         StrCopy (vect_ul[6].show, spec_titre_paragraf->pszParam);
         break;

       case sortie:
         (*niveau) = 2;
         (*indentation) = 1;
         (*nb_ul) = 7;
				 init_vect_ul (vect_ul, *nb_ul);
         bMotReserve (c_res_reflet, vect_ul[0].show);
         spec_titre_paragraf = (t_spec_titre_paragraf_de *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, donnees_geo->wPosSpecifParagraf);
         //
         StrCopy (vect_ul[1].show, spec_titre_paragraf->pszLien);
         StrCopy (vect_ul[2].show, spec_titre_paragraf->pszApplic);
         StrCopy (vect_ul[3].show, spec_titre_paragraf->pszTopic);
         NomVarES (vect_ul[4].show, spec_titre_paragraf->wPosEsLien);
         traitement_num (&(vect_ul[5]), spec_titre_paragraf->wTimeOut);
         StrCopy (vect_ul[6].show, spec_titre_paragraf->pszParam);
         break;

       default:
         break;

       } // switch (titre_paragraf->IdParagraphe)
     break;

    case b_dde_e:
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 5;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->wPosEs);
      dde_e = (tc_dde_e *)pointe_enr (szVERIFSource, __LINE__, b_dde_e, donnees_geo->wPosSpecifEs);
      wNumChamp = 1;
      //
      if (dde_e->wTaille)
        { // c'est un tableau
        traitement_num(&(vect_ul[wNumChamp++]), dde_e->wTaille);
        }
      //
      StrCopy (vect_ul[wNumChamp++].show, dde_e->pszItem);
      //
      if (dde_e->wPosEsRaf)
        { // il y a une raf
        NomVarES (vect_ul[wNumChamp++].show, dde_e->wPosEsRaf);
        //traitement_num(&(vect_ul[wNumChamp++]), dde_e->wPosEsRaf);
        }
      //
      consulte_cst_bd_c (donnees_geo->wPosInitial, vect_ul[wNumChamp].show);
      //
      (*nb_ul) = wNumChamp + 1;
      break;

    case b_dde_r:
      (*niveau) = 3;
      (*indentation) = 2;
      (*nb_ul) = 3;
			init_vect_ul (vect_ul, *nb_ul);
      dde_r = (tc_dde_r *)pointe_enr (szVERIFSource, __LINE__, b_dde_r, donnees_geo->wPosDonnees);
      NomVarES (vect_ul[0].show, dde_r->wPosEs);
      wNumChamp = 1;
      //
      StrCopy (vect_ul[wNumChamp++].show, dde_r->pszItem);
      //
      if (dde_r->wPosEsRaf)
        { // il y a une raf
        NomVarES (vect_ul[wNumChamp].show, dde_r->wPosEsRaf);
        //traitement_num(&(vect_ul[wNumChamp]), dde_r->wPosEsRaf);
        }
      //
      (*nb_ul) = wNumChamp + 1;
      break;

    case b_dde_e_dyn:
      (*niveau) = 2;
      (*indentation) = 1;
      (*nb_ul) = 3;
			init_vect_ul (vect_ul, *nb_ul);
      NomVarES (vect_ul[0].show, donnees_geo->wPosEs);
      dde_e_dyn = (tc_dde_e_dyn *)pointe_enr (szVERIFSource, __LINE__, b_dde_e_dyn, donnees_geo->wPosSpecifEs);
      wNumChamp = 1;
      //
      if (dde_e_dyn->wTaille)
        { // c'est un tableau
        traitement_num(&(vect_ul[wNumChamp++]), dde_e_dyn->wTaille);
        }
      //
      consulte_cst_bd_c (donnees_geo->wPosInitial, vect_ul[wNumChamp].show);
      //
      (*nb_ul) = wNumChamp + 1;
      break;

    case b_dde_r_dyn:
      (*niveau) = 2;
      (*indentation) = 1;
      (*nb_ul) = 1;
			init_vect_ul (vect_ul, *nb_ul);
      dde_r_dyn = (tc_dde_r_dyn *)pointe_enr (szVERIFSource, __LINE__, b_dde_r_dyn, donnees_geo->wPosDonnees);
      NomVarES (vect_ul[0].show, dde_r_dyn->wPosEs);
      //
      (*nb_ul) = 1;
      break;

    default:
      break;

    } // switch
  } // genere_vect_ul

 
//-------------------------------
// valide vecteur UL
static BOOL valide_vect_ul (PUL vect_ul, int iNbUl, DWORD wLigne,
                               REQUETE_EDIT action, DWORD *wProfondeur,
                               PID_PARAGRAPHE tipe_paragraf,
                               DWORD *wNumeroRepere, DWORD *wPositionIdent,
                               DWORD *wPosInit, BOOL *bExistInit,
                               tc_dde_e *spec_e, tc_dde_r *spec_r,
                               tc_dde_e_dyn *spec_e_dyn, tc_dde_r_dyn *spec_r_dyn,
                               t_spec_titre_paragraf_de *spec_titre_paragraf,
                               DWORD *wErreur)

  {
  PENTREE_SORTIE es;
  DWORD wNbrImbrication;
  ID_MOT_RESERVE wNumFonction;
  DWORD wPositionEs;
  BOOL bExistIdent;
  BOOL bBooleen;
  BOOL bUnLog;
  BOOL bUnNum;
  FLOAT rReel;
  t_contexte_de old_contexte;
  PGEO_DDE	val_geo;
  DWORD wPosEsTemp;
  DWORD wMot;


  //***********************************************
  #define Err(code) (*wErreur) = code; return (FALSE)  //Sale Sioux
  //***********************************************

 
  old_contexte = contexte_de;
  (*wErreur) = 0;
  if (iNbUl < 1)
    {
    Err(IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
    }
  if (iNbUl > 7)
    {
    Err(31);
    }
  if (!bReconnaitESValide ((vect_ul+0), &bExistIdent, wPositionIdent))
    {
    if (!bReconnaitMotReserve ((vect_ul+0)->show, &wNumFonction))
      {
      if (!bReconnaitConstante ((vect_ul+0), &bBooleen, &rReel,
                            &bUnLog, &bUnNum, bExistInit, wPosInit))
        {
        Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      if ((bUnLog) || (bUnNum))
        {
        Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      (*wNumeroRepere) = b_pt_constant;
      }
    else
      {
      switch (wNumFonction)
        {
        case c_res_liens_predefinis :
          (*wProfondeur) = 1;
          wNbrImbrication = 0;
          (*wNumeroRepere) = b_titre_paragraf_de;
          (*tipe_paragraf) = liens_predefinis_de;
          contexte_de.wParagraf = liens_predefinis_de;
          break;

        case c_res_client :
          (*wProfondeur) = 1;
          wNbrImbrication = 0;
          (*wNumeroRepere) = b_titre_paragraf_de;
          (*tipe_paragraf) = variables_importees_de;
          contexte_de.wParagraf = variables_importees_de;
          break;

        case c_res_serveur :
          (*wProfondeur) = 1;
          wNbrImbrication = 0;
          (*wNumeroRepere) = b_titre_paragraf_de;
          (*tipe_paragraf) = variables_exportees_de;
          contexte_de.wParagraf = variables_exportees_de;
          break;

        case c_res_e :
        case c_res_reflet :
          if (iNbUl != 7)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          //------------- Nom du Lien
          if (!bReconnaitConstante ((vect_ul+1), &bBooleen, &rReel,
                                &bUnLog, &bUnNum, bExistInit, wPosInit))
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (bUnLog || bUnNum)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrCopy (spec_titre_paragraf->pszLien, vect_ul[1].show);
          StrCopy (contexte_de.pszLien, vect_ul[1].show);
          //------------- Nom Applic
          if (!bReconnaitConstante ((vect_ul+2), &bBooleen, &rReel,
                                &bUnLog, &bUnNum, bExistInit, wPosInit))
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (bUnLog || bUnNum)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrCopy (spec_titre_paragraf->pszApplic, vect_ul[2].show);
          StrCopy (contexte_de.pszApplic, vect_ul[2].show);
          //------------- Nom Topic
          if (!bReconnaitConstante ((vect_ul+3), &bBooleen, &rReel,
                                &bUnLog, &bUnNum, bExistInit, wPosInit))
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (bUnLog || bUnNum)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrCopy (spec_titre_paragraf->pszTopic, vect_ul[3].show);
          StrCopy (contexte_de.pszTopic, vect_ul[3].show);
          //------------- Raf d'Activation
          if (bReconnaitESExistante ((vect_ul+4), &wPositionEs))
            {
            es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wPositionEs);
            if (es->genre != c_res_logique)
              {
              Err(IDSERR_LA_VARIABLE_D_ACTIVATION_N_EST_PAS_DE_TYPE_LOGIQUE);
              }
            spec_titre_paragraf->wPosEsLien = wPositionEs;
            }
          else
            {
            Err(219);
            }
          //------------- Time out
          if (!bReconnaitConstanteNum ((vect_ul+5), &bBooleen, &rReel, &wMot))
            {
            Err(257);
            }
          if ((rReel < 0) || (rReel > MAXWORD))
            {
            Err(257);
            }
          spec_titre_paragraf->wTimeOut = (DWORD) rReel;
          //------------- Cste Parametres
          if (!bReconnaitConstante ((vect_ul+6), &bBooleen, &rReel,
                                &bUnLog, &bUnNum, bExistInit, wPosInit))
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (bUnLog || bUnNum)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrCopy (spec_titre_paragraf->pszParam, vect_ul[6].show);
          //
          (*wProfondeur) = 2;
          wNbrImbrication = 1;
          (*wNumeroRepere) = b_titre_paragraf_de;
          if (wNumFonction == c_res_e)
            {
            (*tipe_paragraf) = entree;
            }
          else
            {
            (*tipe_paragraf) = sortie;
            }
          contexte_de.wSens = (DWORD)wNumFonction;
          break;

        default:
          Err(32);
          break;
        } // switch wNumFonction
      if (!bValideInsereLigneParagraphe (b_titre_paragraf_de, wLigne,
                                           (*wProfondeur), wNbrImbrication,
                                           wErreur))
        {
        contexte_de = old_contexte;
        Err((*wErreur));
        }
      } // mot res
    }// non identificateur
  else
    { // c'est une variable
    if (StrLength (vect_ul[0].show) > 20)
      {
      Err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
      }
    if ((contexte_de.wSens == c_res_e) ||
        (contexte_de.wSens == c_res_client))
      {
      if (action != MODIFIE_LIGNE)
        {
        if (bExistIdent)
          {
          Err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
          }
        }
      else
        { // modification
        val_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, wLigne);
        if ((val_geo->wNumeroRepere == b_dde_e) ||
            (val_geo->wNumeroRepere == b_dde_e_dyn))
          {
          if ((bExistIdent) && (!bStrEgaleANomVarES (vect_ul[0].show, val_geo->wPosEs)))
            {
            Err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
            }
          } // deja description donnees
        else
          {
          if (bExistIdent)
            {
            Err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
            }
          }
        }
      }
    //
    if (iNbUl < 1)
      {
      Err(IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
      }

    switch (contexte_de.wSens)
      {
      case c_res_e : // paragraphe entrees
        if ((iNbUl < 3) || (iNbUl > 5))
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        //-------------------- Taille ou Item
        if (!bReconnaitConstante ((vect_ul+1), &bBooleen, &rReel,
                              &bUnLog, &bUnNum, bExistInit, wPosInit))
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (bUnLog)
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (bUnNum)
          {
          //-------------- VAR TABLEAU : 4 advise ou 5 request
          if (iNbUl < 4)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if ((rReel < 0) || (rReel > MAXWORD))
            {
            Err(IDSERR_LA_TAILLE_DU_TABLEAU_EST_INVALIDE);
            }
          spec_e->wSens = c_res_variable_ta_e;
          spec_e->wTaille = (DWORD) rReel;
          //-------------- Item
          if (!bReconnaitConstante ((vect_ul+2), &bBooleen, &rReel,
                                &bUnLog, &bUnNum, bExistInit, wPosInit))
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (bUnLog || bUnNum)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrCopy (spec_e->pszItem, vect_ul[2].show);
          //-------------- RAF ou pas?
          if (iNbUl == 4)
            {
            spec_e->wPosEsRaf = 0;
            }
          else
            {
            //---------------- c'est un Request : gerer Raf
            if (bReconnaitESExistante ((vect_ul+3), &wPositionEs))
              {
              es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wPositionEs);
              if (es->genre != c_res_logique)
                {
                Err(IDSERR_LA_VARIABLE_D_ACTIVATION_N_EST_PAS_DE_TYPE_LOGIQUE);
                }
              spec_e->wPosEsRaf = wPositionEs;
              }
            else
              {
              Err(219);
              }
            }
          }
        else
          {
          //-------------- VAR SIMPLE : 3 advise ou 4 request
          spec_e->wTaille = 0;
          spec_e->wSens = c_res_e;
          if (iNbUl > 4)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          //-------------- Item
          if (!bReconnaitConstante ((vect_ul+1), &bBooleen, &rReel,
                                &bUnLog, &bUnNum, bExistInit, wPosInit))
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (bUnLog || bUnNum)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          StrCopy (spec_e->pszItem, vect_ul[1].show);
          //-------------- RAF ou pas?
          if (iNbUl == 3)
            {
            spec_e->wPosEsRaf = 0;
            }
          else
            {
            //---------------- c'est un Request : gerer Raf
            if (bReconnaitESExistante ((vect_ul+2), &wPositionEs))
              {
              es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wPositionEs);
              if (es->genre != c_res_logique)
                {
                Err(IDSERR_LA_VARIABLE_D_ACTIVATION_N_EST_PAS_DE_TYPE_LOGIQUE);
                }
              spec_e->wPosEsRaf = wPositionEs;
              }
            else
              {
              Err(219);
              }
            }
          }
        //-------------- Valeur Initiale
        if (!bReconnaitConstante ((vect_ul+iNbUl-1), &bBooleen, &rReel,
                              &bUnLog, &bUnNum, bExistInit, wPosInit))
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (bUnLog)
          {
          spec_e->wGenre = c_res_logique;
          }
        else
          {
          if (bUnNum)
            {
            spec_e->wGenre = c_res_numerique;
            }
          else
            {
            spec_e->wGenre = c_res_message;
            }
          }
        //
        (*wNumeroRepere) = b_dde_e;
        break;

      case c_res_reflet : // paragraphe reflets
        if ((iNbUl < 2) || (iNbUl > 3))
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (!bExistIdent)
          {
          Err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
          }
        //-------------------- Item
        if (!bReconnaitConstante ((vect_ul+1), &bBooleen, &rReel,
                              &bUnLog, &bUnNum, bExistInit, wPosInit))
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if ((bUnLog) || (bUnNum))
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        StrCopy (spec_r->pszItem, vect_ul[1].show);
        //-------------- ACT ou pas?
        wPosEsTemp = dwIdVarESFromPosIdentif (*wPositionIdent);
        //
        if (iNbUl == 2)
          { // c'est une variable simple
          spec_r->wPosEsRaf = 0;
          //
          es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wPosEsTemp);
          if ((es->sens != c_res_e) &&
              (es->sens != c_res_s) &&
              (es->sens != c_res_e_et_s))
            { // la variable est un tableau : il faut une var d'ACTIVATION
            Err(IDSERR_IL_FAUT_UNE_VARIABLE_D_ACTIVATION);
            }
          }
        else
          { // c'est une variable tableau
          //------------- Activation
          if (bReconnaitESExistante ((vect_ul+2), &wPositionEs))
            {
            es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wPositionEs);
            if (es->genre != c_res_logique)
              {
              Err(IDSERR_LA_VARIABLE_D_ACTIVATION_N_EST_PAS_DE_TYPE_LOGIQUE);
              }
            spec_r->wPosEsRaf = wPositionEs;
            }
          else
            {
            Err(219);
            }
          //
          es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wPosEsTemp);
          if ((es->sens != c_res_variable_ta_e) &&
              (es->sens != c_res_variable_ta_s) &&
              (es->sens != c_res_variable_ta_es))
            { // la variable n'est pas un tableau : il ne faut pas de var ACT
            Err(IDSERR_IL_NE_FAUT_PAS_DE_VARIABLE_D_ACTIVATION);
            }
          }
        //
        spec_r->wPosEs = wPosEsTemp;
        spec_r->wGenre = es->genre;
        (*wNumeroRepere) = b_dde_r;
        break;

      case c_res_client :
        if ((iNbUl < 2) || (iNbUl > 3))
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (iNbUl == 2)
          { //-------------------- variable simple
          spec_e_dyn->wTaille = 0;
          spec_e_dyn->wSens = c_res_e;
          }
        else
          { //-------------------- variable tableau
          spec_e_dyn->wSens = c_res_variable_ta_e;
          if (!bReconnaitConstante ((vect_ul+1), &bBooleen, &rReel,
                                &bUnLog, &bUnNum, bExistInit, wPosInit))
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if (!bUnNum)
            {
            Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
            }
          if ((rReel < 0) || (rReel > MAXWORD))
            {
            Err(IDSERR_LA_TAILLE_DU_TABLEAU_EST_INVALIDE);
            }
          spec_e_dyn->wTaille = (DWORD) rReel;
          }
        //---------------------- Valeur Initiale
        if (!bReconnaitConstante ((vect_ul+iNbUl-1), &bBooleen, &rReel,
                              &bUnLog, &bUnNum, bExistInit, wPosInit))
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (bUnLog)
          {
          spec_e_dyn->wGenre = c_res_logique;
          }
        else
          {
          if (bUnNum)
            {
            spec_e_dyn->wGenre = c_res_numerique;
            }
          else
            {
            spec_e_dyn->wGenre = c_res_message;
            }
          }
        //
        (*wNumeroRepere) = b_dde_e_dyn;
        break;

      case c_res_serveur :
        if (iNbUl != 1)
          {
          Err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          }
        if (!bExistIdent)
          {
          Err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
          }
        //-------------- wPosEs
        wPosEsTemp = dwIdVarESFromPosIdentif (*wPositionIdent);
        //
        es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wPosEsTemp);
        //
        spec_r_dyn->wPosEs = wPosEsTemp;
        spec_r_dyn->wGenre = es->genre;
        (*wNumeroRepere) = b_dde_r_dyn;
        break;

      default :
        Err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        break;

      } // switch
    }
  return ((BOOL)((*wErreur) == 0));
  } // valide_vect_ul

//''''''''''''''''''''''''''''''valide ligne''''''''''''''''''''''''''''''''''''

void  valide_ligne_dde (REQUETE_EDIT action, DWORD wLigne,
                             PUL vect_ul, int *iNbUl,
                             BOOL *supprime_ligne, BOOL *accepte_ds_bd,
                             DWORD *erreur_val, int *niveau, int *indentation)
  {
  DWORD erreur;
  DWORD wPosInit;
  BOOL un_paragraphe;
  BOOL bExistInit;
  tc_dde_e spec_e;
  tc_dde_e *ptr_spec_e;
  tc_dde_r spec_r;
  tc_dde_r *ptr_spec_r;
  tc_dde_e_dyn spec_e_dyn;
  tc_dde_e_dyn *ptr_spec_e_dyn;
  tc_dde_r_dyn spec_r_dyn;
  tc_dde_r_dyn *ptr_spec_r_dyn;
  GEO_DDE val_geo;
  PGEO_DDE	donnees_geo;
  ENTREE_SORTIE es;
  DWORD wNumeroRepere;
  DWORD wPosParagraf;
  DWORD wProfondeur;
  ID_PARAGRAPHE tipe_paragraf;
  PTITRE_PARAGRAPHE titre_paragraf;
  t_spec_titre_paragraf_de spec_titre_paragraf;
  t_spec_titre_paragraf_de *ptr_spec_titre_paragraf;

  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;
  switch (action)
    {
    case MODIFIE_LIGNE:
      consulte_titre_paragraf_de (wLigne);
      if (valide_vect_ul(vect_ul, (*iNbUl), wLigne, MODIFIE_LIGNE,
                         &wProfondeur, &tipe_paragraf, 
												 &wNumeroRepere, &(es.i_identif), 
												 &wPosInit, &bExistInit,
                         &spec_e, &spec_r, &spec_e_dyn, &spec_r_dyn,
                         &spec_titre_paragraf, &erreur))
        {
        donnees_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, wLigne);
        val_geo = (*donnees_geo);
        if (val_geo.wNumeroRepere == wNumeroRepere)
          {
          switch (wNumeroRepere)
            {
            case b_dde_e :
              ptr_spec_e = (tc_dde_e *)pointe_enr (szVERIFSource, __LINE__, b_dde_e, val_geo.wPosSpecifEs);
              if ((spec_e.wSens == ptr_spec_e->wSens) &&
                  (spec_e.wGenre == ptr_spec_e->wGenre))
                {
                //
                es.sens = spec_e.wSens;
				        es.taille = spec_e.wTaille;
                es.genre = spec_e.wGenre;
                if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.wPosEs))
                  {
                  modifie_cst_bd_c (vect_ul[(*iNbUl)-1].show, &(val_geo.wPosInitial));
                  //
                  if (ptr_spec_e->wPosEsRaf)
                    { // il y avait une raf
                    DeReferenceES (ptr_spec_e->wPosEsRaf);
                    }
                  //
                  if (spec_e.wPosEsRaf)
                    { // il y a une raf
                    ReferenceES (spec_e.wPosEsRaf);
                    }
                  //
                  (*ptr_spec_e) = spec_e;
                  }
                else
                  {
                  erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                  }
                }
              else
                {
                erreur = 420;
                }
              break;

            case b_dde_r :
              ptr_spec_r = (tc_dde_r *)pointe_enr (szVERIFSource, __LINE__, b_dde_r, val_geo.wPosDonnees);
              if (ptr_spec_r->wPosEs != spec_r.wPosEs)
                {
                ReferenceES (spec_r.wPosEs);
                DeReferenceES (ptr_spec_r->wPosEs);
                }
              //
              if (ptr_spec_r->wPosEsRaf)
                { // il y avait une raf
                DeReferenceES (ptr_spec_r->wPosEsRaf);
                }
              //
              if (spec_r.wPosEsRaf)
                { // il y a une raf
                ReferenceES (spec_r.wPosEsRaf);
                }
              //
              (*ptr_spec_r) = spec_r;
              break;

            case b_dde_e_dyn :
              ptr_spec_e_dyn = (tc_dde_e_dyn *)pointe_enr (szVERIFSource, __LINE__, b_dde_e_dyn, val_geo.wPosSpecifEs);
              if ((spec_e_dyn.wSens == ptr_spec_e_dyn->wSens) &&
                  (spec_e_dyn.wGenre == ptr_spec_e_dyn->wGenre))
                {
                //
                es.sens = spec_e_dyn.wSens;
	        es.taille = spec_e_dyn.wTaille;
                es.genre = spec_e_dyn.wGenre;
                if (bModifieDeclarationES (&es, vect_ul[0].show, val_geo.wPosEs))
                  {
                  modifie_cst_bd_c (vect_ul[(*iNbUl)-1].show, &(val_geo.wPosInitial));
                  //
                  (*ptr_spec_e_dyn) = spec_e_dyn;
                  }
                else
                  {
                  erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                  }
                }
              else
                {
                erreur = 420;
                }
              break;

            case b_dde_r_dyn :
              ptr_spec_r_dyn = (tc_dde_r_dyn *)pointe_enr (szVERIFSource, __LINE__, b_dde_r_dyn, val_geo.wPosDonnees);
              if (ptr_spec_r_dyn->wPosEs != spec_r_dyn.wPosEs)
                {
                ReferenceES (spec_r_dyn.wPosEs);
                DeReferenceES (ptr_spec_r_dyn->wPosEs);
                }
              //
              (*ptr_spec_r_dyn) = spec_r_dyn;
              break;

            case b_pt_constant :
              modifie_cst_bd_c (vect_ul[0].show, &(val_geo.wPosDonnees));
              break;

            case b_titre_paragraf_de:
              titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_de, val_geo.wPosParagraf);
              if (titre_paragraf->IdParagraphe == tipe_paragraf)
                {
                if ((tipe_paragraf == entree) || (tipe_paragraf == sortie))
                  {
                  ptr_spec_titre_paragraf = (t_spec_titre_paragraf_de *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, val_geo.wPosSpecifParagraf);
                  //
                  DeReferenceES (ptr_spec_titre_paragraf->wPosEsLien);
                  ReferenceES (spec_titre_paragraf.wPosEsLien);
                  //
                  (*ptr_spec_titre_paragraf) = spec_titre_paragraf;
                  }
                }
              else
                {
                erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
                }
              break;

            default:
              break;

            } // switch numero repere
          }// meme repere
        else
          {
          erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
          }
        } // valide vect_ul Ok
      if (erreur != 0)
        {
        genere_vect_ul (wLigne, vect_ul, iNbUl, niveau, indentation);
        (*erreur_val) = erreur;
        }
      else
        {
        donnees_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, wLigne);
        (*donnees_geo) = val_geo;
        }
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne)=FALSE;
      break;

    case INSERE_LIGNE:
      consulte_titre_paragraf_de (wLigne - 1);
      if (valide_vect_ul(vect_ul, (*iNbUl), wLigne, INSERE_LIGNE,
                         &wProfondeur, &tipe_paragraf, 
												 &wNumeroRepere, &(es.i_identif),
												 &wPosInit, &bExistInit,
                         &spec_e, &spec_r,
                         &spec_e_dyn, &spec_r_dyn,
                         &spec_titre_paragraf, &erreur))
        {
        wPosParagraf = 0; // init pour mise a jour donnees paragraf
        val_geo.wNumeroRepere = wNumeroRepere;
        switch (wNumeroRepere)
          {
          case b_pt_constant :
            AjouteConstanteBd (wPosInit, bExistInit, vect_ul[0].show, &(val_geo.wPosDonnees));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_dde_e:
            if (!existe_repere (b_dde_e))
              {
              cree_bloc (b_dde_e, sizeof (spec_e), 0);
              }
            val_geo.wPosSpecifEs = nb_enregistrements (szVERIFSource, __LINE__, b_dde_e) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_dde_e, val_geo.wPosSpecifEs);
            ptr_spec_e = (tc_dde_e *)pointe_enr (szVERIFSource, __LINE__, b_dde_e, val_geo.wPosSpecifEs);
            (*ptr_spec_e) = spec_e;
            //
            if (spec_e.wPosEsRaf)
              { // RAF pour REQUEST
              ReferenceES (spec_e.wPosEsRaf);
              }
            //
            es.sens = spec_e.wSens;
	    es.taille = spec_e.wTaille;
            es.genre = spec_e.wGenre;
            es.n_desc = d_dde;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.wPosEs));
            AjouteConstanteBd (wPosInit, bExistInit, vect_ul[(*iNbUl)-1].show, &(val_geo.wPosInitial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_dde_r:
            if (!existe_repere (b_dde_r))
              {
              cree_bloc (b_dde_r, sizeof (spec_r), 0);
              }
            val_geo.wPosDonnees = nb_enregistrements (szVERIFSource, __LINE__, b_dde_r) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_dde_r, val_geo.wPosDonnees);
            ptr_spec_r = (tc_dde_r *)pointe_enr (szVERIFSource, __LINE__, b_dde_r, val_geo.wPosDonnees);
            //
            (*ptr_spec_r) = spec_r;
            //
            ReferenceES (spec_r.wPosEs);
            //
            if (spec_r.wPosEsRaf)
              { // il y a une variable d'activation : c'est un tableau
              ReferenceES (spec_r.wPosEsRaf);
              }
            //
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_dde_e_dyn:
            if (!existe_repere (b_dde_e_dyn))
              {
              cree_bloc (b_dde_e_dyn, sizeof (spec_e_dyn), 0);
              }
            val_geo.wPosSpecifEs = nb_enregistrements (szVERIFSource, __LINE__, b_dde_e_dyn) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_dde_e_dyn, val_geo.wPosSpecifEs);
            ptr_spec_e_dyn = (tc_dde_e_dyn *)pointe_enr (szVERIFSource, __LINE__, b_dde_e_dyn, val_geo.wPosSpecifEs);
            (*ptr_spec_e_dyn) = spec_e_dyn;
            //
            es.sens = spec_e_dyn.wSens;
	    es.taille = spec_e_dyn.wTaille;
            es.genre = spec_e_dyn.wGenre;
            es.n_desc = d_dde;
            InsereVarES (&es, vect_ul[0].show, &(val_geo.wPosEs));
            AjouteConstanteBd (wPosInit, bExistInit, vect_ul[(*iNbUl)-1].show, &(val_geo.wPosInitial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_dde_r_dyn:
            if (!existe_repere (b_dde_r_dyn))
              {
              cree_bloc (b_dde_r_dyn, sizeof (spec_r_dyn), 0);
              }
            val_geo.wPosDonnees = nb_enregistrements (szVERIFSource, __LINE__, b_dde_r_dyn) + 1;
            insere_enr (szVERIFSource, __LINE__, 1, b_dde_r_dyn, val_geo.wPosDonnees);
            ptr_spec_r_dyn = (tc_dde_r_dyn *)pointe_enr (szVERIFSource, __LINE__, b_dde_r_dyn, val_geo.wPosDonnees);
            //
            (*ptr_spec_r_dyn) = spec_r_dyn;
            //
            ReferenceES (spec_r_dyn.wPosEs);
            //
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_titre_paragraf_de :
            InsereTitreParagraphe (b_titre_paragraf_de, wLigne, wProfondeur,
                                     tipe_paragraf, &(val_geo.wPosParagraf));
            wPosParagraf = val_geo.wPosParagraf;
            if ((tipe_paragraf == entree) || (tipe_paragraf == sortie))
              {
              if (!existe_repere (b_spec_titre_paragraf_de))
                {
                val_geo.wPosSpecifParagraf = 1;
                cree_bloc (b_spec_titre_paragraf_de, sizeof (spec_titre_paragraf), 0);
                }
              else
                {
                val_geo.wPosSpecifParagraf = nb_enregistrements (szVERIFSource, __LINE__, b_spec_titre_paragraf_de) + 1;
                }
              insere_enr (szVERIFSource, __LINE__, 1, b_spec_titre_paragraf_de, val_geo.wPosSpecifParagraf);
              ptr_spec_titre_paragraf = (t_spec_titre_paragraf_de *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, val_geo.wPosSpecifParagraf);
              //
              ReferenceES (spec_titre_paragraf.wPosEsLien);
              //
              (*ptr_spec_titre_paragraf) = spec_titre_paragraf;
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            break;

          default:
            break;

          } //switch wNumeroRepere
        if (!existe_repere (b_geo_dde))
          {
          cree_bloc (b_geo_dde, sizeof (val_geo), 0);
          }
        insere_enr (szVERIFSource, __LINE__, 1, b_geo_dde, wLigne);
        donnees_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, wLigne);
        (*donnees_geo) = val_geo;
        un_paragraphe = (BOOL)(wNumeroRepere == b_titre_paragraf_de);
        MajDonneesParagraphe (b_titre_paragraf_de, wLigne, INSERE_LIGNE,
                                un_paragraphe, wProfondeur, wPosParagraf);
        } // valide vect_ul Ok
      else
        {
        (*accepte_ds_bd) = FALSE;
        (*erreur_val) = erreur;
        }
      break;

    case SUPPRIME_LIGNE:
      consulte_titre_paragraf_de (wLigne);
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      donnees_geo = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, wLigne);
      val_geo = (*donnees_geo);
      switch (val_geo.wNumeroRepere)
        {
        case b_pt_constant:
          supprime_cst_bd_c (val_geo.wPosDonnees);
          break;

        case b_dde_e:
          if (!bSupprimeES (val_geo.wPosEs))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.wPosInitial);
            ptr_spec_e = (tc_dde_e *)pointe_enr (szVERIFSource, __LINE__, b_dde_e, val_geo.wPosSpecifEs);
            if (ptr_spec_e->wPosEsRaf)
              {
              DeReferenceES (ptr_spec_e->wPosEsRaf);
              }
            enleve_enr (szVERIFSource, __LINE__, 1, val_geo.wNumeroRepere, val_geo.wPosSpecifEs);
            maj_pointeur_dans_geo (val_geo.wNumeroRepere, val_geo.wPosSpecifEs, TRUE);
            }
          break;

        case b_dde_r:
          ptr_spec_r = (tc_dde_r *)pointe_enr (szVERIFSource, __LINE__, b_dde_r, val_geo.wPosDonnees);
          //
          if (ptr_spec_r->wPosEsRaf)
            { // il y a une raf
            DeReferenceES (ptr_spec_r->wPosEsRaf);
            }
          //
          DeReferenceES (ptr_spec_r->wPosEs);
          enleve_enr (szVERIFSource, __LINE__, 1, val_geo.wNumeroRepere, val_geo.wPosDonnees);
          maj_pointeur_dans_geo (val_geo.wNumeroRepere, val_geo.wPosDonnees, TRUE);
          break;

        case b_dde_e_dyn:
          if (!bSupprimeES (val_geo.wPosEs))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.wPosInitial);
            ptr_spec_e_dyn = (tc_dde_e_dyn *)pointe_enr (szVERIFSource, __LINE__, b_dde_e_dyn, val_geo.wPosSpecifEs);
            enleve_enr (szVERIFSource, __LINE__, 1, val_geo.wNumeroRepere, val_geo.wPosSpecifEs);
            maj_pointeur_dans_geo (val_geo.wNumeroRepere, val_geo.wPosSpecifEs, TRUE);
            }
          break;

        case b_dde_r_dyn:
          ptr_spec_r_dyn = (tc_dde_r_dyn *)pointe_enr (szVERIFSource, __LINE__, b_dde_r_dyn, val_geo.wPosDonnees);
          //
          DeReferenceES (ptr_spec_r_dyn->wPosEs);
          enleve_enr (szVERIFSource, __LINE__, 1, val_geo.wNumeroRepere, val_geo.wPosDonnees);
          maj_pointeur_dans_geo (val_geo.wNumeroRepere, val_geo.wPosDonnees, TRUE);
          break;

        case b_titre_paragraf_de:
          un_paragraphe = TRUE;
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_de, val_geo.wPosParagraf);
          tipe_paragraf = titre_paragraf->IdParagraphe;
          wProfondeur = titre_paragraf->profondeur;
          if (!bSupprimeTitreParagraphe (b_titre_paragraf_de, val_geo.wPosParagraf))
            {
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            (*erreur_val) = IDSERR_SUPPRESSION_DU_PARAGRAPHE_IMPOSSIBLE_A_CETTE_POSITION;
            }
          else
            {
            maj_pointeur_dans_geo (b_titre_paragraf_de, val_geo.wPosParagraf, FALSE);
            if ((tipe_paragraf == entree) || (tipe_paragraf == sortie))
              {
              //
              ptr_spec_titre_paragraf = (t_spec_titre_paragraf_de *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, val_geo.wPosSpecifParagraf);
              DeReferenceES (ptr_spec_titre_paragraf->wPosEsLien);
              //
              enleve_enr (szVERIFSource, __LINE__, 1, b_spec_titre_paragraf_de, val_geo.wPosSpecifParagraf);
              maj_pointeur_dans_geo(val_geo.wNumeroRepere, val_geo.wPosSpecifParagraf, TRUE);
              }
            }
          break;

        default:
          break;
        } // switch wNumeroRepere
      if ((*supprime_ligne))
        {
        MajDonneesParagraphe (b_titre_paragraf_de, wLigne, SUPPRIME_LIGNE,
					un_paragraphe, wProfondeur, 0); //wPosParagraf);
        enleve_enr (szVERIFSource, __LINE__, 1, b_geo_dde, wLigne);
        }
      break;

    case CONSULTE_LIGNE:
       genere_vect_ul (wLigne, vect_ul, iNbUl, niveau, indentation);
       break;

    default:
      break;

    } // switch action
  } // valide_ligne

// ---------------------------------------------------------------------------
void  creer_dde (void)
  {
  dde_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
void  init_dde (DWORD *depart, DWORD *courant, char *nom_applic)
  {
  if (!dde_deja_initialise)
    {
    ligne_depart_dde = 1;
    ligne_courante_dde = ligne_depart_dde;
    dde_deja_initialise = TRUE;
    }
  (*depart) = ligne_depart_dde;
  (*courant) = ligne_courante_dde;
  }

// ---------------------------------------------------------------------------
DWORD  lis_nombre_de_ligne_dde (void)
  {
  DWORD wretour;

  if (existe_repere (b_geo_dde))
    {
    wretour = nb_enregistrements (szVERIFSource, __LINE__, b_geo_dde);
    }
  else
    {
    wretour = 0;
    }
  return (wretour);
  }

// ---------------------------------------------------------------------------
void  set_tdb_dde (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_res[c_nb_car_message_res];
  char chaine_mot [10];

  StrSetNull (tdb);
  StrSetNull (chaine_mot);
  if ((existe_repere (b_titre_paragraf_de)) && (nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf_de) != 0))
    {
    if (nouvelle_ligne)
      {
      ligne--;
      }
    consulte_titre_paragraf_de (ligne);
    switch (contexte_de.wParagraf)
      {
      case liens_predefinis_de :
        bMotReserve (c_res_liens_predefinis, mot_res);
        StrConcat (tdb, mot_res);
        StrConcatChar (tdb, ' ');
        switch (contexte_de.wSens)
          {
          case c_res_e :
            bMotReserve (c_res_e, mot_res);
            StrConcat (tdb, mot_res);
            StrConcatChar (tdb, ' ');
            StrConcat (tdb, contexte_de.pszLien);
            StrConcatChar (tdb, ' ');
            StrConcat (tdb, contexte_de.pszApplic);
            StrConcatChar (tdb, ' ');
            StrConcat (tdb, contexte_de.pszTopic);
            break;

          case c_res_reflet :
            bMotReserve (c_res_reflet, mot_res);
            StrConcat (tdb, mot_res);
            StrConcatChar (tdb, ' ');
            StrConcat (tdb, contexte_de.pszLien);
            StrConcatChar (tdb, ' ');
            StrConcat (tdb, contexte_de.pszApplic);
            StrConcatChar (tdb, ' ');
            StrConcat (tdb, contexte_de.pszTopic);
            break;

          default :
            break;
          }
        break;

      case variables_importees_de :
        bMotReserve (c_res_client, mot_res);
        StrConcat (tdb, mot_res);
        break;

      case variables_exportees_de :
        bMotReserve (c_res_serveur, mot_res);
        StrConcat (tdb, mot_res);
        break;

      default :
        break;
      }
    }
  }

// ---------------------------------------------------------------------------
void  fin_dde (BOOL quitte, DWORD courant)
  {
  if ((!quitte))
    {
    ligne_courante_dde = courant;
    }
  }

// ---------------------------------------------------------------------------
// Interface du descripteur variables DDE
void LisDescripteurDDE (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_dde;
	pDescripteurCf->pfnInitDesc = init_dde;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_dde;
	pDescripteurCf->pfnValideLigne = valide_ligne_dde;
	pDescripteurCf->pfnSetTdb = set_tdb_dde;
	pDescripteurCf->pfnFin = fin_dde;
	}
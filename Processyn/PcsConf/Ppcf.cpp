/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : actpp.C                                                |
 |   Auteur  : AC					        	|
 |   Date    : 08/4/92 					        	|
 |   Version : 3.20							|
 |                                                                      |
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "MemMan.h"
#include "mem.h"
#include "driv_dq.h"
#include "LireLng.h"
#include "IdLngLng.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "initdesc.h"
#include "ClipBMan.h"
#include "ppcf.h"


#define TAILLE_LIGNE_PP 255
#define TAILLE_BLOC_BUFFER_PM 4096
#define b_presse_papier 812

typedef struct
  {
  void *ptr_buffer;
  HFDQ hfic;
  BOOL virtuel;
  DWORD pos_lec_virtuel;
  DWORD pos_ecr_virtuel;
  DWORD taille_buffer_pm;
  } CONTEXTE_PP_CF;

static CONTEXTE_PP_CF ctxt_pp;


// -----------------------------------------------------------------------
//                           ACTIONS PRESSE PAPIER
// -----------------------------------------------------------------------

// -----------------------------------------------------------------------
void vider_presse_papier (void)
  {
  if (ctxt_pp.virtuel)
    {
    enleve_bloc (b_presse_papier);
    ctxt_pp.pos_lec_virtuel = 0;
    }
  }


// -----------------------------------------------------------------------
BOOL bOuvrir_presse_papier (char *nom_fic, t_ouverture_pp type_ouverture)
  {
  BOOL retour_pp = TRUE;

  ctxt_pp.virtuel = (BOOL) (bStrEgales (nom_fic,  NOM_PP_VIRTUEL));
  if (ctxt_pp.virtuel)
    {
    if (type_ouverture == pp_en_ecriture)
      {
      ctxt_pp.pos_ecr_virtuel = 1;
      ctxt_pp.pos_lec_virtuel = 0;
      ctxt_pp.ptr_buffer = pMemAlloue (TAILLE_BLOC_BUFFER_PM);
      ctxt_pp.taille_buffer_pm = TAILLE_BLOC_BUFFER_PM;
      }
    else
      {
			PSTR pszTexte = pszClipBLitTexte ();
      if (pszTexte)
        {
				ctxt_pp.ptr_buffer = pszTexte; //$$ unlock ?
        ctxt_pp.pos_ecr_virtuel = 0;
        ctxt_pp.pos_lec_virtuel = 1;
        }
      else
        {
        retour_pp = FALSE; //110;
        ctxt_pp.pos_lec_virtuel = 0;
        }
      }
    }
  else
    {
    if (type_ouverture == pp_en_ecriture)
      {
      if (bFicPresent (nom_fic))
        {
        uFicEfface (nom_fic);
        }
      }
    retour_pp = (uFicTexteOuvre (&ctxt_pp.hfic, nom_fic)==0);
    }
  return retour_pp;
  }

// -----------------------------------------------------------------------
DWORD ecrire_ligne_presse_papier (char *texte)
  {
  DWORD retour_pp;
  DWORD nb_lettres;
  DWORD pos_car;
  char *caractere;

  retour_pp = 0;
  if (ctxt_pp.virtuel)
    {
    StrConcatChar (texte, '\r');
    StrConcatChar (texte, '\n');
    nb_lettres = StrLength (texte);
    if ((ctxt_pp.pos_ecr_virtuel + nb_lettres) > ctxt_pp.taille_buffer_pm)
      {
      ctxt_pp.taille_buffer_pm = ctxt_pp.taille_buffer_pm + TAILLE_BLOC_BUFFER_PM;
      MemRealloue (&ctxt_pp.ptr_buffer, ctxt_pp.taille_buffer_pm);
      }
    caractere = (char *) ctxt_pp.ptr_buffer;
    for (pos_car = 0; pos_car < nb_lettres ; pos_car++, ctxt_pp.pos_ecr_virtuel++)
      {
      caractere [ctxt_pp.pos_ecr_virtuel-1] = texte [pos_car];
      }
    }
  else
    {
    retour_pp = uFicEcrireLn (ctxt_pp.hfic, texte);
    }
  return (retour_pp);
  }

// -----------------------------------------------------------------------
void consulter_ligne_presse_papier (char *texte, BOOL *possible)
  {
  char *caractere;
  DWORD pos_texte;
  BOOL fini;
  int i;

  (*possible) = TRUE;
  if (ctxt_pp.virtuel)
    {
    if (ctxt_pp.pos_lec_virtuel > 0)
      {
      pos_texte = 0;
      fini = FALSE;
      caractere = (char *) ctxt_pp.ptr_buffer;
      do
        {
        switch (caractere[ctxt_pp.pos_lec_virtuel-1])
          {
          case '\0':
            (*possible) = FALSE;
            fini = TRUE;
            break;

          case '\n':
            texte [pos_texte] = '\0';
            fini = TRUE;
            break;

          case '\t':
            for (i = 0; i < 7; i++)
              {
              texte [pos_texte] = ' ';
              pos_texte++;
              }
              texte [pos_texte] = ' ';
            break;

          case '\r':
            texte [pos_texte] = '\0';
            break;

          default :
            texte [pos_texte] = caractere[ctxt_pp.pos_lec_virtuel-1];
            break;
          }
        pos_texte++;
        ctxt_pp.pos_lec_virtuel++;
        }
      while (!fini);
      }
    else
      {
      (*possible) = FALSE;
      }
    }
  else
    {
    if (uFicLireLn (ctxt_pp.hfic, texte, TAILLE_LIGNE_PP) != 0)
      {
      (*possible) = FALSE;
      }
    }
  }

// -----------------------------------------------------------------------
void analyse_ligne_presse_papier (char *texte, DWORD *numero_desc, DWORD *type_ligne_pp)
  {
  BOOL trouve;
  char mnemo[c_nb_car_message_inf];
  DWORD c_inf_titre;
  DWORD c_inf_mnemo;

  StrDeleteFirstSpaces (texte);
  if (texte[0] == CARACTERE_META_PP)
    {
    if (StrSearchStr (texte, APPLICATION_PP) != STR_NOT_FOUND)
      {
      StrDelete (texte, 0, StrLength (APPLICATION_PP));
      StrDeleteFirstSpaces (texte);
      (*type_ligne_pp) = LIGNE_META_PP_APPLIC;
      }
    else
      {
      if (StrSearchStr (texte, DESCRIPTEUR_PP) != STR_NOT_FOUND)
        {
        StrDeleteFirstSpaces (texte);
        StrDelete (texte, 0, StrLength (DESCRIPTEUR_PP));
        StrDeleteFirstSpaces (texte);
        StrDeleteLastSpaces (texte);
        trouve = FALSE;
        (*numero_desc) = premier_driver;
        while ((!trouve) && ((*numero_desc) <= dernier_driver))
          {
          if (bInfoDescripteur ((*numero_desc), &c_inf_titre, &c_inf_mnemo))
            {
            bLngLireInformation (c_inf_mnemo, mnemo);
            trouve = bStrEgales (texte, mnemo);
            }
          if (!trouve)
            {
            (*numero_desc)++;
            }
          } // while
        if ((*numero_desc) == d_graf)
          {
          (*type_ligne_pp) = LIGNE_META_PP_ANIMATION;
          }
        else
          {
          (*type_ligne_pp) = LIGNE_META_PP_DESCRIPTEUR;
          }
        }
      else
        {
        if (StrSearchStr (texte, FIN_PP) != STR_NOT_FOUND)
          {
          (*type_ligne_pp) = LIGNE_META_PP_FIN;
          }
        else
          {
          // meta inconnu -> ligne PROCESSYN probablement invalide
          (*type_ligne_pp) = LIGNE_PP_STANDARD;
          }
        }
      }
    }
  else
    {
    if (StrIsNull (texte))
      {
      (*type_ligne_pp) = LIGNE_PP_VIERGE;
      }
    else
      {
      (*type_ligne_pp) = LIGNE_PP_STANDARD;
      }
    }
  }

// -----------------------------------------------------------------------
void fermer_presse_papier (void)
  {
  char *caractere;

  if (ctxt_pp.virtuel)
    {
    if (ctxt_pp.pos_lec_virtuel == 0) 
			// ouvert en ecriture -> on transf�re le buffer dans le clipboard
      {
      caractere = (char *)ctxt_pp.ptr_buffer;
      caractere[ctxt_pp.pos_ecr_virtuel-1] = '\0';

			// Copie le texte dans le presse papier
			bClipBEcritTexte ((PSTR)ctxt_pp.ptr_buffer);
      }
    }
  else
    {
    uFicFerme (&ctxt_pp.hfic);
    }
  }

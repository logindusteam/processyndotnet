// Interpreteur.h: interface for the CInterpreteur class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INTERPRETEUR_H__82BA65C3_416C_11D2_A145_0000E8D90704__INCLUDED_)
#define AFX_INTERPRETEUR_H__82BA65C3_416C_11D2_A145_0000E8D90704__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CInterpreteur  
	{
	// enum propre
	typedef enum
		{
		INSERE_LIGNE = 0,
			SUPPRIME_LIGNE = 1,
			CONSULTE_LIGNE = 2,
			MODIFIE_LIGNE = 3
		} REQUETE_EDIT; // Edition : op�ration sur les lignes
	public:
		CInterpreteur();
		virtual ~CInterpreteur();
		
		// Fonctions d'�dition
		virtual BOOL bExiste(); // TRUE si descripteur existe 
		virtual void Cree (void) = 0;
		virtual void InitDesc (DWORD *depart, DWORD *courant, char *nom_applic) = 0;
		virtual	DWORD LisNbLignes (void) = 0;
		virtual void ValideLigne (REQUETE_EDIT action, DWORD ligne, PUL vect_ul, int *nb_ul,
			BOOL *supprime_ligne, BOOL *accepte_ds_bd, DWORD *erreur_val, int *niveau, int *indentation) = 0;
		virtual void SetTdb (DWORD ligne, BOOL nouvelle_ligne, char *tdb) = 0;
		virtual void LisParamDlg (int index_boite, UINT *iddlg, FARPROC * adr_winproc) = 0;
		virtual void Fin (BOOL quitte, DWORD courant) = 0;
		virtual void Satellite (BOOL *bExecutionSynchrone, char *nom, char *param, char *env, char *chemin) = 0;
		
	};

#endif // !defined(AFX_INTERPRETEUR_H__82BA65C3_416C_11D2_A145_0000E8D90704__INCLUDED_)

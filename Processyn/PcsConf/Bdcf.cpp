/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Gestion interface bd              			|
 |   Auteur  : AC							|
 |   Date    : 5/2/92 							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include "std.h"
#include "mem.h"
#include "UStr.h"
#include "FMan.h"
#include "FileMan.h"
#include "lng_res.h"
#include "tipe.h"
#include "cvt_ul.h"
#include "Descripteur.h"
#include "desccf.h"
#include "initdesc.h"
#include "gerebdc.h"
#include "G_Objets.h"
#include "G_sys.h"
#include "bdgr.h"
#include "Bdanmgr.h"
#include "inter_al.h"
#include "Appli.h"
#include "DocMan.h"
#include "PathMan.h"
#include "cmdpcs.h"
#include "LireLng.h"
#include "IdLngLng.h"
#include "IdStrProcessyn.h"
#include "SignatureFichier.h"
#include "Verif.h"
#include "bdcf.h"

VerifInit;

typedef struct
  {
  char nom_descripteur[MAX_PATH];
  DWORD n_desc;
  DWORD pos_geo_depart;
  DWORD pos_geo_segment;
  DWORD pos_geo_debut_selection;
  DWORD pos_geo_fin_selection;
  DWORD pos_geo_consulte_selection;
  DWORD pos_geo_segment_marque;
  } t_ctxt_bd;

// Contexte d'utilisation de la fen�tre du descripteur courant
static t_ctxt_bd ctxt_bd = {"", premier_driver, 1, 1, 0, 0, 1, 1};

// Tableau d'acc�s aux fonctions des descripteur
static DESCRIPTEUR_CF aDescripteurs [dernier_driver+1]; // Attention : l'adresse 0 des tableaux est utilis�e pour le fdpl graphisme en import/export $$ ??

// -------------------------------------------------------------------------
#define MAX(A,B) ((A) >= (B) ? (A) : (B))
#define MIN(A,B) ((A) <= (B) ? (A) : (B))

// -------------------------------------------------------------------------
BOOL dans_bornes_selection (DWORD position)
  {
  DWORD plus_grand = MAX (ctxt_bd.pos_geo_debut_selection, ctxt_bd.pos_geo_fin_selection);
  DWORD plus_petit = MIN (ctxt_bd.pos_geo_debut_selection, ctxt_bd.pos_geo_fin_selection);

  return ((position >= plus_petit) && (position <= plus_grand-1));
  }

// -------------------------------------------------------------------------
//
static void typer_ligne (char *texte, ETAT_LIGNE_BD *type)
  {
  if (texte[0] == CAR_COMMENTAIRE)
    {
    if (texte[1] == CAR_INVALIDE)
      {
      (*type) = ligne_invalide;
      }
    else
      {
      (*type) = ligne_commentaire;
      }
    }
  else
    {
    (*type) = ligne_standard;
    }
  }

// -------------------------------------------------------------------------
//
void mettre_en_forme (char *texte, int indentation, DWORD type) // filtre invalide ou indente
  {
  #define PSZ_ESPACES_INDENTATION "  "
  int n;

  if (type == ligne_invalide)
    {
    StrDelete (texte, 0, 2);  // CAR_COMMENTAIRE + CAR_INVALIDE
    }
  else
    {
    if (StrLength (texte) + (indentation * StrLength (PSZ_ESPACES_INDENTATION)) < NBR_CAR_LIGNE_MAX)
      {
      for (n = 0; (n < indentation) ; n++)
        {
        StrInsertStr (texte, PSZ_ESPACES_INDENTATION, 0);
        }
      }
    }
  }

// ----------------------------------------------------------------
// r�cup�re les fonctions de tous les descripteurs - NULL pour les autres
void initialiser_ctxtbd_general (void)
  {
  DWORD n_desc;
  DWORD c_inf_titre_desc;
  DWORD c_inf_mnemo_desc;

  OuvrirMotsReserves ();
  ctxt_bd.n_desc = d_systeme;
  for (n_desc = 0; n_desc < dernier_driver+1; n_desc++)
    {
		static const DESCRIPTEUR_CF NULL_DESCRIPTEUR = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
		PDESCRIPTEUR_CF pDescripteur = &aDescripteurs[n_desc];

		*pDescripteur = NULL_DESCRIPTEUR;
    if (bInfoDescripteur (n_desc, &c_inf_titre_desc, &c_inf_mnemo_desc))
      {
			// Lis les infos du descripteur
      RecupereDescripteurConfiguration ((ID_DESCRIPTEUR)n_desc, pDescripteur);

			// "Cr�e" le descripteur si possible
			if (pDescripteur->pfnCree)
				pDescripteur->pfnCree ();
      }
    }
  } // initialiser_ctxtbd_general

// ------------------------------------
// renvoi le nom du descripteur courant
PCSTR pszNomDescripteurCourant (void)
  {
  return ctxt_bd.nom_descripteur;
  }

//-------------------------------------------------------------
// Raz de la base de donn�es courante
//
void raz_bd (void)
	{
	raz_mem ();
	}

//-------------------------------------------------------------
// initialisation des blocs g�n�raux et mise a jour du nom d'une application
// le nom 'chaine' doit comporter le path mais pas l'extension
void init_application (PCSTR chaine, PCSTR pszExtension)
  {
	bSetPathNameDoc (chaine);
  MajFichierCmdPcs (chaine);
  initialiser_ctxtbd_general();
  VerifWarning (bChargeVariablesSystemes ());// $$ � am�liorer en cas de Pb sur le fichier
  bdgr_supprimer_blocs_marques ();    // Suppression des marques eventuellement pr�sentes
  bdgr_creer();
  }


//-----------------------------------------------------------
// Charge une application
BOOL monter_application (PCSTR pszPathNameSansExtension, PCSTR pszExtension)
  {
  char szNomApplicationChargee [MAX_PATH];
  BOOL bRetour = FALSE;

  pszCreeNameExt (szNomApplicationChargee, MAX_PATH, pszPathNameSansExtension, pszExtension);

	pszPathNameAbsolu (szNomApplicationChargee, Appli.szPathInitial);
	if (CFman::bFAcces (szNomApplicationChargee, CFman::OF_OUVRE_LECTURE_SEULE))
    {
		bRetour = (charge_memoire (szNomApplicationChargee) == 0);
		if (bRetour)
		{
			CSignatureFichier SignatureFichier(SIGNATURE_FORMAT_PCS_EN_COURS);
			bRetour = SignatureFichier.bDBPCSSignatureValide();
		}
		init_application (pszPathNameSansExtension, pszExtension);
    }
  return bRetour;
  }

//-----------------------------------------------------------
// Enregistre l'application
int sauver_application (PCSTR pszPathNameSansExtension, PCSTR pszExtension)
  {
  HFIC repere_fic;
  char nom_fic[MAX_PATH];
  char nom_fic_bak[MAX_PATH];
  char nom_precedent[MAX_PATH];
  int renvoi = 0;

	pszCreeNameExt (nom_fic, MAX_PATH, pszPathNameSansExtension, pszExtension);
  if (uFileOuvre (&repere_fic, nom_fic, 1, CFman::OF_OUVRE_OU_CREE) == 0)
    {
    uFileFerme (&repere_fic);
    StrCopy (nom_precedent, pszPathNameDoc());
		bSetPathNameDoc (pszPathNameSansExtension);
    if (bFileExiste (nom_fic))   // sauvegarde en .bak
      {
	    pszCreeNameExt (nom_fic_bak, MAX_PATH, pszPathNameSansExtension, EXTENSION_BAK);
      uFileEfface (nom_fic_bak);
      uFileRenomme (nom_fic, nom_fic_bak);
      }
    bdgr_supprimer_blocs_marques ();    // Suppression des marques eventuellement pr�sentes
		CSignatureFichier SignatureFichier(SIGNATURE_FORMAT_PCS_EN_COURS);
		SignatureFichier.DBPCSSetSignature();
    if (sauve_memoire (nom_fic) != 0)
      {
      VerifWarningExit;
      }
    bdgr_creer();
    if (!bStrEgales (nom_precedent, pszPathNameDoc())) // changement de nom => duplication fichier data
      { // gestion du fichier .pa1 pour les messages d'alarmes
			DupliqueFichierMessagesAlarme (nom_precedent);
      }

    // sauvegarde des variables pour linkeur PM
    //FichierVaPcsCf (pszPathNameSansExtension);
    // sauvegarde des variables pour linkeur PM

    MajFichierCmdPcs (pszPathNameSansExtension);
    }
  else
    {
    renvoi = IDSERR_SAUVEGARDE_DU_FICHIER_IMPOSSIBLE_VERIFIER_LE_NOM_ET_OU_LES_ATTRIBUTS;
    }

  return (renvoi);
  }

//-----------------------------------------------------------
void initialiser_ctxtbd_desc (DWORD numero_descripteur)
  {
  DWORD pos_depart;
  DWORD pos_segment;
  DWORD c_inf_titre;
  DWORD c_inf_mnemo;
  char application_courante_c[MAX_PATH];

  ctxt_bd.n_desc = numero_descripteur;
  ctxt_bd.nom_descripteur[0] = '\0';
  bInfoDescripteur (ctxt_bd.n_desc, &c_inf_titre, &c_inf_mnemo);
  bLngLireInformation (c_inf_mnemo, ctxt_bd.nom_descripteur);
  StrCopy (application_courante_c, pszPathNameDoc());

  (aDescripteurs[numero_descripteur].pfnInitDesc)(&pos_depart, &pos_segment, application_courante_c);
  ctxt_bd.pos_geo_depart = pos_depart;
  ctxt_bd.pos_geo_segment = pos_segment;
  ctxt_bd.pos_geo_segment_marque = pos_segment;
  ctxt_bd.pos_geo_debut_selection = pos_depart - 1;
  ctxt_bd.pos_geo_fin_selection = ctxt_bd.pos_geo_debut_selection;
  ctxt_bd.pos_geo_consulte_selection = ctxt_bd.pos_geo_debut_selection;
  }

//-----------------------------------------------------------
BOOL lancer_satellite_bd (BOOL *pbExecutionSynchrone, char *nom, char *param, char *env, char *chemin)
  {
  BOOL bOk = FALSE;

  if (aDescripteurs[ctxt_bd.n_desc].pfnSatellite != NULL)
    {
    bOk = TRUE;
    (aDescripteurs[ctxt_bd.n_desc].pfnSatellite)(pbExecutionSynchrone, nom, param, env, chemin);
    }

  return bOk;
  }

//-----------------------------------------------------------
BOOL satellite_existe_bd (void)
  {
  BOOL bOk = aDescripteurs[ctxt_bd.n_desc].pfnSatellite != NULL;
  return bOk;
  }

//-----------------------------------------------------------
void finir_desc_bd (BOOL quitte)
  {
  if (aDescripteurs[ctxt_bd.n_desc].pfnFin  != NULL)
    {
    (aDescripteurs[ctxt_bd.n_desc].pfnFin )(quitte, ctxt_bd.pos_geo_segment);
    }
  }

//-----------------------------------------------------------
// monte curseur courant
void monter_segment_bd (DWORD *decalage)
  {
  if (ctxt_bd.pos_geo_segment >= ctxt_bd.pos_geo_depart + (*decalage))
    {
    ctxt_bd.pos_geo_segment = ctxt_bd.pos_geo_segment - (*decalage);
    }
  else
    {
    (*decalage) = ctxt_bd.pos_geo_segment - ctxt_bd.pos_geo_depart;
    ctxt_bd.pos_geo_segment = ctxt_bd.pos_geo_depart;
    }
  }

//-----------------------------------------------------------
// descend curseur courant
void descendre_segment_bd (DWORD *decalage)

  {
  if ((ctxt_bd.pos_geo_segment + (*decalage)) <= ((aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)())+1)
    {
    ctxt_bd.pos_geo_segment = ctxt_bd.pos_geo_segment + (*decalage);
    }
  else
    {
    (*decalage) = ((aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)()) - ctxt_bd.pos_geo_segment;
    ctxt_bd.pos_geo_segment = (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)()+1;
    }
  }

//-----------------------------------------------------------
// positionne en % le segment bd
void positionner_segment_bd (DWORD segment_relatif)
// 1 -> premiere ligne non cachee
  {
  DWORD nb_ligne_bd = (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)();
  DWORD n_ligne_vrai = segment_relatif + ctxt_bd.pos_geo_depart - 1;

  if (n_ligne_vrai <= nb_ligne_bd)
    {
    ctxt_bd.pos_geo_segment = n_ligne_vrai;
    }
  else
    {
    if (nb_ligne_bd != 0)
      {
      ctxt_bd.pos_geo_segment = nb_ligne_bd;
      }
    else
      {
      ctxt_bd.pos_geo_segment = ctxt_bd.pos_geo_depart;
      }
    }
  }

//-----------------------------------------------------------
// positionne en % le segment bd
void positionner_segment_bd_100 (UINT pourcentage)
// 0% -> premiere ligne non cachee ; 100% derniere ligne
  {
  DWORD nb_ligne_bd = (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)();

  if ((nb_ligne_bd >= ctxt_bd.pos_geo_depart) && (pourcentage != 0))
    {
		LONG intermlong = (nb_ligne_bd - ctxt_bd.pos_geo_depart);
    intermlong = intermlong * pourcentage / 100;
    ctxt_bd.pos_geo_segment = (DWORD)(intermlong) + ctxt_bd.pos_geo_depart;
    }
  else
    {
    ctxt_bd.pos_geo_segment = ctxt_bd.pos_geo_depart;
    }
  }

//-----------------------------------------------------------
// recherche
void rechercher_mot_bd (DWORD offset, int type_recherche, char *mot_recherche, DWORD *decalage)
  {
  DWORD pos_geo;
  DWORD pos_depart;
  DWORD nb_ligne_bd;
  BOOL trouve;
  UL v_ul[nb_max_ul];
  int nb_ul;
  BOOL bAccepteDansBd;
  BOOL supprime_ligne;
  DWORD erreur_bd;
  int niveau;
  int indentation;
  char texte[NBR_CAR_LIGNE_MAX];
  char entete_invalide[3] = {CAR_COMMENTAIRE, CAR_INVALIDE, '\0'};
  char mot_recherche_majsc[80];

  (*decalage) = 0;

  StrCopy (mot_recherche_majsc, mot_recherche);
  StrUpperCase (mot_recherche_majsc);
  pos_depart = (ctxt_bd.pos_geo_segment + offset - 1); // ligne courante
  nb_ligne_bd = (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)();
  if (type_recherche > 0)
    {
    if (type_recherche == 2)
      {
      StrCopy (mot_recherche, entete_invalide);  // recherche ligne invalide
      StrCopy (mot_recherche_majsc, mot_recherche);
      }
    for (pos_geo = pos_depart + 1, trouve = FALSE;
         ((pos_geo <= nb_ligne_bd) && (!trouve));
         pos_geo++)
      {
      (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(CONSULTE_LIGNE, pos_geo, v_ul,
                                         &nb_ul, &supprime_ligne, &bAccepteDansBd,
                                         &erreur_bd, &niveau, &indentation);
      v_ul_to_texte (nb_ul, v_ul, texte, NBR_CAR_LIGNE_MAX);
      if (bStrEgales (texte, entete_invalide))
        {
        trouve = FALSE;   // ligne vierge
        }
      else
        {
        trouve = (BOOL) ((StrSearchStr (texte, mot_recherche) != STR_NOT_FOUND) ||
                            (StrSearchStr (texte, mot_recherche_majsc) != STR_NOT_FOUND));
        }
      }
    if (trouve)
      {
      (*decalage) = pos_geo - pos_depart - 1;
      }
    }
  else // vers le haut
    {
    for (pos_geo = pos_depart - 1, trouve = FALSE;
         ((pos_geo >= ctxt_bd.pos_geo_depart) && (!trouve));
         pos_geo--)
      {
      (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(CONSULTE_LIGNE, pos_geo, v_ul,
				&nb_ul, &supprime_ligne, &bAccepteDansBd, &erreur_bd, &niveau, &indentation);
      v_ul_to_texte (nb_ul, v_ul, texte, NBR_CAR_LIGNE_MAX);
      trouve = (BOOL) ((StrSearchStr (texte, mot_recherche) != STR_NOT_FOUND) ||
                          (StrSearchStr (texte, mot_recherche_majsc) != STR_NOT_FOUND));
      }
    if (trouve)
      {
      (*decalage) = pos_depart - (pos_geo + 1);
      }
    }
  }

//-----------------------------------------------------------
// contexte bd
void lire_contexte_bd (DWORD offset, char *nom_application, char *nom_descripteur,
											 char *contexte_descripteur, DWORD *pos_relative_ligne_bd, DWORD *taille_relative_bd)
  {
  DWORD pos_geo;
  DWORD nb_ligne_bd;

  pos_geo = ctxt_bd.pos_geo_segment + offset - 1;
  nb_ligne_bd =  (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)();
  StrSetNull (nom_application);
  StrCopy (nom_application, pszPathNameDoc());
  StrUpperCase (nom_application);
  nom_descripteur[0] = '\0';
  StrCopy (nom_descripteur, ctxt_bd.nom_descripteur);
  contexte_descripteur[0] = '\0';
  if (pos_geo > nb_ligne_bd)
    {
    pos_geo--;  // ligne precedente
    }
  if (pos_geo > 0)
    {
    (aDescripteurs[ctxt_bd.n_desc].pfnSetTdb)(pos_geo, FALSE, contexte_descripteur);
    }
  (*pos_relative_ligne_bd) = ctxt_bd.pos_geo_segment + offset - ctxt_bd.pos_geo_depart;
  (*taille_relative_bd) = nb_ligne_bd - ctxt_bd.pos_geo_depart + 1;
  }

//-----------------------------------------------------------
// test fin bd
BOOL fin_bd (DWORD offset)
  {
  BOOL bFin = TRUE;

  if (offset > 0)
    {
		DWORD pos_geo = ctxt_bd.pos_geo_segment + offset - 1;
    bFin = (pos_geo > ((aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)()));
    }
  return bFin;
  }

//-----------------------------------------------------------
// consultation bd
void consulter_bd (DWORD offset, char *texte, ETAT_LIGNE_BD *type,
                           BOOL *pbPossible, int *niveau, int *indentation, DWORD dwTailleTexte)
// renvoi ligne type invalide vide si bd inexistante
  {
  UL v_ul[nb_max_ul];
  int nb_ul = 0;
  BOOL supprime_ligne = FALSE;
  int indentation_local = 0;
  int niveau_local = 0;
  if (offset > 0)
    {
		DWORD pos_geo = 0;
    pos_geo = ctxt_bd.pos_geo_segment + offset - 1;
    if (pos_geo <= ((aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)()))
      {
			BOOL bAccepteDansBd = FALSE;
			DWORD erreur_bd = 0;
      (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(CONSULTE_LIGNE, pos_geo, v_ul,
				&nb_ul, &supprime_ligne, &bAccepteDansBd, &erreur_bd, &niveau_local, &indentation_local);
      v_ul_to_texte (nb_ul, v_ul, texte, dwTailleTexte);
      typer_ligne (texte, type);
      mettre_en_forme (texte, indentation_local, (*type));
      (*pbPossible) = TRUE;
      }
    else
      {
      texte[0] = '\0';   // bd vide
      (*type) = ligne_invalide;
      (*pbPossible) = FALSE;
      }
    }
  else
    {
    (*pbPossible) = FALSE;
    }
  *niveau = niveau_local;
  *indentation = indentation_local;
  }

//-----------------------------------------------------------
// insere une ligne dans le descripteur courant
void inserer_bd (DWORD offset, char *texte, DWORD * pdwErreurBd, BOOL bForcage)
  {
  DWORD pos_geo = ctxt_bd.pos_geo_segment + offset - 1;
  UL v_ul[nb_max_ul];
  int nb_ul;
  BOOL bAccepteDansBd;
  BOOL supprime_ligne;
  int niveau;
  int indentation;

  texte_to_v_ul (&nb_ul, v_ul, texte);
  if ((bForcage) || (StrIsNull (texte)))
    {
		// Demande d'insertion de la ligne en tant que ligne invalide
    StrInsertChar (texte, CAR_INVALIDE, 0);
    StrInsertChar (texte, CAR_COMMENTAIRE, 0);
    texte_to_v_ul (&nb_ul, v_ul, texte);
    (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(INSERE_LIGNE, pos_geo, v_ul,
			&nb_ul, &supprime_ligne, &bAccepteDansBd, pdwErreurBd, &niveau, &indentation);
    }
  else
    {
		// Demande d'insertion normale de la ligne
    (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(INSERE_LIGNE, pos_geo, v_ul,
			&nb_ul, &supprime_ligne, &bAccepteDansBd, pdwErreurBd, &niveau, &indentation);
    }
  }

//-----------------------------------------------------------
void supprimer_bd (DWORD offset, DWORD *pdwErreurBd, BOOL *pbPossible)
  {
  DWORD pos_geo = ctxt_bd.pos_geo_segment + offset - 1;

  if (pos_geo <= ((aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)()))
    {
		UL v_ul[nb_max_ul];
		int nb_ul;
		BOOL bAccepteDansBd;
		BOOL supprime_ligne;
		int niveau;
		int indentation;

    (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(SUPPRIME_LIGNE, pos_geo, v_ul,
			&nb_ul, &supprime_ligne, &bAccepteDansBd, pdwErreurBd, &niveau, &indentation);
    (*pbPossible) = TRUE;
    }
  else
    {
    (*pbPossible) = FALSE;
    }
  }

//-----------------------------------------------------------
void modifier_bd (DWORD offset, char *texte, DWORD *pdwErreurBd, BOOL *pbPossible)
  {
  DWORD pos_geo;
  UL v_ul[nb_max_ul];
  int nb_ul;
  BOOL bAccepteDansBd;
  BOOL supprime_ligne;
  int niveau;
  int indentation;

  texte_to_v_ul (&nb_ul, v_ul, texte);
  pos_geo = ctxt_bd.pos_geo_segment + offset - 1;
  if (pos_geo <= ((aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)()))
    {
    (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(MODIFIE_LIGNE, pos_geo, v_ul,
			&nb_ul, &supprime_ligne, &bAccepteDansBd, pdwErreurBd, &niveau, &indentation);
    (*pbPossible) = TRUE;
    }
  else
    {
    (*pbPossible) = FALSE;
    }
  }

//-----------------------------------------------------------
void sauver_marque_bd (DWORD offset)
  {
  ctxt_bd.pos_geo_segment_marque = ctxt_bd.pos_geo_segment + offset - 1;
  }

//-----------------------------------------------------------
void restaurer_marque_bd (void)
  {
  DWORD nb_ligne = (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)();

  if (ctxt_bd.pos_geo_segment_marque <= nb_ligne)
    {
    ctxt_bd.pos_geo_segment = ctxt_bd.pos_geo_segment_marque;
    }
  else
    {
    if (nb_ligne > 0)
      {
      ctxt_bd.pos_geo_segment = nb_ligne;
      }
    else
      {
      ctxt_bd.pos_geo_segment = 1;
      }
    }
  }

//-----------------------------------------------------------
void debut_selection_bd (DWORD offset)
// si fin-debut = nombre; si nombre = 0 alors rien
  {
  ctxt_bd.pos_geo_debut_selection = ctxt_bd.pos_geo_segment + offset - 1;
  ctxt_bd.pos_geo_consulte_selection = ctxt_bd.pos_geo_debut_selection;
  ctxt_bd.pos_geo_fin_selection = ctxt_bd.pos_geo_debut_selection;
  }

//-----------------------------------------------------------
void selectionner_tout_bd (void)
  {
  ctxt_bd.pos_geo_debut_selection = ctxt_bd.pos_geo_depart;
  ctxt_bd.pos_geo_consulte_selection = ctxt_bd.pos_geo_depart;
  ctxt_bd.pos_geo_fin_selection = (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)() + 1;
  }

//-----------------------------------------------------------
void abandon_selection_bd (void)
  {
  ctxt_bd.pos_geo_debut_selection = ctxt_bd.pos_geo_depart - 1;
  ctxt_bd.pos_geo_consulte_selection = ctxt_bd.pos_geo_debut_selection;
  ctxt_bd.pos_geo_fin_selection = ctxt_bd.pos_geo_debut_selection;
  }

//-----------------------------------------------------------
void typer_selection_bd (DWORD offset, BOOL *mode_selection)
  {
  DWORD pos_geo = ctxt_bd.pos_geo_segment + offset - 1;

  (*mode_selection) = dans_bornes_selection (pos_geo);
  }

//-----------------------------------------------------------
void descendre_selection_bd (DWORD decalage)
  {
  ctxt_bd.pos_geo_fin_selection = ctxt_bd.pos_geo_fin_selection + decalage;
  if (ctxt_bd.pos_geo_fin_selection > (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)())
    {
    ctxt_bd.pos_geo_fin_selection = (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)() + 1;
    }
  if (ctxt_bd.pos_geo_consulte_selection > ctxt_bd.pos_geo_fin_selection)
    {
    ctxt_bd.pos_geo_consulte_selection = ctxt_bd.pos_geo_fin_selection;
    }
  }

//-----------------------------------------------------------
void monter_selection_bd (DWORD decalage)
  {
  if ((ctxt_bd.pos_geo_fin_selection - ctxt_bd.pos_geo_depart) > decalage)
    {
    ctxt_bd.pos_geo_fin_selection = ctxt_bd.pos_geo_fin_selection - decalage;
    }
  else
    {
    ctxt_bd.pos_geo_fin_selection = ctxt_bd.pos_geo_depart;
    }
  if (ctxt_bd.pos_geo_consulte_selection > ctxt_bd.pos_geo_fin_selection)
    {
    ctxt_bd.pos_geo_consulte_selection = ctxt_bd.pos_geo_fin_selection;
    }
  }

//-----------------------------------------------------------
void positionner_selection_bd (DWORD numero_ligne)
  {
  if ((numero_ligne >= ctxt_bd.pos_geo_depart) && (numero_ligne <= (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)()))
    {
    ctxt_bd.pos_geo_fin_selection = numero_ligne;
    if (ctxt_bd.pos_geo_consulte_selection > ctxt_bd.pos_geo_fin_selection)
      {
      ctxt_bd.pos_geo_consulte_selection = ctxt_bd.pos_geo_fin_selection;
      }
    }
  }

//-----------------------------------------------------------
// positionne en % le segment bd
void positionner_selection_bd_100 (UINT pourcentage)
// 0% -> premiere ligne non cachee ; 100% derniere ligne
  {
  DWORD nb_ligne_bd;

  nb_ligne_bd = (aDescripteurs[ctxt_bd.n_desc].pfnLisNbLignes)();
  if ((nb_ligne_bd >= ctxt_bd.pos_geo_depart) && (pourcentage != 0))
    {
    ctxt_bd.pos_geo_fin_selection = (((nb_ligne_bd - ctxt_bd.pos_geo_depart) * pourcentage / 100)) + 1 + ctxt_bd.pos_geo_depart;
    // arrondi ligne dessous
    }
  else
    {
    ctxt_bd.pos_geo_fin_selection = ctxt_bd.pos_geo_depart;
    }
  if (ctxt_bd.pos_geo_consulte_selection > ctxt_bd.pos_geo_fin_selection)
    {
    ctxt_bd.pos_geo_consulte_selection = ctxt_bd.pos_geo_fin_selection;
    }
  }

//-----------------------------------------------------------
void consulterln_selection_bd (char *texte, BOOL *pbPossible, DWORD dwTailleTexte)
  {

	(*pbPossible) = FALSE;
  if (ctxt_bd.pos_geo_debut_selection >= ctxt_bd.pos_geo_depart)
    {
    if (ctxt_bd.pos_geo_debut_selection != ctxt_bd.pos_geo_fin_selection)
      {
			DWORD pos_geo_grand = MAX (ctxt_bd.pos_geo_debut_selection, ctxt_bd.pos_geo_fin_selection);
      if (ctxt_bd.pos_geo_consulte_selection <= (pos_geo_grand - 1))
        {
				ETAT_LIGNE_BD type;
				UL v_ul[nb_max_ul];
				int nb_ul;
				BOOL bAccepteDansBd;
				BOOL supprime_ligne;
				DWORD erreur_bd;
				int niveau;
				int indentation;
        (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(CONSULTE_LIGNE, ctxt_bd.pos_geo_consulte_selection, v_ul,
                                           &nb_ul, &supprime_ligne, &bAccepteDansBd,
                                           &erreur_bd, &niveau, &indentation);
        v_ul_to_texte (nb_ul, v_ul, texte, dwTailleTexte);
        typer_ligne (texte, &type);
        mettre_en_forme (texte, indentation, type);
        (*pbPossible) = TRUE;
        ctxt_bd.pos_geo_consulte_selection++;
        }
      }
    }
  }

//-----------------------------------------------------------
void supprimer_selection_bd (DWORD *pdwErreurBd)
  {
  DWORD pos_geo_petit = MIN (ctxt_bd.pos_geo_debut_selection, ctxt_bd.pos_geo_fin_selection);
  DWORD pos_geo_grand = MAX (ctxt_bd.pos_geo_debut_selection, ctxt_bd.pos_geo_fin_selection);
  (*pdwErreurBd) = 0;

  if (pos_geo_petit >= ctxt_bd.pos_geo_depart)
    {
    for (DWORD pos_geo = pos_geo_grand-1; ((pos_geo >= pos_geo_petit) && (*pdwErreurBd == 0)); pos_geo--)
      {
			UL v_ul[nb_max_ul];
			int nb_ul;
			BOOL bAccepteDansBd;
			BOOL supprime_ligne;
			int niveau;
			int indentation;

      (aDescripteurs[ctxt_bd.n_desc].pfnValideLigne)(SUPPRIME_LIGNE, pos_geo, v_ul,
				&nb_ul, &supprime_ligne, &bAccepteDansBd, pdwErreurBd, &niveau, &indentation);
      }

    if (ctxt_bd.pos_geo_segment >= pos_geo_petit)
      {
      if (pos_geo_petit > ctxt_bd.pos_geo_depart)
        {
        ctxt_bd.pos_geo_segment = pos_geo_petit - 1;
        }
      else
        {
        ctxt_bd.pos_geo_segment = ctxt_bd.pos_geo_depart;
        }
      }
    }
  } // supprimer_selection_bd

//-----------------------------------------------------------
//  Passe � l'interpreteur une intruction de selection d'objet hors fenetre texte
void selection_item_bd (DWORD offset)
  {
	//  $$ peut �tre inutile car utilis� pour l'ancienne REQUETE_EDIT selectionne sans effet
  if (offset == 0)
    {
    (aDescripteurs [ctxt_bd.n_desc].pfnFin)(FALSE, ctxt_bd.pos_geo_segment);
    }
  }

//-----------------------------------------------------------
// R�cup�re l'identificateur et l'adresse de la Window Proc
// de la bo�te a afficher.
// En entr�e, on passe le num�ro de la bo�te parmi les bo�tes du descripteur.
BOOL lis_param_boite_dlg (int index_boite, UINT *id_dlg_boite, FARPROC * ptr_wnd_proc_boite)
  {
  BOOL trouve = FALSE;

  if (aDescripteurs [ctxt_bd.n_desc].pfnLisParamDlg != NULL)
    {
    (aDescripteurs [ctxt_bd.n_desc].pfnLisParamDlg)(index_boite,id_dlg_boite, ptr_wnd_proc_boite);
    trouve = TRUE;
    }
  return trouve;
  }

//-----------------------------------------------------------
// Renvoie le niveau de la boite de dialogue � ouvrir suivant
// la ligne � modifier. Si la ligne est vide ou nouvelle, renvoyer 0.
// on renvoie aussi la chaine (ligne de description)
int lis_niveau_ligne_bd (DWORD ligne, char *chaine, int *indentation, DWORD dwTailleTexte)
  {
  ETAT_LIGNE_BD type_ligne_bd;
  BOOL bPossible;
  int niveau;

  consulter_bd (ligne, chaine, &type_ligne_bd, &bPossible, &niveau, indentation, dwTailleTexte);
  if ((type_ligne_bd == ligne_invalide) && bPossible)
    {
    niveau = 0;
    indentation = 0;
    chaine[0] = '\0';
    }
  return niveau;
  }

//--------------------------- fin Bdcf.cpp --------------------------------


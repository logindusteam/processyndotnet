/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgco.c                                               |
 |   Auteur  :	LM							|
 |   Date    :	01/02/92						|
 |   Remarques : gestion des boites de dialogue dans le descripteur     |
 |               code                                                   |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "appli.h"
#include "MemMan.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "IdConfLng.h"
#include "UEnv.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "ActionCf.h"
#include "Verif.h"

#include "pcsdlgco.h"

VerifInit;

typedef struct
	{
	t_dlg_desc echange_data;
	char ligne_description[255];
	DWORD index_variables;
	DWORD index_fonctions;
	} ENV, *PENV;

static char mot_res[40];

#define NB_CAR_MAX_VAR 20
#define NB_CAR_MAX_LIGNE 300

#define nb_instructions 66
static ID_MOT_RESERVE num_instruction[nb_instructions] = 
	{
	c_res_reflet, c_res_si, c_res_alors, c_res_sinon, c_res_fin_cond, // 18,  20,  42,  40,  41,	// 5 par 5
	c_res_execute_boucle, c_res_fin_exec_boucle, c_res_trans_tableau, c_res_ecrire_ecran, c_res_ecrire_imp_memeligne, //257, 258,	269,	64,	202,
	c_res_ecrire_imprimante, c_res_ecrire_disque, c_res_ouvre_disque, c_res_ferme_disque, c_res_lire_disque, // 65,  66,  97,  98,  80,
	c_res_efface_disque, c_res_copie_disque, c_res_renomme_disque, c_res_lance_m, c_res_arrete_m, //242, 243,	306,   4,	  5,
	c_res_lance_c, c_res_relance_c, c_res_arrete_c, c_res_execute, c_res_fin_exec, //  2,   7,	  3, 148, 149,
	c_res_pointe, c_res_detecte, c_res_execut_fonction, c_res_sinus, c_res_cosinus, //163, 164,	253,	46,	 47,
	c_res_tangente, c_res_log_10, c_res_logarithme, c_res_exponentiel, c_res_absolue, // 48,  57,	 34,	45,	 49,
	c_res_carre, c_res_racine, c_res_partie_entiere, c_res_passe_valeur, c_res_passe_valeur_vers_h, // 50, 	51,  58,  81,  82,
	c_res_passe_valeur_vers_b, c_res_prend_valeur, c_res_prend_valeur_vers_h, c_res_prend_valeur_vers_b, c_res_quitte_valeur, // 83,  84,  86,  88,  85,
	c_res_quitte_valeur_vers_h, c_res_quitte_valeur_vers_b, c_res_stable, c_res_change, c_res_commence_par, // 87,  89,  90,  91,  67,
	c_res_fini_par, c_res_bittest, c_res_car, c_res_val, c_res_bin, // 68, 241,  99,   1,  62,
	c_res_valbin, c_res_valbcd, c_res_bcd, c_res_ou, c_res_et, // 63,  61,  60,  35,  37,
	c_res_ou_x, c_res_non, c_res_tronc, c_res_HEXA, c_res_VALHEXA, // 36,  30,  69, 423, 424
	c_res_ECRIRE_IMP_BRUT
	};

// -----------------------------------------------------------------------
// fonctions relatives a la wndproc
// -----------------------------------------------------------------------

// -----------------------------------------------------------------------
// parametres par defaut
static void defaut_code (HWND hwnd, PENV param_code, BOOL init) 
  {
  if (init)
    {
    param_code->index_variables      = 1;
    param_code->index_fonctions      = 1;
    param_code->ligne_description[0] = '\0';
    }
  SetDlgItemText (hwnd, ID_CO1_DESCRIPTION, "");
  }

// -----------------------------------------------------------------------
static void fabrication_ul_code (HWND hwnd, char *chaine, PENV param_code)
  {
  char buffer[255];

  StrSetNull (chaine);
  // r�cup�ration du nom de variable
  GetDlgItemText (hwnd, ID_CO1_DESCRIPTION, buffer, 255);
  StrCopy (param_code->ligne_description, buffer);
  StrCopy (chaine, buffer);
  }

// -----------------------------------------------------------------------
//Boite de dialogue d'aide � la saisie du code
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgCode (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  mres = TRUE;			   // Valeur de retour
  PENV pEnvDlgCode;
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc *	pechange_data = (t_dlg_desc *) mp2;
			DWORD nb_enr;
			DWORD index;

			// allocation de memoire pour le pteur sur la structure
			pEnvDlgCode = (PENV)pCreeEnv (hwnd, sizeof (ENV));
			pEnvDlgCode->echange_data = (*pechange_data);

      // set edit box text size limit
			SendDlgItemMessage(hwnd, ID_CO1_DESCRIPTION, EM_SETLIMITTEXT , (WPARAM)NB_CAR_MAX_LIGNE, 0);

			// list_box liste des variables
			nb_enr = pEnvDlgCode->echange_data.nb_variables;
			for (index = 1; index < nb_enr; index++)
				{
				char szNomES[c_nb_car_es];
				NomES (szNomES, index);
				SendDlgItemMessage (hwnd, ID_CO1_VARIABLES, LB_ADDSTRING,
					0, (LPARAM)((PSTR) szNomES));
				}

			// list_box liste des instructions
			for (index = 0; index < nb_instructions; index++)
				{
				bMotReserve (num_instruction[index], mot_res);
				SendDlgItemMessage (hwnd, ID_CO1_FONCTIONS, LB_ADDSTRING,
					0, (LPARAM)((PSTR) mot_res));
				}

      // initialisation des champs de la boite
      if (StrIsNull (pEnvDlgCode->echange_data.chaine))
        // pas double clique : on veut inserer
        defaut_code (hwnd, pEnvDlgCode, TRUE);
      else
        { 
				// double clique : on veut modifier, il faut r�afficher la ligne
        StrDeleteFirstSpaces (pEnvDlgCode->echange_data.chaine);
        StrCopy (pEnvDlgCode->ligne_description, pEnvDlgCode->echange_data.chaine);
			  SetDlgItemText (hwnd, ID_CO1_DESCRIPTION, pEnvDlgCode->ligne_description);
        }
      mres = TRUE;
			}
      break;

    case WM_COMMAND:
			{
			BOOL  commande_a_executer = FALSE;
			t_retour_action retour_action;

      switch(LOWORD(mp1))
        {
				t_param_action  param_action;
				int item;
				char texte[NB_CAR_MAX_VAR];
				LRESULT resu;
				DWORD deb;
				DWORD fin;

				case IDCANCEL:        
				case ID_CO1_FERMER:
					{
					pEnvDlgCode = (PENV)pGetEnv (hwnd);
					EndDialog(hwnd, FALSE);
					}
					break;

        case ID_CO1_VARIABLES:
					{
          if (HIWORD(mp1) == LBN_DBLCLK)
						{
						// r�cup�re l'index du texte s�lectionn�
						item = (WORD)(SendDlgItemMessage (hwnd, ID_CO1_VARIABLES, LB_GETCURSEL, 0, 0));
						if (item == LB_ERR)
							break;

						// lit le texte s�lectionn�, ajoute lui ' ' en fin
            SendDlgItemMessage(hwnd, ID_CO1_VARIABLES, LB_GETTEXT, (WPARAM)item, (LPARAM)texte);
						StrConcatChar (texte, ' ');

						// recupere l'ID qui a le focus
						HWND hwndInitial = GetFocus (); //$$ �ventuellement NULL
						// donne le focus � la fen�tre texte
						SetFocus (::GetDlgItem(hwnd, ID_CO1_DESCRIPTION));
						// lecture de la position du curseur ou du texte s�lectionn�
						resu = SendDlgItemMessage(hwnd, ID_CO1_DESCRIPTION, EM_GETSEL, 0, 0);

						// ins�re le texte ds la ligne de description
						SendDlgItemMessage(hwnd, ID_CO1_DESCRIPTION, EM_REPLACESEL, TRUE, (LPARAM)texte);

						// repositionne le curseur
						deb = (WORD) (resu) + StrLength(texte);
						fin = deb;
						SendDlgItemMessage(hwnd, ID_CO1_DESCRIPTION, EM_SETSEL,(WPARAM)deb, (LPARAM)fin);

						// redonne le focus � la fen�tre qui l'avait
						SetFocus (hwndInitial);
						} // if (HIWORD(mp1) == LBN_DBLCLK)
					}
          break;

        case ID_CO1_FONCTIONS:
					{
          if (HIWORD(mp1) == LBN_DBLCLK)
            {
            // r�cup�re l'index du texte s�lectionn�
            item = (int) (WORD)(SendDlgItemMessage(hwnd, ID_CO1_FONCTIONS, LB_GETCURSEL, 0, 0));
            if (item == LB_ERR)
              break;

						// lit le texte s�lectionn�, ajoute lui ' ' en fin
            SendDlgItemMessage(hwnd, ID_CO1_FONCTIONS, LB_GETTEXT, (WPARAM)item, (LPARAM)texte);
            StrConcatChar(texte, ' ');

            // recupere l'ID qui a le focus
            HWND hwndInitial = GetFocus ();
            // donne le focus � la fen�tre texte
            SetFocus (::GetDlgItem(hwnd, ID_CO1_DESCRIPTION));
            // lecture de la position du curseur ou du texte s�lectionn�
            resu = SendDlgItemMessage(hwnd, ID_CO1_DESCRIPTION, EM_GETSEL, 0, 0);
						// ins�re le texte ds la ligne de description
						SendDlgItemMessage(hwnd, ID_CO1_DESCRIPTION, EM_REPLACESEL, TRUE, (LPARAM)texte);
            // repositionne le curseur
            deb = (WORD) (resu) + StrLength(texte);
            fin = deb;
            SendDlgItemMessage(hwnd, ID_CO1_DESCRIPTION, EM_SETSEL, (WPARAM)deb, (LPARAM)fin);
            // redonne le focus � la fen�tre qui l'avait
            SetFocus (hwndInitial);
						} // if (HIWORD(mp1) == LBN_DBLCLK)
					}
	        break;

				case ID_CO1_INSERER:
					{
					pEnvDlgCode = (PENV)pGetEnv (hwnd);

					pEnvDlgCode->echange_data.n_ligne_depart++;
					param_action.entier = pEnvDlgCode->echange_data.n_ligne_depart;
					bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);

					fabrication_ul_code (hwnd, param_action.chaine, pEnvDlgCode);
					param_action.entier = pEnvDlgCode->echange_data.n_ligne_depart;
					bAjouterActionCf (action_validation_dlg, &param_action);

					defaut_code (hwnd, pEnvDlgCode, TRUE);

					commande_a_executer = TRUE;
					}
					break;

				case ID_CO1_MODIFIER:
					{
					pEnvDlgCode = (PENV)pGetEnv (hwnd);
					fabrication_ul_code (hwnd, param_action.chaine, pEnvDlgCode);
					param_action.entier = pEnvDlgCode->echange_data.n_ligne_depart;
					bAjouterActionCf (action_validation_dlg, &param_action);
					commande_a_executer = TRUE;
					}
					break;

				case ID_CO1_DEFAUT:
					{
					pEnvDlgCode = (PENV)pGetEnv (hwnd);
					defaut_code(hwnd, pEnvDlgCode, FALSE);
					}
					break;

				case ID_CO1_RETABLIR:
					{
					pEnvDlgCode = (PENV)pGetEnv (hwnd);
				  SetDlgItemText (hwnd, ID_CO1_DESCRIPTION, pEnvDlgCode->ligne_description);
					}
					break;

        } // switch(LOWORD(mp1))

			if (commande_a_executer)
				{
				pEnvDlgCode = (PENV)pGetEnv (hwnd);
				bExecuteActionCf (&retour_action);
				pEnvDlgCode->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
				StrCopy (pEnvDlgCode->echange_data.chaine, retour_action.chaine);
				}
			}
    break;

    case WM_CLOSE:
      {
      EndDialog(hwnd, FALSE);
      pEnvDlgCode = (PENV)pGetEnv (hwnd);
			}
      break;

    case WM_DESTROY:
      pEnvDlgCode = (PENV)pGetEnv (hwnd);
		  MemLibere ((PVOID*)&pEnvDlgCode); // libere la place allouee pour le pteur sur la structure
			break;

    default:
      mres = FALSE;
      break;
    }

  return mres;
  }


// -------------------------------------------------------------------
// R�cup�re l'identificateur et l'adresse de la Window Proc
// de la bo�te a afficher. En entr�e, on passe le num�ro de la bo�te parmi
//les bo�tes du descripteur.
// --------------------------------------------------------------------*/
void LIS_PARAM_DLG_CO (int index_boite, UINT *iddlg, FARPROC * adr_winproc)
  {
  switch (index_boite)
    {
    case 0 : /* canal, esclave, type variables, variables */
      *iddlg = DLG_INTERPRETE_CO1;
      (*adr_winproc) = (FARPROC) dlgCode;
      break;

    default:
			VerifWarningExit;
      break;
    }
  }


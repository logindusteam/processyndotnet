// --------------------------------------------------------------------------
// Gestion de threads
//

#include "stdafx.h"
#include <process.h>
#include <stddef.h>

#include "std.h"
#include "UStr.h"
#include "USem.h"
#include "MemMan.h"
#include "Verif.h"
#include "UExcept.h"
#include "threads.h"

VerifInit;

#define TACHE_INVALIDE 0

// Structure interne d'un objet point� par un handle thread
typedef struct
  {
  HSEM		hsemAcces;							// Semaphore d'ac�s exclusif � cette structure
  HSEM	  hsemAttenteMessage;			// Semaphore de demande d'execution d'une fonction
  HSEM		hsemTacheTraiteMessage;	// Pris si la tache est en train de traiter un message
  HANDLE	handleTache;						// handle Win32 de la tache
	PFNTHREAD	pfnThread;						// adresse du thread � ex�cuter
  DWORD		dwIdMessage;						// Id du message � �x�cuter
  DWORD		dwTimeOut;							// attente sur lecture fonction
  PVOID		pParamIn;								// Param�tre Message 1
  PVOID		pParamOut;							// Param�tre Message 2
  DWORD *	pdwError;								// ? Ptr sur erreur retourn�e par la fonction execut�e
  }
  THREAD_LI;

typedef THREAD_LI * PTHREAD_LI;

// -------------------------------------------------------
// Fonction thread permettant de faire un appel prot�g� des exceptions
// Au thread utilisateur
static UINT __stdcall uExecuteProtegeThread (void * hThrd)
  {
	UINT uRes;
	__try 
		{
		// appel du thread utilisateur
		uRes = ((PTHREAD_LI)hThrd)->pfnThread (hThrd);
		} // __try 
	__except (nSignaleExceptionUser(GetExceptionInformation()))
		{
		ExitProcess((UINT)-1);// handler des exceptions choisies par le filtre
		}

	return uRes;
	}

//---------------------------------------------------------------------------
static void LibereRessourcesThread (PTHREAD_LI pThreadData)
  {
	if (pThreadData)
		{
		//attente acc�s aux donn�es du thread
		dwSemAttenteLibre (pThreadData->hsemAcces, INFINITE);
  
		// lib�ration des ressources s�maphores
		if (pThreadData->hsemAttenteMessage)
			bSemFerme (&pThreadData->hsemAttenteMessage);
		if (pThreadData->hsemTacheTraiteMessage)
			bSemFerme (&pThreadData->hsemTacheTraiteMessage);
		if (pThreadData->hsemAcces)
			bSemFerme (&pThreadData->hsemAcces);

		if (pThreadData->handleTache != TACHE_INVALIDE)
			{
			// Une exception monte si le handle est invalide
			CloseHandle (pThreadData->handleTache);
			pThreadData->handleTache = TACHE_INVALIDE;
			}

		// lib�ration de la structure de contr�le
		MemLibere ((PVOID *)(&pThreadData));
		}
  }


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							fonctions export�es
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
void ThreadSetDureeAttenteMessage (HTHREAD hThread, DWORD dwTimeOut)
  {
  PTHREAD_LI pThreadData = (PTHREAD_LI)hThread;
	Verif (pThreadData);
  //
  // fixe le time out d'attente
  if (dwSemAttenteLibre (pThreadData->hsemAcces, INFINITE) == 0)
		{
		pThreadData->dwTimeOut = dwTimeOut;
	  bSemLibere (pThreadData->hsemAcces);
		}
  }

// --------------------------------------------------------------------------
BOOL bThreadAttenteMessage 
	(
	HTHREAD hThread,
	PVOID * ppParamIn, PVOID * ppParamOut, DWORD ** ppdwError, UINT * pdwIdMessage // NULLs si non utilis�s
	)
  {
  PTHREAD_LI	pThreadData = (PTHREAD_LI)hThread;
  DWORD        dwTimeOut;
	BOOL				bRes;

	Verif (pThreadData);

  // r�cup�re le time out courant
  Verif (!dwSemAttenteLibre (pThreadData->hsemAcces, INFINITE));
  dwTimeOut = pThreadData->dwTimeOut;
  bSemLibere (pThreadData->hsemAcces);

  // attends le feu vert pour ex�cuter une 'fonction'
  dwSemAttenteLibre (pThreadData->hsemAttenteMessage, dwTimeOut);

	// Ok r�cup�re les param�tres courants pour cette ex�cution
  Verif (!dwSemAttenteLibre (pThreadData->hsemAcces, INFINITE));

	// Valeur retourn�e : continuer
	bRes = (pThreadData->dwIdMessage != THRD_FCT_END);

	// Renvoie � la tache les param�tres du message
	if (pdwIdMessage)
		(*pdwIdMessage) = pThreadData->dwIdMessage;

	if (ppParamIn)
		(*ppParamIn)  = pThreadData->pParamIn;

  if (ppParamOut)
		(*ppParamOut) = pThreadData->pParamOut;

  if (ppdwError)
		(*ppdwError)  = pThreadData->pdwError;

  // Note : ex�cution fonction en cours
  bSemPrends (pThreadData->hsemAttenteMessage);

	// fin acc�s
  bSemLibere (pThreadData->hsemAcces);

  return bRes;
  } // bThreadAttenteMessage

//---------------------------------------------------------------------------
// Le thread signale que le traitement du message est termin�e
void ThreadFinTraiteMessage (HTHREAD hThread)
  {
  PTHREAD_LI pThreadData = (PTHREAD_LI)hThread;

	Verif (pThreadData);

  dwSemAttenteLibre (pThreadData->hsemAcces, INFINITE);

  // Notification de Fin de traitement du message
  bSemLibere (pThreadData->hsemTacheTraiteMessage);

  bSemLibere (pThreadData->hsemAcces);
  } // ThreadFinTraiteMessage

//---------------------------------------------------------------------------
// Notifie le gestionnaire de la fin d'ex�cution de la t�che
// renvoie le code d'erreur sp�cifi� ou -1 s'il y a une erreur
// Les ressources de l'objet thread ne sont pas lib�r�es !
UINT uThreadTermine (HTHREAD hThread, UINT uRetCode)
  {
  PTHREAD_LI pThreadData = (PTHREAD_LI)hThread;

	// acc�s � l'objet thread
	Verif (pThreadData);

	// pas de message en cours de traitement
  ThreadFinTraiteMessage (hThread);

  if (dwSemAttenteLibre (pThreadData->hsemAcces, INFINITE) != 0)
		uRetCode = (UINT)-1;

	// fin de l'acc�s � l'objet thread
  bSemLibere (pThreadData->hsemAcces);

	// Renvoie le code de retour
	return uRetCode;
  }

//--------------------------------------------------------
// Fonctions de pilotages de thread interdites aux threads
//--------------------------------------------------------

// cr�ation des infos de contr�le et lancement d'un thread
BOOL bThreadCree (PHTHREAD phThread, PFNTHREAD pfnThread, DWORD size_stack)
  {
  BOOL        bOk = FALSE;
  PTHREAD_LI pThreadData = (PTHREAD_LI)pMemAlloue (sizeof (THREAD_LI));

  if (pThreadData != NULL)
    {
		// Initialisation des donn�es de la structure thread
    pThreadData->hsemAcces		= hSemCree (FALSE);
    pThreadData->hsemAttenteMessage = hSemCreeAttenteSeule (TRUE);
    pThreadData->hsemTacheTraiteMessage	= hSemCreeAttenteSeule (FALSE);
    pThreadData->handleTache	= TACHE_INVALIDE;
    pThreadData->pfnThread		= pfnThread;
    pThreadData->dwIdMessage	= THRD_FCT_NOP;
    pThreadData->dwTimeOut		= 0;
    pThreadData->pParamIn			= NULL;
    pThreadData->pParamOut		= NULL;
    pThreadData->pdwError			= NULL;

    //cr�ation s�maphores Ok ?
		if (pThreadData->hsemAttenteMessage && pThreadData->hsemTacheTraiteMessage && pThreadData->hsemAcces)
			{
			UINT	uThreadId;

			// oui => Lance le thread prot�g� qui lancera le thread utilisateur
			pThreadData->handleTache = (HANDLE)_beginthreadex 
				(
				NULL, // security
				size_stack, uExecuteProtegeThread, pThreadData, 
				0, // initflag
				&uThreadId);

			// T�che cr��e ?
			if (pThreadData->handleTache != TACHE_INVALIDE)
				{
				// oui => renvoie le handle sur l'objet
				*phThread = (HTHREAD) pThreadData;
				bOk = TRUE;
				}
			else
				LibereRessourcesThread (pThreadData);
			}
    }
  return (bOk);
  } // bThreadCree

//---------------------------------------------------------------------------
// Envoi du message THRD_FCT_END � la t�che et attente de la fin de la t�che,
// puis lib�ration et remise � 0 de *phThread
void bThreadFerme (PHTHREAD phThread, DWORD dwTimeOut)
  {
  DWORD dwError;
  PTHREAD_LI pThreadData = (PTHREAD_LI) (*phThread);

  Verif (pThreadData);
	
	// envoi de la commande THRD_FCT_END au thread
	ThreadEnvoiMessage (*phThread, THRD_FCT_END, NULL, NULL, &dwError);

	// attente de son ex�cution ou du time out
	// ! remplac� par attente fin t�che
	//bThreadAttenteFinTraiteMessage (*phThread, dwTimeOut);

	// attente de la fin de la t�che
	Verif (dwSemAttenteLibre ((HSEM)pThreadData->handleTache, dwTimeOut)!= SEM_TIMEOUT);

	// Handle mis � 0
	*phThread = NULL;

	// lib�ration des ressources li�es � ce thread
	LibereRessourcesThread (pThreadData);

  } // bThreadFerme

//---------------------------------------------------------------------------
// Fait sortir le thread d'une AttenteMessage en lui fournissant ses param�tres
void ThreadEnvoiMessage (HTHREAD hThread, UINT dwIdMessage, PVOID pParamIn, PVOID pParamOut, DWORD *pdwError)
  {
  PTHREAD_LI pThreadData = (PTHREAD_LI)hThread;

  Verif (pThreadData);

	// Attente acc�s exclusif � l'objet
  dwSemAttenteLibre (pThreadData->hsemAcces, INFINITE);

  // La t�che fonctionne ?
	if (pThreadData->handleTache	!= TACHE_INVALIDE)
		{
		// oui => la t�che n'a pas le droit d'�tre en train de traiter un message
		Verif (dwSemAttenteLibre (pThreadData->hsemAttenteMessage , 0) == WAIT_TIMEOUT);

		// on note les param�tres du nouveau message � ex�cuter
		pThreadData->dwIdMessage	= dwIdMessage;
		pThreadData->pParamIn  = pParamIn;
		pThreadData->pParamOut = pParamOut;
		pThreadData->pdwError  = pdwError;
		// 
		bSemPrends (pThreadData->hsemTacheTraiteMessage);

		// Feu vert pour l'ex�cution du message
		bSemLibere (pThreadData->hsemAttenteMessage);
		}

  //
  bSemLibere (pThreadData->hsemAcces);
  }

//---------------------------------------------------------------------------
// Attends au plus dwTimeOut ms que le thread appelle ThreadFinTraiteMessage
BOOL bThreadAttenteFinTraiteMessage (HTHREAD hThread, DWORD dwTimeOut)
  {
  PTHREAD_LI pThreadData = (PTHREAD_LI)hThread;
  BOOL       bEnd;

	Verif (pThreadData);

  // Test Fin Execution Fonction Pr�cedente
  bEnd = (dwSemAttenteLibre (pThreadData->hsemTacheTraiteMessage, dwTimeOut) == 0);

  return (bEnd);
  }

//---------------------------------------------------------------------------
// ThreadEnvoiMessage + bThreadAttenteFinTraiteMessage (INFINITE)
DWORD dwThreadExecuteMessage (HTHREAD hThread, UINT IdMessage, void *pParamIn, void *pParamOut, DWORD *pdwError)
  {
  ThreadEnvoiMessage (hThread, IdMessage, pParamIn, pParamOut, pdwError);
  bThreadAttenteFinTraiteMessage (hThread, INFINITE);
  return ((*pdwError));
  }

//---------------------------------------------------------------------------
//  Fonctions pour tous
//---------------------------------------------------------------------------

// --------------------------------------------------------------------------
// fixe le niveau de priorit� du thread
BOOL bThreadSetPriorite (HTHREAD hThread, int thread_prty)
  {
  BOOL bOk = FALSE;
  PTHREAD_LI pThreadData = (PTHREAD_LI)hThread;

	// Fixe la priorit� du thread
  if (pThreadData && (dwSemAttenteLibre (pThreadData->hsemAcces, INFINITE) == 0))
		{
		bOk = SetThreadPriority (pThreadData->handleTache, thread_prty);
		bSemLibere (pThreadData->hsemAcces);
		}
  return (bOk);
  }

//---------------------------------------------------------------------------

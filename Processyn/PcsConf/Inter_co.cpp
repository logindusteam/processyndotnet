/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_co.c                                               |
 |   Auteur  : MC                                                       |
 |   Date    : 21/09/94                                                 |
 |   Version : 4.0							|
 |                                                                      |
 +----------------------------------------------------------------------*/

// interpreteur code
#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "tipe_co.h"
#include "gener_co.h"
#include "mem.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "pcsdlgco.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "inter_co.h"
#include "Verif.h"
VerifInit;

#define OFFSET_CONDITION 60

static DWORD ligne_depart_code;
static DWORD ligne_courante_code;
static BOOL code_deja_initialise;

static DWORD pos_deb_execute;
static char nom_structure_cour[c_taille_str];

//---------------------------------------------------------------------------
// Renvoie 0 ou le N� de ligne sp�cifiant la structure ex�cute dans laquelle se trouve la ligne sp�cifi�e
static DWORD val_pos_deb_execute (DWORD index_ligne)
  {
  BOOL trouve;
  PGEO_CODE ads_val_geo;
  PC_CODE	ads_val_code_suivant;
  PC_CODE	ads_val_code;
  DWORD dwRes = 0;

  StrCopy(nom_structure_cour,"");
  if ((existe_repere (b_code)) && (nb_enregistrements (szVERIFSource, __LINE__, b_code) != 0))
    {
    trouve = FALSE;
    while ((index_ligne > 1) && (!trouve))
      {
      index_ligne--;
      ads_val_geo = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code, index_ligne);
      ads_val_code = (PC_CODE)pointe_enr (szVERIFSource, __LINE__, b_code,ads_val_geo->dwNPremierCode);
      if (ads_val_code->GenreOpCode == op_reserve)
        {
        if (ads_val_code->donnee_source == c_res_execute)
          {
          dwRes = index_ligne;
          ads_val_code_suivant = (PC_CODE)pointe_enr (szVERIFSource, __LINE__, b_code, ads_val_geo->dwNPremierCode+1);
          consulte_cst_bd_c (ads_val_code_suivant->donnee_source,nom_structure_cour);
          trouve = TRUE;
          }
        else
          {
          trouve = (BOOL)(ads_val_code->donnee_source == c_res_fin_exec);
          }
        }
      } // while
    }
  return dwRes;
  }

//----------------------------------------------------------------------------
// R�cup�re le nb de conditions d'une ligne et l'impact de cette ligne sur les ligne suivantes
static int nNbImbricationsLigne 
	(
	DWORD dwNligne, // ligne pour laquelle on veut savoir
	PINT pnImpactSurLignesSuivante // *pnImpactSurLignesSuivante re�oit -1(fin_si)  0 ou 1(si)
	)
	{
	DWORD nNbImbrications = 0;

	// Pas d'impact par d�faut
  *pnImpactSurLignesSuivante = 0;

	// ligne 0 => nb conditions = 0
	if (dwNligne > 0)
		{
		// autre ligne : nb conditions = nb conditions de la ligne pr�c�dente
		PGEO_CODE pGeoCode = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,dwNligne);
		nNbImbrications = pGeoCode->nNbImbrications;

		// V�rifie si la premi�re instruction � un impact sur l'indentation :
		//(+ 1 si ligne pr�c�dente = SI, BOUCLE, EXECUTE, PROCEDURE)
		//(- 1 si ligne pr�c�dente = FIN_SI, FIN_BOUCLE, FIN_EXECUTE, RETOUR)
		PC_CODE	ads_instruction = (PC_CODE)pPointeEnrSansErr (szVERIFSource, __LINE__, b_code, pGeoCode->dwNPremierCode);
    if (ads_instruction && (ads_instruction->GenreOpCode == op_reserve))
      {
      switch (ads_instruction->donnee_source)
        {
        case c_res_si :
        case c_res_execute_boucle :
        case c_res_execute :
        case c_res_libelle_procedure :
          *pnImpactSurLignesSuivante = 1;
          break;
				case c_res_fin_cond:
        case c_res_fin_exec_boucle :
        case c_res_fin_exec :
        case c_res_retour :
          *pnImpactSurLignesSuivante = -1;
					break;
        }
      } // if 
		} // if (dwNligne > 1)
	return nNbImbrications;
	} // nNbImbricationsLigne

//----------------------------------------------------------------------------
// Insere une ligne de code d�ja analys�e � la ligne sp�cifi�e
static void insere_instructions (int nNbInstructions, PPARAM_COMPIL pams, DWORD dwNligne)
  {
	// Calcule le nombre de conditions s'appliquant � cette ligne � partir de la ligne pr�c�dente
  int deplace_cond;
	int nNbImbrications = nNbImbricationsLigne(dwNligne - 1, &deplace_cond);
	nNbImbrications += deplace_cond;

	// Insere une ligne au num�ro de ligne demand�e
  insere_enr (szVERIFSource, __LINE__, 1,b_geo_code, dwNligne);

	// recopie les infos de la ligne suivante sur la ligne demand�e
  trans_enr (szVERIFSource, __LINE__, 1, b_geo_code, b_geo_code, dwNligne + 1, dwNligne);

	// Mise � jour du nb de conditions de cette ligne
	PGEO_CODE pGeoCode = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,dwNligne);
	pGeoCode->nNbImbrications = nNbImbrications;

	// Affecte les nouvelles instructions � la ligne courante
  DWORD dwNPremierCode = pGeoCode->dwNPremierCode;
  insere_enr (szVERIFSource, __LINE__, nNbInstructions,b_code,dwNPremierCode);
  for (DWORD index = 0; index < (DWORD)nNbInstructions; index++)
    {
		PC_CODE	ads_instruction = (PC_CODE)pointe_enr (szVERIFSource, __LINE__, b_code,dwNPremierCode + index);
    ads_instruction->GenreOpCode = pams->tampon_instructions[index].GenreOpCode;
    ads_instruction->donnee_source = pams->tampon_instructions[index].donnee_source;
    if (pams->tampon_instructions[index].GenreOpCode == op_variable)
      ReferenceES(pams->tampon_instructions[index].donnee_source);
    }

	// r�cup�re l'impact de la nouvelle ligne
	nNbImbricationsLigne(dwNligne, &deplace_cond);

	// r�percute sur les lignes suivantes l'impact de l'insertion des instructions et des conditions
  DWORD max = nb_enregistrements (szVERIFSource, __LINE__, b_geo_code);
  for (DWORD index = dwNligne + 1; index <= max; index++)
    {
    pGeoCode = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,index);
    pGeoCode->dwNPremierCode = pGeoCode->dwNPremierCode + nNbInstructions;
    pGeoCode->nNbImbrications += deplace_cond;
    }
  } // insere_instructions

//----------------------------------------------------------------------------
static void supprime_instructions (DWORD dwNligne)
  {
	// d�termine l'impact de la ligne courante sur les lignes suivantes
  int  deplace_cond = 0;
	nNbImbricationsLigne(dwNligne, &deplace_cond);

	// d�r�f�rence toutes les instructions
  PGEO_CODE ads_debut = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,dwNligne);
  DWORD dwNPremierCode = ads_debut->dwNPremierCode;
  PGEO_CODE ads_suivant = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,dwNligne + 1);
  DWORD suivant = ads_suivant->dwNPremierCode;
  DWORD deplacement = suivant - dwNPremierCode;
	
  for (DWORD index = dwNPremierCode; index < suivant; index++)
    {
		PC_CODE	ads_instruction = (PC_CODE)pointe_enr (szVERIFSource, __LINE__, b_code,index);
    switch (ads_instruction->GenreOpCode)
      {
      case op_variable:
        DeReferenceES(ads_instruction->donnee_source);
        break;
      case op_constante :
        supprime_cst_bd_c(ads_instruction->donnee_source);
        break;
      //default:break;
      } // switch
    } // while

	// supprime les instructions
  enleve_enr (szVERIFSource, __LINE__, deplacement,b_code,dwNPremierCode);

	// supprime la ligne
  enleve_enr (szVERIFSource, __LINE__, 1,b_geo_code,dwNligne);

  // MAJ Nb conditions et Nb instruction des lignes suivant la suppression
  DWORD max = nb_enregistrements (szVERIFSource, __LINE__, b_geo_code);
  for (DWORD index = dwNligne; index <= max; index++)
    {
    ads_debut = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,index);
    ads_debut->dwNPremierCode = ads_debut->dwNPremierCode - deplacement;
    ads_debut->nNbImbrications -= deplace_cond;
    }
  }

//---------------------------------------------------------------
static void genere_vect_ul (PUL vect_ul, int *nb_ul, DWORD dwNligne, int *niveau, int *indentation)
  {
  *niveau = 0;
  PGEO_CODE ads_debut = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,dwNligne);
  DWORD dwNPremierCode = ads_debut->dwNPremierCode;
  (*indentation) = ads_debut->nNbImbrications;
  PGEO_CODE ads_suivant = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,dwNligne + 1);
  DWORD suivant = ads_suivant->dwNPremierCode;
  (*nb_ul) = (int)(suivant - dwNPremierCode);
  
  for (DWORD index = 0;index < (DWORD)(*nb_ul); index++)
    {
		PC_CODE ads_instruction = (PC_CODE)pointe_enr (szVERIFSource, __LINE__, b_code,dwNPremierCode + index);
		C_CODE instruction = (*ads_instruction);
    vect_ul[index].erreur=0;
    switch (instruction.GenreOpCode)
      {
      case op_variable:
				NomVarES (vect_ul[index].show,instruction.donnee_source);
				break;
      case op_constante:
				consulte_cst_bd_c (instruction.donnee_source,vect_ul[index].show);
				break;
      case op_reserve:
				{
				ID_MOT_RESERVE IdMotRes = (ID_MOT_RESERVE)instruction.donnee_source;
				bMotReserve(IdMotRes,vect_ul[index].show);
				if ((*indentation) > 0)
					{
		      switch (IdMotRes)
						{
						case c_res_sinon:
						case c_res_fin_cond:
						case c_res_fin_exec_boucle :
						case c_res_fin_exec :
						case c_res_retour :
							(*indentation)--;
							break;
						}
					}
				}
				break;
			}
    }
  }

//----------------------------------------------------------------------------
static void dereference_cst(PPARAM_COMPIL pams,int  nb_ul)
  {
  for (int i = 0;i < nb_ul;i++)
    {
    if (pams->tampon_instructions[i].GenreOpCode == op_constante)
      supprime_cst_bd_c(pams->tampon_instructions[i].donnee_source);
    }
  }

//----------------------------------------------------------------------------
static BOOL AnalyseLigneCode (PUL vect_ul, int nb_ul, DWORD *erreur, PPARAM_COMPIL pams)
  {
  DWORD position_cst;
  ID_MOT_RESERVE num_fonction;
  DWORD pos_ident;
  BOOL exist_ident;
  FLOAT  val_num;
  BOOL  val_log;
  BOOL  un_log;
  BOOL  un_num;
  BOOL  exist_cst;
  int  i;

  //***********************************************
  #define err(code) (*erreur) = code;return (FALSE)  //Sale Sioux
  //***********************************************

  (*erreur) = 0;
  for (i = 0; i<nb_ul; i++)
    pams->tampon_instructions[i].GenreOpCode = op_bizarre ;

  for (i = 0;i<nb_ul;i++)
    {
    if (bReconnaitESValide((vect_ul+i),&exist_ident,&pos_ident) && (exist_ident))
      {
      pams->tampon_instructions[i].GenreOpCode = op_variable;
      pams->tampon_instructions[i].donnee_source= dwIdVarESFromPosIdentif (pos_ident);
      }
    else
      {
      if (bReconnaitMotReserve ((vect_ul+i)->show,&num_fonction))
        {
        pams->tampon_instructions[i].GenreOpCode = op_reserve;
        pams->tampon_instructions[i].donnee_source = num_fonction;
        }
      else
        {
        if (bReconnaitConstante((vect_ul+i),&val_log,&val_num,&un_log,&un_num,
                       &exist_cst,&position_cst))
          {
          pams->tampon_instructions[i].GenreOpCode = op_constante;
          AjouteConstanteBd(position_cst,exist_cst,vect_ul[i].show,
                        &(pams->tampon_instructions[i].donnee_source));
          }
        else
          {
          err(IDSERR_LA_DESCRIPTION_EST_INVALIDE) ;
          } // test si constante
        } // test si reserve
      } // test si es
    } // for
  pams->erreur = 0 ;
  pams->interpreteur = TRUE;
  pams->instruc_source_courante = 1;
  StrCopy(pams->nom_structure_cour,nom_structure_cour);
  pams->pos_deb_execute = pos_deb_execute;
  pams->fin_instructions_ligne = nb_ul; //nb_instructions ligne
  compile_ligne(pams);
  pos_deb_execute = pams->pos_deb_execute;
  StrCopy(nom_structure_cour, pams->nom_structure_cour);
  (*erreur) = pams->erreur;
  return pams->erreur == 0;
  }

//--------------------------------------------------------------------------
static void valide_ligne_code (REQUETE_EDIT action, DWORD dwNligne,
	PUL vect_ul,int *nb_ul,
	BOOL *supprime_ligne, BOOL *accepte_ds_bd,
	DWORD *erreur_val, int *niveau, int *indentation)
  {
	PARAM_COMPIL	ParamCompil;
  PPARAM_COMPIL	pams = &ParamCompil;
  DWORD erreur;
	
  (*niveau) = 0;
  (*erreur_val) = 0;
  switch (action)
    {
    case MODIFIE_LIGNE:
			{
      //***** mise a jour contexte ****************************
      pos_deb_execute = val_pos_deb_execute (dwNligne);
      //*******************************************************
			if (AnalyseLigneCode (vect_ul,(*nb_ul),&erreur,pams))
				{
				supprime_instructions(dwNligne);
				insere_instructions(*nb_ul,pams,dwNligne);
				}
			else
				{
				dereference_cst(pams,(*nb_ul));
				(*erreur_val) = erreur;
				}
			(*accepte_ds_bd) = TRUE;
			(*supprime_ligne) = FALSE;
			}
			break;
			
		case INSERE_LIGNE:
			{
			// mise a jour contexte
			pos_deb_execute = val_pos_deb_execute (dwNligne);

			if (AnalyseLigneCode (vect_ul,(*nb_ul),&erreur,pams))
				{
				insere_instructions(*nb_ul,pams,dwNligne);
				(*accepte_ds_bd) = TRUE;
				supprime_ligne = FALSE;
				}
			else
				{
				dereference_cst(pams,(*nb_ul));
				accepte_ds_bd = FALSE;
				(*erreur_val) = erreur;
				}
			}
			break;
			
		case SUPPRIME_LIGNE :
			{
			supprime_instructions(dwNligne);
			(*accepte_ds_bd) = TRUE;
			(*supprime_ligne) = TRUE;
			}
			break;
			
		case CONSULTE_LIGNE:
			{
			genere_vect_ul (vect_ul,nb_ul,dwNligne,niveau,indentation);
			}
			break;
			
		default:
			break;
    }
  } // valide_ligne_code

// ---------------------------------------------------------------------------
static void init_code (DWORD *depart, DWORD *courant, char *nom_applic)
	{
	PGEO_CODE pGeoCode;
	
	if (!code_deja_initialise)
		{
		if (!existe_repere (b_geo_code))
			{
			cree_bloc (b_geo_code,sizeof(*pGeoCode),1);
			pGeoCode = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,1);
			pGeoCode->dwNPremierCode = 1;
			pGeoCode->nNbImbrications = 0;
			pos_deb_execute = 0;
			StrCopy(nom_structure_cour,"");
			}
		if (!existe_repere (b_code))
			{
			cree_bloc (b_code,sizeof (C_CODE),0);
			}
		ligne_depart_code = 1;
		ligne_courante_code = ligne_depart_code;
		code_deja_initialise = TRUE;
		}
	(*depart) = ligne_depart_code;
	(*courant) = ligne_courante_code;
	}
// ---------------------------------------------------------------------------
static DWORD lis_nombre_de_ligne_code (void)
  {
  DWORD dwRet = 0;

  if (existe_repere (b_geo_code))
    {
    dwRet = nb_enregistrements (szVERIFSource, __LINE__, b_geo_code) - 1;
    }

  return dwRet;
  }

// ---------------------------------------------------------------------------
// Mise � jour chaine tableau de bord
static void set_tdb_code (DWORD dwNligne, BOOL nouvelle_ligne, char *tdb)
  {
	// Par d�faut pas de commentaires
	StrSetNull (tdb);

	// r�cup�re le nb d'imbrications de la ligne courante
  int deplace_cond;
	int nNbImbrications = nNbImbricationsLigne(dwNligne, &deplace_cond);
	if (nouvelle_ligne)
		nNbImbrications += deplace_cond;

	// Nombre d'imbrications != 0 ?
	if (nNbImbrications!= 0)
		{
		// oui => indique le nombre d'imbrications
		char szTemp[c_taille_str];
		StrINTToStr (tdb, nNbImbrications);
		bLngLireInformation (c_inf_imbrications, szTemp);
		StrConcatChar (tdb, ' ');
		StrConcat (tdb, szTemp);
		}
	// si != fin_si sur la derni�re ligne de code ?
	int nNbConditionsFin = nNbImbricationsLigne(lis_nombre_de_ligne_code(), &deplace_cond);
	nNbConditionsFin += deplace_cond;

  if (nNbConditionsFin != 0)
    { 
		// oui : rajoute (fin = n)
    StrConcat (tdb, " (");
		char szTemp[c_taille_str];
    bLngLireInformation (c_inf_fin, szTemp);
    StrConcat (tdb, szTemp);
		StrINTToStr (szTemp, nNbConditionsFin);
		StrConcat(tdb, " = ");
		StrConcat (tdb, szTemp);
		StrConcatChar (tdb, ')');
    }

  // Mise a jour nom ex�cution en cours
	if (nom_structure_cour[0])
		{
		StrConcat (tdb, " ");
		val_pos_deb_execute (dwNligne);
		StrConcat (tdb, nom_structure_cour);
		}
  } // set_tdb_code

// ---------------------------------------------------------------------------
static void fin_code (BOOL quitte, DWORD courant)
  {
  if ((!quitte))
    {
    ligne_courante_code = courant;	//$$ presque modulaire...
    }
  }

// ---------------------------------------------------------------------------
static void creer_code (void)
  {
  code_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
// Interface du descripteur code
void LisDescripteurCo (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_code;
	pDescripteurCf->pfnInitDesc = init_code;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_code;
	pDescripteurCf->pfnValideLigne = valide_ligne_code;
	pDescripteurCf->pfnSetTdb = set_tdb_code;
	pDescripteurCf->pfnLisParamDlg = LIS_PARAM_DLG_CO;
	pDescripteurCf->pfnFin = fin_code;
	}

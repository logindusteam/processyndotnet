// interface generation du code executable

#define c_taille_str   255


typedef struct
	{
	DWORD position;
	BOOL sinon_present,boucle;
	}enr_pile;

typedef struct
	{
	P_CODE memorisation,indice;
	}enr_boucle;

typedef enr_pile t_pile_cond[80];

typedef enr_boucle t_pile_boucle[80];

typedef struct
	{
	DWORD fin_instructions_ligne;
	DWORD depart_instruc_courante;
	DWORD instruc_source_courante;
	DWORD instruc_courante;
	DWORD erreur;
	DWORD sp_cond;
	DWORD pos_deb_execute;
	DWORD sp_cond_deb_execute;
	DWORD ligne_courante;
	t_pile_cond pile_cond;
	int sp_libelle;
	int sp_boucle;
	t_tampon_instructions tampon_instructions;
	P_CODE instruc_memorisee;
	char nom_structure_cour[c_taille_str];
	t_pile_boucle pile_boucle;
	BOOL mode_debugger;
	BOOL presence_libelle;
	BOOL presence_main;
	BOOL interpreteur;
	}PARAM_COMPIL, *PPARAM_COMPIL;



DWORD genere_co(DWORD *ligne);

void compile_ligne(PPARAM_COMPIL pams);

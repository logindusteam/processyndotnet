/*-------------------------------------------------------------------------*
 | Ce fichier est la propriete de					   |
 |									   |
 |			 Societe LOGIQUE INDUSTRIE			   |
 |	       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		   |
 |									   |
 | Il demeure sa propriete exclusive et est confidentiel.		   |
 | Aucune diffusion n'est possible sans accord ecrit.                      |
 |-------------------------------------------------------------------------|
 | Titre...: pmnucf.c	                                        	   |
 | Auteur..: JB 							   |
 | Date....: 14/04/92							   |
 *-------------------------------------------------------------------------*/

#include "stdafx.h"
#include "pmnucf.h"

// -----------------------------------------------------------------------
// Envoie une notification � une fen�tre
// Remarques: Cette notification est envoy�e sous forme d'un WM_USER � la fen�tre
//				avec (USHORT) (wParam) == notification
//	      HIWORD (mp1) == id	�metteur
//	      (USHORT) (mp2) == parametre 1
//	      HIWORD (mp2) == parametre 2
// -----------------------------------------------------------------------
LRESULT notification_user
	(
	HWND hwnd,						// hwnd destinataire
	UINT id,							// Id de l'�metteur
	USHORT notification,	// voir NU0_WND...
	SHORT val_1,					// param�tre 1
	SHORT val_2						// param�tre 2
	)	// retour = valeur renvoy�e par le destinataire
  {
  return (::SendMessage (hwnd, WM_USER, MAKEWPARAM (notification, id), MAKELPARAM (val_1, val_2)));
  }

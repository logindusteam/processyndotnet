// Module...: WLigne.h
// Objet....: Gestion d'une Fen�tre de saisie Ligne
//	      Cr�ation/Initialisation/Finalisation


// ------------------ D�claration de fonctions
HWND creer_wnd_line  (HWND hwndParent, UINT id, int pos_x, int pos_y, int taille_x, int taille_y,
	DWORD dwNFont, DWORD dwTailleFont, DWORD dwBitsStyle, BYTE byCharSetFont);

void detruire_wnd_line (HWND hwndLigne);

void initialiser_wnd_line (HWND hwndLigne);

void copier_wnd_line (HWND hwnd_wnd_line_destination, HWND hwnd_wnd_line_source);

void effacer_wnd_line (HWND hwndLigne);

void ecrire_wnd_line (HWND hwndLigne, char *texte, UINT etat_ligne);

DWORD position_curseur_wnd_line (HWND hwndLigne);

DWORD longueur_texte_wnd_line (HWND hwndLigne);

void texte_wnd_line (HWND hwndLigne, char *texte);

int scroll_curs_visible_wnd_line (HWND hwndLigne);

DWORD position_car_courant_wnd_line (HWND hwndLigne, UINT index_caractere);

DWORD position_car_visible_wnd_line (HWND hwndLigne, UINT index_caractere);

void enlever_focus_wnd_line (HWND hwndLigne);

void donner_focus_wnd_line (HWND hwndLigne);

void inserer_car_wnd_line (HWND hwndLigne, char caractere);

void modifier_car_wnd_line (HWND hwndLigne, char caractere);

void supprimer_car_wnd_line (HWND hwndLigne);

DWORD droiter_mot_curseur_wnd_line (HWND hwndLigne);
DWORD gaucher_mot_curseur_wnd_line (HWND hwndLigne);

void afficher_select_ligne_wnd_line (HWND hwndLigne, BOOL mode_selection);

void LigneChangePolice (HWND hwndLigne, DWORD dwNFont, DWORD dwTailleFont, DWORD dwBitsStyleFont, BYTE byCharSetFont);

DWORD LigneHauteur (HWND hwndLigne);

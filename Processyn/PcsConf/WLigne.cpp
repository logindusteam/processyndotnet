// -----------------------------------------------------------------------
// Module...: Wnd_Line.c                                                  
// Objet....: Gestion d'une Fen�tre de saisie Ligne                       
//            Cr�ation / Initialisation / Finalisation                        
// -----------------------------------------------------------------------

#include "stdafx.h"
#include "appli.h"
#include "std.h"
#include "couleurs.h"
#include "pmnucf.h"
#include "UStr.h"
#include "UEnv.h"
#include "PcsFont.h"

#include "WLigne.h"

// Constantes caract�ristiques d'une ligne
#define MARGE_GAUCHE 16
#define WND_LINE_NB_CAR 255  // nb caract�res sans compter '\0'
#define WND_LINE_INDEX_DERNIER_CAR	(WND_LINE_NB_CAR - 1)// index du dernier caract�re
#define WND_LINE_INDEX_FIN		(WND_LINE_INDEX_DERNIER_CAR + 1)//index du d�limiteur '\0' final
#define WND_LINE_NB_CAR_TOTAL		(WND_LINE_NB_CAR + 1)// nb caract�res en comptant '\0' final

#define N_FONT_LIGNE 0
#define TAILLE_FONT_LIGNE 14

// donn�es d'une fen�tre Ligne
typedef struct
  {
  BOOL	pLigneALeCaret;
	int		xCaret;	// si pLigneALeCaret
	int		yCaret;
	int		cyCaret;// hauteur caret (largeur par d�faut)

  DWORD	IdLigne;
  DWORD	EtatLigne; // commentaire, invalide, normale
  DWORD	ModeLigne; // 0 non s�lectionn� 1 S�lectionn�
  DWORD	IndexCarCourantLigne;
  DWORD	IndexCarDebutLigne; // index sur le premier caract�re visible
  char	szTexteLigne [WND_LINE_NB_CAR_TOTAL];
	DWORD dwNFont;
	DWORD dwTailleFont;
	DWORD dwCharSet;
	HFONT hFont;
	BYTE byCharSetFont;
  }
  ENV_LIGNE, *PENV_LIGNE;

// ------------------
#define NB_ETATS_WND_LINE_MAX 4
#define NB_MODES_WND_LINE_MAX 2

static LONG td_couleurs_car  [] = {CLR_BLACK, CLR_RED, CLR_BLUE, CLR_DARKGREEN};
static LONG td_couleurs_fond [] = {CLR_WHITE, CLR_GREEN};

#define ETAT_WND_LINE_DEFAUT 0
#define MODE_WND_LINE_DEFAUT 0

static int cxMargeGauche = MARGE_GAUCHE;
static int cxLargeurCaret = 2; // $$ � am�liorer

// classe de la fen�tre ligne
static char szClasseLigne [] = "ClasseLigne";
static BOOL bClasseLigneEnregistree = FALSE;

// -----------------------------------------------------------------------
static _inline BOOL est_un_car_d_un_mot (char caractere)
  {
  return ((BOOL) (caractere != ' '));
  }

// -----------------------------------------------------------------------
static DWORD index_mot_precedent (char *buffer, DWORD index_debut_recherche)
  {
  DWORD    index_debut_mot;
  BOOL un_car_trouve;
  BOOL debut_mot_trouve;

  index_debut_mot = index_debut_recherche;

  // ----- recherche d'un caractere
  un_car_trouve = FALSE;
  while ((!un_car_trouve) && (index_debut_mot > 0))
    {
    if (est_un_car_d_un_mot (buffer [index_debut_mot - 1]))
      {
      un_car_trouve = TRUE;
      }
    else
      {
      index_debut_mot--;
      }
    }

  // ----- recherche du debut du mot
  debut_mot_trouve = FALSE;
  while ((!debut_mot_trouve) && (index_debut_mot > 0))
    {
    if (est_un_car_d_un_mot (buffer [index_debut_mot - 1]))
      {
      index_debut_mot--;
      }
    else
      {
      debut_mot_trouve = TRUE;
      }
    }

  return (index_debut_mot);
  }

// -----------------------------------------------------------------------
static DWORD index_mot_suivant (char *buffer, DWORD index_debut_recherche)
  {
  DWORD    index_delimiteur_fin_ligne;
  DWORD    index_debut_mot;
  BOOL un_espace_trouve;
  BOOL debut_mot_trouve;

  index_delimiteur_fin_ligne = StrLength (buffer);
  index_debut_mot = index_debut_recherche;

  // ----- recherche d'un espace
  un_espace_trouve = FALSE;
  while ((!un_espace_trouve) && (index_debut_mot < index_delimiteur_fin_ligne))
    {
    if (est_un_car_d_un_mot (buffer [index_debut_mot]))
      {
      index_debut_mot++;
      }
    else
      {
      un_espace_trouve = TRUE;
      }
    }

  // ----- recherche du debut du mot
  debut_mot_trouve = FALSE;
  while ((!debut_mot_trouve) && (index_debut_mot < index_delimiteur_fin_ligne))
    {
    if (est_un_car_d_un_mot (buffer [index_debut_mot]))
      {
      debut_mot_trouve = TRUE;
      }
    else
      {
      index_debut_mot++;
      }
    }

  return (index_debut_mot);
  }

// -----------------------------------------------------------------------
// Demande de la taille du texte d'une wnd_line
// -----------------------------------------------------------------------
static _inline DWORD wnd_line_get_texte_len (PENV_LIGNE pEnvLigne)
  {
  return ((DWORD) StrLength (pEnvLigne->szTexteLigne));
  }

// -----------------------------------------------------------------------
// D�termine l'index du caractere d'une Wnd_Line � partir d'une position
// pixel (origine de l'axe: � gauche de la fenetre)
// -----------------------------------------------------------------------
static DWORD wnd_line_index_curs_from_x (HWND hwnd, int x_pixel)
  {
  PENV_LIGNE	pEnvLigne = (PENV_LIGNE)pGetEnv (hwnd);
  PSTR				buffer = pEnvLigne->szTexteLigne;
  DWORD       index_car_debut = pEnvLigne->IndexCarDebutLigne;
  HDC        hdc;
	SIZE			sizeText;
  DWORD       nb_car_cou;

	x_pixel -= cxMargeGauche;

  if (x_pixel > 0)
    {
    nb_car_cou = wnd_line_get_texte_len (pEnvLigne) - index_car_debut + 1;
    hdc = ::GetDC (hwnd);
    do
      {
      nb_car_cou--;
			GetTextExtentPoint32(hdc, buffer + index_car_debut, nb_car_cou,	&sizeText); //$$
      }
    while ( x_pixel < sizeText.cx );
    ::ReleaseDC (hwnd, hdc);
    }
  else
    {
    nb_car_cou = 0;
    }

  return (nb_car_cou + index_car_debut);
  }


// -----------------------------------------------------------------------
// Init ou r�init des donn�es d'une wnd_line
// -----------------------------------------------------------------------
static void wnd_line_init (ENV_LIGNE *pEnvLigne)
  {
  StrSetNull (pEnvLigne->szTexteLigne);
  pEnvLigne->pLigneALeCaret        = FALSE;
	pEnvLigne->xCaret									= 0;
	pEnvLigne->yCaret									= 0;
	pEnvLigne->cyCaret								= 0;
  pEnvLigne->EtatLigne              = ETAT_WND_LINE_DEFAUT;
  pEnvLigne->ModeLigne              = MODE_WND_LINE_DEFAUT;
  pEnvLigne->IndexCarCourantLigne = 0;
  pEnvLigne->IndexCarDebutLigne   = 0;
  return;
  }

// -----------------------------------------------------------------------
// Copie de deux wnd_line
// -----------------------------------------------------------------------
static void wnd_line_copie (ENV_LIGNE *pEnvLigneDest, ENV_LIGNE *pEnvLigneSource)
  {
  StrCopy (pEnvLigneDest->szTexteLigne, pEnvLigneSource->szTexteLigne);
  pEnvLigneDest->EtatLigne              = pEnvLigneSource->EtatLigne;
  pEnvLigneDest->ModeLigne              = pEnvLigneSource->ModeLigne;
  pEnvLigneDest->IndexCarCourantLigne = pEnvLigneSource->IndexCarCourantLigne;
  pEnvLigneDest->IndexCarDebutLigne   = pEnvLigneSource->IndexCarDebutLigne;
  return;
  }

// -----------------------------------------------------------------------
// For�age de l'index sur le caract�re courant d'une wnd_line
// -----------------------------------------------------------------------
static BOOL wnd_line_set_index_car_courant (ENV_LIGNE *pEnvLigne, UINT index_car_courant)
  {
  UINT  longueur_texte;
  UINT  old_index;

  old_index = pEnvLigne->IndexCarCourantLigne;
  if (((int) index_car_courant) < 0)
    {
    pEnvLigne->IndexCarCourantLigne = 0;
    }
  else
    {
    longueur_texte = StrLength (pEnvLigne->szTexteLigne);
    if (index_car_courant > longueur_texte)
      {
      pEnvLigne->IndexCarCourantLigne = longueur_texte;
      }
    else
      {
      pEnvLigne->IndexCarCourantLigne = index_car_courant;
      }
    }
  //return ((BOOL) (old_index != pEnvLigne->IndexCarCourantLigne));
  return (FALSE);
  }

// -----------------------------------------------------------------------
// For�age de l'index sur le premier caract�re visible d'une wnd_line
// -----------------------------------------------------------------------
static BOOL wnd_line_set_index_car_debut (ENV_LIGNE *pEnvLigne, UINT index_car_debut)
  {
  UINT  longueur_texte;
  UINT  old_index;

  old_index = pEnvLigne->IndexCarDebutLigne;
  if (((int) index_car_debut) < 0)
    {
    pEnvLigne->IndexCarDebutLigne = 0;
    }
  else
    {
    longueur_texte = StrLength (pEnvLigne->szTexteLigne);
    if (index_car_debut > longueur_texte)
      {
      pEnvLigne->IndexCarDebutLigne = longueur_texte;
      }
    else
      {
      pEnvLigne->IndexCarDebutLigne = index_car_debut;
      }
    }
  return ((BOOL) (old_index != pEnvLigne->IndexCarDebutLigne));
  }


// -----------------------------------------------------------------------
// For�age du texte d'une wnd_line
// -----------------------------------------------------------------------
static BOOL wnd_line_set_texte (ENV_LIGNE *pEnvLigne, char *texte)
  {
  BOOL modif = FALSE;

  if (StrLength (texte) <= WND_LINE_NB_CAR)
    {
    if (StrCompare (pEnvLigne->szTexteLigne, texte) != 0)
      {
      StrCopy (pEnvLigne->szTexteLigne, texte);
      modif = TRUE;
      }
    if (wnd_line_set_index_car_courant (pEnvLigne, pEnvLigne->IndexCarCourantLigne))
      {
      modif = TRUE;
      }
    if (wnd_line_set_index_car_debut (pEnvLigne, pEnvLigne->IndexCarDebutLigne))
      {
      modif = TRUE;
      }
    }
  return (modif);
  }


// -----------------------------------------------------------------------
// Remplacement d'un caract�re � la position courante
// -----------------------------------------------------------------------
static BOOL wnd_line_replace_car (ENV_LIGNE *pEnvLigne, char caractere)
  {
  BOOL replace_ok = TRUE;
  DWORD    index_caractere;
  DWORD    longueur_texte;

  index_caractere = pEnvLigne->IndexCarCourantLigne;

  if ((index_caractere + 1) > WND_LINE_NB_CAR)
    {
    replace_ok = FALSE;       //d�passement de la longueur total de la chaine
    }
  else
    {
    longueur_texte = StrLength (pEnvLigne->szTexteLigne);
    if (index_caractere != longueur_texte)
      {
      StrSetChar (pEnvLigne->szTexteLigne, index_caractere, caractere);
      }
    else
      {
      if (longueur_texte < WND_LINE_NB_CAR)              //insertion possible
        {
        StrInsertChar (pEnvLigne->szTexteLigne, caractere, index_caractere);
        }
      else
        {
        replace_ok = FALSE; //d�passement de la longueur total de la chaine en insertion
        }
      }
    }

  return ((BOOL) replace_ok);
  }

// -----------------------------------------------------------------------
// Insertion d'un caract�re � la position courante
// -----------------------------------------------------------------------
static BOOL wnd_line_insert_car (ENV_LIGNE *pEnvLigne, char caractere)
  {
  BOOL insert_ok = TRUE;
  DWORD    index_caractere;
  DWORD    longueur_texte;

  index_caractere = pEnvLigne->IndexCarCourantLigne;

  if ((index_caractere + 1) > WND_LINE_NB_CAR)
    {
    insert_ok = FALSE;      //d�passement de la longueur total de la chaine
    }
  else
    {
    longueur_texte = StrLength (pEnvLigne->szTexteLigne);
    if (longueur_texte < WND_LINE_NB_CAR)                //insertion possible
      {
      StrInsertChar (pEnvLigne->szTexteLigne, caractere, index_caractere);
      }
    else
      {
      insert_ok = FALSE;  //d�passement de la longueur total de la chaine en insertion
      }
    }

  return ((BOOL) insert_ok);
  }



// -----------------------------------------------------------------------
// Suppression du caract�re � la position courante
// -----------------------------------------------------------------------
static BOOL wnd_line_supprime_char (ENV_LIGNE *pEnvLigne)
  {
  BOOL supression_ok;
  DWORD    longueur_texte;
  DWORD    idx_car_a_supprimer;

  longueur_texte = StrLength (pEnvLigne->szTexteLigne);
  idx_car_a_supprimer = pEnvLigne->IndexCarCourantLigne;

  if ((idx_car_a_supprimer + 1) <= longueur_texte)
    {
    StrDelete (pEnvLigne->szTexteLigne, idx_car_a_supprimer, 1);
    supression_ok = TRUE;
    }
  else
    {
    supression_ok = FALSE;
    }
  return (supression_ok);
  }


// -----------------------------------------------------------------------
// For�age de l'�tat d'une wnd_line
// -----------------------------------------------------------------------
static BOOL wnd_line_set_etat (ENV_LIGNE *pEnvLigne, UINT etat)
  {
  UINT old_etat;

  old_etat = pEnvLigne->EtatLigne;
  if (etat < NB_ETATS_WND_LINE_MAX)
    {
    pEnvLigne->EtatLigne = etat;
    }
  else
    {
    pEnvLigne->EtatLigne = ETAT_WND_LINE_DEFAUT;
    }
  return ((BOOL) (old_etat != pEnvLigne->EtatLigne));
  }

// -----------------------------------------------------------------------
// For�age du mode d'une wnd_line
// -----------------------------------------------------------------------
static BOOL wnd_line_set_mode (ENV_LIGNE *pEnvLigne, 
															 DWORD Selection) // 0 non s�lectionn�
  {
  DWORD old_mode;

  old_mode = pEnvLigne->ModeLigne;
  if (Selection < NB_MODES_WND_LINE_MAX)
    {
    pEnvLigne->ModeLigne = Selection;
    }
  else
    {
    pEnvLigne->ModeLigne = MODE_WND_LINE_DEFAUT;
    }
  return ((BOOL) (old_mode != pEnvLigne->ModeLigne));
  }

// -----------------------------------------------------------------------
// For�age de l'�tat d'une wnd_line
// -----------------------------------------------------------------------
static BOOL wnd_line_set_focus (ENV_LIGNE *pEnvLigne, BOOL focus)
  {
  BOOL old_focus;

  old_focus = pEnvLigne->pLigneALeCaret;
  pEnvLigne->pLigneALeCaret = focus;
  return ((BOOL) (old_focus != pEnvLigne->pLigneALeCaret));
  }

// -----------------------------------------------------------------------
// Nom......: Create
// Objet....: Traitement du message WM_CREATE
// Remarques: pCtlData = *pmp1 pointe sur les donn�es de contr�le
//            (cf. CreateWindow)
//            pcrst = (CREATESTRUCT*) *pmp2
//            pointe sur une structure CREATESTRUCT
//            pmp2 contient des infos compl�mentaires
// -----------------------------------------------------------------------
static LRESULT lOnCreate (HWND hwnd, WPARAM mp1, LPARAM mp2)
  {
  BOOL bErreur= TRUE;
  CREATESTRUCT *pcrst = (CREATESTRUCT *) mp2;
  // Cr�ation des donn�es d'une wnd_line
  PENV_LIGNE    pEnvLigne = (PENV_LIGNE)pCreeEnv(hwnd, sizeof (ENV_LIGNE));

  if (pEnvLigne != NULL)
    {
    // Init. m�moire pour info sur la fen�tre � cr�er
    wnd_line_init (pEnvLigne);
		pEnvLigne->dwNFont = N_FONT_LIGNE;
		pEnvLigne->dwTailleFont = TAILLE_FONT_LIGNE;
		pEnvLigne->hFont = NULL;
		pEnvLigne->byCharSetFont = Appli.byCharSetFont;
    pEnvLigne->IdLigne = (DWORD)pcrst->hMenu;

		// Init Police
		HDC hdc=GetDC(hwnd);
		pEnvLigne->hFont = fnt_create(hdc,pEnvLigne->dwNFont,0,pEnvLigne->dwTailleFont,pEnvLigne->byCharSetFont);
		SelectObject(hdc,pEnvLigne->hFont);
    bErreur = FALSE;
    }

  // -1 si erreur => avorte la cr�ation
  return (bErreur ? -1 : 0);
  }


// -----------------------------------------------------------------------
// Objet....: Traitement du message WM_DESTROY d'une Ligne d'�dition
// -----------------------------------------------------------------------
static void OnDestroy (HWND hwnd)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwnd);
	if (pEnvLigne)
		{
		fnt_destroy (pEnvLigne->hFont);
		pEnvLigne->hFont=NULL;
		}
  bLibereEnv (hwnd);
  }

// -----------------------------------------------------------------------
// Nom......: OnPaint
// Objet....: Traitement du message WM_PAINT d'une ligne de texte
// -----------------------------------------------------------------------
static void OnPaint (HWND hwnd)
  {
  HDC         hdc;
  RECT        rcl;
	SIZE				sizeText;
	PAINTSTRUCT	ps;
  LONG        couleur_avant;
  ENV_LIGNE   *pEnvLigne;
  char        *pszTexteLigne;
  DWORD       index_car_debut;
  DWORD       index_car_courant;
	int					NbCharsVisibles;

  hdc = ::BeginPaint (hwnd, &ps);
  pEnvLigne = (PENV_LIGNE)pGetEnv (hwnd);

	// r�cup�re la taille de la fen�tre
  ::GetClientRect (hwnd, &rcl);
	// restriction pour laisser la marge gauche
	rcl.left += cxMargeGauche;
	if (rcl.left>rcl.right)
		rcl.left = rcl.right;


  pszTexteLigne = pEnvLigne->szTexteLigne;
  index_car_debut = pEnvLigne->IndexCarDebutLigne;
  index_car_courant = pEnvLigne->IndexCarCourantLigne;
	NbCharsVisibles = StrLength(pszTexteLigne) - (int)(index_car_debut);

	// couleur du texte selon �tat de la s�lection de la ligne
	if (pEnvLigne->ModeLigne)
		couleur_avant = GetSysColor (COLOR_HIGHLIGHTTEXT);
	else
		couleur_avant = td_couleurs_car  [pEnvLigne->EtatLigne]; // $$ couleurs texte
	SetTextColor (hdc, couleur_avant);

	// �criture du texte
	SetBkMode (hdc, TRANSPARENT);
	//pEnvLigne->hFont = fnt_create(hdc,pEnvLigne->dwNFont,0,pEnvLigne->dwTailleFont);
	//HFONT hfontOld = (HFONT)SelectObject(hdc,pEnvLigne->hFont);

	//DrawText (hdc, &pszTexteLigne[index_car_debut], NbCharsVisibles, &rcl, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
	TextOut (hdc, rcl.left, rcl.top, &pszTexteLigne[index_car_debut], NbCharsVisibles);

	// Dessin du Focus
  if (pEnvLigne->pLigneALeCaret)
    {
		pEnvLigne->cyCaret = rcl.bottom - rcl.top;
		CreateCaret(hwnd, (HBITMAP) NULL, cxLargeurCaret, pEnvLigne->cyCaret);

		GetTextExtentPoint32(hdc, &pszTexteLigne[index_car_debut], (int)(index_car_courant - index_car_debut),	&sizeText);

		// mise � jour du caret
		pEnvLigne->xCaret = cxMargeGauche + sizeText.cx - 1; //$$ adapter -1 et cxLargeurCaret � l'espacement inter car de la fonte;
		pEnvLigne->yCaret = 0;
		SetCaretPos(pEnvLigne->xCaret, pEnvLigne->yCaret);

		ShowCaret(hwnd);
    }
	// else DestroyCaret(); // $$ destruction du caret si le FOCUS appartient � une autre fen�tre de la m�me t�che
	
	//// Restaure l'ancienne police
	//SelectObject(hdc,hfontOld);

	// Dessin termin�
  ::EndPaint (hwnd, &ps);
  }


// -----------------------------------------------------------------------
// Traitement des messages WM_MOUSEMOVE,
// WM_LBUTTONDOWN, WM_LBUTTONUP,WM_RBUTTONDOWN, WM_RBUTTONUP,
// WM_LBUTTONDBLCLK, WM_RBUTTONDBLCLK
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// Remarques: x = (USHORT) (mp2);                horizontal position
//            y = HIWORD (mp2);                vertical position
// -----------------------------------------------------------------------
static BOOL OnMouse (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  BOOL         bTraite = FALSE;
  PENV_LIGNE	pEnvLigne = (PENV_LIGNE)pGetEnv (hwnd);
  DWORD       id = pEnvLigne->IdLigne;
	
  switch (msg)
    {
		// la notification n'est envoy�e qu'en capture
    case WM_MOUSEMOVE:
			{
			RECT        rcl;
			LONG        y_pixel;
			
			if (::GetClientRect (hwnd, &rcl))
				{
				
				y_pixel = (LONG) HIWORD (mp2);
				if ((y_pixel > rcl.bottom) || (y_pixel < rcl.top))
					{
					// Coordonnees souris								
					notification_user (::GetParent(hwnd), id, NU0_WND_LINE_LOSE_MOUSE, LOWORD (mp2), HIWORD (mp2));
					}
				}
			*pmres = TRUE;
			bTraite = TRUE;
			}
			break;
			
    case WM_LBUTTONDOWN:
			notification_user (::GetParent(hwnd), id, NU0_WND_LINE_BN_1_DOWN, 
				(WORD)wnd_line_index_curs_from_x (hwnd, LOWORD (mp2)), 0);
			break;
			
    case WM_LBUTTONUP:
			notification_user (::GetParent(hwnd), id, NU0_WND_LINE_BN_1_UP, 
				(WORD)wnd_line_index_curs_from_x (hwnd, LOWORD (mp2)), 0);
			break;
			
    case WM_LBUTTONDBLCLK:
			notification_user (::GetParent(hwnd), id, NU0_WND_LINE_BN_1_DBL_CLICK,
				(WORD)wnd_line_index_curs_from_x (hwnd, LOWORD (mp2)), 0);
			break;
			
    default:
			break;
    }
	
  // Message trait�
  *pmres =  TRUE;
  return (bTraite);
}

/*
static void OnSetFocus (HWND hwnd)
	{
	ENV_LIGNE* pEnvLigne = pGetEnv (hwnd);

  if (wnd_line_set_focus (pEnvLigne, TRUE))
    {
    ::InvalidateRect (hwnd, NULL, TRUE);
    }
	else
		{
		CreateCaret (hwnd, (HBITMAP) NULL, 0, pEnvLigne->cyCaret); // largeur par d�faut
		SetCaretPos (pEnvLigne->xCaret, pEnvLigne->yCaret);
		ShowCaret (hwnd); 
		}
	}
*/

// -----------------------------------------------------------------------
BOOL bOnErase (HWND hwnd, WPARAM wParam)
	{
	BOOL bTraite = FALSE;
	PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwnd);

	// ligne en s�lection ?
	if (pEnvLigne->ModeLigne)
		{
		HDC hdc = (HDC) wParam;
		RECT rectClient;

		::GetClientRect (hwnd, &rectClient);
		FillRect (hdc, &rectClient, (HBRUSH) (COLOR_HIGHLIGHT+1));
		bTraite = TRUE;
		}
	return bTraite;
	}

// -----------------------------------------------------------------------
// Objet....: Fen�tre ligne de la page de l'�diteur
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocLigne (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
{
LRESULT  mres = 0;                // Valeur de retour
BOOL     bTraite = TRUE;         // Indique commande trait�e

  switch (msg)
    {
    case WM_CREATE:
      mres = lOnCreate (hwnd, mp1, mp2);
      break;

    case WM_DESTROY:
      OnDestroy (hwnd);
      break;

		case WM_ERASEBKGND:
			bTraite = bOnErase (hwnd, mp1);
			break;

    case WM_PAINT:
      OnPaint (hwnd);
      break;

		//case WM_SETFOCUS: Actuellement il y a un focus global
		//	OnSetFocus (hwnd);
		//	break; 

    /* $$ case WM_ENABLE:
      ::InvalidateRect (hwnd, NULL, TRUE);
      break;
*/
    //case WM_RBUTTONDBLCLK: Non g�r�s par OnMouse
    //case WM_RBUTTONDOWN:
    //case WM_RBUTTONUP:
    case WM_MOUSEMOVE:
    case WM_LBUTTONDBLCLK:
    case WM_LBUTTONDOWN:
    case WM_LBUTTONUP:
      OnMouse (hwnd, msg, mp1, mp2, &mres);
      break;

    default:
			bTraite = FALSE;
      break;
    }

  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);
  return (mres);
	} // wndprocLigne 

// -----------------------------------------------------------------------
// Objet....: Cr�er une fen�tre de saisie Wnd_Line
// Entr�es..: HWND   hwndParent
//            HWND   hwndOwner
//            USHORT id       identificateur de la Wnd_Line cr��e
// Retour...: HWND : handle de la fen�tre cr��e
// -----------------------------------------------------------------------
HWND creer_wnd_line  (HWND hwndParent, UINT id, int pos_x, int pos_y, int taille_x, int taille_y, 
											DWORD dwNFont, DWORD dwTailleFont, DWORD dwBitsStyle, BYTE byCharSetFont)
  {
  HWND   hwndLigne = NULL;

  // Cr�ation si n�cessaire de la classe Wnd_Line
  if (!bClasseLigneEnregistree)
    {
		WNDCLASS wc;

		wc.style					= CS_DBLCLKS | CS_OWNDC; //CS_HREDRAW | CS_VREDRAW; 
    wc.lpfnWndProc		= wndprocLigne; 
    wc.cbClsExtra			= 0; 
    wc.cbWndExtra			= 0; 
    wc.hInstance			= Appli.hinst; 
    wc.hIcon					= NULL;
    wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
    wc.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1); 
    wc.lpszMenuName		= NULL; 
    wc.lpszClassName	= szClasseLigne; 

    bClasseLigneEnregistree = (BOOL) RegisterClass (&wc);
    }

  // Cr�ation si possible d'une fen�tre Wnd_Line
  if (bClasseLigneEnregistree)
    {
    hwndLigne = CreateWindow (
			szClasseLigne, "",
			WS_VISIBLE | WS_CHILD, //$$ | WS_SYNCPAINT,
			pos_x, pos_y, taille_x, taille_y,
			hwndParent, (HMENU)id,
			Appli.hinst, NULL);
		if (hwndLigne)
			{
			LigneChangePolice(hwndLigne, dwNFont, dwTailleFont, dwBitsStyle, byCharSetFont);
			}
    }

  // Retour : HWND fen�tre cr��e ou NULL
  return (hwndLigne);
  }


// -----------------------------------------------------------------------
// Suppression d'une fen�tre Wnd_Line
// -----------------------------------------------------------------------
void detruire_wnd_line (HWND hwndLigne) // Handle de la fen�tre Wnd_Line � supprimer
  {
  ::DestroyWindow (hwndLigne);
  return;
  }

// -----------------------------------------------------------------------
// Initialisation d'une fen�tre Wnd_Line
// Entr�es..: HWND hwndLigne: Handle de la fen�tre Wnd_Line � initialiser
// Remarques: reaffiche
// -----------------------------------------------------------------------
void initialiser_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE) pGetEnv (hwndLigne);
  wnd_line_init (pEnvLigne);

  ::InvalidateRect (hwndLigne, NULL, TRUE);
  return;
  }

// -----------------------------------------------------------------------
// Nom......: copier_wnd_line
// Objet....: Copie d'une fen�tre Wnd_Line
// Entr�es..: HWND hwnd_wnd_line_destination: Handle de la fen�tre Wnd_Line Destination
//            HWND hwnd_wnd_line_source:      Handle de la fen�tre Wnd_Line Source
// Remarques: reaffiche la ligne destination
// -----------------------------------------------------------------------
void copier_wnd_line (HWND hwnd_wnd_line_destination, HWND hwnd_wnd_line_source)
  {
  PENV_LIGNE	pEnvLigneDest = (PENV_LIGNE)pGetEnv (hwnd_wnd_line_destination);
  PENV_LIGNE	pEnvLigneSource = (PENV_LIGNE)pGetEnv (hwnd_wnd_line_source);

  wnd_line_copie (pEnvLigneDest, pEnvLigneSource);

  ::InvalidateRect (hwnd_wnd_line_destination, NULL, TRUE);
  }

// -----------------------------------------------------------------------
// Objet....: Effacement de la fen�tre Wnd_Line
// Remarques: reaffiche
// -----------------------------------------------------------------------
void effacer_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (wnd_line_set_texte (pEnvLigne, ""))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  }


// -----------------------------------------------------------------------
// Ecriture d'une ligne Wnd_Line
// Remarques: reaffiche
// -----------------------------------------------------------------------
void ecrire_wnd_line (HWND hwndLigne, char *texte, UINT etat_ligne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);
  BOOL    dessine1 = wnd_line_set_texte (pEnvLigne, texte);
  BOOL    dessine2 = wnd_line_set_etat (pEnvLigne, etat_ligne);

  if (dessine1 || dessine2)
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  }


// -----------------------------------------------------------------------
// Objet....: Donner la position du curseur dans la wnd_line
//            (en caractere, � partir du debut de la chaine)
// -----------------------------------------------------------------------
DWORD position_curseur_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);
                                              //Convertion index --> position
  return (pEnvLigne->IndexCarCourantLigne + 1);
  }

// -----------------------------------------------------------------------
// Objet....: R�cuperer la longueur du texte d'un ligne Wnd_Line
// -----------------------------------------------------------------------
DWORD longueur_texte_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE	pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  return wnd_line_get_texte_len (pEnvLigne);
  }

// -----------------------------------------------------------------------
// R�cuperer le texte d'un ligne Wnd_Line
// -----------------------------------------------------------------------
void texte_wnd_line (HWND hwndLigne, char *texte)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  StrCopy (texte, pEnvLigne->szTexteLigne);
  }

// -----------------------------------------------------------------------
// Nom......: scroll_curs_visible_wnd_line
// Objet....: D�terminer le scroll n�cessaire pour visualiser le curseur
//            d'une ligne Wnd_Line
// Retour...: (+) xx si le curseur d�passe par la droite
//            (-) xx si le curseur d�passe par la gauche
//                00 si le curseur est visible
// Remarques:
// -----------------------------------------------------------------------
int scroll_curs_visible_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);
  char       *pszTexteLigne = pEnvLigne->szTexteLigne;
  DWORD       index_car_courant = pEnvLigne->IndexCarCourantLigne;
  DWORD       index_car_debut = pEnvLigne->IndexCarDebutLigne;
	int        scroll_necessaire;

  if (index_car_courant > index_car_debut)
    {
		HDC        hdc;
		RECT      rcl;
		SIZE			sizeText;
		DWORD       index_premier_visible;
		BOOL    curseur_non_visible;

    ::GetClientRect (hwndLigne, &rcl);
    index_premier_visible = index_car_debut;

    hdc = ::GetDC (hwndLigne);

    do
      {
			GetTextExtentPoint32(hdc, &pszTexteLigne[index_premier_visible], index_car_courant - index_premier_visible,	&sizeText);

      curseur_non_visible = (BOOL) ((sizeText.cx + cxMargeGauche + cxLargeurCaret) > rcl.right);
      index_premier_visible++;
      }
    while (curseur_non_visible);

    ::ReleaseDC (hwndLigne, hdc);

    scroll_necessaire = (index_premier_visible - index_car_debut - 1);
    }
  else
    {
    scroll_necessaire = (int)index_car_courant - (int)index_car_debut;
    }
  return scroll_necessaire;
  }

// -----------------------------------------------------------------------
// Positionner le caractere courant d'une ligne Wnd_Line
// Remarques: reaffiche
// -----------------------------------------------------------------------
DWORD position_car_courant_wnd_line (HWND hwndLigne, UINT index_caractere)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (wnd_line_set_index_car_courant (pEnvLigne, index_caractere))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  return (pEnvLigne->IndexCarCourantLigne);
  }

// -----------------------------------------------------------------------
// Positionner le 1� caractere visible d'une ligne Wnd_Line
// Remarques: reaffiche
// -----------------------------------------------------------------------
DWORD position_car_visible_wnd_line (HWND hwndLigne, UINT index_caractere)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (wnd_line_set_index_car_debut (pEnvLigne, index_caractere))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  return (pEnvLigne->IndexCarDebutLigne);
  }


// -----------------------------------------------------------------------
// Enlever le focus d'une ligne Wnd_Line
// Remarques: reaffiche
// -----------------------------------------------------------------------
void enlever_focus_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (wnd_line_set_focus (pEnvLigne, FALSE))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  }

// -----------------------------------------------------------------------
// Donner le focus � une ligne Wnd_Line
// Remarques: reaffiche
// -----------------------------------------------------------------------
void donner_focus_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (wnd_line_set_focus (pEnvLigne, TRUE))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  }


// -----------------------------------------------------------------------
// Insere un caractere a la position courante du curseur
// Remarques: reaffiche
// -----------------------------------------------------------------------
void inserer_car_wnd_line (HWND hwndLigne, char caractere)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (wnd_line_insert_car (pEnvLigne, caractere))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  }

// -----------------------------------------------------------------------
// remplacer un caractere a la position courante du curseur
// Remarques: reaffiche
// -----------------------------------------------------------------------
void modifier_car_wnd_line (HWND hwndLigne, char caractere)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (wnd_line_replace_car (pEnvLigne, caractere))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  }

// -----------------------------------------------------------------------
// supprimer le caractere a la position courante du curseur
// Remarques: reaffiche
// -----------------------------------------------------------------------
void supprimer_car_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (wnd_line_supprime_char (pEnvLigne))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
DWORD gaucher_mot_curseur_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);
  char       *pszTexteLigne = pEnvLigne->szTexteLigne;
  DWORD       index_caractere = pEnvLigne->IndexCarCourantLigne;

  index_caractere = index_mot_precedent (pszTexteLigne, index_caractere);

  if (wnd_line_set_index_car_courant (pEnvLigne, index_caractere))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  return (index_caractere);
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
DWORD droiter_mot_curseur_wnd_line (HWND hwndLigne)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);
  char       *pszTexteLigne = pEnvLigne->szTexteLigne;
  DWORD       index_caractere = pEnvLigne->IndexCarCourantLigne;

  index_caractere = index_mot_suivant (pszTexteLigne, index_caractere);

  if (wnd_line_set_index_car_courant (pEnvLigne, index_caractere))
    {
    ::InvalidateRect (hwndLigne, NULL, TRUE);
    }
  return (index_caractere);
  }


// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void afficher_select_ligne_wnd_line (HWND hwndLigne, BOOL mode_selection)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);

  if (mode_selection == TRUE)
    {
    if (wnd_line_set_mode (pEnvLigne, 1))
      {
      ::InvalidateRect (hwndLigne, NULL, TRUE);
      }
    }
  else
    {
    if (wnd_line_set_mode (pEnvLigne, 0))
      {
      ::InvalidateRect (hwndLigne, NULL, TRUE);
      }
    }

  return;
  }

// -----------------------------------------------------------------------
// Modifie la police de la ligne
// Remarques: reaffiche la ligne
// -----------------------------------------------------------------------
void LigneChangePolice (HWND hwndLigne, DWORD dwNFont, DWORD dwTailleFont, DWORD dwBitsStyleFont, BYTE byCharSetFont)
  {
  PENV_LIGNE pEnvLigne = (PENV_LIGNE)pGetEnv (hwndLigne);
	// Enregistre les nouvelles caract�ristiques de police
	pEnvLigne->dwNFont = dwNFont;
	pEnvLigne->dwTailleFont = dwTailleFont;
	// Cr�e la nouvelle police
	HDC hdc=::GetDC(hwndLigne);
	HFONT hFontEnvOld = pEnvLigne->hFont;
	pEnvLigne->hFont = fnt_create(hdc,pEnvLigne->dwNFont, dwBitsStyleFont, pEnvLigne->dwTailleFont,pEnvLigne->byCharSetFont);
	// S�lectionne la nouvelle police et lib�re l'ancienne
	HFONT hOldFont = (HFONT) ::SelectObject(hdc,pEnvLigne->hFont);
	DeleteObject(hOldFont); 
	// D�truit l'ancienne police
	fnt_destroy(hFontEnvOld);
  ::InvalidateRect (hwndLigne, NULL, TRUE);
  }
// -----------------------------------------------------------------------
// Renvoie la hauteur en pixels de la ligne
// -----------------------------------------------------------------------
DWORD LigneHauteur (HWND hwndLigne)
  {
  HDC hdc = ::GetDC (hwndLigne);
  TEXTMETRIC fm;
  GetTextMetrics (hdc, &fm);
  ::ReleaseDC (hwndLigne, hdc);

  return ((DWORD) fm.tmHeight);
  }

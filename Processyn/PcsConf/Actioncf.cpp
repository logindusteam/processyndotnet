/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : actioncf.C                                               |
 |   Auteur  : AC					        																			|
 |   Date    : 31/1/92 																									|
 |   Version : 3.20																											|
 |   Remarques : Appelle pour chaque commande, les verbes               |
 |               correspondant aux actions bd et visu :                 |
 |               les acc�s a la bd sont segment�s.Le segment correspond |
 |               a la position geo bd de la premiere ligne affich�.     |
 |               actbd gere le segment .On ne doit jamais acceder a la  |
 |               bd avec un offset de taille superieure a la taille     |
 |               de la fenetre. La fenetre gere le curseur de saisie    |
 |               l'offset commence a 1 par commodite                    |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "mem.h"               // pour import
#include "MemMan.h"               // pour import
#include "son.h"
#include "driv_dq.h"
#include "Init_Txt.h"          // ...................definition_imprimante ()
#include "ULpt.h"
#include "driv_im.h"            // Imprimante Texte...Prnxxx ()
#include "WConf.h"
#include "IdConfLng.h"
#include "bdcf.h"
#include "ppcf.h"
#include "G_objets.h"
#include "G_Sys.h"
#include "BdGr.h"
#include "ppgr.h"
#include "WAide.h"
#include "pcsdlg.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "lng_res.h"
#include "tipe.h"
#include "initdesc.h"
#include "cmdpcs.h"
#include "PathMan.h"
#include "DocMan.h"
#include "Appli.h"
#include "ProgMan.h"
#include "Descripteur.h"
#include "inter_al.h"
#include "ULangues.h"
#include "Verif.h"
#include "actioncf.h"
VerifInit;


//---------------------------------
#define PRN_DEFAUT 1

#define NBR_MAX_ACTION 60
#define A_CORRIGER TRUE
#define SAUT_LIGNE "\r\n"
#define SAUT_PAGE "\f"

typedef struct
	{
	DWORD tipe;
	// ATTENTION doit �tre identique a t_param_action de idactcf.h
	char lettre;           
	char chaine[MAX_PATH];
	BOOL booleen;
	int entier;
	DWORD mot;
	// ATTENTION doit �tre identique a t_param_action de idactcf.h
	} ACTION, *PACTION;

// structure de message de notification
typedef struct 
	{
	NMHDR		hdr;
	ACTION	action;
	} MESSAGE_ACTION, *PMESSAGE_ACTION;

typedef enum
  {
  en_insertion = 0,
  en_modification
  } MODE_SAISIE;

typedef struct
  {
  BOOL saisie_en_cours;
  MODE_SAISIE mode_saisie;
  BOOL analyse_syntaxique;
  BOOL en_selection;
  char mot_recherche[82];
  int type_recherche;
  char nom_presse_papier[STR_MAX_CHAR_ARRAY];
  DWORD numero_desc_cour;
  } CONTEXTE_EDITEUR;

typedef struct
  {
  DWORD n_desc;
  DWORD n_ligne;
  char texte [255];
  } IMPORT_INVALIDE, *PIMPORT_INVALIDE;


// Var globale
CONTEXTE_CF ContexteCf = 
	{0,0,0,0};

static CONTEXTE_EDITEUR ContexteEditeur;

// -----------------------------------------------------------------------
// procedures locales de gestion : 
// les verbes sont � l'imperatif pour les externes
// - � l'infinitif avec id module
// -----------------------------------------------------------------------

// ----------------------------------------------------------------------
// associe un etat 'attribut' d'affichage a un type de ligne  
static void associe (ETAT_LIGNE_BD type_ligne, DWORD *etat_ligne_visu)
  {
  switch (type_ligne)
    {
    case ligne_standard:
      (*etat_ligne_visu) = etat_0;
      break;
    case ligne_invalide:
      (*etat_ligne_visu) = etat_1;
      break;
    case ligne_commentaire:
      (*etat_ligne_visu) = etat_2;
      break;
    case ligne_contexte:
      (*etat_ligne_visu) = etat_0;
      break;
    default:
      (*etat_ligne_visu) = etat_0;
      break;
    }
  }

// ----------------------------------------------------------------------
static void met_contexte_a_jour (void)
  {
  char texte [MAX_PATH];
  char nom_application[MAX_PATH];
  char nom_descripteur[10];
  char contexte_descripteur[82];
  char chaine_ligne[5];
  char chaine_colonne[5];
  DWORD pos_relative_ligne_bd;
  DWORD pos_relative_colonne_fenetre;
  DWORD ligne;
  DWORD taille_relative_bd;
  DWORD etat_ligne_visu;

  memoriser_curseur_fenetre (ContexteCf.dwNFenetreEdit);
  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &pos_relative_colonne_fenetre);
  lire_contexte_bd (ligne, nom_application, nom_descripteur, contexte_descripteur, &pos_relative_ligne_bd, &taille_relative_bd);

  // Mise � jour du titre de la fen�tre
  bLngLireInformation (c_inf_configuration, texte);
  StrConcat (texte, " - ");
  StrConcat (texte, pszNameDoc());
  effacer_fenetre (ContexteCf.NFenetreTitre);
  associe (ligne_contexte, &etat_ligne_visu);
  ecrire_ligne_fenetre (ContexteCf.NFenetreTitre, texte, etat_ligne_visu);

  // Barre d'�tat : mise � jour du commentaire
  effacer_fenetre (ContexteCf.NFenetreCommentaire);
  associe (ligne_contexte, &etat_ligne_visu);
  ecrire_ligne_fenetre (ContexteCf.NFenetreCommentaire, contexte_descripteur, etat_ligne_visu);

  // Barre d'�tat : mise � jour des indicateurs
  StrCopy (texte, nom_descripteur);
  if (ContexteEditeur.mode_saisie == en_insertion)
    {
    StrConcat (texte, ": <I>   ");
    }
  else
    {
    StrConcat (texte, ": <R>   ");
    }
  StrDWordToStr (chaine_ligne, pos_relative_ligne_bd);
  StrConcat (texte, chaine_ligne);
  StrConcatChar (texte, '-');
  StrDWordToStr (chaine_colonne, pos_relative_colonne_fenetre);
  StrConcat (texte, chaine_colonne);
  effacer_fenetre (ContexteCf.NFenetreIndicateurs);
  associe (ligne_contexte, &etat_ligne_visu);
  ecrire_ligne_fenetre (ContexteCf.NFenetreIndicateurs, texte, etat_ligne_visu);

  // Mise � jour des scroll bars
  reajuster_ascenseur_fenetre (ContexteCf.dwNFenetreEdit, pos_relative_ligne_bd, taille_relative_bd);
  restaurer_curseur_fenetre (ContexteCf.dwNFenetreEdit);  // focus rendu a travail
  } // met_contexte_a_jour

// ----------------------------------------------------------------------
static void ecrit_ligne ()
  {
  ETAT_LIGNE_BD type_ligne_bd;
  DWORD etat_ligne_visu;
  DWORD ligne;
  DWORD colonne;
  char texte [NBR_CAR_LIGNE_MAX];
  BOOL possible;
  int niveau;
  int indentation;

  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  consulter_bd (ligne, texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
  associe (type_ligne_bd, &etat_ligne_visu);
  ecrire_ligne_fenetre (ContexteCf.dwNFenetreEdit, texte, etat_ligne_visu);
  }

// ----------------------------------------------------------------------
static BOOL bd_fini (void)
  {
  DWORD ligne;
  DWORD colonne;

  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  return fin_bd (ligne);
  }

// ----------------------------------------------------------------------
static void imprime_fenetre (BOOL imp_selection)
  {
  DWORD     decalage;
  char     texte [NBR_CAR_LIGNE_MAX];
  BOOL  possible;
  ETAT_LIGNE_BD     type_ligne_bd;
  BOOL  ligne_imprime;
  int      niveau;
  int      indentation;
  //
  DWORD     wStatusWrite;
  DWORD    wSizeBufWritten;

  sauver_marque_bd (1);
  //
  positionner_segment_bd (1);          // premiere ligne
  decalage = 1;
  possible = TRUE;
  ligne_imprime = FALSE;
  while ((possible) && (decalage))     // decalage 0 si pas possible
    {
    if (imp_selection)
      {
      consulterln_selection_bd (texte, &possible, NBR_CAR_LIGNE_MAX);
      }
    else
      {
      consulter_bd (1, texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
      }
    if (possible)
      {
      StrConcat (texte, SAUT_LIGNE);
			possible = (LImprimanteWaitWrite (PRN_DEFAUT, &wStatusWrite, &wSizeBufWritten)== CLpt::ERREUR_LPT_NONE);
			if (possible)
				{
			  possible = (LImprimanteWriteAsync (PRN_DEFAUT, StrLength (texte), texte,  &wStatusWrite, TRUE)== CLpt::ERREUR_LPT_NONE);
				}
      if (possible)
        {
        ligne_imprime = TRUE;
        }
      if (!imp_selection)
        {
        descendre_segment_bd (&decalage);
        }
      }
    }  // while

  // --------------------------- Saut de Page en Fin d'Impression
  //                             Si au Moins Une Ligne a �t� Imprim�e
  if (ligne_imprime)
    {
    StrCopy (texte, SAUT_PAGE);
		possible = (LImprimanteWaitWrite (PRN_DEFAUT, &wStatusWrite, &wSizeBufWritten)== CLpt::ERREUR_LPT_NONE);
		if (possible)
			{
			possible = (LImprimanteWriteAsync (PRN_DEFAUT, StrLength (texte), texte,  &wStatusWrite, TRUE)== CLpt::ERREUR_LPT_NONE);
			}
    }
  //
  restaurer_marque_bd ();
  }

// ----------------------------------------------------------------------
static void reaffiche_selection (void)
// ATTENTION : memorise et restaure position curseur ecran
  {
  if (ContexteEditeur.en_selection)
    {
    memoriser_curseur_fenetre (ContexteCf.dwNFenetreEdit);
    curseur_invisible_fenetre (ContexteCf.dwNFenetreEdit);
    aller_haut_fenetre (ContexteCf.dwNFenetreEdit);
		DWORD decalage = 1;
    if (bd_fini ())
      {
      //ecrit_ligne (); // pour restaurer etat saisie (rouge)
      }
    else
      {
      while ((!bd_fini ()) && (decalage))                // decalage 0 si pas possible
        {
				DWORD ligne = 0;
				DWORD colonne = 0;
				BOOL mode_selection = FALSE;
        position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
        typer_selection_bd (ligne, &mode_selection);
        afficher_select_ligne_fenetre (ContexteCf.dwNFenetreEdit, mode_selection);
        descendre_curseur_fenetre (ContexteCf.dwNFenetreEdit, decalage);
        }
      curseur_visible_fenetre (ContexteCf.dwNFenetreEdit);
      restaurer_curseur_fenetre (ContexteCf.dwNFenetreEdit);
      }
    }
  }

// ----------------------------------------------------------------------
static void copie_selection (void)
  {
  BOOL possible;
  char    texte [NBR_CAR_LIGNE_MAX];

  if (bOuvrir_presse_papier (ContexteEditeur.nom_presse_papier, pp_en_ecriture))
    {
    consulterln_selection_bd (texte, &possible, NBR_CAR_LIGNE_MAX);
    while (possible)
      {
      ecrire_ligne_presse_papier (texte);
      consulterln_selection_bd (texte, &possible, NBR_CAR_LIGNE_MAX);
      }
    fermer_presse_papier ();
    }
  }
// ----------------------------------------------------------------------
static void selectionne_item (void)
  {
  DWORD ligne;
  DWORD colonne;

  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  selection_item_bd (ligne);
  }

// ----------------------------------------------------------------------
static void reaffiche (void)
// ATTENTION : memorise et restaure position curseur ecran
  {
  memoriser_curseur_fenetre (ContexteCf.dwNFenetreEdit);
  effacer_fenetre (ContexteCf.dwNFenetreEdit);
  DWORD decalage = 1;
  if (bd_fini ())
    {
    ecrit_ligne (); // pour restaurer etat saisie (rouge)
    }
  else
    {
    curseur_invisible_fenetre (ContexteCf.dwNFenetreEdit);
    while ((!bd_fini ()) && (decalage))                // decalage 0 si pas possible
      {
      ecrit_ligne ();
      descendre_curseur_fenetre (ContexteCf.dwNFenetreEdit, decalage);
      }
    ecrit_ligne (); // pour restaurer etat saisie (rouge)
    curseur_visible_fenetre (ContexteCf.dwNFenetreEdit);
    restaurer_curseur_fenetre (ContexteCf.dwNFenetreEdit);
    }
  selectionne_item ();
  }

// ----------------------------------------------------------------------
static void ligne_suivante (void)
  {
  #define UNE_LIGNE 1
  DWORD decalage = UNE_LIGNE;
  if (!bd_fini ())
    {
    descendre_curseur_fenetre (ContexteCf.dwNFenetreEdit, decalage);
    if (!decalage)
      {
      decalage = UNE_LIGNE;
      descendre_segment_bd (&decalage);
      if (decalage)
        {
        scroller_fenetre_ligne (ContexteCf.dwNFenetreEdit, decalage);
        }
      }
    }
  ecrit_ligne();
  }

// ----------------------------------------------------------------------
static void nouvelle_ligne (void)
  {
  BOOL possible;
  BOOL ligne_vierge;
  BOOL en_dessous;
  char texte [NBR_CAR_LIGNE_MAX];
  DWORD erreur_bd;
  ETAT_LIGNE_BD type;
  DWORD ligne;
  DWORD colonne;
  DWORD decalage;
  int niveau;
  int indentation;

  possible = TRUE;
  en_dessous = FALSE;
  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  consulter_bd (ligne, texte, &type, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
  ligne_vierge = (BOOL)((type == ligne_invalide) && (StrIsNull (texte)));  // tres sale mais delicat
  if ((colonne != 1) || (ligne_vierge))
    {
    en_dessous = TRUE;
    ligne++;                                       // insertion en dessous
    consulter_bd (ligne, texte, &type, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX); // derniere ligne => pas possible
    }
  if ((possible) || (ligne_vierge))      // pas d'insertion si derniere ligne sauf si vierge
    {
    StrSetNull (texte);
    inserer_bd (ligne, texte, &erreur_bd, FORCAGE);
    if (erreur_bd != 0)
      {
      // seul cas invalide : le descripteur systeme --> on simule passage ligne suivante
      ligne_suivante ();
      }
    else
      {
      if (en_dessous)
        {
        decalage = UNE_LIGNE;
        descendre_curseur_fenetre (ContexteCf.dwNFenetreEdit, decalage);
        if (!decalage)
          {
          decalage = UNE_LIGNE;
          descendre_segment_bd (&decalage);
          scroller_fenetre_ligne (ContexteCf.dwNFenetreEdit, decalage);
          }
        else
          {
          monter_curseur_fenetre (ContexteCf.dwNFenetreEdit, &decalage);
          inserer_ligne_fenetre (ContexteCf.dwNFenetreEdit);
          if (ligne_vierge)
            {
            decalage = UNE_LIGNE;
            descendre_curseur_fenetre (ContexteCf.dwNFenetreEdit, decalage);
            }
          }
        }
      else
        {
        inserer_ligne_fenetre (ContexteCf.dwNFenetreEdit);
        }
      ecrit_ligne ();
      }
    }
  else
    {
    ligne_suivante ();
    aller_debut_ligne_fenetre (ContexteCf.dwNFenetreEdit);
    }
  }

// ----------------------------------------------------------------------
static DWORD valide_ligne (BOOL avec_dialogue)
  {
  ETAT_LIGNE_BD type_ligne_bd;
  char nv_texte [NBR_CAR_LIGNE_MAX];
  char mem_texte [NBR_CAR_LIGNE_MAX];
  DWORD erreur_bd;
  DWORD retour_operateur;
  DWORD ligne;
  DWORD colonne;
  BOOL possible;
  BOOL suppression_possible;
  int niveau;
  int indentation;

  retour_operateur = ID_POURSUIVRE;
  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  if ((ContexteEditeur.saisie_en_cours | avec_dialogue) && (ligne > 0))
    {
    consulter_ligne_fenetre (ContexteCf.dwNFenetreEdit, nv_texte);
    consulter_bd (ligne, mem_texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
    if (type_ligne_bd == ligne_invalide)
      {
      supprimer_bd (ligne, &erreur_bd, &suppression_possible); // pas possible si vierge
      inserer_bd (ligne, nv_texte, &erreur_bd, SANS_FORCAGE);     // insertion au dessus
      if (erreur_bd != 0)
        {
        if ((avec_dialogue) && (ContexteEditeur.analyse_syntaxique))
          {
          dialoguer_operateur (ContexteCf.dwNFenetreEdit, erreur_bd, &retour_operateur);
          }
        switch (retour_operateur)
          {
          case ID_ABANDONNER:
            if (suppression_possible)
              {
              inserer_bd (ligne, mem_texte, &erreur_bd, FORCAGE);
              }
            ecrit_ligne ();
            break;
          case ID_POURSUIVRE:
            inserer_bd (ligne, nv_texte, &erreur_bd, FORCAGE);
            ecrit_ligne ();
            break;
          case ID_CORRECTION:
            if (suppression_possible)
              {
              inserer_bd (ligne, mem_texte, &erreur_bd, FORCAGE);
              }
            break;
          default:
            break;
          } // switch
        }
      else
        {
        ecrit_ligne ();
        }
      }  // fin ligne initiale invalide
    else
      {
      modifier_bd (ligne, nv_texte, &erreur_bd, &possible); // tjrs possible car bd vide renvoi ligne invalide
      if (erreur_bd != 0)
        {
        if ((avec_dialogue) && (ContexteEditeur.analyse_syntaxique))
          {
          dialoguer_operateur (ContexteCf.dwNFenetreEdit, erreur_bd, &retour_operateur);
          }
        switch (retour_operateur)
          {
          case ID_ABANDONNER:
            ecrit_ligne ();
            break;
          case ID_POURSUIVRE:
            supprimer_bd (ligne, &erreur_bd, &possible);
            if (erreur_bd == 0)  // suppression ancienne ligne possible
              {
              inserer_bd (ligne, nv_texte, &erreur_bd, FORCAGE);  // insertion tjrs possible
              }
            ecrit_ligne ();  // si sup impossible => abandon
            break;
          case ID_CORRECTION:
            break;
          default:
            break;
          } // switch
        }
      else
        {
        ecrit_ligne ();
        }
      }  // fin pas ligne initiale invalide
    } // avec dialogue ou saisie_en cours
  if (retour_operateur != ID_CORRECTION)
    {
    ContexteEditeur.saisie_en_cours = FALSE;
    }
  return (retour_operateur);
  }

// ----------------------------------------------------------------------
static void valide_tout_desc (void)
  {
  ETAT_LIGNE_BD type_ligne_bd;
  char texte [NBR_CAR_LIGNE_MAX];
  DWORD erreur_bd;
  DWORD ligne;
  BOOL possible;
  BOOL suppression_possible;
  int niveau;
  int indentation;

  positionner_segment_bd_100 (0);
  possible = TRUE;
  for (ligne = 1; possible ; ligne++)
    {
    consulter_bd (ligne, texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
    if (possible)
      {
      if (type_ligne_bd == ligne_invalide)
        {
        supprimer_bd (ligne, &erreur_bd, &suppression_possible); // pas possible si vierge
        inserer_bd (ligne, texte, &erreur_bd, SANS_FORCAGE);     // insertion au dessus
        if (erreur_bd != 0)
          {
          inserer_bd (ligne, texte, &erreur_bd, FORCAGE);     // insertion ligne invalide
          }
        } // ligne invalide
      } // consultation possible
    } // for
  }

// ----------------------------------------------------------------------
static void fin_selection (void)
  {
  if (ContexteEditeur.en_selection)
    {
    abandon_selection_bd ();
    abandon_selection_fenetre (ContexteCf.dwNFenetreEdit);
    ContexteEditeur.en_selection = FALSE;
    }
  }

// ----------------------------------------------------------------------
static void OnActionImport (PCSTR pszNomSource, int nTypeImport)
	{
  BOOL	bOk = TRUE; // test si tous les descripteurs
  char  nom_presse_papier[MAX_PATH];

  patienter (TRUE);
  CacheAide ();
  pszCreeNameExt (nom_presse_papier, MAX_PATH, pszNomSource, EXTENSION_SRC);

  if (bOuvrir_presse_papier (nom_presse_papier, pp_en_lecture))
    {
		DWORD  index_invalide = 0;
	  DWORD  decalage = 1;
		BOOL	fin_pp = FALSE;
    DWORD	ligne = 1;
    DWORD	n_erreur_import_syn = 0;
    DWORD	numero_desc = premier_driver;
    DWORD type_ligne_pp_attendu = LIGNE_META_PP_APPLIC;
		char  texte [NBR_CAR_LIGNE_MAX];
		char  TexteDescManquant [NBR_CAR_LIGNE_MAX];
		BOOL	possible;
		DWORD type_ligne_pp;
		char  nom_fic_applic_syn[MAX_PATH];
		DWORD  ligne_desc_fic;
		char  nv_texte [NBR_CAR_LIGNE_MAX];
		char  mem_texte [NBR_CAR_LIGNE_MAX];
		char  contexte_descripteur_anim [2];
		DWORD pos_relative_ligne_anim;
		DWORD offset_ligne_anim;
		DWORD erreur_bd;
		PIMPORT_INVALIDE ptr_import_invalide;
		DWORD dw;
		DWORD  n_ligne_erreur_syn;
		char  mess_erreur[c_nb_car_message_err];

    consulter_ligne_presse_papier (texte, &possible);
    while ((possible) && (!fin_pp))
      {
      analyse_ligne_presse_papier (texte, &numero_desc, &type_ligne_pp);
      if ((type_ligne_pp_attendu == LIGNE_PP_QUELCONQUE) || (type_ligne_pp == type_ligne_pp_attendu))
        {
        switch (type_ligne_pp)
          {
          case LIGNE_META_PP_APPLIC:
            if (nTypeImport == IMPORT_REMPLACEMENT)
              {
							raz_bd();
              init_application (pszNomSource, EXTENSION_SOURCE);
							SetNouveauDoc (FALSE);
							ResetFichierMessagesAlarme ();
              }
            cree_bloc (repere_e, sizeof (IMPORT_INVALIDE), 0);
            initialiser_ctxtbd_desc (premier_driver);
            type_ligne_pp_attendu = LIGNE_META_PP_DESCRIPTEUR;
            break;
          case LIGNE_META_PP_DESCRIPTEUR:
            if (numero_desc <= dernier_driver)   // si descripteur non present
              {
              ligne_desc_fic = 1;
              positionner_segment_bd_100 (0); // remet curseur prec a 1
              finir_desc_bd (FALSE);
              initialiser_ctxtbd_desc (numero_desc);
              type_ligne_pp_attendu = LIGNE_PP_VIERGE;
              }
						else
							{
							StrCopy (TexteDescManquant,texte);
							}
            break;
          case LIGNE_META_PP_ANIMATION:
            ligne_desc_fic = 1;
            positionner_segment_bd_100 (0);
            finir_desc_bd (FALSE);
            initialiser_ctxtbd_desc (numero_desc);
            positionner_segment_bd_100 (100);
            descendre_segment_bd (&decalage);
            decalage = 1;
            lire_contexte_bd (1, mem_texte, nv_texte, contexte_descripteur_anim,
							&pos_relative_ligne_anim, &offset_ligne_anim);
            type_ligne_pp_attendu = LIGNE_PP_VIERGE;
            break;
					case LIGNE_PP_VIERGE:
            if (type_ligne_pp_attendu == LIGNE_PP_VIERGE)
              {
              type_ligne_pp_attendu = LIGNE_PP_QUELCONQUE;
              }
            else  //  attendu == quelconque
              {
              inserer_bd (ligne, texte, &erreur_bd, FORCAGE);
              ligne_desc_fic++;
              descendre_segment_bd (&decalage);
              decalage = 1;
              }
            break;
          case LIGNE_META_PP_FIN:
            fin_pp = TRUE;
            break;
          case LIGNE_PP_STANDARD:
            inserer_bd (ligne, texte, &erreur_bd, SANS_FORCAGE);
            if (erreur_bd != 0)
              {
              if (numero_desc != d_systeme)
                {
                index_invalide++;
                insere_enr (szVERIFSource, __LINE__, 1, repere_e, index_invalide);
                ptr_import_invalide = (PIMPORT_INVALIDE)pointe_enr (szVERIFSource, __LINE__, repere_e, index_invalide);
                ptr_import_invalide->n_desc = numero_desc;
                ptr_import_invalide->n_ligne = ligne_desc_fic;
                if (numero_desc == d_graf)
                  {
                  ptr_import_invalide->n_ligne = ptr_import_invalide->n_ligne + offset_ligne_anim;
                  }
                StrSetNull (ptr_import_invalide->texte);
                StrCopy (ptr_import_invalide->texte, texte);
                }
              }
            else
              {
              descendre_segment_bd (&decalage);
              decalage = 1;
              }
            ligne_desc_fic++;
            break;
          default:
            break;
          }  // switch
        } // if ligne attendu
      consulter_ligne_presse_papier (texte, &possible);
      } // while
    fermer_presse_papier ();
		
    // passe n� 2 //
    finir_desc_bd (FALSE);
    initialiser_ctxtbd_desc (numero_desc);
    positionner_segment_bd_100 (0);
		if (existe_repere (repere_e))
			{
			dw = nb_enregistrements (szVERIFSource, __LINE__, repere_e);
			for (index_invalide = 1; (index_invalide <= dw); index_invalide++)
				{
				ptr_import_invalide = (PIMPORT_INVALIDE)pointe_enr (szVERIFSource, __LINE__, repere_e, index_invalide);
				if (ptr_import_invalide->n_desc <= dernier_driver)
					{ // blindage collection avec descripteurs manquants
					if (ptr_import_invalide->n_desc != numero_desc)
						{
						positionner_segment_bd_100 (0);
						finir_desc_bd (FALSE);
						numero_desc = ptr_import_invalide->n_desc;
						initialiser_ctxtbd_desc (numero_desc);
						positionner_segment_bd_100 (0);
						}
					inserer_bd (ptr_import_invalide->n_ligne, ptr_import_invalide->texte, &erreur_bd, SANS_FORCAGE);
					if (erreur_bd != 0)
						{
						inserer_bd (ptr_import_invalide->n_ligne, ptr_import_invalide->texte, &erreur_bd, FORCAGE);
						}
					}
				else
					{
					bOk = FALSE;
					}
				}
	    enleve_bloc (repere_e);
			}
    positionner_segment_bd_100 (0);
    finir_desc_bd (FALSE);

		// Passe � l'import du synoptique : fichier pr�sent ?
		pszCreeNameExt (nom_fic_applic_syn, MAX_PATH, pszNomSource, EXTENSION_SYN);
    if (bFicPresent (nom_fic_applic_syn))
      {
			// oui => importe comme demand�
      if (nTypeImport == IMPORT_REMPLACEMENT)
        {
        n_erreur_import_syn = dwImporteSynoptique (nom_fic_applic_syn, &n_ligne_erreur_syn, IMPORT_SYNOP_REMPLACEMENT, 0, TRUE); // avec identifiant d'origine
        }
      else
        {
        n_erreur_import_syn = dwImporteSynoptique (nom_fic_applic_syn, &n_ligne_erreur_syn, IMPORT_SYNOP_AJOUT, offset_ligne_anim, TRUE); // avec identifiant d'origine
        }
			// signale une �ventuelle erreur
      if (n_erreur_import_syn != 0)
        {
				char szErr[MAX_PATH+c_nb_car_message_err+100]="";
        bLngLireErreur (n_erreur_import_syn, mess_erreur);
				StrPrintFormat (szErr, "Erreur import synoptique : %s\nFichier %s\nLigne %d", mess_erreur,nom_fic_applic_syn,n_ligne_erreur_syn);
    //    StrDWordToStr (mess_erreur, n_ligne_erreur_syn);
    //    StrEnclose (mess_erreur, '(import ligne ', ')');
    //    StrInsertChar (mess_erreur, ' ', 0);
    //    StrConcat (mess_erreur_bis, mess_erreur);
				//StrPrintFormat(szErr, "%s",mess_erreur);
        //dlg_erreur (mess_erreur_bis);
        dlg_erreur (szErr);
        }
      }
    if (n_erreur_import_syn == 0)
      {
      if (nTypeImport == IMPORT_REMPLACEMENT)
        {
        // $$ Inutile ??? 
				//set_nom_application (nom_fic_applic_pp);
        }
      ContexteEditeur.numero_desc_cour = premier_driver;
      initialiser_ctxtbd_desc (ContexteEditeur.numero_desc_cour);
      reaffiche ();
      }
		met_contexte_a_jour ();
		patienter (FALSE);
		//
		if (!bOk)
			{ // il manque des descripteurs dans cette collection
      bLngLireErreur (424, mess_erreur);
			char szErr[MAX_PATH+c_nb_car_message_err+100]="";
			StrPrintFormat (szErr, "Erreur import synoptique : %s\nFichier %s", mess_erreur, nom_presse_papier);
      //StrConcat (mess_erreur, ": ");
      //StrConcat (mess_erreur, TexteDescManquant);
      //dlg_erreur (mess_erreur);
			}
		}
	} // OnActionImport

// ----------------------------------------------------------------------
static void OnActionExport (PCSTR pszNomDest, BOOL bAnnonceLignesInvalides)
	{
	char  texte [NBR_CAR_LIGNE_MAX];
	char  nom_presse_papier[MAX_PATH];
  
	// le nom arrive complet sans suffixe 
	patienter (TRUE);
	fin_selection ();
	StrSetNull (texte);

	// Export du synoptique. (a conserver en premier pour int�grit� animation)
	pszCreeNameExt (texte, MAX_PATH, pszNomDest, EXTENSION_SYN);
	VerifWarning (bExporteSynoptique (texte, TRUE));

	// Export des descripteurs
	pszCreeNameExt (nom_presse_papier, MAX_PATH, pszNomDest, EXTENSION_SRC);
	if (bOuvrir_presse_papier (nom_presse_papier, pp_en_ecriture))
		{
		DWORD nb_desc_export;
		DWORD index_desc_export;
		DWORD numero_desc;
		DWORD c_inf_mnemo_desc;
		DWORD c_inf_titre_desc;
		DWORD tab_desc_export[dernier_driver];  
		ETAT_LIGNE_BD type_ligne_bd;
		BOOL	possible;
		int   niveau;
		int   indentation;
		char  mes_inf[c_nb_car_message_inf];  
		DWORD  ligne;
		BOOL bLigneInvalideDetectee = FALSE;
  
  	StrCopy (texte, APPLICATION_PP);
		StrConcat (texte, pszNomDest);
		ecrire_ligne_presse_papier (texte);
		StrSetNull (texte);
		ecrire_ligne_presse_papier (texte);
		finir_desc_bd (FALSE);
		nb_desc_export = 0;
		for (numero_desc = 1; (numero_desc <= dernier_driver); numero_desc++)
			{
			if (bInfoDescripteur (numero_desc, &c_inf_titre_desc, &c_inf_mnemo_desc))
				{
				if ((numero_desc != d_code) && (numero_desc != d_graf) && (numero_desc != d_repete))
					{
					tab_desc_export [nb_desc_export] = numero_desc;
					nb_desc_export++;
					}
				}
			}
		tab_desc_export [nb_desc_export] = d_graf;
		nb_desc_export++;
		tab_desc_export [nb_desc_export] = d_repete;
		nb_desc_export++;
		tab_desc_export [nb_desc_export] = d_code;

		for (index_desc_export = 0; (index_desc_export <= nb_desc_export); index_desc_export++)
			{
			// Comptabilise le nb d'erreurs dans le descripteur
			bInfoDescripteur (tab_desc_export [index_desc_export], &c_inf_titre_desc, &c_inf_mnemo_desc);
			initialiser_ctxtbd_desc (tab_desc_export [index_desc_export]);
			consulter_bd (1, texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
			if (possible)
				{
				DWORD dwNbLignesInvalidesDansDescripteur = 0;
				DWORD dwNPremiereLigneInvalide = 0;
				StrCopy (texte, DESCRIPTEUR_PP);
				bLngLireInformation (c_inf_mnemo_desc, mes_inf);
				StrConcat (texte, mes_inf);
				char  szNomDescripteur [NBR_CAR_LIGNE_MAX]="";
				StrCopy (szNomDescripteur, mes_inf);
				ecrire_ligne_presse_papier (texte);
				StrCopy (texte, " ");
				ecrire_ligne_presse_papier (texte);
				positionner_segment_bd (1);
				for (ligne = 1, possible = TRUE; (possible); ligne++)
					{
					consulter_bd (ligne, texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
					// Prise en compte des lignes invalides
					if (type_ligne_bd == ligne_invalide)
						{
						char  szLigneInvalide [NBR_CAR_LIGNE_MAX];
						StrCopy(szLigneInvalide,texte);
						StrDeleteFirstAndLastSpacesAndTabs(szLigneInvalide);
						if (StrLength(szLigneInvalide) != 0)
							{
							bLigneInvalideDetectee = TRUE;
							dwNbLignesInvalidesDansDescripteur += 1;
							if (dwNPremiereLigneInvalide == 0)
								{
								dwNPremiereLigneInvalide = ligne;
								}
							}
						}
					ecrire_ligne_presse_papier (texte);
					}
				// Annonce si demand� les lignes invalides du descripteur
				if (bAnnonceLignesInvalides && (dwNbLignesInvalidesDansDescripteur !=0))
					{
					char  szMessage [NBR_CAR_LIGNE_MAX + 50];
					StrPrintFormat(szMessage,"%s : %d lignes invalides export�es (Cf. ligne %d)", szNomDescripteur, dwNbLignesInvalidesDansDescripteur,dwNPremiereLigneInvalide);
					::MessageBox (Appli.hwnd, szMessage, Appli.szNom, MB_ICONEXCLAMATION|MB_OK);
					}
				}  // possible
			finir_desc_bd (FALSE);
			}  // for descripteurs
		StrCopy (texte, " ");
		ecrire_ligne_presse_papier (texte);
		StrCopy (texte, FIN_PP);
		ecrire_ligne_presse_papier (texte);
		fermer_presse_papier ();
		// Annonce si demand� si aucune lignes invalide n'a �t� d�tect�e
		if (bAnnonceLignesInvalides && (!bLigneInvalideDetectee))
			{
			::MessageBox (Appli.hwnd, "Export : pas de ligne invalide d�tect�e", Appli.szNom, MB_ICONINFORMATION|MB_OK);
			}
		initialiser_ctxtbd_desc (ContexteEditeur.numero_desc_cour);
		reaffiche ();
		met_contexte_a_jour ();
		}
	patienter (FALSE);
	}

// --------------------------------------------------------------------------
// analyse du bloc de commandes et ajout des actions associees
// --------------------------------------------------------------------------
static void InterpreteBlocCmd (BOOL bCdeValide)
  {
  t_param_action param_action;
  DWORD          dwNbEnr;
  DWORD          dwIndex;
  PCOMMANDE_PCS	pCommandes;

  if (bCdeValide)
    {
    if (existe_repere (BLOC_COMMANDES))
			{
      dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, BLOC_COMMANDES);
			}
    else
			{
      dwNbEnr = 0;
			} 
    if (dwNbEnr)
      {
      for (dwIndex = 1; dwIndex <= dwNbEnr; dwIndex++)
        {
        pCommandes = (PCOMMANDE_PCS)pointe_enr (szVERIFSource, __LINE__, BLOC_COMMANDES, dwIndex);
        StrCopy (param_action.chaine, pCommandes->pszChaine);
        switch (pCommandes->carCmd)
          {
          case '\0' :
						{
						// Rend le pathname du fichier absolu
						pszPathNameAbsolu (param_action.chaine, Appli.szPathInitial);

						// fichier SRC ?
						if (bExtensionPresente (param_action.chaine, EXTENSION_SRC))
							{
							// oui => import
							pszSupprimeExt(param_action.chaine);
							param_action.entier = premier_driver;
							param_action.booleen = FALSE;
							bAjouterActionCf (action_initialisation, &param_action);
							bAjouterActionCf (action_import, &param_action);
							bAjouterActionCf (action_sauve_application, &param_action);
							}
						else
							{
							// non => charge le fichier PCS (ou autre extension)
							if (bExtensionPresente (param_action.chaine, EXTENSION_SOURCE))
								pszSupprimeExt(param_action.chaine);
							bAjouterActionCf (action_charge_application, &param_action);
							param_action.entier = premier_driver;
							param_action.booleen = FALSE;
							bAjouterActionCf (action_initialisation, &param_action);
							}
						}
            break;

					case 'L'  : // Load
						{
						if (bExtensionPresente (param_action.chaine, EXTENSION_SOURCE))
							pszSupprimeExt(param_action.chaine);
						pszPathNameAbsolu (param_action.chaine, Appli.szPathInitial);
            bAjouterActionCf (action_charge_application, &param_action);
            param_action.entier = premier_driver;
						param_action.booleen = FALSE;
            bAjouterActionCf (action_initialisation, &param_action);
						}
            break;
          case 'S' :  // Save
						{
            if (!StrIsNull (param_action.chaine))
              { // sauve avec le nom de fichier precedent
							pszPathNameAbsolu (param_action.chaine, Appli.szPathInitial);
							if (bExtensionPresente (param_action.chaine, EXTENSION_SOURCE))
								pszSupprimeExt(param_action.chaine);
              bAjouterActionCf (action_sauve_application, &param_action);
              }
						}
            break;

          case 'I' : // Import
						{
            if (!StrIsNull (param_action.chaine))
              {
							pszPathNameAbsolu (param_action.chaine, Appli.szPathInitial);
							if (bExtensionPresente (param_action.chaine, EXTENSION_SRC))
								pszSupprimeExt(param_action.chaine);
              param_action.entier = premier_driver;
							param_action.booleen = FALSE;
              bAjouterActionCf (action_initialisation, &param_action);
              bAjouterActionCf (action_import, &param_action);
              bAjouterActionCf (action_sauve_application, &param_action);
              }
						}
            break;

					case 'E' : // Export
						{
            if (!StrIsNull (param_action.chaine))
              {
							if (bExtensionPresente (param_action.chaine, EXTENSION_SRC))
								pszSupprimeExt(param_action.chaine);
							pszPathNameAbsolu (param_action.chaine, Appli.szPathInitial);
              bAjouterActionCf (action_charge_application, &param_action);
              param_action.entier = premier_driver;
							param_action.booleen = FALSE;
              bAjouterActionCf (action_initialisation, &param_action);
							param_action.booleen = FALSE;
              bAjouterActionCf (action_export, &param_action);
              }
						}
            break;

					case 'Q' : // Quitte
						{
            bAjouterActionCf (action_quitte, &param_action);
						}
            break;

          default:
              break;
          }  // ------------ switch (char_cmd)
        }
      enleve_bloc (BLOC_COMMANDES);
      }
    else
      { // rien ds la cde ni ds cmdpcs => Nom defaut
      StrCopy (param_action.chaine, pszPathNameDoc());
      bAjouterActionCf (action_charge_application, &param_action);
      param_action.entier = premier_driver;
			param_action.booleen = FALSE;
      bAjouterActionCf (action_initialisation, &param_action);
      }
    }
  else
    { // cde non valide
    bAjouterActionCf (action_quitte, &param_action);
    }
  }


// ----------------------------------------------------------------------
//                         gestion Pile Actions                            
// -----------------------------------------------------------------------

// ----------------------------------------------------------------------
void lire_mot_recherche_courant (char *mot_recherche)
  {
  StrCopy (mot_recherche, ContexteEditeur.mot_recherche);
  }

// ----------------------------------------------------------------------
void InitContexteConf (void)
  {
  ContexteEditeur.saisie_en_cours = FALSE;
  ContexteEditeur.mode_saisie = en_insertion;
  ContexteEditeur.analyse_syntaxique = TRUE;
  ContexteEditeur.en_selection = FALSE;
  StrSetNull (ContexteEditeur.mot_recherche);
  ContexteEditeur.type_recherche = 1; //positif vers le bas, negatif vers le haut
  StrCopy (ContexteEditeur.nom_presse_papier, NOM_PP_VIRTUEL);
  }

static void OnActionAide(void)
	{
	Appli.m_Aide.bAfficheTout(Appli.hwnd);
	/*
	char    mes_inf[c_nb_car_message_inf];
  char    nom_fic_applic[MAX_PATH];

  bLngLireInformation (c_inf_application, mes_inf);
  StrCopy (nom_fic_applic, pszPathNameDoc());
  AjouteIndexAide(nom_fic_applic, mes_inf);
  MontreAide ();
  ChargeFichierAide (pszNomDescripteurCourant(), FALSE);
	*/
	}

static void	OnActionGeneration(void)
	{
  char    nom_satellite[MAX_PATH];
  char    param_satellite[MAX_PATH];
  char    env_satellite[MAX_PATH];
  char    chemin_satellite[MAX_PATH];
  cacher_plan_de_travail ();
	StrCopy (nom_satellite, Appli.szPathExe);
	StrConcat (nom_satellite, "pcsgen");
	StrSetNull (param_satellite);
	StrSetNull (env_satellite);
	StrSetNull (chemin_satellite);
	lancer_satellite_pm (TRUE, nom_satellite, param_satellite, env_satellite, chemin_satellite);
  montrer_plan_de_travail ();
	}

static void	Onaction_mode_saisie(void)
	{
  if (ContexteEditeur.mode_saisie == en_insertion)
    {
    ContexteEditeur.mode_saisie = en_modification;
    }
  else
    {
    ContexteEditeur.mode_saisie = en_insertion;
    }
  met_contexte_a_jour ();
	}

static void	Onaction_analyse_syntaxique(void)
	{
  ContexteEditeur.analyse_syntaxique = (BOOL)(!ContexteEditeur.analyse_syntaxique);
  met_contexte_a_jour ();
	}

static void	Onaction_quitte(void)
	{
	if (Appli.hwnd)
		DestroyWindow (Appli.hwnd);
	}

static void	Onaction_nouvelle_application(void)
	{
	raz_bd();
	InitPathNameDocVierge (EXTENSION_SOURCE);
	init_application (pszPathNameDoc(), EXTENSION_SOURCE);
	}

static void	Onaction_charge_application(PCSTR pszPathNameApplication)
	{
  patienter (TRUE);
  if (monter_application (pszPathNameApplication, EXTENSION_SOURCE))
		{
		SetNouveauDoc (FALSE);
		}
	else
    {
    dlg_num_erreur (449);
		init_application (pszPathNameApplication, EXTENSION_SOURCE);
		ResetFichierMessagesAlarme ();
		raz_bd();
		InitPathNameDocVierge (EXTENSION_SOURCE);
		init_application (pszPathNameDoc(), EXTENSION_SOURCE);
    }
  patienter (FALSE);
	}

static void	Onaction_sauve_application(PCSTR pszPathNameApplication)
	{
	patienter (TRUE);
	vider_presse_papier ();
  int erreur_r = sauver_application (pszPathNameApplication, EXTENSION_SOURCE);
	if ((erreur_r) != 0)
		{
		dlg_num_erreur (erreur_r);
		}
	else
		{
		SetNouveauDoc (FALSE);
		char    mes_inf[c_nb_car_message_inf];
		bLngLireInformation (c_inf_application, mes_inf);
		char    nom_fic_applic[MAX_PATH];
		StrCopy (nom_fic_applic, pszPathNameDoc());
		AjouteIndexAide(nom_fic_applic, mes_inf);

		// enregistrement du code du debugger
		pszCopiePathNameDocExt (nom_fic_applic, MAX_PATH, EXTENSION_DBG);
		uFicEfface (nom_fic_applic);
		HFDQ    hfic;
		if (uFicTexteOuvre  (&hfic, nom_fic_applic)==0)
			{
			initialiser_ctxtbd_desc (2);//CODE
			positionner_segment_bd(1);
			BOOL possible = TRUE;
			for (DWORD ligne = 1; possible; ligne++)
				{
				int niveau;
				int indentation;
				char    texte [NBR_CAR_LIGNE_MAX];
				ETAT_LIGNE_BD   type_ligne_bd;
				consulter_bd (ligne, texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
				if (possible)
					{
					uFicEcrireLn (hfic, texte);
					}
				}
			uFicFerme  (&hfic);
			}
		}
	initialiser_ctxtbd_desc (ContexteEditeur.numero_desc_cour);
	reaffiche ();
	// debugger

	met_contexte_a_jour ();
	patienter (FALSE);
	}

static void Onaction_initialisation(int nDriver, BOOL bModeAlternatif)
	{
	BOOL bExecutionSynchrone;

  if (nDriver != 0)
    { // pour changer de descripteur
    ContexteEditeur.numero_desc_cour = (DWORD)(nDriver);
    }
  initialiser_ctxtbd_desc (ContexteEditeur.numero_desc_cour);
  restaurer_marque_bd ();
  reaffiche ();
  sauver_marque_fenetre (ContexteCf.dwNFenetreEdit); // init pas propre dans le verbe mais efficace dans l'action
  met_contexte_a_jour ();
  CacheAide ();
	// Existe t'il un "satellite" associ� � ce driver ?
	if (satellite_existe_bd()&& (!bModeAlternatif))
		{
		// oui : lance s'il existe le satellite n�cessaire � l'ex�cution du descripteur et attend la fin de son ex�cution
		char    nom_satellite[MAX_PATH];
		char    param_satellite[MAX_PATH];
		char    env_satellite[MAX_PATH];
		char    chemin_satellite[MAX_PATH];
		if (lancer_satellite_bd (&bExecutionSynchrone, nom_satellite, param_satellite,
		 env_satellite, chemin_satellite))
			{
			sauver_application (pszPathNameDoc(), EXTENSION_SAT);
			cacher_plan_de_travail ();
			lancer_satellite_pm (bExecutionSynchrone, nom_satellite, param_satellite,
				env_satellite, chemin_satellite);
			montrer_plan_de_travail ();
			monter_application (pszPathNameDoc(), EXTENSION_SAT);
			ContexteEditeur.numero_desc_cour = premier_driver;
			initialiser_ctxtbd_desc (ContexteEditeur.numero_desc_cour);
			reaffiche ();
			met_contexte_a_jour ();
			}
		}
	}

#define avec_dialogue TRUE
#define sans_dialogue FALSE

static void Onaction_fermeture(int nFermeture) // $$ s�parer
	{
  DWORD   c_inf_mnemo_desc;
  DWORD   c_inf_titre_desc;
  fin_selection ();
  valide_ligne (sans_dialogue);
  switch (nFermeture)
    {
    case QUITTE:
			{
      finir_desc_bd (FALSE);
      for (DWORD numero_desc = premier_driver; (numero_desc <= dernier_driver); numero_desc++)
        {
        if (bInfoDescripteur (numero_desc, &c_inf_titre_desc, &c_inf_mnemo_desc))
          {
          initialiser_ctxtbd_desc (numero_desc);
          finir_desc_bd (TRUE);
          }
        }
			}
      break;

    case CHANGEMENT_DESCRIPTEUR:
      finir_desc_bd (FALSE);
      break;

    case FERME_TOUT:
			{
      finir_desc_bd (FALSE);
      for (DWORD numero_desc = premier_driver; (numero_desc <= dernier_driver); numero_desc++)
        {
        if (bInfoDescripteur (numero_desc, &c_inf_titre_desc, &c_inf_mnemo_desc))
          {
          initialiser_ctxtbd_desc (numero_desc);
          finir_desc_bd (TRUE);
          }
        }
			}
      break;

    default:
      break;
    }
	}

static void Onaction_redessine(void)
	{
  reaffiche ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_taille(int nNbLignesSupplementaires)
	{
  DWORD    ligne;
  DWORD    old_ligne;
  DWORD    colonne;
  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  old_ligne = ligne - nNbLignesSupplementaires;
  fin_selection ();
  valide_ligne (sans_dialogue); // validation sur mauvaise ligne si retrecissement fenetre : sale
  sauver_marque_bd (old_ligne);
  reaffiche ();
  if ((nNbLignesSupplementaires < 0) && (ligne != 0))
    { // retrecissement fenetre
    restaurer_marque_bd ();
    reaffiche ();
    aller_haut_fenetre (ContexteCf.dwNFenetreEdit);
    }
  met_contexte_a_jour ();
	}

static void Onaction_va_a_la_ligne(int nNLigneDesiree)
	{
  valide_ligne (sans_dialogue);
	DWORD dw = 1;
  if (nNLigneDesiree > 0)
    {
    dw = (DWORD)(nNLigneDesiree);
    }
  positionner_segment_bd (dw);
  positionner_selection_bd (dw);
  reaffiche ();
  aller_haut_fenetre (ContexteCf.dwNFenetreEdit);
  selectionne_item ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_premiere_ligne(void)
	{
  valide_ligne (sans_dialogue);
  positionner_segment_bd_100 (0);
  positionner_selection_bd_100 (0);
  reaffiche ();
  aller_haut_fenetre (ContexteCf.dwNFenetreEdit);
  selectionne_item ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_derniere_ligne(void)
	{
  valide_ligne (sans_dialogue);
  effacer_fenetre (ContexteCf.dwNFenetreEdit);
  positionner_segment_bd_100 (100);
  positionner_selection_bd_100 (100);
  ecrit_ligne ();
  selectionne_item ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_curseur_haut(int nNbLignes)
	{
	DWORD decalage = nNbLignes;
  valide_ligne (sans_dialogue);
  if (ContexteEditeur.en_selection)
    {
    monter_selection_bd (decalage);
    }
  monter_curseur_fenetre (ContexteCf.dwNFenetreEdit, &decalage);
  if (decalage != (DWORD)(nNbLignes))  //incomplet
    {
		DWORD decale_un = 1;

    decalage = nNbLignes - decalage;
    monter_segment_bd (&decalage);
    if (decalage)
      {
      scroller_fenetre_ligne (ContexteCf.dwNFenetreEdit, -(long)decalage);
      for (DWORD dw = 0; (dw < decalage); dw++)
        {
        ecrit_ligne ();
        decale_un = 1;
        descendre_curseur_fenetre (ContexteCf.dwNFenetreEdit, decale_un);
        }
      for (DWORD dw = 0; (dw < decalage); dw++)
        {
        decale_un = 1;
        monter_curseur_fenetre (ContexteCf.dwNFenetreEdit, &decale_un);
        }
      }
    }
  selectionne_item ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_curseur_bas(int nNbLignes)
	{
  valide_ligne (sans_dialogue);
  DWORD decalage = nNbLignes;
  if (ContexteEditeur.en_selection)
    {
    descendre_selection_bd (decalage);
    }
  descendre_curseur_fenetre (ContexteCf.dwNFenetreEdit, decalage);
  if ((bd_fini()) || (decalage != (DWORD)(nNbLignes)))     // cas peu frequent
    {
    monter_curseur_fenetre (ContexteCf.dwNFenetreEdit, &decalage); //restauration ancienne position
    for (int i = 0; (i < nNbLignes); i++) // algo sale mais efficace
      {
      ligne_suivante ();
      }
    }
  selectionne_item ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_page_precedente(void)
	{
  valide_ligne (sans_dialogue);
  DWORD decalage = nombre_ligne_fenetre (ContexteCf.dwNFenetreEdit);
  if (ContexteEditeur.en_selection)
    {
    monter_selection_bd (decalage);
    }
  monter_segment_bd (&decalage);
  reaffiche ();
  if (decalage != nombre_ligne_fenetre (ContexteCf.dwNFenetreEdit))   // pas complet
    {
    aller_haut_fenetre (ContexteCf.dwNFenetreEdit);
    }
  selectionne_item ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_page_suivante(void)
	{
  valide_ligne (sans_dialogue);
  DWORD decalage = nombre_ligne_fenetre (ContexteCf.dwNFenetreEdit);
  if (ContexteEditeur.en_selection)
    {
    descendre_selection_bd (decalage);
    }
  descendre_segment_bd (&decalage);
  if (decalage == nombre_ligne_fenetre (ContexteCf.dwNFenetreEdit))
    {
    reaffiche ();
    }
  else
    {
    decalage = 1;
    monter_segment_bd (&decalage);  // le curseur est descendu trop bas
    effacer_fenetre (ContexteCf.dwNFenetreEdit);
    ecrit_ligne ();             // ecriture de la derniere ligne avec curseur dessus
    }
  selectionne_item ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_pose_marque(void)
	{
	fin_selection ();
	valide_ligne (sans_dialogue);
	sauver_marque_bd (1);
	sauver_marque_fenetre (ContexteCf.dwNFenetreEdit);
	met_contexte_a_jour ();
	}

static void Onaction_va_a_la_marque(void)
	{
  fin_selection ();
  valide_ligne (sans_dialogue);
  restaurer_marque_bd ();
  restaurer_marque_fenetre (ContexteCf.dwNFenetreEdit);
  reaffiche ();
  met_contexte_a_jour ();
	}

static void Onaction_init_recherche_mot(int nTypeRecherche, PCSTR pszMotCherche)
	{
  fin_selection ();
  valide_ligne (sans_dialogue);
  StrCopy (ContexteEditeur.mot_recherche, pszMotCherche);
  ContexteEditeur.type_recherche = nTypeRecherche;
	}

static void Onaction_recherche_mot(void)
	{
  patienter (TRUE);
  fin_selection ();
  valide_ligne (sans_dialogue);
  if (!StrIsNull (ContexteEditeur.mot_recherche))
    {
		DWORD    ligne;
		DWORD    colonne;
    position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
    DWORD decalage;
		rechercher_mot_bd (ligne, ContexteEditeur.type_recherche, ContexteEditeur.mot_recherche, &decalage);
    if (decalage)
      {
      if (ContexteEditeur.type_recherche > 0) // vers le bas si positif
        {
        descendre_segment_bd (&decalage);
        }
      else
        {
        DWORD mem_decalage = decalage;
        monter_segment_bd (&decalage);
        decalage = mem_decalage - decalage;
        monter_curseur_fenetre (ContexteCf.dwNFenetreEdit, &decalage);
        }
      reaffiche ();
      met_contexte_a_jour ();
      }
    else
      {
      Sonne (40, 400);
      }
    }
  patienter (FALSE);
	}


static void Onaction_recherche_invalide(void)
	{
  patienter (TRUE);
  fin_selection ();
  valide_ligne (sans_dialogue);
	DWORD ligne;
	DWORD colonne;
  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  DWORD decalage;
  char texte [NBR_CAR_LIGNE_MAX];
	rechercher_mot_bd (ligne, 2, texte, &decalage);
  if (decalage)
    {
    descendre_segment_bd (&decalage);
    }
  else
    {
    Sonne (40, 400);
    }
  reaffiche ();
  met_contexte_a_jour ();
  patienter (FALSE);
	}

static void Onaction_debut_ligne(void)
	{
  aller_debut_ligne_fenetre (ContexteCf.dwNFenetreEdit);
  met_contexte_a_jour ();
	}

static void Onaction_fin_ligne(void)
	{
  aller_fin_ligne_fenetre (ContexteCf.dwNFenetreEdit);
  met_contexte_a_jour ();
	}

static void Onaction_efface_ligne(void)
	{
	DWORD ligne;
	DWORD colonne;
  fin_selection ();
  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  DWORD   erreur_bd;
  BOOL		possible;
  supprimer_bd (ligne, &erreur_bd, &possible);
  if (possible)
    {
    if (erreur_bd == 0)
      {
      reaffiche ();
      }
    else
      {
      dlg_num_erreur (erreur_bd);
      }
    } // possible
  else
    {
    ecrit_ligne ();
    }
  selectionne_item ();
  met_contexte_a_jour ();
	}

static void Onaction_curseur_gauche(DWORD decalage)
	{
  gaucher_curseur_fenetre  (ContexteCf.dwNFenetreEdit, decalage);
  met_contexte_a_jour ();
	}

static void Onaction_curseur_droite(int nDecalage)
	{
  droiter_curseur_fenetre  (ContexteCf.dwNFenetreEdit, (DWORD)(nDecalage));
  met_contexte_a_jour ();
	}

static void Onaction_mot_suivant(void)
	{
  droiter_mot_curseur_fenetre  (ContexteCf.dwNFenetreEdit);
  met_contexte_a_jour ();
	}

static void Onaction_mot_precedent(void)
	{
  gaucher_mot_curseur_fenetre  (ContexteCf.dwNFenetreEdit);
  met_contexte_a_jour ();
	}

static void Onaction_ecrire_car(char lettre)
	{
  fin_selection ();
  ContexteEditeur.saisie_en_cours = TRUE;
  if (ContexteEditeur.mode_saisie == en_insertion)
    {
    inserer_car_ligne_fenetre (ContexteCf.dwNFenetreEdit, lettre);
    }
  else
    {
    modifier_car_ligne_fenetre (ContexteCf.dwNFenetreEdit, lettre);
    }
  met_contexte_a_jour ();
	}

static void Onaction_efface_car(void)
			{
      if (ContexteEditeur.en_selection)
        {
        patienter (TRUE);
        valide_ligne (sans_dialogue);
				DWORD   erreur_bd;
        supprimer_selection_bd (&erreur_bd);
        if (erreur_bd)
          {
          dlg_num_erreur (erreur_bd);
          }
        fin_selection ();
        reaffiche ();
        patienter (FALSE);
        }
      else
        {
        fin_selection ();
        ContexteEditeur.saisie_en_cours = TRUE;
        supprimer_car_ligne_fenetre (ContexteCf.dwNFenetreEdit);
        }
      met_contexte_a_jour ();
			}

static void Onaction_back_car(void)
	{
  fin_selection ();
  ContexteEditeur.saisie_en_cours = TRUE;
  gaucher_curseur_fenetre (ContexteCf.dwNFenetreEdit, 1);
  supprimer_car_ligne_fenetre (ContexteCf.dwNFenetreEdit);
  met_contexte_a_jour ();
	}

static void Onaction_option_police(void)
	{
  fin_selection ();
	PageOptionPolice(ContexteCf.dwNFenetreEdit);
  //met_contexte_a_jour ();
	}

static void Onaction_validation(void)
	{
  fin_selection ();
  if (valide_ligne (avec_dialogue) == ID_POURSUIVRE)
    {
    if (ContexteEditeur.mode_saisie == en_insertion)
      {
      nouvelle_ligne();
      }
    else
      {
      ligne_suivante ();
      }
    //ecrit_ligne();
    }
  selectionne_item ();
  met_contexte_a_jour ();
	}

static void Onaction_valide_toute_ligne(void)
	{
  patienter (TRUE);
  fin_selection ();
  valide_ligne (sans_dialogue);
  sauver_marque_bd (1);
  sauver_marque_fenetre (ContexteCf.dwNFenetreEdit);
  valide_tout_desc ();
  restaurer_marque_bd ();
  restaurer_marque_fenetre (ContexteCf.dwNFenetreEdit);
  reaffiche ();
  met_contexte_a_jour ();
  patienter (FALSE);
	}

static void Onaction_debut_selection(void)
			{
      valide_ligne (sans_dialogue);
      if (!ContexteEditeur.en_selection)
        {
				DWORD ligne;
				DWORD colonne;
        ContexteEditeur.en_selection = TRUE;
        position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
        debut_selection_bd (ligne);
        reaffiche_selection ();
        met_contexte_a_jour ();
        }
			}

static void Onaction_selectionne_tout(void)
	{
  fin_selection ();
  valide_ligne (sans_dialogue);
  ContexteEditeur.en_selection = TRUE;
  positionner_segment_bd_100 (0);
  aller_haut_fenetre (ContexteCf.dwNFenetreEdit);
  selectionner_tout_bd ();
  reaffiche ();
  reaffiche_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_nomme_pp(PCSTR pszNomPressePapier)
	{
	StrCopy (ContexteEditeur.nom_presse_papier, pszNomPressePapier);
  met_contexte_a_jour ();
	}

static void Onaction_copie(void)
	{
  patienter (TRUE);
  valide_ligne (sans_dialogue);
  copie_selection ();
  fin_selection ();
	StrCopy (ContexteEditeur.nom_presse_papier, NOM_PP_VIRTUEL);
  met_contexte_a_jour ();
  patienter (FALSE);
	}

static void Onaction_colle(void)
	{
  patienter (TRUE);
  valide_ligne (sans_dialogue);
  if (bOuvrir_presse_papier (ContexteEditeur.nom_presse_papier, pp_en_lecture))
    {
    DWORD decalage = 1;
    DWORD decalage_retour = 0;
		DWORD ligne;
		DWORD colonne;
    position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
		char    texte [NBR_CAR_LIGNE_MAX];
		BOOL possible;
    consulter_ligne_presse_papier (texte, &possible);
    while (possible)
      {
			DWORD erreur_bd;
      inserer_bd (ligne, texte, &erreur_bd, SANS_FORCAGE);
      if (erreur_bd != 0)
        {
        inserer_bd (ligne, texte, &erreur_bd, FORCAGE);
        }
      descendre_segment_bd (&decalage);
      decalage_retour++;
      consulter_ligne_presse_papier (texte, &possible);
      }
    fermer_presse_papier ();
    monter_segment_bd (&decalage_retour);
    reaffiche ();
    }
	StrCopy (ContexteEditeur.nom_presse_papier, NOM_PP_VIRTUEL);
  met_contexte_a_jour ();
  patienter (FALSE);
	}

static void Onaction_coupe(void)
	{
  patienter (TRUE);
  valide_ligne (sans_dialogue);
  copie_selection ();
	DWORD erreur_bd;
  supprimer_selection_bd (&erreur_bd);
  if (erreur_bd)
    {
    dlg_num_erreur (erreur_bd);
    }
  fin_selection ();
  reaffiche ();
	 StrCopy (ContexteEditeur.nom_presse_papier, NOM_PP_VIRTUEL);
  met_contexte_a_jour ();
  patienter (FALSE);
	}

static void Onaction_fin_selection(void)
	{
  fin_selection ();
  met_contexte_a_jour ();
	}

static void Onaction_abandon(void)
	{
  fin_selection ();
  reaffiche ();
  ContexteEditeur.saisie_en_cours = FALSE;
  met_contexte_a_jour ();
	}

static void Onaction_imprime(int nTypeImpression)
	{
	LPRN_CONFIG_COM SerialData;
  DWORD   wPort;
  BOOL		bIsSerial;

  patienter (TRUE);
  valide_ligne (sans_dialogue);
  //
  if (definition_port_imprimante (PRN_DEFAUT, &wPort, &SerialData.wBaudRate, &SerialData.wDataBits,
		&SerialData.wParity, &SerialData.wStopBits, &bIsSerial))
    {
    SerialData.cXOn  = '\021';
    SerialData.cXOff = '\023';
    if (LImprimanteOuvre (PRN_DEFAUT, wPort, &SerialData) == CLpt::ERREUR_LPT_NONE)
      {
			BOOL		imp_selection;
			DWORD   numero_desc_cour;
			DWORD   numero_desc;
			DWORD   c_inf_mnemo_desc;
			DWORD   c_inf_titre_desc;
      switch (nTypeImpression)
        {
        case 1 :  // $$ tout
          finir_desc_bd (FALSE);
          imp_selection = FALSE;
          numero_desc_cour = ContexteEditeur.numero_desc_cour;
          for (numero_desc = premier_driver; (numero_desc <= dernier_driver); numero_desc++)
            {
            if (bInfoDescripteur (numero_desc, &c_inf_titre_desc, &c_inf_mnemo_desc))
              {
              initialiser_ctxtbd_desc (numero_desc);
              imprime_fenetre (imp_selection);
              finir_desc_bd (FALSE);
              }
            }
          ContexteEditeur.numero_desc_cour = numero_desc_cour;
          initialiser_ctxtbd_desc (ContexteEditeur.numero_desc_cour);
          reaffiche ();
          break;
        case 2 :  // la fenetre
          imp_selection = FALSE;
          imprime_fenetre (imp_selection);
          break;
        case 3 :  // la selection
          imp_selection = TRUE;
          imprime_fenetre (imp_selection);
          fin_selection ();
          break;
        default :
          break;
        }
      LImprimanteFerme (PRN_DEFAUT);
      }
    }
  met_contexte_a_jour ();
  patienter (FALSE);
	}

static void Onaction_ouvrir_boite(BOOL bBoiteDirecte, int niveau)
	{
	// ic�ne menu bo�te ou double clique et encha�nements des bo�tes
  valide_ligne (sans_dialogue);
  fin_selection ();
	DWORD ligne;
	DWORD colonne;
	char chaine[MAX_PATH];
  position_curseur_fenetre (ContexteCf.dwNFenetreEdit, &ligne, &colonne);
  if (bBoiteDirecte)
    { // on a clique sur l'icone boite ou sur un bouton du menu
    StrSetNull (chaine);
    }
  else
    { // double clique sur bouton 1
		int     indentation;
    niveau = lis_niveau_ligne_bd (ligne, chaine, &indentation, MAX_PATH);
    }
	// boite de dialogue existe ?
	UINT id_dlg_boite;
  FARPROC ptr_wnd_proc_boite;
  if (lis_param_boite_dlg (niveau, &id_dlg_boite, &ptr_wnd_proc_boite))
    {
		// oui => ouvre la boite de dialogue
		t_dlg_desc echange_data;

		echange_data.nb_variables = nb_enregistrements (szVERIFSource, __LINE__, bn_identif);
		echange_data.n_ligne_depart = ligne;
		StrCopy (echange_data.chaine, chaine);

		// Appel de la boite de dialogue de configuration
		LangueDialogBoxParam (MAKEINTRESOURCE(id_dlg_boite), Appli.hwnd,
			(DLGPROC)ptr_wnd_proc_boite, (LPARAM)&echange_data);

		// mise � jour de l'�cran apr�s fermeture de la boite de dialogue
		reaffiche ();
		met_contexte_a_jour ();
    }
	}

static void Onaction_enchaine_boite(t_retour_action *retour_action, int niveau, int nNLigneDepart)
	{
	//$$ appel�e par les DLL
  valide_ligne (sans_dialogue);
  fin_selection ();
  retour_action->n_ligne_depart = nNLigneDepart;
  StrSetNull (retour_action->chaine);
  lis_param_boite_dlg (niveau, &(retour_action->id_dlg_boite), &(retour_action->ptr_wnd_proc_boite));
	}

static void Onaction_nouvelle_ligne_dlg(t_retour_action *retour_action, int num_ligne)
	{ // $$appel�e par les DLL
	char    texte [NBR_CAR_LIGNE_MAX];
	ETAT_LIGNE_BD   type_ligne_bd;
  BOOL		possible;
  int     niveau;
  int     indentation;
  consulter_bd (num_ligne-1, texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
  StrSetNull (texte);
	DWORD erreur_bd;
  if (possible)
    {
    inserer_bd (num_ligne, texte, &erreur_bd, FORCAGE);
    DWORD decalage = 1;
    descendre_curseur_fenetre (ContexteCf.dwNFenetreEdit, decalage);
    }
  else
    {
    inserer_bd ((num_ligne-1), texte, &erreur_bd, FORCAGE);
    }
  retour_action->n_ligne_depart = num_ligne;
	}

static void Onaction_validation_dlg(t_retour_action *retour_action, PCSTR pszNouveauTexte, int num_ligne)
	{
	//$$ appel�e par les DLL
  retour_action->wErreur = 0;
  char    nv_texte [NBR_CAR_LIGNE_MAX];
  StrSetNull (nv_texte);
  StrCopy (nv_texte, pszNouveauTexte);
  char    mem_texte [NBR_CAR_LIGNE_MAX];
  BOOL		possible;
  ETAT_LIGNE_BD   type_ligne_bd;
  int     niveau;
  int     indentation;
  DWORD   erreur_bd;
  BOOL		suppression_possible;
  consulter_bd (num_ligne, mem_texte, &type_ligne_bd, &possible, &niveau, &indentation, NBR_CAR_LIGNE_MAX);
  if (type_ligne_bd == ligne_invalide)
    {
    supprimer_bd (num_ligne, &erreur_bd, &suppression_possible); // pas possible si vierge
    inserer_bd (num_ligne, nv_texte, &erreur_bd, SANS_FORCAGE);  // insertion au dessus
    if (erreur_bd != 0)
      {
      retour_action->wErreur = erreur_bd;
      dlg_num_erreur (erreur_bd);
      if (suppression_possible)
        {
        inserer_bd (num_ligne, mem_texte, &erreur_bd, SANS_FORCAGE); // restauration tjrs possible
        }
      }
    }  // fin ligne initiale invalide
  else
    {
    modifier_bd (num_ligne, nv_texte, &erreur_bd, &possible); // tjrs possible car bd vide renvoi ligne invalide
    if (erreur_bd != 0)
      {
      retour_action->wErreur = erreur_bd;
      dlg_num_erreur (erreur_bd);
      }
    }
  retour_action->n_ligne_depart = num_ligne;
	}

static void Onaction_traite_ligne_de_commande(PCSTR pszLigneDeCommande)
	{
	BOOL bCdeValide = FabriqueBlocCmd (pszLigneDeCommande);
	InterpreteBlocCmd (bCdeValide);
	}

// ----------------------------------------------------------------------
// Ex�cution imm�diate d'une action
static BOOL bExecuteUneActionCf (t_retour_action *retour_action, PACTION pAction)
  {
	BOOL     bRet = TRUE;
  
  switch (pAction->tipe)
    {
    case action_aide:
			OnActionAide();
      break;

		case action_generation:
			OnActionGeneration();
			break;

    case action_mode_saisie:
			Onaction_mode_saisie();
      break;

    case action_analyse_syntaxique:
			Onaction_analyse_syntaxique();
      break;

    case action_quitte :
			Onaction_quitte();
      break;

		case action_nouvelle_application:
			Onaction_nouvelle_application();
			break;

    case action_charge_application:
			Onaction_charge_application(pAction->chaine);
      break;

    case action_sauve_application:
			Onaction_sauve_application(pAction->chaine);
      break;

    case action_import:
			OnActionImport (pAction->chaine, pAction->entier);
      break;

    case action_export:
			OnActionExport (pAction->chaine, pAction->booleen);
      break;

    case action_initialisation:
			Onaction_initialisation(pAction->entier,pAction->booleen);
      break;

    case action_fermeture:
			Onaction_fermeture(pAction->entier);
      break;

    case action_redessine:
			Onaction_redessine();
      break;

    case action_taille:
			Onaction_taille(pAction->entier);
      break;

    case action_va_a_la_ligne:
			Onaction_va_a_la_ligne(pAction->entier);
      break;

    case action_premiere_ligne:
			Onaction_premiere_ligne();
      break;

    case action_derniere_ligne:
			Onaction_derniere_ligne();
      break;

    case action_curseur_haut:
			Onaction_curseur_haut(pAction->entier);
      break;

    case action_curseur_bas:
			Onaction_curseur_bas(pAction->entier);
      break;

    case action_page_precedente:
			Onaction_page_precedente();
      break;

    case action_page_suivante:
			Onaction_page_suivante();
      break;

    case action_pose_marque:
			Onaction_pose_marque();
      break;

    case action_va_a_la_marque:
			Onaction_va_a_la_marque();
			break;

    case action_init_recherche_mot:
			Onaction_init_recherche_mot(pAction->entier, pAction->chaine);
      break;

    case action_recherche_mot:
			Onaction_recherche_mot();
      break;

    case action_recherche_invalide:
			Onaction_recherche_invalide();
      break;

    case action_debut_ligne:
			Onaction_debut_ligne();
      break;

    case action_fin_ligne:
			Onaction_fin_ligne();
      break;

    case action_efface_ligne:
			Onaction_efface_ligne();
      break;

    case action_curseur_gauche:
			Onaction_curseur_gauche((DWORD)pAction->entier);
      break;

    case action_curseur_droite:
			Onaction_curseur_droite(pAction->entier);
      break;

    case action_mot_suivant:
			Onaction_mot_suivant();
      break;

    case action_mot_precedent:
			Onaction_mot_precedent();
      break;

    case action_ecrire_car:
			Onaction_ecrire_car(pAction->lettre);
      break;

    case action_efface_car:
			Onaction_efface_car();
      break;

    case action_back_car:
			Onaction_back_car();
      break;

    case action_validation:
			Onaction_validation();
      break;

    case action_valide_toute_ligne:
			Onaction_valide_toute_ligne();
      break;

    case action_debut_selection:
			Onaction_debut_selection();
      break;

    case action_selectionne_tout:
			Onaction_selectionne_tout();
      break;

    case action_nomme_pp:
			Onaction_nomme_pp(pAction->chaine);
      break;

    case action_copie:
			Onaction_copie();
      break;

    case action_colle:
			Onaction_colle();
      break;

    case action_coupe:
			Onaction_coupe();
      break;

    case action_fin_selection:
			Onaction_fin_selection();
      break;

    case action_abandon:
			Onaction_abandon();
      break;

    case action_imprime:
			Onaction_imprime(pAction->entier);
      break;

    case action_ouvrir_boite :
			Onaction_ouvrir_boite(pAction->booleen, pAction->entier);
      break;

    case action_enchaine_boite : 
			Onaction_enchaine_boite(retour_action, pAction->entier, (int)pAction->mot);
      break;

    case action_nouvelle_ligne_dlg :
			Onaction_nouvelle_ligne_dlg(retour_action, pAction->entier);
      break;

    case action_validation_dlg :
			Onaction_validation_dlg(retour_action, pAction->chaine, pAction->entier);
      break;

    case action_traite_ligne_de_commande :
			Onaction_traite_ligne_de_commande(pAction->chaine);
      break;

		case action_option_police :
			Onaction_option_police();
    } // switch (pAction->tipe)
	return bRet;
  } // bExecuteUneActionCf

// ----------------------------------------------------------------------
// Ex�cution des actions dans la pile
BOOL bExecuteActionCf (t_retour_action *retour_action)
  {
  BOOL     bRet = TRUE;
	VerifWarningExit;
/*
  while (PileActions.wIndexLu != PileActions.wIndexEcrit)
    {
		if (PileActions.wIndexLu != NBR_MAX_ACTION)
			{
			PileActions.wIndexLu++;
			}
		else
			{
			PileActions.wIndexLu = 0;
			}
		PACTION pAction = &PileActions.aActions[PileActions.wIndexLu];
    if (!bExecuteUneActionCf (retour_action, pAction))
			bRet = FALSE;
    } // while nbr_action
*/
  return bRet;
  } // bExecuteActionCf

// ----------------------------------------------------------------------
// Ajout d'une action dans la queue des action
void bAjouterActionCf (DWORD action, t_param_action *param)
  {
	// Alloue un message de notification
	PMESSAGE_ACTION pMessageAction = (PMESSAGE_ACTION)pMemAlloueInit0 (sizeof *pMessageAction);

	pMessageAction->hdr.hwndFrom = NULL;
	pMessageAction->hdr.idFrom = ID_MESSAGE_ACTION;
	pMessageAction->hdr.code = action;
	
	PACTION paction = &pMessageAction->action;
  paction->tipe = action;
  switch (action)
    {
    case action_charge_application:
    case action_sauve_application:
    case action_nomme_pp:
		case action_traite_ligne_de_commande:
      StrCopy (paction->chaine, param->chaine);
      break;
    case action_export:
			paction->booleen = param->booleen;
      StrCopy (paction->chaine, param->chaine);
      break;

    case action_fermeture:
    case action_imprime:
    case action_taille:
    case action_va_a_la_ligne:
    case action_curseur_droite:
    case action_curseur_gauche:
    case action_curseur_bas:
    case action_curseur_haut:
    case action_nouvelle_ligne_dlg:
      paction->entier = param->entier;
      break;
    case action_initialisation:
      paction->booleen = param->booleen;
      paction->entier = param->entier;
      break;

    case action_ouvrir_boite:
      paction->booleen  = param->booleen;
      paction->entier = param->entier;
      break;

    case action_enchaine_boite:
      paction->booleen  = param->booleen;
      paction->entier = param->entier;
      paction->mot = param->mot;
      break;

    case action_ecrire_car:
      paction->lettre = param->lettre;
      break;

    case action_import:
    case action_init_recherche_mot:
    case action_validation_dlg:
      StrCopy (paction->chaine, param->chaine);
      paction->entier = param->entier;
      break;

    default: // pas de parametre
      break;
    }

	// Ajoute le message � la queue de message de l'application
	VerifWarning (::PostMessage (Appli.hwnd, WM_ACTION_CF, (WPARAM)ID_MESSAGE_ACTION, (LPARAM)pMessageAction));
  } // bAjouterActionCf

// ----------------------------------------------------------------------
// Traite un message WM_NOTIFY
// S'il s'agit d'un message d'action, ex�cution imm�diate et renvoie TRUE
// sinon renvoie FALSE
BOOL bTraiteNotifyActionCf (LPARAM lparam)
  {
  BOOL bRes = FALSE;

	PMESSAGE_ACTION pMessageAction = (PMESSAGE_ACTION)lparam;

	// message de notification est de type action ?
	if (
		pMessageAction && 
		(pMessageAction->hdr.hwndFrom == NULL)&&
		(pMessageAction->hdr.idFrom = ID_MESSAGE_ACTION)
		)
		{
		// oui => traite l'action
		PACTION pAction = &pMessageAction->action;

		t_retour_action retour_action;
		bExecuteUneActionCf (&retour_action, pAction);

		// lib�re le message
		MemLibere ((PVOID*)&pMessageAction);

		// notification trait�e
		bRes = TRUE;
		}
  return bRes;
  } // bTraiteNotifyActionCf

/*
// ------------------------------------------------------------------------
// lecture cmd : demarrage, clavier ou macro
0void ChercheAction (PARAMS_CHERCHE_ACTION *pParamsChercheAction)
  {
  switch (pParamsChercheAction->mode_lecture)
    {
    case mode_demarrage:
			{
			BOOL bCdeValide = FabriqueBlocCmd (pParamsChercheAction->ligne_commande);
      InterpreteBlocCmd (bCdeValide);
			}
      break;
		case mode_interactif :
			{
			// lecture + dispatch d'un message de la queue de message
			MSG     msg;
			t_param_action param_action;

			if (GetMessage (&msg, NULL, 0, 0))
				{
				TranslateMessage(&msg);
				DispatchMessage (&msg);
				}
			else
				bAjouterActionCf (action_quitte, &param_action);
			}
      break;  // mode interactif

    case mode_boite:
			{
		  t_dlg_desc echange_data;

      echange_data.nb_variables = nb_enregistrements (szVERIFSource, __LINE__, bn_identif);
      echange_data.n_ligne_depart = pParamsChercheAction->n_ligne_depart;
      StrCopy (echange_data.chaine, pParamsChercheAction->chaine);

			LangueDialogBoxParam (MAKEINTRESOURCE(pParamsChercheAction->id_dlg_boite), Appli.hwnd,
				(DLGPROC)pParamsChercheAction->ptr_wnd_proc_boite, (LPARAM)&echange_data);
			}
      break;

    default:
      break;
    } // switch mode lecture
  }
*/

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : gener_co.c                                               |
 |   Auteur  : MC                                                       |
 |   Date    : 21/09/94                                                 |
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/
// generation du code executable Processyn
// Win32 18/6/98 JS
// utilis� en interpr�tation pour v�rifier la coh�rence et en generation du code executable

#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_co.h"
#include "tipe_re.h"
#include "mem.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "UChrono.h"
#include "tipe_tp.h"
#include "findbdex.h"
#include "IdLngLng.h"
#include "UExcept.h"
#include "UInter.h"
#include "GenereBD.h"

#include "gener_co.h"
#include "Verif.h"
VerifInit;

#define max_pile_code  100
#define b_libelle      897
#define nb_si_maxi     80
#define nb_boucle_maxi 80

typedef P_CODE t_pile_ins [max_pile_code];

typedef struct
	{
	char nom[c_nb_car_es]; //libell�s = m�me taille que les noms de variables
	DWORD no_inst;
	} LIBELLE, *PLIBELLE;

typedef struct
	{
	C_CODE c_ins;
	C_CODE c_ins1;
	P_CODE x_ins;
	P_CODE tempo_memorise;
	P_CODE indice_boucle;
	DWORD position_es;
	DWORD pos_nom_execute;
	PCS_OP_CODE PcsOpCode;
	DWORD instruc;
	FLOAT reel_temp;
	ID_MOT_RESERVE type_op1,type_op2,sens_op1;
	BOOL val_trouvee,deux_points_trouve;
	t_pile_ins pile_ins;
	int sp_ins;
	char representation_constante[c_taille_str];
	} CONTEXTE_COMPILE, *PCONTEXTE_COMPILE;


static PCONTEXTE_COMPILE pCxt; //$$ Global
//static PPARAM_COMPIL pam; //$$ Global
//static jmp_buf termine; //$$ Global + type

static BOOL fonction_deux_points(PPARAM_COMPIL pams);
static ID_MOT_RESERVE test(PPARAM_COMPIL pams, BOOL un_indice);
static void valide_crochet(PPARAM_COMPIL pams);

//--------------------------------------------------------------------------
//		    rafraichissement chronometre et metronome
//--------------------------------------------------------------------------
// $$ code redondant avec Scrut_tp.c!
static DWORD index_spec_metro (DWORD index_dans_bd)
  {
  BOOL  bTrouve = FALSE;
  DWORD  i = 0;

  while ((!bTrouve) && (i < nb_enregistrements (szVERIFSource, __LINE__, bx_metronome)))
    {
		PX_METRONOME ads_metro;

    i++;
    ads_metro = (PX_METRONOME)pointe_enr (szVERIFSource, __LINE__, bx_metronome,i);
    bTrouve = (BOOL)(ads_metro->index_dans_bd == index_dans_bd);
    } // while

  if (!bTrouve)
    i = 0;

  return i;
  }

//---------------------------------------------------------------------------
static DWORD index_spec_chrono(DWORD index_dans_bd)
  {
  PX_CHRONOMETRE	ads_chrono;
  BOOL  bTrouve = FALSE;
  DWORD  i = 0;
  while ((!bTrouve) && (i < nb_enregistrements (szVERIFSource, __LINE__, bx_chronometre)))
    {
    i++;
    ads_chrono = (PX_CHRONOMETRE)pointe_enr (szVERIFSource, __LINE__, bx_chronometre,i);
    bTrouve = (ads_chrono->index_dans_bd == index_dans_bd);
    } // while 

  if (!bTrouve)
    i = 0;

  return i;
  }

// -----------------------
static BOOL trouve_libelle(char *s_libelle,DWORD *index)
  {
  BOOL  bTrouve = FALSE;
  DWORD  i = 0;
	DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_libelle);

  while ((!bTrouve) && (i < dwNbEnr))
    {
    i++;
		PLIBELLE	pLibelle = (PLIBELLE)pointe_enr (szVERIFSource, __LINE__, b_libelle,i);
    bTrouve = bStrEgales(pLibelle->nom,s_libelle);
    }
	
  *index = i;
  return bTrouve;
  }

//  -----------------------
static DWORD position_nom_execute (char *nom_execute)
   {
   DWORD ligne,pointeur_tab;
   PTITRE_PARAGRAPHE	titre_paragraf;
   PSTR spec_titre_paragraf;
   BOOL fini;
   PGEO_REPETE	ads_val_geo;
   DWORD position_nom_execute = 0;

   if (existe_repere (b_geo_re))
     {
     pointeur_tab = 0;
     ligne = 0;
     fini = FALSE;
     while ((ligne < nb_enregistrements (szVERIFSource, __LINE__, b_geo_re)) && (!fini))
       {
       ligne++;
       ads_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,ligne);
       switch (ads_val_geo->numero_repere)
         {
         case b_titre_paragraf_re :
           titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re,ads_val_geo->pos_paragraf);
           if (titre_paragraf->IdParagraphe == structure_re)
             {
             pointeur_tab = pointeur_tab + 1;
             spec_titre_paragraf = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_re,ads_val_geo->pos_specif_paragraf);
             if (bStrEgales(nom_execute,spec_titre_paragraf))
               {
               position_nom_execute = pointeur_tab;
               fini = TRUE;
               }
             }
           break;

         case b_pt_constant:
         case b_e_s:
         case b_liste:
           break;

         default :
           break;

         } // switch
       }
     }
  return position_nom_execute;
  }

/* $$
//  --------------------
static void err(UINT code)
  {
  if (code!=0)
    {
    pam->erreur = code;
    longjmp(termine,-1); // $$ BEUARK!!!
    }
  }
*/

//  -------------------------------------------------------------
static void stocke_ins (PPARAM_COMPIL pams)
  {
  PSTR	ads_mes;
  PFLOAT  ads_num;
  PDWORD	pdwNLigneCode;
  PP_CODE ads_x_instruc;
	DWORD depart,interm;
  DWORD index_genc_genx;
  BOOL bTrouve;
  PNOM_TEMP_REPETE	ads_nom_structure;
  PTEMP_GENC_GENX	ads_genc_genx;
  DWORD i;

  if (pCxt->sp_ins != -1)
    {
    insere_enr (szVERIFSource, __LINE__, pCxt->sp_ins+1,bx_code,pams->depart_instruc_courante);
    if (pams->mode_debugger)
      {
      insere_enr (szVERIFSource, __LINE__, pCxt->sp_ins+1,bx_debug_code,pams->depart_instruc_courante);
      }
    for (i = 0;i <=(DWORD)pCxt->sp_ins;i++)
      {
      if (pams->mode_debugger)
        {
        pdwNLigneCode = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_debug_code, pams->depart_instruc_courante +i);
        (*pdwNLigneCode) = pams->ligne_courante;
        }
      switch (pCxt->pile_ins[i].PcsOpCode)
        {
        case push_const_num_t:
					{
          //////// non optimise sur les redondances
          depart=nb_enregistrements (szVERIFSource, __LINE__, bx_num)+1;
          insere_enr (szVERIFSource, __LINE__, 1,bx_num,depart);
          consulte_cst_bd_c (pCxt->pile_ins[i].operande,pCxt->representation_constante);
          ads_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_num,depart);
          if (!StrToFLOAT(ads_num,pCxt->representation_constante))
						ExcepErreurCompile(90);
          pCxt->pile_ins[i].operande=depart;
          pCxt->pile_ins[i].PcsOpCode=push_const_num;
					}
          break;

        case push_const_num:
					{
          // non optimise sur les redondances
          depart=nb_enregistrements (szVERIFSource, __LINE__, bx_num)+1;
          insere_enr (szVERIFSource, __LINE__, 1,bx_num,depart);
          consulte_cst_bd_c (pCxt->pile_ins[i].operande,pCxt->representation_constante);
          ads_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_num,depart);
          if (!StrToFLOAT(ads_num,pCxt->representation_constante))ExcepErreurCompile(90);
          supprime_cst_bd_c (pCxt->pile_ins[i].operande);
          pCxt->pile_ins[i].operande=depart;
					}
          break;

        case push_const_mes:
					{
          //////// optimise bestialement sur les redondances
          consulte_cst_bd_c (pCxt->pile_ins[i].operande,pCxt->representation_constante);
          depart = 0;
          bTrouve = FALSE;
          while ((depart < nb_enregistrements (szVERIFSource, __LINE__, bx_mes)) && (!bTrouve))
            {
            depart++;
            ads_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_mes,depart);
            bTrouve = bStrEgales(pCxt->representation_constante,ads_mes);
            } /* while */
          if (!bTrouve)
            {
            depart++;
            insere_enr (szVERIFSource, __LINE__, 1,bx_mes,depart);
            ads_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_mes,depart);
            StrCopy(ads_mes,pCxt->representation_constante);
            }
          supprime_cst_bd_c (pCxt->pile_ins[i].operande);
          pCxt->pile_ins[i].operande=depart;
					}
          break;

        case push_var_log:
        case push_avar_log:
        case pop_var_log:
        case push_val_operande_log:
					{
          ExcepErreurCompile(recherche_index_execution (pCxt->pile_ins[i].operande , &interm));
          pCxt->pile_ins[i].operande=interm;
					}
          break;

        case push_var_num:
        case push_avar_num:
        case pop_var_num:
        case push_val_operande_num:
					{
          ExcepErreurCompile(recherche_index_execution (pCxt->pile_ins[i].operande , &interm));
          pCxt->pile_ins[i].operande=interm - ContexteGen.nbr_logique;
					}
          break;

        case push_var_mes:
        case push_avar_mes:
        case pop_var_mes:
        case push_val_operande_mes:
					{
          ExcepErreurCompile(recherche_index_execution (pCxt->pile_ins[i].operande , &interm));
          pCxt->pile_ins[i].operande=interm - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
					}
          break;

        case depart_chrono:
        case arret_chrono:
        case continu_chrono :
        case raz_chrono :
					{
          ads_x_instruc = (PP_CODE)pointe_enr (szVERIFSource, __LINE__, bx_code, pams->depart_instruc_courante + (i-1));
          if (ads_x_instruc->PcsOpCode == push_val_operande)
            {
            // maj index chrono pour chrono non repetitif
            ExcepErreurCompile(recherche_index_execution (ads_x_instruc->operande , &interm));
            interm = interm - ContexteGen.nbr_logique;
            ads_x_instruc->operande = index_spec_chrono (interm);
            if (ads_x_instruc->operande == 0) ExcepErreurCompile(151);
            }
					}
          break;

        case depart_metro:
        case arret_metro :
					{
          ads_x_instruc = (PP_CODE)pointe_enr (szVERIFSource, __LINE__, bx_code, pams->depart_instruc_courante + (i-1));
          if (ads_x_instruc->PcsOpCode == push_val_operande)
            {
            // maj index metro pour metro non repetitif
            ExcepErreurCompile(recherche_index_execution (ads_x_instruc->operande , &interm));
            ads_x_instruc->operande = index_spec_metro (interm);
            if (ads_x_instruc->operande == 0) ExcepErreurCompile(152);
            }
					}
          break;

        case push_var_log_r:
        case push_var_num_r:
        case push_var_mes_r:
        case push_avar_log_r:
        case push_avar_num_r:
        case push_avar_mes_r:
        case push_const_log_r:
        case push_const_num_r:
        case push_const_mes_r:
        case push_index_metro_r:
        case push_index_chrono_r:
        case push_val_operande_r:
        case pop_var_log_r:
        case pop_var_num_r:
        case pop_var_mes_r:
					{
          if (existe_repere (b_temp_genc_genx))
            {
            index_genc_genx = 0;
            bTrouve = FALSE;
            while ((index_genc_genx < nb_enregistrements (szVERIFSource, __LINE__, b_temp_genc_genx)) && (!bTrouve))
              {
              index_genc_genx++;
              ads_genc_genx = (PTEMP_GENC_GENX)pointe_enr (szVERIFSource, __LINE__, b_temp_genc_genx,index_genc_genx);
              if (ads_genc_genx->pos_es == pCxt->pile_ins[i].operande)
                {
                pCxt->pile_ins[i].operande = ads_genc_genx->pos_bx_liste;
                bTrouve = TRUE;
                }
              } /* while */
            if (bTrouve)
              {
              ads_nom_structure = (PNOM_TEMP_REPETE)pointe_enr (szVERIFSource, __LINE__, b_temp_nom_structure,ads_genc_genx->pnt_b_temp_nom_structure);
              if (!bStrEgales(ads_nom_structure->nom,pams->nom_structure_cour)) ExcepErreurCompile(209);
              }
            else //bTrouve
              {
              ExcepErreurCompile(22);
              }
            }
          else   // repere inexistant
            {
            ExcepErreurCompile(22);
            }
					}
          break;

        case push_var_log_t:
        case push_var_num_t:
        case push_var_mes_t:
        case push_avar_log_t:
        case push_avar_num_t:
        case push_avar_mes_t:
        case pop_var_log_t:
        case pop_var_num_t:
        case pop_var_mes_t:
        case push_tableau:
					{
          ExcepErreurCompile(recherche_index_tableau (pCxt->pile_ins[i].operande , &interm));
          pCxt->pile_ins[i].operande=interm;
					}
          break;

        case push_var_num_i:
					{
          ExcepErreurCompile(recherche_index_execution (pCxt->pile_ins[i].operande , &interm));
          pCxt->pile_ins[i].operande=interm - ContexteGen.nbr_logique;
          pCxt->pile_ins[i].PcsOpCode = push_var_num;
					}
          break;

        case deb_execute:
					{
          if (existe_repere (b_temp_nom_structure))
            {
            ads_nom_structure = (PNOM_TEMP_REPETE)pointe_enr (szVERIFSource, __LINE__, b_temp_nom_structure,pCxt->pile_ins[i].operande);
            StrCopy(pams->nom_structure_cour,ads_nom_structure->nom);
            pCxt->pile_ins[i].operande = ads_nom_structure->nb_va_gen;
            // maj push val operande precedent avec la taille de la liste
            ads_x_instruc = (PP_CODE)pointe_enr (szVERIFSource, __LINE__, bx_code, pams->depart_instruc_courante + (i-1));
            ads_x_instruc->operande = ads_nom_structure->taille_liste;
            }
          else
            {
            ExcepErreurCompile(22);
            }
					}
          break;

        default:
          break;
        } // switch
      ads_x_instruc = (PP_CODE)pointe_enr (szVERIFSource, __LINE__, bx_code, pams->depart_instruc_courante + i);
      pCxt->x_ins = pCxt->pile_ins [i];
      (*ads_x_instruc)=pCxt->x_ins;
      } // for
    }
  } // stocke_ins

//  ------------------
static void suivant (PPARAM_COMPIL pams)
  {
  PC_CODE	ads_instruc;
  if (pams->instruc_source_courante > pams->fin_instructions_ligne)
    {
    pCxt->c_ins.GenreOpCode = uls_terminees;
    }
  else
    {
    if (pams->interpreteur)
      {
      pCxt->c_ins = pams->tampon_instructions[pams->instruc_source_courante-1];
      }
    else
      {
      ads_instruc = (PC_CODE)pointe_enr (szVERIFSource, __LINE__, b_code,pams->instruc_source_courante);
      pCxt->c_ins = (*ads_instruc);
      }
    pams->instruc_source_courante++;
    }
  }

// ---------------------------------------------
static void ajoute_instruc (PPARAM_COMPIL pams, PCS_OP_CODE instruc)
	{
	pCxt->sp_ins++;
	if (pCxt->sp_ins > max_pile_code) ExcepErreurCompile(210);
	pams->instruc_courante++;
	pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = instruc;
	}

// ---------------------------------------------
static void ajoute_instruc_operande (PPARAM_COMPIL pams, PCS_OP_CODE instruc, DWORD pnt_operande)
	{
	pCxt->sp_ins++;
	if (pCxt->sp_ins > max_pile_code) ExcepErreurCompile(210);
	pams->instruc_courante++;
	pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = instruc;
	pCxt->pile_ins[pCxt->sp_ins].operande = pnt_operande;
	}

// ---------------------------------------------
static void decale_instruc(PPARAM_COMPIL pams, int nb_instruc,int pos_operateur)
  {
  int i;
  if ((pCxt->sp_ins+nb_instruc) > max_pile_code) ExcepErreurCompile(210);
  for (i= pCxt->sp_ins;i>=pos_operateur;i--)
    {
    pCxt->pile_ins[i+nb_instruc] = pCxt->pile_ins[i];
    }
  pCxt->sp_ins = pCxt->sp_ins + nb_instruc ;
  pams->instruc_courante=pams->instruc_courante + nb_instruc;
  }

// ---------------------------------------------
static void copie_instruc(PPARAM_COMPIL pams, int pos_depart,int pos_fin)
	{
	for (int i = pos_depart; i<=pos_fin; i++)
		{
		pCxt->sp_ins++;
		if (pCxt->sp_ins > max_pile_code) ExcepErreurCompile(210);
		pCxt->pile_ins[pCxt->sp_ins] = pCxt->pile_ins[i];
		if (pCxt->pile_ins[i].PcsOpCode == push_const_num)
			{
			pCxt->pile_ins[i].PcsOpCode = push_const_num_t;
			}
		switch (pCxt->pile_ins[pCxt->sp_ins].PcsOpCode)
			{
			case push_var_log:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_log ;
				break;
			case push_var_num:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_num ;
				break;
			case push_var_mes:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_mes ;
				break;
			case push_var_log_r:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_log_r ;
				break;
			case push_var_num_r:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_num_r ;
				break;
			case push_var_mes_r:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_mes_r ;
				break;
			case push_var_log_t:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_log_t ;
				break;
			case push_var_num_t:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_num_t ;
				break;
			case push_var_mes_t:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_avar_mes_t ;
				break;
			case push_var_num_i:
				pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = push_var_num ;
				break;
			case push_avar_log:
			case push_avar_num:
			case push_avar_mes:
			case push_avar_log_r:
			case push_avar_num_r:
			case push_avar_mes_r:
			case push_avar_log_t:
			case push_avar_num_t:
			case push_avar_mes_t:
				ExcepErreurCompile(141);
				break;
			default:break ;
			}
		}
	pams->instruc_courante=pams->instruc_courante + pos_fin-pos_depart+1;
	}

// ---------------------------------------------
static void ajoute_si(PPARAM_COMPIL pams)
	{
	if (pams->sp_cond >= nb_si_maxi) ExcepErreurCompile(91);
	pCxt->sp_ins++;
	if (pCxt->sp_ins > max_pile_code) ExcepErreurCompile(210);
	pams->instruc_courante++;
	pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = saut_cond;
	pams->sp_cond++;
	pams->pile_cond[pams->sp_cond].position = pams->instruc_courante;
	pams->pile_cond[pams->sp_cond].sinon_present = FALSE ;
	pams->pile_cond[pams->sp_cond].boucle = FALSE ;
	}

// ---------------------------------------------
static void ajoute_sinon (PPARAM_COMPIL pams)
	{
	PP_CODE	ads_x_instruc;
	
	if (pams->sp_cond > 0)
		{
		if (pams->pile_cond[pams->sp_cond].boucle) ExcepErreurCompile(272);
		if (pams->pile_cond[pams->sp_cond].sinon_present)
			ExcepErreurCompile(92);
		else
			pams->pile_cond[pams->sp_cond].sinon_present = TRUE ;
		}
	else
		ExcepErreurCompile(93);
	pCxt->sp_ins++;
	if (pCxt->sp_ins > max_pile_code) ExcepErreurCompile(210);
	pams->instruc_courante++;
	pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = saut;
	ads_x_instruc = (PP_CODE)pointe_enr (szVERIFSource, __LINE__, bx_code,pams->pile_cond[pams->sp_cond].position);
	ads_x_instruc->operande=pams->instruc_courante + 1; // evite le saut absolu
	pams->pile_cond[pams->sp_cond].position=pams->instruc_courante; ////////
	if (pams->sp_cond_deb_execute != 0)
		if (pams->sp_cond == pams->sp_cond_deb_execute) ExcepErreurCompile(211);
	}

// --------------------------
static void ajoute_fin_cond (PPARAM_COMPIL pams)
	{
	PP_CODE	ads_x_instruc;
	
	if (pams->sp_cond <= 0) ExcepErreurCompile(94); // FIN_SI en trop
	if (pams->pile_cond[pams->sp_cond].boucle) ExcepErreurCompile(272);
	ads_x_instruc = (PP_CODE)pointe_enr (szVERIFSource, __LINE__, bx_code,pams->pile_cond[pams->sp_cond].position);
	ads_x_instruc->operande=pams->instruc_courante + 1;//ici on a saute le code indesirable;
	pams->sp_cond--;
	if (pams->sp_cond_deb_execute != 0)
		if (pams->sp_cond < pams->sp_cond_deb_execute) ExcepErreurCompile(211);
	}

// --------------------------
static void ajoute_principal (PPARAM_COMPIL pams)
	{
	PDWORD pdwNPCode;
	if (!existe_repere (bx_main_nb))
		{
		cree_bloc(bx_main_nb,sizeof(*pdwNPCode),1);
		pdwNPCode = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_main_nb,1);
		(*pdwNPCode) = pams->instruc_courante + 1;
		pams->presence_main = TRUE;
		}
	else
		{
		ExcepErreurCompile(434);
		}
	}

// --------------------------
static void ajoute_goto (PPARAM_COMPIL pams)
	{
	DWORD index;
	
	if (!existe_repere (b_libelle))
		cree_bloc(b_libelle,sizeof(LIBELLE),0);
	
	if (trouve_libelle(pCxt->representation_constante,&index))
		{
		PLIBELLE	pLibelle = (PLIBELLE)pointe_enr (szVERIFSource, __LINE__, b_libelle,index);
		ajoute_instruc_operande (pams, push_goto,pLibelle->no_inst+1);
		}
	else
		ExcepErreurCompile(435);
	}

// --------------------------
static void ajoute_libelle (PPARAM_COMPIL pams)
	{
	DWORD index;
	
	if (!existe_repere (b_libelle))
		cree_bloc(b_libelle,sizeof(LIBELLE),0);
	
	if (pams->sp_libelle == 0)
		{
		if (!trouve_libelle(pCxt->representation_constante,&index))
			{
			if (pams->presence_main) ExcepErreurCompile(439);
			insere_enr (szVERIFSource, __LINE__, 1,b_libelle,nb_enregistrements (szVERIFSource, __LINE__, b_libelle)+1);
			pams->sp_libelle++;
			PLIBELLE	pLibelle = (PLIBELLE)pointe_enr (szVERIFSource, __LINE__, b_libelle,nb_enregistrements (szVERIFSource, __LINE__, b_libelle));
			pLibelle->no_inst =pams->instruc_courante;
			// $$ JS
			if (StrLength (pCxt->representation_constante) >= c_nb_car_es) ExcepErreurCompile(IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
			// $$ fin JS
			StrCopy(pLibelle->nom,pCxt->representation_constante);
			pams->presence_libelle = TRUE;
			}
		else
			ExcepErreurCompile(437);
		}
	else
		ExcepErreurCompile(436);
	}

// --------------------------
static void ajoute_retour (PPARAM_COMPIL pams)
	{
	if (!existe_repere (b_libelle)) cree_bloc(b_libelle,sizeof(PLIBELLE),0);
	(pams->sp_libelle)--;
	if (pams->sp_libelle == 0)
		{
		ajoute_instruc(pams, pop_ret);
		}
	else
		ExcepErreurCompile(438);
	}

// ---------------------------------------------
static void ajoute_num_0 (DWORD *position_cst_0)
	{
	UL vect_ul_0;
	BOOL val_log,un_log,un_num,exist;
	DWORD position;
	FLOAT val_num;
	
	StrCopy(vect_ul_0.show,"0");
	vect_ul_0.genre = g_num ;
	vect_ul_0.erreur = 0 ;
	if (bReconnaitConstante (&vect_ul_0, &val_log, &val_num, &un_log, &un_num, &exist, &position))
		{
		AjouteConstanteBd (position,exist,vect_ul_0.show,position_cst_0);
		}
	}

// ---------------------------------------------
static void converti_message (PPARAM_COMPIL pams, ID_MOT_RESERVE *t_op1,ID_MOT_RESERVE *t_op2,int pos_op1)
	{
	DWORD position_cst_0;
	if (*t_op1 == c_res_message)
		{
		switch (*t_op2)
			{
			case c_res_logique :
				(*t_op2) = c_res_message ;
				if (!pams->interpreteur)
					{
					ajoute_num_0 (&position_cst_0);
					ajoute_instruc_operande (pams, push_const_num,position_cst_0);
					ajoute_instruc(pams, formate_log);
					}
				break;
			case c_res_numerique :
				(*t_op2) = c_res_message ;
				if (!pams->interpreteur)
					{
					ajoute_num_0(&position_cst_0);
					ajoute_instruc_operande (pams, push_const_num,position_cst_0);
					ajoute_num_0(&position_cst_0);
					ajoute_instruc_operande (pams, push_const_num,position_cst_0);
					ajoute_instruc(pams, formate_num);
					}
				break;
			default:break;
			}
		}
	else
		{
		if ((*t_op2) == c_res_message)
			{
			switch (*t_op1)
				{
				case c_res_logique :
					if (!pams->interpreteur)
						{
						decale_instruc(pams, 2,pos_op1);
						ajoute_num_0(&position_cst_0);
						pCxt->pile_ins[pos_op1].PcsOpCode = push_const_num;
						pCxt->pile_ins[pos_op1].operande = position_cst_0;
						pCxt->pile_ins[pos_op1 + 1].PcsOpCode = formate_log;
						}
					(*t_op1) = c_res_message ;
					break;
				case c_res_numerique :
					if (!pams->interpreteur)
						{
						decale_instruc(pams, 3,pos_op1);
						ajoute_num_0(&position_cst_0);
						pCxt->pile_ins[pos_op1].PcsOpCode = push_const_num;
						pCxt->pile_ins[pos_op1].operande = position_cst_0;
						ajoute_num_0(&position_cst_0);
						pCxt->pile_ins[pos_op1+1].PcsOpCode = push_const_num;
						pCxt->pile_ins[pos_op1+1].operande = position_cst_0;
						pCxt->pile_ins[pos_op1+2].PcsOpCode = formate_num;
						}
					(*t_op1) = c_res_message;
					break;
				default:break;
				}
			}
		}
	}

// -------------------------------
static void valide_nom_fichier (PPARAM_COMPIL pams)
	{
	if (pCxt->c_ins.GenreOpCode == op_variable)
		{
		PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, pCxt->c_ins.donnee_source);
		if (es->genre != c_res_message) ExcepErreurCompile(4);
		if (es->sens != c_res_variable_re)
			{
			if (es->sens != c_res_cste_re)
				{
				if ((es->sens == c_res_variable_ta_es) ||
					(es->sens == c_res_variable_ta_e) ||
					(es->sens == c_res_variable_ta_s))
					{
					ExcepErreurCompile(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
					}
				else
					{
					ajoute_instruc_operande (pams, push_var_mes,pCxt->c_ins.donnee_source);
					}
				}
			else
				{
				if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
				ajoute_instruc_operande (pams, push_const_mes_r,pCxt->c_ins.donnee_source);
				}
			}
		else
			{
			if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
			ajoute_instruc_operande (pams, push_var_mes_r,pCxt->c_ins.donnee_source);
			}
		}
	else
		{
		if (pCxt->c_ins.GenreOpCode == op_constante)
			{
			ajoute_instruc_operande (pams, push_const_mes,pCxt->c_ins.donnee_source);
			}
		else
			{
			ExcepErreurCompile(4);
			}
		}
	}

// -----------------------------
static ID_MOT_RESERVE primaire (PPARAM_COMPIL pams, BOOL un_indice)
	{
	ID_MOT_RESERVE type_resultant;
	DWORD position_cst_0;
	DWORD c_ins_tableau;
	FLOAT reel_temp;
	
	pCxt->val_trouvee=TRUE;
	switch (pCxt->c_ins.GenreOpCode)
		{
		case op_variable:
			{
			PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
			type_resultant = es->genre;
			switch (es->genre)
				{
				case c_res_logique:
					{
					if (es->sens == c_res_variable_re)
						{
						if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
						ajoute_instruc_operande(pams, push_var_log_r,pCxt->c_ins.donnee_source);
						}
					else
						{
						if (es->sens == c_res_cste_re)
							{
							if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
							ajoute_instruc_operande(pams, push_const_log_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if ((es->sens == c_res_variable_ta_es) ||
								(es->sens == c_res_variable_ta_e) ||
								(es->sens == c_res_variable_ta_s))
								{
								c_ins_tableau = pCxt->c_ins.donnee_source;
								valide_crochet(pams);
								ajoute_instruc_operande(pams, push_var_log_t,c_ins_tableau);
								}
							else
								ajoute_instruc_operande(pams, push_var_log,pCxt->c_ins.donnee_source);
							}
						}
					}
					break;
					
				case c_res_numerique:
					if (es->sens == c_res_variable_re)
						{
						if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
						ajoute_instruc_operande(pams, push_var_num_r,pCxt->c_ins.donnee_source);
						}
					else
						{
						if (es->sens == c_res_cste_re)
							{
							if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
							ajoute_instruc_operande(pams, push_const_num_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if ((es->sens == c_res_variable_ta_es) ||
								(es->sens == c_res_variable_ta_e) ||
								(es->sens == c_res_variable_ta_s))
								{
								c_ins_tableau = pCxt->c_ins.donnee_source;
								valide_crochet(pams);
								ajoute_instruc_operande(pams, push_var_num_t,c_ins_tableau);
								}
							else
								{
								if (un_indice)
									{
									ajoute_instruc_operande(pams, push_var_num_i,pCxt->c_ins.donnee_source);
									}
								else
									{
									ajoute_instruc_operande(pams, push_var_num,pCxt->c_ins.donnee_source);
									}
								}
							}
						}
					break;
					
				case c_res_message:
					if (es->sens == c_res_variable_re)
						{
						if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
						ajoute_instruc_operande(pams, push_var_mes_r,pCxt->c_ins.donnee_source);
						}
					else
						{
						if (es->sens == c_res_cste_re)
							{
							if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
							ajoute_instruc_operande(pams, push_const_mes_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if ((es->sens == c_res_variable_ta_es) ||
								(es->sens == c_res_variable_ta_e) ||
								(es->sens == c_res_variable_ta_s))
								{
								c_ins_tableau = pCxt->c_ins.donnee_source;
								valide_crochet(pams);
								ajoute_instruc_operande(pams, push_var_mes_t,c_ins_tableau);
								}
							else
								ajoute_instruc_operande(pams, push_var_mes,pCxt->c_ins.donnee_source);
							}
						}
					break;
					
				default: ExcepErreurCompile(95);
				} // switch (es->genre)
			}
			break;
			
		case op_constante:
			{
			consulte_cst_bd_c (pCxt->c_ins.donnee_source,pCxt->representation_constante);
			if (pCxt->representation_constante[0] == '"')
				{
				ajoute_instruc_operande(pams, push_const_mes,pCxt->c_ins.donnee_source);
				type_resultant= c_res_message;
				}
			else
				{
				if (!StrToFLOAT(&reel_temp,pCxt->representation_constante)) ExcepErreurCompile(96);
				ajoute_instruc_operande(pams, push_const_num,pCxt->c_ins.donnee_source);
				type_resultant = c_res_numerique;
				}
			}
			break;
			
		case op_reserve:
			{
			switch (pCxt->c_ins.donnee_source)
				{
				case c_res_parenthese_ouverte:
					suivant(pams);
					type_resultant=test(pams, un_indice);
					if ((pCxt->c_ins.GenreOpCode != op_reserve) ||
						(pCxt->c_ins.donnee_source != c_res_parenthese_fermee)) ExcepErreurCompile(97);
					break;
				case c_res_zero:
					ajoute_instruc(pams, push_const_0);
					type_resultant=c_res_logique;
					break;
				case c_res_un:
					ajoute_instruc(pams, push_const_1);
					type_resultant=c_res_logique;
					break;
				default:
					pCxt->val_trouvee=FALSE;
					//type_resultant n'est pas mis a jour sur val_trouve faux
					break;
				}
			}
			break;
		default: ExcepErreurCompile(98);
		} // switch (pCxt->c_ins.GenreOpCode)
		
	if (pCxt->val_trouvee)
		{
		suivant(pams);
		if (!pCxt->deux_points_trouve)
			{
			switch (type_resultant)
				{
				case c_res_logique :
					if (fonction_deux_points(pams))
						{
						if (primaire(pams, FALSE) != c_res_numerique) ExcepErreurCompile(99);////'numerique attendu'
						ajoute_instruc(pams, formate_log);
						type_resultant = c_res_message;
						pCxt->deux_points_trouve = FALSE ;
						}
					break;
				case c_res_numerique:
					if (fonction_deux_points(pams))
						{
						if (fonction_deux_points(pams))
							{
							ajoute_num_0 (&position_cst_0);
							ajoute_instruc_operande(pams, push_const_num,position_cst_0);
							if (primaire(pams, FALSE) != c_res_numerique) ExcepErreurCompile(99);// numerique attendu
							ajoute_instruc(pams, formate_num);
							}
						else
							{
							pCxt->deux_points_trouve = TRUE;
							if (primaire(pams, FALSE) != c_res_numerique) ExcepErreurCompile(99);// numerique attendu
							if (fonction_deux_points(pams))
								{
								if (primaire(pams, FALSE) != c_res_numerique) ExcepErreurCompile(99);// numerique attendu
								ajoute_instruc(pams, formate_num);
								}
							else
								{
								ajoute_instruc(pams, formate_num_sc);
								}
							}
						type_resultant = c_res_message;
						pCxt->deux_points_trouve = FALSE ;
						}
					break;
				case c_res_message :
					if (fonction_deux_points(pams))
						{
						if (primaire(pams, FALSE) != c_res_numerique) ExcepErreurCompile(99);// numerique attendu
						ajoute_instruc(pams, formate_mes) ;
						pCxt->deux_points_trouve = FALSE ;
						}
					break;
				default: ExcepErreurCompile(130);
				} 
			}
		}
	return type_resultant;
	} // primaire

// --------------------
static ID_MOT_RESERVE op_unaire (PPARAM_COMPIL pams, BOOL un_indice)
	{
	ID_MOT_RESERVE type_resultant;
	ID_MOT_RESERVE operateur_unaire;
	int  pos_depart_duplic;
	
	type_resultant=primaire(pams, un_indice);
	if (!pCxt->val_trouvee)
		{
		if (pCxt->c_ins.GenreOpCode == op_reserve)
			{
			operateur_unaire = (ID_MOT_RESERVE)pCxt->c_ins.donnee_source;
			pos_depart_duplic = pCxt->sp_ins + 1;
			suivant(pams);
			type_resultant = op_unaire(pams, un_indice);
			switch (operateur_unaire)
				{
				case c_res_plus : if (type_resultant!=c_res_numerique) ExcepErreurCompile(101);
					break;
				case c_res_non :
					if (type_resultant!=c_res_logique) ExcepErreurCompile(102);
					ajoute_instruc(pams, op_non);
					break;
				case c_res_moins:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(103);
					ajoute_instruc(pams, change_signe);
					break;
				case c_res_logarithme:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(104);
					ajoute_instruc(pams, op_logarithme);
					break;
				case c_res_exponentiel:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(105);
					ajoute_instruc(pams, op_exponentiel);
					break;
				case c_res_sinus:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(106);
					ajoute_instruc(pams, op_sinus);
					break;
				case c_res_cosinus:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(107);
					ajoute_instruc(pams, op_cosinus);
					break;
				case c_res_tangente:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(108);
					ajoute_instruc(pams, op_tangente);
					break;
				case c_res_absolue:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(109);
					ajoute_instruc(pams, op_absolue);
					break;
				case c_res_carre:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(110);
					ajoute_instruc(pams, op_carre);
					break;
				case c_res_racine:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(111);
					ajoute_instruc(pams, op_racine);
					break;
				case c_res_log_10:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(112);
					ajoute_instruc(pams, op_log_10);
					break;
				case c_res_partie_entiere:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(113);
					ajoute_instruc(pams, op_partie_entiere);
					break;
				case c_res_bcd:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(114);
					ajoute_instruc(pams, op_bcd);
					break;
				case c_res_valbcd:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(115);
					ajoute_instruc(pams, op_valbcd);
					break;
				case c_res_bin:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(IDSERR_L_OPERANDE_SUIVANT_UN_BIN_DOIT_ETRE_DE_TYPE_NUMERIQUE);
					ajoute_instruc(pams, op_bin);
					type_resultant = c_res_message;
					break;
				case c_res_valbin:
					if (type_resultant!=c_res_message) ExcepErreurCompile(IDSERR_L_OPERANDE_SUIVANT_UN_VALBIN_DOIT_DE_TYPE_MESSAGE);
					ajoute_instruc(pams, op_valbin);
					type_resultant = c_res_numerique;
					break;
				case c_res_val:
					if (type_resultant!=c_res_message) ExcepErreurCompile(7);
					ajoute_instruc(pams, op_val);
					type_resultant = c_res_numerique;
					break;
				case c_res_val_ascii:
					if (type_resultant!=c_res_message) ExcepErreurCompile(7);
					ajoute_instruc(pams, op_val_ascii);
					type_resultant = c_res_numerique;
					break;
				case c_res_car:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(IDSERR_L_OPERANDE_SUIVANT_UN_BIN_DOIT_ETRE_DE_TYPE_NUMERIQUE);
					ajoute_instruc(pams, op_car);
					type_resultant = c_res_message;
					break;
				case c_res_tronc:
					if (pCxt->pile_ins[pCxt->sp_ins].PcsOpCode != formate_mes) ExcepErreurCompile(118);
					pCxt->pile_ins[pCxt->sp_ins].PcsOpCode = op_tronc;
					break;
				case c_res_change:
					copie_instruc(pams, pos_depart_duplic,pCxt->sp_ins);
					switch (type_resultant)
						{
						case c_res_numerique : ajoute_instruc(pams, different_num);
							break;
						case c_res_logique : ajoute_instruc(pams, different_log);
							break;
						case c_res_message : ajoute_instruc(pams, different_mes);
							break;
						default: ExcepErreurCompile(130);
						}
					type_resultant = c_res_logique;
					break;
				case c_res_stable:
					copie_instruc(pams, pos_depart_duplic,pCxt->sp_ins);
					switch (type_resultant)
						{
						case c_res_numerique : ajoute_instruc(pams, egal_num);
							break;
						case c_res_logique : ajoute_instruc(pams, egal_log);
							break;
						case c_res_message : ajoute_instruc(pams, egal_mes);
							break;
						default: ExcepErreurCompile(130);
						}
					type_resultant = c_res_logique;
					break;
				case c_res_HEXA:
					if (type_resultant!=c_res_numerique) ExcepErreurCompile(IDSERR_L_OPERANDE_SUIVANT_UN_HEXA_DOIT_ETRE_DE_TYPE_NUMERIQUE);
					ajoute_instruc(pams, op_hexa);
					type_resultant = c_res_message;
					break;
				case c_res_VALHEXA:
					if (type_resultant!=c_res_message) ExcepErreurCompile(IDSERR_L_OPERANDE_SUIVANT_UN_VALHEXA_DOIT_ETRE_DE_TYPE_MESSAGE);
					ajoute_instruc(pams, op_valhexa);
					type_resultant = c_res_numerique;
					break;

				default: ExcepErreurCompile(119);
				} /* switch */
			}
		else ExcepErreurCompile(120);
		}
	return(type_resultant);
	} // op_unaire

// ------------------
static ID_MOT_RESERVE produit (PPARAM_COMPIL pams, BOOL un_indice)
	{
	ID_MOT_RESERVE type_op2;
	ID_MOT_RESERVE operateur_produit;
	ID_MOT_RESERVE type_resultant = op_unaire(pams, un_indice);
	
	while ((pCxt->c_ins.GenreOpCode==op_reserve) &&
		((pCxt->c_ins.donnee_source== c_res_divise)||
		(pCxt->c_ins.donnee_source== c_res_multiplie)||
		(pCxt->c_ins.donnee_source== c_res_et)))
		{
		operateur_produit = (ID_MOT_RESERVE)pCxt->c_ins.donnee_source;
		suivant(pams);
		type_op2= op_unaire(pams, un_indice);
		switch (operateur_produit)
			{
			case c_res_et:
				if (type_resultant == c_res_logique)
					{
					if (type_op2!=c_res_logique) ExcepErreurCompile(121);
					ajoute_instruc (pams, op_et);
					}
				else
					{
					if (type_resultant == c_res_numerique)
						{
						if (type_op2!=c_res_numerique) ExcepErreurCompile(121);
						ajoute_instruc (pams, op_et_num);
						}
					else
						ExcepErreurCompile(121);
					}
				break;
			case c_res_divise:
				if ((type_resultant!=c_res_numerique) || (type_op2!=c_res_numerique)) ExcepErreurCompile(122);
				ajoute_instruc (pams, op_divise);
				break;
			case c_res_multiplie:
				if ((type_resultant!=c_res_numerique) || (type_op2!=c_res_numerique)) ExcepErreurCompile(123);
				ajoute_instruc (pams, op_multiplie);
				break;
			default: ExcepErreurCompile(124);
			}
		}
	return type_resultant;
	}

// ----------------
static ID_MOT_RESERVE somme (PPARAM_COMPIL pams, BOOL un_indice)
	{
	ID_MOT_RESERVE type_op2;
	ID_MOT_RESERVE type_tempo;
	ID_MOT_RESERVE operateur_somme;
	int  position_op1;
	ID_MOT_RESERVE type_resultant = produit(pams, un_indice);
	while ((pCxt->c_ins.GenreOpCode==op_reserve) &&
		((pCxt->c_ins.donnee_source== c_res_plus)||
		(pCxt->c_ins.donnee_source== c_res_moins)||
		(pCxt->c_ins.donnee_source== c_res_ou)||
		(pCxt->c_ins.donnee_source== c_res_ou_x)||
		(pCxt->c_ins.donnee_source== c_res_concatene)))
		{
		operateur_somme = (ID_MOT_RESERVE)pCxt->c_ins.donnee_source;
		suivant(pams);
		position_op1 = pCxt->sp_ins + 1 ;
		type_op2=produit(pams, un_indice);
		switch (operateur_somme)
			{
			case c_res_ou:
				if (type_resultant == c_res_logique)
					{
					if (type_op2 != c_res_logique) ExcepErreurCompile(125);
					ajoute_instruc(pams, op_ou);
					}
				else
					{
					if (type_resultant == c_res_numerique)
						{
						if (type_op2 != c_res_numerique) ExcepErreurCompile(125);
						ajoute_instruc(pams, op_ou_num);
						}
					else
						ExcepErreurCompile(125);
					}
				break;
			case c_res_ou_x:
				if ((type_resultant!=c_res_logique) || (type_op2!=c_res_logique)) ExcepErreurCompile(126);
				ajoute_instruc(pams, op_ou_x);
				break;
			case c_res_plus:
				if ((type_resultant!=c_res_numerique) || (type_op2!=c_res_numerique)) ExcepErreurCompile(127);
				ajoute_instruc(pams, op_plus);
				break;
			case c_res_moins:
				if ((type_resultant!=c_res_numerique) || (type_op2!=c_res_numerique)) ExcepErreurCompile(128);
				ajoute_instruc(pams, op_moins);
				break;
			case c_res_concatene:
				if ((type_resultant!=c_res_message) &&
					(type_op2!=c_res_message))
					{
					type_tempo = c_res_message ;
					converti_message(pams, &type_resultant,&type_tempo,position_op1);
					}
				converti_message(pams, &type_resultant,&type_op2,position_op1);
				ajoute_instruc(pams, op_concatene);
				break;
			default: ExcepErreurCompile(124);
			}
		}
	return type_resultant;
	}

//   ---------------
ID_MOT_RESERVE test (PPARAM_COMPIL pams, BOOL un_indice)
	{
	ID_MOT_RESERVE operateur_test;
	ID_MOT_RESERVE type_op2;
	int  pos_instruc_a_formater;
	int  pos_depart_duplic = pCxt->sp_ins + 1;
	ID_MOT_RESERVE type_resultant = somme(pams, un_indice);

	while ((pCxt->c_ins.GenreOpCode==op_reserve) &&
		((pCxt->c_ins.donnee_source == c_res_egal)||
		(pCxt->c_ins.donnee_source == c_res_superieur)||
		(pCxt->c_ins.donnee_source == c_res_inferieur)||
		(pCxt->c_ins.donnee_source == c_res_different)||
		(pCxt->c_ins.donnee_source == c_res_superieur_egal)||
		(pCxt->c_ins.donnee_source == c_res_inferieur_egal)||
		(pCxt->c_ins.donnee_source == c_res_commence_par)||
		(pCxt->c_ins.donnee_source == c_res_fini_par)||
		(pCxt->c_ins.donnee_source == c_res_bittest)||
		(pCxt->c_ins.donnee_source == c_res_passe_valeur_vers_h)||
		(pCxt->c_ins.donnee_source == c_res_passe_valeur_vers_b)||
		(pCxt->c_ins.donnee_source == c_res_passe_valeur)||
		(pCxt->c_ins.donnee_source == c_res_prend_valeur)||
		(pCxt->c_ins.donnee_source == c_res_quitte_valeur)||
		(pCxt->c_ins.donnee_source == c_res_prend_valeur_vers_h)||
		(pCxt->c_ins.donnee_source == c_res_quitte_valeur_vers_h)||
		(pCxt->c_ins.donnee_source == c_res_prend_valeur_vers_b)||
		(pCxt->c_ins.donnee_source == c_res_quitte_valeur_vers_b)))
		{
		operateur_test = (ID_MOT_RESERVE)pCxt->c_ins.donnee_source;
		suivant(pams);
		pos_instruc_a_formater = pCxt->sp_ins + 1;
		type_op2 = somme(pams, un_indice);
		if ((type_resultant!=type_op2) &&	(type_resultant!=c_res_message) && (type_op2!=c_res_message)) ExcepErreurCompile(129);
		converti_message(pams, &type_resultant,&type_op2,pos_instruc_a_formater);
		switch (operateur_test)
			{
			case c_res_egal:
				switch (type_resultant)
					{
					case c_res_logique: ajoute_instruc(pams, egal_log);
						break;
					case c_res_numerique: ajoute_instruc(pams, egal_num);
						break;
					case c_res_message: ajoute_instruc(pams, egal_mes);
						break;
					default: ExcepErreurCompile(130);
					}
				break;
			case c_res_superieur_egal:
				switch (type_resultant)
					{
					case c_res_logique: ajoute_instruc(pams, superieur_egal_log);
						break;
					case c_res_numerique: ajoute_instruc(pams, superieur_egal_num);
						break;
					case c_res_message: ajoute_instruc(pams, superieur_egal_mes);
						break;
					default: ExcepErreurCompile(130);
					}
				break;
			case c_res_superieur:
				switch (type_resultant)
					{
					case c_res_logique: ajoute_instruc(pams, superieur_log);
						break;
					case c_res_numerique: ajoute_instruc(pams, superieur_num);
						break;
					case c_res_message: ajoute_instruc(pams, superieur_mes);
						break;
					default: ExcepErreurCompile(130);
					}
				break;
			case c_res_inferieur_egal :
				switch (type_resultant)
					{
					case c_res_logique: ajoute_instruc(pams, inferieur_egal_log);
						break;
					case c_res_numerique: ajoute_instruc(pams, inferieur_egal_num);
						break;
					case c_res_message: ajoute_instruc(pams, inferieur_egal_mes);
						break;
					default: ExcepErreurCompile(130);
					}
				break;
			case c_res_different :
				switch (type_resultant)
					{
					case c_res_logique: ajoute_instruc(pams, different_log);
						break;
					case c_res_numerique: ajoute_instruc(pams, different_num);
						break;
					case c_res_message: ajoute_instruc(pams, different_mes);
						break;
					default: ExcepErreurCompile(130);
					}
				break;
			case c_res_inferieur :
				switch (type_resultant)
					{
					case c_res_logique: ajoute_instruc(pams, inferieur_log);
						break;
					case c_res_numerique: ajoute_instruc(pams, inferieur_num);
						break;
					case c_res_message: ajoute_instruc(pams, inferieur_mes);
						break;
					default: ExcepErreurCompile(130);
					}
				break;
			case c_res_commence_par :
				if (type_resultant != c_res_message) ExcepErreurCompile(131);
				ajoute_instruc(pams, deb_par);
				break;
			case c_res_fini_par :
				if (type_resultant != c_res_message) ExcepErreurCompile(132);
				ajoute_instruc(pams, fin_par);
				break;
			case c_res_bittest :
				if (type_resultant != c_res_numerique) ExcepErreurCompile(129);
				ajoute_instruc(pams, bit_test);
				break;
			case c_res_passe_valeur :
				if (type_resultant != c_res_numerique) ExcepErreurCompile(143);
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				ajoute_instruc(pams, passe_val);
				break;
			case c_res_passe_valeur_vers_h :
				if (type_resultant != c_res_numerique) ExcepErreurCompile(144);
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				ajoute_instruc(pams, passe_val_h);
				break;
			case c_res_passe_valeur_vers_b :
				if (type_resultant != c_res_numerique) ExcepErreurCompile(145);
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				ajoute_instruc(pams, passe_val_b);
				break;
			case c_res_prend_valeur :
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				switch (type_resultant)
					{
					case c_res_logique : ajoute_instruc(pams, prend_val_log);
						break;
					case c_res_numerique : ajoute_instruc(pams, prend_val_num);
						break;
					case c_res_message : ajoute_instruc(pams, prend_val_mes);
						break;
					default: ExcepErreurCompile(130);
					}
				break;
			case c_res_quitte_valeur :
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				switch (type_resultant)
					{
					case c_res_logique : ajoute_instruc(pams, quitte_val_log);
						break;
					case c_res_numerique : ajoute_instruc(pams, quitte_val_num);
						break;
					case c_res_message : ajoute_instruc(pams, quitte_val_mes);
						break;
					default: ExcepErreurCompile(130);
					}
				break;
			case c_res_prend_valeur_vers_h :
				if (type_resultant !=  c_res_numerique) ExcepErreurCompile(146);
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				ajoute_instruc(pams, prend_val_h);
				break;
			case c_res_quitte_valeur_vers_h :
				if (type_resultant != c_res_numerique) ExcepErreurCompile(147);
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				ajoute_instruc(pams, quitte_val_h);
				break;
			case c_res_prend_valeur_vers_b :
				if (type_resultant != c_res_numerique) ExcepErreurCompile(148);
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				ajoute_instruc(pams, prend_val_b);
				break;
			case c_res_quitte_valeur_vers_b :
				if (type_resultant != c_res_numerique) ExcepErreurCompile(149);
				copie_instruc(pams, pos_depart_duplic,pos_instruc_a_formater-1);
				ajoute_instruc(pams, quitte_val_b);
				break;
			default: ExcepErreurCompile(124);
			} // switch (operateur_test)
		type_resultant=c_res_logique;
		}
	return type_resultant;
	} // test

//-------------------------------
void valide_crochet (PPARAM_COMPIL pams)
  {
  suivant(pams);
  if ((pCxt->c_ins.GenreOpCode == op_reserve) &&
     (pCxt->c_ins.donnee_source == c_res_crochet_ouvert))
    {
    suivant(pams);
		ID_MOT_RESERVE type_resultant = test(pams, TRUE);
    if (type_resultant != c_res_numerique) ExcepErreurCompile(273);
    if ((pCxt->c_ins.GenreOpCode != op_reserve) &&
       (pCxt->c_ins.donnee_source == c_res_crochet_ferme))
      {
      ExcepErreurCompile(274);
      }
    }
    else
    {
    ExcepErreurCompile(275);
    }
  }

//-------------------------------
static void valide_borne_execute (PPARAM_COMPIL pams, BOOL borne_depart,BOOL cest1execute, PP_CODE	tempo_memorise)
  {
  FLOAT reel_temp;
	
  if (pCxt->c_ins.GenreOpCode == op_variable)
    {
		PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
    if (es->genre != c_res_numerique) ExcepErreurCompile(212);
    if ((es->sens != c_res_e) && (es->sens != c_res_s) &&
			(es->sens != c_res_e_et_s) && (cest1execute)) ExcepErreurCompile(213);
    if (es->sens == c_res_variable_re)
      {
      if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
      tempo_memorise->PcsOpCode = push_var_num_r;
      tempo_memorise->operande = pCxt->c_ins.donnee_source;
      }
    else
      {
      if (es->sens == c_res_cste_re)
        {
        if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
        tempo_memorise->PcsOpCode = push_const_num_r;
        tempo_memorise->operande = pCxt->c_ins.donnee_source;
        }
      else
        {
        if ((es->sens == c_res_variable_ta_es) ||
					(es->sens == c_res_variable_ta_e) ||
					(es->sens == c_res_variable_ta_s))
          {
          ExcepErreurCompile(298);
          }
        else
          {
          tempo_memorise->PcsOpCode = push_var_num;
          tempo_memorise->operande = pCxt->c_ins.donnee_source;
          }
        }
      }
    if (borne_depart)
      ajoute_instruc_operande (pams, tempo_memorise->PcsOpCode,tempo_memorise->operande);
    }
  else
    {
    if (pCxt->c_ins.GenreOpCode == op_constante)
      {
      consulte_cst_bd_c (pCxt->c_ins.donnee_source,pCxt->representation_constante);
      if (!StrToFLOAT(&reel_temp,pCxt->representation_constante)) ExcepErreurCompile(156);
      if (borne_depart)
        {
        ajoute_instruc_operande (pams, push_const_num,pCxt->c_ins.donnee_source);
        }
      else
        {
        tempo_memorise->PcsOpCode = push_const_num;
        tempo_memorise->operande = pCxt->c_ins.donnee_source;
        }
      }
    else
      {
      ExcepErreurCompile(214);
      }
    }
  } // valide_borne_execute

// -----------------------------
BOOL fonction_deux_points (PPARAM_COMPIL pams)
  {
  BOOL bRet = FALSE;
  pCxt->deux_points_trouve = FALSE ;
  if ((pCxt->c_ins.GenreOpCode == op_reserve) &&
		(pCxt->c_ins.donnee_source == c_res_deux_points))
		{
		bRet =TRUE;
		pCxt->deux_points_trouve = TRUE ;
		suivant(pams);
		}
  return bRet;
  }


// --------------------------------------
// Compile une ligne de code Processyn et g�n�re ou pas le code objet
// selon les param�tres de PPARAM_COMPIL
// Provoque une exception EXCEPTION_ERR_COMPILE si erreur rencontr�e
void compile_ligne (PPARAM_COMPIL pams)
	{
	PENTREE_SORTIE es;
	PP_CODE	ads_x_instruc;

	// Substitution JS
	CONTEXTE_COMPILE ContexteCompile;
	pCxt = &ContexteCompile;
	/*
	pCxt = (PCONTEXTE_COMPILE) pMemAlloue(sizeof(*pCxt));
	if (setjmp(termine) != 0)
		{
		MemLibere((PVOID *)(&pCxt));
		return;
		}
	*/
	// Fin Substitution JS

	__try
		{
		pCxt->sp_ins= -1;//pour les tableaux en c
		pCxt->deux_points_trouve = FALSE ;
		suivant(pams);
		pCxt->c_ins1=pCxt->c_ins;
		switch (pCxt->c_ins1.GenreOpCode)
			{
			case op_variable: //affectation
				{
				es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins1.donnee_source);
				pCxt->type_op1= es->genre;
				pCxt->sens_op1= es->sens;
				if ((es->sens == c_res_variable_ta_es) || (es->sens == c_res_variable_ta_e) || (es->sens == c_res_variable_ta_s))
					valide_crochet (pams);
				suivant(pams);
				if ((pCxt->c_ins.GenreOpCode != op_reserve) || (pCxt->c_ins.donnee_source != c_res_recoit))
					ExcepErreurCompile(133);
				
				suivant(pams);
				pCxt->type_op2 = test(pams, FALSE);
				if ((pCxt->type_op1 != pCxt->type_op2) && (pCxt->type_op1 != c_res_message))
					{
					ExcepErreurCompile(134);
					}
				else
					{
					switch (pCxt->type_op1)
						{
						case c_res_logique:
							if (pCxt->sens_op1 == c_res_variable_re)
								{
								if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
								ajoute_instruc_operande(pams, pop_var_log_r,pCxt->c_ins1.donnee_source);
								}
							else
								{
								if (pCxt->sens_op1 == c_res_cste_re)
									{
									ExcepErreurCompile(203);
									}
								else
									{
									if ((pCxt->sens_op1 == c_res_variable_ta_es) ||
										(pCxt->sens_op1 == c_res_variable_ta_e) ||
										(pCxt->sens_op1 == c_res_variable_ta_s))
										{
										ajoute_instruc_operande(pams, pop_var_log_t,pCxt->c_ins1.donnee_source);
										}
									else
										ajoute_instruc_operande(pams, pop_var_log,pCxt->c_ins1.donnee_source);
									}
								}
							break;
						case c_res_numerique:
							if (pCxt->sens_op1 == c_res_variable_re)
								{
								if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
								ajoute_instruc_operande(pams, pop_var_num_r,pCxt->c_ins1.donnee_source);
								}
							else
								{
								if (pCxt->sens_op1 == c_res_cste_re)
									{
									ExcepErreurCompile(203);
									}
								else
									{
									if ((pCxt->sens_op1 == c_res_variable_ta_es) ||
										(pCxt->sens_op1 == c_res_variable_ta_e) ||
										(pCxt->sens_op1 == c_res_variable_ta_s))
										{
										ajoute_instruc_operande(pams, pop_var_num_t,pCxt->c_ins1.donnee_source);
										}
									else
										ajoute_instruc_operande(pams, pop_var_num,pCxt->c_ins1.donnee_source);
									}
								}
							break;
						case c_res_message:
							converti_message(pams, &pCxt->type_op1,&pCxt->type_op2,0);
							if (pCxt->sens_op1 == c_res_variable_re)
								{
								if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
								ajoute_instruc_operande(pams, pop_var_mes_r,pCxt->c_ins1.donnee_source);
								}
							else
								{
								if (pCxt->sens_op1 == c_res_cste_re)
									{
									ExcepErreurCompile(203);
									}
								else
									{
									if ((pCxt->sens_op1 == c_res_variable_ta_es) ||
										(pCxt->sens_op1 == c_res_variable_ta_e) ||
										(pCxt->sens_op1 == c_res_variable_ta_s))
										{
										ajoute_instruc_operande(pams, pop_var_mes_t,pCxt->c_ins1.donnee_source);
										}
									else
										ajoute_instruc_operande(pams, pop_var_mes,pCxt->c_ins1.donnee_source);
									}
								}
							break;
						default: ExcepErreurCompile(130);
						} // switch (pCxt->type_op1)
					} // else
				if (pCxt->c_ins.GenreOpCode!=uls_terminees)
					ExcepErreurCompile(135);
				} // case op_variable
				break;
					
			case op_reserve:
				{
				switch (pCxt->c_ins1.donnee_source)
					{
					case c_res_si:
						suivant(pams);
						if (test(pams, FALSE) !=c_res_logique) ExcepErreurCompile(136);
						if ((pCxt->c_ins.GenreOpCode!=op_reserve) ||
							(pCxt->c_ins.donnee_source!=c_res_alors)) ExcepErreurCompile(137);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode!=uls_terminees) ExcepErreurCompile(135);
						if (!pams->interpreteur) ajoute_si(pams);
						break;
					case c_res_sinon:
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode!=uls_terminees) ExcepErreurCompile(135);
						if (!pams->interpreteur) ajoute_sinon(pams);
						break;
					case c_res_fin_cond:
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						if (!pams->interpreteur) ajoute_fin_cond(pams);
						break;
					case c_res_principal:
						suivant(pams);
						if (!pams->interpreteur) ajoute_principal(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						break;
					case c_res_aller_sp:
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_constante) ExcepErreurCompile(155);
						consulte_cst_bd_c (pCxt->c_ins.donnee_source,pCxt->representation_constante);
						suivant(pams);
						if (!pams->interpreteur) ajoute_goto(pams);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						break;
					case c_res_retour:
						suivant(pams);
						if (!pams->interpreteur)
							ajoute_retour(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						break;
					case c_res_libelle_procedure:
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_constante) ExcepErreurCompile(155);
						consulte_cst_bd_c (pCxt->c_ins.donnee_source,pCxt->representation_constante);
						suivant(pams);
						if (!pams->interpreteur) ajoute_libelle(pams);
						// $$ JS
						else
							{
							if (StrLength (pCxt->representation_constante) >= c_nb_car_es) ExcepErreurCompile(IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
							}
						// $$ fin JS
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						break;
					case c_res_execute:
						if (pams->pos_deb_execute != 0) ExcepErreurCompile(204);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_constante) ExcepErreurCompile(155);
						consulte_cst_bd_c (pCxt->c_ins.donnee_source,pCxt->representation_constante);
						pCxt->pos_nom_execute = position_nom_execute (pCxt->representation_constante);
						if (pCxt->pos_nom_execute == 0) ExcepErreurCompile(155);
						suivant(pams);
						valide_borne_execute (pams, TRUE,TRUE,&pCxt->tempo_memorise);
						ajoute_instruc_operande (pams, push_val_operande,pCxt->pos_nom_execute);
						suivant(pams);
						valide_borne_execute (pams, FALSE,TRUE,&pams->instruc_memorisee); // memorise l'instruction
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						ajoute_instruc_operande (pams, deb_execute,pCxt->pos_nom_execute);
						if (!pams->interpreteur)
							{
							pams->pos_deb_execute = pams->instruc_courante;
							pams->sp_cond_deb_execute = pams->sp_cond;
							}
						break;
					case c_res_fin_exec:
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						if (pams->pos_deb_execute == 0) ExcepErreurCompile (205);
						ajoute_instruc_operande (pams, pams->instruc_memorisee.PcsOpCode,pams->instruc_memorisee.operande);
						ajoute_instruc_operande (pams, fin_execute,pams->pos_deb_execute);
						if (!pams->interpreteur)
							{
							pams->pos_deb_execute = 0;
							if (pams->sp_cond > pams->sp_cond_deb_execute) ExcepErreurCompile(211);
							pams->sp_cond_deb_execute = 0;
							}
						break;
					case c_res_execute_boucle:
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable) ExcepErreurCompile(276);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						if (es->genre != c_res_numerique) ExcepErreurCompile(276);
						if (es->sens == c_res_variable_re)
							{
							if (pams->pos_deb_execute == 0) ExcepErreurCompile(215);
							pCxt->indice_boucle.PcsOpCode = push_var_num_r;
							pCxt->PcsOpCode = pop_var_num_r;
							}
						else
							{
							if (es->sens == c_res_cste_re)
								{
								ExcepErreurCompile(276);
								}
							else
								{
								if ((es->sens == c_res_variable_ta_es) ||
									(es->sens == c_res_variable_ta_e) ||
									(es->sens == c_res_variable_ta_s))
									{
									pCxt->indice_boucle.PcsOpCode = push_var_num_t;
									pCxt->PcsOpCode = pop_var_num_t;
									}
								else
									{
									pCxt->indice_boucle.PcsOpCode = push_var_num;
									pCxt->PcsOpCode = pop_var_num;
									}
								}
							}
						pCxt->indice_boucle.operande = pCxt->c_ins.donnee_source;
						suivant(pams);
						valide_borne_execute (pams, TRUE,FALSE,&pCxt->tempo_memorise);
						suivant(pams);
						valide_borne_execute (pams, FALSE,FALSE,&pCxt->tempo_memorise); // memorise l'instruction
						if (pCxt->tempo_memorise.PcsOpCode == push_const_num)
							{
							ajoute_instruc_operande (pams, push_const_num_t,pCxt->tempo_memorise.operande);
							}
						else
							{
							ajoute_instruc_operande (pams, pCxt->tempo_memorise.PcsOpCode,pCxt->tempo_memorise.operande);
							}
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						if (!pams->interpreteur)
							{
							if (pams->sp_boucle >= nb_boucle_maxi) ExcepErreurCompile(277);
							pams->sp_boucle = pams->sp_boucle + 1;
							pams->pile_boucle[pams->sp_boucle].memorisation = pCxt->tempo_memorise;
							pams->pile_boucle[pams->sp_boucle].indice = pCxt->indice_boucle;
							ajoute_instruc (pams, saut_boucle);
							if (pams->sp_cond >= nb_si_maxi) ExcepErreurCompile(91);
							pams->sp_cond = pams->sp_cond + 1;
							pams->pile_cond[pams->sp_cond].position = pams->instruc_courante;
							pams->pile_cond[pams->sp_cond].boucle = TRUE;
							ajoute_instruc_operande (pams, pCxt->PcsOpCode,pCxt->indice_boucle.operande);
							}
						break;
					case c_res_fin_exec_boucle:
						{
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						if (!pams->interpreteur)
							{
							if (pams->sp_boucle <= 0) ExcepErreurCompile(278); // FIN_BOUCLE en trop
							ajoute_instruc_operande (pams, pams->pile_boucle[pams->sp_boucle].indice.PcsOpCode,
								pams->pile_boucle[pams->sp_boucle].indice.operande);
							ajoute_instruc_operande (pams, pams->pile_boucle[pams->sp_boucle].memorisation.PcsOpCode,
								pams->pile_boucle[pams->sp_boucle].memorisation.operande);
							pams->sp_boucle = pams->sp_boucle - 1;
							if (pams->sp_cond <= 0) ExcepErreurCompile(278); // FIN_BOUCLE en trop
							if (!pams->pile_cond[pams->sp_cond].boucle) ExcepErreurCompile(272); // fin_si et fin_boucle mal imbrique
							ajoute_instruc_operande (pams, fin_boucle,pams->pile_cond[pams->sp_cond].position+1);
							ads_x_instruc = (PP_CODE)pointe_enr (szVERIFSource, __LINE__, bx_code,pams->pile_cond[pams->sp_cond].position);
							ads_x_instruc->operande = pams->instruc_courante + 1;
							pams->sp_cond = pams->sp_cond - 1;
							if (pams->sp_cond_deb_execute != 0)
								if (pams->sp_cond < pams->sp_cond_deb_execute) ExcepErreurCompile(279);
							}
						}
						break;
						
					case c_res_trans_tableau:
						{
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable) ExcepErreurCompile(141);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						pCxt->type_op1 = es->genre;
						if ((es->sens != c_res_variable_ta_es) &&
							(es->sens != c_res_variable_ta_e) &&
							(es->sens != c_res_variable_ta_s)) ExcepErreurCompile(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
						pCxt->c_ins1 = pCxt->c_ins;
						valide_crochet(pams);
						ajoute_instruc_operande(pams, push_tableau,pCxt->c_ins1.donnee_source);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable) ExcepErreurCompile(141);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						if ((es->sens != c_res_variable_ta_es) &&
							(es->sens != c_res_variable_ta_e) &&
							(es->sens != c_res_variable_ta_s)) ExcepErreurCompile(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
						
						if (pCxt->type_op1 != es->genre) ExcepErreurCompile(319);
						pCxt->c_ins1 = pCxt->c_ins;
						valide_crochet(pams);
						ajoute_instruc_operande(pams, push_tableau,pCxt->c_ins1.donnee_source);
						suivant(pams);
						valide_borne_execute (pams, TRUE,FALSE,&pCxt->tempo_memorise);
						suivant(pams);
						switch (pCxt->type_op1)
							{
							case c_res_logique :
								ajoute_instruc(pams, trans_tab_log);
								break;
							case c_res_numerique :
								ajoute_instruc(pams, trans_tab_num);
								break;
							case c_res_message :
								ajoute_instruc(pams, trans_tab_mes);
								break;
							default: ExcepErreurCompile(130);
							}
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						}
						break;
						
					case c_res_ecrire_ecran:
						{
						suivant(pams);
						pCxt->type_op2 = test(pams, FALSE) ;
						if (pCxt->type_op2 != c_res_message)
							{
							pCxt->type_op1 = c_res_message;
							converti_message(pams, &(pCxt->type_op1),&(pCxt->type_op2),0);
							}
						ajoute_instruc(pams, ecr_ecran);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						}
						break;
						
					case c_res_ecrire_imprimante:
					case c_res_ecrire_imp_memeligne:
					case c_res_ECRIRE_IMP_BRUT:
						{
						suivant(pams);
						pCxt->type_op2 = test(pams, FALSE) ;
						if (pCxt->type_op2 != c_res_message)
							{
							pCxt->type_op1 = c_res_message;
							converti_message(pams, &(pCxt->type_op1),&(pCxt->type_op2),0);
							}
						switch (pCxt->c_ins1.donnee_source)
							{
							case c_res_ecrire_imprimante :
								ajoute_instruc(pams, ecr_imp);
								break;
							case c_res_ecrire_imp_memeligne:
								ajoute_instruc(pams, ecr_imp_mlg);
								break;
							case c_res_ECRIRE_IMP_BRUT:
								ajoute_instruc(pams, ecr_imp_brut);
								break;
							}
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(135);
						}
						break;
						
					case c_res_ouvre_disque:
						{
						suivant(pams);
						valide_nom_fichier(pams);
						ajoute_instruc(pams, ouvre_disq);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(141);
						}
						break;
						
					case c_res_ecrire_disque:
						{
						suivant(pams);
						valide_nom_fichier(pams);
						ajoute_instruc(pams, ecr_disq);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(141);
						}
						break;
						
					case c_res_lire_disque:
						{
						suivant(pams);
						valide_nom_fichier(pams);
						ajoute_instruc(pams, lec_disq);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(141);
						}
						break;
						
					case c_res_ferme_disque:
						{
						suivant(pams);
						valide_nom_fichier(pams);
						ajoute_instruc(pams, ferme_disq);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(141);
						}
						break;
						
					case c_res_efface_disque:
						{
						suivant(pams);
						valide_nom_fichier(pams);
						ajoute_instruc(pams, efface_disq);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(141);
						}
						break;
						
					case c_res_copie_disque:
						{
						suivant(pams);
						valide_nom_fichier(pams);
						suivant(pams);
						valide_nom_fichier(pams);
						ajoute_instruc(pams, copie_disq);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(141);
						}
						break;
						
					case c_res_renomme_disque:
						{
						suivant(pams);
						valide_nom_fichier(pams);
						suivant(pams);
						valide_nom_fichier(pams);
						ajoute_instruc(pams, renomme_disq);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees) ExcepErreurCompile(141);
						}
						break;
						
					case c_res_lance_c:
						{
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable ) ExcepErreurCompile (151);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						pCxt->type_op1= es->genre;
						if (pCxt->type_op1 != c_res_numerique) ExcepErreurCompile (151);
						if ((es->sens == c_res_variable_re))
							{
							ajoute_instruc_operande (pams, push_index_chrono_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if ((es->sens == c_res_e))
								{
								ajoute_instruc_operande (pams, push_val_operande,pCxt->c_ins.donnee_source);
								}
							else
								{
								ExcepErreurCompile(151);
								}
							}
						ajoute_instruc (pams, depart_chrono);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees ) ExcepErreurCompile (141);
						}
						break;
						
					case c_res_arrete_c:
						{
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable ) ExcepErreurCompile (151);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						pCxt->type_op1= es->genre;
						if (pCxt->type_op1 != c_res_numerique) ExcepErreurCompile (151);
						if (es->sens == c_res_variable_re)
							{
							ajoute_instruc_operande (pams, push_index_chrono_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if ((es->sens == c_res_e))
								{
								ajoute_instruc_operande (pams, push_val_operande,pCxt->c_ins.donnee_source);
								}
							else
								ExcepErreurCompile(151);
							}
						ajoute_instruc (pams, arret_chrono);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees ) ExcepErreurCompile (141);
						}
						break;
						
					case c_res_raz_c:
						{
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable ) ExcepErreurCompile (151);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						pCxt->type_op1= es->genre;
						if (pCxt->type_op1 != c_res_numerique) ExcepErreurCompile (151);
						if (es->sens == c_res_variable_re)
							{
							ajoute_instruc_operande (pams, push_index_chrono_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if ((es->sens == c_res_e))
								{
								ajoute_instruc_operande (pams, push_val_operande,pCxt->c_ins.donnee_source);
								}
							else
								ExcepErreurCompile(151);
							}
						ajoute_instruc (pams, raz_chrono);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees ) ExcepErreurCompile (141);
						}
						break;
						
					case c_res_relance_c:
						{
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable ) ExcepErreurCompile (151);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						pCxt->type_op1= es->genre;
						if (pCxt->type_op1 != c_res_numerique) ExcepErreurCompile (151);
						if ((es->sens == c_res_variable_re))
							{
							ajoute_instruc_operande (pams, push_index_chrono_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if (es->sens == c_res_e)
								{
								ajoute_instruc_operande (pams, push_val_operande,pCxt->c_ins.donnee_source);
								}
							else
								ExcepErreurCompile(151);
							}
						ajoute_instruc (pams, continu_chrono);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees ) ExcepErreurCompile (141);
						}
						break;
						
					case c_res_lance_m:
						{
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable ) ExcepErreurCompile (152);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						pCxt->type_op1= es->genre;
						if (pCxt->type_op1 != c_res_logique) ExcepErreurCompile (152);
						if ((es->sens == c_res_variable_re))
							{
							ajoute_instruc_operande (pams, push_index_metro_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if (es->sens == c_res_e)
								{
								ajoute_instruc_operande (pams, push_val_operande,pCxt->c_ins.donnee_source);
								}
							else
								ExcepErreurCompile(152);
							}
						ajoute_instruc (pams, depart_metro);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees ) ExcepErreurCompile (141);
						}
						break;
						
					case c_res_arrete_m:
						{
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != op_variable ) ExcepErreurCompile (152);
						es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pCxt->c_ins.donnee_source);
						pCxt->type_op1= es->genre;
						if (pCxt->type_op1 != c_res_logique) ExcepErreurCompile (152);
						if ((es->sens == c_res_variable_re))
							{
							ajoute_instruc_operande (pams, push_index_metro_r,pCxt->c_ins.donnee_source);
							}
						else
							{
							if (es->sens == c_res_e)
								{
								ajoute_instruc_operande (pams, push_val_operande,pCxt->c_ins.donnee_source);
								}
							else
								ExcepErreurCompile(152);
							}
						ajoute_instruc (pams, arret_metro);
						suivant(pams);
						if (pCxt->c_ins.GenreOpCode != uls_terminees ) ExcepErreurCompile (141);
						}
						break;
						
					case c_res_execut_fonction:
						{
						suivant(pams);
						pCxt->type_op2 = test(pams, FALSE) ;
						if (pCxt->type_op2 != c_res_numerique) ExcepErreurCompile(90);
						ajoute_instruc (pams, lance_fonction);
						if (pCxt->c_ins.GenreOpCode != uls_terminees ) ExcepErreurCompile (141);
						}
						break;
					default: ExcepErreurCompile(138);
					} // switch (pCxt->c_ins1.donnee_source)
				}
				break; // case op_reserve:
					
			case op_constante: //commentaire
				{
				consulte_cst_bd_c (pCxt->c_ins.donnee_source,pCxt->representation_constante);
				if (pCxt->representation_constante[0] != '"')
					ExcepErreurCompile(139);
				suivant(pams);
				if (pCxt->c_ins.GenreOpCode!=uls_terminees)
					ExcepErreurCompile(135);
				if (!pams->interpreteur)
					supprime_cst_bd_c (pCxt->c_ins.donnee_source);
				}
				break;
					
			default: ExcepErreurCompile(140);
			} // switch (pCxt->c_ins1.GenreOpCode)
		if (!pams->interpreteur)
			stocke_ins(pams);
		
		// Suppression JS $$
		//MemLibere((PVOID *)(&pCxt));
		//
		}
		CATCH_ERREUR (pams->erreur);
  } // compile_ligne


//----------------------------------------------
static void maj_enlevement (DWORD nb_instruction,DWORD *pointeur_bloc_geo,DWORD *pointeur_bloc_code)
	{
	(*pointeur_bloc_geo)++;
	if ((*pointeur_bloc_geo) > 99)
		{
		(*pointeur_bloc_geo) = 0;
		enleve_enr (szVERIFSource, __LINE__, 100,b_geo_code,1); // vide 100 lignes traitees
		}
	(*pointeur_bloc_code) = (*pointeur_bloc_code) + nb_instruction;
	if ((*pointeur_bloc_code) > 99)
		{
		enleve_enr (szVERIFSource, __LINE__, (*pointeur_bloc_code),b_code,1); // vide les instructions traitees
		(*pointeur_bloc_code) = 0;
		}
	}

// -------------------------------------------------------------------------
DWORD genere_co(DWORD *ligne)
  {
  PGEO_CODE ads_geo;
  DWORD pointeur_bloc_code;
  DWORD pointeur_bloc_geo;
  DWORD nb_lignes;
  DWORD depart_ligne_courante;
  DWORD depart_ligne_suivante;
  DWORD nb_instructions_ligne;
	PARAM_COMPIL	ParamCompil;
  PPARAM_COMPIL	pams = &ParamCompil;
  DWORD erreur;

  //----------------------------------------------
  if (!existe_repere (bx_num))
		cree_bloc(bx_num,sizeof(FLOAT),0);
  if (!existe_repere (bx_mes))
		cree_bloc(bx_mes,c_taille_str,0);

  pams->erreur = 0;
  pams->sp_cond = 0;
  pams->sp_libelle = 0;
  pams->sp_boucle = 0;
  pams->sp_cond_deb_execute = 0;
  pams->pos_deb_execute = 0;
  pams->mode_debugger = TRUE;
  pams->presence_libelle = FALSE;
  pams->presence_main = FALSE;
  StrCopy (pams->nom_structure_cour,"");

  pointeur_bloc_code = 0;
  pointeur_bloc_geo = 0;

  if (existe_repere(b_geo_code))
    {
    enleve_enr (szVERIFSource, __LINE__, 1,b_geo_code,1);
    nb_lignes=nb_enregistrements (szVERIFSource, __LINE__, b_geo_code);
    if (nb_lignes > 0)
			cree_bloc (bx_code,sizeof (P_CODE),0);
    if (pams->mode_debugger)
      if (nb_lignes > 0)
				cree_bloc (bx_debug_code, sizeof (DWORD),0);
    depart_ligne_courante=1;
    depart_ligne_suivante=1;
    pams->instruc_courante=0;
    pams->ligne_courante=1;
    pams->interpreteur=FALSE;
    while ((pams->ligne_courante <= nb_lignes) && (pams->erreur == 0))
      {
      depart_ligne_courante=depart_ligne_suivante;
      ads_geo = (PGEO_CODE)pointe_enr (szVERIFSource, __LINE__, b_geo_code,pointeur_bloc_geo + 1);
      depart_ligne_suivante = ads_geo->dwNPremierCode; //$$ JS (anciennement *(PDWORD))
      nb_instructions_ligne = depart_ligne_suivante - depart_ligne_courante;
      pams->fin_instructions_ligne = nb_instructions_ligne + pointeur_bloc_code; //$$$$
      pams->instruc_source_courante = pointeur_bloc_code +1;
      pams->depart_instruc_courante = pams->instruc_courante + 1;

      compile_ligne(pams);

      //-----
      //compilation des Uls (1) a (nb_instructions_ligne-1)  de b_code;
      //--
      if (pams->erreur == 0)
        {
        maj_enlevement (nb_instructions_ligne,&pointeur_bloc_geo,&pointeur_bloc_code);
        }
      pams->ligne_courante = pams->ligne_courante + 1 ;
      } // while

    if ((pams->presence_libelle) && (!pams->presence_main) && (pams->erreur == 0)) pams->erreur = 434;
    if (pams->erreur == 0)
      if (pams->sp_cond > 0)
        pams->erreur = 142;
    if (pams->erreur == 0)
      if (pams->sp_boucle > 0)
        pams->erreur = 280;
    if (pams->erreur == 0)
      if (pams->pos_deb_execute != 0)
        pams->erreur = 206;
    if (pams->erreur !=0)
      {
      (*ligne) = pams->ligne_courante - 1;
      }
    else
      {
      enleve_bloc (b_geo_code);
      enleve_bloc (b_code);
      enleve_bloc (b_titre_paragraf_re);
      enleve_bloc (b_spec_titre_paragraf_re);
      enleve_bloc (b_liste);
      enleve_bloc (b_temp_genc_genx);
      enleve_bloc (b_temp_nom_structure);
      enleve_bloc (b_libelle);
      } // else
    } // b_geo_code inexistant
  erreur = pams->erreur;
  return erreur;
  } // genere_co

//----------------------------- fin gener_co.cpp --------------------------------------

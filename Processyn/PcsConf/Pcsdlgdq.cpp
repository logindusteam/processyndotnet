/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlgdq.c                                               |
 |   Auteur  :	LM							|
 |   Date    :	21/01/93						|
 |   Remarques : gestion des boites de dialogue dans le descripteur     |
 |               disque                                                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "appli.h"
#include "std.h"
#include "MemMan.h"
#include "UStr.h"
#include "CheckMan.h"
#include "lng_res.h"
#include "IdConfLng.h"
#include "ActionCf.h"
#include "Tipe.h"
#include "Descripteur.h"
#include "pcsdlg.h"
#include "UEnv.h"
#include "Verif.h"
#include "IdLngLng.h"
#include "pcsdlgdq.h"

VerifInit;

typedef struct
	{
	t_dlg_desc echange_data;
	DWORD index_type_ligne;
	DWORD index_type_var;
	char nom_fic[255];
	char nom_var[80];
	char taille_tab[80];
	char val_init[80];
	} t_param_disque;

static char mot_res1[40];
static char mot_res2[40];
static char nom_fic[255];
static char nom_var[30];
static char val_init[100];
static char taille_tab[30];
static DWORD texte_normal = ES_CENTER | WS_GROUP | WS_VISIBLE | WS_CHILD;
static DWORD texte_grise  = ES_CENTER | WS_GROUP | WS_VISIBLE | WS_CHILD; // // $$ | DT_HALFTONE; WS_DISABLED?
static BOOL tab_bool[15] = 
	{
	FALSE, FALSE, FALSE, FALSE, FALSE,
	FALSE, FALSE, FALSE, FALSE, FALSE,
	FALSE, FALSE, FALSE, FALSE, FALSE
	};


// -----------------------------------------------------------------------
// Nom......:
// Objet....: sert a cacher les objets
// Entr�es..: HWND hwnd
// Sorties..:
// Retour...:
// Remarques:
// -----------------------------------------------------------------------
void init_montre_ou_cache (HWND hwnd, BOOL bool1, BOOL bool2, BOOL bool3, BOOL bool4,
     BOOL bool5, BOOL bool6, BOOL bool7, BOOL bool8, BOOL bool9, BOOL bool10,
     BOOL bool11, BOOL bool12, BOOL bool13, BOOL bool14, BOOL bool15)
  {
  if (bool1)
    SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE1), GWL_STYLE, texte_normal);
  else
    SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE1), GWL_STYLE, texte_grise);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TEXTE1), NULL, TRUE);

  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_NOM_FIC), bool2);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_NOM_FIC), NULL, TRUE);

  if (bool3)
    SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE2), GWL_STYLE, texte_normal);
  else
    SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE2), GWL_STYLE, texte_grise);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TEXTE2), NULL, TRUE);

  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_NOM_VAR), bool4);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_NOM_VAR), NULL, TRUE);

  if (bool5)
    SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE3), GWL_STYLE, texte_normal);
  else
    SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE3), GWL_STYLE, texte_grise);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TEXTE3), NULL, TRUE);

  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_VAL_INIT), bool6);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_VAL_INIT), NULL, TRUE);

  if (bool7)
    SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE4), GWL_STYLE, texte_normal);
  else
    SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE4), GWL_STYLE, texte_grise);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TEXTE4), NULL, TRUE);

  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_TAILLE_TAB), bool8);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TAILLE_TAB), NULL, TRUE);

  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_TYPE_VAR), bool9);
  ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TYPE_VAR), NULL, TRUE);

  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_LOG_SIMPLE), bool10);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_NUM_SIMPLE), bool11);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_MESS_SIMPLE), bool12);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_LOG_TAB), bool13);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_NUM_TAB), bool14);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_MESS_TAB), bool15);

  tab_bool[0]  = bool1;
  tab_bool[1]  = bool2;
  tab_bool[2]  = bool3;
  tab_bool[3]  = bool4;
  tab_bool[4]  = bool5;
  tab_bool[5]  = bool6;
  tab_bool[6]  = bool7;
  tab_bool[7]  = bool8;
  tab_bool[8]  = bool9;
  tab_bool[9]  = bool10;
  tab_bool[10] = bool11;
  tab_bool[11] = bool12;
  tab_bool[12] = bool13;
  tab_bool[13] = bool14;
  tab_bool[14] = bool15;
  }

// -----------------------------------------------------------------------
// sert a cacher les objets
// Entr�es..: HWND hwnd
// -----------------------------------------------------------------------
void montre_ou_cache (HWND hwnd, BOOL bool1, BOOL bool2, BOOL bool3, BOOL bool4,
     BOOL bool5, BOOL bool6, BOOL bool7, BOOL bool8, BOOL bool9, BOOL bool10,
     BOOL bool11, BOOL bool12, BOOL bool13, BOOL bool14, BOOL bool15)
  {
  if (bool1 != tab_bool[0])
    {
    if (bool1)
      SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE1), GWL_STYLE, texte_normal);
    else
      SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE1), GWL_STYLE, texte_grise);
    ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TEXTE1), NULL, TRUE);
    }

  if (bool2 != tab_bool[1])
    {
    ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_NOM_FIC), bool2);
    if (bool2)
      { // on vient de cliquer sur fichier
      SetDlgItemText(hwnd, ID_DQ1_NOM_FIC, nom_fic);
      }
    else
      {
      GetDlgItemText (hwnd, ID_DQ1_NOM_FIC, nom_fic, 255);
      SetDlgItemText(hwnd, ID_DQ1_NOM_FIC, "");
      }
    }

  if (bool3 != tab_bool[2])
    {
    if (bool3)
      SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE2), GWL_STYLE, texte_normal);
    else
      SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE2), GWL_STYLE, texte_grise);
    ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TEXTE2), NULL, TRUE);
    }

  if (bool4 != tab_bool[3])
    {
    ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_NOM_VAR), bool4);
    if (bool4)
      {
      SetDlgItemText(hwnd, ID_DQ1_NOM_VAR, nom_var);
      }
    else
      {
      GetDlgItemText (hwnd, ID_DQ1_NOM_VAR, nom_var, 30);
      SetDlgItemText(hwnd, ID_DQ1_NOM_VAR, "");
      }
    }

  if (bool5 != tab_bool[4])
    {
    if (bool5)
      SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE3), GWL_STYLE, texte_normal);
    else
      SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE3), GWL_STYLE, texte_grise);
    ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TEXTE3), NULL, TRUE);
    }
  if (bool6 != tab_bool[5])
    {
    ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_VAL_INIT), bool6);
    if (bool6)
      {
      SetDlgItemText(hwnd, ID_DQ1_VAL_INIT, val_init);
      }
    else
      {
      GetDlgItemText (hwnd, ID_DQ1_VAL_INIT, val_init, 100);
      SetDlgItemText(hwnd, ID_DQ1_VAL_INIT, "");
      }
    }

  if (bool7 != tab_bool[6])
    {
    if (bool7)
      SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE4), GWL_STYLE, texte_normal);
    else
      SetWindowLong(::GetDlgItem(hwnd, ID_DQ1_TEXTE4), GWL_STYLE, texte_grise);
    ::InvalidateRect(::GetDlgItem(hwnd, ID_DQ1_TEXTE4), NULL, TRUE);
    }

  if (bool8 != tab_bool[7])
    {
    ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_TAILLE_TAB), bool8);
    if (bool8)
      {
      SetDlgItemText(hwnd, ID_DQ1_TAILLE_TAB, taille_tab);
      }
    else
      {
      GetDlgItemText (hwnd, ID_DQ1_TAILLE_TAB, taille_tab, 30);
      SetDlgItemText(hwnd, ID_DQ1_TAILLE_TAB, "");
      }
    }

  if (bool9 != tab_bool[8])
    {
    }

  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_LOG_SIMPLE), bool10);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_NUM_SIMPLE), bool11);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_MESS_SIMPLE), bool12);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_LOG_TAB), bool13);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_NUM_TAB), bool14);
  ::EnableWindow(::GetDlgItem(hwnd, ID_DQ1_MESS_TAB), bool15);

  tab_bool[0]  = bool1;
  tab_bool[1]  = bool2;
  tab_bool[2]  = bool3;
  tab_bool[3]  = bool4;
  tab_bool[4]  = bool5;
  tab_bool[5]  = bool6;
  tab_bool[6]  = bool7;
  tab_bool[7]  = bool8;
  tab_bool[8]  = bool9;
  tab_bool[9]  = bool10;
  tab_bool[10] = bool11;
  tab_bool[11] = bool12;
  tab_bool[12] = bool13;
  tab_bool[13] = bool14;
  tab_bool[14] = bool15;
  }

// -----------------------------------------------------------------------
// Nom......:
// Objet....: fonctions relatives a la wndproc dq1
// Entr�es..: HWND hwnd
// Sorties..:
// Retour...:
// Remarques:
// -----------------------------------------------------------------------
void retablir_disque(HWND hwnd, t_param_disque *param_disque) /* parametres sauvegardes */

  {
  StrCopy (nom_fic, param_disque->nom_fic);
  StrCopy (nom_var, param_disque->nom_var);
  StrCopy (val_init, param_disque->val_init);
  StrCopy (taille_tab, param_disque->taille_tab);
  SetDlgItemText (hwnd, ID_DQ1_NOM_FIC, param_disque->nom_fic);
  SetDlgItemText (hwnd, ID_DQ1_NOM_VAR, param_disque->nom_var);
  SetDlgItemText (hwnd, ID_DQ1_VAL_INIT, param_disque->val_init);
  SetDlgItemText (hwnd, ID_DQ1_TAILLE_TAB, param_disque->taille_tab);
  switch (param_disque->index_type_ligne)
    {
    case 1: /* FICHIER */
    SendDlgItemMessage (hwnd, ID_DQ1_FICHIER, BM_SETCHECK, TRUE, 0);
    montre_ou_cache (hwnd, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
                     FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
    break;

    case 2: /* VA. LECTURE */
    SendDlgItemMessage (hwnd, ID_DQ1_LECTURE, BM_SETCHECK, TRUE, 0);
    montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE,
                     FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
    break;

    case 3: /* VA. ECRITURE */
    SendDlgItemMessage (hwnd, ID_DQ1_ECRITURE, BM_SETCHECK, TRUE, 0);
    montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE,
                     FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
    break;

    case 4: /* VA. TAILLE */
    SendDlgItemMessage (hwnd, ID_DQ1_TAILLE, BM_SETCHECK, TRUE, 0);
    montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE,
                     FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
    break;

    case 5: /* VA. CHAMP */
    SendDlgItemMessage (hwnd, ID_DQ1_CHAMP, BM_SETCHECK, TRUE, 0);

    switch (param_disque->index_type_var)
      {
      case 1: /* SIMPLE */
      montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, TRUE,
                       TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
      SendDlgItemMessage (hwnd, ID_DQ1_LOG_SIMPLE, BM_SETCHECK, TRUE, 0);
      break;

      case 2:
      montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, TRUE,
                       TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
      SendDlgItemMessage (hwnd, ID_DQ1_NUM_SIMPLE, BM_SETCHECK, TRUE, 0);
      break;

      case 3:
      montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, TRUE,
                       TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
      SendDlgItemMessage (hwnd, ID_DQ1_MESS_SIMPLE, BM_SETCHECK, TRUE, 0);
      break;

      case 4: // Tableau
      montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE,
                       TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
      SendDlgItemMessage (hwnd, ID_DQ1_LOG_TAB, BM_SETCHECK, TRUE, 0);
      break;

      case 5:
      montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE,
                       TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
      SendDlgItemMessage (hwnd, ID_DQ1_NUM_TAB, BM_SETCHECK, TRUE, 0);
      break;

      case 6:
      montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE,
                       TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
      SendDlgItemMessage (hwnd, ID_DQ1_MESS_TAB, BM_SETCHECK, TRUE, 0);
      break;

      default:
        break;

      } /* switch */

    break;

    default:
      break;

    } /* switch */
  }

// -----------------------------------------------------------------------
void defaut_disque (HWND hwnd, t_param_disque *param_disque,
                     BOOL init) /* parametres par defaut */

  {
  param_disque->index_type_ligne = 1;
  param_disque->index_type_var   = 1;
  StrSetNull (param_disque->nom_fic);
  StrSetNull (param_disque->nom_var);
  StrSetNull (param_disque->val_init);
  StrSetNull (param_disque->taille_tab);
  StrCopy (nom_fic, param_disque->nom_fic);
  StrCopy (nom_var, param_disque->nom_var);
  StrCopy (val_init, param_disque->val_init);
  StrCopy (taille_tab, param_disque->taille_tab);
  SetDlgItemText (hwnd, ID_DQ1_NOM_FIC, param_disque->nom_fic);
  SetDlgItemText (hwnd, ID_DQ1_NOM_VAR, param_disque->nom_var);
  SetDlgItemText (hwnd, ID_DQ1_VAL_INIT, param_disque->val_init);
  SetDlgItemText (hwnd, ID_DQ1_TAILLE_TAB, param_disque->taille_tab);
  SendDlgItemMessage (hwnd, ID_DQ1_LOG_SIMPLE, BM_SETCHECK, TRUE, 0);
  SendDlgItemMessage (hwnd, ID_DQ1_FICHIER, BM_SETCHECK, TRUE, 0);
  montre_ou_cache (hwnd, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
                   FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
  }

// -----------------------------------------------------------------------
static void type_constante (t_param_disque *pctxt, char *champ,
                     BOOL *un_log, BOOL *un_num, BOOL *un_mess)
  {
  char mot_res1[40];
  char mot_res2[40];

  *un_log  = FALSE;
  *un_num  = FALSE;
  *un_mess = FALSE;
  //(*pctxt->echange_data.ads_proc_message_mot_reserve) (c_res_zero, mot_res1);
  bMotReserve (c_res_zero, mot_res1);
  //(*pctxt->echange_data.ads_proc_message_mot_reserve) (c_res_un, mot_res2);
  bMotReserve (c_res_un, mot_res2);
  if (bStrEgales (champ, mot_res1) || bStrEgales (champ, mot_res2))
    { /* va log */
    *un_log = TRUE;
    }
  else
    {
    if (StrGetChar (champ, 0) == '"')
      { /* va mess */
      *un_mess = TRUE;
      }
    else
      { /* va num */
      *un_num = TRUE;
      }
    }
  }
// -----------------------------------------------------------------------
DWORD fabrication_ul_disque (HWND hwnd, char *chaine,
                            t_param_disque *param_disque)
  {
  LONG index;
  BOOL un_log, un_num, un_mess;
  DWORD retour = 0;

  /* FABRICATION de la CHAINE � partir des champs de la boite */
  StrSetNull (chaine);
  /* r�cup�ration du type */
  index = nCheckIndex (hwnd, ID_DQ1_FICHIER); //SendDlgItemMessage (hwnd, ID_DQ1_FICHIER, BM_QUERYCHECKINDEX, 0, 0);
  param_disque->index_type_ligne = (DWORD)index;
  switch(index)
    {
    case 1: // FICHIER
      //(*param_disque->echange_data.ads_proc_message_mot_reserve) (c_res_fichier, mot_res1);
      bMotReserve (c_res_fichier, mot_res1);
      StrConcat (chaine, mot_res1);
      StrConcatChar (chaine, ' ');
      GetDlgItemText (hwnd, ID_DQ1_NOM_FIC, nom_fic, 255);
      StrConcat (chaine, nom_fic);
      StrCopy (param_disque->nom_fic, nom_fic);
      break;

    case 2: // VA. LECTURE
      //(*param_disque->echange_data.ads_proc_message_mot_reserve) (c_res_lecture, mot_res1);
      bMotReserve (c_res_lecture, mot_res1);
      StrConcat (chaine, mot_res1);
      StrConcatChar (chaine, ' ');
      GetDlgItemText (hwnd, ID_DQ1_NOM_VAR, nom_var, 30);
      StrConcat (chaine, nom_var);
      StrCopy (param_disque->nom_var, nom_var);
      break;

    case 3: // VA. ECRITURE 
      //(*param_disque->echange_data.ads_proc_message_mot_reserve) (c_res_ecriture, mot_res1);
      bMotReserve (c_res_ecriture, mot_res1);
      StrConcat (chaine, mot_res1);
      StrConcatChar (chaine, ' ');
      GetDlgItemText (hwnd, ID_DQ1_NOM_VAR, nom_var, 30);
      StrConcat (chaine, nom_var);
      StrCopy (param_disque->nom_var, nom_var);
      break;

    case 4: // VA. TAILLE
      //(*param_disque->echange_data.ads_proc_message_mot_reserve) (c_res_enregistrements, mot_res1);
      bMotReserve (c_res_enregistrements, mot_res1);
      StrConcat (chaine, mot_res1);
      StrConcatChar (chaine, ' ');
      GetDlgItemText (hwnd, ID_DQ1_NOM_VAR, nom_var, 30);
      StrConcat (chaine, nom_var);
      StrCopy (param_disque->nom_var, nom_var);
      break;

    case 5: /* VA. CHAMP */
      GetDlgItemText (hwnd, ID_DQ1_NOM_VAR, nom_var, 30);
      StrConcat (chaine, nom_var);
      StrConcatChar (chaine, ' ');
      StrCopy (param_disque->nom_var, nom_var);
      /* r�cup�ration du type */
      index = nCheckIndex(hwnd, ID_DQ1_LOG_SIMPLE);//SendDlgItemMessage (hwnd, ID_DQ1_LOG_SIMPLE, BM_QUERYCHECKINDEX, 0, 0);
      param_disque->index_type_var = (DWORD)++index;
      if (index > 3)
        { /* variable champ tableau */
        /* r�cup�ration de la Taille */
        GetDlgItemText (hwnd, ID_DQ1_TAILLE_TAB, taille_tab, 30);
        if (StrIsNull (taille_tab))
          {
          retour = IDSERR_LA_TAILLE_DU_TABLEAU_EST_INVALIDE;
          }
        StrCopy (param_disque->taille_tab, taille_tab);
        StrConcat (chaine, taille_tab);
        StrConcatChar (chaine, ' ');
        }

      /* r�cup�ration de la Valeur Initiale */
      GetDlgItemText (hwnd, ID_DQ1_VAL_INIT, val_init, 100);
      StrCopy (param_disque->val_init, val_init);
      StrConcat (chaine, val_init);

      type_constante (param_disque, val_init, &un_log, &un_num, &un_mess);

      switch (index)
        {
        case 1: /* logique */
        case 4:
          if (!un_log)
            {
            retour = IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE;
            }
          break;

        case 2: /* numerique */
        case 5:
          if (!un_num)
            {
            retour = IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE;
            }
          break;

        case 3: /* message */
        case 6:
          if (!un_mess)
            {
            retour = IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE;
            }
          break;
        default:
          break;

        }
      break;

    default:
      break;
    }
  return (retour);
  }

// -----------------------------------------------------------------------
// Nom......: fnwp_dq1
// Objet....: boite disque
// Retour...: LRESULT: r�sultat du traitement du message
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgDisque (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  bRes = 0;			   // Valeur de retour
  BOOL  commande_a_executer = FALSE;
  t_param_disque *pctxt;
  t_param_action  param_action;
  t_retour_action retour_action;
  LONG index;
  DWORD i;
  DWORD erreur;
  DWORD nb_champs;
  char tab_champs[6][STR_MAX_CHAR_ARRAY];
  BOOL un_log, un_num, un_mess;

  switch (msg)
    {
    case WM_INITDIALOG:
			{
      SetFocus(::GetDlgItem (hwnd, ID_DQ1_NOM_FIC));
      init_montre_ou_cache (hwnd, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
                            FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
			{
			// la structure se trouve a l'adresse donnee par mp2
			t_dlg_desc *pechange_data = (t_dlg_desc *) mp2;

			// allocation de memoire pour le pteur sur la structure
			pctxt = (t_param_disque *)pCreeEnv (hwnd, sizeof (t_param_disque));
			pctxt->echange_data = (*pechange_data);
			}

      // initialisation des champs de la boite 
      if (StrIsNull (pctxt->echange_data.chaine))
        { // pas double clique : on veut inserer
        defaut_disque (hwnd, pctxt, TRUE);
        }
      else
        { // double clique : on veut modifier, il faut r�afficher la ligne

        defaut_disque (hwnd, pctxt, TRUE);

        StrDeleteFirstSpaces (pctxt->echange_data.chaine);
        nb_champs = StrToStrArray (tab_champs, 6, pctxt->echange_data.chaine);
        if (StrGetChar (tab_champs[0], 0) != '"')
          {
          //(*pctxt->echange_data.ads_proc_message_mot_reserve) (c_res_fichier, mot_res1);
          bMotReserve (c_res_fichier, mot_res1);
          if (bStrEgales (tab_champs[0], mot_res1))
            { // FICHIER
            pctxt->index_type_ligne = 1;
            StrSetNull (pctxt->nom_fic);
            for (i=1; (i<nb_champs); i++)
              {
              StrConcat (pctxt->nom_fic, tab_champs[i]);
              }
            }
          else
            {
            //(*pctxt->echange_data.ads_proc_message_mot_reserve) (c_res_lecture, mot_res1);
            bMotReserve (c_res_lecture, mot_res1);
            if (bStrEgales (tab_champs[0], mot_res1))
              { // LECTURE
              pctxt->index_type_ligne = 2;
              StrCopy (pctxt->nom_var, tab_champs[1]);
              }
            else
              {
              //(*pctxt->echange_data.ads_proc_message_mot_reserve) (c_res_ecriture, mot_res1);
              bMotReserve (c_res_ecriture, mot_res1);
              if (bStrEgales (tab_champs[0], mot_res1))
                { // ECRITURE
                pctxt->index_type_ligne = 3;
                StrCopy (pctxt->nom_var, tab_champs[1]);
                }
              else
                {
                //(*pctxt->echange_data.ads_proc_message_mot_reserve) (c_res_enregistrements, mot_res1);
                bMotReserve (c_res_enregistrements, mot_res1);
                if (bStrEgales (tab_champs[0], mot_res1))
                  { // TAILLE
                  pctxt->index_type_ligne = 4;
                  StrCopy (pctxt->nom_var, tab_champs[1]);
                  }
                else
                  { // c'est une variable champ
                  pctxt->index_type_ligne = 5;
                  StrCopy (pctxt->nom_var, tab_champs[0]);

                  type_constante (pctxt, tab_champs[nb_champs-1],
                                  &un_log, &un_num, &un_mess);
                  if (un_log)
                    { /* va log */
                    pctxt->index_type_var = 1;
                    }
                  else
                    {
                    if (un_num)
                      { /* va num */
                      pctxt->index_type_var = 2;
                      }
                    else
                      { /* va mess */
                      pctxt->index_type_var = 3;
                      }
                    }
                  if (nb_champs > 2)
                    { // variable tableau
                    pctxt->index_type_var = pctxt->index_type_var + 3;
                    StrCopy (pctxt->taille_tab, tab_champs[1]);
                    StrCopy (pctxt->val_init, tab_champs[2]);
                    }
                  else
                    { // variable simple
                    StrCopy (pctxt->val_init, tab_champs[1]);
                    }
                  }
                }
              }
            }
          }
        else // ligne commentaire
          {
          defaut_disque (hwnd, pctxt, TRUE);
          }
        retablir_disque(hwnd, pctxt);
        }
      bRes = (LRESULT)FALSE;
			}
      break;

    case WM_CLOSE:
			{
      EndDialog(hwnd, FALSE);
      pctxt = (t_param_disque *)pGetEnv (hwnd);
			}
      break;


    case WM_DESTROY:
      bLibereEnv (hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_DQ1_INSERER:
          pctxt = (t_param_disque *)pGetEnv (hwnd);
          pctxt->echange_data.n_ligne_depart++;
          param_action.entier = pctxt->echange_data.n_ligne_depart;
          bAjouterActionCf (action_nouvelle_ligne_dlg, &param_action);
          fabrication_ul_disque (hwnd, param_action.chaine, pctxt);
          param_action.entier = pctxt->echange_data.n_ligne_depart;
          bAjouterActionCf (action_validation_dlg, &param_action);
          commande_a_executer = TRUE;
          break;

        case ID_DQ1_MODIFIER:
          pctxt = (t_param_disque *)pGetEnv (hwnd);
          if ((erreur = fabrication_ul_disque (hwnd, param_action.chaine, pctxt)) == 0)
            {
            param_action.entier = pctxt->echange_data.n_ligne_depart;
            bAjouterActionCf (action_validation_dlg, &param_action);
            commande_a_executer = TRUE;
            }
          else
            {
            dlg_num_erreur (erreur);
            }
          break;

        case ID_DQ1_DEFAUT:
          pctxt = (t_param_disque *)pGetEnv (hwnd);
          defaut_disque(hwnd, pctxt, FALSE);
          break;

        case ID_DQ1_RETABLIR:
          pctxt = (t_param_disque *)pGetEnv (hwnd);
          retablir_disque(hwnd, pctxt);
          break;

				case ID_DQ1_FERMER:
          pctxt = (t_param_disque *)pGetEnv (hwnd);
          EndDialog(hwnd, FALSE);
          break;

				case IDCANCEL:
					pctxt = (t_param_disque *)pGetEnv (hwnd);
					EndDialog(hwnd, FALSE);
					break;

				default:
				if (HIWORD( mp1 ) == BN_CLICKED)
					switch(LOWORD( mp1 ) )
						{
						case ID_DQ1_FICHIER:
							SetFocus (::GetDlgItem (hwnd, ID_DQ1_NOM_FIC));
							montre_ou_cache (hwnd, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE,
															 FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
							break;

						case ID_DQ1_LECTURE:
							SetFocus (::GetDlgItem (hwnd, ID_DQ1_NOM_VAR));
							montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE,
															 FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
							break;

						case ID_DQ1_ECRITURE:
							SetFocus(::GetDlgItem (hwnd, ID_DQ1_NOM_VAR));
							montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE,
															 FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
							break;

						case ID_DQ1_TAILLE:
							SetFocus(::GetDlgItem (hwnd, ID_DQ1_NOM_VAR));
							montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE,
															 FALSE, FALSE, FALSE, FALSE, FALSE, FALSE);
							break;

						case ID_DQ1_CHAMP:
							SetFocus(::GetDlgItem (hwnd, ID_DQ1_NOM_VAR));
							// r�cup�ration du type
							montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, TRUE,
															 TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
							index = nCheckIndex (hwnd, ID_DQ1_LOG_SIMPLE); //SendDlgItemMessage (hwnd, ID_DQ1_LOG_SIMPLE, BM_QUERYCHECKINDEX, 0, 0);
							if (index > 2)
								{ // variable champ tableau
								montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE,
																 TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
								}
							else
								{
								montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, TRUE,
																 TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
								}
							break;

						case ID_DQ1_LOG_SIMPLE:
						case ID_DQ1_NUM_SIMPLE:
						case ID_DQ1_MESS_SIMPLE:
							montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, TRUE,
															 TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
							break;

						case ID_DQ1_LOG_TAB:
						case ID_DQ1_NUM_TAB:
						case ID_DQ1_MESS_TAB:
							montre_ou_cache (hwnd, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE,
															 TRUE, TRUE, TRUE, TRUE, TRUE, TRUE);
							break;
					} // default : switch(LOWORD(mp1))
        } // switch(LOWORD(mp1))
			if (commande_a_executer)
				{
				pctxt = (t_param_disque *)pGetEnv (hwnd);
				bExecuteActionCf (&retour_action);
				pctxt->echange_data.n_ligne_depart = retour_action.n_ligne_depart;
				StrCopy (pctxt->echange_data.chaine, retour_action.chaine);
				}
      break;

    default:
      bRes = FALSE;
      break;
    }  // fin switch

  return (bRes);
  }

// -------------------------------------------------------------------
// R�cup�re l'identificateur et l'adresse de la Window Proc
// de la bo�te a afficher. En entr�e, on passe le num�ro de la bo�te parmi
// les bo�tes du descripteur.
//--------------------------------------------------------------------
void LIS_PARAM_DLG_DQ (int index_boite, UINT *iddlg, FARPROC * adr_winproc)
  {
  switch (index_boite)
    {
    case 0 :
      *iddlg = DLG_INTERPRETE_DQ1;
      (*adr_winproc) = (FARPROC) dlgDisque;
      break;

    default:
			VerifWarningExit;
      break;
    }
  }


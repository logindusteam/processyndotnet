/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de 						|
 |				Societe LOGIQUE INDUSTRIE 					|
 |			 Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3				|
 | Il est demeure sa propriete exclusive et est confidentiel. 			|
 | Aucune diffusion n'est possible sans accord ecrit. 											|
 |--------------------------------------------------------------------------|
 |	 Titre	 : CV_JB.C									|
 |	 Auteur  : AC 								|
 |	 Date 	 : 31/12/93 								|
 // 28/3/97 Version WIN32
 +--------------------------------------------------------------------------*/
#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_jb.h"
#include "Modbus.h"
#include "IdLngLng.h"

#include "cvt_jb.h"

// ---------------------------------------------------------------------------
// Conversion d'une chaine d'adresse logique automate en adresse MODBUS
//
// La valeur *pwSensOuNombreSurAutomate sert a definir le code fonction MODBUS qui sera
// utilise.
//
// En lecture automate :
//	wSensSurAutomate = e ==> appel des fonctions	MODBUS_F_LECTURE_BITS_ENTREE ou	MODBUS_F_LECTURE_MOTS_ENTREE
//	wSensSurAutomate = s ==> appel des fonctions	MODBUS_F_LECTURE_BITS_SORTIE ou	MODBUS_F_LECTURE_MOTS_SORTIE
//
// En ecriture automate:
//	wSensSurAutomate = e ==> appel des fonctions	MODBUS_F_ECRITURE_N_BITS ou MODBUS_F_ECRITURE_N_MOTS
//	wSensSurAutomate = s ==> appel des fonctions	MODBUS_F_ECRITURE_BIT ou	MODBUS_F_ECRITURE_MOT
// ---------------------------------------------------------------------------
DWORD dwConversionAdresseModbus 
	(
	MODBUS_AUTOMATE nAutomate, 
	DWORD sens, DWORD genre, PCSTR pszAdresse, 
	DWORD * pwAdresseModbus,
	DWORD * pwSensOuNombreSurAutomate
	)
	{
	char en_tete;
	char pot[10];
	BOOL bTest;
	DWORD offset;
	DWORD wretour;
	char chaine_adresse[82];
	char chaine_res [c_nb_car_message_res];
	DWORD wTampon;

	chaine_adresse[0] = '\0';
	chaine_res[0] = '\0';
	pot[0] = '\0';
	StrCopy (chaine_adresse, pszAdresse);
	switch (nAutomate)
		{
		case MODBUS_AUTOMATE_SMC:
			en_tete = StrGetChar (chaine_adresse, 0);
			StrDelete (chaine_adresse, 0, 1);
			if (!StrToDWORD  (&wTampon, chaine_adresse))
				{
				wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
				}
			else
				{
				wretour = 0;
				(*pwAdresseModbus) = wTampon;
				if (genre == c_res_logique)
					{
					switch (en_tete)
						{
						case 'a':
						case 'A':
							if ((*pwAdresseModbus) > 1023)
								{
								(*pwAdresseModbus) = 65535;
								}
							else
								{
								if ((*pwAdresseModbus) > 511)
									{
									(*pwAdresseModbus) = ((*pwAdresseModbus) - 512) + 3584;
									}
								else
									{
									if ((*pwAdresseModbus) > 127)
										{
										(*pwAdresseModbus) = ((*pwAdresseModbus) - 128) + 640;
										}
									else
										{
										(*pwAdresseModbus) = (*pwAdresseModbus) + 128;
										}
									}
								}
							break;

						case 'b':
						case 'B' :
							if ((*pwAdresseModbus) > 1023)
								{
								(*pwAdresseModbus) = 65535;
								}
							else
								{
								if ((*pwAdresseModbus) > 511)
									{
									(*pwAdresseModbus) = ((*pwAdresseModbus) - 512) + 2048;
									}
								else
									{
									(*pwAdresseModbus) = (*pwAdresseModbus) + 1024;
									}
								}
							break;

						case 'e':
						case 'E' :
							if ((*pwAdresseModbus) > 3023)
								{
								(*pwAdresseModbus) = 65535;
								}
							else
								{
								if ((*pwAdresseModbus) > 2511)
									{
									(*pwAdresseModbus) = ((*pwAdresseModbus) - 2512) + 3584;
									}
								else
									{
									if ((*pwAdresseModbus) > 2127)
										{
										(*pwAdresseModbus) = ((*pwAdresseModbus) - 2128) + 640;
										}
									else
										{
										if ((*pwAdresseModbus) > 1999)
											{
											(*pwAdresseModbus) = ((*pwAdresseModbus) - 2000) + 128;
											}
										else
											{
											if ((*pwAdresseModbus) > 1023)
												{
												(*pwAdresseModbus) = 65535;
												}
											else
												{
												if ((*pwAdresseModbus) > 511)
													{
													(*pwAdresseModbus) = ((*pwAdresseModbus) - 512) + 3072;
													}
												else
													{
													if ((*pwAdresseModbus) > 127)
														{
														(*pwAdresseModbus) = ((*pwAdresseModbus) - 128) + 256;
														}
													}
												}
											}
										}
									}
								}
							break;

						case 'r':
						case 'R' :
							if ((*pwAdresseModbus) > 1023)
								{
								(*pwAdresseModbus) = 65535;
								}
							else
								{
								if ((*pwAdresseModbus) > 511)
									{
									(*pwAdresseModbus) = ((*pwAdresseModbus) - 512) + 2560;
									}
								else
									{
									(*pwAdresseModbus) = (*pwAdresseModbus) + 1536;
									}
								}
							break;

						default:
							(*pwAdresseModbus) = 65535;
							break;
						}
					if ((*pwAdresseModbus) != 65535)
						{
						wretour = 0;
						}
					else
						{
						wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
						}
					} // logique
				}
				(*pwSensOuNombreSurAutomate) = c_res_s; // si lecture alors fonctions 1 et 3
			break; // cas SMC 									si ecriture fct 5 et 6

		case MODBUS_AUTOMATE_PB:
		case MODBUS_AUTOMATE_ALSPA:
			bTest = (BOOL)  ((StrLength (chaine_adresse) > 1) &&
												 (StrLength (chaine_adresse) < 6) &&
												 (StrGetChar (chaine_adresse, 0) == 'H'));
			if (bTest)
				{
				StrDelete (chaine_adresse, 0, 1);
				if (!StrToDWORDBase (&wTampon, chaine_adresse, 16))
					{
					wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
					}
				else
					{
					(*pwAdresseModbus) = wTampon;
					wretour = 0;
					}
				}
			else
				{
				wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
				}
			(*pwSensOuNombreSurAutomate) = c_res_s; 			// si lecture alors fonctions 1 et 3
			break; // cas PB/ALSPA						// si ecriture fct 5 et 6

		case MODBUS_AUTOMATE_F15_F16:
			bTest = (BOOL)  ((StrLength (chaine_adresse) > 1) &&
				(StrLength (chaine_adresse) < 6) &&
				(StrGetChar (chaine_adresse, 0) == 'H'));
			if (bTest)
				{
				StrDelete (chaine_adresse, 0, 1);
				if (!StrToDWORDBase (&wTampon, chaine_adresse, 16))
					{
					wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
					}
				else
					{
					(*pwAdresseModbus) = wTampon;
					wretour = 0;
					}
				}
			else
				{
				wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
				}
			if (sens == c_res_e)
				{
				(*pwSensOuNombreSurAutomate) = c_res_s; // si lecture alors fonctions 1 et 3
				}
			else
				{
				(*pwSensOuNombreSurAutomate) = c_res_e; // si ecriture alors fonctions 15 et 16
				}
			break; //F15_F16

		case MODBUS_AUTOMATE_TELE:
		case MODBUS_AUTOMATE_SIEMENS:
		case MODBUS_AUTOMATE_SMCX00:
			if (sens == c_res_e)
				{
				bMotReserve (c_res_e, chaine_res);
				if (chaine_adresse [0] == chaine_res [0])
					{
					(*pwSensOuNombreSurAutomate) = c_res_e; // si lecture entree alors fct 2 et 4
					bTest = TRUE;
					}
				else
					{
					bMotReserve (c_res_s, chaine_res);
					if (chaine_adresse [0] == chaine_res [0])
						{
						(*pwSensOuNombreSurAutomate) = c_res_s; // si lecture sortie alors fct 1 et 3
						bTest = TRUE;
						}
					else
						{
						bTest = FALSE;
						}
					}
				offset = 2;
				} // entree de processyn
			else
				{
				(*pwSensOuNombreSurAutomate) = c_res_s; // si ecriture fct 5 et 6
				offset = 1;
				bTest = TRUE;
				}
			if (bTest)
				{
				bTest = (BOOL) ((StrLength (chaine_adresse) > 1) &&
													(StrLength (chaine_adresse) < (offset + 5)) &&
													(StrGetChar (chaine_adresse, offset - 1) == 'H'));
				}
			if (bTest)
				{
				StrDelete (chaine_adresse, 0, offset);
				if (!StrToDWORDBase (&wTampon, chaine_adresse, 16))
					{
					wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
					}
				else
					{
					(*pwAdresseModbus) = wTampon;
					wretour = 0;
					}
				}
			else
				{
				wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
				}
			break; // cas TELEMECANIQUE

		case MODBUS_AUTOMATE_GOULD:
			if (sens == c_res_e)
				{
				if ((chaine_adresse [0] == '1') || (chaine_adresse [0] == '3'))
					{
					(*pwSensOuNombreSurAutomate) = c_res_e; // si lecture entree alors fct 2 et 4
					bTest = TRUE;
					}
				else
					{
					if ((chaine_adresse [0] == '0') || (chaine_adresse [0] == '4'))
						{
						(*pwSensOuNombreSurAutomate) = c_res_s; // si lecture sortie alors fct 1 et 3
						bTest = TRUE;
						}
					else
						{
						bTest = FALSE;
						}
					}
				} // entree de processyn
			else
				{
				(*pwSensOuNombreSurAutomate) = c_res_s; // si ecriture fct 5 et 6
				bTest = TRUE;
				}
			if (bTest)
				{
				bTest = (BOOL) ((StrLength (chaine_adresse) == 5) || (StrLength (chaine_adresse) == 6));
				}
			if (bTest)
				{
				StrDelete (chaine_adresse, 0, 1);
				bTest = StrToDWORD (&wTampon, chaine_adresse);
				if (bTest)
					{
					(*pwAdresseModbus) = wTampon - 1;
					bTest = (BOOL)((*pwAdresseModbus) <= 65536);
					}
				}
			if (bTest)
				{
				wretour = 0;
				}
			else
				{
				wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
				}
			break; // cas GOULD

		default:
			wretour = IDSERR_LA_DESCRIPTION_EST_INVALIDE;
			break;

		} // switch sorte auto
	return (wretour);
	}

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------*/
// inter_opc.cpp
// Version 1.0 Win32 JS 1/9/98

#include "stdafx.h"
#include <stdlib.h>
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "opc.h"
#include "OPCError.h"
#include "TemplateArray.h"
#include "OPCClient.h"
#include "tipe_opc.h"
#include "cvt_ul.h"
#include "IdLngLng.h"
#include "UExcept.h"
#include "UInter.h"
#include "Verif.h"
#include "inter_opc.h"
VerifInit;

//-------------------------- Variables locales ---------------------
typedef struct
  {
  char szNomServeur[c_nb_car_ex_mes];
  char szNomGroupe[c_nb_car_ex_mes];
  ID_MOT_RESERVE sens; //libre ou c_res_e ou c_res_s
  ID_MOT_RESERVE genre_donnees; // (libre ou c_res_serveur, c_res_groupe)
  } CONTEXTE_OPC;

static CONTEXTE_OPC ContexteOPC;

static DWORD ligne_depart_opc;
static DWORD ligne_courante_opc;
static BOOL bOpcDejaInitialise;

//-------------------------------------------------------------------------
// Mise � jour du contexte pour une ligne donn�e
static void consulte_titre_paragraf_opc (DWORD ligne)
  {
  CONTEXTE_PARAGRAPHE_LIGNE aContexteParagrapheLigne[3];
  DWORD dwProfondeurLigneParagraphe;

	// r�cup�ration contexte Ok ?
  if (bDonneContexteParagrapheLigne (b_titre_paragraf_opc, ligne, &dwProfondeurLigneParagraphe, aContexteParagrapheLigne))
    { // oui =>
    for (DWORD dwProfondeurLigne = 0; (dwProfondeurLigne < dwProfondeurLigneParagraphe); dwProfondeurLigne++)
      {
      switch (aContexteParagrapheLigne [dwProfondeurLigne].IdParagraphe)
        {
        case PARAGRAPHE_SERVEUR_OPC :
					{
					PTITRE_PARAGRAPHE pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_opc, aContexteParagrapheLigne [dwProfondeurLigne].position);
          PGEO_OPC	pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc, pTitreParagraphe->pos_geo_debut);
					PSPEC_TITRE_PARAGRAPHE_OPC	pSpecTitreParagraphe = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc, pGeoOPC->pos_specif_paragraf);
          StrCopy (ContexteOPC.szNomServeur, pSpecTitreParagraphe->Serveur.szNomServeur);
					ContexteOPC.genre_donnees = c_res_serveur;
          ContexteOPC.sens  = libre;
					}
          break;
					
        case PARAGRAPHE_GROUPE_OPC :
					{
					PTITRE_PARAGRAPHE pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_opc, aContexteParagrapheLigne [dwProfondeurLigne].position);
          PGEO_OPC	pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc, pTitreParagraphe->pos_geo_debut);
					PSPEC_TITRE_PARAGRAPHE_OPC	pSpecTitreParagraphe = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc, pGeoOPC->pos_specif_paragraf);
          StrCopy (ContexteOPC.szNomGroupe, pSpecTitreParagraphe->Groupe.szNomGroupe);
					ContexteOPC.genre_donnees = c_res_groupe;
          ContexteOPC.sens  = pSpecTitreParagraphe->Groupe.IdSens;
					}
          break;
					
				default:
					ContexteOPC.genre_donnees = libre;
          ContexteOPC.sens  = libre;
				  break;
        } // switch
      } // for dwProfondeurLigne
    } // donne_contexte_ok
  else
    {
		ContexteOPC.genre_donnees = libre;
    //StrSetNull (ContexteOPC.szNomServeur);
    }
  } // consulte_titre_paragraf_opc

//-------------------------------------------------------------------------
//                             DIVERS
//-------------------------------------------------------------------------

static void maj_pointeur_dans_geo (DWORD numero_repere, DWORD limite, BOOL bSpec)
  {
  DWORD nbr_enr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_geo_opc);
	
  for (DWORD i = 1; (i <= nbr_enr); i++)
    {
		PGEO_OPC pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc,i);

    if (pGeoOPC->numero_repere == numero_repere)
      {
      switch (numero_repere)
        {
        case b_opc_item:
					{
          if (pGeoOPC->pos_specif_es >= limite)
						{
            pGeoOPC->pos_specif_es--;
						}
					}
          break;
					
        case b_titre_paragraf_opc:
					{
					if (bSpec)
						{
            if (pGeoOPC->pos_specif_paragraf >= limite)
							{
              pGeoOPC->pos_specif_paragraf--;
							}
            }
					else
						{
						if (pGeoOPC->pos_paragraf >= limite)
							{
							pGeoOPC->pos_paragraf--;
							}
						}
					}
          break;
					
				default:
					VerifWarningExit;
          break;
					
        } //switch
      } //repere trouv�
    } // for
  }


//-------------------------------------------------------------------------
void static traitement_num (PUL ul, DWORD num)
  {
  StrDWordToStr (ul->show, num);
  ul->erreur = 0;
  }

//-------------------------------------------------------------------------
void static traitement_mes (PUL ul, PCSTR psz)
  {
  StrCopy (ul->show, psz);
  ul->erreur = 0;
  }

//-------------------------------------------------------------------------
//                             EDITION
//-------------------------------------------------------------------------

//-------------------------------------------------------------------------
// generation vecteur UL
//-------------------------------------------------------------------------
static void genere_vect_ul (DWORD ligne, PUL vect_ul, int *nb_ul, int *niveau, int *indentation)
  {
  PGEO_OPC pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc,ligne);

  (*niveau) = 0;
  (*indentation) = 0;
  switch (pGeoOPC->numero_repere)
    {
    case b_pt_constant:
			// g�n�re une ligne de description de commentaires
			{
      (*nb_ul) = 1;
      vect_ul[0].erreur = 0;
      vect_ul[0].show[0] = '\0';
      consulte_cst_bd_c (pGeoOPC->pos_donnees, vect_ul[0].show);
			}
      break;
			
    case b_titre_paragraf_opc :
			// g�n�re une ligne de description de paragraphe
			{
      PTITRE_PARAGRAPHE pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_opc,pGeoOPC->pos_paragraf);
      switch (pTitreParagraphe->IdParagraphe)
        {
        case PARAGRAPHE_SERVEUR_OPC:
					{
          (*niveau) = 1;
          (*indentation) = 0;
          (*nb_ul) = 5;
					init_vect_ul (vect_ul, *nb_ul);
          PSPEC_TITRE_PARAGRAPHE_OPC	pSpecTitreParagraphe = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc, pGeoOPC->pos_specif_paragraf);
          bMotReserve (c_res_serveur, vect_ul[0].show);
          traitement_mes(&(vect_ul[1]), pSpecTitreParagraphe->Serveur.szNomServeur);
					NomVarES (vect_ul[2].show, pSpecTitreParagraphe->Serveur.dwIdESStatut);
					switch (pSpecTitreParagraphe->Serveur.dwIdAcces)
						{
						case libre:
							(*nb_ul) = 3;
							break;
						case c_res_opc_remote:
							(*nb_ul) = 5;
							bMotReserve (c_res_opc_remote, vect_ul[3].show);
							traitement_mes(&(vect_ul[4]), pSpecTitreParagraphe->Serveur.szNomPoste);
							break;
						default:
							(*nb_ul) = 4;
							bMotReserve (pSpecTitreParagraphe->Serveur.dwIdAcces, vect_ul[3].show);
							break;
						}
					if (pSpecTitreParagraphe->Serveur.dwIdESExecution != 0)
						{
						NomVarES (vect_ul[*nb_ul].show, pSpecTitreParagraphe->Serveur.dwIdESExecution);
						*nb_ul = (*nb_ul)+1;
						}
					}
          break; // PARAGRAPHE_SERVEUR_OPC

        case PARAGRAPHE_GROUPE_OPC:
					{
          (*niveau) = 2;
          (*indentation) = 1;
          (*nb_ul) = 6;
					init_vect_ul (vect_ul, *nb_ul);
          bMotReserve (c_res_groupe, vect_ul[0].show);
					PSPEC_TITRE_PARAGRAPHE_OPC	pSpecTitreParagraphe = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc,pGeoOPC->pos_specif_paragraf);
          traitement_mes(&(vect_ul[1]), pSpecTitreParagraphe->Groupe.szNomGroupe);
					NomVarES (vect_ul[2].show, pSpecTitreParagraphe->Groupe.dwIdESRaf);
					NomVarES (vect_ul[3].show, pSpecTitreParagraphe->Groupe.dwIdESStatut);
          bMotReserve (pSpecTitreParagraphe->Groupe.dwIdOperation, vect_ul[4].show);
					if (pSpecTitreParagraphe->Groupe.dwIdOperation == c_res_opc_entree_cache)
						{
						traitement_num(&(vect_ul[5]), pSpecTitreParagraphe->Groupe.dwCadenceMS);
						}
					else
						(*nb_ul) = 5;
					}
          break; // PARAGRAPHE_SERVEUR_OPC

        } //switch IdParagraphe
			}
			break; //b_titre_paragraf_opc
				
    case b_opc_item: // g�n�re une ligne de description d'item
			{
			int n_ul = 0;
      (*niveau) = 3;
      (*indentation) = 2;
			init_vect_ul (vect_ul, 6);
      NomVarES (vect_ul[n_ul].show, pGeoOPC->dwIdES); // Nom var
			n_ul++;
			PITEM_OPC_CF pOpcItem = (PITEM_OPC_CF) pointe_enr (szVERIFSource, __LINE__, b_opc_item, pGeoOPC->pos_specif_es);
			DWORD dwTaille = dwTailleES(pGeoOPC->dwIdES);
			if (dwTaille) // �ventuelle dimension du tableau
				{
				traitement_num(&(vect_ul[n_ul]), dwTaille);
				n_ul++;
				}
			StrCopy (vect_ul[n_ul].show, pOpcItem->szAdresseItem);
			n_ul++;
			if (!StrIsNull(pOpcItem->szAdresseItemExt1)) // V6.0.6 : ajoute une Extension 1 adresse si non vide (champ optionnel)
				{
				StrCopy (vect_ul[n_ul].show, pOpcItem->szAdresseItemExt1);
				n_ul++;
				}
			if (!StrIsNull(pOpcItem->szAdresseItemExt2)) // V6.0.6 : ajoute une Extension 2 adresse  si non vide (champ optionnel)
				{
				StrCopy (vect_ul[n_ul].show, pOpcItem->szAdresseItemExt2);
				n_ul++;
				}
      NomVarES (vect_ul[n_ul].show, pOpcItem->dwIdESRaf); // nom raf
			n_ul++;
      NomVarES (vect_ul[n_ul].show, pOpcItem->dwIdESStatut); // statut / qualit�
			n_ul++;
			if (pOpcItem->dwIdTypeVar != libre) // �ventuel type
				{
				bMotReserve (pOpcItem->dwIdTypeVar, vect_ul[n_ul].show);
				n_ul++;
				}
			if (pGeoOPC->pos_initial) // �ventuelle valeur initiale
				{
				consulte_cst_bd_c (pGeoOPC->pos_initial, vect_ul[n_ul].show);
				n_ul++;
				}
      (*nb_ul) = n_ul;
			}
      break;
			
    default:
			VerifWarningExit;
      break;
    } // switch numero repere
  } // genere_vect_ul

//-------------------------------------------------------------------------
// V�rifie la syntaxe sans toucher � la base de donn�es
// et renvoie le r�sultat de l'analyse
//-------------------------------------------------------------------------
static void AnalyseLigneOPC
	(
	REQUETE_EDIT Requete, PUL vect_ul, int nb_ul, DWORD ligne, DWORD *profondeur,
	PID_PARAGRAPHE tipe_paragraf, DWORD *numero_repere, PVARIABLE_RECONNUE_CF pVarESReconnue,
	PCONSTANTE_RECONNUE_CF pConstanteInit,
	PSPEC_TITRE_PARAGRAPHE_OPC	pspec_titre_paragraf, PITEM_OPC_CF pSpecItem
	)
  {
  DWORD nbr_imbrication;
  ID_MOT_RESERVE dwNMotReserve;
  DWORD mot;
  DWORD position_es;
  BOOL booleen;
  FLOAT reel;
  BOOL un_log;
  BOOL un_num;
	int NUl = 0;
	BOOL bExisteAdresse;

	// D�termine le type du premier mot
  CONTEXTE_OPC ContexteOPCAncien = ContexteOPC;

	// Premier mot de la ligne = identificateur ?
  if (bReconnaitESValide (NUl, (vect_ul+NUl), pVarESReconnue))
		{
		// oui => La ligne doit d�crire un ITEM :
		// Cette ligne est d�clar�e dans un groupe ?
		if (ContexteOPC.genre_donnees != c_res_groupe)
			{
			ExcepErreurCompile (IDSERR_PARAGRAPHE_INCORRECT);
			}

		// D�termine le type de description de variable par sa description...
		// puis d�termine les conditions de d�claration de la variable

		// * Nom ES
		ULSuivante (&NUl,nb_ul);

		// * [Nb Elements tableau] ou adresse OPC
		// Constante de n'importe quel type ?
		DWORD dwIdConstante;
		if (bReconnaitConstante ((vect_ul+NUl), &booleen, &reel, &un_log, &un_num, &bExisteAdresse, 
			&dwIdConstante))
			{
			// oui => num�rique ?
			if (un_num)
				{
				// oui => Nombre d'�l�ments tableau valide ?
				if ((reel < 1) || (reel>NB_MAX_ELEMENTS_VAR_TABLEAU))
					{
					ExcepErreurCompile (IDSERR_LA_TAILLE_DU_TABLEAU_EST_COMPRISE_ENTRE_1_ET_10000);
					}

				// oui => Il s'agit d'un tableau
				pVarESReconnue->es.taille = (DWORD) reel;

				// * adresse = constante message ?
				ULSuivante (&NUl,nb_ul);
				if (!bReconnaitConstanteMessage ((vect_ul+NUl), &bExisteAdresse, &dwIdConstante))
					{
					// non => erreur
					ExcepErreurCompile (IDSERR_L_ADRESSE_EST_INVALIDE);
					}
				}
			else
				{
				// non = > logique ?
				if (un_log)
					{
					// oui => ni num�rique ni message => non autoris�
					ExcepErreurCompile (IDSERR_L_ADRESSE_EST_INVALIDE);
					}
				}
			// Prise en compte adresse OPC (par d�faut sans extension)
			StrCopy (pSpecItem->szAdresseItem, (vect_ul+NUl)->show);
			StrSetNull (pSpecItem->szAdresseItemExt1);
			StrSetNull (pSpecItem->szAdresseItemExt2);

			// Version 6.0.6 : traite 2 constantes messages optionelles = extensions possibles � l'adresse Item
			if (bReconnaitConstanteMessage ((vect_ul+NUl+1), &bExisteAdresse, &dwIdConstante))
				{
				// Prise en compte extension 1 adresse OPC
				StrCopy (pSpecItem->szAdresseItemExt1, (vect_ul+NUl+1)->show);
				ULSuivante (&NUl,nb_ul);
				if (bReconnaitConstanteMessage ((vect_ul+NUl+1), &bExisteAdresse, &dwIdConstante))
					{
					// Prise en compte extension 2 adresse OPC
					StrCopy (pSpecItem->szAdresseItemExt2, (vect_ul+NUl+1)->show);
					ULSuivante (&NUl,nb_ul);
					}
				}
			}


		// * Variable logique d'Activation / rafraichissement d�ja d�clar�e
		ULSuivante (&NUl,nb_ul);
		if (!bReconnaitESExistanteDeType((vect_ul+NUl), &pSpecItem->dwIdESRaf, c_res_logique))
			ExcepErreurCompile (IDSERR_VARIABLE_DE_RAFRAICHISSEMENT_DE_TYPE_INVALIDE_OU_NON_DECLAREE);

		// * Qualit� / statut
		ULSuivante (&NUl,nb_ul);
		if (!bReconnaitESExistanteDeType((vect_ul+NUl), &pSpecItem->dwIdESStatut, c_res_numerique))
			ExcepErreurCompile (IDSERR_LA_VARIABLE_DE_STATUT_EST_INVALIDE);

		// par d�faut : pas de type sp�cifi�, pas de constante de valeur initiale
		pSpecItem->dwIdTypeVar = libre;

		BOOL bValeurInitialeLue = FALSE;
		// * [Type] mot r�serv�
		if (bULSuivante (&NUl,nb_ul))
			{
			bValeurInitialeLue = TRUE;

			ID_MOT_RESERVE idLu = libre;
			if (bReconnaitMotReserve ((vect_ul+NUl)->show, &idLu) && 
				(idLu != c_res_un) && (idLu != c_res_zero))
				{
				if (
					(idLu != c_res_T_BOOL) &&
					(idLu != c_res_T_ERR) &&
					(idLu != c_res_T_W1) &&
					(idLu != c_res_T_I2) &&
					(idLu != c_res_T_UI2) &&
					(idLu != c_res_T_I4) &&
					(idLu != c_res_T_R4) &&
					(idLu != c_res_T_R8) &&
					(idLu != c_res_T_STR)
					)
					ExcepErreurCompile (IDSERR_LE_TYPE_DE_LA_VARIABLE_EST_INVALIDE);
				pSpecItem->dwIdTypeVar = idLu;
				bValeurInitialeLue = FALSE;
				}

			// * [Valeur initiale]
			if (!bValeurInitialeLue)
				{
				if (bULSuivante (&NUl,nb_ul))
					bValeurInitialeLue = TRUE;
				}

			if (bValeurInitialeLue)
				{
				if (bReconnaitConstante ((vect_ul+NUl), &booleen, &reel, &un_log, &un_num, 
					&pConstanteInit->bConstanteExiste, &pConstanteInit->dwPosConstante))
					{
					pConstanteInit->nULConstante = NUl;
					if (un_log)
						pVarESReconnue->es.genre = c_res_logique;
					else
						{
						if (un_num)
							pVarESReconnue->es.genre = c_res_numerique;
						else
							pVarESReconnue->es.genre = c_res_message;
						}
					}
				else
					ExcepErreurCompile (IDSERR_LA_VALEUR_INITIALE_EST_INVALIDE);
				}
			}

		// Si la variable est nouvelle, alors elle doit contenir son initialisation
		BOOL bVariableNouvelle = pVarESReconnue->dwPosESExistante == 0;
		if (bVariableNouvelle && !bValeurInitialeLue)
			ExcepErreurCompile (IDSERR_LA_VALEUR_INITIALE_EST_INVALIDE);

		// Si la variable n'est pas initialis�e alors le groupe doit �tre en sortie
		if ((!bValeurInitialeLue) && (ContexteOPC.sens != c_res_s))
			ExcepErreurCompile (IDSERR_LA_VALEUR_INITIALE_EST_INVALIDE);

		// Fin analyse
		ULsTerminees (NUl,nb_ul);

		// Si la variable existe d�ja et qu'on est en insertion de ligne, alors elle ne doit pas �tre initialis�e
		if ((!bVariableNouvelle) && (Requete == INSERE_LIGNE) && (bValeurInitialeLue))
			ExcepErreurCompile (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);

		// Ligne d'item Ok : prends en compte les r�sultats
    (*profondeur) = 3;
    nbr_imbrication = 2;
    (*numero_repere) = b_opc_item;
    } // if (bReconnaitESValide ((vect_ul+0), &bESExiste, pdwPositionIdent))
	else
    {
		// non => le premier mot n'est pas un identificateur : Mot r�serv� ?
    if (bReconnaitMotReserve ((vect_ul+NUl)->show, &dwNMotReserve))
      {
			// premier mot de la ligne = mot r�serv�
			ULSuivante (&NUl,nb_ul);
      switch (dwNMotReserve)
        {
				case c_res_serveur: 
					{
					// * mot r�serv� SERVEUR
					// * Nom du serveur = constante message
					DWORD dwTemp;
		      if (!bReconnaitConstanteMessage ((vect_ul+NUl), &bExisteAdresse, &dwTemp))
						{
						ExcepErreurCompile (IDSERR_L_ADRESSE_EST_INVALIDE);
						}
					StrCopy (pspec_titre_paragraf->Serveur.szNomServeur, (vect_ul+NUl)->show);
					pspec_titre_paragraf->Serveur.dwIdESExecution = 0;
					
					// * Variable statut num�rique d�ja d�clar�
					ULSuivante (&NUl,nb_ul);
					if (!bReconnaitESExistante((vect_ul+NUl), &position_es) || !bESEstNumerique(position_es))
						ExcepErreurCompile (IDSERR_LA_VARIABLE_DE_STATUT_EST_INVALIDE);
					pspec_titre_paragraf->Serveur.dwIdESStatut = position_es;

					// * [optionnel] INPROC, LOCAL ou REMOTE
					pspec_titre_paragraf->Serveur.dwIdAcces = libre;
					StrSetNull(pspec_titre_paragraf->Serveur.szNomPoste);
					BOOL bExisteVariableExecution = FALSE;
					if (bULSuivante (&NUl,nb_ul))
						{
						if (bReconnaitMotReserve ((vect_ul+NUl)->show, &dwNMotReserve))
							{
							if ((dwNMotReserve != c_res_opc_inproc)&&(dwNMotReserve != c_res_opc_local)&&(dwNMotReserve != c_res_opc_remote))
								ExcepErreurCompile (IDSERR_LE_TYPE_EST_INVALIDE);
							else
								{
								pspec_titre_paragraf->Serveur.dwIdAcces = dwNMotReserve;

								// * si REMOTE : nom du poste REMOTE
								if (dwNMotReserve == c_res_opc_remote)
									{
									ULSuivante (&NUl,nb_ul);
									BOOL bExiste;
									DWORD dwPos;
									if (!bReconnaitConstanteMessage ((vect_ul+NUl), &bExiste, &dwPos))
										{
										ExcepErreurCompile (IDSERR_NOM_DE_POSTE_INVALIDE);
										}
									StrCopy (pspec_titre_paragraf->Serveur.szNomPoste, (vect_ul+NUl)->show);
									}
								if (bULSuivante (&NUl,nb_ul))
									bExisteVariableExecution = TRUE;
								}
							}
						else
							{
							bExisteVariableExecution = TRUE;
							}
						}

					// * [optionnel] Var de commande d'ex�cution du client
					if (bExisteVariableExecution)
						{
						if (!bReconnaitESExistante((vect_ul+NUl), &position_es) || !bESEstLogique(position_es))
							ExcepErreurCompile (IDSERR_VARIABLE_DE_RAFRAICHISSEMENT_DE_TYPE_INVALIDE_OU_NON_DECLAREE);
						pspec_titre_paragraf->Serveur.dwIdESExecution = position_es;
						}

					// Fin analyse
					ULsTerminees (NUl,nb_ul);

					// Ligne Ok : prends en compte les r�sultats
          StrCopy (ContexteOPC.szNomServeur, pspec_titre_paragraf->Serveur.szNomServeur);
					ContexteOPC.genre_donnees = c_res_serveur;
					ContexteOPC.sens = libre;

          (*profondeur) = 1;
          nbr_imbrication = 0;
          (*numero_repere) = b_titre_paragraf_opc;
          (*tipe_paragraf) = PARAGRAPHE_SERVEUR_OPC;
					} // c_res_serveur
          break;

				case c_res_groupe:
					{
					// 1)  mot r�serv� GROUPE
					// Cette ligne est d�clar�e hors d'un serveur ?
					if (ContexteOPC.genre_donnees == libre)
						{
						ExcepErreurCompile (IDSERR_L_INSERTION_D_UN_PARAGRAPHE_EST_IMPOSSIBLE_A_CETTE_POSITION);
						}

					// 2) Nom du groupe = constante message
					BOOL bExiste;
					DWORD dwPos;
		      if (!bReconnaitConstanteMessage ((vect_ul+NUl), &bExiste, &dwPos))
						{
						ExcepErreurCompile (IDSERR_L_ADRESSE_EST_INVALIDE);
						}
					StrCopy (pspec_titre_paragraf->Groupe.szNomGroupe, (vect_ul+NUl)->show);

					// 3) Variable logique de rafraichissement d�ja d�clar�
					ULSuivante (&NUl,nb_ul);
					if (!bReconnaitESExistante((vect_ul+NUl), &position_es))
						ExcepErreurCompile (IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_A_PAS_ETE_DECLAREE);
					if (!bESEstLogique(position_es))
						ExcepErreurCompile (IDSERR_LA_VARIABLE_DE_RAFRAICHISSEMENT_N_EST_PAS_DE_TYPE_LOGIQUE);
					pspec_titre_paragraf->Groupe.dwIdESRaf = position_es;

					// 4) Variable statut num�rique d�ja d�clar�
					ULSuivante (&NUl,nb_ul);
					if (!bReconnaitESExistanteDeType((vect_ul+NUl), &pspec_titre_paragraf->Groupe.dwIdESStatut, c_res_numerique))
						ExcepErreurCompile (IDSERR_LA_VARIABLE_DE_STATUT_EST_INVALIDE);

					// 5) ENTREE_DIRECTE, ENTREE_CACHE ou SORTIE_DIRECTE
					ULSuivante (&NUl,nb_ul);
					dwNMotReserve = libre;

					if (!bReconnaitMotReserve ((vect_ul+NUl)->show, &dwNMotReserve) || 
						((dwNMotReserve != c_res_opc_entree_directe)&&(dwNMotReserve != c_res_opc_entree_cache)&&(dwNMotReserve != c_res_opc_sortie_directe)))
						ExcepErreurCompile (IDSERR_LE_TYPE_EST_INVALIDE);

					pspec_titre_paragraf->Groupe.dwIdOperation = dwNMotReserve;
					if ((dwNMotReserve != c_res_opc_entree_directe)&&(dwNMotReserve != c_res_opc_entree_cache))
						pspec_titre_paragraf->Groupe.IdSens = c_res_s;
					else
						pspec_titre_paragraf->Groupe.IdSens = c_res_e;

					// 6) [si ENTREE_CACHE] : constante numerique Cadence en millisecondes
					pspec_titre_paragraf->Groupe.dwCadenceMS = 0; // par d�faut
					if (dwNMotReserve == c_res_opc_entree_cache)
						{
						ULSuivante (&NUl,nb_ul);

						if ((!bReconnaitConstanteNum ((vect_ul+NUl), &booleen, &reel, &mot)) ||
              (reel < 0) || (reel > MAXDWORD))
							{
							ExcepErreurCompile (IDSERR_VALEUR_NUMERIQUE_INCORRECTE);
							}
						pspec_titre_paragraf->Groupe.dwCadenceMS = (DWORD)reel;
						}

					// Fin analyse
					ULsTerminees (NUl,nb_ul);

					// Ligne de groupe Ok : prends en compte les r�sultats
          StrCopy (ContexteOPC.szNomGroupe, pspec_titre_paragraf->Groupe.szNomGroupe);
					ContexteOPC.genre_donnees = c_res_groupe;
					ContexteOPC.sens = pspec_titre_paragraf->Groupe.IdSens;

          (*profondeur) = 2;
          nbr_imbrication = 1;
          (*numero_repere) = b_titre_paragraf_opc;
          (*tipe_paragraf) = PARAGRAPHE_GROUPE_OPC;
					}
					break; // c_res_groupe

				default:
          ExcepErreurCompile (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
          break;
        } //switch (dwNMotReserve)
			
			DWORD dwErreurValideInsereLigneParagraphe = 0;

			// Valide l'insertion de ligne paragraphe
			if (!bValideInsereLigneParagraphe (b_titre_paragraf_opc,ligne,
				(*profondeur),nbr_imbrication, &dwErreurValideInsereLigneParagraphe) )
				{
				ContexteOPC = ContexteOPCAncien;
				ExcepErreurCompile(dwErreurValideInsereLigneParagraphe);
				}
      } // if (bReconnaitMotReserve ((vect_ul+NUl)->show, &dwNMotReserve))
		else
      {
			// ni ES ni mot r�serv� : constante message (= commentaire) ?
      if (bReconnaitConstanteMessage ((vect_ul+NUl), &pConstanteInit->bConstanteExiste, &pConstanteInit->dwPosConstante))
				(*numero_repere) = b_pt_constant;
			else
        {
        ExcepErreurCompile(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
        }
      }
    } //else (bReconnaitESValide ((
  } // AnalyseLigneOPC

//-------------------------------------------------------------
// valide ligne
//-------------------------------------------------------------
static void valide_ligne_opc (REQUETE_EDIT Requete, DWORD ligne,	PUL vect_ul, int *nb_ul,
	BOOL *supprime_ligne, BOOL *accepte_ds_bd, DWORD *erreur_val, int *niveau, int *indentation)
  {
  DWORD erreur = 0;
  BOOL un_paragraphe;
	ITEM_OPC_CF OpcItem;
  DWORD numero_repere;
  DWORD pos_paragraf;
  DWORD profondeur;
  ID_PARAGRAPHE tipe_paragraf;
  SPEC_TITRE_PARAGRAPHE_OPC spec_titre_paragraf;
  PTITRE_PARAGRAPHE pTitreParagraphe;
  GEO_OPC GeoOPC;
	
  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;
  switch (Requete)
    {
    case MODIFIE_LIGNE:
			{
			VARIABLE_RECONNUE_CF VarESReconnue;
			CONSTANTE_RECONNUE_CF ConstanteInit = CONSTANTE_RECONNUE_CF_NULLE;
			// r�cup�re le contexte de la ligne courante
      consulte_titre_paragraf_opc (ligne);

			// analyse la nouvelle ligne
			__try
				{
				AnalyseLigneOPC (MODIFIE_LIGNE, vect_ul, (*nb_ul), ligne, &profondeur, &tipe_paragraf, &numero_repere,
					&VarESReconnue,	&ConstanteInit, &spec_titre_paragraf, &OpcItem);
				}
			CATCH_ERREUR(erreur)

			// nouvelle ligne Ok ?
      if (erreur == 0)
        {
				// oui => on modifie la ligne
        PGEO_OPC	pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc,ligne);
				GeoOPC = (*pGeoOPC);

				// m�me type de ligne ?
				if (GeoOPC.numero_repere == numero_repere)
					{
					// oui => traitement selon le m�me type de ligne
					switch (numero_repere)
            {
						case b_pt_constant : // modifie commentaire
							{
							modifie_cst_bd_c (vect_ul[0].show, &(GeoOPC.pos_donnees));
							}
							break; //b_pt_constant
							
            case b_titre_paragraf_opc: // modifie paragraphe
							{
							// m�me type de paragraphe ?
              pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_opc,GeoOPC.pos_paragraf);
							if ((pTitreParagraphe->IdParagraphe == tipe_paragraf) && 
								((tipe_paragraf == PARAGRAPHE_SERVEUR_OPC) || (tipe_paragraf == PARAGRAPHE_GROUPE_OPC)))
                {
								// oui => mise � jour des valeurs
                PSPEC_TITRE_PARAGRAPHE_OPC	pSpecTitreParagraphe = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc,GeoOPC.pos_specif_paragraf);

								// Enregistre les donn�es de paragraphe
								switch (tipe_paragraf)
									{
									case PARAGRAPHE_SERVEUR_OPC:
										{
										ReferenceES (spec_titre_paragraf.Serveur.dwIdESStatut);
										DeReferenceES (pSpecTitreParagraphe->Serveur.dwIdESStatut);

										if (spec_titre_paragraf.Serveur.dwIdESExecution)
											ReferenceES (spec_titre_paragraf.Serveur.dwIdESExecution);
										if (pSpecTitreParagraphe->Serveur.dwIdESExecution)
											DeReferenceES (pSpecTitreParagraphe->Serveur.dwIdESExecution);
										(*pSpecTitreParagraphe) = spec_titre_paragraf;
										}
										break;
									case PARAGRAPHE_GROUPE_OPC:
										{
										// m�me sens (entr�e ou sorties)
										if (spec_titre_paragraf.Groupe.IdSens == pSpecTitreParagraphe->Groupe.IdSens)
											{
											ReferenceES (spec_titre_paragraf.Groupe.dwIdESRaf);
											ReferenceES (spec_titre_paragraf.Groupe.dwIdESStatut);
											DeReferenceES (pSpecTitreParagraphe->Groupe.dwIdESRaf);
											DeReferenceES (pSpecTitreParagraphe->Groupe.dwIdESStatut);
											(*pSpecTitreParagraphe) = spec_titre_paragraf;
											}
										else
											// $$OPC possible si pas d'item d�clar�s
											erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
										}
										break;
									} // switch (tipe_paragraf)
                } // m�me type de paragraphe
							else
                {
                erreur = IDSERR_LA_MODIFICATION_DU_NOM_DU_PARAGRAPHE_EST_IMPOSSIBLE;
								} //modif nom paragraf impossible
							}
              break; //b_titre_paragraf_opc
							
            case b_opc_item: // Modifie item opc
							{
							// Modification : uniquement de item � item
							if (GeoOPC.numero_repere != b_opc_item)
		            erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
							else
								{
								// pointe sur l'ancienne valeur de la ligne
		            PITEM_OPC_CF pOpcItem = (PITEM_OPC_CF)pointe_enr (szVERIFSource, __LINE__, b_opc_item, GeoOPC.pos_specif_es);

								// Mise � jour de la variable Ok ?

								// D�claration de variable avant et apr�s ?
								if (ConstanteInit.bConstanteExiste && (pGeoOPC->pos_initial != 0))
									{
									// oui => mise � jour de la variable Ok ?
									if (VarESReconnue.es.taille > 0)
										{
										// Tableau => mise � jour du sens
										switch (ContexteOPC.sens)
											{
											case c_res_e:
												VarESReconnue.es.sens = c_res_variable_ta_e;
												break;
											case c_res_s:
												VarESReconnue.es.sens = c_res_variable_ta_s;
												break;
											default:
												break;
											}
										}
									else
										{
										VarESReconnue.es.sens = ContexteOPC.sens;
										}
									VarESReconnue.es.n_desc = d_opc;
									if (bModifieDeclarationES (&VarESReconnue.es, vect_ul[VarESReconnue.nULVariable].show, GeoOPC.dwIdES))
										{
										modifie_cst_bd_c (vect_ul[ConstanteInit.nULConstante].show, &(GeoOPC.pos_initial));
										}
									else
										erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
									}
								else
									{
									// Reflet avant et apr�s ?
									if ((!ConstanteInit.bConstanteExiste) && (pGeoOPC->pos_initial == 0))
										{
										// oui => change les r�f�rences
										ReferenceES (VarESReconnue.dwPosESExistante);
										DeReferenceES (GeoOPC.dwIdES);
										pGeoOPC->dwIdES = VarESReconnue.dwPosESExistante;
										}
									else
										{
										// non => modification interdite $$ en attendant
										erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_VARIABLE_EST_IMPOSSIBLE;
										}
									}
								// pas d'erreur ?
								if (erreur == 0)
									{
									// d�r�f�rence les anciennes variables et r�f�rence les nouvelles
									ReferenceES (OpcItem.dwIdESStatut);
									ReferenceES (OpcItem.dwIdESRaf);
									DeReferenceES (pOpcItem->dwIdESStatut);
									DeReferenceES (pOpcItem->dwIdESRaf);
									*pOpcItem = OpcItem;
									}
								}
							}
							break; // b_opc_item
							
						} //switch numero repere
					} //meme repere : m�me type de ligne
				else
					{
					erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
					} //modif du type de la ligne impossible
				} // valide vect_ul Ok

			if (erreur != 0)
				{
				genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
				(*erreur_val) = erreur;
				}
			else
				{
			  PGEO_OPC	pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc, ligne);
				(*pGeoOPC) = GeoOPC;
				}
			(*accepte_ds_bd) = TRUE;
			(*supprime_ligne)=FALSE;
			}
			break; // case MODIFIE_LIGNE:
				
    case INSERE_LIGNE:
			{
			VARIABLE_RECONNUE_CF VarESReconnue;
			CONSTANTE_RECONNUE_CF ConstanteInit = CONSTANTE_RECONNUE_CF_NULLE;
			// r�cup�re le contexte de la ligne pr�cedente
      consulte_titre_paragraf_opc (ligne - 1);

			// analyse la nouvelle ligne
			__try
				{
				AnalyseLigneOPC(INSERE_LIGNE, vect_ul, (*nb_ul), ligne, &profondeur, &tipe_paragraf, &numero_repere,
					&VarESReconnue, &ConstanteInit, &spec_titre_paragraf,	&OpcItem);
				}
			CATCH_ERREUR(erreur)

			// nouvelle ligne Ok ?
      if (erreur == 0) //
        {
				// oui => on ins�re la ligne
				pos_paragraf = 0; // init pour mise a jour donnees paragraf
				GeoOPC.numero_repere = numero_repere;
				switch (numero_repere)
          {
					case b_pt_constant : // Ins�re commentaire
						{
						AjouteConstanteBd (ConstanteInit.dwPosConstante, ConstanteInit.bConstanteExiste, vect_ul[0].show,	&(GeoOPC.pos_donnees));
						(*accepte_ds_bd) = TRUE;
						(*supprime_ligne) = FALSE;
						}
						break;
						
          case b_titre_paragraf_opc : // Ins�re paragraphe
						{
						// Ajoute le titre de paragraphe
            InsereTitreParagraphe (b_titre_paragraf_opc, ligne, profondeur, tipe_paragraf, &(GeoOPC.pos_paragraf));
						pos_paragraf = GeoOPC.pos_paragraf;
            if ((tipe_paragraf == PARAGRAPHE_SERVEUR_OPC)||(tipe_paragraf == PARAGRAPHE_GROUPE_OPC))
              {
							// Cr�e � la vol�e le bloc des donn�es sp�cifiques de paragraphe
              if (!existe_repere (b_spec_titre_paragraf_opc))
                {
                GeoOPC.pos_specif_paragraf = 1;
                cree_bloc (b_spec_titre_paragraf_opc,sizeof (spec_titre_paragraf),0);
                }
              else
                {
                GeoOPC.pos_specif_paragraf = nb_enregistrements (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc) + 1;
                }
							// Ajoute un enr sp�cifique paragraphe
              insere_enr (szVERIFSource, __LINE__, 1,b_spec_titre_paragraf_opc,GeoOPC.pos_specif_paragraf);
              PSPEC_TITRE_PARAGRAPHE_OPC	pSpecTitreParagraphe = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc,GeoOPC.pos_specif_paragraf);

							// Enregistre les donn�es de paragraphe
							switch (tipe_paragraf)
								{
								case PARAGRAPHE_SERVEUR_OPC:
									{
									ReferenceES (spec_titre_paragraf.Serveur.dwIdESStatut);
									if (spec_titre_paragraf.Serveur.dwIdESExecution)
										ReferenceES (spec_titre_paragraf.Serveur.dwIdESExecution);
									}
									break;
								case PARAGRAPHE_GROUPE_OPC:
									{
									ReferenceES (spec_titre_paragraf.Groupe.dwIdESRaf);
									ReferenceES (spec_titre_paragraf.Groupe.dwIdESStatut);
									}
									break;
								}
              (*pSpecTitreParagraphe) = spec_titre_paragraf;
              }
						(*accepte_ds_bd) = TRUE;
						(*supprime_ligne)= FALSE;
						}
            break;  //b_titre_paragraf_opc

					case b_opc_item : // Ins�re Item OPC
						{
            if (!existe_repere (b_opc_item))
							{
              cree_bloc (b_opc_item,sizeof (ITEM_OPC_CF),0);
							}
            GeoOPC.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_opc_item) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_opc_item, GeoOPC.pos_specif_es);
            PITEM_OPC_CF pOpcItem = (PITEM_OPC_CF)pointe_enr (szVERIFSource, __LINE__, b_opc_item, GeoOPC.pos_specif_es);
						*pOpcItem = OpcItem;

						/// Variable d�ja d�clar�e ?
						if (VarESReconnue.dwPosESExistante)
							{
							// oui => r�f�rence la
							GeoOPC.dwIdES = VarESReconnue.dwPosESExistante;
							ReferenceES (GeoOPC.dwIdES);
							// pas de constante d'initialisation
							GeoOPC.pos_initial = 0;
							}
						else
							{
							// non => d�clare la
							if (VarESReconnue.es.taille > 0)
								{
								// Tableau => mise � jour du sens
								switch (ContexteOPC.sens)
									{
									case c_res_e:
										VarESReconnue.es.sens = c_res_variable_ta_e;
										break;
									case c_res_s:
										VarESReconnue.es.sens = c_res_variable_ta_s;
										break;
									default:
										break;
									}
								}
							else
								{
								VarESReconnue.es.sens = ContexteOPC.sens;
								}
							VarESReconnue.es.n_desc = d_opc;
							InsereVarES (&VarESReconnue.es, vect_ul[VarESReconnue.nULVariable].show, &(GeoOPC.dwIdES));

							// d�claration => il existe une constante d'initialisation
							AjouteConstanteBd (ConstanteInit.dwPosConstante, ConstanteInit.bConstanteExiste, vect_ul[ConstanteInit.nULConstante].show, &GeoOPC.pos_initial);
							(*accepte_ds_bd) = TRUE;
							(*supprime_ligne)= FALSE;
							}

						ReferenceES (OpcItem.dwIdESRaf);
						ReferenceES (OpcItem.dwIdESStatut);
						}
						break;
						
					default:
						VerifWarningExit;
            break;
					} //switch (numero_repere

				// Enregistre le nouveau GeoOPC
				if (!existe_repere (b_geo_opc))
					{
					cree_bloc (b_geo_opc,sizeof (GeoOPC),0);
					}
				insere_enr (szVERIFSource, __LINE__, 1,b_geo_opc,ligne);
				PGEO_OPC	pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc,ligne);
				(*pGeoOPC) = GeoOPC;
				un_paragraphe = (BOOL)(numero_repere == b_titre_paragraf_opc);
				MajDonneesParagraphe (b_titre_paragraf_opc, ligne, INSERE_LIGNE,
					un_paragraphe, profondeur, pos_paragraf);
        }//vect_ul Ok
			else
				{
				(*accepte_ds_bd) = FALSE;
				(*erreur_val) = erreur;
				}
			}
			break; // case INSERE_LIGNE
				
    case SUPPRIME_LIGNE:
			{
      consulte_titre_paragraf_opc (ligne);
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      PGEO_OPC	pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc,ligne);
      GeoOPC = (*pGeoOPC);
      switch (GeoOPC.numero_repere)
        {
				case b_pt_constant :
					{
					supprime_cst_bd_c (GeoOPC.pos_donnees);
					}
          break;
					
        case b_titre_paragraf_opc : // suppression paragraphe OPC
					{
					un_paragraphe = TRUE;
          pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_opc, GeoOPC.pos_paragraf);
					PSPEC_TITRE_PARAGRAPHE_OPC	pSpecTitreParagraphe = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc,GeoOPC.pos_specif_paragraf);
					tipe_paragraf = pTitreParagraphe->IdParagraphe;
					profondeur = pTitreParagraphe->profondeur;
          if (!bSupprimeTitreParagraphe (b_titre_paragraf_opc,GeoOPC.pos_paragraf) )
						{
						(*accepte_ds_bd) = TRUE;
						(*supprime_ligne) = FALSE;
            (*erreur_val) = IDSERR_SUPPRESSION_DU_PARAGRAPHE_IMPOSSIBLE_A_CETTE_POSITION;
						}
					else
						{
            maj_pointeur_dans_geo (b_titre_paragraf_opc,GeoOPC.pos_paragraf,FALSE);
            if (tipe_paragraf == PARAGRAPHE_SERVEUR_OPC)
              {
							// Enregistre les donn�es de paragraphe
							switch (tipe_paragraf)
								{
								case PARAGRAPHE_SERVEUR_OPC:
									DeReferenceES (pSpecTitreParagraphe->Serveur.dwIdESStatut);
									if (pSpecTitreParagraphe->Serveur.dwIdESExecution)
										DeReferenceES (pSpecTitreParagraphe->Serveur.dwIdESExecution);
									break;
								case PARAGRAPHE_GROUPE_OPC:
									{
									DeReferenceES (pSpecTitreParagraphe->Groupe.dwIdESRaf);
									DeReferenceES (pSpecTitreParagraphe->Groupe.dwIdESStatut);
									}
									break;
								} // switch (tipe_paragraf)

              enleve_enr (szVERIFSource, __LINE__, 1,b_spec_titre_paragraf_opc,GeoOPC.pos_specif_paragraf);
              maj_pointeur_dans_geo (GeoOPC.numero_repere,GeoOPC.pos_specif_paragraf,TRUE);
              }
						} //suppression possible
					}
          break; //b_titre_paragraf_opc

				case b_opc_item : // Supprime Item OPC
					{
					// pointe sur l'ancienne valeur de la ligne
		      PITEM_OPC_CF pOpcItem = (PITEM_OPC_CF)pointe_enr (szVERIFSource, __LINE__, b_opc_item, GeoOPC.pos_specif_es);

					// Suppresion d'une variable d�clar�e ?
					if (pGeoOPC->pos_initial != 0)
						{
						// oui => non essaye de supprimer la d�claration de la variable
						if (bSupprimeES (GeoOPC.dwIdES))
							{
	            supprime_cst_bd_c (GeoOPC.pos_initial);
							}
						else
							{
							(*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
							(*accepte_ds_bd) = TRUE;
							(*supprime_ligne) = FALSE;
							}
						}
					else
						{
						// d�r�f�rence simplement la variable
						DeReferenceES (GeoOPC.dwIdES);
						}

					// pas d'erreur ?
					if (*supprime_ligne)
						{
						// D�r�f�rence les autres variables...
						DeReferenceES (pOpcItem->dwIdESStatut);
						DeReferenceES (pOpcItem->dwIdESRaf);

						// et supprime les infos sp�cifiques
            enleve_enr (szVERIFSource, __LINE__, 1,GeoOPC.numero_repere,GeoOPC.pos_specif_es);
            maj_pointeur_dans_geo (GeoOPC.numero_repere,GeoOPC.pos_specif_es,TRUE);
						}
					}
        } //switch GeoOPC.numero_repere

			if ((*supprime_ligne))
        {
        MajDonneesParagraphe (b_titre_paragraf_opc,ligne,SUPPRIME_LIGNE,
					un_paragraphe,profondeur, 0); // pos_paragraf
        enleve_enr (szVERIFSource, __LINE__, 1,b_geo_opc,ligne);
        }
			}
      break;
			
		case CONSULTE_LIGNE:
			genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
			break;
    } // switch Requete
  } // valide_ligne

// ---------------------------------------------------------------------------
static void creer_opc (void)
  {
  bOpcDejaInitialise = FALSE;
  }

// ---------------------------------------------------------------------------
// Initialise ce descripteur
static void init_opc (DWORD *pdwDepart, DWORD * pdwCourant, char *nom_applic)
  {
  if (!bOpcDejaInitialise)
    {
    ligne_depart_opc = 1;
    ligne_courante_opc = 1;
    bOpcDejaInitialise = TRUE;
    }
  *pdwDepart = ligne_depart_opc;
  *pdwCourant = ligne_courante_opc;
  } // init_opc

// ---------------------------------------------------------------------------
// Renvoie le nombre total de lignes �dit�es dans ce descripteur
static DWORD lis_nombre_de_ligne_opc (void)
  {
  DWORD dwRes = 0;

  if (existe_repere (b_geo_opc))
    dwRes = nb_enregistrements (szVERIFSource, __LINE__, b_geo_opc);

  return dwRes;
  } // lis_nombre_de_ligne_opc

//---------------------------------------------------------------------------
// Edition : Cr�e une chaine "tableau de bord" d'�dition
static void set_tdb_opc (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char mot_res[c_nb_car_message_res];

	// Initialisation : chaine tableau de bord vide
  StrSetNull (tdb);
  if ((existe_repere (b_titre_paragraf_opc)) && (nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf_opc) != 0))
    {
    if (nouvelle_ligne)
      {
      ligne--;
      }
		// Appel de la mise � jour du contexte OPC
    consulte_titre_paragraf_opc (ligne);

		// Description du contexte : dans serveur ou groupe ou variable ?
    if (ContexteOPC.genre_donnees != libre)
      {
			// oui => indique le serveur
      bMotReserve (c_res_serveur, mot_res);
      StrConcat (tdb, mot_res);
      StrConcatChar (tdb, ' ');
      StrConcat (tdb, ContexteOPC.szNomServeur);

			// dans un groupe ou une variable ?
      if (ContexteOPC.sens != libre)
        {
				// oui => ajoute le groupe et son sens
        StrConcatChar (tdb, ' ');
				bMotReserve (c_res_groupe, mot_res);
				StrConcat (tdb, mot_res);
				StrConcatChar (tdb, ' ');
				StrConcat (tdb, ContexteOPC.szNomGroupe);
				StrConcatChar (tdb, ' ');
        bMotReserve (ContexteOPC.sens, mot_res);
        StrConcat (tdb, mot_res);
        } // if (ContexteOPC.sens != libre)
      } // if (ContexteOPC.genre_donnees != libre)
    }
  } // set_tdb_opc

// ---------------------------------------------------------------------------
static void fin_opc (BOOL quitte, DWORD courant)
  {
  if (!quitte)
    {
    ligne_courante_opc = courant;
    }
  }

// ---------------------------------------------------------------------------
// Interface du descripteur OPC
void LisDescripteurOPC (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_opc;
	pDescripteurCf->pfnInitDesc = init_opc;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_opc;
	pDescripteurCf->pfnValideLigne = valide_ligne_opc;
	pDescripteurCf->pfnSetTdb = set_tdb_opc;
	pDescripteurCf->pfnFin = fin_opc;
	}

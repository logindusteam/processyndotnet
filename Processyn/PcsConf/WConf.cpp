// -----------------------------------------------------------------------
// WConf.c
// Gestion de la fen�tre principale de PcsConf
// -----------------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "appli.h"
#include "couleurs.h"
#include "UStr.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "MemMan.h"
#include "Threads.h" // type de lancement
#include "ProgMan.h" // type de lancement
#include "FMan.h" // type de lancement
#include "initdesc.h"
#include "PathMan.h"          
#include "DocMan.h"          
#include "actioncf.h"
#include "ppcf.h"
#include "USignale.h"
#include "pmnucf.h"
#include "WPage.h"
#include "WStatut.h"
#include "WBarOuti.h"
#include "WFondBar.h"
#include "IdConfLng.h"
#include "IdConf.h"
#include "pcsdlg.h"
#include "pcsdlgcf.h"
#include "pcsfont.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "WAide.h"
#include "Verif.h"
#include "MenuMan.h"
#include "VersMan.h"
#include "RegMan.h"
#include "WConf.h"

VerifInit;

// -------------------- Variables Globales et locales au module ----------
// enregistrer les fen�tres cr��es
typedef struct
  {
  HWND	  hwnd_sous_fen;       // HWND sous-fenetre
  DWORD	  type_sous_fen;       // type sous-fen: in_txt, in_ligne, bouton...
  DWORD	  pos_x_sous_fen;      // Position en % par rapport � la fen client
  DWORD	  pos_y_sous_fen;      // de pcs_conf (origine au SUD-OUEST)
  DWORD	  taille_x_sous_fen;   // Taille en %
  DWORD   taille_y_sous_fen;
  DWORD   zone_sous_fen;      // Haut, Bas, Gauche, Droite, Milieu
  BOOL en_selection;
  }
  SOUS_FENETRE;

#define NB_SOUS_FENETRES_MAX  16
static  SOUS_FENETRE  aSousFenetres [NB_SOUS_FENETRES_MAX];

// ------------------ Structure pour enregistrer les zones cr��es
#define NB_ZONES 5
typedef struct
  {
  DWORD ordre_zone;
  DWORD taille_zone;
  }
  ZONE_FENETRE;

static ZONE_FENETRE aZonesFenetres [NB_ZONES];

static	PCSTR	pszClasseWConf	= "ClasseWConf";
static  BOOL  BasculeItemInterprete = TRUE;

#define WM_FIN_SATELLITE (WM_USER+2)
#define SIZE_STACK_THREAD_SATELLITE 4096

typedef struct
  {
  BOOL bExecutionSynchrone;
  char nom [255];
  char param [255];
  char env [255];
  char chemin [255];
  } PARAMS_SATELLITE, *PPARAMS_SATELLITE;

static BOOL fin_satellite;
#define POSITION_MENU_DESCRIPTEURS 3

// Chemin des infos utilisateur de l'appli dans la base de registre
#define REGISTRY_PATH_USER_CONFIG "Software\\Logique Industrie\\Processyn6.0\\PcsConf"

// Police de l'�diteur
#define REGISTRY_CLE_FONT_N "FontEditNum"
#define REGISTRY_CLE_FONT_BITS_STYLE "FontEditBitsStyle"
#define REGISTRY_CLE_FONT_TAILLE "FontEditSizePixels"
//#define REGISTRY_CLE_FONT_CHARSET "FontCharSet"
static DWORD dwConfNFont =0;
static DWORD dwConfBitsStyle = 0;
static DWORD dwConfTailleFont = 14;
//Utiliser plutot Appli.byCharSetFont static BYTE	byConfCharSetFont = ANSI_CHARSET;

// -----------------------------------------------------------------------
// R�cup�re les param�tres utilisateur de Processyn dans la base de registre
//------------------------------------------------------------------------
static void LireConfigUtilisateur()
	{
	CRegMan RegMan;
	// Config Police de l'�diteur
	RegMan.bUserLireDWORD(dwConfNFont,REGISTRY_PATH_USER_CONFIG, REGISTRY_CLE_FONT_N, 0);
	RegMan.bUserLireDWORD(dwConfBitsStyle,REGISTRY_PATH_USER_CONFIG, REGISTRY_CLE_FONT_BITS_STYLE, 0);
	RegMan.bUserLireDWORD(dwConfTailleFont,REGISTRY_PATH_USER_CONFIG, REGISTRY_CLE_FONT_TAILLE, 14);
	//DWORD dwTemp = 0;
	//RegMan.bUserLireDWORD(dwTemp,REGISTRY_PATH_USER_CONFIG, REGISTRY_CLE_FONT_CHARSET, ANSI_CHARSET);
	//byConfCharSetFont = (BYTE)dwTemp;
	}

// -----------------------------------------------------------------------
// Ecrit les param�tres utilisateur de Processyn dans la base de registre
//------------------------------------------------------------------------
static void EcrireConfigUtilisateur()
	{
	CRegMan RegMan;
	// Config Police de l'�diteur
	RegMan.bUserEcrireDWORD(dwConfNFont,REGISTRY_PATH_USER_CONFIG, REGISTRY_CLE_FONT_N);
	RegMan.bUserEcrireDWORD(dwConfBitsStyle,REGISTRY_PATH_USER_CONFIG, REGISTRY_CLE_FONT_BITS_STYLE);
	RegMan.bUserEcrireDWORD(dwConfTailleFont,REGISTRY_PATH_USER_CONFIG, REGISTRY_CLE_FONT_TAILLE);
	//RegMan.bUserEcrireDWORD((DWORD)byConfCharSetFont,REGISTRY_PATH_USER_CONFIG, REGISTRY_CLE_FONT_CHARSET);
	}

// -----------------------------------------------------------------------
// Determine l'action � effectuer sur sauvegarde: Soit l'applic existe et on sauve
// Soit on demande un chemin et un nom de sauvegarde
// -----------------------------------------------------------------------
static void OnSauve (void)
	{
	char nom_fic[MAX_PATH];
  t_param_action  param_action;
  char      saisie[MAX_PATH];
  char      titre[30];
  char      action[30];

	pszCreeNameExt (nom_fic, MAX_PATH, pszPathNameDoc(), EXTENSION_SOURCE);
	if (bGetNouveauDoc ())
		{
    bLngLireInformation (c_inf_tdlg_sauve, titre);
    bLngLireInformation (c_inf_action_sauve, action);
		pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
    if (bDlgFichierEnregistrerSous (Appli.hwnd, saisie, titre, "pcs", action))
      {
      StrCopy (param_action.chaine, pszSupprimeExt (saisie));
      bAjouterActionCf (action_sauve_application, &param_action);
      }
		}
	else
		{
    StrCopy (param_action.chaine, pszPathNameDoc());
    bAjouterActionCf (action_sauve_application, &param_action);
		}
	}

// -----------------------------------------------------------------------
// Calcul du Rectangle d'une Zone � partir du rectangle parent
// -----------------------------------------------------------------------
static void Zone_Rectangle (DWORD zone, RECT *rclParent, RECT *rclZone)
  {
  *rclZone	= *rclParent;
  switch (zone)
    {
    case HAUT:
      rclZone->bottom = rclParent->top + aZonesFenetres [HAUT].taille_zone;
			// si zone haute cr�e apr�s zone gauche
      if (aZonesFenetres [HAUT].ordre_zone > aZonesFenetres [GAUCHE].ordre_zone)
        {
				// Position gauche de la zone haute augment�e d�cal�e de la taille de la zone de gauche
        rclZone->left += aZonesFenetres [GAUCHE].taille_zone;
        }
      if (aZonesFenetres [HAUT].ordre_zone > aZonesFenetres [DROITE].ordre_zone)
        {
        rclZone->right -= aZonesFenetres [DROITE].taille_zone;
        }
      break;

    case GAUCHE:
      rclZone->right  = rclParent->left + aZonesFenetres [GAUCHE].taille_zone;
      if (aZonesFenetres [GAUCHE].ordre_zone > aZonesFenetres [BAS].ordre_zone)
        {
        rclZone->bottom -= aZonesFenetres [BAS].taille_zone;
        }
      if (aZonesFenetres [GAUCHE].ordre_zone > aZonesFenetres [HAUT].ordre_zone)
        {
        rclZone->top += aZonesFenetres [HAUT].taille_zone;
        }
      break;

    case BAS:
      rclZone->top  = rclParent->bottom - aZonesFenetres [BAS].taille_zone;
      if (aZonesFenetres [BAS].ordre_zone > aZonesFenetres [DROITE].ordre_zone)
        {
        rclZone->right -= aZonesFenetres [DROITE].taille_zone;
        }
      if (aZonesFenetres [BAS].ordre_zone > aZonesFenetres [GAUCHE].ordre_zone)
        {
        rclZone->left += aZonesFenetres [GAUCHE].taille_zone;
        }
      break;

    case DROITE:
      rclZone->left   = rclParent->right - aZonesFenetres [DROITE].taille_zone;
      if (aZonesFenetres [DROITE].ordre_zone > aZonesFenetres [HAUT].ordre_zone)
        {
        rclZone->top += aZonesFenetres [HAUT].taille_zone;
        }
      if (aZonesFenetres [DROITE].ordre_zone > aZonesFenetres [BAS].ordre_zone)
        {
        rclZone->bottom -= aZonesFenetres [BAS].taille_zone;
        }
      break;

    case MILIEU:
      rclZone->left   += aZonesFenetres [GAUCHE].taille_zone;
      rclZone->bottom -= aZonesFenetres [BAS].taille_zone;
      rclZone->right  -= aZonesFenetres [DROITE].taille_zone;
      rclZone->top    += aZonesFenetres [HAUT].taille_zone;
      break;

    default:
      rclZone->left   = 0L; //$$
      rclZone->bottom = 0L;
      rclZone->right  = 0L;
      rclZone->top    = 0L;
      break;
    }

  return;
  }

// -----------------------------------------------------------------------
// Mapping de points d'un rectangle � un autre rectangle
// -----------------------------------------------------------------------
static void Map_Coordonnees (RECT *rclReference, RECT *rclNewZone,
                             LONG *lPosX, LONG *lPosY, LONG *lSizeX, LONG *lSizeY)
  {
  *lPosX = ((*lPosX * (rclNewZone->left - rclNewZone->right)) / (rclReference->left - rclReference->right)) + rclNewZone->left;
  *lPosY = ((*lPosY * (rclNewZone->bottom - rclNewZone->top)) / (rclReference->bottom - rclReference->top)) + rclNewZone->top;
  *lSizeX = (*lSizeX * (rclNewZone->left - rclNewZone->right)) / (rclReference->left - rclReference->right);
  *lSizeY = (*lSizeY * (rclNewZone->bottom - rclNewZone->top)) / (rclReference->bottom - rclReference->top);
  return;
  }

// ----------------------------------------------------------------------- 
// Traitement du message WM_CREATE
// -----------------------------------------------------------------------
static BOOL OnCreate (HWND hwnd, WPARAM mp1, LRESULT * pmres)
  {
	HMENU		hmenuAppli = ::GetMenu (hwnd);

	// Enregistre le hwnd de l'appli
	Appli.bSetHWndPrincipal (hwnd);

	// Ajout d'un menu pop up des descripteurs
  if (VerifWarning (hmenuAppli != NULL))
		{
		DWORD   driver;
		char    tab_caractere_raccourci [27];
		HMENU   hPopUp = CreatePopupMenu (); //GetSubMenu (hmenuAppli, 3);

		// Insertion des descripteurs dans le pop up
		StrSetNull (tab_caractere_raccourci);
		for (driver = premier_driver; driver <= dernier_driver; driver++)
			{
			char    titre_desc [c_nb_car_message_inf];
			DWORD   c_inf_mnemo_desc;
			DWORD   c_inf_titre_desc;

			if (bInfoDescripteur (driver, &c_inf_titre_desc, &c_inf_mnemo_desc))
				{
				bLngLireInformation (c_inf_titre_desc, titre_desc);
				insere_raccourci (titre_desc, tab_caractere_raccourci); 
				AppendMenu (hPopUp, MF_STRING, driver , titre_desc);
				}
			}

		// Insertion du pop up dans le menu
		MenuInsertPopUpMI (hmenuAppli, POSITION_MENU_DESCRIPTEURS, c_inf_descripteur, hPopUp);
		}

	// Cr�ation de la boite de dialogue d'aide
	InitAide ();

  return TRUE;
  } // OnCreate

// ----------------------------------------------------------------------- 
// Traitement du message WM_DESTROY
// -----------------------------------------------------------------------
static BOOL OnDestroy (HWND hwnd, WPARAM mp1, LRESULT * pmres)
  {
	// Enregistre la configuration utilisateur
	EcrireConfigUtilisateur();

	// Supprime le menu popup Descripteurs
	HMENU hPopUp = GetSubMenu (::GetMenu(hwnd), POSITION_MENU_DESCRIPTEURS);

	DestroyMenu (hPopUp);

	// Fermeture de la boite de dialogue d'aide
	FermeAide ();

	// hwnd de l'appli ferm�
	Appli.bSetHWndPrincipal (NULL);

	// Fin de la boucle de message
	PostQuitMessage (0);

  return TRUE;
  } // OnDestroy

// ----------------------------------------------------------------------- 
// Traitement du message WM_NOTIFY
// -----------------------------------------------------------------------
static BOOL OnNotify (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
	BOOL bRes = FALSE;
	
	switch (mp1)
		{
		case ID_MESSAGE_ACTION: // message de type action
			{
			bRes = bTraiteNotifyActionCf (mp2);
			}
			break;
		}

  return bRes;
  } // OnDestroy

// ----------------------------------------------------------------------- 
// Traitement du message WM_KEYDOWN (pour touches sp�ciales)
// -----------------------------------------------------------------------
static BOOL OnKeyDown (HWND hwnd, WPARAM mp1, LRESULT * pmres)
  {
  BOOL    bToucheTraitee = TRUE;
  t_param_action  param_action;
	BOOL		bControle = ((0x8000 & GetKeyState (VK_CONTROL)) != 0);
  BOOL		bShift = ((0x8000 & GetKeyState (VK_SHIFT))!= 0);
	char		c = LOBYTE (mp1);
	
	switch (c)
		{
		case VK_HOME:
			if (bControle)
				bAjouterActionCf (action_premiere_ligne, &param_action);
			else
				bAjouterActionCf (action_debut_ligne, &param_action);
			break;

		case VK_END:
			if (bControle)
				bAjouterActionCf (action_derniere_ligne, &param_action);
			else
				bAjouterActionCf (action_fin_ligne, &param_action);
			break;

		case VK_LEFT:
			if (bControle)
				bAjouterActionCf (action_mot_precedent, &param_action);
			else
				{
				param_action.entier = 1;
				bAjouterActionCf (action_curseur_gauche, &param_action);
				}
			break;

		case VK_RIGHT:
			if (bControle)
				bAjouterActionCf (action_mot_suivant, &param_action);
			else
				{
				param_action.entier = 1;
				bAjouterActionCf (action_curseur_droite, &param_action);
				}
			break;

		case VK_UP:
			if (bShift)
				bAjouterActionCf (action_debut_selection, &param_action);
			param_action.entier = 1;
			bAjouterActionCf (action_curseur_haut, &param_action);
			break;

		case VK_DOWN:
			if (bShift)
				bAjouterActionCf (action_debut_selection, &param_action);
			param_action.entier = 1;
			bAjouterActionCf (action_curseur_bas, &param_action);
			break;

		case VK_PRIOR: // VK_PRIOR:
			if (bShift)
				bAjouterActionCf (action_debut_selection, &param_action);
			bAjouterActionCf (action_page_precedente, &param_action);
			break;

		case VK_NEXT: // VK_NEXT:
			if (bShift)
				bAjouterActionCf (action_debut_selection, &param_action);
			bAjouterActionCf (action_page_suivante, &param_action);
			break;

		case VK_DELETE:
			if (bShift)
				bAjouterActionCf (action_coupe, &param_action);
			else
				bAjouterActionCf (action_efface_car, &param_action);
			break;

		case VK_BACK:// VK_BACK:
			bAjouterActionCf (action_back_car, &param_action);
			break;

		case VK_INSERT:
			if (!(bShift && bControle))
				{
				if (bControle)
					bAjouterActionCf (action_copie, &param_action);
				else
					{
					if (bShift)
						bAjouterActionCf (action_colle, &param_action);
					else
						bAjouterActionCf (action_mode_saisie, &param_action);
					}
				}
			else
				bToucheTraitee = FALSE;
			break;

		case VK_F5:
			bAjouterActionCf (action_redessine, &param_action);
			break;

		default:
			bToucheTraitee = FALSE;
			break;
		}

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = bToucheTraitee;
  return (bToucheTraitee);
  }

// -----------------------------------------------------------------------
// Traitement du message WM_CHAR touches normales
// -----------------------------------------------------------------------
static BOOL OnChar (HWND hwnd, WPARAM mp1, LRESULT * pmres)
  {
  BOOL      bToucheTraitee = TRUE;
  t_param_action  param_action;
	char			c = LOBYTE (mp1);

	if (StrCharCanBeDisplayed (c))
		{
		param_action.lettre = c;
		bAjouterActionCf (action_ecrire_car, &param_action);
		}
	else
		{
		switch (c)
			{
			case '\3': // Ctrl C
				bAjouterActionCf (action_copie, &param_action);
				break;

			case '\x16': // Ctrl V
				bAjouterActionCf (action_colle, &param_action);
				break;

			case '\x18': // Ctrl X
				bAjouterActionCf (action_coupe, &param_action);
				break;

			case '\x19': // Ctrl Y
        bAjouterActionCf (action_efface_ligne, &param_action);
				break;

			case VK_RETURN:
        bAjouterActionCf (action_validation, &param_action);
        break;

      case VK_ESCAPE:
        bAjouterActionCf (action_abandon, &param_action);
        break;

			default:
				bToucheTraitee = FALSE;
				break;
			}
		}

  *pmres = bToucheTraitee;
  return (bToucheTraitee);
  }

// -----------------------------------------------------------------------
// Traitement du message WM_HELP
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// -----------------------------------------------------------------------
static BOOL OnHelp (HWND hwnd, LRESULT * pmres)
  {
  t_param_action param_action;

  bAjouterActionCf (action_aide, &param_action);
  *pmres = 0;

  return(TRUE);
  }

static void OnOptionPolice(HWND hwnd)
	{
  t_param_action  param_action;
  bAjouterActionCf (action_option_police, &param_action);
	}

// -----------------------------------------------------------------------
// Traitement du message WM_COMMAND
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// -----------------------------------------------------------------------
static BOOL OnCommand (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  DWORD     cmd_menu = LOWORD (mp1);
  t_param_action  param_action;
  char      saisie[MAX_PATH];
  char      titre[30];
  char      action[30];
  char      mess[c_nb_car_message_inf];

  switch (cmd_menu)
    {
    case CMD_MENU_NOUVEAU:
			{
      bLngLireInformation (c_inf_confir_sauve, mess);
      switch (dlg_confirmation (Appli.hwnd, mess))
        {
        case IDYES:
          param_action.entier = FERME_TOUT;
          bAjouterActionCf (action_fermeture, &param_action);
					OnSauve ();
          bAjouterActionCf (action_nouvelle_application, &param_action);
          param_action.entier = premier_driver;
					param_action.booleen = FALSE;
          bAjouterActionCf (action_initialisation, &param_action);
          break;
        case IDNO:
          param_action.entier = FERME_TOUT;
          bAjouterActionCf (action_fermeture, &param_action);
          param_action.entier = premier_driver;
          param_action.booleen = FALSE;
					bAjouterActionCf (action_initialisation, &param_action);
          bAjouterActionCf (action_nouvelle_application, &param_action);
          param_action.entier = premier_driver;
          param_action.booleen = FALSE;
          bAjouterActionCf (action_initialisation, &param_action);
          break;
        //case IDCANCEL:
        }
			}
      break;

    case CMD_MENU_OUVRE:
      bLngLireInformation (c_inf_confir_sauve, mess);
      switch (dlg_confirmation (Appli.hwnd, mess))
        {
        case IDYES:
					{
          param_action.entier = FERME_TOUT;
          bAjouterActionCf (action_fermeture, &param_action);
					OnSauve ();
          bLngLireInformation (c_inf_tdlg_ouvrir, titre);
          bLngLireInformation (c_inf_action_ouvre, action);
					pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
          if (bDlgFichierOuvrir (Appli.hwnd, saisie, titre, "pcs", action))
            {
            StrCopy (param_action.chaine, pszSupprimeExt (saisie));
            bAjouterActionCf (action_charge_application, &param_action);
            }
          else
            {
            StrCopy (param_action.chaine, pszPathNameDoc());
            bAjouterActionCf (action_charge_application, &param_action);
            }
          param_action.entier = premier_driver;
					param_action.booleen = FALSE;
          bAjouterActionCf (action_initialisation, &param_action);
					}
          break;
        case IDNO:
					{
          bLngLireInformation (c_inf_tdlg_ouvrir, titre);
          bLngLireInformation (c_inf_action_ouvre, action);
					pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
          if (bDlgFichierOuvrir (Appli.hwnd, saisie, titre, "pcs", action))
            {
            param_action.entier = FERME_TOUT;
            bAjouterActionCf (action_fermeture, &param_action);
            StrCopy (param_action.chaine, pszSupprimeExt (saisie));
            bAjouterActionCf (action_charge_application, &param_action);
            param_action.entier = premier_driver;
						param_action.booleen = FALSE;
            bAjouterActionCf (action_initialisation, &param_action);
            }
					}
          break;
        //case IDCANCEL:
        }
      break;

    case CMD_MENU_SAUVE:
			{
			OnSauve();
			}
      break;

    case CMD_MENU_SAUVE_SOUS:
			{
      bLngLireInformation (c_inf_tdlg_sauve, titre);
      bLngLireInformation (c_inf_action_sauve, action);
			pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
      if (bDlgFichierEnregistrerSous (Appli.hwnd, saisie, titre, "pcs", action))
        {
        StrCopy (param_action.chaine, pszSupprimeExt (saisie));
        bAjouterActionCf (action_sauve_application, &param_action);
        }
			}
      break;

		case CMD_MENU_GENERE:
			{
      bLngLireInformation (c_inf_confir_sauve, mess);
      switch (dlg_confirmation (Appli.hwnd, mess))
        {
        case IDYES:
					OnSauve ();
          bAjouterActionCf (action_generation, &param_action);
          break;
        case IDNO:
          bAjouterActionCf (action_generation, &param_action);
          break;
        //case IDCANCEL:
        }
			}
      break;

    case CMD_MENU_IMPORTE:
			{
      bLngLireInformation (c_inf_confir_sauve, mess);
      switch (dlg_confirmation (Appli.hwnd, mess))
        {
        case IDYES:
					OnSauve ();
          bLngLireInformation (c_inf_tdlg_importe, titre);
          bLngLireInformation (c_inf_action_importe, action);
					pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SRC);
          if (bDlgFichierOuvrir (Appli.hwnd, saisie, titre, "src", action))
            {
            param_action.entier = FERME_TOUT;
            bAjouterActionCf (action_fermeture, &param_action);
            param_action.entier = IMPORT_REMPLACEMENT;
            StrCopy (param_action.chaine, pszSupprimeExt (saisie));
            bAjouterActionCf (action_import, &param_action);
            }
          break;
        case IDNO:
          bLngLireInformation (c_inf_tdlg_importe, titre);
          bLngLireInformation (c_inf_action_importe, action);
					pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SRC);
          if (bDlgFichierOuvrir (Appli.hwnd, saisie, titre, "src", action))
            {
            param_action.entier = FERME_TOUT;
            bAjouterActionCf (action_fermeture, &param_action);
            param_action.entier = IMPORT_REMPLACEMENT;
            StrCopy (param_action.chaine, pszSupprimeExt (saisie));
            bAjouterActionCf (action_import, &param_action);
            }
          break;
        //case IDCANCEL:
        }
			}
      break;

    case CMD_MENU_AJOUTE:
			{
      bLngLireInformation (c_inf_tdlg_ajoute, titre);
      bLngLireInformation (c_inf_action_ajoute, action);
			pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SRC);
      if (bDlgFichierOuvrir (Appli.hwnd, saisie, titre, "src", action))
        {
        param_action.entier = FERME_TOUT;
        bAjouterActionCf (action_fermeture, &param_action);
        param_action.entier = IMPORT_AJOUT;
        StrCopy (param_action.chaine, pszSupprimeExt (saisie));
        bAjouterActionCf (action_import, &param_action);
        }
			}
      break;

    case CMD_MENU_EXPORTE:
			{
			// Controle enfonc� => ex�cution de l'export avec annonce des lignes invalides
			BOOL bAnnonceLigneInvalide = ((GetAsyncKeyState(VK_LCONTROL) & 0x8000) || (GetAsyncKeyState(VK_RCONTROL) & 0x8000));
      bLngLireInformation (c_inf_tdlg_exporte, titre);
      bLngLireInformation (c_inf_action_exporte, action);
			pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SRC);
      if (bDlgFichierEnregistrerSous (Appli.hwnd, saisie, titre, "src", action))
        {
        StrCopy (param_action.chaine, pszSupprimeExt (saisie));
				param_action.booleen = bAnnonceLigneInvalide;
				bAjouterActionCf (action_export, &param_action);
        }
			}
      break;

    case CMD_MENU_IMPRIME:
			{
		  t_dlg_imprime retour_imprime = {"",1,NULL};

      if (dlg_imprime (Appli.hwnd, &retour_imprime))
        {
        param_action.entier = (int) retour_imprime.selecteur;
        bAjouterActionCf (action_imprime, &param_action);
        }
			}
      break;

    case CMD_MENU_A_PROPOS :
			{
			bAfficheInfoVersion ();
			}
      break;

    case CMD_MENU_QUITTE:
			{
			SendMessage (hwnd, WM_CLOSE, mp1, mp2);
			}
      break;

    case CMD_MENU_TOUT_SELECTIONNE:
      bAjouterActionCf (action_selectionne_tout, &param_action);
      break;

    case CMD_MENU_COUPE:
      bAjouterActionCf (action_coupe, &param_action);
      break;

    case CMD_MENU_COPIE:
      bAjouterActionCf (action_copie, &param_action);
      break;

    case CMD_MENU_COLLE:
      bAjouterActionCf (action_colle, &param_action);
      break;

    case CMD_MENU_COPIE_SPECIAL:
      bLngLireInformation (c_inf_tdlg_copier_special, titre);
      bLngLireInformation (c_inf_copier, action);
			pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SRX);
      if (bDlgFichierEnregistrerSous (Appli.hwnd, saisie, titre, "srx", action))
        {
        StrCopy (param_action.chaine, saisie);
        bAjouterActionCf (action_nomme_pp, &param_action);
        bAjouterActionCf (action_copie, &param_action);
        }
      break;

    case CMD_MENU_COLLE_SPECIAL:
      bLngLireInformation (c_inf_tdlg_coller_special, titre);
      bLngLireInformation (c_inf_coller, action);
			saisie [0] = '\0'; // $$ v�rifier
      if (bDlgFichierOuvrir (Appli.hwnd, saisie, titre, "srx", action))
        {
        StrCopy (param_action.chaine, saisie);
        bAjouterActionCf (action_nomme_pp, &param_action);
        bAjouterActionCf (action_colle, &param_action);
        }
      break;

    case CMD_MENU_CHERCHE:
			{
		  t_dlg_recherche retour_recherche = {"","",1,NULL};

      lire_mot_recherche_courant (retour_recherche.saisie);
      if (dlg_recherche_mot (Appli.hwnd, &retour_recherche))
        {
        param_action.entier = retour_recherche.sens; // sens recherche
        StrCopy (param_action.chaine, retour_recherche.saisie);
        bAjouterActionCf (action_init_recherche_mot, &param_action);
        bAjouterActionCf (action_recherche_mot, &param_action);
        }
			}
      break;

    case CMD_MENU_CHERCHE_INVALIDE:
      bAjouterActionCf (action_recherche_invalide, &param_action);
      break;

    case CMD_MENU_POURSUIT_RECHERCHE:
      bAjouterActionCf (action_recherche_mot, &param_action);
      break;

    case CMD_MENU_VA_A_LA_LIGNE:
			{
			PARAM_DLG_SAISIE_NUM retour_saisie_num = {"",1,NULL};

      bLngLireInformation (c_inf_tdlg_goto_ligne, retour_saisie_num.titre);
      if (dlg_saisie_num (Appli.hwnd, &retour_saisie_num))
        {
        param_action.entier = retour_saisie_num.saisie;
        bAjouterActionCf (action_va_a_la_ligne, &param_action);
        }
			}
      break;

    case CMD_MENU_MARQUE_LA_LIGNE:
      bAjouterActionCf (action_pose_marque, &param_action);
      break;

    case CMD_MENU_VA_A_LA_MARQUE:
      bAjouterActionCf (action_va_a_la_marque, &param_action);
      break;

    case CMD_MENU_INTERPRETATION:
      bAjouterActionCf (action_analyse_syntaxique, &param_action);
      BasculeItemInterprete = !BasculeItemInterprete;
			MenuAppliCocheItem (CMD_MENU_INTERPRETATION, BasculeItemInterprete);
      break;

    case CMD_MENU_VALIDE_TOUT:
      bAjouterActionCf (action_valide_toute_ligne, &param_action);
      break;

    case CMD_MENU_OUVRIR_BOITE_DLG:
      param_action.entier = 0; // premiere boite du descripteur
      param_action.booleen = BOITE_DIRECTE;
      bAjouterActionCf (action_ouvrir_boite, &param_action);
      break;

		case CMD_MENU_OPTIONS_POLICE:
			OnOptionPolice(hwnd);
			break;

		case CMD_MENU_AIDE:
			OnHelp (hwnd, pmres);
			break;

    default:
      param_action.entier = CHANGEMENT_DESCRIPTEUR;
      bAjouterActionCf (action_fermeture, &param_action);
      param_action.entier = cmd_menu;
			// Controle enfonc� ?
			if ((GetAsyncKeyState(VK_LCONTROL) & 0x8000) || (GetAsyncKeyState(VK_RCONTROL) & 0x8000))
				{
				// Oui => ex�cution de l'initialisation en mode alternatif
				param_action.booleen = TRUE;
				bAjouterActionCf (action_initialisation, &param_action);
				}
			else
				{
				// Non => ex�cution de l'initialisation en mode normal
				param_action.booleen = FALSE;
				bAjouterActionCf (action_initialisation, &param_action);
				}
      break;
    }
  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return (TRUE);
  }

// -----------------------------------------------------------------------
// Traitement du message WM_SIZE
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// -----------------------------------------------------------------------
static BOOL OnSize (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  DWORD     i;
  LONG     lPosX;
  LONG     lPosY;
  LONG     lSizeX;
  LONG     lSizeY;
  RECT    rclReference;
  RECT    rclNew;
  RECT    rclNewZone;

	if (mp1 != SIZE_MINIMIZED)
		{
		rclReference.left   = 0;
		rclReference.top    = 0;
		rclReference.bottom = 100;
		rclReference.right  = 100;

		rclNew.left   = 0;
		rclNew.top = 0;
		rclNew.right  = (int) LOWORD (mp2);
		rclNew.bottom    = (int) HIWORD (mp2);

		for (i = 0; i < NB_SOUS_FENETRES_MAX; i++)
			{
			if (aSousFenetres [i].hwnd_sous_fen != NULL)
				{
				lPosX  = (LONG) aSousFenetres [i].pos_x_sous_fen;
				lPosY  = (LONG) aSousFenetres [i].pos_y_sous_fen;
				lSizeX = (LONG) aSousFenetres [i].taille_x_sous_fen;
				lSizeY = (LONG) aSousFenetres [i].taille_y_sous_fen;
				Zone_Rectangle (aSousFenetres [i].zone_sous_fen, &rclNew, &rclNewZone);
				Map_Coordonnees (&rclReference, &rclNewZone,
												 &lPosX, &lPosY, &lSizeX, &lSizeY);

				switch (aSousFenetres [i].type_sous_fen)
					{
					case FENETRE_WND_PAGE:
					case FENETRE_MNU_BMP:
					case FENETRE_LN_TXT:
						::MoveWindow (aSousFenetres [i].hwnd_sous_fen, lPosX, lPosY,lSizeX, lSizeY, TRUE);
						//::SetWindowPos (aSousFenetres [i].hwnd_sous_fen, HWND_TOP, lPosX, lPosY,lSizeX, lSizeY, 0);//$$SWP_ACTIVATE
						break;
	//        case FENETRE_TITRE:
	//          break;
					default:
						break;
					}
				}
			}
		}
  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return (TRUE);
  }


// -----------------------------------------------------------------------
// Traitement du message WM_HSCROLL rout� par Page
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// Remarques: id =  (mp1);         control-window identifier
//            sPos = HIWORD (mp2);                 slider position
//            usCmd = LOWORD (mp2);                        command
// -----------------------------------------------------------------------
static BOOL OnHScrollPage (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  t_param_action  param_action;

  switch (LOWORD (mp2))
    {
		/* $$ non appel� ???
    case SB_TOP:
			bAjouterActionCf (action_fin_ligne, &param_action);
			break;*/

		case SB_PAGELEFT:
			bAjouterActionCf (action_mot_precedent, &param_action);
			break;

    case SB_LINELEFT:
      param_action.entier = 1;
      bAjouterActionCf (action_curseur_gauche, &param_action);
      break;
/* $$ non appel� ???
		case SB_BOTTOM:
			bAjouterActionCf (action_debut_ligne, &param_action);
			break;
*/
    case SB_PAGERIGHT:
			bAjouterActionCf (action_mot_suivant, &param_action);
			break;

    case SB_LINERIGHT:
      param_action.entier = 1;
      bAjouterActionCf (action_curseur_droite, &param_action);
      break;
		/* $$ a implanter
    case SB_THUMBPOSITION:
      break;
    case SB_THUMBTRACK:
      break;*/
    // case SB_ENDSCROLL: // fin du scroll (pas int�ressant)
    }
  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return (TRUE);
  }


// -----------------------------------------------------------------------
// Nom......: OnVScrollPage
// Objet....: Traitement du message WM_VSCROLL issu de la page de l'�diteur
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// Remarques: id = (USHORT) (mp1);                 control-window ID
//            sPos = HIWORD (mp2);               slider position
//            usCmd = LOWORD (mp2);              command
// -----------------------------------------------------------------------
static BOOL OnVScrollPage (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  t_param_action  param_action;

  switch (LOWORD (mp2))
    {
    case SB_LINEUP:
      param_action.entier = 1;
      bAjouterActionCf (action_curseur_haut, &param_action);
      break;
    case SB_LINEDOWN:
      param_action.entier = 1;
      bAjouterActionCf (action_curseur_bas, &param_action);
      break;
    case SB_PAGEUP:
      bAjouterActionCf (action_page_precedente, &param_action);
      break;
    case SB_PAGEDOWN:
      bAjouterActionCf (action_page_suivante, &param_action);
      break;
    case SB_THUMBPOSITION:
      param_action.entier = (int) HIWORD(mp2);
      bAjouterActionCf (action_va_a_la_ligne, &param_action);
      break;
//			/*$$ aboutit a un blocage entre les deux piles de l'appli
    case SB_THUMBTRACK:
      param_action.entier = (int) HIWORD(mp2);
      bAjouterActionCf (action_va_a_la_ligne, &param_action);
      break;
//			*/
    //case SB_ENDSCROLL: pas int�ressant
    }

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = 0;
  return (TRUE);
  }

// -----------------------------------------------------------------------
// Nom......: OnUser
// Objet....: Traitement du message WM_USER = Notification utilisateur
// Entr�es..: HWND     hwnd
//            UINT *  pmsg
//            WPARAM *  pmp1
//            LPARAM *  pmp2
//            LRESULT * pmres
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : TRUE si le message a �t� trait�, FALSE si non
// Remarques: notif  = (USHORT) (*pmp1);   window notification
//            id     = HIWORD (*pmp1);   window ID
//            param1 = (USHORT) (*pmp2);
//            param2 = HIWORD (*pmp2);
// -----------------------------------------------------------------------
static BOOL OnUser (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  BOOL     bTraite = FALSE;
  int      delta_ln;
  int      delta_col;
  t_param_action param_action;

  switch ((USHORT) (mp1))
  {
  case NU0_WND_PAGE_BN_1_DBL_CLICK:
    param_action.booleen = BOITE_INDIRECTE;
    bAjouterActionCf (action_ouvrir_boite, &param_action);
    break;

  case NU0_WND_PAGE_DEBUT_GLISSE:
    bAjouterActionCf (action_debut_selection, &param_action);
    break;

  case NU0_WND_PAGE_FIN_GLISSE:
    bAjouterActionCf (action_fin_selection, &param_action);
    break;

  case NU0_WND_PAGE_SIZE:
    param_action.entier = (USHORT) (mp2);
    bAjouterActionCf (action_taille, &param_action);
    break;

  case NU0_WND_PAGE_DEPLACE_CURSEUR:
    delta_ln  = (SHORT)LOWORD(mp2);
    if (delta_ln != 0)
      {
      if (delta_ln > 0)
        {
        param_action.entier = delta_ln;
        bAjouterActionCf (action_curseur_bas, &param_action);
        }
      else
        {
        param_action.entier = -delta_ln;
        bAjouterActionCf (action_curseur_haut, &param_action);
        }
      }
    delta_col = (SHORT) HIWORD (mp2);
    if (delta_col != 0)
      {
      if (delta_col > 0)
        {
        param_action.entier = delta_col;
        bAjouterActionCf (action_curseur_droite, &param_action);
        }
      else
        {
        param_action.entier = -delta_col;
        bAjouterActionCf (action_curseur_gauche, &param_action);
        }
      }
    bTraite = TRUE;
    break;

  case NU0_WND_DLG_ED_VA_LIGNE:
    param_action.entier = HIWORD(mp2);
    bAjouterActionCf (action_va_a_la_ligne, &param_action);
    bTraite = TRUE;
    break;
  }

  *pmres = 0;
  return bTraite;
  }

// -----------------------------------------------------------------------
// Callback de la fen�tre principale de PCSConf
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocWConf (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;                       // Valeur de retour
  BOOL     bTraite = TRUE;                // Indique commande non trait�e
  t_param_action param_action;

  switch (msg)
    {
    case WM_CREATE: 
			bTraite = OnCreate (hwnd, mp1, &mres);
			break;

    case WM_DESTROY:
			bTraite = OnDestroy (hwnd, mp1, &mres);
			break;

		case WM_ACTION_CF://WM_NOTIFY:
			bTraite = OnNotify (hwnd, mp1, mp2, &mres);
			break;

    case WM_CHAR:
      bTraite = OnChar (hwnd, mp1, &mres);
      break;

		case WM_KEYDOWN:
			bTraite = OnKeyDown (hwnd, mp1, &mres);
			break;

    case WM_COMMAND:
       bTraite = OnCommand (hwnd, mp1, mp2, &mres);
       break;

		case WM_CLOSE:
			{
		  char mess[c_nb_car_message_inf];

			bLngLireInformation (c_inf_confir_sauve, mess);
			switch (dlg_confirmation (Appli.hwnd, mess))
				{
				case IDYES:
					param_action.entier = QUITTE;
					bAjouterActionCf (action_fermeture, &param_action);
					OnSauve (); //$$ Erreur imm�diat / diff�r� � corriger et g�n�raliser
					bAjouterActionCf (action_quitte, &param_action);
					break;
				case IDNO:
					param_action.entier = QUITTE;
					bAjouterActionCf (action_fermeture, &param_action);
					bAjouterActionCf (action_quitte, &param_action);
					break;
				//case IDCANCEL: rien � faire
				}
			}
			break;

    case WM_SIZE:
			bTraite = OnSize (hwnd, mp1, mp2, &mres);
			break;

		case WM_QUERYOPEN: // r�initialise l'interpr�teur si on �tait en ic�ne
      param_action.entier = 0;
			param_action.booleen = FALSE;
      bAjouterActionCf (action_initialisation, &param_action);
			mres = TRUE;
			break;

    case WM_FIN_SATELLITE:
      mres = 0;
      break;

    case WM_USER:
			bTraite = OnUser (hwnd, mp1, mp2, &mres);
			break;

    case WM_HSCROLL:
			bTraite = OnHScrollPage (hwnd, mp1, mp2, &mres);
			break;

    case WM_VSCROLL:
			bTraite = OnVScrollPage (hwnd, mp1, mp2, &mres);
			break;

    case WM_HELP: // $$ jamais appel� ?
			bTraite = OnHelp (hwnd, &mres);
			break;

//		case WM_WININICHANGE: // = WM_SETTINGCHANGE Changement environnement (NT)
		case WM_SETTINGCHANGE: // Changement environnement (windows95)
			Appli.GetTailleEcran(); // $$ g�n�raliser ! les param�tres ne sont pas les m�mes => ignor�s
			break;

		case WM_INPUTLANGCHANGEREQUEST:
			// On n'accepte pas le changement de langue � la vol�e
			break;

		//case WM_INPUTLANGCHANGE:
			// jamais re�u puisque toujours refus�
			//break;

    default:
       bTraite = FALSE;
       break;
    } // switch (msg)

  // message non trait�, traitement par d�faut
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  } // wndprocWConf

// -----------------------------------------------------------------------
// Cr�ation de la fen�tre principale de PcsConf
// Renvoie TRUE si la creation et l'initialisation se sont bien deroulees.
// -----------------------------------------------------------------------
BOOL bCreeFenetreConf (void)
  {
  BOOL  creation_ok = FALSE;  // Valeur retour
  DWORD     i;
	WNDCLASS wc;

	// Lire la configuration utilisateur
	LireConfigUtilisateur();

	// Initialisation des infos de sous fen�tres
  for (i = 0; i < NB_SOUS_FENETRES_MAX; i++)
    {
    aSousFenetres [i].hwnd_sous_fen     = NULL;
    aSousFenetres [i].type_sous_fen     = NO_FENETRE;
    aSousFenetres [i].pos_x_sous_fen    = 0;
    aSousFenetres [i].pos_y_sous_fen    = 0;
    aSousFenetres [i].taille_x_sous_fen = 0;
    aSousFenetres [i].taille_y_sous_fen = 0;
    }
  DefinirZones (HAUT, 0, BAS, 0, GAUCHE, 0, DROITE, 0);

	// Cr�e la fen�tre principale de l'application
	wc.style					= CS_HREDRAW | CS_VREDRAW; 
  wc.lpfnWndProc		= wndprocWConf; 
  wc.cbClsExtra			= 0; 
  wc.cbWndExtra			= 0; 
  wc.hInstance			= Appli.hinst; 
  wc.hIcon					= LoadIcon(Appli.hinst, MAKEINTRESOURCE (ID_ICO_PCS));
  wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
  wc.hbrBackground	= NULL; //(HBRUSH)(COLOR_APPWORKSPACE+1); pas d'effacement
  wc.lpszMenuName		= NULL;
  wc.lpszClassName	= pszClasseWConf; 

  if (RegisterClass (&wc))
    {
		Appli.GetTailleEcran();

		// lance la fen�tre principale de l'application
    if (CreateWindow (pszClasseWConf, Appli.szNom, WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_CLIPCHILDREN, 
			0, 0, Appli.sizEcran.cx, Appli.sizEcran.cy, NULL, LoadMenu (Appli.hinstDllLng ,MAKEINTRESOURCE (IDMENU_CF)) , Appli.hinst, NULL))
      creation_ok = TRUE;
    }
  return creation_ok;
  } // bCreeFenetreConf

// -----------------------------------------------------------------------
// d�finir la taille d'une zone �crans
// -----------------------------------------------------------------------
void FixerTailleZone (DWORD zone_a_fixer, DWORD taille_a_donner)
  {
  aZonesFenetres [zone_a_fixer].taille_zone = taille_a_donner;
  }

// -----------------------------------------------------------------------
// Nom......: DefinirZones
// Objet....: d�finir l'ordre de cr�ation des zones �crans et leurs tailles
// Exemple..: +----------------------------------------------+
//            |              Zone HAUT  ordre 1              |
//            +-----------+---------------------+------------+
//            |Zone       |                     |Zone        |
//            |           |                     |            |
//            | GAUCHE    |      Zone           | DROITE     |
//            |           |                     |            |
//            |  ordre    |     MILIEU          |  ordre     |
//            |           |                     |            |
//            |   2       |                     |     4      |
//            |           |                     |            |
//            |           +---------------------+------------+
//            |           | Zone  BAS  ordre 3               |
//            |           |                                  |
//            +-----------+----------------------------------+
//
// -----------------------------------------------------------------------
void DefinirZones 
	(DWORD zone1, DWORD taille1,
	DWORD zone2, DWORD taille2,
	DWORD zone3, DWORD taille3,
	DWORD zone4, DWORD taille4
	)
  {
  aZonesFenetres [zone1].ordre_zone  = 1;
  FixerTailleZone (zone1, taille1);

  aZonesFenetres [zone2].ordre_zone  = 2;
  FixerTailleZone (zone2, taille2);

  aZonesFenetres [zone3].ordre_zone  = 3;
  FixerTailleZone (zone3, taille3);

  aZonesFenetres [zone4].ordre_zone  = 4;
  FixerTailleZone (zone4, taille4);

  aZonesFenetres [MILIEU].ordre_zone  = 0;
  FixerTailleZone (MILIEU, 0);
  }

// -----------------------------------------------------------------------
// Cr�ation d'une des sous fen�tres du bureau de PcsConf
// -----------------------------------------------------------------------
static BOOL bCreerSousFenetreBureau 
	(DWORD *repere, DWORD type_fenetre,
	int pos_x_init, int pos_y_init,
	int taille_x, int taille_y,
	DWORD zone_fenetre,
	DWORD couleur_fond_plan, DWORD couleur_avant_plan,
	PDESCRIPTION_BARRE_OUTIL donnees_specifiques)
  {
  BOOL repere_trouve;
  BOOL	bRes = FALSE;
  DWORD	  i;
  LONG	  lPosX;
  LONG	  lPosY;
  LONG	  lSizeX;
  LONG	  lSizeY;
  RECT   rclReference = {0,0,100,100}; // rectangle de r�f�rence : taille = 100%
  RECT   rclNew;
  RECT   rclNewZone;

	// recherche d'une "sous fen�tre" du bureau libre
  repere_trouve = FALSE;
  i = NB_SOUS_FENETRES_MAX;
  while ((!repere_trouve) && (i != 0))
    {
    i--;
    repere_trouve = (BOOL) (aSousFenetres [i].hwnd_sous_fen == NULL);
    }

	// "sous fen�tre" libre ?
  if (repere_trouve)
    {
		// oui => on enregistre ses caract�ristiques
    *repere = i;
    aSousFenetres [i].type_sous_fen     = type_fenetre;
    aSousFenetres [i].pos_x_sous_fen    = pos_x_init;
    aSousFenetres [i].pos_y_sous_fen    = pos_y_init;
    aSousFenetres [i].taille_x_sous_fen = taille_x;
    aSousFenetres [i].taille_y_sous_fen = taille_y;
    aSousFenetres [i].zone_sous_fen    = zone_fenetre;
    aSousFenetres [i].en_selection      = FALSE;

		// calcul des dimensions de la sous fen�tre (en %):
		// r�cup�re les dimensions de la zone de la sous fen�tre
    ::GetClientRect (Appli.hwnd, &rclNew);
    Zone_Rectangle (zone_fenetre, &rclNew, &rclNewZone);

		// r�cup�re les dimensions de la sous fen�tre elle m�me
    lPosX  = (LONG) pos_x_init;
    lPosY  = (LONG) pos_y_init;
    lSizeX = (LONG) taille_x;
    lSizeY = (LONG) taille_y;
    Map_Coordonnees (&rclReference, &rclNewZone,
                     &lPosX, &lPosY, &lSizeX, &lSizeY);

		// cr�e la sous fen�tre � l'emplacement calcul�
    switch (type_fenetre)
      {
      case FENETRE_WND_PAGE:
		aSousFenetres [i].hwnd_sous_fen = creer_wnd_page 
					(Appli.hwnd, Appli.hwnd, i, lPosX, lPosY, lSizeX, lSizeY); //$$ double hwnd inutile
		bRes = (aSousFenetres [i].hwnd_sous_fen != NULL);
				if (bRes)
					PageChangePolice(aSousFenetres [i].hwnd_sous_fen, dwConfNFont, dwConfTailleFont, dwConfBitsStyle, Appli.byCharSetFont);
		break;

      case FENETRE_MNU_BMP:
        aSousFenetres [i].hwnd_sous_fen =  hwndCreeFondBarreOutil
					(Appli.hwnd, lPosX,lPosY,lSizeX,lSizeY,i, donnees_specifiques);
        bRes = (aSousFenetres [i].hwnd_sous_fen != NULL);
        break;

      case FENETRE_LN_TXT:
         aSousFenetres [i].hwnd_sous_fen = hwndStatutInit 
					 (Appli.hwnd, lPosX,lPosY,lSizeX,lSizeY,i);
        bRes = (aSousFenetres [i].hwnd_sous_fen != NULL);
        break;

      case FENETRE_TITRE:
				aSousFenetres [i].hwnd_sous_fen = Appli.hwnd;
        bRes = (aSousFenetres [i].hwnd_sous_fen != NULL);
        break;

      default:
        break;
      }
	  }
  return bRes;
  } // bCreerSousFenetreBureau 

// -----------------------------------------------------------------------
// Cr�e la barre d'outils des commandes PcsConf
static BOOL bCreerBarreOutilsCommandesConf (void)
  {
  DWORD  repere_barre_outils;
  //
  // Description Barre d'outils
  #define NB_ITEMS_BARRE_OUTILS_COMMANDE_CONF   11
  //
  static DESCRIPTION_BOUTON_BARRE_OUTIL aItemsCommandeConf [NB_ITEMS_BARRE_OUTILS_COMMANDE_CONF] =
    {
    "IDB_CMD_SAUVER"  , BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_SAUVE              ,
    "IDB_CMD_GENERER" , BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_GENERE             ,
    "IDB_CMD_IMPRIMER", BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_IMPRIME            ,
    "IDB_CMD_COUPER"  , BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_COUPE              ,
    "IDB_CMD_COPIER"  , BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_COPIE              ,
    "IDB_CMD_COLLER"  , BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_COLLE              ,
    "IDB_CMD_CHERCHER", BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_CHERCHE ,
    "IDB_CMD_QHLP_RESR", BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_POURSUIT_RECHERCHE ,
    "IDB_CMD_MARQUER" , BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_MARQUE_LA_LIGNE    ,
    "IDB_CMD_GOMARQUE", BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_VA_A_LA_MARQUE     ,
    "IDB_CMD_DLG_DESC", BO_COL_SUIVANTE, BO_BOUTON, CMD_MENU_OUVRIR_BOITE_DLG
    };
  //
  DESCRIPTION_BARRE_OUTIL mnu =
    {
    NB_ITEMS_BARRE_OUTILS_COMMANDE_CONF,  //Nb Items Max
    FALSE,                                //Pas de Frame
    4, 0,                                 //Espaces X et Y entre les Bit-Maps
    BO_POS_GAUCHE | BO_POS_CENTRE,				//Position relative des bit-maps par rapport � la fenetre
    0, 0,                                 //Position du coin Bas-Gauche par rapport � la fenetre
    aItemsCommandeConf                    //Tableau des Items
    };

  FixerTailleZone (HAUT, 30);
  return bCreerSousFenetreBureau (&repere_barre_outils, FENETRE_MNU_BMP, 0, 0, 100, 100, HAUT, 0, 0, &mnu);
  } // bCreerBarreOutilsCommandesConf

// -----------------------------------------------------------------------
// Cr�e la barre outil des descripteurs de PCS-CONF
// Retour...: TRUE si OK
// -----------------------------------------------------------------------
static BOOL bCreerBarreOutilsDescripteurs (void)
  {
  #define NB_ITEMS_MAX  8
  #define ESPACE_BMP 38
  DWORD  repere_acces_desc;
  DWORD nb_item = 0;
  DWORD i_item;
  DWORD nb_colonne;
  DWORD driver;
  char mnemo_desc [c_nb_car_message_inf];
  DWORD c_inf_mnemo_desc;
  DWORD c_inf_titre_desc;
  DESCRIPTION_BARRE_OUTIL mnu;
  DESCRIPTION_BOUTON_BARRE_OUTIL *pitem;
  BOOL retour_cree;

  for (driver = premier_driver; driver <= dernier_driver; driver++)
    {
    if (bInfoDescripteur (driver, &c_inf_titre_desc, &c_inf_mnemo_desc))
      {
      nb_item++;
      }
    }
  nb_colonne = (nb_item / NB_ITEMS_MAX);
  if ((nb_item % NB_ITEMS_MAX) != 0)
    nb_colonne++;

  pitem = (PDESCRIPTION_BOUTON_BARRE_OUTIL)pMemAlloue (nb_item * sizeof (DESCRIPTION_BOUTON_BARRE_OUTIL));
	i_item = 0;
  for (driver = premier_driver; driver <= dernier_driver; driver++)
    {
    if (bInfoDescripteur (driver, &c_inf_titre_desc, &c_inf_mnemo_desc))
      {
			// g�n�re le nom du bitmap du descripteur = IDB_(nom descripteur)
      bLngLireInformation (c_inf_mnemo_desc, mnemo_desc);
      StrInsertStr (mnemo_desc, "IDB_",0);
      pitem[i_item].FileName = (char *)pMemAlloue (StrLength (mnemo_desc)+1);
      StrCopy (pitem[i_item].FileName, mnemo_desc);
      if ((i_item % nb_colonne) == 0)
        pitem[i_item].PositionInMenu = BO_NOUVELLE_LN;
      else
        pitem[i_item].PositionInMenu = BO_COL_SUIVANTE;

      pitem[i_item].Mode = BO_BOUTON;
			pitem[i_item].Id = driver;

			i_item++;
      }
    } // for (driver = 
	mnu.NbBoutons = nb_item;
  mnu.bAvecBordure = FALSE;
  mnu.XSpaceBetweenItemBmp = 0;
  mnu.YSpaceBetweenItemBmp = 0;
  mnu.RelativePositionBmpMnu = BO_POS_HAUTE | BO_POS_CENTRE;
  mnu.xOrigine = 0;
  mnu.yOrigine = 0;
  mnu.ItemBmpMnu = pitem;

  FixerTailleZone (DROITE, nb_colonne * ESPACE_BMP);
  retour_cree = bCreerSousFenetreBureau (&repere_acces_desc, FENETRE_MNU_BMP, 0, 0, 100, 100, DROITE, 0, 0, &mnu);
  for (i_item = 0; i_item < nb_item; i_item++)
    MemLibere ((PVOID *)(&pitem[i_item].FileName));
  MemLibere ((PVOID *)(&pitem));
  return retour_cree;
  } // bCreerBarreOutilsDescripteurs

// -----------------------------------------------------------------------
// Cr�ation des sous fenetres de WConf
BOOL bCreerSousFenetresBureau (void)
  {
	int cyMenu = GetSystemMetrics (SM_CYMENU);

  DefinirZones (HAUT, 0, BAS, cyMenu + 3, GAUCHE, 0, DROITE, 0);
  bCreerBarreOutilsCommandesConf ();
  bCreerBarreOutilsDescripteurs ();
  creer_qhelp_fen();
	bCreerSousFenetreBureau (&ContexteCf.NFenetreTitre, FENETRE_TITRE,  0, 0, 0, 0, 0, 0, 0, NULL);
  bCreerSousFenetreBureau (&ContexteCf.NFenetreCommentaire, FENETRE_LN_TXT,  0, 0, 75, 100, BAS, 0, 0, NULL);
  bCreerSousFenetreBureau (&ContexteCf.NFenetreIndicateurs, FENETRE_LN_TXT, 75, 0, 25, 100, BAS, 0, 0, NULL);
  bCreerSousFenetreBureau (&ContexteCf.dwNFenetreEdit, FENETRE_WND_PAGE, 0, 0, 100, 100, MILIEU, 0, etat_0, NULL);
  effacer_fenetre (ContexteCf.dwNFenetreEdit);
	return TRUE; //$$ err bidon
  }

// -----------------------------------------------------------------------
// Nom......: effacer_fenetre
// Objet....: Effacer une fen�tre
// Remarques: curseur en (0,0) + scroll_col <- 0 + txt_ln <- "" + reaffiche
// -----------------------------------------------------------------------
void effacer_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      effacer_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    case FENETRE_LN_TXT:
      ::SetWindowText (aSousFenetres [repere].hwnd_sous_fen, "");
      break;
    case FENETRE_TITRE:
      ::SetWindowText (Appli.hwnd, "");
      break;
    default:
      break;
    }
  return;
  }

// -----------------------------------------------------------------------
// Ecriture d'une ligne � la ligne courante (r�affiche la ligne)
// -----------------------------------------------------------------------
void ecrire_ligne_fenetre (DWORD repere, char *texte, DWORD etat_ligne)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      ecrire_wnd_page (aSousFenetres [repere].hwnd_sous_fen, texte, etat_ligne);
      break;
    case FENETRE_LN_TXT:
      ::SetWindowText (aSousFenetres [repere].hwnd_sous_fen, texte);
      break;
    case FENETRE_TITRE:
      ::SetWindowText (Appli.hwnd, texte);
      break;
    }
  }

// -----------------------------------------------------------------------
// R�cuperer le nombre de lignes visibles d'une fen�tre
// -----------------------------------------------------------------------
DWORD nombre_ligne_fenetre (DWORD repere)
  {
  DWORD nb_lignes = 1;

  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      nb_lignes = nombre_ligne_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  return nb_lignes;
  }


// -----------------------------------------------------------------------
// R�cuperer le texte de la ligne courante d'une fen�tre
// -----------------------------------------------------------------------
void consulter_ligne_fenetre (DWORD repere, char *texte)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      texte_cour_wnd_page (aSousFenetres [repere].hwnd_sous_fen, texte);
      break;
    }
  }

// -----------------------------------------------------------------------
// D�placement vertical du texte
// Remarques: d�calage > 0 ==> d�placement du texte du bas vers le haut
//            les nouvelles lignes visualis�es sont effac�e + r�affiche
// -----------------------------------------------------------------------
void scroller_fenetre_ligne (DWORD repere, int decalage)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      scroller_ligne_wnd_page (aSousFenetres [repere].hwnd_sous_fen, decalage);
      break;
    }
  }

// -----------------------------------------------------------------------
// D�placement horizontal du texte
// Remarques: d�calage > 0 ==> d�placement du texte de la droite vers la gauche
//            reaffiche
// -----------------------------------------------------------------------
void scroller_fenetre_colonne (DWORD repere, int decalage)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      scroller_colonne_wnd_page (aSousFenetres [repere].hwnd_sous_fen, decalage);
      break;
    }
  }

// -----------------------------------------------------------------------
// Positionner le curseur en d�but de la ligne courante
// Remarques: + scroll en colonnes de toutes les lignes
//            reaffiche
// -----------------------------------------------------------------------
void aller_debut_ligne_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      aller_debut_ligne_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
// Positionner le curseur en fin de la ligne courante
// Remarques: + scroll en colonnes de toutes les lignes
//            reaffiche
// -----------------------------------------------------------------------
void aller_fin_ligne_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      aller_fin_ligne_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
// D�placer le curseur
// Remarques: si impossible, le curseur n'est pas du tout d�plac�
//            reaffiche
// -----------------------------------------------------------------------
void monter_curseur_fenetre (DWORD repere, DWORD *decalage)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      monter_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen, decalage);
      break;
    }
  }

// -----------------------------------------------------------------------
// D�placer le curseur
// Sorties..: possible ou pas
// Remarques: si impossible, le curseur n'est pas du tout d�plac�
//            reaffiche
// -----------------------------------------------------------------------
void descendre_curseur_fenetre (DWORD repere, DWORD & Decalage)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      descendre_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen, Decalage);
      break;
    }
  }

// -----------------------------------------------------------------------
// D�placer le curseur vers la doite (scroll si n�cessaire)
// -----------------------------------------------------------------------
void droiter_curseur_fenetre (DWORD repere, DWORD decalage)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      droiter_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen, decalage);
      break;
    }
  }


// -----------------------------------------------------------------------
// D�placer le curseur vers la gauche (scroll si n�cessaire)
// -----------------------------------------------------------------------
void gaucher_curseur_fenetre (DWORD repere, DWORD decalage)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      gaucher_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen, decalage);
      break;
    }
  }

// -----------------------------------------------------------------------
// Il fait un peut trop de choses!
//            test debut_ln + txt [ln_cour] <-- "" + etat [ln_cour] <-- 0
//            + scroll + reaffiche
// -----------------------------------------------------------------------
void inserer_ligne_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      inserer_ligne_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
// Insere un caractere a la position courante du curseur (reaffiche)
// -----------------------------------------------------------------------
void inserer_car_ligne_fenetre (DWORD repere, char caractere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      inserer_car_wnd_page (aSousFenetres [repere].hwnd_sous_fen, caractere);
      break;
    }
  }

// -----------------------------------------------------------------------
// remplace le caractere a la position courante du curseur (reaffiche)
// -----------------------------------------------------------------------
void modifier_car_ligne_fenetre (DWORD repere, char caractere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      modifier_car_wnd_page (aSousFenetres [repere].hwnd_sous_fen, caractere);
      break;
    }
  }

// -----------------------------------------------------------------------
// Supprime le caractere a la position courante du curseur (reaffiche)
// -----------------------------------------------------------------------
void supprimer_car_ligne_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      supprimer_car_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
void dialoguer_operateur (DWORD repere, DWORD erreur_bd,
                          DWORD *retour_operateur_commande)
  {
	char erreur[80] = "";
	DWORD commande = ID_ABANDONNER;
  bLngLireErreur (erreur_bd, erreur);
  aSousFenetres [repere].hwnd_sous_fen;
	char szErr [2048];

	// $$ traduction
	wsprintf (szErr, "%s\n\nModifier imm�diatement ?", erreur);
	switch (nSignale (szErr, NULL, MB_ICONQUESTION | MB_YESNOCANCEL))
		{
		case IDYES:
			commande = ID_CORRECTION;
			break;
		case IDNO:
			commande = ID_POURSUIVRE;
			break;
		default: // = case IDCANCEL:
			commande = ID_ABANDONNER;
			break;
		}

  (*retour_operateur_commande) = commande;
  }

// --------------------------------------------------------------------------
void reajuster_ascenseur_fenetre (DWORD repere, DWORD pos_relative_ligne, DWORD taille_relative)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      reajuster_ascenseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen, pos_relative_ligne, taille_relative);
      break;
    }
  }

// --------------------------------------------------------------------------
// renvoie la position courante du curseur ligne et colonne ABSOLU (scroll) de la fenetre
// --------------------------------------------------------------------------
void position_curseur_fenetre (DWORD repere, DWORD *ligne, DWORD *colonne)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      position_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen, ligne, colonne);
      break;
    }
  }

// --------------------------------------------------------------------------
void aller_haut_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      aller_haut_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// --------------------------------------------------------------------------
void aller_bas_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      aller_bas_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// --------------------------------------------------------------------------
void droiter_mot_curseur_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      droiter_mot_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// --------------------------------------------------------------------------
void gaucher_mot_curseur_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      gaucher_mot_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// --------------------------------------------------------------------------
// ATTENTION DANGER : ne restaure que si la pos courante > pos memorise
void memoriser_curseur_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      memoriser_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }
// ATTENTION DANGER : ne restaure que si la pos courante > pos memorise
void restaurer_curseur_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      restaurer_curseur_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
void sauver_marque_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      sauver_marque_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
void restaurer_marque_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      restaurer_marque_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
void afficher_select_ligne_fenetre (DWORD repere, BOOL mode_selection)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      afficher_select_ligne_wnd_page (aSousFenetres [repere].hwnd_sous_fen, mode_selection);
      break;
    }
  }

// -----------------------------------------------------------------------
void abandon_selection_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      abandon_selection_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
void curseur_invisible_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      curseur_invisible_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
void curseur_visible_fenetre (DWORD repere)
  {
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
      curseur_visible_wnd_page (aSousFenetres [repere].hwnd_sous_fen);
      break;
    }
  }

// -----------------------------------------------------------------------
void cacher_plan_de_travail (void)
  {
  ::ShowWindow (Appli.hwnd, SW_HIDE);
  }

// -----------------------------------------------------------------------
void montrer_plan_de_travail (void)
  {
  ::ShowWindow (Appli.hwnd, SW_SHOW);
  }


// -----------------------------------------------------------------------
// Cette procedure est un thread
UINT _stdcall LanceSatellite (void *hThrd)
  {
  UINT dwFct;
  void * pParamIn;
  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);

  while (bThreadAttenteMessage ((HTHREAD)hThrd, &pParamIn, NULL, NULL, &dwFct))
    {
    switch (dwFct)
      {
      case THRD_FCT_EXEC:
				{
				PARAMS_SATELLITE *ptr_satellite = (PPARAMS_SATELLITE)pParamIn;
				DWORD uNbEnv = 0;
			  char TabPtrChaine[1][STR_MAX_CHAR_ARRAY];

				if (!StrIsNull (ptr_satellite->env))
					{
					uNbEnv = 1;
					if (StrLength(ptr_satellite->env) <= STR_MAX_CHAR_ARRAY)
						{
					  StrCopy (TabPtrChaine[0], ptr_satellite->env);
						}
					else
						{
						VerifWarningExit;
						}
					}
				// Erreur ex�cution du programe Ok ?
			  PROCESS_INFORMATION	ProcessInformation;
				if (!bProgExecute (&ProcessInformation, ptr_satellite->bExecutionSynchrone, 
					ptr_satellite->nom, ptr_satellite->param, TabPtrChaine, uNbEnv, ptr_satellite->chemin))
					{
					char szTemp [MAX_PATH + 100];

					wsprintf (szTemp, "Le programme '%s' n'a pas pu �tre ex�cut� correctement", ptr_satellite->nom);
					dlg_erreur (szTemp);
					}

        fin_satellite = TRUE;
        ::PostMessage (Appli.hwnd, WM_FIN_SATELLITE, 0, 0);
        ThreadFinTraiteMessage ((HTHREAD)hThrd);
				}
        break;

      default:
        break;
      }
    }
  return uThreadTermine ((HTHREAD)hThrd, 0);
  }

// -----------------------------------------------------------------------
void lancer_satellite_pm (BOOL bExecutionSynchrone, char *nom, char *param, char *env, char *chemin)
  {
  MSG             msg;
  HTHREAD					hThrdSatellite;
  DWORD           pdwError;
  PARAMS_SATELLITE param_satellite;

	//$$ � v�rifier => REMPLACER par une attente de fin de process
  fin_satellite = FALSE;
  param_satellite.bExecutionSynchrone = bExecutionSynchrone;
  StrCopy (param_satellite.nom, nom);
  StrCopy (param_satellite.param, param);
  StrCopy (param_satellite.env, env);
  StrCopy (param_satellite.chemin, chemin);
  if (bThreadCree (&hThrdSatellite, LanceSatellite, SIZE_STACK_THREAD_SATELLITE))
    {
    ThreadEnvoiMessage (hThrdSatellite, THRD_FCT_EXEC, &param_satellite, NULL, &pdwError);
    while (!fin_satellite)
      {
      GetMessage (&msg, NULL, 0, 0); //$$WM_QUIT, TranslateMessage ...
      DispatchMessage (&msg);
      }
    bThreadAttenteFinTraiteMessage (hThrdSatellite, INFINITE);
    bThreadFerme (&hThrdSatellite, INFINITE);
    }
  }

// -----------------------------------------------------------------------
// Nom......: patienter
// Objet....: Afficher un "signal" pour faire patienter l'utilisateur
// Entr�es..: TRUE    pour le d�but du signal
//            FALSE   pour la fin du signal
// Remarques: sous PM, le "signal" est un curseur souris "sablier".
//            sous PM, si pas de souris, le "signal" est quand m�me affich�.
// -----------------------------------------------------------------------
void patienter (BOOL patience)
  {
  if (patience)
    {
    SetCursor (LoadCursor (NULL, IDC_WAIT));
    if (GetSystemMetrics (SM_MOUSEPRESENT) == 0)
      ShowCursor (TRUE);
    }
  else
    {
    if (GetSystemMetrics (SM_MOUSEPRESENT) == 0)
      ShowCursor (FALSE);

    SetCursor (LoadCursor (NULL, IDC_ARROW));
    }
  }

// -----------------------------------------------------------------------
// Choix d'une police pour l'�diteur
void PageOptionPolice(DWORD repere)
	{
  switch (aSousFenetres [repere].type_sous_fen)
    {
    case FENETRE_WND_PAGE:
			if (fnt_user(Appli.hwnd, &dwConfNFont, &dwConfBitsStyle, &dwConfTailleFont, Appli.byCharSetFont))
				{
				PageChangePolice (aSousFenetres [repere].hwnd_sous_fen, dwConfNFont, dwConfTailleFont, dwConfBitsStyle, Appli.byCharSetFont);
				}
      break;
    }
	}
// -----------------------------------------------------------------------
void creer_qhelp_fen (void)
  {
  DWORD i;
  char titre_desc[c_nb_car_message_inf];
  char mnemo_desc[c_nb_car_message_inf];
  DWORD c_inf_mnemo;
  DWORD c_inf_titre;

	// Remplissage de l'index de l'aide
  bLngLireInformation (c_inf_aide_general,mnemo_desc);
  bLngLireInformation (c_inf_aide_generalite,titre_desc);
  AjouteIndexAide(mnemo_desc,titre_desc);
  i = premier_driver;
  while (i < dernier_driver+1)
    {
    if (bInfoDescripteur (i, &c_inf_titre, &c_inf_mnemo))
      {
      bLngLireInformation (c_inf_mnemo,mnemo_desc);
      bLngLireInformation (c_inf_titre,titre_desc);
      AjouteIndexAide(mnemo_desc,titre_desc);

      bLngLireInformation (c_inf_code_mnemo,titre_desc);
      if (bStrEgales (mnemo_desc,titre_desc))
        {
        bLngLireInformation (c_inf_aide_execute_f,mnemo_desc);
        bLngLireInformation (c_inf_aide_fonction_code,titre_desc);
        AjouteIndexAide(mnemo_desc,titre_desc);
        }
      }
    i++;
    }
  } // creer_qhelp_fen


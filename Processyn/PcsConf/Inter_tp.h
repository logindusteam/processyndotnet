/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_tp.h                                               |
 |   Auteur  : lm	         				        |
 |   Date    : 13/04/94  				        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

// Interface du descripteur chronomètres et métronomes
void LisDescripteurTp (PDESCRIPTEUR_CF pDescripteurCf);

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_ap.h                                               |
 |   Auteur  : 	         				        	|
 |   Date    :  					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

// Interface du descripteur Applicom
void LisDescripteurAp (PDESCRIPTEUR_CF pDescripteurCf);

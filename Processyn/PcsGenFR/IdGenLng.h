//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by E:\PcsNT\Sources\FR\PcsGenFR.rc
//
#define IDS_ICO_ANIME                   101
#define IDDLG_GEN                       102
#define ID_GENERER                      256
#define ID_OUVRIR_FICHIER               257
#define ID_ABANDONNER_GENERATION        258
#define ID_QUITTER                      259
#define ID_ICO_GN1                      260
#define ID_ICO_GN2                      261
#define ID_NOM_APPLIC                   262
#define IDC_STATUT_GENERATION           264
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif

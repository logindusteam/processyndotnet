// KeyBLock.cpp : Defines the entry point for the DLL application.
// 
// Version 0.0
// 16/9/99
// Win32 pour Windows NT 4.0 SP 3.0 minimum exclusivement
// Permet d'inhiber ou pas l'appui des combinaisons de touches suivantes :
// Ctrl Esc
// Alt Esc
// Ctrl MAJ Esc
// Ctrl Alt 'A'...'Z' '0'..'9'
// (Touche fen�tre) seule ou en combinaison
// Alt Tab
#include "stdafx.h"
#include "Liclock.h"

static HINSTANCE hInstance = NULL;

static BOOL bInhibe = FALSE;
static HHOOK hHook = NULL;
static BOOL bCtrl = FALSE;
static BOOL bShift = FALSE;
static BOOL bLockClavierTache = FALSE;
static BOOL bLockClavierFenetre = FALSE;


BOOL bTermineInhibition (void)
	{
	BOOL bRes = FALSE;
	if (UnhookWindowsHookEx(hHook))
		{
		hHook = NULL;
		bInhibe = FALSE;
		bRes = TRUE;
		}
	return bRes;
	}


//--------------------------------------------------------
// Fonction callback appell�e pour chaque appui de caract�re
LRESULT CALLBACK LowLevelKeyboardProc2 (INT nCode, WPARAM wParam, LPARAM lParam)
	{
	// By returning a non-zero value from the hook procedure, the
	// message does not get passed to the target window
	LPKBDLLHOOKSTRUCT pkbhs = (LPKBDLLHOOKSTRUCT)lParam;
	BOOL bControlKeyDown = 0;
	
	switch (nCode)
    {
		case HC_ACTION:
			{
			// Check to see if the CTRL key is pressed
			bControlKeyDown = GetAsyncKeyState (VK_CONTROL) >> ((sizeof(SHORT) * 8) - 1);
			
			// Disable CTRL+ESC
			if ((bLockClavierTache) && (pkbhs->vkCode == VK_ESCAPE && bControlKeyDown))
				return 1;
			
			// Disable ATL+TAB
			if ((bLockClavierTache) && (pkbhs->vkCode == VK_TAB && pkbhs->flags & LLKHF_ALTDOWN))
				return 1;
			
			// Disable ALT+ESC
			if ((bLockClavierTache) && (pkbhs->vkCode == VK_ESCAPE && pkbhs->flags & LLKHF_ALTDOWN))
				return 1;
			
			// Disable the WINDOWS key
			if ((bLockClavierTache) && (pkbhs->vkCode == VK_LWIN || pkbhs->vkCode == VK_RWIN))
				return 1;
			
			// Disable ALT+F4
			if ((bLockClavierFenetre) && (pkbhs->vkCode == VK_F4 && pkbhs->flags & LLKHF_ALTDOWN))
				return 1;
			
			break;
			}
			
		default:
			break;
    }
	return CallNextHookEx (hHook, nCode, wParam, lParam);
	} 
 
// -----------------------------------------------
// Point d'entr�e utilisateur
//------------------------------------------------
LICLOCK_API int APIENTRY bInhibeClavier(int iLockClavierTache, int iLockClavierFenetre)
	{
	BOOL bRes = 0;

	// Conversion VB �ventuelle
	bLockClavierTache = (iLockClavierTache != 0);
	bLockClavierFenetre = (iLockClavierFenetre != 0);
	BOOL bDemandeInhibe = (bLockClavierTache || bLockClavierFenetre);

	// Changement d'�tat demand� ?
	if (bInhibe != bDemandeInhibe)
		{
		// oui => inhiber ?
		if (bDemandeInhibe)
			{
			// oui => inhibition
			hHook = SetWindowsHookEx(
				WH_KEYBOARD_LL, // Sp�cifique NT : type of hook to install : LowLevelKeyboardProc
				LowLevelKeyboardProc2,     // address of hook procedure
				hInstance,    // handle to application instance
				0   // identity of thread to install hook for : ALL
				);
 
			bInhibe = (hHook != NULL);
			}
		else
			{
			// non => arr�t inhibition
			bTermineInhibition ();
			}
		// Renvoie la satisfaction de la demande (compatible Bboolean VB)
		if (bInhibe == bDemandeInhibe)
			bRes =-1;
		}

	return bRes;
	}


//---------------------------------------------- 
// Point d'entr�e standard DLL
BOOL APIENTRY DllMain(HINSTANCE hModule, DWORD  ul_reason_for_call, LPVOID lpReserved)
	{
	switch (ul_reason_for_call)
		{
		case DLL_PROCESS_ATTACH:
			hInstance = (HINSTANCE)hModule;
			break;
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			if (bInhibe)
				bTermineInhibition();
			break;
    }
	// Accepte  toutes les op�rations
	return TRUE;
	}


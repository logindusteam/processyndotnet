/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : TIPE_TP.H         Declaration des types du driver temps  |
 |   Auteur  : lm                                                       |
 |   Date    : 13/04/94                                                 |
 |   Version : 4.00                                                     |
 +----------------------------------------------------------------------*/

//------------------------------ BLOCS
//
#define b_geo_chrono            82
#define b_titre_paragraf_chrono 80
#define b_metronome             81
#define bx_chronometre          83
#define bx_metronome            84

//------------------------------ TYPES
//
typedef struct
  {
  DWORD numero_repere;
  union
    {
    DWORD pos_donnees;
    DWORD pos_paragraf;
    struct
      {
      DWORD pos_es;
      DWORD pos_initial;
      DWORD pos_specif_es;
      };
    };
  } GEO_CHRONO, *PGEO_CHRONO;

typedef struct
  {
  DWORD		index_dans_bd;
  CHRONO	chrono;
  } X_CHRONOMETRE, *PX_CHRONOMETRE;

typedef struct
  {
  DWORD index_dans_bd;
  MINUTERIE tempo;
  LONG val_duree;
  } X_METRONOME, *PX_METRONOME;

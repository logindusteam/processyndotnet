/*--------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                   |
 |              Societe LOGIQUE INDUSTRIE                             |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                |
 |   Il est demeure sa propriete exclusive et est confidentiel.       |
 |   Aucune diffusion n'est possible sans accord ecrit                |
 |--------------------------------------------------------------------|
 |	       																														|
 |   Auteur  : LM																											|
 |   Date    : 08/02/94 																							|
 |                                                                    |
 +--------------------------------------------------------------------*/
#include "stdafx.h"
#include "std.h"           // Types Standards
#include "lng_res.h"       // Mots Reserves pour tests alarmes
#include "tipe.h"          // Types Generaux
#include "UStr.h"        // Chaines de Caractères            Strxxx ()
#include "mem.h"           // Memory Manager
#include "pcs_sys.h"       // Variables systemes
#include "findbdex.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "dicho.h"         // ordre alphabetique des variables
#include "tipe_de.h"       // Types des Blocs
#include "GenereBD.h"

#include "gener_de.h"
#include "Verif.h"
VerifInit;

// --------------------------------------------------------------------------
// Comparaison pour recherche
static int Comparaison_i_Identif_Entree (const void *element_1, DWORD numero)
  {
  PX_VAR_DDE_CLIENT_DYN pVarClientDyn = (X_VAR_DDE_CLIENT_DYN*)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, numero);

  return ((*((PDWORD)element_1)) - pVarClientDyn->wPosBxEs);
  }

// --------------------------------------------------------------------------
// Comparaison pour recherche
static int Comparaison_i_Identif_Reflet (const void *element_1, DWORD numero)
  {
  PX_VAR_SERVEUR_DDE	element_2 = (X_VAR_SERVEUR_DDE *)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, numero);

  return ((*((PDWORD)element_1)) - element_2->wPosBxEs);
  }

// --------------------------------------------------------------------------
// Debut de la Generation
// --------------------------------------------------------------------------
DWORD genere_de (DWORD *wLigne)
  {
  PGEO_DDE                  pGeoDe;
  GEO_DDE                   GeoDe;
  PTITRE_PARAGRAPHE					pTitreParagraf;
  t_spec_titre_paragraf_de*pSpecTitreParagraf;
  tc_dde_e*pDdeE;
  tc_dde_r*pDdeR;
  tx_dde_lien_e*pxDdeLienE;
  tx_dde_lien_r*pxDdeLienR;
  tx_dde_e_advise*pxDdeEAdvise;
  tx_dde_e_request*pxDdeERequest;
  PX_VAR_DDE_CLIENT_R pxDdeR;
  //
  tc_dde_e_dyn*pDdeEDyn;
  tc_dde_r_dyn*pDdeRDyn;
  PX_VAR_DDE_CLIENT_DYN pVarClientDyn;
  PX_VAR_SERVEUR_DDE	pVarServeurDde;
  DWORD                      wErreur;
  DWORD                      wLigneCour;
  DWORD                      wPosition;
  PENTREE_SORTIE	pES;
  BOOL                    bServeurOk = FALSE;
  DWORD                       wNbrEnr;

  wErreur = 0;
  if (existe_repere (b_geo_dde) && (nb_enregistrements (szVERIFSource, __LINE__, b_geo_dde) != 0))
    {
    wLigneCour = 0;
    while ((wLigneCour < nb_enregistrements (szVERIFSource, __LINE__, b_geo_dde)) && (wErreur == 0))
      {
      wLigneCour++;
      pGeoDe = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, wLigneCour);
      GeoDe = (*pGeoDe);
      switch (GeoDe.wNumeroRepere)
        {
        case b_pt_constant :
          supprime_cst_bd_c (GeoDe.wPosDonnees);
          break;

				case b_titre_paragraf_de :
					pTitreParagraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_de, GeoDe.wPosParagraf);
					switch (pTitreParagraf->IdParagraphe)
						{
						case liens_predefinis_de :
						case variables_importees_de :
							break;

						case variables_exportees_de :
							bServeurOk = TRUE;
							break;

						case entree :
							if (!existe_repere (bx_dde_lien_e))
								{
								cree_bloc (bx_dde_lien_e, sizeof (tx_dde_lien_e), 0);
								}
							if (!existe_repere (bx_dde_e_advise))
								{
								cree_bloc (bx_dde_e_advise, sizeof (tx_dde_e_advise), 0);
								}
							if (!existe_repere (bx_dde_e_request))
								{
								cree_bloc (bx_dde_e_request, sizeof (tx_dde_e_request), 0);
								}
							insere_enr (szVERIFSource, __LINE__, 1, bx_dde_lien_e, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_e) + 1);
							pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_e));
										//
              pSpecTitreParagraf = (t_spec_titre_paragraf_de*)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, GeoDe.wPosSpecifParagraf);
              //
              StrCopy (pxDdeLienE->pszLien, pSpecTitreParagraf->pszLien);
              StrDeleteDoubleQuotation (pxDdeLienE->pszLien);
              StrCopy (pxDdeLienE->pszApplic, pSpecTitreParagraf->pszApplic);
              StrDeleteDoubleQuotation (pxDdeLienE->pszApplic);
              StrCopy (pxDdeLienE->pszTopic, pSpecTitreParagraf->pszTopic);
              StrDeleteDoubleQuotation (pxDdeLienE->pszTopic);
							wErreur = recherche_index_execution (pSpecTitreParagraf->wPosEsLien, &pxDdeLienE->wPosEsLien);
              if (wErreur == 0)
                {
                pxDdeLienE->bMemLien = FALSE; // maj a ds les sorties dde
                pxDdeLienE->wTimeOut = pSpecTitreParagraf->wTimeOut;
                StrCopy (pxDdeLienE->pszParam, pSpecTitreParagraf->pszParam);
                StrDeleteDoubleQuotation (pxDdeLienE->pszParam);
                pxDdeLienE->hconvClient = NULL;
                pxDdeLienE->bLienOk = FALSE;
                pxDdeLienE->bLienDetruit = TRUE;
                pxDdeLienE->wPremItemAdvise = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_e_advise) + 1;
                pxDdeLienE->wNbItemAdvise = 0;
                pxDdeLienE->wPremItemRequest = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_e_request) + 1;
                pxDdeLienE->wNbItemRequest = 0;
                }
              break;

						case sortie : // reflet
							if (!existe_repere (bx_dde_lien_r))
                {
								cree_bloc (bx_dde_lien_r, sizeof (tx_dde_lien_r), 0);
                }
							if (!existe_repere (bx_dde_r))
                {
								cree_bloc (bx_dde_r, sizeof (X_VAR_DDE_CLIENT_R), 0);
                }
							insere_enr (szVERIFSource, __LINE__, 1, bx_dde_lien_r, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_r) + 1);
							pxDdeLienR = (tx_dde_lien_r*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_r, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_r));
              //
              pSpecTitreParagraf = (t_spec_titre_paragraf_de*)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_de, GeoDe.wPosSpecifParagraf);
              //
              StrCopy (pxDdeLienR->pszLien, pSpecTitreParagraf->pszLien);
              StrDeleteDoubleQuotation (pxDdeLienR->pszLien);
              StrCopy (pxDdeLienR->pszApplic, pSpecTitreParagraf->pszApplic);
              StrDeleteDoubleQuotation (pxDdeLienR->pszApplic);
              StrCopy (pxDdeLienR->pszTopic, pSpecTitreParagraf->pszTopic);
              StrDeleteDoubleQuotation (pxDdeLienR->pszTopic);
							wErreur = recherche_index_execution (pSpecTitreParagraf->wPosEsLien, &pxDdeLienR->wPosEsLien);
              if (wErreur == 0)
                {
                pxDdeLienR->wTimeOut = pSpecTitreParagraf->wTimeOut;
                StrCopy (pxDdeLienR->pszParam, pSpecTitreParagraf->pszParam);
                StrDeleteDoubleQuotation (pxDdeLienR->pszParam);
                pxDdeLienR->hconvClient = NULL;
                pxDdeLienR->bLienOk = FALSE;
                pxDdeLienR->bLienDetruit = TRUE;
                pxDdeLienR->wPremItem = nb_enregistrements (szVERIFSource, __LINE__, bx_dde_r) + 1;
                pxDdeLienR->wNbItem = 0;
                }
              break;

            default :
              break;
            } // switch (pTitreParagraf->tipe)  paragraph
          break;

				case b_dde_e :
					pDdeE = (tc_dde_e*)pointe_enr (szVERIFSource, __LINE__, b_dde_e, GeoDe.wPosSpecifEs);
          if (pDdeE->wPosEsRaf)
            { // c'est un request
						insere_enr (szVERIFSource, __LINE__, 1, bx_dde_e_request, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_e_request) + 1);
						pxDdeERequest = (tx_dde_e_request*)pointe_enr (szVERIFSource, __LINE__, bx_dde_e_request, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_e_request));
            //
            if (pDdeE->wTaille == 0)
              {  // c'est une variable simple
							wErreur = recherche_index_execution (GeoDe.wPosEs, &pxDdeERequest->wPosEs);
              if (wErreur == 0)
                {
                switch (pDdeE->wGenre)
                  {
                  case c_res_logique :
                    pxDdeERequest->wGenre = b_cour_log;
                    break;
                  case c_res_numerique :
                    pxDdeERequest->wPosEs = pxDdeERequest->wPosEs - ContexteGen.nbr_logique;
                    pxDdeERequest->wGenre = b_cour_num;
                    break;
                  case c_res_message :
                    pxDdeERequest->wPosEs = pxDdeERequest->wPosEs - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                    pxDdeERequest->wGenre = b_cour_mes;
                    break;
                  default :
                    wErreur = 95; // genre inconnu
                    break;
                  }
                }
              }
            else
              { // c'est un request avec une variable tableau
              wErreur = recherche_index_tableau_i (GeoDe.wPosEs, 1, &pxDdeERequest->wPosEs);
              if (wErreur == 0)
                {
                switch (pDdeE->wGenre)
                  {
                  case c_res_logique :
                    pxDdeERequest->wGenre = b_cour_log;
                    break;
                  case c_res_numerique :
                    pxDdeERequest->wGenre = b_cour_num;
                    break;
                  case c_res_message :
                    pxDdeERequest->wGenre = b_cour_mes;
                    break;
                  default :
                    wErreur = 95; // genre inconnu
                    break;
                  }
                }
              }
            //
            if (wErreur == 0)
              { // request simple ou tableau
              pxDdeERequest->wTaille = pDdeE->wTaille;
              StrCopy (pxDdeERequest->pszItem, pDdeE->pszItem);
              StrDeleteDoubleQuotation (pxDdeERequest->pszItem);
							wErreur = recherche_index_execution (pDdeE->wPosEsRaf, &pxDdeERequest->wPosEsRaf);
              //
							pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_e));
              pxDdeLienE->wNbItemRequest++;
              }
            //
            }
          else
            { // c'est un advise
						insere_enr (szVERIFSource, __LINE__, 1, bx_dde_e_advise, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_e_advise) + 1);
						pxDdeEAdvise = (tx_dde_e_advise*)pointe_enr (szVERIFSource, __LINE__, bx_dde_e_advise, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_e_advise));
            //
            if (pDdeE->wTaille == 0)
              {  // c'est une variable simple
							wErreur = recherche_index_execution (GeoDe.wPosEs, &pxDdeEAdvise->wPosEs);
              if (wErreur == 0)
                {
                switch (pDdeE->wGenre)
                  {
                  case c_res_logique :
                    pxDdeEAdvise->wGenre = b_cour_log;
                    break;
                  case c_res_numerique :
                    pxDdeEAdvise->wPosEs = pxDdeEAdvise->wPosEs - ContexteGen.nbr_logique;
                    pxDdeEAdvise->wGenre = b_cour_num;
                    break;
                  case c_res_message :
                    pxDdeEAdvise->wPosEs = pxDdeEAdvise->wPosEs - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                    pxDdeEAdvise->wGenre = b_cour_mes;
                    break;
                  default :
                    wErreur = 95; // genre inconnu
                    break;
                  }
                }
              }
            else
              { // c'est un request avec une variable tableau
              wErreur = recherche_index_tableau_i (GeoDe.wPosEs, 1, &pxDdeEAdvise->wPosEs);
              if (wErreur == 0)
                {
                switch (pDdeE->wGenre)
                  {
                  case c_res_logique :
                    pxDdeEAdvise->wGenre = b_cour_log;
                    break;
                  case c_res_numerique :
                    pxDdeEAdvise->wGenre = b_cour_num;
                    break;
                  case c_res_message :
                    pxDdeEAdvise->wGenre = b_cour_mes;
                    break;
                  default :
                    wErreur = 95; // genre inconnu
                    break;
                  }
                }
              }
            // advise simple ou tableau
            if (wErreur == 0)
              {
              pxDdeEAdvise->wTaille = pDdeE->wTaille;
              StrCopy (pxDdeEAdvise->pszItem, pDdeE->pszItem);
              StrDeleteDoubleQuotation (pxDdeEAdvise->pszItem);
              pxDdeEAdvise->bDataRecue = FALSE;
              //
							pxDdeLienE = (tx_dde_lien_e*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_e, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_e));
              pxDdeLienE->wNbItemAdvise++;
              }
            //
            }
          break;

				case b_dde_r :
					insere_enr (szVERIFSource, __LINE__, 1, bx_dde_r, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_r) + 1);
					pxDdeR = (PX_VAR_DDE_CLIENT_R)pointe_enr (szVERIFSource, __LINE__, bx_dde_r, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_r));
					pDdeR = (tc_dde_r*)pointe_enr (szVERIFSource, __LINE__, b_dde_r, GeoDe.wPosDonnees);
          pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, pDdeR->wPosEs);
          //
          if (pES->taille == 0)
            {  // c'est une variable simple
						wErreur = recherche_index_execution (pDdeR->wPosEs, &pxDdeR->wPosEs);
            if (wErreur == 0)
              {
              switch (pDdeR->wGenre)
                {
                case c_res_logique :
                  pxDdeR->wGenre = b_cour_log;
                  break;
                case c_res_numerique :
                  pxDdeR->wPosEs = pxDdeR->wPosEs - ContexteGen.nbr_logique;
                  pxDdeR->wGenre = b_cour_num;
                  break;
                case c_res_message :
                  pxDdeR->wPosEs = pxDdeR->wPosEs - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                  pxDdeR->wGenre = b_cour_mes;
                  break;
                default :
                  wErreur = 95; // genre inconnu
                  break;
                }
              pxDdeR->wPosEsRaf = 0;
              }
            }
          else
            { // c'est un reflet avec une variable tableau
            wErreur = recherche_index_tableau_i (pDdeR->wPosEs, 1, &pxDdeR->wPosEs);
            if (wErreur == 0)
              {
              switch (pDdeR->wGenre)
                {
                case c_res_logique :
                  pxDdeR->wGenre = b_cour_log;
                  break;
                case c_res_numerique :
                  pxDdeR->wGenre = b_cour_num;
                  break;
                case c_res_message :
                  pxDdeR->wGenre = b_cour_mes;
                  break;
                default :
                  wErreur = 95; // genre inconnu
                  break;
                }
							wErreur = recherche_index_execution (pDdeR->wPosEsRaf, &pxDdeR->wPosEsRaf);
              }
            }
          //
          if (wErreur == 0)
            { // request simple ou tableau
            pxDdeR->wTaille = pES->taille;
            StrCopy (pxDdeR->pszItem, pDdeR->pszItem);
            StrDeleteDoubleQuotation (pxDdeR->pszItem);
            //
						pxDdeLienR = (tx_dde_lien_r*)pointe_enr (szVERIFSource, __LINE__, bx_dde_lien_r, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_lien_r));
            pxDdeLienR->wNbItem++;
            }
          break;

				case b_dde_e_dyn :
					if (!existe_repere (bx_var_dde_client_dyn))
            {
            cree_bloc (bx_var_dde_client_dyn, sizeof (X_VAR_DDE_CLIENT_DYN), 0);
            }
					pDdeEDyn = (tc_dde_e_dyn*)pointe_enr (szVERIFSource, __LINE__, b_dde_e_dyn, GeoDe.wPosSpecifEs);
          //
          pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, GeoDe.wPosEs);
          //
					if (trouve_position_dichotomie (&pES->i_identif, 1, nb_enregistrements (szVERIFSource, __LINE__, bx_var_dde_client_dyn),
																								Comparaison_i_Identif_Entree, &wPosition))
						{
            // la variable est deja declaree
						}
					else
						{
						insere_enr (szVERIFSource, __LINE__, 1, bx_var_dde_client_dyn, wPosition);
						pVarClientDyn = (PX_VAR_DDE_CLIENT_DYN)pointe_enr (szVERIFSource, __LINE__, bx_var_dde_client_dyn, wPosition);
            //
            pVarClientDyn->wPosBxEs = pES->i_identif;
            //
            if (pDdeEDyn->wTaille == 0)
              {  // c'est une variable simple
							wErreur = recherche_index_execution (GeoDe.wPosEs, &pVarClientDyn->wPosEs);
              if (wErreur == 0)
                {
                switch (pDdeEDyn->wGenre)
                  {
                  case c_res_logique :
                    pVarClientDyn->wGenre = b_cour_log;
                    break;
                  case c_res_numerique :
                    pVarClientDyn->wPosEs = pVarClientDyn->wPosEs - ContexteGen.nbr_logique;
                    pVarClientDyn->wGenre = b_cour_num;
                    break;
                  case c_res_message :
                    pVarClientDyn->wPosEs = pVarClientDyn->wPosEs - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                    pVarClientDyn->wGenre = b_cour_mes;
                    break;
                  default :
                    wErreur = 95; // genre inconnu
                    break;
                  }
                }
              }
            else
              { // c'est un request avec une variable tableau
              wErreur = recherche_index_tableau_i (GeoDe.wPosEs, 1, &pVarClientDyn->wPosEs);
              if (wErreur == 0)
                {
                switch (pDdeEDyn->wGenre)
                  {
                  case c_res_logique :
                    pVarClientDyn->wGenre = b_cour_log;
                    break;
                  case c_res_numerique :
                    pVarClientDyn->wGenre = b_cour_num;
                    break;
                  case c_res_message :
                    pVarClientDyn->wGenre = b_cour_mes;
                    break;
                  default :
                    wErreur = 95; // genre inconnu
                    break;
                  }
                }
              }
            // advise simple ou tableau
            if (wErreur == 0)
              {
              pVarClientDyn->hconvClient = NULL;
              pVarClientDyn->bLienOk = FALSE;
              pVarClientDyn->bLienDetruit = TRUE;
              pVarClientDyn->bDataRecue = FALSE;
              pVarClientDyn->wTaille = pDdeEDyn->wTaille;
              }
            } // trouve_position_dichotomie
          break;

				case b_dde_r_dyn :
					if (!existe_repere (bx_dde_var_serveur))
            {
            cree_bloc (bx_dde_var_serveur, sizeof (X_VAR_SERVEUR_DDE), 0);
            }
					pDdeRDyn = (tc_dde_r_dyn*)pointe_enr (szVERIFSource, __LINE__, b_dde_r_dyn, GeoDe.wPosDonnees);
          //
          pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, pDdeRDyn->wPosEs);
          //
					if (trouve_position_dichotomie (&pES->i_identif, 1, nb_enregistrements (szVERIFSource, __LINE__, bx_dde_var_serveur),
                                          Comparaison_i_Identif_Reflet, &wPosition))
						{
            // la variable est deja declaree
						}
					else
						{
						insere_enr (szVERIFSource, __LINE__, 1, bx_dde_var_serveur, wPosition);
						pVarServeurDde = (PX_VAR_SERVEUR_DDE)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, wPosition);
            //
            pVarServeurDde->wPosBxEs = pES->i_identif;
            pVarServeurDde->wNbClientsEnAdvise = 0;
            } // trouve_position_dichotomie
          break;

				default :
          wErreur = 421;
          break;

        } // fin switch
      } // while
    } // existe geo_de et nb_enr != 0

  if (bServeurOk && (!existe_repere (bx_dde_var_serveur)))
    { // il faut toutes les variables de la BD en serveur
    wNbrEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_e_s);
    cree_bloc (bx_dde_var_serveur, sizeof (X_VAR_SERVEUR_DDE), wNbrEnr);
    for (wPosition = 1; wPosition <= wNbrEnr; wPosition++)
      {
      pVarServeurDde = (PX_VAR_SERVEUR_DDE)pointe_enr (szVERIFSource, __LINE__, bx_dde_var_serveur, wPosition);
      pVarServeurDde->wPosBxEs = wPosition;
      pVarServeurDde->wNbClientsEnAdvise = 0;
      } // for
    }

  if (wErreur == 0)
    {
    // Ok
    }
  else
    {
    (*wLigne) = wLigneCour;
    }

  if (wErreur == 0)
    {
    if (existe_repere (b_titre_paragraf_de))
      {
      enleve_bloc (b_titre_paragraf_de);
      }
    if (existe_repere (b_spec_titre_paragraf_de))
      {
      enleve_bloc (b_spec_titre_paragraf_de);
      }
    if (existe_repere (b_dde_e))
      {
      enleve_bloc (b_dde_e);
      }
    if (existe_repere (b_dde_r))
      {
      enleve_bloc (b_dde_r);
      }
    if (existe_repere (b_dde_e_dyn))
      {
      enleve_bloc (b_dde_e_dyn);
      }
    if (existe_repere (b_dde_r_dyn))
      {
      enleve_bloc (b_dde_r_dyn);
      }
    } // wErreur == 0

  return (wErreur);
  }

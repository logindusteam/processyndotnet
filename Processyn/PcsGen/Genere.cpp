 /*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENERE.C 						|
 |   Auteur  : AC							|
 |   Date    : 12/08/96 						|
 |     A r�ecrire completement 					|	|
 |                                                                      |
 +----------------------------------------------------------------------*/
//bloc de compilation de l'application

#include "stdafx.h"
#include "col.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "UStr.h"
#include "MemMan.h"
#include "PathMan.h"
#include "Threads.h"
#include "mem.h"
#include "USem.h"
#include "WGen.h"
#include "FMan.h"
#include "FileMan.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "Pcs_Sys.h"
#include "PcsVarEx.h"
#include "UChrono.h"
#include "dongle.h"
#include "cvt_ul.h"
#include "IdLngLng.h"

#include "tipe_co.h"
#include "tipe_sy.h"
#include "tipe_me.h"
#include "tipe_tb.h"
#include "Driv_dq.h"
#include "tipe_dq.h"
#include "tipe_ap.h"
#include "tipe_de.h"
#include "tipe_tp.h"
#include "tipe_al.h"
#include "tipe_re.h"
#include "G_Objets.h"
#include "G_sys.h"
#include "BdGr.h"
#include "BdAnmGr.h"
#include "IdStrProcessyn.h"
#include "SignatureFichier.h"

#include "tipe_gr.h"

#ifdef c_modbus
  #include "tipe_jb.h"
  #include "gener_jb.h"
#endif

#ifdef c_ap
  #include "tipe_ap.h"
  #include "gener_ap.h"
#endif

#ifdef c_opc
	#include "opc.h"
	#include "OPCError.h"
	#include "TemplateArray.h"
	#include "OPCClient.h"
  #include "tipe_opc.h"
  #include "gener_opc.h"
#endif

#include "gener_de.h"
#include "gener_re.h"
#include "gener_vi.h" //$$ renommer en "gener_gr.h"
#include "gener_gr.h"//$$ renommer en "gener_Animgr.h"
#include "gener_al.h"
#include "gener_co.h"
#include "gener_dq.h"
#include "gener_tp.h"
#include "gener_vs.h"
#include "gener_tb.h"

#include "GenereBD.h"
#include "Verif.h"
#include "genere.h"
VerifInit;



//---------------------------------------------------------------------------
// Pour un driver donn� :
// Recherche ligne apr�s ligne - � partir de la ligne *pdwPosGeoRecherche +1
// jusqu'a trouver la premi�re ES d�clar�e.
// Met egalement a jour les blocs tableaux sur les drivers qui en declarent
//---------------------------------------------------------------------------
static DWORD dwPositionProchaineESGenere
	(
	ID_DESCRIPTEUR IdDes,				// id du driver vis�
	DWORD * pdwPosGeoRecherche,	// position g�o - 1 � partir de laquelle chercher : � mettre � jour lors de la recherche
	DWORD *pos_geo_trouve				// sortie : position g�o de l'ES trouv�e
	) // Renvoie 0 ou la position de l'ES trouvee dans le bloc geo a partir de la position suivant pos geo .
  {
  DWORD dwRetour = 0; // par d�faut rien trouv�

  switch (IdDes)
    {
    case d_systeme :
			{
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_syst))
        {
			  PGEO_SYST val_geo_syst;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_syst);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
          (*pdwPosGeoRecherche)++;
          val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, (*pdwPosGeoRecherche));
          if ((val_geo_syst->taille_tableau == 0))
            {
            dwRetour = val_geo_syst->pos_es;
            bTrouve = TRUE;
            }
          else
            { // tableau
            genere_tableau (val_geo_syst->pos_es, val_geo_syst->pos_init);
            }
          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
			}
      break;

    case d_mem :
			{
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_mem))
        {
			  PGEO_MEM	val_geo_mem;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_mem);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
          (*pdwPosGeoRecherche)++;
          val_geo_mem = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem, (*pdwPosGeoRecherche));
          switch (val_geo_mem->type_memoire)
            {
            case mem_interne_simple :
              dwRetour = val_geo_mem->position_es;
              bTrouve = TRUE;
              break;
            case mem_interne_tableau:
              genere_tableau (val_geo_mem->position_es, val_geo_mem->position_constante);
              break;
            } // case
          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
			}
      break;

    case d_chrono :
			{
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_chrono))
        {
			  PGEO_CHRONO	val_geo_chrono;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_chrono);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
          (*pdwPosGeoRecherche)++;
          val_geo_chrono = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono, (*pdwPosGeoRecherche));
          if ((val_geo_chrono->numero_repere == b_e_s) ||
              (val_geo_chrono->numero_repere == b_metronome))
            {
            dwRetour = val_geo_chrono->pos_es;
            bTrouve = TRUE;
            }
          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
			}
      break;

    case d_disq :
			{
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_disq))
        {
			  PGEO_DISQUE	val_geo_disq;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_disq);
				while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
          (*pdwPosGeoRecherche)++;
          val_geo_disq = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,(*pdwPosGeoRecherche));
          switch (val_geo_disq->numero_repere)
            {
            case b_e_s:
              if (val_geo_disq->longueur != 0)
                {
                genere_tableau (val_geo_disq->pos_es, val_geo_disq->pos_initial);
             		}
              else
                {
             		dwRetour = val_geo_disq->pos_es;
             		bTrouve = TRUE;
             		(*pos_geo_trouve) = (*pdwPosGeoRecherche);
                }
              break;

            case b_titre_paragraf_dq:
							{
						  PSPEC_TITRE_PARAGRAPHE_DISQUE	spec_titre_paragraf_dq = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo_disq->pos_specif_paragraf);
						  PTITRE_PARAGRAPHE	titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,val_geo_disq->pos_paragraf);
              switch (titre_paragraf->IdParagraphe)
                {
                case ecrit_dq:
                case lit_dq:
                case enr_dq:
                  dwRetour = spec_titre_paragraf_dq->pos_es;
                  (*pos_geo_trouve) = (*pdwPosGeoRecherche);
                  bTrouve = TRUE;
                  break;

                default:
                  break;
                } // switch tipe
							}
              break;

            default:
              break;
            } // switch numero repere
          } // while
        } // bloc existe
			}
      break;

    case d_repete :
			{
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_re))
        {
			  PGEO_REPETE	val_geo_re;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_re);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
   				(*pdwPosGeoRecherche)++;
   				val_geo_re = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,(*pdwPosGeoRecherche));
   				if ((val_geo_re->numero_repere == b_e_s))
   					{
   					dwRetour = val_geo_re->pos_es;
   					bTrouve = TRUE;
   					}
          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
			}
      break;

    case d_graf :
			{
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_vdi))
        {
			  PGEO_VDI val_geo_vdi;
			  PVDI_E_LOG ads_vdi_e_log;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_vdi);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
   				(*pdwPosGeoRecherche)++;
   				val_geo_vdi = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi, (*pdwPosGeoRecherche));
   				switch (val_geo_vdi->numero_repere)
						{
   					case b_vdi_e_log_nv1 :
   						ads_vdi_e_log = (PVDI_E_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_log_nv1,val_geo_vdi->pos_specif_es);
   						switch (ads_vdi_e_log->selecteur)
								{
								case EL_TRANSPARENTE:
								case EL_CONTROL_CHECK:
								case EL_CONTROL_BOUTON_PULSE:
		   						dwRetour = val_geo_vdi->pos_es;
									bTrouve = TRUE;
									break;
								default:
									break;
   							}
							break;
   					case b_vdi_e_mes:
   					case b_vdi_e_num:
   					case b_vdi_courbe_temps:
   					case b_vdi_courbe_cmd :
						case b_vdi_control_static:
   						dwRetour = val_geo_vdi->pos_es;
   						bTrouve = TRUE;
							break;
						default:
							break;
   					} // switch num repere

          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
			}
      break;

    case d_dde :
			{
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_dde))
        {
			  PGEO_DDE	val_geo_dde;
				tc_dde_e* ads_dde_e;
				tc_dde_e_dyn*ads_dde_e_dyn;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_dde);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
          (*pdwPosGeoRecherche)++;
          val_geo_dde = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde,(*pdwPosGeoRecherche));
          switch (val_geo_dde->wNumeroRepere)
            {
            case b_dde_e :
              ads_dde_e = (tc_dde_e*)pointe_enr (szVERIFSource, __LINE__, b_dde_e, val_geo_dde->wPosSpecifEs);
              if (ads_dde_e->wTaille == 0)
                { // c'est une entree simple
                dwRetour = val_geo_dde->wPosEs;
                bTrouve = TRUE;
              	}
              else
                { // c'est une variable tableau
                genere_tableau (val_geo_dde->wPosEs, val_geo_dde->wPosInitial);
                }
              break;

            case b_dde_e_dyn :
              ads_dde_e_dyn = (tc_dde_e_dyn*)pointe_enr (szVERIFSource, __LINE__, b_dde_e_dyn, val_geo_dde->wPosSpecifEs);
              if (ads_dde_e_dyn->wTaille == 0)
                { // c'est une entree simple
                dwRetour = val_geo_dde->wPosEs;
                bTrouve = TRUE;
              	}
              else
                { // c'est une variable tableau
                genere_tableau (val_geo_dde->wPosEs, val_geo_dde->wPosInitial);
                }
              break;

            default:
              break;
            } // case
          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
			}
      break;

    case d_al :
      // pas besoin car ces variables sont deja ds la BD
      break;

    case d_jbus :
			{
#ifdef c_modbus
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_jbus))
        {
				PGEO_MODBUS	val_geo_jbus;
				tc_jbus_tab*val_specif_jbus;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_jbus);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
          (*pdwPosGeoRecherche)++;
          val_geo_jbus = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, (*pdwPosGeoRecherche));
          switch (val_geo_jbus->numero_repere)
            {
            case b_jbus_e:
            case b_jbus_e_mes:
            case b_jbus_s:
            case b_jbus_s_mes :
              dwRetour = val_geo_jbus->pos_es;
              bTrouve = TRUE;
              break;
            case b_jbus_s_tab:
              val_specif_jbus = (tc_jbus_tab*)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_tab,val_geo_jbus->pos_specif_es);
              genere_tableau (val_geo_jbus->pos_es,
                              val_geo_jbus->pos_initial);
              break;
            case b_jbus_e_tab:
              val_specif_jbus = (tc_jbus_tab*)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_tab,val_geo_jbus->pos_specif_es);
              genere_tableau (val_geo_jbus->pos_es,
                              val_geo_jbus->pos_initial);
              break;
            default:
              break;
            } // switch num repere
          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
#endif
			}
      break; // d_jbus

    case d_ap :
			{
#ifdef c_ap
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_ap))
        {
				PGEO_APPLICOM	val_geo_ap;
				C_APPLICOM_TAB*val_specif_ap_tab;

        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_ap);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
          (*pdwPosGeoRecherche)++;
          val_geo_ap = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap,(*pdwPosGeoRecherche));
          switch (val_geo_ap->numero_repere)
            {
            case b_ap_e:
            case b_ap_e_mes:
            case b_ap_s:
            case b_ap_s_mes:
            case b_ap_fct_cyc:
              dwRetour = val_geo_ap->pos_es;
              bTrouve = TRUE;
              break;
            case b_ap_e_tab:
              val_specif_ap_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab,val_geo_ap->pos_specif_es);
              genere_tableau (val_geo_ap->pos_es,
                              val_geo_ap->pos_initial);
              break;
            case b_ap_s_tab:
              val_specif_ap_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, b_ap_s_tab,val_geo_ap->pos_specif_es);
              genere_tableau (val_geo_ap->pos_es,
                              val_geo_ap->pos_initial);
              break;
            default:
              break;
            } // switch num repere
          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
			}
#endif
      break; // d_ap

    case d_opc :
			{
#ifdef c_opc
      BOOL bTrouve = FALSE;
      if (existe_repere (b_geo_opc))
        {
        DWORD dwNbEnr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_opc);
        while (((*pdwPosGeoRecherche) < dwNbEnr) && (!bTrouve))
          {
          (*pdwPosGeoRecherche)++;
					PGEO_OPC pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc,(*pdwPosGeoRecherche));
          switch (pGeoOPC->numero_repere)
            {
						case b_opc_item:
							{
							// Variable d�lar�e ici ?
							if (pGeoOPC->pos_initial)
								{
								// oui => variable trouv�e : variable tableau ?
									{
									if (dwTailleES(pGeoOPC->dwIdES))
										{
										// oui
										genere_tableau (pGeoOPC->dwIdES, pGeoOPC->pos_initial);
										}
									else
										{
										// non
										dwRetour = pGeoOPC->dwIdES;
										bTrouve = TRUE;
										}
									}
								}
							}
							break;
/*
            case b_opc_e:
            case b_opc_e_mes:
            case b_opc_s:
            case b_opc_s_mes:
//          case b_opc_fct_cyc:
							{
              dwRetour = pGeoOPC->dwIdES;
              bTrouve = TRUE;
							}
              break;
            case b_opc_e_tab:
							{
							C_OPC_TAB*val_specif_opc_tab;
              val_specif_opc_tab = (C_OPC_TAB*)pointe_enr (szVERIFSource, __LINE__, b_opc_e_tab,pGeoOPC->pos_specif_es);
              genere_tableau (pGeoOPC->dwIdES, pGeoOPC->pos_initial);
							}
              break;
            case b_opc_s_tab:
							{
							C_OPC_TAB*val_specif_opc_tab;
              val_specif_opc_tab = (C_OPC_TAB*)pointe_enr (szVERIFSource, __LINE__, b_opc_s_tab,pGeoOPC->pos_specif_es);
              genere_tableau (pGeoOPC->dwIdES, pGeoOPC->pos_initial);
							}
              break;
*/
            } // switch num repere
          } // while
        (*pos_geo_trouve) = (*pdwPosGeoRecherche);
        } // bloc existe
#endif
			}
      break; // d_opc
    } // switch (IdDes)

  return dwRetour;
  } // dwPositionProchaineESGenere

//--------------------------------------------------------------------
// Classe les entrees/sorties memoires par genre,driver,sens en fabriquant une table d'index.
// Elabore egalement une table ou sont stockees les valeurs limites pour
// chaque driver et calcule le nombre d'entit� (logique,numerique,message);
static void organise_bloc_es (void)
  {
	typedef struct
		{
		BOOL bEntree;
		BOOL bSortie;
		} t_driver_es;
	t_driver_es tab_driver_es [dernier_driver];

  ORGANISATION_ES OrganisationES;
  PENTREE_SORTIE es;
  DWORD pos_a_inserer;
  char chaine[10];
  BOOL bEntreesExistent;
  BOOL bSortiesExistent;
  PDWORD periph;
  ID_DESCRIPTEUR wDriver;

  // initialisation
  if (!existe_repere (b_organise_es))
    {
    cree_bloc (b_organise_es, sizeof (ORGANISATION_ES), 0);
    }
  DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_organise_es);
  if (nbr_enr != 0)
    {
    enleve_enr (szVERIFSource, __LINE__, nbr_enr, b_organise_es, 1);
    }
  if (!existe_repere (b_e_s))
    {
    abort_genere ("err appel genere : b_e_s inexistant",0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_e_s);

  ContexteGen.nbr_logique = 0;
  ContexteGen.nbr_numerique = 0;
  ContexteGen.nbr_message = 0;
  
	// Boucle de driver en driver
  for (wDriver = premier_driver; (wDriver <= dernier_driver); wDriver = (ID_DESCRIPTEUR)((DWORD)wDriver+1))
    {
		tab_driver_es [wDriver-1].bEntree = FALSE;
		tab_driver_es [wDriver-1].bSortie = FALSE;
    if (wDriver != d_code)
      {
			DWORD position_geo_debut = 0;
			DWORD position_geo_trouve;

			// boucle  de parcours des entr�e sorties pour le driver courant
      DWORD dwPosESTrouvee = dwPositionProchaineESGenere (wDriver, &position_geo_debut, &position_geo_trouve);
      while (dwPosESTrouvee != 0)
        {
        es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPosESTrouvee);
				ID_MOT_RESERVE wSens = es->sens;
				ID_MOT_RESERVE wGenre = es->genre;

        // elaboration cle
        OrganisationES.pos_geo = position_geo_trouve;
        OrganisationES.driver = wDriver;
        StrCopy (OrganisationES.cle, "000000000");
        StrDWordToStr (chaine, es->genre);
        MemCopy (&(OrganisationES.cle[3-StrLength(chaine)]), chaine, StrLength(chaine));
        StrDWordToStr (chaine, wDriver);
        MemCopy (&(OrganisationES.cle[6-StrLength(chaine)]), chaine, StrLength(chaine));
        StrDWordToStr (chaine, es->sens);
        MemCopy (&(OrganisationES.cle[9-StrLength(chaine)]), chaine, StrLength(chaine));

        // recherche position d'insertion
        bTrouveChaineDansOrganiseES (OrganisationES.cle, &pos_a_inserer);
 
        // mise a jour du bloc d'organisation
        insere_enr (szVERIFSource, __LINE__, 1, b_organise_es, pos_a_inserer);
        PORGANISATION_ES pOrganisationES = (PORGANISATION_ES)pointe_enr (szVERIFSource, __LINE__, b_organise_es, pos_a_inserer);
        *pOrganisationES = OrganisationES;

        // mise a jour Table driver avec ES
        switch (wSens)
          {
          case c_res_e :
						tab_driver_es [wDriver-1].bEntree = TRUE;
            break;
          case c_res_s :
						tab_driver_es [wDriver-1].bSortie = TRUE;
            break;
          case c_res_e_et_s :
						tab_driver_es [wDriver-1].bEntree = TRUE;
						tab_driver_es [wDriver-1].bSortie = TRUE;
            break;
          case c_res_cste_re:
          case c_res_tab_re:
          case c_res_variable_re:
            break;
          default:
            abort_genere ("erreur syst : cas sens inconnu tri",0);
            break;
          } // switch sens

        // mise a jour Nbr de variable hors tableau
        switch (wGenre)
          {
          case c_res_logique :
						ContexteGen.nbr_logique++;
            break;
          case c_res_numerique :
						ContexteGen.nbr_numerique++;
            break;
          case c_res_message :
						ContexteGen.nbr_message++;
            break;
          default:
            abort_genere ("erreur syst : cas genre inconnu ",0);
            break;
          } // switch sens
				
        dwPosESTrouvee = dwPositionProchaineESGenere (wDriver, &position_geo_debut, &position_geo_trouve);
        } // while (dwPosESTrouvee != 0)
      }
    } // for wDriver

  // Mise � jour des bloc sp�cifiant les drivers ayant des entrees AUTRES QUE LES ES simples (ex :tableaux)...
	// et ceux ayant des sorties """
	// boucle de driver en driver pour savoir pour chacun s'il a des entr�es puis des sorties suppl�mentaires
  for (wDriver = premier_driver; (wDriver <= dernier_driver); wDriver = (ID_DESCRIPTEUR)((DWORD)wDriver+1))
    {
    if ((wDriver != d_code) && (wDriver != d_mem))
      {
      bEntreesExistent = tab_driver_es [wDriver-1].bEntree;

      if (wDriver == d_dde)
        {
				/*
        if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_dde_e) != 0)
          {
          bEntreesExistent = TRUE;
          }
        if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_dde_e_dyn) != 0)
          {
          bEntreesExistent = TRUE;
          }
				*/
        bEntreesExistent = TRUE;
				}

			if (wDriver == d_graf)
        {
				if (existe_repere(B_GEO_PAGES_GR) && (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, B_PAGES_GR) != 0))
					{
					bEntreesExistent = TRUE;
					}
        }

      if (wDriver == d_al)
        {
				if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_alarme) != 0)
					{
					bEntreesExistent = TRUE;
					}
				}

#ifdef c_modbus
      if (wDriver == d_jbus)
        {
        if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_jbus_e_tab) != 0)
          {
          bEntreesExistent = TRUE;
          }
				}
#endif

#ifdef c_ap
			if (wDriver == d_ap)
        {
        if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_ap_e_tab) !=0)
          {
          bEntreesExistent = TRUE;
          }
				}
#endif

#ifdef c_opc
			if (wDriver == d_opc)
        {
				// OPC doit d�tecter s'il existe des variables en entr�es autres que ES normales d�ja d�tect�es ex : tableau
				// $$ manque de finesse
				if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_item) != 0)
					{
					bEntreesExistent = TRUE;
					}
/*
        if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_e_tab) !=0)
          {
          bEntreesExistent = TRUE;
          }
*/ 
				}
#endif

			// Des entr�es existent ?
      if (bEntreesExistent)
        {
				// oui => ajoute les au bloc de la liste des p�riph�riques d'entree
        if (!existe_repere (b_periph_entree))
          {
          cree_bloc (b_periph_entree, sizeof (DWORD),0);
          }
        insere_enr (szVERIFSource, __LINE__, 1, b_periph_entree, 1);
        periph = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_periph_entree,1);
        *periph = wDriver;
        }

			// D�tecte si sorties suppl�mentaires
      bSortiesExistent = tab_driver_es [wDriver-1].bSortie;

      if (wDriver == d_dde)
        {
				bSortiesExistent = TRUE;
        }

      if (wDriver == d_al)
        {
				if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_alarme) != 0)
					{
					bSortiesExistent = TRUE;
					}
        }

      if (wDriver == d_graf)
        {
				if ((existe_repere(B_GEO_PAGES_GR)) && (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, B_PAGES_GR) != 0))
					{
					bSortiesExistent = TRUE;
					}
        }

      #ifdef c_modbus
        if (wDriver == d_jbus)
          {
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_jbus_s_tab) != 0)
            {
            bSortiesExistent = TRUE;
            }
					}
      #endif

#ifdef c_ap
				if (wDriver == d_ap)
          {
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_ap_s_tab) != 0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_ap_s_tab_reflet) != 0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_ap_s_tab_reflet_mes) != 0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_ap_s_reflet) != 0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_ap_s_reflet_mes) !=0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_ap_fct_cyc) != 0)
            {
            bSortiesExistent = TRUE;
            }
          }
#endif

#ifdef c_opc
				if (wDriver == d_opc)
          {
					// OPC doit d�tecter s'il existe des variables en sortie autres que ES normales d�ja d�tect�es ex : tableau
					// $$ manque de finesse
					if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_item) != 0)
						{
						bSortiesExistent = TRUE;
						}
/*
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_s_tab) != 0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_s_tab_reflet) != 0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_s_tab_reflet_mes) != 0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_s_reflet) != 0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_s_reflet_mes) !=0)
            {
            bSortiesExistent = TRUE;
            }
          if (dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_opc_fct_cyc) != 0)
            {
            bSortiesExistent = TRUE;
            }
*/
          }
#endif

			if (bSortiesExistent)
        {
        if (!existe_repere (b_periph_sortie))
          {
          cree_bloc (b_periph_sortie,sizeof(DWORD),0);
          }
        insere_enr (szVERIFSource, __LINE__, 1, b_periph_sortie, 1);
        periph = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_periph_sortie, 1);
        *periph = wDriver;
        }
      }  // wDriver != d_code et d_mem
    } // for (wDriver...
  } // organise_bloc_es

//--------------------------------------------------------------------------
static void initialise_bx_es (void)
  {
  if (existe_repere (bn_identif))
    {
		DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bn_identif);
    cree_bloc (bx_e_s, sizeof(X_E_S), nbr_enr);
    for (DWORD dw = 1; (dw <= nbr_enr); dw++)
      {
			PIDENTIF	identif = (PIDENTIF)pointe_enr (szVERIFSource, __LINE__, bn_identif, dw);
			PX_E_S	pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, dw);
      StrCopy (pxES->pszNomVar, identif->nom);
      }
    }
  } // initialise_bx_es

// --------------------------------------------------------------------------
// Elabore les blocs cour et mem en les initialisant avec les valeurs initiales
// de la base de donnees.
// vide les blocs b_e_s et b_organise_es
// --------------------------------------------------------------------------
static void initialise_bloc_es (DWORD *pos_organise, DWORD b_cour, DWORD b_mem,
                                DWORD bx_e_s_cour, DWORD nb_es, DWORD genre)
  {
  DWORD position_es;
  DWORD pos_init;
  DWORD index;
  ID_MOT_RESERVE numero_fct;
  PORGANISATION_ES pOrganisationES;
  PENTREE_SORTIE es;
  PX_E_S	pxES;
  char valeur_initiale[c_nb_car_ex_mes];
  FLOAT valeur;
  PBOOL cour_log;
  PBOOL mem_log;
  PFLOAT cour_num;
  PFLOAT mem_num;
  PSTR cour_mes;
  PSTR mem_mes;
  UL ul;
  PGEO_MEM	val_geo_mem;
  PGEO_SYST val_geo_syst;
  PGEO_CHRONO val_geo_chrono;
  PGEO_DISQUE	val_geo_disq;
  PGEO_DDE	val_geo_dde;
  PSPEC_TITRE_PARAGRAPHE_DISQUE	spec_titre_paragraf;
  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_VDI val_geo_vdi;
  PGEO_REPETE	val_geo_re;
  PX_E_S_COUR pxESCour;

  #ifdef c_modbus
    PGEO_MODBUS	val_geo_jbus;
  #endif
  #ifdef c_ap
    PGEO_APPLICOM	val_geo_ap;
  #endif
  #ifdef c_opc
    PGEO_OPC	pGeoOPC; //$$
  #endif

  index = 0;
  while ((*pos_organise) < nb_es)
    {
    (*pos_organise)++;
    index++;
    insere_enr (szVERIFSource, __LINE__, 1, b_cour, index);
    insere_enr (szVERIFSource, __LINE__, 1, b_mem, index);
    insere_enr (szVERIFSource, __LINE__, 1, bx_e_s_cour, index);
    pOrganisationES = (PORGANISATION_ES)pointe_enr (szVERIFSource, __LINE__, b_organise_es, (*pos_organise));
    switch (pOrganisationES->driver)
      {
      case d_systeme:
        val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, pOrganisationES->pos_geo);
        position_es = val_geo_syst->pos_es;
        pos_init = val_geo_syst->pos_init;
        break;

      case d_chrono:
        val_geo_chrono = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono,pOrganisationES->pos_geo);
        position_es = val_geo_chrono->pos_es;
        pos_init = val_geo_chrono->pos_initial;
        break;

      case d_mem:
        val_geo_mem = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem,pOrganisationES->pos_geo);
        position_es = val_geo_mem->position_es;
        pos_init = val_geo_mem->position_constante;
        break;

      case d_dde:
        val_geo_dde = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde,pOrganisationES->pos_geo);
        position_es = val_geo_dde->wPosEs;
        pos_init = val_geo_dde->wPosInitial;
        break;

      case d_graf:
        val_geo_vdi = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,pOrganisationES->pos_geo);
        position_es = val_geo_vdi->pos_es;
        pos_init = val_geo_vdi->pos_initial;
        break;

      case d_repete:
        val_geo_re = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,pOrganisationES->pos_geo);
        position_es = val_geo_re->pos_es;
        pos_init = val_geo_re->pos_initial;
        break;

      case d_disq:
        val_geo_disq = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,pOrganisationES->pos_geo);
        switch (val_geo_disq->numero_repere)
          {
          case b_e_s:
            position_es = val_geo_disq->pos_es;
            pos_init = val_geo_disq->pos_initial;
            break;

          case b_titre_paragraf_dq :
            titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,val_geo_disq->pos_paragraf);
            spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo_disq->pos_specif_paragraf);
            switch (titre_paragraf->IdParagraphe)
              {
              case ecrit_dq:
              case lit_dq:
              case enr_dq :
                position_es = spec_titre_paragraf->pos_es;
                pos_init = spec_titre_paragraf->pos_init;
                break;

              default:
                break;
              } // switch tipe paragraf
            break;

						default:
            break;
          } // switch num repere
        break;

      case d_al:
        break;

      case d_jbus:
        #ifdef c_modbus
          val_geo_jbus = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus,pOrganisationES->pos_geo);
          position_es = val_geo_jbus->pos_es;
          pos_init = val_geo_jbus->pos_initial;
        #endif
        break;

      case d_ap:
#ifdef c_ap
				val_geo_ap = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap,pOrganisationES->pos_geo);
				position_es = val_geo_ap->pos_es;
				pos_init = val_geo_ap->pos_initial;
#endif
        break;
				
      case d_opc:
#ifdef c_opc
				//$$OPC VerifWarningExit;
				pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc,pOrganisationES->pos_geo);
				position_es = pGeoOPC->dwIdES;
				pos_init = pGeoOPC->pos_initial;
#endif
        break;
				
      default:
        abort_genere ("cas driver inconnu",0);
        break;

      } // switch driver

    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
    pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, es->i_identif);
    pxES->wPosEx = index;
    pxES->wBlocGenre = b_cour;
    if ((es->sens == c_res_variable_ta_e) || (es->sens == c_res_variable_ta_s) || (es->sens == c_res_variable_ta_es))
      {
      pxES->wTaille = es->taille;
      }
    else
      {
      pxES->wTaille = 0;
      }
    pxESCour = (PX_E_S_COUR)pointe_enr (szVERIFSource, __LINE__, bx_e_s_cour, index);
    pxESCour->dwPosDansBx_e_s = es->i_identif;

    StrSetNull (valeur_initiale);
    action_cst_bd_c (CONSULTE_LIGNE, valeur_initiale, pos_init);
    action_cst_bd_c (SUPPRIME_LIGNE, valeur_initiale, pos_init);
    switch (genre)
      {
      case c_res_logique :
        cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour, index);
        mem_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_mem, index);
        StrCopy (ul.show, valeur_initiale);
        ul.genre = g_id;
        if (!bReconnaitMotReserve (ul.show, &numero_fct))
          abort_genere ("err stock init log",0 );
        switch (numero_fct)
          {
          case c_res_un :
            *cour_log = TRUE;
            *mem_log = TRUE;
            break;
          case c_res_zero :
            *cour_log = FALSE;
            *mem_log = FALSE;
            break;
          default:
            abort_genere ("erreur syst1 : cas initial inconnu",0);
            break;
          } // switch numero_fct
        break;

      case c_res_numerique :
        cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour,index);
        mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_mem,index);
        if (StrToFLOAT (&valeur, valeur_initiale))
          {
          *cour_num = valeur;
          *mem_num = valeur;
          }
        else
          {
          abort_genere ("erreur syst2 : cas initial inconnu",0);
          }
        break;

      case c_res_message :
        StrDeleteDoubleQuotation (valeur_initiale);
        cour_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour,index);
        mem_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_mem,index);
        StrCopy (cour_mes, valeur_initiale);
        StrCopy (mem_mes, valeur_initiale);
        break;

      default:
        abort_genere ("erreur syst3 cas genre inconnu",0);
        break;
      } // switch genre
    } // while pos_organise < nb_es
  } // initialise_bloc_es

// ---------------------------------------------------------------------------
// Transforme la base de donn�e de configuration en base de donn�ee d'exploitation
// (g�n�ration)
// ---------------------------------------------------------------------------
DWORD dwGenereApplic
	(
	DWORD * pdwNLigne,			// En cas d'erreur, met � jour la ligne ayant provoqu� l'erreur
	PCSTR pszNomApplication	// Nom de l'application (utilis� pour la sauvegarde)
	) // Renvoie 0 si Ok sinon un num�ro d'erreur de g�n�ration
  {
  DWORD erreur;
  DWORD pos_organise;
  DWORD dw;

	// initialise la base de donn�es d'ex�cution :

	// bloc ES en ex�cution
  initialise_bx_es (); // AVANT enleve bloc bn_identif
  MiseAJourTaillesTableauxVarSysGr ();

  organise_bloc_es ();

  // mise a jour de l'index dans BX tableau qui permet de pointer sur les b_cour
  // cet index est renvoy� aux differents generateurs par recherche_index_tableau.
  maj_index_tableau ();
  erreur = 0;

  // G�n�ration du descripteur des variables systeme
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
    AfficheDescripteurDansStatutGen (d_systeme, TRUE);
    erreur = genere_va_syst (pdwNLigne);
    *pdwNLigne = 1;
    }

  // G�n�ration du descripteur temps (attention position avant code indispensable)
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
    AfficheDescripteurDansStatutGen (d_chrono, TRUE);
    erreur = genere_cn (pdwNLigne);
    }

  // G�n�ration du descripteur de structure repetitive (attention position avant code indispensable)
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
    AfficheDescripteurDansStatutGen (d_repete, TRUE);
    erreur = genere_re (pdwNLigne);
    }

  // G�n�ration du descripteur code
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
    AfficheDescripteurDansStatutGen (d_code, TRUE);
    erreur = genere_co (pdwNLigne);
    }

  // G�n�ration du descripteur D D E
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
    AfficheDescripteurDansStatutGen (d_dde, TRUE);
    erreur = genere_de (pdwNLigne);
    }

  // G�n�ration du descripteur alarmes
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
    AfficheDescripteurDansStatutGen (d_al, TRUE);
    erreur = genere_al (pdwNLigne);
    }

  // G�n�ration du descripteur disque
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
    AfficheDescripteurDansStatutGen (d_disq, TRUE);
    erreur = genere_disq (pdwNLigne);
    }

  // G�n�ration du descripteur graphisme
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
    AfficheDescripteurDansStatutGen (d_graf, TRUE);
    erreur = genere_vi (pdwNLigne);
    // ATTENTION le bloc b_organise_cle
    // ne doit plus �tre modifi�
    // jusqu'a reorganisation.
    }

  // G�n�ration du descripteur modbus
#ifdef c_modbus
    if ((erreur == 0) && (!bAbandonnerCompil))
      {
      AfficheDescripteurDansStatutGen (d_jbus, TRUE);
      erreur = genere_jb (pdwNLigne);
      }
#endif

  // G�n�ration du descripteur Applicom
#ifdef c_ap
    if ((erreur == 0) && (!bAbandonnerCompil))
      {
      AfficheDescripteurDansStatutGen (d_ap, TRUE);
      erreur = genere_ap (pdwNLigne);
      }
#endif

  // G�n�ration du descripteur OPC
#ifdef c_opc
    if ((erreur == 0) && (!bAbandonnerCompil))
      {
      AfficheDescripteurDansStatutGen (d_opc, TRUE);
      erreur = genere_opc (pdwNLigne);
      }
#endif

  //---------------------------------------------------------
	// Fin de la g�n�ration : optimise la base de donn�es d'ex�cution
  //---------------------------------------------------------
  AfficheDescripteurDansStatutGen (d_systeme, TRUE);

	// peut on continuer ?
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
		// Suppression des guillemets des constantes
    for (dw = 1; (dw <= nb_enregistrements (szVERIFSource, __LINE__, bx_mes)); dw++)
      {
		  PSTR pszConstante = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_mes, dw);
			EnleveGuillemetsMessage (pszConstante);
			//	erreur = IDSERR_LES_GUILLEMETS_DOIVENT_TOUJOURS_ETRE_PRESENTS;
/*
			// saute le prmier guillemet �ventuel
			if (*pszOrigine == '"')
				pszOrigine++;

			// longueur = nb cars + 0 terminateur - dernier guillemet
			int nLen = StrLength (pszOrigine);

			// coupe l'�ventuel dernier guillemet
			if (pszOrigine [nLen-1]== '"')
				{
				nLen--;
				pszOrigine [nLen]= '\0';
				}

			for (int nCar = 0; nCar <= nLen; nCar++)
				{
				//caract�re guillemet ?
				if (*pszOrigine == '"')
					{
					// oui => passe au caract�re suivant
					pszOrigine ++;
					nCar ++;
					// lui m�me guillemet ?
					if (*pszOrigine == '"')
						{
						// oui => recopie le
						*pszRecopie = *pszOrigine;
						pszRecopie ++;
						pszOrigine ++;
						}
					}
				else
					{
					// non => recopie le et passe au suivant
					*pszRecopie = *pszOrigine;
					pszOrigine ++;
					pszRecopie ++;
					}
				} // for 
*/
      //StrDeleteDoubleQuotation (valeur_constante);
      } // for (dw = 1; (dw <= nb_enregistrements (szVERIFSource, __LINE__, bx_mes)); dw++)
    (*pdwNLigne) = 1;
    } // if ((erreur == 0) && (!bAbandonnerCompil))

  // Initialisation des blocs n�cessaires � l'ex�cution
  pos_organise = 0;
  if (!existe_repere (b_cour_log)) cree_bloc (b_cour_log,sizeof (BOOL),0);
  if (!existe_repere (bva_mem_log) ) cree_bloc (bva_mem_log,sizeof (BOOL),0);
  if (!existe_repere (bx_e_s_cour_log) ) cree_bloc (bx_e_s_cour_log,sizeof (X_E_S_COUR),0);

  if (!existe_repere (b_cour_num) ) cree_bloc (b_cour_num,sizeof (FLOAT),0);
  if (!existe_repere (bva_mem_num) ) cree_bloc (bva_mem_num,sizeof (FLOAT),0);
  if (!existe_repere (bx_e_s_cour_num) ) cree_bloc (bx_e_s_cour_num,sizeof (X_E_S_COUR),0);

  if (!existe_repere (b_cour_mes) ) cree_bloc (b_cour_mes,c_nb_car_ex_mes,0);
  if (!existe_repere (bva_mem_mes) ) cree_bloc (bva_mem_mes,c_nb_car_ex_mes,0);
  if (!existe_repere (bx_e_s_cour_mes) ) cree_bloc (bx_e_s_cour_mes,sizeof (X_E_S_COUR),0);

  initialise_bloc_es (&pos_organise, b_cour_log, bva_mem_log, bx_e_s_cour_log,
                      ContexteGen.nbr_logique,c_res_logique);
  initialise_bloc_es (&pos_organise, b_cour_num, bva_mem_num, bx_e_s_cour_num,
                      ContexteGen.nbr_logique + ContexteGen.nbr_numerique,c_res_numerique);
  initialise_bloc_es (&pos_organise, b_cour_mes, bva_mem_mes, bx_e_s_cour_mes,
                      ContexteGen.nbr_logique + ContexteGen.nbr_numerique + ContexteGen.nbr_message, c_res_message);

  // Suppression des blocs de configuration devenus inutiles
  enleve_bloc (b_organise_es);
  enleve_bloc (b_geo_mem);
  enleve_bloc (b_geo_dde);
  enleve_bloc (b_geo_syst);
  enleve_bloc (b_represente_syst);
  enleve_bloc (b_geo_disq);
  enleve_bloc (b_geo_chrono);
  enleve_bloc (b_spec_titre_paragraf_dq);
  enleve_bloc (b_titre_paragraf_dq);
  enleve_bloc (b_geo_vdi);
  //enleve_bloc (b_geo_dlg);
  enleve_bloc (b_geo_al);
  enleve_bloc (b_geo_re);
  //enleve_bloc (b_spec_titre_paragraf_dlg);
  //enleve_bloc (b_dlg_ctrl);

  #ifdef c_modbus
    enleve_bloc (b_geo_jbus);
  #endif
  #ifdef c_ap
    enleve_bloc (b_geo_ap);
  #endif
  #ifdef c_opc
    enleve_bloc (b_geo_opc);
  #endif

  initialisation_tableau ();

	// creation et initialisation des blocs contenant l'indicateur modifi�
	// attention toujours appres initialisation_tableau
  CPcsVarEx::CreeBlocsExValeurModifie ();
	CPcsVarEx::ResetBlocsExValeurModifie ();

  // Restauration vdi ($$ ???)
  maj_init_sys_gr ();

  // Suppression bloc generaux
  enleve_bloc (b_tableau);
  //enleve_bloc (b_e_s);
  enleve_bloc (bn_identif);
  enleve_bloc (b_pt_constant);
  enleve_bloc (bn_constant);
  enleve_bloc (b_cst_nom);

  //-------------------------------
  // Sauvegarde de l'objet g�n�r�
  //-------------------------------
  if ((erreur == 0) && (!bAbandonnerCompil))
    {
	  HFIC repere_fic;
	  char nom [MAX_PATH];

    encrypte_xpc ();
    StrCopy (nom, pszNomApplication);
    pszNomAjouteExt (nom, MAX_PATH, EXTENSION_A_FROID);

		AfficheInformationDansStatutGen(c_inf_enregistrement_en_cours);
		if (uFileOuvre (&repere_fic, nom, 1, CFman::OF_OUVRE_OU_CREE) == 0)
			{
			uFileFerme (&repere_fic);
			CSignatureFichier SignatureFichier(SIGNATURE_FORMAT_XPC_EN_COURS);
			SignatureFichier.DBPCSSetSignature();

			sauve_memoire (nom);
			AfficheInformationDansStatutGen(c_inf_Ok);
			}
		else
			{
			*pdwNLigne = 0;
			erreur = IDSERR_SAUVEGARDE_DU_FICHIER_IMPOSSIBLE_VERIFIER_LE_NOM_ET_OU_LES_ATTRIBUTS;
			}
    }
	Beep (100,100);

  return erreur;
  } // dwGenereApplic

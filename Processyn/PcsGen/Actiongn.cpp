/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : actiongn.c                                               |
 |   Auteur  : LM					        	|
 |   Date    : 07/12/92					        	|
 |   Version : 3.20							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "USem.h"
#include "QueueMan.h"
#include "UStr.h"
#include "Threads.h"
#include "Appli.h"
#include "Mem.h"
#include "MemMan.h"
#include "FMan.h"
#include "LireLng.h"
#include "initdesc.h"
#include "WGen.h"
#include "genere.h"
#include "PathMan.h" 
#include "DocMan.h" 
#include "CmdPcs.h" 
#include "IdLngLng.h" 
#include "Verif.h"

#include "actiongn.h"

VerifInit;

typedef struct
    {
    DWORD tipe;
    char lettre;
    char chaine[MAX_PATH];
    BOOL booleen;
    int entier16;
    DWORD mot;
    } t_action;

static HQUEUE queue_action;

// -----------------------------------------------------------------------------
// envoie un message au thread ayant cr�� la fen�tre principale de l'application
static BOOL bPostMessageThreadHwndAppli (UINT msg, WPARAM wparam, LPARAM lparam)
	{
	DWORD dwThreadId = GetWindowThreadProcessId (Appli.hwnd, NULL);
	return PostThreadMessage (dwThreadId, msg, wparam, lparam);
	}

// ----------------------------------------------------------------------
//
void creer_action (void)
  {
  Verif (queue_action = hCreeQueue ("ActionsGen"));
  }

// ----------------------------------------------------------------------
//
int ajouter_action (DWORD type_action, t_param_action *param)
  {
  int rretour = 0;
  t_action *paction = (t_action *)pMemAlloue (sizeof (t_action));

  paction->tipe = type_action;
  switch (type_action)
    {
    case action_choisit_application:
      StrCopy (paction->chaine, param->chaine);
      break;

    case action_genere:
      StrCopy (paction->chaine, param->chaine);
      break;

    case action_quitte:
      break;

    default: // pas de parametre
      break;
    }
  Verif (bEcritQueue (queue_action, 0, sizeof (t_action), paction));
  return (rretour);
  }

// ----------------------------------------------------------------------
//
int executer_action (void)
  {
  int rretour;
  DWORD ligne;
  DWORD erreur = 0;
  DWORD taille;
  t_action *paction;

	// lecture de l'action � traiter
  Verif (uLitQueue (queue_action, NULL, &taille, (PVOID *)&paction, TRUE, NULL) == 0);
  rretour = paction->tipe;
  switch (paction->tipe)
    {
    case action_aide :
      break;

    case action_choisit_application:
			{
      if (!bQuitterGen)
        {
				bSetPathNameDoc  (paction->chaine);
        ::PostMessage (Appli.hwnd, WM_SET_TEXTE, 0L, 0L); //$$ v�rifier dest et suivants
				AfficheInformationDansStatutGen(c_inf_Ok);
        }
			}
      break;

    case action_genere:
			{
		  char nom_fic[MAX_PATH];

      if (!bQuitterGen)
        {
				pszCreeNameExt (nom_fic, MAX_PATH, paction->chaine, EXTENSION_SOURCE);
				if (CFman::bFAcces (nom_fic, CFman::OF_OUVRE_LECTURE_SEULE))
					{
					AfficheInformationDansStatutGen(c_inf_chargement_en_cours);
					if (charge_memoire (nom_fic) == 0)
						{
						AfficheInformationDansStatutGen(c_inf_Ok);
						MajFichierCmdPcs (paction->chaine);
						::PostMessage (Appli.hwnd, WM_DEBUT_GENERE, 0L, 0L);
						OuvrirMotsReserves();
						Verif(bChargeVariablesSystemes());
						erreur = dwGenereApplic (&ligne, paction->chaine);
						if ((erreur != 0) && (!bAbandonnerCompil))
							{
							AfficheInformationDansStatutGen(c_inf_generation_terminee_avec_erreur);
							affi_err_ligne (erreur,ligne);
							}
						else
							{
							AfficheInformationDansStatutGen(c_inf_generation_terminee_avec_succes);
							}
						::PostMessage (Appli.hwnd, WM_FIN_GENERE, 0L, 0L);
						}
					else // application non chargeable
						{
						affi_err (IDSERR_LE_NOM_DU_FICHIER_EST_INVALIDE);
						}
					}
				else // application inexistante
					{
					affi_err (IDSERR_LE_NOM_DU_FICHIER_EST_INVALIDE);
					}
        } // quitter
			}
      break;

    case action_quitte:
      ::PostMessage (Appli.hwnd, WM_QUITTE_GENERE, 0L, 0L);
      break;

    } // switch
  MemLibere ((PVOID *)(&paction));

  return rretour;
  }






/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_VS.H                                               |
 |                                                                      |
 |   Auteur  : RP                                                       |
 |   Date    : 09/09/94                                                 |
 +----------------------------------------------------------------------*/

DWORD genere_va_syst (DWORD *ligne);

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_DQ.C                                               |
 |   Auteur  : RP                                                       |
 |   Date    : 07/09/94                                                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "Driv_dq.h"
#include "tipe_dq.h"
#include "mem.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "findbdex.h"
#include "UStr.h"
#include "GenereBD.h"

#include "gener_dq.h"
#include "Verif.h"
VerifInit;

//-------------------------- Procedures locales ---------------------
static BOOL  nom_fichier_ambigu (char *nom_fichier , DWORD index_depart_recherche);



//-----------------------------------------------------------------
static BOOL  nom_fichier_ambigu (char *nom_fichier ,DWORD index_depart_recherche)
  {
  BOOL	trouve = FALSE;
  DWORD	index = index_depart_recherche;// �a part � l'index pass� + 1;

  while ((index < nb_enregistrements (szVERIFSource, __LINE__, bx_disq)) && (!trouve))
    {
		PFICHIER_DQ PtrTab;
    index ++;
    PtrTab = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,index);
    trouve = bStrEgales (PtrTab->nom_fichier,nom_fichier);
    }
  return(trouve);
  }
//-----------------------------------------------------------------



DWORD genere_disq (DWORD *ligne)
  {

  PFICHIER_DQ tableau_fichiers;
  t_tableau_vars_fichiers*vars_fichiers;
  DWORD                      pointeur_tab;
  DWORD                      pointeur_debut_tab;
  DWORD                      pointeur_fin_tab;
  DWORD                      pointeur_var;
  DWORD                      wEnr;
  DWORD                      erreur;
  DWORD                      ligne_cour;
  PGEO_DISQUE									PtrValGeo;
  GEO_DISQUE                  val_geo;
  PSPEC_TITRE_PARAGRAPHE_DISQUE	spec_titre_paragraf;
  PENTREE_SORTIE						es;
  char                      nom_fichier[82];
  PTITRE_PARAGRAPHE					titre_paragraf;


  erreur = 0;
  if (existe_repere (b_geo_disq) && nb_enregistrements (szVERIFSource, __LINE__, b_geo_disq) != 0)
    {
    if (!existe_repere (bx_disq))
      {
      cree_bloc (bx_disq,sizeof (*tableau_fichiers),0);
      }
    if (!existe_repere (bx_var_disq))
      {
      cree_bloc (bx_var_disq,sizeof (*vars_fichiers),0);
      }

  //''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    pointeur_debut_tab = 0;
    pointeur_fin_tab = 0;
    pointeur_var = 0;
    ligne_cour = 0;
    
    while ((ligne_cour < nb_enregistrements (szVERIFSource, __LINE__, b_geo_disq)) && (erreur == 0))
      {
      ligne_cour ++;
      PtrValGeo = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,ligne_cour);
      val_geo = (*PtrValGeo);
      switch (val_geo.numero_repere)
        {

        case b_pt_constant:
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;

        case b_titre_paragraf_dq :
					{
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_dq,val_geo.pos_paragraf);
          switch (titre_paragraf->IdParagraphe)
            {
            case fichier_dq :
              if (pointeur_debut_tab > 0)
                {
                for (pointeur_tab = pointeur_debut_tab; pointeur_tab <= pointeur_fin_tab; pointeur_tab ++)
                  {
                  tableau_fichiers = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,pointeur_tab);
                  tableau_fichiers->pos_fin_var = pointeur_var;
                  }
                }
              pointeur_debut_tab = pointeur_fin_tab + 1;
              spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
              pointeur_fin_tab = pointeur_fin_tab + spec_titre_paragraf->nb_fichier;
              insere_enr (szVERIFSource, __LINE__, (pointeur_fin_tab-pointeur_debut_tab+1),bx_disq,pointeur_debut_tab);
              for (pointeur_tab = pointeur_debut_tab; pointeur_tab <= pointeur_fin_tab; pointeur_tab ++)
                {
                spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
                tableau_fichiers = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,pointeur_tab);
                consulte_cst_bd_c (spec_titre_paragraf->position_fichier[(int)(pointeur_tab-pointeur_debut_tab)],nom_fichier);
                StrDeleteDoubleQuotation (nom_fichier);
                StrCopy (tableau_fichiers->nom_fichier,nom_fichier);
                tableau_fichiers->pos_debut_var = pointeur_var + 1;
                tableau_fichiers->dwPosESEcriture = 0;
                tableau_fichiers->dwPosESLecture = 0;
                tableau_fichiers->dwPosESTaille = 0;
                tableau_fichiers->f_handle = 0;
                tableau_fichiers->bOuvert = FALSE;
                supprime_cst_bd_c (spec_titre_paragraf->position_fichier[(int)(pointeur_tab-pointeur_debut_tab)]);
                }
              break;

            case ecrit_dq:
              spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
              for (pointeur_tab = pointeur_debut_tab; pointeur_tab <= pointeur_fin_tab; pointeur_tab ++)
                {
                tableau_fichiers = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,pointeur_tab);
                erreur = recherche_index_execution(spec_titre_paragraf->pos_es,&tableau_fichiers->dwPosESEcriture);
                tableau_fichiers->dwPosESEcriture = tableau_fichiers->dwPosESEcriture - ContexteGen.nbr_logique;
                }
              break;

            case lit_dq:
              spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
              for (pointeur_tab = pointeur_debut_tab; pointeur_tab <= pointeur_fin_tab; pointeur_tab ++)
                {
                tableau_fichiers = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,pointeur_tab);
                erreur = recherche_index_execution(spec_titre_paragraf->pos_es,&tableau_fichiers->dwPosESLecture);
                tableau_fichiers->dwPosESLecture = tableau_fichiers->dwPosESLecture - ContexteGen.nbr_logique;
                }
              break;

            case enr_dq:
              spec_titre_paragraf = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo.pos_specif_paragraf);
              for (pointeur_tab = pointeur_debut_tab; pointeur_tab <= pointeur_fin_tab; pointeur_tab ++)
                {
                tableau_fichiers = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,pointeur_tab);
                erreur = recherche_index_execution(spec_titre_paragraf->pos_es,&tableau_fichiers->dwPosESTaille);
                tableau_fichiers->dwPosESTaille = tableau_fichiers->dwPosESTaille - ContexteGen.nbr_logique;
                }
              break;

            default:
              break;
            }
					}
          break; // b_titre_paragraf_dq

        case b_e_s :
          pointeur_var ++;
          insere_enr (szVERIFSource, __LINE__, 1,bx_var_disq,pointeur_var);
          es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,val_geo.pos_es);
          vars_fichiers = (t_tableau_vars_fichiers*)pointe_enr (szVERIFSource, __LINE__, bx_var_disq,pointeur_var);
          vars_fichiers->genre = es->genre;
          if (val_geo.longueur != 0)
            {
            erreur = recherche_index_tableau_i (val_geo.pos_es,1,&vars_fichiers->pos);
            vars_fichiers->taille = val_geo.longueur;
            }
          else
            {
            erreur = recherche_index_execution(val_geo.pos_es,&vars_fichiers->pos);
            vars_fichiers->taille = 0;
            switch (es->genre)
              {
              case c_res_logique:
                break;

              case c_res_numerique :
                vars_fichiers->pos = vars_fichiers->pos - ContexteGen.nbr_logique;
                break;

              case c_res_message :
                vars_fichiers->pos = vars_fichiers->pos - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                break;

              default:
                break;
              }
            }
          break;


        default:
          break;

        }
      }
    if (erreur == 0)
      {
      if (pointeur_debut_tab > 0)
        {
        for (pointeur_tab = pointeur_debut_tab; pointeur_tab <= pointeur_fin_tab; pointeur_tab ++)
          {
          tableau_fichiers = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,pointeur_tab);
          tableau_fichiers->pos_fin_var = pointeur_var;
          }
        }
      wEnr = 0;
      while ((wEnr < nb_enregistrements (szVERIFSource, __LINE__, bx_disq)) && (erreur == 0))
        {
        wEnr ++;
        tableau_fichiers = (PFICHIER_DQ)pointe_enr (szVERIFSource, __LINE__, bx_disq,wEnr);
        if (tableau_fichiers->pos_fin_var < tableau_fichiers->pos_debut_var)
          {
          erreur = 173;
          ligne_cour = 1;
          }
        else
          {
          if (nom_fichier_ambigu (tableau_fichiers->nom_fichier,wEnr))
            {
            erreur = 4;
            ligne_cour = 1;
            }
          }
        }
      }
    }
  if (erreur != 0)
    {
    (*ligne) = ligne_cour;
    }

  return(erreur);
  }

/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de                                           |
 |                                                                          |
 |                  Societe LOGIQUE INDUSTRIE                               |
 |           Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |                                                                          |
 | Il est demeure sa propriete exclusive et est confidentiel.               |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |                                                                          |
 |   Titre   : INITDESC.C     Gestion des fichiers pour la traduction de PCS|
 |   Auteur  : Jean                                                         |
 |                                                                          |
 +--------------------------------------------------------------------------*/
// Mise � jour JS WIN32 27/1/98

#include "stdafx.h"
#include "col.h"               //Compilation selective
#include "std.h"               //Definition des types standard pour PCS
#include "UStr.h"
#include "LireLng.h"
#include "IdLngLng.h"
#include "lng_res.h"  //Mots reserve
#include "lng_sys.h"  //Variables systeme
#include "pcs_sys.h"
#include "tipe.h"
#include "Descripteur.h"
#include "tipe_sy.h"
#include "gerebdc.h"
#include "mem.h"
#include "Verif.h"
#include "initdesc.h"
VerifInit;

// -----------------------------------------------------------------------
// renomme une variable en conflit
static void NouveauNomVariable (PSTR pszIdentif)
  {
  StrInsertChar (pszIdentif, '_', 0);
	VerifWarning (StrLength (pszIdentif) < c_nb_car_nom_variable_sys);
  }

// --------------------------------------------------------------------------
// Traduction de toutes les constantes logiques H et B
// --------------------------------------------------------------------------
static void TraductionConstantesLogiques (void)
  {
  char szNouvelleValeur [c_nb_car_message_res];

	// Traduit la constante H
  if (bMotReserve (c_res_un, szNouvelleValeur))
    {
		PGEO_SYST	pGeoSyst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_val__vrai);

    renomme_cst_bd_c (szNouvelleValeur, pGeoSyst->pos_init);
    }

	// Traduit la constante B
  if (bMotReserve (c_res_zero, szNouvelleValeur))
    {
		PGEO_SYST pGeoSyst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_val__faux);
    renomme_cst_bd_c (szNouvelleValeur, pGeoSyst->pos_init);
    }
  }

// --------------------------------------------------------------------------
// Insertion d'une variable systeme dans la BDD
// --------------------------------------------------------------------------
static void insere_va_syst (ENTREE_SORTIE es, char * ident, char *init, DWORD taille_tableau, DWORD pos_a_inserer)
  {
  es.n_desc = d_systeme;
  es.taille = taille_tableau;
  GEO_SYST  GeoSyst;
  InsereVarES (&es, ident, &GeoSyst.pos_es);
  DWORD     pos_alpha;
  BOOL exist = bTrouveChaineDansConstant (init, &pos_alpha);
  AjouteConstanteBd (pos_alpha, exist, init, &GeoSyst.pos_init);
  GeoSyst.taille_tableau = taille_tableau;
  PGEO_SYST	pGeoSyst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, pos_a_inserer);
  *pGeoSyst = GeoSyst;
  } // insere_va_syst

// --------------------------------------------------------------------------
// Teste si le driver est dans la collection PROCESSYN,
// et donne les constantes informations mn�moniques du driver.
// --------------------------------------------------------------------------
BOOL bInfoDescripteur (DWORD numero_driver, DWORD *c_inf_titre, DWORD *c_inf_mnemo)
	{
	BOOL bRes = TRUE;

	switch (numero_driver)
		{
		case d_systeme:
			(*c_inf_titre) = c_inf_syst_titre;
			(*c_inf_mnemo) = c_inf_syst_mnemo;
			break;
			
		case d_code:
			(*c_inf_titre) = c_inf_code_titre;
			(*c_inf_mnemo) = c_inf_code_mnemo;
			break;
			
		case d_repete:
			(*c_inf_titre) = c_inf_repete_titre;
			(*c_inf_mnemo) = c_inf_repete_mnemo;
			break;
			
		case d_mem:
			(*c_inf_titre) = c_inf_mem_titre;
			(*c_inf_mnemo) = c_inf_mem_mnemo;
			break;
			
		case d_disq:
			(*c_inf_titre) = c_inf_disq_titre;
			(*c_inf_mnemo) = c_inf_disq_mnemo;
			break;
			
		case d_chrono:
			(*c_inf_titre) = c_inf_chron_titre;
			(*c_inf_mnemo) = c_inf_chron_mnemo;
			break;
			
		case d_graf:
			(*c_inf_titre) = c_inf_vdi_titre;
			(*c_inf_mnemo) = c_inf_vdi_mnemo;
			break;
			
		case d_dde:
			(*c_inf_titre) = c_inf_dde_titre;
			(*c_inf_mnemo) = c_inf_dde_mnemo;
			break;
			
		case d_al:
			(*c_inf_titre) = c_inf_alarmes_titre;
			(*c_inf_mnemo) = c_inf_alarmes_mnemo;
			break;
			
#ifdef c_modbus
		case d_jbus:
			(*c_inf_titre) = c_inf_mdbus_titre;
			(*c_inf_mnemo) = c_inf_mdbus_mnemo;
			break;
#endif
			
#ifdef c_ap
		case d_ap:
			(*c_inf_titre) = c_inf_ap_titre;
			(*c_inf_mnemo) = c_inf_ap_mnemo;
			break;
#endif
			
#ifdef c_opc
		case d_opc:
			(*c_inf_titre) = c_inf_opc_titre;
			(*c_inf_mnemo) = c_inf_opc_mnemo;
			break;
#endif
			
		default:
			(*c_inf_titre) = 0;
			(*c_inf_mnemo) = 0;
			bRes = FALSE;
			break;
		}
	return bRes;
	} // bInfoDescripteur

// --------------------------------------------------------------------------
// Charge � partir des ressources les variables syst�mes dans l'application courante
BOOL bChargeVariablesSystemes (void)
  {
  BOOL	bRes = FALSE;

	// chargement des variables � partir des ressources possible ?
  if (bOuvrirVariablesSysteme ())
    {
		// oui => Variables syst�mes d�ja en m�moire ?
    DWORD	nb_va_sys = dwNbVariablesSysteme ();
		bRes = TRUE;
    if (!existe_repere (b_geo_syst))
      {
			// oui => l'application n'existe pas : chargement de chaque variable syst�me
      cree_bloc (b_geo_syst, sizeof (GEO_SYST), nb_va_sys);
      for (DWORD	numero_enre_sys = 1; numero_enre_sys <= nb_va_sys; numero_enre_sys++)
        {
				INFO_VAR_SYSTEME	InfoVarSysteme;

        if (bLireVariableSysteme (numero_enre_sys, &InfoVarSysteme))
          {
       		ENTREE_SORTIE es;
          es.genre = InfoVarSysteme.genre_variable;
          es.sens = InfoVarSysteme.sens_variable;
					char   ident [c_nb_car_nom_variable_sys];
					StrCopy (ident, InfoVarSysteme.nom_variable);
					char   init [c_nb_car_valeur_initiale_sys];
          StrCopy (init, InfoVarSysteme.valeur_initiale);
          bTrouveChaineDansIdenti (ident, &es.i_identif);
          insere_va_syst (es, ident, init, InfoVarSysteme.taille_tableau, InfoVarSysteme.numero_variable);
          }
        else
          {
					//erreur lecture variable systeme
          bRes = FALSE;
          }
        }
      } // if (!existe_repere (b_geo_syst))
    else      
      {
			// non => le bloc est d�ja cr�� : on va op�rer une fusion
		  DWORD	old_nb_va_sys = nb_enregistrements (szVERIFSource, __LINE__, b_geo_syst);
      
			// Ajoute des enregistrements � b_geo_syst si n�cessaire
      if (old_nb_va_sys < nb_va_sys)
        {
        insere_enr (szVERIFSource, __LINE__, (nb_va_sys - old_nb_va_sys), b_geo_syst, (old_nb_va_sys + 1));
        }

      // Fusion pour chaque variable syst�me
      for (DWORD numero_enre_sys = 1; numero_enre_sys <= nb_va_sys; numero_enre_sys++)
        {
				INFO_VAR_SYSTEME InfoVarSysteme;

        if (bLireVariableSysteme (numero_enre_sys, &InfoVarSysteme))
          {
					ENTREE_SORTIE es;
          es.genre = InfoVarSysteme.genre_variable;
          es.sens = InfoVarSysteme.sens_variable;
          es.taille = InfoVarSysteme.taille_tableau;
					char   ident [c_nb_car_nom_variable_sys];
          StrCopy (ident, InfoVarSysteme.nom_variable);
					char   init [c_nb_car_valeur_initiale_sys];
          StrCopy (init, InfoVarSysteme.valeur_initiale);

          // Nouvelle variable syst�me ?
          if (InfoVarSysteme.numero_variable > old_nb_va_sys)
            {
            // oui => nom de variable libre ?
            if (!bTrouveChaineDansIdenti (ident, &es.i_identif))
              {
							// oui => ins�re la nouvelle variable
              insere_va_syst (es, ident, init, InfoVarSysteme.taille_tableau, InfoVarSysteme.numero_variable);
              }
            else
              {
							// non => renomme la variable utilisant le nom de la variable syt�me puis ins�re la
              DWORD  pos_es_a_renommer = dwIdVarESFromPosIdentif (es.i_identif);
           		PENTREE_SORTIE	pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, pos_es_a_renommer);
           		ENTREE_SORTIE   es_a_renommer = *pES;
           		char   old_ident [c_nb_car_nom_variable_sys];
							StrCopy (old_ident, ident);
              do
                {
                NouveauNomVariable (old_ident);
                }
              while (!bModifieDeclarationES (&es_a_renommer, old_ident, pos_es_a_renommer));

              bTrouveChaineDansIdenti (ident, &es.i_identif);
              insere_va_syst (es, ident, init, InfoVarSysteme.taille_tableau, InfoVarSysteme.numero_variable);
              }
            } // if (InfoVarSysteme.numero_variable > old_nb_va_sys)
          else                         
            {
						// non => pas une nouvelle variable syst�me : mise � jour de l'actuelle
         		PGEO_SYST	pGeoSyst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, InfoVarSysteme.numero_variable);
						char   old_ident [c_nb_car_nom_variable_sys];
            NomVarES (old_ident, pGeoSyst->pos_es);
            
						// Modifie sens et genre :
						// m�me nom de variable ?
            if (bStrEgales (ident, old_ident))
              {
							// oui => met � jour sens et genre
              pGeoSyst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, InfoVarSysteme.numero_variable);
           		PENTREE_SORTIE	pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, pGeoSyst->pos_es);
              pES->genre = es.genre;
              pES->sens = es.sens;
              }
            else
              {
              // non => changement ou traduction du nom de la variable syst�me :
							// La variable a un nouveau nom ?
              if (!bTrouveChaineDansIdenti (ident, &es.i_identif))
                {
								// oui => mise � jour de ses nouvelles infos
                pGeoSyst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, InfoVarSysteme.numero_variable);
             		GEO_SYST GeoSyst = *pGeoSyst;
                bModifieDeclarationES (&es, ident, GeoSyst.pos_es);
                }
              else
                {
								// non => le nouveau nom de la variable est d�ja utilis�
                DWORD  pos_es_a_renommer = dwIdVarESFromPosIdentif (es.i_identif);
                PENTREE_SORTIE pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, pos_es_a_renommer);
             		ENTREE_SORTIE  es_a_renommer = *pES;
                StrCopy (old_ident, ident);
                do
                  {
                  NouveauNomVariable (old_ident);
                  }
                while (!bModifieDeclarationES (&es_a_renommer, old_ident, pos_es_a_renommer));

                pGeoSyst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, InfoVarSysteme.numero_variable);
                GEO_SYST GeoSyst = *pGeoSyst;
                bModifieDeclarationES (&es, ident, GeoSyst.pos_es);
                }
              }
            }
          }
        else
          {
					//erreur lecture variable systeme
          bRes = FALSE;        
          }
        }
      enleve_bloc (b_represente_syst);
      } // else (!existe_repere (b_geo_syst))

		// Chargement des variables syst�me Ok ?
    if (bRes)
      {
			// oui => cr�e et remplis le bloc b_represente_syst contenant les num�ros de variables syst�mes g�r�es
		  DWORD	numero_enre_sys;

      cree_bloc (b_represente_syst, sizeof (DWORD), 0); // $$ typer
      for (numero_enre_sys = 1; numero_enre_sys <= nb_va_sys; numero_enre_sys++)
        {
				INFO_VAR_SYSTEME InfoVarSysteme;
        if (bLireVariableSysteme (numero_enre_sys, &InfoVarSysteme))
          {
					DWORD	dwIdMnemo;
					DWORD	dwIdTitre;

					// Variable syst�me appartenant � un descripteur g�r� ?
          if (bInfoDescripteur (InfoVarSysteme.numero_descripteur, &dwIdTitre,  &dwIdMnemo))
            {
						// oui =>
            insere_enr (szVERIFSource, __LINE__, 1, b_represente_syst, nb_enregistrements (szVERIFSource, __LINE__, b_represente_syst) + 1);
            PDWORD position = (PDWORD)pointe_enr (szVERIFSource, __LINE__, b_represente_syst, nb_enregistrements (szVERIFSource, __LINE__, b_represente_syst));
            *position = InfoVarSysteme.numero_variable;
            }
          }
        else
          {
          //$$ Commentarisation cause erreur anciennement non test�e 
					// bRes = FALSE;  //erreur lecture variable systeme
          }
        } // fin 'for' pour creation du bloc "b_represente_syst"

      TraductionConstantesLogiques ();
      } // if (bRes)

		// Fin d'acc�s aux resources
		FermerVariablesSysteme ();
    } // if (bOuvrirVariablesSysteme () == 0)

  return bRes;
  } // bChargeVariablesSystemes

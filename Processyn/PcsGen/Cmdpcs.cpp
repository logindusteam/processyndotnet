/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il demeure sa propriete exclusive et est confidentiel.             |
 |   Aucune diffusion n'est possible sans accord ecrit.                 |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : gestion fichier de commande															|
 |   Auteur  : LM                                                       |
 |   Date    : 11/08/94                                                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "Appli.h"
#include "FMan.h"
#include "FileMan.h"
#include "UStr.h"
#include "PathMan.h"
#include "Verif.h"
#include "mem.h"

#include "cmdpcs.h"


VerifInit;

// Nom du fichier cmdpcs
static PCSTR pszNomCmdpcs = "CmdPcs";

// Extern => Valeur suivant la commande /P:
static char szParametresLigneCommandePcs [MAX_PATH]= "";

void CopieParametresLigneCommandePcs (PSTR pszDest)
	{
	StrCopy (pszDest, szParametresLigneCommandePcs);
	}

// --------------------------------------------------------------------------
// lecture de la ligne de texte dans le fichier cmdpcs (dans le r�pertoire de l'exe)
// --------------------------------------------------------------------------
BOOL LitFichierCmdPcs (PSTR pszLigneCmdLue)
  {
  char  pszNomFicCmd [MAX_PATH];
  HFIC	hfic;
  BOOL	bRetour = FALSE;

	StrSetNull (pszLigneCmdLue);
  pszCreePathName (pszNomFicCmd, MAX_PATH, Appli.szPathExe,  pszNomCmdpcs);
  if (uFileOuvre (&hfic, pszNomFicCmd, 1, CFman::OF_OUVRE_LECTURE_SEULE_EXCLUSIF) == NO_ERROR)
    {
		char	pszLigneLue  [MAX_PATH];

    if (uFileLireLn (hfic, pszLigneLue, MAX_PATH) == NO_ERROR)
      {
      bRetour = TRUE;
      StrCopy (pszLigneCmdLue, pszLigneLue);
      }
		uFileFerme (&hfic);
		}
  return (bRetour);
  }

//----------------------------------------------------------------------
// La Ligne de Cmd 'pszLigneCmd' doit comporter un Path+nom sans extension
void MajFichierCmdPcs (PCSTR pszLigneCmd)
  {
  char pszNomFicCmd [MAX_PATH];
  HFIC hfic;

	// G�n�re le nom du fichier Cmdspcs dans le r�pertoire de l'application
  pszCreePathName (pszNomFicCmd, MAX_PATH, Appli.szPathExe, pszNomCmdpcs);

	// Re�cris le avec la ligne de commande pass�e en param�tre
  if (uFileOuvre (&hfic, pszNomFicCmd, 1, CFman::OF_CREE_EXCLUSIF) == 0)
		{
		uFileEcrireSzLn (hfic, pszLigneCmd);
		uFileFerme (&hfic);
		}
  } // MajFichierCmdPcs

// --------------------------------------------------------------------------
// fabrication d'1 bloc de commande a partir de la ligne de commande
// champs du bloc : car_cde, chaine associee
// si la ligne de commande est vide, on lit le fichier de commande.
// si le fichier de commande est aussi vide, le bloc sera vide.
// cette fonction retourne valide si la ligne de commande est correcte.
// --------------------------------------------------------------------------
BOOL FabriqueBlocCmd (PCSTR pszLigneDeCommandeSysteme)
  {
  BOOL      bCdeValide = TRUE;
  DWORD     dwIndexSepCmdSuivante;
  DWORD     dwNbCar;
  char      pszLigneDeCommandeSystemeTemp[MAX_PATH];
  char      pszCommandeTemp[MAX_PATH];
  PCOMMANDE_PCS pCommandes;

	StrSetNull (szParametresLigneCommandePcs);
	//
	if (StrIsNull(pszLigneDeCommandeSysteme))
		{
    LitFichierCmdPcs (pszLigneDeCommandeSystemeTemp); // prends la ligne de commande stock�e dans le fichier de commande par d�faut
		}
	else
		{
		StrCopy (pszLigneDeCommandeSystemeTemp, pszLigneDeCommandeSysteme);
		}
	StrDeleteFirstSpaces (pszLigneDeCommandeSystemeTemp);
	StrDeleteLastSpaces (pszLigneDeCommandeSystemeTemp);
	//
	if (!StrIsNull(pszLigneDeCommandeSystemeTemp))
		{
		cree_bloc (BLOC_COMMANDES, sizeof (COMMANDE_PCS), 0);
		do
			{
			insere_enr (szVERIFSource, __LINE__, 1, BLOC_COMMANDES, nb_enregistrements (szVERIFSource, __LINE__, BLOC_COMMANDES) + 1);
			pCommandes = (PCOMMANDE_PCS)pointe_enr (szVERIFSource, __LINE__, BLOC_COMMANDES, nb_enregistrements (szVERIFSource, __LINE__, BLOC_COMMANDES));
			if (pszLigneDeCommandeSystemeTemp [0] == CAR_SEPARATEUR_LIGNE_CDE)
				{
				StrDeleteFirstChars (pszLigneDeCommandeSystemeTemp, CAR_SEPARATEUR_LIGNE_CDE); // suppression du premier car separateur
				dwIndexSepCmdSuivante = StrSearchChar	(pszLigneDeCommandeSystemeTemp, CAR_SEPARATEUR_LIGNE_CDE); // recherche car sep suivant
				if (dwIndexSepCmdSuivante == STR_NOT_FOUND)
					{
					// derniere commande => on prend toute la chaine et on analyse
					StrCopy (pszCommandeTemp, pszLigneDeCommandeSystemeTemp);
					StrSetNull (pszLigneDeCommandeSystemeTemp);
					}
				else
					{
					// il existe une autre commande => on extrait et on analyse
					StrExtract (pszCommandeTemp, pszLigneDeCommandeSystemeTemp, 0, dwIndexSepCmdSuivante); // extraction de la commande � traiter jusqu'au car sep suivant non compris
					StrDelete (pszLigneDeCommandeSystemeTemp, 0, dwIndexSepCmdSuivante); // suppression de la commande � traiter dans la chaine courante
					}
				// analyse de la commande
				StrDeleteLastSpaces(pszCommandeTemp); // suppression des derniers espaces
				dwNbCar = StrLength (pszCommandeTemp); 
				if (dwNbCar > 0)
					{
					pCommandes->carCmd = pszCommandeTemp [0];
					pCommandes->carCmd = StrUpperCaseChar (pCommandes->carCmd);
					if (dwNbCar > 1)
						{ 
						// il y a une chaine derriere la commande
						if (pszCommandeTemp[1] == CAR_SEPARATEUR_NOM)
							{
							StrExtract (pCommandes->pszChaine, pszCommandeTemp, 2, dwNbCar - 2);
							StrDeleteLastSpaces (pCommandes->pszChaine);
							StrDeleteDoubleQuotation (pCommandes->pszChaine);
							if (pCommandes->carCmd == CARACTERE_COMMANDE_PCS)
								{
								StrCopy (szParametresLigneCommandePcs, pCommandes->pszChaine);
								// on supprime l'enregistrement du bloc de commande: -> acces direct via la va static
								enleve_enr (szVERIFSource, __LINE__, 1, BLOC_COMMANDES, nb_enregistrements (szVERIFSource, __LINE__, BLOC_COMMANDES));
								}
							}
						else
							{
							bCdeValide = FALSE;
							StrSetNull (pszLigneDeCommandeSystemeTemp);
							}
						}
					else // pas de nom de fichier derriere la commande
						{
						LitFichierCmdPcs (pCommandes->pszChaine);
						}
					}
				else
					{
					bCdeValide = FALSE;
					StrSetNull (pszLigneDeCommandeSystemeTemp);
					}
				} // Trouve un separateur de commande
			else // nom de fichier tout seul
				{
				pCommandes->carCmd = '\0';
				StrCopy (pCommandes->pszChaine, pszLigneDeCommandeSystemeTemp);
				StrDeleteDoubleQuotation (pCommandes->pszChaine);
				// $ac  a priori inutile ???  MajFichierCmdPcs (pCommandes->pszChaine);
				StrSetNull (pszLigneDeCommandeSystemeTemp);
				}
			} while (!StrIsNull (pszLigneDeCommandeSystemeTemp));

    //
    if (!bCdeValide)
      {
			enleve_bloc (BLOC_COMMANDES);
			VerifWarningExit;
      }
    } // !StrIsNull(pszLigneDeCommandeSystemeTemp) => bloc vide

  return bCdeValide;
  }

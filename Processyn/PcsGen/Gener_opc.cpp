 /*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------*/
// Gener_opc.cpp
// G�n�ration client OPC
// JS	Win32 01/04/98

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "opc.h"
#include "OPCError.h"
#include "TemplateArray.h"
#include "OPCClient.h"
#include "tipe_opc.h"
#include "mem.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "findbdex.h"
#include "Cvt_UL.h"
#include "UStr.h"
#include "Verif.h"
#include "GenereBD.h"

#include "gener_opc.h"
VerifInit;

//--------------------------------------------------------------------------
DWORD genere_opc (DWORD *ligne)
  {
  DWORD ligne_cour = 0;
  DWORD erreur = 0;

  if (existe_repere (b_geo_opc))
    {
		// Num�ro d'enr en ex�cutiondu serveur  courant
		DWORD dwNServeurExCourant = 1;
		// Num�ro d'enr en ex�cution du groupe courant
		DWORD dwNGroupeExCourant = 1;
		// Num�ro d'enr en ex�cution de l'item courant
		DWORD dwNItemExCourant = 1;

    while ((ligne_cour < nb_enregistrements (szVERIFSource, __LINE__, b_geo_opc)) && (erreur == 0))
      {
      ligne_cour++;
			PGEO_OPC pGeoOPC = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc, ligne_cour);
			GEO_OPC GeoOPC = (*pGeoOPC);
      switch (GeoOPC.numero_repere)
        {
        case b_pt_constant :
					{
          supprime_cst_bd_c (GeoOPC.pos_donnees);
					}
          break;

        case b_titre_paragraf_opc :
					{
					PTITRE_PARAGRAPHE	pTitreParagraf;
          pTitreParagraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_opc, GeoOPC.pos_paragraf);
          switch (pTitreParagraf->IdParagraphe)
            {
            case PARAGRAPHE_SERVEUR_OPC :
							{
							// Nouveau serveur
              PSPEC_TITRE_PARAGRAPHE_OPC pSpecTitreParagrafOPC = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc, GeoOPC.pos_specif_paragraf);
              if (!existe_repere (bx_opc_serveur))
                {
                cree_bloc (bx_opc_serveur, sizeof (X_CLIENT_OPC), 0);
                }
							PX_CLIENT_OPC pXClientOPC = (PX_CLIENT_OPC)pAjouteEnrFin (szVERIFSource, __LINE__, 1, bx_opc_serveur);
							StrCopy (pXClientOPC->szNomServeur, pSpecTitreParagrafOPC->Serveur.szNomServeur);
							EnleveGuillemetsMessage(pXClientOPC->szNomServeur);
              erreur = recherche_index_execution (pSpecTitreParagrafOPC->Serveur.dwIdESStatut, &pXClientOPC->dwIdESStatut);
							pXClientOPC->dwIdESStatut  -= ContexteGen.nbr_logique;
							if (erreur == 0)
								{
								if (pSpecTitreParagrafOPC->Serveur.dwIdESExecution != 0)
									{
									erreur = recherche_index_execution (pSpecTitreParagrafOPC->Serveur.dwIdESExecution, 
										&pXClientOPC->dwIdESExecution);
									}
								else
									pXClientOPC->dwIdESExecution = 0;
								}

							pXClientOPC->dwIdAcces = pSpecTitreParagrafOPC->Serveur.dwIdAcces;
							StrCopy (pXClientOPC->szNomPoste, pSpecTitreParagrafOPC->Serveur.szNomPoste);
							EnleveGuillemetsMessage(pXClientOPC->szNomPoste);
							pXClientOPC->pCOPCClientEx = NULL;
							// Note le premier groupe en cours
							pXClientOPC->dwNPremierGroupeEx = dwNGroupeExCourant;
							pXClientOPC->dwNbGroupesEx = 0; // par d�faut aucun groupe
							// un Serveur de plus
							dwNServeurExCourant++;
							}
              break; // PARAGRAPHE_SERVEUR_OPC

            case PARAGRAPHE_GROUPE_OPC :
							{
              PSPEC_TITRE_PARAGRAPHE_OPC pSpecTitreParagrafOPC = (PSPEC_TITRE_PARAGRAPHE_OPC)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_opc, GeoOPC.pos_specif_paragraf);
              if (!existe_repere (bx_opc_groupe))
                {
                cree_bloc (bx_opc_groupe, sizeof (X_GROUPE_OPC), 0);
                }
							PX_GROUPE_OPC pXGroupeOPC = (PX_GROUPE_OPC)pAjouteEnrFin (szVERIFSource, __LINE__, 1, bx_opc_groupe);
							StrCopy (pXGroupeOPC->szNomGroupe, pSpecTitreParagrafOPC->Groupe.szNomGroupe);
							EnleveGuillemetsMessage(pXGroupeOPC->szNomGroupe);
              erreur = recherche_index_execution (pSpecTitreParagrafOPC->Groupe.dwIdESStatut, &pXGroupeOPC->dwIdESStatut);
							pXGroupeOPC->dwIdESStatut  -= ContexteGen.nbr_logique;
              erreur = recherche_index_execution (pSpecTitreParagrafOPC->Groupe.dwIdESRaf, &pXGroupeOPC->dwIdESRaf);
							pXGroupeOPC->IdSens = pSpecTitreParagrafOPC->Groupe.IdSens;
							pXGroupeOPC->dwIdOperation = pSpecTitreParagrafOPC->Groupe.dwIdOperation;
							pXGroupeOPC->dwCadenceMS = pSpecTitreParagrafOPC->Groupe.dwCadenceMS;
							pXGroupeOPC->pCOPCGroupeEx = NULL; // par d�faut pas d'objet
							pXGroupeOPC->nIdGroupeEx = -1; // par d�faut Id invalide

							// Note le premier item en cours
							pXGroupeOPC->dwNPremierItemEx = dwNItemExCourant;
							pXGroupeOPC->dwNbItemsEx = 0; // par d�faut aucun groupe

							// Un groupe de plus
							dwNGroupeExCourant ++;

							// d�compte le aussi dans le serveur ex�ction courant
							PX_CLIENT_OPC pXClientOPC = (PX_CLIENT_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_serveur, dwNServeurExCourant - 1);
							(pXClientOPC->dwNbGroupesEx)++;
							}
              break; // PARAGRAPHE_SERVEUR_OPC


            default:
              break;
            } // switch (pTitreParagraf->IdParagraphe)
					}
          break; // case b_titre_paragraf_opc

        case b_opc_item :
					{
     			PITEM_OPC_CF pOpcItem = (PITEM_OPC_CF) pointe_enr (szVERIFSource, __LINE__, b_opc_item, GeoOPC.pos_specif_es);
          if (!existe_repere (bx_opc_item))
            {
            cree_bloc (bx_opc_item, sizeof (X_ITEM_OPC), 0);
            }
					PX_ITEM_OPC pXItemOPC = (PX_ITEM_OPC)pAjouteEnrFin (szVERIFSource, __LINE__, 1, bx_opc_item);
					StrCopy (pXItemOPC->szAdresseItem, pOpcItem->szAdresseItem);
					EnleveGuillemetsMessage(pXItemOPC->szAdresseItem);
					// V 6.0.6 2 extensions suppl�mentaires optionelles pour szAdresseItem
					StrCopy (pXItemOPC->szAdresseItemExt1, pOpcItem->szAdresseItemExt1);
					EnleveGuillemetsMessage(pXItemOPC->szAdresseItemExt1);
					StrCopy (pXItemOPC->szAdresseItemExt2, pOpcItem->szAdresseItemExt2);
					EnleveGuillemetsMessage(pXItemOPC->szAdresseItemExt2);

					pXItemOPC->IdGenre = nGenreES(GeoOPC.dwIdES);

					// conversion type ES en VARTYPE VARIANT
					ID_MOT_RESERVE dwIdTypeVar = pOpcItem->dwIdTypeVar;
					VARTYPE VarType = VT_EMPTY;

					// par d�faut : r�cup�re le type de la variable
					if (dwIdTypeVar == libre)
						dwIdTypeVar = pXItemOPC->IdGenre;

					// conversion VARTYPE
					switch (dwIdTypeVar)
						{
						case c_res_logique:
							VarType = VT_BOOL; 
							break;
						case c_res_numerique:
							VarType = VT_R4;
							break;
						case c_res_message:
							VarType = VT_BSTR;
							break;
						case c_res_T_BOOL:
							VarType = VT_BOOL;
							break;
						case c_res_T_ERR:
							VarType = VT_ERROR;
							break;
						case c_res_T_W1:
							VarType = VT_UI1;
							break;
						case c_res_T_I2:
							VarType = VT_I2;
							break;
						case c_res_T_UI2:
							VarType = VT_UI2;
							break;
						case c_res_T_I4:
							VarType = VT_I4;
							break;
						case c_res_T_R4:
							VarType = VT_R4;
							break;
						case c_res_T_R8:
							VarType = VT_R8;
							break;
						case c_res_T_STR:
							VarType = VT_BSTR;
							break;
						}

					pXItemOPC->dwTaille = dwTailleES (GeoOPC.dwIdES);
					if (pXItemOPC->dwTaille > 0)
						{
						// Variable tableau
						pXItemOPC->VarType = VarType | VT_ARRAY;
            erreur = recherche_index_tableau_i (GeoOPC.dwIdES, 1, &pXItemOPC->dwIdES);
						}
					else
						{
						// Variable simple
						pXItemOPC->VarType = VarType;
						erreur = recherche_index_execution (GeoOPC.dwIdES, &pXItemOPC->dwIdES);
						switch (pXItemOPC->IdGenre)
							{
							case c_res_numerique:
								// correction n�cessaire de num ...
								pXItemOPC->dwIdES -= ContexteGen.nbr_logique; //$$ Beuark!
								break;
							case c_res_message:
								// correction n�cessaire de mes ...
								pXItemOPC->dwIdES -= ContexteGen.nbr_logique + ContexteGen.nbr_numerique;
								break;
							}
						}

          erreur = recherche_index_execution (pOpcItem->dwIdESRaf, &pXItemOPC->dwIdESRaf);
          erreur = recherche_index_execution (pOpcItem->dwIdESStatut, &pXItemOPC->dwIdESStatut);
					pXItemOPC->dwIdESStatut -=  ContexteGen.nbr_logique;

					pXItemOPC->pCOPCItemEx = NULL; // par d�faut pas d'objet
					pXItemOPC->nIdItemEx = -1;

					// Un item de plus
					dwNItemExCourant ++;

					// d�compte le aussi dans le serveur ex�ction courant
					PX_GROUPE_OPC pXGroupeOPC = (PX_GROUPE_OPC)pointe_enr (szVERIFSource, __LINE__, bx_opc_groupe, dwNGroupeExCourant - 1);
					(pXGroupeOPC->dwNbItemsEx)++;
					}
					break; // case b_opc_item

        default:
					VerifWarningExit;
          break;
        } // switch GeoOPC.num_repere
      } // while ligne_cour < nb_enr
    } // existe b_geo_opc

	// pas d'erreur � la g�n�ration ?
  if (erreur == 0)
    {
		// oui => enl�ve les blocs devenus inutiles
    if (existe_repere (b_opc_item))
      {
      enleve_bloc (b_opc_item);
      }
    if (existe_repere (b_titre_paragraf_opc))
      {
      enleve_bloc (b_titre_paragraf_opc);
      }
    if (existe_repere (b_spec_titre_paragraf_opc))
      {
      enleve_bloc (b_spec_titre_paragraf_opc);
      }
    } // erreur = 0

  if (erreur != 0)
    {
    (*ligne) = ligne_cour;
    }
  return erreur;
  } // genere_opc

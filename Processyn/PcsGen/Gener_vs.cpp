/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_VS.C                                               |
 |   Auteur  : RP                                                       |
 |   Date    : 09/09/94                                                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_sy.h"
#include "mem.h"
#include "findbdex.h"
#include "GenereBD.h"

#include "gener_vs.h"
#include "Verif.h"
VerifInit;

DWORD genere_va_syst (DWORD *ligne)
  {
  DWORD           erreur;
  DWORD           i;
  PX_VAR_SYS	pos_es_ex;
  PGEO_SYST				val_geo;
  PENTREE_SORTIE	es;

  cree_bloc (b_va_systeme, sizeof (*pos_es_ex), 0);
  erreur = 0;
  i = 0;
  while ((i < nb_enregistrements (szVERIFSource, __LINE__, b_geo_syst)) && (erreur == 0))
    {
    i++;
    insere_enr (szVERIFSource, __LINE__, 1, b_va_systeme, i);
    pos_es_ex = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, i);
    val_geo = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, i);
    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo->pos_es);
    switch (es->sens)
      {
      case c_res_e :
      case c_res_s :
      case c_res_e_et_s :
        erreur = recherche_index_execution (val_geo->pos_es, &pos_es_ex->dwPosCourEx);
        switch (es->genre)
          {
					case c_res_logique:
						pos_es_ex->wBlocGenreEx = b_cour_log;
						break;

          case c_res_numerique :
						pos_es_ex->wBlocGenreEx = b_cour_num;
            pos_es_ex->dwPosCourEx -= ContexteGen.nbr_logique;
            break;

          case c_res_message :
						pos_es_ex->wBlocGenreEx = b_cour_mes;
            pos_es_ex->dwPosCourEx -= ContexteGen.nbr_logique + ContexteGen.nbr_numerique;
            break;
          }
        break;

      case c_res_variable_ta_e :
      case c_res_variable_ta_s :
      case c_res_variable_ta_es :
        erreur = recherche_index_tableau_i (val_geo->pos_es, 1, &pos_es_ex->dwPosCourEx);
        switch (es->genre)
          {
					case c_res_logique:
						pos_es_ex->wBlocGenreEx = b_cour_log;
						break;

          case c_res_numerique :
						pos_es_ex->wBlocGenreEx = b_cour_num;
            break;

          case c_res_message :
						pos_es_ex->wBlocGenreEx = b_cour_mes;
            break;
					}
        break;

      default:
				// $$ ? VerifWarningExit;
        break;

      }
    }
  if (erreur != 0)
    {
    (*ligne) = i;
    }
  return(erreur);
  }

/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Auteur  : LM							|
 |   Date    : 08/04/93 						|
 +----------------------------------------------------------------------*/

/* -------------- fonctions export�es --------------------------- */

void initialise_mode_graph (void);
void termine_mode_graph (void);
void interact_pm_graph (void);
BOOL animation_presente_gr (DWORD *n_elt, DWORD *n_ligne);

void    MiseAJourTaillesTableauxVarSysGr (void);
void    maj_init_sys_gr (void);
void    organise_services_speciaux (void);
BOOL existe_fenetres_graphiques (void);
DWORD    organise_fenetre (void);
BOOL existence_page_gr (DWORD numero_page);
void    maj_page_gr_courante (DWORD numero_page);
DWORD    insere_variable_entree (DWORD bloc_genre, DWORD pos_es);

HEADER_SERVICE_ANM_GR anim_e_log (DWORD n_elt, DWORD Type, DWORD pos_ret, DWORD numero_page);
HEADER_SERVICE_ANM_GR anim_e_num (DWORD n_elt, DWORD couleur, DWORD color_fond,
                   UINT nbr_caractere, BOOL cadre, BOOL masque, BOOL video,
                   FLOAT min, FLOAT max, LONG NbDecimales, DWORD pos_ret, DWORD numero_page);
HEADER_SERVICE_ANM_GR anim_e_mes (DWORD n_elt, DWORD couleur, DWORD color_fond,
                   UINT nbr_caractere, BOOL cadre, BOOL masque,
                   BOOL video, DWORD pos_ret, DWORD numero_page);
HEADER_SERVICE_ANM_GR anim_e_liste (DWORD n_elt, DWORD pos_ret, DWORD numero_page);
HEADER_SERVICE_ANM_GR anim_e_radio (DWORD n_elt, DWORD pos_ret, DWORD numero_page, BOOL *bPremierBouton);
HEADER_SERVICE_ANM_GR anim_e_combobox (DWORD n_elt, DWORD pos_ret_indice, DWORD pos_ret_saisie, DWORD numero_page);

HEADER_SERVICE_ANM_GR anim_r_log (DWORD n_elt, DWORD color_m_n, DWORD color_a_n, DWORD color_fond, DWORD tipe_anim);
HEADER_SERVICE_ANM_GR anim_r_log_4etats (DWORD n_elt, DWORD color_h_h, DWORD color_h_b,
                          DWORD color_b_h, DWORD color_b_b, DWORD color_fond, DWORD tipe_anim);
HEADER_SERVICE_ANM_GR anim_r_num (DWORD n_elt, DWORD modele, DWORD color, DWORD color_fond, DWORD style, FLOAT max, FLOAT min);
HEADER_SERVICE_ANM_GR anim_r_num_dep (DWORD n_elt, DWORD color_cste, DWORD color, DWORD color_fond, DWORD tipe_anim);
HEADER_SERVICE_ANM_GR anim_r_mes (DWORD n_elt, DWORD color_cste, DWORD color, DWORD color_fond);
HEADER_SERVICE_ANM_GR anim_r_num_alpha (DWORD n_elt, DWORD color_cste,
                         DWORD color, DWORD color_fond, UINT nbr_car, DWORD nbr_dec);
HEADER_SERVICE_ANM_GR anim_r_courbe (DWORD n_elt, DWORD page, FLOAT min, FLOAT max, DWORD couleur, DWORD color_fond,
                      DWORD style, DWORD type_courbe, DWORD nb_points_max, int nb_points_scroll,
                      DWORD *erreur);
HEADER_SERVICE_ANM_GR anim_r_courbe_archive (DWORD n_elt, DWORD page);

/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |									    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 |									    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |									    |
 |   Titre   : LNG_SYS.H      Module de gestion des variables syst�mes	    |
 |   Auteur  : Jean							    |
 |   Date    : 21/01/98 						    |
 |									    |
 |		Gestion des variables syst�mes (charg�es dans les ressources). |
 |		   Exemples: 'ARRET_EXPLOITATION', 'TERMINAL'. 				    |
 +--------------------------------------------------------------------------*/


// ---------------- Taille des chaines de caracteres
#define c_nb_car_nom_variable_sys     22
#define c_nb_car_valeur_initiale_sys  82

// Structure d'une info variable syst�me
typedef struct
  {
  char nom_variable [c_nb_car_nom_variable_sys];
  ID_MOT_RESERVE sens_variable;
  ID_MOT_RESERVE genre_variable;
  DWORD numero_descripteur;
  DWORD numero_variable;
  DWORD taille_tableau;  // 0 : variable simple
  char valeur_initiale [c_nb_car_valeur_initiale_sys];
  } INFO_VAR_SYSTEME, *PINFO_VAR_SYSTEME;


// --------------------------------------------------------------------------
// Initialiser le 'gestionnaire' des variables syst�mes
BOOL bOuvrirVariablesSysteme (void); // Renvoie TRUE

// --------------------------------------------------------------------------
// Fermer le 'gestionnaire' des variables syst�mes
void FermerVariablesSysteme (void);

// --------------------------------------------------------------------------
// Donner la variable syst�me numero xxx
BOOL bLireVariableSysteme
	(
	DWORD numero,		// numero de l'info variable syst�me � lire
	PINFO_VAR_SYSTEME pInfoVarSysteme	// adresse d'un informations variable syst�me � mettre � jour
	);								// Renvoie TRUE si pas d'erreur

// --------------------------------------------------------------------------
// Donner le nombre de variables systemes
DWORD dwNbVariablesSysteme
	(void);	// Renvoie (DWORD) -1 si erreur, nombre de variables systemes si non


/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inpugn.h                                                 |
 |   Auteur  : LM																												|
 |   Date    : 07/12/92 																								|
 |   Version : 3.20																											|
 +----------------------------------------------------------------------*/

typedef struct
	{
	int mode_lecture;
	BOOL attente;
	DWORD n_ligne_depart;
	char ligne_commande[80];
	} t_param_lecture;


#define mode_demarrage 0
#define mode_interactif 1
#define mode_boite 2

#define avec_attente TRUE
#define sans_attente FALSE

int lire_commandes (t_param_lecture *param_lecture);

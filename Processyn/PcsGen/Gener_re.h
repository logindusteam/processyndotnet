/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_RE.H                                               |
 |                                                                      |
 |   Auteur  : AC                                                       |
 |   Date    : 13/08/96                                                 |
 +----------------------------------------------------------------------*/

DWORD genere_re (DWORD *ligne);

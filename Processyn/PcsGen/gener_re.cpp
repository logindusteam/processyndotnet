// unite de generation du code executable des traitements repetitifs

#include "stdafx.h"
#include  "std.h"
#include "lng_res.h"
#include  "tipe.h"
#include  "tipe_re.h"
#include  "tipe_co.h"
#include  "mem.h"
#include  "UStr.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include  "gerebdc.h"
#include  "findbdex.h"
#include "GenereBD.h"

#include  "gener_re.h"
#include "Verif.h"
VerifInit;

//---------------------------------------------------------------
static DWORD genre_gen (DWORD ligne, DWORD numero, REQUETE_EDIT action)
  {
  DWORD nbr_va;
  DWORD dw;
  DWORD pos_paragraf;
  PTITRE_PARAGRAPHE titre_paragraf;
  PGEO_REPETE	donnees_geo;
  BOOL trouve;
  PENTREE_SORTIE es;
  DWORD wRetour;

  wRetour = 0;
  if (bLigneDansParagraphe (b_titre_paragraf_re, ligne, structure_re, action, &pos_paragraf))
    {
    titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re, pos_paragraf);
    dw = titre_paragraf->pos_geo_debut;
    trouve = FALSE;
    nbr_va = 0;
    while ((dw < (titre_paragraf->pos_geo_debut + titre_paragraf->nbr_ligne)) && (!trouve))
      {
      dw++;
      donnees_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re, dw);
      if (donnees_geo->numero_repere == b_e_s)
        {
        nbr_va++;
        }
      trouve = (nbr_va == numero);
      } // while
    if (trouve)
      {
      es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, donnees_geo->pos_es);
      wRetour= es->genre;
      }
    }
  return (wRetour);
  }

// --------------------------------------------------------------------------
DWORD genere_re (DWORD *ligne)
  {
  PGEO_REPETE	ads_val_geo;
  GEO_REPETE val_geo;
  PTITRE_PARAGRAPHE titre_paragraf;
  char *spec_titre_paragraf;
  char nom_de_la_structure[Taille_Bloc_Titre_Paragraf+1];
  char representation_constante[c_nb_car_ex_mes];
  PNOM_TEMP_REPETE ads_nom_structure;
  PTEMP_GENC_GENX	ads_genc_genx;
  t_liste_re*ads_liste;
  t_liste_re valeur_liste;
  PDWORD pnt_bx_liste;
  DWORD index_structure_cour;
  DWORD index_genc_genx;
  DWORD index_bx_liste;
  DWORD dw;
  DWORD nb_va_gen_deja_declare;
  DWORD index;
  DWORD ligne_cour;
  PFLOAT ads_num;
  PSTR ads_mes;
  BOOL trouve;
  BOOL fini;
  DWORD erreur;

  //***********************************************
  #define err(code) (*ligne) = ligne_cour;return (code);  //Sale Sioux
  //***********************************************

  erreur = 0;
  if (existe_repere (b_geo_re))
    {
     for (ligne_cour = 1; (ligne_cour <= nb_enregistrements (szVERIFSource, __LINE__, b_geo_re)); ligne_cour++)
      {
      ads_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re, ligne_cour);
      val_geo = (*ads_val_geo);
      switch (val_geo.numero_repere)
        {
        case b_pt_constant:
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;
        case b_titre_paragraf_re:
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re, val_geo.pos_paragraf);
          if (titre_paragraf->IdParagraphe == structure_re)
            {
            index = ligne_cour;
            /* Verification que des elements existe dans la liste */
            fini = FALSE;
            while ((index < nb_enregistrements (szVERIFSource, __LINE__, b_geo_re)) && (!fini))
              {
              index++;
              ads_val_geo = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re, index);
              switch (ads_val_geo->numero_repere)
                {
                case b_titre_paragraf_re :        // Une autre liste ou mot reserve
                  //on regarde si on a pas une d�claration ELEMENT
                  titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_re, ads_val_geo->pos_paragraf);
                  if (titre_paragraf->IdParagraphe == structure_re)
                    {
                    err(208);
                    fini = TRUE;
                    }
                  break;
                case b_liste :                    // Des elements OK
                  fini = TRUE;
                  break;
                default:
                  break;
                }
              } // while
            if (!fini)
              {
              err(208);
              }
            spec_titre_paragraf = (char *)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_re, val_geo.pos_specif_paragraf);
            StrCopy (nom_de_la_structure, spec_titre_paragraf);
            index_structure_cour = 0;
            if (existe_repere (b_temp_nom_structure))
              {
              // test si nom de structure se retrouve 2 fois //
              trouve = FALSE;
              while ((index_structure_cour < nb_enregistrements (szVERIFSource, __LINE__, b_temp_nom_structure)) && (!trouve))
                {
                index_structure_cour++;
                ads_nom_structure = (PNOM_TEMP_REPETE)pointe_enr (szVERIFSource, __LINE__, b_temp_nom_structure, index_structure_cour);
                trouve = bStrEgales (ads_nom_structure->nom, nom_de_la_structure);
                } // while
              if (trouve)
                {
                err(207);
                }
              } // existe b_temp_nom_structure
            else
              {
              cree_bloc (b_temp_nom_structure, sizeof (NOM_TEMP_REPETE), 0);
              }
            index_structure_cour++;
            insere_enr (szVERIFSource, __LINE__, 1, b_temp_nom_structure, index_structure_cour);
            ads_nom_structure = (PNOM_TEMP_REPETE)pointe_enr (szVERIFSource, __LINE__, b_temp_nom_structure, index_structure_cour);
            StrCopy (ads_nom_structure->nom, nom_de_la_structure);
            ads_nom_structure->nb_va_gen = 0;
            ads_nom_structure->taille_liste = 0;
            nb_va_gen_deja_declare = 0;
            } // structure_re
          break; // cas b_titre_paragraf_re
        case b_e_s:
          ads_nom_structure = (PNOM_TEMP_REPETE)pointe_enr (szVERIFSource, __LINE__, b_temp_nom_structure, index_structure_cour);
          ads_nom_structure->nb_va_gen++;
          if (!existe_repere (b_temp_genc_genx))
            {
            cree_bloc (b_temp_genc_genx, sizeof (TEMP_GENC_GENX), 0);
            }
          index_genc_genx = nb_enregistrements (szVERIFSource, __LINE__, b_temp_genc_genx) + 1;
          insere_enr (szVERIFSource, __LINE__, 1, b_temp_genc_genx, index_genc_genx);
          ads_genc_genx = (PTEMP_GENC_GENX)pointe_enr (szVERIFSource, __LINE__, b_temp_genc_genx, index_genc_genx);
          ads_genc_genx->pnt_b_temp_nom_structure = index_structure_cour;
          ads_genc_genx->pos_es = val_geo.pos_es;
          nb_va_gen_deja_declare++;
          if (existe_repere (bx_liste))
            {
            ads_genc_genx->pos_bx_liste = nb_enregistrements (szVERIFSource, __LINE__, bx_liste) + nb_va_gen_deja_declare;
            }
          else
            {
            ads_genc_genx->pos_bx_liste = nb_va_gen_deja_declare;
            }
          break; // cas b_e_s
        case b_liste:
          ads_nom_structure = (PNOM_TEMP_REPETE)pointe_enr (szVERIFSource, __LINE__, b_temp_nom_structure, index_structure_cour);
          ads_nom_structure->taille_liste++;
          if (!existe_repere (bx_liste))
            {
            cree_bloc (bx_liste, sizeof (DWORD), 0);
            }
          index_bx_liste = nb_enregistrements (szVERIFSource, __LINE__, bx_liste) + 1;
          insere_enr (szVERIFSource, __LINE__, nb_va_gen_deja_declare, bx_liste, index_bx_liste);
           for (dw = index_bx_liste; (dw <= nb_enregistrements (szVERIFSource, __LINE__, bx_liste)); dw++)
            {
            pnt_bx_liste = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste,dw);
            ads_liste = (t_liste_re*)pointe_enr (szVERIFSource, __LINE__, b_liste, val_geo.pos_donnees + dw - index_bx_liste);
            valeur_liste = (*ads_liste);
            switch (valeur_liste.numero_repere)
              {
              case b_e_s:
                if (recherche_index_execution (valeur_liste.position, pnt_bx_liste) != 0)
                  {
                  err(22);
                  }
                switch (genre_gen (ligne_cour, (dw-index_bx_liste+1), CONSULTE_LIGNE))
                  {
                  case c_res_numerique :
                    (*pnt_bx_liste) = (*pnt_bx_liste) - ContexteGen.nbr_logique;
                    break;
                  case c_res_message:
                    (*pnt_bx_liste) = (*pnt_bx_liste) - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                    break;
                  default:
                    break;
                  } // switch genre_gen
                break;
              case b_pt_constant:
                switch (genre_gen (ligne_cour, (dw-index_bx_liste+1), CONSULTE_LIGNE))
                  {
                  case c_res_logique:
                  case c_res_message:
                    if (!existe_repere (bx_mes))
                      {
											cree_bloc (bx_mes, c_nb_car_ex_mes, 0);
                      }
                    consulte_cst_bd_c (valeur_liste.position, representation_constante);
										trouve = FALSE ;
										index = 0;
										while ((index < nb_enregistrements (szVERIFSource, __LINE__, bx_mes)) && (!trouve))
											{
											index++;
                      ads_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_mes, index);
                      trouve = bStrEgales (ads_mes, representation_constante);
											}
										if (!trouve)
											{
											index++;
											insere_enr (szVERIFSource, __LINE__, 1, bx_mes, index);
											ads_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bx_mes, index);
											StrCopy (ads_mes, representation_constante);
											}
                    supprime_cst_bd_c (valeur_liste.position);
                    pnt_bx_liste = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste, dw);
                    (*pnt_bx_liste) = index;
                    break;
                  case c_res_numerique:
                    if (!existe_repere (bx_num))
                      {
                      cree_bloc (bx_num,sizeof (FLOAT),0);
                      }
                    index = nb_enregistrements (szVERIFSource, __LINE__, bx_num) + 1;
                    insere_enr (szVERIFSource, __LINE__, 1, bx_num, index);
                    consulte_cst_bd_c (valeur_liste.position, representation_constante);
                    ads_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bx_num, index);
                    if (!StrToFLOAT (ads_num, representation_constante))
                      {
                      err(159);
                      }
                    supprime_cst_bd_c (valeur_liste.position);
                    pnt_bx_liste = (PDWORD)pointe_enr (szVERIFSource, __LINE__, bx_liste, dw);
                    (*pnt_bx_liste) = index;
                    break;
                  default:
                    break;
                  } // switch genre_gen
                break;
              default:
                err(208);
                break;
              } // switch num repere
            } //  for
          break; // cas b_liste
        default:
          break;
        } // switch num repere
      } //  for ligne_cour

    if ((existe_repere (b_liste)) && (nb_enregistrements (szVERIFSource, __LINE__, b_liste) != 0))
      {
      enleve_enr (szVERIFSource, __LINE__, nb_enregistrements (szVERIFSource, __LINE__, b_liste), b_liste, 1);
      }
    *ligne = ligne_cour;
    } // existe liste
  else
    {
    *ligne = 1;
    }
  return (erreur);
  }

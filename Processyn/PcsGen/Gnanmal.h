 /*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :  GnAnmAl.h                                         		|
 |	       								|
 |   Auteur  : LM							|
 |   Date    : 12/08/93 						|
 +----------------------------------------------------------------------*/

// Génération de l'animation des alarmes

HANDLE_ANM_AL FabricationHandleAl(DWORD service, DWORD element);

HANDLE_ANM_AL AnimGeoAl (DWORD wGenreVarAl, t_valeur_al *Valeur, DWORD wPosGeoAl);

void AnimGlobAl
	(char *pszNomFic, DWORD wNbreAlMax, DWORD wTailleFicMax,
	int iFenAlX, int iFenAlY, int iFenAlCx,int iFenAlCy,
	DWORD wTypeHoroDatage, LONG lTempoStabiliteEvt
	);

void AnimGroupeAl
	(char *pszNomGroupe, DWORD coul_al_pres_non_ack,
	DWORD coul_al_pres_ack, DWORD coul_al_non_pres_non_ack,
	DWORD type_ack
	);

void   AnimVariableAl(DWORD wGenreVar, DWORD wTypeVar, t_valeur_al Valeur);

HANDLE_ANM_AL AnimElementAl
	(DWORD wTestAlarme, DWORD wNumeroEnregistrement,
	DWORD wNumeroGroupe, DWORD wNumeroPriorite,
	DWORD wNbreVariable
	);

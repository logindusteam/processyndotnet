/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_JB.C	                                        |
 |   Mise a jours:                                                      |
 |                                                                      |
 |   +--------+--------+---+--------------------------------------+     |
 |   | date   | qui    |no | raisons                              |     |
 |   +--------+--------+---+--------------------------------------+     |
 |   |04/08/92| PR     |80 | Int�gration Time_out April s�rie 1000|     |
 |   |        |        |   |                                      |     |
 |   +--------+--------+---+--------------------------------------+     |
 +----------------------------------------------------------------------*/

// generation JBUS

#include "stdafx.h"
#include  "std.h"
#include  "lng_res.h"
#include  "tipe.h"
#include  "UStr.h"
#include  "MemMan.h"
#include  "mem.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include  "gerebdc.h"
#include  "findbdex.h"
#include  "modbus.h"
#include  "cvt_jb.h"
#include  "tipe_jb.h"
#include "GenereBD.h"

#include  "gener_jb.h"
#include "Verif.h"
VerifInit;

//--------------------------------------------------------------------------
// Renvoie le nombre minimum de mots 16 bits n�cessaires pour contenir dwNbOctets ctets
static DWORD nombre_de_mot (DWORD dwNbOctets)
  {
  return (dwNbOctets + 1) / 2;
  }

//--------------------------------------------------------------------------
static DWORD elabore_donnees_jbus (void)
  {
  tc_jbus_e*donnees_jbus_e;
  tc_jbus_tab*donnees_jbus_e_tab;
  tc_jbus_s*donnees_jbus_s;
  tc_jbus_tab*donnees_jbus_s_tab;
  tc_jbus_e_mes*donnees_jbus_e_mes;
  tc_jbus_s_mes*donnees_jbus_s_mes;
  PX_MODBUS_S	donneex_jbus_s;
  PX_MODBUS_TAB	donneex_jbus_tab;
  tg_jbus*donnees_jbus_generees;
  GEO_MODBUS val_geo;
  PGEO_MODBUS	ptr_val_geo;
  PTITRE_PARAGRAPHE	titre_paragraf;
  t_spec_titre_paragraf_jb*ptr_spec_titre_paragraf_jb;
  t_parametre_com*ptr_para_com;
  DWORD i_cmd_rafraich;
  DWORD numero_canal_cour;
  DWORD numero_esclave_cour;
  DWORD EsclVarOuCste_cour;
  DWORD CanalVarOuCste_cour;
  FLOAT time_out_cour;
  DWORD retry_cour;
  DWORD sens_sur_auto;
  MODBUS_AUTOMATE espece_d_auto;
  DWORD sens_cour;
  DWORD genre_cour;
  DWORD genre_num_cour;
  DWORD index;
  DWORD erreur;

  erreur = 0;
  if (existe_repere (b_geo_jbus))
    {
    index = 0;
    while ((index < nb_enregistrements (szVERIFSource, __LINE__, b_geo_jbus)) && (erreur == 0))
      {
      index++;
      ptr_val_geo = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus, index);
      val_geo = (*ptr_val_geo);
      switch (val_geo.numero_repere)
        {
        case b_pt_constant :
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;
        case b_titre_paragraf_jb :
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_jb, val_geo.pos_paragraf);
          switch (titre_paragraf->IdParagraphe)
            {
            case canal_jb :
              if (!existe_repere (bx_para_jbus_com))
                {
                cree_bloc (bx_para_jbus_com, sizeof (t_parametre_com), 0);
                }
              insere_enr (szVERIFSource, __LINE__, 1, bx_para_jbus_com, nb_enregistrements (szVERIFSource, __LINE__, bx_para_jbus_com) + 1);
              ptr_para_com = (t_parametre_com*)pointe_enr (szVERIFSource, __LINE__, bx_para_jbus_com, nb_enregistrements (szVERIFSource, __LINE__, bx_para_jbus_com));
              ptr_spec_titre_paragraf_jb = (t_spec_titre_paragraf_jb*)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
              ptr_para_com->vitesse = ptr_spec_titre_paragraf_jb->vitesse;
              ptr_para_com->nb_de_bits = ptr_spec_titre_paragraf_jb->nb_de_bits;
              ptr_para_com->parite = ptr_spec_titre_paragraf_jb->parite;
              ptr_para_com->encadrement = ptr_spec_titre_paragraf_jb->encadrement;
							ptr_para_com->CanalVarOuCste = ptr_spec_titre_paragraf_jb->CanalVarOuCste;
              CanalVarOuCste_cour = ptr_spec_titre_paragraf_jb->CanalVarOuCste;
              if (CanalVarOuCste_cour == ID_CHAMP_CTE)
                {
	              numero_canal_cour = ptr_spec_titre_paragraf_jb->numero_canal;
								}
							else
								{
                erreur = recherche_index_execution (ptr_spec_titre_paragraf_jb->numero_canal,
                                                     &numero_canal_cour);
                numero_canal_cour = numero_canal_cour - ContexteGen.nbr_logique;
								}
              ptr_para_com->numero_canal = numero_canal_cour;
              break;

            case esclave_jb :
              ptr_spec_titre_paragraf_jb = (t_spec_titre_paragraf_jb*)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_jb, val_geo.pos_specif_paragraf);
              espece_d_auto = (MODBUS_AUTOMATE)ptr_spec_titre_paragraf_jb->tipe;
              EsclVarOuCste_cour = ptr_spec_titre_paragraf_jb->EsclVarOuCste;
              if (EsclVarOuCste_cour == ID_CHAMP_CTE)
                {
                numero_esclave_cour = ptr_spec_titre_paragraf_jb->numero_esclave;
              	}
              else
                {
                erreur = recherche_index_execution (ptr_spec_titre_paragraf_jb->numero_esclave,
                                                     &numero_esclave_cour);
                numero_esclave_cour = numero_esclave_cour - ContexteGen.nbr_logique;
                }
              time_out_cour = ptr_spec_titre_paragraf_jb->fNbUnitesTimeOut;
              retry_cour = ptr_spec_titre_paragraf_jb->retry;
              sens_cour = 0;
              genre_cour = 0;
              break;

            case entre_num:
            case entre_num_mot:
            case entre_num_reel:
              sens_cour = c_res_e;
              genre_cour = c_res_numerique;
              if (titre_paragraf->IdParagraphe == entre_num)
                {
                genre_num_cour = c_res_entier;
                if (!existe_repere (bg_jbus_e_num_entier))
                  {
                  cree_bloc (bg_jbus_e_num_entier, sizeof (tg_jbus), 0);
                  }
                if (!existe_repere (bx_jbus_e_num_entier_tab))
                  {
                  cree_bloc (bx_jbus_e_num_entier_tab, sizeof (X_MODBUS_TAB), 0);
                  }
              	}
              else
                {
                if (titre_paragraf->IdParagraphe == entre_num_mot)
                  {
                  genre_num_cour = c_res_mots;
                  if (!existe_repere (bg_jbus_e_num_mot))
                    {
                    cree_bloc (bg_jbus_e_num_mot, sizeof (tg_jbus), 0);
                    }
                  if (!existe_repere (bx_jbus_e_num_mot_tab))
                    {
                    cree_bloc (bx_jbus_e_num_mot_tab, sizeof (X_MODBUS_TAB), 0);
                    }
                  }
                else
                  {
                  genre_num_cour = c_res_reels;
                  if (!existe_repere (bg_jbus_e_num_reel))
                    {
                    cree_bloc (bg_jbus_e_num_reel, sizeof (tg_jbus), 0);
                    }
                  if (!existe_repere (bx_jbus_e_num_reel_tab))
                    {
                    cree_bloc (bx_jbus_e_num_reel_tab, sizeof (X_MODBUS_TAB), 0);
                    }
                  }
                }
              break;

            case entre_log :
              sens_cour = c_res_e;
              genre_cour = c_res_logique;
              if (!existe_repere (bg_jbus_e_log))
                {
                cree_bloc (bg_jbus_e_log, sizeof (tg_jbus), 0);
                }
              if (!existe_repere (bx_jbus_e_log_tab))
                {
                cree_bloc (bx_jbus_e_log_tab, sizeof (X_MODBUS_TAB), 0);
                }
              break;

            case entre_mes :
              sens_cour = c_res_e;
              genre_cour = c_res_message;
              if (!existe_repere (bg_jbus_e_mes))
                {
                cree_bloc (bg_jbus_e_mes, sizeof (tg_jbus), 0);
                }
              break;

            case sorti_num:
            case sorti_num_mot:
            case sorti_num_reel :
              sens_cour = c_res_s;
              genre_cour = c_res_numerique;
              if (titre_paragraf->IdParagraphe == sorti_num)
                {
                genre_num_cour = c_res_entier;
                if (!existe_repere (bx_jbus_s_num_entier))
                  {
                  cree_bloc (bx_jbus_s_num_entier, sizeof (X_MODBUS_S), 0);
                  }
                if (!existe_repere (bx_jbus_s_num_entier_tab))
                  {
                  cree_bloc (bx_jbus_s_num_entier_tab, sizeof (X_MODBUS_TAB), 0);
                  }
              	}
              else
                {
                if (titre_paragraf->IdParagraphe == sorti_num_mot)
                  {
                  genre_num_cour = c_res_mots;
                  if (!existe_repere (bx_jbus_s_num_mot))
                    {
                    cree_bloc (bx_jbus_s_num_mot, sizeof (X_MODBUS_S), 0);
                    }
                  if (!existe_repere (bx_jbus_s_num_mot_tab))
                    {
                    cree_bloc (bx_jbus_s_num_mot_tab, sizeof (X_MODBUS_TAB), 0);
                    }
                  }
                else
                  {
                  genre_num_cour = c_res_reels;
                  if (!existe_repere (bx_jbus_s_num_reel))
                    {
                    cree_bloc (bx_jbus_s_num_reel, sizeof (X_MODBUS_S), 0);
                    }
                  if (!existe_repere (bx_jbus_s_num_reel_tab))
                    {
                    cree_bloc (bx_jbus_s_num_reel_tab, sizeof (X_MODBUS_TAB), 0);
                    }
                  }
                }
              break;

            case sorti_log :
              sens_cour = c_res_s;
              genre_cour = c_res_logique;
              if (!existe_repere (bx_jbus_s_log))
                {
                cree_bloc (bx_jbus_s_log, sizeof (X_MODBUS_S), 0);
                }
              if (!existe_repere (bx_jbus_s_log_tab))
                {
                cree_bloc (bx_jbus_s_log_tab, sizeof (X_MODBUS_TAB), 0);
                }
              break;

            case sorti_mes :
              sens_cour = c_res_s;
              genre_cour = c_res_message;
              if (!existe_repere (bx_jbus_s_mes))
                {
                cree_bloc (bx_jbus_s_mes, sizeof (X_MODBUS_S), 0);
                }
              break;

            default:
              break;
            } // switch titre_par.tipe
          break; // case b_titre_para

        case b_jbus_e:
        case b_jbus_e_mes :
          switch (genre_cour)
            {
            case c_res_logique :
              insere_enr (szVERIFSource, __LINE__, 1, bg_jbus_e_log, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_log) + 1);
              donnees_jbus_generees = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus_e_log, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_log));
              erreur = recherche_index_execution (val_geo.pos_es, &(donnees_jbus_generees->index_es_associe));
              donnees_jbus_e = (tc_jbus_e*)pointe_enr (szVERIFSource, __LINE__, b_jbus_e, val_geo.pos_specif_es);
              donnees_jbus_generees->longueur = 1;
              i_cmd_rafraich = donnees_jbus_e->i_cmd_rafraich;
              erreur = dwConversionAdresseModbus (espece_d_auto, c_res_e, c_res_logique, donnees_jbus_e->adresse_automate,
                                                 &donnees_jbus_generees->adresse_automate,
                                                 &donnees_jbus_generees->sens_sur_auto);
             break;

            case c_res_numerique :
              switch (genre_num_cour)
                {
                case c_res_entier :
                  insere_enr (szVERIFSource, __LINE__, 1, bg_jbus_e_num_entier, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_num_entier) + 1);
                  donnees_jbus_generees = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus_e_num_entier, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_num_entier));
                  break;
                case c_res_mots :
                  insere_enr (szVERIFSource, __LINE__, 1, bg_jbus_e_num_mot, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_num_mot) + 1);
                  donnees_jbus_generees = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus_e_num_mot, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_num_mot));
                  break;
                case c_res_reels :
                  insere_enr (szVERIFSource, __LINE__, 1, bg_jbus_e_num_reel, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_num_reel) + 1);
                  donnees_jbus_generees = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus_e_num_reel, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_num_reel));                  break;
                } // fin switch genre_num cour
              erreur = recherche_index_execution (val_geo.pos_es, &donnees_jbus_generees->index_es_associe);
              donnees_jbus_generees->index_es_associe =donnees_jbus_generees->index_es_associe - ContexteGen.nbr_logique;
              donnees_jbus_e = (tc_jbus_e*)pointe_enr (szVERIFSource, __LINE__, b_jbus_e, val_geo.pos_specif_es);
              donnees_jbus_generees->longueur = 1;
              i_cmd_rafraich =donnees_jbus_e->i_cmd_rafraich;
              erreur = dwConversionAdresseModbus (espece_d_auto, c_res_e, c_res_numerique, donnees_jbus_e->adresse_automate,
                                                 &donnees_jbus_generees->adresse_automate,
                                                 &donnees_jbus_generees->sens_sur_auto);
              break;

            case c_res_message:
              insere_enr (szVERIFSource, __LINE__, 1, bg_jbus_e_mes, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_mes) + 1);
              donnees_jbus_generees = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus_e_mes, nb_enregistrements (szVERIFSource, __LINE__, bg_jbus_e_mes));
              erreur = recherche_index_execution (val_geo.pos_es, &donnees_jbus_generees->index_es_associe);
              donnees_jbus_generees->index_es_associe = donnees_jbus_generees->index_es_associe
                                                         - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
              donnees_jbus_e_mes = (tc_jbus_e_mes*)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_mes, val_geo.pos_specif_es);
              donnees_jbus_generees->longueur = donnees_jbus_e_mes-> longueur;
              i_cmd_rafraich =donnees_jbus_e_mes->i_cmd_rafraich;
              erreur = dwConversionAdresseModbus (espece_d_auto, c_res_e, c_res_message, donnees_jbus_e_mes->adresse_automate,
                                                 &donnees_jbus_generees->adresse_automate,
                                                 &donnees_jbus_generees->sens_sur_auto);
              break;

            default:
              break;
            } // switch genre_cour
          if (erreur == 0)
            {
            donnees_jbus_generees->numero_canal = numero_canal_cour;
            donnees_jbus_generees->CanalVarOuCste = CanalVarOuCste_cour;
            donnees_jbus_generees->numero_esclave = numero_esclave_cour;
            donnees_jbus_generees->EsclVarOuCste = EsclVarOuCste_cour;
            donnees_jbus_generees->tipe_esclave = espece_d_auto;
            donnees_jbus_generees->fNbUnitesTimeOut = time_out_cour;
            donnees_jbus_generees->retry = retry_cour;
            erreur = recherche_index_execution (i_cmd_rafraich,
                                                &donnees_jbus_generees->i_cmd_rafraich);
            }
          break; // b_jbus_e

        case b_jbus_s :
          switch (genre_cour)
            {
            case c_res_logique :
              insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_log, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_log) + 1);
              donneex_jbus_s = (PX_MODBUS_S)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_log, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_log));
              donnees_jbus_s = (tc_jbus_s*)pointe_enr (szVERIFSource, __LINE__, b_jbus_s, val_geo.pos_specif_es);
              erreur = dwConversionAdresseModbus (espece_d_auto, c_res_s, c_res_logique, donnees_jbus_s->adresse_automate,
                                                 &donneex_jbus_s->adresse_automate, &sens_sur_auto);
              if (erreur == 0)
                {
                if (sens_sur_auto == c_res_s)
                  {
                  donneex_jbus_s->numero_fonction = 5;
                  }
                else
                  {
                  donneex_jbus_s->numero_fonction = 15;
                  }
                erreur = recherche_index_execution (val_geo.pos_es, &donneex_jbus_s->index_es_associe);
                }
              donneex_jbus_s->nbre_a_ecrire = 1;
              break;

            case c_res_numerique :
              switch (genre_num_cour)
                {
                case c_res_entier :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_num_entier, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_entier) + 1);
                  donneex_jbus_s = (PX_MODBUS_S)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_entier, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_entier));
                  break;
                case c_res_mots :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_num_mot, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_mot) + 1);
                  donneex_jbus_s = (PX_MODBUS_S)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_mot, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_mot));
                  break;
                case c_res_reels :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_num_reel, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_reel) + 1);
                  donneex_jbus_s = (PX_MODBUS_S)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_reel, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_reel));
                  break;
                } // fin switch
              donnees_jbus_s = (tc_jbus_s*)pointe_enr (szVERIFSource, __LINE__, b_jbus_s, val_geo.pos_specif_es);
              erreur = dwConversionAdresseModbus (espece_d_auto, c_res_s, c_res_numerique, donnees_jbus_s->adresse_automate,
                                                 &donneex_jbus_s->adresse_automate,
                                                 &sens_sur_auto);
              if (erreur == 0)
                {
                if ((sens_sur_auto == c_res_s) && (genre_num_cour != c_res_reels))
                  {
                  donneex_jbus_s->numero_fonction = 6;
                  }
                else
                  {
                  donneex_jbus_s->numero_fonction = 16;
                  }
                erreur = recherche_index_execution (val_geo.pos_es, &donneex_jbus_s->index_es_associe);
                donneex_jbus_s->index_es_associe = donneex_jbus_s->index_es_associe - ContexteGen.nbr_logique;
                donneex_jbus_s->nbre_a_ecrire = 1;
                }
              break;

            default:
              break;
            } // switch
          if (erreur == 0)
            {
            donneex_jbus_s->numero_canal = numero_canal_cour;
            donneex_jbus_s->CanalVarOuCste = CanalVarOuCste_cour;
            donneex_jbus_s->numero_esclave = numero_esclave_cour;
            donneex_jbus_s->EsclVarOuCste = EsclVarOuCste_cour;
            donneex_jbus_s->tipe_esclave = espece_d_auto;
            donneex_jbus_s->fNbUnitesTimeOut = time_out_cour;
            donneex_jbus_s->retry = retry_cour;
            }
          break;

        case b_jbus_e_tab :
          switch (genre_cour)
            {
            case c_res_logique :
              insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_e_log_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_log_tab) + 1);
              donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e_log_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_log_tab));
              donnees_jbus_e_tab = (tc_jbus_tab*)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_tab, val_geo.pos_specif_es);
              donneex_jbus_tab->AdrVarOuCste = donnees_jbus_e_tab->AdrVarOuCste;
              donneex_jbus_tab->espece_d_auto = espece_d_auto;
              if (donneex_jbus_tab->AdrVarOuCste == ID_CHAMP_CTE)
                {
                erreur = dwConversionAdresseModbus (espece_d_auto, c_res_e, c_res_logique, donnees_jbus_e_tab->adresse_automate,
                                                   &donneex_jbus_tab->adresse_automate,
                                                   &sens_sur_auto);
                if (erreur == 0)
                  {
                  if (sens_sur_auto == c_res_s)
                    {
                    donneex_jbus_tab->numero_fonction = 1;
                    }
                  else
                    {
                    donneex_jbus_tab->numero_fonction = 2;
                    }
                  }
              	}
              else
                {
                erreur = recherche_index_execution (donnees_jbus_e_tab->PosVarAdresse,
                                                    &donneex_jbus_tab->PosVarAdresse);
                donneex_jbus_tab->PosVarAdresse = donneex_jbus_tab->PosVarAdresse - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                }
              donneex_jbus_tab->LongVarOuCste = donnees_jbus_e_tab->LongVarOuCste;
              if (donneex_jbus_tab->LongVarOuCste == ID_CHAMP_CTE)
                {
                donneex_jbus_tab->longueur = donnees_jbus_e_tab->longueur;
              	}
              else
                {
                erreur = recherche_index_execution (donnees_jbus_e_tab->longueur,
                                                    &donneex_jbus_tab->longueur);
                donneex_jbus_tab->longueur = donneex_jbus_tab->longueur - ContexteGen.nbr_logique;
                }
              erreur = recherche_index_tableau_i (val_geo.pos_es, 1, &donneex_jbus_tab->index_es_associe);
              erreur = recherche_index_execution (donnees_jbus_e_tab->i_cmd_rafraich,
                                                  &donneex_jbus_tab->i_cmd_rafraich);
              break;

            case c_res_numerique :
              switch (genre_num_cour)
                {
                case c_res_entier :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_e_num_entier_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_entier_tab) + 1);
                  donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e_num_entier_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_entier_tab));
                  break;
                case c_res_mots :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_e_num_mot_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_mot_tab) + 1);
                  donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e_num_mot_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_mot_tab));
                  break;
                case c_res_reels :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_e_num_reel_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_reel_tab) + 1);
                  donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e_num_reel_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e_num_reel_tab));
                  break;
                } // fin switch
              donnees_jbus_e_tab = (tc_jbus_tab*)pointe_enr (szVERIFSource, __LINE__, b_jbus_e_tab, val_geo.pos_specif_es);
              donneex_jbus_tab->espece_d_auto = espece_d_auto;
              donneex_jbus_tab->AdrVarOuCste = donnees_jbus_e_tab->AdrVarOuCste;
              if (donneex_jbus_tab->AdrVarOuCste == ID_CHAMP_CTE)
                {
                erreur = dwConversionAdresseModbus (espece_d_auto, c_res_e, c_res_numerique, donnees_jbus_e_tab->adresse_automate,
                                                   &donneex_jbus_tab->adresse_automate,
                                                   &sens_sur_auto);
                if (erreur == 0)
                  {
                  if (sens_sur_auto == c_res_s)
                    {
                    donneex_jbus_tab->numero_fonction = 3;
                    }
                  else
                    {
                    donneex_jbus_tab->numero_fonction = 4;
                    }
                  }
              	}
              else
                {
                erreur = recherche_index_execution (donnees_jbus_e_tab->PosVarAdresse,
                                                    &donneex_jbus_tab->PosVarAdresse);
                donneex_jbus_tab->PosVarAdresse = donneex_jbus_tab->PosVarAdresse - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                }
              erreur = recherche_index_execution (donnees_jbus_e_tab->i_cmd_rafraich,
                                                  &donneex_jbus_tab->i_cmd_rafraich);
              erreur = recherche_index_tableau_i (val_geo.pos_es, 1, &donneex_jbus_tab->index_es_associe);
              donneex_jbus_tab->LongVarOuCste = donnees_jbus_e_tab->LongVarOuCste;
              if (donneex_jbus_tab->LongVarOuCste == ID_CHAMP_CTE)
                {
                donneex_jbus_tab->longueur = donnees_jbus_e_tab->longueur;
              	}
              else
                {
                erreur = recherche_index_execution (donnees_jbus_e_tab->longueur,
                                                    &donneex_jbus_tab->longueur);
                donneex_jbus_tab->longueur = donneex_jbus_tab->longueur - ContexteGen.nbr_logique;
                }
              break;

            default:
              break;
            } // switch
          if (erreur == 0)
            {
            donneex_jbus_tab->CanalVarOuCste = CanalVarOuCste_cour;
            donneex_jbus_tab->numero_canal = numero_canal_cour;
            donneex_jbus_tab->EsclVarOuCste = EsclVarOuCste_cour;
            donneex_jbus_tab->numero_esclave = numero_esclave_cour;
            donneex_jbus_tab->tipe_esclave = espece_d_auto;
            donneex_jbus_tab->fNbUnitesTimeOut = time_out_cour;
            donneex_jbus_tab->retry = retry_cour;
            }
          break;

        case b_jbus_s_tab :
          switch (genre_cour)
            {
            case c_res_logique :
              insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_log_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_log_tab) + 1);
              donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_log_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_log_tab));
              donnees_jbus_s_tab = (tc_jbus_tab*)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_tab, val_geo.pos_specif_es);
              donneex_jbus_tab->espece_d_auto = espece_d_auto;
              donneex_jbus_tab->AdrVarOuCste = donnees_jbus_s_tab->AdrVarOuCste;
              if (donneex_jbus_tab->AdrVarOuCste == ID_CHAMP_CTE)
                {
                erreur = dwConversionAdresseModbus (espece_d_auto, c_res_s, c_res_logique, donnees_jbus_s_tab->adresse_automate,
                                                   &donneex_jbus_tab->adresse_automate,
                                                   &sens_sur_auto);
                if (erreur == 0)
                  {
                  donneex_jbus_tab->numero_fonction = 15;
                  }
              	}
              else
                {
                erreur = recherche_index_execution (donnees_jbus_s_tab->PosVarAdresse,
                                                    &donneex_jbus_tab->PosVarAdresse);
                donneex_jbus_tab->PosVarAdresse = donneex_jbus_tab->PosVarAdresse - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                }
              erreur = recherche_index_execution (donnees_jbus_s_tab->i_cmd_rafraich,
                                                  &donneex_jbus_tab->i_cmd_rafraich);
              erreur = recherche_index_tableau_i (val_geo.pos_es, 1, &donneex_jbus_tab->index_es_associe);
              donneex_jbus_tab->LongVarOuCste = donnees_jbus_s_tab->LongVarOuCste;
              if (donneex_jbus_tab->LongVarOuCste == ID_CHAMP_CTE)
                {
                donneex_jbus_tab->longueur = donnees_jbus_s_tab->longueur;
              	}
              else
                {
                erreur = recherche_index_execution (donnees_jbus_s_tab->longueur,
                                                    &donneex_jbus_tab->longueur);
                donneex_jbus_tab->longueur = donneex_jbus_tab->longueur - ContexteGen.nbr_logique;
              	}
              break;

            case c_res_numerique :
              switch (genre_num_cour)
                {
                case c_res_entier :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_num_entier_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_entier_tab) + 1);
                  donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_entier_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_entier_tab));
                  break;
                case c_res_mots :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_num_mot_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_mot_tab) + 1);
                  donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_mot_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_mot_tab));
                  break;
                case c_res_reels :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_num_reel_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_reel_tab) + 1);
                  donneex_jbus_tab = (PX_MODBUS_TAB)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_num_reel_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_num_reel_tab));
                  break;
                } // fin switch
              donnees_jbus_s_tab = (tc_jbus_tab*)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_tab, val_geo.pos_specif_es);
              donneex_jbus_tab->espece_d_auto = espece_d_auto;
              donneex_jbus_tab->AdrVarOuCste = donnees_jbus_s_tab->AdrVarOuCste;
              if (donneex_jbus_tab->AdrVarOuCste == ID_CHAMP_CTE)
                {
                erreur = dwConversionAdresseModbus (espece_d_auto, c_res_s, c_res_numerique, donnees_jbus_s_tab->adresse_automate,
                                                   &donneex_jbus_tab->adresse_automate,
                                                   &sens_sur_auto);
                if (erreur == 0)
                  {
                  donneex_jbus_tab->numero_fonction = 16;
                  }
              	}
              else
                {
                erreur = recherche_index_execution (donnees_jbus_s_tab->PosVarAdresse,
                                                    &donneex_jbus_tab->PosVarAdresse);
                donneex_jbus_tab->PosVarAdresse = donneex_jbus_tab->PosVarAdresse - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
                }
              erreur = recherche_index_execution (donnees_jbus_s_tab->i_cmd_rafraich,
                                                  &donneex_jbus_tab->i_cmd_rafraich);
              erreur = recherche_index_tableau_i (val_geo.pos_es, 1, &donneex_jbus_tab->index_es_associe);
              donneex_jbus_tab->LongVarOuCste = donnees_jbus_s_tab->LongVarOuCste;
              if (donneex_jbus_tab->LongVarOuCste == ID_CHAMP_CTE)
                {
                donneex_jbus_tab->longueur = donnees_jbus_s_tab->longueur;
              	}
              else
                {
                erreur = recherche_index_execution (donnees_jbus_s_tab->longueur,
                                                    &donneex_jbus_tab->longueur);
                donneex_jbus_tab->longueur = donneex_jbus_tab->longueur - ContexteGen.nbr_logique;
                }
              break;

            default:
              break;
            } // switch
          if (erreur == 0)
            {
            donneex_jbus_tab->numero_canal = numero_canal_cour;
            donneex_jbus_tab->CanalVarOuCste = CanalVarOuCste_cour;
            donneex_jbus_tab->numero_esclave = numero_esclave_cour;
            donneex_jbus_tab->EsclVarOuCste = EsclVarOuCste_cour;
            donneex_jbus_tab->numero_esclave = numero_esclave_cour;
            donneex_jbus_tab->tipe_esclave = espece_d_auto;
            donneex_jbus_tab->fNbUnitesTimeOut = time_out_cour;
            donneex_jbus_tab->retry = retry_cour;
            }
          break;

        case b_jbus_s_mes :
          insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_s_mes, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_mes) + 1);
          donneex_jbus_s = (PX_MODBUS_S)pointe_enr (szVERIFSource, __LINE__, bx_jbus_s_mes, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_s_mes));
          donnees_jbus_s_mes = (tc_jbus_s_mes*)pointe_enr (szVERIFSource, __LINE__, b_jbus_s_mes, val_geo.pos_specif_es);
          erreur = dwConversionAdresseModbus (espece_d_auto, c_res_s, c_res_numerique, donnees_jbus_s_mes->adresse_automate,
                                             &donneex_jbus_s->adresse_automate,
                                             &sens_sur_auto);
          if (erreur == 0)
            {
						donneex_jbus_s->nbre_a_ecrire = nombre_de_mot (donnees_jbus_s_mes->longueur);
            donneex_jbus_s->numero_fonction = 16;
            donneex_jbus_s->numero_canal = numero_canal_cour;
            donneex_jbus_s->numero_esclave = numero_esclave_cour;
            donneex_jbus_s->tipe_esclave = espece_d_auto;
            donneex_jbus_s->fNbUnitesTimeOut = time_out_cour;
            donneex_jbus_s->retry = retry_cour;
            donneex_jbus_s->longueur = donnees_jbus_s_mes->longueur;
            erreur = recherche_index_execution (val_geo.pos_es, &donneex_jbus_s->index_es_associe);
            donneex_jbus_s->index_es_associe = donneex_jbus_s->index_es_associe - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
            }

          break; // b_jbus_s_mes

        default:
          break;

        } // switch val_geo.num_repere

      } // while index < nb_enr

    } /* existe b_geo_jbus */
  return (erreur);
  }

//--------------------------------------------------------------------------

static void organise_bloc_jbus_entree (DWORD bg_jbus)
  {
  #define TAILLE_CLE 31

  char chaine[TAILLE_CLE];
  char cle_cour[TAILLE_CLE];
  PSTR cle;
  DWORD pos_a_inserer;
  DWORD repere;
  tg_jbus*donnees_jbus;
  tg_jbus donnees_jbus_temp;

  if ((existe_repere (bg_jbus)) && (nb_enregistrements (szVERIFSource, __LINE__, bg_jbus) != 0))
    {
    if (!existe_repere (b_organise_cle))
      {
      cree_bloc (b_organise_cle, TAILLE_CLE, 0);
      }
    repere = 1;
    while (repere <= nb_enregistrements (szVERIFSource, __LINE__, bg_jbus))
      {
      donnees_jbus = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus, repere);
      //------------------------------------------------------------
      //                   elaboration cle
      // pour positionner : (derniere position voulue + 1) - longueur
      //-------------------------------------------------------------
      StrCopy (cle_cour, "000000000000000000000000000000");
      StrDWordToStr (chaine, donnees_jbus->numero_canal);
      MemCopy (&(cle_cour[2-StrLength(chaine)]), chaine, StrLength(chaine));
      StrDWordToStr (chaine, donnees_jbus->numero_esclave);
      MemCopy (&(cle_cour[12-StrLength(chaine)]), chaine, StrLength(chaine));
      StrDWordToStr (chaine, donnees_jbus->i_cmd_rafraich);
      MemCopy (&(cle_cour[22-StrLength(chaine)]), chaine, StrLength(chaine));
      StrDWordToStr (chaine, donnees_jbus->sens_sur_auto);
      MemCopy (&(cle_cour[24-StrLength(chaine)]), chaine, StrLength(chaine));
      StrDWordToStr (chaine, donnees_jbus->adresse_automate);
      MemCopy (&(cle_cour[29-StrLength(chaine)]), chaine, StrLength(chaine));
      //-------------------------------------------------------------
      //          recherche de la position d'insertion
      //-------------------------------------------------------------
      bTrouveChaineDansOrganiseCle (cle_cour, &pos_a_inserer);
      //-------------------------------------------------------------
      //              mise � jour du bloc des cles
      //-------------------------------------------------------------
      insere_enr (szVERIFSource, __LINE__, 1, b_organise_cle, pos_a_inserer);
      cle = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_organise_cle, pos_a_inserer);
      StrCopy (cle, cle_cour);
      //-------------------------------------------------------------
      //              mise � jour de la table elle_m�me
      //-------------------------------------------------------------
      donnees_jbus = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus, repere);
      donnees_jbus_temp = (*donnees_jbus);
      insere_enr (szVERIFSource, __LINE__, 1, bg_jbus, pos_a_inserer);
      donnees_jbus = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus, pos_a_inserer);
      (*donnees_jbus) = donnees_jbus_temp;
      if (pos_a_inserer < repere)
        {
        enleve_enr (szVERIFSource, __LINE__, 1, bg_jbus, repere + 1);
      	}
      else
        {
        enleve_enr (szVERIFSource, __LINE__, 1, bg_jbus, repere);
        }
      repere++;
      } // while
    if (existe_repere (b_organise_cle))
      {
      enleve_bloc (b_organise_cle);
      }
    } // existe bg_jbus
  }

//--------------------------------------------------------------------------
static void elabore_trame (DWORD bg_jbus, DWORD bx_para_jbus, DWORD bx_jbus_e,
                           DWORD genre, DWORD taille_trame_max, DWORD ecart_adresse)
  {
  PX_PARAMETRE_TRAME_MODBUS parametres_trames;
  PX_MODBUS_E	donneex_jbus;
  tg_jbus *donnees_jbus;
  BOOL fini;
  BOOL autre_cle;
  BOOL depasse;
  DWORD pos_debut;
  DWORD pos_fin;
  DWORD pos_cour;
  DWORD nbr_enr;
  DWORD nbr_mots;
  DWORD numero_canal_cour;
  DWORD numero_esclave_cour;
  DWORD tipe_esclave_cour;
  DWORD i_cmd_rafraich_cour;
  DWORD sens_sur_auto_cour;
  FLOAT time_out_cour;
  DWORD retry_cour;
  DWORD adresse_automate_depart;
  DWORD adresse_automate_cour;
  DWORD longueur_cour;
  DWORD EsclVarOuCste_cour;
  DWORD CanalVarOuCste_cour;


  if ((existe_repere (bg_jbus) && (nb_enregistrements (szVERIFSource, __LINE__, bg_jbus) != 0)))
    {
    if (!existe_repere (bx_para_jbus))
      {
      cree_bloc (bx_para_jbus, sizeof (X_PARAMETRE_TRAME_MODBUS), 0);
      }
    if (!existe_repere (bx_jbus_e))
      {
      cree_bloc (bx_jbus_e, sizeof (X_MODBUS_E), 0);
      }
    pos_fin = 0;
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bg_jbus);
    do
      {
      pos_debut = pos_fin + 1;
      pos_fin = pos_debut;
      donnees_jbus = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus, pos_fin);
      numero_canal_cour = donnees_jbus->numero_canal;
      CanalVarOuCste_cour = donnees_jbus->CanalVarOuCste;
      numero_esclave_cour = donnees_jbus->numero_esclave;
      EsclVarOuCste_cour = donnees_jbus->EsclVarOuCste;
      tipe_esclave_cour = donnees_jbus->tipe_esclave;
      time_out_cour = donnees_jbus->fNbUnitesTimeOut;
      retry_cour = donnees_jbus->retry;
      i_cmd_rafraich_cour = donnees_jbus->i_cmd_rafraich;
      sens_sur_auto_cour = donnees_jbus->sens_sur_auto;
      adresse_automate_depart = donnees_jbus->adresse_automate;
      do
        {
        pos_cour = pos_fin;
        insere_enr (szVERIFSource, __LINE__, 1, bx_jbus_e, nb_enregistrements (szVERIFSource, __LINE__, bx_jbus_e) + 1);
        donnees_jbus = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus, pos_cour);
        adresse_automate_cour = donnees_jbus->adresse_automate;
        longueur_cour = donnees_jbus->longueur;
        donneex_jbus = (PX_MODBUS_E)pointe_enr (szVERIFSource, __LINE__, bx_jbus_e, pos_cour);
        donneex_jbus->index_es_associe = donnees_jbus->index_es_associe;
        if (bg_jbus == bg_jbus_e_num_reel)
          {
          donneex_jbus->i_tab_recep_significatif = ((donnees_jbus->adresse_automate - adresse_automate_depart) / 2 + 1);
          }
        else
          {
          donneex_jbus->i_tab_recep_significatif = donnees_jbus->adresse_automate - adresse_automate_depart + 1;
          }
        donneex_jbus->longueur = donnees_jbus->longueur;
        pos_fin++;
        fini = (pos_fin > nbr_enr);
        if (!fini)
          {
          donnees_jbus = (tg_jbus*)pointe_enr (szVERIFSource, __LINE__, bg_jbus, pos_fin);
          autre_cle = (donnees_jbus->numero_canal != numero_canal_cour) ||
                       (donnees_jbus->numero_esclave != numero_esclave_cour) ||
                       (donnees_jbus->sens_sur_auto != sens_sur_auto_cour) ||
                       (donnees_jbus->fNbUnitesTimeOut != time_out_cour) ||
                       (donnees_jbus->retry != retry_cour) ||
                       (donnees_jbus->i_cmd_rafraich != i_cmd_rafraich_cour);
          if (!autre_cle)
            {
            if (bg_jbus == bg_jbus_e_mes)
              {
              nbr_mots = nombre_de_mot (donnees_jbus->longueur);
              }
            else
              {
              nbr_mots = 0;
              }
            depasse = (donnees_jbus->adresse_automate + nbr_mots - adresse_automate_depart >= taille_trame_max) ||
                      (donnees_jbus->adresse_automate - adresse_automate_cour > ecart_adresse) ||
                      (donnees_jbus->adresse_automate == adresse_automate_cour);
            }
          } // !fini
        } while ((!fini) && (!autre_cle) && (!depasse));
      pos_fin = pos_cour;
      insere_enr (szVERIFSource, __LINE__, 1, bx_para_jbus, nb_enregistrements (szVERIFSource, __LINE__, bx_para_jbus) + 1);
      parametres_trames = (PX_PARAMETRE_TRAME_MODBUS)pointe_enr (szVERIFSource, __LINE__, bx_para_jbus, nb_enregistrements (szVERIFSource, __LINE__, bx_para_jbus));
      parametres_trames->numero_canal = numero_canal_cour;
      parametres_trames->CanalVarOuCste = CanalVarOuCste_cour;
      parametres_trames->numero_esclave = numero_esclave_cour;
      parametres_trames->EsclVarOuCste = EsclVarOuCste_cour;
      parametres_trames->tipe_esclave = tipe_esclave_cour;
      parametres_trames->fNbUnitesTimeOut = time_out_cour;
      parametres_trames->retry = retry_cour;
      parametres_trames->adresse_automate_depart = adresse_automate_depart;
      parametres_trames->i_cmd_rafraich = i_cmd_rafraich_cour;
      parametres_trames->index_debut = pos_debut;
      parametres_trames->index_fin = pos_fin;
      if (bg_jbus == bg_jbus_e_mes)
        {
        nbr_mots = nombre_de_mot (longueur_cour);
      	}
      else
        {
        nbr_mots = 1;
        }
      if (bg_jbus == bg_jbus_e_num_reel)
        {
        parametres_trames->nbre_a_lire = (adresse_automate_cour - adresse_automate_depart) / 2 + nbr_mots;
      	}
      else
        {
        parametres_trames->nbre_a_lire = adresse_automate_cour - adresse_automate_depart + nbr_mots ;
        }
      switch (genre)
        {
        case c_res_logique :
          if (sens_sur_auto_cour == c_res_e)
            {
            parametres_trames->numero_fonction = 2;
            }
          else
            {
            parametres_trames->numero_fonction = 1;
            }
          break;
        case c_res_numerique:
        case c_res_message :
          if (sens_sur_auto_cour == c_res_e)
            {
            parametres_trames->numero_fonction = 4;
            }
          else
            {
            parametres_trames->numero_fonction = 3;
            }
          break;
        } // switch genre
      } while (!fini);
    } // existe bg_jbus
  }

//--------------------------------------------------------------------------

DWORD genere_jb (DWORD *ligne)
  {
  DWORD erreur;

  erreur = elabore_donnees_jbus ();
  if (erreur == 0)
    {
    if (existe_repere (b_jbus_e))
      {
      enleve_bloc (b_jbus_e);
      }
    if (existe_repere (b_jbus_e_tab))
      {
      enleve_bloc (b_jbus_e_tab);
      }
    if (existe_repere (b_jbus_s))
      {
      enleve_bloc (b_jbus_s);
      }
    if (existe_repere (b_jbus_s_tab))
      {
      enleve_bloc (b_jbus_s_tab);
      }
    if (existe_repere (b_jbus_e_mes))
      {
      enleve_bloc (b_jbus_e_mes);
      }
    if (existe_repere (b_jbus_s_mes))
      {
      enleve_bloc (b_jbus_s_mes);
      }
    if (existe_repere (b_titre_paragraf_jb))
      {
      enleve_bloc (b_titre_paragraf_jb);
      }
    if (existe_repere (b_spec_titre_paragraf_jb))
      {
      enleve_bloc (b_spec_titre_paragraf_jb);
      }

    // tri des trois tables d'execution jbus d'entree
    organise_bloc_jbus_entree (bg_jbus_e_log);
    organise_bloc_jbus_entree (bg_jbus_e_num_entier);
    organise_bloc_jbus_entree (bg_jbus_e_num_mot);
    organise_bloc_jbus_entree (bg_jbus_e_num_reel);
    organise_bloc_jbus_entree (bg_jbus_e_mes);

    // elaboration des trames jbus d'entree
    if (existe_repere (bg_jbus_e_log))
      {
      elabore_trame (bg_jbus_e_log, bx_para_jbus_log, bx_jbus_e_log, c_res_logique, 2048, 112);
      enleve_bloc (bg_jbus_e_log);
      }

    if (existe_repere (bg_jbus_e_num_entier))
      {
      elabore_trame (bg_jbus_e_num_entier, bx_para_jbus_num_entier, bx_jbus_e_num_entier, c_res_numerique, 128, 7);
      enleve_bloc (bg_jbus_e_num_entier);
      }
    if (existe_repere (bg_jbus_e_num_mot))
      {
      elabore_trame (bg_jbus_e_num_mot, bx_para_jbus_num_mot, bx_jbus_e_num_mot, c_res_numerique, 128, 7);
      enleve_bloc (bg_jbus_e_num_mot);
      }
    if (existe_repere (bg_jbus_e_num_reel))
      {
      elabore_trame (bg_jbus_e_num_reel, bx_para_jbus_num_reel, bx_jbus_e_num_reel, c_res_numerique, 128, 4);
      enleve_bloc (bg_jbus_e_num_reel);
      }

    if (existe_repere (bg_jbus_e_mes))
      {
      elabore_trame (bg_jbus_e_mes, bx_para_jbus_mes, bx_jbus_e_mes, c_res_message, 128, 7);
      enleve_bloc (bg_jbus_e_mes);
      }

    } // erreur = 0
  return (erreur);
  }

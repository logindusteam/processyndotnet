/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_TP.C                                               |
 |   Auteur  : RP                                                       |
 |   Date    : 06/09/94                                                 |
 +----------------------------------------------------------------------*/


#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "UChrono.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "tipe_tp.h"
#include "findbdex.h"
#include "mem.h"
#include "temps.h"
#include "GenereBD.h"

#include "gener_tp.h"
#include "Verif.h"
VerifInit;

//----------------------------------------------------------------------------
//                   generation chronometre et metronome
//----------------------------------------------------------------------------

DWORD genere_cn (DWORD *ligne)
  {
  PGEO_CHRONO	pValGeo;
  GEO_CHRONO      ValGeo;
  DWORD              wRetour;
  DWORD              wLigneCour;
  PTITRE_PARAGRAPHE	pTitreParagraf;
  PX_CHRONOMETRE	pChronometre;
  PX_METRONOME	pMetronomeX;
  PLONG pMetronome;


  wRetour = 0;
  if (existe_repere (b_geo_chrono) && (nb_enregistrements (szVERIFSource, __LINE__, b_geo_chrono) != 0))
    {
    wLigneCour = 0;
    while (wLigneCour < nb_enregistrements (szVERIFSource, __LINE__, b_geo_chrono))
      {
      wLigneCour++;
      pValGeo = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono,wLigneCour);
      ValGeo = (*pValGeo);
      switch (ValGeo.numero_repere)
        {
        case b_pt_constant:
          break;

        case b_titre_paragraf_chrono :
          pTitreParagraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_chrono,ValGeo.pos_paragraf);
          switch (pTitreParagraf->IdParagraphe)
            {
            case chronometre_cn :
              if (!existe_repere (bx_chronometre))
                {
                cree_bloc (bx_chronometre,sizeof (*pChronometre),0);
                }
              break;

            case metronome_cn :
              if (!existe_repere (bx_metronome))
                {
                cree_bloc (bx_metronome,sizeof (*pMetronomeX),0);
                }
              break;
           
            default:
               break;
 
            }
          break;

        case b_e_s:
          insere_enr (szVERIFSource, __LINE__, 1,bx_chronometre,1);
          pChronometre = (PX_CHRONOMETRE)pointe_enr (szVERIFSource, __LINE__, bx_chronometre,1);
          wRetour = recherche_index_execution(ValGeo.pos_es,&(pChronometre->index_dans_bd));
          pChronometre->index_dans_bd = pChronometre->index_dans_bd - ContexteGen.nbr_logique;
          ChronoInitSuspendu (&(pChronometre->chrono));
          break;

        case b_metronome:
          insere_enr (szVERIFSource, __LINE__, 1,bx_metronome,1);
          pMetronomeX = (PX_METRONOME)pointe_enr (szVERIFSource, __LINE__, bx_metronome,1);
          pMetronome = (PLONG)pointe_enr (szVERIFSource, __LINE__, b_metronome,ValGeo.pos_specif_es);
          pMetronomeX->val_duree = (*pMetronome);
          MinuterieInitSuspendue (&pMetronomeX->tempo);
          wRetour = recherche_index_execution(ValGeo.pos_es,&(pMetronomeX->index_dans_bd));
          break;

        default:
          break;

        }
      }
    }
  enleve_bloc (b_titre_paragraf_chrono);
  enleve_bloc (b_metronome);
  if (wRetour != 0)
    {
    (*ligne) = wLigneCour;
    }
  return(wRetour);
  }

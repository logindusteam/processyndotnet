/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : actiongn.h                                               |
 |   Auteur  : LM					        	|
 |   Date    : 07/12/92 				        	|
 |   Version : 3.20							|
 +----------------------------------------------------------------------*/

// ------------------  PROCEDURES
#define  action_choisit_application 0
#define  action_genere             1
#define  action_quitte             2
#define  action_aide               3

#define  action_rien 255

// dernier utilis� : 3

typedef struct
	{
	char lettre;
	char chaine[MAX_PATH];
	BOOL booleen;
	int entier16;
	DWORD mot;
	}  t_param_action;

void creer_action (void);
int ajouter_action (DWORD type_action, t_param_action *param);
int executer_action (void);

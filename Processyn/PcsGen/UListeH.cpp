/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : UListeH.c                                               |
 |   Auteur  : JS					        	|
 |   Date    : 23/7/95 					        	|
 |   Gestion de tableaux de handles
 |   RESTRICTIONS : pas d'IPC
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "Std.h"
#include "MemMan.h"
#include "BufMan.h"
//
#include "UListeH.h"

// Structure interne d'un objet liste de handles
typedef struct
	{
	DWORD		dwSignature;							// Liste couramment Initialis�
	DWORD		dwNbHandles;				// Nombre courant de handles dans la liste (NULLs ou pas).
	HANDLE	handleInvalide;			// Valeur d'un handle invalide ou non allou�(pas forc�ment NULL)
	BOOL		bAccesMultiThread;	// TRUE si l'utilisateur veut avoir un acc�s multi thread
	HANDLE * pHandles;					// Adresse du tableau courant de handles
	DWORD		dwTailleBufHandles;	// Taille en Octets de ce buffer
	HANDLE	hMutexAccesHandles;	// s�maphore d'acc�s mutellement exclusif � cet objet (si multi thread)
	} LISTE_H, *PLISTE_H;

#define DW_SIGNATURE 'LstH'
//------------------------------------------------------------------------------
// Conversions Handle / pointeur objet LISTE_H
static __inline PLISTE_H pFromHLISTE_H (HLISTE_H hListeH)
	{
	PLISTE_H pListeH = (PLISTE_H)hListeH;
	if (!pListeH || (pListeH->dwSignature != DW_SIGNATURE))
		pListeH = NULL;

	return pListeH;
	}

static __inline HLISTE_H hFromPLISTE_H (PLISTE_H pListeH)
	{
	return (HLISTE_H)pListeH;
	}

//------------------------------------------------------------------------------
void AccesModifListeH (HLISTE_H hListeH)
	{
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);

	if (pListeH && pListeH->bAccesMultiThread)
		WaitForSingleObject (pListeH->hMutexAccesHandles, INFINITE);
	}

void FinAccesModifListeH (HLISTE_H hListeH)
	{
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);

	if (pListeH && pListeH->bAccesMultiThread)
		ReleaseMutex (pListeH->hMutexAccesHandles);
	}

//------------------------------------------------------------------------------
void AccesLectureListeH (HLISTE_H hListeH)
	{ // $$ am�liorer
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);

	if (pListeH && pListeH->bAccesMultiThread)
		WaitForSingleObject (pListeH->hMutexAccesHandles, INFINITE);
	}

void FinAccesLectureListeH (HLISTE_H hListeH)
	{
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);

	if (pListeH && pListeH->bAccesMultiThread)
		ReleaseMutex (pListeH->hMutexAccesHandles);
	}

//------------------------------------------------------------------------------
// initialisation du LISTE_H
BOOL bCreeListeH (PHLISTE_H phListeH, HANDLE handleInvalide, BOOL bAccesMultiThread)
	{
	PLISTE_H pListeH = (PLISTE_H)pMemAlloueInit0 (sizeof (LISTE_H));
	BOOL	bRes = TRUE;

	// Initialise les �l�ments de la liste
	*phListeH = NULL;
	pListeH->dwSignature = DW_SIGNATURE;
	pListeH->handleInvalide = handleInvalide;
	pListeH->bAccesMultiThread = bAccesMultiThread;
	pListeH->pHandles = NULL;
	pListeH->dwTailleBufHandles = 0;
	pListeH->dwNbHandles = 0;
	if (bAccesMultiThread)
		{
		pListeH->hMutexAccesHandles = CreateMutex (NULL, FALSE, NULL); // s�curit�, owned, nom
		if (!pListeH->hMutexAccesHandles)
			bRes = FALSE;
		}
	else
		{
		pListeH->hMutexAccesHandles = NULL;
		}

	if (bRes)
		*phListeH = hFromPLISTE_H (pListeH);
	else
		{
		MemLibere ((PVOID *)(&pListeH));
		*phListeH = NULL;
		}

	return bRes;
  } // bCreeListeH

//------------------------------------------------------------------------------
// fermeture du LISTE_H
BOOL bFermeListeH (PHLISTE_H phListeH)
	{
	PLISTE_H	pListeH = pFromHLISTE_H (*phListeH);
	BOOL			bRes = FALSE;

	if (pListeH)
		{
		pListeH->dwSignature = 0;
		if (pListeH->bAccesMultiThread)
			CloseHandle (pListeH->hMutexAccesHandles);
		if (pListeH->pHandles)
			MemLibere (pListeH->pHandles);
		pListeH->pHandles = NULL;
		pListeH->dwTailleBufHandles = 0;
		pListeH->dwNbHandles = 0;
		MemLibere ((PVOID *)(&pListeH));
		bRes = TRUE;
		*phListeH = NULL;
		}
	return bRes;
	} // bFermeListeH

// -----------------------------------------------------------------------------
// recherche d'un handle dans le tableau
PHANDLE phTrouveDansListeH (HLISTE_H hListeH, HANDLE handle)
	{
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);
	DWORD dwNHandle;
	PHANDLE phTrouve = pListeH->pHandles;

	// Recherche du handle dans le tableau
	for (dwNHandle = 0; dwNHandle < pListeH->dwNbHandles; dwNHandle++)
		{
		// trouv� ?
		if (*phTrouve == handle)
			// renvoie son adresse
			return phTrouve;

		// adresse suivante
		phTrouve++;
		}
	// non trouv� => adresse vaut 0
	return NULL;
	} // phTrouveDansListeH

// -----------------------------------------------------------------------------
// ajoute un handle � la liste des handles
BOOL bAjouteDansListeH (HLISTE_H hListeH, HANDLE handle)
	{
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);
	BOOL	bRet = FALSE;
	PHANDLE phandle; 

	// Ajoute le handle au tableau : recherche d'un trou
	phandle = phTrouveDansListeH (hListeH, pListeH->handleInvalide);

	// trou trouv� ?
	if (phandle)
		{
		// oui => stocke le handle dans le trou
		*phandle = handle;
		bRet = TRUE;
		}
	// Handle d�ja ajout� ? 
	if (!bRet)
		{
		// non => on rajoute le handle � la fin de la liste des handles
		if (bBufAjouteFin ((PSTR *)&pListeH->pHandles, &pListeH->dwTailleBufHandles, (PSTR)&handle, sizeof (handle)))
			{
			// un handle de plus
			pListeH->dwNbHandles ++;

			bRet = TRUE;
			}
		}

	return bRet;
	} // bAjouteDansListeH

// -----------------------------------------------------------------------------
// Supprime un handle de la liste 
BOOL bSupprimeDansListeH (HLISTE_H hListeH, HANDLE handle)
	{
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);
	BOOL	bRet = FALSE;

	// Handle trouv� dans la liste ?
	PHANDLE phandle = phTrouveDansListeH (hListeH, handle);

	if (phandle)
		{
		// oui => Ok, on le supprime
		*phandle = pListeH->handleInvalide;
		bRet = TRUE;
		}

	return bRet;
	} //bSupprimeDansListeH

// -----------------------------------------------------------------------------
// R�cup�re le handle num�ro dwNHandle de la liste (de 0 � n) ou handleInvalide si hors limite
HANDLE hDansListeH (HLISTE_H hListeH, DWORD dwNHandle)
	{
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);
	HANDLE hRes = NULL;

	if (pListeH)
		{
		if (dwNHandle < pListeH->dwNbHandles)
			hRes = pListeH->pHandles[dwNHandle];
		else
			hRes = pListeH->handleInvalide;
		}

	return hRes;
	} // hDansListeH

// -----------------------------------------------------------------------------
// Renvoie le nombre de handle ouverts ou invalides de la liste
DWORD dwNbHDansListeH (HLISTE_H hListeH)
	{
	PLISTE_H pListeH = pFromHLISTE_H (hListeH);
	DWORD dwRes = 0;

	if (pListeH)
		dwRes = pListeH->dwNbHandles;

	return dwRes;
	} // dwNbHDansListeH

//------------------- fin UListeH.c --------------------------------------

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inpugn.c                                                 |
 |   Auteur  : LM					        	|
 |   Date    : 07/12/92 					        |
 |   Version : 3.20							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "mem.h"
#include "UStr.h"
#include "DocMan.h"
#include "actiongn.h"
#include "cmdpcs.h"       // fichier de commande
#include "Verif.h"
VerifInit;


#include "inpugn.h"

static void InterpreteBlocCmd (BOOL bCdeValide);


// --------------------------------------------------------------------------
// analyse du bloc de commandes et ajout des actions associees
// --------------------------------------------------------------------------
static void InterpreteBlocCmd (BOOL bCdeValide)
  {
  t_param_action param_action;
  DWORD           wNbEnr;
  DWORD           wIndex;
  PCOMMANDE_PCS	pCommandes;

  if (bCdeValide)
    {
    if (existe_repere (BLOC_COMMANDES))
      {
      wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, BLOC_COMMANDES);
      }
    else
      {
      wNbEnr = 0;
      }
    if (wNbEnr)
      {
      for (wIndex = 1; wIndex <= wNbEnr; wIndex++)
        {
        pCommandes = (PCOMMANDE_PCS)pointe_enr (szVERIFSource, __LINE__, BLOC_COMMANDES, wIndex);
        StrCopy (param_action.chaine, pCommandes->pszChaine);
        switch (pCommandes->carCmd)
          {
          case  '\0': // chaine sans commande
            ajouter_action (action_choisit_application, &param_action);
            break;

          case  'G':  // charge + genere
            ajouter_action (action_choisit_application, &param_action);
            ajouter_action (action_genere, &param_action);
            break;

          case  'Q':  // quitte
            ajouter_action (action_quitte, &param_action);
            break;

          default:
              break;
          }  // ------------ switch (char_cmd)
        }
      enleve_bloc (BLOC_COMMANDES);
      }
    else
      { // rien ds la cde ni ds cmdpcs
      StrCopy (param_action.chaine, pszPathNameDoc());
      ajouter_action (action_choisit_application, &param_action);
		  }
    }
  else
    { // cde non valide
    ajouter_action (action_quitte, &param_action);
    }
  }

// ------------------  lecture cmd : demarrage, clavier ou macro ----------
// ------------------------------------------------------------------------
int lire_commandes (t_param_lecture *param_lecture)
  {
  int     rretour = 0;
  BOOL bCdeValide;

  switch (param_lecture->mode_lecture)
    {
    case mode_demarrage:
      bCdeValide = FabriqueBlocCmd (param_lecture->ligne_commande);
      InterpreteBlocCmd (bCdeValide);
      break;

    default:
      break;
    } // switch mode lecture

  return (rretour);
  }

/***************************************************************************
* SPROMEPS.H
*
* (C) Copyright 1989-1993 Rainbow Technologies, Inc. All rights reserved.
*
* Description - SuperPro Multiple Entry Points Header file.
*
* Purpose     - This module provides a method for performing SuperPro API
*               commands so you do not have to deal with command packets.
*               It provides a function for each API command.                                                    * 
*
****************************************************************************/
#ifndef _SPROMEPS_H
#define _SPROMEPS_H

                                       /* for dword alignment roll-up      */
#define SPRO_APIPACKET_ALIGNMENT_VALUE (sizeof(unsigned long))
#define SPRO_APIPACKET_SIZE            (1024+SPRO_APIPACKET_ALIGNMENT_VALUE)
#define SPRO_MAX_QUERY_SIZE            56              /* in bytes         */

/**  SuperPro API error codes.  **/
#define SP_SUCCESS                      0
#define SP_INVALID_FUNCTION_CODE        1
#define SP_INVALID_PACKET               2
#define SP_UNIT_NOT_FOUND               3
#define SP_ACCESS_DENIED                4
#define SP_INVALID_MEMORY_ADDRESS       5
#define SP_INVALID_ACCESS_CODE          6
#define SP_PORT_IS_BUSY                 7
#define SP_WRITE_NOT_READY              8
#define SP_NO_PORT_FOUND                9
#define SP_ALREADY_ZERO                 10
#define SP_DRIVER_OPEN_ERROR            11
#define SP_DRIVER_NOT_INSTALLED         12
#define SP_IO_COMMUNICATIONS_ERROR      13
#define SP_PACKET_TOO_SMALL             15
#define SP_INVALID_PARAMETER            16
#define SP_MEM_ACCESS_ERROR             17
#define SP_VERSION_NOT_SUPPORTED        18
#define SP_OS_NOT_SUPPORTED             19
#define SP_QUERY_TOO_LONG               20
#define SP_INVALID_COMMAND              21
#define SP_MEM_ALIGNMENT_ERROR          29
#define SP_DRIVER_IS_BUSY               30
#define SP_PORT_ALLOCATION_FAILURE      31
#define SP_PORT_RELEASE_FAILURE         32
#define SP_ACQUIRE_PORT_TIMEOUT         39
#define SP_SIGNAL_NOT_SUPPORTED         42
#define SP_UNKNOWN_MACHINE              44
#define SP_SYS_API_ERROR                45
#define SP_UNIT_IS_BUSY                 46
#define SP_INVALID_PORT_TYPE            47
#define SP_INVALID_MACH_TYPE            48
#define SP_INVALID_IRQ_MASK             49
#define SP_INVALID_CONT_METHOD          50
#define SP_INVALID_PORT_FLAGS           51
#define SP_INVALID_LOG_PORT_CFG         52
#define SP_INVALID_OS_TYPE              53
#define SP_INVALID_LOG_PORT_NUM         54
#define SP_INVALID_ROUTER_FLGS          56
#define SP_INIT_NOT_CALLED              57
#define SP_DRVR_TYPE_NOT_SUPPORTED      58
#define SP_FAIL_ON_DRIVER_COMM          59


/* Define possible driver types (Not used refer to Driver Types). */
#define SP_DRVR_WIN31             4
#define SP_DRVR_WINNT             5

/* Create SP types */
#ifdef __cplusplus
#define SP_EXPORT extern "C"
#else
#define SP_EXPORT extern
#endif
#define SP_LOCAL static

#if (defined(__OS2__) || defined(OS2_INCLUDED) )
#ifndef _OS2_
#define _OS2_
#endif
#endif

#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__))
#ifndef _WIN32_
#define _WIN32_ 1
#endif
#endif

/****************************************************************************
* SETTINGS FOR IBM C SET/2
****************************************************************************/
#if defined(__IBMC__)
#if defined(_OS2_)
#define SP_STDCALL
#define SP_FASTCALL _Fastcall
#define SP_PASCAL   _Pascal
#define SP_CDECL    _Cdecl
#define SP_FAR
#define SP_NEAR
#define SP_HUGE
#define SP_API      EXPENTRY
#define SP_PTR      *
#endif
#endif
/***************************************************************************/

/****************************************************************************
* SETTINGS FOR BORLAND C
****************************************************************************/
#if defined(__BORLANDC__)
#if defined(_WIN32_)
#define SP_STDCALL  __stdcall
#define SP_FASTCALL __fastcall
#define SP_PASCAL
#define SP_CDECL    __cdecl
#define SP_FAR
#define SP_NEAR
#define SP_HUGE
#define SP_API      WINAPI
#define SP_PTR      *
#elif (defined(__OS2__))
#define SP_STDCALL
#define SP_FASTCALL __fastcall
#define SP_PASCAL   __pascal
#define SP_CDECL    __cdecl
#define SP_FAR
#define SP_NEAR
#define SP_HUGE
#define SP_PTR     *
#define SP_API     EXPENTRY
#elif (defined(OS2_INCLUDED))
#define SP_LOADDS _loadds
#define SP_STDCALL
#define SP_FASTCALL
#define SP_PASCAL  pascal
#define SP_CDECL   _cdecl
#define SP_FAR     far
#define SP_NEAR    near
#define SP_HUGE    huge
#define SP_PTR     SP_FAR *
#define SP_API     SP_FAR SP_PASCAL
#else
#define SP_STDCALL
#define SP_FASTCALL
#define SP_PASCAL  pascal
#define SP_CDECL   _cdecl
#define SP_FAR     far
#define SP_NEAR    near
#define SP_HUGE    huge
#define SP_PTR     SP_FAR *
#define SP_API     SP_FAR SP_PASCAL
#endif
#endif
/***************************************************************************/

/****************************************************************************
* SETTINGS FOR MICROSOFT C
****************************************************************************/
#if  defined(_MSC_VER)
#if  defined(_WIN32_)
#define SP_STDCALL  __stdcall
#define SP_FASTCALL __fastcall
#define SP_PASCAL
#define SP_CDECL    __cdecl
#define SP_FAR
#define SP_NEAR
#define SP_HUGE
#if (defined(_DRVDRV_) || defined(_TESTDRVR_))
#define SP_API      SP_STDCALL
#else
#define SP_API      WINAPI
#endif
#define SP_PTR      *
#elif defined(_OS2_)
#define SP_STDCALL
#define SP_FASTCALL _fastcall
#define SP_PASCAL   _pascal
#define SP_CDECL    _cdecl
#define SP_FAR      _far
#define SP_NEAR     _near
#define SP_HUGE     _huge
#define SP_API      SP_FAR SP_PASCAL
#define SP_PTR      SP_FAR *
#elif (_MSC_VER <= 7)
#define SP_LOADDS   _loadds
#define SP_STDCALL
#define SP_FASTCALL _fastcall
#define SP_PASCAL   _pascal
#define SP_CDECL    _cdecl
#define SP_FAR      _far
#define SP_NEAR     _near
#define SP_HUGE     _huge
#define SP_API      SP_FAR SP_PASCAL
#define SP_PTR      SP_FAR *
#else
#define SP_STDCALL  __stdcall
#define SP_FASTCALL __fastcall
#define SP_PASCAL
#define SP_CDECL    __cdecl
#define SP_FAR
#define SP_NEAR
#define SP_HUGE
#define SP_API      SP_CDECL
#define SP_PTR      *
#endif
#endif
/***************************************************************************/

/****************************************************************************
* SETTINGS FOR WATCOM C
****************************************************************************/
#if defined(__WATCOMC__)
#if defined(_WIN32_)
#define SP_STDCALL  __stdcall
#define SP_FASTCALL __fastcall
#define SP_PASCAL   
#define SP_CDECL    __cdecl
#define SP_FAR
#define SP_NEAR
#define SP_HUGE
#define SP_API      WINAPI
#define SP_PTR      *
#elif defined(_OS2_)
#define SP_STDCALL
#define SP_FASTCALL __fastcall
#define SP_PASCAL   __pascal
#define SP_CDECL    __cdecl
#define SP_FAR
#define SP_NEAR
#define SP_HUGE
#define SP_API      EXPENTRY
#define SP_PTR      SP_FAR *
#else
#define SP_STDCALL
#define SP_FASTCALL __fastcall
#define SP_PASCAL   __pascal
#define SP_CDECL    __cdecl
#define SP_FAR
#define SP_NEAR
#define SP_HUGE
#define SP_API      SP_FAR SP_PASCAL
#define SP_PTR      SP_FAR *
#endif
#endif
/***************************************************************************/

#define SP_IN
#define SP_OUT
#define SP_IO

#if !defined(_RBTYPES_INC)

typedef                void  RB_VOID;
typedef unsigned       char  RB_BOOLEAN;
typedef unsigned       char  RB_BYTE;
typedef unsigned short int   RB_WORD;
typedef unsigned long  int   RB_DWORD;

typedef RB_VOID    SP_PTR RBP_VOID;
typedef RB_BYTE    SP_PTR RBP_BYTE;
typedef RB_BOOLEAN SP_PTR RBP_BOOLEAN;
typedef RB_WORD    SP_PTR RBP_WORD;
typedef RB_DWORD   SP_PTR RBP_DWORD;

#endif


typedef RB_DWORD RB_SPRO_APIPACKET[SPRO_APIPACKET_SIZE/sizeof(RB_DWORD)];
typedef RB_WORD  SP_STATUS;
typedef RBP_VOID RBP_SPRO_APIPACKET;


/* provided for packward compatibility for OS/2 spromeps.h */
#undef RNBO_SPRO_API
#define RNBO_SPRO_APIPACKET     RB_SPRO_APIPACKET
#define RNBO_SPRO_APIPACKET_PTR RBP_SPRO_APIPACKET
#define RNBO_SPRO_API           SP_API
#define RNBO_USHORT_PTR         RBP_WORD
#define RNBO_UCHAR_PTR          RBP_BYTE
#define RNBO_QUERY_PTR          RBP_BYTE
#define RNBO_ULONG_PTR          RBP_DWORD

/* machine types */
#define RB_MIN_MACH_TYPE          0
#define RB_AUTODETECT_MACHINE     0          /* Autodetect machine type    */
#define RB_IBM_MACHINE            1          /* defines IBM type hw        */
#define RB_NEC_MACHINE            2          /* defines NEC PC-9800 hw     */
#define RB_FMR_MACHINE            3          /* defines Fujitsu FMR hw     */
#define RB_MAX_MACH_TYPE          3

/* OS types */
#define RB_MIN_OS_TYPE            0
#define RB_AUTODETECT_OS_TYPE     0          /* Autodetect OS type         */
#define RB_OS_DOS                 1          /* DOS operating system       */
#define RB_OS_RSRV1               2          /* reserved                   */
#define RB_OS_RSRV2               3          /* reserved                   */
#define RB_OS_WIN3x               4          /* Windows 3.x operating env  */
#define RB_OS_WINNT               5          /* Windows NT operating system*/
#define RB_OS_OS2                 6          /* OS/2 operating system      */
#define RB_OS_WIN95               7          /* Windows 95 operating system*/
#define RB_OS_WIN32s              8          /* Windows WIN32s env         */
#define RB_MAX_OS_TYPE            8

/* Driver types */
#define RB_DOSRM_LOCAL_DRVR       1          /* DOS Real Mode local driver */
#define RB_WIN3x_LOCAL_DRVR       2          /* Windows 3.x local driver   */
#define RB_WIN32s_LOCAL_DRVR      3          /* Win32s local driver        */
#define RB_WIN3x_SYS_DRVR         4          /* Windows 3.x system driver  */
#define RB_WINNT_SYS_DRVR         5          /* Windows NT system driver   */
#define RB_OS2_SYS_DRVR           6          /* OS/2 system driver         */
#define RB_WIN95_SYS_DRVR         7          /* Windows 95 system driver   */

/* Router Flags */
#define RB_ROUTER_USE_LOCAL_DRVR  0x0001     /* use linked in driver       */
#define RB_ROUTER_USE_SYS_DRVR    0x0002     /* use system driver          */
#define RB_ROUTER_AUTODETECT_DRVR (RB_ROUTER_USE_LOCAL_DRVR | \
                                   RB_ROUTER_USE_SYS_DRVR)      
#define RB_MAX_ROUTER_FLAGS       (RB_ROUTER_USE_LOCAL_DRVR | \
                                   RB_ROUTER_USE_SYS_DRVR)

/* Port Params flags */
#define RB_FIRST_LOG_PORT         0          /* first logical port         */
#define RB_LAST_LOG_PORT          3          /* last logical port          */
#define RB_VALIDATE_PORT          0x00000001 /* I/O validate port exsitence*/
#define RB_CONT_HNDLR_INSTALLED   0x00000002 /* OUT    system contention   */
#define RB_USER_DEFINED_PORT      0x00000004 /* OUT    user defined port   */
#define RB_RSRV_PORT_FLAGS        0xFFFFFFF8 /* reserved                   */
#define RB_DEFAULT_PORT_FLAGS     (RB_VALIDATE_PORT)
#define RB_USE_AUTOTIMING         0

/* Port types */
#define RB_MIN_PORT_TYPE          0
#define RB_AUTODETECT_PORT_TYPE   0          /* IN   autodetect port type  */
#define RB_NEC_PORT_TYPE          1          /* I/O NEC-PC9800 series port */
#define RB_FMR_PORT_TYPE          2          /* I/O Fujitus FMR series port*/
#define RB_PS2_PORT_TYPE          3          /* I/O IBM/AT/PS2 series port */
#define RB_PS2_DMA_PORT_TYPE      4          /* I/O IBM PS2 DMA series port*/
#define RB_MAX_PORT_TYPE          4

/* Contention Methods (bit mask) */
#define RB_CONT_METH_SYS_ALLOC    0x00000001 /* I/O System port allocation */     
#define RB_CONT_METH_NT_RIRQL     0x00000002 /* OUT    NT Raise IRQ level  */    
#define RB_CONT_METH_SYS_INT      0x00000004 /* I/O Disable System Ints    */ 
#define RB_CONT_METH_MASK_INT     0x00000008 /* I/O Mask ints at PIC       */     
#define RB_CONT_METH_WIN_CS       0x00000010 /* I/O Windows Critical Sect  */   
#define RB_CONT_METH_POLL_HW      0x00000020 /* I/O H/W polling of port    */
#define RB_CONT_METH_RBW          0x00000040 /* I/O Read Before Write      */
#define RB_CONT_METH_DRVR_DEFINED 0x80000000 /* Contention defined by drvr.*/

/* Interrupts to mask (bit mask) */
#define RB_IRQ_MASK_LPT1      0x0001         /* mask LPT1  interrupt       */
#define RB_IRQ_MASK_LPT2      0x0002         /* mask LPT2  interrupt       */
#define RB_IRQ_MASK_TIMER     0x0004         /* mask TIMER interrupt       */
#define RB_IRQ_MAX_MASK       (RB_IRQ_MASK_LPT1 | \
                               RB_IRQ_MASK_LPT2 | \
                               RB_IRQ_MASK_TIMER)
#define RB_IRQ_MASK_DEF       (RB_IRQ_MASK_LPT1 | \
                               RB_IRQ_MASK_TIMER)

/* Define default retry counts and intervals */
#define RB_PORT_CONT_RETRY_CNT_DEF 100       /* 100 retries for port cont  */
#define RB_PORT_CONT_RETRY_INT_DEF 300       /* 300 ms retry interval      */
#define RB_DEV_RETRY_CNT_DEF       100       /* 100 retries for device     */

/* Define the cmd field values for RB_SPRO_LIB_PARAMS  */
#define RB_SET_LIB_PARAMS_CMD      0x0001    /* Set library parameters     */
#define RB_GET_LIB_PARAMS_CMD      0x0002    /* Get library parameters     */

/* define the func field values for RB_SPRO_LIB_PARAMS */
#define RB_MACHINE_TYPE_FUNC       0x0001    /* Set/Get Machine type       */
#define RB_DELAY_FUNC              0x0002    /* Set/Get Delay value        */
#define RB_MASK_INTS_FUNC          0x0003    /* Set/Get Mask interrupts    */
#define RB_ROUTER_FLAGS_FUNC       0x0004    /* Set/Get Router flags       */
#define RB_OS_PARAMS_FUNC          0x0005    /* Set/Get O/S parameters     */
#define RB_PORT_PARAMS_FUNC        0x0006    /* Set/Get Port Parameters    */

typedef struct _RB_SP_OS_PARAMS {
SP_IO  RB_WORD osType;                       /* type of Operating System   */
SP_OUT RB_WORD osVer;                        /* version of Operating System*/
} RB_SP_OS_PARAMS;
typedef RB_SP_OS_PARAMS SP_PTR RBP_SP_OS_PARAMS;

typedef struct _RB_SP_PORT_PARAMS {
SP_IO  RB_WORD  logPortNum;                  /* logical port number         */
SP_IO  RB_WORD  sysPortNum;                  /* system  port number         */
SP_IO  RB_WORD  portType;                    /* port type                   */
SP_IO  RB_WORD  phyAddr;                     /* physcial address            */
SP_OUT RB_WORD  mappedAddr;                  /* map address                 */
SP_IO  RB_WORD  deviceRetryCnt;              /* device retry count          */
SP_IO  RB_WORD  contentionRetryCnt;          /* port contention retry count */
SP_IO  RB_WORD  padding1;
SP_IO  RB_DWORD contentionMethod;            /* port contention method      */
SP_IO  RB_DWORD contentionRetryInterval;     /* port contention retry int   */
SP_IO  RB_DWORD flags1;                      /* port flags                  */
} RB_SP_PORT_PARAMS;
typedef RB_SP_PORT_PARAMS SP_PTR RBP_SP_PORT_PARAMS;

typedef union  _RB_SP_CFG_PARAMS {
SP_IO RB_WORD        machineType;      /* machine type: IBM, NEC, or FMR   */
SP_IO RB_WORD        delay;            /* number of loops for 2us delay    */
SP_IO RB_WORD        maskInterrupts;   /* interrupts to mask               */
SP_IO RB_WORD        routerFlags;      /* request routing flags            */
SP_IO RB_SP_OS_PARAMS   osParams;      /* OS parameters                    */
SP_IO RB_SP_PORT_PARAMS portParams;    /* port parameters                  */
} RB_SP_CFG_PARAMS;
typedef RB_SP_CFG_PARAMS SP_PTR RBP_SP_CFG_PARAMS;

typedef struct _RB_SPRO_LIB_PARAMS {
SP_IN RB_WORD   cmd;                   /* command - set/get parameters     */
SP_IN RB_WORD   func;                  /* function to set/get              */
SP_IO RB_SP_CFG_PARAMS params;
} RB_SPRO_LIB_PARAMS;
typedef RB_SPRO_LIB_PARAMS SP_PTR RBP_SPRO_LIB_PARAMS;

/* Define the extern routines */
SP_EXPORT
SP_STATUS SP_API RNBOsproCfgLibParams( SP_IO RBP_SPRO_APIPACKET  thePacket,
                                       SP_IO RBP_SPRO_LIB_PARAMS params );
SP_EXPORT
SP_STATUS SP_API RNBOsproFormatPacket( SP_OUT RBP_SPRO_APIPACKET thePacket,
                                       SP_IN  RB_WORD            thePacketSize );
SP_EXPORT
SP_STATUS SP_API RNBOsproInitialize( SP_OUT RBP_SPRO_APIPACKET packet );

SP_EXPORT
SP_STATUS SP_API RNBOsproFindFirstUnit( SP_IN RBP_SPRO_APIPACKET packet,
                                        SP_IN RB_WORD            devleoperID );
SP_EXPORT
SP_STATUS SP_API RNBOsproFindNextUnit( SP_IN RBP_SPRO_APIPACKET packet );

SP_EXPORT
SP_STATUS SP_API RNBOsproRead( SP_IN  RBP_SPRO_APIPACKET packet,
                               SP_IN  RB_WORD            address,
                               SP_OUT RBP_WORD           data );
SP_EXPORT
SP_STATUS SP_API RNBOsproExtendedRead( SP_IN  RBP_SPRO_APIPACKET packet,
                                       SP_IN  RB_WORD            address,
                                       SP_OUT RBP_WORD           data,
                                       SP_OUT RBP_BYTE           accessCode );
SP_EXPORT
SP_STATUS SP_API RNBOsproWrite( SP_IN RBP_SPRO_APIPACKET packet,
                                SP_IN RB_WORD            writePassword,
                                SP_IN RB_WORD            address,
                                SP_IN RB_WORD            data,
                                SP_IN RB_BYTE            accessCode );
SP_EXPORT
SP_STATUS SP_API RNBOsproOverwrite( SP_IN RBP_SPRO_APIPACKET packet,
                                    SP_IN RB_WORD            writePassword,
                                    SP_IN RB_WORD            overwritePassword1,
                                    SP_IN RB_WORD            overwritePassword2,
                                    SP_IN RB_WORD            address,
                                    SP_IN RB_WORD            data,
                                    SP_IN RB_BYTE            accessCode );
SP_EXPORT
SP_STATUS SP_API RNBOsproDecrement( SP_IN RBP_SPRO_APIPACKET packet,
                                    SP_IN RB_WORD            writePassword,
                                    SP_IN RB_WORD            address );
SP_EXPORT
SP_STATUS SP_API RNBOsproActivate( SP_IN RBP_SPRO_APIPACKET packet,
                                   SP_IN RB_WORD            writePassword,
                                   SP_IN RB_WORD            activatePassword1,
                                   SP_IN RB_WORD            activatePassword2,
                                   SP_IN RB_WORD            address );
SP_EXPORT
SP_STATUS SP_API RNBOsproQuery( SP_IN  RBP_SPRO_APIPACKET packet,
                                SP_IN  RB_WORD            address,
                                SP_IN  RBP_VOID           queryData,
                                SP_OUT RBP_VOID           response,
                                SP_OUT RBP_DWORD          response32,
                                SP_IN  RB_WORD            length );
SP_EXPORT
RB_WORD SP_API RNBOsproGetFullStatus( SP_IN RBP_SPRO_APIPACKET thePacket );

SP_EXPORT
SP_STATUS SP_API RNBOsproGetVersion( SP_IN  RBP_SPRO_APIPACKET thePacket,
                                     SP_OUT RBP_BYTE           majVer,
                                     SP_OUT RBP_BYTE           minVer,
                                     SP_OUT RBP_BYTE           rev,
                                     SP_OUT RBP_BYTE           osDrvrType );

#endif                                       /* _SPROMEPS_H                */
/* end of file */

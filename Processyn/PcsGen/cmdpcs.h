/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il demeure sa propriete exclusive et est confidentiel.             |
 |   Aucune diffusion n'est possible sans accord ecrit.                 |
 |----------------------------------------------------------------------|
 |   Auteur  : LM                                                       |
 |   Date    : 11/08/94                                                 |
 +----------------------------------------------------------------------*/
// CmdPcs.h gestion nom application et fichier de commande
// Version Win32

// -------------------------- Constantes
#define CAR_SEPARATEUR_LIGNE_CDE    '/'
#define CAR_SEPARATEUR_NOM          '='
#define CARACTERE_ENCHAINE_A_CHAUD  '2'
#define CARACTERE_COMMANDE_PCS      'P'

#define NB_CAR_MAX_TITRE            255
#define NB_CHAMPS_MAX                80

#define BLOC_COMMANDES 851

// Structures
//
typedef struct
  {
  char carCmd;
  char pszChaine[MAX_PATH];
  } COMMANDE_PCS, *PCOMMANDE_PCS;

// --------------------- Fonctions Exportees
//
BOOL LitFichierCmdPcs (PSTR pszLigneCmdLue);
void  MajFichierCmdPcs (PCSTR pszLigneCmd);
BOOL  FabriqueBlocCmd (PCSTR LigneDeCommande);

void CopieParametresLigneCommandePcs (PSTR pszDest);

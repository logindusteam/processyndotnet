//-------------------------------------------------------------
//|   Ce fichier est la propriete de                           
//|              Societe LOGIQUE INDUSTRIE                     
//|       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3        
//|   Il est demeure sa propriete exclusive et est confidentiel.
//|   Aucune diffusion n'est possible sans accord ecrit        
//|------------------------------------------------------------
//|                                                            
//|   Titre   : PCSGEN.C 						|
//|   Auteur  : LM							|
//|   Date    : 24/11/92 						|
//|   Version :Win32								|
//|   G�n�ration d'un ex�cutable processyn         
//|                                                            
//-------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "DocMan.h"
#include "VersMan.h"
#include "UStr.h"
#include "inpugn.h"
#include "LireLng.h"
#include "actiongn.h"

#include "threads.h"
#include "mem.h"
#include "FileMan.h"
#include "USem.h"
#include "UChrono.h"
#include "dongle.h"
#include "trap.h"
#include "UExcept.h"
#include "WGen.h"
#include "Verif.h"

VerifInit;

//------------------------------------------------------------------------
void LancerThreadWGen (void)
  {
	#define STACK_SIZE_GEN 8192
  DWORD wError;

	// $$ Erreurs ?
	Verif (hsemCreationWGen = hSemCree (TRUE));
  bThreadCree (&hThrdPM, uThreadWGen, STACK_SIZE_GEN);
  dwThreadExecuteMessage (hThrdPM, THRD_FCT_INIT, NULL, NULL, &wError);
  ThreadEnvoiMessage (hThrdPM, THRD_FCT_EXEC, NULL, NULL, &wError);
	dwSemAttenteLibre (hsemCreationWGen, INFINITE);
  }

//------------------------------------------------------------------------
void ArreterThreadWGen (void)
  {
  bThreadAttenteFinTraiteMessage (hThrdPM, INFINITE);
  bThreadFerme (&hThrdPM, INFINITE);
  }

// --------------- ouverture des ressources materielles ------------------
//                 horloge, ecran, clavier, souris, trap disque, vdi
int ouvrir_ressources (void)
  {
  int erreur = 0;

	creer_bd_vierge ();

  erreur = 0;
  bQuitterGen = FALSE;
  lance_trap ();
  ouvrir_dongle (NULL);
  return (erreur);
  }

// --------------- fermeture des ressources materielles -------------------
void fermer_ressources (void)
  {
  fermer_dongle ();
	fermer_bd ();
  arrete_trap ();
  }

// -------------------------------------------------------------------------
//                    Point d'entr�e G�n�ration Windows
// -------------------------------------------------------------------------

int WINAPI WinMain
	(
	HINSTANCE hInstance, // handle de cette instance de l'application
	HINSTANCE hPrevInstance,	// 0 sur Win32 actuel
	LPSTR lpCmdLine,	// param�tres de la ligne de commande
	int nCmdShow 	// show state of window
	)
  {
  t_param_lecture param_lecture_editeur;

	__try
		{
		param_lecture_editeur.mode_lecture = mode_demarrage;
		StrSetNull (param_lecture_editeur.ligne_commande);
		if (lpCmdLine)
			StrCopy(param_lecture_editeur.ligne_commande, lpCmdLine);

		// initialisation de l'application
		CHAR szVersionComplete [MAX_PATH];
		Appli.Init (hInstance, "Processyn g�n�ration", GetVersionComplete(szVersionComplete));
		bOuvrirLng();
		InitPathNameDoc ();

		ouvrir_ressources();
		creer_action ();
		lire_commandes (&param_lecture_editeur);
		
		// lancement de la t�che interface utilisateur
		LancerThreadWGen ();

		// traitement des actions demand�es par l'utilisateur
		while (executer_action () != action_quitte);

		// arr�t de la t�che interface utilisateur
		ArreterThreadWGen ();

		fermer_ressources ();
		}	
	__except (nSignaleExceptionUser(GetExceptionInformation()))
		{
		ExitProcess((UINT)-1);// handler des exceptions choisies par le filtre
		}
	return 0;// $$ ???
  }
// -----------------------------------------------------------------------


// interface unite declaration des types de structure repetitive


#define b_geo_re 152
#define b_titre_paragraf_re 153
#define b_spec_titre_paragraf_re 154
#define b_liste 155
#define bx_liste 156
#define b_temp_nom_structure 157
#define b_temp_genc_genx 158

#define Taille_Bloc_Titre_Paragraf 22

typedef struct
  {
  DWORD numero_repere;
  union
    {
    DWORD pos_donnees;
    struct
      {
      DWORD pos_paragraf;
      DWORD pos_specif_paragraf;
      };
    struct
      {
      DWORD pos_es;
      DWORD pos_initial;
      };
    };
  } GEO_REPETE, *PGEO_REPETE;


typedef struct
  {
  DWORD numero_repere;
  DWORD  position;
  }  t_liste_re;

typedef struct
  {
  char nom[22];
  DWORD nb_va_gen;
  DWORD taille_liste;
  } NOM_TEMP_REPETE, *PNOM_TEMP_REPETE;

typedef struct
  {
  DWORD pnt_b_temp_nom_structure;
  DWORD pos_es;
  DWORD pos_bx_liste;
  } TEMP_GENC_GENX, *PTEMP_GENC_GENX; // $$ nom clair ?

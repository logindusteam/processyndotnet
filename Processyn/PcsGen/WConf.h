// -----------------------------------------------------------------------
// WConf.h
// Gestion du bureau de PcsConf
//
// Ces fonctions constituent un tronc commun entre la configuration
// en mode CARACTERE et la configuration en mode PM
//
// -----------------------------------------------------------------------



// -----------------------------------------------------------------------
// etat d'affichage:
// ------------ permet de gerer des couleurs differentes suivant les etats

#define etat_0	0
#define etat_1	1
#define etat_2	2
#define etat_3	3

BOOL bCreeFenetreConf (void);
BOOL bCreerSousFenetresBureau (void);

void effacer_fenetre (DWORD repere);

void ecrire_ligne_fenetre (DWORD repere, char *texte, DWORD etat_ligne);
DWORD nombre_ligne_fenetre (DWORD repere);
void consulter_ligne_fenetre (DWORD repere, char *texte);

void scroller_fenetre_ligne (DWORD repere, int decalage);
void scroller_fenetre_colonne (DWORD repere, int decalage);
void aller_debut_ligne_fenetre (DWORD repere);
void aller_fin_ligne_fenetre (DWORD repere);
void monter_curseur_fenetre (DWORD repere, DWORD *decalage);
void descendre_curseur_fenetre (DWORD repere, DWORD &decalage);
void droiter_curseur_fenetre (DWORD repere, DWORD decalage);
void gaucher_curseur_fenetre (DWORD repere, DWORD decalage);
void aller_haut_fenetre (DWORD repere);
void aller_bas_fenetre (DWORD repere);

void inserer_ligne_fenetre (DWORD repere);
void inserer_car_ligne_fenetre (DWORD repere, char caractere);
void modifier_car_ligne_fenetre (DWORD repere, char caractere);
void supprimer_car_ligne_fenetre (DWORD repere);

void reajuster_ascenseur_fenetre (DWORD repere, DWORD pos_relative_ligne, DWORD taille_relative);
void position_curseur_fenetre (DWORD repere, DWORD *ligne, DWORD *colonne);

void memoriser_curseur_fenetre (DWORD repere);
void restaurer_curseur_fenetre (DWORD repere);

void droiter_mot_curseur_fenetre (DWORD repere);
void gaucher_mot_curseur_fenetre (DWORD repere);

void sauver_marque_fenetre (DWORD repere);
void restaurer_marque_fenetre (DWORD repere);

void afficher_select_ligne_fenetre (DWORD repere, BOOL mode_selection);
void abandon_selection_fenetre (DWORD repere);

void curseur_invisible_fenetre (DWORD repere);
void curseur_visible_fenetre (DWORD repere);

void patienter (BOOL patience);

void dialoguer_operateur (DWORD repere, DWORD erreur_bd, DWORD *retour_operateur);

//------------------------------------------------------------------------
// partie sp�cifique gestion du bureau
// --------------------------- Types de fenetres -------------------------
#define NO_FENETRE	   0
#define FENETRE_WND_PAGE   1
#define FENETRE_MNU_BMP    2
#define FENETRE_LN_TXT     3
#define FENETRE_TITRE      4

// --------------------- Zones des sous fen�tres ------------------------
#define  MILIEU  0
#define  HAUT    1
#define  BAS     2
#define  GAUCHE  3
#define  DROITE  4

// -----------------------------------------------------------------------
int lire_taille_menu (void);

// -----------------------------------------------------------------------
// d�finir la taille d'une zone �cran
// -----------------------------------------------------------------------
void FixerTailleZone (DWORD zone_a_fixer, DWORD taille_a_donner);

// -----------------------------------------------------------------------
// d�finir l'ordre de cr�ation des zones �crans et leurs tailles
// Exemple..: +----------------------------------------------+
//            |              Zone HAUT  ordre 1              |
//            +-----------+---------------------+------------+
//            |Zone       |                     |Zone        |
//            |           |                     |            |
//            | GAUCHE    |      Zone           | DROITE     |
//            |           |                     |            |
//            |  ordre    |     MILIEU          |  ordre     |
//            |           |                     |            |
//            |   2       |                     |     4      |
//            |           |                     |            |
//            |           +---------------------+------------+
//            |           | Zone  BAS  ordre 3               |
//            |           |                                  |
//            +-----------+----------------------------------+
//
// -----------------------------------------------------------------------
void DefinirZones 
	(DWORD zone1, DWORD taille1,
	DWORD zone2, DWORD taille2,
	DWORD zone3, DWORD taille3,
	DWORD zone4, DWORD taille4
	);

void creer_qhelp_fen(void);
void PageOptionPolice(DWORD repere);

void cacher_plan_de_travail (void);
void montrer_plan_de_travail (void);
void lancer_satellite_pm (BOOL bExecutionSynchrone, char *nom,
			  PSTR param, PSTR env, PSTR chemin);
// -----------------------------------------------------------------------



/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : ctxtgr.h                                               |
 |   Auteur  : AC					        	|
 |   Date    : 20/11/92					        	|
 |   Version : 3.20							|
 |                                                                      |
 +----------------------------------------------------------------------*/

#define EN_SELECTION             0
#define EN_TRACE                 1
#define EN_TEST                  3

#define TRACE_PLEIN  0
#define TRACE_VIDE   1


#define COULEUR_FOND   0
#define COULEUR_DESSIN 1

void initialiser_ctxtgr_editeur (void);

void creer_ctxtgr_fenetre (void);
void supprimer_ctxtgr_fenetre (void);

BOOL selectionner_ctxtgr_premier (void);
BOOL selectionner_ctxtgr_suivant (void);
BOOL selectionner_ctxtgr_id_fenetre (DWORD id_fenetre);

BOOL existe_ctxtgr_fenetre (void);
BOOL existe_ctxtgr_page (DWORD page, DWORD *id_fenetre);

DWORD get_ctxtgr_id_fenetre (void);
HBDGR	get_ctxtgr_hBdGr (void);
BOOL	get_ctxtgr_lancement_os (void);
DWORD CtxtgrGetModeEdition (void);
DWORD get_ctxtgr_mode_trace (void);
DWORD get_ctxtgr_mode_couleur (void);
BOOL get_ctxtgr_dessin_en_cours (void);
BOOL get_ctxtgr_selection_en_cours (void);
BOOL get_ctxtgr_deplacement_en_cours (void);
BOOL get_ctxtgr_deplacement_contour (void);
BOOL get_ctxtgr_deformation_en_cours (void);
BOOL get_ctxtgr_duplication_en_cours (void);
BOOL get_ctxtgr_charge_symb_en_cours (void);
DWORD get_ctxtgr_page_origine_dup (void);
G_ACTION CtxtgrGetAction (void);
DWORD get_ctxtgr_anim_rl (void);
DWORD get_ctxtgr_anim_indice_couleur2 (void);
DWORD get_ctxtgr_anim_indice_couleur4 (void);
DWORD get_ctxtgr_taille_texte (void);
DWORD get_ctxtgr_n_elt_texte (void);
DWORD get_ctxtgr_n_id_texte (void);
G_COULEUR get_ctxtgr_couleur_trace (void);
G_COULEUR get_ctxtgr_couleur_fond (void);
G_STYLE_LIGNE get_ctxtgr_style_ligne (void);
G_STYLE_REMPLISSAGE get_ctxtgr_style_remplissage (void);
DWORD get_ctxtgr_police (void);
DWORD get_ctxtgr_style_police (void);
DWORD get_ctxtgr_type_curseur (void);
void  get_ctxtgr_pos_debut_selection (LONG *x, LONG *y);
void  get_ctxtgr_pos_debut_translat (LONG *x, LONG *y);
void  get_ctxtgr_boite_init_deform (LONG *x1, LONG *y1, LONG *x2, LONG *y2);
BOOL get_ctxtgr_draw (void);
char    *get_ctxtgr_draw_texte (void);
char    *get_ctxtgr_nom_presse_papier (void);
char    *get_ctxtgr_nom_symbole (void);

void set_ctxtgr_id_fenetre (DWORD id_fenetre);
void set_ctxtgr_hbdgr (HBDGR hbdgr);
void set_ctxtgr_lancement_os (BOOL lancement_os);
void CtxtgrSetModeEdition (DWORD mode);
void set_ctxtgr_mode_trace (DWORD mode);
void set_ctxtgr_mode_couleur (DWORD mode);
void set_ctxtgr_dessin_en_cours (BOOL mode);
void set_ctxtgr_selection_en_cours (BOOL mode);
void set_ctxtgr_deplacement_en_cours (BOOL mode);
void set_ctxtgr_deplacement_contour (BOOL mode);
void set_ctxtgr_deformation_en_cours (BOOL mode);
void set_ctxtgr_duplication_en_cours (BOOL mode);
void set_ctxtgr_charge_symb_en_cours (BOOL mode);
void CtxtgrSetAction (G_ACTION trace);
void set_ctxtgr_anim_rl (DWORD anim_rl);
void set_ctxtgr_anim_indice_couleur2 (DWORD indice_couleur);
void set_ctxtgr_anim_indice_couleur4 (DWORD indice_couleur);
void set_ctxtgr_page (DWORD ctxt_page);
void set_ctxtgr_page_origine_dup (DWORD page);
void set_ctxtgr_taille_texte (DWORD ctxt_taille_texte);
void set_ctxtgr_n_elt_texte (DWORD ctxt_n_elt_texte);
void set_ctxtgr_n_id_texte (DWORD ctxt_n_id_texte);
void set_ctxtgr_couleur_trace (G_COULEUR couleur_trace);
void set_ctxtgr_couleur_fond (G_COULEUR couleur_fond);
void set_ctxtgr_style_ligne (G_STYLE_LIGNE style_ligne);
void set_ctxtgr_style_remplissage (G_STYLE_REMPLISSAGE style_remplissage);
void set_ctxtgr_police (DWORD IdPolice);
void set_ctxtgr_style_police (DWORD style_police);
void set_ctxtgr_type_curseur (DWORD type_curseur);
void set_ctxtgr_pos_debut_selection (LONG x, LONG y);
void set_ctxtgr_pos_debut_translat (LONG x, LONG y);
void set_ctxtgr_boite_init_deform (LONG x1, LONG y1, LONG x2, LONG y2);
void set_ctxtgr_draw (BOOL draw);
void set_ctxtgr_draw_texte (char *texte);
void set_ctxtgr_nom_presse_papier (char *texte);
void set_ctxtgr_nom_symbole (char *texte);

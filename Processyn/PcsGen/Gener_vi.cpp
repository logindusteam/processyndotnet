/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_VI.C                                               |
 |   Auteur  : AC                                                       |
 |   Date    : 13/08/94                                                 |
 |   Version :								|
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include <stddef.h>

#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "G_Objets.h"
#include "G_sys.h"
#include "BdGr.h"
#include "BdAnmGr.h"
#include "UChrono.h"
#include "tipe_gr.h"
#include "findbdex.h"
#include "mem.h"
#include "gener_gr.h"
#include "Pcs_sys.h"        // touches de fonctions
#include "GenereBD.h"
#include "Verif.h"

#include "gener_vi.h"

VerifInit;

//----------------------------------------------------------------------------
// fonctions locales
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
static DWORD genere_e_log (DWORD numero_page, DWORD *n_elt, DWORD pos_es, DWORD ptr_sur_b_vdi_e_log)
  {
  DWORD wErreur;
  DWORD pos_ret;
  DWORD pos_es_x;
  HEADER_SERVICE_ANM_GR handle_r_e_log;
  PVDI_E_LOG donnees_vdi_e_log;
	DWORD ptr_sur_variable;
	DWORD ptr_sur_element;
	tx_fenetre_vi*donnees_bx_fenetre;
	tx_element_vi*donnees_bx_element;
	tx_variable_vi*donnees_bx_variable;

  wErreur = 0;

	// Retour des entrees par l'animateur
  if (pos_es != 0)
    {
    wErreur = recherche_index_execution (pos_es, &pos_es_x);
    pos_ret = insere_variable_entree (b_cour_log, pos_es_x);
    }
  donnees_vdi_e_log = (PVDI_E_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_log_nv1, ptr_sur_b_vdi_e_log);
  handle_r_e_log = anim_e_log ((*n_elt), donnees_vdi_e_log->selecteur, pos_ret, numero_page);

	donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
	ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
	donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

	insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
	donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

	ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
	insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
	donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);
	donnees_bx_element->prem_variable = ptr_sur_variable;
	donnees_bx_element->nbr_variable = 1;

	donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
	donnees_bx_variable->bloc_genre = b_cour_log;
	wErreur = recherche_index_execution (pos_es, &(donnees_bx_variable->pos_es));
	if (wErreur == 0)
		{
		donnees_bx_variable->pos_es = donnees_bx_variable->pos_es;
		donnees_bx_element->HeaderServiceAnmGr = handle_r_e_log;
		}
	if (donnees_vdi_e_log->selecteur != EL_TRANSPARENTE)
		{
		(*n_elt)++;
		}
	// else le anim_e_log supprime l'elt pour l'exploitation => pas d'increment de n_elt

  return (wErreur);
  }

//----------------------------------------------------------------------------
static DWORD genere_e_num (DWORD numero_page, DWORD n_elt, DWORD pos_es, DWORD ptr_sur_b_vdi_e_num)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  DWORD pos_ret;
  DWORD pos_es_x;
  PVDI_E_NUM donnees_vdi_e_num;
  HEADER_SERVICE_ANM_GR handle_r_e_num;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_e_num = (PVDI_E_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_num, ptr_sur_b_vdi_e_num);
  if (pos_es != 0)
    {
    wErreur = recherche_index_execution (pos_es, &pos_es_x);
    pos_es_x = pos_es_x - ContexteGen.nbr_logique;
    pos_ret = insere_variable_entree (b_cour_num, pos_es_x);
    }
  handle_r_e_num = anim_e_num (n_elt,
                               donnees_vdi_e_num->couleur,
                               donnees_vdi_e_num->couleur_fond,
                               donnees_vdi_e_num->nbr_caractere,
                               donnees_vdi_e_num->cadre,
                               donnees_vdi_e_num->masque,
                               donnees_vdi_e_num->video_focus,
                               donnees_vdi_e_num->min,
                               donnees_vdi_e_num->max,
                               donnees_vdi_e_num->NbDecimales,
                               pos_ret,
                               numero_page);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->HeaderServiceAnmGr = handle_r_e_num;
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable  = 1;
  donnees_bx_variable->bloc_genre = b_cour_num;
  donnees_bx_variable->pos_es = pos_es_x;

  return (wErreur);
  }

//----------------------------------------------------------------------------
static DWORD genere_e_control_radio (DWORD numero_page, DWORD n_elt, DWORD pos_donnees)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  DWORD pos_ret;
	DWORD pos_es;
  DWORD pos_es_x;
	BOOL bPremierBouton;
  PVDI_CONTROL_RADIO donnees_vdi_control_radio;
  HEADER_SERVICE_ANM_GR handle_r_e_radio;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;

  donnees_vdi_control_radio = (PVDI_CONTROL_RADIO)pointe_enr (szVERIFSource, __LINE__, b_vdi_control_radio, pos_donnees);
	pos_es = donnees_vdi_control_radio->position_es;
	// Gestion des entr�es
  if (pos_es != 0)
    {
    wErreur = recherche_index_execution (pos_es, &pos_es_x);
    pos_es_x = pos_es_x - ContexteGen.nbr_logique;
    pos_ret = insere_variable_entree (b_cour_num, pos_es_x);
    }
  handle_r_e_radio = anim_e_radio (n_elt, pos_ret, numero_page, &bPremierBouton);

	// Gestion mise a jour par forcage PCSEXE uniquement sur le premier bouton
	if ((wErreur == 0) && (bPremierBouton))
		{
		donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
		ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
		donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;
		insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
		donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

		ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
		insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
		donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

		donnees_bx_element->HeaderServiceAnmGr = handle_r_e_radio;
		donnees_bx_element->prem_variable = ptr_sur_variable;
		donnees_bx_element->nbr_variable  = 1;
		donnees_bx_variable->bloc_genre = b_cour_num;
		donnees_bx_variable->pos_es = pos_es_x;
		}

  return (wErreur);
  }


//----------------------------------------------------------------------------
static DWORD genere_e_mes (DWORD numero_page, DWORD n_elt, DWORD pos_es, DWORD ptr_sur_b_vdi_e_mes)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  DWORD pos_ret;
  DWORD pos_es_x;
  PVDI_E_MES donnees_vdi_e_mes;
  HEADER_SERVICE_ANM_GR handle_r_e_mes;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_e_mes = (PVDI_E_MES)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_mes, ptr_sur_b_vdi_e_mes);

  if (pos_es != 0)
    {
    wErreur = recherche_index_execution (pos_es, &pos_es_x);
    pos_es_x = pos_es_x - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
    pos_ret = insere_variable_entree (b_cour_mes, pos_es_x);
    }

  handle_r_e_mes = anim_e_mes (n_elt,
                               donnees_vdi_e_mes->couleur,
                               donnees_vdi_e_mes->couleur_fond,
                               donnees_vdi_e_mes->nbr_caractere,
                               donnees_vdi_e_mes->cadre,
                               donnees_vdi_e_mes->masque,
                               donnees_vdi_e_mes->video_focus,
                               pos_ret,
                               numero_page);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->HeaderServiceAnmGr = handle_r_e_mes;
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable  = 1;
  donnees_bx_variable->bloc_genre = b_cour_mes;
  donnees_bx_variable->pos_es = pos_es_x;

  return (wErreur);
  }

//----------------------------------------------------------------------------
static DWORD genere_r_log (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_log)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_LOG	donnees_vdi_r_log;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_log = (PVDI_R_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log, ptr_sur_b_vdi_r_log);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi)+1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->HeaderServiceAnmGr = anim_r_log (n_elt,
                                           donnees_vdi_r_log->color_m_n,
                                           donnees_vdi_r_log->color_a_n,
                                           donnees_vdi_r_log->couleur_fond,
                                           donnees_vdi_r_log->tipe_anim);
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable = 1;

  donnees_bx_variable->bloc_genre = b_cour_log;
  if (donnees_vdi_r_log->position_es_normal != 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_r_log->position_es_normal,
                                         &(donnees_bx_variable->pos_es));
    }
  return (wErreur);
  }

//----------------------------------------------------------------------------
static DWORD genere_r_log_tab (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_log)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_LOG_TAB donnees_vdi_r_log;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_log = (PVDI_R_LOG_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_tab, ptr_sur_b_vdi_r_log);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi)+1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->HeaderServiceAnmGr = anim_r_log (n_elt,
                                           donnees_vdi_r_log->color_m_n,
                                           donnees_vdi_r_log->color_a_n,
                                           donnees_vdi_r_log->couleur_fond,
                                           donnees_vdi_r_log->tipe_anim);
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable = 1;

  donnees_bx_variable->bloc_genre = b_cour_log;
  if (donnees_vdi_r_log->position_es_normal != 0)
    {
    // position execution directe car logique
    wErreur = recherche_index_tableau_i (donnees_vdi_r_log->position_es_normal,
                                         donnees_vdi_r_log->indice,
                                         &(donnees_bx_variable->pos_es));
    }
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_log_4etats (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_log)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_LOG_4_ETATS donnees_vdi_r_log;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_log = (PVDI_R_LOG_4_ETATS)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats, ptr_sur_b_vdi_r_log);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  donnees_bx_element->HeaderServiceAnmGr = anim_r_log_4etats (n_elt,
                                           donnees_vdi_r_log->color_h_h,
                                           donnees_vdi_r_log->color_h_b,
                                           donnees_vdi_r_log->color_b_h,
                                           donnees_vdi_r_log->color_b_b,
                                           donnees_vdi_r_log->couleur_fond,
                                           donnees_vdi_r_log->tipe_anim);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi)+1;
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable = 2;

  insere_enr (szVERIFSource, __LINE__, 2, bx_variable_vi, ptr_sur_variable); // 2 valeurs
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable->bloc_genre = b_cour_log;
  if (donnees_vdi_r_log->position_es_un != 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_r_log->position_es_un,
                                         &(donnees_bx_variable->pos_es));
    }

  if ((wErreur == 0))
    {
    donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable+1);
    donnees_bx_variable->bloc_genre = b_cour_log;
    if (donnees_vdi_r_log->position_es_deux != 0)
      {
      wErreur = recherche_index_execution (donnees_vdi_r_log->position_es_deux,
                                           &(donnees_bx_variable->pos_es));
      }
    }

  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_log_4etats_tab (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_log)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_LOG_4_ETATS_TAB donnees_vdi_r_log;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_log = (PVDI_R_LOG_4_ETATS_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats_tab, ptr_sur_b_vdi_r_log);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  donnees_bx_element->HeaderServiceAnmGr = anim_r_log_4etats (n_elt,
                                           donnees_vdi_r_log->color_h_h,
                                           donnees_vdi_r_log->color_h_b,
                                           donnees_vdi_r_log->color_b_h,
                                           donnees_vdi_r_log->color_b_b,
                                           donnees_vdi_r_log->couleur_fond,
                                           donnees_vdi_r_log->tipe_anim);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi)+1;
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable = 2;

  insere_enr (szVERIFSource, __LINE__, 2, bx_variable_vi, ptr_sur_variable); // 2 valeurs
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable->bloc_genre = b_cour_log;
  if (donnees_vdi_r_log->position_es_un != 0)
    {
    // position execution directe car logique
    wErreur = recherche_index_tableau_i (donnees_vdi_r_log->position_es_un,
                                         donnees_vdi_r_log->indice_un,
                                         &(donnees_bx_variable->pos_es));
    }

  if ((wErreur == 0))
    {
    donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable+1);
    donnees_bx_variable->bloc_genre = b_cour_log;
    if (donnees_vdi_r_log->position_es_deux != 0)
      {
      wErreur = recherche_index_tableau_i (donnees_vdi_r_log->position_es_deux,
                                           donnees_vdi_r_log->indice_deux,
                                           &(donnees_bx_variable->pos_es));
      }
    }
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_num (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_num)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  DWORD modele_bar;
  PVDI_R_NUM donnees_vdi_r_num;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_num = (PVDI_R_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num, ptr_sur_b_vdi_r_num);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
  switch (donnees_vdi_r_num->modele)
    {
    case c_res_up:
      modele_bar = BAR_UP;
      break;
    case c_res_down:
      modele_bar = BAR_DOWN;
      break;
    case c_res_left:
      modele_bar = BAR_LEFT;
      break;
    case c_res_right:
      modele_bar = BAR_RIGHT;
      break;
    default:
      break;
    }

  donnees_bx_element->HeaderServiceAnmGr = anim_r_num (n_elt,
                                            modele_bar,
                                            donnees_vdi_r_num->color,
                                            donnees_vdi_r_num->couleur_fond,
                                            donnees_vdi_r_num->style,
                                            donnees_vdi_r_num->max,
                                            donnees_vdi_r_num->min
                                            );
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable  = 1;

  donnees_bx_variable->bloc_genre = b_cour_num;
  if (donnees_vdi_r_num->position_es != 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_r_num->position_es,
                                         &(donnees_bx_variable->pos_es));
    donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
    }
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_num_tab (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_num)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  DWORD modele_bar;
  PVDI_R_NUM_TAB donnees_vdi_r_num;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_num = (PVDI_R_NUM_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_tab, ptr_sur_b_vdi_r_num);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
  switch (donnees_vdi_r_num->modele)
    {
    case c_res_up:
      modele_bar = BAR_UP;
      break;
    case c_res_down:
      modele_bar = BAR_DOWN;
      break;
    case c_res_left:
      modele_bar = BAR_LEFT;
      break;
    case c_res_right:
      modele_bar = BAR_RIGHT;
      break;
    default:
      break;
    }

  donnees_bx_element->HeaderServiceAnmGr = anim_r_num (n_elt,
                                            modele_bar,
                                            donnees_vdi_r_num->color,
                                            donnees_vdi_r_num->couleur_fond,
                                            donnees_vdi_r_num->style,
                                            donnees_vdi_r_num->max,
                                            donnees_vdi_r_num->min
                                            );
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable  = 1;

  donnees_bx_variable->bloc_genre = b_cour_num;
  if (donnees_vdi_r_num->position_es != 0)
    {
    wErreur = recherche_index_tableau_i (donnees_vdi_r_num->position_es,
                                         donnees_vdi_r_num->indice,
                                         &(donnees_bx_variable->pos_es));
    }
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_num_dep (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_num)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_NUM_DEP donnees_vdi_r_num;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_num = (PVDI_R_NUM_DEP)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_dep, ptr_sur_b_vdi_r_num);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->prem_variable = ptr_sur_variable;

  donnees_bx_variable->bloc_genre = b_cour_num;
  if (donnees_vdi_r_num->position_es_un != 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_r_num->position_es_un,
                                         &(donnees_bx_variable->pos_es));
    donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
    }

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_variable->bloc_genre = b_cour_num;
  if (donnees_vdi_r_num->position_es_deux != 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_r_num->position_es_deux,
                                         &(donnees_bx_variable->pos_es));
    donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
    }

  switch (donnees_vdi_r_num->color_cste)
    {
    case 0: // couleur en variable
      donnees_bx_element->nbr_variable = 3;

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_num->color != 0)
        {
        wErreur = recherche_index_execution (donnees_vdi_r_num->color,
                                             &(donnees_bx_variable->pos_es));
        donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
        }
      break;

    case 1: // couleur cste
      donnees_bx_element->nbr_variable  = 2;
      break;
    default:
      break;
    }
  donnees_bx_element->HeaderServiceAnmGr = anim_r_num_dep (n_elt,
                                               donnees_vdi_r_num->color_cste,
                                               donnees_vdi_r_num->color,
                                               donnees_vdi_r_num->couleur_fond,
                                               donnees_vdi_r_num->tipe_anim
                                               );
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_num_dep_tab (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_num)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_NUM_DEP_TAB donnees_vdi_r_num;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_num = (PVDI_R_NUM_DEP_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_dep_tab, ptr_sur_b_vdi_r_num);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->prem_variable = ptr_sur_variable;

  donnees_bx_variable->bloc_genre = b_cour_num;
  if (donnees_vdi_r_num->position_es_un != 0)
    {
    wErreur = recherche_index_tableau_i (donnees_vdi_r_num->position_es_un,
                                         donnees_vdi_r_num->indice_un,
                                         &(donnees_bx_variable->pos_es));
    }

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_variable->bloc_genre = b_cour_num;
  if (donnees_vdi_r_num->position_es_deux != 0)
    {
    wErreur = recherche_index_tableau_i (donnees_vdi_r_num->position_es_deux,
                                         donnees_vdi_r_num->indice_deux,
                                         &(donnees_bx_variable->pos_es));
    }

  switch (donnees_vdi_r_num->color_cste)
    {
    case 0: // couleur en variable
      donnees_bx_element->nbr_variable = 3;

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_num->color != 0)
        {
        wErreur = recherche_index_execution (donnees_vdi_r_num->color,
                                             &(donnees_bx_variable->pos_es));
        donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
        }
      break;

    case 1: //couleur cste
      donnees_bx_element->nbr_variable = 2;
      break;

    default:
      break;;
    }
  donnees_bx_element->HeaderServiceAnmGr = anim_r_num_dep (n_elt,
                                                donnees_vdi_r_num->color_cste,
                                                donnees_vdi_r_num->color,
                                                donnees_vdi_r_num->couleur_fond,
                                                donnees_vdi_r_num->tipe_anim
                                                );
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_mes (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_mes)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_MES donnees_vdi_r_mes;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_mes = (PVDI_R_MES)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_mes_nv2, ptr_sur_b_vdi_r_mes);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->prem_variable = ptr_sur_variable;
  switch (donnees_vdi_r_mes->color_cste)
    {
    case 1: // couleur cste
      donnees_bx_element->nbr_variable  = 1;
      break;
    case 2: // couleur en variable
      donnees_bx_element->nbr_variable = 2;
      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_mes->color != 0)
        {
        wErreur = recherche_index_execution (donnees_vdi_r_mes->color,
                                             &(donnees_bx_variable->pos_es));
        donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
        }

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
      break;
    case 3: // couleur variable tableau
      donnees_bx_element->nbr_variable = 2;
      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_mes->color != 0)
        {
        wErreur = recherche_index_tableau_i (donnees_vdi_r_mes->color,
                                             donnees_vdi_r_mes->indice_color,
                                             &(donnees_bx_variable->pos_es));
        }

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
      break;
    default:
      break;;
    }

  donnees_bx_variable->bloc_genre = b_cour_mes;
  if (donnees_vdi_r_mes->position_es != 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_r_mes->position_es,
                                         &(donnees_bx_variable->pos_es));
    donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
    }

  donnees_bx_element->HeaderServiceAnmGr = anim_r_mes (n_elt,
                                            donnees_vdi_r_mes->color_cste,
                                            donnees_vdi_r_mes->color,
                                            donnees_vdi_r_mes->couleur_fond
                                            );

  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_combobox (DWORD numero_page, DWORD n_elt, DWORD pos_donnees)
  {
  DWORD wErreur;

  DWORD ptr_sur_variable;
  DWORD ptr_sur_element;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;
  PVDI_CONTROL_COMBOBOX donnees_vdi_control_combobox;
	DWORD PosEsTabMes;
	DWORD PosEsIndice;
	DWORD PosEsSaisie;
  DWORD pos_ret_indice;
  DWORD pos_ret_saisie;
  DWORD pos_es_x;
  HEADER_SERVICE_ANM_GR handle_r_combobox;

  wErreur = 0;

  donnees_vdi_control_combobox = (PVDI_CONTROL_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, b_vdi_control_combobox, pos_donnees);
	PosEsTabMes = donnees_vdi_control_combobox->position_es_liste;
	PosEsIndice = donnees_vdi_control_combobox->position_es_indice;
	PosEsSaisie = donnees_vdi_control_combobox->position_es_saisie;

	// Gestion des entr�es
  if (PosEsIndice != 0)
    {
    wErreur = recherche_index_execution (PosEsIndice, &pos_es_x);
    pos_es_x = pos_es_x - ContexteGen.nbr_logique;
    pos_ret_indice = insere_variable_entree (b_cour_num, pos_es_x);
    }
	if (wErreur == 0)
		{
		if (PosEsSaisie != 0)
			{
			wErreur = recherche_index_execution (PosEsSaisie, &pos_es_x);
			pos_es_x = pos_es_x - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
			pos_ret_saisie = insere_variable_entree (b_cour_mes, pos_es_x);
			}
		}
	handle_r_combobox = anim_e_combobox (n_elt, pos_ret_indice, pos_ret_saisie, numero_page);

	// Gestion mise a jour par forcage PCSEXE
	if (wErreur == 0)
		{
		donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
		ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
		donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

		insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
		donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

		ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
		insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
		donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);
		donnees_bx_element->prem_variable = ptr_sur_variable;
		donnees_bx_element->nbr_variable = 2;

		donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
		donnees_bx_variable->bloc_genre = bx_vdi_dlg_combo_liste;
		wErreur = recherche_index_tableau (PosEsTabMes, &(donnees_bx_variable->pos_es));
		if (wErreur == 0)
			{
			ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
			insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
			donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
			donnees_bx_variable->bloc_genre = b_cour_num;
			wErreur = recherche_index_execution (PosEsIndice,&(donnees_bx_variable->pos_es));
			if (wErreur == 0)
				{
				donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
				donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);
				donnees_bx_element->HeaderServiceAnmGr = handle_r_combobox;
				}
			}
		}
	return (wErreur);
	}

//----------------------------------------------------------------------------
//
static DWORD genere_r_liste (DWORD numero_page, DWORD n_elt, DWORD pos_donnees)
  {
  DWORD wErreur;

  DWORD ptr_sur_variable;
  DWORD ptr_sur_element;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;
  PVDI_CONTROL_LISTE donnees_vdi_control_liste;
	DWORD PosEsTabMes;
	DWORD PosEsIndice;
  DWORD pos_ret;
  DWORD pos_es_x;
  HEADER_SERVICE_ANM_GR handle_r_liste;

  wErreur = 0;
  donnees_vdi_control_liste = (PVDI_CONTROL_LISTE)pointe_enr (szVERIFSource, __LINE__, b_vdi_control_liste, pos_donnees);
	PosEsTabMes = donnees_vdi_control_liste->position_es_liste;
	PosEsIndice = donnees_vdi_control_liste->position_es_indice;

	// Gestion des entr�es
  if (PosEsIndice != 0)
    {
    wErreur = recherche_index_execution (PosEsIndice, &pos_es_x);
    pos_es_x = pos_es_x - ContexteGen.nbr_logique;
    pos_ret = insere_variable_entree (b_cour_num, pos_es_x);
    }
  handle_r_liste = anim_e_liste (n_elt, pos_ret, numero_page);

	// Gestion mise � jour par forcage PCS
	if (wErreur == 0)
		{
		donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
		ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
		donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

		insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
		donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

		ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
		insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
		donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);
		donnees_bx_element->prem_variable = ptr_sur_variable;
		donnees_bx_element->nbr_variable = 2;

		donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
		donnees_bx_variable->bloc_genre = bx_vdi_dlg_combo_liste;
		wErreur = recherche_index_tableau (PosEsTabMes, &(donnees_bx_variable->pos_es));
		if (wErreur == 0)
			{
			ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
			insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
			donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
			donnees_bx_variable->bloc_genre = b_cour_num;
			wErreur = recherche_index_execution (PosEsIndice,&(donnees_bx_variable->pos_es));
			if (wErreur == 0)
				{
				donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
				donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);
				donnees_bx_element->HeaderServiceAnmGr = handle_r_liste;
				}
			}
		}
	return (wErreur);
	}

//----------------------------------------------------------------------------
//
static DWORD genere_r_control_static (DWORD numero_page, DWORD n_elt, DWORD PosEs)
  {
  DWORD wErreur;

  DWORD ptr_sur_variable;
  DWORD ptr_sur_element;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable = 1;

  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
	donnees_bx_variable->bloc_genre = b_cour_mes;
  wErreur = recherche_index_execution (PosEs, &(donnees_bx_variable->pos_es));
  if (wErreur == 0)
		{
	  donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
		donnees_bx_element->HeaderServiceAnmGr.Service = SERVICE_ANM_GR_ANM_FORCAGE_TEXT;
		donnees_bx_element->HeaderServiceAnmGr.Element = n_elt;
		}

	return (wErreur);
	}
//----------------------------------------------------------------------------
//
static DWORD genere_r_mes_tab (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_mes)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_MES_TAB donnees_vdi_r_mes;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_mes = (PVDI_R_MES_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_mes_tab_nv2, ptr_sur_b_vdi_r_mes);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->prem_variable = ptr_sur_variable;
  switch (donnees_vdi_r_mes->color_cste)
    {
    case 1: // couleur cste
      donnees_bx_element->nbr_variable  = 1;
      break;
    case 2: // couleur en variable
      donnees_bx_element->nbr_variable = 2;
      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_mes->color != 0)
        {
        wErreur = recherche_index_execution (donnees_vdi_r_mes->color,
                                             &(donnees_bx_variable->pos_es));
        donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
        }

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
      break;
    case 3: // couleur variable tableau
      donnees_bx_element->nbr_variable = 2;
      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_mes->color != 0)
        {
        wErreur = recherche_index_tableau_i (donnees_vdi_r_mes->color,
                                             donnees_vdi_r_mes->indice_color,
                                             &(donnees_bx_variable->pos_es));
        }

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
      break;
    default:
      break;;
    }

  donnees_bx_variable->bloc_genre = b_cour_mes;
  if (donnees_vdi_r_mes->position_es != 0)
    {
    wErreur = recherche_index_tableau_i (donnees_vdi_r_mes->position_es,
                                         donnees_vdi_r_mes->indice,
                                         &(donnees_bx_variable->pos_es));
    }

  donnees_bx_element->HeaderServiceAnmGr = anim_r_mes (n_elt,
                                            donnees_vdi_r_mes->color_cste,
                                            donnees_vdi_r_mes->color,
                                            donnees_vdi_r_mes->couleur_fond
                                            );

  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_num_alpha (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_num)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_NUM_ALPHA donnees_vdi_r_num;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_num = (PVDI_R_NUM_ALPHA)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_alpha_nv1, ptr_sur_b_vdi_r_num);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->prem_variable = ptr_sur_variable;
  switch (donnees_vdi_r_num->color_cste)
    {
    case 1: // couleur cste
      donnees_bx_element->nbr_variable  = 1;
      break;
    case 2: // couleur en variable
      donnees_bx_element->nbr_variable = 2;
      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_num->color != 0)
        {
        wErreur = recherche_index_execution (donnees_vdi_r_num->color,
                                             &(donnees_bx_variable->pos_es));
        donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
        }

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
      break;
    case 3: // couleur variable tableau
      donnees_bx_element->nbr_variable = 2;
      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_num->color != 0)
        {
        wErreur = recherche_index_tableau_i (donnees_vdi_r_num->color,
                                             donnees_vdi_r_num->indice_color,
                                             &(donnees_bx_variable->pos_es));
        }

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
      break;
    default:
      break;
    }

  donnees_bx_variable->bloc_genre = b_cour_num;
  if (donnees_vdi_r_num->position_es != 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_r_num->position_es,
                                         &(donnees_bx_variable->pos_es));
    donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
    }

  donnees_bx_element->HeaderServiceAnmGr = anim_r_num_alpha (n_elt,
                                                  donnees_vdi_r_num->color_cste,
                                                  donnees_vdi_r_num->color,
                                                  donnees_vdi_r_num->couleur_fond,
                                                  donnees_vdi_r_num->nbr_car,
                                                  donnees_vdi_r_num->nbr_dec
                                                  );
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_num_alpha_tab (DWORD numero_page, DWORD n_elt, DWORD ptr_sur_b_vdi_r_num)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  PVDI_R_NUM_ALPHA_TAB donnees_vdi_r_num;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_r_num = (PVDI_R_NUM_ALPHA_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_alpha_tab_nv1, ptr_sur_b_vdi_r_num);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->prem_variable = ptr_sur_variable;
  switch (donnees_vdi_r_num->color_cste)
    {
    case 1: // couleur cste
      donnees_bx_element->nbr_variable  = 1;
      break;
    case 2: // couleur en variable
      donnees_bx_element->nbr_variable = 2;
      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_num->color != 0)
        {
        wErreur = recherche_index_execution (donnees_vdi_r_num->color,
                                             &(donnees_bx_variable->pos_es));
        donnees_bx_variable->pos_es = donnees_bx_variable->pos_es - ContexteGen.nbr_logique;
        }

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
      break;
    case 3: // couleur variable tableau
      donnees_bx_element->nbr_variable = 2;
      donnees_bx_variable->bloc_genre = b_cour_num;
      if (donnees_vdi_r_num->color != 0)
        {
        wErreur = recherche_index_tableau_i (donnees_vdi_r_num->color,
                                             donnees_vdi_r_num->indice_color,
                                             &(donnees_bx_variable->pos_es));
        }

      ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
      donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);
      break;
    default:
      break;
    }

  donnees_bx_variable->bloc_genre = b_cour_num;
  if (donnees_vdi_r_num->position_es != 0)
    {
    wErreur = recherche_index_tableau_i (donnees_vdi_r_num->position_es,
                                         donnees_vdi_r_num->indice,
                                         &(donnees_bx_variable->pos_es));
    }

  donnees_bx_element->HeaderServiceAnmGr = anim_r_num_alpha (n_elt,
                                                  donnees_vdi_r_num->color_cste,
                                                  donnees_vdi_r_num->color,
                                                  donnees_vdi_r_num->couleur_fond,
                                                  donnees_vdi_r_num->nbr_car,
                                                  donnees_vdi_r_num->nbr_dec
                                                  );
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_courbe_temps (DWORD numero_page, DWORD n_elt,
                                   DWORD pos_es, DWORD ptr_sur_b_vdi_courbe)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  DWORD ptr_bx_vdi_courbe;
  PVDI_COURBE_TEMPS donnees_vdi_courbe;
  PXGR_COURBE_TEMPS donneex_vdi_courbe;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_courbe = (PVDI_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_temps, ptr_sur_b_vdi_courbe);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);


  //--------------------- gestion du bloc bx_vdi_courbe
  //
  if (!existe_repere (bx_vdi_courbe_temps))
    {
    cree_bloc (bx_vdi_courbe_temps, sizeof (XGR_COURBE_TEMPS), 0);
    }
  ptr_bx_vdi_courbe = nb_enregistrements (szVERIFSource, __LINE__, bx_vdi_courbe_temps) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_vdi_courbe_temps, ptr_bx_vdi_courbe);
  donneex_vdi_courbe = (PXGR_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_temps, ptr_bx_vdi_courbe);

  donneex_vdi_courbe->pfBufferEchantillons = NULL;
  wErreur = recherche_index_execution (pos_es, &(donneex_vdi_courbe->pos_es_enclanche));
  if (wErreur == 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_courbe->pos_es_a_ech, &(donneex_vdi_courbe->pos_es_stocke));
    if (wErreur == 0)
      {
      donneex_vdi_courbe->pos_es_stocke = donneex_vdi_courbe->pos_es_stocke - ContexteGen.nbr_logique;

      donneex_vdi_courbe->courant_tampon      = 0;
      donneex_vdi_courbe->nbr_echantillon     = donnees_vdi_courbe->nbr_echantillon;
      donneex_vdi_courbe->nbr_stocke          = 0;
      donneex_vdi_courbe->periode_ech_duree   = donnees_vdi_courbe->periode_echantillon;
      donneex_vdi_courbe->periode_ech_tampon  = 0; //$$ MinuterieInitSuspendue
      donneex_vdi_courbe->tampon_plein        = FALSE;

      if (wErreur == 0)
        {
        donnees_bx_element->HeaderServiceAnmGr = anim_r_courbe (n_elt,
                                                    numero_page,
                                                    donnees_vdi_courbe->min,
                                                    donnees_vdi_courbe->max,
                                                    donnees_vdi_courbe->couleur,
                                                    donnees_vdi_courbe->couleur_fond,
                                                    donnees_vdi_courbe->style,
                                                    LINEAIRE,
                                                    donnees_vdi_courbe->nbr_echantillon,
                                                    donnees_vdi_courbe->nb_pts_scroll,
                                                    &wErreur
                                                    );

        if (wErreur == 0)
          {
          donnees_bx_element->prem_variable = ptr_sur_variable;
          donnees_bx_element->nbr_variable  = 1;

          donnees_bx_variable->bloc_genre = bx_vdi_courbe_temps;
          donnees_bx_variable->pos_es     = ptr_bx_vdi_courbe;
          } // wErreur == 0
        } // wErreur == 0
      } // wErreur == 0
    } // wErreur == 0
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_courbe_cmd (DWORD numero_page, DWORD n_elt,
                                 DWORD pos_es, DWORD ptr_sur_b_vdi_courbe)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  DWORD ptr_bx_vdi_courbe;
  PVDI_COURBE_CMD donnees_vdi_courbe;
  PXGR_COURBE_CMD	donneex_vdi_courbe;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;

  wErreur = 0;
  donnees_vdi_courbe = (PVDI_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_cmd, ptr_sur_b_vdi_courbe);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);


  //--------------------- gestion du bloc bx_vdi_courbe_cmd
  //
  if (!existe_repere (bx_vdi_courbe_cmd))
    {
    cree_bloc (bx_vdi_courbe_cmd, sizeof (XGR_COURBE_CMD), 0);
    }
  ptr_bx_vdi_courbe = nb_enregistrements (szVERIFSource, __LINE__, bx_vdi_courbe_cmd) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_vdi_courbe_cmd, ptr_bx_vdi_courbe);
  donneex_vdi_courbe = (PXGR_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_cmd, ptr_bx_vdi_courbe);

  donneex_vdi_courbe->pfBufferEchantillons = NULL;
  wErreur = recherche_index_execution (pos_es, &(donneex_vdi_courbe->pos_es_reset_buffer));
  if (wErreur == 0)
    {
    wErreur = recherche_index_execution (donnees_vdi_courbe->pos_es_a_ech, &(donneex_vdi_courbe->pos_es_stocke));
    if (wErreur == 0)
      {
      donneex_vdi_courbe->pos_es_stocke = donneex_vdi_courbe->pos_es_stocke - ContexteGen.nbr_logique;
      wErreur = recherche_index_execution (donnees_vdi_courbe->pos_es_echantillonnage, &(donneex_vdi_courbe->pos_es_echantillonnage));
      if (wErreur == 0)
        {
        donneex_vdi_courbe->courant_tampon      = 0;
        donneex_vdi_courbe->nbr_echantillon     = donnees_vdi_courbe->nbr_echantillon;
        donneex_vdi_courbe->tampon_plein        = FALSE;

        if (wErreur == 0)
          {
          donnees_bx_element->HeaderServiceAnmGr = anim_r_courbe (n_elt,
                                                      numero_page,
                                                      donnees_vdi_courbe->min,
                                                      donnees_vdi_courbe->max,
                                                      donnees_vdi_courbe->couleur,
                                                      donnees_vdi_courbe->couleur_fond,
                                                      donnees_vdi_courbe->style,
                                                      ESCALIER,
                                                      donnees_vdi_courbe->nbr_echantillon,
                                                      donnees_vdi_courbe->nb_pts_scroll,
                                                      &wErreur
                                                      );
          if (wErreur == 0)
            {
            donnees_bx_element->prem_variable = ptr_sur_variable;
            donnees_bx_element->nbr_variable  = 1;

            donnees_bx_variable->bloc_genre = bx_vdi_courbe_cmd;
            donnees_bx_variable->pos_es     = ptr_bx_vdi_courbe;
            } // wErreur == 0
          } // wErreur == 0
        } // wErreur == 0
      } // wErreur == 0
    } // wErreur == 0
  return (wErreur);
  }

//----------------------------------------------------------------------------
//
static DWORD genere_r_archive (DWORD numero_page, DWORD n_elt,
                              DWORD ptr_sur_b_vdi_courbe)
  {
  DWORD wErreur;
  DWORD ptr_sur_element;
  DWORD ptr_sur_variable;
  DWORD ptr_bx_vdi_archive;
  PVDI_COURBE_FIC donnees_vdi_archive;
  tx_vdi_courbe_archive*donneex_vdi_archive;
  tx_fenetre_vi *donnees_bx_fenetre;
  tx_element_vi *donnees_bx_element;
  tx_variable_vi *donnees_bx_variable;
  DWORD position_es;

  wErreur = 0;
  donnees_vdi_archive = (PVDI_COURBE_FIC)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_fic, ptr_sur_b_vdi_courbe);

  donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, numero_page);
  ptr_sur_element = donnees_bx_fenetre->prem_elt + donnees_bx_fenetre->nbr_elt;
  donnees_bx_fenetre->nbr_elt = donnees_bx_fenetre->nbr_elt + 1;

  insere_enr (szVERIFSource, __LINE__, 1, bx_element_vi, ptr_sur_element);
  donnees_bx_element = (tx_element_vi*)pointe_enr (szVERIFSource, __LINE__, bx_element_vi, ptr_sur_element);

  ptr_sur_variable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_vi, ptr_sur_variable);
  donnees_bx_variable = (tx_variable_vi*)pointe_enr (szVERIFSource, __LINE__, bx_variable_vi, ptr_sur_variable);

  donnees_bx_element->HeaderServiceAnmGr = anim_r_courbe_archive (n_elt, numero_page);
  donnees_bx_element->prem_variable = ptr_sur_variable;
  donnees_bx_element->nbr_variable  = 1;

  //--------------------- gestion du bloc bx_vdi_archive
  //
  if (!existe_repere (bx_vdi_courbe_archive))
    {
    cree_bloc (bx_vdi_courbe_archive, sizeof (tx_vdi_courbe_archive), 0);
    }
  ptr_bx_vdi_archive = nb_enregistrements (szVERIFSource, __LINE__, bx_vdi_courbe_archive) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_vdi_courbe_archive, ptr_bx_vdi_archive);
  donneex_vdi_archive = (tx_vdi_courbe_archive*)pointe_enr (szVERIFSource, __LINE__, bx_vdi_courbe_archive, ptr_bx_vdi_archive);
  wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_fic, &position_es);
  if (wErreur == 0)
    { // fichier
    donneex_vdi_archive->pos_spec_fichier = position_es - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
    wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_couleur, &position_es);
    if (wErreur == 0)
      { // couleur
      donneex_vdi_archive->pos_spec_couleur = position_es - ContexteGen.nbr_logique;
      wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_style, &position_es);
      if (wErreur == 0)
        { // style
        donneex_vdi_archive->pos_spec_style = position_es - ContexteGen.nbr_logique;
        wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_modele, &position_es);
        if (wErreur == 0)
          { // modele
          donneex_vdi_archive->pos_spec_modele = position_es - ContexteGen.nbr_logique;
          wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_min, &position_es);
          if (wErreur == 0)
            { // min
            donneex_vdi_archive->pos_spec_min = position_es - ContexteGen.nbr_logique;
            wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_max, &position_es);
            if (wErreur == 0)
              { // max
              donneex_vdi_archive->pos_spec_max = position_es - ContexteGen.nbr_logique;
              wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_taille_enr, &position_es);
              if (wErreur == 0)
                { // taille
                donneex_vdi_archive->pos_spec_taille = position_es - ContexteGen.nbr_logique;
                wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_offset_enr, &position_es);
                if (wErreur == 0)
                  { // offset
                  donneex_vdi_archive->pos_spec_offset = position_es - ContexteGen.nbr_logique;
                  wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_num_enr, &position_es);
                  if (wErreur == 0)
                    { // de_enr
                    donneex_vdi_archive->pos_spec_enr = position_es - ContexteGen.nbr_logique;
                    wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_nb_pts, &position_es);
                    if (wErreur == 0)
                      { // nb_enr
                      donneex_vdi_archive->pos_spec_nb_enr = position_es - ContexteGen.nbr_logique;
                      wErreur = recherche_index_execution (donnees_vdi_archive->pos_es_cmd_affichage,
                                                           &(donneex_vdi_archive->pos_spec_affichage));
                      } // nb_enr
                    } // de_enr
                  } // offset
                } // taille
              } // max
            } // min
          } // modele
        } // style
      } // couleur
    } // fichier

  donnees_bx_variable->bloc_genre = bx_vdi_courbe_archive;
  donnees_bx_variable->pos_es     = ptr_bx_vdi_archive;

  return (wErreur);
  }

//-------------------------------------------------------------------------
//
static void InsereTouches (void)
  {
  PX_VAR_SYS	pwPosition;

  if (!existe_repere (bx_entrees_vi))
    {
    cree_bloc (bx_entrees_vi, sizeof (tx_entrees_vi), 0);
    }
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff1);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff2);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff3);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff4);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff5);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff6);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff7);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff8);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff9);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff10);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff11);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff12);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff13);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff14);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff15);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff16);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff17);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff18);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff19);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  pwPosition = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, ff20);
  insere_variable_entree (b_cour_log, pwPosition->dwPosCourEx);
  }

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
DWORD genere_vi (DWORD *ligne)
  {
  DWORD n_ligne;
  DWORD nbr_page;
  DWORD wRetour;
  DWORD n_elt;
  PGEO_VDI val_geo;
  DWORD n_page;
  tx_fenetre_vi *donnees_bx_fenetre;
  PX_ANM_FENETRE_VI	donnees_bx_anm_fenetre;

  wRetour = 0;
  if (existe_fenetres_graphiques () )
    {
    initialise_mode_graph ();
    organise_services_speciaux ();
    //
    InsereTouches (); // ATTENTION : entrees a inserer en premier
    //
    wRetour = organise_fenetre ();
    if (wRetour == 0)
      {
      nbr_page = nb_enregistrements (szVERIFSource, __LINE__, bx_fenetre_vi);
      }
    else
      {
      nbr_page = 0;
      }
    n_page = 1;
    while ((n_page <= nbr_page) && (wRetour == 0))
      {
      donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, n_page);
      if (donnees_bx_fenetre->existe)
        {
        maj_page_gr_courante (n_page);
        donnees_bx_anm_fenetre = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, n_page);
        donnees_bx_anm_fenetre->prem_entree = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_vi) + 1;
        donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, n_page);
        donnees_bx_fenetre->prem_elt = nb_enregistrements (szVERIFSource, __LINE__, bx_element_vi) + 1;
        n_elt = 1;
        while (animation_presente_gr (&n_elt, &n_ligne))
          {
          interact_pm_graph ();
          val_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi, n_ligne);
          switch (val_geo->numero_repere)
            {
            case b_vdi_e_log_nv1:
              wRetour = genere_e_log (n_page, &n_elt, val_geo->pos_es, val_geo->pos_specif_es);
							// pas d'increment de n_elt i�i car depend du selecteur. Le traitement est fait dans anim_e_log
              break;

            case b_vdi_e_num:
              wRetour = genere_e_num (n_page, n_elt, val_geo->pos_es, val_geo->pos_specif_es);
							// pas d'increment de n_elt car l'element graphique est supprim�
              break;

            case b_vdi_e_mes:
              wRetour = genere_e_mes (n_page, n_elt, val_geo->pos_es, val_geo->pos_specif_es);
							// pas d'increment de n_elt car l'element graphique est supprim�
              break;

            case b_vdi_r_log :
              wRetour = genere_r_log (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_log_tab :
              wRetour = genere_r_log_tab (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_log_4etats :
              wRetour = genere_r_log_4etats (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_log_4etats_tab :
              wRetour = genere_r_log_4etats_tab (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_num:
              wRetour = genere_r_num (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_num_tab :
              wRetour = genere_r_num_tab (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_num_dep :
              wRetour = genere_r_num_dep (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_num_dep_tab :
              wRetour = genere_r_num_dep_tab (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_mes_nv2 :
              wRetour = genere_r_mes (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_mes_tab_nv2 :
              wRetour = genere_r_mes_tab (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_num_alpha_nv1 :
              wRetour = genere_r_num_alpha (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_r_num_alpha_tab_nv1 :
              wRetour = genere_r_num_alpha_tab (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

            case b_vdi_courbe_temps :
              wRetour = genere_r_courbe_temps (n_page, n_elt, val_geo->pos_es, val_geo->pos_specif_es);
							// pas d'increment de n_elt car l'element graphique est supprim�
              break;

            case b_vdi_courbe_cmd :
              wRetour = genere_r_courbe_cmd (n_page, n_elt, val_geo->pos_es, val_geo->pos_specif_es);
							// pas d'increment de n_elt car l'element graphique est supprim�
              break;

            case b_vdi_courbe_fic :
              wRetour = genere_r_archive (n_page, n_elt, val_geo->pos_donnees);
							// pas d'increment de n_elt car l'element graphique est supprim�
              break;

						case b_vdi_control_radio:       
              wRetour = genere_e_control_radio (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

						case b_vdi_control_liste:
              wRetour = genere_r_liste (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

						case b_vdi_control_combobox:
              wRetour = genere_r_combobox (n_page, n_elt, val_geo->pos_donnees);
              n_elt++;
              break;

						case b_vdi_control_static:      
              wRetour = genere_r_control_static (n_page, n_elt, val_geo->pos_es);
              n_elt++;
              break;


            default:
              n_elt++;
              break;

            } // switch numero repere

          } // while animation

        } // fenetre existe
      n_page++;
      } // while fenetre

    enleve_bloc (b_vdi_e_log_nv1);
    enleve_bloc (b_vdi_e_num);
    enleve_bloc (b_vdi_e_mes);

    enleve_bloc (b_vdi_r_log);
    enleve_bloc (b_vdi_r_log_tab);
    enleve_bloc (b_vdi_r_log_4etats);
    enleve_bloc (b_vdi_r_log_4etats_tab);

    enleve_bloc (b_vdi_r_num);
    enleve_bloc (b_vdi_r_num_tab);
    enleve_bloc (b_vdi_r_num_dep);
    enleve_bloc (b_vdi_r_num_dep_tab);
    enleve_bloc (b_vdi_r_num_alpha_nv1);
    enleve_bloc (b_vdi_r_num_alpha_tab_nv1);
    enleve_bloc (b_vdi_r_mes_nv2);
    enleve_bloc (b_vdi_r_mes_tab_nv2);
    enleve_bloc (b_vdi_courbe_temps);
    enleve_bloc (b_vdi_courbe_cmd);
    enleve_bloc (b_vdi_courbe_fic);

    enleve_bloc (b_vdi_control_static);
    enleve_bloc (b_vdi_control_radio);
    enleve_bloc (b_vdi_control_liste);
    enleve_bloc (b_vdi_control_combobox);

    termine_mode_graph ();
    } // existe pages
  return (wRetour);
  }

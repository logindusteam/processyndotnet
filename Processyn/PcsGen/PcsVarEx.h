/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de	Societe LOGIQUE INDUSTRIE				    |
 | Il est et demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/
// PcsVarEx.h
// Gestion des variables de Processyn en exploitation
// Win32 JS 15/5/97
class CPcsVarEx  
	{
	public:
		// CPcsVarEx();
		// virtual ~CPcsVarEx();

	public:
		//---------------------------------------------------------------------------
		//  EN EXPLOITATION FONCTION D'ACCES ECRITURE ET LECTURE AUX B_COUR
		//  CES FONCTIONS PRENNENT EN COMPTE LA MISE A JOUR D'UN BIT DE MODIFICATION
		//---------------------------------------------------------------------------

		// REMARQUE : A GENERALISER DANS TOUS LES SCRUT_XX

		//---------------------------------------------------------------------------
		// creation avec le m�me nombre d'enregistrement que les valeurs courantes
		// des blocs contenant l'indicateur de modification
		static void CreeBlocsExValeurModifie (void);
		//---------------------------------------------------------------------------
		// remise � la valeur FALSE de tous les enregistrements
		// des blocs contenant l'indicateur de modification
		static void ResetBlocsExValeurModifie (void);
		// Lance un programme externe
		static DWORD wGetNombreEsApplicBis (void);

		//---------------------------------------------------------------------------
		// Exploitation : renvoie position et bloc (b_cour_log, b_cour_num ou b_cour_mes) d'une variable - ou 0 
		static DWORD dwGetVarNomEx
			(
			PCSTR pszNomVar,	// Adresse du nom de la variable recherch�e.
			DWORD * pdwBloc		// Adresse du genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
			);								// Retour : 0  si var introuvable, sinon position dans b_cour de la variable

		//---------------------------------------------------------------------------
		// Exploitation : renvoie position, nombre d'enregistrements, et bloc d'une variable - ou 0  
		static DWORD dwGetVarNomTailleEx
			(
			PCSTR pszNomVar,	// Adresse du nom de la variable recherch�e.
			DWORD * pdwBloc,		// Adresse du genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
			DWORD * pdwTaille	// Adresse du nb d'enregistrements (0 pour une variable simple, nb d'enregistrements max pour un tableau)
			);								// Retour : 0  si var introuvable, sinon position dans b_cour de la variable

		//---------------------------------------------------------------------------
		// Exploitation : renvoie le nombre total d'entr�es sorties - tous genres confondus
		static DWORD dwGetNbGlobalVarsEx	(void);	// Retour : le nombre

		//---------------------------------------------------------------------------
		// Exploitation : renvoie des infos sur une var d'entr�es sorties - tous genres confondus
		static BOOL bListeGlobalVarEx(
			DWORD dwNGlobalVarEx, // Num�ro Global de la variable d'entr�e sortie (de 1 � dwGetNbGlobalVarsEx)
			PDWORD pdwPos,				// position dans le bloc
			PDWORD pdwBloc				// bloc (b_cour_log, b_cour_num ou b_cour_mes)
			);

		//---------------------------------------------------------------------------
		// Exploitation : renvoie position et bloc (b_cour_log, b_cour_num ou b_cour_mes) d'une variable syst�me- ou 0 
		static DWORD dwGetVarSysEx
			(
			DWORD dwIdVarSys,	// Id de la variable syst�me (= Position dans le bloc b_va_systeme) ex : premier_passage
			DWORD * pdwBloc		// Adresse du genre (b_cour_log, b_cour_num ou b_cour_mes sinon 0)
			);								// Retour : 0  si var introuvable, sinon position dans b_cour de la variable

		//---------------------------------------------------------------------------
		// Exploitation : mise � jour de la valeur d'une variable (d'apr�s sa position dans b_courXXXX)
		// et de son indicateur de modification
		static void SetValVarLogEx (DWORD dwIndex, BOOL bVal);
		static void SetValVarNumEx (DWORD dwIndex, FLOAT fVal);
		static void SetValVarMesEx (DWORD dwIndex, PCSTR pszVal);

		//---------------------------------------------------------------------------
		// Exploitation : mise � jour de la valeur d'une variable (d'apr�s sa position dans b_courXXXX)
		// et de son indicateur de modification - SEULEMENT si la valeur est diff�rente
		// Renvoie TRUE si la valeur a �t� modifi�e, FALSE sinon
		static BOOL bSetValVarLogExDif (DWORD dwIndex, BOOL bVal);
		static BOOL bSetValVarNumExDif (DWORD dwIndex, FLOAT fVal);
		static BOOL bSetValVarMesExDif (DWORD dwIndex, PCSTR pszVal);

		//---------------------------------------------------------------------------
		// Exploitation : lecture de la valeur courante d'une variable (d'apr�s sa position dans b_courXXX)
		static BOOL	bGetValVarLogEx		(DWORD dwIndex); // Renvoie la valeur courante
		static FLOAT	fGetValVarNumEx		(DWORD dwIndex); // Renvoie la valeur courante
		// pszDest est un buffer dimensionn� � au moins c_nb_car_ex_mes car dans lequel le message est recopi�
		static PSTR	pszGetValVarMesEx (PSTR pszDest, DWORD dwIndex); // Renvoie pszDest mis � jour avec la valeur courante

		//---------------------------------------------------------------------------
		// Exploitation : lecture de l'ancienne valeur d'une variable (d'apr�s sa position dans b_courXXX)
		static BOOL	bGetValMemVarLogEx		(DWORD dwIndex); // Renvoie l'ancienne valeur
		static FLOAT	fGetValMemVarNumEx		(DWORD dwIndex); // Renvoie l'ancienne valeur
		// pszDest est un buffer dimensionn� � au moins c_nb_car_ex_mes car dans lequel le message est recopi�
		static PSTR	pszGetValMemVarMesEx (PSTR pszDest, DWORD dwIndex); // Renvoie pszDest mis � jour avec l'ancienne valeur
		// Exploitation : �criture de l'ancienne valeur d'une variable (d'apr�s sa position dans b_courXXX)
		static void SetValMemVarLogEx (DWORD dwIndex, BOOL bVal);
		static void SetValMemVarNumEx (DWORD dwIndex, FLOAT fVal);
		static void SetValMemVarMesEx (DWORD dwIndex, PCSTR pszVal);

		//---------------------------------------------------------------------------
		// Exploitation : mise � jour de la valeur d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
		// et de son indicateur de modification (d'apr�s son Id dans b_va_systeme)
		static void SetValVarSysLogEx (ID_VAR_SYS dwIdVarSys, BOOL bVal);
		static void SetValVarSysNumEx (ID_VAR_SYS dwIdVarSys, FLOAT fVal);
		static void SetBitsValVarSysNumEx (ID_VAR_SYS dwIdVarSys, DWORD dwBitsOu); // (DWORD)val |= dwBitsOu;
		static void SetValVarSysMesEx (ID_VAR_SYS dwIdVarSys, PCSTR pszVal);

		//---------------------------------------------------------------------------
		// Exploitation : lecture de la valeur courante d'une variable syst�me (d'apr�s sa position dans b_va_systeme)
		static BOOL	bGetValVarSysLogEx		(ID_VAR_SYS dwIdVarSys); // Renvoie la valeur courante
		static FLOAT	fGetValVarSysNumEx		(ID_VAR_SYS dwIdVarSys); // Renvoie la valeur courante
		// pszDest est un buffer dimensionn� � au moins c_nb_car_ex_mes car dans lequel le message est recopi�
		static PSTR	pszGetValVarSysMesEx (PSTR pszDest, ID_VAR_SYS dwIdVarSys); // Renvoie pszDest mis � jour avec la valeur courante

		//---------------------------------------------------------------------------
		// Transfert de valeurs du bloc b_cour pour un nombre d'enregistrement donn�
		// de la position source � la position destination ainsi que de la valeur TRUE de l'enregistrement
		// correspondant du bloc contenant l'indicateur de modification
		static void TransfertBlocCourLogEx (DWORD dwNbATransferer, DWORD dwIndexSource, DWORD dwIndexDestination);
		static void TransfertBlocCourNumEx (DWORD dwNbATransferer, DWORD dwIndexSource, DWORD dwIndexDestination);
		static void TransfertBlocCourMesEx (DWORD dwNbATransferer, DWORD dwIndexSource, DWORD dwIndexDestination);

		//---------------------------------------------------------------------------
		// Renvoie l'indicateur de modification de la valeur de la variable dwIndex dans b_cour_log, b_cour_num ou b_cour_mes
		static BOOL bGetBlocCourLogExValeurModifie (DWORD dwIndex);
		static BOOL bGetBlocCourNumExValeurModifie (DWORD dwIndex);
		static BOOL bGetBlocCourMesExValeurModifie (DWORD dwIndex);
		//---------------------------------------------------------------------------
		// Renvoie l'indicateur de modification de la valeur de la variable dwPos dans le bloc sp�cifi�
		static BOOL bGetBlocExValeurModifie
			(
			DWORD dwIndex,
			DWORD dwBloc	// b_cour_log, b_cour_num, b_cour_mes
			);

		//---------------------------------------------------------------------------
		// Renvoie la position dans b_cour_log d'une variable logique ou 0 si un param�tre est invalide
		static DWORD dwPosVarLogEx
			(
			DWORD dwPos, // position dans b_cour_log
			DWORD dwBloc,// doit �tre b_cour_log
			DWORD dwIndex// 0 pour une variable simple 1..Nb �l�ments tableau pour un tableau
			);

		//---------------------------------------------------------------------------
		// Renvoie la position dans b_cour_num d'une variable num�rique ou 0 si un param�tre est invalide
		static DWORD dwPosVarNumEx
			(
			DWORD dwPos, // position dans b_cour_num
			DWORD dwBloc,// doit �tre b_cour_num
			DWORD dwIndex// 0 pour une variable simple 1..Nb �l�ments tableau pour un tableau
			);

		//---------------------------------------------------------------------------
		// Renvoie la position dans b_cour_mes d'une variable num�rique ou 0 si un param�tre est invalide
		static DWORD dwPosVarMesEx
			(
			DWORD dwPos, // position dans b_cour_mes
			DWORD dwBloc,// doit �tre b_cour_mes
			DWORD dwIndex// 0 pour une variable simple 1..Nb �l�ments tableau pour un tableau
			);

		//---------------------------------------------------------------------------
		// Renvoie les infos d'une Var en exploitation
		static BOOL bInfoVarEx
			(
			DWORD dwPos, // position dans le bloc
			DWORD dwBloc,// bloc (b_cour_log, b_cour_num ou b_cour_mes)
			PX_E_S px_e_s	// adresse des infos de la var voulue
			);

	// membres
	private:
		// Comparaison pour recherche d'une variable d'entr�e sortie en exploitation d'apr�s son nom
		static int ComparaisonNomVar (const void *pszNomCherche, DWORD numero);
	};



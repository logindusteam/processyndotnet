/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------*/
 // Win32 10/12/97
// WGen.c
// Thread et Fen�tre interface utilisateur principale de PcsGen

#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "UStr.h"
#include "USem.h"
#include "threads.h"
#include "initdesc.h"
#include "LireLng.h"
#include "IdLngLng.h"
#include "ULangues.h"
#include "lng_res.h"
#include "tipe.h"
#include "actiongn.h"
#include "inpugn.h"
#include "pcsdlg.h"
#include "PathMan.h"
#include "DocMan.h"
#include "IdGen.h"
#include "IdGenLng.h"
#include "VersMan.h"

#include "WGen.h"

// variables globales
BOOL bAbandonnerCompil = FALSE;
BOOL bQuitterGen = FALSE;
HTHREAD hThrdPM = NULL;
HSEM hsemCreationWGen = NULL;


typedef enum {PAS_DE_MOULIN, MOULIN_1, MOULIN_2} ETAT_MOULIN;
static ETAT_MOULIN etatMoulin = PAS_DE_MOULIN;

static DWORD cpteur_tab_pb = 2;
static HICON hIcon1;
static HICON hIcon2;

// Variables globales
static char mess[c_nb_car_message_err];
static char titre[c_nb_car_message_inf];
static char msg_abort[256];
//static char szBarreEtat[c_nb_car_message_inf];

// --------------------------------------------------------------------------
static void ChoisirApplication (HWND hwnd)
	{
	char action[c_nb_car_message_inf];
	char titre[c_nb_car_message_inf];
	char saisie[MAX_PATH];
  t_param_action param_action;

  bLngLireInformation (c_inf_tdlg_ouvrir, titre);
  bLngLireInformation (c_inf_action_ouvre, action);
	saisie[0] = '\0'; //$$ arranger
	pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
	if (bDlgFichierOuvrir (hwnd, saisie, titre, "pcs", action))
    {
    StrCopy (param_action.chaine, pszSupprimeExt (saisie));
    }
  else
    {
    StrCopy (param_action.chaine, pszPathNameDoc());
    }
  ajouter_action (action_choisit_application, &param_action);
	}

// -----------------------------------------------------------------------
// Traitement du message WM_COMMAND
// -----------------------------------------------------------------------
static void OnCommand (HWND hwnd, WPARAM mp1)
  {
  t_param_action param_action;

  switch (LOWORD (mp1))
    {
    case ID_OUVRIR_FICHIER:
			ChoisirApplication (hwnd);
			break;

    case ID_GENERER:
      bAbandonnerCompil = FALSE;
      StrCopy (param_action.chaine, pszPathNameDoc());
      ajouter_action (action_genere, &param_action);
      break;

    case ID_ABANDONNER_GENERATION:
      bAbandonnerCompil = TRUE;
      break;

		case IDCANCEL:
    case ID_QUITTER:
			EndDialog (hwnd, 1);
      //quitter = TRUE;
      //ajouter_action (action_quitte, &param_action);
      break;
    }
  } // OnCommand

// ----------------------------------------------------------------
static void SetEtatMoulin (HWND hdlg, ETAT_MOULIN NouvelEtat)
	{
	HICON hIcon = NULL;

	// changement d'�tat de l'ic�ne ?
	if (etatMoulin != NouvelEtat)
		{
		// oui : on enregistre son nouvel �tat ...
		etatMoulin = NouvelEtat;
		switch (etatMoulin)
			{
			case MOULIN_1:
				hIcon = hIcon1;
				break;

			case MOULIN_2:
				hIcon = hIcon2;
				break;
			}
		// ... et remplace l'icone d'animation
		hIcon = (HICON)SendDlgItemMessage (hdlg, IDS_ICO_ANIME, STM_SETICON, (WPARAM)hIcon, 0);
		}
	} // SetEtatMoulin

// ----------------------------------------------------------------
//	WndProc de la fen�tre de g�n�ration - traite les messages re�us
static BOOL CALLBACK dlgprocGenere (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL bRes = FALSE;      // Valeur de retour
  BOOL bTraite = TRUE; // Indique commande trait�e

  switch (msg)
    {
    case WM_INITDIALOG:
			// initialise les ressources n�cessaires
			SetEtatMoulin (hwnd, PAS_DE_MOULIN);
			Appli.bSetHWndPrincipal(hwnd);
      bAbandonnerCompil = FALSE;
      bQuitterGen    = FALSE;
			// charge l'icone de l'appli
      hIcon1 = LoadIcon (Appli.hinst, MAKEINTRESOURCE(PCS_GEN));
			::SendMessage (hwnd, WM_SETICON, TRUE, (LPARAM)hIcon1);
			// charge les icones d'animation
      hIcon1 = LoadIcon (Appli.hinstDllLng, MAKEINTRESOURCE(ID_ICO_GN1));
      hIcon2 = LoadIcon (Appli.hinstDllLng, MAKEINTRESOURCE(ID_ICO_GN2));
      ::SetTimer (hwnd, 1, 500, NULL);
			bSemLibere (hsemCreationWGen);
      break;

		case WM_DESTROY:
			// lib�re les ressources
      SetEtatMoulin (hwnd, PAS_DE_MOULIN);
			DestroyIcon(hIcon1);
			DestroyIcon(hIcon2);
      ::KillTimer (hwnd, 1);
			Appli.bSetHWndPrincipal(NULL);
			bSemFerme (&hsemCreationWGen);
			break;

    case WM_TIMER:
			// Animation : alternance Moulin 1 / Moulin 2
			if (etatMoulin == MOULIN_1)
				SetEtatMoulin (hwnd, MOULIN_2);
			else
				if (etatMoulin == MOULIN_2)
				SetEtatMoulin (hwnd, MOULIN_1);
      break;

    case WM_SET_TEXTE:
      SetDlgItemText (hwnd, ID_NOM_APPLIC, pszPathNameDoc());
      break;

    case WM_AFFI_ERR:
			SetEtatMoulin (hwnd, PAS_DE_MOULIN); //$$ ??
      dlg_erreur ((PSTR)(mp1));
      break;

    case WM_NOUVEAU_STATUT_GENERATION:
      ::ShowWindow (::GetDlgItem (hwnd, IDC_STATUT_GENERATION), SW_SHOW);
      SetDlgItemText (hwnd, IDC_STATUT_GENERATION, (PCSTR)(mp1));
      break;

    case WM_CACHE_STATUT_GENERATION:
      ::ShowWindow (::GetDlgItem (hwnd, IDC_STATUT_GENERATION), SW_HIDE);
      break;

		case WM_CHOISIR_APPLIC:
			ChoisirApplication (hwnd);
			break;

    case WM_DEBUT_GENERE:
			{
			::EnableWindow (::GetDlgItem(hwnd, ID_OUVRIR_FICHIER), FALSE);
			::EnableWindow (::GetDlgItem(hwnd, ID_GENERER), FALSE);
			::EnableWindow (::GetDlgItem(hwnd, ID_QUITTER), FALSE);
			::EnableWindow (::GetDlgItem(hwnd, ID_ABANDONNER_GENERATION), TRUE);
			SetEtatMoulin (hwnd, MOULIN_1);
			}
      break;

    case WM_FIN_GENERE:
			::EnableWindow (::GetDlgItem(hwnd, ID_OUVRIR_FICHIER), TRUE);
			::EnableWindow (::GetDlgItem(hwnd, ID_GENERER), TRUE);
			::EnableWindow (::GetDlgItem(hwnd, ID_QUITTER), TRUE);
			::EnableWindow (::GetDlgItem(hwnd, ID_ABANDONNER_GENERATION), FALSE);
      bAbandonnerCompil = FALSE;
      SetEtatMoulin (hwnd, PAS_DE_MOULIN);
      break;

		case WM_CLOSE:
    case WM_QUITTE_GENERE:
      EndDialog(hwnd, 1);
			//quitter = TRUE;
      bAbandonnerCompil = TRUE;
      break;

		case WM_HELP:
			bAfficheInfoVersion();
			break;

    case WM_COMMAND:
      OnCommand (hwnd, mp1);
      break;

    default:
      bTraite = FALSE;
      break;
    }

  if (!bTraite)
    bRes = FALSE;

  return bRes;
  } // dlgprocGenere


// --------------------------------------------------------------------------
// thread
UINT	__stdcall uThreadWGen (void *hThrd)
  {
  UINT      wFct;

  ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
  while (bThreadAttenteMessage ((HTHREAD)hThrd, NULL, NULL, NULL, &wFct))
    {
	  t_param_action param_action;

    switch (wFct)
      {
      case THRD_FCT_INIT:
        bQuitterGen = FALSE;
        //initialiser_fenetres ();
        ThreadFinTraiteMessage ((HTHREAD)hThrd);
        break;

      case THRD_FCT_EXEC:
        LangueDialogBoxParam (MAKEINTRESOURCE (IDDLG_GEN), NULL, dlgprocGenere, 0);
				bQuitterGen = TRUE;
		    ajouter_action (action_quitte, &param_action);

        if (bQuitterGen)
          {
          ThreadFinTraiteMessage ((HTHREAD)hThrd);
          ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, INFINITE);
          }
        else
          {
          ThreadSetDureeAttenteMessage ((HTHREAD)hThrd, 0L);
          }
        break;
      default:
        break;
      }
    }
  return uThreadTermine ((HTHREAD)hThrd, 0);
  } // ThreadWGen


// -----------------------------------------------------------------------
void affi_err (DWORD dwNMessageErreur)
  {
  if (!bQuitterGen)
    {
    mess[0] = '\0';
    bLngLireErreur (dwNMessageErreur, mess);
    ::PostMessage (Appli.hwnd, WM_AFFI_ERR, (WPARAM)(mess), 0L);
    }
  }
// -----------------------------------------------------------------------
void affi_err_ligne (DWORD numero, DWORD ligne)
  {
  char chaine[10];

  if (!bQuitterGen)
    {
    StrSetNull (mess);
    bLngLireErreur (numero, mess);
    StrDWordToStr (chaine, ligne);
    StrConcat (mess, " (");
    StrConcat (mess, chaine);
    StrConcat (mess, ")");
    ::PostMessage (Appli.hwnd, WM_AFFI_ERR, (WPARAM)(mess), 0L);
    }
  }

// -----------------------------------------------------------------------
void abort_genere (char *chaine, DWORD numero) //$$ non appel�
  {
  char num[12];

  if (!bQuitterGen)
    {
    StrCopy (msg_abort, chaine);
    StrConcat (msg_abort, "  ");
    StrDWordToStr (num, numero);
    StrConcat (msg_abort, num);
    ::PostMessage (Appli.hwnd, WM_AFFI_ERR, (WPARAM)(msg_abort), 0L);
    }
  }

// -----------------------------------------------------------------------
void AfficheDescripteurDansStatutGen (DWORD numero_driver, BOOL affiche)
  {
  DWORD c_inf_titre, c_inf_mnemo;

  if (!bQuitterGen)
    {
    if (bInfoDescripteur (numero_driver, &c_inf_titre, &c_inf_mnemo) & affiche)
      {
      titre[0] = '\0';
      bLngLireInformation (c_inf_titre, titre);
      ::PostMessage (Appli.hwnd, WM_NOUVEAU_STATUT_GENERATION, (WPARAM)(titre), 0L);
      }
    else
      {
      ::PostMessage (Appli.hwnd, WM_CACHE_STATUT_GENERATION, 0L, 0L);
      }
    }
  }

// -----------------------------------------------------------------------
void AfficheInformationDansStatutGen (DWORD wNumMessInfo)
  {
  titre[0] = '\0';
  bLngLireInformation (wNumMessInfo, titre);
  ::PostMessage (Appli.hwnd, WM_NOUVEAU_STATUT_GENERATION, (WPARAM)(titre), 0L);
  }

// -----------------------------------------------------------------------
void affi_mess_err (char *pszMessage)
  {
  StrCopy (mess, pszMessage);
  ::PostMessage (Appli.hwnd, WM_AFFI_ERR, (WPARAM)(mess), 0L);
  }

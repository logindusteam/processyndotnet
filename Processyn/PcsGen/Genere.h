// Genere.h

// ---------------------------------------------------------------------------
// Transforme la base de donn�e de configuration en base de donn�ee d'exploitation
// (g�n�ration)
// ---------------------------------------------------------------------------
DWORD dwGenereApplic
	(
	DWORD * pdwNLigne,			// En cas d'erreur, met � jour la ligne ayant provoqu� l'erreur
	PCSTR pszNomApplication	// Nom de l'application (utilis� pour la sauvegarde)
	); // Renvoie 0 si Ok sinon un num�ro d'erreur de g�n�ration

// fin Genere.h

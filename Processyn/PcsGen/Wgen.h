/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Auteur  : LM
 |   Date    : 07/12/92
 +----------------------------------------------------------------------*/


#define WM_FIN_GENERE      (WM_USER + 2)
#define WM_AFFICHE_NOM_FIC (WM_USER + 3)
#define WM_AFFI_ERR        (WM_USER + 4)
#define WM_NOUVEAU_STATUT_GENERATION       (WM_USER + 5)
#define WM_CACHE_STATUT_GENERATION     (WM_USER + 6)
#define WM_SET_TEXTE       (WM_USER + 7)
#define WM_QUITTE_GENERE   (WM_USER + 8)
#define WM_DEBUT_GENERE    (WM_USER + 9)
#define WM_CHOISIR_APPLIC  (WM_USER + 10)

extern HTHREAD hThrdPM;
extern BOOL bAbandonnerCompil;
extern BOOL bQuitterGen;
extern HSEM hsemCreationWGen;

void affi_err (DWORD dwNMessageErreur);
void affi_err_ligne (DWORD numero, DWORD ligne);
void AfficheDescripteurDansStatutGen (DWORD numero_driver, BOOL affiche);
void AfficheInformationDansStatutGen (DWORD wNumMessInfo);
void affi_mess_err (char *pszMessage);
void abort_genere (char *chaine, DWORD numero);
UINT __stdcall uThreadWGen (void *hThrd);


/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Interface de charge fichier ovr				|
 |	       								|
 |   Auteur  : AC							|
 |   Date    : 5/2/92 							|
 +----------------------------------------------------------------------*/


BOOL bInfoDescripteur (DWORD numero_driver, DWORD *c_inf_titre, DWORD *c_inf_mnemo);
// Charge � partir des ressources les variables syst�mes dans l'application courante
BOOL bChargeVariablesSystemes (void);

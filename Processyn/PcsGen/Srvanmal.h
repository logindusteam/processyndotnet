// ---------------------------------- Type du service demand�
//
#define  ANM_AL_TYPE_SRV_INIT              0
#define  ANM_AL_TYPE_SRV_EXEC              1
#define  ANM_AL_TYPE_SRV_TERM              2
#define  NB_TYPES_SERVICES_ANM_AL      3


// ---------------------------------- Liste des services de l'animateur
//
#define  ANM_AL_SRV_ACK                    0   // NULL

#define  ANM_AL_SRV_FORCAGE                1   // Boolean ou FLOAT
#define  ANM_AL_SRV_GEO                    2   // Boolean ou FLOAT
#define  ANM_AL_SRV_RAZ                    3   // DWORD ss. fct. ...
#define  ANM_AL_SRV_EVT                    4   // Buffer : 1 ou plsieurs vars. log, num ou mes
#define  ANM_AL_SRV_FIN                    5   // NULL

#define  NB_SERVICES_ANM_AL        6

// ---------------- Sous fonctions du service raz et acq: ANM_AL_SRV_RAZ
//
#define  ANM_AL_RAZ_GLOB     0
#define  ANM_AL_RAZ_GRP      1
#define  ANM_AL_ACQ_GLOB     2


// ----------------------------- Taille Buffer de reception pour l'animateur
//                                  "     "    d' emission  pour PCS-EXE
//
#define  ANM_AL_MAX_BUFF_SERVICES      30720

// ----------------------------- Taille Buffer de emission  pour l'animateur
//                                  "     "    de reception pour PCS-EXE
//
#define  ANM_AL_MAX_BUFF_EVENTS        30720

// ---------------------------------- Type Des Donn�es Re�ues par l'animateur
//                                                     Emises par PCS-EXE
typedef struct
  {
  DWORD  TypeService;
  DWORD  NbServices;
  BYTE  bBufferServices [ANM_AL_MAX_BUFF_SERVICES];
  } ANM_AL_INFOS_SERVICES;

/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Auteur  : LM							|
 |   Date    : 12/08/93 						|
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include "std.h"           // Types Standards
#include "lng_res.h"       // Mots Reserves pour tests alarmes
#include "tipe.h"          // Types Generaux
#include "UStr.h"        // Chaines de Caract�res            Strxxx ()
#include "mem.h"           // Memory Manager
#include "pcs_sys.h"       // Variables systemes
#include "findbdex.h"
#include "driv_dq.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "UChrono.h"
#include "tipe_al.h"       // Types des Blocs
#include "srvanmal.h"      // Types Services Animateur Alarmes
#include "gnanmal.h"       // Fabrications des Blocs Animateur Alarmes
#include "GenereBD.h"
#include "Verif.h"
VerifInit;

#include "gener_al.h"


// -------------------------------------------------------------------------
//
static void InsereGeoAl (DWORD wNumVarSys, DWORD wIndexVarSys, DWORD wBlocGenre,
                         DWORD wPosGeoAl, DWORD wGenreVarAl)
  {
  t_valeur_al   Valeur;
  tx_geo_al*	pGeoAl;
  PX_VAR_SYS pVaSysteme;

  Valeur.bValLogique = FALSE;
  Valeur.rValNumerique = (FLOAT)0;
  pGeoAl = (tx_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_geo_al, wPosGeoAl);
  pGeoAl->wBlocGenre = wBlocGenre;
  pVaSysteme = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, wNumVarSys);
  pGeoAl->wPosEs = pVaSysteme->dwPosCourEx + wIndexVarSys;
  pGeoAl->bForcage = FALSE;
  pGeoAl->handle_al = AnimGeoAl (wGenreVarAl, &Valeur, wPosGeoAl);
  }

//-------------------------------------------------------------------------
//
static void InsereTouches (void)
  {
  InsereGeoAl (ff1,  0, b_cour_log, AL_TOUCHES_FCT + 0,  LOGIQUE_AL);
  InsereGeoAl (ff2,  0, b_cour_log, AL_TOUCHES_FCT + 1,  LOGIQUE_AL);
  InsereGeoAl (ff3,  0, b_cour_log, AL_TOUCHES_FCT + 2,  LOGIQUE_AL);
  InsereGeoAl (ff4,  0, b_cour_log, AL_TOUCHES_FCT + 3,  LOGIQUE_AL);
  InsereGeoAl (ff5,  0, b_cour_log, AL_TOUCHES_FCT + 4,  LOGIQUE_AL);
  InsereGeoAl (ff6,  0, b_cour_log, AL_TOUCHES_FCT + 5,  LOGIQUE_AL);
  InsereGeoAl (ff7,  0, b_cour_log, AL_TOUCHES_FCT + 6,  LOGIQUE_AL);
  InsereGeoAl (ff8,  0, b_cour_log, AL_TOUCHES_FCT + 7,  LOGIQUE_AL);
  InsereGeoAl (ff9,  0, b_cour_log, AL_TOUCHES_FCT + 8,  LOGIQUE_AL);
  InsereGeoAl (ff10, 0, b_cour_log, AL_TOUCHES_FCT + 9,  LOGIQUE_AL);
  InsereGeoAl (ff11, 0, b_cour_log, AL_TOUCHES_FCT + 10, LOGIQUE_AL);
  InsereGeoAl (ff12, 0, b_cour_log, AL_TOUCHES_FCT + 11, LOGIQUE_AL);
  InsereGeoAl (ff13, 0, b_cour_log, AL_TOUCHES_FCT + 12, LOGIQUE_AL);
  InsereGeoAl (ff14, 0, b_cour_log, AL_TOUCHES_FCT + 13, LOGIQUE_AL);
  InsereGeoAl (ff15, 0, b_cour_log, AL_TOUCHES_FCT + 14, LOGIQUE_AL);
  InsereGeoAl (ff16, 0, b_cour_log, AL_TOUCHES_FCT + 15, LOGIQUE_AL);
  InsereGeoAl (ff17, 0, b_cour_log, AL_TOUCHES_FCT + 16, LOGIQUE_AL);
  InsereGeoAl (ff18, 0, b_cour_log, AL_TOUCHES_FCT + 17, LOGIQUE_AL);
  InsereGeoAl (ff19, 0, b_cour_log, AL_TOUCHES_FCT + 18, LOGIQUE_AL);
  InsereGeoAl (ff20, 0, b_cour_log, AL_TOUCHES_FCT + 19, LOGIQUE_AL);
  }

// --------------------------------------------------------------------------
//
static void OrganiseGeoAl (void)
  {
  DWORD wIndex;

  cree_bloc (bx_geo_al, sizeof (tx_geo_al), AL_NB_VAR_GEO_AL);
  InsereGeoAl (al_surv_glob, 0, b_cour_log, AL_SURV_GLOB, LOGIQUE_AL);
  InsereGeoAl (al_visu_glob, 0, b_cour_log, AL_VISU_GLOB, LOGIQUE_AL);
  InsereGeoAl (al_arch_glob, 0, b_cour_log, AL_ARCH_GLOB, LOGIQUE_AL);
  InsereGeoAl (al_impr_glob, 0, b_cour_log, AL_IMPR_GLOB, LOGIQUE_AL);
  InsereGeoAl (al_cptr_glob, 0, b_cour_num, AL_CPTR_GLOB, NUMERIQUE_AL);
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_surv_grp, wIndex, b_cour_log, AL_SURV_GRP + wIndex, LOGIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_visu_grp, wIndex, b_cour_log, AL_VISU_GRP + wIndex , LOGIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_arch_grp, wIndex, b_cour_log, AL_ARCH_GRP + wIndex, LOGIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_impr_grp, wIndex, b_cour_log, AL_IMPR_GRP + wIndex, LOGIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_cptr_grp, wIndex, b_cour_num, AL_CPTR_GRP + wIndex, NUMERIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_surv_pri, wIndex, b_cour_num, AL_SURV_PRI + wIndex, NUMERIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_visu_pri, wIndex, b_cour_num, AL_VISU_PRI + wIndex, NUMERIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_arch_pri, wIndex, b_cour_num, AL_ARCH_PRI + wIndex, NUMERIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_impr_pri, wIndex, b_cour_num, AL_IMPR_PRI + wIndex, NUMERIQUE_AL);
    }
  for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
    {
    InsereGeoAl (al_visu_active, wIndex, b_cour_log, AL_VISU_ACTIVE + wIndex, LOGIQUE_AL);
    }
  InsereGeoAl (al_priorite_unique, 0, b_cour_log, AL_PRIORITE_UNIQUE, LOGIQUE_AL);
  InsereGeoAl (status_al, 0, b_va_systeme, AL_STATUT, NUMERIQUE_AL);
  InsereGeoAl (al_visible, 0, b_cour_log, AL_VISIBLE, LOGIQUE_AL);
  InsereTouches ();
  InsereGeoAl (c_sys_al_type_visu, 0, b_cour_num, AL_TYPE_VISU, NUMERIQUE_AL);
  }

// --------------------------------------------------------------------------
//
static void MiseEnOrdrePriorite (void)
  {
  tx_groupe_al* pGroupeAl;
  tx_priorite_al*pPrioriteAl;
  tx_priorite_al  TabPriorite[NB_GROUPE_MAX_AL];
  DWORD            wIndexGroupe;
  DWORD            wIndexPriorite;
  DWORD            wPointeur;
  DWORD            wNbrEnr;
  DWORD            wIndex;

  if (existe_repere (bx_priorite_al))
    {
    wIndexGroupe = 0;
    wNbrEnr = nb_enregistrements (szVERIFSource, __LINE__, bx_groupe_al);
    while (wIndexGroupe < wNbrEnr)
      {
      // initialisation de tab a zero
      wIndexGroupe++;
      for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
        {
        TabPriorite[wIndex].wNumeroPriorite = 0;
        }
      pGroupeAl = (tx_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_groupe_al, wIndexGroupe);

      // remise en ordre dans tab
      wIndexPriorite = 0;
      while (wIndexPriorite < pGroupeAl->wNbrePriorite)
        {
        wIndexPriorite++;
        pPrioriteAl = (tx_priorite_al*)pointe_enr (szVERIFSource, __LINE__, bx_priorite_al, pGroupeAl->wPosPremPriorite + wIndexPriorite - 1);
        TabPriorite [pPrioriteAl->wNumeroPriorite - 1] = (*pPrioriteAl);
        }

      // remise en ordre dans bx_priorite
      wPointeur = pGroupeAl->wPosPremPriorite;
      for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
        {
        if (TabPriorite[wIndex].wNumeroPriorite != 0)
          { // modifier dans bx_priorite
          pPrioriteAl = (tx_priorite_al*)pointe_enr (szVERIFSource, __LINE__, bx_priorite_al, wPointeur);
          (*pPrioriteAl) = TabPriorite[wIndex];
          wPointeur++;
          }
        }
      } // boucle sur bx_groupe
    } // existe repere bx_priorite
  }

// ------------------------ Cr�ation du bloc bx_services_speciaux_al
//
static void OrganiseServicesSpeciaux (void)
  {
  tx_services_speciaux_al* pServSpec;

  if (!existe_repere (bx_services_speciaux_al))
    {
    cree_bloc (bx_services_speciaux_al, sizeof (tx_services_speciaux_al), NBR_SRV_SPEC_AL);

    pServSpec = (tx_services_speciaux_al*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_al, PTR_SRV_SPEC_AL_ACK);
    pServSpec->handle_al = FabricationHandleAl (ANM_AL_SRV_ACK, 0);

    pServSpec = (tx_services_speciaux_al*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_al, PTR_SRV_SPEC_AL_FIN);
    pServSpec->handle_al = FabricationHandleAl (ANM_AL_SRV_FIN, 0);

    pServSpec = (tx_services_speciaux_al*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_al, PTR_SRV_SPEC_AL_RAZ);
    pServSpec->handle_al = FabricationHandleAl (ANM_AL_SRV_RAZ, 0);
    }
  }


// --------------------------------------------------------------------------
// Debut de la Generation
// --------------------------------------------------------------------------
DWORD genere_al (DWORD *wLigne)
  {
  tx_glob_al*	pGlobAl;
  PGEO_ALARME               pGeoAl;
  GEO_ALARME                   GeoAl;
  PTITRE_PARAGRAPHE					pTitreParagraf;
  PSPEC_TITRE_PARAGRAPHE_ALARME  pSpecTitreParagraf;
  PC_ALARME                 pAlarme;
  tx_groupe_al              *pGroupeAl;
  tx_priorite_al            *pPrioriteAl;
  tx_element_al* pElementAl;
  tx_variable_al*pVariableAl;
  DWORD                       wErreur;
  DWORD                       wLigneCour;
  DWORD                       wNbreAlMax;
  char                       pszNomFic[42];
  char                       pszNomGroupe[14];
  DWORD                      wTailleFicMax;
  BOOL                    bTestPriorite[NB_GROUPE_MAX_AL];
  DWORD                       wNumeroGroupe;
  DWORD                       wNumeroPriorite;
  DWORD                       wNbrePriorite;
  BOOL                    bPremierePriorite;
  DWORD                       wNbreAlarme;
  DWORD                       wNbreVariable;
  DWORD                       wNbreConstante;
  t_valeur_al                Valeur;
  DWORD                       wIndex;
  int                        iFenAlX;
  int                        iFenAlY;
  int                        iFenAlCx;
  int                        iFenAlCy;
  DWORD                       wTypeHoroDatage;
  LONG                        lTempoStabiliteEvt;
  DWORD                       wOffsetHoroDatage;

  wErreur = 0;
  OrganiseGeoAl ();
  if (existe_repere (b_geo_al) && (nb_enregistrements (szVERIFSource, __LINE__, b_geo_al) != 0))
    {
    wLigneCour = 0;
    while ((wLigneCour < nb_enregistrements (szVERIFSource, __LINE__, b_geo_al)) && (wErreur == 0))
      {
      wLigneCour++;
      pGeoAl = (PGEO_ALARME)pointe_enr (szVERIFSource, __LINE__, b_geo_al, wLigneCour);
      GeoAl = (*pGeoAl);
      switch (GeoAl.numero_repere)
        {
        case b_pt_constant :
          supprime_cst_bd_c (GeoAl.pos_donnees);
          break;

				case b_titre_paragraf_al :
					pTitreParagraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_al, GeoAl.pos_paragraf);
          switch (pTitreParagraf->IdParagraphe)
            {
						case surveillance_ala :
							if (!existe_repere (bx_glob_al))
                {
								cree_bloc (bx_glob_al, sizeof (tx_glob_al), AL_NB_VAR_GLOB_AL);
                }
							pGlobAl = (tx_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_glob_al, AL_SURV_GLOB);
							pSpecTitreParagraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, GeoAl.pos_specif_paragraf);
              pGlobAl->bValInit = pSpecTitreParagraf->val_init_surv;
              iFenAlX  = pSpecTitreParagraf->fen_al_x;
              iFenAlY  = pSpecTitreParagraf->fen_al_y;
              iFenAlCx = pSpecTitreParagraf->fen_al_cx;
              iFenAlCy = pSpecTitreParagraf->fen_al_cy;
              wTypeHoroDatage = pSpecTitreParagraf->type_horodatage;
              if (pSpecTitreParagraf->tempo_stabilite_evenement != 0)
                {
                // ancien horodatage lTempoStabiliteEvt = pSpecTitreParagraf->tempo_stabilite_evenement / 10;
								// Autorisation fen�tres / barres d'outil
                lTempoStabiliteEvt = pSpecTitreParagraf->tempo_stabilite_evenement;
                }
              break;

						case visualisation_ala :
							pGlobAl = (tx_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_glob_al, AL_VISU_GLOB);
							pSpecTitreParagraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, GeoAl.pos_specif_paragraf);
              pGlobAl->bValInit = pSpecTitreParagraf->val_init_vis;
              wNbreAlMax = pSpecTitreParagraf->nbre_al_maxi;
              break;

						case archivage_ala :
							pGlobAl = (tx_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_glob_al, AL_ARCH_GLOB);
							pSpecTitreParagraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, GeoAl.pos_specif_paragraf);
              pGlobAl->bValInit = pSpecTitreParagraf->val_init_arch;
              StrCopy (pszNomFic, pSpecTitreParagraf->nom_fichier_alarme);
              wTailleFicMax = pSpecTitreParagraf->taille_fic_max;
              break;

						case impression_ala :
							pGlobAl = (tx_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_glob_al, AL_IMPR_GLOB);
							pSpecTitreParagraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, GeoAl.pos_specif_paragraf);
              pGlobAl->bValInit = pSpecTitreParagraf->val_init_imp;
							wNumeroGroupe = 0;
              AnimGlobAl (pszNomFic, wNbreAlMax, wTailleFicMax,
                          iFenAlX, iFenAlY, iFenAlCx, iFenAlCy,
                          wTypeHoroDatage, lTempoStabiliteEvt);
              break;

						case groupe_ala :
							if (wNumeroGroupe > 0)
                { // pas premier groupe
                }
							else
                { // premier groupe
								if (!existe_repere (bx_groupe_al))
                  {
									cree_bloc (bx_groupe_al, sizeof (tx_groupe_al), 0);
                  }
                }
              wNumeroGroupe++;
							insere_enr (szVERIFSource, __LINE__, 1, bx_groupe_al, nb_enregistrements (szVERIFSource, __LINE__, bx_groupe_al) + 1);
							pGroupeAl = (tx_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_groupe_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_groupe_al));
							pSpecTitreParagraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, GeoAl.pos_specif_paragraf);
							pGroupeAl->wPosPremPriorite = 0;
							pGroupeAl->wNbrePriorite = 0;

              StrCopy (pszNomGroupe, pSpecTitreParagraf->nom_groupe);
              AnimGroupeAl (pszNomGroupe, pSpecTitreParagraf->coul_al_pres_non_ack,
                            pSpecTitreParagraf->coul_al_pres_ack, pSpecTitreParagraf->coul_al_non_pres_non_ack,
                            pSpecTitreParagraf->type_ack);

							bPremierePriorite = TRUE;
							wNbrePriorite = 0;
							// initialise tab_test a bas (N� priorite pas utilis�)
							for (wIndex = 0; wIndex < NB_GROUPE_MAX_AL; wIndex++)
                {
								bTestPriorite[wIndex] = FALSE;
                }
              break;

						case priorite_ala :
							if (!bPremierePriorite) // maj nb_alarmes ds la priorite precedente
                {
								insere_enr (szVERIFSource, __LINE__, 1, bx_priorite_al, nb_enregistrements (szVERIFSource, __LINE__, bx_priorite_al) + 1);
                }
							else
                { // premiere priorite
								if (!existe_repere (bx_priorite_al))
                  {
									cree_bloc (bx_priorite_al, sizeof (tx_priorite_al), 0);
                  }
								insere_enr (szVERIFSource, __LINE__, 1, bx_priorite_al, nb_enregistrements (szVERIFSource, __LINE__, bx_priorite_al) + 1);
								// met numero de premiere priorite dans groupe
								pGroupeAl = (tx_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_groupe_al, wNumeroGroupe);
								pGroupeAl->wPosPremPriorite = nb_enregistrements (szVERIFSource, __LINE__, bx_priorite_al);
								bPremierePriorite = FALSE;
                }
							pSpecTitreParagraf = (PSPEC_TITRE_PARAGRAPHE_ALARME)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_al, GeoAl.pos_specif_paragraf);
							pPrioriteAl = (tx_priorite_al*)pointe_enr (szVERIFSource, __LINE__, bx_priorite_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_priorite_al));
							pPrioriteAl->wNumeroPriorite = pSpecTitreParagraf->numero_priorite;
							wNumeroPriorite = pSpecTitreParagraf->numero_priorite;
							wNbrePriorite++;
              pGroupeAl = (tx_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_groupe_al, wNumeroGroupe);
							pGroupeAl->wNbrePriorite = wNbrePriorite;
							pPrioriteAl->wPosPremAlarme = 0; // initialisations
							pPrioriteAl->wNbreAlarme = 0;    // initialisations
							if (!bTestPriorite [pPrioriteAl->wNumeroPriorite - 1])
                { // on met bTestPriorite correspondant a vrai (Niveau de priorite utilise)
								bTestPriorite [pPrioriteAl->wNumeroPriorite - 1] = TRUE;
                }
							else
                {
								wErreur = 222; // plusieurs priorites de meme niveau ds le meme groupe
                }
							wNbreAlarme = 0;
              break;

            default :
              break;
            } // paragraph
          break;

				case b_alarme :
					if (wNbreAlarme == 0) // maj pteur debut alarme sur bx_element_al
            {
						if (!existe_repere (bx_variable_al))
              {
              cree_bloc (bx_variable_al, sizeof (tx_variable_al), 0);
              }
						if (!existe_repere (bx_element_al))
              {
              cree_bloc (bx_element_al, sizeof (tx_element_al), 0);
              }
						insere_enr (szVERIFSource, __LINE__, 1, bx_element_al, nb_enregistrements (szVERIFSource, __LINE__, bx_element_al) + 1);
						pPrioriteAl = (tx_priorite_al*)pointe_enr (szVERIFSource, __LINE__, bx_priorite_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_priorite_al));
						pPrioriteAl->wPosPremAlarme = nb_enregistrements (szVERIFSource, __LINE__,  bx_element_al);
            }
					else
            {
						insere_enr (szVERIFSource, __LINE__, 1, bx_element_al, nb_enregistrements (szVERIFSource, __LINE__, bx_element_al) + 1);
            }
          wNbreVariable = 1; // tjrs au moins une variable
          wNbreConstante = 0;

          insere_enr (szVERIFSource, __LINE__, 1, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_variable_al) + 1);
					pElementAl = (tx_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_element_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_element_al));
					pElementAl->wPosPremVariable = nb_enregistrements (szVERIFSource, __LINE__, bx_variable_al);
					wNbreAlarme++;
					pPrioriteAl = (tx_priorite_al*)pointe_enr (szVERIFSource, __LINE__, bx_priorite_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_priorite_al));
					pPrioriteAl->wNbreAlarme = wNbreAlarme;

					pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
					pVariableAl = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_variable_al));
					switch (pAlarme->test_alarme)
            {
            case c_res_change:
            case c_res_stable:
              if (pAlarme->var_logique)
                {
                if (wTypeHoroDatage == HORODATAGE_PC)
                  {
   								if (pAlarme->indice_tableau == 0)
                    { // variable simple
  									wErreur = recherche_index_execution (pAlarme->pos_var, &pVariableAl->wPosEs);
                    }
  								else
  									{ // variable tableau
  									wErreur = recherche_index_tableau_i (pAlarme->pos_var,
                              pAlarme->indice_tableau, &pVariableAl->wPosEs);
                    }
                  pVariableAl->wBlocGenre = b_cour_log;
                  pVariableAl->bTestStable = (BOOL)(pAlarme->test_alarme == c_res_stable);
                  AnimVariableAl (LOGIQUE_AL, VARIABLE_AL, Valeur);
                  }
                else
                  {
                  wErreur = 447;
                  }
                }
              else
                { // numerique
								if (pAlarme->indice_tableau == 0)
                  { // variable simple
                  if (wTypeHoroDatage == HORODATAGE_PC)
                    {
      	            wErreur = recherche_index_execution (pAlarme->pos_var, &pVariableAl->wPosEs);
                    pVariableAl->wPosEs = pVariableAl->wPosEs - ContexteGen.nbr_logique;
                    pVariableAl->wBlocGenre = b_cour_num;
                    pVariableAl->bTestStable = (BOOL)(pAlarme->test_alarme == c_res_stable);
                    AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);
                    }
                  else
                    {
                    wErreur = 448;
                    }
                  }
								else
									{ // variable tableau
                  if (wTypeHoroDatage == HORODATAGE_PC)
                    {
 										wErreur = recherche_index_tableau_i (pAlarme->pos_var,
                              pAlarme->indice_tableau, &pVariableAl->wPosEs);
                    pVariableAl->wBlocGenre = b_cour_num;
                    pVariableAl->bTestStable = (BOOL)(pAlarme->test_alarme == c_res_stable);
                    AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);
                    }
                  else
                    {
 										wErreur = recherche_index_tableau_i (pAlarme->pos_var,
                              pAlarme->indice_tableau, &pVariableAl->wPosEs);
                    pVariableAl->wBlocGenre = b_cour_num;
                    pVariableAl->bTestStable = (BOOL)(pAlarme->test_alarme == c_res_stable);
                    AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);
                    }
                  }
                }
              break;

            case c_res_zero:
            case c_res_un:
              if (wTypeHoroDatage == HORODATAGE_PC)
                {
                if (pAlarme->var_logique)
                  {
  								if (pAlarme->indice_tableau == 0)
                    { // variable simple
  									wErreur = recherche_index_execution (pAlarme->pos_var, &pVariableAl->wPosEs);
                    }
  	          else
  	            { // variable tableau
  	            wErreur = recherche_index_tableau_i (pAlarme->pos_var,
                              pAlarme->indice_tableau, &pVariableAl->wPosEs);
                    }
                  pVariableAl->wBlocGenre = b_cour_log;
                  pVariableAl->bTestStable = FALSE;
                  AnimVariableAl (LOGIQUE_AL, VARIABLE_AL, Valeur);
                  }
                }
              else
                {
                wErreur = 447;
                }
              break;

            case c_res_egal:
            case c_res_different:
            case c_res_superieur:
            case c_res_inferieur:
            case c_res_superieur_egal:
            case c_res_inferieur_egal:
            case c_res_passe_valeur:
            case c_res_passe_valeur_vers_h:
            case c_res_passe_valeur_vers_b:
            case c_res_prend_valeur:
            case c_res_prend_valeur_vers_h:
            case c_res_prend_valeur_vers_b:
            case c_res_quitte_valeur:
            case c_res_quitte_valeur_vers_h:
            case c_res_quitte_valeur_vers_b:
            case c_res_testbit_h:
            case c_res_testbit_b:
              if (!pAlarme->var_logique)
                { // numerique
								if (pAlarme->indice_tableau == 0)
                  { // variable simple
                  if (wTypeHoroDatage == HORODATAGE_PC)
                    {
  									wErreur = recherche_index_execution (pAlarme->pos_var, &pVariableAl->wPosEs);
                    pVariableAl->wPosEs = pVariableAl->wPosEs - ContexteGen.nbr_logique;
                    }
                  else
                    {
                    wErreur = 448;
                    }
                  }
								else
									{ // variable tableau
									wErreur = recherche_index_tableau_i (pAlarme->pos_var,
                            pAlarme->indice_tableau, &pVariableAl->wPosEs);
                  }
                pVariableAl->wBlocGenre = b_cour_num;
                pVariableAl->bTestStable = FALSE;
                AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);

								pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
                if (pAlarme->une_variable1)
                  {
                  wNbreVariable++;
                  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_variable_al) + 1);
									pVariableAl = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_variable_al));
									pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
									wErreur = recherche_index_execution (pAlarme->pos_var1, &pVariableAl->wPosEs);
                  pVariableAl->wPosEs = pVariableAl->wPosEs - ContexteGen.nbr_logique;
                  pVariableAl->wBlocGenre = b_cour_num;
                  pVariableAl->bTestStable = FALSE;
                  AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);
                  }
                else
                  { // constante
                  wNbreConstante++;
                  Valeur.rValNumerique = pAlarme->cste1_alarme;
                  AnimVariableAl (NUMERIQUE_AL, CONSTANTE_AL, Valeur);
                  }
                }
              break;

            case c_res_entre_alarme:
            case c_res_pavhd:
            case c_res_pavbd:
              if (!pAlarme->var_logique)
                { // numerique
								if (pAlarme->indice_tableau == 0)
                  { // variable simple
                  if (wTypeHoroDatage == HORODATAGE_PC)
                    {
										wErreur = recherche_index_execution (pAlarme->pos_var, &pVariableAl->wPosEs);
                    pVariableAl->wPosEs = pVariableAl->wPosEs - ContexteGen.nbr_logique;
                    }
                  else
                    {
                    wErreur = 448;
                    }
                  }
								else
									{ // variable tableau
									wErreur = recherche_index_tableau_i (pAlarme->pos_var,
                            pAlarme->indice_tableau, &pVariableAl->wPosEs);
                  }
                pVariableAl->wBlocGenre = b_cour_num;
                pVariableAl->bTestStable = FALSE;
                AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);

								pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
                if (pAlarme->une_variable1)
                  {
                  wNbreVariable++;
                  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_variable_al) + 1);
									pVariableAl = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_variable_al));
									pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
									wErreur = recherche_index_execution (pAlarme->pos_var1, &pVariableAl->wPosEs);
                  pVariableAl->wPosEs = pVariableAl->wPosEs - ContexteGen.nbr_logique;
                  pVariableAl->wBlocGenre = b_cour_num;
                  pVariableAl->bTestStable = FALSE;
                  AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);
                  }
                else
                  { // constante
                  wNbreConstante++;
                  Valeur.rValNumerique = pAlarme->cste1_alarme;
                  AnimVariableAl (NUMERIQUE_AL, CONSTANTE_AL, Valeur);
                  }

								pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
                if (pAlarme->une_variable2)
                  {
                  wNbreVariable++;
                  insere_enr (szVERIFSource, __LINE__, 1, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_variable_al) + 1);
									pVariableAl = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_variable_al));
									pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
									wErreur = recherche_index_execution (pAlarme->pos_var2, &pVariableAl->wPosEs);
                  pVariableAl->wPosEs = pVariableAl->wPosEs - ContexteGen.nbr_logique;
                  pVariableAl->wBlocGenre = b_cour_num;
                  pVariableAl->bTestStable = FALSE;
                  AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);
                  }
                else
                  { // constante
                  wNbreConstante++;
                  Valeur.rValNumerique = pAlarme->cste2_alarme;
                  AnimVariableAl (NUMERIQUE_AL, CONSTANTE_AL, Valeur);
                  }
                }
              break;

            default:
              break;
            } // switch

          if (wErreur == 0)
            {
   					pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
            if (pAlarme->pos_message != 0)
              { // il y a une variable message
              wNbreVariable++;
              insere_enr (szVERIFSource, __LINE__, 1, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_variable_al) + 1);
  						pVariableAl = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_variable_al));
  						pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
  						wErreur = recherche_index_execution (pAlarme->pos_message, &pVariableAl->wPosEs);
              pVariableAl->wPosEs = pVariableAl->wPosEs - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
              pVariableAl->wBlocGenre = b_cour_mes;
              pVariableAl->bTestStable = FALSE;
              AnimVariableAl (MESSAGE_AL, VARIABLE_AL, Valeur);
              }

            // Attention : Il faut toujours que les variables d'horodatage
            // soient les dernieres de l'ensemble
            if (wTypeHoroDatage == HORODATAGE_SOURCE)
              {
              for (wOffsetHoroDatage = 1; (wOffsetHoroDatage <= NB_VAL_HORODATAGE); wOffsetHoroDatage++)
                {
                wNbreVariable++;
                insere_enr (szVERIFSource, __LINE__, 1, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_variable_al) + 1);
								pVariableAl = (tx_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_variable_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_variable_al));
								pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
								wErreur = recherche_index_tableau_i (pAlarme->pos_var,
                                                     pAlarme->indice_tableau + wOffsetHoroDatage,
                                                     &pVariableAl->wPosEs);
                pVariableAl->wBlocGenre = VA_HORODATAGE_AL;
                pVariableAl->bTestStable = FALSE;
                AnimVariableAl (NUMERIQUE_AL, VARIABLE_AL, Valeur);
                }
              }
  					pElementAl = (tx_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_element_al, nb_enregistrements (szVERIFSource, __LINE__,  bx_element_al));
            pElementAl->wNbreVariable = wNbreVariable;
  					pAlarme = (PC_ALARME)pointe_enr (szVERIFSource, __LINE__, b_alarme, GeoAl.pos_donnees);
            pElementAl->handle_al = AnimElementAl (pAlarme->test_alarme,
                                                pAlarme->numero_enregistrement,
                                                wNumeroGroupe,
                                                wNumeroPriorite,
                                                wNbreVariable + wNbreConstante);
            }
						break; // fin case b_alarme

				default :
          //otherwise abort ('tipe paragraf al invalide', 0, 0);
          Beep(1000, 1000);
        } // fin switch
      } // while
    } // existe geo_al et nb_enr != 0

  if (wErreur == 0)
    {
    OrganiseServicesSpeciaux ();
    MiseEnOrdrePriorite ();
    }
  else
    {
    (*wLigne) = wLigneCour;
    }

  if (wErreur == 0)
    {
    if (existe_repere (b_titre_paragraf_al))
      {
      enleve_bloc (b_titre_paragraf_al);
      }
    if (existe_repere (b_spec_titre_paragraf_al))
      {
      enleve_bloc (b_spec_titre_paragraf_al);
      }
    if (existe_repere (b_pt_libre))
      {
      enleve_bloc (b_pt_libre);
      }
    if (existe_repere (b_alarme))
      {
      enleve_bloc (b_alarme);
      }
    } // wErreur == 0

  return (wErreur);
  }

/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_TB.H                                               |
 |                                                                      |
 |   Auteur  : RP                                                       |
 |   Date    : 09/09/94                                                 |
 |   Version :                                                          |
 |                    Auto Bonne Moyenne Impossible                     |
 |   Portabilite :  x                                                   |
 |                                                                      |
 |   Mise a jours:                                                      |
 |                                                                      |
 |   +--------+--------+---+--------------------------------------+     |
 |   | date   | qui    |no | raisons                              |     |
 |   +--------+--------+---+--------------------------------------+     |
 |   |        |        |   |                                      |     |
 |   +--------+--------+---+--------------------------------------+     |
 |                                                                      |
 |   Remarques :                                                        |
 |                                                                      |
 +----------------------------------------------------------------------*/

void genere_tableau (DWORD position_es, DWORD position_cst);

void maj_index_tableau (void);

void initialisation_tableau (void);

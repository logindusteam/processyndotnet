#pragma once

class CSignatureFichier
{
public:
	CSignatureFichier(void);
	CSignatureFichier(PCSTR pszSignatureCourante);
	~CSignatureFichier(void);
public:
private:
	// Signature
	char mszSignatureCourante [MAX_PATH];
public:
	void SetSignatureCourante(PCSTR pszSignatureCourante);
	BOOL bSignatureValide(PCSTR pszSignatureAVerifier);
	BOOL bDBPCSSignatureValide();
	void DBPCSSetSignature();
};

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_TB.C                                               |
 |   Auteur  : RP                                                       |
 |   Date    : 09/09/94                                                 |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_tb.h"
#include "mem.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "findbdex.h"
#include "UStr.h"
#include "GenereBD.h"

#include "gener_tb.h"
#include "Verif.h"
VerifInit;

#define TailleMessage 82


void genere_tableau (DWORD position_es, DWORD position_cst)
  {
  PTABLEAU				PtrTab;
  PX_TABLEAU			PtrXTab;

	// Pour chaque tableau dans les ES, un enr TABLEAU est cr�� pour initialisation ult�rieur
  if (!existe_repere (b_tableau))
    {
    cree_bloc(b_tableau,sizeof(*PtrTab),0);
    cree_bloc(bx_tableau,sizeof(*PtrXTab),0);
    }
  DWORD nb_tab = nb_enregistrements (szVERIFSource, __LINE__, b_tableau);
  nb_tab ++;
  insere_enr (szVERIFSource, __LINE__, 1,b_tableau,nb_tab);
  insere_enr (szVERIFSource, __LINE__, 1,bx_tableau,nb_tab);
  PtrTab = (PTABLEAU)pointe_enr (szVERIFSource, __LINE__, b_tableau,nb_tab);
  PtrTab->pos_es = position_es;
  PtrTab->pos_cst = position_cst;
  PtrXTab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,nb_tab);
  PENTREE_SORTIE	PtrEs = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
  PtrXTab->taille = PtrEs->taille;
  }

// -------------------------------------------------------------------------
void  maj_index_tableau (void)
  {
  DWORD            nb_log_tab;
  DWORD            nb_num_tab;
  DWORD            nb_mes_tab;
  DWORD            nb_tab;
  DWORD            taille_tab;
  DWORD            index;
  PTABLEAU				PtrTab;
  PX_TABLEAU      PtrXTab;
  PENTREE_SORTIE	es;

  if (existe_repere (b_tableau))
    {
    nb_tab = nb_enregistrements (szVERIFSource, __LINE__, b_tableau);
    nb_log_tab = ContexteGen.nbr_logique;
    nb_num_tab = ContexteGen.nbr_numerique;
    nb_mes_tab = ContexteGen.nbr_message;
    for (index = 1; index <= nb_tab; index++)
      {
      PtrTab = (PTABLEAU)pointe_enr (szVERIFSource, __LINE__, b_tableau,index);
      es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,PtrTab->pos_es);
      switch (es->genre)
        {
        case c_res_logique :
          PtrXTab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,index);
          PtrXTab->pos_x_es = nb_log_tab + 1;
          taille_tab = PtrXTab->taille;
          nb_log_tab = nb_log_tab + taille_tab;
          break;

        case c_res_numerique :
          PtrXTab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,index);
          PtrXTab->pos_x_es = nb_num_tab + 1;
          taille_tab = PtrXTab->taille;
          nb_num_tab = nb_num_tab + taille_tab;
          break;

        case c_res_message :
          PtrXTab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,index);
          PtrXTab->pos_x_es = nb_mes_tab + 1;
          taille_tab = PtrXTab->taille;
          nb_mes_tab = nb_mes_tab + taille_tab;
          break;

        default:
          break;
        }
      }
    }
  }

// -------------------------------------------------------------------------
void initialisation_tableau (void)
  {
  DWORD erreur = 0;
	// Existe t'il des tableaux ?
  if (existe_repere (b_tableau))
    {
		// oui : initialise les
		DWORD nb_enr = nb_enregistrements (szVERIFSource, __LINE__, b_tableau);
		DWORD nb_log = nb_enregistrements (szVERIFSource, __LINE__, b_cour_log);
		DWORD nb_num = nb_enregistrements (szVERIFSource, __LINE__, b_cour_num);
		DWORD nb_mes = nb_enregistrements (szVERIFSource, __LINE__, b_cour_mes);

		ID_MOT_RESERVE numero_fct;
		FLOAT valeur;
		UL ul;
		PBOOL cour_log;
		PBOOL mem_log;
		PFLOAT cour_num;
		PFLOAT mem_num;
		PSTR cour_mes; //ads of ts80;
		PSTR mem_mes; // ads of ts80;
		DWORD index;
		PX_E_S	pxES;
		PX_E_S_COUR pxESCour;

    for (DWORD index1 = 1; index1 <= nb_enr; index1 ++)
      {
   		PTABLEAU	PtrTab = (PTABLEAU)pointe_enr (szVERIFSource, __LINE__, b_tableau,index1);
   		TABLEAU donnees_tab = (*PtrTab);
   		PX_TABLEAU	PtrXTab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,index1);
   		DWORD taille_tab = PtrXTab->taille;
   		PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,donnees_tab.pos_es);
   		DWORD pos_init = donnees_tab.pos_cst;
			char valeur_initiale [82] ; // $$
      StrSetNull (valeur_initiale);
      action_cst_bd_c (CONSULTE_LIGNE,valeur_initiale,pos_init);
      action_cst_bd_c (SUPPRIME_LIGNE,valeur_initiale,pos_init);
      switch (es->genre)
        {
        case c_res_logique :
					{
          insere_enr (szVERIFSource, __LINE__, taille_tab,b_cour_log, nb_log+1);
          insere_enr (szVERIFSource, __LINE__, taille_tab,bva_mem_log, nb_log+1);

          insere_enr (szVERIFSource, __LINE__, taille_tab, bx_e_s_cour_log, nb_log+1);
          pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, es->i_identif);
          //--- pszNomVar deja initialise ds genere.pas
          pxES->wPosEx = nb_log+1;
          pxES->wBlocGenre = b_cour_log;
          pxES->wTaille = es->taille;

          for (index = nb_log + 1; index <=  nb_log + taille_tab; index ++)
            {
            cour_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log,index);
            mem_log = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log,index);

            StrCopy (ul.show,valeur_initiale);
            ul.genre = g_id;
            if (!bReconnaitMotReserve (ul.show,&numero_fct))
              {
              (*cour_log) = FALSE;
              (*mem_log) = FALSE;
              }
            switch (numero_fct)
              {
              case c_res_un :
                (*cour_log) = TRUE;
                (*mem_log) = TRUE;
                break;

              case c_res_zero :
                (*cour_log) = FALSE;
                (*mem_log) = FALSE;
                break;

              default:
                (*cour_log) = FALSE;
                (*mem_log) = FALSE;
                break;
              }
            pxESCour = (PX_E_S_COUR)pointe_enr (szVERIFSource, __LINE__, bx_e_s_cour_log, index);
            pxESCour->dwPosDansBx_e_s = es->i_identif;

            }
          nb_log = nb_log + taille_tab;
					}
          break; // case c_res_logique

        case c_res_numerique :
					{
          insere_enr (szVERIFSource, __LINE__, taille_tab,b_cour_num,nb_num+1);
          insere_enr (szVERIFSource, __LINE__, taille_tab,bva_mem_num,nb_num+1);

          insere_enr (szVERIFSource, __LINE__, taille_tab, bx_e_s_cour_num, nb_num+1);
          pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, es->i_identif);
          //--- pszNomVar deja initialise ds genere.pas
          pxES->wPosEx = nb_num+1;
          pxES->wBlocGenre = b_cour_num;
          pxES->wTaille = es->taille;

          for (index = nb_num + 1; index <= nb_num + taille_tab; index ++)
            {
            cour_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num,index);
            mem_num = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num,index);
            valeur = (FLOAT)0;
            StrToFLOAT (&valeur,valeur_initiale);
            (*cour_num) = valeur;
            (*mem_num) = valeur;

            pxESCour = (PX_E_S_COUR)pointe_enr (szVERIFSource, __LINE__, bx_e_s_cour_num, index);
            pxESCour->dwPosDansBx_e_s = es->i_identif;

            }
          nb_num = nb_num + taille_tab;
					}
          break; // case c_res_numerique

        case c_res_message :
					{
          insere_enr (szVERIFSource, __LINE__, taille_tab,b_cour_mes,nb_mes+1);
          insere_enr (szVERIFSource, __LINE__, taille_tab,bva_mem_mes,nb_mes+1);

          insere_enr (szVERIFSource, __LINE__, taille_tab, bx_e_s_cour_mes, nb_mes+1);
          pxES = (PX_E_S)pointe_enr (szVERIFSource, __LINE__, bx_e_s, es->i_identif);
          //--- pszNomVar deja initialise ds genere.pas
          pxES->wPosEx = nb_mes+1;
          pxES->wBlocGenre = b_cour_mes;
          pxES->wTaille = es->taille;

          for (index = nb_mes + 1; index <= nb_mes + taille_tab; index ++)
            {
            StrDeleteDoubleQuotation (valeur_initiale);
            cour_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, b_cour_mes,index);
            mem_mes = (PSTR)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes,index);
            StrCopy (cour_mes, valeur_initiale);
            StrCopy (mem_mes, valeur_initiale);

            pxESCour = (PX_E_S_COUR)pointe_enr (szVERIFSource, __LINE__, bx_e_s_cour_mes, index);
            pxESCour->dwPosDansBx_e_s = es->i_identif;

            }
          nb_mes = nb_mes + taille_tab;
					}
          break; // case c_res_message :
        }
      }
    } // if (existe_repere (b_tableau))
  } // initialisation_tableau


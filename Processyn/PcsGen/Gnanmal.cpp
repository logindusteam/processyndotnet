/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Auteur  : LM							|
 |   Date    : 12/08/93 						|
 |	G�n�ration de l'animateur d'alarme
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"           // Types Standards
#include "UStr.h"        // Chaines de Caract�res            Strxxx ()
#include "mem.h"           // Memory Manager
#include "driv_dq.h"
#include "lng_res.h"       // Mots Reserves pour tests alarmes
#include "UChrono.h"
#include "tipe_al.h"       // Types des Blocs
#include "srvanmal.h"      // Types Services Animateur Alarmes
#include "gnanmal.h"
#include "Verif.h"
VerifInit;

//-----------------------------------------------------------------------
// fabrication du handle alarmes
HANDLE_ANM_AL FabricationHandleAl (DWORD service, DWORD element)
  {
  HANDLE_ANM_AL handle_anm_al;

  handle_anm_al.Service = service;
  handle_anm_al.Element = element;
  return handle_anm_al;
  }

// -------------------------------------------------------------------------
//
HANDLE_ANM_AL AnimGeoAl (DWORD wGenreVarAl, t_valeur_al *Valeur, DWORD wPosGeoAl)
  {
  tx_anm_geo_al*pGeoAl;

  if (!existe_repere (bx_anm_geo_al))
    {
    cree_bloc (bx_anm_geo_al, sizeof (tx_anm_geo_al), 0);
    }
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_geo_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_geo_al) + 1);
  pGeoAl = (tx_anm_geo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_geo_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_geo_al));
  pGeoAl->wTypeValeur = wGenreVarAl;
  pGeoAl->Valeur = (*Valeur);
  pGeoAl->wPosRet = wPosGeoAl;

  return (FabricationHandleAl (ANM_AL_SRV_GEO, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_geo_al)));
  }

// -------------------------------------------------------------------------
//
void   AnimGlobAl(char *pszNomFic, DWORD wNbreAlMax, DWORD wTailleFicMax,
                  int iFenAlX, int iFenAlY, int iFenAlCx,int iFenAlCy,
                  DWORD wTypeHoroDatage, LONG lTempoStabiliteEvt)
  {
  tx_anm_glob_al*pGlobAl;

  enleve_bloc (bx_anm_glob_al);
  cree_bloc (bx_anm_glob_al, sizeof (tx_anm_glob_al), 1);
  pGlobAl = (tx_anm_glob_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_glob_al, 1);
  StrCopy (pGlobAl->pszNomFic, pszNomFic);
  pGlobAl->wNbreAlMax = wNbreAlMax;
  pGlobAl->wTailleFicMax = wTailleFicMax;
  pGlobAl->bImprimanteOuverte = FALSE;
  pGlobAl->iFenAlX  = iFenAlX;
  pGlobAl->iFenAlY  = iFenAlY;
  pGlobAl->iFenAlCx = iFenAlCx;
  pGlobAl->iFenAlCy = iFenAlCy;
  pGlobAl->bScroll  = TRUE;
  pGlobAl->wTypeHoroDatage = wTypeHoroDatage;
  pGlobAl->lTempoStabiliteEvt = lTempoStabiliteEvt;
  }

// -------------------------------------------------------------------------
//
void   AnimGroupeAl(char *pszNomGroupe, DWORD coul_al_pres_non_ack,
                    DWORD coul_al_pres_ack, DWORD coul_al_non_pres_non_ack,
                    DWORD type_ack)
  {
  tx_anm_groupe_al*pGroupeAl;

  if (!existe_repere (bx_anm_groupe_al))
    {
    cree_bloc (bx_anm_groupe_al, sizeof (tx_anm_groupe_al), 0);
    }
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_groupe_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al) + 1);
  pGroupeAl = (tx_anm_groupe_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_groupe_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_groupe_al));
  StrDelete (pszNomGroupe, 0, 1); // enleve les guillemets
  StrSetLength (pszNomGroupe, StrLength (pszNomGroupe) - 1);
  StrCopy (pGroupeAl->pszNomGroupe, pszNomGroupe);
  pGroupeAl->coul_al_pres_non_ack      = (coul_al_pres_non_ack & 0x00F0) >> 4;
  pGroupeAl->coul_al_pres_ack          = (coul_al_pres_ack & 0x00F0) >> 4;
  pGroupeAl->coul_al_non_pres_non_ack  = (coul_al_non_pres_non_ack & 0x00F0) >> 4;
  pGroupeAl->fcoul_al_pres_non_ack     = coul_al_pres_non_ack & 0x000F;
  pGroupeAl->fcoul_al_pres_ack         = coul_al_pres_ack & 0x000F;
  pGroupeAl->fcoul_al_non_pres_non_ack = coul_al_non_pres_non_ack & 0x000F;
  pGroupeAl->type_ack                  = type_ack;
  }

// -------------------------------------------------------------------------
//
void AnimVariableAl (DWORD wGenreVar, DWORD wTypeVar, t_valeur_al Valeur)
  {
  tx_anm_variable_al*pVariableAl;
  tx_anm_cour_log_al*pCourLogAl;
  tx_anm_cour_num_al*pCourNumAl;
  tx_anm_cour_mes_al*pCourMesAl;

  if (!existe_repere (bx_anm_variable_al))
    {
    cree_bloc (bx_anm_variable_al, sizeof (tx_anm_variable_al), 0);
    }
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_variable_al) + 1);

  if (!existe_repere (bx_anm_cour_log_al))
    {
    cree_bloc (bx_anm_cour_log_al, sizeof (tx_anm_cour_log_al), 0);
    }
  if (!existe_repere (bx_anm_cour_num_al))
    {
    cree_bloc (bx_anm_cour_num_al, sizeof (tx_anm_cour_num_al), 0);
    }
  if (!existe_repere (bx_anm_cour_mes_al))
    {
    cree_bloc (bx_anm_cour_mes_al, sizeof (tx_anm_cour_mes_al), 0);
    }

  switch (wGenreVar)
    {
    case LOGIQUE_AL :
      insere_enr (szVERIFSource, __LINE__, 1, bx_anm_cour_log_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_log_al) + 1);
      pCourLogAl = (tx_anm_cour_log_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_log_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_log_al));
      pCourLogAl->bValeur = Valeur.bValLogique;
      pVariableAl = (tx_anm_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_variable_al));
      pVariableAl->wBlocGenre = bx_anm_cour_log_al;
      pVariableAl->wPosEs = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_log_al);
      pVariableAl->wTypeVar = wTypeVar;
      break;

    case NUMERIQUE_AL :
      insere_enr (szVERIFSource, __LINE__, 1, bx_anm_cour_num_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_num_al) + 1);
      pCourNumAl = (tx_anm_cour_num_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_num_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_num_al));
      pCourNumAl->rValeur = Valeur.rValNumerique;
      pVariableAl = (tx_anm_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_variable_al));
      pVariableAl->wBlocGenre = bx_anm_cour_num_al;
      pVariableAl->wPosEs = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_num_al);
      pVariableAl->wTypeVar = wTypeVar;
      break;

    case MESSAGE_AL :
      insere_enr (szVERIFSource, __LINE__, 1, bx_anm_cour_mes_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_mes_al) + 1);
      pCourMesAl = (tx_anm_cour_mes_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_cour_mes_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_mes_al));
      StrCopy (pCourMesAl->pszValeur, Valeur.pszValMessage);
      pVariableAl = (tx_anm_variable_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_variable_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_variable_al));
      pVariableAl->wBlocGenre = bx_anm_cour_mes_al;
      pVariableAl->wPosEs = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_cour_mes_al);
      pVariableAl->wTypeVar = wTypeVar;
      break;

    default :
      break;
    }
  }

// -------------------------------------------------------------------------
//
static DWORD AnimAlarmeDuree (DWORD wPosEsElement)
  {
  tx_anm_tempo_al*pTempoAl;

  if (!existe_repere (bx_anm_tempo_al))
    {
    cree_bloc (bx_anm_tempo_al, sizeof (tx_anm_tempo_al), 0);
    }
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_tempo_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_tempo_al) + 1);
  pTempoAl = (tx_anm_tempo_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_tempo_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_tempo_al));
  pTempoAl->bTempoEnCour = FALSE;
  pTempoAl->wPosEsElement = wPosEsElement;

  return (nb_enregistrements (szVERIFSource, __LINE__, bx_anm_tempo_al));
  }

// -------------------------------------------------------------------------
static void QualifieTestAlarme (DWORD *wTypeTestAlarme, DWORD wTestAlarme)
  {
  switch (wTestAlarme)
    {
    case c_res_inferieur_egal:
    case c_res_superieur_egal:
    case c_res_inferieur:
    case c_res_superieur:
    case c_res_different:
    case c_res_egal:
    case c_res_zero:
    case c_res_un:
    case c_res_testbit_h:
    case c_res_testbit_b:
    case c_res_entre_alarme:
    case c_res_pavhd:
    case c_res_pavbd:
      (*wTypeTestAlarme) = TEST_AL_ETAT;
      break;
    case c_res_change:
    case c_res_stable:
    case c_res_passe_valeur:
    case c_res_passe_valeur_vers_h:
    case c_res_passe_valeur_vers_b:
    case c_res_prend_valeur:
    case c_res_prend_valeur_vers_h:
    case c_res_prend_valeur_vers_b:
    case c_res_quitte_valeur:
    case c_res_quitte_valeur_vers_h:
    case c_res_quitte_valeur_vers_b:
      (*wTypeTestAlarme) = TEST_AL_FRONT;
      break;
    default:
      break;
    }
  return;
  }
// -------------------------------------------------------------------------
//
HANDLE_ANM_AL AnimElementAl (DWORD wTestAlarme, DWORD wNumeroEnregistrement,
                      DWORD wNumeroGroupe, DWORD wNumeroPriorite,
                      DWORD wNbreVariable)
  {
  tx_anm_element_al*pElementAl;

  if (!existe_repere (bx_anm_element_al))
    {
    cree_bloc (bx_anm_element_al, sizeof (tx_anm_element_al), 0);
    }
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_element_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_element_al) + 1);
  pElementAl = (tx_anm_element_al*)pointe_enr (szVERIFSource, __LINE__, bx_anm_element_al, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_element_al));
  pElementAl->wTestAlarme = wTestAlarme;
  QualifieTestAlarme (&(pElementAl->wTypeTestAlarme), wTestAlarme);
  pElementAl->wNumeroEnregistrement = wNumeroEnregistrement;
  pElementAl->wNumeroGroupe = wNumeroGroupe;
  pElementAl->wNumeroPriorite = wNumeroPriorite;
  pElementAl->wPosPremVariable = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_variable_al) - wNbreVariable + 1;
  pElementAl->wNbreVariable = wNbreVariable;
  pElementAl->bEtatAcq = FALSE;
  pElementAl->bEtatPresent = FALSE;


  //----------- gestion des alarmes avec tests de dur�es
  //
  switch (wTestAlarme)
    {
    case c_res_pavhd:
    case c_res_pavbd:
      pElementAl->wPosTempo = AnimAlarmeDuree (nb_enregistrements (szVERIFSource, __LINE__,  bx_anm_element_al));
      break;

    default :
      pElementAl->wPosTempo = 0;
      break;
    } // fin switch

  return (FabricationHandleAl (ANM_AL_SRV_EVT, nb_enregistrements (szVERIFSource, __LINE__, bx_anm_element_al)));
  }

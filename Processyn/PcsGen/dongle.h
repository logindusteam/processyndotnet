/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : dongle.h                                                 |
 |   Auteur  : AC					        	|
 |   Date    : 16/6/93 					        	|
 |   Version : 4.00							|
 +----------------------------------------------------------------------*/


typedef struct
  {
  void (*fct_fermer)(void);
  DWORD modele;
  DWORD n_serie;
  DWORD produit;
  DWORD fonction;
  DWORD version;
  DWORD nb_heures_depannage;
  DWORD nb_tentatives;
	DWORD nb_variables;
	DWORD date_limite;
  DWORD n_serie_precedent;
  LONG  limitation_temps;
  BOOL  limitation_enchainement;
  DWORD limitation_memoire;
  MINUTERIE tempo_limitation_temps;
  MINUTERIE tempo_test;
  HSEM  semhdl;
  } t_dongle;

// --------------------------------------------------------------------------

BOOL InitDongle (DWORD gDevID);
DWORD ouvrir_dongle (void (*ptr_fermeture)(void));
void ecrit_tout_dongle (DWORD PasseOverwrite1, DWORD PasseOverwrite2);
BOOL ecrit_dongle (DWORD valeur, DWORD reg);
void encrypte_xpc (void);
void encrypte_hot (void);
void suit_cailloux (void);
BOOL limite_temps_ecoule (void);
void limiter_temps (void);
void limiter_memoire (void);
void limiter_enchainement (void);
void init_test_dongle (void);
void test_dongle (void);
void fermer_dongle (void);
void diagnostic_dongle (DWORD *n_fonction, DWORD *n_variable);
void programmer_dongle (DWORD n_saisie, DWORD n_fonction, DWORD n_variable);
void maj_nb_variables (void);
void limiter_nb_variables (void);
void compose_date_limite (DWORD *pdwDateLimite, DWORD dwNumJour, DWORD dwNumMois, DWORD dwNumAnnee);
void decompose_date_limite (DWORD dwDateLimite, DWORD *pdwNumJour, DWORD *pdwNumMois, DWORD *pdwNumAnnee);
void spirale_n (int *i, CHAR *nom);
t_dongle *get_dongle(void);
DWORD lit_nb_variables (void);

 /*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : FINDBDEX.C                                               |
 |   Auteur  : RP                                                       |
 |   Date    : 12/08/94                                                 |
 |   Version : 4.05                                                     |
 +----------------------------------------------------------------------*/
//  implementation gestion de la base de donnees version 2
//  ------------------------------------------------------
#include "stdafx.h"
#include "col.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_me.h"
#include "tipe_tb.h"
#include "Driv_dq.h"
#include "tipe_dq.h"
#include "mem.h"
#include "UChrono.h"
#include "tipe_tp.h"
#include "tipe_de.h"
#include "tipe_re.h"
#include "tipe_sy.h"
#include "G_Objets.h"
#include "G_sys.h"
#include "Descripteur.h"
#include "BdGr.h"
#include "BdAnmGr.h"
#include "tipe_gr.h"
#include "Verif.h"

#ifdef c_modbus
  #include "tipe_jb.h"
#endif

#ifdef c_ap
  #include "tipe_ap.h"
#endif

#ifdef c_opc
	#include "opc.h"
	#include "OPCError.h"
	#include "TemplateArray.h"
	#include "OPCClient.h"
  #include "tipe_opc.h"
#endif

#include "findbdex.h"
VerifInit;

// --------------------------------------------------------------------------
// renvoie la position de l'index dans la table d'organisation des donnees
// a partir de la position dans la table ES de la base de donnees.
// ATTENTION la position renvoyee ne tient pas compte de l'eclatement en
// 3 tables (log,num,mes) au moment de l'execution:faire la mise a l'echelle
// au retour a l'aide des variables valeurs limites
UINT recherche_index_execution
	(
	DWORD pos_es_descript,
	DWORD * pos_es_exec
	) // renvoie 0 si trouv�, une erreur PCS sinon
  {
  UINT wRetour;
  DWORD index = 0;
  BOOL trouve = FALSE;
  DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_organise_es);

  while ((index < nbr_enr) && (!trouve))
    {
    index ++;
    PORGANISATION_ES organe = (PORGANISATION_ES)pointe_enr (szVERIFSource, __LINE__, b_organise_es,index);
    switch (organe->driver)
      {
      case d_systeme :
				{
			  PGEO_SYST val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst,organe->pos_geo);
        trouve = (val_geo_syst->pos_es == pos_es_descript);
				}
        break;

      case d_chrono :
				{
			  PGEO_CHRONO val_geo_chrono = (PGEO_CHRONO)pointe_enr (szVERIFSource, __LINE__, b_geo_chrono,organe->pos_geo);
        trouve = (val_geo_chrono->pos_es == pos_es_descript);
				}
        break;

      case d_mem :
				{
			  PGEO_MEM	val_geo_mem = (PGEO_MEM)pointe_enr (szVERIFSource, __LINE__, b_geo_mem,organe->pos_geo);
        trouve = (val_geo_mem->position_es == pos_es_descript);
				}
        break;

      case d_dde :
				{
			  PGEO_DDE val_geo_dde = (PGEO_DDE)pointe_enr (szVERIFSource, __LINE__, b_geo_dde, organe->pos_geo);
        trouve = (val_geo_dde->wPosEs == pos_es_descript);
				}
        break;

      case d_jbus :
				{
#ifdef c_modbus
		    PGEO_MODBUS	val_geo_jbus = (PGEO_MODBUS)pointe_enr (szVERIFSource, __LINE__, b_geo_jbus,organe->pos_geo);
        trouve = (val_geo_jbus->pos_es == pos_es_descript);
#endif
				}
        break;

      case d_ap :
				{
#ifdef c_ap
				PGEO_APPLICOM val_geo_ap = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap,organe->pos_geo);
				trouve = (val_geo_ap->pos_es == pos_es_descript);
#endif
				}
        break;

      case d_repete :
				{
			  PGEO_REPETE val_geo_re = (PGEO_REPETE)pointe_enr (szVERIFSource, __LINE__, b_geo_re,organe->pos_geo);
				trouve = (val_geo_re->pos_es == pos_es_descript);
				}
        break;

      case d_graf :
				{
			  PGEO_VDI val_geo_vdi = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,organe->pos_geo);
				trouve = (val_geo_vdi->pos_es == pos_es_descript);
				}
        break;

      case d_disq :
				{
			  PGEO_DISQUE val_geo_disq = (PGEO_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_geo_disq,organe->pos_geo);
        switch (val_geo_disq->numero_repere)
          {
          case b_e_s :
            trouve = (val_geo_disq->pos_es == pos_es_descript);
            break;

          case b_titre_paragraf_dq:
						{
						PSPEC_TITRE_PARAGRAPHE_DISQUE	spec_titre_paragraf_dq = (PSPEC_TITRE_PARAGRAPHE_DISQUE)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_dq,val_geo_disq->pos_specif_paragraf);
            trouve = (spec_titre_paragraf_dq->pos_es == pos_es_descript);
						}
            break;
     
          default:
            break;
          }
				}
        break;

      case d_opc :
				{
#ifdef c_opc
				// $$OPC v�rifier
			  PGEO_OPC val_geo_opc = (PGEO_OPC)pointe_enr (szVERIFSource, __LINE__, b_geo_opc,organe->pos_geo);
				trouve = (val_geo_opc->dwIdES == pos_es_descript);
#endif
				}
        break;

      default:
        break;
      }
    }

  if (trouve)
    {
    (*pos_es_exec) = index;
    wRetour = 0;
    }
  else
    {
    wRetour = 22;
    }
  return wRetour;
  }

// -------------------------------------------------------------------------
UINT recherche_index_tableau (DWORD position_recherche, DWORD *position_trouve)
  {
  DWORD nb_enr = nb_enregistrements (szVERIFSource, __LINE__, b_tableau);
  UINT  erreur = 22;
  BOOL  trouve = FALSE;

  (*position_trouve) = 0;
  while (((*position_trouve) < nb_enr) && (!trouve))
    {
    (*position_trouve) ++;
		PTABLEAU	ptr_tab = (PTABLEAU)pointe_enr (szVERIFSource, __LINE__, b_tableau,(*position_trouve));
    if (ptr_tab->pos_es == position_recherche)
      {
      trouve = TRUE;
      erreur = 0;
      }
    }
  return erreur;
  }

// -------------------------------------------------------------------------
UINT recherche_index_tableau_i (DWORD position_recherche, DWORD indice, DWORD *position_trouve)
  {
  DWORD nb_enr = nb_enregistrements (szVERIFSource, __LINE__, b_tableau);
  UINT  erreur = 320;
  BOOL  trouve = FALSE;

  (*position_trouve) = 0;
  while (((*position_trouve) < nb_enr) && (!trouve))
    {
    (*position_trouve)++;
		PTABLEAU	ptr_tab = (PTABLEAU)pointe_enr (szVERIFSource, __LINE__, b_tableau,(*position_trouve));
    if (ptr_tab->pos_es == position_recherche)
      {
      trouve = TRUE;
      }
    }
  if (trouve)
    {
		PX_TABLEAU pointeur_tab = (PX_TABLEAU)pointe_enr (szVERIFSource, __LINE__, bx_tableau,(*position_trouve));
    if (indice <= pointeur_tab->taille)
      {
      erreur = 0;
      (*position_trouve) = pointeur_tab->pos_x_es + indice - 1;
      }
    }
  return erreur;
  }

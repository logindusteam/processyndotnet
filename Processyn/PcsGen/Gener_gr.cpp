/*---------------------------------------------------------------------+
|   Ce fichier est la propriete de                                     |
|              Societe LOGIQUE INDUSTRIE                               |
|       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
|   Il est demeure sa propriete exclusive et est confidentiel.         |
|   Aucune diffusion n'est possible sans accord ecrit                  |
|----------------------------------------------------------------------|
|   Auteur  : LM		
|   Date    : 08/04/93 						|
+----------------------------------------------------------------------*/
// Gener_gr.c
// WIN32 5/3/97

#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_sy.h"
#include "G_Objets.h"
#include "g_sys.h"
#include "BdGr.h"
#include "BdAnmGr.h"
#include "BdPageGr.h"
#include "BdElemGr.h"
#include "BdContGr.h"
#include "UChrono.h"
#include "tipe_gr.h"
#include "UStr.h"
#include "Threads.h"
#include "mem.h"
#include "pcs_sys.h"
#include "findbdex.h"
#include "ctxtgr.h"
#include "pcsfont.h"
#include "pcsspace.h"
#include "USem.h"
#include "WGen.h"
#include "cmdpcs.h"
#include "Verif.h"

#include "gener_gr.h"

VerifInit;

// ------------------------------------------------- Constantes
#define POLICE_DEFAUT         1
#define STYLE_POLICE_DEFAUT   0

//------------------- Variables -------------------------
static HWND  hwndGraph;          // Handle client
static const char * class_Wnd_Graph = "Wnd_Graph";
static HGSYS handle_gsys = NULL;
static HBDGR hbdgr = NULL;

//---------------------------
void capturer_pages_gr (void)
  {
/* $$
  DWORD     wNbPages;
  DWORD     wNumPage;
  DWORD     wCptPage;
  BOOL  bFullScreen;
  char     pszNomBmpFondSansExt [c_NB_CAR_NOM_BMP_FOND + 1];
  char     pszNomBmpFondAvecExt [c_NB_CAR_NOM_BMP_FOND + 1];
  char     pszExtention [4];
  void    *hdl;
  SWP      swpScreen;

  bFullScreen = FALSE;
  StrCopy (pszNomBmpFondSansExt, application_courante ());
  pszSupprimeExt (pszNomBmpFondSansExt);
  wCptPage = 0;
  wNbPages = bdgr_nombre_pages ();
  for (wNumPage = 1; wNumPage <= wNbPages; wNumPage++)
    {
    if (bdgr_existe_page (wNumPage))
      {
      maj_page_gr_courante (wNumPage);
      if (hbdgr != NULL)
        {
				// Emplacement ancien code Bitmap en ex�cution
        }
      }
    }
*/
  }
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
//             Initialisation du mode 'Presentation Manager'
//--------------------------------------------------------------------------
BOOL init_mode_pm_graph (void)
  {
  return TRUE;
  }

//--------------------------------------------------------------------------
//             Finitialisation du mode 'Presentation Manager'
//--------------------------------------------------------------------------
BOOL fini_mode_pm_graph (void)
  {
  return TRUE;
  }

//-------------------------------------------------------------------
//	WndProc de la fen�tre Graph - traite les messages re�us
LRESULT CALLBACK WndProcGraph (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT mres = 0;	// Valeur de retour

//  switch (msg)
//    {
//    default:
       mres = DefWindowProc (hwnd, msg, mp1, mp2);
//       break;
//    }
  return (mres);
  }


//-----------------------------------------------------------------------
// creation de la fenetre invisible
void initialiser_fenetre_graph (void)
  {
	WNDCLASS wc;
	//$$ inutile (et tout ce qui est li� � hwndGraph)
	wc.style					= CS_HREDRAW | CS_VREDRAW; 
	wc.lpfnWndProc		= WndProcGraph; 
	wc.cbClsExtra			= 0; 
	wc.cbWndExtra			= 0; 
	wc.hInstance			= Appli.hinst; 
	wc.hIcon					= NULL;
	wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
	wc.hbrBackground	= NULL; //(HBRUSH)(COLOR_APPWORKSPACE+1); pas d'effacement
	wc.lpszMenuName		= NULL; 
	wc.lpszClassName	= class_Wnd_Graph; 

  if (RegisterClass (&wc))
    {
    hwndGraph = CreateWindow (class_Wnd_Graph, "", WS_POPUP | WS_VISIBLE, 0, 0, 0, 0, Appli.hwnd, // $$ parent = Appli.hwnd ?
			(HMENU) 0, Appli.hinst, NULL);
    }
  }

// -----------------------------------------------------------------------
void supprimer_fenetre_graph (void)
  {
  ::DestroyWindow (hwndGraph);
  }

//-----------------------------------------------------------------------
// inits pour utilisation bdgr
void init_configuration_gr (void)
  {
  bdgr_supprimer_blocs_marques ();   //supression eventuelle de toutes les marques
  bdgr_creer ();
  handle_gsys = g_open (hwndGraph);
  g_init (handle_gsys);
  g_set_page_units (handle_gsys, c_G_NB_X, c_G_NB_Y, Appli.sizEcran.cx, Appli.sizEcran.cy);
  }

// -----------------------------------------------------------------------
void termine_configuration_gr (void)
  {
  if (hbdgr)
    {
    bdgr_fermer_page (&hbdgr, FALSE);
    }
  g_close (&handle_gsys);
  bdgr_supprimer_blocs_marques ();
  }

//-----------------------------------------------------------------------
// appell�e au d�but de gener_vi
void initialise_mode_graph (void)
  {
  init_mode_pm_graph ();
  initialiser_fenetre_graph (); //$$ pourquoi ? La wndproc ne fait rien !
  init_configuration_gr();
  hbdgr = NULL;
  }

//-----------------------------------------------------------------------
// appell�e � la fin de gener_vi
void termine_mode_graph (void)
  {
  capturer_pages_gr ();
  //
  termine_configuration_gr();
  supprimer_fenetre_graph ();
  fini_mode_pm_graph ();
  }

//-----------------------------------------------------------------------
// Appell�e � chaque ligne de description g�n�r�e
void interact_pm_graph (void)
  {
  MSG msg;

  while (PeekMessage (&msg, NULL, 0, 0, PM_REMOVE))
    {
    DispatchMessage (&msg);
    }
  }

//-----------------------------------------------------------------------
// initialise taille_tableau dans b_geo_syst et b_e_s
//-----------------------------------------------------------------------
void MiseAJourTaillesTableauxVarSysGr (void)
  {
  PGEO_SYST				val_geo_syst;
  DWORD						nb_page;
  PENTREE_SORTIE	pES;

  nb_page = bdgr_nombre_pages ();
  if (nb_page != 0)
    {
    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_visible);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;

    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_x);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;

    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_y);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;

    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_cx);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;

    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_cy);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;

    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_etat);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;

    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_impression);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;

    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_nom_document);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;

    val_geo_syst = (PGEO_SYST)pointe_enr (szVERIFSource, __LINE__, b_geo_syst, c_sys_fen_user);
    val_geo_syst->taille_tableau = nb_page;
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, val_geo_syst->pos_es);
    pES->taille = nb_page;
    }
  }

//-----------------------------------------------------------------------
void maj_init_sys_gr (void)
  {
  DWORD      nb_page;
  DWORD      n_page;
  int        xFen;
  int        yFen;
  int        cxFen;
  int        cyFen;
  PX_VAR_SYS	pos_es_visible = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_visible);
  PX_VAR_SYS	pos_es_x = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_x);
  PX_VAR_SYS	pos_es_y = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_y);
  PX_VAR_SYS	pos_es_cx = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_cx);
  PX_VAR_SYS	pos_es_cy = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_cy);
  PX_VAR_SYS	pos_es_etat = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_etat);
  PX_VAR_SYS	pos_es_impression = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_impression);
  PX_VAR_SYS	pos_es_nom_doc = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_nom_document);
  PX_VAR_SYS	pos_es_user = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_user);
  PBOOL   bool_exe;
  PFLOAT  num_exe;
  char*mes_exe;
  BOOL   visibilite;

  nb_page = bdgr_nombre_pages ();
  for (n_page = 1; (n_page <= nb_page); n_page++)
    {
    if (bdgr_existe_page (n_page))
      {
      bdgr_lire_coord_page_direct  (n_page, &xFen, &yFen, &cxFen, &cyFen);
      visibilite = bdgr_page_visible (n_page);

      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pos_es_x->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (xFen);
      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, (pos_es_x->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (xFen);

      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pos_es_y->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (yFen);
      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, (pos_es_y->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (yFen);

      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pos_es_cx->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (cxFen);
      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, (pos_es_cx->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (cxFen);

      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pos_es_cy->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (cyFen);
      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, (pos_es_cy->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (cyFen);

      bool_exe = (PBOOL)pointe_enr (szVERIFSource, __LINE__, b_cour_log, (pos_es_visible->dwPosCourEx) + n_page - 1);
      (*bool_exe) = visibilite;
      bool_exe = (PBOOL)pointe_enr (szVERIFSource, __LINE__, bva_mem_log, (pos_es_visible->dwPosCourEx) + n_page - 1);
      (*bool_exe) = visibilite;

      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pos_es_etat->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (DIM_NORMALE);
      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, (pos_es_etat->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (DIM_NORMALE);

      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pos_es_impression->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (0);
      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, (pos_es_impression->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (0);

      mes_exe = (char*)pointe_enr (szVERIFSource, __LINE__, b_cour_mes, (pos_es_nom_doc->dwPosCourEx) + n_page - 1);
      StrSetNull (mes_exe);
      mes_exe = (char*)pointe_enr (szVERIFSource, __LINE__, bva_mem_mes, (pos_es_nom_doc->dwPosCourEx) + n_page - 1);
      StrSetNull (mes_exe);

      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, b_cour_num, (pos_es_user->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (0);
      num_exe = (PFLOAT)pointe_enr (szVERIFSource, __LINE__, bva_mem_num, (pos_es_user->dwPosCourEx) + n_page - 1);
      (*num_exe) = (FLOAT) (0);

      }
    }
  } // maj_init_sys_gr

//-----------------------------------------------------------------------
// fabrication du handle graphisme
static HEADER_SERVICE_ANM_GR fabrication_handle_gr (ID_SERVICE_ANM_GR service, DWORD element)
  {
  HEADER_SERVICE_ANM_GR HeaderServiceAnmGr;

  HeaderServiceAnmGr.Service = service;	// Num�ro de service
  HeaderServiceAnmGr.Element = element;	// 
  return HeaderServiceAnmGr;
  }

//-------------------------------------------------------------------
// Cr�ation du bloc bx_services_speciaux_vi : ACK, FEN, PAGE
void organise_services_speciaux (void)
  {
  if (!existe_repere (bx_services_speciaux_vi))
    {
		tx_services_speciaux_vi* donneex_bx_srv_spec;

    cree_bloc (bx_services_speciaux_vi, sizeof (tx_services_speciaux_vi), 0);

    insere_enr (szVERIFSource, __LINE__, 1, bx_services_speciaux_vi, PTR_SRV_SPEC_VI_ACK);
    donneex_bx_srv_spec = (tx_services_speciaux_vi*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_vi, PTR_SRV_SPEC_VI_ACK);
    donneex_bx_srv_spec->handle = fabrication_handle_gr (SERVICE_ANM_GR_ACK, 0);

    insere_enr (szVERIFSource, __LINE__, 1, bx_services_speciaux_vi, PTR_SRV_SPEC_VI_PAGE);
    donneex_bx_srv_spec = (tx_services_speciaux_vi*)pointe_enr (szVERIFSource, __LINE__, bx_services_speciaux_vi, PTR_SRV_SPEC_VI_PAGE);
    donneex_bx_srv_spec->handle = fabrication_handle_gr (SERVICE_ANM_GR_SELECT_PAGE, 0);
    }
  }


//-------------------------------------------------------------------
// insere 1 var ds bx_entrees_vi et renvoie code_ret
DWORD insere_variable_entree (DWORD bloc_genre, DWORD pos_es)
  {
  DWORD nbr_enr_bx_entrees;
  tx_entrees_vi*donnees_bx_entrees;

  nbr_enr_bx_entrees = nb_enregistrements (szVERIFSource, __LINE__, bx_entrees_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_entrees_vi, nbr_enr_bx_entrees);
  donnees_bx_entrees = (tx_entrees_vi*)pointe_enr (szVERIFSource, __LINE__, bx_entrees_vi, nbr_enr_bx_entrees);
  donnees_bx_entrees->bloc_genre = bloc_genre;
  donnees_bx_entrees->pos_es     = pos_es;
  return nbr_enr_bx_entrees;
  }

//-------------------------------------------------------------------
// Gestion des animations fenetres
// fabrication des blocs bx_fenetre_vi et bx_anm_fenetre_vi
BOOL existe_fenetres_graphiques (void)
  {
  return (bdgr_nombre_pages () != 0);
  }

//-------------------------------------------------------------------
// Gestion des animations fenetres
// fabrication des blocs bx_fenetre_vi et bx_anm_fenetre_vi
DWORD organise_fenetre (void)
  {
  DWORD erreur;
  DWORD index;
  DWORD nbr_enr;
  tx_fenetre_vi*donnees_bx_fenetre;
  tx_vdi_nom_page*donnees_bx_nom_page;
  PX_ANM_FENETRE_VI	donnees_bx_anm_fenetre;
  PX_VAR_SYS	pos_es_visible = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_visible);
  PX_VAR_SYS	pos_es_x = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_x);
  PX_VAR_SYS	pos_es_y = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_y);
  PX_VAR_SYS	pos_es_cx = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_cx);
  PX_VAR_SYS	pos_es_cy = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_cy);
  PX_VAR_SYS	pos_es_etat = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_etat);
  PX_VAR_SYS	pos_es_impression = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_impression);
  PX_VAR_SYS	pos_es_nom_doc = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_nom_document);
  PX_VAR_SYS	pos_es_user = (PX_VAR_SYS)pointe_enr (szVERIFSource, __LINE__, b_va_systeme, c_sys_fen_user);
  char chaine_titre[c_NB_CAR_TITRE_PAGE_GR+1];

  erreur = 0;
  nbr_enr = bdgr_nombre_pages ();
  if (!existe_repere (bx_fenetre_vi))
    {
    cree_bloc (bx_fenetre_vi, sizeof (tx_fenetre_vi), nbr_enr);
    }
  if (!existe_repere (bx_vdi_nom_page))
    {
    cree_bloc (bx_vdi_nom_page, sizeof (tx_vdi_nom_page), nbr_enr);
    }
  if (!existe_repere (bx_element_vi))
    {
    cree_bloc (bx_element_vi, sizeof (tx_element_vi), 0);
    }
  if (!existe_repere (bx_variable_vi))
    {
  cree_bloc (bx_variable_vi, sizeof (tx_variable_vi), 0);
    }
  if (!existe_repere (bx_anm_fenetre_vi))
    {
    cree_bloc (bx_anm_fenetre_vi, sizeof (X_ANM_FENETRE_VI), nbr_enr);
    }
  if (!existe_repere (bx_anm_e_vi))
    {
    cree_bloc (bx_anm_e_vi, sizeof (X_ANM_E_VI), 0);
    }

  for (index = 1; (index <= nbr_enr); index++)
    {
    // initialisation de bx_anm_fenetre_vi
    donnees_bx_anm_fenetre = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, index);
    donnees_bx_anm_fenetre->visible  = FALSE;
    donnees_bx_anm_fenetre->cx_client_ref  = 0;
    donnees_bx_anm_fenetre->cy_client_ref  = 0;
    donnees_bx_anm_fenetre->prem_entree  = 0;
    donnees_bx_anm_fenetre->nbr_entree  = 0;
    if (bdgr_existe_page (index))
      {
      // fabrication de bx_fenetre_vi ***************
      donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, index);
      donnees_bx_fenetre->existe = TRUE;
      donnees_bx_fenetre->prem_elt = 0;
      donnees_bx_fenetre->nbr_elt = 0;

      // visibilite
      donnees_bx_fenetre->visible.handle = fabrication_handle_gr (SERVICE_ANM_GR_FEN_AFFICHE, index);
      donnees_bx_fenetre->visible.pos_es = (pos_es_visible->dwPosCourEx) + index - 1;
      donnees_bx_anm_fenetre->code_ret_visible = insere_variable_entree (b_cour_log, donnees_bx_fenetre->visible.pos_es);

      donnees_bx_fenetre->invisible.handle = fabrication_handle_gr (SERVICE_ANM_GR_FEN_EFFACE, index);
      donnees_bx_fenetre->invisible.pos_es = (pos_es_visible->dwPosCourEx) + index - 1;

      // impression
      donnees_bx_fenetre->impression.handle = fabrication_handle_gr (SERVICE_ANM_GR_PAGE_IMPRESSION, index);
      donnees_bx_fenetre->impression.pos_es_impression = (pos_es_impression->dwPosCourEx) + index - 1;
      donnees_bx_fenetre->impression.pos_es_nom_doc    = (pos_es_nom_doc->dwPosCourEx) + index - 1;

      // taille
      donnees_bx_fenetre->taille.handle = fabrication_handle_gr (SERVICE_ANM_GR_FEN_TAILLE, index);

      donnees_bx_fenetre->taille.pos_es_x = (pos_es_x->dwPosCourEx) + index - 1;
      donnees_bx_anm_fenetre->code_ret_x = insere_variable_entree (b_cour_num, donnees_bx_fenetre->taille.pos_es_x);

      donnees_bx_fenetre->taille.pos_es_y = (pos_es_y->dwPosCourEx) + index - 1;
      donnees_bx_anm_fenetre->code_ret_y = insere_variable_entree (b_cour_num, donnees_bx_fenetre->taille.pos_es_y);

      donnees_bx_fenetre->taille.pos_es_cx = (pos_es_cx->dwPosCourEx) + index - 1;
      donnees_bx_anm_fenetre->code_ret_cx = insere_variable_entree (b_cour_num, donnees_bx_fenetre->taille.pos_es_cx);

      donnees_bx_fenetre->taille.pos_es_cy = (pos_es_cy->dwPosCourEx) + index - 1;
      donnees_bx_anm_fenetre->code_ret_cy = insere_variable_entree (b_cour_num, donnees_bx_fenetre->taille.pos_es_cy);

      donnees_bx_fenetre->taille.pos_es_dimension = (pos_es_etat->dwPosCourEx) + index - 1;
      donnees_bx_anm_fenetre->code_ret_dimension = insere_variable_entree (b_cour_num, donnees_bx_fenetre->taille.pos_es_dimension);

      donnees_bx_anm_fenetre->code_ret_user = insere_variable_entree (b_cour_num, (pos_es_user->dwPosCourEx) + index - 1);

      // fabrication de bx_vdi_nom_page
      donnees_bx_nom_page = (tx_vdi_nom_page*)pointe_enr (szVERIFSource, __LINE__, bx_vdi_nom_page, index);
      donnees_bx_nom_page->existe = TRUE;
      StrDWordToStr (donnees_bx_nom_page->titre_page, index);
      StrConcat (donnees_bx_nom_page->titre_page," - ");
      if (bdgr_lire_titre_page_direct (index, chaine_titre))
        {
        StrConcat (donnees_bx_nom_page->titre_page, chaine_titre);
        }

      }    // existe page
    else
      {
      donnees_bx_fenetre = (tx_fenetre_vi*)pointe_enr (szVERIFSource, __LINE__, bx_fenetre_vi, index);
      donnees_bx_fenetre->existe = FALSE;
      donnees_bx_fenetre->prem_elt = 0;
      donnees_bx_fenetre->nbr_elt = 0;

      donnees_bx_nom_page = (tx_vdi_nom_page*)pointe_enr (szVERIFSource, __LINE__, bx_vdi_nom_page, index);
      donnees_bx_nom_page->existe = FALSE;
      }
    }      // for
  return erreur;
  }


//-------------------------------------------------------------------
// teste si la page existe ds gr
BOOL existence_page_gr (DWORD numero_page)
  {
  return bdgr_existe_page (numero_page);
  }

//-------------------------------------------------------------------
void maj_page_gr_courante (DWORD numero_page)
  {
  if (hbdgr != NULL)
    {
    bdgr_fermer_page (&hbdgr, FALSE);
    }
  hbdgr = hbdgr_ouvrir_page (numero_page, FALSE, FALSE);
  bdgr_associer_espace (hbdgr, handle_gsys);
  }

//-------------------------------------------------------------------
BOOL animation_presente_gr (DWORD *n_elt, DWORD *n_ligne)
  {
  BOOL bTrouve;

  bTrouve = bdgr_trouver_animation (hbdgr, n_elt, n_ligne);
  return (bTrouve);
  }

//-------------------------------------------------------------------
// Gestion des entrees logiques
HEADER_SERVICE_ANM_GR anim_e_log (DWORD n_elt, DWORD Type, DWORD pos_ret, DWORD numero_page)
  {
  DWORD pos_a_inserer;
  DWORD nbr_enr;
  PX_ANM_E_VI	donneex_anm_e;
  PX_ANM_FENETRE_VI	donnees_bx_anm_fenetre;
  int x1;
  int y1;
  int x2;
  int y2;
	POINT	pt0, pt1;
  ID_SERVICE_ANM_GR Service;
  DWORD Element;

  donnees_bx_anm_fenetre = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, numero_page);
  pos_a_inserer = donnees_bx_anm_fenetre->prem_entree + donnees_bx_anm_fenetre->nbr_entree;
  donnees_bx_anm_fenetre->nbr_entree++;

	switch (Type)
		{
		case EL_TRANSPARENTE:
			{
		  PX_ANM_E_LOG_VI	donneex_anm_e_log;

			if (!existe_repere (bx_anm_e_log_vi))
				{
				cree_bloc (bx_anm_e_log_vi, sizeof (X_ANM_E_LOG_VI), 0);
				}
			nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_log_vi)+1;
			insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_log_vi, nbr_enr);
			donneex_anm_e_log = (PX_ANM_E_LOG_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_log_vi, nbr_enr);
			bdgr_point_ref_element (hbdgr, n_elt, &pt0, &pt1);
			G_OrdonneBipoint (&pt0, &pt1);
			x1 = pt0.x; x2 = pt1.x; y1 = pt0.y; y2 = pt1.y;
			donneex_anm_e_log->x  = x1;
			donneex_anm_e_log->y  = y1;
			donneex_anm_e_log->cx = (DWORD) (x2 - x1);
			donneex_anm_e_log->cy = (DWORD) (y2 - y1);
			donneex_anm_e_log->pos_ret = pos_ret;
			donneex_anm_e_log->bValeurCourante = FALSE; // $$ faute de mieux
			donneex_anm_e_log->hwnd = NULL;
			bdgr_ligne_anim_element (hbdgr, n_elt, 0);
			bdgr_supprimer_element (hbdgr, MODE_VIRTUEL, n_elt);

			insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_vi, pos_a_inserer);
			donneex_anm_e = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pos_a_inserer);
			donneex_anm_e->n_bloc = bx_anm_e_log_vi;
			donneex_anm_e->n_enr  = nbr_enr;
			Service = SERVICE_ANM_GR_ANM_FORCAGE_LOG_TRANSPARENT;
			Element = nbr_enr;
			}
			break;

		case EL_CONTROL_CHECK:
			{
		  PX_ANM_E_CONTROL_LOG_VI	donneex_anm_e_control_log;

			if (!existe_repere (bx_anm_e_control_log))
				{
				cree_bloc (bx_anm_e_control_log, sizeof (X_ANM_E_CONTROL_LOG_VI), 0);
				}
			nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_control_log)+1;
			insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_control_log, nbr_enr);
			donneex_anm_e_control_log = (PX_ANM_E_CONTROL_LOG_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_control_log, nbr_enr);
			donneex_anm_e_control_log->n_elt = n_elt;
			donneex_anm_e_control_log->pos_ret = pos_ret;

			insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_vi, pos_a_inserer);
			donneex_anm_e = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pos_a_inserer);
			donneex_anm_e->n_bloc = bx_anm_e_control_log;
			donneex_anm_e->n_enr  = nbr_enr;
			Service = SERVICE_ANM_GR_ANM_FORCAGE_CHECK;
			Element = n_elt;
			}
			break;

		case EL_CONTROL_BOUTON_PULSE:
			{
		  PX_ANM_E_CONTROL_LOG_VI	donneex_anm_e_control_log;

			if (!existe_repere (bx_anm_e_control_log))
				{
				cree_bloc (bx_anm_e_control_log, sizeof (X_ANM_E_CONTROL_LOG_VI), 0);
				}
			nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_control_log)+1;
			insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_control_log, nbr_enr);
			donneex_anm_e_control_log = (PX_ANM_E_CONTROL_LOG_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_control_log, nbr_enr);
			donneex_anm_e_control_log->n_elt = n_elt;
			donneex_anm_e_control_log->pos_ret = pos_ret;

			insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_vi, pos_a_inserer);
			donneex_anm_e = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pos_a_inserer);
			donneex_anm_e->n_bloc = bx_anm_e_control_log;
			donneex_anm_e->n_enr  = nbr_enr;
			Service = SERVICE_ANM_GR_ANM_FORCAGE_BOUTON_PULSE;
			Element = n_elt;
			}
			break;

		default:
			VerifExit;
			break;
		}
	return fabrication_handle_gr (Service, Element);
  }

//-------------------------------------------------------------------
// Gestion des entrees numeriques
HEADER_SERVICE_ANM_GR anim_e_num (DWORD n_elt, DWORD couleur, DWORD color_fond,
                   UINT nbr_caractere, BOOL cadre, BOOL masque, BOOL video,
                   FLOAT min, FLOAT max, LONG NbDecimales, DWORD pos_ret, DWORD numero_page)
  {
  DWORD pos_a_inserer;
  DWORD nbr_enr;
  PX_ANM_E_NUM_VI	donneex_anm_e_num;
  PX_ANM_E_VI	donneex_anm_e;
  PX_ANM_FENETRE_VI	donnees_bx_anm_fenetre;
  int x1;
  int y1;
  int x2;
  int y2;
	POINT	pt0, pt1;

  donnees_bx_anm_fenetre = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, numero_page);
  pos_a_inserer = donnees_bx_anm_fenetre->prem_entree + donnees_bx_anm_fenetre->nbr_entree;
  donnees_bx_anm_fenetre->nbr_entree++;

  if (!existe_repere (bx_anm_e_num_vi))
    {
    cree_bloc (bx_anm_e_num_vi, sizeof (X_ANM_E_NUM_VI), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_num_vi)+1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_num_vi, nbr_enr);
  donneex_anm_e_num = (PX_ANM_E_NUM_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_num_vi, nbr_enr);
	bdgr_point_ref_element (hbdgr, n_elt, &pt0, &pt1);
  G_OrdonneBipoint (&pt0, &pt1);
	x1 = pt0.x; x2 = pt1.x; y1 = pt0.y; y2 = pt1.y;
    donneex_anm_e_num->x1  = x1;
  donneex_anm_e_num->y1  = y1;
  donneex_anm_e_num->cx = (DWORD) (x2 - x1);
  donneex_anm_e_num->cy = (DWORD) (y2 - y1);

  bdgr_lire_style_texte_element (hbdgr, n_elt, &(donneex_anm_e_num->police),
                                 &(donneex_anm_e_num->style_police));
  donneex_anm_e_num->nbr_car = (DWORD) nbr_caractere;
  donneex_anm_e_num->min     = min;
  donneex_anm_e_num->max     = max;
  donneex_anm_e_num->cadre   = cadre;
  donneex_anm_e_num->masque  = masque;
  donneex_anm_e_num->video_focus = video;
  donneex_anm_e_num->couleur = (G_COULEUR)couleur;
  donneex_anm_e_num->couleur_fond = (G_COULEUR)color_fond;
  donneex_anm_e_num->NbDecimales = NbDecimales; //!ac
  donneex_anm_e_num->pos_ret = pos_ret;
  donneex_anm_e_num->val_init = (FLOAT)0;
  donneex_anm_e_num->hdl_inline = NULL;
  bdgr_ligne_anim_element (hbdgr, n_elt, 0);
  bdgr_supprimer_element (hbdgr, MODE_VIRTUEL, n_elt);

  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_vi, pos_a_inserer);
  donneex_anm_e = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pos_a_inserer);
  donneex_anm_e->n_bloc = bx_anm_e_num_vi;
  donneex_anm_e->n_enr  = nbr_enr;
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_FORCAGE_NUM, nbr_enr));
  }

//-------------------------------------------------------------------
// Gestion des entrees messages
HEADER_SERVICE_ANM_GR anim_e_mes (DWORD n_elt, DWORD couleur, DWORD color_fond,
                   UINT nbr_caractere, BOOL cadre, BOOL masque, BOOL video,
                   DWORD pos_ret, DWORD numero_page)
  {
  DWORD pos_a_inserer;
  DWORD nbr_enr;
  PX_ANM_E_MES_VI	donneex_anm_e_mes;
  PX_ANM_E_VI	donneex_anm_e;
  PX_ANM_FENETRE_VI	donnees_bx_anm_fenetre;
  int x1;
  int y1;
  int x2;
  int y2;
	POINT	pt0, pt1;

  donnees_bx_anm_fenetre = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, numero_page);
  pos_a_inserer = donnees_bx_anm_fenetre->prem_entree + donnees_bx_anm_fenetre->nbr_entree;
  donnees_bx_anm_fenetre->nbr_entree++;

  if (!existe_repere (bx_anm_e_mes_vi))
    {
    cree_bloc (bx_anm_e_mes_vi, sizeof (X_ANM_E_MES_VI), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_mes_vi)+1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_mes_vi, nbr_enr);
  donneex_anm_e_mes = (PX_ANM_E_MES_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_mes_vi, nbr_enr);
	bdgr_point_ref_element (hbdgr, n_elt, &pt0, &pt1);
  G_OrdonneBipoint (&pt0, &pt1);
	x1 = pt0.x; x2 = pt1.x; y1 = pt0.y; y2 = pt1.y;
    donneex_anm_e_mes->x1  = x1;
  donneex_anm_e_mes->y1  = y1;
  donneex_anm_e_mes->cx = (DWORD) (x2 - x1);
  donneex_anm_e_mes->cy = (DWORD) (y2 - y1);
  bdgr_lire_style_texte_element (hbdgr, n_elt, &(donneex_anm_e_mes->police),
                                 &(donneex_anm_e_mes->style_police));
  donneex_anm_e_mes->nbr_car = (DWORD) nbr_caractere;
  donneex_anm_e_mes->couleur = (G_COULEUR) (couleur);
  donneex_anm_e_mes->couleur_fond = (G_COULEUR)(color_fond);
  donneex_anm_e_mes->cadre   = cadre;
  donneex_anm_e_mes->masque  = masque;
  donneex_anm_e_mes->video_focus = video;
  donneex_anm_e_mes->pos_ret = pos_ret;
  donneex_anm_e_mes->val_init [0] = '\0';
  donneex_anm_e_mes->hdl_inline = NULL;
  bdgr_ligne_anim_element (hbdgr, n_elt, 0);
  bdgr_supprimer_element (hbdgr, MODE_VIRTUEL, n_elt);

  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_vi, pos_a_inserer);
  donneex_anm_e = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pos_a_inserer);
  donneex_anm_e->n_bloc = bx_anm_e_mes_vi;
  donneex_anm_e->n_enr  = nbr_enr;
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_FORCAGE_MES, nbr_enr));
  }

//-------------------------------------------------------------------
// Gestion des entrees radio
HEADER_SERVICE_ANM_GR anim_e_radio (DWORD n_elt, DWORD pos_ret, DWORD numero_page, BOOL *bPremierBouton)
  {
  DWORD pos_a_inserer;
  DWORD nbr_enr;
	DWORD StyleLigne;
	DWORD StyleRemplissage;
  PX_ANM_E_VI	donneex_anm_e;
  PX_ANM_E_RADIO	donneex_anm_e_radio;
  PX_ANM_FENETRE_VI	donnees_bx_anm_fenetre;

  donnees_bx_anm_fenetre = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, numero_page);
  pos_a_inserer = donnees_bx_anm_fenetre->prem_entree + donnees_bx_anm_fenetre->nbr_entree;
  donnees_bx_anm_fenetre->nbr_entree++;

	if (!existe_repere (bx_anm_e_radio))
		{
		cree_bloc (bx_anm_e_radio, sizeof (X_ANM_E_RADIO), 0);
		}
	nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_radio)+1;
	insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_radio, nbr_enr);
	donneex_anm_e_radio = (PX_ANM_E_RADIO)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_radio, nbr_enr);
	donneex_anm_e_radio->n_elt = n_elt;
	donneex_anm_e_radio->pos_ret = pos_ret;

	insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_vi, pos_a_inserer);
	donneex_anm_e = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pos_a_inserer);
	donneex_anm_e->n_bloc = bx_anm_e_radio;
	donneex_anm_e->n_enr  = nbr_enr;
	bdgr_lire_style_element (hbdgr, n_elt, &StyleLigne, &StyleRemplissage);
	(*bPremierBouton) = ((StyleRemplissage & WS_GROUP) == WS_GROUP);
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_FORCAGE_RADIO, n_elt));
  }

//-------------------------------------------------------------------
// Gestion des entrees liste
HEADER_SERVICE_ANM_GR anim_e_liste (DWORD n_elt, DWORD pos_ret, DWORD numero_page)
  {
  DWORD pos_a_inserer;
  DWORD nbr_enr;
  PX_ANM_E_VI	donneex_anm_e;
  PX_ANM_E_LISTE	donneex_anm_e_liste;
  PX_ANM_FENETRE_VI	donnees_bx_anm_fenetre;

  donnees_bx_anm_fenetre = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, numero_page);
  pos_a_inserer = donnees_bx_anm_fenetre->prem_entree + donnees_bx_anm_fenetre->nbr_entree;
  donnees_bx_anm_fenetre->nbr_entree++;

	if (!existe_repere (bx_anm_e_liste))
		{
		cree_bloc (bx_anm_e_liste, sizeof (X_ANM_E_LISTE), 0);
		}
	nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_liste)+1;
	insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_liste, nbr_enr);
	donneex_anm_e_liste = (PX_ANM_E_LISTE)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_liste, nbr_enr);
	donneex_anm_e_liste->n_elt = n_elt;
	donneex_anm_e_liste->pos_ret = pos_ret;

	insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_vi, pos_a_inserer);
	donneex_anm_e = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pos_a_inserer);
	donneex_anm_e->n_bloc = bx_anm_e_liste;
	donneex_anm_e->n_enr  = nbr_enr;
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_LISTE_COMBOBOX, n_elt));
  }


//-------------------------------------------------------------------
// Gestion des entrees combobox
HEADER_SERVICE_ANM_GR anim_e_combobox (DWORD n_elt, DWORD pos_ret_indice, DWORD pos_ret_saisie, DWORD numero_page)
  {
  DWORD pos_a_inserer;
  DWORD nbr_enr;
  PX_ANM_E_VI	donneex_anm_e;
  PX_ANM_E_COMBOBOX	donneex_anm_e_combobox;
  PX_ANM_FENETRE_VI	donnees_bx_anm_fenetre;

  donnees_bx_anm_fenetre = (PX_ANM_FENETRE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_fenetre_vi, numero_page);
  pos_a_inserer = donnees_bx_anm_fenetre->prem_entree + donnees_bx_anm_fenetre->nbr_entree;
  donnees_bx_anm_fenetre->nbr_entree++;

	if (!existe_repere (bx_anm_e_combobox))
		{
		cree_bloc (bx_anm_e_combobox, sizeof (X_ANM_E_COMBOBOX), 0);
		}
	nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_e_combobox)+1;
	insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_combobox, nbr_enr);
	donneex_anm_e_combobox = (PX_ANM_E_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_combobox, nbr_enr);
	donneex_anm_e_combobox->n_elt = n_elt;
	donneex_anm_e_combobox->pos_ret_indice = pos_ret_indice;
	donneex_anm_e_combobox->pos_ret_saisie = pos_ret_saisie;

	insere_enr (szVERIFSource, __LINE__, 1, bx_anm_e_vi, pos_a_inserer);
	donneex_anm_e = (PX_ANM_E_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_e_vi, pos_a_inserer);
	donneex_anm_e->n_bloc = bx_anm_e_combobox;
	donneex_anm_e->n_enr  = nbr_enr;
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_LISTE_COMBOBOX, n_elt));
  }

//-------------------------------------------------------------------
// Gestion des reflets logiques
HEADER_SERVICE_ANM_GR anim_r_log (DWORD n_elt, DWORD color_m_n, DWORD color_a_n, DWORD color_fond, DWORD tipe_anim)
  {
  DWORD nbr_enr;
  PX_ANM_R_LOG_VI	donneex_anm_r_log;

  if (!existe_repere (bx_anm_r_log_vi))
    {
    cree_bloc (bx_anm_r_log_vi, sizeof (X_ANM_R_LOG_VI), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_r_log_vi)+1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_r_log_vi, nbr_enr);
  donneex_anm_r_log = (PX_ANM_R_LOG_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_log_vi, nbr_enr);
  donneex_anm_r_log->color_m_n = (G_COULEUR)color_m_n;
  donneex_anm_r_log->color_a_n = (G_COULEUR)color_a_n;
  switch (tipe_anim)
    {
    case c_res_c:
    case c_res_couleur:
      donneex_anm_r_log->tipe_anim = STYLE_COULEUR;
      bdgr_couleur_element (hbdgr, MODE_VIRTUEL, n_elt, G_BLACK);
      break;

    case c_res_recoit:
    case c_res_remplissage:
      donneex_anm_r_log->tipe_anim = STYLE_REMPLISSAGE;
      break;

    case c_res_s:
    case c_res_style:
      donneex_anm_r_log->tipe_anim = STYLE_LIGNE;
      break;

    default:
      break;
    }
  donneex_anm_r_log->numero_gr = n_elt;
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_LOG_2_ETATS, nbr_enr));
  }

//-------------------------------------------------------------------
// Gestion des reflets logiques 4 etats
HEADER_SERVICE_ANM_GR anim_r_log_4etats (DWORD n_elt, DWORD color_h_h, DWORD color_h_b,
                          DWORD color_b_h, DWORD color_b_b, DWORD color_fond,
                          DWORD tipe_anim)
  {
  DWORD nbr_enr;
  PX_ANM_R_LOG_4_ETATS	donneex_anm_r_log;

  if (!existe_repere (bx_anm_r_log_4etats_vi))
    {
    cree_bloc (bx_anm_r_log_4etats_vi, sizeof (X_ANM_R_LOG_4_ETATS), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_r_log_4etats_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_r_log_4etats_vi, nbr_enr);
  donneex_anm_r_log = (PX_ANM_R_LOG_4_ETATS)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_log_4etats_vi, nbr_enr);
  donneex_anm_r_log->color_h_h = (G_COULEUR)color_h_h;
  donneex_anm_r_log->color_h_b = (G_COULEUR)color_h_b;
  donneex_anm_r_log->color_b_h = (G_COULEUR)color_b_h;
  donneex_anm_r_log->color_b_b = (G_COULEUR)color_b_b;
  switch (tipe_anim)
    {
    case c_res_c:
    case c_res_couleur:
      donneex_anm_r_log->tipe_anim = STYLE_COULEUR;
      bdgr_couleur_element (hbdgr, MODE_VIRTUEL, n_elt, G_BLACK);
    break;

    case c_res_recoit:
    case c_res_remplissage:
      donneex_anm_r_log->tipe_anim = STYLE_REMPLISSAGE;
    break;

    case c_res_s:
    case c_res_style:
      donneex_anm_r_log->tipe_anim = STYLE_LIGNE;
    break;

    default:
    break;
    }
  donneex_anm_r_log->numero_gr = n_elt;
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_LOG_4_ETATS, nbr_enr));
  }

//-------------------------------------------------------------------
// Gestion des reflets numeriques bargraphes
HEADER_SERVICE_ANM_GR anim_r_num (DWORD n_elt, DWORD modele, DWORD color, DWORD color_fond, DWORD style, FLOAT max, FLOAT min)
  {
  DWORD nbr_enr;
  int  numerateur;
  PX_ANM_R_NUM	donneex_anm_r_num;
  int x1;
  int y1;
  int x2;
  int y2;
	POINT	pt0, pt1;

  if (!existe_repere (bx_anm_r_num_vi))
    {
    cree_bloc (bx_anm_r_num_vi, sizeof (X_ANM_R_NUM), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_r_num_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_r_num_vi, nbr_enr);
  donneex_anm_r_num = (PX_ANM_R_NUM)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_num_vi, nbr_enr);
  donneex_anm_r_num->modele = modele;
  donneex_anm_r_num->max    = max;
  donneex_anm_r_num->min    = min;
	bdgr_point_ref_element (hbdgr, n_elt, &pt0, &pt1);
  G_OrdonneBipoint (&pt0, &pt1);
	x1 = pt0.x; x2 = pt1.x; y1 = pt0.y; y2 = pt1.y;
  
  switch (modele)
    {
    case BAR_UP:
      numerateur =  y1 - y2;
      donneex_anm_r_num->homothetie  = ((FLOAT)numerateur) / (max - min);
      donneex_anm_r_num->translation = y2 - (int)(min * donneex_anm_r_num->homothetie);
			break;

    case BAR_DOWN:
      numerateur = y2 - y1;
      donneex_anm_r_num->homothetie  = ((FLOAT)numerateur) / (max - min);
      donneex_anm_r_num->translation = y1 - (int)(min * donneex_anm_r_num->homothetie);
			break;

    case BAR_RIGHT:
      numerateur = x2 - x1;
      donneex_anm_r_num->homothetie  = ((FLOAT)numerateur) / (max - min);
      donneex_anm_r_num->translation = x1 - (int)(min * donneex_anm_r_num->homothetie);
			break;

    case BAR_LEFT:
      numerateur = x1 - x2;
      donneex_anm_r_num->homothetie  = ((FLOAT)numerateur) / (max - min);
      donneex_anm_r_num->translation = x2 - (int)(min * donneex_anm_r_num->homothetie);
			break;

    default:
    break;
    }

  bdgr_substituer_action_element (hbdgr, n_elt, G_ACTION_RECTANGLE_PLEIN);
  bdgr_coordonnees_element (hbdgr, MODE_VIRTUEL, n_elt, x1, y1, x2, y2);
  bdgr_couleur_element (hbdgr, MODE_VIRTUEL, n_elt, (G_COULEUR)color);
  bdgr_style_remplissage_element (hbdgr, MODE_VIRTUEL, n_elt, (G_STYLE_REMPLISSAGE)style);
  donneex_anm_r_num->numero_gr = n_elt;
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_NUM_BARAGRAPHE, nbr_enr));
  }

//-------------------------------------------------------------------
// Gestion des reflets numeriques deplacement
HEADER_SERVICE_ANM_GR anim_r_num_dep (DWORD n_elt, DWORD color_cste, DWORD color, DWORD color_fond, DWORD tipe_anim)
  {
  DWORD nbr_enr;
  PX_ANM_R_NUM_DEPLACE	donneex_anm_r_num;

  if (!existe_repere (bx_anm_r_num_dep_vi))
    {
    cree_bloc (bx_anm_r_num_dep_vi, sizeof (X_ANM_R_NUM_DEPLACE), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_r_num_dep_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_r_num_dep_vi, nbr_enr);
  donneex_anm_r_num = (PX_ANM_R_NUM_DEPLACE)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_num_dep_vi, nbr_enr);

  donneex_anm_r_num->numero_gr = n_elt;
  bdgr_point_ref_element (hbdgr, donneex_anm_r_num->numero_gr,
                          &donneex_anm_r_num->pt0, &donneex_anm_r_num->pt1);

  switch (color_cste)
    {
    case 0: // couleur variable
      switch (tipe_anim)
        {
        case c_res_c:
        case c_res_couleur:
          donneex_anm_r_num->tipe_anim = STYLE_COULEUR;
          donneex_anm_r_num->couleur_fond = color_fond;
					break;

        case c_res_recoit:
        case c_res_remplissage:
          donneex_anm_r_num->tipe_anim = STYLE_REMPLISSAGE;
        break;

        case c_res_s:
        case c_res_style:
          donneex_anm_r_num->tipe_anim = STYLE_LIGNE;
        break;

        default:
        break;
        }
    break;

    case 1: // couleur cste
      donneex_anm_r_num->tipe_anim = STYLE_RIEN;
      switch (tipe_anim)
        {
        case c_res_c:
        case c_res_couleur:
          bdgr_couleur_element (hbdgr, MODE_VIRTUEL, donneex_anm_r_num->numero_gr, (G_COULEUR)color);
        break;

        case c_res_recoit:
        case c_res_remplissage:
          bdgr_style_remplissage_element (hbdgr, MODE_VIRTUEL, donneex_anm_r_num->numero_gr, (G_STYLE_REMPLISSAGE)color);
        break;

        case c_res_s:
        case c_res_style:
          bdgr_style_ligne_element (hbdgr, MODE_VIRTUEL, donneex_anm_r_num->numero_gr, (G_STYLE_LIGNE)color);
        break;

        default:
        break;
        }
    break;

    default:
    break;
    }
  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_NUM_DEPLACEMENT, nbr_enr));
  }

//-------------------------------------------------------------------
// Gestion des reflets messages
HEADER_SERVICE_ANM_GR anim_r_mes (DWORD n_elt, DWORD color_cste, DWORD color, DWORD color_fond)
  {
  DWORD nbr_enr;
  PX_ANM_R_MES	donneex_anm_r_mes;
  HEADER_SERVICE_ANM_GR handle_tmp;

  if (!existe_repere (bx_anm_r_mes_vi))
    {
    cree_bloc (bx_anm_r_mes_vi, sizeof (X_ANM_R_MES), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_r_mes_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_r_mes_vi, nbr_enr);
  donneex_anm_r_mes = (PX_ANM_R_MES)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_mes_vi, nbr_enr);
  switch (color_cste)
    {
    case 1: // couleur cste
      handle_tmp = fabrication_handle_gr (SERVICE_ANM_GR_ANM_MES_SIMPLE, nbr_enr);
      bdgr_couleur_element (hbdgr, MODE_VIRTUEL, n_elt, (G_COULEUR)color);
    break;

    case 2: // couleur variable
    case 3: // couleur variable tableau
      handle_tmp = fabrication_handle_gr (SERVICE_ANM_GR_ANM_MES_COULEUR, nbr_enr);
      donneex_anm_r_mes->couleur_fond = color_fond;
    break;

    default:
    break;
    }

  donneex_anm_r_mes->numero_gr = n_elt;

  return (handle_tmp);
  }

//-------------------------------------------------------------------
// Gestion des reflets numeriques alpha
HEADER_SERVICE_ANM_GR anim_r_num_alpha (DWORD n_elt, DWORD color_cste,
                         DWORD color, DWORD color_fond, UINT nbr_car, DWORD nbr_dec)
  {
  DWORD nbr_enr;
  PX_ANM_R_NUM_ALPHA	donneex_anm_r_num;
  HEADER_SERVICE_ANM_GR handle_tmp;
  char psz_num[16];

  if (!existe_repere (bx_anm_r_num_alpha_vi))
    {
    cree_bloc (bx_anm_r_num_alpha_vi, sizeof (X_ANM_R_NUM_ALPHA), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_r_num_alpha_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_r_num_alpha_vi, nbr_enr);
  donneex_anm_r_num = (PX_ANM_R_NUM_ALPHA)pointe_enr (szVERIFSource, __LINE__, bx_anm_r_num_alpha_vi, nbr_enr);
  switch (color_cste)
    {
    case 1: // couleur cste
      handle_tmp = fabrication_handle_gr (SERVICE_ANM_GR_ANM_MES_NUM_SIMPLE, nbr_enr);
      bdgr_couleur_element (hbdgr, MODE_VIRTUEL, n_elt, (G_COULEUR)color);
      break;

    case 2: // couleur variable
    case 3: // couleur variable tableau
      handle_tmp = fabrication_handle_gr (SERVICE_ANM_GR_ANM_MES_NUM_COULEUR, nbr_enr);
      donneex_anm_r_num->couleur_fond = (G_COULEUR)color_fond;
      break;

    default:
      break;
    }

  StrCopy (donneex_anm_r_num->pszformat, "%-");
  StrDWordToStr (psz_num, nbr_car);
  StrConcat (donneex_anm_r_num->pszformat, psz_num);
  StrConcat (donneex_anm_r_num->pszformat, ".");
  StrDWordToStr (psz_num, nbr_dec);
  StrConcat (donneex_anm_r_num->pszformat, psz_num);
  StrConcat (donneex_anm_r_num->pszformat, "lf");

  donneex_anm_r_num->numero_gr = n_elt;

  return (handle_tmp);
  }

//-------------------------------------------------------------------
// Gestion des courbes
HEADER_SERVICE_ANM_GR anim_r_courbe (DWORD n_elt, DWORD page, FLOAT min, FLOAT max, DWORD couleur, DWORD color_fond,
                      DWORD style, DWORD type_courbe, DWORD nb_points_max, int nb_points_scroll,
                      DWORD *erreur)
  {
  DWORD nbr_enr;
  LONG y_min, y_max;
  PX_ANM_COURBE_VI	donneex_anm_r_courbe;
  int x1;
  int y1;
  int x2;
  int y2;
	POINT	pt0, pt1;

  if (!existe_repere (bx_anm_courbe_vi))
    {
    cree_bloc (bx_anm_courbe_vi, sizeof (X_ANM_COURBE_VI), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_courbe_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_courbe_vi, nbr_enr);
  donneex_anm_r_courbe = (PX_ANM_COURBE_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_vi, nbr_enr);
	bdgr_point_ref_element (hbdgr, n_elt, &pt0, &pt1);
  G_OrdonneBipoint (&pt0, &pt1);
	x1 = pt0.x; x2 = pt1.x; y1 = pt0.y; y2 = pt1.y;
  
  y_min = (LONG)y1;
  y_max = (LONG)y2;
  donneex_anm_r_courbe->numero_page = page;
  donneex_anm_r_courbe->min         = min;
  donneex_anm_r_courbe->max         = max;
  donneex_anm_r_courbe->homothetie  = (FLOAT)(y_min - y_max) / (max - min);
  donneex_anm_r_courbe->translation = (FLOAT)y_max - (min * donneex_anm_r_courbe->homothetie);
  donneex_anm_r_courbe->yHaut       = y_min;
  donneex_anm_r_courbe->yBas        = y_max;
  donneex_anm_r_courbe->x_org       = (LONG)x1 * 65536;
  donneex_anm_r_courbe->pas_x       = ((LONG)((x2 - x1 + 1) * 65536) / nb_points_max);
  if (HIWORD(donneex_anm_r_courbe->pas_x) == 0)
    {
    *erreur = 154;
    }

  donneex_anm_r_courbe->couleur          = (G_COULEUR)couleur;
  donneex_anm_r_courbe->style            = style;
  donneex_anm_r_courbe->type_courbe      = type_courbe;
  donneex_anm_r_courbe->nb_points_max    = nb_points_max;
  donneex_anm_r_courbe->nb_points_scroll = nb_points_scroll;
  donneex_anm_r_courbe->nb_points        = 0;
  donneex_anm_r_courbe->paPoints       = NULL;

  bdgr_ligne_anim_element (hbdgr, n_elt, 0);
  bdgr_supprimer_element (hbdgr, MODE_VIRTUEL, n_elt);

  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_COURBES, nbr_enr));
  }

//-------------------------------------------------------------------
// Gestion des courbes archivage
HEADER_SERVICE_ANM_GR anim_r_courbe_archive (DWORD n_elt, DWORD page)
  {
  DWORD nbr_enr;
  PX_ANM_COURBE_FIC_VI	donneex_anm_r_courbe;
  int x1;
  int y1;
  int x2;
  int y2;
	POINT	pt0, pt1;

  if (!existe_repere (bx_anm_courbe_archive_vi))
    {
    cree_bloc (bx_anm_courbe_archive_vi, sizeof (X_ANM_COURBE_FIC_VI), 0);
    }
  nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, bx_anm_courbe_archive_vi, nbr_enr);
  donneex_anm_r_courbe = (PX_ANM_COURBE_FIC_VI)pointe_enr (szVERIFSource, __LINE__, bx_anm_courbe_archive_vi, nbr_enr);
	bdgr_point_ref_element (hbdgr, n_elt, &pt0, &pt1);
  G_OrdonneBipoint (&pt0, &pt1);
	x1 = pt0.x; x2 = pt1.x; y1 = pt0.y; y2 = pt1.y;
  
  donneex_anm_r_courbe->numero_page    = page;
  donneex_anm_r_courbe->yHaut          = y1;
  donneex_anm_r_courbe->yBas	         = y2;
  donneex_anm_r_courbe->coef_en_x      = (((LONG)(x2 - x1 + 1)) * 65536);
  donneex_anm_r_courbe->x_min          = (LONG)x1 * 65536;

  donneex_anm_r_courbe->nb_points_max  = 0;
  donneex_anm_r_courbe->paPoints     = NULL;

  bdgr_ligne_anim_element (hbdgr, n_elt, 0);
  bdgr_supprimer_element (hbdgr, MODE_VIRTUEL, n_elt);

  return (fabrication_handle_gr (SERVICE_ANM_GR_ANM_ARCHIVES, nbr_enr));
  }

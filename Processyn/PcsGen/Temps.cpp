/*--------------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                         |
 |                                                                          |
 |                    Societe LOGIQUE INDUSTRIE                             |
 |            Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                 |
 |        Il demeure sa propriete exclusive et est confidentiel.            |
 |                                                                          |
 |   Aucune diffusion n'est possible sans accord ecrit.                     |
 |--------------------------------------------------------------------------|
 |                                                                          |
 |   Titre   : Temps.C         Gestion Date/Heure Syteme + Chronos + Tempos |
 |   Auteur  : JB                                                           |
 |   Date    : 08/03/93                                                     |
 |   Mise a jours:                                                          |
 |                                                                          |
 | +--------+--------+---+------------------------------------------------+ |
 | | date   | qui    |no | raisons                                        | |
 | +--------+--------+---+------------------------------------------------+ |
 | |08/03/93| JB     | x | Creation: Extraction des fonctions du Temps    | |
 | |        |        |   |           � partir de TOOL86P.asm              | |
 | |29/07/93| JB     | x | Correction Bug: Retardement des Chronom�tres   | |
 | |        |        |   |                 d'environ 2 Secondes par Minute| |
 | |17/09/97| JS     | x | Extraction et adaptation int 64 Chronos et     | |
 | |        |        |   | Tempos pour WIN32 dans UChrono									| |
 | |        |        |   |                                                | |
 | +--------+--------+---+------------------------------------------------+ |
 |                                                                          |
 |   Remarques: Les lectures/ecritures des dates et heures systemes passent |
 |              par un seul canal: DosGetDateTime()/DosSetDateTime().       |
 +--------------------------------------------------------------------------*/

#include "stdafx.h"
#include "Temps.h"

// --------------------------------------------------------------------------
BOOL TpsLireDateHeure (DWORD *pJour, DWORD *pMois, DWORD *pAnnee, DWORD *pJourDeSemaine,
	DWORD *pHeures, DWORD *pMinutes, DWORD *pSecondes, DWORD *pCentiemes)
  {
  SYSTEMTIME DateHeure;

  GetLocalTime (&DateHeure);
  *pJour          = (DWORD) DateHeure.wDay;
  *pMois          = (DWORD) DateHeure.wMonth;
  *pAnnee         = (DWORD) DateHeure.wYear;
  *pJourDeSemaine = (DWORD) DateHeure.wDayOfWeek;
  *pHeures        = (DWORD) DateHeure.wHour;
  *pMinutes       = (DWORD) DateHeure.wMinute;
  *pSecondes      = (DWORD) DateHeure.wSecond;
  *pCentiemes     = (DWORD) DateHeure.wMilliseconds/10;
  return TRUE;
  }

// --------------------------------------------------------------------------
BOOL TpsForcerDateHeure (DWORD Jour, DWORD Mois, DWORD Annee,
                            DWORD Heures, DWORD Minutes, DWORD Secondes, DWORD Centiemes)
  {
  BOOL  bOk;
  SYSTEMTIME DateHeure;

	DateHeure.wDay					= (WORD) Jour;       ;
	DateHeure.wMonth 				= (WORD) Mois;
	DateHeure.wYear   			= (WORD) Annee;
	DateHeure.wDayOfWeek   	= (WORD) 0; // ignor�
	DateHeure.wHour     		= (WORD) Heures;
	DateHeure.wMinute   		= (WORD) Minutes;
	DateHeure.wSecond   		= (WORD) Secondes;
	DateHeure.wMilliseconds = (WORD) Centiemes * 10;
  bOk = SetLocalTime (&DateHeure);
  return (bOk);
  }

// --------------------------------------------------------------------------
BOOL TpsLireDate (DWORD *Jour, DWORD *Mois, DWORD *Annee, DWORD *JourDeSemaine)
  {
  BOOL bOk;
  DWORD    wHeures;
  DWORD    wMinutes;
  DWORD    wSecondes;
  DWORD    wCentiemes;

  bOk = TpsLireDateHeure (Jour, Mois, Annee, JourDeSemaine, &wHeures, &wMinutes, &wSecondes, &wCentiemes);

  return (bOk);
  }

// --------------------------------------------------------------------------
BOOL TpsLireHeure (DWORD *Heures, DWORD *Minutes, DWORD *Secondes, DWORD *Centiemes)
  {
  BOOL bOk;
  DWORD    wJour;
  DWORD    wMois;
  DWORD    wAnnee;
  DWORD    wJourDeSemaine;

  bOk = TpsLireDateHeure (&wJour, &wMois, &wAnnee, &wJourDeSemaine, Heures, Minutes, Secondes, Centiemes);

  return (bOk);
  }


// --------------------------------------------------------------------------
BOOL TpsForcerDate (DWORD Jour, DWORD Mois, DWORD Annee)
  {
  BOOL bOk;
  DWORD    wHeures;
  DWORD    wMinutes;
  DWORD    wSecondes;
  DWORD    wCentiemes;

  bOk = TpsLireHeure (&wHeures, &wMinutes, &wSecondes, &wCentiemes);
  if (bOk)
    {
    bOk = TpsForcerDateHeure (Jour, Mois, Annee, wHeures, wMinutes, wSecondes, wCentiemes);
    }

  return (bOk);
  }

// --------------------------------------------------------------------------
BOOL TpsForcerHeure (DWORD Heures, DWORD Minutes, DWORD Secondes, DWORD Centiemes)
  {
  BOOL bOk;
  DWORD    wJour;
  DWORD    wMois;
  DWORD    wAnnee;
  DWORD    wJourDeSemaine;

  bOk = TpsLireDate (&wJour, &wMois, &wAnnee, &wJourDeSemaine);
  if (bOk)
    {
    bOk = TpsForcerDateHeure (wJour, wMois, wAnnee, Heures, Minutes, Secondes, Centiemes);
    }

  return (bOk);
  }


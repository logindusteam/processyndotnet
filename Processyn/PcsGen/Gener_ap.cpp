 /*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : GENER_AP.C                                               |
 |   Auteur  : AC							|
 |   Date    : 06/05/96                                                 |
 |   Version : 4.40							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "tipe_ap.h"
#include "mem.h"
#include "Descripteur.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "findbdex.h"
#include "GenereBD.h"
#include "Verif.h"
VerifInit;

#include "gener_ap.h"

// types pour l'interprétation
typedef struct
  {
  int numero_carte;
  LONG adresse_max_bit;
  LONG adresse_max_mot;
  } tx_applicom_adresse_max;

//---------------------------------------------------------------------------
static void maj_adresse_max_applicom (int numero_carte, int taille,
	DWORD genre_info, DWORD genre_num, LONG adresse)
  {
  tx_applicom_adresse_max* applicom_adresse_max;
  DWORD i;

  if (!existe_repere (bx_applicom_adresse_max))
    {
    cree_bloc (bx_applicom_adresse_max, sizeof (tx_applicom_adresse_max), 4);
    for (i = 1; i <= 4; i++)
      {
      applicom_adresse_max = (tx_applicom_adresse_max*)pointe_enr (szVERIFSource, __LINE__, bx_applicom_adresse_max, i);
      applicom_adresse_max->adresse_max_bit = 0;
      applicom_adresse_max->adresse_max_mot = 0;
      }
    }
  applicom_adresse_max = (tx_applicom_adresse_max*)pointe_enr (szVERIFSource, __LINE__, bx_applicom_adresse_max, (DWORD)numero_carte);
  switch (genre_info)
    {
    case c_res_logique :
      if (applicom_adresse_max->adresse_max_bit < (adresse + taille - 1))
        {
        applicom_adresse_max->adresse_max_bit = adresse + taille - 1;
        }
      break; // logique

    case c_res_numerique :
      switch (genre_num)
        {
        case c_res_entier:
        case c_res_mots:
          if (applicom_adresse_max->adresse_max_mot < (adresse + taille - 1))
            {
            applicom_adresse_max->adresse_max_mot = adresse + taille - 1;
            }
          break; // entier et mots

        case c_res_reels:
          if (applicom_adresse_max->adresse_max_mot < (adresse + (taille*2) - 1))
            {
            applicom_adresse_max->adresse_max_mot = adresse + (taille*2) - 1;
            }
          break; // reels

        default:
          break;
        } // switch genre_num
      break; // numerique

    case c_res_message :
      if (applicom_adresse_max->adresse_max_mot < (adresse + taille - 1))
        {
        applicom_adresse_max->adresse_max_mot = adresse + taille - 1;
        }
      break; // message

    default:
      break;
    } // switch genre_info
  } // maj_adresse_max_applicom

//---------------------------------------------------------------------------
static int nombre_de_mot (DWORD longueur)
  {
  int iRetour;

  if ((longueur % 2) != 0)
    {
    iRetour = (int)((longueur / 2) + 1);
    }
  else
    {
    iRetour = (int)(longueur / 2);
    }
  return (iRetour);
  }

//---------------------------------------------------------------------------
// procedure qui permet le transfert des infos simples et
// tableaux (sauf messages) uniquement sortie
// procedure commune pour les infos normales et declarations
//
static DWORD genere_ap_bis (PGEO_APPLICOM	ptr_val_geo, DWORD numero_carte_cour,
                           DWORD genre_cour, DWORD genre_num_cour)
  {
  DWORD wErreur;
  C_APPLICOM_S*donnees_ap_s;
  C_APPLICOM_TAB *donnees_ap_s_tab;
  C_APPLICOM_S_MES *donnees_ap_s_mes;
  C_APPLICOM_TAB_S_REFLET_MES*donnees_ap_s_tab_reflet_mes;
  C_APPLICOM_TAB_S_REFLET *donnees_ap_s_tab_reflet;
  X_APPLICOM_S* donneex_ap_s;
  X_APPLICOM_TAB_MES* donneex_ap_tab_mes;
  X_APPLICOM_TAB_S_REFLET_MES* donneex_ap_s_tab_mes_reflet;

  wErreur = 0;
  switch (ptr_val_geo->numero_repere)
    {
    case b_ap_s:
    case b_ap_s_reflet:
      switch (genre_cour)
        {
        case c_res_logique :
          insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_log, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log) + 1);
          donneex_ap_s = (X_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_log, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log));
          wErreur = recherche_index_execution (ptr_val_geo->pos_es, &donneex_ap_s->index_es_associe);
          break;

        case c_res_numerique :
          switch (genre_num_cour)
            {
            case c_res_entier :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_entier, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier) + 1);
              donneex_ap_s = (X_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_entier, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier));
              break;
            case c_res_mots :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_mot, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot) + 1);
              donneex_ap_s = (X_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_mot, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot));
              break;
            case c_res_reels :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_reel, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel) + 1);
              donneex_ap_s = (X_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_reel, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel));
              break;
            } // switch genre_num_cour
          wErreur = recherche_index_execution (ptr_val_geo->pos_es, &donneex_ap_s->index_es_associe);
          donneex_ap_s->index_es_associe = donneex_ap_s->index_es_associe - ContexteGen.nbr_logique;
          break; // numerique

        default:
          break;
        } // switch
      donnees_ap_s = (C_APPLICOM_S*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
      donneex_ap_s->adresse = donnees_ap_s->adresse;
      maj_adresse_max_applicom (numero_carte_cour, 1, genre_cour,
                                genre_num_cour, donneex_ap_s->adresse);
      donneex_ap_s->numero_carte = numero_carte_cour;
      break; // b_ap_s,b_ap_s_reflet

    case b_ap_s_mes:
    case b_ap_s_reflet_mes :
      insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_mes, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_mes) + 1);
      donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_mes, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_mes));
      donnees_ap_s_mes = (C_APPLICOM_S_MES*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
      donneex_ap_tab_mes->adresse = donnees_ap_s_mes->adresse;
      donneex_ap_tab_mes->numero_carte = numero_carte_cour;
      if (ptr_val_geo->numero_repere == b_ap_s_mes)
        {
        donneex_ap_tab_mes->nbre = nombre_de_mot (donnees_ap_s_mes->longueur);
        donneex_ap_tab_mes->longueur = donnees_ap_s_mes->longueur;
        maj_adresse_max_applicom (numero_carte_cour,donneex_ap_tab_mes->nbre,
                                  genre_cour,genre_num_cour, donneex_ap_tab_mes->adresse);
      	}
      else
        {
        donneex_ap_tab_mes->nbre = 0;
        donneex_ap_tab_mes->longueur = 0;
        maj_adresse_max_applicom (numero_carte_cour, 40, genre_cour,
                                  genre_num_cour, donneex_ap_tab_mes->adresse);
        }
      wErreur = recherche_index_execution (ptr_val_geo->pos_es, &donneex_ap_tab_mes->index_es_associe);
      donneex_ap_tab_mes->index_es_associe = donneex_ap_tab_mes->index_es_associe - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
      break; // b_ap_s_mes,b_ap_s_reflet_mes

    case b_ap_s_tab :
      switch (genre_cour)
        {
        case c_res_logique :
          insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_log_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log_tab) + 1);
          donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_log_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log_tab));
          donnees_ap_s_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
          donneex_ap_tab_mes->nbre = (int)(donnees_ap_s_tab->taille);
          break;

        case c_res_numerique :
          switch (genre_num_cour)
            {
            case c_res_entier :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_entier_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab) + 1);
              donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab));
              donnees_ap_s_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
              donneex_ap_tab_mes->nbre = (int)(donnees_ap_s_tab->taille);
              break;
            case c_res_mots :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_mot_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab) + 1);
              donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab));
              donnees_ap_s_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
              donneex_ap_tab_mes->nbre = (int)(donnees_ap_s_tab->taille);
              break;
            case c_res_reels :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_reel_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab) + 1);
              donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab));
              donnees_ap_s_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
              donneex_ap_tab_mes->nbre = (int)(donnees_ap_s_tab->taille*2);
              break;
            } // fin case
          break;

        default:
          break;
        } // switch
      donneex_ap_tab_mes->adresse = donnees_ap_s_tab->adresse;
      maj_adresse_max_applicom (numero_carte_cour, (int)(donnees_ap_s_tab->taille),
                                genre_cour,genre_num_cour, donneex_ap_tab_mes->adresse);
      wErreur = recherche_index_tableau_i (ptr_val_geo->pos_es,1, &donneex_ap_tab_mes->index_es_associe);
      wErreur = recherche_index_execution (donnees_ap_s_tab->i_cmd_rafraich, &donneex_ap_tab_mes->i_cmd_rafraich);
      donneex_ap_tab_mes->longueur = donnees_ap_s_tab->taille;
      donneex_ap_tab_mes->numero_carte = numero_carte_cour;
      break; // b_ap_s_tab

    case b_ap_s_tab_reflet :
      switch (genre_cour)
        {
        case c_res_logique :
          insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_log_tab_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log_tab_reflet) + 1);
          donneex_ap_s_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_log_tab_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_log_tab_reflet));
					donnees_ap_s_tab_reflet = (C_APPLICOM_TAB_S_REFLET*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
          break;

        case c_res_numerique :
          switch (genre_num_cour)
            {
            case c_res_entier :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_entier_tab_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab_reflet) + 1);
              donneex_ap_s_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_entier_tab_reflet));
							donnees_ap_s_tab_reflet = (C_APPLICOM_TAB_S_REFLET*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
              break;
            case c_res_mots :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_mot_tab_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab_reflet) + 1);
              donneex_ap_s_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_mot_tab_reflet));
							donnees_ap_s_tab_reflet = (C_APPLICOM_TAB_S_REFLET*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
              break;
            case c_res_reels :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_num_reel_tab_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab_reflet) + 1);
              donneex_ap_s_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_num_reel_tab_reflet));
							donnees_ap_s_tab_reflet = (C_APPLICOM_TAB_S_REFLET*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
              break;
            } // fin case
          break;

        default:
          break;
        } // switch
      donneex_ap_s_tab_mes_reflet->adresse = donnees_ap_s_tab_reflet->adresse;
      maj_adresse_max_applicom (numero_carte_cour,1, genre_cour,
                                genre_num_cour, donneex_ap_s_tab_mes_reflet->adresse);
      wErreur = recherche_index_tableau_i (ptr_val_geo->pos_es,
					   donnees_ap_s_tab_reflet->indice,
					   &donneex_ap_s_tab_mes_reflet->index_es_associe);
      donneex_ap_s_tab_mes_reflet->numero_carte = numero_carte_cour;
      break; // b_ap_s_tab_reflet

    case b_ap_s_tab_reflet_mes:
      insere_enr (szVERIFSource, __LINE__, 1, bx_ap_s_tab_mes_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_tab_mes_reflet) + 1);
      donneex_ap_s_tab_mes_reflet = (X_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_s_tab_mes_reflet, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_s_tab_mes_reflet));
      donnees_ap_s_tab_reflet_mes = (C_APPLICOM_TAB_S_REFLET_MES*)pointe_enr (szVERIFSource, __LINE__, ptr_val_geo->numero_repere, ptr_val_geo->pos_specif_es);
      donneex_ap_s_tab_mes_reflet->adresse = donnees_ap_s_tab_reflet_mes->adresse;
      maj_adresse_max_applicom (numero_carte_cour, 40, genre_cour,
                                genre_num_cour, donneex_ap_s_tab_mes_reflet->adresse);
      wErreur = recherche_index_tableau_i (ptr_val_geo->pos_es,
                                           donnees_ap_s_tab_reflet_mes->indice,
					   &donneex_ap_s_tab_mes_reflet->index_es_associe);
      donneex_ap_s_tab_mes_reflet->numero_carte = numero_carte_cour;
      break;

    default:
      break;
    } // switch ptr_val_geo->numero_repere
  return (wErreur);
  } // genere_ap_bis

//--------------------------------------------------------------------------
DWORD genere_ap (DWORD *ligne)
  {
  C_APPLICOM_E*donnees_ap_e;
  C_APPLICOM_TAB*donnees_ap_e_tab;
  C_APPLICOM_E_MES*donnees_ap_e_mes;
  C_APPLICOM_FCT_CYC*donnees_ap_fct_cyc;
  X_APPLICOM_E* donneex_ap_e;
  X_APPLICOM_TAB_MES*donneex_ap_tab_mes;
  X_APPLICOM_FCT_CYC*donneex_ap_fct_cyc;
  GEO_APPLICOM val_geo;
  PGEO_APPLICOM	ptr_val_geo;
  PTITRE_PARAGRAPHE	titre_paragraf;
  PSPEC_TITRE_PARAGRAPHE_APPLICOM ptr_spec_titre_paragraf_ap;
  DWORD genre_cour;
  DWORD genre_num_cour;
  DWORD ligne_cour;
  DWORD erreur;
  int numero_carte_cour;


  erreur = 0;
  if (existe_repere (b_geo_ap))
    {
    ligne_cour = 0;
    while ((ligne_cour < nb_enregistrements (szVERIFSource, __LINE__, b_geo_ap)) && (erreur == 0))
      {
      ligne_cour++;
      ptr_val_geo = (PGEO_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_geo_ap, ligne_cour);
      val_geo = (*ptr_val_geo);
      switch (val_geo.numero_repere)
        {
        case b_pt_constant :
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;

        case b_titre_paragraf_ap :
          titre_paragraf = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf_ap, val_geo.pos_paragraf);
          switch (titre_paragraf->IdParagraphe)
            {
            case carte_ap :
              ptr_spec_titre_paragraf_ap = (PSPEC_TITRE_PARAGRAPHE_APPLICOM)pointe_enr (szVERIFSource, __LINE__, b_spec_titre_paragraf_ap, val_geo.pos_specif_paragraf);
              numero_carte_cour = (int)(ptr_spec_titre_paragraf_ap->numero_carte);
              break; // carte_ap

            case fct_cyclique_ap:
              genre_cour = c_res_logique;
              if (!existe_repere (bx_ap_fct_cyc))
								{
								cree_bloc (bx_ap_fct_cyc, sizeof (X_APPLICOM_FCT_CYC), 0);
								}
							break; // cyclique

/* $$
            case fct_cyclique_ap_salve:
              genre_cour = c_res_logique;
              if (!existe_repere (bx_ap_fct_cyc_salve))
								{
								cree_bloc (bx_ap_fct_cyc_salve, sizeof (X_APPLICOM_FCT_CYC), 0);
								}
							break; // cyclique
*/
						case entre_num:
              genre_cour = c_res_numerique;
							genre_num_cour = c_res_entier;
              if (!existe_repere (bx_ap_e_num_entier))
                {
                cree_bloc (bx_ap_e_num_entier, sizeof (X_APPLICOM_E), 0);
                }
              if (!existe_repere (bx_ap_e_num_entier_tab) )
                {
                cree_bloc (bx_ap_e_num_entier_tab, sizeof (X_APPLICOM_TAB_MES), 0);
                }
              break;

						case entre_num_mot :
              genre_cour = c_res_numerique;
							genre_num_cour = c_res_mots;
              if (!existe_repere (bx_ap_e_num_mot))
                {
                cree_bloc (bx_ap_e_num_mot, sizeof (X_APPLICOM_E), 0);
                }
              if (!existe_repere (bx_ap_e_num_mot_tab))
                {
                cree_bloc (bx_ap_e_num_mot_tab, sizeof (X_APPLICOM_TAB_MES), 0);
                }
              break;

            case entre_num_reel :
              genre_cour = c_res_numerique;
							genre_num_cour = c_res_reels;
              if (!existe_repere (bx_ap_e_num_reel))
                {
                cree_bloc (bx_ap_e_num_reel, sizeof (X_APPLICOM_E), 0);
                }
              if (!existe_repere (bx_ap_e_num_reel_tab))
                {
                cree_bloc (bx_ap_e_num_reel_tab, sizeof (X_APPLICOM_TAB_MES), 0);
                }
              break;

            case entre_log :
              genre_cour = c_res_logique;
              if (!existe_repere (bx_ap_e_log))
                {
                cree_bloc (bx_ap_e_log, sizeof (X_APPLICOM_E), 0);
                }
              if (!existe_repere (bx_ap_e_log_tab))
                {
                cree_bloc (bx_ap_e_log_tab, sizeof (X_APPLICOM_TAB_MES), 0);
                }
              break;

            case entre_mes :
              genre_cour = c_res_message;
              if (!existe_repere (bx_ap_e_mes))
                {
                cree_bloc (bx_ap_e_mes, sizeof (X_APPLICOM_TAB_MES), 0);
								}
              break;

						case sorti_num :
              genre_cour = c_res_numerique;
							genre_num_cour = c_res_entier;
              if (!existe_repere (bx_ap_s_num_entier))
                {
                cree_bloc (bx_ap_s_num_entier, sizeof (X_APPLICOM_S), 0);
                }
              if (!existe_repere (bx_ap_s_num_entier_tab))
								{
                cree_bloc (bx_ap_s_num_entier_tab, sizeof (X_APPLICOM_TAB_MES), 0);
								}
							if (!existe_repere (bx_ap_s_num_entier_tab_reflet))
                {
								cree_bloc (bx_ap_s_num_entier_tab_reflet, sizeof (X_APPLICOM_TAB_S_REFLET_MES), 0);
                }
              break;

						case sorti_num_mot :
              genre_cour = c_res_numerique;
							genre_num_cour = c_res_mots;
              if (!existe_repere (bx_ap_s_num_mot))
								{
                cree_bloc (bx_ap_s_num_mot, sizeof (X_APPLICOM_S), 0);
								}
              if (!existe_repere (bx_ap_s_num_mot_tab))
								{
                cree_bloc (bx_ap_s_num_mot_tab, sizeof (X_APPLICOM_TAB_MES), 0);
								}
							if (!existe_repere (bx_ap_s_num_mot_tab_reflet))
                {
								cree_bloc (bx_ap_s_num_mot_tab_reflet, sizeof (X_APPLICOM_TAB_S_REFLET_MES), 0);
                }
              break;

            case sorti_num_reel :
              genre_cour = c_res_numerique;
								genre_num_cour = c_res_reels;
              if (!existe_repere (bx_ap_s_num_reel))
								{
                cree_bloc (bx_ap_s_num_reel, sizeof (X_APPLICOM_S), 0);
								}
              if (!existe_repere (bx_ap_s_num_reel_tab))
								{
                cree_bloc (bx_ap_s_num_reel_tab, sizeof (X_APPLICOM_TAB_MES), 0);
								}
							if (!existe_repere (bx_ap_s_num_reel_tab_reflet))
                {
								cree_bloc (bx_ap_s_num_reel_tab_reflet, sizeof (X_APPLICOM_TAB_S_REFLET_MES), 0);
                }
              break;

            case sorti_log :
              genre_cour = c_res_logique;
              if (!existe_repere (bx_ap_s_log))
                {
                cree_bloc (bx_ap_s_log, sizeof (X_APPLICOM_S), 0);
								}
							if (!existe_repere (bx_ap_s_log_tab) )
                {
                cree_bloc (bx_ap_s_log_tab, sizeof (X_APPLICOM_TAB_MES), 0);
                }
							if (!existe_repere (bx_ap_s_log_tab_reflet) )
                {
								cree_bloc (bx_ap_s_log_tab_reflet, sizeof (X_APPLICOM_TAB_S_REFLET_MES), 0);
                }
              break;

            case sorti_mes :
              genre_cour = c_res_message;
              if (!existe_repere (bx_ap_s_mes))
                {
                cree_bloc (bx_ap_s_mes, sizeof (X_APPLICOM_TAB_MES), 0);
                }
              if (!existe_repere (bx_ap_s_tab_mes_reflet))
                {
                cree_bloc (bx_ap_s_tab_mes_reflet, sizeof (X_APPLICOM_TAB_S_REFLET_MES), 0);
                }
              break;

            default:
              break;
            } // switch titre_par.tipe
          break;

        case b_ap_e :
          switch (genre_cour)
            {
            case c_res_logique :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_log, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_log) + 1);
              donneex_ap_e = (X_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_log, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_log));
              erreur = recherche_index_execution (val_geo.pos_es, &donneex_ap_e->index_es_associe);
              break;

            case c_res_numerique :
              switch (genre_num_cour)
                {
								case c_res_entier :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_num_entier, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_entier) + 1);
                  donneex_ap_e = (X_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_entier, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_entier));
                  break;
								case c_res_mots :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_num_mot, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_mot) + 1);
                  donneex_ap_e = (X_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_mot, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_mot));
                  break;
								case c_res_reels :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_num_reel, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_reel) + 1);
                  donneex_ap_e = (X_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_reel, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_reel));
                  break;
                default:
                  break;
								} // switch genre_num_cour
              erreur = recherche_index_execution (val_geo.pos_es, &donneex_ap_e->index_es_associe);
              donneex_ap_e->index_es_associe = donneex_ap_e->index_es_associe - ContexteGen.nbr_logique;
              break; // numerique

            default:
              break;
            } // genre_cour

          donnees_ap_e = (C_APPLICOM_E*)pointe_enr (szVERIFSource, __LINE__, b_ap_e, val_geo.pos_specif_es);
          donneex_ap_e->numero_carte = numero_carte_cour;
          erreur = recherche_index_execution (donnees_ap_e->i_cmd_rafraich,
                                              &donneex_ap_e->i_cmd_rafraich);
          donneex_ap_e->adresse = donnees_ap_e->adresse;
          maj_adresse_max_applicom (numero_carte_cour,1,
                                    genre_cour,genre_num_cour,
                                    donneex_ap_e->adresse);
          break; // b_ap_e

        case b_ap_e_mes :
          insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_mes, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_mes) + 1);
          donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_mes, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_mes));
          erreur = recherche_index_execution (val_geo.pos_es, &donneex_ap_tab_mes->index_es_associe);
          donneex_ap_tab_mes->index_es_associe = donneex_ap_tab_mes->index_es_associe
                                                  - ContexteGen.nbr_logique - ContexteGen.nbr_numerique;
          donnees_ap_e_mes = (C_APPLICOM_E_MES*)pointe_enr (szVERIFSource, __LINE__, b_ap_e_mes, val_geo.pos_specif_es);
          donneex_ap_tab_mes->numero_carte = numero_carte_cour;
          donneex_ap_tab_mes->longueur = donnees_ap_e_mes-> longueur;
          donneex_ap_tab_mes->nbre = nombre_de_mot(donnees_ap_e_mes->longueur);
          erreur = recherche_index_execution (donnees_ap_e_mes->i_cmd_rafraich,
                                              &donneex_ap_tab_mes->i_cmd_rafraich);
          donneex_ap_tab_mes->adresse = donnees_ap_e_mes->adresse;
          maj_adresse_max_applicom (numero_carte_cour,donneex_ap_tab_mes->nbre,
                                    genre_cour, genre_num_cour,
                                    donneex_ap_tab_mes->adresse);
          break; // b_ap_e_mes

        case b_ap_e_tab :
          switch (genre_cour)
            {
            case c_res_logique :
              insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_log_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_log_tab) + 1);
              donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_log_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_log_tab));
              donnees_ap_e_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab, val_geo.pos_specif_es);
              donneex_ap_tab_mes->nbre = (int)(donnees_ap_e_tab->taille);
              break;

            case c_res_numerique :
              switch (genre_num_cour)
                {
                case c_res_entier :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_num_entier_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_entier_tab) + 1);
                  donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_entier_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_entier_tab));
                  donnees_ap_e_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab, val_geo.pos_specif_es);
                  donneex_ap_tab_mes->nbre = (int)(donnees_ap_e_tab->taille);
                  break;
                case c_res_mots :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_num_mot_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_mot_tab) + 1);
                  donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_mot_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_mot_tab));
                  donnees_ap_e_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab, val_geo.pos_specif_es);
                  donneex_ap_tab_mes->nbre = (int)(donnees_ap_e_tab->taille);
                  break;
                case c_res_reels :
                  insere_enr (szVERIFSource, __LINE__, 1, bx_ap_e_num_reel_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_reel_tab) + 1);
                  donneex_ap_tab_mes = (X_APPLICOM_TAB_MES*)pointe_enr (szVERIFSource, __LINE__, bx_ap_e_num_reel_tab, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_e_num_reel_tab));
                  donnees_ap_e_tab = (C_APPLICOM_TAB*)pointe_enr (szVERIFSource, __LINE__, b_ap_e_tab, val_geo.pos_specif_es);
                  donneex_ap_tab_mes->nbre = (int)(donnees_ap_e_tab->taille*2);
                  break;
                default:
                  break;
                } // switch
              break; // numerique
            default:
              break;
            } // switch
          donneex_ap_tab_mes->adresse = donnees_ap_e_tab->adresse;
          donneex_ap_tab_mes->longueur = donnees_ap_e_tab->taille;
          erreur = recherche_index_tableau_i (val_geo.pos_es, 1, &donneex_ap_tab_mes->index_es_associe);
          erreur = recherche_index_execution (donnees_ap_e_tab->i_cmd_rafraich,
                                              &donneex_ap_tab_mes->i_cmd_rafraich);
          donneex_ap_tab_mes->numero_carte = numero_carte_cour;
          maj_adresse_max_applicom (numero_carte_cour,(int)(donneex_ap_tab_mes->longueur),
                                    genre_cour,genre_num_cour,
                                    donneex_ap_tab_mes->adresse);
          break; // b_ap_e_tab

        case b_ap_s:
        case b_ap_s_mes:
        case b_ap_s_tab :
          genere_ap_bis (&val_geo, numero_carte_cour, genre_cour, genre_num_cour);
          break;

        case b_ap_s_reflet:
        case b_ap_s_reflet_mes:
        case b_ap_s_tab_reflet:
        case b_ap_s_tab_reflet_mes :
          genere_ap_bis (&val_geo, numero_carte_cour, genre_cour, genre_num_cour);
          break;

        case b_ap_fct_cyc :
          insere_enr (szVERIFSource, __LINE__, 1, bx_ap_fct_cyc, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_fct_cyc) + 1);
          donneex_ap_fct_cyc = (X_APPLICOM_FCT_CYC*)pointe_enr (szVERIFSource, __LINE__, bx_ap_fct_cyc, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_fct_cyc));
          erreur = recherche_index_execution (val_geo.pos_es, &donneex_ap_fct_cyc->index_es_associe);
          donnees_ap_fct_cyc = (C_APPLICOM_FCT_CYC*)pointe_enr (szVERIFSource, __LINE__, b_ap_fct_cyc, val_geo.pos_specif_es);
          donneex_ap_fct_cyc->numero_canal = (int)(donnees_ap_fct_cyc->numero_canal);
          donneex_ap_fct_cyc->numero_fonction_cyclique = (int)(donnees_ap_fct_cyc->numero_fonction_cyclique);
          break;
/* $$
        case b_ap_fct_cyc_salve :
					{
					X_APPLICOM_FCT_CYC *donneex_ap_fct_cyc_salve;
					C_APPLICOM_FCT_CYC *donnees_ap_fct_cyc_salve;

          insere_enr (szVERIFSource, __LINE__, 1, bx_ap_fct_cyc_salve, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_fct_cyc_salve) + 1);
          donneex_ap_fct_cyc_salve = pointe_enr (szVERIFSource, __LINE__, bx_ap_fct_cyc_salve, nb_enregistrements (szVERIFSource, __LINE__, bx_ap_fct_cyc_salve));
          erreur = recherche_index_execution (val_geo.pos_es, &donneex_ap_fct_cyc_salve->index_es_associe);
          donnees_ap_fct_cyc_salve = pointe_enr (szVERIFSource, __LINE__, b_ap_fct_cyc_salve, val_geo.pos_specif_es);
          donneex_ap_fct_cyc_salve->numero_canal = (int)(donnees_ap_fct_cyc_salve->numero_canal);
          donneex_ap_fct_cyc_salve->numero_fonction_cyclique = (int)(donnees_ap_fct_cyc_salve->numero_fonction_cyclique);
					}
          break;
*/
        default:
          break;
        } // switch val_geo.num_repere
      } // while ligne_cour < nb_enr
    } // existe b_geo_ap

  if (erreur == 0)
    {
    if (existe_repere (b_ap_e))
      {
      enleve_bloc (b_ap_e);
      }
    if (existe_repere (b_ap_e_tab))
      {
      enleve_bloc (b_ap_e_tab);
      }
    if (existe_repere (b_ap_s))
      {
      enleve_bloc (b_ap_s);
      }
    if (existe_repere (b_ap_s_tab))
      {
      enleve_bloc (b_ap_s_tab);
      }
    if (existe_repere (b_ap_e_mes))
      {
      enleve_bloc (b_ap_e_mes);
      }
    if (existe_repere (b_ap_s_mes))
      {
      enleve_bloc (b_ap_s_mes);
      }
    if (existe_repere (b_titre_paragraf_ap))
      {
      enleve_bloc (b_titre_paragraf_ap);
      }
    if (existe_repere (b_ap_s_reflet))
      {
      enleve_bloc (b_ap_s_reflet);
      }
    if (existe_repere (b_ap_s_tab_reflet))
      {
      enleve_bloc (b_ap_s_tab_reflet);
      }
    if (existe_repere (b_ap_s_tab_reflet_mes))
      {
      enleve_bloc (b_ap_s_tab_reflet_mes);
      }
    if (existe_repere (b_ap_fct_cyc))
      {
      enleve_bloc (b_ap_fct_cyc);
      }
/* $$
    if (existe_repere (b_ap_fct_cyc_salve))
      {
      enleve_bloc (b_ap_fct_cyc_salve);
      }
*/
    if (existe_repere (b_spec_titre_paragraf_ap))
      {
      enleve_bloc (b_spec_titre_paragraf_ap);
      }
    } // erreur = 0

  if (erreur != 0)
    {
    (*ligne) = ligne_cour;
    }
  return (erreur);
  }

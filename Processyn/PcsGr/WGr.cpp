// -----------------------------------------------------------------------
// Wgr.c
// Gestion de la fen�tre principale de PCSGR

#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "MemMan.h"
#include "PathMan.h"
#include "DocMan.h"
#include "UStr.h"
#include "UEnv.h"
#include "mem.h"
#include "MenuMan.h"
#include "pcsspace.h"
#include "appligr.h"          // uniquement en lecture pour acc�s contexte
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "G_Objets.h"
#include "actiongr.h"
#include "WBarOuti.h"
#include "g_sys.h"
#include "bdgr.h"             // toutes les �critures doivent imp�rativement
#include "bdanmgr.h"          // uniquement pour type animation appel boite
#include "BdPageGr.h"
#include "ctxtgr.h"
#include "WEditSyn.h"
#include "idmenugr.h"           // �tre effectu�es par une commande de actiongr
#include "pcsfont.h"

#include "gerebdc.h"
#include "cmdpcs.h"
#include "pcsdlg.h"
#include "pcsdlggr.h"
#include "IdGr.h"
#include "IdGrLng.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "WCoord.h"		// fen�tre de coordonn�es
#include "WEchanti.h"	// fen�tre echantillon couleur et styles
#include "WCouleur.h"	// fen�tre de couleurs
#include "WStyleLi.h"	// fen�tre de style ligne
#include "WStyleRe.h"	// fen�tre de style ligne
#include "VersMan.h"
#include "Verif.h"

#include "WGr.h"


VerifInit;

#define CAR_SEPARATEUR_TITRE_PAGE "-"
#define NB_CAR_TITRE_PAGE_DLG 50

// Variables globales
DWORD wEspaceH = DEFAUT_ESPACEMENT_H;
DWORD wEspaceV = DEFAUT_ESPACEMENT_V;

// Variables locales
static const char szClasseGr [] = "ClasseGr";

// Gestion des barres d'outils diverses utilis�es dans ce module
typedef struct
  {
	BOOL	bFenetre;
  HBO		hbo;
	HWND	hwnd;
  BOOL	bVisible;
  } BARRE_OUTIL_GR;


// Handles d'objets globaux
static BARRE_OUTIL_GR BODessinPlein;
static BARRE_OUTIL_GR BODessinVide;
static BARRE_OUTIL_GR BOOperation;
static BARRE_OUTIL_GR BOStyleLigne;
static BARRE_OUTIL_GR BOStyleRemplissage;
static BARRE_OUTIL_GR BOAlignement;
static BARRE_OUTIL_GR BOAnimRL;

static BARRE_OUTIL_GR BOCouleur;
static BARRE_OUTIL_GR BOCoord;
static const char szTitreBOOperation [] = "Op�rations";
static const char szTitreBOAnimRL [] = "Reflets";
static const char szTitreBODessin [] = "Objets";

// -----------------------------------------------------------------------
void animer_coord_gr (int coord_x, int coord_y)
	{
	if (BOCoord.hwnd)
		MAJCoord (BOCoord.hwnd, coord_x, coord_y);
	}

// -----------------------------------------------------------------------
static void message_operateur_ouverture (void)
  {
  char message [c_nb_car_message_err];

  bLngLireErreur (397, message);
  dlg_erreur (message);
  }

// -----------------------------------------------------------------------
static void initialise_num_page_dlg (DWORD *index, char *chaine)
  {
  DWORD nbr_page;
  BOOL trouve_page;
  char chaine_titre[c_NB_CAR_TITRE_PAGE_GR+1];

  nbr_page = bdgr_nombre_pages ();
  trouve_page = FALSE;
  while  (((*index) <= nbr_page) && (!trouve_page))
    {
    trouve_page = bdgr_existe_page (*index);
    if (trouve_page)
      {
      StrDWordToStr (chaine, (*index));
      if (bdgr_lire_titre_page_direct ((*index), chaine_titre))
        {
        StrConcat (chaine," ");
        StrConcat (chaine, CAR_SEPARATEUR_TITRE_PAGE);
        StrConcat (chaine, " ");
        StrConcat (chaine, chaine_titre);
        }
      }
    (*index)++;
    }
  if (!trouve_page)
    {
    (*index) = 0;
    }
  }

// -----------------------------------------------------------------------
static BOOL extraire_numero_page (DWORD *page_gr, char *texte)
  {
  BOOL saisie_valide;
  BOOL trouve;
  int position;
  char texte_num [NB_CAR_TITRE_PAGE_DLG];

  saisie_valide = FALSE;
  if (!StrIsNull (texte))
    {
    trouve = FALSE;
    for (position = 1; ((position <= (int)StrLength (texte)) && (!trouve)); position++)
      {
      trouve = (BOOL) (texte[position - 1] == CAR_SEPARATEUR_TITRE_PAGE[0]);
      }
    if (trouve)
      {
      position--;
      if ((position > 2) && (position < 6))
        {
        position--;
        StrSetNull (texte_num);
        StrExtract (texte_num, texte, 0, position);
        StrDeleteSpaces (texte_num);
        (*page_gr) = 0;
        StrToDWORD (page_gr, texte_num);
        saisie_valide = (BOOL) (*page_gr != 0);
        }
      }
    else
      {
      StrDeleteSpaces (texte);
      (*page_gr) = 0;
      StrToDWORD (page_gr, texte);
      saisie_valide = (BOOL) (*page_gr != 0);
      }
    }
  return (saisie_valide);
  }

// -----------------------------------------------------------------------
// r�cup�re tous les param�tres de la page de synoptique courante
static void initialise_style_fenetre (DLGGR_STYLE_FEN * pProp)
  {
  HBDGR hBdGr = get_ctxtgr_hBdGr();

	// R�cup�re les propri�t�s de fen�tre
  bdgr_lire_styles_page (hBdGr,
                         &pProp->bFenModale,
                         &pProp->bFenDialog,
                         &pProp->mode_affichage,
                         &pProp->facteur_zoom,
                         &pProp->mode_deformation,
                         &pProp->un_bord,
                         &pProp->un_titre,
                         pProp->titre,
                         &pProp->un_sysmenu,
                         &pProp->un_minimize,
                         &pProp->un_maximize,
                         &pProp->un_scroll_h,
                         &pProp->un_scroll_v,
                         &pProp->grille_x,
                         &pProp->grille_y);
	// bitmap de fond
  pProp->bBitmapFond = bdgr_lire_nom_bmp_fond_page (hBdGr, pProp->szPathBitmapFond);

	// couleur de fond
	pProp->dwCouleurFond = bdgr_lire_couleur_fond_page (hBdGr);
  }

// -----------------------------------------------------------------------
void initialiser_menu_gr (void)
  {
  if (get_ctxtgr_lancement_os())  //Appel direct
    {
		MenuAppliGriseItem (CMD_MENU_MODE_TEST, TRUE);
    //::SendMessage (hmenu,MM_SETITEMATTR,MAKEWPARAM (CMD_MENU_MODE_TEST, TRUE),MAKELPARAM (MIA_DISABLED, MIA_DISABLED));
    }
  else
    {
		MenuAppliGriseItem (CMD_MENU_OUVRE, TRUE);
    //::SendMessage (hmenu, MM_SETITEMATTR,MAKEWPARAM (CMD_MENU_OUVRE, TRUE),MAKELPARAM (MIA_DISABLED, MIA_DISABLED));
		MenuAppliGriseItem (CMD_MENU_SAUVE_SOUS, TRUE);
		// ::SendMessage (hmenu,MM_SETITEMATTR,MAKEWPARAM (CMD_MENU_SAUVE_SOUS, TRUE),MAKELPARAM (MIA_DISABLED, MIA_DISABLED));
    }
  }

// -----------------------------------------------------------------------
// Initialisation d'une barre d'outil graphique
static void InitBOGr (BARRE_OUTIL_GR * pbogr, HWND hwndInit, HBO hboInit, BOOL bVisible)
	{
	Verif (pbogr && (hwndInit || hboInit));

	pbogr->hwnd = hwndInit;
	pbogr->hbo = hboInit;
	pbogr->bVisible = bVisible;
	}

// -----------------------------------------------------------------------
// Montre une barre d'outil ou une Fen�tre tenant lieu de barre d'outil
static void MontreBOGr (BARRE_OUTIL_GR * pbogr, BOOL bVisible)
  {
	Verif (pbogr);
	// $$ pbogr->bVisible = bVisible;
  if (pbogr->hwnd)
    ::ShowWindow (pbogr->hwnd, bVisible ? SW_SHOW : SW_HIDE);
	else
		{
		if (pbogr->hbo)
			BOMontre (pbogr->hbo, bVisible);
		}
  }

// -----------------------------------------------------------------------
static void SupprimeBOGr (BARRE_OUTIL_GR * pbogr)
  {
	Verif (pbogr);
  if (pbogr->hwnd)
		{
    ::DestroyWindow (pbogr->hwnd);
		pbogr->hwnd = NULL;
		}
	else
		{
		if (pbogr->hbo)
			{
			BODetruit (&pbogr->hbo);
			pbogr->hbo = NULL;
			}
		}
	pbogr->bVisible = FALSE;
  }

// ---------------------------------------------------------------------
//                            Barres d'outils
// ---------------------------------------------------------------------


// -----------------------------------------------------------------------
// Cr�ation des barre d'outils de PcsGr ()
// -----------------------------------------------------------------------
BOOL bCreeLesBOGr (HWND hwndParent)
  {
  RECT	rectBOOperation;
  RECT	rectBO;
  RECT	rectAppli;

  ::GetWindowRect (hwndParent, &rectAppli);

	// Barre d'outil Op�rations
	InitBOGr (&BOOperation, NULL, hBOCree (szTitreBOOperation, TRUE, TRUE, FALSE, 0, 0), TRUE);

	// Affichage / effacement des barres d'outil
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_DESSIN), BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_AFFI_BO_DESSIN);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_PALETTE),BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_AFFI_BO_PALETTE);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_STL),    BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_AFFI_STL);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_STR),    BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_AFFI_STR);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_ALIGN),  BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_AFFI_ALIGN);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_COORD),  BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_AFFI_COORD);
	// Op�rations
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_SELECT), BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_MODE_SELECTION);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_POLICE), BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_POLICE);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_DEVANT), BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_DEVANT);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_DERRIERE),BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_DERRIERE);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_GROUP),  BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_GROUPER);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_DEGROUP),BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_DEGROUPER);
	bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_DUP),    BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_DUPLIQUER);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_REMPLI), BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_CHANGE_REMPLISSAGE);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_ROT),    BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_ROTATION);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_SYMH),   BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_SYMETRIE_H);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_OP_SYMV),   BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_SYMETRIE_V);
  bBOAjouteBouton (BOOperation.hbo, MAKEINTRESOURCE(IDB_CMD_BOUTON_ID),   BO_COL_SUIVANTE,BO_BOUTON, CMD_MENU_BMP_IDENTIFIANT);

  bBOOuvre (BOOperation.hbo, TRUE, hwndParent, 3, rectAppli.bottom, 0, BOOperation.bVisible);

	// Barre d'outils animation de reflets logiques
	InitBOGr (&BOAnimRL, NULL, hBOCree (szTitreBOAnimRL, TRUE, FALSE, FALSE, 0, 0), TRUE);

  bBOAjouteBouton (BOAnimRL.hbo, MAKEINTRESOURCE(IDB_CMD_ANM_VDIRL1), BO_COL_SUIVANTE,BO_COCHE, CMD_MENU_BMP_RL1);
  bBOAjouteBouton (BOAnimRL.hbo, MAKEINTRESOURCE(IDB_CMD_ANM_VDIRL2), BO_COL_SUIVANTE,BO_COCHE, CMD_MENU_BMP_RL2);
  bBOAjouteBouton (BOAnimRL.hbo, MAKEINTRESOURCE(IDB_CMD_ANM_VDIRND), BO_COL_SUIVANTE,BO_COCHE, CMD_MENU_BMP_RN_DEP1);

  // $$ G�rer d�placements BOAnimRL et BOOperation simultan�s
  BOGetRect (BOOperation.hbo, &rectBOOperation);
  bBOOuvre(BOAnimRL.hbo, TRUE, hwndParent, rectBOOperation.right, rectAppli.bottom, 0, BOAnimRL.bVisible);
  BOCliqueBouton(BOAnimRL.hbo,CMD_MENU_BMP_RL1);

	// Barre d'outils Dessin vide
	InitBOGr (&BODessinVide, NULL, hBOCree (szTitreBODessin, TRUE, TRUE, FALSE, 0, 0), FALSE);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_SUBST_L),	BO_NOUVELLE_LN, BO_BOUTON,CMD_MENU_BMP_MODE_TRACE);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_TEXT),			BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_TEXTE_H);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_LIGNE),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_LIGNE);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_CERCL),		BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CERCLE);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_RECT),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_RECTANGLE);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_DCERCLH),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_D_CERCLE_H);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_DCERCLV),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_D_CERCLE_V);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_ARCRECT),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_Q_CERCLE_R);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_ARC),			BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_Q_CERCLE);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_VANNEV),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_VANNE_V);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_VANNEH),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_VANNE_H);
  bBOAjouteBouton (BODessinVide.hbo,MAKEINTRESOURCE(IDB_CMD_DV_ISOCELE_H),BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_ISOCELE_H);
  bBOAjouteBouton (BODessinVide.hbo,MAKEINTRESOURCE(IDB_CMD_DV_ISOCELE_V),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_ISOCELE_V);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_TRIANGL),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_TRIANGLE);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_DV_POLY),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_POLYLIGNE);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIEL1),		BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_EL1);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIEL2),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_EL_BITMAP);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIEM1),		BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_EM1);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIEN1),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_EN1);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIRM1),		BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_RM_TEXT1);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIRN1),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_RM_NUM1);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDICOURC),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_RN_COURBE_C);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIRNB),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_RN_BAR1);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDICOURA),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_RN_COURBE_A);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDICOURT),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_RN_COURBE_T);

  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_STATIC  ),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_STATIC  );
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_EDIT    ),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_EDIT    );
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_GROUP   ),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_GROUP   );
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_BOUTON  ),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_BOUTON  );
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_CHECK   ),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_CHECK   );
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_RADIO   ),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_RADIO   );
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_COMBO   ),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_COMBO   );
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_LIST    ),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_LIST    );
  //bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_SCROLL_H),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_SCROLL_H);
  //bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_SCROLL_V),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_SCROLL_V);
  bBOAjouteBouton (BODessinVide.hbo, MAKEINTRESOURCE(IDB_CMD_D_BOUTON_VAL),	BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_CONT_BOUTON_VAL);
  bBOOuvre (BODessinVide.hbo, TRUE, hwndParent, rectBOOperation.left, rectBOOperation.bottom, 0, BODessinVide.bVisible);

	// Barre d'outils Dessin plein (recouvre la BO Dessin vide)
  BOGetRect (BODessinVide.hbo, &rectBO);

	InitBOGr (&BODessinPlein, NULL, hBOCree (szTitreBODessin, TRUE, TRUE, FALSE, 0, 0), TRUE);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_SUBST_L),		BO_NOUVELLE_LN, BO_BOUTON,CMD_MENU_BMP_MODE_TRACE);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_TEXT),			BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_TEXTE_H);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_LIGNE),			BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_LIGNE);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_CERCLF),		BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CERCLE_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_RECTF),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_RECTANGLE_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_DCERCLHF),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_D_CERCLE_PLEIN_H);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_DCERCLVF),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_D_CERCLE_PLEIN_V);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_ARCRECTF),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_Q_CERCLE_PLEIN_R);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_ARCF),			BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_Q_CERCLE_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_VANNEVF),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_VANNE_V_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_VANNEHF),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_VANNE_H_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_ISOCELE_H),BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_ISOCELE_H_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_ISOCELE_V),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_ISOCELE_V_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_TRIANGLF),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_TRIANGLE_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_DP_POLYF),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_POLYLIGNE_PLEIN);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIEL1),		BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_EL1);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIEL2),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_EL_BITMAP);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIEM1),		BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_EM1);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIEN1),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_EN1);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIRM1),		BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_RM_TEXT1);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIRN1),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_RM_NUM1);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDICOURC),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_RN_COURBE_C);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDIRNB),		BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_RN_BAR1);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDICOURA),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_RN_COURBE_A);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_VDICOURT),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_RN_COURBE_T);
  
	bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_STATIC  ),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_STATIC  );
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_EDIT    ),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_EDIT    );
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_GROUP   ),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_GROUP   );
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_BOUTON  ),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_BOUTON  );
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_CHECK   ),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_CHECK   );
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_RADIO   ),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_RADIO   );
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_COMBO   ),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_COMBO   );
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_LIST    ),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_LIST    );
  //bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_SCROLL_H),	BO_NOUVELLE_LN, BO_COCHE,CMD_MENU_BMP_CONT_SCROLL_H);
  //bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_SCROLL_V),	BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_CONT_SCROLL_V);
  bBOAjouteBouton (BODessinPlein.hbo, MAKEINTRESOURCE(IDB_CMD_D_BOUTON_VAL),	BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_CONT_BOUTON_VAL);
  bBOOuvre (BODessinPlein.hbo, TRUE, hwndParent, rectBO.left, rectBO.top, 0, BODessinPlein.bVisible);

	BOValideBouton(BODessinVide.hbo, CMD_MENU_BMP_EL_BITMAP, FALSE);
  BOValideBouton(BODessinPlein.hbo, CMD_MENU_BMP_EL_BITMAP, FALSE);

	// Cr�ation de la barre d'outil style de remplissage
	InitBOGr (&BOStyleRemplissage, hwndCreeChoixStyleRemplissages (hwndParent, FALSE, FALSE), NULL, FALSE);
	/*
	InitBOGr (&BOStyleRemplissage, NULL, hBOCree ("Motif", TRUE, TRUE, FALSE, 0, 0), FALSE);
	bBOAjouteBouton (BOStyleRemplissage.hbo, MAKEINTRESOURCE(IDB_CMD_STR0),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR0);
	bBOAjouteBouton (BOStyleRemplissage.hbo, MAKEINTRESOURCE(IDB_CMD_STR1),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR1);
	bBOAjouteBouton (BOStyleRemplissage.hbo, MAKEINTRESOURCE(IDB_CMD_STR2),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR2);
	bBOAjouteBouton (BOStyleRemplissage.hbo, MAKEINTRESOURCE(IDB_CMD_STR3),BO_NOUVELLE_LN ,BO_COCHE,CMD_MENU_BMP_STR3);
	bBOAjouteBouton (BOStyleRemplissage.hbo, MAKEINTRESOURCE(IDB_CMD_STR4),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR4);
	bBOAjouteBouton (BOStyleRemplissage.hbo, MAKEINTRESOURCE(IDB_CMD_STR5),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR5);
	bBOOuvre (BOStyleRemplissage.hbo, TRUE, hwndParent, rectBO.right, rectBO.top, 0, BOStyleRemplissage.bVisible);
	*/

	// Cr�ation de la barre d'outil style de ligne
	InitBOGr (&BOStyleLigne, hwndCreeChoixStyleLignes (hwndParent, FALSE, FALSE), NULL, FALSE);
	/*
	InitBOGr (&BOStyleLigne, NULL, hBOCree ("Style", TRUE, TRUE, FALSE, 0, 0), FALSE);
	bBOAjouteBouton (BOStyleLigne.hbo, MAKEINTRESOURCE(IDB_CMD_STL0),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL0);
	bBOAjouteBouton (BOStyleLigne.hbo, MAKEINTRESOURCE(IDB_CMD_STL1),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL1);
	bBOAjouteBouton (BOStyleLigne.hbo, MAKEINTRESOURCE(IDB_CMD_STL2),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL2);
	bBOAjouteBouton (BOStyleLigne.hbo, MAKEINTRESOURCE(IDB_CMD_STL3),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL3);
	bBOAjouteBouton (BOStyleLigne.hbo, MAKEINTRESOURCE(IDB_CMD_STL4),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL4);
	bBOOuvre (BOStyleLigne.hbo, TRUE, hwndParent, rectBO.right, rectBO.top, 0, BOStyleLigne.bVisible);
  */
	// Cr�ation de la barre d'outil alignement
  #define NB_BITMAP_ALIGNEMENT 9

  wEspaceH = DEFAUT_ESPACEMENT_H;
  wEspaceV = DEFAUT_ESPACEMENT_V;
	InitBOGr (&BOAlignement, NULL, hBOCree ("Alignement", TRUE, TRUE, FALSE, 0, 0), FALSE);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_PAR_ESP) ,BO_NOUVELLE_LN,BO_BOUTON,CMD_MENU_BMP_ESPACE_PARAM);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_ESPACE_V),BO_NOUVELLE_LN,BO_BOUTON,CMD_MENU_BMP_ESPACE_V);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_ESPACE_H),BO_COL_SUIVANTE,BO_BOUTON,CMD_MENU_BMP_ESPACE_H);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_ALIGN_G) ,BO_NOUVELLE_LN,BO_BOUTON,CMD_MENU_BMP_ALIGN_G);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_ALIGN_H) ,BO_COL_SUIVANTE,BO_BOUTON,CMD_MENU_BMP_ALIGN_H);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_ALIGN_D) ,BO_NOUVELLE_LN,BO_BOUTON,CMD_MENU_BMP_ALIGN_D);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_ALIGN_B) ,BO_COL_SUIVANTE,BO_BOUTON,CMD_MENU_BMP_ALIGN_B);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_ALIGN_VC),BO_NOUVELLE_LN,BO_BOUTON,CMD_MENU_BMP_ALIGN_VC);
  bBOAjouteBouton (BOAlignement.hbo, MAKEINTRESOURCE(IDB_CMD_ALI_ALIGN_HC),BO_COL_SUIVANTE,BO_BOUTON,CMD_MENU_BMP_ALIGN_HC);
  bBOOuvre (BOAlignement.hbo, TRUE, hwndParent, rectBO.right, rectBO.top, 0, BOAlignement.bVisible);

	// Cr�ation de la barre d'outil choix couleur
	InitBOGr (&BOCouleur, hwndCreeChoixCouleurs (hwndParent, FALSE, FALSE), NULL, FALSE);

	// Cr�ation de la barre d'outil affichage des coordonn�es
	InitBOGr (&BOCoord, hwndCreeFenCoord (hwndParent, 0, 400, FALSE), NULL, FALSE);

  return TRUE;
  } // bCreeLesBOGr

// -----------------------------------------------------------------------
void efface_fenetres_accessoire (void)
  {
  MontreBOGr (&BODessinPlein, FALSE);
  MontreBOGr (&BODessinVide, FALSE);
  MontreBOGr (&BOOperation, FALSE);
  MontreBOGr (&BOAnimRL, FALSE);
  MontreBOGr (&BOAlignement, FALSE);
  MontreBOGr (&BOCouleur, FALSE);
  MontreBOGr (&BOStyleRemplissage, FALSE);
  MontreBOGr (&BOStyleLigne, FALSE);
  MontreBOGr (&BOCoord, FALSE);
  }

// -----------------------------------------------------------------------
static void restore_fenetres_accessoire (void)
  {
  switch (get_ctxtgr_mode_trace ())
    {
    case TRACE_PLEIN:
      MontreBOGr (&BODessinPlein, BODessinPlein.bVisible);
      break;
    case TRACE_VIDE:
      MontreBOGr (&BODessinVide, BODessinVide.bVisible);
      break;
    default:
      break;
    }
  MontreBOGr (&BOOperation, BOOperation.bVisible);
  MontreBOGr (&BOAnimRL, BOAnimRL.bVisible);
  MontreBOGr (&BOAlignement, BOAlignement.bVisible);
  MontreBOGr (&BOCouleur, BOCouleur.bVisible);
  MontreBOGr (&BOStyleRemplissage, BOStyleRemplissage.bVisible);
  MontreBOGr (&BOStyleLigne, BOStyleLigne.bVisible);
  MontreBOGr (&BOCoord, BOCoord.bVisible);
  }

// -----------------------------------------------------------------------
// ajoute une action action_change_shape
static void change_shape (G_ACTION action)
	{
  t_param_action  param_action;

  param_action.fonction = action;
  ajouter_action (action_change_shape, &param_action);
	}

// -----------------------------------------------------------------------
// ajoute une action action_change_style_ligne
static void change_style_ligne (G_STYLE_LIGNE dwStyleLigne)
	{
  t_param_action  param_action;

  param_action.style_gr = dwStyleLigne;
  ajouter_action (action_change_style_ligne, &param_action);
  }

// -----------------------------------------------------------------------
// ajoute une action action_change_style_remplissage
static void change_style_remplissage (G_STYLE_REMPLISSAGE dwStyleRemplissage)
	{
  t_param_action  param_action;

  param_action.style_gr = (G_STYLE_LIGNE)dwStyleRemplissage;
  ajouter_action (action_change_style_remplissage, &param_action);
  }

// -----------------------------------------------------------------------
// ajoute une action action_change_color
static void change_color (G_COULEUR dwCouleur)
	{
  t_param_action  param_action;

  param_action.couleur_gr = dwCouleur;
  ajouter_action (action_change_color, &param_action);
  }
 
// -----------------------------------------------------------------------
// ajoute une action action_change_color
static void change_animation_rl (DWORD dwAnimation)
	{
  t_param_action  param_action;

  param_action.param_fonction = dwAnimation;
  ajouter_action (action_animation_rl, &param_action);
  }

// -----------------------------------------------------------------------
// Traitement d'une commande envoy�e � la fen�tre principale de PcsGr
// Retour...: TRUE si le message a �t� trait�, FALSE si non
// -----------------------------------------------------------------------
static BOOL TraiteCommande (HWND hwnd, WPARAM mp1)
  {
  t_param_action  param_action;
  char            saisie[256];
  char            titre[c_nb_car_message_inf];
  char            action[c_nb_car_message_inf];
  char            action_bis[c_nb_car_message_inf];
  char            titre_fen [c_NB_CAR_TITRE_PAGE_GR + 1];
  char            mess [c_nb_car_message_inf];
  DLGGR_STYLE_FEN    retour_style_fen_gr;
  t_dlg_liste_saisie_gr retour_liste_saisie_gr = {NULL, 255,"",""};
  t_dlg_parametres_gr   retour_parametres_gr;

  switch (LOWORD (mp1))
    {
    case CMD_MENU_OUVRE:
      bLngLireInformation (c_inf_tdlg_ouvrir, titre);
      bLngLireInformation (c_inf_action_ouvre, action);
      //bLngLireInformation (c_inf_abandonner, action_bis);
			pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
      if (bDlgFichierOuvrir (hwnd, saisie, titre, "pcs", action))
        {
        StrCopy (param_action.texte, pszSupprimeExt (saisie));
        param_action.on = TRUE;
        ajouter_action (action_charge_application, &param_action);
        }
      break;

    case CMD_MENU_SAUVE:
      //$$pszCopiePathNameDocExt (param_action.texte, MAX_PATH, EXTENSION_SOURCE);
			if (bGetNouveauDoc ())
				{
				bLngLireInformation (c_inf_tdlg_sauve, titre);
				bLngLireInformation (c_inf_action_sauve, action);
				pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
				if (bDlgFichierEnregistrerSous (Appli.hwnd, saisie, titre, "pcs", action))
					{
					StrCopy (param_action.texte, pszSupprimeExt (saisie));
					ajouter_action (action_sauve_application, &param_action);
					}
				}
			else
				{
				StrCopy (param_action.texte, pszPathNameDoc ());
	      ajouter_action (action_sauve_application, &param_action);
				}
      break;

    case CMD_MENU_SAUVE_SOUS:
      bLngLireInformation (c_inf_tdlg_sauve, titre);
      bLngLireInformation (c_inf_action_sauve, action);
			pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
      if (bDlgFichierEnregistrerSous (Appli.hwnd, saisie, titre, "pcs", action))
        {
        StrCopy (param_action.texte, pszSupprimeExt (saisie));
        ajouter_action (action_sauve_application, &param_action);
        }
      break;

    case CMD_MENU_IMPORTE:
      bLngLireInformation (c_inf_tdlg_importe, titre);
      bLngLireInformation (c_inf_action_importe, action);
      //bLngLireInformation (c_inf_abandonner, action_bis);
			saisie[0] = '\0'; //$$ arranger
      if (bDlgFichierOuvrir (hwnd, saisie, titre, "SNP", action))
        {
        StrCopy (param_action.texte, saisie);
        ajouter_action (action_import, &param_action);
        }
      break;

    case CMD_MENU_EXPORTE:
      bLngLireInformation (c_inf_tdlg_exporte, titre);
      bLngLireInformation (c_inf_action_exporte, action);
      bLngLireInformation (c_inf_abandonner, action_bis);
			saisie[0] = '\0'; //$$ arranger
      if (bDlgFichierEnregistrerSous (hwnd, saisie, titre, "SNP", action))
        {
        StrCopy (param_action.texte, saisie);
        ajouter_action (action_export, &param_action);
        }
      break;

    case CMD_MENU_CHARGE_SYMB:
      bLngLireInformation (c_inf_tdlg_charge_symb, titre);
      bLngLireInformation (c_inf_action_charge_symb, action);
      //bLngLireInformation (c_inf_abandonner, action_bis);
			saisie[0] = '\0'; //$$ arranger
      if (bDlgFichierOuvrir (hwnd, saisie, titre, "BLI", action))
        {
        StrCopy (param_action.texte, saisie);
        ajouter_action (action_charge_symb, &param_action);
        }
      break;

    case CMD_MENU_SAUVE_SYMB:
      bLngLireInformation (c_inf_tdlg_sauve_symb, titre);
      bLngLireInformation (c_inf_action_sauve, action);
      //bLngLireInformation (c_inf_abandonner, action_bis);
			saisie[0] = '\0'; //$$ arranger
      if (bDlgFichierEnregistrerSous (hwnd, saisie, titre, "BLI", action))
        {
        StrCopy (param_action.texte, saisie);
        ajouter_action (action_sauve_symb, &param_action);
        }
      break;

    case CMD_MENU_IMPRIME:
      break;

    case CMD_MENU_QUITTE:
        if (get_ctxtgr_lancement_os())
          {
          bLngLireInformation (c_inf_confir_sauve, mess);
          switch (dlg_confirmation (Appli.hwnd, mess))
            {
						case IDYES:
							if (bGetNouveauDoc ())
								{
								bLngLireInformation (c_inf_tdlg_sauve, titre);
								bLngLireInformation (c_inf_action_sauve, action);
								pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
								if (bDlgFichierEnregistrerSous (Appli.hwnd, saisie, titre, "pcs", action))
									{
									StrCopy (param_action.texte, pszSupprimeExt (saisie));
									ajouter_action (action_sauve_application, &param_action);
									ajouter_action (action_quitte, &param_action);
									}
								}
							else
								{
								StrCopy (param_action.texte, pszPathNameDoc ());
								ajouter_action (action_sauve_application, &param_action);
								ajouter_action (action_quitte, &param_action);
								}
              break;
            case IDNO:
              ajouter_action (action_quitte, &param_action);
              break;
            case IDCANCEL:
            default:
              break;
            }
          }
        else
          {
		      // pszCopiePathNameDocExt (param_action.texte, MAX_PATH, EXTENSION_SAT);
	        StrCopy (param_action.texte, pszPathNameDoc ());
          ajouter_action (action_sauve_application_sat, &param_action);
          ajouter_action (action_quitte, &param_action);
          }
      break;

    case CMD_MENU_MODE_TEST:
			{
			VerifWarningExit;
			/*
		  SWP             swp;

      WinQueryWindowPos (::GetParent(hwnd), &swp);
      ::SetWindowPos (::GetParent (hwnd), NULL, swp.x, swp.y, swp.cx, swp.cy, 
				SWP_NOACTIVATE | SWP_NOZORDER); //SWP_MINIMIZE|SWP_MOVE|SWP_SIZE);
			::ShowWindow (::GetParent (hwnd), SW_MINIMIZE);
      efface_fenetres_accessoire ();
			//pszCopiePathNameDocExt (param_action.texte, MAX_PATH, EXTENSION_SAT);
			StrCopy (param_action.texte, pszPathNameDoc ());
      ajouter_action (action_sauve_application, &param_action);
      ajouter_action (action_mode_test, &param_action);
			*/
			}
      break;

    case CMD_MENU_BMP_ESPACE_PARAM:
      retour_parametres_gr.wEspaceH = wEspaceH;
      retour_parametres_gr.wEspaceV = wEspaceV;
      if (bParametreAlignementGr (Appli.hwnd, &retour_parametres_gr))
        {
        wEspaceH = retour_parametres_gr.wEspaceH;
        wEspaceV = retour_parametres_gr.wEspaceV;
        }
      break;

    case CMD_MENU_BMP_AFFI_BO_DESSIN:
			// Montre / cache la barre d'outil dessin
      switch (get_ctxtgr_mode_trace ())
        {
        case TRACE_PLEIN:
          BODessinPlein.bVisible = !BODessinPlein.bVisible;
          MontreBOGr (&BODessinPlein, BODessinPlein.bVisible);
          break;
        case TRACE_VIDE:
          BODessinVide.bVisible = !BODessinVide.bVisible;
          MontreBOGr (&BODessinVide, BODessinVide.bVisible);
          break;
        }
      break;

    case CMD_MENU_BMP_AFFI_STL:
      BOStyleLigne.bVisible = !BOStyleLigne.bVisible;
      MontreBOGr (&BOStyleLigne, BOStyleLigne.bVisible);
      break;

    case CMD_MENU_BMP_AFFI_STR:
      BOStyleRemplissage.bVisible = !BOStyleRemplissage.bVisible;
      MontreBOGr (&BOStyleRemplissage, BOStyleRemplissage.bVisible);
      break;

    case CMD_MENU_BMP_AFFI_BO_PALETTE:
      BOCouleur.bVisible = !BOCouleur.bVisible;
      MontreBOGr (&BOCouleur, BOCouleur.bVisible);
      break;

    case CMD_MENU_BMP_AFFI_ALIGN:
      BOAlignement.bVisible = !BOAlignement.bVisible;
      MontreBOGr (&BOAlignement, BOAlignement.bVisible);
      break;

    case CMD_MENU_BMP_AFFI_COORD:
      BOCoord.bVisible = !BOCoord.bVisible;
      MontreBOGr (&BOCoord, BOCoord.bVisible);
      break;

    case CMD_MENU_POLICE:
      param_action.police_gr = get_ctxtgr_police ();
      param_action.style_gr  = (G_STYLE_LIGNE)get_ctxtgr_style_police ();
      param_action.taille  = get_ctxtgr_taille_texte ();
			// Boite de dialogue modification de police valid�e ?
      if (fnt_user (Appli.hwnd, &param_action.police_gr, (PDWORD)&param_action.style_gr, &param_action.taille,Appli.byCharSetFont))
        {
				// oui => Modification de la police
        ajouter_action (action_change_police, &param_action);
        }
      break;

    case CMD_MENU_TOUT_SELECTIONNER:
      param_action.param_fonction = EN_SELECTION;
      ajouter_action (action_mode_dessin, &param_action);
      ajouter_action (action_tout_selectionner, &param_action);
      break;

    case CMD_MENU_SELECTIONNER_ANIMATIONS:
      param_action.param_fonction = EN_SELECTION;
      ajouter_action (action_mode_dessin, &param_action);
      ajouter_action (action_selectionner_animes, &param_action);
      break;

    case CMD_MENU_DESELECTIONNER_ANIMATIONS:
      ajouter_action (action_deselectionner_animes, &param_action);
      break;

    case CMD_MENU_SELECTIONNER_NON_ANIMATIONS:
      param_action.param_fonction = EN_SELECTION;
      ajouter_action (action_mode_dessin, &param_action);
      ajouter_action (action_selectionner_non_animes, &param_action);
      break;

    case CMD_MENU_DESELECTIONNER_NON_ANIMATIONS:
      ajouter_action (action_deselectionner_non_animes, &param_action);
      break;

    case CMD_MENU_SELECTIONNER_ELT_HORS_PAGE:
      param_action.param_fonction = EN_SELECTION;
      ajouter_action (action_mode_dessin, &param_action);
      ajouter_action (action_selectionner_elt_hors_page, &param_action);
      break;

    case CMD_MENU_BMP_MODE_DESSIN:
      param_action.param_fonction = EN_TRACE;
      ajouter_action (action_mode_dessin, &param_action);
      break;

    case CMD_MENU_BMP_MODE_SELECTION:
      param_action.param_fonction = EN_SELECTION;
      ajouter_action (action_mode_dessin, &param_action);
      break;

    case CMD_MENU_BMP_MODE_TRACE:
      switch (get_ctxtgr_mode_trace ())
        {
				RECT rect;

        case TRACE_PLEIN:
          BODessinVide.bVisible = TRUE;
				  BOGetRect (BODessinPlein.hbo, &rect);
					BODeplace (BODessinVide.hbo, rect.left, rect.top, 0);
          MontreBOGr (&BODessinVide, TRUE);
          BODessinPlein.bVisible = FALSE;
          MontreBOGr (&BODessinPlein, FALSE);
          BOCheckBouton (BODessinVide.hbo, CMD_MENU_BMP_LIGNE, TRUE);
          break;

        case TRACE_VIDE:
					BODessinPlein.bVisible = TRUE;
				  BOGetRect (BODessinVide.hbo, &rect);
					BODeplace (BODessinPlein.hbo, rect.left, rect.top, 0);
					MontreBOGr (&BODessinPlein, TRUE);
					BODessinVide.bVisible = FALSE;
					MontreBOGr (&BODessinVide, FALSE);
					BOCheckBouton (BODessinPlein.hbo, CMD_MENU_BMP_LIGNE, TRUE);
          break;
        }
      param_action.fonction = G_ACTION_LIGNE;
      ajouter_action (action_change_shape, &param_action);
      ajouter_action (action_mode_trace, &param_action);
      break;

    case  CMD_MENU_BMP_LIGNE:            change_shape (G_ACTION_LIGNE); break;
    case  CMD_MENU_BMP_RECTANGLE:        change_shape (G_ACTION_RECTANGLE); break;
    case  CMD_MENU_BMP_RECTANGLE_PLEIN:  change_shape (G_ACTION_RECTANGLE_PLEIN); break;
    case  CMD_MENU_BMP_CERCLE:           change_shape (G_ACTION_CERCLE); break;
    case  CMD_MENU_BMP_CERCLE_PLEIN:     change_shape (G_ACTION_CERCLE_PLEIN); break;
    case  CMD_MENU_BMP_Q_CERCLE:         change_shape (G_ACTION_QUART_ARC); break;
    case  CMD_MENU_BMP_Q_CERCLE_PLEIN:   change_shape (G_ACTION_QUART_ARC_PLEIN); break;
    case  CMD_MENU_BMP_Q_CERCLE_R:       change_shape (G_ACTION_QUART_CERCLE); break;
    case  CMD_MENU_BMP_Q_CERCLE_PLEIN_R: change_shape (G_ACTION_QUART_CERCLE_PLEIN); break;
    case  CMD_MENU_BMP_D_CERCLE_H:       change_shape (G_ACTION_H_DEMI_CERCLE); break;
    case  CMD_MENU_BMP_D_CERCLE_PLEIN_H: change_shape (G_ACTION_H_DEMI_CERCLE_PLEIN); break;
    case  CMD_MENU_BMP_D_CERCLE_V:       change_shape (G_ACTION_V_DEMI_CERCLE); break;
    case  CMD_MENU_BMP_D_CERCLE_PLEIN_V: change_shape (G_ACTION_V_DEMI_CERCLE_PLEIN); break;
    case  CMD_MENU_BMP_TRIANGLE:         change_shape (G_ACTION_TRI_RECT); break;
    case  CMD_MENU_BMP_TRIANGLE_PLEIN:   change_shape (G_ACTION_TRI_RECT_PLEIN); break;
    case  CMD_MENU_BMP_ISOCELE_H:         change_shape (G_ACTION_H_TRI_ISO); break;
    case  CMD_MENU_BMP_ISOCELE_H_PLEIN:   change_shape (G_ACTION_H_TRI_ISO_PLEIN); break;
    case  CMD_MENU_BMP_ISOCELE_V:         change_shape (G_ACTION_V_TRI_ISO); break;
    case  CMD_MENU_BMP_ISOCELE_V_PLEIN:   change_shape (G_ACTION_V_TRI_ISO_PLEIN); break;
    case  CMD_MENU_BMP_VANNE_H:          change_shape (G_ACTION_H_VANNE); break;
    case  CMD_MENU_BMP_VANNE_H_PLEIN:    change_shape (G_ACTION_H_VANNE_PLEIN); break;
    case  CMD_MENU_BMP_VANNE_V:          change_shape (G_ACTION_V_VANNE); break;
    case  CMD_MENU_BMP_VANNE_V_PLEIN:    change_shape (G_ACTION_V_VANNE_PLEIN); break;
    case  CMD_MENU_BMP_TEXTE_H:          change_shape (G_ACTION_H_TEXTE); break;
		//	G_ACTION_V_TEXTE
    case  CMD_MENU_BMP_POLYLIGNE:        change_shape (G_ACTION_POLYGONE); break;
    case  CMD_MENU_BMP_POLYLIGNE_PLEIN:  change_shape (G_ACTION_POLYGONE_PLEIN); break;
    case  CMD_MENU_BMP_RN_BAR1:					 change_shape (G_ACTION_BARGRAPHE); break;
    case  CMD_MENU_BMP_RN_COURBE_T:			 change_shape (G_ACTION_COURBE_T); break;
    case  CMD_MENU_BMP_RN_COURBE_C:			 change_shape (G_ACTION_COURBE_C); break;
    case  CMD_MENU_BMP_RN_COURBE_A:			 change_shape (G_ACTION_COURBE_A); break;
    case  CMD_MENU_BMP_RM_TEXT1:				 change_shape (G_ACTION_STATIC_TEXTE); break;
    case  CMD_MENU_BMP_RM_NUM1:					 change_shape (G_ACTION_STATIC_NUM); break;
    case  CMD_MENU_BMP_EL1:							 change_shape (G_ACTION_ENTREE_LOG); break;
    case  CMD_MENU_BMP_EL_BITMAP:				 change_shape (G_ACTION_RECTANGLE); break; //$$ ?
    case  CMD_MENU_BMP_EN1:							 change_shape (G_ACTION_EDIT_NUM); break;
    case  CMD_MENU_BMP_EM1:							 change_shape (G_ACTION_EDIT_TEXTE); break;
    case  CMD_MENU_BMP_CONT_STATIC:			 change_shape (G_ACTION_CONT_STATIC); break;
    case  CMD_MENU_BMP_CONT_EDIT:				 change_shape (G_ACTION_CONT_EDIT); break;
    case  CMD_MENU_BMP_CONT_GROUP:			 change_shape (G_ACTION_CONT_GROUP); break;
    case  CMD_MENU_BMP_CONT_BOUTON:			 change_shape (G_ACTION_CONT_BOUTON); break;
    case  CMD_MENU_BMP_CONT_CHECK:			 change_shape (G_ACTION_CONT_CHECK); break;
    case  CMD_MENU_BMP_CONT_RADIO:			 change_shape (G_ACTION_CONT_RADIO); break;
    case  CMD_MENU_BMP_CONT_COMBO:			 change_shape (G_ACTION_CONT_COMBO); break;
    case  CMD_MENU_BMP_CONT_LIST:				 change_shape (G_ACTION_CONT_LIST); break;
    case  CMD_MENU_BMP_CONT_SCROLL_H:		 change_shape (G_ACTION_CONT_SCROLL_H); break;
    case  CMD_MENU_BMP_CONT_SCROLL_V:		 change_shape (G_ACTION_CONT_SCROLL_V); break;
    case  CMD_MENU_BMP_CONT_BOUTON_VAL:	 change_shape (G_ACTION_CONT_BOUTON_VAL); break;


    case CMD_MENU_BMP_STL0: change_style_ligne (G_STYLE_LIGNE_CONTINUE); break;
    case CMD_MENU_BMP_STL1: change_style_ligne (G_STYLE_LIGNE_TIRETS); break;
    case CMD_MENU_BMP_STL2: change_style_ligne (G_STYLE_LIGNE_POINTS); break;
    case CMD_MENU_BMP_STL3: change_style_ligne (G_STYLE_LIGNE_TIRETS_POINTS); break;
    case CMD_MENU_BMP_STL4: change_style_ligne (G_STYLE_LIGNE_TIRETS_2_POINTS); break;

    case CMD_MENU_BMP_STR0: change_style_remplissage (G_STYLE_REMPLISSAGE_PLEIN); break;
    case CMD_MENU_BMP_STR1: change_style_remplissage (G_STYLE_REMPLISSAGE_DIAG_AV); break;
    case CMD_MENU_BMP_STR2: change_style_remplissage (G_STYLE_REMPLISSAGE_DIAG_AR); break;
    case CMD_MENU_BMP_STR3: change_style_remplissage (G_STYLE_REMPLISSAGE_HACHURE_DIAG); break;
    case CMD_MENU_BMP_STR4: change_style_remplissage (G_STYLE_REMPLISSAGE_LIGNES_V); break;
    case CMD_MENU_BMP_STR5: change_style_remplissage (G_STYLE_REMPLISSAGE_LIGNES_H); break;


    case CMD_MENU_BMP_RN_DEP1:change_animation_rl (ANIMATION_DEPLACE); break;
    case CMD_MENU_BMP_RL1:change_animation_rl (ANIMATION_R_1_LOG); break;
    case CMD_MENU_BMP_RL2:change_animation_rl (ANIMATION_R_2_LOG); break;

    case CMD_MENU_BMP_GROUPER:
      ajouter_action (action_group, &param_action);
      break;

    case CMD_MENU_BMP_DEGROUPER:
      ajouter_action (action_degroup, &param_action);
      break;

    case CMD_MENU_BMP_ALIGN_VC:
      ajouter_action (action_align_vc, &param_action);
      break;

    case CMD_MENU_BMP_ALIGN_HC :
      ajouter_action (action_align_hc, &param_action);
      break;

    case CMD_MENU_BMP_ALIGN_D :
      ajouter_action (action_align_d, &param_action);
      break;

    case CMD_MENU_BMP_ALIGN_B :
      ajouter_action (action_align_b, &param_action);
      break;

    case CMD_MENU_BMP_ALIGN_G :
      ajouter_action (action_align_g, &param_action);
      break;

    case CMD_MENU_BMP_ALIGN_H :
      ajouter_action (action_align_h, &param_action);
      break;

    case CMD_MENU_BMP_ESPACE_H :
      ajouter_action (action_espace_h, &param_action);
      break;

    case CMD_MENU_BMP_ESPACE_V :
      ajouter_action (action_espace_v, &param_action);
      break;

    case CMD_MENU_BMP_SYMETRIE_V:
      ajouter_action (action_symetrie_v, &param_action);
      break;

    case CMD_MENU_BMP_SYMETRIE_H:
      ajouter_action (action_symetrie_h, &param_action);
      break;

    case CMD_MENU_BMP_ROTATION:
      ajouter_action (action_rotation, &param_action);
      break;

    case CMD_MENU_BMP_CHANGE_REMPLISSAGE:
      ajouter_action (action_change_remplissage, &param_action);
      break;

    case CMD_MENU_BMP_DUPLIQUER:
      param_action.page_gr = 0;
      ajouter_action (action_duplicate, &param_action);
      break;

    case CMD_MENU_BMP_DEVANT:
      ajouter_action (action_passage_devant, &param_action);
      break;

    case CMD_MENU_BMP_DERRIERE:
      ajouter_action (action_passage_derriere, &param_action);
      break;

		case CMD_MENU_BMP_IDENTIFIANT:
      ajouter_action (action_change_identifiant, &param_action);
			break;

    case CMD_MENU_OUVRIR_FENETRE:
      retour_liste_saisie_gr.nb_max_car_saisie = NB_CAR_TITRE_PAGE_DLG;
      bLngLireInformation (c_inf_num_page_synop, retour_liste_saisie_gr.titre);
      retour_liste_saisie_gr.ptr_proc_maj_liste = (t_proc_maj_liste *) (&initialise_num_page_dlg);
      if (dlg_liste_saisie_gr (Appli.hwnd, &retour_liste_saisie_gr))
        {
        if (extraire_numero_page (&param_action.page_gr, retour_liste_saisie_gr.saisie))
          {
          ajouter_action (action_ouvre_fenetre, &param_action);
          }
        else
          {
          afficher_err (417);
          }
        }
      break;

    case CMD_MENU_FERMER_FENETRE:
      ajouter_action (action_ferme_fenetre, &param_action);
      break;

    case CMD_MENU_SUPPRIMER_FENETRE:
      saisie[0] = '\0';
      titre_fen[0] = '\0';
      bLngLireInformation (c_inf_sup_fenetre, mess);
      bdgr_lire_titre_page (get_ctxtgr_hBdGr (), titre_fen);
      StrConcat (saisie, mess);
      StrConcat (saisie, titre_fen);
      if (dlg_confirmation (hwnd, saisie) == IDYES)
        {
        ajouter_action (action_supprime_fenetre, &param_action);
        }
      break;

    case CMD_MENU_MODIFIER_FENETRE:
      initialise_style_fenetre (&retour_style_fen_gr);
      if (dlg_style_fenetre_gr (hwnd, &retour_style_fen_gr))
        {
        param_action.bFenModale = retour_style_fen_gr.bFenModale;
        param_action.bFenDialog = retour_style_fen_gr.bFenDialog;
        param_action.bBitmapFond = retour_style_fen_gr.bBitmapFond;
        param_action.un_titre = retour_style_fen_gr.un_titre;
        param_action.un_sysmenu =  retour_style_fen_gr.un_sysmenu;
        param_action.un_minimize = retour_style_fen_gr.un_minimize;
        param_action.un_maximize = retour_style_fen_gr.un_maximize;
        param_action.un_bord =  retour_style_fen_gr.un_bord;
        param_action.une_barreh = retour_style_fen_gr.un_scroll_h;
        param_action.une_barrev = retour_style_fen_gr.un_scroll_v;
        param_action.grille_x = retour_style_fen_gr.grille_x;
        param_action.grille_y = retour_style_fen_gr.grille_y;
        param_action.mode_aff = retour_style_fen_gr.mode_affichage;
        param_action.mode_deform = retour_style_fen_gr.mode_deformation;
        param_action.facteur_zoom = retour_style_fen_gr.facteur_zoom;
				StrCopy (param_action.texte, retour_style_fen_gr.titre);
        param_action.couleur_gr = retour_style_fen_gr.dwCouleurFond;
				StrCopy (param_action.szPath, retour_style_fen_gr.szPathBitmapFond);
	      ajouter_action (action_change_style_fenetre, &param_action);
        }
      break;

    case CMD_MENU_A_PROPOS:
			bAfficheInfoVersion ();
      break;

    default:
			{
			DWORD CtlId = LOWORD (mp1);
			int nCouleur = (int)(CtlId - CMD_MENU_BMP_OFFSET_COULEUR);
			if ((nCouleur >= 0 ) && (nCouleur < G_NB_COULEURS))
				change_color ((G_COULEUR)nCouleur);
			else
				VerifWarningExit;
			}
      break;
    }
  // Valeurs retourn�es si le message a �t� trait�
  return TRUE;
  }

/* $$ porter
// -----------------------------------------------------------------------
// Nom......: MinMaxFrame
// Objet....: Traitement du message WM_MINMAXFRAME
// Entr�es..: HWND     hwnd
//            PUINT  pmsg
//            WPARAM *  pmp1
//            WPARAM *  pmp2
//            LRESULT * pmres
// Sorties..: LRESULT * pmres mis � jour
// Retour...: BOOL         : FALSE
// Remarque : (*pmp1) = pointeur sur struct SWP
// -----------------------------------------------------------------------
static BOOL MinMaxFrame (HWND hwnd, PUINT pmsg, WPARAM * pmp1, WPARAM * pmp2, LRESULT * pmres)
  {
  BOOL rretour;
  t_param_action  param_action;
  PSWP pswpNew;

  pswpNew = (PSWP) (PVOID)(*pmp1);
  if (pswpNew->fs & SWP_MINIMIZE)
    {
    efface_fenetres_accessoire ();
    ajouter_action (action_mise_icone, &param_action);
    rretour = FALSE;
    }
  else
    {
    if (pswpNew->fs & SWP_RESTORE)
      {
      restore_fenetres_accessoire ();
      if (CtxtgrGetModeEdition () == EN_TEST)
        {
				pszCopiePathNameDocExt (param_action.texte, MAX_PATH, EXTENSION_SAT);
        param_action.on = FALSE;
        ajouter_action (action_charge_application, &param_action);
        }
      else
        {
        ajouter_action (action_restore_dicone, &param_action);
        }
      rretour = FALSE;
      }
    else
      {
      if (pswpNew->fs & SWP_MAXIMIZE)
        {
        rretour = TRUE;
        }
      }
    }
  *pmres = (LRESULT) FALSE;
  return (rretour);
  }
*/

//----------------------------------------------
// Traitement des messages de wndprocGr
//----------------------------------------------

static void OnCreate (HWND hwnd)
	{
	if (!bCreeLesBOGr (hwnd))
		VerifWarningExit;
	}
 
static void OnDestroy (void)
	{
	SupprimeBOGr (&BODessinPlein);
	SupprimeBOGr (&BODessinVide);
	SupprimeBOGr (&BOOperation);
	SupprimeBOGr (&BOAnimRL);
	SupprimeBOGr (&BOStyleRemplissage);
	SupprimeBOGr (&BOStyleLigne);
	SupprimeBOGr (&BOAlignement);
	SupprimeBOGr (&BOCouleur);
	SupprimeBOGr (&BOCoord);
	}

static void OnClose (void)
  {
	t_param_action param_action;

	if (get_ctxtgr_lancement_os())
		{
		char mess[c_nb_car_message_inf];

		bLngLireInformation (c_inf_confir_sauve, mess);
		switch (dlg_confirmation (Appli.hwnd, mess))
			{
			case IDYES:
				if (bGetNouveauDoc ())
					{
					char  titre[c_nb_car_message_inf];
					char  action[c_nb_car_message_inf];
					char  saisie[256];

					bLngLireInformation (c_inf_tdlg_sauve, titre);
					bLngLireInformation (c_inf_action_sauve, action);
					pszCopiePathNameDocExt (saisie, MAX_PATH, EXTENSION_SOURCE);
					if (bDlgFichierEnregistrerSous (Appli.hwnd, saisie, titre, "pcs", action))
						{
						StrCopy (param_action.texte, pszSupprimeExt (saisie));
						ajouter_action (action_sauve_application, &param_action);
						ajouter_action (action_quitte, &param_action);
						}
					}
				else
					{
					StrCopy (param_action.texte, pszPathNameDoc ());
					ajouter_action (action_sauve_application, &param_action);
					ajouter_action (action_quitte, &param_action);
					}
				break;
			case IDNO:
				ajouter_action (action_quitte, &param_action);
				break;
			} // switch (dlg_confirmation (Appli.hwnd, mess))
		} // if (get_ctxtgr_lancement_os())
  else
    {
		//pszCopiePathNameDocExt (param_action.texte, MAX_PATH, EXTENSION_SAT);
	  StrCopy (param_action.texte, pszPathNameDoc ());
    ajouter_action (action_sauve_application_sat, &param_action);
    ajouter_action (action_quitte, &param_action);
    }
  }

static BOOL bOnCommand (HWND hwnd, WPARAM mp1)
	{
	BOOL bRes = TRUE;

  switch (LOWORD (mp1))
    {
    case CMD_MENU_OUVRE:
    case CMD_MENU_SAUVE:
    case CMD_MENU_SAUVE_SOUS:
    case CMD_MENU_IMPORTE:
    case CMD_MENU_EXPORTE:
    case CMD_MENU_QUITTE:
    case CMD_MENU_OUVRIR_FENETRE:
    case CMD_MENU_BMP_RL1:
    case CMD_MENU_BMP_RL2:
    case CMD_MENU_BMP_RN_DEP1:
    case CMD_MENU_BMP_AFFI_BO_DESSIN:
    case CMD_MENU_BMP_AFFI_BO_PALETTE:
    case CMD_MENU_BMP_AFFI_STL:
    case CMD_MENU_BMP_AFFI_STR:
    case CMD_MENU_BMP_AFFI_ALIGN:
    case CMD_MENU_BMP_AFFI_COORD:
		case CMD_MENU_A_PROPOS:
			// Traitement des commandes ne n�cessitant pas de fen�tre synoptique
      bRes = TraiteCommande (hwnd, mp1);
      break;

    default:
			// Existe t'il une fen�tre graphique ?
      if (existe_ctxtgr_fenetre ())
				// oui => on traite la commande
        bRes = TraiteCommande (hwnd, mp1);
      else
				// non => erreur
        message_operateur_ouverture ();
      break;
    } // switch (LOWORD (mp1))
	return bRes;
	}

// -----------------------------------------------------------------------
// Fen�tre principale de l'application PcsGr
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocGr (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;
  BOOL     bTraite = TRUE;

  switch (msg)
    {
    case WM_CREATE:
			OnCreate (hwnd);
			break;

    case WM_DESTROY:
			OnDestroy ();
			break;

    case WM_CLOSE:
			OnClose();
      break;

    case WM_COMMAND:
			bTraite = bOnCommand(hwnd, mp1);
			break;

/*   $$ porter case WM_MINMAXFRAME:
         bTraite = MinMaxFrame (hwnd, &msg, &mp1, &mp2, &mres);
         break;
*/
    default:
			bTraite = FALSE;
			break;
    } // switch (msg)

  // message non trait�, traitement par d�faut ---------------
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  } // wndprocGr

// -----------------------------------------------------------------------
// Cr�ation de la classe Gr de la fen�tre principale de PcsGr.exe
// -----------------------------------------------------------------------
static BOOL bCreeClasseGr (void)
  {
	static BOOL bClasseEnregistree = FALSE;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= 0; 
		wc.lpfnWndProc		= wndprocGr; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= LoadIcon (Appli.hinst, MAKEINTRESOURCE(IDICO_APPLI));
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= (HBRUSH)(COLOR_APPWORKSPACE+1);
		wc.lpszMenuName		= NULL;
		wc.lpszClassName	= szClasseGr; 
		bClasseEnregistree = RegisterClass (&wc);
		}
  return bClasseEnregistree;
  }

// -----------------------------------------------------------------------
// Cr�ation de la fen�tre principale de PcsGr
// -----------------------------------------------------------------------
BOOL bCreeFenetreGr (void)
  {
  BOOL	bRet = FALSE;
	// Dimension de la fen�tre en vertical : 2*bordure redimensionnable+ titre+menu
	int	cyFen = (2 * GetSystemMetrics (SM_CYSIZEFRAME)) + GetSystemMetrics (SM_CYMENU) + GetSystemMetrics (SM_CYCAPTION);
	
	if (
		bCreeClasseGr ()&&
		bCreeClasseEchantillon() &&
		bCreeClasseCouleur() &&
		bCreeClasseStyleLignes() &&
		bCreeClasseStyleRemplissages() &&
    bInitEditSyn ()
		)
		{
		// Cr�ation de la fen�tre principale de l'application
		if (Appli.bSetHWndPrincipal (CreateWindow (szClasseGr,	"",
			WS_THICKFRAME | WS_SYSMENU | WS_CAPTION | WS_VISIBLE | WS_MINIMIZEBOX, // WS_BORDER |WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN |WS_CLIPSIBLINGS | WS_CHILD window style
			0, 0, Appli.sizEcran.cx, cyFen,
			NULL,	LoadMenu (Appli.hinstDllLng, MAKEINTRESOURCE (IDMENU_GR)),	Appli.hinst, NULL)))
			{
			bRet = TRUE;
			}
		}
  return bRet;
  }

// -----------------------------------------------------------------------
// Change le titre de la fen�tre principale dePcsGr
// -----------------------------------------------------------------------
void afficher_titre_menu_gr (char *texte)
  {
  ::SetWindowText (Appli.hwnd, texte);
  }

// -----------------------------------------------------------------------
// Destruction de la fen�tre principale dePcsGr
// -----------------------------------------------------------------------
void FermeFenetreGr (void)
  {
  ::DestroyWindow (Appli.hwnd);
  }

// ---------------------------------------------------------------------
//                            MENU COULEUR                              
// ---------------------------------------------------------------------


// -----------------------------------------------------------------------
// Met � jour les attributs couramment s�lectionn�s sur la barre d'outil couleur
void animer_menu_couleur_gr (DWORD id_action, DWORD id_couleur)
  {
  switch (id_action)
    {
    case action_change_color_dessin:
			::SendMessage (BOCouleur.hwnd, CM_SET_COULEUR_DESSIN, id_couleur,0);
      break;
    case action_change_color_fond_page:
			::SendMessage (BOCouleur.hwnd, CM_SET_COULEUR_FOND, id_couleur,0);
      break;
    case action_change_mode_color:
			::SendMessage (BOCouleur.hwnd, CM_CHANGE_MODE, 0,0);
    }
  }

// -----------------------------------------------------------------------
//                       BOITES DE DIALOGUE ET DIVERS
// -----------------------------------------------------------------------

// ---------------------------------------------------------------------
void relacher_souris (void) // $$ capturer_souris non appel�
  {
  ReleaseCapture ();
  }

// -----------------------------------------------------------------------
int afficher_err (DWORD numero)
  {
  dlg_num_erreur (numero);
  return (0);
  }

// -----------------------------------------------------------------------
void afficher_preambule (void)
  {
  dlg_preambule (HWND_DESKTOP, TRUE);
  }

// -----------------------------------------------------------------------
// Afficher un curseur normal ou sablier pour faire patienter l'utilisateur.
// -----------------------------------------------------------------------
void patienter (BOOL patience)
  {
  if (patience)
    SetCursor (LoadCursor(NULL, IDC_WAIT));//WinQuerySysPointer (HWND_DESKTOP, SPTR_WAIT, FALSE));
  else
    SetCursor (LoadCursor(NULL, IDC_ARROW));//WinQuerySysPointer (HWND_DESKTOP, SPTR_ARROW, FALSE));
  return;
  }

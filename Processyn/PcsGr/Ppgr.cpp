/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : ppgr.c                                                   |
 |   Auteur  : AC					        	|
 |   Date    : 16/4/93 					        	|
 |   Version : 5.00							|
 +----------------------------------------------------------------------*/
// Import et export de synoptique et de symbole
// Version Win32 30/4/97

#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "UStr.h"
#include "mem.h"
#include "FMan.h"
#include "lng_res.h"
#include "tipe.h"
#include "G_Objets.h"
#include "g_sys.h"
#include "Descripteur.h"
#include "bdgr.h"
#include "BdAnmGr.h"
#include "BdPageGr.h"
#include "BdMarqGr.h"
#include "UChrono.h"
#include "tipe_gr.h"
#include "inter_gr.h"
#include "pcsspace.h"
#include "pcsfont.h"
//#include "NumSer"             // c_version
#include "IdLngLng.h"
#include "LireLng.h"
#include "IdAutoPcsMan.h"
#include "ppgr.h"
#include "Verif.h"

VerifInit;

//-------------------------------------------------------------------
// Num�ros de r�vision de syntaxe des synoptiques et �volutions
// R�f�rence 

#define REV_PM440			1	// compatible des Processyn PM 4.10 � 4.40

// Evolutions : inversion haut/bas, styles couleur, styles remplissage, Noms de polices
#define REV_NT500	2	// NT 5.00

// Evolutions : Identification de tous les elements graphqiques par label. 
//              Relation avec les animations par reference sur le label.
#define REV_COURANTE	3	// NT 5.10

#define b_tempo_poly_gr           324
#define b_tempo_liste_gr          325
#define b_tempo_liste_elt_gr      323

#define TAILLE_LIGNE_GR           255
#define TAILLE_CHAMPS_GR          STR_MAX_CHAR_ARRAY
#define NB_CHAMPS_PAGE             25
#define NB_CHAMPS_COORD_POLYGONNE   2
#define NB_MAX_CHAMPS             (NB_CHAMPS_PAGE)
#define TAILLE_MES_SYNTAXE         40
#define c_NB_MODE                   3

static const char c_SEPARATEUR [] =				" ";
static const char c_VRAI [] =							"1";
static const char c_FAUX [] =							"0";
static const char c_NEUTRAL [] =					"NEUTRAL";
static const char c_INVISIBLE [] =				"INVISIBLE";
static const char c_SYNTAXE_GR_SYNOP [] =	"@SYNOPTIC";
static const char c_SYNTAXE_GR_SYMBOL [] ="@SYMBOL";
static const char c_SYNTAXE_GR_SAUT_LIGNE [] = "";
static const char c_SYNTAXE_GR_PAGE [] =		"@PAGE";
static const char c_SYNTAXE_GR_LIST [] =		"@LIST";
static const char c_SYNTAXE_GR_POLYGON [] ="@POLYGON";
static const char c_SYNTAXE_GR_FIN [] =		"@END";

typedef enum
	{
	TYPE_LIGNE_GR_DEPART							 		=0,
	TYPE_LIGNE_GR_VIDE										=1,
	TYPE_LIGNE_META_GR_SYNOP							=2,
	TYPE_LIGNE_META_GR_PAGE								=3,
	TYPE_LIGNE_META_GR_LIST								=4,
	TYPE_LIGNE_META_GR_POLYGON						=5,
	TYPE_LIGNE_META_GR_FIN								=6,
	TYPE_LIGNE_GR_ELEMENTS_DESSIN					=7,
	TYPE_LIGNE_GR_ELEMENTS_LISTE					=8,
	TYPE_LIGNE_GR_ELEMENTS_POLYGON				=9,
	TYPE_LIGNE_GR_ELEMENTS_WRITE					=10,
	TYPE_LIGNE_GR_COORD_POLYGONNE					=11,
	TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE			=12,
	TYPE_LIGNE_GR_ELEMENTS_CONT						=13,
	TYPE_LIGNE_GR_INVALIDE								=14,
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN		=15,
	TYPE_LIGNE_GR_ELEMENTS_POLYGON_PLEIN	=16
	} TYPE_LIGNE_GR, *PTYPE_LIGNE_GR;

//-------------------------------------------------------------------------
typedef struct
  {
  DWORD numero_bd;
  DWORD bloc_element;
  DWORD numero_element;
  DWORD numero_import;
  } GR_POLY_TEMPO, *PGR_POLY_TEMPO;

typedef struct
  {
  DWORD numero_bd;
  } GR_LIST_TEMPO, *PGR_LIST_TEMPO;

typedef struct
  {
  DWORD numero_import;
  DWORD numero_element;
  DWORD bloc_element;
  DWORD n_element_relatif;
  } GR_LISTE_ELT_TEMPO, *PGR_LISTE_ELT_TEMPO;

typedef struct
  {
  TYPE_LIGNE_GR nTypeLigneCourante;
  DWORD bloc_element;
  DWORD bloc_nombre;
  DWORD numero_nombre;
  DWORD numero_polygonne;
  BOOL bInitialisation;
  } t_contexte_ppgr;

typedef const char * TABLE_MOTS;

// Mots r�serv�s import / export
static TABLE_MOTS aszMode [c_NB_MODE] = {"CLIP", "ZOOM", "PZOOM"};

#define OFFSET_aszCouleur 2 //(2 couleurs n�gatives)
static TABLE_MOTS aszCouleur [G_NB_COULEURS + OFFSET_aszCouleur] =
	{
	c_INVISIBLE, c_NEUTRAL,
	"BLACK",
	"WHITE",
	"RED",
	"DARKCYAN",
	"YELLOW",
	"DARKBLUE",
	"DARKGREEN",
	"PINK",
	"CYAN",
	"DARKRED",
	"DARKGRAY",
	"PALEGRAY",
	"DARKPINK",
	"GREEN",
	"BLUE",
	"BROWN",
	"WHITERED",
	"WHITEORANGE",
	"WHITEYELLOW",
	"WHITEYELLOWGREEN",
	"WHITEGREEN",
	"WHITECYANGREEN",
	"WHITECYAN",
	"WHITEAZUR",
	"WHITEBLUE",
	"WHITEVIOLET",
	"WHITEPINK",
	"WHITEPURPLE",
	"WHITEGRAY",
	"PALERED",
	"PALEORANGE",
	"PALEYELLOW",
	"PALEYELLOWGREEN",
	"PALEGREEN",
	"PALECYANGREEN",
	"PALECYAN",
	"PALEAZUR",
	"PALEBLUE",
	"PALEVIOLET",
	"PALEPINK",
	"PALEPURPLE",
	"ORANGE",
	"YELLOWGREEN",
	"CYANGREEN",
	"AZUR",
	"VIOLET",
	"PURPLE",
	"GRAY",
	"SHADOWRED",
	"SHADOWORANGE",
	"SHADOWYELLOW",
	"SHADOWYELLOWGREEN",
	"SHADOWGREEN",
	"SHADOWCYANGREEN",
	"SHADOWCYAN",
	"SHADOWAZUR",
	"SHADOWBLUE",
	"SHADOWVIOLET",
	"SHADOWPINK",
	"SHADOWPURPLE",
	"DARKORANGE",
	"DARKYELLOWGREEN",
	"DARKCYANGREEN",
	"DARKAZUR",
	"DARKVIOLET",
	"DARKPURPLE",
	"BLACKRED",
	"BLACKORANGE",
	"BLACKYELLOW",
	"BLACKYELLOWGREEN",
	"BLACKGREEN",
	"BLACKCYANGREEN",
	"BLACKCYAN",
	"BLACKAZUR",
	"BLACKBLUE",
	"BLACKVIOLET",
	"BLACKPINK",
	"BLACKPURPLE"
	};

#define OFFSET_aszStyleLigne 1 //(1 style de ligne n�gatif)
static TABLE_MOTS aszStyleLigne [8 + OFFSET_aszStyleLigne] = //$$G_NB_STYLES_LIGNE
	{
	c_NEUTRAL,
	"LINE_SOLID", "LINE_LONGDASH", "LINE_DOT",
	"LINE_DASHDOT", "LINE_DASHDOUBLEDOT",
	"LINE_DOUBLEDOT", "LINE_SHORTDASH", "LINE_ALTERNATE"};

#define OFFSET_aszStyleRemplissage 1 //(1 style de remplissage n�gatif)
static TABLE_MOTS aszStyleRemplissage [8 + OFFSET_aszStyleRemplissage] = // $$G_NB_STYLES_REMPLISSAGE
	{
	c_NEUTRAL,
	"FILL_SOLID", "FILL_DIAG1","FILL_DIAG2",
	"FILL_HALFTONE", "FILL_VERT","FILL_HORIZ", 
	"FILL_DIAG3","FILL_DIAG4"};

static TABLE_MOTS aszAction [G_NB_ACTIONS] =
	{	
	"LINE", 
	"RECTANGLE", "FILL_RECTANGLE",
	"ELLIPSE", "FILL_ELLIPSE",
	"ARC", "FILL_ARC",
	"H_SEMI_ELLIPSE", "FILL_H_SEMI_ELLIPSE",
	"V_SEMI_ELLIPSE", "FILL_V_SEMI_ELLIPSE",
	"TRIANGLE_RECT", 	"FILL_TRIANGLE_RECT",
	"H_TRIANGLE_ISO",	"FILL_H_TRIANGLE_ISO",
	"V_TRIANGLE_ISO",	"FILL_V_TRIANGLE_ISO",
	"H_VALVE", 	"FILL_H_VALVE",
	"V_VALVE", 	"FILL_V_VALVE",
	"ARC_RECT",	"FILL_CHORD",
	"H_TEXT", 
	"V_TEXT",
	"POLYGON", "FILL_POLYGON",
	"LIST", 	"FILL_LIST",
	"T_TREND", 	"C_TREND", 	"A_TREND",
	"BARGRAF", 
	"A_ENTRYFIELD", "N_ENTRYFIELD",
	"A_STATIC_TEXT",	"N_STATIC_TEXT", 
	"LOGIC_ENTRY",	"NOP",
	"CONTROL_STATIC", 	"CONTROL_EDIT", 	"CONTROL_GROUP", 	
	"CONTROL_BOUTON", 	"CONTROL_CHECK", 	"CONTROL_RADIO",
	"CONTROL_COMBO", 	"CONTROL_LIST", 
	"CONTROL_SCROLL_H", 	"CONTROL_SCROLL_V", 
	"CONTROL_BOUTON_VAL"
	};

static TYPE_LIGNE_GR aTypesLigneAction [G_NB_ACTIONS] =
	{
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"LINE", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"RECTANGLE", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_RECTANGLE",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"ELLIPSE", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_ELLIPSE",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"ARC", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_ARC",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"H_SEMI_ELLIPSE", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_H_SEMI_ELLIPSE",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"V_SEMI_ELLIPSE", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_V_SEMI_ELLIPSE",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"TRIANGLE_RECT", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_TRIANGLE_RECT",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"H_TRIANGLE_ISO", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_H_TRIANGLE_ISO",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"V_TRIANGLE_ISO", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_V_TRIANGLE_ISO",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"H_VALVE", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_H_VALVE",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"V_VALVE", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_V_VALVE",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"ARC_RECT", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"FILL_CHORD",
	TYPE_LIGNE_GR_ELEMENTS_WRITE,							//"H_TEXT", 
	TYPE_LIGNE_GR_ELEMENTS_WRITE,							//"V_TEXT",
	TYPE_LIGNE_GR_ELEMENTS_POLYGON,						//"POLYGON", 
	TYPE_LIGNE_GR_ELEMENTS_POLYGON_PLEIN,			//"FILL_POLYGON",
	TYPE_LIGNE_GR_ELEMENTS_LISTE,							//"LIST", 
	TYPE_LIGNE_GR_ELEMENTS_LISTE,							//"FILL_LIST",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"T_TREND", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"C_TREND", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"A_TREND",
	TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN,				//"BARGRAF", 
	TYPE_LIGNE_GR_ELEMENTS_WRITE,							//"A_ENTRYFIELD", 
	TYPE_LIGNE_GR_ELEMENTS_WRITE,							//"N_ENTRYFIELD",
	TYPE_LIGNE_GR_ELEMENTS_WRITE,							//"A_STATIC_TEXT", 
	TYPE_LIGNE_GR_ELEMENTS_WRITE,							//"N_STATIC_TEXT", 
	TYPE_LIGNE_GR_ELEMENTS_DESSIN,							//"LOGIC_ENTRY",
	TYPE_LIGNE_GR_INVALIDE,										//"NOP",
	TYPE_LIGNE_GR_ELEMENTS_CONT,								//"CONT_STATIC", 
	TYPE_LIGNE_GR_ELEMENTS_CONT,								//"CONT_EDIT", 
	TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE,					//"CONT_GROUP", 
	TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE,					//"CONT_BOUTON", 
	TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE,					//"CONT_CHECK", 
	TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE,					//"CONT_RADIO",
	TYPE_LIGNE_GR_ELEMENTS_CONT,								//"CONT_COMBO", 
	TYPE_LIGNE_GR_ELEMENTS_CONT,								//"CONT_LIST", 
	TYPE_LIGNE_GR_ELEMENTS_CONT,								//"CONT_SCROLL_H", 
	TYPE_LIGNE_GR_ELEMENTS_CONT,								//"CONT_SCROLL_V", 
	TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE					//"CONT_BOUTON_VAL"
	};


//-------------------------------------------------------------------------
//                         LOCAL IMPORT
//-------------------------------------------------------------------------

//---------------- Conversions --------------------------------------------
// Variables globales pour optimisation passage param�tres
static DWORD dwNumRev = REV_COURANTE;
static int	nCyPage = 0;

//-------------------------------------------------------------------------
// conversion haut bas d'un d�placement vertical
static _inline void ConvDY_REV_PM440 (LONG * pnDY)
	{
	*pnDY = - *pnDY;
	}

//-------------------------------------------------------------------------
// conversion haut bas d'une coordonn�e absolue
static _inline void ConvY_REV_PM440 (LONG * pnDY)
	{
	*pnDY = nCyPage - *pnDY;
	}

//-------------------------------------------------------------------------
// conversion ASCII->OEM d'un texte
static void ConvTexte_REV_PM440 (PSTR pszTexte)
	{
	OemToChar (pszTexte, pszTexte); // !!! Attention Ne pas utiliser en caract�re large
	}

//-------------------------------------------------------------------------
// cherche si un mot se trouve dans une table
static BOOL bTrouveMot (TABLE_MOTS * ppMotsRef, DWORD dwNbMots, PCSTR pszCherchee, DWORD * pdwNTrouve)
  {
  DWORD dwNMot;

	for (dwNMot = 0; dwNMot < dwNbMots; dwNMot++)
		{
		if (bStrEgales (*ppMotsRef, pszCherchee))
			{
			* pdwNTrouve = dwNMot;
			return TRUE;
			}
		ppMotsRef ++;
		}
	return FALSE;
  }

//-------------------------------------------------------------------------
// conversion d'un num�ro de police
static void ConvNPolice_REV_PM440 (DWORD * pdwNPolice)
	{
	static DWORD adwConvPolicesPM430ToNT500 [22] =
		{
		1, //"System Proportional"        , // Non utilis� par PcsGr
		0, //"Courier"                    , // celui-ci + suite sont utilis�s parPcsGr
		0, //"Courier Bold"               ,
		0, //"Courier Bold Italic"        ,
		0, //"Courier Italic"             ,
		1, //"Helv"                       , //
		1, //"Helv Bold"                  ,
		1, //"Helv Bold Italic"           ,
		1, //"Helv Italic"                ,
		1, //"Helvetica"                  , //
		1, //"Helvetica Bold"             ,
		1, //"Helvetica Bold Italic"      ,
		1, //"Helvetica Italic"           ,
		3, //"Symbol"                     , //
		2, //"Times New Roman"            , //
		2, //"Times New Roman Bold"       ,
		2, //"Times New Roman Bold Italic",
		2, //"Times New Roman Italic"     ,
		2, //"Tms Rmn"                    , //
		2, //"Tms Rmn Bold"               ,
		2, //"Tms Rmn Bold Italic"        ,
		2, //"Tms Rmn Italic"
		};

	if ((* pdwNPolice <0) || (* pdwNPolice >22))
		* pdwNPolice = 0;
	* pdwNPolice = adwConvPolicesPM430ToNT500[* pdwNPolice];
	}

// import
//-------------------------------------------------------------------------
static DWORD lire_numero_elt (DWORD repere_nombre, DWORD index_repere_nombre, DWORD index_repere_element)
  {
  DWORD n_element = 0;

  switch (repere_nombre)
    {
    case B_PAGES_GR:
			{
			PPAGE_GR ppage_gr = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_nombre, index_repere_nombre);

      n_element = index_repere_element - ppage_gr->PremierElement + 1;
			}
      break;
    case B_GEO_ELEMENTS_LISTES_GR:
			{
			PGEO_ELEMENTS_LISTES_GR	pgeo_liste = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, repere_nombre, index_repere_nombre);

      n_element = index_repere_element - pgeo_liste->PremierElement + 1;
			}
      break;
    }
  return n_element;
  }

//-------------------------------------------------------------------------
void static maj_nombre_elt (DWORD repere_nombre, DWORD repere_element,
                            DWORD index_repere_nombre, DWORD *index_repere_element)
  {
  DWORD n_enr;
  DWORD dernier_enr;

  switch (repere_nombre)
    {
    case B_PAGES_GR:
			{
			PPAGE_GR ppage_gr = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_nombre, index_repere_nombre);
      if (ppage_gr->NbElements == 0)
        {
        ppage_gr->PremierElement = nb_enregistrements (szVERIFSource, __LINE__, repere_element) + 1;
        }
      ppage_gr->NbElements++;
      (*index_repere_element) = ppage_gr->PremierElement + ppage_gr->NbElements - 1;
      insere_enr (szVERIFSource, __LINE__, 1, repere_element, (*index_repere_element));
      dernier_enr = nb_enregistrements (szVERIFSource, __LINE__, repere_nombre) + 1;
      for (n_enr = 1; n_enr < dernier_enr; n_enr++)
        {
        ppage_gr = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_nombre, n_enr);
        if (ppage_gr->PremierElement > (*index_repere_element))
          {
          ppage_gr->PremierElement++;
          }
        else
          {
          if ((ppage_gr->PremierElement == (*index_repere_element)) && (n_enr != index_repere_nombre))
            {
            ppage_gr->PremierElement++;
            }
          }
        }
			}
      break;
    case B_GEO_ELEMENTS_LISTES_GR:
			{
			PGEO_ELEMENTS_LISTES_GR	pgeo_liste = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, repere_nombre, index_repere_nombre);
      if (pgeo_liste->NbElements == 0)
        {
        pgeo_liste->PremierElement = nb_enregistrements (szVERIFSource, __LINE__, repere_element) + 1;
        }
      pgeo_liste->NbElements++;
      (*index_repere_element) = pgeo_liste->PremierElement + pgeo_liste->NbElements - 1;
      insere_enr (szVERIFSource, __LINE__, 1, repere_element, (*index_repere_element));
      dernier_enr = nb_enregistrements (szVERIFSource, __LINE__, repere_nombre) + 1;
      for (n_enr = 1; n_enr < dernier_enr; n_enr++)
        {
        pgeo_liste = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, repere_nombre, n_enr);
        if (pgeo_liste->PremierElement > (*index_repere_element))
          {
          pgeo_liste->PremierElement++;
          }
        else
          {
          if ((pgeo_liste->PremierElement == (*index_repere_element)) && (n_enr != index_repere_nombre))
            {
            pgeo_liste->PremierElement++;
            }
          }
        }
			}
      break;
    }
  } // maj_nombre_elt


//-------------------------------------------------------------------------
static void marquer_polygonne (HBDGR hbdgr)
  {
  DWORD w;
  DWORD nb_enr;
  PGR_POLY_TEMPO ptempo_poly;

  if (existe_repere (b_tempo_poly_gr))
    {
    nb_enr = nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr);
    for (w = 1; (w <= nb_enr); w++)
      {
      ptempo_poly = (PGR_POLY_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_poly_gr, w);
      if (ptempo_poly->bloc_element == B_ELEMENTS_PAGES_GR)
        {
        bdgr_marquer_element (hbdgr, ptempo_poly->numero_element, MODE_VIRTUEL);
        }
      }
    }
  }
//-------------------------------------------------------------------------
static void link_liste (HBDGR hbdgr)
  {
  PELEMENT_PAGE_GR pelt_gr;
  PGEO_ELEMENTS_LISTES_GR	pgeo_elts_liste_gr;
  PGR_LIST_TEMPO	ptempo_list;
  PGR_LISTE_ELT_TEMPO ptempo_list_elt;
  DWORD w;
  DWORD nb_enr;

  nb_enr = nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_elt_gr);
  for (w = 1; w <= nb_enr; w++)
    {
    ptempo_list_elt = (PGR_LISTE_ELT_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_elt_gr, w);
    pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, ptempo_list_elt->bloc_element, ptempo_list_elt->numero_element);
    ptempo_list = (PGR_LIST_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_gr, ptempo_list_elt->numero_import);
    pgeo_elts_liste_gr = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, ptempo_list->numero_bd);
    pelt_gr->pt1.y = ptempo_list->numero_bd;
    (pgeo_elts_liste_gr->NbReferencesListe)++;
    }
  for (w = 1; w <= nb_enr; w++)
    {
    ptempo_list_elt = (PGR_LISTE_ELT_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_elt_gr, w);
    if ((ptempo_list_elt->bloc_element == B_ELEMENTS_PAGES_GR) && (hbdgr != NULL))
      {
      bdgr_marquer_element (hbdgr, ptempo_list_elt->n_element_relatif, MODE_VIRTUEL);
      }
    }
  } // link_liste

//-------------------------------------------------------------------------
DWORD	static link_animation_par_identifiant(DWORD action) //IMPORT_SYNOP_REMPLACEMENT ou IMPORT_SYNOP_AJOUT
	{
	PGEO_PAGES_GR pgeopage;
	PPAGE_GR ppage_gr;
	PELEMENT_PAGE_GR pelt_gr;
	DWORD dwRetour = 0;
	DWORD dwPosGeoAnimation = 1;
	char strIdentifiantAnimation [c_NB_CAR_ID_ELT_GR_ANIM + 1];
	DWORD dwNumeroPage;
	DWORD dwNbElements;
	BOOL bTrouve = FALSE;
	DWORD dwIndiceElement = 0;
	DWORD index_repere_element = 1;

	//Si ajout : reset tous les pelt_gr->PosGeoAnimation
	if (action==IMPORT_SYNOP_AJOUT)
		{
			for  (dwPosGeoAnimation = 1; (bdanmgr_get_identifiant_animation (dwPosGeoAnimation, &dwNumeroPage, strIdentifiantAnimation) && (dwRetour == 0)); dwPosGeoAnimation++) 
			{
			if (dwNumeroPage != 0)
				{
				pgeopage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, dwNumeroPage);
				ppage_gr = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, pgeopage->ReferencePage);
				dwNbElements = ppage_gr->NbElements;
				bTrouve = FALSE;
				for (dwIndiceElement = 0; ((dwIndiceElement < ppage_gr->NbElements) && (!bTrouve) && (dwRetour == 0)); dwIndiceElement++)
					{
					index_repere_element = ppage_gr->PremierElement + dwIndiceElement;
					pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, index_repere_element);
					pelt_gr->PosGeoAnimation =0;
					} // boucle element de la page
				}
			} // boucle animations graphiques
		}

	//
	for  (dwPosGeoAnimation = 1; (bdanmgr_get_identifiant_animation (dwPosGeoAnimation, &dwNumeroPage, strIdentifiantAnimation) && (dwRetour == 0)); dwPosGeoAnimation++) 
		{
		if (dwNumeroPage != 0)
			{
      pgeopage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, dwNumeroPage);
      ppage_gr = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, pgeopage->ReferencePage);
			dwNbElements = ppage_gr->NbElements;
			bTrouve = FALSE;
			for (dwIndiceElement = 0; ((dwIndiceElement < ppage_gr->NbElements) && (!bTrouve) && (dwRetour == 0)); dwIndiceElement++)
				{
				index_repere_element = ppage_gr->PremierElement + dwIndiceElement;
				pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, index_repere_element);
				bTrouve =  bStrEgales (strIdentifiantAnimation, pelt_gr->strIdentifiant);
				if (bTrouve)
					{
					if (pelt_gr->PosGeoAnimation != 0)
						{
						dwRetour = IDSERR_IDENTIFIANT_D_ANIMATION_GRAPHIQUE_EN_DOUBLON;
						}
					else
						{
						pelt_gr->PosGeoAnimation = dwPosGeoAnimation;
						}
					}
				} // boucle element de la page
			if (!bTrouve)
				{
				dwRetour = IDSERR_ANIMATION_ASSOCIEE_A_AUCUN_ELEMENT_GRAPHIQUE;
				char  mess_erreur[c_nb_car_message_err];
				char szErr[MAX_PATH+c_nb_car_message_err+100]="";
				bLngLireErreur (dwRetour, mess_erreur);
				StrPrintFormat (szErr,"%s : Ignorer cette erreur ?\n(Identifiant d'animation = %s)", mess_erreur, strIdentifiantAnimation);
				if (MessageBox (Appli.hwnd, szErr, "Erreur Import graphique : lien d'animation invalide",MB_ICONEXCLAMATION |MB_YESNO)==IDYES)
					{
					dwRetour = 0;
					}
				}
			}
		} // boucle animations graphiques
	return (dwRetour);
	}

//-------------------------------------------------------------------------
DWORD	static link_animation_par_pointeur()
	{
	DWORD dwRetour = 0;
	DWORD dwIndexElt = 1;
	PELEMENT_PAGE_GR pelt_gr;

	if (existe_repere (B_ELEMENTS_PAGES_GR))
		{
		for (dwIndexElt = 1; ((dwIndexElt <= nb_enregistrements (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR)) && (dwRetour == 0)); dwIndexElt++)
			{
			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, dwIndexElt);
			if (pelt_gr->PosGeoAnimation != 0)
				{
				dwRetour = bdanmgr_maj_identifiant_graphique (pelt_gr->PosGeoAnimation, pelt_gr->strIdentifiant);
				// Erreur mise � jour animation impossible ?
				if (dwRetour != 0)
					{
					// Oui => Signale l'erreur
					char  mess_erreur[c_nb_car_message_err];
					char szErr[MAX_PATH+c_nb_car_message_err+100]="";
					bLngLireErreur (dwRetour, mess_erreur);
					StrPrintFormat (szErr,"%s : Ignorer cette erreur ?\n(N� ligne dans le descripteur d'animation = %d)", mess_erreur, pelt_gr->PosGeoAnimation);
					if (MessageBox (Appli.hwnd, szErr, "Erreur Import graphique : lien d'animation invalide",MB_ICONEXCLAMATION |MB_YESNO)==IDYES)
						{
						pelt_gr->PosGeoAnimation = 0;
						dwRetour = 0;
						}
					}
				}
			}
		}
	return (dwRetour);
	}

//-------------------------------------------------------------------------
void static decode_mode (char *mode_mes, DWORD *mode_num, DWORD *pdwErr)
  {
	if (bTrouveMot (aszMode, c_NB_MODE, mode_mes, mode_num))
		(*mode_num)++;
	else
		*pdwErr = 415;
	}

//-------------------------------------------------------------------------
void static decode_couleur (PCSTR pszCouleur, PG_COULEUR pnCouleur, DWORD *pdwErr)
  {
	DWORD	dwTemp;

	if (bTrouveMot (aszCouleur, G_NB_COULEURS + OFFSET_aszCouleur, pszCouleur, &dwTemp))
		{
		*pnCouleur = (G_COULEUR)(dwTemp - OFFSET_aszCouleur);
		}
	else
		*pdwErr = 414;
  }

//-------------------------------------------------------------------------
static void decode_action (PCSTR pszAction, PG_ACTION pdwAction, DWORD *pdwRetour)
  {
	DWORD	dwTemp;

	if (bTrouveMot (aszAction, G_NB_ACTIONS, pszAction, &dwTemp))
		*pdwAction = (G_ACTION)dwTemp;
	else
    (*pdwRetour) = 413;
  }

//-------------------------------------------------------------------------
// d�code un style de ligne ou un style de remplissage
static void decode_style (PCSTR pszStyle, PG_STYLE_LIGNE pnStyle, DWORD *pdwErr)
  {
	DWORD	dwTemp;

	if (bTrouveMot (aszStyleLigne, 8 + OFFSET_aszStyleLigne, pszStyle, &dwTemp))
    {
		*pnStyle = (G_STYLE_LIGNE)(dwTemp - OFFSET_aszStyleLigne);

		// Conversion anciens styles de ligne
		if ((*pnStyle) >= G_NB_STYLES_LIGNE)
			*pnStyle = G_STYLE_LIGNE_POINTS;
    }
  else
    {
		if (bTrouveMot (aszStyleRemplissage, 8 + OFFSET_aszStyleRemplissage, pszStyle, &dwTemp))
			{
			*pnStyle = (G_STYLE_LIGNE) (dwTemp - OFFSET_aszStyleRemplissage);

			// Conversion anciens styles de remplissage
			if ((*pnStyle) >= G_NB_STYLES_REMPLISSAGE)
				*pnStyle = (G_STYLE_LIGNE)G_STYLE_REMPLISSAGE_DIAG_AV;
      }
    else
      *pdwErr = 412;
    }
  } // decode_style

//-------------------------------------------------------------------------
void static decode_reference (char * reference_mes, DWORD type_action, DWORD *reference_num)
  {
  StrDeleteDoubleQuotation (reference_mes);
  StrDelete (reference_mes, 0, StrLength (aszAction [type_action]));
  *reference_num = 0;
  StrToDWORD (reference_num, reference_mes);
  }

//-------------------------------------------------------------------------
void static decode_logique (BOOL * pbValeur, PCSTR pszLogique, DWORD * pdwErr)
  {
	char c = pszLogique[0];

	// chaine de un caract�re ?
  if ((!pszLogique[1]) && ((c == '1') || (c == '0')))
		*pbValeur = (c == '1');
	else
		*pdwErr = 411;
  }

//-------------------------------------------------------------------------
// R�partis les UL de la ligne lue dans tab_champs et r�cup�re et valide le type de ligne
static BOOL bAnalyseLigneLue (char *ligne, TYPE_LIGNE_GR nTypeLigneCourante,
	PTYPE_LIGNE_GR pTypeLigneLue, DWORD * pNbChampsLus, char tab_champs[][TAILLE_CHAMPS_GR])
  {
  BOOL bok;
	TYPE_LIGNE_GR nTypeLigneLue = *pTypeLigneLue;

	// Nettoie la ligne lue et r�partis ses mots dans le tableau de champs
  StrDeleteFirstSpaces (ligne);
  StrDeleteLastSpaces (ligne);
  (*pNbChampsLus) = StrToStrArray (tab_champs, NB_MAX_CHAMPS, ligne);

  // D�termine le type de ligne de la ligne lue
	if (*pNbChampsLus != 0)
    {
    if (bStrEgales (tab_champs [0], c_SYNTAXE_GR_SYNOP))
      nTypeLigneLue = TYPE_LIGNE_META_GR_SYNOP;
    else
    if (bStrEgales (tab_champs [0], c_SYNTAXE_GR_SYMBOL))
      nTypeLigneLue = TYPE_LIGNE_GR_VIDE;
    else
    if (bStrEgales (tab_champs [0], c_SYNTAXE_GR_PAGE))
      nTypeLigneLue = TYPE_LIGNE_META_GR_PAGE;
    else
    if (bStrEgales (tab_champs [0], c_SYNTAXE_GR_LIST))
      nTypeLigneLue = TYPE_LIGNE_META_GR_LIST;
    else
    if (bStrEgales (tab_champs [0], c_SYNTAXE_GR_POLYGON))
       nTypeLigneLue = TYPE_LIGNE_META_GR_POLYGON;
    else
    if (bStrEgales (tab_champs [0], c_SYNTAXE_GR_FIN))
      nTypeLigneLue = TYPE_LIGNE_META_GR_FIN;
    else
      {
      if (*pNbChampsLus == NB_CHAMPS_COORD_POLYGONNE)
        {
        nTypeLigneLue = TYPE_LIGNE_GR_COORD_POLYGONNE;
        }
      else
        {
			  DWORD index;

				if (bTrouveMot (aszAction, G_NB_ACTIONS, tab_champs [0], &index))
					nTypeLigneLue = aTypesLigneAction [index];
        else
          nTypeLigneLue = TYPE_LIGNE_GR_INVALIDE;
        }
      }
    }
  else
    nTypeLigneLue = TYPE_LIGNE_GR_VIDE;

	// Renvoie le type de ligne lue
	*pTypeLigneLue = nTypeLigneLue;

	// V�rifie si le type de ligne lue est compatible avec son contexte
  switch (nTypeLigneLue)
    {
    case TYPE_LIGNE_GR_VIDE:
      bok = TRUE;
      break;
    case TYPE_LIGNE_GR_INVALIDE:
      bok = FALSE;
      break;
    default:
      switch (nTypeLigneCourante)
        {
        case TYPE_LIGNE_GR_DEPART:
          bok = (nTypeLigneLue == TYPE_LIGNE_META_GR_SYNOP);
          break;
        case TYPE_LIGNE_META_GR_SYNOP:
          bok = (nTypeLigneLue == TYPE_LIGNE_META_GR_PAGE);
          break;
        case TYPE_LIGNE_META_GR_PAGE:
        case TYPE_LIGNE_GR_ELEMENTS_DESSIN:
        case TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN:
        case TYPE_LIGNE_GR_ELEMENTS_POLYGON:
        case TYPE_LIGNE_GR_ELEMENTS_POLYGON_PLEIN:
        case TYPE_LIGNE_GR_ELEMENTS_WRITE:
        case TYPE_LIGNE_GR_ELEMENTS_LISTE:
				case TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE:
				case TYPE_LIGNE_GR_ELEMENTS_CONT:
          bok = ((nTypeLigneLue == TYPE_LIGNE_META_GR_PAGE) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_DESSIN) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_POLYGON_PLEIN) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_POLYGON) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_WRITE) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_LISTE) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_CONT) ||
						(nTypeLigneLue == TYPE_LIGNE_META_GR_LIST) ||
						(nTypeLigneLue == TYPE_LIGNE_META_GR_POLYGON) ||
						(nTypeLigneLue == TYPE_LIGNE_META_GR_FIN));
          break;
        case TYPE_LIGNE_META_GR_LIST:
          bok = ((nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_DESSIN) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_POLYGON) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_POLYGON_PLEIN) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_WRITE) ||
						//(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE) ||
						//(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_CONT) ||
						(nTypeLigneLue == TYPE_LIGNE_GR_ELEMENTS_LISTE));
          break;
        case TYPE_LIGNE_META_GR_POLYGON:
          bok = ((nTypeLigneLue == TYPE_LIGNE_GR_COORD_POLYGONNE));
          break;
        case TYPE_LIGNE_GR_COORD_POLYGONNE:
          bok = ((nTypeLigneLue == TYPE_LIGNE_GR_COORD_POLYGONNE) ||
            (nTypeLigneLue == TYPE_LIGNE_META_GR_POLYGON) ||
            (nTypeLigneLue == TYPE_LIGNE_META_GR_FIN));
          break;
        default :
          bok = FALSE;
          break;
        } // switch nTypeLigneCourante
      break;
    } // switch type ligne lue

  return bok;
  } // bAnalyseLigneLue

//-------------------------------------------------------------------------
static void cree_tampon (void)
  {
  cree_bloc (b_tempo_poly_gr, sizeof (GR_POLY_TEMPO), 0);
  cree_bloc (b_tempo_liste_gr, sizeof (GR_LIST_TEMPO), 0);
  cree_bloc (b_tempo_liste_elt_gr, sizeof (GR_LISTE_ELT_TEMPO), 0);
  }

//-------------------------------------------------------------------------
static void supprime_tampon (void)
  {
  enleve_bloc (b_tempo_poly_gr);
  enleve_bloc (b_tempo_liste_gr);
  enleve_bloc (b_tempo_liste_elt_gr);
  }

//-------------------------------------------------------------------------
static BOOL initialise_bd_gr (DWORD action)
  {
  BOOL bRetour;

  bRetour = TRUE;
  bdgr_init_proc_anm (); //lis_nombre_de_ligne_vdi, valide_ligne_vdi,suppression_possible_vdi, NULL, NULL, NULL);
  if (action == IMPORT_SYNOP_REMPLACEMENT)
    {
    bRetour = bdgr_supprimer_tout_dessin ();
    }
  if (bRetour)
    {
    bdgr_creer ();
    cree_tampon ();
    }
  return (bRetour);
  }

//-------------------------------------------------------------------------
static DWORD recherche_numero_poly_bd_gr (DWORD numero_import, DWORD *numero_bd)
  {
  DWORD             dwErr;
  PGR_POLY_TEMPO	ptempo;
  DWORD             wIdxPoly;

  dwErr = 410;                 //Polygone NON Trouv�      (0 si trouv�)
  wIdxPoly = nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr);
  while ((wIdxPoly > 0) && (dwErr != 0))
    {
    ptempo = (PGR_POLY_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_poly_gr, wIdxPoly);
    if (ptempo->numero_import == numero_import)
      {
      dwErr = 0;               //Polygone trouv�
      (*numero_bd) = ptempo->numero_bd;
      }
    else
      {
      wIdxPoly--;
      }
    }

  return dwErr;
  } // recherche_numero_poly_bd_gr

//-------------------------------------------------------------------------
// Maj PosGeoAnimation et Identifiant element graphique suivant version
static DWORD MajPosGeoAnimEtIdentifiant (ELEMENT_PAGE_GR & pelt_gr, PSTR strChamp, DWORD offset_ligne_anim)
	{
	DWORD dwErr = 0;

	if (dwNumRev <= REV_NT500)
		{
		StrToDWORD (&(pelt_gr.PosGeoAnimation), strChamp);
		if (pelt_gr.PosGeoAnimation != 0)
			{
			pelt_gr.PosGeoAnimation += offset_ligne_anim;
			}
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, pelt_gr.strIdentifiant);
		}
	else
		{
		pelt_gr.PosGeoAnimation = 0;
		if (StrCharIsInStr('"', strChamp))
			{
			StrDeleteDoubleQuotation (strChamp);
			if (StrIsNull (strChamp))
				{
				GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, pelt_gr.strIdentifiant);
				}
			else
				{
				if (StrLength (strChamp) <= c_NB_CAR_ID_ELT_GR)
					{
					StrCopy (pelt_gr.strIdentifiant, strChamp);
					SetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, pelt_gr.strIdentifiant);
					}
				else
					{
					StrCopy (pelt_gr.strIdentifiant, "?");
					dwErr = IDSERR_L_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EST_TROP_LONG_20_CAR_MAX;
					}
				}
			}
		else
			{
			// pas de quotes => peut-etre import d'un symbole dont on ne connait pas le num�ro de r�vision
			DWORD dwTemp = 0;
			if (StrToDWORD (&dwTemp, strChamp))
				{
				if (dwTemp == 0)
					{
					// import d'un symbole de version < NT 5.10
					GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, pelt_gr.strIdentifiant);
					}
				else
					{
					StrCopy (pelt_gr.strIdentifiant, "?");
					dwErr = IDSERR_L_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EST_INVALIDE;
					}
				}
			else
				{
				StrCopy (pelt_gr.strIdentifiant, "?");
				dwErr = IDSERR_L_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EST_INVALIDE;
				}
			}
		}
	return (dwErr);
	}

//-------------------------------------------------------------------------
// Analyse et insertion dans la BDD d'une ligne PAGE
static DWORD insere_page_bd_gr (char tab_champs[][TAILLE_CHAMPS_GR], DWORD *numero_page, DWORD nb_champs)
  {
  int			numero_page_lu = 0;
  DWORD		dwErr = 0;

  StrToINT (&numero_page_lu, tab_champs[1]);
  if (numero_page_lu > 0)
    {
		DWORD pt_enr = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_PAGES_GR) + 1;
		PPAGE_GR						ppage;
		PMARQUES_GR					pmarque;
		PGEO_POINTS_SAUVEGARDES_GR	pgeo_sav;
		DWORD pt_enr_page;
		DWORD w;
		int nCoord;
		PGEO_PAGES_GR	pgeo_page;

    for (w = pt_enr;  (w <= ((DWORD)numero_page_lu)); w++)
      {
      insere_enr (szVERIFSource, __LINE__, 1, B_GEO_PAGES_GR, w);
      pgeo_page = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, w);
      pgeo_page->ReferencePage = 0;
      }
    pt_enr_page = nb_enregistrements (szVERIFSource, __LINE__, B_PAGES_GR) + 1;
    pgeo_page = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, numero_page_lu);
    if (pgeo_page->ReferencePage == 0)
      {
      pgeo_page->ReferencePage = pt_enr_page;
      //
      insere_enr (szVERIFSource, __LINE__, 1, B_PAGES_GR, pt_enr_page);
      ppage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, pt_enr_page);

      ppage->bFenModale = FALSE;
      ppage->bFenDialog = FALSE;
      ppage->bFenBmpFond = FALSE;
      StrSetNull (ppage->FenNomBmpFond);
      if (nb_champs == 24)
        {
        decode_logique (&(ppage->bFenModale), tab_champs[21], &dwErr);
        decode_logique (&(ppage->bFenBmpFond), tab_champs[22], &dwErr);
        StrDeleteDoubleQuotation (tab_champs[23]);
        if (StrLength (tab_champs[23]) <= c_NB_CAR_NOM_BMP_FOND)
          {
          StrCopy (ppage->FenNomBmpFond, tab_champs[23]);
          }
        }
			else
				{
				if (nb_champs == 25)
					{
					decode_logique (&(ppage->bFenModale), tab_champs[21], &dwErr);
					decode_logique (&(ppage->bFenDialog), tab_champs[22], &dwErr);
					decode_logique (&(ppage->bFenBmpFond), tab_champs[23], &dwErr);
					StrDeleteDoubleQuotation (tab_champs[24]);
					if (StrLength (tab_champs[24]) <= c_NB_CAR_NOM_BMP_FOND)
						{
						StrCopy (ppage->FenNomBmpFond, tab_champs[24]);
						}
					}
				}

      decode_couleur (tab_champs[3], &(ppage->FenCouleurFond), &dwErr);
      decode_logique (&(ppage->bFenMenu), tab_champs[13], &dwErr);
      StrDeleteDoubleQuotation (tab_champs[2]);
      if (StrLength (tab_champs[2]) <= c_NB_CAR_TITRE_PAGE_GR)
        {
        StrCopy (ppage->FenTextTitre, tab_champs[2]);
        //
        insere_enr (szVERIFSource, __LINE__, 1, B_MARQUES_GR, pt_enr_page);  // m�me n� enr que  B_PAGES_GR
        pmarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, pt_enr_page);
        pmarque->PremiereMarque = 1;
        pmarque->NbMarques = 0;
        (pmarque->RectMarques).x1 = 0;
        (pmarque->RectMarques).y1 = 0;
        (pmarque->RectMarques).x2 = 0;
        (pmarque->RectMarques).y2 = 0;
        //
        insere_enr (szVERIFSource, __LINE__, 1, B_GEO_POINTS_SAUVEGARDES_GR, pt_enr_page); // m�me n� enr que  B_PAGES_GR
        pgeo_sav = (PGEO_POINTS_SAUVEGARDES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR, pt_enr_page);
        pgeo_sav->PremierPointSauvegarde = 1;
        pgeo_sav->NbPointsSauvegarde = 0;
        }
      else
        {
        dwErr = 406;
        }
      decode_logique (&(ppage->bFenBorder), tab_champs[9], &dwErr);
      decode_logique (&(ppage->bFenSizeBorder), tab_champs[10], &dwErr);
      decode_logique (&(ppage->bFenTitre), tab_champs[17], &dwErr);
      decode_logique (&(ppage->bFenScrollBarH), tab_champs[11], &dwErr);
      decode_logique (&(ppage->bFenScrollBarV), tab_champs[12], &dwErr);
      decode_logique (&(ppage->bFenSysMenu), tab_champs[14], &dwErr);
      decode_logique (&(ppage->bFenMinBouton), tab_champs[15], &dwErr);
      decode_logique (&(ppage->bFenMaxBouton), tab_champs[16], &dwErr);
      ppage->FenGrilleX = 0;
      ppage->FenGrilleY = 0;
      nCoord = 0;
      StrToINT (&nCoord, tab_champs[4]);
      ppage->FrameFenX = nCoord;
      nCoord = 0;
      StrToINT (&nCoord, tab_champs[5]);
      ppage->FrameFenY = nCoord;
      nCoord = 0;
      StrToINT (&nCoord, tab_champs[6]);
      ppage->FrameFenCx = nCoord;
      nCoord = 0;
      StrToINT (&nCoord, tab_champs[7]);
      ppage->FrameFenCy = nCoord;
			
			// Mise � jour variable globale taille de la fen�tre (pour conversion haut bas PM<->NT)
			nCyPage = nCoord;

      ppage->OrigineX = 0;
      ppage->OrigineY = 0;
      decode_logique (&(ppage->bFenVisible), tab_champs[18], &dwErr);
      decode_mode (tab_champs[8], &(ppage->FenModeAff), &dwErr);
      ppage->FenFacteurZoom = 0;
      StrToDWORD (&ppage->FenFacteurZoom, tab_champs[19]);
      ppage->FenModeDeform = 0;
      StrToDWORD (&ppage->FenModeDeform, tab_champs[20]);
      if ((ppage->FenModeDeform != MODE_STANDARD) && (ppage->FenModeDeform != MODE_PROPORTIONNEL))
        {
        ppage->FenModeDeform = MODE_STANDARD;
        }
      ppage->FenPageMere = 0;
      ppage->FenEtat = 0;

      ppage->NbElements = 0;
      ppage->PremierElement = 1;
      (*numero_page) = (WORD)pt_enr_page;
			if (dwErr == 0)
				{
				// Conversions
				if (dwNumRev == REV_PM440)
					{
					ppage->bFenModale = FALSE;
					ppage->bFenDialog = FALSE;
					ppage->FrameFenX = (int)(SHORT)(ppage->FrameFenX);
					ppage->FrameFenY = (int)(SHORT)(ppage->FrameFenY);
					ppage->FrameFenY = c_G_NB_Y - ppage->FrameFenCy;
					ConvTexte_REV_PM440 (ppage->FenTextTitre);
					}
				}
      }
    else
      {
      dwErr = 405;
      }
    }
  else
    {
    dwErr = 405;
    }
  return dwErr;
  } // insere_page_bd_gr

//-------------------------------------------------------------------------
static void insere_liste_bd_gr (DWORD numero_import_liste, DWORD *numero_bd)
  {
	// insertion sur ligne de description @LIST "LISTx"
	// cree un nouvel enregistrement en position n=nombre+1 dans B_GEO_ELEMENTS_LISTES_GR avec valeurs initiales nulles
	// cree m enregistrements dans le bloc temporaire b_tempo_liste_gr pour completer le bloc jusqu'a l'enregistrement x
	// met dans l'enregistrement x du bloc b_tempo_liste_gr la valeur n pointant sur l'enr de B_GEO_ELEMENTS_LISTES_GR 
  DWORD nombre_insere_tempo;
  DWORD depart_tempo;
  DWORD w;
  PGEO_ELEMENTS_LISTES_GR	pgeo_elts_liste_gr;
  PGR_LIST_TEMPO ptempo;

  (*numero_bd) = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, B_GEO_ELEMENTS_LISTES_GR, (*numero_bd));
  if (numero_import_liste > nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_gr))
    {
    depart_tempo = nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_gr) + 1;
    nombre_insere_tempo = numero_import_liste - nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_gr);
    insere_enr (szVERIFSource, __LINE__, nombre_insere_tempo, b_tempo_liste_gr, depart_tempo);
    for (w = depart_tempo; (w < (depart_tempo+nombre_insere_tempo)); w++)
      {
      ptempo = (PGR_LIST_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_gr, w);
      ptempo->numero_bd = 0;
      }
    }
    pgeo_elts_liste_gr = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, (*numero_bd));
    pgeo_elts_liste_gr->PremierElement = 1;
    pgeo_elts_liste_gr->NbElements = 0;
    pgeo_elts_liste_gr->NbReferencesListe = 0;
    ptempo = (PGR_LIST_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_gr, numero_import_liste);
    ptempo->numero_bd = (*numero_bd);
  }
//-------------------------------------------------------------------------
static DWORD insere_coord_poly_bd_gr (char tab_champs[][TAILLE_CHAMPS_GR], DWORD numero_geo_poly)
  {
  DWORD								dwErr = 0;
  DWORD								index_poly;
  PPOINTS_POLY_GR			ppoints_poly;
  PGEO_POINTS_POLY_GR	pgeo_poly_gr;

  index_poly = nb_enregistrements (szVERIFSource, __LINE__, B_POINTS_POLY_GR) + 1;
  insere_enr (szVERIFSource, __LINE__, 1 , B_POINTS_POLY_GR, index_poly);
  ppoints_poly = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, index_poly);
  ppoints_poly->x = 0;
  StrToLONG (&ppoints_poly->x, tab_champs[0]);
  ppoints_poly->y = 0;
  StrToLONG (&ppoints_poly->y, tab_champs[1]);
	// Conversions
	if (dwNumRev == REV_PM440)
		{
		ConvDY_REV_PM440 (&ppoints_poly->y);
		}

  pgeo_poly_gr = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, numero_geo_poly);
  if (pgeo_poly_gr->NbPoints == 0)
    {
    pgeo_poly_gr->PremierPoint = index_poly;
    }
  (pgeo_poly_gr->NbPoints)++;
  return dwErr;
  }

//-------------------------------------------------------------------------
static DWORD insere_elt_dessin_bd_gr (DWORD repere_element, DWORD repere_nombre,
                                     DWORD index_repere_nombre, char tab_champs[][TAILLE_CHAMPS_GR],
                                     DWORD *n_element, DWORD offset_ligne_anim,
                                     DWORD nb_champ, BOOL bAvecIdentifiant)
  {
  DWORD dwErr = 0;
  DWORD index_repere_element;
  ELEMENT_PAGE_GR elt_gr = ELEMENT_PAGE_GR_NULL;
  PELEMENT_PAGE_GR pelt_gr;

  decode_action (tab_champs[0], &(elt_gr.nAction), &dwErr);

  decode_couleur (tab_champs[1], &(elt_gr.nCouleurLigne), &dwErr);
  elt_gr.nCouleurRemplissage = G_COULEUR_INDETERMINEE;

  decode_style (tab_champs[2], &(elt_gr.nStyleLigne), &dwErr);
  elt_gr.nStyleRemplissage = G_STYLE_REMPLISSAGE_PLEIN;

  StrToLONG (&elt_gr.pt0.x, tab_champs[3]);
  StrToLONG (&elt_gr.pt0.y, tab_champs[4]);
  StrToLONG (&elt_gr.pt1.x, tab_champs[5]);
  StrToLONG (&elt_gr.pt1.y, tab_champs[6]);

	// Conversions
	if (dwNumRev == REV_PM440)
		{
		ConvY_REV_PM440 (&elt_gr.pt0.y);
		ConvY_REV_PM440 (&elt_gr.pt1.y);
		}

  if ((nb_champ == 8) && (bAvecIdentifiant))
    {
		dwErr = MajPosGeoAnimEtIdentifiant (elt_gr, tab_champs[7], offset_ligne_anim);
    }
  else
    {
    elt_gr.PosGeoAnimation = 0;
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, elt_gr.strIdentifiant);
    }
  if (dwErr == 0)
    {
		if (bIdentifiantDejaPresent (repere_element, index_repere_nombre, elt_gr.strIdentifiant))
			{
			dwErr = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
			}
		else
			{
			maj_nombre_elt (repere_nombre, repere_element,index_repere_nombre, &index_repere_element);
			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			*pelt_gr = elt_gr;
			*n_element = lire_numero_elt (repere_nombre, index_repere_nombre, index_repere_element);
			}
    }
  return dwErr;
  } //insere_elt_dessin_bd_gr

//-------------------------------------------------------------------------
static DWORD insere_elt_dessin_plein_bd_gr (DWORD repere_element, DWORD repere_nombre,
																						DWORD index_repere_nombre, char tab_champs[][TAILLE_CHAMPS_GR],
																						DWORD *n_element, DWORD offset_ligne_anim,
																						DWORD nb_champ, BOOL bAvecIdentifiant)
  {
  DWORD dwErr = 0;
  DWORD index_repere_element;
  ELEMENT_PAGE_GR elt_gr = ELEMENT_PAGE_GR_NULL;
  PELEMENT_PAGE_GR pelt_gr;

  decode_action (tab_champs[0], &(elt_gr.nAction), &dwErr);

  decode_couleur (tab_champs[1], &(elt_gr.nCouleurRemplissage), &dwErr);
  elt_gr.nCouleurLigne = G_COULEUR_INDETERMINEE;

  decode_style (tab_champs[2], (PG_STYLE_LIGNE)&(elt_gr.nStyleRemplissage), &dwErr);
  elt_gr.nStyleLigne = G_STYLE_LIGNE_CONTINUE;

  StrToLONG (&elt_gr.pt0.x, tab_champs[3]);
  StrToLONG (&elt_gr.pt0.y, tab_champs[4]);
  StrToLONG (&elt_gr.pt1.x, tab_champs[5]);
  StrToLONG (&elt_gr.pt1.y, tab_champs[6]);

	// Conversions
	if (dwNumRev == REV_PM440)
		{
		ConvY_REV_PM440 (&elt_gr.pt0.y);
		ConvY_REV_PM440 (&elt_gr.pt1.y);
		}

  if ((nb_champ == 8) && (bAvecIdentifiant))
    {
		dwErr = MajPosGeoAnimEtIdentifiant (elt_gr, tab_champs[7], offset_ligne_anim);
    }
  else
    {
    elt_gr.PosGeoAnimation = 0;
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, elt_gr.strIdentifiant);
    }
  if (dwErr == 0)
    {
		if (bIdentifiantDejaPresent (repere_element, index_repere_nombre, elt_gr.strIdentifiant))
			{
			dwErr = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
			}
		else
			{
			maj_nombre_elt (repere_nombre, repere_element,index_repere_nombre, &index_repere_element);
			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			*pelt_gr = elt_gr;
			*n_element = lire_numero_elt (repere_nombre, index_repere_nombre, index_repere_element);
			}
    }
  return dwErr;
  } //insere_elt_dessin_plein_bd_gr

//-------------------------------------------------------------------------
static DWORD insere_elt_cont_gr (DWORD repere_element, DWORD repere_nombre,
                                     DWORD index_repere_nombre, char tab_champs[][TAILLE_CHAMPS_GR],
                                     DWORD *n_element, DWORD offset_ligne_anim,
                                     DWORD nb_champ, BOOL bAvecIdentifiant)
  {
  DWORD dwErr = 0;
  DWORD index_repere_element;
  ELEMENT_PAGE_GR elt_gr= ELEMENT_PAGE_GR_NULL;
  PELEMENT_PAGE_GR pelt_gr;

  decode_action (tab_champs[0], &(elt_gr.nAction), &dwErr);
  StrToLONG ((PLONG)&elt_gr.nStyleRemplissage, tab_champs[1]);
  StrToLONG (&elt_gr.pt0.x, tab_champs[2]);
  StrToLONG (&elt_gr.pt0.y, tab_champs[3]);
  StrToLONG (&elt_gr.pt1.x, tab_champs[4]);
  StrToLONG (&elt_gr.pt1.y, tab_champs[5]);

	// 
	if (bAvecIdentifiant)
		{
		dwErr = MajPosGeoAnimEtIdentifiant (elt_gr, tab_champs[6], offset_ligne_anim);
		}
	else
    {
    elt_gr.PosGeoAnimation = 0;
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, elt_gr.strIdentifiant);
    }

  if (dwErr == 0)
    {
		if (bIdentifiantDejaPresent (repere_element, index_repere_nombre, elt_gr.strIdentifiant))
			{
			dwErr = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
			}
		else
			{
			maj_nombre_elt (repere_nombre, repere_element,index_repere_nombre, &index_repere_element);
			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			(*pelt_gr) = elt_gr;
			(*n_element) = lire_numero_elt (repere_nombre, index_repere_nombre, index_repere_element);
			}
    }
  return dwErr;
  } //insere_elt_cont_bd_gr

//-------------------------------------------------------------------------
static DWORD insere_elt_cont_write_bd_gr (DWORD repere_element, DWORD repere_nombre,
                                    DWORD index_repere_nombre, char tab_champs[][TAILLE_CHAMPS_GR],
																		DWORD *n_element,
                                    DWORD offset_ligne_anim, DWORD nb_champ, BOOL bAvecIdentifiant)
  {
  DWORD							dwErr = 0;
  DWORD							index_repere_element;
  ELEMENT_PAGE_GR		elt_gr = ELEMENT_PAGE_GR_NULL;
  PELEMENT_PAGE_GR	pelt_gr;
  PGEO_TEXTES_GR		pgeo_ref_texte_gr;
  PTEXTES_GR				ptexte_gr;
  DWORD							position_texte;

  decode_action (tab_champs[0], &(elt_gr.nAction), &dwErr);
  StrToLONG ((PLONG)&elt_gr.nStyleRemplissage, tab_champs[1]);
  StrToLONG (&elt_gr.pt0.x, tab_champs[2]);
  StrToLONG (&elt_gr.pt0.y, tab_champs[3]);
  StrToLONG (&elt_gr.pt1.x, tab_champs[4]);
  StrToLONG (&elt_gr.pt1.y, tab_champs[5]);
  StrDeleteDoubleQuotation (tab_champs[6]);
  if (dwErr == 0)
    {
    if (StrLength (tab_champs[6]) <= c_NB_CAR_TEXTE_GR)
      {
      elt_gr.nStyleLigne = (G_STYLE_LIGNE)(nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1);
      position_texte = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, elt_gr.nStyleLigne);
      insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, position_texte);
      pgeo_ref_texte_gr = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, elt_gr.nStyleLigne);
      pgeo_ref_texte_gr->ReferenceTexte = position_texte;
      ptexte_gr  = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, position_texte);
      StrCopy (ptexte_gr->Texte, tab_champs[6]);
			// Num police
      ptexte_gr->Police = 0;
			// Style police
      ptexte_gr->StylePolice = 0;
      }
    else
      {
      dwErr = 404;
      }
    }
	if (bAvecIdentifiant)
		{
		dwErr = MajPosGeoAnimEtIdentifiant (elt_gr, tab_champs[7], offset_ligne_anim);
		}
	else
    {
    elt_gr.PosGeoAnimation = 0;
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, elt_gr.strIdentifiant);
    }

  if (dwErr == 0)
    {
		if (bIdentifiantDejaPresent (repere_element, index_repere_nombre, elt_gr.strIdentifiant))
			{
			dwErr = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
			}
		else
			{
			maj_nombre_elt (repere_nombre, repere_element, index_repere_nombre, &index_repere_element);
			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			(*pelt_gr) = elt_gr;
			(*n_element) = lire_numero_elt (repere_nombre, index_repere_nombre, index_repere_element);
			}
    }

  return dwErr;
  } // insere_elt_cont_write_bd_gr


//-------------------------------------------------------------------------
static DWORD insere_elt_liste_bd_gr (DWORD repere_element, DWORD repere_nombre,
                                    DWORD index_repere_nombre, char tab_champs[][TAILLE_CHAMPS_GR],
                                    DWORD offset_ligne_anim, DWORD nb_champ, BOOL bAvecIdentifiant)
  {
  DWORD dwErr = 0;
  DWORD index_repere_element;
  DWORD index_repere_tempo;
  DWORD numero_import;
  ELEMENT_PAGE_GR elt_gr = ELEMENT_PAGE_GR_NULL;
  PELEMENT_PAGE_GR pelt_gr;
  PGR_LISTE_ELT_TEMPO ptempo;

  decode_action (tab_champs[0], &(elt_gr.nAction), &dwErr);
  decode_couleur (tab_champs[1], &(elt_gr.nCouleurLigne), &dwErr);
  elt_gr.nCouleurRemplissage = elt_gr.nCouleurLigne;

  decode_style (tab_champs[2], &(elt_gr.nStyleLigne), &dwErr);

  decode_style (tab_champs[3], (PG_STYLE_LIGNE)&(elt_gr.nStyleRemplissage), &dwErr);
  StrToLONG (&elt_gr.pt0.x, tab_champs[4]);
  StrToLONG (&elt_gr.pt0.y, tab_champs[5]);
	// Conversions
	if (dwNumRev == REV_PM440)
		{
		ConvDY_REV_PM440 (&elt_gr.pt0.y);
		}

  decode_reference (tab_champs[6], G_ACTION_LISTE, &numero_import);
  if ((nb_champ == 8) && (bAvecIdentifiant))
    {
		dwErr = MajPosGeoAnimEtIdentifiant (elt_gr, tab_champs[7], offset_ligne_anim);
		}
	else
		{
    elt_gr.PosGeoAnimation = 0;
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, elt_gr.strIdentifiant);
		}

  if (dwErr == 0)
    {
		if (bIdentifiantDejaPresent (repere_element, index_repere_nombre, elt_gr.strIdentifiant))
			{
			dwErr = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
			}
		else
			{
			maj_nombre_elt (repere_nombre, repere_element, index_repere_nombre, &index_repere_element);
			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			(*pelt_gr) = elt_gr;
			index_repere_tempo = nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_elt_gr) + 1;
			insere_enr (szVERIFSource, __LINE__, 1, b_tempo_liste_elt_gr, index_repere_tempo);
			ptempo = (PGR_LISTE_ELT_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_elt_gr, index_repere_tempo);
			ptempo->numero_import = numero_import;
			ptempo->numero_element = index_repere_element;
			ptempo->bloc_element = repere_element;
			ptempo->n_element_relatif = lire_numero_elt (repere_nombre, index_repere_nombre, index_repere_element);
			}
    }
  return dwErr;
  } // insere_elt_liste_bd_gr

//-------------------------------------------------------------------------
static DWORD insere_elt_polygon_bd_gr
	(DWORD repere_element, DWORD repere_nombre,
	DWORD index_repere_nombre, char tab_champs[][TAILLE_CHAMPS_GR],
	DWORD *n_element,
	DWORD offset_ligne_anim, DWORD nb_champ, BOOL bAvecIdentifiant)

  {
  DWORD               dwErr = 0;
  DWORD               index_repere_element;
  DWORD               numero_import;
  ELEMENT_PAGE_GR     elt_gr = ELEMENT_PAGE_GR_NULL;
  PELEMENT_PAGE_GR    pelt_gr;
  PGEO_POINTS_POLY_GR	pgeo_poly_gr;
  PGR_POLY_TEMPO		ptempo;
  DWORD               wIdxPoly;

  decode_action (tab_champs[0], &(elt_gr.nAction), &dwErr);
  decode_couleur (tab_champs[1], &(elt_gr.nCouleurLigne), &dwErr);
  elt_gr.nCouleurRemplissage = G_COULEUR_INDETERMINEE;

  decode_style (tab_champs[2], &(elt_gr.nStyleLigne), &dwErr);
  elt_gr.nStyleRemplissage = G_STYLE_REMPLISSAGE_PLEIN;
  StrToLONG (&elt_gr.pt0.x, tab_champs[3]);
  StrToLONG (&elt_gr.pt0.y, tab_champs[4]);
	// Conversions
	if (dwNumRev == REV_PM440)
		{
		ConvY_REV_PM440 (&elt_gr.pt0.y);
		}

  decode_reference (tab_champs[5], G_ACTION_POLYGONE, &numero_import);
  if ((nb_champ == 7) && (bAvecIdentifiant))
    {
		dwErr = MajPosGeoAnimEtIdentifiant (elt_gr, tab_champs[6], offset_ligne_anim);
    }
	else
		{
    elt_gr.PosGeoAnimation = 0;
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, elt_gr.strIdentifiant);
		}

  if (dwErr == 0)
    {
		if (bIdentifiantDejaPresent (repere_element, index_repere_nombre, elt_gr.strIdentifiant))
			{
			dwErr = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
			}
		else
			{
			maj_nombre_elt (repere_nombre, repere_element, index_repere_nombre, &index_repere_element);
			(*n_element) = lire_numero_elt (repere_nombre, index_repere_nombre, index_repere_element);
			if (recherche_numero_poly_bd_gr (numero_import, (PDWORD)&(elt_gr.pt1.y)) != 0) //$$
				{
				elt_gr.pt1.y = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR) + 1;
				insere_enr (szVERIFSource, __LINE__, 1, B_GEO_POINTS_POLY_GR, elt_gr.pt1.y);
				pgeo_poly_gr = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, elt_gr.pt1.y);
				pgeo_poly_gr->PremierPoint = 1;
				pgeo_poly_gr->NbPoints = 0;
				pgeo_poly_gr->NbReferencesPoly = 1;
				}
			else
				{
				pgeo_poly_gr = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, elt_gr.pt1.y);
				(pgeo_poly_gr->NbReferencesPoly)++;
				}
			wIdxPoly = nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr) + 1;
			insere_enr (szVERIFSource, __LINE__, 1, b_tempo_poly_gr, wIdxPoly);
			ptempo = (PGR_POLY_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_poly_gr, wIdxPoly);
			ptempo->numero_bd      = elt_gr.pt1.y;
			ptempo->bloc_element   = repere_element;
			ptempo->numero_element = (*n_element);
			ptempo->numero_import  = numero_import;

			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			*pelt_gr = elt_gr;
			}
    }
  return dwErr;
  } // insere_elt_polygon_bd_gr

//-------------------------------------------------------------------------
static DWORD insere_elt_polygon_plein_bd_gr
	(DWORD repere_element, DWORD repere_nombre,
	DWORD index_repere_nombre, char tab_champs[][TAILLE_CHAMPS_GR],
	DWORD *n_element,
	DWORD offset_ligne_anim, DWORD nb_champ, BOOL bAvecIdentifiant)

  {
  DWORD               dwErr = 0;
  DWORD               index_repere_element;
  DWORD               numero_import;
  ELEMENT_PAGE_GR     elt_gr = ELEMENT_PAGE_GR_NULL;
  PELEMENT_PAGE_GR    pelt_gr;
  PGEO_POINTS_POLY_GR	pgeo_poly_gr;
  PGR_POLY_TEMPO		ptempo;
  DWORD               wIdxPoly;

  decode_action (tab_champs[0], &(elt_gr.nAction), &dwErr);
  decode_couleur (tab_champs[1], &(elt_gr.nCouleurRemplissage), &dwErr);
  elt_gr.nCouleurLigne = G_COULEUR_INDETERMINEE;

  decode_style (tab_champs[2], (PG_STYLE_LIGNE)&(elt_gr.nStyleRemplissage), &dwErr);
  elt_gr.nStyleLigne = G_STYLE_LIGNE_CONTINUE;
  StrToLONG (&elt_gr.pt0.x, tab_champs[3]);
  StrToLONG (&elt_gr.pt0.y, tab_champs[4]);
	// Conversions
	if (dwNumRev == REV_PM440)
		{
		ConvY_REV_PM440 (&elt_gr.pt0.y);
		}

  decode_reference (tab_champs[5], G_ACTION_POLYGONE, &numero_import);
  if ((nb_champ == 7) && (bAvecIdentifiant))
    {
		dwErr = MajPosGeoAnimEtIdentifiant (elt_gr, tab_champs[6], offset_ligne_anim);
    }
	else
		{
    elt_gr.PosGeoAnimation = 0;
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, elt_gr.strIdentifiant);
		}
  if (dwErr == 0)
    {
		if (bIdentifiantDejaPresent (repere_element, index_repere_nombre, elt_gr.strIdentifiant))
			{
			dwErr = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
			}
		else
			{
			maj_nombre_elt (repere_nombre, repere_element, index_repere_nombre, &index_repere_element);
			(*n_element) = lire_numero_elt (repere_nombre, index_repere_nombre, index_repere_element);
			if (recherche_numero_poly_bd_gr (numero_import, (PDWORD)&(elt_gr.pt1.y)) != 0)
				{
				elt_gr.pt1.y = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR) + 1;
				insere_enr (szVERIFSource, __LINE__, 1, B_GEO_POINTS_POLY_GR, elt_gr.pt1.y);
				pgeo_poly_gr = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, elt_gr.pt1.y);
				pgeo_poly_gr->PremierPoint = 1;
				pgeo_poly_gr->NbPoints = 0;
				pgeo_poly_gr->NbReferencesPoly = 1;
				}
			else
				{
				pgeo_poly_gr = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, elt_gr.pt1.y);
				(pgeo_poly_gr->NbReferencesPoly)++;
				}
			wIdxPoly = nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr) + 1;
			insere_enr (szVERIFSource, __LINE__, 1, b_tempo_poly_gr, wIdxPoly);
			ptempo = (PGR_POLY_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_poly_gr, wIdxPoly);
			ptempo->numero_bd      = elt_gr.pt1.y;
			ptempo->bloc_element   = repere_element;
			ptempo->numero_element = (*n_element);
			ptempo->numero_import  = numero_import;

			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			*pelt_gr = elt_gr;
			}
    }
  return dwErr;
  } // insere_elt_polygon_plein_bd_gr

//-------------------------------------------------------------------------
static DWORD insere_elt_write_bd_gr (DWORD repere_element, DWORD repere_nombre,
                                    DWORD index_repere_nombre, char tab_champs[][TAILLE_CHAMPS_GR],
																		DWORD *n_element,
                                    DWORD offset_ligne_anim, DWORD nb_champ, BOOL bAvecIdentifiant)
  {
  DWORD							dwErr = 0;
  DWORD							index_repere_element;
  ELEMENT_PAGE_GR		elt_gr = ELEMENT_PAGE_GR_NULL;
  PELEMENT_PAGE_GR	pelt_gr;
  PGEO_TEXTES_GR		pgeo_ref_texte_gr;
  PTEXTES_GR				ptexte_gr;
  DWORD							position_texte;

  decode_action (tab_champs[0], &(elt_gr.nAction), &dwErr);
  // $$ 2 couleurs � r�cup�rer et � dispatcher
  decode_couleur (tab_champs[1], &(elt_gr.nCouleurLigne), &dwErr);
  elt_gr.nCouleurRemplissage = elt_gr.nCouleurLigne;

  StrToLONG (&elt_gr.pt0.x, tab_champs[2]);
  StrToLONG (&elt_gr.pt0.y, tab_champs[3]);
  StrToLONG (&elt_gr.pt1.x, tab_champs[4]);
  StrToLONG (&elt_gr.pt1.y, tab_champs[5]);
  StrDeleteDoubleQuotation (tab_champs[6]);
  if (dwErr == 0)
    {
    if (StrLength (tab_champs[6]) <= c_NB_CAR_TEXTE_GR)
      {
      elt_gr.nStyleLigne = (G_STYLE_LIGNE)(nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1);
      position_texte = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, elt_gr.nStyleLigne);
      insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, position_texte);
      pgeo_ref_texte_gr = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, elt_gr.nStyleLigne);
      pgeo_ref_texte_gr->ReferenceTexte = position_texte;
      ptexte_gr  = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, position_texte);
      StrCopy (ptexte_gr->Texte, tab_champs[6]);
			// Num police
      ptexte_gr->Police = 0;
      StrToDWORD (&ptexte_gr->Police, tab_champs[7]);
			// Style police
      ptexte_gr->StylePolice = 0;
      StrToDWORD (&ptexte_gr->StylePolice, tab_champs[8]);
	
			// Conversions
			if (dwNumRev == REV_PM440)
				{
				int	yTemp;

				// champs attributs police
				// Inversion haut/bas
				ConvY_REV_PM440 (&elt_gr.pt0.y);
				ConvY_REV_PM440 (&elt_gr.pt1.y);
				yTemp = elt_gr.pt0.y;
				elt_gr.pt0.y = elt_gr.pt1.y;
				elt_gr.pt1.y = yTemp;
				switch (elt_gr.nAction)
					{
					case G_ACTION_H_TEXTE:
						// x2 devient largeur car (normale par d�faut)
						elt_gr.pt1.x = 0;
						// y2 devient la hauteur car absolue
						elt_gr.pt1.y -= elt_gr.pt0.y;
						if (elt_gr.pt1.y < 0)
							elt_gr.pt1.y = -elt_gr.pt1.y;
						break;

					case G_ACTION_V_TEXTE:
						// y2 devient la hauteur car absolue
						elt_gr.pt1.y = elt_gr.pt1.x - elt_gr.pt0.x;
						if (elt_gr.pt1.y < 0)
							elt_gr.pt1.y = -elt_gr.pt1.y;
						// x2 devient largeur car (normale par d�faut)
						elt_gr.pt1.x = 0;
						break;
					}

				// Conversion des polices
				ConvNPolice_REV_PM440 (&ptexte_gr->Police);

				// Conversion ASCII/ANSII
				ConvTexte_REV_PM440 (ptexte_gr->Texte);
				}
      }
    else
      {
      dwErr = 404;
      }
    }
  if ((nb_champ == 10) && (bAvecIdentifiant))
    {
		dwErr = MajPosGeoAnimEtIdentifiant (elt_gr, tab_champs[9], offset_ligne_anim);
    }
	else
		{
    elt_gr.PosGeoAnimation = 0;
		GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, elt_gr.strIdentifiant);
		}
  if (dwErr == 0)
    {
		if (bIdentifiantDejaPresent (repere_element, index_repere_nombre, elt_gr.strIdentifiant))
			{
			dwErr = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
			}
		else
			{
			maj_nombre_elt (repere_nombre, repere_element, index_repere_nombre, &index_repere_element);
			pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			(*pelt_gr) = elt_gr;
			(*n_element) = lire_numero_elt (repere_nombre, index_repere_nombre, index_repere_element);
			}
    }

  return dwErr;
  } // insere_elt_write_bd_gr


//-------------------------------------------------------------------------
//                         LOCAL EXPORT
//-------------------------------------------------------------------------
static void concatene_logique (char *ligne, BOOL bEtat)
  {
  if (bEtat)
    {
    StrConcat (ligne, c_VRAI);
    }
  else
    {
    StrConcat (ligne, c_FAUX);
    }
  }

//-------------------------------------------------------------------------
static void memorise_elt_poly (DWORD n_poly)
  {
  PGR_POLY_TEMPO ptemp_poly_gr;
  BOOL trouve;
  DWORD index;

  trouve = FALSE;
  index  = 1;
  while ((!trouve) && (index <= nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr)))
    {
    ptemp_poly_gr = (PGR_POLY_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_poly_gr, index);
    trouve = (BOOL) (ptemp_poly_gr->numero_bd == n_poly);
    index++;
    }
  if (!trouve)
    {
    insere_enr (szVERIFSource, __LINE__, 1, b_tempo_poly_gr, nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr) + 1);
    ptemp_poly_gr = (PGR_POLY_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_poly_gr, nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr));
    ptemp_poly_gr->numero_bd = n_poly;
    }
  }

//-------------------------------------------------------------------------
static void memorise_elt_liste (DWORD n_liste)
  {
  PGR_LIST_TEMPO ptemp_list_gr;
  BOOL trouve;
  DWORD index;

  trouve = FALSE;
  index  = 1;
  while ((!trouve) && (index <= nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_gr)))
    {
    ptemp_list_gr = (PGR_LIST_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_gr, index);
    trouve = (BOOL) (ptemp_list_gr->numero_bd == n_liste);
    index++;
    }
  if (!trouve)
    {
    insere_enr (szVERIFSource, __LINE__, 1, b_tempo_liste_gr, nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_gr) + 1);
    ptemp_list_gr = (PGR_LIST_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_gr, nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_gr));
    ptemp_list_gr->numero_bd = n_liste;
    }
  }

//-------------------------------------------------------------------------
static void encode_element (char *ligne, DWORD bloc, DWORD position_element, BOOL bAvecIdentifiant)
  {
  PELEMENT_PAGE_GR	pelement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, bloc, position_element);
  char							chaine_num[TAILLE_LIGNE_GR];
  char							chaine_texte[TAILLE_LIGNE_GR];
  PGEO_TEXTES_GR		pgeo_txt;
  PTEXTES_GR				ptxt;

  switch (pelement->nAction)
    {
    case G_ACTION_LIGNE          :
    case G_ACTION_RECTANGLE     :
    case G_ACTION_CERCLE        :
    case G_ACTION_QUART_ARC         :
    case G_ACTION_H_DEMI_CERCLE       :
    case G_ACTION_V_DEMI_CERCLE       :
    case G_ACTION_TRI_RECT      :
    case G_ACTION_H_TRI_ISO     :
    case G_ACTION_V_TRI_ISO     :
    case G_ACTION_H_VANNE       :
    case G_ACTION_V_VANNE       :
    case G_ACTION_QUART_CERCLE    :
    case G_ACTION_COURBE_T       :
    case G_ACTION_COURBE_C       :
    case G_ACTION_COURBE_A       :
    case G_ACTION_ENTREE_LOG   :
		  StrCopy (ligne, aszAction[pelement->nAction]);
			StrConcat (ligne, c_SEPARATEUR);
			StrConcat (ligne, aszCouleur[pelement->nCouleurLigne + OFFSET_aszCouleur]);

			StrConcat (ligne, c_SEPARATEUR);
			StrConcat (ligne, aszStyleLigne[pelement->nStyleLigne + OFFSET_aszStyleLigne]);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrSetNull (chaine_texte);
			if (bAvecIdentifiant)
				{
	      StrCopy (chaine_texte, pelement->strIdentifiant);
				}
      StrAddDoubleQuotation (chaine_texte);
      StrConcat (ligne, chaine_texte);
			break;

    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_BARGRAPHE        :
		  StrCopy (ligne, aszAction[pelement->nAction]);
			StrConcat (ligne, c_SEPARATEUR);
			StrConcat (ligne, aszCouleur[pelement->nCouleurRemplissage + OFFSET_aszCouleur]);
			StrConcat (ligne, c_SEPARATEUR);
			StrConcat (ligne, aszStyleRemplissage [pelement->nStyleRemplissage + OFFSET_aszStyleRemplissage]);

			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrSetNull (chaine_texte);
			if (bAvecIdentifiant)
				{
	      StrCopy (chaine_texte, pelement->strIdentifiant);
				}
      StrAddDoubleQuotation (chaine_texte);
      StrConcat (ligne, chaine_texte);
			break;

    case G_ACTION_H_TEXTE       :
    case G_ACTION_V_TEXTE       :
    case G_ACTION_STATIC_TEXTE :
    case G_ACTION_STATIC_NUM :
    case G_ACTION_EDIT_TEXTE :
    case G_ACTION_EDIT_NUM :
			StrCopy (ligne, aszAction[pelement->nAction]);
			StrConcat (ligne, c_SEPARATEUR);
			// $$ 2 couleurs
			StrConcat (ligne, aszCouleur[pelement->nCouleurLigne + OFFSET_aszCouleur]);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			pgeo_txt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pelement->nStyleLigne);
			ptxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pgeo_txt->ReferenceTexte);
			StrCopy (chaine_texte, ptxt->Texte);
			StrAddDoubleQuotation (chaine_texte);
			StrConcat (ligne, chaine_texte);
			StrConcat (ligne, c_SEPARATEUR);
			StrDWordToStr (chaine_num, ptxt->Police);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrDWordToStr (chaine_num, ptxt->StylePolice);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrSetNull (chaine_texte);
			if (bAvecIdentifiant)
				{
	      StrCopy (chaine_texte, pelement->strIdentifiant);
				}
      StrAddDoubleQuotation (chaine_texte);
      StrConcat (ligne, chaine_texte);
			break;

    case G_ACTION_POLYGONE     :
		  StrCopy (ligne, aszAction[pelement->nAction]);
			StrConcat (ligne, c_SEPARATEUR);
			StrConcat (ligne, aszCouleur[pelement->nCouleurLigne + OFFSET_aszCouleur]);
			StrConcat (ligne, c_SEPARATEUR);
			StrConcat (ligne, aszStyleLigne [pelement->nStyleLigne + OFFSET_aszStyleLigne]);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrCopy (chaine_texte, aszAction[G_ACTION_POLYGONE]);
			StrLONGToStr (chaine_num, pelement->pt1.y);
			StrConcat (chaine_texte, chaine_num);
			StrAddDoubleQuotation (chaine_texte);
			StrConcat (ligne, chaine_texte);
			memorise_elt_poly (pelement->pt1.y);
			StrConcat (ligne, c_SEPARATEUR);
			StrSetNull (chaine_texte);
			if (bAvecIdentifiant)
				{
	      StrCopy (chaine_texte, pelement->strIdentifiant);
				}
      StrAddDoubleQuotation (chaine_texte);
      StrConcat (ligne, chaine_texte);
			break;

    case G_ACTION_POLYGONE_PLEIN:
		  StrCopy (ligne, aszAction[pelement->nAction]);
			StrConcat (ligne, c_SEPARATEUR);
			StrConcat (ligne, aszCouleur[pelement->nCouleurRemplissage + OFFSET_aszCouleur]);
			StrConcat (ligne, c_SEPARATEUR);
			StrConcat (ligne, aszStyleRemplissage [pelement->nStyleRemplissage + OFFSET_aszStyleRemplissage]);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrCopy (chaine_texte, aszAction[G_ACTION_POLYGONE]);
			StrLONGToStr (chaine_num, pelement->pt1.y);
			StrConcat (chaine_texte, chaine_num);
			StrAddDoubleQuotation (chaine_texte);
			StrConcat (ligne, chaine_texte);
			memorise_elt_poly (pelement->pt1.y);
			StrConcat (ligne, c_SEPARATEUR);
			StrSetNull (chaine_texte);
			if (bAvecIdentifiant)
				{
	      StrCopy (chaine_texte, pelement->strIdentifiant);
				}
      StrAddDoubleQuotation (chaine_texte);
      StrConcat (ligne, chaine_texte);
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN: // LIST CoulRemp StyleL StyleR dx dy "LISTnAnim" nAnim
		  StrCopy (ligne, aszAction[pelement->nAction]);
			StrConcat (ligne, c_SEPARATEUR);
			if (pelement->nCouleurRemplissage != G_COULEUR_INDETERMINEE)
				{
				StrConcat (ligne, aszCouleur[pelement->nCouleurRemplissage + OFFSET_aszCouleur]);
				}
			else
				{
				StrConcat (ligne, c_NEUTRAL);
				}
			StrConcat (ligne, c_SEPARATEUR);
			if (pelement->nStyleLigne != G_STYLE_LIGNE_INDETERMINE)
				{
				StrConcat (ligne, aszStyleLigne [pelement->nStyleLigne + OFFSET_aszStyleLigne]);
				}
			else
				{
				StrConcat (ligne, c_NEUTRAL);
				}
			StrConcat (ligne, c_SEPARATEUR);
			if (pelement->nStyleRemplissage != G_STYLE_REMPLISSAGE_INDETERMINE)
				{
				StrConcat (ligne, aszStyleRemplissage [pelement->nStyleRemplissage + OFFSET_aszStyleRemplissage]);
				}
			else
				{
				StrConcat (ligne, c_NEUTRAL);
				}
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrCopy (chaine_texte, aszAction[G_ACTION_LISTE]);
			StrLONGToStr (chaine_num, pelement->pt1.y);
			StrConcat (chaine_texte, chaine_num);
			StrAddDoubleQuotation (chaine_texte);
			StrConcat (ligne, chaine_texte);
			memorise_elt_liste (pelement->pt1.y);
			StrConcat (ligne, c_SEPARATEUR);
			StrSetNull (chaine_texte);
			if (bAvecIdentifiant)
				{
	      StrCopy (chaine_texte, pelement->strIdentifiant);
				}
      StrAddDoubleQuotation (chaine_texte);
      StrConcat (ligne, chaine_texte);
			break;


		case G_ACTION_CONT_GROUP:
		case G_ACTION_CONT_BOUTON:
		case G_ACTION_CONT_CHECK:
		case G_ACTION_CONT_RADIO:
		case G_ACTION_CONT_BOUTON_VAL:
			StrCopy (ligne, aszAction[pelement->nAction]);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->nStyleRemplissage);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			pgeo_txt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pelement->nStyleLigne);
			ptxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pgeo_txt->ReferenceTexte);
			StrCopy (chaine_texte, ptxt->Texte);
			StrAddDoubleQuotation (chaine_texte);
			StrConcat (ligne, chaine_texte);
			StrConcat (ligne, c_SEPARATEUR);
			StrSetNull (chaine_texte);
			if (bAvecIdentifiant)
				{
	      StrCopy (chaine_texte, pelement->strIdentifiant);
				}
      StrAddDoubleQuotation (chaine_texte);
      StrConcat (ligne, chaine_texte);
			break;

		case G_ACTION_CONT_COMBO:
		case G_ACTION_CONT_LIST:
		case G_ACTION_CONT_STATIC:
		case G_ACTION_CONT_EDIT:
		case G_ACTION_CONT_SCROLL_H:
		case G_ACTION_CONT_SCROLL_V:
		  StrCopy (ligne, aszAction[pelement->nAction]);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->nStyleRemplissage);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt0.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.x);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrLONGToStr (chaine_num, pelement->pt1.y);
			StrConcat (ligne, chaine_num);
			StrConcat (ligne, c_SEPARATEUR);
			StrSetNull (chaine_texte);
			if (bAvecIdentifiant)
				{
	      StrCopy (chaine_texte, pelement->strIdentifiant);
				}
      StrAddDoubleQuotation (chaine_texte);
      StrConcat (ligne, chaine_texte);
			break;

    default:
			VerifWarningExit;
		  StrSetNull (ligne);
      break;
    }
  }

//-------------------------------------------------------------------------
// Ecriture du synoptique dans un fichier .SYN (pcsconf) ou .SNP (pcsgr)
BOOL bExporteSynoptique (PCSTR pszNomFic, BOOL bAvecIdentifiant)
  {
  CFman::HF	hf;
  BOOL bOk = TRUE;

	// Donn�es � exporter ?
  if (bdgr_nombre_pages () != 0)
    {
		// oui => cr�ation du fichier d'export Ok ?
    bOk = CFman::bFOuvre (&hf, pszNomFic, CFman::OF_CREE);
    if (bOk)
      {
			char ligne[TAILLE_LIGNE_GR];
			char chaine_num[TAILLE_LIGNE_GR];
			char chaine_texte[TAILLE_LIGNE_GR];
			PGEO_PAGES_GR	pgeopage;
			PPAGE_GR ppage;
			DWORD n_page;
			DWORD premier_element;
			DWORD dernier_element;
			DWORD position_element;
			PGEO_ELEMENTS_LISTES_GR	pgeo_liste;
			DWORD  dernier_geo_liste;
			DWORD  i_geo_liste;
			DWORD  premier_elmt_liste;
			DWORD  dernier_elmt_liste;
			DWORD  elmt_liste;
			PGEO_POINTS_POLY_GR	pgeo_poly;
			DWORD  dernier_geo_poly;
			DWORD  i_geo_poly;
			DWORD  premier_point_poly;
			DWORD  dernier_point_poly;
			DWORD  point_poly;
			PPOINTS_POLY_GR	ppoint_poly;


			// oui => c'est parti !
      cree_tampon ();
      StrSetNull (ligne);

			// Int�grit� base de donn�es: 
			// supprime les animations orphelines h�rit�es de versions pr�c�dentes
			bdanmgr_supprimer_tout_inutilise ();

      // En Tete pour signature de la version de syntaxe
      StrCopy (ligne, c_SYNTAXE_GR_SYNOP);
      StrConcat (ligne, c_SEPARATEUR);
      StrWORDToStr (chaine_num, REV_COURANTE);
      StrConcat (ligne, chaine_num);
      CFman::bFEcrireSzLn (hf, ligne);

      // Section PAGE
      for (n_page = 1; (n_page <= bdgr_nombre_pages ()); n_page ++)
        {
        if (bdgr_existe_page (n_page))
          {
          StrSetNull (ligne);
          StrSetNull (chaine_num);
          StrSetNull (chaine_texte);

          StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
          CFman::bFEcrireSzLn (hf, ligne);

          pgeopage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, n_page);
          ppage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, pgeopage->ReferencePage);
          StrCopy (ligne, c_SYNTAXE_GR_PAGE);

          StrConcat (ligne, c_SEPARATEUR);
          StrDWordToStr (chaine_num, n_page);
          StrConcat (ligne, chaine_num);

          StrConcat (ligne, c_SEPARATEUR);
          StrCopy (chaine_texte, ppage->FenTextTitre);
          StrAddDoubleQuotation (chaine_texte);
          StrConcat (ligne, chaine_texte);

          StrConcat (ligne, c_SEPARATEUR);
          StrConcat (ligne, aszCouleur[ppage->FenCouleurFond + OFFSET_aszCouleur]);

          StrConcat (ligne, c_SEPARATEUR);
          StrINTToStr (chaine_num, ppage->FrameFenX);
          StrConcat (ligne, chaine_num);

          StrConcat (ligne, c_SEPARATEUR);
          StrINTToStr (chaine_num, ppage->FrameFenY);
          StrConcat (ligne, chaine_num);

          StrConcat (ligne, c_SEPARATEUR);
          StrINTToStr (chaine_num, ppage->FrameFenCx);
          StrConcat (ligne, chaine_num);

          StrConcat (ligne, c_SEPARATEUR);
          StrINTToStr (chaine_num, ppage->FrameFenCy);
          StrConcat (ligne, chaine_num);

          StrConcat (ligne, c_SEPARATEUR);
          StrConcat (ligne, aszMode[(ppage->FenModeAff - 1)]);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenBorder);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenSizeBorder);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenScrollBarH);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenScrollBarV);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenMenu);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenSysMenu);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenMinBouton);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenMaxBouton);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenTitre);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenVisible);

          StrConcat (ligne, c_SEPARATEUR);
          StrDWordToStr (chaine_num, (DWORD) ppage->FenFacteurZoom);
          StrConcat (ligne, chaine_num);

          StrConcat (ligne, c_SEPARATEUR);
          StrDWordToStr (chaine_num, (DWORD) ppage->FenModeDeform);
          StrConcat (ligne, chaine_num);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenModale);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenDialog);

          StrConcat (ligne, c_SEPARATEUR);
          concatene_logique (ligne, ppage->bFenBmpFond);

          StrConcat (ligne, c_SEPARATEUR);
          StrCopy (chaine_texte, ppage->FenNomBmpFond);
          StrAddDoubleQuotation (chaine_texte);
          StrConcat (ligne, chaine_texte);

          CFman::bFEcrireSzLn (hf, ligne);

          StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
          CFman::bFEcrireSzLn (hf, ligne);

          // Section ELEMENT
          ppage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, pgeopage->ReferencePage);
          premier_element = ppage->PremierElement;
          dernier_element = ppage->NbElements + premier_element;
          for (position_element = premier_element; position_element < dernier_element; position_element++)
            {
            StrSetNull (ligne);
            encode_element (ligne, B_ELEMENTS_PAGES_GR, position_element, bAvecIdentifiant);
            CFman::bFEcrireSzLn (hf, ligne);
            }

          }
        } // for page

      // Section LISTE
      if (existe_repere (B_GEO_ELEMENTS_LISTES_GR))
        {
        dernier_geo_liste = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR);
        for (i_geo_liste = 1; (i_geo_liste <= dernier_geo_liste); i_geo_liste++)
          {
          pgeo_liste = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, i_geo_liste);
          if (pgeo_liste->NbReferencesListe != 0)
            {
            StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
            CFman::bFEcrireSzLn (hf, ligne);

            StrCopy (ligne, c_SYNTAXE_GR_LIST);
            StrConcat (ligne, c_SEPARATEUR);
            StrCopy (chaine_texte, aszAction[G_ACTION_LISTE]);
            StrDWordToStr (chaine_num, i_geo_liste);
            StrConcat (chaine_texte, chaine_num);
            StrAddDoubleQuotation (chaine_texte);
            StrConcat (ligne, chaine_texte);
            CFman::bFEcrireSzLn (hf, ligne);

            StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
            CFman::bFEcrireSzLn (hf, ligne);

            pgeo_liste = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, i_geo_liste);
            premier_elmt_liste = pgeo_liste->PremierElement;
            dernier_elmt_liste = premier_elmt_liste + pgeo_liste->NbElements;
            for (elmt_liste = premier_elmt_liste; (elmt_liste < dernier_elmt_liste); elmt_liste++)
              {
              StrSetNull (ligne);
              encode_element (ligne, B_ELEMENTS_LISTES_GR, elmt_liste, bAvecIdentifiant);
              CFman::bFEcrireSzLn (hf, ligne);
              }
            }
          }
        }

      // Section POLYGONE
      if (existe_repere (B_GEO_POINTS_POLY_GR))
        {
        dernier_geo_poly = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR);
        for (i_geo_poly = 1; (i_geo_poly <= dernier_geo_poly); i_geo_poly++)
          {
          pgeo_poly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, i_geo_poly);
          if (pgeo_poly->NbReferencesPoly != 0)
            {
            StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
            CFman::bFEcrireSzLn (hf, ligne);

            StrCopy (ligne, c_SYNTAXE_GR_POLYGON);
            StrConcat (ligne, c_SEPARATEUR);
            StrCopy (chaine_texte, aszAction[G_ACTION_POLYGONE]);
            StrDWordToStr (chaine_num, i_geo_poly);
            StrConcat (chaine_texte, chaine_num);
            StrAddDoubleQuotation (chaine_texte);
            StrConcat (ligne, chaine_texte);
            CFman::bFEcrireSzLn (hf, ligne);

            StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
            CFman::bFEcrireSzLn (hf, ligne);

            pgeo_poly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, i_geo_poly);
            premier_point_poly = pgeo_poly->PremierPoint;
            dernier_point_poly = premier_point_poly + pgeo_poly->NbPoints;
            for (point_poly = premier_point_poly; (point_poly < dernier_point_poly); point_poly++)
              {
              ligne [0] = '\0';
              ppoint_poly = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, point_poly);
              StrLONGToStr (ligne, (long)ppoint_poly->x);
              StrConcat (ligne, c_SEPARATEUR);
              StrLONGToStr (chaine_num, (long)ppoint_poly->y);
              StrConcat (ligne, chaine_num);
              CFman::bFEcrireSzLn (hf, ligne);
              }
            }
          }
        }

      // Section FIN
      StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
      CFman::bFEcrireSzLn (hf, ligne);
      StrCopy (ligne, c_SYNTAXE_GR_FIN);
      CFman::bFEcrireSzLn (hf, ligne);

      CFman::bFFerme (&hf);
      supprime_tampon ();
      }
    } // if (bdgr_nombre_pages () != 0)
	else
		{
		// non => pas d'export : efface le fichier s'il existe
		if (CFman::bFAcces (pszNomFic, CFman::OF_OUVRE_LECTURE_SEULE))
			bOk = CFman::bFEfface (pszNomFic);
		}

  return bOk;
  } // bExporteSynoptique

//-------------------------------------------------------------------------
// Exporte en un fichier les �l�ments marqu�s dans la page sp�cifi�e
BOOL bExporteSymbole (PCSTR pszNomFic, DWORD page, BOOL bAvecIdentifiant)
  {
  BOOL bOk = TRUE;
  PGEO_PAGES_GR pgeopage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, page);
  DWORD referencepage = pgeopage->ReferencePage;

  if (referencepage > 0)
    {
		PMARQUES_GR	pmarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, referencepage);
    if (pmarque->NbMarques > 0)
      {
			CFman::HF hf;

      bOk = CFman::bFOuvre (&hf, pszNomFic, CFman::OF_CREE);
      if (bOk)
        {
				char ligne[TAILLE_LIGNE_GR];
				char chaine_num[TAILLE_LIGNE_GR];
				char chaine_texte[TAILLE_LIGNE_GR];
				PGEO_MARQUES_GR	pgeomarque;
				DWORD premiere_geo_marque;
				DWORD derniere_geo_marque;
				DWORD position_geo_marque;
				PGR_LIST_TEMPO ptemp_list_gr;
				DWORD  i_tempo_list;
				PGEO_ELEMENTS_LISTES_GR	pgeo_liste;
				DWORD i_geo_list;
				DWORD  premier_elmt_liste;
				DWORD  dernier_elmt_liste;
				DWORD  elmt_liste;
				PGR_POLY_TEMPO ptemp_poly_gr;
				DWORD  i_tempo_poly;
				PGEO_POINTS_POLY_GR	pgeo_poly;
				DWORD  dernier_tempo_poly;
				DWORD  i_geo_poly;
				DWORD  premier_point_poly;
				DWORD  dernier_point_poly;
				DWORD  point_poly;
				PPOINTS_POLY_GR	ppoint_poly;

        cree_tampon ();
        StrSetNull (ligne);

        //------------------ En Tete pour faire joli --------------------------------
        StrCopy (ligne, c_SYNTAXE_GR_SYMBOL);
        CFman::bFEcrireSzLn (hf, ligne);
        StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
        CFman::bFEcrireSzLn (hf, ligne);

        //------------------ Section ELEMENT --------------------------------
        pmarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, referencepage);
        premiere_geo_marque = pmarque->PremiereMarque;
        derniere_geo_marque = pmarque->NbMarques + premiere_geo_marque;
        for (position_geo_marque = premiere_geo_marque; (position_geo_marque < derniere_geo_marque); position_geo_marque++)
          {
          StrSetNull (ligne);
          ligne [0] = '\0';
          pgeomarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, position_geo_marque);
          encode_element (ligne, B_ELEMENTS_PAGES_GR, pgeomarque->Element, bAvecIdentifiant);
          CFman::bFEcrireSzLn (hf, ligne);
          }

        //------------------ Section LISTE --------------------------------
        if (nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_gr) != 0)
          {
          // Attention laisser le nb_enregistrements dans la boucle for
          for (i_tempo_list = 1; (i_tempo_list <= nb_enregistrements (szVERIFSource, __LINE__, b_tempo_liste_gr)); i_tempo_list++)
            {
            ptemp_list_gr = (PGR_LIST_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_liste_gr, i_tempo_list);
            i_geo_list = ptemp_list_gr->numero_bd;
            pgeo_liste = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, i_geo_list);
            if (pgeo_liste->NbReferencesListe != 0)
              {
              StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
              CFman::bFEcrireSzLn (hf, ligne);

              StrCopy (ligne, c_SYNTAXE_GR_LIST);
              StrConcat (ligne, c_SEPARATEUR);
              StrCopy (chaine_texte, aszAction[G_ACTION_LISTE]);
              StrDWordToStr (chaine_num, i_geo_list);
              StrConcat (chaine_texte, chaine_num);
              StrAddDoubleQuotation (chaine_texte);
              StrConcat (ligne, chaine_texte);
              CFman::bFEcrireSzLn (hf, ligne);

              StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
              CFman::bFEcrireSzLn (hf, ligne);

              pgeo_liste = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, i_geo_list);
              premier_elmt_liste = pgeo_liste->PremierElement;
              dernier_elmt_liste = premier_elmt_liste + pgeo_liste->NbElements;
              for (elmt_liste = premier_elmt_liste; (elmt_liste < dernier_elmt_liste); elmt_liste++)
                {
                StrSetNull (ligne);
                encode_element (ligne, B_ELEMENTS_LISTES_GR, elmt_liste, bAvecIdentifiant);
                CFman::bFEcrireSzLn (hf, ligne);
                }
              }
            }
          }

        //------------------ Section POLYGONNE --------------------------------
        if (nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr) != 0)
          {
          dernier_tempo_poly = nb_enregistrements (szVERIFSource, __LINE__, b_tempo_poly_gr);
          for (i_tempo_poly = 1; (i_tempo_poly <= dernier_tempo_poly); i_tempo_poly++)
            {
            ptemp_poly_gr = (PGR_POLY_TEMPO)pointe_enr (szVERIFSource, __LINE__, b_tempo_poly_gr, i_tempo_poly);
            i_geo_poly = ptemp_poly_gr->numero_bd;
            pgeo_poly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, i_geo_poly);
            if (pgeo_poly->NbReferencesPoly != 0)
              {
              StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
              CFman::bFEcrireSzLn (hf, ligne);

              StrCopy (ligne, c_SYNTAXE_GR_POLYGON);
              StrConcat (ligne, c_SEPARATEUR);
              StrCopy (chaine_texte, aszAction[G_ACTION_POLYGONE]);
              StrDWordToStr (chaine_num, i_geo_poly);
              StrConcat (chaine_texte, chaine_num);
              StrAddDoubleQuotation (chaine_texte);
              StrConcat (ligne, chaine_texte);
              CFman::bFEcrireSzLn (hf, ligne);

              StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
              CFman::bFEcrireSzLn (hf, ligne);

              pgeo_poly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, i_geo_poly);
              premier_point_poly = pgeo_poly->PremierPoint;
              dernier_point_poly = premier_point_poly + pgeo_poly->NbPoints;
              for (point_poly = premier_point_poly; (point_poly < dernier_point_poly); point_poly++)
                {
                ligne [0] = '\0';
                ppoint_poly = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, point_poly);
                StrLONGToStr (ligne, ppoint_poly->x);
                StrConcat (ligne, c_SEPARATEUR);
                StrLONGToStr (chaine_num, ppoint_poly->y);
                StrConcat (ligne, chaine_num);
                CFman::bFEcrireSzLn (hf, ligne);
                }
              }
            }
          }

        //------------------ Section FIN --------------------------------
        StrCopy (ligne, c_SYNTAXE_GR_SAUT_LIGNE);
        CFman::bFEcrireSzLn (hf, ligne);
        StrCopy (ligne, c_SYNTAXE_GR_FIN);
        CFman::bFEcrireSzLn (hf, ligne);

        CFman::bFFerme (&hf);
        supprime_tampon ();
        }
      } // if (pmarque->NbMarques > 0)
    } // if (referencepage > 0)
  return bOk;
  } // bExporteSymbole

//------------------------------------------------------------------------------------------------------------------
// Import d'un synoptique. Conserve ou ignore les identifiants d'origine suivant bAvecIdentifiant
// Cree de nouveaux identifiants si version < 510 ou chaine vide ou bAvecIdentifiant � FALSE
DWORD dwImporteSynoptique (PCSTR pszNomFic, DWORD *pNLigne, DWORD action, DWORD offset_ligne_anim, BOOL bAvecIdentifiant)
  {
  DWORD dwErr = 0;
  CFman::HF hf;

  *pNLigne = 0;

	// Ouverture du fichier synoptique ok ?
  if (CFman::bFOuvre (&hf, pszNomFic, CFman::OF_OUVRE_LECTURE_SEULE))
    {
		t_contexte_ppgr contexte;
		DWORD n_element;
		TYPE_LIGNE_GR nTypeLigneLue;
		DWORD dwSignature = 0;
		char tab_champs[NB_MAX_CHAMPS][TAILLE_CHAMPS_GR];
		DWORD NbChampsLus;
		DWORD numero_ref_import;
		char ligne[TAILLE_LIGNE_GR];

		// Initialisation de l'import
    StrSetNull (ligne);
    nTypeLigneLue = TYPE_LIGNE_GR_VIDE;
    contexte.nTypeLigneCourante = TYPE_LIGNE_GR_DEPART;
    contexte.bInitialisation = FALSE;

		// boucle lecture ligne � ligne
    while ((dwErr == 0) && (nTypeLigneLue != TYPE_LIGNE_META_GR_FIN))
      {
      if (CFman::bFLireLn (hf, ligne, TAILLE_LIGNE_GR))
        {
				// R�partition de la ligne en champs, d�termination du type de ligne et 
				// v�rification de la coh�rence du s�quencement des lignes
        (*pNLigne)++;
        if (bAnalyseLigneLue (ligne, contexte.nTypeLigneCourante, &nTypeLigneLue, &NbChampsLus, tab_champs))
          {
					// traitement des champs de la ligne selon le type de ligne lue
          switch (nTypeLigneLue)
            {
            case TYPE_LIGNE_GR_VIDE:
              break;
            case TYPE_LIGNE_META_GR_SYNOP:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              if (NbChampsLus > 1)
                {
                StrToDWORD (&dwSignature, tab_champs[1]);
                }
							// R�cup�re les anciennes signatures qui �taient les num�ros de version de Processyn
							if ((dwSignature >= 410) && (dwSignature <= 450))
								{
								dwSignature = REV_PM440; // Signature synoptique compatible PM 430
								}

							// signature non g�r�e => erreur Version non importable
							if ((dwSignature < REV_PM440) || (dwSignature > REV_COURANTE))
								{
								dwErr = 22;
								}
							else
								{
								dwNumRev = dwSignature; //$$ Variable globale
								}
              break;

            case TYPE_LIGNE_META_GR_PAGE:
              if (!contexte.bInitialisation)
                {
                if (initialise_bd_gr (action))
                  {
                  contexte.bInitialisation = TRUE;
                  }
                else
                  {
                  dwErr = 432;
                  }
                }
              if (dwErr == 0)
                {
                contexte.nTypeLigneCourante = nTypeLigneLue;
                contexte.bloc_element = B_ELEMENTS_PAGES_GR;
                contexte.bloc_nombre = B_PAGES_GR;
                dwErr = insere_page_bd_gr (tab_champs, &contexte.numero_nombre, NbChampsLus);
                }
              break;
            case TYPE_LIGNE_META_GR_LIST:
              contexte.nTypeLigneCourante = TYPE_LIGNE_META_GR_LIST;
              contexte.bloc_element = B_ELEMENTS_LISTES_GR;
              contexte.bloc_nombre = B_GEO_ELEMENTS_LISTES_GR;
              decode_reference (tab_champs[1], G_ACTION_LISTE, &numero_ref_import);
              insere_liste_bd_gr (numero_ref_import, &contexte.numero_nombre);
              break;
            case TYPE_LIGNE_META_GR_POLYGON:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              decode_reference (tab_champs[1], G_ACTION_POLYGONE, &numero_ref_import);
              dwErr = recherche_numero_poly_bd_gr (numero_ref_import, &contexte.numero_polygonne);
              break;
            case TYPE_LIGNE_META_GR_FIN:
              break;
            case TYPE_LIGNE_GR_ELEMENTS_DESSIN:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_dessin_bd_gr (contexte.bloc_element, contexte.bloc_nombre, 
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_dessin_plein_bd_gr (contexte.bloc_element, contexte.bloc_nombre, 
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_cont_write_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_CONT:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_cont_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_LISTE:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_liste_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_POLYGON:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_polygon_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_POLYGON_PLEIN:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_polygon_plein_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_WRITE:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_write_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_COORD_POLYGONNE:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_coord_poly_bd_gr (tab_champs, contexte.numero_polygonne);
              break;
            default:
              dwErr = 403;
              break;
            } // switch (nTypeLigneLue)
          } // if (bAnalyseLigneLue (ligne
        else
          dwErr = 403;
        } // if (bFLireLn (hf, ligne, TAILLE_LIGNE_GR))
			else
				dwErr = 22;
      } // while
    if (contexte.bInitialisation)
      {
      if (dwErr == 0)
        {
        link_liste (NULL);
				if (dwNumRev <= REV_NT500)
					{
					dwErr = link_animation_par_pointeur();
					}
				else
					{
					dwErr = link_animation_par_identifiant(action); // Diff�rencie Import global ou ajout 
					}
        }
      }
    else
      {
      dwErr = 402;
      }
    supprime_tampon ();
    CFman::bFFerme (&hf);
    } // ok ouverture
  else
    {
    dwErr = 402;
    }
  return dwErr;
  }

//-------------------------------------------------------------------------
DWORD dwImporteSymbole (PCSTR pszNomFic, HBDGR hbdgr, DWORD * pNLigne, BOOL bAvecIdentifiant)
  {
  DWORD dwErr = 0;
	DWORD offset_ligne_anim = 0;
  CFman::HF hf;

  (*pNLigne) = 0;
  if (CFman::bFOuvre (&hf, pszNomFic, CFman::OF_OUVRE_LECTURE_SEULE))
    {
		DWORD page;
		TYPE_LIGNE_GR nTypeLigneLue;
		DWORD numero_ref_import;
		char ligne[TAILLE_LIGNE_GR];
		char tab_champs[NB_MAX_CHAMPS][TAILLE_CHAMPS_GR];
		DWORD NbChampsLus;
		t_contexte_ppgr contexte;
		PGEO_PAGES_GR	pgeopage;
		DWORD n_element;

    StrSetNull (ligne);
    cree_tampon ();
    nTypeLigneLue = TYPE_LIGNE_GR_VIDE;
    contexte.bInitialisation = FALSE;
    contexte.nTypeLigneCourante = TYPE_LIGNE_META_GR_PAGE;
    contexte.bloc_element = B_ELEMENTS_PAGES_GR;
    contexte.bloc_nombre = B_PAGES_GR;
    page = bdgr_lire_page (hbdgr);
    pgeopage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, page);
    contexte.numero_nombre = pgeopage->ReferencePage;
    while ((dwErr == 0) && (nTypeLigneLue != TYPE_LIGNE_META_GR_FIN))
      {
      if (CFman::bFLireLn (hf, ligne, TAILLE_LIGNE_GR))
        {
        (*pNLigne)++;
        if (bAnalyseLigneLue (ligne, contexte.nTypeLigneCourante, &nTypeLigneLue, &NbChampsLus, tab_champs))
          {
          switch (nTypeLigneLue)
            {
            case TYPE_LIGNE_GR_VIDE:
            case TYPE_LIGNE_META_GR_FIN:
              break;
            case TYPE_LIGNE_META_GR_LIST:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              contexte.bloc_element = B_ELEMENTS_LISTES_GR;
              contexte.bloc_nombre = B_GEO_ELEMENTS_LISTES_GR;
              decode_reference (tab_champs[1], G_ACTION_LISTE, &numero_ref_import);
              insere_liste_bd_gr (numero_ref_import, &contexte.numero_nombre);
              break;
            case TYPE_LIGNE_META_GR_POLYGON:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              decode_reference (tab_champs[1], G_ACTION_POLYGONE, &numero_ref_import);
              dwErr = recherche_numero_poly_bd_gr (numero_ref_import, &contexte.numero_polygonne);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_DESSIN:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              dwErr = insere_elt_dessin_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              if (contexte.bloc_element == B_ELEMENTS_PAGES_GR)
                bdgr_marquer_element (hbdgr, n_element, MODE_VIRTUEL);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_DESSIN_PLEIN:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              dwErr = insere_elt_dessin_plein_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              if (contexte.bloc_element == B_ELEMENTS_PAGES_GR)
                bdgr_marquer_element (hbdgr, n_element, MODE_VIRTUEL);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_CONT_WRITE:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_cont_write_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              if (contexte.bloc_element == B_ELEMENTS_PAGES_GR)
                bdgr_marquer_element (hbdgr, n_element, MODE_VIRTUEL);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_CONT:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              dwErr = insere_elt_cont_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              if (contexte.bloc_element == B_ELEMENTS_PAGES_GR)
                bdgr_marquer_element (hbdgr, n_element, MODE_VIRTUEL);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_LISTE:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              dwErr = insere_elt_liste_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_POLYGON:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              dwErr = insere_elt_polygon_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_POLYGON_PLEIN:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              dwErr = insere_elt_polygon_plein_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
								contexte.numero_nombre, tab_champs, &n_element, offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              break;
            case TYPE_LIGNE_GR_ELEMENTS_WRITE:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              dwErr = insere_elt_write_bd_gr (contexte.bloc_element, contexte.bloc_nombre,
                                                contexte.numero_nombre, tab_champs, &n_element,
                                                offset_ligne_anim, NbChampsLus, bAvecIdentifiant);
              if (contexte.bloc_element == B_ELEMENTS_PAGES_GR)
                {
                bdgr_marquer_element (hbdgr, n_element, MODE_VIRTUEL);
                }
              break;
            case TYPE_LIGNE_GR_COORD_POLYGONNE:
              contexte.nTypeLigneCourante = nTypeLigneLue;
              contexte.bInitialisation = TRUE;
              dwErr = insere_coord_poly_bd_gr (tab_champs, contexte.numero_polygonne);
              break;
            case TYPE_LIGNE_GR_DEPART:
            case TYPE_LIGNE_META_GR_SYNOP:
            case TYPE_LIGNE_META_GR_PAGE:
            case TYPE_LIGNE_GR_INVALIDE:
            default:
              dwErr = 403;
              break;
            } // switch (nTypeLigneLue)
          } // if (bAnalyseLigneLue (...
        else
          dwErr = 403;
        } // if (bFLireLn (hf, ligne, TAILLE_LIGNE_GR))
			else
				dwErr = 22;
      } // while
    if (contexte.bInitialisation)
      {
      if (dwErr == 0)
        {
        link_liste (hbdgr);
        marquer_polygonne (hbdgr);
        }
      }
    else
      {
      dwErr = 402;
      }
    supprime_tampon ();
    CFman::bFFerme (&hf);
    } // ok ouverture
  else
    {
    dwErr = 402;
    }
  return dwErr;
  }


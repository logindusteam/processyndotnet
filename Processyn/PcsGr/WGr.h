// --------------------------------------------------------------------------
//		WGr.h		Gestion de la fen�tre principale de PcsGr
// --------------------------------------------------------------------------

BOOL bCreeFenetreGr (void);
void FermeFenetreGr (void);

void initialiser_menu_gr (void);
void afficher_titre_menu_gr (char *texte);

void animer_menu_couleur_gr (DWORD id_action, DWORD id_couleur);

void animer_coord_gr (int coord_x, int coord_y);

void relacher_souris (void);
void patienter (BOOL patience);

void afficher_preambule (void);
int afficher_err (DWORD numero);

extern DWORD wEspaceH;
extern DWORD wEspaceV;


/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : actiongr.h                                               |
 |   Auteur  : AC					        	|
 |   Date    : 26/10/92					        	|
 |   Version : 3.20							|
 +----------------------------------------------------------------------*/


// actions de l'�diteur graphique PcsGr
typedef enum
	{
	action_charge_application         = 0,
	action_sauve_application          = 1,
	action_import                     = 2,
	action_export                     = 3,

	action_ouvre_fenetre              = 4, 
	action_ferme_fenetre              = 5, 
	action_supprime_fenetre           = 6, 
	action_active_fenetre             = 7, 
	action_change_style_fenetre       = 8, 
	action_change_pos_fenetre         = 9, 
	action_change_origine             = 10,
	action_redessine_paint            = 11,
	action_imprime                    = 12,
	action_quitte                     = 13,


	action_abandon                    = 14,
	action_mode_test                  = 15,

	action_colle_bit_map              = 16,
	action_charge_symb                = 17,
	action_sauve_symb                 = 18,

	action_lire_interactif            = 19,
	action_lire_macro                 = 20,

	action_click                      = 21,
	action_click_up                   = 22,
	action_click2                     = 23,
	action_double_click               = 24,
	action_goto_lc                    = 25,
	action_validation_dessin          = 26,
	action_validation_texte           = 27,
	action_fin_polyline               = 28,

	action_change_shape               = 29,
	action_change_style_remplissage   = 30,
	action_change_style_ligne         = 31,
	action_change_cursor_shape        = 32,
	action_change_color_dessin        = 33,
	action_change_color_fond_user     = 34,
	action_change_color_fond_page     = 35,
	action_change_bmp_fond_page       = 36,
	action_change_color               = 37,
	action_change_mode_color          = 38,
	action_change_police              = 39,

	action_animation_rl               = 40,

	action_titre_page                 = 41,

	action_tout_selectionner          = 42,
	action_preambule                  = 43,

	action_mode_dessin                = 44,
	action_mode_trace                 = 45,
	action_rotation                   = 46,
	action_symetrie_h                 = 47,
	action_symetrie_v                 = 48,
	action_group                      = 49,
	action_degroup                    = 50,
	action_duplicate                  = 51,
	action_change_remplissage         = 52,
	action_suppression                = 53,
	action_align_vc                   = 54,
	action_align_hc                   = 55,
	action_align_d                    = 56,
	action_align_b                    = 57,
	action_align_g                    = 58,
	action_align_h                    = 59,
	action_espace_h                   = 60,
	action_espace_v                   = 61,
	action_change_identifiant         = 62,

	action_passage_devant             = 63,
	action_passage_derriere           = 64,

	action_del                        = 65,

	action_mise_icone                 = 66,
	action_restore_dicone             = 67,

	action_anim_couleur2              = 68,
	action_anim_couleur4              = 69,

	action_charge_application_sat     = 70,
	action_sauve_application_sat      = 71,

	action_selectionner_animes				=	72,
	action_deselectionner_animes			= 73,
	action_selectionner_non_animes		=	74,
	action_deselectionner_non_animes	=	75,
	action_deplace_selection					=	76,
	action_selectionner_elt_hors_page	=	77,

	action_rien												= 255
	} ID_ACTION_GR, *PID_ACTION_GR;

#define DEBUG_MACRO TRUE
#define L_MAX_TEXTE_ACTION MAX_PATH

// ------------------------------ TYPES ---------------------------

typedef struct
  {
  ID_ACTION_GR tipe;
  DWORD id_fen;
  DWORD page;
  DWORD taille;
  G_COULEUR couleur;
  DWORD police;
  G_STYLE_LIGNE style;
  G_ACTION fonction;
  int param_fonction;
  FLOAT version;
  LONG delta_x;
  LONG delta_y;
  BOOL on;
  int x1;
  int y1;
  int x2;
  int y2;
  DWORD errorlevel;
  BOOL bFenModale;
	BOOL bFenDialog;
  BOOL bBitmapFond;
  BOOL un_titre;
  BOOL un_bord;
  BOOL une_barreh;
  BOOL une_barrev;
  BOOL un_sysmenu;
  BOOL un_minimize;
  BOOL un_maximize;
  DWORD grille_x;
  DWORD grille_y;
  DWORD mode_aff;
  DWORD mode_deform;
  DWORD facteur_zoom;
	char	szPath[MAX_PATH];
  } t_bloc_action;

typedef struct
	{
	DWORD tipe;
	DWORD id_fen;
	DWORD page_gr;
	DWORD taille;
	G_COULEUR couleur_gr;
	G_STYLE_LIGNE style_gr;
	DWORD police_gr;
	G_ACTION fonction;
	int		param_fonction;
	FLOAT version;
	LONG  delta_x;
	LONG  delta_y;
	BOOL	on;
	int		x1;
	int		y1;
	int		x2;
	int		y2;
	DWORD errorlevel_gr;
	char	texte[L_MAX_TEXTE_ACTION];
	char	szPath[MAX_PATH];
	BOOL	bFenModale;
	BOOL	bFenDialog;
	BOOL	bBitmapFond; //bmp_en_execution;
	BOOL	un_titre;
	BOOL	un_bord;
	BOOL	une_barreh;
	BOOL	une_barrev;
	BOOL	un_sysmenu;
	BOOL	un_minimize;
	BOOL	un_maximize;
	DWORD grille_x;
	DWORD grille_y;
	DWORD mode_aff;
	DWORD mode_deform;
	DWORD facteur_zoom;
	} t_param_action;

typedef struct
  {
  int mode_lecture;
  char chaine[L_MAX_TEXTE_ACTION];
  } t_retour_action;

// ------------------  PROCEDURES  -----------------------

void initialiser_action_gr (void);

// attention a la synchronisation car 'ajouter_action' dans window proc
void ajouter_action (ID_ACTION_GR action, t_param_action *param_action);
int executer_action_bufferise (t_retour_action *retour_action);
void executer_action_immediate (t_bloc_action *ptr_action, char *ptr_texte, t_retour_action *retour_action);


/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inpugr.h                                                 |
 |   Auteur  : AC					        	|
 |   Date    : 26/10/92 					        	|
 |   Version : 4.0							|
 |                    Auto Bonne Moyenne Impossible                     |
 |   Portabilite :     x						|
 |                                                                      |
 |   Mise a jours:                                                      |
 |                                                                      |
 |   +--------+--------+---+--------------------------------------+     |
 |   | date   | qui    |no | raisons                              |     |
 |   +--------+--------+---+--------------------------------------+     |
 |   |        |        |   |                                      |     |
 |   +--------+--------+---+--------------------------------------+     |
 |                                                                      |
 |   Remarques :                                                        |
 |                                                                      |
 |                                                                      |
 +----------------------------------------------------------------------*/


void *creer_lecture (void);
void init_lecture (void *ptr_lecture_editeur, int mode_lec, char *chaine);
void lire_commandes (void *ptr_lecture_editeur);

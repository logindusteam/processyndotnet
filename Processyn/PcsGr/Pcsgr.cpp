/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 +----------------------------------------------------------------------*/
// Pcsgr.c
// Editeur Graphique de processyn 
// Version Win32 5/3/97

#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "UStr.h"
#include "inpugr.h"
#include "UExcept.h"
#include "mem.h"
#include "trap.h"
#include "Versman.h"
#include "Fileman.h"
#include "G_Objets.h"
#include "actiongr.h"
#include "appligr.h"
#include "LireLng.h"
#include "pcsspace.h"
#include "g_sys.h"
#include "lng_res.h"
#include "Tipe.h"
#include "Descripteur.h"
#include "BdGr.h"
#include "bdanmgr.h"
#include "ctxtgr.h"
#include "WGr.h"
#include "WEditSyn.h"
#include "Verif.h"
#include "PcsGr.h"
VerifInit;

// --------------- fermeture des ressources materielles -------------------
void fermer_ressources (void)
  {
  arrete_trap ();
	fermer_bd();
  }

// --------------- ouverture des ressources materielles ------------------
int ouvrir_ressources (void)
  {
  int erreur = 0;

	creer_bd_vierge();

  lance_trap ();
  return erreur;
  }


//-------------------------------------
// fermeture des fenetres de PcsGr
static void supprimer_fenetres (void)
  {
  supprimer_espace_ref_fen_gr ();
  FermeFenetreGr ();
  }

//------------------------------------
// Corps de l'application
int WINAPI WinMain
	(
	HINSTANCE hInstance, // handle de cette instance de l'application
	HINSTANCE hPrevInstance,	// 0 sur Win32 actuel
	LPSTR lpCmdLine,	// param�tres de la ligne de commande
	int nCmdShow 	// show state of window
	)

  {
  t_retour_action retour_action;
  void *ptr_lecture_editeur;
  char ligne_commande[260];

	__try 
		{
		// Initialisations
		CHAR szVersionComplete [MAX_PATH];
		Appli.Init (hInstance, "Processyn synoptiques", GetVersionComplete(szVersionComplete));
	  bOuvrirLng ();
		ouvrir_ressources ();
	  patienter (TRUE);

		//InterpreteFichierIni (); $$ v�rifier
		initialiser_action_gr ();
		initialiser_ctxt_application ();
		initialiser_ctxtgr_editeur ();

		// creation des fenetres de PcsGr
		bCreeFenetreGr ();
		creer_espace_ref_fen_gr ();

		ptr_lecture_editeur = creer_lecture ();
		StrCopy (ligne_commande, lpCmdLine);
		init_lecture (ptr_lecture_editeur, MODE_DEMARRAGE, ligne_commande);
	  patienter (FALSE);
		lire_commandes (ptr_lecture_editeur);
		while (executer_action_bufferise (&retour_action) != action_quitte)
			{
			init_lecture (ptr_lecture_editeur, retour_action.mode_lecture, retour_action.chaine);
			lire_commandes (ptr_lecture_editeur);
			}
		supprimer_fenetres ();
		fermer_ressources ();
		}	
	__except (nSignaleExceptionUser(GetExceptionInformation()))
		{
		ExitProcess((UINT)-1);// handler des exceptions choisies par le filtre
		}
	return 0;//$$
  }

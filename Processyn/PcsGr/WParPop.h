// -------------------------------------------------------------------------
// WParPop.h
// Fen�tre parente invisible de Pop ups
// -------------------------------------------------------------------------

// ----------------------------------------------------------------------------
// Gestion de "modalit�" entre certains pop ups du parent
typedef enum {POPUP_INDEPENDANT, POPUP_MODAL, POPUP_NON_MODAL} MODALITE_POPUP;

// Message envoy� aux fen�tres popup pour connaitre leur �tat de modalit�:
// renvoyer une des valeurs MODALITE_POPUP (DefWindowProc renvoie POPUP_INDEPENDANT)
#define WM_GET_MODALITE_POPUP (WM_USER + 4)

// Message envoy� aux fen�tres popup pour signaler un evenement timer : gere egalement dans param une bascule
#define WM_TIMER_PARPOP (WM_USER + 5)


// ------------------------------------------------------------------------------------------------------------------
// Cr�e une fen�tre Parente des pop ups
HWND hwndCreeParentPopUp (void);

// ----------------------------------------------------------------------------------------------------------------------------
// Demarrage d'une fonction qui signale sous forme de message a toutes les fenetres filles un evenement de type timer � bascule
BOOL bLanceDiffusionEvtTimerPopUp (HWND hwndParentPopUp, UINT uiIntervalleTimer);

// ------------------------------------------------------------------------------------------------------------------
// Cr�ation de fen�tre pop up : met la fen�tre en conformit� avec sa modalit�
void OnCreatePopUp (HWND hwndParent, HWND hwndPopUp);

// Destruction d'une fen�tre pop up
void OnDestroyPopUp (HWND hwndParent, HWND hwndPopUp);

// R�ception d'un message WM_ACTIVATE par le pop up
void OnActivatePopUp (HWND hwndParent, HWND hwndPopUp, WPARAM wParam);

// pop up change de modalit� : met la fen�tre en conformit� avec son nouvel �tat modal ou pas
void ChangeModalitePopUp (HWND hwndParent, HWND hwndPopUp);



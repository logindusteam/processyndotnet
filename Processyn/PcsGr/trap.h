//-------------------------------------------------------------
//|   Ce fichier est la propriete de                           
//|              Societe LOGIQUE INDUSTRIE                     
//|       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3        
//|   Il est demeure sa propriete exclusive et est confidentiel
//|   Aucune diffusion n'est possible sans accord ecrit        
//|                                                            
//|   Titre   : trap.h							|
//|   Auteur  : LM							|
//|   Date    : 05-06-92 						|
//|  Gestion  des erreurs critiques
//-------------------------------------------------------------

void lance_trap (void);
void arrete_trap (void);

//-------------------------------------------------------------------------

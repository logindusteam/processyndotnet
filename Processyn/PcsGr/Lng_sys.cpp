/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/
// Lng_sys.cpp 
// Module de gestion de l'initialisation des variables syst�mes

// Chaque info variable syst�me est stock�e sous forme d'une string
// dans les ressources traduites.

// IDS_NB_VARIABLES_SYSTEME est l'identifiant d'une string contenant le nombre de variables syst�mes
// Les identifiants des mots r�serv�s valent IDS_NB_VARIABLES_SYSTEME+1, IDS_NB_VARIABLES_SYSTEME+2...
//
// A chaque variable syst�me correspond une string en ressource au format :
//	SSSS,GGGG,DDDD,VVVV,TTTTTT,NomVariable,ValeurInitale
// avec SSSS : sens_variable en d�cimal sur 4 chiffres (voir ID_MOT_RESERVE c_res_e...)
//			GGGG : genre_variable en d�cimal sur 4 chiffres (voir ID_MOT_RESERVE c_res_logique...)
//			DDDD : numero_descripteur en d�cimal sur 4 chiffres (voir ID_DESCRIPTEUR d_systeme...)
//			VVVV : numero_variable en d�cimal sur 4 chiffres () (voir ID_VAR_SYS VA_SYS_STATUS_OPC...)
//			TTTTTT : taille_tableau en d�cimal sur 6 chiffres
//			NomVariable : nom_variable
//			ValeurInitale : valeur_initiale
//
// Les Ids des strings en ressource importent peu tant qu'ils sont a des num�ros 
// cons�cutifs suivant IDS_NB_VARIABLES_SYSTEME

#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "Appli.h"
#include "PathMan.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "Verif.h"
#include "lng_res.h"
#include "lng_sys.h"
VerifInit;

#define OFFSET_SENS_VARIABLE 0
#define OFFSET_GENRE_VARIABLE 5
#define OFFSET_NUMERO_DESCRIPTEUR 10
#define OFFSET_NUMERO_VARIABLE 15
#define OFFSET_TAILLE_TABLEAU 20
#define OFFSET_NOM_VARIABLE 27
#define TAILLE_INFO_RESSOURCE (OFFSET_NOM_VARIABLE+1+c_nb_car_nom_variable_sys+c_nb_car_valeur_initiale_sys)

// Num�ro du dernier mot r�serv� � charger dans les ressources
DWORD dwNbVariablesSys = 0;


// --------------------------------------------------------------------------
// Initialiser le 'gestionnaire' des variables syst�mes
// --------------------------------------------------------------------------
BOOL bOuvrirVariablesSysteme (void) // Renvoie TRUE
  {
	char szNb [10];
	// Lecture du nombre de mots r�serv�s � charger
	dwNbVariablesSys = 0;
	VerifWarning (bLngLireString (IDS_NB_VARIABLES_SYSTEME, szNb, 10) && StrToDWORD (&dwNbVariablesSys, szNb));

  return TRUE; 
  }

// --------------------------------------------------------------------------
// Fermer le 'gestionnaire' des variables syst�mes
// --------------------------------------------------------------------------
void FermerVariablesSysteme (void)
  {
  }

// --------------------------------------------------------------------------
// Donner la variable syst�me numero xxx
// --------------------------------------------------------------------------
BOOL bLireVariableSysteme
	(
	DWORD numero,		// numero de l'info variable syst�me � lire
	PINFO_VAR_SYSTEME pInfoVarSysteme	// adresse d'un informations variable syst�me � mettre � jour
	)								// Renvoie TRUE si pas d'erreur
  {
	char szTemp [TAILLE_INFO_RESSOURCE];
	DWORD dwOffsetSeparateurValInit;

	// lecture de la ressource du mot r�serv� Ok ?
	BOOL	bRes = bLngLireString (numero + IDS_NB_VARIABLES_SYSTEME, szTemp, TAILLE_INFO_RESSOURCE) &&
		(StrLength (szTemp) >= (OFFSET_NOM_VARIABLE+1));

	if (bRes)
		{
		// oui => d�codage.
		szTemp[OFFSET_GENRE_VARIABLE-1] = '\0';
		szTemp[OFFSET_NUMERO_DESCRIPTEUR-1] = '\0';
		szTemp[OFFSET_NUMERO_VARIABLE-1] = '\0';
		szTemp[OFFSET_TAILLE_TABLEAU-1] = '\0';
		szTemp[OFFSET_NOM_VARIABLE-1] = '\0';
		DWORD dwTemp;
		VerifWarning(StrToDWORD  (&dwTemp, &szTemp[OFFSET_SENS_VARIABLE]));
		pInfoVarSysteme->sens_variable = (ID_MOT_RESERVE)dwTemp;
		VerifWarning(StrToDWORD  (&dwTemp,	&szTemp[OFFSET_GENRE_VARIABLE]));
		pInfoVarSysteme->genre_variable = (ID_MOT_RESERVE)dwTemp;
		VerifWarning(StrToDWORD  (&pInfoVarSysteme->numero_descripteur, &szTemp[OFFSET_NUMERO_DESCRIPTEUR]));
		VerifWarning(StrToDWORD  (&pInfoVarSysteme->numero_variable,		&szTemp[OFFSET_NUMERO_VARIABLE]));
		VerifWarning(StrToDWORD  (&pInfoVarSysteme->taille_tableau,			&szTemp[OFFSET_TAILLE_TABLEAU]));

		// Recherche de la fin du nom de la variable = ','
		dwOffsetSeparateurValInit = StrSearchChar (&szTemp[OFFSET_NOM_VARIABLE], ',');
		VerifWarning (dwOffsetSeparateurValInit !=STR_NOT_FOUND);
		szTemp[OFFSET_NOM_VARIABLE+dwOffsetSeparateurValInit] = '\0';

		// Recopie du nom et de la valeur intiale
		StrCopy(pInfoVarSysteme->nom_variable, &szTemp[OFFSET_NOM_VARIABLE]);
		StrCopy(pInfoVarSysteme->valeur_initiale, &szTemp[OFFSET_NOM_VARIABLE+dwOffsetSeparateurValInit+1]);
		}

  return bRes;
  }

// --------------------------------------------------------------------------
// Donner le nombre de variables systemes
// --------------------------------------------------------------------------
DWORD dwNbVariablesSysteme
	(void)	// Renvoie (DWORD) -1 si erreur, nombre de variables systemes si non
  {
  return dwNbVariablesSys;
  }

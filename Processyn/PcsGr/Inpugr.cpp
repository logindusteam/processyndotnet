/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inpugr.c                                                 |
 |   Auteur  : AC					        	|
 |   Date    : 26/10/92					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "Appli.h"
#include "PathMan.h"
#include "DocMan.h"
#include "Verif.h"
VerifInit;

// ATTENTION mettre action quitte si pas bon ou entrer en interactif apres
// message err
// + ecrire_str ('READING INSTRUCTIONS FILE .....')
// gerer le dispatch pendant l'execution des commandes

#include "pcsgr.h"
#include "G_Objets.h"
#include "actiongr.h"
#include "lng_res.h"
#include "tipe.h"
#include "mem.h"
#include "Cmdpcs.h"

#include "inpugr.h"

#define APPLICATION_DEFAUT "Applic" //$$ DOUBLON + � mettre ailleurs

// commande
typedef struct
  {
  int mode_lecture;
  char chaine[MAX_PATH];
  } PARAM_LECTURE, *PPARAM_LECTURE;

PARAM_LECTURE param_lecture; // $$ variable globale : � inclure dans un objet


// ----------------------------------------------------------------------
//
void *creer_lecture (void)
  {
  return (&param_lecture);
  }

// -------------------------------------------------------------------
//
void init_lecture (void *ptr_lecture_editeur, int mode_lec, char *chaine)
  {
  PPARAM_LECTURE ptr_lecture = (PPARAM_LECTURE)ptr_lecture_editeur;   // retype

  ptr_lecture->mode_lecture = mode_lec;
  StrSetNull (ptr_lecture->chaine);
  StrCopy (ptr_lecture->chaine, chaine);
  }

// -------------------------------------------------------------------
// analyse du bloc de commandes et ajout des actions associees
// -------------------------------------------------------------------
static void InterpreteBlocCmd (BOOL bCdeValide)
  {
  t_param_action param_action;
  DWORD           wNbEnr;
  DWORD           wIndex;
  PCOMMANDE_PCS	pCommandes;

  if (bCdeValide)
    {
    if (existe_repere (BLOC_COMMANDES))
      {
      wNbEnr = nb_enregistrements (szVERIFSource, __LINE__, BLOC_COMMANDES);
      }
    else
      {
      wNbEnr = 0;
      }
    if (wNbEnr)
      {
      for (wIndex = 1; wIndex <= wNbEnr; wIndex++)
        {
        pCommandes = (PCOMMANDE_PCS)pointe_enr (szVERIFSource, __LINE__, BLOC_COMMANDES, wIndex);
        StrCopy (param_action.texte, pCommandes->pszChaine);
        switch (pCommandes->carCmd)
          {
          case  '\0': // chaine sans commande
            ajouter_action (action_preambule, &param_action);
            if (StrIsNull (param_action.texte))
              {
              if (!LitFichierCmdPcs (param_action.texte))
                {
                StrCopy (param_action.texte, APPLICATION_DEFAUT);
                }
              }
            // $$ pszNomAjouteExt (param_action.texte, MAX_PATH, EXTENSION_SOURCE);
            param_action.on = TRUE;
            ajouter_action (action_charge_application, &param_action);
            break;

          case  'A':
            param_action.on = (BOOL)(!DEBUG_MACRO);
            ajouter_action (action_lire_macro, &param_action);
            break;

          case  'B':
            param_action.on = (BOOL)(DEBUG_MACRO);
            ajouter_action (action_lire_macro, &param_action);
            break;

          case  'C':  //
            if (StrIsNull (param_action.texte))
              {
              if (!LitFichierCmdPcs (param_action.texte))
                {
                StrCopy (param_action.texte, APPLICATION_DEFAUT);
                }
              }
            param_action.on = FALSE;
            ajouter_action (action_charge_application_sat, &param_action);
            break;

          default:
            ajouter_action (action_quitte, &param_action);
            break;
          }  // ------------ switch (char_cmd)
        }
      enleve_bloc (BLOC_COMMANDES);
      }
    else
      { // rien ds la cde ni ds cmdpcs
			::MessageBox(Appli.hwnd, "Pas d'application � charger : lancer PcsGr � partir de PcsConf.", NULL, MB_OK);
      ajouter_action (action_quitte, &param_action);
      }
    }
  else
    { // cde non valide
    ajouter_action (action_quitte, &param_action);
    }
  }

// ------------------------------------------------------------------------
// lecture d'une commande : demarrage, interactif ou macro
// ------------------------------------------------------------------------
void lire_commandes (void *ptr_lecture_editeur)
  {
  PPARAM_LECTURE ptr_lecture = (PPARAM_LECTURE)ptr_lecture_editeur;
  t_param_action   param_action;
  MSG             msg;
  BOOL          bCdeValide;

  switch (ptr_lecture->mode_lecture)
    {
    case MODE_DEMARRAGE:
      bCdeValide = FabriqueBlocCmd (ptr_lecture->chaine);
      InterpreteBlocCmd (bCdeValide);
      break;

    case MODE_INTERACTIF :
      if (GetMessage (&msg, NULL, 0, 0))
        {
				TranslateMessage(&msg); //$$ v�rifier
        DispatchMessage (&msg);
        }
      else
        {
        param_action.errorlevel_gr = 0;
        ajouter_action (action_quitte, &param_action);
        }
      break;

    case MODE_MACRO:
      //lit_fichier_macro (ptr_lecture->chaine);
      break;

    default:
      break;
    } // switch mode lecture
  }

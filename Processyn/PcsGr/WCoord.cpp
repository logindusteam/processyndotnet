#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "UStr.h"
#include "UEnv.h"

#include "WCoord.h"


#define NB_CAR_MAX_FEN_COORD 80
static char szClasseCoordonneesGr [] = "ClasseCoordGr";

// -----------------------------------------------------------------------
// Fen�tre d'affichage des coordonn�es
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocCoordonneesGr (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT   mres = 0;             // Valeur de retour
  BOOL      bTraite = TRUE;       // Indique commande trait�e
  PSTR		pszTexte;

  switch (msg)
    {
    case WM_CREATE:
			// cr�ation Env Ok ?
			pszTexte = (PSTR)pCreeEnv (hwnd, NB_CAR_MAX_FEN_COORD+1);
			if (pszTexte)
				{
				// oui => Initialise hdc et Texte
				HDC hdc = ::GetDC(hwnd);

				SetBkMode (hdc, TRANSPARENT);
				SelectObject (hdc, GetStockObject (DEFAULT_GUI_FONT));
				::ReleaseDC (hwnd, hdc);

				StrSetNull (pszTexte);
				}
			else
				// non => erreur de cr�ation de la fen�tre
				mres = -1;
			break;

		case WM_DESTROY:
			bLibereEnv (hwnd);
			break;

		case WM_SETTEXT:
			{
			LPCTSTR pSource = (LPCTSTR)mp2;

			if (pSource && (StrLength (pSource) <= NB_CAR_MAX_FEN_COORD))
				{
				pszTexte = (PSTR)pGetEnv (hwnd);
				StrCopy (pszTexte, pSource);
				mres = TRUE;
				::InvalidateRect (hwnd, NULL, TRUE);
				}
			}
			break;

    case WM_PAINT:
			{
			// Affiche le texte courant avec les attributs courants
			PAINTSTRUCT ps;
			RECT			rect;

      ::BeginPaint (hwnd, &ps);
			pszTexte = (PSTR)pGetEnv (hwnd);
			::GetClientRect (hwnd, &rect);
			DrawText (ps.hdc, pszTexte, -1, &rect, DT_CENTER|DT_VCENTER);
			::EndPaint (hwnd, &ps);
			}
      break;

    default:
         bTraite = FALSE;
         break;
    } //switch (msg)

  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  }


// -----------------------------------------------------------------------
// Cr�ation de la fenetre d'affichage des coordonn�es
// -----------------------------------------------------------------------
HWND hwndCreeFenCoord (HWND hwndParent, int x, int y, BOOL bVisible)
  {
	HWND	hwndRet = NULL;
	DWORD	dwStyle = WS_POPUP | WS_CAPTION|WS_BORDER; // WS_VISIBLE | WS_CLIPCHILDREN |WS_CLIPSIBLINGS
	static BOOL bClasseEnregistree = FALSE;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= CS_OWNDC; //CS_HREDRAW | CS_VREDRAW; 
		wc.lpfnWndProc		= wndprocCoordonneesGr; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0;
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= (HBRUSH)(COLOR_MENU+1); //pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseCoordonneesGr; 
		bClasseEnregistree = RegisterClass (&wc);
		}

	if (bClasseEnregistree)
		{
		if (bVisible)
			dwStyle |= WS_VISIBLE;

		hwndRet = CreateWindowEx (
			WS_EX_PALETTEWINDOW,//WS_EX_CLIENTEDGE|
			szClasseCoordonneesGr,	// class name
			"x,y",	// window name
			dwStyle, 
			x, y,
			60, 16 + GetSystemMetrics(SM_CYSMCAPTION) + (2* GetSystemMetrics(SM_CYEDGE)), //$$
			hwndParent,	// handle parent window
			(HMENU)0,	// handle to menu or child-window identifier
			Appli.hinst,	// handle to application instance
			NULL);	// pointer to window-creation data
		}

	return hwndRet;
  }

// ---------------------------------------------------------------------
// Affichage des nouvelles coordon�es
void MAJCoord (HWND hwndCoord, int x, int y)
  {
  char szTexte[NB_CAR_MAX_FEN_COORD];

  StrPrintFormat (szTexte, "%4i,%4i",x,y);
  ::SetWindowText (hwndCoord, szTexte);
  }

// -----------------------------------------------------------------------
// WEditSyn.c
// Gestion des fen�tres d'�dition de synoptiques de PCSGR
// Version WIN32 10/6/97
// -----------------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "MemMan.h"
#include "UStr.h"
#include "UEnv.h"
#include "MenuMan.h"
#include "mem.h"

#include "idmenugr.h"
#include "G_Objets.h"
#include "actiongr.h"
#include "LireLng.h"
#include "IdLngLng.h"

#include "WInLine.h"
#include "IdGrLng.h"
#include "pcsdlg.h"

#include "appligr.h"
#include "lng_res.h"
#include "tipe.h"
#include "Space.h"
#include "PcsSpace.h"
#include "g_sys.h"
#include "bdgr.h"
#include "bdanmgr.h"
#include "BdPageGr.h"
#include "BdElemGr.h"
#include "ctxtgr.h"
#include "WParPop.h"
#include "Verif.h"

#include "WEditSyn.h"
VerifInit;

#define ID_FEN_SAISIE_TEXTE (0x0001)


#define NBR_CAR_MAX_GR 80

#define OFFSET_ID_SYSMENU      100
#define CMD_SYSMENU_GROSSIR (OFFSET_ID_SYSMENU+1)
#define CMD_SYSMENU_MAIGRIR (OFFSET_ID_SYSMENU+2)
#define CMD_SYSMENU_FACTEUR (OFFSET_ID_SYSMENU+3)

#define FACTEUR_SCROLL_X 50
#define FACTEUR_SCROLL_Y 50

typedef struct
  {
  DWORD id;
  DWORD mode_affichage;
  } ENV_EDITSYN;
typedef ENV_EDITSYN * PENV_EDITSYN;

// --------------------------------------------------------------------
typedef struct
  {
  HWND  hwndGr;
  HGSYS	hgsys;
  HBDGR	hBdGr;
  HWND	hdl_inline;
  BOOL	fen_libre;
  DWORD facteur_zoom;  // utile en mode test uniquement
  FLOAT coeff_zoom;    // utile en mode test uniquement
  int cx_client_ref;
  int cy_client_ref;
  } FENETRE_SYN, *PFENETRE_SYN;

// ---------------------------
// bloc utilis� exclusivement ici
#define b_fenetre_gr 630

// Variables locales
static PCSTR szClasseEditSyn = "ClasseEditSyn";
static HSPACE hSpcFen = NULL;
static HCURSOR hptrcroix = NULL;
static HCURSOR hptrfleche = NULL;
static HCURSOR hptrcible = NULL;
static HWND			hwndParentPage = NULL;

// ------------------------------------------------------
//      Espace conversion pour coord fen en generalisees
// ------------------------------------------------------
void creer_espace_ref_fen_gr (void)
  {
  hSpcFen = hSpcCree (Appli.sizEcran.cx, Appli.sizEcran.cy, c_G_NB_X, c_G_NB_Y);
  }

// ------------------------------------------------------
void supprimer_espace_ref_fen_gr (void)
  {
  SpcFerme (&hSpcFen);
  }

// -----------------------------------------------------------------------
static void bornage_frame (HWND hwnd, int *cx, int *cy)
  {
	/* $$ porter
  int nb_hwnd;
  LONG p_reference_nb_x = sizEcran.cx;
  LONG p_reference_nb_y = sizEcran.cy;
  LONG offset;
  BOOL size_border;
  HWND tab_hwnd [FID_CLIENT - FID_SYSMENU + 1];

  if (((*cx) > sizEcran.cx) || ((*cy) > sizEcran.cy))
    {
    size_border = (BOOL)((GetWindowLong (hwnd, GWL_STYLE) & FS_SIZEBORDER) == FS_SIZEBORDER);
    nb_hwnd =  WinMultWindowFromIDs (hwnd, tab_hwnd, FID_SYSMENU, FID_CLIENT);
    if (nb_hwnd != 0)
      {
      if (size_border)
        {
        offset = 2 * (LONG)GetSystemMetrics (SM_CYBORDER);
        }
      else
        {
        offset = 0;
        }
      if ((tab_hwnd [FID_SYSMENU - FID_SYSMENU] != NULL) ||
          (tab_hwnd [FID_MINMAX - FID_SYSMENU] != NULL) ||
          (tab_hwnd [FID_TITLEBAR - FID_SYSMENU] != NULL)) //$$ ??
        {
        offset = offset + (LONG)GetSystemMetrics (SM_CYCAPTION);
        }
      if (tab_hwnd [FID_HORZSCROLL - FID_SYSMENU] != NULL)
        {
        offset = offset + (LONG)GetSystemMetrics (SM_CYHSCROLL);
        }
      p_reference_nb_y = p_reference_nb_y + offset;

      if (size_border)
        {
        offset = 2 * (LONG)GetSystemMetrics (SM_CXSIZEBORDER);
        }
      else
        {
        offset = 0;
        }
      if (tab_hwnd [FID_VERTSCROLL - FID_SYSMENU] != NULL)
        {
        offset = offset + (LONG)GetSystemMetrics (SM_CXVSCROLL);
        }
      p_reference_nb_x = p_reference_nb_x + offset;

      if ((*cx) > p_reference_nb_x)
        {
        (*cx) = p_reference_nb_x;
        Beep (100,100);
        }
      if ((*cy) > p_reference_nb_y)
        {
        (*cy) = p_reference_nb_y;
        Beep (100,100);
        }
      }
    }
		*/
  }
// -----------------------------------------------------------------------
static void calcule_taille_fen_relative (HWND hwnd, LONG *lcx, LONG *lcy)
  {
  PENV_EDITSYN pEnvEditSyn;
  PFENETRE_SYN pFenetreSyn;
  RECT rectFen;

  pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
  if ((CtxtgrGetModeEdition() == EN_TEST) && (pEnvEditSyn->mode_affichage == MODE_ZOOM))
    {
    pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
    *lcx = (LONG)(pFenetreSyn->cx_client_ref / pFenetreSyn->coeff_zoom);
    *lcy = (LONG)(pFenetreSyn->cy_client_ref / pFenetreSyn->coeff_zoom);
    }
  else
    {
    ::GetWindowRect (hwnd, &rectFen);
    *lcx = (LONG)(rectFen.right- rectFen.left);
    *lcy = (LONG)(rectFen.bottom - rectFen.top);
    Spc_CvtSizeEnVirtuel (hSpcFen, lcx,  lcy);
    }
  }
// -----------------------------------------------------------------------
static void maj_thumbsize_scroll (HWND hwnd, void *hGsys)
  {
  LONG lcx;
  LONG lcy;
// $$ � stocker dans l'environnement	SCROLLINFO si;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);

	// mise � jour de la scroll bar horizontale
/*	if (GetScrollInfo (hwnd, SB_HORZ, &si))
    //$$::SendMessage (hwndScroll, SBM_SETTHUMBSIZE, MAKEWPARAM ((SHORT) lcx, c_G_MAX_X - c_G_MIN_X), 0);
    ;
  hwndScroll = ::GetDlgItem (hwndFrame, FID_VERTSCROLL);
  if (hwndScroll != NULL)
    {
    //$$::SendMessage (hwndScroll, SBM_SETTHUMBSIZE, MAKEWPARAM ((SHORT) lcy, c_G_MAX_Y - c_G_MIN_Y), 0);
    }
		*/
  }

// -----------------------------------------------------------------------
static void maj_espace_scroll_bar_h (HWND hwnd, HGSYS hGsys)
  {
  LONG lcx;
  LONG lcy;
  LONG xOrigine;
  LONG yOrigine;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  g_get_origin (hGsys, &xOrigine, &yOrigine);
	/* $$ � porter
  HWND hwndScroll;
  hwndScroll = ::GetDlgItem (hwndFrame, FID_HORZSCROLL);
  if (hwndScroll != NULL)
    {
    ::SendMessage (hwndScroll, SBM_SETSCROLLBAR, xOrigine, MAKELPARAM (c_G_MIN_X, c_G_MAX_X - lcx));
    }
		*/
  }

// -----------------------------------------------------------------------
static void maj_espace_scroll_bar_v (HWND hwnd, HGSYS hGsys)
  {
  HWND hwndFrame;
  LONG lcx;
  LONG lcy;
  LONG xOrigine;
  LONG yOrigine;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  g_get_origin (hGsys, &xOrigine, &yOrigine);
  hwndFrame = ::GetParent (hwnd);
	/* $$ � porter
  HWND hwndScroll;
  hwndScroll = ::GetDlgItem (hwndFrame, FID_VERTSCROLL);
  if (hwndScroll != NULL)
    {
    ::SendMessage (hwndScroll, SBM_SETSCROLLBAR, (c_G_MAX_Y-lcy) - yOrigine, MAKELPARAM (c_G_MIN_Y, (c_G_MAX_Y-lcy)));
    }
		*/
  }

// -----------------------------------------------------------------------
static void bornage (LONG *val, LONG petit, LONG grand)
  {
  if ((*val) < petit)
    *val = petit;
  else
    {
    if ((*val) > grand)
      (*val) = grand;
    }
  }

// -----------------------------------------------------------------------
static void ajuste_origine_fen_x (HWND hwnd, HGSYS hGsys, LONG *dx0)
  {
  LONG   sMin;
  LONG   sMax;
  HWND  hwndFrame;
  LONG   lcx;
  LONG   lcy;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  sMin = c_G_MIN_X;
  sMax = c_G_MAX_X - lcx;
  bornage (dx0, sMin, sMax);
  hwndFrame = ::GetParent (hwnd);
	/* $$ porter
  HWND  hwndScroll;
  hwndScroll = ::GetDlgItem (hwndFrame, FID_HORZSCROLL);
  if (hwndScroll != NULL)
    {
    ::SendMessage (hwndScroll, SBM_SETPOS, *dx0, 0);
    }
  g_set_x_origine (hGsys, *dx0);
	*/
  }

// -----------------------------------------------------------------------
static void ajuste_origine_fen_y (HWND hwnd, void *hGsys, LONG *dy0)
  {
  LONG   sMin;
  LONG   sMax;
  HWND  hwndFrame;
  LONG   lcx;
  LONG   lcy;

  calcule_taille_fen_relative (hwnd, &lcx, &lcy);
  sMin = c_G_MIN_Y;
  sMax = c_G_MAX_Y - lcy;
  bornage (dy0, sMin, sMax);
  hwndFrame = ::GetParent (hwnd);
	/* $$ � porter
  HWND  hwndScroll;
  LRESULT mresQuery;
  hwndScroll = ::GetDlgItem (hwndFrame, FID_VERTSCROLL);
  if (hwndScroll != NULL)
    {
    mresQuery = ::SendMessage (hwndScroll, SBM_QUERYRANGE, 0, 0);
    sMax = (LONG)HIWORD (mresQuery);
    ::SendMessage (hwndScroll, SBM_SETPOS, sMax-(*dy0), 0);
    }
  g_set_y_origine (hGsys, *dy0);
	*/
  }

// -----------------------------------------------------------------------
static void param_fenetre (PFENETRE_SYN pFenetreSyn, LONG *CentrexDev, LONG *CentreyDev,
													 LONG *NewTaillexDev, LONG *NewTailleyDev)
  {
  RECT rcl;
  RECT rectFen;

  ::GetWindowRect (pFenetreSyn->hwndGr, &rectFen);
  ::GetClientRect (pFenetreSyn->hwndGr, &rcl);

  (*CentrexDev) = ((LONG)rcl.right + (LONG)rcl.left) / 2;
  (*CentreyDev) = ((LONG)rcl.top + (LONG)rcl.bottom) / 2;
  (*NewTaillexDev) = (LONG) ((FLOAT)(rectFen.right - rectFen.left) * pFenetreSyn->coeff_zoom);
  (*NewTailleyDev) = (LONG) ((FLOAT)(rectFen.bottom - rectFen.top) * pFenetreSyn->coeff_zoom);
  if ((*NewTaillexDev) < 1)
    {
    (*NewTaillexDev) = 1;
    }
  if ((*NewTailleyDev) < 1)
    {
    (*NewTailleyDev) = 1;
    }
  }

// -----------------------------------------------------------------------
static BOOL origine_x_modifie (HWND hwnd, PENV_EDITSYN pEnvEditSyn, LPARAM mp2, LONG *dx0)
  {
  BOOL bScroller = FALSE;
  PFENETRE_SYN pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
  RECT rcl;
  LONG dx;
  LONG pas_scroll;
  RECT rectFen;

  ::GetWindowRect (pFenetreSyn->hwndGr, &rectFen);
  pas_scroll = g_convert_dx_dev_to_page (pFenetreSyn->hgsys, rectFen.left - rectFen.right) / FACTEUR_SCROLL_X;
  if (pas_scroll <= 0)
    {
    pas_scroll = g_convert_dx_dev_to_page (pFenetreSyn->hgsys, rectFen.left - rectFen.right) / 2; //1/2 fenetre
    }
  switch (HIWORD (mp2))
    {
    case SB_LINELEFT:
         //Action sur Fl�che Gauche
         (*dx0) = g_add_dx_page (pFenetreSyn->hgsys, (*dx0), -pas_scroll);
         bScroller = TRUE;
         break;
    case SB_LINERIGHT:
         //Action sur Fl�che Droite
         (*dx0) = g_add_dx_page (pFenetreSyn->hgsys, (*dx0), pas_scroll);
         bScroller = TRUE;
         break;
    case SB_PAGELEFT:
         //Action sur Page Gauche
         ::GetWindowRect (hwnd, &rcl);
         dx = g_convert_dx_dev_to_page (pFenetreSyn->hgsys, -(rcl.right - rcl.left));
         (*dx0) = g_add_dx_page (pFenetreSyn->hgsys, (*dx0), dx);
         bScroller = TRUE;
         break;
    case SB_PAGERIGHT:
         //Action sur Page Droite
         ::GetWindowRect (hwnd, &rcl);
         dx = g_convert_dx_dev_to_page (pFenetreSyn->hgsys, rcl.right - rcl.left);
         (*dx0) = g_add_dx_page (pFenetreSyn->hgsys, (*dx0), dx);
         bScroller = TRUE;
         break;
    case SB_THUMBPOSITION:
         (*dx0) = LOWORD (mp2);
         bScroller = TRUE;
         break;
    case SB_THUMBTRACK:
         //Ne rien faire (scroll dynamique... (dessin trop lent))
         break;
    case SB_ENDSCROLL:
         //Ne rien faire (la souris quitte le scroll bar)
         break;
    default:
         break;
    }
  return (bScroller);
  }

// ----------------------------------------------------------------------- 
static BOOL origine_y_modifie (HWND hwnd, PENV_EDITSYN pEnvEditSyn, LPARAM mp2, LONG *dy0)
  {
  BOOL bScroller = FALSE;
  PFENETRE_SYN pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
  RECT rcl;
  LONG dy;
  LONG pas_scroll;
  RECT	rectFen;

  ::GetWindowRect (pFenetreSyn->hwndGr, &rectFen);
  pas_scroll = g_convert_dy_dev_to_page (pFenetreSyn->hgsys, rectFen.bottom - rectFen.top) / FACTEUR_SCROLL_Y;
  if (pas_scroll <= 0)
    {
    pas_scroll = g_convert_dy_dev_to_page (pFenetreSyn->hgsys, rectFen.bottom - rectFen.top) / 2; //1/2 fenetre
    }
  switch (HIWORD (mp2))
    {
    case SB_LINEUP:
         //Action sur Fl�che Haut
         (*dy0) = g_add_dy_page (pFenetreSyn->hgsys, (*dy0), pas_scroll);
         bScroller = TRUE;
         break;
    case SB_LINEDOWN:
         //Action sur Fl�che Bas
         (*dy0) = g_add_dy_page (pFenetreSyn->hgsys, (*dy0), -pas_scroll);
         bScroller = TRUE;
         break;
    case SB_PAGEUP:
         //Action sur Page Haut
         ::GetWindowRect (hwnd, &rcl);
         dy = g_convert_dy_dev_to_page (pFenetreSyn->hgsys, (rcl.top - rcl.bottom));
         (*dy0) = g_add_dy_page (pFenetreSyn->hgsys, (*dy0), dy);
         bScroller = TRUE;
         break;
    case SB_PAGEDOWN:
         //Action sur Page Bas
         ::GetWindowRect (hwnd, &rcl);
         dy = g_convert_dy_dev_to_page (pFenetreSyn->hgsys, -(rcl.top - rcl.bottom));
         (*dy0) = g_add_dy_page (pFenetreSyn->hgsys, (*dy0), dy);
         bScroller = TRUE;
         break;
    case SB_THUMBPOSITION:
					/*$$ porter
					{
				  HWND  hwndScroll;
					LONG   sMax;
					LRESULT mresQuery;
					HWND  hwndFrame;

         hwndFrame = ::GetParent (hwnd);
         hwndScroll = ::GetDlgItem (hwndFrame, FID_VERTSCROLL);
         mresQuery = ::SendMessage (hwndScroll, SBM_QUERYRANGE, 0, 0);
         sMax = (LONG)HIWORD (mresQuery);
         (*dy0) = sMax - (LONG)LOWORD (mp2);
         bScroller = TRUE;
				 }
				 */
         break;
    case SB_THUMBTRACK:
         //Ne rien faire (scroll dynamique... (dessin trop lent))
         break;
    case SB_ENDSCROLL:
         //Ne rien faire (la souris quitte le scroll bar)
         break;
    default:
         break;
    }
  return (bScroller);
  }

// -----------------------------------------------------------------------
static void change_origine_fen_bd (LONG x, LONG y)
  {
  t_param_action param_action;

  if (CtxtgrGetModeEdition() != EN_TEST)
    {
    param_action.x1 = x;
    param_action.y1 = y;
    ajouter_action (action_change_origine, &param_action);
    }
  }

// -----------------------------------------------------------------------
static void ajuste_espace_fen (HWND hwnd, DWORD id_fen)
  {

  if (existe_repere (b_fenetre_gr) && (nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_gr)!= 0))
    {
		PFENETRE_SYN pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, id_fen);
		LONG        lx1;
		LONG        ly1;
		PENV_EDITSYN pEnvEditSyn;
		int new_cx_client;
		int new_cy_client;
		RECT	rectFen;

    ::GetWindowRect (hwnd, &rectFen);
    new_cx_client = rectFen.right - rectFen. left;
    new_cy_client = rectFen.bottom - rectFen.top;
    g_set_page_units (pFenetreSyn->hgsys, c_G_NB_X, c_G_NB_Y, Appli.sizEcran.cx, Appli.sizEcran.cy);

    if (CtxtgrGetModeEdition() != EN_TEST)
      {
      lx1 = (LONG)new_cx_client;
      ly1 = (LONG)new_cy_client;
      g_convert_delta_dev_to_page (pFenetreSyn->hgsys, &lx1,  &ly1);
      pFenetreSyn->cx_client_ref = lx1;
      pFenetreSyn->cy_client_ref = ly1;
      }
    else
      {
      pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
      if (pEnvEditSyn->mode_affichage == MODE_ZOOM)
        {
        lx1 = (LONG) ((FLOAT)new_cx_client * pFenetreSyn->coeff_zoom);
        ly1 = (LONG) ((FLOAT)new_cy_client * pFenetreSyn->coeff_zoom);
        g_set_page_units (pFenetreSyn->hgsys, pFenetreSyn->cx_client_ref, pFenetreSyn->cy_client_ref, lx1, ly1);
        }
      }
    }
  }

// -----------------------------------------------------------------------
static void change_pos_fen_bd (HWND hwnd, DWORD id_fen)
  {
  LONG        lx1;
  LONG        ly1;
  t_param_action param_action;
  RECT	rectFen;

  if (existe_repere (b_fenetre_gr) && (nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_gr)!= 0))
    {
    if (CtxtgrGetModeEdition() != EN_TEST)
      {
      ::GetWindowRect (hwnd ,&rectFen);
      param_action.id_fen = id_fen;
      lx1 = rectFen.left;
      ly1 = rectFen.top;
      Spc_CvtPtEnVirtuel (hSpcFen, &lx1,  &ly1);
      param_action.x1 =  lx1;
      param_action.y1 =  ly1;
      lx1 = rectFen.right - rectFen.left;
      ly1 = rectFen.bottom - rectFen.top;
      Spc_CvtSizeEnVirtuel (hSpcFen, &lx1,  &ly1);
      param_action.x2 =  lx1;
      param_action.y2 =  ly1;
      ajouter_action (action_change_pos_fenetre, &param_action);
      }
    }
  }

// -----------------------------------------------------------------------
static BOOL SourisDansFenetre (HWND hwnd, int *x, int *y)
  {
  BOOL   bOk = FALSE;
	/* $$ porter
   POINT ptl;
  RECT  rcl;
 if (WinQueryPointerPos (HWND_DESKTOP, &ptl))
    {
    if (WinMapWindowPoints (HWND_DESKTOP, hwnd, &ptl, 1))
      {
      (*x) =  ptl.x;
      (*y) =  ptl.y;
      if (::GetWindowRect (hwnd, &rcl))
        {
        if (PtInRect (&rcl, ptl))
          {
          bOk = TRUE;
          }
        }
      }
    }
		*/
  return (bOk);
  }

// -----------------------------------------------------------------------
// Demande un d�placement relatif de la s�lection par pas de grille
// ou par pixel si Ctrl est enfonc�
static void DeplaceSelectionParClavier (HWND hwnd, LONG lDx, LONG lDy)
  {
	PENV_EDITSYN pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
	PFENETRE_SYN pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
	g_convert_point_dev_to_page (pFenetreSyn->hgsys, &lDx,  &lDy);
	t_param_action param_action;
	param_action.id_fen = pEnvEditSyn->id;
	param_action.x1 = lDx;
	param_action.y1 = lDy;
	// $$ ajouter le d�placement par pas de grille
	BOOL bControle = ((0x8000 & GetKeyState (VK_CONTROL)) != 0);
	if (bControle)
		param_action.param_fonction	= 0;
	else
		param_action.param_fonction	= 1;
	ajouter_action (action_deplace_selection, &param_action);
  }

// -----------------------------------------------------------------------
// Traitement du message WM_CHAR (caract�res normaux)
// renvoie TRUE si la touche a �t� trait�e
// -----------------------------------------------------------------------
static BOOL OnChar (HWND hwnd, WPARAM mp1, LRESULT * pmres)
  {
  BOOL						bToucheTraitee = TRUE;
  t_param_action  param_action;
  PENV_EDITSYN		pEnvEditSyn;
  int             x;
  int             y;
	char			c = LOBYTE (mp1);

	if (CtxtgrGetModeEdition() != EN_TEST)
		{
		switch (c)
			{
			case VK_RETURN: // VK_NEWLINE
        if (SourisDansFenetre (hwnd, &x, &y))
          {
					BOOL		bControle = ((0x8000 & GetKeyState (VK_CONTROL)) != 0);
					BOOL		bShift = ((0x8000 & GetKeyState (VK_SHIFT))!= 0);
          if (bShift)
            {
            ::SendMessage (hwnd, WM_LBUTTONUP, 0, MAKELPARAM (x, y));
            ::SendMessage (hwnd, WM_LBUTTONDBLCLK, 0, MAKELPARAM (x, y));
            ::SendMessage (hwnd, WM_LBUTTONUP, 0, MAKELPARAM (x, y));
            bToucheTraitee = TRUE;
            }
          if (bControle)
            {
            ::SendMessage (hwnd, WM_RBUTTONUP, 0, MAKELPARAM (x, y));
            ::SendMessage (hwnd, WM_RBUTTONDBLCLK, 0, MAKELPARAM (x, y));
            ::SendMessage (hwnd, WM_RBUTTONUP, 0, MAKELPARAM (x, y));
            bToucheTraitee = TRUE;
            }
          }
				break;

			case VK_ESCAPE:
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				param_action.id_fen = pEnvEditSyn->id;
				ajouter_action (action_abandon, &param_action);
				break;

			default:
				bToucheTraitee = FALSE;
				break;
			}
		}
	else
		bToucheTraitee = FALSE;

  *pmres = bToucheTraitee;
  return (bToucheTraitee);
  } // OnChar

// ----------------------------------------------------------------------- 
// Traitement du message WM_KEYDOWN (pour touches sp�ciales)
// renvoie TRUE si la touche a �t� trait�e
// -----------------------------------------------------------------------
static BOOL OnKeyDown (HWND hwnd, WPARAM mp1, LPARAM mp2, LRESULT * pmres)
  {
  BOOL						bToucheTraitee = TRUE;
	char						c = LOBYTE (mp1);
  t_param_action  param_action;
  PENV_EDITSYN		pEnvEditSyn;
  
  if (CtxtgrGetModeEdition() == EN_TEST)
    {
		switch (c)
			{
			case VK_F1:
        ajouter_action (action_anim_couleur2, &param_action);
				break;

			case VK_F2:
        ajouter_action (action_anim_couleur4, &param_action);
				break;

			default:
				bToucheTraitee = FALSE;
				break;
			} // switch (c)
		} // fin mode TEST
	else
		{
		switch (c)
			{
			/*
			case VK_SHIFT:
				// premier appui et souris sur la fen�tre ?
        if ((mp2 & 40000000) && SourisDansFenetre (hwnd, &x, &y))
          ::SendMessage (hwnd, WM_LBUTTONDOWN, 0, MAKELPARAM (x, y));
				else
          bToucheTraitee = FALSE;
				break;

			case VK_CONTROL:
				// premier appui et souris sur la fen�tre ?
				if ((mp2 & 40000000) && SourisDansFenetre (hwnd, &x, &y))
					::SendMessage (hwnd, WM_RBUTTONDOWN, 0, MAKELPARAM (x, y));
				else
					bToucheTraitee = FALSE;
  			break;
*/
			case VK_LEFT: // fl�che vers la gauche
        DeplaceSelectionParClavier (hwnd, -1, 0);
				break;

			case VK_RIGHT: // fl�che vers la droite
        DeplaceSelectionParClavier (hwnd, 1, 0);
				break;

			case VK_UP: // fl�che vers le haut
        DeplaceSelectionParClavier (hwnd, 0, -1);
				break;

			case VK_DOWN: // fl�che vers le bas
        DeplaceSelectionParClavier (hwnd, 0, 1);
				break;

			case VK_DELETE:
				ajouter_action (action_suppression, &param_action);
				break;

			case VK_F1:
        pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
        param_action.id_fen = pEnvEditSyn->id;
        ajouter_action (action_click2, &param_action);
				break;

			case VK_F2:
        param_action.page_gr = 0;
        ajouter_action (action_duplicate, &param_action);
				break;

			case VK_F3:
        ajouter_action (action_group, &param_action);
				break;

			case VK_F4:
        ajouter_action (action_degroup, &param_action);
				break;

			case VK_F5:
				ajouter_action (action_rotation, &param_action);
				break;

			case VK_F6:
        ajouter_action (action_change_remplissage, &param_action);
				break;

			case VK_F7:
        ajouter_action (action_symetrie_v, &param_action);
				break;

			case VK_F8:
        ajouter_action (action_symetrie_h, &param_action);
				break;

			default:
				bToucheTraitee = FALSE;
				break;
			} // switch (c)
		}

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = bToucheTraitee;
  return (bToucheTraitee);
  } // OnKeyDown


// ----------------------------------------------------------------------- 
// Traitement du message WM_KEYUP (pour touches sp�ciales)
// renvoie TRUE si la touche a �t� trait�e
// -----------------------------------------------------------------------
static BOOL OnKeyUp (HWND hwnd, WPARAM mp1, LRESULT * pmres)
  {
  BOOL    bToucheTraitee = TRUE;
	char		c = LOBYTE (mp1);
  //t_param_action  param_action;
  //ENV_EDITSYN     *pEnvEditSyn;
  int             x;
  int             y;

  if (CtxtgrGetModeEdition() != EN_TEST)
		{
		switch (c)
			{
			case VK_SHIFT:
        SourisDansFenetre (hwnd, &x, &y);
        ::SendMessage (hwnd, WM_LBUTTONUP, 0, MAKELPARAM (x, y));
				break;

			case VK_CONTROL:
        SourisDansFenetre (hwnd, &x, &y);
        ::SendMessage (hwnd, WM_RBUTTONUP, 0, MAKELPARAM (x, y));
  			break;

			default:
				bToucheTraitee = FALSE;
				break;
			} // switch (c)
		}

  // Valeurs retourn�es si le message a �t� trait�
  *pmres = bToucheTraitee;
  return (bToucheTraitee);
  } // OnKeyUp

//---------------------------------------------------------------------
// WM_SYSCOMMAND
static BOOL OnSysCommand (HWND hwnd, WPARAM mp1, LRESULT * pmres)
	{
	BOOL					bTraite = TRUE;
	PFENETRE_SYN pFenetreSyn;
	t_param_action param_action;
	PENV_EDITSYN pEnvEditSyn;
	LONG NewTaillexDev;
	LONG NewTailleyDev;
	LONG Originex;
	LONG Originey;
	LONG CentrexDev;
	LONG CentreyDev;
	LONG OldCentrex;
	LONG OldCentrey;
	LONG NewCentrex;
	LONG NewCentrey;
	LONG dx;
	LONG dy;
	PARAM_DLG_SAISIE_NUM retour_saisie_num;
	char titre_saisie_facteur[c_nb_car_message_inf];

	pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
	switch (mp1 & 0xFFF0)
		{
		case SC_CLOSE:
			ajouter_action (action_ferme_fenetre, &param_action);
			break;

		case CMD_SYSMENU_GROSSIR:
			if (CtxtgrGetModeEdition() == EN_TEST)
				{
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				if (pFenetreSyn->facteur_zoom < 100)
					{
					pFenetreSyn->coeff_zoom = pFenetreSyn->coeff_zoom / (FLOAT)pFenetreSyn->facteur_zoom * (FLOAT)100.0;
					}
				else
					{
					pFenetreSyn->coeff_zoom = pFenetreSyn->coeff_zoom * (FLOAT)pFenetreSyn->facteur_zoom / (FLOAT)100.0;
					}
				param_fenetre (pFenetreSyn, &CentrexDev, &CentreyDev, &NewTaillexDev, &NewTailleyDev);

				OldCentrex = g_convert_x_dev_to_page (pFenetreSyn->hgsys, CentrexDev);
				OldCentrey = g_convert_y_dev_to_page (pFenetreSyn->hgsys, CentreyDev);

				g_set_page_units (pFenetreSyn->hgsys, pFenetreSyn->cx_client_ref, pFenetreSyn->cy_client_ref, NewTaillexDev, NewTailleyDev);

				NewCentrex = g_convert_x_dev_to_page (pFenetreSyn->hgsys, CentrexDev);
				NewCentrey = g_convert_y_dev_to_page (pFenetreSyn->hgsys, CentreyDev);
				dx = OldCentrex - NewCentrex;
				dy = OldCentrey - NewCentrey;
				g_get_origin (pFenetreSyn->hgsys, &Originex, &Originey);
				g_add_deltas_page (pFenetreSyn->hgsys, &Originex, &Originey, dx, dy);
				ajuste_origine_fen_x (pFenetreSyn->hwndGr, pFenetreSyn->hgsys, &Originex);
				ajuste_origine_fen_y (pFenetreSyn->hwndGr, pFenetreSyn->hgsys, &Originey);

				::InvalidateRect (hwnd, NULL, TRUE);
				maj_espace_scroll_bar_h (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
				maj_espace_scroll_bar_v (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
				maj_thumbsize_scroll (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
				}
			else
				Beep (300,300);
			break;

		case CMD_SYSMENU_MAIGRIR:
			if (CtxtgrGetModeEdition() == EN_TEST)
				{
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				if (pFenetreSyn->facteur_zoom < 100)
					{
					pFenetreSyn->coeff_zoom = pFenetreSyn->coeff_zoom * (FLOAT)pFenetreSyn->facteur_zoom / (FLOAT)100.0;
					}
				else
					{
					pFenetreSyn->coeff_zoom = pFenetreSyn->coeff_zoom / (FLOAT)pFenetreSyn->facteur_zoom * (FLOAT)100.0;
					}
				param_fenetre (pFenetreSyn, &CentrexDev, &CentreyDev, &NewTaillexDev, &NewTailleyDev);

				OldCentrex = g_convert_x_dev_to_page (pFenetreSyn->hgsys, CentrexDev);
				OldCentrey = g_convert_y_dev_to_page (pFenetreSyn->hgsys, CentreyDev);

				g_set_page_units (pFenetreSyn->hgsys, pFenetreSyn->cx_client_ref, pFenetreSyn->cy_client_ref, NewTaillexDev, NewTailleyDev);

				NewCentrex = g_convert_x_dev_to_page (pFenetreSyn->hgsys, CentrexDev);
				NewCentrey = g_convert_y_dev_to_page (pFenetreSyn->hgsys, CentreyDev);
				dx = OldCentrex - NewCentrex;
				dy = OldCentrey - NewCentrey;
				g_get_origin (pFenetreSyn->hgsys, &Originex, &Originey);
				g_add_deltas_page (pFenetreSyn->hgsys, &Originex, &Originey, dx, dy);
				ajuste_origine_fen_x (pFenetreSyn->hwndGr, pFenetreSyn->hgsys, &Originex);
				ajuste_origine_fen_y (pFenetreSyn->hwndGr, pFenetreSyn->hgsys, &Originey);

				::InvalidateRect (hwnd, NULL, TRUE);
				maj_thumbsize_scroll (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
				maj_espace_scroll_bar_h (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
				maj_espace_scroll_bar_v (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
				}
			else
				Beep (300,300);
			break;

		case CMD_SYSMENU_FACTEUR:
			if (CtxtgrGetModeEdition() == EN_TEST)
				{
				titre_saisie_facteur [0] = '\0';
				retour_saisie_num.titre [0] = '\0';
				bLngLireInformation (c_inf_titre_facteur_zoom, titre_saisie_facteur);
				StrCopy (retour_saisie_num.titre, titre_saisie_facteur);
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				retour_saisie_num.saisie = (int)pFenetreSyn->facteur_zoom;
				if (dlg_saisie_num (hwnd, &retour_saisie_num))
					{
					if (retour_saisie_num.saisie > 0)
						{
						pFenetreSyn->facteur_zoom = (DWORD)retour_saisie_num.saisie;
						pFenetreSyn->coeff_zoom = (FLOAT)pFenetreSyn->facteur_zoom / (FLOAT)100.0;
						param_fenetre (pFenetreSyn, &CentrexDev, &CentreyDev, &NewTaillexDev, &NewTailleyDev);

						OldCentrex = g_convert_x_dev_to_page (pFenetreSyn->hgsys, CentrexDev);
						OldCentrey = g_convert_y_dev_to_page (pFenetreSyn->hgsys, CentreyDev);

						g_set_page_units (pFenetreSyn->hgsys, pFenetreSyn->cx_client_ref, pFenetreSyn->cy_client_ref, NewTaillexDev, NewTailleyDev);

						NewCentrex = g_convert_x_dev_to_page (pFenetreSyn->hgsys, CentrexDev);
						NewCentrey = g_convert_y_dev_to_page (pFenetreSyn->hgsys, CentreyDev);
						dx = OldCentrex - NewCentrex;
						dy = OldCentrey - NewCentrey;
						g_get_origin (pFenetreSyn->hgsys, &Originex, &Originey);
						g_add_deltas_page (pFenetreSyn->hgsys, &Originex, &Originey, dx, dy);
						ajuste_origine_fen_x (pFenetreSyn->hwndGr, pFenetreSyn->hgsys, &Originex);
						ajuste_origine_fen_y (pFenetreSyn->hwndGr, pFenetreSyn->hgsys, &Originey);

						::InvalidateRect (pFenetreSyn->hwndGr , NULL, TRUE);
						maj_espace_scroll_bar_h (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
						maj_espace_scroll_bar_v (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
						maj_thumbsize_scroll (pFenetreSyn->hwndGr, pFenetreSyn->hgsys);
						}
					else
						Beep (300, 300);
					}
				}
			break;

		default:
			bTraite = FALSE;
			break;
		} // switch (mp1 & 0xFFF0)

		return bTraite;
	} //OnSysCommand

  

// -----------------------------------------------------------------------
// Fen�tre d'�dition d'une page de synoptique
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocEditSyn (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT    mres = 0;                       // Valeur de retour
  BOOL       bTraite = TRUE;                // Indique commande non trait�e
  PENV_EDITSYN pEnvEditSyn;
  LONG        lx1;
  LONG        ly1;
  t_param_action param_action;
  t_bloc_action action;
  t_retour_action retour_action;
  PFENETRE_SYN pFenetreSyn;
  //
  LONG     dx0;
  LONG     dy0;

  switch (msg)
    {
    case WM_CREATE:
			{
			// cr�e et initialise l'environnement
			pEnvEditSyn = (PENV_EDITSYN)pCreeEnv (hwnd, sizeof (ENV_EDITSYN));
		  pEnvEditSyn->id = *(DWORD *)(((LPCREATESTRUCT)mp2)->lpCreateParams);
			pEnvEditSyn->mode_affichage = MODE_CLIP; // $$ par d�faut

			// continue l'initialisation de l'objet
			pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
			pFenetreSyn->hwndGr = hwnd;
			pFenetreSyn->hgsys = g_open (hwnd);
			g_init (pFenetreSyn->hgsys);

			g_set_page_units (pFenetreSyn->hgsys, c_G_NB_X, c_G_NB_Y, Appli.sizEcran.cx, Appli.sizEcran.cy);
			g_set_x_origine (pFenetreSyn->hgsys, 0); //$$ xOrigine); (� r�cup�rer au CreateWindow)
			g_set_y_origine (pFenetreSyn->hgsys, 0); //$$ yOrigine);

			// Mise � jour de l'objet associ� � hBdGr
			bdgr_associer_espace (pFenetreSyn->hBdGr, pFenetreSyn->hgsys);
			bdgr_charger_bmp_fond_page (pFenetreSyn->hBdGr);
			VerifWarning (bCreerFenetresElementGr (pFenetreSyn->hBdGr, PREMIER_ID_PAGE));
			}
			break;

    case WM_DESTROY:
		  pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
			pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
			VerifWarning (bFermerFenetresElementGr (pFenetreSyn->hBdGr));

			g_close (&pFenetreSyn->hgsys);
			pFenetreSyn->fen_libre = TRUE;
			pFenetreSyn->hwndGr = NULL;

			bLibereEnv (hwnd);
			break;

		/* $$ porter
    case WM_TRANSLATEACCEL:
         mres = (LRESULT) FALSE;
         break;
*/
    case WM_CHAR:
			bTraite = OnChar (hwnd, mp1, &mres);
			break;

    case WM_KEYDOWN:
			bTraite = OnKeyDown (hwnd, mp1, mp2, &mres);
			break;

    case WM_KEYUP:
			bTraite = OnKeyUp (hwnd, mp1, &mres);
			break;

		case WM_SYSCOMMAND:
			bTraite = OnSysCommand (hwnd, mp1, &mres);
      break;

		/* $$ � porter
    case WM_ADJUSTWINDOWPOS:
			{
			PSWP pswp = (PSWP) (PVOID)(mp1);
      pswp->fs = pswp->fs | SWP_ZORDER;
      pswp->hwndInsertBehind = hwndZOrder; //$$
      bornage_frame (hwnd, &(pswp->cx), &(pswp->cy));
      mres = CallWindowProc (old_wproc_frame, hwnd, msg, mp1, mp2);
			}
      break;
*/
    case WM_COMMAND:
		  pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
			switch (LOWORD(mp1))
				{
				case ID_FEN_SAISIE_TEXTE:
					{
					switch (HIWORD(mp1))
						{
						case INLINE_VALIDATION:
							{
							RECT	rectFen;
							POINT ptOrigine;

							// Validation du texte saisi dans une fen�tre Winline:
							// pr�pare les donn�es � traiter
							param_action.on = TRUE;
							param_action.id_fen = pEnvEditSyn->id;
							pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
							//$$ mauvais handle ::GetWindowText (hwnd, param_action.texte, L_MAX_TEXTE_ACTION);
							::GetWindowText ((HWND)mp2, param_action.texte, L_MAX_TEXTE_ACTION);
							::GetWindowRect (::GetDlgItem (hwnd, ID_FEN_SAISIE_TEXTE), &rectFen);
							ptOrigine.x = rectFen.left;
							ptOrigine.y = rectFen.top;
							::MapWindowPoints (NULL, hwnd, &ptOrigine, 1);
							g_convert_point_dev_to_page (pFenetreSyn->hgsys , &ptOrigine.x,  &ptOrigine.y);
							param_action.x1 =  ptOrigine.x;
							param_action.y1 =  ptOrigine.y;
							// ajoute les dans la queue des actions
							ajouter_action (action_validation_texte, &param_action);
							// ferme la fen�tre
							::DestroyWindow (pFenetreSyn->hdl_inline);
							pFenetreSyn->hdl_inline = NULL;
							}
							break;
						case INLINE_ABANDON:
							param_action.on = FALSE;
							param_action.id_fen = pEnvEditSyn->id;
							ajouter_action (action_validation_texte, &param_action);
							pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
							::DestroyWindow (pFenetreSyn->hdl_inline);
							pFenetreSyn->hdl_inline = NULL;
							break;
						// default: ID_PREMIER_CONTROLE break;
						} // switch (HIWORD(mp1))
					}
				break;
				}
			break;

    case WM_ERASEBKGND:
			// effacement du fond int�gr� au WM_PAINT => pas d'effacement effectu� ici
			// le flag fErase du WM_PAINT sera mis � TRUE dans le WM_PAINT
			mres = FALSE;
			break;

    case WM_PAINT:
			{
			PAINTSTRUCT ps;

			pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
			VerifWarning (::BeginPaint (hwnd, &ps));
			// ps.fErase pourrait �tre pass� en param�tre � la fonction de redessin
			action.tipe = action_redessine_paint;
			action.id_fen = pEnvEditSyn->id;
			pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
			VerifWarning (g_get_hdc (pFenetreSyn->hgsys) == ps.hdc);
			executer_action_immediate (&action, "", &retour_action);
			::EndPaint (hwnd, &ps);
			}
			break;

    case WM_SIZE:
			if (mp1 != SIZE_MINIMIZED)
				{
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				g_get_origin (pFenetreSyn->hgsys, &dx0, &dy0);

				ajuste_espace_fen (hwnd, pEnvEditSyn->id);
				change_pos_fen_bd (hwnd, pEnvEditSyn->id);

				//--------------------------------------------------------------------------------
				// Il faut recalculer la taille totale du document (s'il est en zoom)
				// Il faut recalculer la partie visible
				// Il faut recalculer la position si elle est � l'ext�rieur de la partie visible
				// Il faut remodifier la taille de la fen�tre si elle est plus grande que la taille totale du document
				//-------------------------------------------------------------------------------
				ajuste_origine_fen_x (hwnd, pFenetreSyn->hgsys, &dx0);
				ajuste_origine_fen_y (hwnd, pFenetreSyn->hgsys, &dy0);
				maj_thumbsize_scroll (hwnd, pFenetreSyn->hgsys);
				maj_espace_scroll_bar_h (hwnd, pFenetreSyn->hgsys);
				maj_espace_scroll_bar_v (hwnd, pFenetreSyn->hgsys);

				//$$ Redessine tout
				action.tipe = action_redessine_paint;
				action.id_fen = pEnvEditSyn->id;
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				//VerifWarning (g_get_hdc (pFenetreSyn->hgsys) == ps.hdc);
				executer_action_immediate (&action, "", &retour_action);
				}
			break;

    case WM_MOVE:
			{
			POINT ptOrigineClientPix = {(int)(short) LOWORD(mp2), (int)(short) HIWORD(mp2)};

			pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
			change_pos_fen_bd (hwnd, pEnvEditSyn->id);
			pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
			g_set_origine_dc (pFenetreSyn->hgsys, ptOrigineClientPix);
			}
			break;

    case WM_HSCROLL:
			//$$ v�rifier
			if (TRUE) //LOWORD (mp1) == FID_HORZSCROLL)
				{
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				g_get_origin (pFenetreSyn->hgsys, &dx0, &dy0);
				if (origine_x_modifie (hwnd, pEnvEditSyn, mp2, &dx0))
					{
					ajuste_origine_fen_x (hwnd, pFenetreSyn->hgsys, &dx0);
					change_origine_fen_bd (dx0, dy0);
					::InvalidateRect (hwnd, NULL, TRUE);
					}
				}
			break;

    case WM_VSCROLL:
			if (TRUE) //$$ v�rifier LOWORD (mp1) == FID_VERTSCROLL)
				{
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				g_get_origin (pFenetreSyn->hgsys, &dx0, &dy0);
				if (origine_y_modifie (hwnd, pEnvEditSyn, mp2, &dy0))
					{
					ajuste_origine_fen_y (hwnd, pFenetreSyn->hgsys, &dy0);
					change_origine_fen_bd (dx0, dy0);
					::InvalidateRect (hwnd, NULL, TRUE);
					}
				}
			break;

		// case WM_CHILDACTIVATE:
    case WM_ACTIVATE:
			if (LOWORD(mp1)!= WA_INACTIVE)
				{
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				param_action.id_fen = pEnvEditSyn->id;
				ajouter_action (action_active_fenetre, &param_action);
				}
			// Gestion de palette $$ retard de l'activation de la page par rapport � la palette
//			::SendMessage(hwnd, WM_QUERYNEWPALETTE, 0, 0);
			break;

/*
		case WM_ACTIVATE:
			::SendMessage(hwnd, WM_QUERYNEWPALETTE, 0, 0L);
			break;

		case WM_PALETTECHANGED:
			// Dib charg� ?
			if (pEnv == NULL || pEnv->hdd == NULL)
				break;

			// oui => changement de palette ?
			hdc = ::GetDC(hwnd);
			if (f = DrawDibRealize (pEnv->hdd, hdc, TRUE))
				::InvalidateRect(hwnd,NULL,TRUE);

			::ReleaseDC(hwnd,hdc);
			return f;

		case WM_QUERYNEWPALETTE:
			// Dib charg� ?
			if (pEnv == NULL || pEnv->hdd == NULL)
				break;

			// oui => changement de palette ?
			hdc = ::GetDC(hwnd);
			if (f = DrawDibRealize(pEnv->hdd, hdc, FALSE))
				// oui => redessin de la fen�tre
				::InvalidateRect(hwnd,NULL,TRUE);

			::ReleaseDC(hwnd,hdc);
			// renvoie le bon r�sultat
			return f;
*/

    case WM_LBUTTONDOWN:
			if (CtxtgrGetModeEdition() != EN_TEST)
				{
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				param_action.id_fen = pEnvEditSyn->id;
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);

				// Activation �ventuelle de la fen�tre ($$ doublon avec DefWindowProc ?)
				if (GetActiveWindow () != pFenetreSyn->hwndGr)
					{
					param_action.id_fen = pEnvEditSyn->id;
					ajouter_action (action_active_fenetre, &param_action);
					}
				// envoie les param�tres de l'appui : position et �tat du clavier
				lx1 = (int) LOWORD(mp2);
				ly1 = (int) HIWORD(mp2);
				g_convert_point_dev_to_page (pFenetreSyn->hgsys, &lx1,  &ly1);
				param_action.x1 =  lx1;
				param_action.y1 =  ly1;
				param_action.param_fonction	= (int)mp1;
				ajouter_action (action_click, &param_action);
				}
			bTraite = FALSE;
			break;

    case WM_LBUTTONUP:
			if (CtxtgrGetModeEdition() != EN_TEST)
				{
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				param_action.id_fen = pEnvEditSyn->id;
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				lx1 = (int) LOWORD(mp2);
				ly1 = (int) HIWORD(mp2);
				g_convert_point_dev_to_page (pFenetreSyn->hgsys, &lx1,  &ly1);
				param_action.x1 =  lx1;
				param_action.y1 =  ly1;
				param_action.param_fonction	= (int)mp1;
				ajouter_action (action_click_up, &param_action);
				}
			bTraite = FALSE;
			break;

    case WM_LBUTTONDBLCLK:
			if (CtxtgrGetModeEdition() != EN_TEST)
				{
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				param_action.id_fen = pEnvEditSyn->id;
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				lx1 = (int) LOWORD(mp2);
				ly1 = (int) HIWORD(mp2);
				g_convert_point_dev_to_page (pFenetreSyn->hgsys, &lx1,  &ly1);
				param_action.x1 =  lx1;
				param_action.y1 =  ly1;
				param_action.param_fonction	= (int)mp1;
				ajouter_action (action_double_click, &param_action);
				}
			bTraite = FALSE;
			break;

    case WM_RBUTTONDOWN:
			pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
			if (CtxtgrGetModeEdition() != EN_TEST)
				{
				param_action.id_fen = pEnvEditSyn->id;
				ajouter_action (action_click2, &param_action);
				}
			bTraite = FALSE;
			break;

    case WM_MOUSEMOVE:
			{
			if (CtxtgrGetModeEdition() != EN_TEST)
				{
				pEnvEditSyn = (PENV_EDITSYN)pGetEnv (hwnd);
				param_action.id_fen = pEnvEditSyn->id;
				pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, pEnvEditSyn->id);
				lx1 = (SHORT) LOWORD(mp2);
				ly1 = (SHORT) HIWORD(mp2);
				g_convert_point_dev_to_page (pFenetreSyn->hgsys , &lx1,  &ly1);
				bornage (&lx1, c_G_MIN_X, c_G_MAX_X);
				bornage (&ly1, c_G_MIN_Y, c_G_MAX_Y);
				param_action.x1 =  lx1;
				param_action.y1 =  ly1;
				param_action.param_fonction	= (int)mp1;
				ajouter_action (action_goto_lc, &param_action);
				mres = TRUE;
				}
			else
				bTraite = FALSE;
			}
			break;

    default:
			bTraite = FALSE;
			break;
		} // switch (msg)

  // Message non trait� => traitement par d�faut
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  } // wndprocEditSyn


// -----------------------------------------------------------------------
// Enregistre la classe szClasseEditSyn et cr�e le parent des pages
BOOL bInitEditSyn (void)
  {
	BOOL				bRes = FALSE;
  static BOOL bClasseEnregistree = FALSE;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= CS_OWNDC | CS_DBLCLKS; // $$ v�rifier => | CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc		= wndprocEditSyn; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= LoadIcon (Appli.hinstDllLng, MAKEINTRESOURCE(IDICO_DOC_GR));
		wc.hCursor				= NULL; // pas de curseur par d�faut
		wc.hbrBackground	= NULL; // pas de brosse d'effacement par d�faut
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseEditSyn; 
		bClasseEnregistree = (RegisterClass (&wc)!= 0);
		}

	if (bClasseEnregistree)
		{
		// Cr�e une fen�tre parente pour toutes les pages
		hwndParentPage = hwndCreeParentPopUp ();
		if (hwndParentPage != NULL)
			bRes = TRUE;
		}

  return bRes;
  }

// -----------------------------------------------------------------------
static void trouve_id_fenetre_gr (DWORD *id_fenetre)
  {
  PFENETRE_SYN pFenetreSyn;
  BOOL trouve;

  if (!existe_repere (b_fenetre_gr))
    {
    cree_bloc (b_fenetre_gr, sizeof (FENETRE_SYN), 1);
    (*id_fenetre) = 1;
    }
  else
    {
    (*id_fenetre) = 0;
    trouve = FALSE;
    while (((*id_fenetre) < nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_gr)) && (!trouve))
      {
      (*id_fenetre)++;
      pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, (*id_fenetre));
      trouve = pFenetreSyn->fen_libre;
      }
    if (!trouve)
      {
      (*id_fenetre)++;
      insere_enr (szVERIFSource, __LINE__, 1, b_fenetre_gr, (*id_fenetre));
      }
    }
  pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, (*id_fenetre));
  pFenetreSyn->fen_libre = FALSE;
  }

// -----------------------------------------------------------------------
void InitFenetreSyn (DWORD * pdwIdFenetre, HBDGR hBdGr)
  {
  PFENETRE_SYN pFenetreSyn;

  trouve_id_fenetre_gr (pdwIdFenetre);
  pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, *pdwIdFenetre);
  pFenetreSyn->hwndGr = NULL;
  pFenetreSyn->hgsys = NULL;
  pFenetreSyn->hBdGr = hBdGr;
  pFenetreSyn->coeff_zoom = (FLOAT) 1;
  pFenetreSyn->facteur_zoom = 100;
  }

// -----------------------------------------------------------------------
static void cree_menu_systeme (DWORD id_fen, DWORD mode_aff, DWORD facteur_zoom)
  {
  PFENETRE_SYN	pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, id_fen);
	HMENU					hwndsysmenu = ::GetSystemMenu (pFenetreSyn->hwndGr, FALSE);

	MenuAppendSeparateur (hwndsysmenu);
	MenuAppendMI (hwndsysmenu, c_inf_grossir, CMD_SYSMENU_GROSSIR);
	MenuGriseItem (hwndsysmenu, CMD_SYSMENU_GROSSIR, mode_aff != MODE_ZOOM);
  
	MenuAppendMI (hwndsysmenu, c_inf_maigrir, CMD_SYSMENU_MAIGRIR);
	MenuGriseItem (hwndsysmenu, CMD_SYSMENU_MAIGRIR, mode_aff != MODE_ZOOM);

	MenuAppendMI (hwndsysmenu, c_inf_facteur_zoom, CMD_SYSMENU_FACTEUR);
	MenuGriseItem (hwndsysmenu, CMD_SYSMENU_FACTEUR, mode_aff != MODE_ZOOM);

  if (mode_aff == MODE_ZOOM)
    {
    pFenetreSyn->coeff_zoom = (FLOAT)facteur_zoom / (FLOAT)100.0;
    pFenetreSyn->facteur_zoom = facteur_zoom;
    }
  }

// -----------------------------------------------------------------------
void CreeFenetreEditSyn (DWORD id_fenetre, HGSYS * phgsys)
  {
  PFENETRE_SYN pFenetreSyn;
  PENV_EDITSYN	pEnvEditSyn;
  DWORD dwStyle = WS_POPUP; // ? WS_VISIBLE  WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN |WS_CLIPSIBLINGS | WS_CHILD window style
	DWORD	dwStyleEx = WS_EX_WINDOWEDGE; // | WS_EX_CONTROLPARENT
  DWORD mode_aff;
  DWORD facteur_zoom;
  DWORD mode_deform;
  BOOL sizeborder;
  BOOL titre;
  char texte_titre[c_NB_CAR_TITRE_PAGE_GR+1];
	BOOL bFenModale;
	BOOL bFenDialog;
  BOOL un_sysmenu;
  BOOL un_minimize;
  BOOL un_maximize;
  BOOL scroll_bar_h;
  BOOL scroll_bar_v;
  DWORD grille_x;
  DWORD grille_y;
  int xFen, yFen;
  int cxFen, cyFen;
  LONG xOrigine, yOrigine;
  LONG lxFen, lyFen;
  LONG lcxFen, lcyFen;

  // chargement si n�cessaire des ressources
	if (!hptrcroix)
		{
		hptrcroix = LoadCursor (NULL, IDC_CROSS);
		hptrcible = LoadCursor (Appli.hinstDllLng, MAKEINTRESOURCE (IDC_CURSOR_DUPLIQUE)); // NULL, IDC_UPARROW);
		hptrfleche = LoadCursor (NULL, IDC_ARROW);
		}

  pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, id_fenetre);
  bdgr_lire_styles_page  (pFenetreSyn->hBdGr, &bFenModale, &bFenDialog, &mode_aff, &facteur_zoom, &mode_deform,
                          &sizeborder, &titre, texte_titre, &un_sysmenu, &un_minimize,
                          &un_maximize, &scroll_bar_h, &scroll_bar_v,
                          &grille_x, &grille_y);
  bdgr_lire_coord_page (pFenetreSyn->hBdGr, &xFen, &yFen, &cxFen, &cyFen);
  lxFen = xFen;   lyFen = yFen;
  lcxFen = cxFen; lcyFen = cyFen;
  Spc_CvtPtEnPixel (hSpcFen, &lxFen,  &lyFen);
  Spc_CvtSizeEnPixel (hSpcFen, &lcxFen,  &lcyFen);
  xFen = lxFen;   
	yFen = lyFen;
  cxFen = lcxFen;
	cyFen = lcyFen;
  bdgr_lire_origine_page  (pFenetreSyn->hBdGr, &xOrigine, &yOrigine);
	pFenetreSyn->hwndGr = NULL;

  if (sizeborder)
    dwStyle |= WS_THICKFRAME;
	else
	  dwStyle |= WS_DLGFRAME;
  if (un_minimize)
    dwStyle |= WS_MINIMIZEBOX;
  if (un_maximize)
    dwStyle |= WS_MAXIMIZEBOX;

	// Ajoute un menu syst�me si n�cessaire
  if (un_sysmenu || un_minimize || un_maximize)
    dwStyle |= WS_SYSMENU;

	// Rajoute une barre de titre si n�cessaire
  if (titre || un_minimize || un_maximize || un_sysmenu)
    dwStyle |= WS_CAPTION;

	// La fen�tre est toujours rajout�e dans la barre de t�che
	dwStyleEx |= WS_EX_APPWINDOW;

  if (scroll_bar_h)
    dwStyle |= WS_HSCROLL;
  if (scroll_bar_v)
    dwStyle |= WS_VSCROLL;

	// cr�ation de la fen�tre
	CreateWindowEx (dwStyleEx, szClasseEditSyn, texte_titre, dwStyle,
		CW_USEDEFAULT, CW_USEDEFAULT,	CW_USEDEFAULT, CW_USEDEFAULT,
		hwndParentPage,	NULL,	Appli.hinst, &id_fenetre);

  pEnvEditSyn = (PENV_EDITSYN)pGetEnv (pFenetreSyn->hwndGr);
  pEnvEditSyn->mode_affichage = mode_aff;

  (*phgsys) = pFenetreSyn->hgsys;
  bornage_frame (pFenetreSyn->hwndGr, &cxFen, &cyFen); // un peu violent pour modif type fen:
  ::SetWindowPos (pFenetreSyn->hwndGr, HWND_TOP, xFen, yFen, cxFen, cyFen, SWP_SHOWWINDOW); // le WM_AJUST n'est pas recu sur WinSet

	// Ajout de commandes sp�cifiques au menu syst�me
  if (un_sysmenu)
    cree_menu_systeme (id_fenetre, mode_aff, facteur_zoom);
  }

// -----------------------------------------------------------------------
void fermer_fenetre_gr (DWORD id_fenetre)
  {
  PFENETRE_SYN pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, id_fenetre);

  ::DestroyWindow (pFenetreSyn->hwndGr);
  }

// -----------------------------------------------------------------------
void ouvrir_fenetre_saisie_gr (DWORD id_fenetre, G_COULEUR backcolor, G_COULEUR textcolor,
                               DWORD police, DWORD style_police, DWORD taille_police,
                               LONG x1, LONG y1, char *texte_depart)

  {
  PFENETRE_SYN pFenetreSyn;
#define CX_SAISIE_TEXTE 150

  pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, id_fenetre);
  g_convert_point_page_to_dev (pFenetreSyn->hgsys, &x1, &y1);
  taille_police = g_convert_dy_page_to_dev (pFenetreSyn->hgsys, taille_police);
  pFenetreSyn->hdl_inline = InLineCreate (pFenetreSyn->hwndGr,
                              ID_FEN_SAISIE_TEXTE, x1, y1,
                              CX_SAISIE_TEXTE, taille_police + 4,
                              backcolor, textcolor, NBR_CAR_MAX_GR,
                              police, style_police, taille_police, texte_depart,
                              TRUE, FALSE, FALSE, TRUE);
	if (pFenetreSyn->hdl_inline)
		SetFocus (pFenetreSyn->hdl_inline);
  }

// -----------------------------------------------------------------------
void afficher_titre_fenetre_gr (DWORD id_fenetre)
  {
  PFENETRE_SYN pFenetreSyn;
  char texte[c_NB_CAR_TITRE_PAGE_GR+1];

  texte [0] = '\0';
  pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, id_fenetre);
  if (bdgr_lire_titre_page (pFenetreSyn->hBdGr, texte))
    ::SetWindowText (pFenetreSyn->hwndGr, texte);
  }

// --------------------------------------------------------------------
void capturer_souris_fenetre_gr (DWORD id_fenetre)
  {
  PFENETRE_SYN pFenetreSyn;

  pFenetreSyn = (PFENETRE_SYN)pointe_enr (szVERIFSource, __LINE__, b_fenetre_gr, id_fenetre);
	if (pFenetreSyn->hwndGr)
		SetCapture (pFenetreSyn->hwndGr);
	else
		ReleaseCapture();
  }

// --------------------------------------------------------------------
void dessiner_souris_fenetre_gr (DWORD type_souris)
  {
  switch (type_souris)
    {
    case SOURIS_FLECHE:
      SetCursor (hptrfleche);
      break;
    case SOURIS_CROIX:
      SetCursor (hptrcroix);
      break;
    case SOURIS_CIBLE:
      SetCursor (hptrcible);
      break;
    default:
      break;
    }
  }

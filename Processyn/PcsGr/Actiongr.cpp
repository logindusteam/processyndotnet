/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : actiongr.C                                               |
 |   Auteur  : AC					        	|
 |   Date    : 26/10/92					        	|
 |   Version : 3.20							|
 +----------------------------------------------------------------------*/
// Gestion des commandes de l'�diteur de synoptique PcsGr
// Version Win32

#include "stdafx.h"
#include "std.h"
#include "lng_res.h"
#include "tipe.h"
#include "UStr.h"
#include "DocMan.h"
#include "mem.h"
#include "son.h"
#include "driv_dq.h"
#include "LireLng.h"
#include "IdLngLng.h"
#include "pcsgr.h"
#include "appligr.h"
#include "Descripteur.h"
#include "G_Objets.h"
#include "inter_gr.h"
#include "pcsspace.h"  // c_G_MAX_X et c_G_MAX_Y
#include "g_sys.h"
#include "bdgr.h"
#include "bdanmgr.h"   // type animation : pas d'appel de verbe
#include "BdPageGr.h"
#include "BdMarqGr.h"
#include "ctxtgr.h"
#include "PcsDlg.h"
#include "ppgr.h"
#include "WGr.h"
#include "WEditSyn.h"
#include "cmdpcs.h"
#include "IdStrProcessyn.h"
#include "SignatureFichier.h"
#include "Verif.h"
#include "actiongr.h"
VerifInit;

#define c_OUVERTURE_FEN    1
#define c_RESTAURATION_FEN 2

#define b_commande       627      // non sauv� dans application
#define b_text_commande  629      // non sauv� dans application

#define B_ACTION			b_commande			// numero de bloc utilis� comme queue d'action
#define B_TXT_ACTION	b_text_commande

//$$ il ne faudrait pas de sauvegarde des queues si stockage sur blocs hors BD
static const char fic_bloc_action [] = "b_action";	// nom du fichier pour sauver la queue : sale
static const char fic_bloc_text_action [] = "btextcmd";	// nom du fichiers pour sauver les textes : sale
static int mode_lecture_action = MODE_INTERACTIF;		// mode lecture interactif ou macro

static PCSTR pszBackUpSyn = "BackSyn.bak";

// ----------------------------------------------------------------------
//                          PROCEDURE LOCALES                            
// ----------------------------------------------------------------------
static BOOL point_dans_rectangle (LONG ptx, LONG pty, LONG x1, LONG y1, LONG x2, LONG y2)
// attention x1 < x2 et y1 < y2 � l'entree
  {
  BOOL inside;

  inside = (BOOL) ((ptx < x2) && (ptx > x1) && (pty < y2) && (pty > y1));
  return (inside);
  }

// ---------------------------------------------------------------------
// Renvoie TRUE si le param�tre wParam d'une op�ration de souris indique que la touche CTRL est enfonc�e
static BOOL Touche_CTRL_Enfoncee (WPARAM wParam)
  {
  return wParam & MK_CONTROL;
  }

//---------------------------------------------------------------------
// Mise � jour de la barre d'outil de couleur
static void dessine_menu_couleur (void)
  {
  animer_menu_couleur_gr (action_change_color_fond_page, get_ctxtgr_couleur_fond ());
  animer_menu_couleur_gr (action_change_color_dessin, get_ctxtgr_couleur_trace ());
  }

// ----------------------------------------------------------------------
static void initialise_epure_rect_selection (HBDGR hbdgr)
  {
  bdgr_action_epure (hbdgr, G_ACTION_RECTANGLE);
  bdgr_couleur_epure (hbdgr, G_YELLOW);
  bdgr_style_ligne_epure (hbdgr, G_STYLE_LIGNE_POINTS);
  }

// ----------------------------------------------------------------------
// Remplis l'objet �pure avec les valeurs courantes - pas de points
static void initialise_epure (HBDGR hbdgr)
  {
  bdgr_abandonner_epure (hbdgr);
  bdgr_action_epure (hbdgr, CtxtgrGetAction ());
  bdgr_couleur_epure (hbdgr, get_ctxtgr_couleur_trace ());
  bdgr_style_ligne_epure (hbdgr, get_ctxtgr_style_ligne ());
  bdgr_style_remplissage_epure (hbdgr, get_ctxtgr_style_remplissage());
  bdgr_style_texte_epure (hbdgr, get_ctxtgr_police(), get_ctxtgr_style_police());
  bdgr_texte_epure (hbdgr, get_ctxtgr_draw_texte ());
  }

// ---------------------------------------------------------------------
// Pour tous les contextes de fen�tres
static void reset_marque_epure_partout (void)
  {
  DWORD id_fen_origine;
	HBDGR hbdgr;

  if (existe_ctxtgr_fenetre ())
    {
    id_fen_origine = get_ctxtgr_id_fenetre ();
    if (selectionner_ctxtgr_premier ())
      {
      do
        {
        hbdgr = get_ctxtgr_hBdGr ();
        bdgr_demarquer_page (hbdgr, MODE_MAJ_ECRAN);
        initialise_epure (hbdgr);
        } while (selectionner_ctxtgr_suivant ());
      }
    selectionner_ctxtgr_id_fenetre (id_fen_origine);
    }
  }


// ---------------------------------------------------------------------
static void met_en_selection (void)
  {
  reset_marque_epure_partout ();
  set_ctxtgr_dessin_en_cours (FALSE);
  CtxtgrSetModeEdition (EN_SELECTION);
  set_ctxtgr_selection_en_cours (FALSE);
  set_ctxtgr_deplacement_en_cours (FALSE);
  set_ctxtgr_deformation_en_cours (FALSE);
  relacher_souris ();
  }

// ---------------------------------------------------------------------
static void met_en_trace (void)
  {
  reset_marque_epure_partout ();
  set_ctxtgr_dessin_en_cours (FALSE);
  set_ctxtgr_selection_en_cours (FALSE);
  set_ctxtgr_deplacement_en_cours (FALSE);
  set_ctxtgr_deformation_en_cours (FALSE);
  set_ctxtgr_charge_symb_en_cours (FALSE);
  set_ctxtgr_duplication_en_cours (FALSE);
  CtxtgrSetModeEdition (EN_TRACE);
  relacher_souris ();
  }

// ----------------------------------------------------------------------
static void ouvre_fenetre (DWORD page)
  {
  DWORD id_fenetre;
  HBDGR hbdgr = hbdgr_ouvrir_page (page, TRUE, TRUE); // $$ BOOL bEnEdition en mode test
  HGSYS hgsys;

  InitFenetreSyn (&id_fenetre, hbdgr);

	// initialisation du contexte de la fen�tre
  creer_ctxtgr_fenetre ();
  set_ctxtgr_id_fenetre (id_fenetre);
  set_ctxtgr_hbdgr (hbdgr);
  set_ctxtgr_page (page);
  set_ctxtgr_couleur_fond (bdgr_lire_couleur_fond_page (hbdgr));

	// Cr�ation de la fen�tre
  CreeFenetreEditSyn (id_fenetre, &hgsys);
  //bdgr_charger_bmp_fond_page (hbdgr);

	// fin ouverture fen�tre
  dessine_menu_couleur ();
  initialise_epure (hbdgr);
  }

// ----------------------------------------------------------------------
static void ferme_fenetre (BOOL majvisible)
  {
  HBDGR hbdgr = get_ctxtgr_hBdGr ();

  bdgr_supprimer_bmp_fond_page (hbdgr);
  fermer_fenetre_gr (get_ctxtgr_id_fenetre ());
  bdgr_fermer_page (&hbdgr, majvisible);
	set_ctxtgr_hbdgr (hbdgr);
  supprimer_ctxtgr_fenetre ();
  }

// ----------------------------------------------------------------------
static void ouvre_toute_fenetre_visible (void)
  {
  DWORD nb_page;
  DWORD n_page;

  nb_page = bdgr_nombre_pages ();
  for (n_page = 1; (n_page <= nb_page); n_page++)
    {
    if (bdgr_page_visible (n_page))
      {
      ouvre_fenetre (n_page);
      }
    }
  }

// ----------------------------------------------------------------------
// ferme toutes les fenetres qui sont presentes sur l'ecran
static void ferme_toute_fenetre_gr (BOOL majvisibilite)
  {
  if (selectionner_ctxtgr_premier ())
    {
    do
      {
      ferme_fenetre (majvisibilite);
      } while (selectionner_ctxtgr_premier ());
    }
  }

// ----------------------------------------------------------------------
static void active_fenetre (DWORD id_fenetre)
  {
  if (id_fenetre != get_ctxtgr_id_fenetre())
    {
		HBDGR hbdgr = get_ctxtgr_hBdGr ();

    if (CtxtgrGetModeEdition () == EN_TRACE)
      {
      initialise_epure (hbdgr);
      }
    selectionner_ctxtgr_id_fenetre (id_fenetre);
    hbdgr = get_ctxtgr_hBdGr ();
    set_ctxtgr_couleur_fond (bdgr_lire_couleur_fond_page (hbdgr));
    set_ctxtgr_dessin_en_cours (FALSE);
    set_ctxtgr_selection_en_cours (FALSE);
    set_ctxtgr_deplacement_en_cours (FALSE);
    set_ctxtgr_deformation_en_cours (FALSE);
    initialise_epure (hbdgr);
    dessine_menu_couleur ();
    }
  }


// ----------------------------------------------------------------------
//                             INITIALISATION                            
// ----------------------------------------------------------------------

// ----------------------------------------------------------------------
// Initialise la queue d'actions
void initialiser_action_gr (void)
  {
  // Initialisation des blocs associ�s � la queue
	enleve_bloc (B_ACTION);
	cree_bloc (B_ACTION, sizeof (t_bloc_action), 0);
	enleve_bloc (B_TXT_ACTION);
	cree_bloc (B_TXT_ACTION, L_MAX_TEXTE_ACTION, 0);
	}

// ----------------------------------------------------------------------
//                                ACTION                                 
// ----------------------------------------------------------------------

//-----------------------------------------------------------
// Rajoute dans la queue de texte une chaine
static void AjouteTexteDansQueue (PCSTR pszTexteAStocker)
	{
  PSTR pszQueue;

  insere_enr (szVERIFSource, __LINE__, 1, B_TXT_ACTION, 1);
  pszQueue = (PSTR)pointe_enr (szVERIFSource, __LINE__, B_TXT_ACTION, 1);
  StrCopy (pszQueue, pszTexteAStocker);
	}

// ----------------------------------------------------------------------
// Ajoute une action dans la queue d'actions � la position sp�cifi�e
// attention pas de blindage sur position
// texte toujours en 1.Faire insertion texte en dernier ou repointer sur pAction
static void inserer_action (ID_ACTION_GR action, t_param_action *param_action, DWORD position)
  {
  t_bloc_action * pAction;
  
  if ((action != action_rien) || ((action == action_rien) && (nb_enregistrements (szVERIFSource, __LINE__, B_ACTION) == 0)))
    {
    insere_enr (szVERIFSource, __LINE__, 1, B_ACTION, position);
    pAction = (t_bloc_action *)pointe_enr (szVERIFSource, __LINE__, B_ACTION, nb_enregistrements (szVERIFSource, __LINE__, B_ACTION));
    }
  pAction->tipe = action;
  switch (action)
    {
    case action_active_fenetre:
    case action_redessine_paint:
    case action_abandon:
      pAction->id_fen = param_action->id_fen;
      break;

    case action_ouvre_fenetre:
    case action_duplicate:
      pAction->page = param_action->page_gr;
      break;

    case action_change_color:
    case action_change_color_dessin:
    case action_change_color_fond_user:
    case action_change_color_fond_page:
      pAction->couleur = param_action->couleur_gr;
      break;

    case action_change_style_ligne:
    case action_change_style_remplissage:
      pAction->style = param_action->style_gr;
      break;

    case action_change_police:
      pAction->police = param_action->police_gr;
      pAction->style = param_action->style_gr;
      pAction->taille = param_action->taille;
      break;

    case action_change_cursor_shape:
      pAction->fonction = param_action->fonction;
      break;

    case action_change_shape:
    case action_change_mode_color:
      pAction->fonction = param_action->fonction;
      pAction->param_fonction = param_action->param_fonction;
      break;

    case action_sauve_application:
    case action_sauve_application_sat:
    case action_titre_page:
    case action_export:
    case action_charge_symb:
    case action_sauve_symb:
    case action_change_bmp_fond_page:
			AjouteTexteDansQueue (param_action->texte);
      break;

    case action_charge_application:
    case action_charge_application_sat:
			AjouteTexteDansQueue (param_action->texte);
      pAction->on = param_action->on;
      break;

    case action_change_style_fenetre:
      pAction->bFenModale = param_action->bFenModale;
      pAction->bFenDialog = param_action->bFenDialog;
      pAction->bBitmapFond = param_action->bBitmapFond;
      pAction->un_titre = param_action->un_titre;
      pAction->un_bord = param_action->un_bord;
      pAction->une_barreh = param_action->une_barreh;
      pAction->une_barrev = param_action->une_barrev;
      pAction->un_sysmenu = param_action->un_sysmenu;
      pAction->un_minimize = param_action->un_minimize;
      pAction->un_maximize = param_action->un_maximize;
      pAction->mode_aff = param_action->mode_aff;
      pAction->facteur_zoom = param_action->facteur_zoom;
      pAction->mode_deform = param_action->mode_deform;
      pAction->grille_x = param_action->grille_x;
      pAction->grille_y = param_action->grille_y;
      pAction->couleur = param_action->couleur_gr;
			if (pAction->bBitmapFond)
				StrCopy (pAction->szPath, param_action->szPath);
			AjouteTexteDansQueue (param_action->texte);
      break;

    case action_import:
      pAction->version = param_action->version;
			AjouteTexteDansQueue (param_action->texte);
      break;

    case action_mode_dessin:
    case action_mode_trace:
    case action_animation_rl:
      pAction->param_fonction = param_action->param_fonction;
      break;

    case action_lire_macro:
      pAction->on = param_action->on;
			AjouteTexteDansQueue (param_action->texte);
      break;

    case action_change_pos_fenetre:
      pAction->id_fen = param_action->id_fen;
      pAction->x1 = param_action->x1;
      pAction->y1 = param_action->y1;
      pAction->x2 = param_action->x2;
      pAction->y2 = param_action->y2;
      break;

    case action_goto_lc:
    case action_click:
    case action_click_up:
    case action_double_click:
    case action_change_origine:
		case action_deplace_selection:
      pAction->id_fen = param_action->id_fen;
      pAction->x1 = param_action->x1;
      pAction->y1 = param_action->y1;
      pAction->param_fonction = param_action->param_fonction;
      break;

    case action_validation_texte:
      pAction->on = param_action->on;
      pAction->id_fen = param_action->id_fen;
      pAction->x1 = param_action->x1;
      pAction->y1 = param_action->y1;
			AjouteTexteDansQueue (param_action->texte);
      break;

    case action_click2:
      pAction->id_fen = param_action->id_fen;
      break;

    default: // pas de parametre
      break;
    }
  }
// ----------------------------------------------------------------------
void ajouter_action (ID_ACTION_GR action, t_param_action * param_action)
  {
  DWORD position = nb_enregistrements (szVERIFSource, __LINE__, B_ACTION) + 1;

  inserer_action (action, param_action, position);
  }

// ----------------------------------------------------------------------
static void supprimer_action (void)
  {
  t_bloc_action *pAction;

  pAction = (t_bloc_action *)pointe_enr (szVERIFSource, __LINE__, B_ACTION, 1);
  switch (pAction->tipe)
    {
    case action_charge_application_sat:
    case action_charge_application:
    case action_sauve_application:
    case action_sauve_application_sat:
    case action_change_style_fenetre:
    case action_validation_texte:
    case action_titre_page:
    case action_export:
    case action_import:
    case action_charge_symb:
    case action_sauve_symb:
    case action_change_bmp_fond_page:
      enleve_enr (szVERIFSource, __LINE__, 1, B_TXT_ACTION, 1);
      break;
    }
  enleve_enr (szVERIFSource, __LINE__, 1, B_ACTION, 1);
  }

// ----------------------------------------------------------------------
static void reset_action (void)
  {
  if (existe_repere (B_ACTION))
    {
    enleve_bloc (B_ACTION);
    }
  if (existe_repere (B_TXT_ACTION))
    {
    enleve_bloc (B_TXT_ACTION);
    }
  }

// ----------------------------------------------------------------------
// enleve les blocs et surtout les sauve dans le sens de la cr�ation
static void sauver_action (void)
  {
  HFDQ repere_fichier;
  t_bloc_action *pAction;
  char *ptr_text;
  DWORD i;

  uFicEfface (fic_bloc_action);
  if (uFicOuvre (&repere_fichier, fic_bloc_action, sizeof (t_bloc_action)) == 0)
    {
    for (i = 1; (i <= nb_enregistrements (szVERIFSource, __LINE__, B_ACTION)); i++)
      {
      pAction = (t_bloc_action *)pointe_enr (szVERIFSource, __LINE__, B_ACTION, i);
      uFicEcrireTout (repere_fichier, pAction, 1);
      }
    uFicFerme (&repere_fichier);
    }

  uFicEfface (fic_bloc_text_action);
  if (uFicOuvre (&repere_fichier, fic_bloc_text_action, L_MAX_TEXTE_ACTION) == 0)
    {
    for (i = 1; (i <= nb_enregistrements (szVERIFSource, __LINE__, B_TXT_ACTION)); i++)
      {
      ptr_text = (char *)pointe_enr (szVERIFSource, __LINE__, B_TXT_ACTION, i);
      uFicEcrireTout (repere_fichier, ptr_text, 1);
      }
    uFicFerme (&repere_fichier);
    }
  reset_action ();
  }

// ----------------------------------------------------------------------
// restaure les blocs dans le sens de la creation : primordial.
// $$ utiliser des blocs hors Base De Donn�es
static void restaurer_action (void)
  {
  HFDQ repere_fichier;
  t_bloc_action *pAction;
  char *ptr_text;
  DWORD i;

  enleve_bloc (B_ACTION);
  cree_bloc (B_ACTION, sizeof (t_bloc_action), 0);
  enleve_bloc (B_TXT_ACTION);
  cree_bloc (B_TXT_ACTION, L_MAX_TEXTE_ACTION, 0);
  if (bFicPresent (fic_bloc_action))
    {
    if ((uFicOuvre (&repere_fichier, fic_bloc_action, sizeof (t_bloc_action)) == 0)&&
			(uFicNbEnr (repere_fichier, &i) == NO_ERROR))
      {
      for (; (i > 0); i--)
        {
        insere_enr (szVERIFSource, __LINE__, 1, B_ACTION, nb_enregistrements (szVERIFSource, __LINE__, B_ACTION)+1);
        pAction = (t_bloc_action *)pointe_enr (szVERIFSource, __LINE__, B_ACTION, nb_enregistrements (szVERIFSource, __LINE__, B_ACTION));
        uFicLireTout (repere_fichier, pAction, 1);
        }
      uFicFerme (&repere_fichier);
      }
    uFicEfface (fic_bloc_action);
    }

  if (bFicPresent (fic_bloc_text_action))
    {
    if ((uFicOuvre (&repere_fichier, fic_bloc_text_action, L_MAX_TEXTE_ACTION) == 0) &&
			(uFicNbEnr (repere_fichier, &i) == NO_ERROR))
      {
      for (; (i > 0); i--)
        {
        insere_enr (szVERIFSource, __LINE__, 1, B_TXT_ACTION, nb_enregistrements (szVERIFSource, __LINE__, B_TXT_ACTION)+1);
        ptr_text = (char *)pointe_enr (szVERIFSource, __LINE__, B_TXT_ACTION, nb_enregistrements (szVERIFSource, __LINE__, B_TXT_ACTION));
        uFicLireTout (repere_fichier, ptr_text, 1);
        }
      uFicFerme (&repere_fichier);
      }
    uFicEfface (fic_bloc_text_action);
    }
  }

// ----------------------------------------------------------------------
// ----------------------------------------------------------------------
static void AlignElements (HBDGR hbdgr, DWORD wTypeAlign)
  {
  LONG  sx1,sy1,sx2,sy2;

  if (CtxtgrGetModeEdition () == EN_SELECTION)
    {
    set_ctxtgr_charge_symb_en_cours (FALSE);
    set_ctxtgr_duplication_en_cours (FALSE);
    bdgr_boite_marques (hbdgr, &sx1, &sy1, &sx2, &sy2);
    bdgr_align_marques (hbdgr, MODE_MAJ_ECRAN, wTypeAlign, sx1, sy1, sx2, sy2);
    }
  }

// ----------------------------------------------------------------------
static void EspaceElements (HBDGR hbdgr, DWORD wTypeEspace, DWORD wTailleMax, DWORD wEspacement)
  {
  DWORD wErreur;

  if (CtxtgrGetModeEdition () == EN_SELECTION)
    {
    set_ctxtgr_charge_symb_en_cours (FALSE);
    set_ctxtgr_duplication_en_cours (FALSE);
    if ((wErreur = bdgr_espace_marques (hbdgr, MODE_MAJ_ECRAN, wTypeEspace,
                                        wTailleMax, (LONG)wEspacement)) != 0)
      { // depassement des limites MAX_X ou MAX_Y
      afficher_err (431);
      }
    }
  }

// ----------------------------------------------------------------------
void executer_action_immediate (t_bloc_action *action, char *texte_action, t_retour_action *retour_action)
  {
  t_param_action param_action;
  DWORD id_origine;
  DWORD id_fenetre;
  LONG  lx2,ly2;
  LONG  c1_debut_selection;
  LONG  l1_debut_selection;
  LONG  deplacement_deform_x;
  LONG  deplacement_deform_y;
  LONG  taille_boite_x;
  LONG  taille_boite_y;
  LONG  sx1,sy1,sx2,sy2;
  LONG  sx1bis,sy1bis,sx2bis,sy2bis;
  HBDGR hbdgr;
  DWORD id_trace;
  DWORD repere_dessin;
  char texte[c_NB_CAR_TEXTE_GR+1];
  DWORD n_element_texte;
  DWORD n_id_texte;
  G_COULEUR couleur;
  DWORD IndiceCouleur;
  DWORD police;
  DWORD page;
  DWORD type_anim_rl;
  DWORD style_police;
  DWORD mode_dessin;
  LONG coin_deformation;
  BOOL berreur;
  DWORD n_ligne_erreur;
  DWORD n_erreur;
  char mess[c_nb_car_message_err];
  char mess_ligne_erreur[c_nb_car_message_err];
  char pszNomBmp [c_NB_CAR_NOM_BMP_FOND + 1];

  hbdgr = get_ctxtgr_hBdGr ();
  switch (action->tipe)
    {
    case action_preambule:
			{
      // $$ Non implant� afficher_preambule ();
			}
      break;

    case action_charge_application_sat:
    case action_charge_application:
			{
      patienter (TRUE);
      ferme_toute_fenetre_gr (FALSE);
      bdgr_supprimer_blocs_marques ();
      sauver_action (); // le chargement efface la memoire donc les blocs actions
			n_erreur = charger_application (texte_action, !(action->on)); // action->on true si lancement en direct, false si /C
      if (n_erreur != 0) 
				{
        // $$ mis hors service car invisible : 
        // dlg_num_erreur (n_erreur);
				if (!action->on)   // lancement par pcsconf => on sort. sinon on continue sur applic vierge
					{
					initialiser_action_gr ();
					ajouter_action (action_quitte, &param_action);
					}
				else
					{
					restaurer_action ();
					}
				}
			else
				{
				restaurer_action ();
				}
			bLngLireInformation (c_inf_editeur_graphique, texte_action);
			StrConcat (texte_action, " - ");
			StrConcat (texte_action, pszNameDoc());
			afficher_titre_menu_gr (texte_action);
			bdgr_creer ();
			bdgr_init_proc_anm (); //lis_nombre_de_ligne_vdi, valide_ligne_vdi,suppression_possible_vdi, dwDlgAnimationGR,lis_couleurs2_vdi, lis_couleurs4_vdi);
			initialiser_ctxtgr_editeur ();
			set_ctxtgr_lancement_os (action->on);
			initialiser_menu_gr ();
			ouvre_toute_fenetre_visible ();
			patienter (FALSE);
			}
      break;

    case action_sauve_application:
    case action_sauve_application_sat:
			{
      patienter (TRUE);
      met_en_trace ();
      sauver_action ();
      bdgr_supprimer_blocs_marques ();
      n_erreur = sauver_application (texte_action, action->tipe == action_sauve_application_sat);
			if (n_erreur != 0)
				{
        dlg_num_erreur (n_erreur);
				}
			bLngLireInformation (c_inf_editeur_graphique, texte_action);
			StrConcat (texte_action, " - ");
			StrConcat (texte_action, pszNameDoc());
			afficher_titre_menu_gr (texte_action);
			restaurer_action ();
			bdgr_creer ();
			bdgr_init_proc_anm (); //lis_nombre_de_ligne_vdi, valide_ligne_vdi,suppression_possible_vdi, dwDlgAnimationGR,lis_couleurs2_vdi, lis_couleurs4_vdi);
			patienter (FALSE);
			}
      break;

    case action_export:
			{
      patienter (TRUE);
      VerifWarning (bExporteSynoptique (texte_action, FALSE));
      patienter (FALSE);
			}
      break;

    case action_import:
			{
      patienter (TRUE);
      berreur = FALSE;
			CSignatureFichier SignatureFichier(SIGNATURE_FORMAT_PCS_EN_COURS);
			SignatureFichier.DBPCSSetSignature();
      sauve_memoire (pszBackUpSyn);
      ferme_toute_fenetre_gr (TRUE);
			// Ignore les identifiant d'origine et en cree de nouveau
      n_erreur = dwImporteSynoptique (texte_action, &n_ligne_erreur, IMPORT_SYNOP_REMPLACEMENT, 0, FALSE); 
      if (n_erreur != 0)
        {
        berreur = TRUE;
        bLngLireErreur (n_erreur, mess);
        StrDWordToStr (mess_ligne_erreur, n_ligne_erreur);
        StrInsertStr (mess_ligne_erreur, " (", 0);
        StrConcatChar (mess_ligne_erreur, ')');
        StrConcat (mess, mess_ligne_erreur);
        dlg_erreur (mess);
        }
      if (berreur)
        {
        charge_memoire (pszBackUpSyn);
        }
      uFicEfface (pszBackUpSyn);
      initialiser_ctxtgr_editeur ();
      ouvre_toute_fenetre_visible ();
      patienter (FALSE);
			}
      break;

    case action_charge_symb:
			{
      patienter (TRUE);
      switch (CtxtgrGetModeEdition ())
        {
        case EN_SELECTION:
          reset_marque_epure_partout ();
          break;

        default: // passage en selection
          met_en_selection ();
          break;
        }
      set_ctxtgr_charge_symb_en_cours (TRUE);
      set_ctxtgr_nom_symbole (texte_action);
      patienter (FALSE);
			}
      break;

    case action_sauve_symb:
			{
      patienter (TRUE);
      set_ctxtgr_charge_symb_en_cours (FALSE);
      set_ctxtgr_duplication_en_cours (FALSE);
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        bExporteSymbole (texte_action, bdgr_lire_page (hbdgr), FALSE);
        }
      else
        {
        dlg_num_erreur (416);
        }
      patienter (FALSE);
			}
      break;

    case action_lire_macro:
			{
      mode_lecture_action = MODE_MACRO;
			}
      break;

    case action_lire_interactif:
			{
      mode_lecture_action = MODE_INTERACTIF;
			}
      break;

    case action_ouvre_fenetre:
			{
      if (!existe_ctxtgr_page (action->page, &id_fenetre))
        {
        ouvre_fenetre (action->page);
        }
      else
        {
        active_fenetre (id_fenetre);
        }
			}
      break;

    case action_mise_icone:
			{
      ferme_toute_fenetre_gr (FALSE);
			}
      break;

    case action_restore_dicone:
			{
      ouvre_toute_fenetre_visible ();
			}
      break;

    case action_change_pos_fenetre:
			{
      id_origine = get_ctxtgr_id_fenetre ();
      selectionner_ctxtgr_id_fenetre (action->id_fen);
      hbdgr = get_ctxtgr_hBdGr ();
      bdgr_coord_page (hbdgr, action->x1, action->y1, action->x2, action->y2);
      selectionner_ctxtgr_id_fenetre (id_origine);
			}
      break;

    case action_change_origine:
			{
      id_origine = get_ctxtgr_id_fenetre ();
      selectionner_ctxtgr_id_fenetre (action->id_fen);
      hbdgr = get_ctxtgr_hBdGr ();
      bdgr_origine_page (hbdgr, action->x1, action->y1);
      selectionner_ctxtgr_id_fenetre (id_origine);
			}
      break;

    case action_change_style_fenetre:
			{
      page = bdgr_lire_page (hbdgr);
      bdgr_couleur_fond_page (hbdgr, action->couleur);
      if (action->bBitmapFond)
				bdgr_bmp_fond_page (hbdgr, TRUE, action->szPath);
			else
	      bdgr_bmp_fond_page (hbdgr, FALSE, "");

      ferme_fenetre (TRUE);
      bdgr_styles_page  (page, action->bFenModale, action->bFenDialog, action->mode_aff, action->facteur_zoom, action->mode_deform,
                         action->un_bord, action->un_titre, texte_action,
                         action->un_sysmenu, action->un_minimize, action->un_maximize,
                         action->une_barreh, action->une_barrev,
                         action->grille_x, action->grille_y);
      ouvre_fenetre (page);
			}
      break;

    case action_ferme_fenetre:
			{
      set_ctxtgr_charge_symb_en_cours (FALSE);
      set_ctxtgr_duplication_en_cours (FALSE);
      ferme_fenetre (TRUE);
			}
      break;

    case action_supprime_fenetre:
			{
      if (bdgr_supprimer_page (hbdgr))
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
				ferme_fenetre (TRUE);
        }
      else
        {
        dlg_num_erreur (432);
        }
			}
      break;

    case action_active_fenetre:
			{
      if (existe_ctxtgr_fenetre ())
        {
        active_fenetre (action->id_fen); //Desynchronisation WindowProc
        }
			}
      break;

    case action_redessine_paint:
			{
      id_origine = get_ctxtgr_id_fenetre ();
      selectionner_ctxtgr_id_fenetre (action->id_fen);
      hbdgr = get_ctxtgr_hBdGr ();
			// $$ r�affiche le titre � chaque paint (� optimiser)
      afficher_titre_fenetre_gr (get_ctxtgr_id_fenetre ());
      //if (bdgr_dessiner_bmp_fond_page (hbdgr))
//        mode_dessin = 0;
//      else
        mode_dessin = MODE_DESSIN_COULEUR_FOND;
      mode_dessin |= MODE_DESSIN_ELEMENT | MODE_DESSIN_SELECTION | MODE_DESSIN_DEFORMATION;
      if (CtxtgrGetModeEdition () != EN_TEST)
        {
        mode_dessin = mode_dessin | MODE_DESSIN_GRILLE;
        }
      bdgr_dessiner_page (hbdgr, mode_dessin);
      selectionner_ctxtgr_id_fenetre (id_origine);
			}
      break;

    case action_quitte:
      break;

    case action_abandon:
			{
      set_ctxtgr_charge_symb_en_cours (FALSE);
      set_ctxtgr_duplication_en_cours (FALSE);
      param_action.id_fen = action->id_fen;
      inserer_action (action_redessine_paint, &param_action, 1);
			}
      break;

    case action_mode_trace:
			{
      switch (get_ctxtgr_mode_trace ())
        {
        case TRACE_PLEIN:
          set_ctxtgr_mode_trace (TRACE_VIDE);
          break;
        case TRACE_VIDE:
          set_ctxtgr_mode_trace (TRACE_PLEIN);
          break;
        default:
          break;
        }
			}
      break;

    case action_mode_dessin:
			{
      switch (action->param_fonction)
        {
        case EN_TRACE:
          met_en_trace ();
          break;
        case EN_SELECTION:
          met_en_selection ();
          break;
        default:
          break;
        }
			}
      break;

    case action_animation_rl:
			{
      set_ctxtgr_anim_rl (action->param_fonction);
			}
      break;

    case action_mode_test:
			{
      ferme_toute_fenetre_gr (FALSE);
      CtxtgrSetModeEdition (EN_TEST);
      ouvre_toute_fenetre_visible ();
			}
      break;

    case action_anim_couleur2:
			{
      if (CtxtgrGetModeEdition () == EN_TEST)
        {
        IndiceCouleur = get_ctxtgr_anim_indice_couleur2 ();
        bdgr_animer_couleurs2_page (hbdgr, IndiceCouleur);
        set_ctxtgr_anim_indice_couleur2 (IndiceCouleur+1);
        }
			}
      break;

    case action_anim_couleur4:
			{
      if (CtxtgrGetModeEdition () == EN_TEST)
        {
        IndiceCouleur = get_ctxtgr_anim_indice_couleur4 ();
        bdgr_animer_couleurs4_page (hbdgr, IndiceCouleur);
        set_ctxtgr_anim_indice_couleur4 (IndiceCouleur+1);
        }
			}
      break;

    case action_change_color_dessin:
			{
      set_ctxtgr_couleur_trace (action->couleur);
      couleur  = action->couleur;
      switch (CtxtgrGetModeEdition ())
        {
        case EN_TRACE:
          initialise_epure (hbdgr);
          break;
        case EN_SELECTION:
          bdgr_couleur_marques (hbdgr, MODE_MAJ_ECRAN, couleur);
          break;
        default:
          break;
        }
      dessine_menu_couleur ();
			}
      break;

    case action_change_color_fond_user:
			{
      set_ctxtgr_couleur_fond (action->couleur);
      couleur  = get_ctxtgr_couleur_trace ();
      switch (CtxtgrGetModeEdition ())
        {
        case EN_TRACE:
          initialise_epure (hbdgr);
          break;
        case EN_SELECTION:
          bdgr_couleur_marques (hbdgr, MODE_MAJ_ECRAN, couleur);
          break;
        default:
          break;
        }
      dessine_menu_couleur ();
			}
      break;

    case action_change_color_fond_page:
			{
      // peut on lire le nom du bitmap de fond de page ?
      if (bdgr_lire_nom_bmp_fond_page (hbdgr, pszNomBmp))
        {
				// oui => lib�re l'ancien bitmap
        bdgr_supprimer_bmp_fond_page (hbdgr);
				// Bitmap vide dans la page
        bdgr_bmp_fond_page (hbdgr, FALSE, "");
				}

			// Change la couleur de fond de la page
      bdgr_couleur_fond_page (hbdgr, action->couleur);
			// Invalide la page;
      bdgr_invalide_page (hbdgr);

      set_ctxtgr_couleur_fond (action->couleur);
      if (CtxtgrGetModeEdition () == EN_TRACE)
        {
        initialise_epure (hbdgr);
        }
      dessine_menu_couleur ();
			}
      break;

    case action_change_bmp_fond_page:
			{
      bdgr_supprimer_bmp_fond_page (hbdgr);
      bdgr_bmp_fond_page (hbdgr, TRUE, texte_action);
      bdgr_charger_bmp_fond_page (hbdgr);
			// Invalide la page;
      bdgr_invalide_page (hbdgr);
			}
      break;

    case action_change_color:
			{
      // uniquement interactif
      param_action.couleur_gr = action->couleur;
      if (get_ctxtgr_mode_couleur () == COULEUR_FOND)
        {
        inserer_action (action_change_color_fond_user, &param_action, 1);
        }
      else
        {
        inserer_action (action_change_color_dessin, &param_action, 1);
        }
			}
      break;

    case action_change_mode_color:
			{
      set_ctxtgr_mode_couleur (action->fonction);
      animer_menu_couleur_gr (action_change_mode_color, 0);
			}
      break;

    case action_change_style_ligne:
			{
      set_ctxtgr_style_ligne (action->style);
      switch (CtxtgrGetModeEdition ())
        {
        case EN_TRACE:
          initialise_epure (hbdgr);
          break;
        case EN_SELECTION:
          bdgr_style_ligne_marques (hbdgr, MODE_MAJ_ECRAN, action->style);
          break;
        default:
          break;
        }
			}
      break;

    case action_change_police:
			{
      set_ctxtgr_police (action->police);
      set_ctxtgr_style_police (action->style);
			set_ctxtgr_taille_texte (action->taille);

      switch (CtxtgrGetModeEdition ())
        {
        case EN_TRACE:
          initialise_epure (hbdgr);
          break;
        case EN_SELECTION:
          bdgr_style_texte_marques (hbdgr, MODE_MAJ_ECRAN, action->police, action->style, action->taille);
          break;
        default:
          break;
        }
			}
      break;

    case action_change_style_remplissage:
			{
      set_ctxtgr_style_remplissage ((G_STYLE_REMPLISSAGE)action->style);
      switch (CtxtgrGetModeEdition ())
        {
        case EN_TRACE:
          initialise_epure (hbdgr);
          break;
        case EN_SELECTION:
          bdgr_style_remplissage_marques (hbdgr, MODE_MAJ_ECRAN, (G_STYLE_REMPLISSAGE)action->style);
          break;
        default:
          break;
        }
			}
      break;

    case action_change_shape:
			{
      if (CtxtgrGetModeEdition () != EN_TRACE)
        {
        met_en_trace ();
        }
      CtxtgrSetAction (action->fonction);
      switch (action->fonction)
        {
        case G_ACTION_EDIT_TEXTE:
          set_ctxtgr_draw_texte ("T");
          break;
        case G_ACTION_STATIC_TEXTE:
          set_ctxtgr_draw_texte ("Abc");
          break;
        case G_ACTION_STATIC_NUM:
          set_ctxtgr_draw_texte ("710");
          break;
        case G_ACTION_EDIT_NUM:
          set_ctxtgr_draw_texte ("0");
          break;
        case G_ACTION_H_TEXTE:
        case G_ACTION_V_TEXTE:
          set_ctxtgr_draw_texte ("");
          break;
        case G_ACTION_CONT_GROUP   :
          set_ctxtgr_draw_texte ("[Group]");
          break;
        case G_ACTION_CONT_BOUTON  :
          set_ctxtgr_draw_texte ("[Button]");
          break;
        case G_ACTION_CONT_BOUTON_VAL:
          set_ctxtgr_draw_texte ("[Valid Button]");
          break;
        case G_ACTION_CONT_CHECK   :
          set_ctxtgr_draw_texte ("[Check]");
          break;
        case G_ACTION_CONT_RADIO   :
          set_ctxtgr_draw_texte ("[Radio]");
          break;
        case G_ACTION_CONT_STATIC:
        case G_ACTION_CONT_EDIT:
        case G_ACTION_CONT_COMBO:
        case G_ACTION_CONT_LIST:
        case G_ACTION_CONT_SCROLL_H:
        case G_ACTION_CONT_SCROLL_V:
          set_ctxtgr_draw_texte ("");
          break;
        }
      initialise_epure (hbdgr);
			}
      break;

    case action_titre_page:
			{
      bdgr_titre_page (hbdgr, texte_action);
      afficher_titre_fenetre_gr (get_ctxtgr_id_fenetre ());
			}
      break;

		case action_deplace_selection:
			{
			if (CtxtgrGetModeEdition () == EN_SELECTION)
				{
        if ((!get_ctxtgr_duplication_en_cours ()) &&
					(!get_ctxtgr_charge_symb_en_cours ()) &&
					(!get_ctxtgr_selection_en_cours ()) &&
					(!get_ctxtgr_deplacement_en_cours ()) &&
					(!get_ctxtgr_deformation_en_cours ()))
					{
					bdgr_translater_marques (hbdgr, MODE_MAJ_ECRAN, action->x1, action->y1);
					}
				}
			}
			break; // action_deplace_selection

    case action_goto_lc:
			{
      animer_coord_gr (action->x1, action->y1);
      switch (CtxtgrGetModeEdition ())
        {
        case EN_TRACE:
          dessiner_souris_fenetre_gr (SOURIS_CROIX);
          bdgr_modifier_point_epure (hbdgr, action->x1, action->y1);
          break;
        case EN_SELECTION:
          if ((get_ctxtgr_duplication_en_cours ()) || (get_ctxtgr_charge_symb_en_cours ()))
            {
            dessiner_souris_fenetre_gr (SOURIS_CIBLE);
            }
          else
            {
            dessiner_souris_fenetre_gr (SOURIS_FLECHE);
            if (get_ctxtgr_selection_en_cours ())
              {
              bdgr_modifier_point_epure (hbdgr, action->x1, action->y1);
              }
            else
              {
              if (get_ctxtgr_deplacement_en_cours ())
                {
                get_ctxtgr_pos_debut_translat (&sx1, &sy1);
                if ((sx1 != action->x1) || (sy1 != action->y1))
                  {
                  if (get_ctxtgr_deplacement_contour ())
                    {
                    bdgr_translater_marques (hbdgr, MODE_MAJ_DEFORMATION|MODE_MAJ_SELECTION, 
											action->x1 - sx1, action->y1 - sy1);
                    }
                  else
                    {
                    bdgr_translater_marques (hbdgr, MODE_MAJ_ECRAN, action->x1 - sx1, action->y1 - sy1);
                    }
                  set_ctxtgr_pos_debut_translat (action->x1, action->y1);
                  }
                }
              else
                {
                if (get_ctxtgr_deformation_en_cours ())
                  {
                  get_ctxtgr_pos_debut_translat (&sx1, &sy1);
                  if ((sx1 != action->x1) || (sy1 != action->y1))
                    {
                    get_ctxtgr_boite_init_deform (&sx1bis, &sy1bis, &sx2bis, &sy2bis);
                    taille_boite_x = sx2bis - sx1bis;
                    taille_boite_y = sy2bis - sy1bis;
                    deplacement_deform_x = (action->x1 - sx1);
                    deplacement_deform_y = (action->y1 - sy1);
                    bdgr_deformer_marques (hbdgr, MODE_MAJ_ECRAN,
                                           sx1bis, sy1bis,
                                           (taille_boite_x + deplacement_deform_x), taille_boite_x,
                                           (taille_boite_y + deplacement_deform_y), taille_boite_y);
                    }
                  }
                }
              }
            }
          break;
        } // switch (CtxtgrGetModeEdition ())
			}
      break; // case action_goto_lc:

    case action_validation_dessin:  // attention pas de test mode_dessin et lock
			{
      repere_dessin = bdgr_valider_epure (hbdgr);
      initialise_epure (hbdgr);
      set_ctxtgr_dessin_en_cours (FALSE);
			}
      break;

    case action_validation_texte:  // attention pas de test mode_dessin et lock
			{
      if (action->on)
        {
        n_element_texte = get_ctxtgr_n_elt_texte ();
        if (n_element_texte != 0)
          {
          bdgr_texte (hbdgr, MODE_MAJ_ECRAN, n_element_texte, get_ctxtgr_n_id_texte (), texte_action);
          }
        else
          {
          bdgr_ajouter_point_epure (hbdgr , action->x1, action->y1);
          lx2 = 0; // $$ largeur du texte � null => texte normal + lx2;
          ly2 = bdgr_taille_police_courante (hbdgr, get_ctxtgr_taille_texte());
          bdgr_ajouter_point_epure (hbdgr, lx2, ly2);
          bdgr_texte_epure (hbdgr, texte_action);
          repere_dessin = bdgr_valider_epure (hbdgr);
          }
        }
      initialise_epure (hbdgr);
      set_ctxtgr_dessin_en_cours (FALSE);
			}
      break;

    case action_click2:
			{
      switch (CtxtgrGetModeEdition ())
        {
        case EN_TRACE:
          if (get_ctxtgr_dessin_en_cours ())
            {
            if (get_ctxtgr_id_fenetre () == action->id_fen)
              {
              id_trace = CtxtgrGetAction();
              if ((id_trace == G_ACTION_POLYGONE) || (id_trace == G_ACTION_POLYGONE_PLEIN))
                {
                inserer_action (action_validation_dessin, &param_action, 1);
                relacher_souris ();
                }
              }
            }
          else
            {
            met_en_selection ();
            }
          break;

        case EN_SELECTION:
          met_en_trace ();
          break;

        default:
          break;
        }
			}
      break; // action_click2

    case action_click_up:
			{
      if (get_ctxtgr_id_fenetre () == action->id_fen)
        {
        switch (CtxtgrGetModeEdition ())
          {
          case EN_TRACE:
            if (get_ctxtgr_dessin_en_cours ())
              {
              id_trace = CtxtgrGetAction();
              switch (id_trace)
                {
                case G_ACTION_H_TEXTE:
                case G_ACTION_V_TEXTE:
                case G_ACTION_POLYGONE:
                case G_ACTION_POLYGONE_PLEIN:
                  // ne rien faire
                  break;

                default:
                  inserer_action (action_validation_dessin, &param_action, 1);
                  relacher_souris ();
                  break;
                }
              }
            break;

          case EN_SELECTION:
						// fin d'appui en cours de s�lection
            get_ctxtgr_pos_debut_selection (&c1_debut_selection, &l1_debut_selection);
            if (get_ctxtgr_selection_en_cours ())
              {
              bdgr_abandonner_epure (hbdgr);
              set_ctxtgr_selection_en_cours (FALSE);
              if ((c1_debut_selection != action->x1) || (l1_debut_selection != action->y1))
                {
                if (!Touche_CTRL_Enfoncee (action->param_fonction))
                  {
                  bdgr_demarquer_page (hbdgr, MODE_MAJ_ECRAN);
                  }
                bdgr_marquer_rectangle (hbdgr, c1_debut_selection, l1_debut_selection,
                                        action->x1, action->y1, MODE_MAJ_ECRAN);
                }
              else  // meme point au d�but et en fin de s�lection
                {
                if (Touche_CTRL_Enfoncee (action->param_fonction))
                  {
                  if (!bdgr_marquer_point (hbdgr, c1_debut_selection, l1_debut_selection, MODE_MAJ_ECRAN))
                    {
                    // deja marqu� => on demarque
                    bdgr_demarquer_point (hbdgr, c1_debut_selection, l1_debut_selection, MODE_MAJ_ECRAN);
                    }
                  }
                else
                  {
                  bdgr_demarquer_page (hbdgr, MODE_MAJ_ECRAN);
                  bdgr_marquer_point (hbdgr, c1_debut_selection, l1_debut_selection, MODE_MAJ_ECRAN);
                  }
                }
              }
            else
              {
              if (get_ctxtgr_deplacement_en_cours ())
                {
								// fin de d�placement de la s�lection
                set_ctxtgr_deplacement_en_cours (FALSE);
                if (get_ctxtgr_deplacement_contour ())
                  {
                  set_ctxtgr_deplacement_contour (FALSE);
                  }
                }
              else
                {
                set_ctxtgr_deformation_en_cours (FALSE);
                }
              }
            relacher_souris ();
            break; // EN_SELECTION

          default:
            break;
          }
        }
			}
      break; // action_click_up

    case action_click: // Appui bouton gauche de la souris
			{
			// contexte ad�quat ?
      if (get_ctxtgr_id_fenetre () == action->id_fen)
        {
				// oui=> Mode de fonctionnement de l'�diteur
        switch (CtxtgrGetModeEdition ())
          {
          case EN_TRACE:
						{
            if (get_ctxtgr_dessin_en_cours ())
              {
              switch (CtxtgrGetAction())
                {
                case G_ACTION_H_TEXTE            :
                case G_ACTION_V_TEXTE:
                  // ne rien faire
                  break;

                case G_ACTION_POLYGONE     :
                case G_ACTION_POLYGONE_PLEIN:
                  bdgr_ajouter_point_epure (hbdgr , action->x1, action->y1);
                  break;

                default:
                  // cas unique en mode auto
                  // en mode manu c'est click_up qui valide
                  inserer_action (action_validation_dessin, &param_action, 1);
                  break;
                }
              } // if (get_ctxtgr_dessin_en_cours ())
            else
              {
							// D�marre la saisie d'une nouvelle action graphique
              set_ctxtgr_dessin_en_cours (TRUE);
              switch (CtxtgrGetAction())
                {
                case G_ACTION_H_TEXTE:
                case G_ACTION_V_TEXTE:
									{
									DWORD taille_police;

                  // pas en mode auto sinon souk
									// regarde si l'utilisateur a cliqu� sur un �l�ment graphique texte existant
                  n_element_texte = bdgr_pointer_texte_element (hbdgr, action->x1, action->y1,
										&n_id_texte, &police, &style_police, texte, &sx1, &sy1, &sx2, &sy2);

									taille_police = sy2; //$$sy2 - sy1;
                  set_ctxtgr_n_elt_texte (n_element_texte);
                  set_ctxtgr_n_id_texte (n_id_texte);
                  if (n_element_texte == 0)
                    {
                    StrSetNull (texte);
                    sx1 = action->x1;
                    sy1 = action->y1;
                    police = get_ctxtgr_police ();
										// Conversion
										taille_police = bdgr_taille_police_courante (hbdgr, get_ctxtgr_taille_texte());
                    style_police = get_ctxtgr_style_police ();
                    }
                  ouvrir_fenetre_saisie_gr (get_ctxtgr_id_fenetre (),
                                            get_ctxtgr_couleur_fond (), get_ctxtgr_couleur_trace (),
                                            police, style_police, taille_police, sx1, sy1, texte);
									}
                  break;

								default:
                  bdgr_ajouter_point_epure (hbdgr, action->x1, action->y1);
                  bdgr_ajouter_point_epure (hbdgr, action->x1, action->y1);
                  capturer_souris_fenetre_gr (get_ctxtgr_id_fenetre ());
                  break;
                } // switch (CtxtgrGetAction())
              } // else  (get_ctxtgr_dessin_en_cours ())
						}
            break; // EN_TRACE

          case EN_SELECTION:
						{
            if (get_ctxtgr_duplication_en_cours ())
              {
              if (get_ctxtgr_page_origine_dup () != bdgr_lire_page (hbdgr))
                {
                bdgr_demarquer_page (hbdgr, MODE_MAJ_ECRAN);
                }
              n_erreur = bdgr_dupliquer_marques (hbdgr, MODE_GOMME_SELECTION|MODE_GOMME_DEFORMATION, get_ctxtgr_page_origine_dup (), TRUE);
              bdgr_boite_marques (hbdgr, &sx1, &sy1, &sx2, &sy2);
              if ((sx1 != action->x1) || (sy1 != action->y1))
                {
                // $$ v�rifier bdgr_translater_marques (hbdgr, MODE_VIRTUEL, action->x1 - sx1, action->y1 - sy1);
                bdgr_translater_marques (hbdgr, MODE_GOMME_SELECTION, action->x1 - sx1, action->y1 - sy1);
                }
              bdgr_dessiner_page (hbdgr, (MODE_DESSIN_SELECTION|MODE_DESSIN_DEFORMATION|MODE_DESSIN_ELTS_MARQUES));
              set_ctxtgr_duplication_en_cours (FALSE);
              if (n_erreur != 0)
                {
                dlg_num_erreur (n_erreur);
                }
              } // if (get_ctxtgr_duplication_en_cours ())
            else
              {
              if (get_ctxtgr_charge_symb_en_cours ())
                {
                n_erreur = dwImporteSymbole (get_ctxtgr_nom_symbole (), hbdgr, &n_ligne_erreur, FALSE);
                if (n_erreur == 0)
                  {
                  bdgr_boite_marques (hbdgr, &sx1, &sy1, &sx2, &sy2);
                  if ((sx1 != action->x1) || (sy1 != action->y1))
                    {
                    bdgr_translater_marques (hbdgr, MODE_VIRTUEL, action->x1 - sx1, action->y1 - sy1);
                    }
                  bdgr_dessiner_page (hbdgr, (MODE_DESSIN_SELECTION|MODE_DESSIN_DEFORMATION|MODE_DESSIN_ELTS_MARQUES));
                  }
                else
                  {
                  bLngLireErreur (n_erreur, mess);
                  StrDWordToStr (mess_ligne_erreur, n_ligne_erreur);
                  StrInsertStr (mess_ligne_erreur, " (", 0);
                  StrConcatChar (mess_ligne_erreur, ')');
                  StrConcat (mess, mess_ligne_erreur);
                  dlg_erreur (mess);
                  }
                set_ctxtgr_charge_symb_en_cours (FALSE);
                } // if (get_ctxtgr_charge_symb_en_cours ())
              else
                {
								// S�lection
                set_ctxtgr_pos_debut_selection (action->x1, action->y1);
                if (bdgr_pointer_marques (hbdgr, action->x1, action->y1, &coin_deformation))
                  {
                  if (Touche_CTRL_Enfoncee (action->param_fonction))
                    {
                    set_ctxtgr_deplacement_contour (FALSE);
                    }
                  else
                    {
										bdgr_gommer_elements_marques (hbdgr);
                    //bdgr_dessiner_page (hbdgr, MODE_GOMME_ELTS_MARQUES);
                    set_ctxtgr_deplacement_contour (TRUE);
                    }
                  set_ctxtgr_deplacement_en_cours (TRUE);
                  set_ctxtgr_pos_debut_translat (action->x1, action->y1);
                  }
                else
                  {
									// pas de marques
                  if (coin_deformation == c_PAS_COIN)
                    {
                    set_ctxtgr_selection_en_cours (TRUE);
                    initialise_epure_rect_selection (hbdgr);
                    bdgr_ajouter_point_epure (hbdgr , action->x1, action->y1);
                    bdgr_ajouter_point_epure (hbdgr , action->x1, action->y1);
                    }
                  else
                    {
                    set_ctxtgr_deformation_en_cours (TRUE);
                    set_ctxtgr_pos_debut_translat (action->x1, action->y1);
                    bdgr_boite_marques (hbdgr, &sx1, &sy1, &sx2, &sy2);
                    bdgr_debut_deformation_marques (hbdgr);
                    switch (coin_deformation)
                      {
                      case c_COIN_BG:
                        set_ctxtgr_boite_init_deform (sx2, sy2, sx1, sy1);
                        break;
                      case c_COIN_HG:
                        set_ctxtgr_boite_init_deform (sx2, sy1, sx1, sy2);
                        break;
                      case c_COIN_BD:
                        set_ctxtgr_boite_init_deform (sx1, sy2, sx2, sy1);
                        break;
                      case c_COIN_HD:
                        set_ctxtgr_boite_init_deform (sx1, sy1, sx2, sy2);
                        break;
                      default:
                        break;
                      }
                    }
                  }
                }
              } // else (get_ctxtgr_duplication_en_cours ())
            capturer_souris_fenetre_gr (get_ctxtgr_id_fenetre());
						}
            break; // EN_SELECTION

          default:
						VerifWarningExit;
            break;
          }
        }
			}
      break; // action_click

    case action_double_click:
			{
      if (get_ctxtgr_id_fenetre () == action->id_fen)
        {
        switch (CtxtgrGetModeEdition ())
          {
          case EN_SELECTION:
            type_anim_rl = get_ctxtgr_anim_rl ();
            n_erreur = bdgr_param_animation_marques (hbdgr, (ANIMATION)type_anim_rl);
            if (n_erreur != 0)
              {
              dlg_num_erreur (n_erreur);
              met_en_selection ();
              }
            break;
          default:
            Sonne (20,100);
            break;
          }
        }
			}
      break;


		case action_change_identifiant:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
				DWORD dwNErr = bdgr_change_identifiant_marques (hbdgr);
				if (dwNErr != 0)
					{
          dlg_num_erreur (dwNErr);
					}
				}
			}
			break;

		case action_passage_devant:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
        bdgr_change_profondeur_marques (hbdgr, TRUE, MODE_MAJ_ECRAN);
        }
			}
      break;

		case action_passage_derriere:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
        bdgr_change_profondeur_marques (hbdgr, FALSE, MODE_MAJ_ECRAN);
        }
			}
      break;

    case action_group:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
        bdgr_grouper_marques (hbdgr, MODE_MAJ_ECRAN);
        }
			}
      break;

    case action_degroup:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
        if (!bdgr_degrouper_marques (hbdgr, MODE_MAJ_ECRAN))
          {
          dlg_num_erreur (442);
          }
        }
			}
      break;

    case action_duplicate:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        if (bdgr_nombre_marques_page  (hbdgr) != 0)
          {
          set_ctxtgr_duplication_en_cours (TRUE);
          set_ctxtgr_page_origine_dup (bdgr_lire_page (hbdgr));
          }
        else
          {
          set_ctxtgr_duplication_en_cours (FALSE);
          }
        }
			}
      break;

    case action_tout_selectionner:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
				bdgr_demarquer_page (hbdgr, MODE_MAJ_ECRAN);
				bdgr_marquer_page (hbdgr, MODE_MAJ_ECRAN);
				}
			}
			break;

    case action_selectionner_animes:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
				bdgr_demarquer_page (hbdgr, MODE_MAJ_ECRAN);
				bdgr_marquer_page_elts_animes (hbdgr, MODE_MAJ_ECRAN);
				}
			}
			break;

    case action_deselectionner_animes:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
				bdgr_demarquer_page_elts_animes (hbdgr, MODE_MAJ_ECRAN);
				}
			}
			break;

    case action_selectionner_non_animes:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
				bdgr_demarquer_page (hbdgr, MODE_MAJ_ECRAN);
				bdgr_marquer_page_elts_non_animes (hbdgr, MODE_MAJ_ECRAN);
				}
			}
			break;

    case action_deselectionner_non_animes:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
				bdgr_demarquer_page_elts_non_animes (hbdgr, MODE_MAJ_ECRAN);
				}
			}
			break;

    case action_selectionner_elt_hors_page:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
				bdgr_demarquer_page (hbdgr, MODE_MAJ_ECRAN);
				bdgr_marquer_page_elt_hors_page (hbdgr, MODE_MAJ_ECRAN);
				}
			}
			break;


    case action_change_remplissage:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
        couleur  = get_ctxtgr_couleur_trace ();

        // $$ plus de for�age de la couleur bdgr_couleur_marques (hbdgr, MODE_MAJ_ECRAN, couleur);
        bdgr_change_remplissage_marques (hbdgr, MODE_MAJ_ECRAN);
        }
			}
      break;

    case action_align_vc :
      AlignElements (hbdgr, c_ALIGN_VC);
      break;
    case action_align_hc :
      AlignElements (hbdgr, c_ALIGN_HC);
      break;
    case action_align_d :
      AlignElements (hbdgr, c_ALIGN_D);
      break;
    case action_align_b :
      AlignElements (hbdgr, c_ALIGN_B);
      break;
    case action_align_g :
      AlignElements (hbdgr, c_ALIGN_G);
      break;
    case action_align_h :
      AlignElements (hbdgr, c_ALIGN_H);
      break;
    case action_espace_h :
      EspaceElements (hbdgr, c_ESPACE_H, c_G_MAX_X, wEspaceH);
      break;
    case action_espace_v :
      EspaceElements (hbdgr, c_ESPACE_V, c_G_MAX_Y, wEspaceV);
      break;

    case action_symetrie_v:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
        bdgr_boite_marques (hbdgr, &sx1, &sy1, &sx2, &sy2);
        sx1 = (sx1 + sx2) / 2;
        sy1 = (sy1 + sy2) / 2;
        bdgr_symetrie_marques (hbdgr, MODE_MAJ_ECRAN, c_SYMETRIE_VERTICALE, sx1, sy1);
        }
			}
      break;

    case action_symetrie_h:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
        bdgr_boite_marques (hbdgr, &sx1, &sy1, &sx2, &sy2);
        sx1 = (sx1 + sx2) / 2;
        sy1 = (sy1 + sy2) / 2;
        bdgr_symetrie_marques (hbdgr, MODE_MAJ_ECRAN, c_SYMETRIE_HORIZONTALE, sx1, sy1);
        }
			}
      break;

    case action_rotation:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        set_ctxtgr_charge_symb_en_cours (FALSE);
        set_ctxtgr_duplication_en_cours (FALSE);
        bdgr_boite_marques (hbdgr, &sx1, &sy1, &sx2, &sy2);
        sx1 = (sx1 + sx2 + 1) / 2;
        sy1 = (sy1 + sy2 + 1) / 2;
        bdgr_tourner_marques  (hbdgr, MODE_MAJ_ECRAN, sx1, sy1, 1);
        }
			}
      break;

    case action_suppression:
			{
      if (CtxtgrGetModeEdition () == EN_SELECTION)
        {
        if (bdgr_supprimer_marques (hbdgr, MODE_MAJ_ECRAN))
          {
          set_ctxtgr_charge_symb_en_cours (FALSE);
          set_ctxtgr_duplication_en_cours (FALSE);
          }
        else
          {
          dlg_num_erreur (432);
          }
        }
			}
      break;
    } // switch 
  retour_action->mode_lecture = mode_lecture_action;
  }

// ---------------------------------------------------------------------
BOOL y_a_des_actions (void)
  {
  BOOL bRes = FALSE;

  if (existe_repere (B_ACTION))
    {
    bRes = (nb_enregistrements (szVERIFSource, __LINE__, B_ACTION) != 0);
    }
  return bRes;
  }
// ----------------------------------------------------------------------
int executer_action_bufferise (t_retour_action *retour_action)
  {
  int rretour;
  t_bloc_action *pAction;
  t_bloc_action action;
  char *ptr_texte;
  char texte[L_MAX_TEXTE_ACTION] = "";

  rretour = action_rien;
  while (y_a_des_actions ())
    {
    pAction = (t_bloc_action *)pointe_enr (szVERIFSource, __LINE__, B_ACTION, 1);
    action = (*pAction);
    if (nb_enregistrements (szVERIFSource, __LINE__, B_TXT_ACTION) != 0)
      {
      ptr_texte = (char *)pointe_enr (szVERIFSource, __LINE__, B_TXT_ACTION, 1);
      StrCopy (texte, ptr_texte);
      }
    rretour = action.tipe;
    supprimer_action ();  // la sup est indispensable ici pour les insere eventuels
    executer_action_immediate (&action, texte, retour_action);
    } // while nbr_action 
  return (rretour);
  }

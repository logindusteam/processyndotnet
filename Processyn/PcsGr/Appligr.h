/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : interface action chargement application        		|
 |	       								|
 |   Auteur  : AC							|
 |   Date    : 27/10/92 						|
 |   Version :                                                          |
 +----------------------------------------------------------------------*/

void initialiser_ctxt_application (void);

int charger_application (char * pszNom, BOOL bApplicationSat);

int sauver_application (char *chaine, BOOL bApplicationSat);

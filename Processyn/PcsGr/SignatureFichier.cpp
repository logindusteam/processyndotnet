#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "USignale.h"
#include "mem.h"
#include "lng_res.h"
#include "Tipe.h"
#include "Verif.h"

#include "SignatureFichier.h"
VerifInit;


CSignatureFichier::CSignatureFichier(void)
{
	mszSignatureCourante[0] = 0;
}

CSignatureFichier::CSignatureFichier(PCSTR pszSignatureCourante)
{
	SetSignatureCourante(pszSignatureCourante);
}

CSignatureFichier::~CSignatureFichier(void)
{
}

void CSignatureFichier::SetSignatureCourante(PCSTR pszSignatureCourante)
{
	StrCopyUpToLength (mszSignatureCourante,pszSignatureCourante,MAX_PATH);
}
BOOL CSignatureFichier::bSignatureValide(PCSTR pszSignatureAVerifier)
{
	return bStrEgales (pszSignatureAVerifier, mszSignatureCourante);
}

BOOL CSignatureFichier::bDBPCSSignatureValide()
{
	BOOL bRet = FALSE;
	PCSTR pszSignature = (PCSTR)pPointeEnrSansErr (szVERIFSource, __LINE__, B_SIGNATURE_FICHIER, 1);
	if (pszSignature)
	{
		bRet = bSignatureValide(pszSignature);
	}
	return bRet;
}

void CSignatureFichier::DBPCSSetSignature()
{
	if (!existe_repere(B_SIGNATURE_FICHIER))
	{
		cree_bloc (B_SIGNATURE_FICHIER, sizeof (mszSignatureCourante), 1);
	}
	PSTR pszSignature = (PSTR)pointe_enr (szVERIFSource, __LINE__,B_SIGNATURE_FICHIER , 1);
	StrCopyUpToLength (pszSignature,mszSignatureCourante,MAX_PATH);
}

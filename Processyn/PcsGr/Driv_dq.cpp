//--------------------------------------------------------------------------
// Driv_dq.c	Gestion de fichiers pour scrut_dq (pcsexe)
//--------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"
#include "fman.h"
#include "fileman.h"
#include "UStr.h"

#include "Driv_DQ.h"

// $$ rendre r�entrant ou accessible en multit�che
// --------------------------------------------------------------------------
static HFIC ahfics [DQ_NB_FIC_MAX] = 
  {
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,

  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC,
  INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC, INVALID_HFIC
  };

//---------------------------------------------------------------------
// => V�rifie si un fichier est d�ja ouvert par ce module
// ne fonctionne dans tous les cas qu'avec des paths absolus
static HFIC hFichierOuvert (PCSTR pszFile)
  {
  DWORD   dwNFichier;
	HFIC		hficRes = INVALID_HFIC;

	// recherche un handle ouvert sur le m�me nom de fichier
	for (dwNFichier = 0; dwNFichier < DQ_NB_FIC_MAX; dwNFichier++)
		{
		HFIC	hf = ahfics [dwNFichier];

		if ((hf != INVALID_HFIC) && (StrICompare (pszFileNom (hf), pszFile) == 0))
			{
			hficRes = hf;
			break;
			}
		}
	return hficRes;
  }

// --------------------------------------------------------------------------
// recherche une valeur de handle dans le tableau global de handles
// renvoie TRUE si trouv� (*phfdq mis � jour)
static BOOL trouve_repere (HFIC hFichier, PHFDQ phfdq)
  {
  BOOL	bTrouve = FALSE;
  DWORD	PremierLibre = 0;

  while (PremierLibre < DQ_NB_FIC_MAX)
    {
    if (ahfics [PremierLibre] == hFichier)
			{
			bTrouve = TRUE;
			*phfdq = (HFDQ)PremierLibre;
			break;
			}
    PremierLibre++;
    }

  return bTrouve;
  }

//-----------------------------------------------------------------------
// r�cup�re le pointeur sur un des HFICS de ahfics ou NULL
static _inline PHFIC pficFromHfdq (HFDQ hfdq)
	{
	DWORD	dwNhfic = (DWORD) hfdq;
	PHFIC	pRes = NULL;

	if ((dwNhfic >= 0) && (dwNhfic < DQ_NB_FIC_MAX))
		pRes = &ahfics [dwNhfic];

	return pRes;
	}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
void init_driv_dq (void)
  {
  }

//-----------------------------------------------------------------------
void termine_driv_dq (void)
  {
  reinit_driv_dq_c ();
  }

//-----------------------------------------------------------------------
void reinit_driv_dq_c (void)
  {
	// Fermeture de tous les fichier ouverts
  for (DWORD wRepere = 0; wRepere < DQ_NB_FIC_MAX; wRepere++)
    {
		if (ahfics [wRepere] != INVALID_HFIC)
			uFileFerme (&ahfics [wRepere]);
    }
  }

//----------------------------------------------------------------------------
BOOL bFicPresent (PCSTR pszFile)
  {
  return CFman::bFAcces (pszFile, CFman::modeRead|CFman::shareDenyNone|CFman::modeNoTruncate);
  }

//----------------------------------------------------------------------------
BOOL bFicOuvert (PHFDQ phfdq, PCSTR nom_fich)
  {
  HFIC  hFichier = hFichierOuvert (nom_fich);
  BOOL  bOuvert = FALSE;

  if (hFichier != INVALID_HFIC)
    {
    bOuvert = TRUE;
    trouve_repere (hFichier, phfdq);
    }

  return bOuvert;
  }

//-----------------------------------------------------------------------
// Fichier binaire : ouverture ou r�cup�ration du handle
UINT uFicOuvre (PHFDQ phfdq, PCSTR nom_fich, DWORD dwTailleEnr)
  {
  HFIC  hFichier = hFichierOuvert (nom_fich);
  UINT  uRes = NO_ERROR;

	// fichier d�j� ouvert ?
  if (hFichier != INVALID_HFIC)
    {
		// oui => on renvoie le handle du fichier ouvert $$ SANS test taille enr
		if (!trouve_repere (hFichier, phfdq))
			uRes = ERROR_NO_MORE_SEARCH_HANDLES;
    }
  else
		{
		// non => ouvre le
		uRes = uFileOuvre (&hFichier, nom_fich, dwTailleEnr, OF_DRIV_DQ);
		if (uRes == NO_ERROR)
			{
			if (trouve_repere (INVALID_HFIC, phfdq))
				{
				PHFIC	phfic = pficFromHfdq (*phfdq);

				*phfic = hFichier;
				}
			else
				uRes = ERROR_NO_MORE_SEARCH_HANDLES;
			}
		}

  return uRes;
  } // uFicOuvre

//-----------------------------------------------------------------------
// Fichier texte : ouverture ou r�cup�ration du handle
UINT uFicTexteOuvre (PHFDQ phfdq, PCSTR nom_fich)
  {
	return uFicOuvre (phfdq, nom_fich, 1);
  }

//-----------------------------------------------------------------------
UINT uFicFerme (PHFDQ phfdq)
  {
	PHFIC	phfic = pficFromHfdq (*phfdq);
  UINT uRes = ERROR_INVALID_HANDLE;
	
	if (phfic)
		{
		uRes = uFileFerme (phfic);

		if (uRes == NO_ERROR)
			*phfdq = INVALID_HFDQ;
		}

  return uRes;
  }

//-----------------------------------------------------------------------
// Nb d'enregistrements dans un fichier
UINT uFicNbEnr (HFDQ hfdq, DWORD * pdwNbEnr)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT uRes = ERROR_INVALID_HANDLE;

  *pdwNbEnr = 0;
	if (phfic)
		uRes = uFileNbEnr (*phfic, pdwNbEnr);

	return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicPointeLecture (HFDQ hfdq, DWORD dwNEnr)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT uRes = ERROR_INVALID_HANDLE;
	
	if (phfic)
		uRes = uFilePointeLecture (*phfic, dwNEnr - 1);

	return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicPointeurLecture (HFDQ hfdq, DWORD * pdwNEnr)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT uRes = ERROR_INVALID_HANDLE;
	
	*pdwNEnr = 0;
	if (phfic)
		{
		uRes = uFilePointeurLecture (*phfic, pdwNEnr);

		if (uRes == NO_ERROR)
			(*pdwNEnr)++;
		}

  return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicPointeEcriture (HFDQ hfdq, DWORD dwNEnr)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT	uRes = ERROR_INVALID_HANDLE;
	
	if (phfic)
		uRes = uFilePointeEcriture (*phfic, dwNEnr - 1);

	return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicPointeurEcriture (HFDQ hfdq, DWORD * pdwNEnr)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT uRes = ERROR_INVALID_HANDLE;
	
	*pdwNEnr = 0;
	if (phfic)
		{
	  uRes = uFilePointeurEcriture (*phfic, pdwNEnr);

		if (uRes == NO_ERROR)
			(*pdwNEnr)++;
		}

  return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicLireTout (HFDQ hfdq, PVOID buffer, DWORD dwNbEnrALire)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT	uRes = ERROR_INVALID_HANDLE;
	
	if (phfic)
		uRes = uFileLireTout (*phfic, buffer, dwNbEnrALire);

  return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicLireDirect	(HFDQ hfdq, DWORD dwNEnr, PVOID buffer)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT uRes = ERROR_INVALID_HANDLE;
	
	if (phfic)
		{
		uRes = uFicPointeLecture (hfdq, dwNEnr);

		if (uRes == NO_ERROR)
			uRes = uFicLireTout (hfdq, buffer, 1);
		}

  return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicEcrireTout (HFDQ hfdq, const void * buffer, DWORD dwNbEnrAEcrire)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT uRes = ERROR_INVALID_HANDLE;
	
	if (phfic)
		{
		DWORD	wRecWritten = 0;

		uRes = uFileEcrire (*phfic, buffer, dwNbEnrAEcrire, &wRecWritten);
		if (uRes == NO_ERROR)
			{
			if (wRecWritten != dwNbEnrAEcrire)
				uRes = ERROR_HANDLE_DISK_FULL; // $$ anciennement 41;
			}
		}

  return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicEcrireDirect	(HFDQ hfdq, DWORD dwNEnr, PVOID buffer)
  {
	UINT	uRes = uFicPointeEcriture (hfdq, dwNEnr);

	if (uRes == NO_ERROR)
		uRes = uFicEcrireTout (hfdq, buffer, 1);

  return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicLireLn  (HFDQ hfdq, PSTR pszDest, UINT nTailleDest)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT uRes = ERROR_INVALID_HANDLE;
	
	if (phfic)
		uRes = uFileLireLn (*phfic, pszDest, nTailleDest);

  return uRes;
  }

//-----------------------------------------------------------------------
UINT uFicEcrireLn (HFDQ hfdq, PCSTR pszSource)
  {
	PHFIC	phfic = pficFromHfdq (hfdq);
  UINT	uRes = ERROR_INVALID_HANDLE;
	
	if (phfic)
		uRes = uFileEcrireSzLn (*phfic, pszSource);

  return uRes;
  }


//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
UINT uFicEfface (PCSTR nom_fich)
  {
  return uFileEfface (nom_fich);
  }

//----------------------------------------------------------------------------
UINT uFicCopie (PCSTR nom_source, PCSTR nom_destination, BOOL merge)
  {
	// $$ Attention utilitaire n'assurant pas la r�utilisation des fichiers ouvert
  return uFileCopie (nom_source, nom_destination, merge);
  }

//-------------------------------------------------------------------------
UINT uFicRenomme (PCSTR nom_source, PCSTR nom_destination)
  {
  return uFileRenomme (nom_source, nom_destination);
  }

// --------------------------------------------------------------------------
BOOL bFicNomValide (PCSTR pszNom)
  {
	// Ne peut pas dire si le nom est valide dans tous les cas (ex : COM20)
	// Renvoie vrai si au moins un caract�re
  return (pszNom && (pszNom[0]));
  }

//-------------------------------------------------------------------------
UINT uFicTaille (PCSTR nom_fich)
  {
	// $$ Risque d'erreur en dynamique : devrait fonctionner sur Handle
  UINT	uTaille = (UINT)-1;
	WIN32_FIND_DATA FindData;
	HANDLE hFindFile = FindFirstFile (nom_fich, &FindData);

	if (hFindFile != INVALID_HANDLE_VALUE)
		{
		if (FindData.dwFileAttributes & (FILE_ATTRIBUTE_SYSTEM | FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_ARCHIVE))
			uTaille = FindData.nFileSizeLow;
		FindClose (hFindFile);
		}

  return uTaille;
  }

//---------------------------------------------------------------------------
// r�cup�re la place libre sur un disque
UINT place_disque_libre (DWORD unite, __int64 * taille)
  {
  UINT erreur;
	char	szRacineUnite[] = "A:\\";
	PSTR	pszRacineUnite = szRacineUnite;
	DWORD	SectorsPerCluster;
  DWORD	BytesPerSector;
  DWORD	NumberOfFreeClusters;
  DWORD	TotalNumberOfClusters;

  *taille = 0;

	// Nom de la racine du disque � tester
	if (unite)
		szRacineUnite[0] += (char) (unite -1);
	else
		pszRacineUnite = NULL; // disque courant

	if (GetDiskFreeSpace (pszRacineUnite, &SectorsPerCluster,	&BytesPerSector,
    &NumberOfFreeClusters, &TotalNumberOfClusters))
		{
    *taille = ((__int64)SectorsPerCluster) * ((__int64)BytesPerSector) * ((__int64)NumberOfFreeClusters);
		erreur = 0 ;
		}
	else
		erreur = GetLastError();
  return erreur;
  }

//-----------------------------------------------------------------------
BOOL bFicValeurEnv (PCSTR var_env, PSTR valeur)
  {
  BOOL	bRes = FALSE;

  FileGetEnvVarValue (var_env, valeur);
  if (valeur [0] != '\0')
		bRes = TRUE;

  return bRes;
  }


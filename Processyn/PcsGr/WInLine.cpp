//-------------------------------------------------------------------------------
// WInLine.c
// Auteur : AC
// gestion sommaire d'un controle Edit utilis� dans les synoptiques � l'ex�cution
// version Win32
//-------------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "MemMan.h"
#include "UStr.h"
#include "UEnv.h"
#include "pcsfont.h"
#include "G_Objets.h"
#include "DevMan.h"
#include "ClipbMan.h"
#include "Verif.h"
#include "LireLng.h"
//
#include "WInLine.h"
VerifInit;

#define NB_CARS_NUMERIQUE_MAX 60

#define OFFSET_INLINE_X 4
#define OFFSET_INLINE_Y 2
#define LETTRE_INLINE_REF_X "W"
#define LETTRE_INLINE_REF_Y "j"
#define NB_CAR_MAX_INLINE 255
#define CARACTERE_INLINE_MASQUE '*'

static const char szNomClasse [] = "ClasseInline";

// param�tres de cr�ation d'une fen�tre InLine
typedef struct
	{
	G_COULEUR couleur_fond;
	G_COULEUR couleur_texte;
	DWORD NbCarsMax;
	DWORD police;
	DWORD style_police;
	DWORD taille_police;
	char * pszTexte;
	BOOL	bCadre;
	BOOL	bMasque;
	BOOL	bRazSurFocus;
	BOOL	bFocusEnVideoInverse; // En vid�o inverse lorsqu'on a le Focus
	BOOL	bNumScientifique;
	} PARAM_INLINE;

// Environnement d'une fen�tre InLine
typedef struct
  {
	PARAM_INLINE Il;
  HWND			hwnd;
  HDEV			hdevice;
	int				cxFen;

  G_COULEUR couleur_texte_inv;
  G_COULEUR	couleur_fond_inv;

	BOOL			bALeFocus;
	POINT			pointCaret;					// position du Caret

  int				nNCarCaret;				//dessin curseur derriere le caractere point�
  int				nNPremierCarVisible;

  PSTR			pszDernierTexte;
  FLOAT			num_min;
  FLOAT			num_max;
	LONG			num_nb_decimales;
  BOOL			bEnInsertion;
  BOOL			bCapture;
  BOOL			bNumerique;
  DWORD			err_num;
  BOOL			bTexteEnEdition;				// Le texte est en cours de modification par l'utilisateur
  BOOL			bTexteEnEditionValide;	// Le texte a �t� �dit� et valid� par l'utilisateur
  HCURSOR   hcursorTexte;
  HCURSOR   hcursorNormal;
  HCURSOR   hcursorInterdit;
  } ENV_INLINE, * PENV_INLINE;

// ---------------------------------------------------------------
static void pouet (void)
  {
  Beep (300,100);
  }

// ---------------------------------------------------------------
// Teste la validit� de la saisie en num�rique
static DWORD wInLineNumInMinMax (PENV_INLINE pEnvInLine)
  {
  FLOAT val;
  DWORD wInMinMax = 0;

  if (pEnvInLine->bNumerique)
    {
    if (StrToFLOAT (&val, pEnvInLine->Il.pszTexte))
      {
      if ((val < pEnvInLine->num_min) || (val > pEnvInLine->num_max))
        {
        wInMinMax = 2;
        }
      }
    else
      {
      wInMinMax = 1;
      }
    }
  return wInMinMax;
  }

// ---------------------------------------------------------------
static void rendre_focus (PENV_INLINE pEnvInLine)
  {
  SetFocus (::GetParent (pEnvInLine->hwnd));
  }

// ---------------------------------------------------------------
// Copie le texte �dit� �ventuellement sous forme crypt�e
static void CopieTexteVu (PENV_INLINE pEnvInLine, char *texte)
  {
  StrSetNull (texte);
  if (pEnvInLine->Il.bMasque)
    {
   DWORD nNCar;
   for (nNCar = 0; nNCar < StrLength (pEnvInLine->Il.pszTexte); nNCar++)
      {
      StrSetChar (texte, nNCar, CARACTERE_INLINE_MASQUE);
      }
    StrSetLength (texte, nNCar);
    }
  else
    {
    StrCopy (texte, pEnvInLine->Il.pszTexte);
    }
  }

// ----------------------------------------------------------------------------
// Largeur en pixels des nNbCarsMax premiers cars de la chaine sp�cifi�e
static int cxPartieTexte  (PENV_INLINE pEnvInLine, PCSTR pszTexte, DWORD nNbCarsMax)
  {
  char szTexteTemp [NB_CAR_MAX_INLINE];
	POINT	pt1;

	// r�cup�re la sous chaine
  StrExtract (szTexteTemp, pszTexte, 0, nNbCarsMax);

	// calcule l'encombrement de la sous chaine
  d_get_box_h_write (pEnvInLine->hdevice, 0, 0, &pt1.x, &pt1.y, pEnvInLine->Il.police,
		pEnvInLine->Il.style_police, pEnvInLine->Il.taille_police, szTexteTemp);

	// renvoie sa largeur
	return pt1.x;
  }

// ---------------------------------------------------------------
// recherche la position de curseur dans la chaine correspondant au x de click
static int nNCarClick (PENV_INLINE pEnvInLine, LONG x_curseur)
  {
  int	nRes;
  int x_precedent;
	int	cxDebutTexte;
  BOOL bTrouve = FALSE;
  char szVu [NB_CAR_MAX_INLINE];
	PCSTR pszAffiche;

  CopieTexteVu (pEnvInLine, szVu);
	pszAffiche = & szVu[pEnvInLine->nNPremierCarVisible];

  nRes = (int)StrLength (pszAffiche);
  cxDebutTexte = cxPartieTexte (pEnvInLine, pszAffiche, nRes);
  if (x_curseur < cxDebutTexte)
    {
    while ((nRes > 0) && (!bTrouve))
      {
      x_precedent = cxDebutTexte;
      nRes --;
      cxDebutTexte = cxPartieTexte (pEnvInLine, pszAffiche, nRes);
      bTrouve = ((cxDebutTexte <= x_curseur) && (x_curseur < x_precedent));
      }
    }
  return nRes;
  }

// ---------------------------------------------------------------
// Demande de d�placement du caret � une nouvelle position dans le texte �dit�
static BOOL bDeplaceCaret (PENV_INLINE pEnvInLine, int nNCarCaretNouveau)
	{
	BOOL	bRes = FALSE;
	BOOL	bRedessine = FALSE;
	int		nNbCars = (int)StrLength (pEnvInLine->Il.pszTexte);

	// Nouvelle position demand�e valide ?
	if ((nNCarCaretNouveau >= 0) && (nNCarCaretNouveau <= nNbCars))
		{
		char	szVu[NB_CAR_MAX_INLINE];
		int		cxPasScroll = pEnvInLine->cxFen/4;
		int		xNouveauCaret;

		// r�cup�re la chaine visible
		CopieTexteVu (pEnvInLine, szVu);

		// oui => Nouvelle position du caret trop � gauche ?
		if (nNCarCaretNouveau < pEnvInLine->nNPremierCarVisible)
			{
			// oui => scroll � faire pour afficher la partie gauche de la chaine :
			// calcule le premier caract�re visible de fa�on � ce qu'aparaisse au plus
			// un pas de scroll du texte avant le caret
			int		nNCarDebut = nNCarCaretNouveau;

			// Cherche la nouvelle origine de l'affichage du texte
			while ((nNCarDebut >= 0) &&
				(cxPartieTexte(pEnvInLine, &szVu[nNCarDebut], nNCarCaretNouveau - nNCarDebut) < cxPasScroll))
			 {
			 nNCarDebut --;
			 }
			if (nNCarDebut < 0)
				nNCarDebut = 0;
			
			// Nouvelle origine trouv�e => enregistre la et invalide la fen�tre pour tout redessiner
			pEnvInLine->nNPremierCarVisible = nNCarDebut;
			bRedessine = TRUE;

			// Nouvelle position du caret
			xNouveauCaret = cxPartieTexte (pEnvInLine, &szVu[pEnvInLine->nNPremierCarVisible], nNCarCaretNouveau - pEnvInLine->nNPremierCarVisible);

			} // if (nNCarCaretNouveau < pEnvInLine->nNPremierCarVisible)
		else
			{
			// Nouvelle position du caret trop � droite ?
			xNouveauCaret = cxPartieTexte (pEnvInLine, &szVu[pEnvInLine->nNPremierCarVisible], nNCarCaretNouveau - pEnvInLine->nNPremierCarVisible);

			if (xNouveauCaret >= pEnvInLine->cxFen)
				{
				// oui => scroll � faire pour afficher la partie droite de la chaine :
				// calcule le premier caract�re visible de fa�on � ce qu'aparaisse au plus
				// un pas de scroll de texte apr�s le caret
				int nNCarDebut = pEnvInLine->nNPremierCarVisible;

				// Cherche la premi�re origine de l'affichage du texte d�couvrant un pas de scroll
				while ((nNCarDebut < nNCarCaretNouveau) &&
					(cxPartieTexte (pEnvInLine, &szVu[nNCarDebut], nNCarCaretNouveau - nNCarDebut) > (pEnvInLine->cxFen - cxPasScroll)))
					{
					nNCarDebut ++;
					}

				// Nouvelle origine trouv�e => enregistre la et invalide la fen�tre pour tout redessiner
				pEnvInLine->nNPremierCarVisible = nNCarDebut;
				bRedessine = TRUE;
				
				// Nouvelle position du caret
				xNouveauCaret = cxPartieTexte (pEnvInLine, &szVu[pEnvInLine->nNPremierCarVisible], nNCarCaretNouveau - pEnvInLine->nNPremierCarVisible);
				} // if (xNouveauCaret >= pEnvInLine->cxFen)
			} // else (nNCarCaretNouveau < pEnvInLine->nNPremierCarVisible)

		// Dans tous les cas :
		// Enregistre la nouvelle position du caret
		pEnvInLine->nNCarCaret = nNCarCaretNouveau;

		// Positionne le nouveau caret
		pEnvInLine->pointCaret.x = xNouveauCaret;

		// d�place le
		if (pEnvInLine->bALeFocus)
			SetCaretPos (pEnvInLine->pointCaret.x, pEnvInLine->pointCaret.y);

		// r�affiche le texte si n�cessaire
		if (bRedessine)
			::InvalidateRect (pEnvInLine->hwnd, NULL, TRUE);

		bRes = TRUE;
		} // if ((nNCarCaretNouveau >= 0) && (nNCarCaretNouveau <= nNbCars))
	return bRes;
	} // bDeplaceCaret

//---------------------------------------------------------
// Remplace le texte �dit� par la chaine sp�cifi�e
static BOOL bRemplaceTexteEdition (PENV_INLINE pEnvInLine, PCSTR pszSource)
	{
	BOOL	bRes = FALSE;

	if (pEnvInLine->Il.pszTexte && pszSource)
		{
		if (StrLength (pszSource) <= (pEnvInLine->bNumerique ? NB_CARS_NUMERIQUE_MAX : pEnvInLine->Il.NbCarsMax))
			{
			// oui => Mise � jour du buffer, du caret et redessin
			StrCopy (pEnvInLine->Il.pszTexte, pszSource);
			::InvalidateRect (pEnvInLine->hwnd, NULL, TRUE);
			bDeplaceCaret (pEnvInLine, 0);

			// reset Modif utilisateur
			pEnvInLine->bTexteEnEdition = TRUE;

			// r�sultat Ok
			bRes = TRUE;
			}
		}
	return bRes;
	} // bRemplaceTexteEdition

// ---------------------------------------------------------------
// Traitement de WM_KEYDOWN
static BOOL OnKeyDown (WPARAM mp1, PENV_INLINE pEnvInLine)
  {
  BOOL  btouchetraite = TRUE;
  BOOL	bControle = ((0x8000 & GetKeyState (VK_CONTROL)) != 0);
  BOOL	bShift = ((0x8000 & GetKeyState (VK_SHIFT))!= 0);
	char	c = LOBYTE (mp1);
	
  switch (c)
    {
    case VK_INSERT:
			if (!(bShift && bControle))
				{
				if (bControle)
					// Provoque la copie du texte courant dans le presse papier
					::SendMessage (pEnvInLine->hwnd, WM_COPY, 0, 0);
				else
					{
					if (bShift)
						// Colle le texte du presse papier dans le texte courant
						::SendMessage (pEnvInLine->hwnd, WM_PASTE, 0, 0);
					else
						pEnvInLine->bEnInsertion = !pEnvInLine->bEnInsertion; //$$ rappel visuel
					}
				}
			break;

    case VK_DELETE :
			if (bShift)
				// Coupe le texte courant dans le presse papier
				::SendMessage (pEnvInLine->hwnd, WM_CUT, 0, 0);
			else
				{
				if (pEnvInLine->nNCarCaret < (int)StrLength (pEnvInLine->Il.pszTexte))
					{
					StrDelete (pEnvInLine->Il.pszTexte, pEnvInLine->nNCarCaret, 1);

					// Texte modifi� par l'utilisateur
					pEnvInLine->bTexteEnEdition = TRUE;
					
					// Redessine
					::InvalidateRect (pEnvInLine->hwnd, NULL, TRUE);
					}
				else
					pouet ();
				}
			break;

    case VK_RIGHT :
			if (!bDeplaceCaret (pEnvInLine, pEnvInLine->nNCarCaret + 1))
				pouet ();
			break;

    case VK_LEFT  :
			if (!bDeplaceCaret (pEnvInLine, pEnvInLine->nNCarCaret - 1))
				pouet ();
			break;

    case VK_HOME  :
			if (!bDeplaceCaret (pEnvInLine, 0))
				pouet ();
			break;

    case VK_END   :
			if (!bDeplaceCaret (pEnvInLine, (int)StrLength (pEnvInLine->Il.pszTexte)))
				pouet ();
			break;

		default: 
			btouchetraite = FALSE;
			break;
    } // switch (c)

  return btouchetraite;
  } // OnKeyDown

// ---------------------------------------------------------------
// Traitement de WM_SYSKEYDOWN
static BOOL OnSysKeyDown (HWND hwnd, WPARAM mp1, LPARAM mp2)
  {
  BOOL  btouchetraite = FALSE;
	char	c = LOBYTE (mp1);
	
  switch (c)
    {
    case VK_BACK :
			// Touche ALT enfonc�e ?
			if (mp2 & 0x20000000)
				{
				// Remplace le texte courant avec sa version initiale
				::SendMessage (hwnd, EM_UNDO, 0, 0);
				btouchetraite = TRUE;
				}
			break;
    } // switch (c)

  return btouchetraite;
  } // OnSysKeyDown

// ---------------------------------------------------------------
// Appui de touche Ansi
static BOOL OnChar (HWND hwnd, WPARAM mp1, PENV_INLINE pEnvInLine)
  {
  BOOL		btouchetraite = TRUE;
	char		c = LOBYTE (mp1);
	
	switch (c)
    {
    case VK_TAB:
			{
			// Dis � la boite de dialogue d'activer le contr�le pr�c�dent (Shift Tab) ou suivant (Tab)
		  BOOL	bShift = ((0x8000 & GetKeyState (VK_SHIFT))!= 0);

			::PostMessage (::GetParent (hwnd), WM_NEXTDLGCTL, bShift, FALSE);
			}
      break;

    case VK_RETURN:
			{
			BOOL		bOk = FALSE;
			char		mess_erreur [c_nb_car_message_err];

			if (pEnvInLine->bNumerique)
				{
				switch (wInLineNumInMinMax(pEnvInLine))
					{
					case 0:
						bOk = TRUE;
						break;
					case 1:
						bLngLireErreur (pEnvInLine->err_num, mess_erreur);
						break;
					case 2:
						bLngLireErreur (pEnvInLine->err_num+1, mess_erreur);
						break;
					}
				}
			else
				bOk = TRUE;

			// Texte valid� par VK_RETURN?
			if (bOk)
				{
				// Enregistre le dernier texte
			  StrCopy (pEnvInLine->pszDernierTexte, pEnvInLine->Il.pszTexte);
				pEnvInLine->bTexteEnEdition = FALSE;
				pEnvInLine->bTexteEnEditionValide = TRUE;

				// Notification au parent
				::PostMessage (::GetParent(pEnvInLine->hwnd), WM_COMMAND, 
					MAKEWPARAM (::GetDlgCtrlID(pEnvInLine->hwnd), INLINE_VALIDATION), (LPARAM)hwnd);

				rendre_focus (pEnvInLine);
				}
			else
				{
				pouet();
				::MessageBox (pEnvInLine->hwnd, mess_erreur, NULL, MB_OK|MB_ICONHAND|MB_SYSTEMMODAL);
				}
			}
			break;

    case VK_ESCAPE:
			//restaure le dernier texte
			bRemplaceTexteEdition (pEnvInLine, pEnvInLine->pszDernierTexte);
			pEnvInLine->bTexteEnEdition = FALSE;

				// Notification au parent
			::PostMessage (::GetParent(pEnvInLine->hwnd), WM_COMMAND, 
				MAKEWPARAM (::GetDlgCtrlID(pEnvInLine->hwnd), INLINE_ABANDON), (LPARAM)hwnd);

			rendre_focus (pEnvInLine);
			break;

    case VK_BACK : // BackSpace
			if (bDeplaceCaret (pEnvInLine, pEnvInLine->nNCarCaret - 1))
				{
				StrDelete (pEnvInLine->Il.pszTexte, pEnvInLine->nNCarCaret, 1);

				// Texte modifi� par l'utilisateur
				pEnvInLine->bTexteEnEdition = TRUE;
				
				// Redessine
				::InvalidateRect (pEnvInLine->hwnd, NULL, TRUE);
				}
			else
				pouet ();
			break;

		case '\3': // Ctrl C
			// Provoque la copie du texte courant dans le presse papier
			::SendMessage (pEnvInLine->hwnd, WM_COPY, 0, 0);
			break;

		case '\x16': // Ctrl V
			// Colle le texte du presse papier dans le texte courant
			::SendMessage (pEnvInLine->hwnd, WM_PASTE, 0, 0);
			break;

		case '\x18': // Ctrl X
			// Coupe le texte courant dans le presse papier
			::SendMessage (pEnvInLine->hwnd, WM_CUT, 0, 0);
			break;

    default:
			{
			// autres caract�res
			BOOL		bOk = FALSE;

			// impossibilit� d'insertion d'un nouveau caract�re ?
			if ((pEnvInLine->bEnInsertion) &&	(StrLength(pEnvInLine->Il.pszTexte) >= pEnvInLine->Il.NbCarsMax))
				{
				// oui => erreur
				pouet ();
				}
			else
				{
				// non $$ test ?
				if (pEnvInLine->nNCarCaret < (int)pEnvInLine->Il.NbCarsMax)
					{
					// caract�re autoris� ?
					if (pEnvInLine->bNumerique)
						bOk = StrCharIsInStr (c, ".0123456789-+EedD");
					else
						bOk = (c >= ' '); // caract�re valide � partir de l'espace

					if (bOk)
						{
						// oui => ajoute du caract�re au texte �dit� ?
						if ((pEnvInLine->bEnInsertion) || (pEnvInLine->nNCarCaret >= (int)StrLength(pEnvInLine->Il.pszTexte)))
							// oui => ajoute le
							StrInsertChar (pEnvInLine->Il.pszTexte, c, pEnvInLine->nNCarCaret);
						else
							// non => remplace le
							pEnvInLine->Il.pszTexte [(pEnvInLine->nNCarCaret)] = c;

						// Texte modifi� par l'utilisateur
						pEnvInLine->bTexteEnEdition = TRUE;

						// Redessine
						::InvalidateRect (pEnvInLine->hwnd, NULL, TRUE);

						// ajuste le curseur
						if (!bDeplaceCaret (pEnvInLine, pEnvInLine->nNCarCaret + 1))
							pouet ();
						}
					else
						pouet();
					}
				else
					pouet ();
				}
			}
      break;
    }

  return btouchetraite;
  }

// ---------------------------------------------------------------
// Traitement WM_PAINT
static void OnWMPaint (HWND hwnd)
  {
	PENV_INLINE pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
	PAINTSTRUCT ps;

	::BeginPaint (hwnd, &ps);

	if (pEnvInLine->hdevice != NULL)
		{
		G_COULEUR  couleur_fond;
		G_COULEUR  couleur_texte;
		char	szVu [NB_CAR_MAX_INLINE];
		#define COULEUR_CADRE G_WHITE
		BOOL	bFondEnInverse = FALSE;

		// Regarde si l'affichage se fait en inverse vid�o
		if (pEnvInLine->bALeFocus)
			{
			if (pEnvInLine->Il.bFocusEnVideoInverse)
				// En inverse si Focus et bFocusEnVideoInverse
				bFondEnInverse = TRUE;
			}
		else
			{
			if (pEnvInLine->bTexteEnEdition)
				// En inverse si pas de Focus et texte en cours d'�dition
				bFondEnInverse = TRUE;
			}
		if (bFondEnInverse)
			{
			couleur_fond = pEnvInLine->couleur_fond_inv;
			couleur_texte = pEnvInLine->couleur_texte_inv;
			}
		else
			{
			couleur_fond = pEnvInLine->Il.couleur_fond;
			couleur_texte = pEnvInLine->Il.couleur_texte;
			}

		// Dessin du fond
		d_fill_background (pEnvInLine->hdevice, couleur_fond);

		// Dessin du cadre
		if (pEnvInLine->Il.bCadre)
			{
			RECT rect;
			::GetClientRect (pEnvInLine->hwnd, &rect);
			d_rectangle (pEnvInLine->hdevice, COULEUR_CADRE, G_STYLE_LIGNE_CONTINUE, 0, 0, rect.right, rect.bottom);
			}

		// Dessin de la partie visible du texte
		CopieTexteVu (pEnvInLine, szVu);
		d_h_write (pEnvInLine->hdevice, couleur_texte, 0, 0, 0, pEnvInLine->Il.taille_police,
			pEnvInLine->Il.police, pEnvInLine->Il.style_police, &szVu [pEnvInLine->nNPremierCarVisible]);
		} // if (pEnvInLine->hdevice != NULL)

	::EndPaint (hwnd, &ps);
	} // OnWMPaint

// ---------------------------------------------------------------
//  fen�tre wndprocInLine (sorte de contr�le Edit)
// ---------------------------------------------------------------
static LRESULT CALLBACK wndprocInLine (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
	{
	LRESULT 		mres = 0;
	BOOL				bTraite = TRUE;
	PENV_INLINE	pEnvInLine;

	switch (msg)
		{
		case WM_CREATE:
			{
			// r�cup�re les param�tres de cr�ation
			PARAM_INLINE * pIl = (PARAM_INLINE *)(((LPCREATESTRUCT)mp2)->lpCreateParams);

			// cr�e l'environnement
			pEnvInLine = (PENV_INLINE)pCreeEnvInit (hwnd, sizeof (ENV_INLINE), NULL);

			// Initialise le
			pEnvInLine->Il = *pIl;
			pEnvInLine->hcursorTexte = LoadCursor(NULL, IDC_IBEAM);
			pEnvInLine->hcursorNormal = LoadCursor(NULL, IDC_ARROW);
			pEnvInLine->hcursorInterdit = LoadCursor(NULL, IDC_NO);
			pEnvInLine->pointCaret.x = 0;
			pEnvInLine->pointCaret.y = 1;

			pEnvInLine->hwnd = hwnd;
			pEnvInLine->hdevice = d_open ();
			d_set_param (pEnvInLine->hdevice, hwnd);
			d_init (pEnvInLine->hdevice);

			pEnvInLine->couleur_texte_inv = pEnvInLine->Il.couleur_fond;
			pEnvInLine->couleur_fond_inv = pEnvInLine->Il.couleur_texte;

			pEnvInLine->bNumerique = FALSE;
			pEnvInLine->num_max = FLOAT_MAX;
			pEnvInLine->num_min = (-FLOAT_MAX);
			pEnvInLine->err_num = 0;
			pEnvInLine->bCapture = FALSE;

			pEnvInLine->pszDernierTexte = (PSTR)pMemAlloueInit (uMemTaille(pEnvInLine->Il.pszTexte), pEnvInLine->Il.pszTexte);
			pEnvInLine->bEnInsertion = TRUE;
			pEnvInLine->nNPremierCarVisible = 0;
			pEnvInLine->nNCarCaret = StrLength (pEnvInLine->Il.pszTexte);
			pEnvInLine->bTexteEnEdition = FALSE;
			pEnvInLine->bTexteEnEditionValide = FALSE;

			// Enregistre dernier texte
		  StrCopy (pEnvInLine->pszDernierTexte, pEnvInLine->Il.pszTexte);
			}
			break;

    case WM_DESTROY:
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			d_fini (pEnvInLine->hdevice);
			d_close (&pEnvInLine->hdevice);
			MemLibere ((PVOID *)(&pEnvInLine->Il.pszTexte));
			bLibereEnv (hwnd);
			break;

    case WM_ERASEBKGND:
			mres = (LRESULT) TRUE;
			break;

		case WM_PAINT:
			OnWMPaint (hwnd);
			break;

		case WM_GETDLGCODE:
			mres = DLGC_WANTALLKEYS; //|DLGC_UNDEFPUSHBUTTON|DLGC_WANTARROWS |DLGC_WANTCHARS|
			break;

		case WM_SIZE:
			if (mp1 != SIZE_MINIMIZED)
				{
				pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);

				pEnvInLine->cxFen = LOWORD (mp2);
				bDeplaceCaret (pEnvInLine, pEnvInLine->nNCarCaret);
				}
			break;

		case WM_SETFOCUS:
			{
			// R�cup�re le Focus :
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			VerifWarning (!pEnvInLine->bALeFocus);
			pEnvInLine->bALeFocus = TRUE;

			// Cr�e un "caret" plein noir. 
			CreateCaret(hwnd, (HBITMAP) NULL, 1, pEnvInLine->cxFen - 2); 

			// Positionne le
			SetCaretPos (pEnvInLine->pointCaret.x, pEnvInLine->pointCaret.y); 

			// Montre le caret. 
			ShowCaret(hwnd); 

			// Capture la souris
			SetCapture (pEnvInLine->hwnd);
			pEnvInLine->bCapture = TRUE;

			// Pas de modification en cours ?
			if (!pEnvInLine->bTexteEnEdition)
				{
				// oui  => Remise � z�ro sur Focus ?
				if (pEnvInLine->Il.bRazSurFocus)
					{
					// oui => vide la chaine d'�dition (pas de modif utilisateur)
					VerifWarning (bRemplaceTexteEdition (pEnvInLine, ""));
					pEnvInLine->bTexteEnEdition = FALSE;
					}
				}

			bDeplaceCaret (pEnvInLine, (int) StrLength (pEnvInLine->Il.pszTexte));
			// Redessine
			::InvalidateRect (hwnd, NULL, TRUE);
			}
			break;

		case WM_KILLFOCUS:
			{
			// perte de focus
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);

			// note la perte de focus
			VerifWarning (pEnvInLine->bALeFocus);
			pEnvInLine->bALeFocus = FALSE;

			// Pas de modification en cours ?
			if (!pEnvInLine->bTexteEnEdition)
				{
				// oui  => Remise � z�ro sur Focus ?
				if (pEnvInLine->Il.bRazSurFocus)
					{
					// oui => restaure le dernier texte
					VerifWarning (bRemplaceTexteEdition (pEnvInLine, pEnvInLine->pszDernierTexte));
					pEnvInLine->bTexteEnEdition = FALSE;
					}
				}

			// Redessine
		  ::InvalidateRect (hwnd, NULL, TRUE);

			// Lib�re l'�ventuelle capture de la souris
			if (pEnvInLine->bCapture)
				{
				ReleaseCapture ();
				pEnvInLine->bCapture = FALSE;
				// $$ restaurer le curseur souris initial
				}

			DestroyCaret(); 
			}
			break;

    case WM_MOUSEMOVE:
			{
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			// Souris captur�e ?
			if (pEnvInLine->bCapture)
				{
				POINT  point = {(LONG) LOWORD(mp2), (LONG) HIWORD(mp2)};
				RECT   rect;

				// oui => souris dans la zone de saisie ?
				::GetClientRect (hwnd, &rect);
				if (PtInRect (&rect, point))
					// oui => curseur texte
					SetCursor (pEnvInLine->hcursorTexte);
				else
					// non => curseur interdit
					SetCursor (pEnvInLine->hcursorInterdit);
				}
			else
				// non => curseur texte puisqu'au dessus de la zone de saisie
				SetCursor (pEnvInLine->hcursorTexte);
			}
			break;

    case WM_LBUTTONDOWN:
			{
			POINT  point = {(int) LOWORD(mp2), (int) HIWORD(mp2)};
			RECT   rect;

			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			if (!pEnvInLine->bALeFocus)
			  SetFocus (hwnd);

			::GetClientRect (hwnd, &rect);
			if (PtInRect(&rect, point))
				bDeplaceCaret (pEnvInLine, nNCarClick (pEnvInLine, point.x));
			else
				pouet ();
			}
			break;

    case WM_KEYDOWN:
			{
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			mres = (LRESULT) OnKeyDown (mp1, pEnvInLine);
			bTraite = mres;
			}
			break;

    case WM_SYSKEYDOWN:
			{
			mres = (LRESULT) OnSysKeyDown (hwnd, mp1, mp2);
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			bTraite = mres;
			}
			break;

    case WM_CHAR:
			{
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			mres = (LRESULT) OnChar (hwnd, mp1, pEnvInLine);
			bTraite = mres;
			}
			break;

		case WM_GETTEXTLENGTH:
			{
			// For�age de la chaine d'�dition possible ?
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);

			if (pEnvInLine->pszDernierTexte) //Il.pszTexte)
				mres = StrLength (pEnvInLine->pszDernierTexte); //Il.pszTexte);
			}
			break;

		case WM_GETTEXT:
			{
			PSTR	pszDest = (PSTR)mp2;

			// Lecture de la chaine d'�dition possible ?
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			if (pEnvInLine->pszDernierTexte && pszDest) // Il.pszTexte
				{
				StrExtract (pszDest, pEnvInLine->pszDernierTexte, 0, (DWORD)mp1); // Il.pszTexte
				mres = StrLength (pszDest);  
				}
			}
			break;

		case WM_SETTEXT:
			{
			PCSTR	pszSource = (PCSTR)mp2;

			// For�age de la chaine d'�dition possible ?
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);

			// r�sultat Ok si texte pris en compte
			mres = bRemplaceTexteEdition (pEnvInLine, pszSource);
			if (!mres)
				{
				pouet();
				mres = bRemplaceTexteEdition (pEnvInLine, "#");
				}
			if (mres)
				{
				// oui => reset Modif utilisateur et m�morise la valeur initiale
				pEnvInLine->bTexteEnEdition = FALSE;
				pEnvInLine->bTexteEnEditionValide = FALSE;
				StrCopy (pEnvInLine->pszDernierTexte, pEnvInLine->Il.pszTexte);
				}
			}
			break;

		case WM_COPY:
			{
			// copie du texte courant dans le presse papier
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			if (pEnvInLine->Il.pszTexte)
				{
				if (!bClipBEcritTexte (pEnvInLine->Il.pszTexte))
					pouet();
				}
			}
			break;

		case WM_CUT:
			{
			// Coupe le texte courant dans le presse papier
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			if (pEnvInLine->Il.pszTexte)
				{
				// Ecris le texte courant dans le presse papier ?
				if (bClipBEcritTexte (pEnvInLine->Il.pszTexte))
					{
					// oui => vide le texte courant
					if (!bRemplaceTexteEdition (pEnvInLine, ""))
						pouet();
					}
				else
					pouet();
				}
			}
			break;

		case WM_PASTE:
			{
			// Colle le texte du presse papier dans le texte courant
			// r�cup�re le texte du presse papier ?
			PSTR	pszClip = pszClipBLitTexte ();

			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			if (pszClip)
				{
				// oui => remplace le texte courant avec ce texte
				if (!bRemplaceTexteEdition (pEnvInLine, pszClip))
					pouet();
				MemLibere((PVOID *)(& pszClip));
				}
			}
			break;

		case EM_CANUNDO:
			// Support du Undo :
			mres = TRUE;
			break;

		case EM_UNDO:
			// Remplace le texte courant avec sa version initiale
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			if (pEnvInLine->Il.pszTexte && pEnvInLine->pszDernierTexte)
				{
				// oui => remplace le texte courant avec le dernier texte
				if (bRemplaceTexteEdition (pEnvInLine, pEnvInLine->pszDernierTexte))
					pEnvInLine->bTexteEnEdition = FALSE;
				else
					pouet();
				}
			break;

		case EM_GETMODIFY:
			pEnvInLine = (PENV_INLINE)pGetEnv (hwnd);
			mres = pEnvInLine->bTexteEnEditionValide;
			break;

    default:
			bTraite = FALSE;
			break;
    }

  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return mres;
  }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//		Fonctions export�es
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------
// Formate dans tous les cas un r�el pour affichage dans Processyn
// Renvoie FALSE si le format d�sir� n'est pas atteint
static BOOL bFormatteFloatProcessyn (PSTR pszResultat, int NbCarsMax, int NbDecimales, BOOL bNumScientifique, FLOAT fVal)
	{
	DWORD dwTailleResultat = 0;

	// Blindage
	if (NbDecimales > (NB_CARS_NUMERIQUE_MAX-9))
		NbDecimales = (NB_CARS_NUMERIQUE_MAX-9);
	else
		{
		if (NbDecimales < 0)
			NbDecimales = 0;
		}

	// Demande d'un affichage en scientifique ?
	if (bNumScientifique)
		{
		// oui => conversion au format scientifique en sp�cifiant le nombre de chiffres apr�s la virgule
		StrPrintFormat (pszResultat, "%.*E", NbDecimales, fVal);
		dwTailleResultat = StrLength (pszResultat);;
		}
	else
		{
		// non => conversion de la valeur en une chaine au format d�cimal (forme NNN.DDD)
		StrPrintFormat (pszResultat, "%.*f", NbDecimales, fVal);

		// chaine d�passe la taille sp�cifi�e ?
		dwTailleResultat = StrLength (pszResultat);
		if (dwTailleResultat > (DWORD) NbCarsMax)
			{
			// oui => un format scientifique N.DDDDDDE+XXX peut �tre plus compact ?
			if (dwTailleResultat > 12)
				{
				// conversion au format scientifique (6 chiffres significatifs)
				char  szScientifique[NB_CARS_NUMERIQUE_MAX];
				DWORD dwTailleScientifique;

				StrPrintFormat (szScientifique, "%E", fVal);
				dwTailleScientifique = StrLength (szScientifique);

				// r�sultat plus compact ?
				if (dwTailleScientifique < dwTailleResultat)
					// oui => utilise ce format (pas demise � jour de dwTailleResultat pour garder l'erreur)
					strcpy (pszResultat, szScientifique);
				}
			}
		}

	// Renvoie TRUE si la taille de la chaine formatt�e est valide
	return dwTailleResultat <= (DWORD)NbCarsMax;
	} // bFormatteFloatProcessyn

// ----------------------------------------------------------------
// Passage en mode �dition de nombre
BOOL bInLineSetModeNum (HWND hwndInline, FLOAT num_min, FLOAT num_max, LONG NbDecimales, DWORD err_num)
  {
	BOOL bRes = FALSE;

	//$$ Envoyer un message
	if (IsWindow(hwndInline))
		{
		PENV_INLINE	pEnvInLine = (PENV_INLINE)pGetEnv (hwndInline);

		Verif (pEnvInLine);
		bRes = TRUE;
		pEnvInLine->bNumerique = TRUE;
		pEnvInLine->err_num = err_num;
		pEnvInLine->num_max = num_max;
		pEnvInLine->num_min = num_min;
		pEnvInLine->num_nb_decimales = NbDecimales;
		if (wInLineNumInMinMax (pEnvInLine) != 0)
			bRemplaceTexteEdition (pEnvInLine, "");
		}
	return bRes;
  }

// ---------------------------------------------------------------
// Assigne les couleurs de ce contr�le
BOOL bInLineSetColor (HWND hwndInline, G_COULEUR couleur_fond, G_COULEUR couleur_texte)
  {
	BOOL bRes = FALSE;
	if (IsWindow(hwndInline))
		{
		bRes = TRUE;
		PENV_INLINE	pEnvInLine = (PENV_INLINE)pGetEnv(hwndInline);

		Verif (pEnvInLine);
		pEnvInLine->Il.couleur_fond = couleur_fond;
		pEnvInLine->Il.couleur_texte = couleur_texte;
		// Redessine
		::InvalidateRect (pEnvInLine->hwnd, NULL, TRUE);
		}
	return bRes;
  }

// ---------------------------------------------------------------
// Assigne un nouveau nombre � ce contr�le
BOOL bInLineNumSet (HWND hwndInline, FLOAT fVal)
  {
	BOOL	bRes = FALSE;
	if (IsWindow(hwndInline))
		{
		PENV_INLINE pEnvInLine = (PENV_INLINE)pGetEnv(hwndInline);

		Verif (pEnvInLine);
		if (pEnvInLine->bNumerique)
			{
			char  szResultat[NB_CAR_MAX_INLINE];
//			bRes = bFormatteFloatProcessyn (szResultat, pEnvInLine->Il.NbCarsMax, pEnvInLine->num_nb_decimales, pEnvInLine->Il.bNumScientifique, fVal);
			bRes = TRUE;
			if (!bFormatteFloatProcessyn (szResultat, pEnvInLine->Il.NbCarsMax, pEnvInLine->num_nb_decimales, pEnvInLine->Il.bNumScientifique, fVal))
				{
				char		mess_erreur [c_nb_car_message_err];
				pouet();
				bLngLireErreur (pEnvInLine->err_num, mess_erreur);
				::MessageBox (pEnvInLine->hwnd, mess_erreur, NULL, MB_OK|MB_ICONHAND|MB_SYSTEMMODAL);
				}
			
			// Mise � jour du texte avec la nouvelle chaine
			if (!::SetWindowText (pEnvInLine->hwnd, szResultat))
				bRes = FALSE;
  		}
		}

	return bRes;
  } // 

// ---------------------------------------------------------------
// Cr�ation de la fen�tre d'�dition
// ---------------------------------------------------------------
HWND InLineCreate (HWND hwndParent, DWORD id,
	LONG xFen, LONG yFen, LONG cxFen, LONG cyFen,
	G_COULEUR couleur_fond, G_COULEUR couleur_texte,
	int NbCarsMax, DWORD police, DWORD style_police, DWORD taille_police,
	PCSTR pszTexte, BOOL bCadre, BOOL bMasque, BOOL bRazSurFocus, BOOL bFocusEnVideoInverse)

  {
	static BOOL	bClasseEnregistree = FALSE;
  HWND hwnd = NULL;

	// Enregistre la classe de la fen�tre si n�cessaire
	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= CS_OWNDC;
		wc.lpfnWndProc		= wndprocInLine; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= NULL; // pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szNomClasse;
		bClasseEnregistree = RegisterClass (&wc);
		}

	if (bClasseEnregistree)
		{
		PARAM_INLINE il;
		
		// Initialise les param�tres de cr�ation
		il.couleur_fond = couleur_fond;
		il.couleur_texte = couleur_texte;
		il.bNumScientifique = (NbCarsMax < 0);
		if (il.bNumScientifique)
			NbCarsMax = - NbCarsMax;
		il.NbCarsMax = NbCarsMax;
		il.police = police;
		il.style_police = style_police;
		il.taille_police = taille_police;

		if (NbCarsMax < NB_CARS_NUMERIQUE_MAX)
			NbCarsMax = NB_CARS_NUMERIQUE_MAX;
		il.pszTexte = (PSTR)pMemAlloueInit0 ((DWORD)NbCarsMax + 1);
		if (StrLength(pszTexte) <= (DWORD)NbCarsMax)
			MemCopy (il.pszTexte, pszTexte, StrLength(pszTexte));
		il.bCadre = bCadre;
		il.bMasque = bMasque;
		il.bRazSurFocus = bRazSurFocus;
		il.bFocusEnVideoInverse = bFocusEnVideoInverse;

		// cr�e la fen�tre
		hwnd = CreateWindow (szNomClasse, "", WS_VISIBLE|WS_CHILD|WS_TABSTOP, xFen, yFen, cxFen, cyFen,
			hwndParent, (HMENU) id, Appli.hinst, &il);
		}

  return hwnd;
  }

//------------------ fin WInLine.c ----------------------------
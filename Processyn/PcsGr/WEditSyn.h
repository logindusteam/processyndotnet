// -----------------------------------------------------------------------
// WEditSyn.h
// Gestion des fen�tres d'�dition de synoptique de PCSGR
// -----------------------------------------------------------------------

#define SOURIS_FLECHE 1
#define SOURIS_CROIX  2
#define SOURIS_CIBLE  3

// --------------------------------------------------------------------
void creer_espace_ref_fen_gr (void);
void supprimer_espace_ref_fen_gr (void);

// --------------------------------------------------------------------
BOOL bInitEditSyn (void);

// --------------------------------------------------------------------
void InitFenetreSyn (DWORD *id_fenetre, HBDGR hBdGr);
void CreeFenetreEditSyn (DWORD id_fenetre, HGSYS * phgsys);
void fermer_fenetre_gr (DWORD id_fenetre);

//---------------------------------------------------------------------
void ouvrir_fenetre_saisie_gr (DWORD id_fenetre, G_COULEUR backcolor, G_COULEUR textcolor,
                               DWORD police, DWORD style_police, DWORD taille_police,
                               LONG x1, LONG y1, char *texte_depart);

// --------------------------------------------------------------------
void afficher_titre_fenetre_gr (DWORD id_fenetre);

// --------------------------------------------------------------------
void capturer_souris_fenetre_gr (DWORD id_fenetre);
void dessiner_souris_fenetre_gr (DWORD type_souris);


/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Gestion de l'interface des applications utilisateur de PcsGr
 |	       								|
 |   Auteur  : AC							|
 |   Date    : 26/10/92 							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "mem.h"
#include "lng_res.h"
#include "tipe.h"
#include "UStr.h"
#include "PathMan.h"
#include "FMan.h"
#include "FileMan.h"
#include "pcsgr.h"
#include "LireLng.h"
#include "lng_sys.h"
#include "cmdpcs.h"
#include "IdLngLng.h"
#include "DocMan.h"
#include "IdStrProcessyn.h"
#include "SignatureFichier.h"
#include "Appli.h"
#include "Verif.h"

#include "appligr.h"
VerifInit;

// ----------------------------------------------------------------
void initialiser_ctxt_application (void)
  {
  OuvrirMotsReserves ();
  InitPathNameDoc ();
  }

// ---------------------- charger application --------------------------
int charger_application (char * pszNom, BOOL bApplicationSat)
  {
  char szNomApplicationChargee[MAX_PATH];
  int renvoi = 0;

  initialiser_ctxt_application();
  bSetPathNameDoc (pszNom);

	// Cr�e le nom du fichier � charger
	if (bApplicationSat)
		Verif (pszCopiePathNameDocExt (szNomApplicationChargee, MAX_PATH, EXTENSION_SAT));
	else
		Verif (pszCopiePathNameDocExt (szNomApplicationChargee, MAX_PATH, EXTENSION_SOURCE));

	pszPathNameAbsolu (szNomApplicationChargee, Appli.szPathInitial);
  if (CFman::bFAcces (szNomApplicationChargee, CFman::OF_OUVRE_LECTURE_SEULE) && (charge_memoire (szNomApplicationChargee) == 0))
    {
	  SetNouveauDoc (FALSE);
    bOuvrirLng ();
    OuvrirMotsReserves ();
		if (!bApplicationSat)  
			{
			MajFichierCmdPcs (pszPathNameDoc());
			}
    }
	else 
		{
		if (!bApplicationSat)  //g�n�re une application par d�faut sinon sortira apr�s message
			{
			InitPathNameDocVierge (EXTENSION_SOURCE);		
			raz_mem ();
			bOuvrirLng ();
			OuvrirMotsReserves ();
			renvoi = 449;
			}
		else
			{
			renvoi = 4;
			}
		}
  return renvoi;
  }

// ----------------------------------------------------------------
void duplique_fic_extend (char *nom_source, char *nom_destination, char *extension)
  {
  char nom_depart[MAX_PATH];
  char nom_arrivee[MAX_PATH];

  StrCopy (nom_depart, nom_source);
  StrConcat (nom_depart, extension);
  StrCopy (nom_arrivee, nom_destination);
  StrConcat (nom_arrivee, extension);
  uFileCopie (nom_depart, nom_arrivee, FALSE);
	}

// ---------------------- sauver application --------------------------
int sauver_application (char * chaine, BOOL bApplicationSat)
  {
  HFIC repere_fic;
  char nom_fic[MAX_PATH];
  char nom_fic_bak[MAX_PATH];
  char nom_precedent[MAX_PATH];
  UINT uRes;

  // fichier courant existe ?
	if (bApplicationSat)
		pszCopiePathNameDocExt (nom_fic, MAX_PATH, EXTENSION_SAT);
	else
		pszCopiePathNameDocExt (nom_fic, MAX_PATH, EXTENSION_SOURCE);

	uRes = uFileOuvre (&repere_fic, nom_fic, 1, CFman::OF_OUVRE_OU_CREE);
	if (uRes == 0)
    {
		// oui => sauvegarde en .bak
    uFileFerme (&repere_fic);
    StrCopy (nom_precedent, pszPathNameDoc());
    bSetPathNameDoc (chaine);
    if (bFileExiste (nom_fic))
      {
	    pszCreeNameExt (nom_fic_bak, MAX_PATH, chaine, EXTENSION_BAK);
      uFileEfface (nom_fic_bak);
      uFileRenomme (nom_fic, nom_fic_bak);
      }

		CSignatureFichier SignatureFichier(SIGNATURE_FORMAT_PCS_EN_COURS);
		SignatureFichier.DBPCSSetSignature();
    if (sauve_memoire (nom_fic) == 0)
			{
			if (!bStrIEgales (nom_precedent, pszPathNameDoc())) // changement de nom => duplication fichier data
				{
				duplique_fic_extend (nom_precedent, pszPathNameDoc(), EXTENSION_PA1);
				}
			if (!bApplicationSat)
				{
				SetNouveauDoc (FALSE);
				MajFichierCmdPcs (pszPathNameDoc());
				}
			}
		else
			{
			uRes = IDSERR_SAUVEGARDE_DU_FICHIER_IMPOSSIBLE_VERIFIER_LE_NOM_ET_OU_LES_ATTRIBUTS;
			}
    }
  else
    {
    uRes = IDSERR_SAUVEGARDE_DU_FICHIER_IMPOSSIBLE_VERIFIER_LE_NOM_ET_OU_LES_ATTRIBUTS; 
    }
  return (int) uRes;
  }

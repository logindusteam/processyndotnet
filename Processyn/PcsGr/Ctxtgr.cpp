/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : ctxtgr.c                                               |
 |   Auteur  : AC					        	|
 |   Date    : 20/11/92					        	|
 |   Version : 3.20							|
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "mem.h"
#include "G_Objets.h"
#include "g_sys.h"     // pour definition action
#include "lng_res.h"
#include "Tipe.h"
#include "Descripteur.h"
#include "BdGr.h"
#include "bdanmgr.h"

#include "ctxtgr.h"
#include "Verif.h"
VerifInit;

#define TAILLE_TEXTE_BASE 12		// pass�e de 3 (taille arbitraire) en taille en pixel

#define b_fenetre_ctxtgr 628

typedef struct
  {
  DWORD id_fenetre;
  HBDGR	hbdgr;
  DWORD page;
  } CTXT_FENETRE_GR, *PCTXT_FENETRE_GR;

typedef struct
  {
  BOOL lancement_os;
  DWORD mode_edition;
  DWORD mode_trace;
  DWORD mode_couleur;
  BOOL dessin_en_cours;
  BOOL selection_en_cours;
  BOOL deplacement_en_cours;
  BOOL deplacement_contour;
  BOOL deformation_en_cours;
  BOOL duplication_en_cours;
  BOOL charge_symb_en_cours;
  G_ACTION trace;
  DWORD anim_rl;
  DWORD anim_indice_couleur2;
  DWORD anim_indice_couleur4;
  DWORD taille_texte;
  DWORD n_element_texte;
  DWORD n_id_texte;
  G_COULEUR couleur_trace;
  G_COULEUR couleur_fond;
  G_STYLE_LIGNE style_ligne;
  G_STYLE_REMPLISSAGE style_remplissage;
  DWORD IdPolice;
  DWORD style_police;
  DWORD type_curseur;
  LONG x_debut_selection;
  LONG y_debut_selection;
  LONG x_debut_translation;
  LONG y_debut_translation;
  LONG x1_boite_deformation;
  LONG y1_boite_deformation;
  LONG x2_boite_deformation;
  LONG y2_boite_deformation;
  BOOL draw;
  char draw_texte[80];
  char nom_presse_papier[82];
  char nom_symbole[164];
  DWORD page_origine_duplication;
  } CONTEXTE_EDITEUR;

static DWORD n_ctxtgr_fenetre;
static CONTEXTE_EDITEUR ContexteEditeur;


// -----------------------------------------------
void initialiser_ctxtgr_editeur (void)
  {
  enleve_bloc (b_fenetre_ctxtgr);
  ContexteEditeur.mode_edition = EN_TRACE;
  ContexteEditeur.mode_trace = TRACE_PLEIN;
  ContexteEditeur.mode_couleur = COULEUR_DESSIN;
  ContexteEditeur.dessin_en_cours = FALSE;
  ContexteEditeur.selection_en_cours = FALSE;
  ContexteEditeur.deplacement_en_cours = FALSE;
  ContexteEditeur.deplacement_contour = FALSE;
  ContexteEditeur.deformation_en_cours = FALSE;
  ContexteEditeur.duplication_en_cours = FALSE;
  ContexteEditeur.charge_symb_en_cours = FALSE;
  ContexteEditeur.trace = G_ACTION_LIGNE;
  ContexteEditeur.anim_rl = ANIMATION_R_1_LOG;
  ContexteEditeur.anim_indice_couleur2 = 0;
  ContexteEditeur.anim_indice_couleur4 = 0;
  ContexteEditeur.page_origine_duplication = 1;
  ContexteEditeur.taille_texte = TAILLE_TEXTE_BASE;
  ContexteEditeur.n_id_texte = 0;
  ContexteEditeur.n_element_texte = 0;
  ContexteEditeur.couleur_trace = G_GREEN;
  ContexteEditeur.couleur_fond = G_BLACK;
  ContexteEditeur.style_ligne = G_STYLE_LIGNE_CONTINUE;
  ContexteEditeur.style_remplissage = G_STYLE_REMPLISSAGE_PLEIN;
  ContexteEditeur.IdPolice = 0;
  ContexteEditeur.style_police = 0;
  ContexteEditeur.type_curseur = 0;
  ContexteEditeur.x_debut_selection = 0;
  ContexteEditeur.y_debut_selection = 0;
  ContexteEditeur.x_debut_translation = 0;
  ContexteEditeur.y_debut_translation = 0;
  ContexteEditeur.x1_boite_deformation = 0;
  ContexteEditeur.y1_boite_deformation = 0;
  ContexteEditeur.x2_boite_deformation = 0;
  ContexteEditeur.y2_boite_deformation = 0;
  ContexteEditeur.draw = FALSE;
  ContexteEditeur.draw_texte[0] = '\0';
  ContexteEditeur.nom_presse_papier[0] = '\0';
  ContexteEditeur.nom_symbole[0] = '\0';
  }

// -----------------------------------------------
void creer_ctxtgr_fenetre (void)
  {

  if (!existe_repere (b_fenetre_ctxtgr))
    {
    cree_bloc (b_fenetre_ctxtgr, sizeof (CTXT_FENETRE_GR), 0);
    }
  n_ctxtgr_fenetre = nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_ctxtgr) + 1;
  insere_enr (szVERIFSource, __LINE__, 1, b_fenetre_ctxtgr, n_ctxtgr_fenetre);
  }

// -----------------------------------------------
BOOL existe_ctxtgr_fenetre (void)
  {
  BOOL bRet = FALSE;

  if  (existe_repere (b_fenetre_ctxtgr))
    bRet = (nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_ctxtgr) != 0);

  return bRet;
  }

// -----------------------------------------------
void supprimer_ctxtgr_fenetre (void)
  {
  enleve_enr (szVERIFSource, __LINE__, 1, b_fenetre_ctxtgr, n_ctxtgr_fenetre);
  n_ctxtgr_fenetre = nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_ctxtgr);
  }

// -----------------------------------------------
// S�lectionne le premier contexte de fen�tre
BOOL selectionner_ctxtgr_premier (void)
  {
  BOOL existe;

  existe = FALSE;
  n_ctxtgr_fenetre = 0;
  if (existe_ctxtgr_fenetre())
    {
    n_ctxtgr_fenetre = 1;
    existe = TRUE;
    }
  return existe;
  }

// -----------------------------------------------
BOOL selectionner_ctxtgr_suivant (void)
// pas test existence bloc
  {
  BOOL existe = (n_ctxtgr_fenetre < nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_ctxtgr));

  if (existe)
    n_ctxtgr_fenetre++;

  return existe;
  }

// -----------------------------------------------
BOOL selectionner_ctxtgr_id_fenetre (DWORD id_fenetre)
  {
  BOOL existe;
  PCTXT_FENETRE_GR ptr_ctxtgr_fenetre;

  existe = FALSE;
  n_ctxtgr_fenetre = 0;
  while ((!existe) && (n_ctxtgr_fenetre < nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_ctxtgr)))
    {
    n_ctxtgr_fenetre++;
    ptr_ctxtgr_fenetre = (PCTXT_FENETRE_GR)pointe_enr (szVERIFSource, __LINE__, b_fenetre_ctxtgr, n_ctxtgr_fenetre);
    existe = (ptr_ctxtgr_fenetre->id_fenetre == id_fenetre);
    }
  return existe;
  }

// -----------------------------------------------
BOOL existe_ctxtgr_page (DWORD page, DWORD *id_fenetre)
  {
  BOOL existe;
  PCTXT_FENETRE_GR ptr_ctxtgr_fenetre;
  DWORD numero_ctxtgr_fenetre;

  existe = FALSE;
  if (existe_ctxtgr_fenetre ())
    {
    numero_ctxtgr_fenetre = 0;
    while ((!existe) && (numero_ctxtgr_fenetre < nb_enregistrements (szVERIFSource, __LINE__, b_fenetre_ctxtgr)))
      {
      numero_ctxtgr_fenetre++;
      ptr_ctxtgr_fenetre = (PCTXT_FENETRE_GR)pointe_enr (szVERIFSource, __LINE__, b_fenetre_ctxtgr, numero_ctxtgr_fenetre);
      existe = (ptr_ctxtgr_fenetre->page == page);
      }
    if (existe)
      *id_fenetre = ptr_ctxtgr_fenetre->id_fenetre;
    }
  return existe;
  }

// -----------------------------------------------
DWORD get_ctxtgr_id_fenetre (void)
  {
  PCTXT_FENETRE_GR ptr_ctxtgr_fenetre;

  ptr_ctxtgr_fenetre = (PCTXT_FENETRE_GR)pointe_enr (szVERIFSource, __LINE__, b_fenetre_ctxtgr, n_ctxtgr_fenetre);
  return ptr_ctxtgr_fenetre->id_fenetre;
  }

// -----------------------------------------------
HBDGR get_ctxtgr_hBdGr (void)
  {
  PCTXT_FENETRE_GR ptr_ctxtgr_fenetre;

  if (existe_ctxtgr_fenetre())
    {
    ptr_ctxtgr_fenetre = (PCTXT_FENETRE_GR)pointe_enr (szVERIFSource, __LINE__, b_fenetre_ctxtgr, n_ctxtgr_fenetre);
    return ptr_ctxtgr_fenetre->hbdgr;
    }
  else
    return NULL;
  }

// -----------------------------------------------
BOOL get_ctxtgr_lancement_os (void)
  {
  return ContexteEditeur.lancement_os;
  }

// -----------------------------------------------
DWORD CtxtgrGetModeEdition (void)
  {
  return ContexteEditeur.mode_edition;
  }

// -----------------------------------------------
DWORD get_ctxtgr_mode_trace (void)
  {
  return ContexteEditeur.mode_trace;
  }

// -----------------------------------------------
DWORD get_ctxtgr_mode_couleur (void)
  {
  return ContexteEditeur.mode_couleur;
  }

// -----------------------------------------------
BOOL get_ctxtgr_dessin_en_cours (void)
  {
  return ContexteEditeur.dessin_en_cours;
  }

// -----------------------------------------------
BOOL get_ctxtgr_selection_en_cours (void)
  {
  return ContexteEditeur.selection_en_cours;
  }

// -----------------------------------------------
BOOL get_ctxtgr_deplacement_en_cours (void)
  {
  return ContexteEditeur.deplacement_en_cours;
  }

// -----------------------------------------------
BOOL get_ctxtgr_deplacement_contour (void)
  {
  return ContexteEditeur.deplacement_contour;
  }

// -----------------------------------------------
BOOL get_ctxtgr_deformation_en_cours (void)
  {
  return ContexteEditeur.deformation_en_cours;
  }

// -----------------------------------------------
BOOL get_ctxtgr_duplication_en_cours (void)
  {
  return ContexteEditeur.duplication_en_cours;
  }

// -----------------------------------------------
BOOL get_ctxtgr_charge_symb_en_cours (void)
  {
  return ContexteEditeur.charge_symb_en_cours;
  }

// -----------------------------------------------
DWORD get_ctxtgr_anim_rl (void)
  {
  return ContexteEditeur.anim_rl;
  }

// -----------------------------------------------
DWORD get_ctxtgr_anim_indice_couleur2 (void)
  {
  return ContexteEditeur.anim_indice_couleur2;
  }

// -----------------------------------------------
DWORD get_ctxtgr_anim_indice_couleur4 (void)
  {
  return ContexteEditeur.anim_indice_couleur4;
  }

// -----------------------------------------------
G_ACTION CtxtgrGetAction (void)
  {
  return ContexteEditeur.trace;
  }

// -----------------------------------------------
DWORD get_ctxtgr_page_origine_dup (void)
  {
  return ContexteEditeur.page_origine_duplication;
  }

// -----------------------------------------------
DWORD get_ctxtgr_taille_texte (void)
  {
  return ContexteEditeur.taille_texte;
  }

// -----------------------------------------------
DWORD get_ctxtgr_n_id_texte (void)
  {
  return ContexteEditeur.n_id_texte;
  }

// -----------------------------------------------
DWORD get_ctxtgr_n_elt_texte (void)
  {
  return ContexteEditeur.n_element_texte;
  }

// -----------------------------------------------
G_COULEUR get_ctxtgr_couleur_trace (void)
  {
  return ContexteEditeur.couleur_trace;
  }

// -----------------------------------------------
G_COULEUR get_ctxtgr_couleur_fond (void)
  {
  return ContexteEditeur.couleur_fond;
  }

// -----------------------------------------------
G_STYLE_LIGNE get_ctxtgr_style_ligne (void)
  {
  return ContexteEditeur.style_ligne;
  }

// -----------------------------------------------
G_STYLE_REMPLISSAGE get_ctxtgr_style_remplissage (void)
  {
  return ContexteEditeur.style_remplissage;
  }

// -----------------------------------------------
DWORD get_ctxtgr_police (void)
  {
  return ContexteEditeur.IdPolice;
  }

// -----------------------------------------------
DWORD get_ctxtgr_style_police (void)
  {
  return ContexteEditeur.style_police;
  }

// -----------------------------------------------
DWORD get_ctxtgr_type_curseur (void)
  {
  return ContexteEditeur.type_curseur;
  }

// -----------------------------------------------
void get_ctxtgr_pos_debut_selection (LONG *x, LONG *y)
  {
  *x = ContexteEditeur.x_debut_selection;
  *y = ContexteEditeur.y_debut_selection;
  }

// -----------------------------------------------
void get_ctxtgr_pos_debut_translat (LONG *x, LONG *y)
  {
  *x = ContexteEditeur.x_debut_translation;
  *y = ContexteEditeur.y_debut_translation;
  }

// -----------------------------------------------
void get_ctxtgr_boite_init_deform (LONG *x1, LONG *y1, LONG *x2, LONG *y2)
  {
  *x1 = ContexteEditeur.x1_boite_deformation;
  *y1 = ContexteEditeur.y1_boite_deformation;
  *x2 = ContexteEditeur.x2_boite_deformation;
  *y2 = ContexteEditeur.y2_boite_deformation;
  }

// -----------------------------------------------
BOOL get_ctxtgr_draw (void)
  {
  return ContexteEditeur.draw;
  }

// -----------------------------------------------
char *get_ctxtgr_draw_texte (void)
  {
  return ContexteEditeur.draw_texte;
  }

// -----------------------------------------------
char *get_ctxtgr_nom_presse_papier (void)
  {
  return ContexteEditeur.nom_presse_papier;
  }

// -----------------------------------------------
char *get_ctxtgr_nom_symbole (void)
  {
  return ContexteEditeur.nom_symbole;
  }

// -----------------------------------------------
void set_ctxtgr_id_fenetre (DWORD id_fenetre)
  {
  PCTXT_FENETRE_GR ptr_ctxtgr_fenetre;

  ptr_ctxtgr_fenetre = (PCTXT_FENETRE_GR)pointe_enr (szVERIFSource, __LINE__, b_fenetre_ctxtgr, n_ctxtgr_fenetre);
  ptr_ctxtgr_fenetre->id_fenetre = id_fenetre;
  }

// -----------------------------------------------
void set_ctxtgr_hbdgr (HBDGR hbdgr)
  {
  PCTXT_FENETRE_GR ptr_ctxtgr_fenetre;

  ptr_ctxtgr_fenetre = (PCTXT_FENETRE_GR)pointe_enr (szVERIFSource, __LINE__, b_fenetre_ctxtgr, n_ctxtgr_fenetre);
  ptr_ctxtgr_fenetre->hbdgr = hbdgr;
  }

// -----------------------------------------------
void set_ctxtgr_page (DWORD ctxt_page)
  {
  PCTXT_FENETRE_GR ptr_ctxtgr_fenetre;

  ptr_ctxtgr_fenetre = (PCTXT_FENETRE_GR)pointe_enr (szVERIFSource, __LINE__, b_fenetre_ctxtgr, n_ctxtgr_fenetre);
  ptr_ctxtgr_fenetre->page = ctxt_page;
  }

// -----------------------------------------------
void set_ctxtgr_lancement_os (BOOL lancement_os)
  {
  ContexteEditeur.lancement_os = lancement_os;
  }

// -----------------------------------------------
void CtxtgrSetModeEdition (DWORD mode)
  {
  ContexteEditeur.mode_edition = mode;
  }

// -----------------------------------------------
void set_ctxtgr_mode_trace (DWORD mode)
  {
  ContexteEditeur.mode_trace = mode;
  }

// -----------------------------------------------
void set_ctxtgr_mode_couleur (DWORD mode)
  {
  ContexteEditeur.mode_couleur = mode;
  }

// -----------------------------------------------
void set_ctxtgr_dessin_en_cours (BOOL mode)
  {
  ContexteEditeur.dessin_en_cours = mode;
  }

// -----------------------------------------------
void set_ctxtgr_selection_en_cours (BOOL mode)
  {
  ContexteEditeur.selection_en_cours = mode;
  }

// -----------------------------------------------
void set_ctxtgr_deplacement_en_cours (BOOL mode)
  {
  ContexteEditeur.deplacement_en_cours = mode;
  }

// -----------------------------------------------
void set_ctxtgr_deplacement_contour (BOOL mode)
  {
  ContexteEditeur.deplacement_contour = mode;
  }

// -----------------------------------------------
void set_ctxtgr_deformation_en_cours (BOOL mode)
  {
  ContexteEditeur.deformation_en_cours = mode;
  }

// -----------------------------------------------
void set_ctxtgr_duplication_en_cours (BOOL mode)
  {
  ContexteEditeur.duplication_en_cours = mode;
  }

// -----------------------------------------------
void set_ctxtgr_charge_symb_en_cours (BOOL mode)
  {
  ContexteEditeur.charge_symb_en_cours = mode;
  }

// -----------------------------------------------
void set_ctxtgr_anim_rl (DWORD anim_rl)
  {
  ContexteEditeur.anim_rl = anim_rl;
  }

// -----------------------------------------------
void set_ctxtgr_anim_indice_couleur2 (DWORD indice_couleur)
  {
  if (indice_couleur < 2)
    {
    ContexteEditeur.anim_indice_couleur2 = indice_couleur;
    }
  else
    {
    ContexteEditeur.anim_indice_couleur2 = 0;
    }
  }

// -----------------------------------------------
void set_ctxtgr_anim_indice_couleur4 (DWORD indice_couleur)
  {
  if (indice_couleur < 4)
    {
    ContexteEditeur.anim_indice_couleur4 = indice_couleur;
    }
  else
    {
    ContexteEditeur.anim_indice_couleur4 = 0;
    }
  }

// -----------------------------------------------
void CtxtgrSetAction (G_ACTION trace)
  {
  ContexteEditeur.trace = trace;
  }

// -----------------------------------------------
void set_ctxtgr_page_origine_dup (DWORD page)
  {
  ContexteEditeur.page_origine_duplication = page;
  }

// -----------------------------------------------
void set_ctxtgr_taille_texte (DWORD ctxt_taille_texte)
  {
  ContexteEditeur.taille_texte = ctxt_taille_texte;
  }

// -----------------------------------------------
void set_ctxtgr_n_elt_texte (DWORD ctxt_n_elt_texte)
  {
  ContexteEditeur.n_element_texte = ctxt_n_elt_texte;
  }

// -----------------------------------------------
void set_ctxtgr_n_id_texte (DWORD ctxt_n_id_texte)
  {
  ContexteEditeur.n_id_texte = ctxt_n_id_texte;
  }

// -----------------------------------------------
void set_ctxtgr_couleur_trace (G_COULEUR couleur_trace)
  {
  ContexteEditeur.couleur_trace = couleur_trace;
  }

// -----------------------------------------------
void set_ctxtgr_couleur_fond (G_COULEUR couleur_fond)
  {
  ContexteEditeur.couleur_fond = couleur_fond;
  }

// -----------------------------------------------
void set_ctxtgr_style_ligne (G_STYLE_LIGNE style_ligne)
  {
  ContexteEditeur.style_ligne = style_ligne;
  }

// -----------------------------------------------
void set_ctxtgr_style_remplissage (G_STYLE_REMPLISSAGE style_remplissage)
  {
  ContexteEditeur.style_remplissage = style_remplissage;
  }

// -----------------------------------------------
void set_ctxtgr_police (DWORD IdPolice)
  {
  ContexteEditeur.IdPolice = IdPolice;
  }

// -----------------------------------------------
void set_ctxtgr_style_police (DWORD style_police)
  {
  ContexteEditeur.style_police = style_police;
  }

// -----------------------------------------------
void set_ctxtgr_type_curseur (DWORD type_curseur)
  {
  ContexteEditeur.type_curseur = type_curseur;
  }

// -----------------------------------------------
void set_ctxtgr_pos_debut_selection (LONG x, LONG y)
  {
  ContexteEditeur.x_debut_selection = x;
  ContexteEditeur.y_debut_selection = y;
  }

// -----------------------------------------------
void set_ctxtgr_pos_debut_translat (LONG x, LONG y)
  {
  ContexteEditeur.x_debut_translation = x;
  ContexteEditeur.y_debut_translation = y;
  }

// -----------------------------------------------
void set_ctxtgr_boite_init_deform (LONG x1, LONG y1, LONG x2, LONG y2)
  {
  ContexteEditeur.x1_boite_deformation = x1;
  ContexteEditeur.y1_boite_deformation = y1;
  ContexteEditeur.x2_boite_deformation = x2;
  ContexteEditeur.y2_boite_deformation = y2;
  }

// -----------------------------------------------
void set_ctxtgr_draw (BOOL draw)
  {
  ContexteEditeur.draw = draw;
  }

// -----------------------------------------------
void set_ctxtgr_draw_texte (char *texte)
  {
  StrCopy (ContexteEditeur.draw_texte, texte);
  }

// -----------------------------------------------
void set_ctxtgr_nom_presse_papier (char *texte)
  {
  StrCopy (ContexteEditeur.nom_presse_papier, texte);
  }

// -----------------------------------------------
void set_ctxtgr_nom_symbole (char *texte)
  {
  StrCopy (ContexteEditeur.nom_symbole, texte);
  }

/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : pcsgr.h                                                 |
 |   Auteur  : AC					        	|
 |   Date    : 26/10/92					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

#define MODE_DEMARRAGE  0
#define MODE_INTERACTIF 1
#define MODE_MACRO      2

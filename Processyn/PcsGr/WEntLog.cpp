// WEntLog.c
// Gestion d'une fen�tre entr�e logique pour processyn GR
// $$ am�liorer sur :
// passage de param�tres au parent lors de l'activation et de l'initialisation
// Chargement du curseur initial
// classe propre au lieu du sous classement d'un bouton
// extension possible avec gestion d'un titre et dessin d'un bouton ou case � cocher etc ...
#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "Appli.h"
#include "UEnv.h"
#include "IdExeLng.h"

#include "WEntLog.h"

static CHAR szClasseEntLog [] = "ClasseEntLog";

typedef struct
	{
	BOOL	bEnCaptureSouris;
	BOOL	bALeFocus;
	} ENV_ENTLOG;
typedef ENV_ENTLOG * PENV_ENTLOG;

// --------------------------------------------------------------------------
// wndproc d'un bouton transparent utilis�e pour les entr�es logiques dans les synoptiques
static LRESULT CALLBACK wndprocEntreeLogique (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT			mres = 0;
  BOOL				bTraite = TRUE;

  switch (msg)
    {
		case WM_CREATE:
			{
			PENV_ENTLOG pEnvEntLog = (PENV_ENTLOG)pCreeEnv (hwnd, sizeof(ENV_ENTLOG));
			pEnvEntLog->bEnCaptureSouris = FALSE;
			pEnvEntLog->bALeFocus = FALSE;
			}
			break;

		case WM_GETDLGCODE:
			mres = DLGC_WANTALLKEYS; // Traite toutes les touches y compris TAB, entr�e, esc ...; //|DLGC_UNDEFPUSHBUTTON|DLGC_WANTARROWS |DLGC_WANTCHARS|
			break;

		case WM_DESTROY:
			bLibereEnv(hwnd);
			break;

		case WM_SETFOCUS:
			{
			PENV_ENTLOG pEnvEntLog= (PENV_ENTLOG)pGetEnv (hwnd);
				
			pEnvEntLog->bALeFocus = TRUE;

			// redessin pour afficher le focus
			::InvalidateRect (hwnd, NULL, TRUE);
			}
			break;

		case WM_KILLFOCUS:
			{
			PENV_ENTLOG pEnvEntLog= (PENV_ENTLOG)pGetEnv (hwnd);
			RECT				rectWindow;

			pEnvEntLog->bALeFocus = FALSE;
			// capture en cours ?
			if (pEnvEntLog->bEnCaptureSouris)
				{
				// oui => plus de capture
				pEnvEntLog->bEnCaptureSouris = FALSE;
        ReleaseCapture ();
				}
			// redessin de la zone occup�e par la fen�tre pour effacer le focus
			::GetWindowRect (hwnd, &rectWindow);
			::MapWindowPoints (NULL, ::GetParent (hwnd), (LPPOINT)(&rectWindow), 2); // $$ cast officiel mais moche
			::InvalidateRect (::GetParent (hwnd), &rectWindow, TRUE);
			}
			break;

    case WM_LBUTTONDOWN:
			{
			// bouton baiss� => capture la souris
			PENV_ENTLOG pEnvEntLog= (PENV_ENTLOG)pGetEnv (hwnd);

			pEnvEntLog->bEnCaptureSouris = TRUE;
			SetCapture(hwnd);
			// et capture le focus
			SetFocus (hwnd);
			}
      break;

    case WM_CHAR:
			{ 
			// touche Entr�e utilis�e ?
			char		c = LOBYTE (mp1);
			
			switch (c)
				{
				case VK_TAB:
					{
					// Dis � la boite de dialogue d'activer le contr�le pr�c�dent (Shift Tab) ou suivant (Tab)
					BOOL	bShift = ((0x8000 & GetKeyState (VK_SHIFT))!= 0);

					::PostMessage (::GetParent (hwnd), WM_NEXTDLGCTL, bShift, FALSE);
					}
					break;
				case VK_RETURN:
					// oui => notification du "click" au parent
					::SendMessage (::GetParent(hwnd), WM_COMMAND, 
						MAKEWPARAM (::GetDlgCtrlID (hwnd), BN_CLICKED), (LPARAM)hwnd);
					break;
				case VK_ESCAPE:
					// oui => donne le focus au parent
					SetFocus (::GetParent(hwnd));
					break;
				}
			}
			break;

    case WM_LBUTTONUP: 
			{
			// lever du bouton : capture en cours ?
			PENV_ENTLOG pEnvEntLog= (PENV_ENTLOG)pGetEnv (hwnd);
			if (pEnvEntLog->bEnCaptureSouris)
				{
				int x = (SHORT)LOWORD(mp2);
				int y = (SHORT)HIWORD(mp2);
				RECT rect;

				// oui => plus de capture
				pEnvEntLog->bEnCaptureSouris = FALSE;
        ReleaseCapture ();

				// dans la zone cliente ?
        ::GetClientRect (hwnd, &rect);
        if ((x >= rect.left) && (y > rect.top) && (x < rect.right) & (y < rect.bottom))
          {
					// oui => notification du "click" au parent
					::SendMessage (::GetParent(hwnd), WM_COMMAND, 
						MAKEWPARAM (::GetDlgCtrlID(hwnd), BN_CLICKED), (LPARAM)hwnd);
			    }
        }
			}
      break;

    case WM_PAINT:
			{
			// dessin �ventuel du focus
			PENV_ENTLOG pEnvEntLog= (PENV_ENTLOG)pGetEnv (hwnd);
			PAINTSTRUCT ps;
			RECT	rectClient;

			::GetClientRect (hwnd, &rectClient);
      ::BeginPaint (hwnd,&ps);

			// dessine �ventuellement le focus
			if (pEnvEntLog->bALeFocus)
				DrawFocusRect (ps.hdc, &rectClient);

      ::EndPaint (hwnd,&ps);
			}
      break;

		case WM_ERASEBKGND:
			// pas d'effacement
			mres = TRUE;
			break;

    default:
      bTraite = FALSE;
      break;
    }

  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2); 

  return mres;
  } // wndprocEntreeLogique


// --------------------------------------------------------------------------------------------------
// cr�ation d'une fen�tre entr�e logique (pour animation de synoptique)
HWND hwndCreeFenetreEntLog (HWND hwndParent, INT x, INT y, INT cx, INT cy, UINT uId)
	{
	HWND hwndRet = NULL;
	static BOOL bClasseEnregistree = FALSE;

	// cr�ation de la classe de la fen�tre � la vol�e
	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= CS_PARENTDC; // un DC est allou� en permanence pour la fen�tre
		wc.lpfnWndProc		= wndprocEntreeLogique; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (Appli.hinstDllLng, MAKEINTRESOURCE (IDP_DOIGT));
		wc.hbrBackground	= NULL; //pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseEntLog; 
		bClasseEnregistree = RegisterClass (&wc);
		}
  if (bClasseEnregistree)
    {
		hwndRet = CreateWindowEx (WS_EX_TRANSPARENT, szClasseEntLog, "", BS_PUSHBUTTON | WS_CHILD | WS_TABSTOP | WS_VISIBLE, //$$BS_GROUPBOX | WS_GROUP | WS_VISIBLE
			x, y,cx,cy, hwndParent, (HMENU)uId, Appli.hinst, NULL);
		}

	return hwndRet;
	}




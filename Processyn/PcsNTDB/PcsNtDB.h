//---------------------------------------------------------------------------
//											PcsNtDB.h
//
// Interface de la DLL PcsNtDB.dll.
//
// Cette DLL a �t� permet � une DLL utilisateur de Processyn d'acc�der
// aux variables de la base de donn�es Proceessyn NT.
//---------------------------------------------------------------------------


// types des variables de la base de donn�e Processyn
#define TAILLE_MESSAGE 82 // 82 caract�res ANSII y compris le 0 de fin de chaine
#define TAILLE_VARIABLE 22 // 22 caract�res ANSII y compris le 0 de fin de chaine

typedef float   NUMERIQUE, *PNUMERIQUE;
typedef BOOL    LOGIQUE, *PLOGIQUE;
typedef char    MESSAGE [TAILLE_MESSAGE];

// constantes utilis�es par certaines fonctions (leur utilisation est comment�e)
#define VARIABLE_INCONNUE           -1
#define TYPE_LOGIQUE                11
#define TYPE_NUMERIQUE              12
#define TYPE_MESSAGE                13

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//	Ouverture de l'acc�s � Processyn NT en exploitation
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
// Enregistre le handle de Processyn pour pouvoir y acc�der dans les appels suivants
//---------------------------------------------------------------------------
void WINAPI PcsNtDBHandleProcessyn
	(
	HANDLE hProcessyn	// handle � enregistrer (pass� par Processyn � la DLL utilisateur)
	);

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//	Fonctions de renseignements sur les variables de Processyn
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
// R�cup�re le type d'une variable.
//---------------------------------------------------------------------------
int WINAPI GetGenreVar
	(
	PCSTR pszNomVar	// Adresse du nom de la variable cible.
	);							// Retour : VARIABLE_INCONNUE, TYPE_LOGIQUE, TYPE_NUMERIQUE ou TYPE_MESSAGE

//---------------------------------------------------------------------------
// R�cup�re la taille d'un tableau.
//---------------------------------------------------------------------------
int WINAPI GetTailleVar
	(
	PCSTR pszNomVar	// Adresse du nom du tableau cible.
	);							// Retour : VARIABLE_INCONNUE, 0  si ce n'est pas un tableau, sinon la taille du tableau

//---------------------------------------------------------------------------
// R�cup�re le contenu de la variable syst�me Processyn 'REG_NUM1' au moment
// de l'appel de la DLL. Cette valeur peut �tre utilis�e par la DLL en tant que
// num�ro de fonction appel�e dans la DLL.
//---------------------------------------------------------------------------
int WINAPI GetNumeroFonction
	(void); // Retour : le contenu de la variable 'REG_NUM1' au moment de l'appel ou VARIABLE_INCONNUE.

//---------------------------------------------------------------------------
// Utilitaire : force la longueur d'une chaine de caract�res � TAILLE_MESSAGE
// caract�res maxi (caract�re 0 de fin de chaine compris), afin d'�viter un 
// d�bordement lors de la recopie dans une variable de type MESSAGE.
//---------------------------------------------------------------------------
BOOL WINAPI FixStrLen 
	(
	char *pszStr		// Adresse de la chaine � traiter.
	); // Renvoie TRUE si la chaine a �t� tronqu�e

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//								ACCES AUX VALEURS DES VARIABLES DE PROCESSYN
//
//	Nomenclature : [Get ou Set] [Val, Param ou Tab] [Logique, Numerique ou Message]
//
//		Get :	r�cup�re la valeur de la variable processyn.
//		Set : affecte la valeur de la variable processyn.
//
//		Val : acc�s � une variable (NON tableau) dont le nom est sp�cifi�.
//		Tab :	acc�s � la variable (tableau ou non) dont l'index et le nom sont sp�cifi�s.
//		Param : acc�s � la variable (tableau ou non) dont l'index est sp�cifi� et 
//						dont le nom est lu dans la variable syst�me suivante :
//							REG_MES1 si l'on acc�de � une une variable Message.
//							REG_MES2 si l'on acc�de � une une variable Numerique.
//							REG_MES3 si l'on acc�de � une une variable Logique.
//
//		Message	: l'op�ration concerne une variable message.
//		Numerique : l'op�ration concerne une variable numerique.
//		Logique	: l'op�ration concerne une variable logique.
//
// Valeur de retour : TRUE si l'op�ration a pu se faire, FALSE sinon
// Note : pour les fonctions Tab et Param, un num�ro d'indice est sp�cifi�; il doit
//				valoir 0 si la variable sp�cifi�e n'est PAS un tableau. Sinon il est
//				le num�ro d'indice (de 1 � n) de la valeur acc�d�e dans le tableau.
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
// R�cup�re la valeur de la variable message 'pszNomVar' dans pszBuffer.
//---------------------------------------------------------------------------
BOOL WINAPI GetValMessage
	(
	PCSTR pszNomVar,		// Adresse du nom de la variable cible
	PSTR	pszBuffer,		// Adresse du buffer recevant les caract�res de la variable
	DWORD dwTailleBuffer// Nombre d'octets du buffer (doit pouvoir contenir chaine et car 0)
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Affecte la variable message 'pszNomVar' avec la valeur pszBuffer.
//---------------------------------------------------------------------------
BOOL WINAPI SetValMessage
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	PSTR pszBuffer			// Valeur � affecter � la variable
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// R�cup�re la valeur de la variable num�rique 'pszNomVar' dans *pfNum.
//---------------------------------------------------------------------------
BOOL WINAPI GetValNumerique
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	PNUMERIQUE pfNum		// Adresse du r�el recevant la valeur de la variable
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Affecte la variable num�rique 'pszNomVar' avec la valeur fNum.
//---------------------------------------------------------------------------
BOOL WINAPI SetValNumerique
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	NUMERIQUE fNum			// Valeur � affecter � la variable
	);									// Retour : TRUE si la variable � �t� trouv�e, FALSE sinon.

//---------------------------------------------------------------------------
// R�cup�re la valeur de la variable logique 'pszNomVar' dans *pbLog.
//---------------------------------------------------------------------------
BOOL WINAPI GetValLogique
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	PLOGIQUE pbLog			// Adresse du logique recevant la valeur de la variable
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Affecte la variable logique 'pszNomVar' avec la valeur Log.
//---------------------------------------------------------------------------
BOOL WINAPI SetValLogique
	(
	PCSTR pszNomVar, 	// Adresse du nom de la variable cible
	LOGIQUE Log				// Valeur � affecter � la variable
	);								// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// R�cup�re la valeur de la variable message 'pszNomVar' dans pszBuffer.
//---------------------------------------------------------------------------
BOOL WINAPI GetTabMessage
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	int	nNumero, 				// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	PSTR	pszBuffer,		// Adresse du buffer recevant les caract�res de la variable
	DWORD dwTailleBuffer// Nombre d'octets du buffer (doit pouvoir contenir chaine et car 0)
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Affecte la variable message 'pszNomVar' avec la valeur pszBuffer.
//---------------------------------------------------------------------------
BOOL WINAPI SetTabMessage
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	int nNumero, 				// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	PSTR pszBuffer			// Valeur � affecter � la variable
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// R�cup�re la valeur de la variable num�rique 'pszNomVar' dans *pfNum.
//---------------------------------------------------------------------------
BOOL WINAPI GetTabNumerique
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	int nNumero, 				// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	PNUMERIQUE pfNum		// Adresse du r�el recevant la valeur de la variable
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Affecte la variable num�rique 'pszNomVar' avec la valeur fNum.
//---------------------------------------------------------------------------
BOOL WINAPI SetTabNumerique
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	int nNumero, 				// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	NUMERIQUE fNum			// Valeur � affecter � la variable
	);									// Retour : TRUE si la variable � �t� trouv�e, FALSE sinon.

//---------------------------------------------------------------------------
// R�cup�re la valeur de la variable logique 'pszNomVar' dans *pbLog.
//---------------------------------------------------------------------------
BOOL WINAPI GetTabLogique
	(
	PCSTR pszNomVar, 		// Adresse du nom de la variable cible
	int nNumero, 				// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	PLOGIQUE pbLog			// Adresse du logique recevant la valeur de la variable
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Affecte la variable logique 'pszNomVar' avec la valeur Log.
//---------------------------------------------------------------------------
BOOL WINAPI SetTabLogique
	(
	PCSTR pszNomVar, 	// Adresse du nom de la variable cible
	int nNumero, 			// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	LOGIQUE Log				// Valeur � affecter � la variable
	);								// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Copie le texte d'une variable message processyn (nom en REG_MES1)
//---------------------------------------------------------------------------
BOOL WINAPI GetParamMessage
	(
	int		nNumero,			// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	PSTR	pszBuffer,		// Adresse du buffer recevant le texte de la variable
	DWORD dwTailleBuffer// Nombre d'octets du buffer (doit pouvoir contenir chaine et car 0)
	);									// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Copie un texte dans la variable message processyn (nom en REG_MES1)
//---------------------------------------------------------------------------
BOOL WINAPI SetParamMessage
	(
	int		nNumero,		// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	PSTR pszBuffer		// Adresse du texte � copier
	);								// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// R�cup�re la valeur d'une variable processyn num�rique (nom en REG_MES2)
//---------------------------------------------------------------------------
BOOL WINAPI GetParamNumerique 
	(
	int nNumero,			// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	PNUMERIQUE pfNum	// Adresse du r�el recevant la valeur de la variable
	);								// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// Copie une valeur num�rique dans une variable num�rique processyn (nom en REG_MES2)
//---------------------------------------------------------------------------
BOOL WINAPI SetParamNumerique
	(
	int nNumero, 			// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	NUMERIQUE fNum		// Valeur du r�el � copier
	);								// Retour : TRUE si op�ration r�ussie, FALSE sinon.

//---------------------------------------------------------------------------
// R�cup�re la valeur d'une variable processyn logique (nom en REG_MES3)
//---------------------------------------------------------------------------
BOOL WINAPI GetParamLogique
	(
	int nNumero,			// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	PLOGIQUE pbLog		// Adresse du BOOL recevant la valeur de la variable
	);

//---------------------------------------------------------------------------
// Copie une valeur logique dans une variable logique processyn (nom en REG_MES3)
//---------------------------------------------------------------------------
BOOL WINAPI SetParamLogique
	(
	int nNumero,			// 0 ou num�ro d'indice de la variable dans le tableau (de 1 � n)
	LOGIQUE Log				// Valeur du BOOL � copier
	);

//---------------------- fin PcsNtDB.h -------------------------

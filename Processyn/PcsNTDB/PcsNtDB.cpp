//-------------------------------------------------------------
// PcsNtDB.c		DLL d'acc�s � la base de donn�es de Processyn NT
// Version WIN32	29/9/97
// La DLL utilisateur PcsUsrNT.DLL appelle les services de cette DLL pour acc�der 
// � la base de donn�es de Processyn NT.
//-------------------------------------------------------------
#include <windows.h>
#include "pcs_sys.h"
#include "lng_res.h"
#include "tipe.h"
#include "DllMuxEx.h"
#include "PcsNtDB.h"

// V�rification des red�finitions de constantes
#if TAILLE_MESSAGE == c_nb_car_ex_mes
#else
#error incoh�rence : TAILLE_MESSAGE != c_nb_car_ex_mes
#endif

#if TAILLE_VARIABLE == c_nb_car_es
#else
#error incoh�rence : TAILLE_VARIABLE != c_nb_car_es
#endif

//---------------------------------------------------------------------------
// Variables locales

// instance de la DLL
static HINSTANCE	hInst;

// handle du multiplexeur Pcs en exploitation
static HANDLE hMuxPcs = NULL;

//---------------------------------------------------------------------------
//                            fonctions Locales
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Ne copie Source dans dest que si la taille de la chaine source peut �tre
// contenue dans Dest
static BOOL bCopieStrLimite (PSTR pszDest, PCSTR pszSource, DWORD dwTailleDest)
	{
	BOOL bRes = FALSE;

	if (strlen (pszSource) < dwTailleDest)
		{
		strcpy_s (pszDest, dwTailleDest,pszSource);
		bRes = TRUE;
		}
	return bRes;
	}

//---------------------------------------------------------------------------
// Lit ou �crit une variable logique (appel du Mux Processyn)
static BOOL bValeurVarLogique 
	(
	PCSTR pszNomVar,	// Nom de la variable � rechercher.
	DWORD wIndice,		// 0 ou indice tableau (1..n)
	BOOL	bAffectePcs,// TRUE si �criture Var processyn , FALSE si lecture
	PLOGIQUE pLog			// adresse de la variable transf�r�e
	)									// Retour : TRUE si op�ration effectu�e
	{
	// pr�pare le message pour le multiplexeur Processyn
  BOOL											bRes = FALSE;
	PARAM_MXDB_COPIE_VAR_LOG	Mes;
		
	Mes.header.dwTaille = sizeof (Mes);
	Mes.header.uIdService = ID_SERVICE_MX_DB_PCS;
	Mes.header.uIdFonction = ID_FONCTION_MXDB_COPIE_VAR_LOG;
	Mes.bAffecte = bAffectePcs;							// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	Mes.dwIndex = wIndice;									// 0 ou index de tableau (1..n)
	Mes.bValeur = *pLog;

	if (bCopieStrLimite (Mes.szNomVar, pszNomVar, sizeof (Mes.szNomVar)))
		bRes = bAppelMuxOk (hMuxPcs, &Mes.header);

	if (bRes && (!bAffectePcs))
		*pLog = Mes.bValeur;

	return bRes;
	}

//---------------------------------------------------------------------------
// Lit ou �crit une variable num�rique
static BOOL bValeurVarNumerique 
	(
	PCSTR pszNomVar,		// Nom de la variable � rechercher.
	DWORD wIndice,			// 0 ou indice tableau (1..n)
	BOOL	bAffectePcs,	// TRUE si �criture Var processyn , FALSE si lecture
	PNUMERIQUE pNum			// adresse de la variable transf�r�e
	)										// Retour : TRUE si op�ration effectu�e
	{
	// pr�pare le message pour le multiplexeur Processyn
  BOOL											bRes = FALSE;
	PARAM_MXDB_COPIE_VAR_NUM	Mes;
		
	Mes.header.dwTaille = sizeof (Mes);
	Mes.header.uIdService = ID_SERVICE_MX_DB_PCS;
	Mes.header.uIdFonction = ID_FONCTION_MXDB_COPIE_VAR_NUM;
	Mes.bAffecte = bAffectePcs;							// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	Mes.dwIndex = wIndice;									// 0 ou index de tableau (1..n)
	Mes.fValeur = *pNum;

	if (bCopieStrLimite (Mes.szNomVar, pszNomVar, sizeof (Mes.szNomVar)))
		// message Ok => envoi synchrone au multiplexeur Processyn
		bRes = bAppelMuxOk (hMuxPcs, &Mes.header);

	if (bRes && (!bAffectePcs))
		*pNum = Mes.fValeur;

	return bRes;
	}

//---------------------------------------------------------------------------
// Lit ou �crit une variable message
static BOOL bValeurVarMessage 
	(
	PCSTR pszNomVar,		// Nom de la variable � rechercher.
	DWORD wIndice,			// 0 ou indice tableau (1..n)
	BOOL	bAffectePcs,	// TRUE si �criture Var processyn , FALSE si lecture
	PSTR  pszMes,				// adresse de la variable transf�r�e
	DWORD dwTailleMes		// Nb octets allou�s � la chaine (terminateur 0 compris)
	)										// Retour : TRUE si op�ration effectu�e
	{
	// pr�pare le message pour le multiplexeur Processyn
  BOOL											bRes = FALSE;
	BOOL											bErr = FALSE;
	PARAM_MXDB_COPIE_VAR_MES	Mes;
		
	Mes.header.dwTaille = sizeof (Mes);
	Mes.header.uIdService = ID_SERVICE_MX_DB_PCS;
	Mes.header.uIdFonction = ID_FONCTION_MXDB_COPIE_VAR_MES;
	Mes.bAffecte = bAffectePcs;							// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	Mes.dwIndex = wIndice;									// 0 ou index de tableau (1..n)

	if (bAffectePcs)
		bErr = !bCopieStrLimite (Mes.szValeur, pszMes, sizeof (Mes.szValeur));

	if (!bErr && bCopieStrLimite (Mes.szNomVar, pszNomVar, sizeof (Mes.szNomVar)))
		// message Ok => envoi synchrone au multiplexeur Processyn
		bRes = bAppelMuxOk (hMuxPcs, &Mes.header);

	if (bRes && (!bAffectePcs))
		bRes = bCopieStrLimite (pszMes, Mes.szValeur, dwTailleMes);

	return bRes;
	}

//---------------------------------------------------------------------------
// Lit ou �crit une variable logique
static BOOL bValeurVarSysLogique 
	(
	DWORD dwIdVarSys,	// Id de la variable � rechercher.
	DWORD wIndice,		// 0 ou indice tableau (1..n)
	BOOL	bAffectePcs,// TRUE si �criture Var processyn , FALSE si lecture
	PLOGIQUE pLog			// adresse de la variable transf�r�e
	)									// Retour : TRUE si op�ration effectu�e
	{
	// pr�pare le message pour le multiplexeur Processyn
  BOOL											bRes = FALSE;
	PARAM_MXDB_COPIE_VAR_SYS_LOG	Mes;
		
	Mes.header.dwTaille = sizeof (Mes);
	Mes.header.uIdService = ID_SERVICE_MX_DB_PCS;
	Mes.header.uIdFonction = ID_FONCTION_MXDB_COPIE_VAR_SYS_LOG;
	Mes.bAffecte = bAffectePcs;							// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	Mes.dwIndex = wIndice;									// 0 ou index de tableau (1..n)
	Mes.dwIdVarSys = dwIdVarSys; 			// nom de la variable
	Mes.bValeur = *pLog;

	// envoi synchrone au multiplexeur Processyn
	bRes = bAppelMuxOk (hMuxPcs, &Mes.header);

	if (bRes && (!bAffectePcs))
		*pLog = Mes.bValeur;

	return bRes;
	}

//---------------------------------------------------------------------------
// Lit ou �crit une variable num�rique
static BOOL bValeurVarSysNumerique 
	(
	DWORD dwIdVarSys,		// Id de la variable � rechercher.
	DWORD wIndice,			// 0 ou indice tableau (1..n)
	BOOL	bAffectePcs,	// TRUE si �criture Var processyn , FALSE si lecture
	PNUMERIQUE pNum			// adresse de la variable transf�r�e
	)										// Retour : TRUE si op�ration effectu�e
	{
	// pr�pare le message pour le multiplexeur Processyn
  BOOL											bRes = FALSE;
	PARAM_MXDB_COPIE_VAR_SYS_NUM	Mes;
		
	Mes.header.dwTaille = sizeof (Mes);
	Mes.header.uIdService = ID_SERVICE_MX_DB_PCS;
	Mes.header.uIdFonction = ID_FONCTION_MXDB_COPIE_VAR_SYS_NUM;
	Mes.bAffecte = bAffectePcs;							// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	Mes.dwIndex = wIndice;									// 0 ou index de tableau (1..n)
	Mes.dwIdVarSys = dwIdVarSys; 					// Id de la variable
	Mes.fValeur = *pNum;

	// envoi synchrone au multiplexeur Processyn
	bRes = bAppelMuxOk (hMuxPcs, &Mes.header);

	if (bRes && (!bAffectePcs))
		*pNum = Mes.fValeur;

	return bRes;
	} // bValeurVarSysNumerique

//---------------------------------------------------------------------------
// Lit ou �crit une variable message
static BOOL bValeurVarSysMessage 
	(
	DWORD dwIdVarSys,		// Id de la variable � rechercher.
	DWORD wIndice,			// 0 ou indice tableau (1..n)
	BOOL	bAffectePcs,	// TRUE si �criture Var processyn , FALSE si lecture
	PSTR  pszMes,				// adresse de la variable transf�r�e
	DWORD dwTailleMes		// Nb octets allou�s � la chaine (terminateur 0 compris)
	)										// Retour : TRUE si op�ration effectu�e
	{
	// pr�pare le message pour le multiplexeur Processyn
  BOOL													bRes = FALSE;
	BOOL													bErr = FALSE;
	PARAM_MXDB_COPIE_VAR_SYS_MES	Mes;
		
	Mes.header.dwTaille = sizeof (Mes);
	Mes.header.uIdService = ID_SERVICE_MX_DB_PCS;
	Mes.header.uIdFonction = ID_FONCTION_MXDB_COPIE_VAR_SYS_MES;
	Mes.bAffecte = bAffectePcs;							// TRUE : affecte la variable / FALSE : r�cup�re sa valeur
	Mes.dwIndex = wIndice;									// 0 ou index de tableau (1..n)
	Mes.dwIdVarSys = dwIdVarSys; 						// nom de la variable

	if (bAffectePcs)
		bErr = !bCopieStrLimite (Mes.szValeur, pszMes, sizeof (Mes.szValeur));

	if (!bErr)
		// message Ok => envoi synchrone au multiplexeur Processyn
		bRes = bAppelMuxOk (hMuxPcs, &Mes.header);

	if (bRes && (!bAffectePcs))
		bRes = bCopieStrLimite (pszMes, Mes.szValeur, dwTailleMes);

	return bRes;
	} // bValeurVarSysMessage

//---------------------------------------------------------------------------
// Lit ou �crit une variable message
static BOOL bInfoVar
	(
	PCSTR pszNomVar,		// Nom de la variable � rechercher.
	DWORD	*pdwGenre,		// c_res_message, c_res_numerique ou c_res_logique
	DWORD	*pdwIndexMax	// 0 = var simple ou index max de tableau (1..n)
	)										// Retour : TRUE si op�ration effectu�e
	{
	// pr�pare le message pour le multiplexeur Processyn
  BOOL											bRes = FALSE;
	PARAM_MXDB_INFO_VAR	Mes;
		
	Mes.header.dwTaille = sizeof (Mes);
	Mes.header.uIdService = ID_SERVICE_MX_DB_PCS;
	Mes.header.uIdFonction = ID_FONCTION_MXDB_INFO_VAR;

	if (bCopieStrLimite (Mes.szNomVar, pszNomVar, sizeof (Mes.szNomVar)))
		bRes = bAppelMuxOk (hMuxPcs, &Mes.header);

	if (bRes)
		{
		*pdwGenre = Mes.dwGenre;
		*pdwIndexMax = Mes.dwIndexMax;
		}

	return bRes;
	} // bInfoVar


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Points d'entr�es de la DLL
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
// Contr�le du chargement de la DLL par Windows.
//---------------------------------------------------------------------------
BOOL APIENTRY DllMain (HINSTANCE hinst, DWORD dwReason, LPVOID lpReserved)
	{
	switch (dwReason)
		{
		case DLL_PROCESS_ATTACH:
			// DLL mapp�e dans l'espace d'adresse du process :
			// M�morisation du hInst
			hInst = hinst;
			hMuxPcs = NULL;
			break;

		// Pas de traitement par thread dans la mesure ou seul le thread code acc�de � cette DLL
		//case DLL_THREAD_ATTACH:  break;// un thread est cr�� break;
		//case DLL_THREAD_DETACH: break;// un thread se termine proprement : Cf DLL_THREAD_ATTACH

		case DLL_PROCESS_DETACH:
			// La DLL est d� - mapp�e de l'espace d'adresse du process :
			// reset du hInst
			hInst = 0;
			hMuxPcs = NULL;
			break;
		}
	return TRUE;
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// FONCTIONS PUBLIQUES appelables par le code utilisateur de la DLL
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
// Enregistre le handle de Processyn pour pouvoir y acc�der dans les appels suivants
//---------------------------------------------------------------------------
void WINAPI PcsNtDBHandleProcessyn
	(
	HANDLE hProcessyn	// handle � enregistrer (pass� par Processyn � la DLL utilisateur)
	)
	{
	hMuxPcs = hProcessyn;
	}

//---------------------------------------------------------------------------
int WINAPI GetNumeroFonction (void) // Retour : le contenu de la variable 'REG_NUM1' au moment de l'appel.
  {
	int					nRes = VARIABLE_INCONNUE;
  NUMERIQUE  fNumero = (NUMERIQUE)0;

	if (bValeurVarSysNumerique (VA_SYS_REG_NUM1, 0, FALSE, &fNumero))
		nRes = (int) fNumero;

  return nRes;
  }

//---------------------------------------------------------------------------
BOOL WINAPI FixStrLen (char *pszStr)
	{
	BOOL	bRes = FALSE;

	if (lstrlen (pszStr) > TAILLE_MESSAGE)
		{
		pszStr[TAILLE_MESSAGE - 1] = 0;
		bRes = TRUE;
		}
	return bRes;
	}

//---------------------------------------------------------------------------
int WINAPI GetGenreVar (PCSTR pszNomVar)
	{
	int   nRes = VARIABLE_INCONNUE;
	DWORD	dwGenre;
	DWORD	dwIndexMax;

	if (bInfoVar (pszNomVar, &dwGenre, &dwIndexMax))
		{
		nRes = (int) dwGenre;		
		}
	return nRes;
	}

//---------------------------------------------------------------------------
int WINAPI GetTailleVar	(PCSTR pszNomVar)
	{
	int   nRes = VARIABLE_INCONNUE;
	DWORD	dwGenre;
	DWORD	dwIndexMax;

	if (bInfoVar (pszNomVar, &dwGenre, &dwIndexMax))
		{
		nRes = (int) dwIndexMax;		
		}
  return nRes;
	}

//---------------------------------------------------------------------------
int WINAPI GetParamMessage (int wNumero, char * pszBuffer, DWORD dwTailleBuffer)
  {
	char szNomVar [c_nb_car_es];

	return bValeurVarSysMessage (VA_SYS_REG_MES1, 0, FALSE, szNomVar, sizeof(szNomVar)) && bValeurVarMessage (szNomVar, wNumero, FALSE, pszBuffer, dwTailleBuffer);
  }

//---------------------------------------------------------------------------
BOOL WINAPI SetParamMessage (int wNumero, PSTR pszBuffer)
  {
	char szNomVar [c_nb_car_es];

	return bValeurVarSysMessage (VA_SYS_REG_MES1, 0, FALSE, szNomVar, sizeof(szNomVar)) && bValeurVarMessage (szNomVar, wNumero, TRUE, pszBuffer, 0);
  }

//---------------------------------------------------------------------------
BOOL WINAPI GetParamNumerique (int wNumero, NUMERIQUE * pfNum)
  {
	char szNomVar [c_nb_car_es];

	return bValeurVarSysMessage (VA_SYS_REG_MES2, 0, FALSE, szNomVar, sizeof(szNomVar)) && bValeurVarNumerique (szNomVar, wNumero, FALSE, pfNum);
	}

//---------------------------------------------------------------------------
int WINAPI SetParamNumerique (int wNumero, NUMERIQUE fNum)
  {
	char szNomVar [c_nb_car_es];

	return bValeurVarSysMessage (VA_SYS_REG_MES2, 0, FALSE, szNomVar, sizeof(szNomVar)) && bValeurVarNumerique (szNomVar, wNumero, TRUE, &fNum);
  }

//---------------------------------------------------------------------------
int WINAPI GetParamLogique (int wNumero, LOGIQUE * pbLog)
  {
	char szNomVar [c_nb_car_es];

	return bValeurVarSysMessage (VA_SYS_REG_MES3, 0, FALSE, szNomVar, sizeof(szNomVar)) && bValeurVarLogique (szNomVar, wNumero, FALSE, pbLog);
  }

//---------------------------------------------------------------------------
BOOL WINAPI SetParamLogique (int wNumero, LOGIQUE Log)
  {
	char szNomVar [c_nb_car_es];

	return bValeurVarSysMessage (VA_SYS_REG_MES3, 0, FALSE, szNomVar, sizeof(szNomVar)) && bValeurVarLogique (szNomVar, wNumero, TRUE, &Log);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
BOOL WINAPI GetValMessage (PCSTR pszNomVar,	char * pszBuffer, DWORD dwTailleBuffer)
	{
  return bValeurVarMessage (pszNomVar, 0, FALSE, pszBuffer, dwTailleBuffer);
	}

//---------------------------------------------------------------------------
BOOL WINAPI SetValMessage (PCSTR pszNomVar, PSTR pszBuffer)
	{
  return bValeurVarMessage (pszNomVar, 0, TRUE, pszBuffer, 0);
	}

//---------------------------------------------------------------------------
BOOL WINAPI GetValNumerique (PCSTR pszNomVar, NUMERIQUE * pfNum)
	{
  return bValeurVarNumerique (pszNomVar, 0, FALSE, pfNum);
	}

//---------------------------------------------------------------------------
BOOL WINAPI SetValNumerique (PCSTR pszNomVar, NUMERIQUE fNum)
	{
  return bValeurVarNumerique (pszNomVar, 0, TRUE, &fNum);
	}

//---------------------------------------------------------------------------
BOOL WINAPI GetValLogique (PCSTR pszNomVar, LOGIQUE * pbLog)
	{
  return bValeurVarLogique (pszNomVar, 0, FALSE, pbLog);
	}

//---------------------------------------------------------------------------
BOOL WINAPI SetValLogique (PCSTR pszNomVar, LOGIQUE Log)
	{
  return bValeurVarLogique (pszNomVar, 0, TRUE, &Log);
	}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
int WINAPI GetTabMessage (PCSTR pszNomTab, int Indice, PSTR pszBuffer, DWORD dwTailleBuffer)
	{
  return bValeurVarMessage (pszNomTab, Indice, FALSE, pszBuffer, dwTailleBuffer);
	}

//---------------------------------------------------------------------------
BOOL WINAPI SetTabMessage (PCSTR pszNomTab, int Indice, PSTR pszBuffer)
	{
  return bValeurVarMessage (pszNomTab, Indice, TRUE, pszBuffer, 0);
	}

//---------------------------------------------------------------------------
int WINAPI GetTabNumerique (PCSTR pszNomTab, int Indice, PNUMERIQUE pfNum)
	{
  return bValeurVarNumerique (pszNomTab, Indice, FALSE, pfNum);
	}

//---------------------------------------------------------------------------
int WINAPI SetTabNumerique (PCSTR pszNomTab, int Indice, NUMERIQUE  fNum)
	{
  return bValeurVarNumerique (pszNomTab, Indice, TRUE, &fNum);
	}
//---------------------------------------------------------------------------
int WINAPI GetTabLogique (PCSTR pszNomTab, int Indice, LOGIQUE * pbLog)
	{
  return bValeurVarLogique (pszNomTab, Indice, FALSE, pbLog);
	}

//---------------------------------------------------------------------------
BOOL WINAPI SetTabLogique (PCSTR pszNomTab, int Indice, LOGIQUE bLog)
	{
  return bValeurVarLogique (pszNomTab, Indice, TRUE, &bLog);
	}

//------------------------- fin ----------------------------------


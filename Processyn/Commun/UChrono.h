#ifndef UCHRONO_H
#define UCHRONO_H
// --------------------------------------------------------------------------
//  UChrono.h
// Gestion Chronos + Minuteries en millisecondes
// Win32 15/9/97 JS

// Le syst�me fournit le nombre de millisecondes �coul�es depuis le lancement du syst�me
// la fonction i64NbMsMaintenant appel�e directement ou � travers les fonctions temps r�el
// ou la fonction d'�chantillonage (au moins une fois tous les 49 jours), maintient un 
// compteur interne permttant d'atteindre 162 millions d'ann�es.
// Sur cette base temporelle, deux objets sont g�r�s : Le chrono et la minuterie.

// Un chrono est soit en marche, soit suspendu (avec maintien de la valeur cumul�e).
//
// Une Minuterie est soit en marche, soit suspendue, arriv�e � �ch�ance ou pas.

typedef __int64 CHRONO, *PCHRONO;
typedef __int64 MINUTERIE, *PMINUTERIE;

// Macros pratiques pour les dur�es (toujours sp�cifi�es en millisecondes)
#define NB_MS_PAR_SECONDE 1000
#define NB_MS_PAR_MINUTE	(NB_MS_PAR_SECONDE * 60)
#define NB_MS_PAR_HEURE (NB_MS_PAR_MINUTE * 60)
#define NB_MS_PAR_JOUR (NB_MS_PAR_HEURE * 24)

#define NB_MS_PAR_CENTIEME(Centieme) ((Centieme) * 10)
#define NB_CENTIEME_PAR_MS(Milli) ((Milli) / 10)

// --------------------------------------------------------------------------
//Initialise l'origine du nombre de millisecondes pour les fonctions de datation (non implant�es)
void InitChronosEtMinuteries (void);

// --------------------------------------------------------------------------
// Met � jour et renvoie le nombre de Millisecondes depuis l'origine de l'instant actuel
__int64 i64NbMsMaintenant (void);

// Le chronom�tre est marqu� suspendu (avec un cumul � 0)
void ChronoInitSuspendu (PCHRONO pChrono);

// La minuterie est marqu�e suspendue (avec un d�passement d'�ch�ance � 0)
// Attention! Cette minuterie suspendue est donc � �ch�ance 
void MinuterieInitSuspendue (PMINUTERIE pMinuterie);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							Chronom�trage temps r�el
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Le chronom�tre est marqu� en marche avec un cumul � 0
void ChronoLance (PCHRONO pChrono);

// Renvoie la valeur courante (en millisecondes) du chronom�tre qu'il soit arr�t� ou en marche
__int64 i64ValChronoMs (const CHRONO * pChrono);

// Suspends l'ex�cution d'un chrono si ce n'est pas d�ja fait
void ChronoSuspends (PCHRONO pChrono);

// Reprends l'ex�cution du chrono si elle �tait suspendue
void ChronoReprends (PCHRONO pChrono);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							Minuterie temps r�el
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Une Minuterie est marqu�e pour �tre � �ch�ance au bout de la dur�e sp�cifi�e - � partir de maintenant
void MinuterieLanceMs (PMINUTERIE pMinuterie, __int64 i64DureeMinuterieMs);

// Renvoie TRUE si l'�ch�ance de la Minuterie est atteinte ou d�pass�e
BOOL bEcheanceMinuterie (MINUTERIE Minuterie);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Fonctions avec un instant choisi comme r�f�rence de temps
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Utiliser l'instant actuel comme r�f�rence de temps pour les fonctions �chantillon�es
// ! Attention l'�chantillon est global
void ChronoEchantilloneMsMaintenant (void);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							Chronom�trage par rapport au dernier EchantillonMsMaintenant
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Le chronom�tre est marqu� en marche � compter de maintenant (cumul � 0)
void ChronoLanceEch (PCHRONO pChrono);

// Renvoie la valeur courante (en millisecondes) du chronom�tre qu'il soit arr�t� ou en marche
__int64 i64ValChronoMsEch (const CHRONO * pChrono);

// Suspends l'ex�cution d'un chrono si ce n'est pas d�ja fait
void ChronoSuspendsEch (PCHRONO pChrono);

// Reprends l'ex�cution du chrono si elle �tait suspendue
void ChronoReprendsEch (PCHRONO pChrono);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							Minuterie par rapport au dernier EchantillonMsMaintenant
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// Une Minuterie est marqu�e pour �tre � �ch�ance au bout de la dur�e sp�cifi�e
void MinuterieLanceMsEch (PMINUTERIE pMinuterie, __int64 i64DureeMinuterieMs);

// D�place l'�ch�ance d'une Minuterie (ne change pas l'�tat suspendu)
void MinuterieAjouteMsEch (PMINUTERIE pMinuterie, __int64 i64DureeAjouteeMs);

// Renvoie TRUE si l'�ch�ance de la Minuterie est atteinte ou d�pass�e
BOOL bEcheanceMinuterieEch (MINUTERIE Minuterie);

// Renvoie TRUE si l'ex�cution de la Minuterie est suspendue
BOOL bMinuterieSuspendueEch (MINUTERIE Minuterie);

// Suspends l'ex�cution de la Minuterie si ce n'est pas d�ja fait
void MinuterieSuspendsEch (PMINUTERIE pMinuterie);

// Reprends l'ex�cution de la Minuterie si elle est suspendue
void MinuterieReprendsEch (PMINUTERIE pMinuterie);

// Reprends l'ex�cution de la Minuterie si elle est suspendue et la relance si elle est � �ch�ance
// Renvoie TRUE si la minuterie �tait � �ch�ance
BOOL bMinuterieRelanceSiEcheanceEch (PMINUTERIE pMinuterie, __int64 i64DureeAjouteeMs);

#endif

#include "stdafx.h"
#include "CheckMan.h"

// ------------------------------------------
// renvoie TRUE si le hwnd donn� est un Radio bouton
static BOOL bEstBoutonRadio (HWND hwndControle)
	{
	// Contr�le = radio button ?
	LONG lStyle = GetWindowLong (hwndControle, GWL_STYLE) & 0x0f; // Attention ! sp�cifique impl�mentation

	return (lStyle == BS_RADIOBUTTON) || (lStyle == BS_AUTORADIOBUTTON);
	}

// ------------------------------------------
// renvoie TRUE si le hwnd donn� est marqu� comme groupe
static BOOL bEstMarqueGroupe (HWND hwndControle)
	{
	// Contr�le = radio button ?
	return (GetWindowLong (hwndControle, GWL_STYLE) & WS_GROUP) != 0;
	}

// ------------------------------------------
// renvoie, 1 si premier bouton check�, 2 si second ... -1 sinon
int nCheckIndex(HWND hDlgParente, int nIdPremierBouton)
	{
	HWND	hwndControle = ::GetDlgItem (hDlgParente, nIdPremierBouton);
	int		nIndex = 1;

	// parcours de contr�le en contr�le
	for (nIndex = 1; hwndControle; nIndex++)
		{
		// Contr�le = radio button ?
		if (!bEstBoutonRadio (hwndControle))
			{
			// non => chou blanc
			hwndControle = NULL;
			break;
			}
		
		// bouton Check� ?
		if (BST_CHECKED == ::SendMessage (hwndControle, BM_GETCHECK, 0, 0))
			// oui => termin� Ok
			break;
		else
			{
			// Non => contr�le suivant
			hwndControle = GetNextWindow (hwndControle, GW_HWNDNEXT);

			// Ce contr�le est un d�but de groupe ? 
			if (hwndControle && bEstMarqueGroupe(hwndControle))
				{
				// oui => chou blanc
				hwndControle = NULL;
				break;
				}
			}
		} // for (nIndex = 1; hwndControle; nIndex++)

	// Pas de bouton trouv� ?
	if (!hwndControle)
		// oui => renvoie -1
		nIndex = -1;

	return nIndex;
	}

//------------------------------------------------------------------------
// R�cup�re l'Id du premier radio bouton du groupe auquel appartient le radio bouton sp�cifi�
BOOL bIdPremierRadio (HWND hDlgParente, HWND hwndRadio, int * pnIdPremierRadio)
	{
	BOOL bOk = FALSE;
	
	*pnIdPremierRadio = 0;

	// parcours en remontant la liste des contr�les
	while (hwndRadio && bEstBoutonRadio (hwndRadio))
		{
		// d�but de groupe ?
		if (bEstMarqueGroupe (hwndRadio))
			{
			// oui => r�cup�re son ID, termin�.
			*pnIdPremierRadio = ::GetDlgCtrlID(hwndRadio);
			bOk = TRUE;
			break;
			}

		// Controle pr�c�dent
		hwndRadio = GetNextWindow (hwndRadio, GW_HWNDPREV);
		}

	return bOk;
	}

//------------------------------------------------------------------------
// R�cup�re la position (1..n) dans son groupe du radio radio bouton sp�cifi�
BOOL bPositionRadioDansGroupe (HWND hDlgParente, HWND hwndRadio, int * pnNPositionDansGroupe)
	{
	BOOL bOk = FALSE;
	int	nNPositionDansGroupe = 1;
	
	*pnNPositionDansGroupe = 0;

	// parcours en remontant la liste des contr�les
	while (hwndRadio && bEstBoutonRadio (hwndRadio))
		{
		// d�but de groupe ?
		if (bEstMarqueGroupe (hwndRadio))
			{
			// oui => Met � jour sa position, termin�.
			*pnNPositionDansGroupe = nNPositionDansGroupe;
			bOk = TRUE;
			break;
			}

		// Controle pr�c�dent
		hwndRadio = GetNextWindow (hwndRadio, GW_HWNDPREV);
		nNPositionDansGroupe++;
		}

	return bOk;
	}

//------------------------------------------------------------------------
// Check du n-i�me (1..n) radio bouton du groupe auquel appartient le radio bouton sp�cifi�
BOOL bCheckRadioBouton (HWND hDlgParente, HWND hwndRadio, int nNRadio)
	{
	BOOL bOk = FALSE;
	int	nIdPremierRadio;
	
	if (bIdPremierRadio (hDlgParente, hwndRadio, &nIdPremierRadio))
		{
		int	nIndex = 1;
		hwndRadio = ::GetDlgItem (hDlgParente, nIdPremierRadio);

		// parcours en descendant la liste des contr�les
		while (hwndRadio && bEstBoutonRadio (hwndRadio))
			{
			// met la coche s'il s'agit du bon, enl�ve la sinon
			if (nIndex == nNRadio)
				{
				::SendMessage (hwndRadio, BM_SETCHECK, BST_CHECKED, 0);
				bOk = TRUE;
				}
			else
				::SendMessage (hwndRadio, BM_SETCHECK, BST_UNCHECKED,0);

			// Controle suivant
			hwndRadio = GetNextWindow (hwndRadio, GW_HWNDNEXT);
			nIndex ++;

			// nouveau d�but de groupe ?
			if (bEstMarqueGroupe (hwndRadio))
				// oui => termin�
				break;
			}
		}

	return bOk;
	} // bCheckRadioBouton

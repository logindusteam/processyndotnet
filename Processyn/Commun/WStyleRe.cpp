// WStyleRe.c
// Version Win32 11/2/97

#include "stdafx.h"
#include "Appli.h"
#include "UEnv.h"

#include "WBarOuti.h"
#include "idmenugr.h"           // �tre effectu�es par une commande de actiongr

#include "IdGrLng.h"
#include "LireLng.h"
#include "IdLngLng.h"

#include "G_Objets.h" 
#include "WEchanti.h" 

#include "Verif.h"
#include "WStyleRe.h"
VerifInit;

static const char szClasseChoixStyleRemplissagesGr [] = "ClasseChoixStyleRemplissages";

// -----------------------------------------------------------------------
// Fen�tre de s�lection d'un Style de Remplissage
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocChoixStyleRemplissages (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;
  BOOL     bTraite = TRUE;
	typedef struct
		{
		HBO		hbo;
		} ENV, *PENV;

	PENV pEnv;

  switch (msg)
    {
    case WM_CREATE:
			{  
			int		cx;
			int		cy;
			RECT	rectBO;
			RECT	rect;

			pEnv = (PENV)pCreeEnv (hwnd, sizeof (ENV));

			// cr�ation de la barre d'outil fille choix de Remplissage
			pEnv->hbo = hBOCree (NULL, FALSE, TRUE, FALSE, 0, 0);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STR0),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR0);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STR1),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR1);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STR2),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR2);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STR3),BO_NOUVELLE_LN ,BO_COCHE,CMD_MENU_BMP_STR3);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STR4),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR4);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STR5),BO_COL_SUIVANTE,BO_COCHE,CMD_MENU_BMP_STR5);

			bBOOuvre (pEnv->hbo, FALSE, hwnd, 0, 0, BO_POS_DROITE | BO_POS_HAUTE, TRUE);

			//$ac old param	bBOOuvre (pEnv->hbo, TRUE, hwnd, rectBO.right, rectBO.top, 0, BOStyleRemplissage.bVisible);

			// fen�tre de choix calcul�e � partir de la barre d'outil
			BOGetRect (pEnv->hbo, &rectBO);
			cy = rectBO.bottom - rectBO.top;
			//cx = rectBO.right - rectBO.left + cy;
			cx = rectBO.right - rectBO.left;
			rect.left = (Appli.sizEcran.cx - cx) / 2;
			rect.top = (Appli.sizEcran.cy - cy) / 2;
			rect.right = rect.left + cx;
			rect.bottom = rect.top + cy;
			if (AdjustWindowRectEx (&rect, GetWindowLong (hwnd, GWL_STYLE), FALSE, GetWindowLong (hwnd, GWL_EXSTYLE)))
				{
				// redimensionne cette fen�tre � partir des dimensions de la barre d'outil choix Remplissage
				::MoveWindow (hwnd, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, FALSE);
				//BODeplace (pEnv->hbo, 0, 0, BO_POS_DROITE | BO_POS_HAUTE);
				BODeplace (pEnv->hbo, 0, 0, BO_POS_CENTRE);
				}
			}
			break;

    case WM_DESTROY:
			pEnv = (PENV)pGetEnv (hwnd);
			BODetruit (&pEnv->hbo);
			// NB : la destruction des fen�tres filles est syst�matique
			bLibereEnv (hwnd);
			break;


    case WM_COMMAND:
      switch (LOWORD (mp1))
        {
				case CMD_MENU_BMP_STR0:
				case CMD_MENU_BMP_STR1:
				case CMD_MENU_BMP_STR2:
				case CMD_MENU_BMP_STR3:
				case CMD_MENU_BMP_STR4:
				case CMD_MENU_BMP_STR5:
					::SendMessage (::GetParent (hwnd), WM_COMMAND, mp1, mp2);
          break;
        } // switch (LOWORD (mp1))
				break;

			case WM_CLOSE:
					// demande de fermeture de la fen�tre :
					// on simule l'appui sur le bouton Remplissages de la barre d'outil des barres d'outils
					break;
    default:
			bTraite = FALSE;
			break;
    } // switch (msg)

  // message non trait�, traitement par d�faut ---------------
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  } // wndprocChoixStyleRemplissages

// -----------------------------------------------------------------------
// Cr�ation de la classe StyleRemplissageGr de PcsGr
// -----------------------------------------------------------------------
BOOL bCreeClasseStyleRemplissages (void)
  {
	static BOOL bClasseEnregistree = FALSE;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= 0;//CS_HREDRAW | CS_VREDRAW; 
		wc.lpfnWndProc		= wndprocChoixStyleRemplissages; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= NULL; //(HBRUSH)(COLOR_APPWORKSPACE+1); pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseChoixStyleRemplissagesGr; 
		bClasseEnregistree = RegisterClass (&wc);
		}

  return bClasseEnregistree;
  } // bCreeClasseRemplissage

// -----------------------------------------------------------------------
// Cr�ation d'une fen�tre de choix de style de remplissage (contenant sa barre d'outil)
// -----------------------------------------------------------------------
HWND hwndCreeChoixStyleRemplissages (HWND hwndParent, BOOL bVisible, BOOL bChild)
  {
	HWND	hwndRet;
  char  titre [c_nb_car_message_inf] = "";
	DWORD	dwStyle = WS_BORDER;
	
	if (bVisible)
		dwStyle |= WS_VISIBLE;

  bLngLireInformation (c_inf_style_remplissage, titre);
	if (bChild)
		hwndRet = CreateWindow (szClasseChoixStyleRemplissagesGr,titre,	dwStyle | WS_CHILD,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,	hwndParent,	NULL,	Appli.hinst, NULL);
	else
		hwndRet = CreateWindowEx (WS_EX_PALETTEWINDOW, szClasseChoixStyleRemplissagesGr,titre,	dwStyle | WS_CAPTION | WS_POPUP,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,	hwndParent,	NULL,	Appli.hinst, NULL);

  return hwndRet;
  } // hwndCreeChoixStyleRemplissages


// ---------------------------------------------------------------------------
// interface unite declaration des types du driver DISQUE
// ---------------------------------------------------------------------------


// ---------------------------------------------------------------------------
#define  b_titre_paragraf_dq       55
#define  b_spec_titre_paragraf_dq  54
#define  b_geo_ancien_dq           56
#define  bx_disq                   74
#define  bx_var_disq               75
#define  b_geo_disq               570   // suite a modif tableau --> voir update


// ---------------------------------------------------------------------------

#define c_NB_FIC_MAX 70

typedef struct
  {
  union
    {
    struct
      {
      DWORD position_fichier [c_NB_FIC_MAX];
      DWORD nb_fichier;
      };
    struct
      {
      DWORD pos_es;
      DWORD pos_init;
      };
    };
  } SPEC_TITRE_PARAGRAPHE_DISQUE, *PSPEC_TITRE_PARAGRAPHE_DISQUE;

typedef struct
  {
  DWORD numero_repere;
  union
    {
    DWORD pos_donnees;
    struct
      {
      DWORD pos_paragraf;
      DWORD pos_specif_paragraf;
      };
    struct
      {
      DWORD pos_es;
      DWORD pos_initial;
      DWORD longueur;
      };
    };
  } GEO_DISQUE, *PGEO_DISQUE;

typedef struct
  {
  char nom_fichier[82]; //  $$ constante de taille !!!!
  DWORD dwPosESEcriture;
  DWORD dwPosESLecture;
  DWORD dwPosESTaille;
  DWORD pos_debut_var;
  DWORD pos_fin_var;
  HFDQ f_handle;
  BOOL bOuvert;
  void * pBuffer;
  } FICHIER_DQ, *PFICHIER_DQ;


typedef struct
  {
  DWORD pos;
  DWORD taille;
  DWORD genre;
  } t_tableau_vars_fichiers;

typedef enum // $$ � utiliser
	{
	STATUT_DQ_ERR_N_ENR_ECR = 1,
	STATUT_DQ_ERR_FICHIER_VIDE = 2,
	STATUT_DQ_ERR_N_ENR_LEC = 4,
	STATUT_DQ_ERR_OUVERTURE = 8,
	STATUT_DQ_ERR_COPIE = 16,
	STATUT_DQ_ERR_RENOMME = 32,
	STATUT_DQ_ERR_NUMERIQUE_INVALIDE = 64,
	STATUT_DQ_ERR_NOM_FIC = 128,
	STATUT_DQ_ERR_FORMAT = 256,
	STATUT_DQ_ERR_OS = 32768
	} STATUT_DQ;
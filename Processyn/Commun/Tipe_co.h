// interface commune CODE

#define  nb_max_caractere 80

typedef enum
	{
	uls_terminees = 0,
	op_variable = 1,
	op_constante = 2,
	op_reserve = 3,
	op_bizarre = 4
	} GENRE_OP_CODE, *PGENRE_OP_CODE;

typedef enum
	{
	push_var_log = 0,
	push_var_num = 1,
	push_var_mes = 2,
	push_avar_log = 77,
	push_avar_num = 78,
	push_avar_mes = 79,
	push_const_0 = 3,
	push_const_1 = 4,
	push_const_num = 5,
	push_const_mes = 6,
	push_var_log_r = 90,
	push_var_num_r = 91,
	push_var_mes_r = 92,
	push_var_log_t = 121,
	push_var_num_t = 122,
	push_var_mes_t = 123,
	push_avar_log_t = 130,
	push_avar_num_t = 131,
	push_avar_mes_t = 132,
	push_var_num_i = 133,
	push_avar_log_r = 93,
	push_avar_num_r = 94,
	push_avar_mes_r = 95,
	push_const_log_r = 96,
	push_const_num_r = 97,
	push_const_mes_r = 98,
	push_val_operande = 104,
	push_val_operande_log = 111,
	push_val_operande_num = 112,
	push_val_operande_mes = 113,
	push_val_operande_r = 114,
	push_index_chrono_r = 105,
	push_index_metro_r = 106,
	pop_var_log = 7,
	pop_var_num = 8,
	pop_var_mes = 9,
	pop_var_log_r = 99,
	pop_var_num_r = 100,
	pop_var_mes_r = 101,
	pop_var_log_t = 124,
	pop_var_num_t = 125,
	pop_var_mes_t = 126,
	inferieur_log = 10,
	inferieur_num = 11,
	inferieur_mes = 12,
	superieur_log = 13,
	superieur_num = 14,
	superieur_mes = 15,
	egal_log = 16,
	egal_num = 17,
	egal_mes = 18,
	saut = 19,
	saut_cond = 20,
	change_signe = 21,
	op_non = 22,
	op_logarithme = 23,
	op_ou = 24,
	op_ou_x = 25,
	op_ou_num = 116,
	op_et = 26,
	op_et_num = 117,
	op_plus = 27,
	op_moins = 28,
	op_multiplie = 29,
	op_divise = 30,
	op_exponentiel = 31,
	op_sinus = 32,
	op_cosinus = 33,
	op_tangente = 34,
	op_absolue = 35,
	op_carre = 36,
	op_racine = 37,
	op_concatene = 38,
	inferieur_egal_log = 39,
	inferieur_egal_num = 40,
	inferieur_egal_mes = 41,
	superieur_egal_log = 42,
	superieur_egal_num = 43,
	superieur_egal_mes = 44,
	different_log = 45,
	different_num = 46,
	different_mes = 47,
	bit_test = 115,
	formate_log = 48,
	formate_num = 49,
	formate_num_sc = 80,
	formate_mes = 50,
	op_log_10 = 51,
	op_partie_entiere = 52,
	op_bcd = 53,
	op_valbcd = 54,
	op_bin = 55,
	op_valbin = 56,
	op_val = 81,
	op_car = 89,
	ecr_ecran = 57,
	ecr_imp = 58,
	ecr_imp_mlg = 108,
	ouvre_disq = 87,
	ecr_disq = 59,
	op_tronc = 60,
	deb_par = 61,
	fin_par = 62,
	lec_disq = 63,
	ferme_disq = 88,
	efface_disq = 118,
	copie_disq = 119,
	depart_chrono = 82,
	arret_chrono = 83,
	continu_chrono = 84,
	depart_metro = 85,
	arret_metro = 86,
	passe_val = 64,
	passe_val_h = 65,
	passe_val_b = 66,
	prend_val_log = 67,
	prend_val_num = 68,
	prend_val_mes = 69,
	quitte_val_log = 70,
	quitte_val_num = 71,
	quitte_val_mes = 72,
	prend_val_h = 73,
	prend_val_b = 74,
	quitte_val_h = 75,
	quitte_val_b = 76,
	deb_execute = 102,
	fin_execute = 103,
	fin_boucle = 127,
	lance_resident = 107,
	op_pointe_log = 109,
	op_pointe_num = 134,
	op_pointe_mes = 135,
	op_detecte_log = 110,
	op_detecte_num = 136,
	op_detecte_mes = 137,
	lance_fonction = 120,
	push_const_num_t = 128,
	saut_boucle = 129,
	trans_tab_log = 138,
	trans_tab_num = 139,
	trans_tab_mes = 140,
	push_tableau = 141,
	renomme_disq = 142,
	lance_ov1 = 143,
	lance_ov2 = 144,
	push_goto  = 145,
	pop_ret  = 146,
	op_val_ascii = 147,
	raz_chrono = 148,
	op_hexa = 149,
	op_valhexa = 150,
	ecr_imp_brut = 151
	} PCS_OP_CODE, *PPCS_OP_CODE;

//$$ utiliser table de fonctions si ce n'est d�ja fait
// dernier numero - 150 -

#define  b_code 40
#define  b_geo_code 41
#define  bx_num 47
#define  bx_mes 48
#define  bx_code 17
#define  bx_debug_code 892
#define  bx_main_nb 898


typedef struct
  {
  GENRE_OP_CODE GenreOpCode;
  DWORD donnee_source;
  } C_CODE, *PC_CODE;

typedef struct
  {
  PCS_OP_CODE PcsOpCode;
  DWORD operande;
  } P_CODE, *PP_CODE;

typedef struct
  {
  DWORD dwNPremierCode;
  int nNbImbrications;
  } GEO_CODE, *PGEO_CODE;

typedef C_CODE t_tampon_instructions [150];

//---------------------------------------------------------------------------
// Mapping de coordonn�es
// Un objet HSPACE contient la taille de deux espaces (SPC_VIRTUEL et SPC_PIXEL),
// et le d�calage de SPC_VIRTUEL par rapport � SPC_PIXEL
//---------------------------------------------------------------------------
DECLARE_HANDLE (HSPACE);

//---------------------------------------------------------------------------
// Cr�ation / destruction d'un objet HSPACE
HSPACE hSpcCree (LONG cxPixel, LONG cyPixel, LONG cxVirtuel, LONG cyVirtuel);
void SpcFerme (HSPACE * phspace); // phspace remis � NULL

// $$ Modifications globale de la taille de l'espace Pixel ou virtuel (par exemple en cas de changement de r�solution �cran)
void SpcModifie (HSPACE hspace, LONG cxPixel, LONG cyPixel, LONG cxVirtuel, LONG cyVirtuel);

// modifications de la taille de l'espace pixel
void SpcModifieEspacePixel (HSPACE hspace, LONG dx, LONG dy);
// modifications de la taille de l'espace virtuel
void SpcModifieEspaceVirtuel (HSPACE hspace, LONG dx, LONG dy);

// Nouvelle origine de l'espace virtuel par rapport � l'espace de r�f�rence (en coordonn�es virtuelles)
void  SpcSetXOrigineVirtuel (HSPACE hspace, LONG xOrigine);
void  SpcSetYOrigineVirtuel (HSPACE hspace, LONG yOrigine);

// R�cup�re les coordonn�es de l'origine de l'espace virtuel (en coordonn�es virtuelles)
void  SpcGetPointOrigineVirtuel	(HSPACE hspace,	POINT * pptOrigineVirtuel);
void  SpcGetOrigineVirtuel (HSPACE hspace, LONG * pxOrigineVirtuel, LONG * pyOrigineVirtuel);

// D�place l'origine de l'espace virtuel de dx ou dy (sp�cifi�s en virtuel)
LONG  lSpcDeplaceXOrigineVirtuel (HSPACE hspace, LONG dx);
LONG  lSpcDeplaceYOrigineVirtuel (HSPACE hspace, LONG dy);

// Conversion d'un d�placement
LONG  lSpcCvtDxEnVirtuel	(HSPACE hspace, LONG dx);
LONG  lSpcCvtDyEnVirtuel  (HSPACE hspace, LONG dy);
void  Spc_CvtSizeEnVirtuel(HSPACE hspace, LONG * pdx, LONG * pdy);

LONG  lSpcCvtDxEnPixel	(HSPACE hspace, LONG dx);
LONG  lSpcCvtDyEnPixel  (HSPACE hspace, LONG dy);
void  Spc_CvtSizeEnPixel(HSPACE hspace, LONG * pdx, LONG * pdy);

// Conversion de positions (avec cadrage)
LONG  lSpcCvtXEnPixel			(HSPACE hspace, LONG x);
LONG  lSpcCvtYEnPixel			(HSPACE hspace, LONG y);
void  SpcCvtPointsEnPixel (HSPACE hspace, DWORD wNbPt, POINT *pPoint);
void  Spc_CvtPtEnPixel    (HSPACE hspace, LONG * pdx, LONG * pdy);

LONG  lSpcCvtXEnVirtuel		(HSPACE hspace, LONG x);
LONG  lSpcCvtYEnVirtuel		(HSPACE hspace, LONG y);
void  SpcCvtPointsEnVirtuel (HSPACE hspace, DWORD wNbPt, POINT *pPoint);
void  Spc_CvtPtEnVirtuel  (HSPACE hspace, LONG * pdx, LONG * pdy);

// Cadrage (alignement) par double conversion d'une coordonn�e sp�cifi�e dans l'espace virtuel
LONG  lSpcVirtuelAjusteX  (HSPACE hspace, LONG x);
LONG  lSpcVirtuelAjusteY  (HSPACE hspace, LONG y);

// Additionne deux d�placements et Cadre le r�sultat
// valeurs sp�cifi�es dans l'espace virtuel
LONG  lSpcVirtuelAjouteDx (HSPACE hspace, LONG dx1, LONG dx2);
LONG  lSpcVirtuelAjouteDy (HSPACE hspace, LONG dy1, LONG dy2);

// Additionne un d�placement � une position (renvoie une position cadr�e) 
// valeurs sp�cifi�es dans l'espace virtuel
LONG  lSpcVirtuelAjouteX (HSPACE hspace, LONG x, LONG dx);
LONG  lSpcVirtuelAjouteY (HSPACE hspace, LONG y, LONG dy);

//---------------------------------------------------------------------------

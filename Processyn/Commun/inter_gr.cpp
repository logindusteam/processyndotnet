/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|*/

// unite d'analyse des commandes vdi
// Version : 4.0

#include "stdafx.h"
#include "lng_res.h"
#include "std.h"
#include "mem.h"
#include "tipe.h"
#include "Appli.h" 
#include "UStr.h"
#include "ProgMan.h" // type de lancement
#include "cvt_ul.h"
#include "G_Objets.h"
#include "g_sys.h"
#include "Descripteur.h"
#include "bdgr.h"
#include "BdAnmGr.h"
#include "UChrono.h"
#include "tipe_gr.h"
#include "pcsspace.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "verif.h"
#include "IdLngLng.h"
#include "inter_gr.h"
VerifInit;

//----------------------------------------------
#define err(code) return (code)  //Sale Sioux
//----------------------------------------------

//----------------------------------------------
#define err1(code) (*erreur) = code;return (FALSE)  //Sale Sioux
//----------------------------------------------


typedef struct
	{
	PVDI_R_LOG spec_r_log;
	PVDI_R_LOG_TAB spec_r_log_tab;
	PVDI_R_LOG_4_ETATS spec_r_log_4etats;
	PVDI_R_LOG_4_ETATS_TAB spec_r_log_4etats_tab;
	PVDI_R_NUM spec_r_num;
	PVDI_R_NUM_TAB spec_r_num_tab;
	PVDI_R_NUM_DEP spec_r_num_dep;
	PVDI_R_NUM_DEP_TAB spec_r_num_dep_tab;
	PVDI_R_MES spec_r_mes;
	PVDI_R_MES_TAB spec_r_mes_tab;
	PVDI_R_NUM_ALPHA spec_r_num_alpha;
	PVDI_R_NUM_ALPHA_TAB spec_r_num_alpha_tab;
	PVDI_COURBE_TEMPS spec_courbe_temps;
	PVDI_COURBE_CMD spec_courbe_cmd;
	PVDI_COURBE_FIC spec_courbe_fic;
	PVDI_E_LOG spec_e_log;
	PVDI_E_NUM spec_e_num;
	PVDI_E_MES spec_e_mes;
	PVDI_CONTROL_RADIO spec_control_radio;
	PVDI_CONTROL_LISTE spec_control_liste;
	PVDI_CONTROL_COMBOBOX spec_control_combobox;
	PVDI_CONTROL_STATIC spec_control_static;
	} INTER_GR_PARAM_VALIDE_VECT_UL, *PINTER_GR_PARAM_VALIDE_VECT_UL;

//-------------------------- Variables ---------------------

static DWORD ligne_depart_vdi;
static DWORD ligne_courante_vdi;
static BOOL vdi_deja_initialise;

BOOL bPortagePM = FALSE;

//---------------------------------------------------------------------------
static void maj_pointeur_spec_dans_geo (DWORD numero_repere, DWORD limite)
  {
  PGEO_VDI donnees_geo;
  DWORD nbr_enr;
  DWORD i;

  if (!existe_repere (b_geo_vdi))
    {
    nbr_enr = 0;
    }
  else
    {
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_geo_vdi);
    }
  for (i = 1; (i <= nbr_enr); i++)
    {
    donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi, i);
    if (donnees_geo->numero_repere == numero_repere)
      {
      switch (numero_repere)
        {
        case b_vdi_r_log:
        case b_vdi_r_log_tab:
        case b_vdi_r_num:
        case b_vdi_r_num_tab:
        case b_vdi_r_mes_nv2:
        case b_vdi_r_mes_tab_nv2:
        case b_vdi_r_num_alpha_nv1:
        case b_vdi_r_num_alpha_tab_nv1:
        case b_vdi_courbe_fic:
        case b_vdi_r_log_4etats:
        case b_vdi_r_log_4etats_tab:
        case b_vdi_r_num_dep:
        case b_vdi_r_num_dep_tab :
				case b_vdi_control_radio :
				case b_vdi_control_liste :
				case b_vdi_control_combobox :
          if (donnees_geo->pos_donnees >= limite)
            {
            donnees_geo->pos_donnees--;
            }
          break;

        case b_vdi_e_log_nv1:
        case b_vdi_e_num:
        case b_vdi_e_mes:
        case b_vdi_courbe_temps:
        case b_vdi_courbe_cmd:
				case b_vdi_control_static:
          if (donnees_geo->pos_specif_es >= limite)
            {
            donnees_geo->pos_specif_es--;
            }
          break;

        default:
          break;
       
        }
      }
    }
  }

//------------------------------------------------------------------------
BOOL lis_couleurs2_vdi (DWORD n_ligne, DWORD indice, PG_COULEUR pnCouleur, PG_COULEUR pnCouleurFond)
  {
  PGEO_VDI donnees_geo;
  BOOL      bRetour;
  PVDI_R_LOG vdi_r_log;
  PVDI_R_LOG_TAB vdi_r_log_tab;

  donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi, n_ligne);
  switch (donnees_geo->numero_repere)
    {
    case b_vdi_r_log :
      vdi_r_log = (PVDI_R_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log,donnees_geo->pos_donnees);
      if ((vdi_r_log->tipe_anim == c_res_c) ||
          (vdi_r_log->tipe_anim == c_res_couleur))
        {
        if (indice > 0)
          {
          (*pnCouleur) = vdi_r_log->color_m_n;
          }
        else
          {
          (*pnCouleur) = vdi_r_log->color_a_n;
          }
        (*pnCouleurFond) = vdi_r_log->couleur_fond;
        bRetour = TRUE;
        }
      else
        {
        bRetour = FALSE;
        }
      break;

    case b_vdi_r_log_tab :
      vdi_r_log_tab = (PVDI_R_LOG_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_tab,donnees_geo->pos_donnees);
      if ((vdi_r_log_tab->tipe_anim == c_res_c) ||
          (vdi_r_log_tab->tipe_anim == c_res_couleur))
        {
        if (indice > 0)
          {
          (*pnCouleur) = vdi_r_log_tab->color_m_n;
          }
        else
          {
          (*pnCouleur) = vdi_r_log_tab->color_a_n;
          }
        (*pnCouleurFond) = vdi_r_log_tab->couleur_fond;
        bRetour = TRUE;
        }
      else
        {
        bRetour = FALSE;
        }
      break;

    default:
      bRetour = FALSE;
      break;
    }
  return (bRetour);
  }

//-------------------------------------------------------------------------
BOOL lis_couleurs4_vdi (DWORD n_ligne, DWORD indice, PG_COULEUR pnCouleur, PG_COULEUR pnCouleurFond)
  {
  PGEO_VDI								donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi, n_ligne);
  BOOL										bRetour = FALSE;

  switch (donnees_geo->numero_repere)
    {
    case b_vdi_r_log_4etats:
			{
			PVDI_R_LOG_4_ETATS	vdi_r_log_4etats = (PVDI_R_LOG_4_ETATS)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats,donnees_geo->pos_donnees);
      if ((vdi_r_log_4etats->tipe_anim == c_res_c) ||
          (vdi_r_log_4etats->tipe_anim == c_res_couleur))
        {
        switch (indice)
          {
          case 3:
            (*pnCouleur) = vdi_r_log_4etats->color_h_h;
            break;
          case 2:
            (*pnCouleur) = vdi_r_log_4etats->color_h_b;
            break;
          case 1:
            (*pnCouleur) = vdi_r_log_4etats->color_b_h;
            break;
          case 0:
            (*pnCouleur) = vdi_r_log_4etats->color_b_b;
            break;
          }
        (*pnCouleurFond) = vdi_r_log_4etats->couleur_fond;
        bRetour = TRUE;
        }
			}
      break;

    case b_vdi_r_log_4etats_tab:
			{
			PVDI_R_LOG_4_ETATS_TAB vdi_r_log_4etats_tab = (PVDI_R_LOG_4_ETATS_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats_tab,donnees_geo->pos_donnees);
      if ((vdi_r_log_4etats_tab->tipe_anim == c_res_c) ||
          (vdi_r_log_4etats_tab->tipe_anim == c_res_couleur))
        {
        switch (indice)
          {
          case 3:
            (*pnCouleur) = vdi_r_log_4etats_tab->color_h_h;
            break;
          case 2:
            (*pnCouleur) = vdi_r_log_4etats_tab->color_h_b;
            break;
          case 1:
            (*pnCouleur) = vdi_r_log_4etats_tab->color_b_h;
            break;
          case 0:
            (*pnCouleur) = vdi_r_log_4etats_tab->color_b_b;
            break;
          }
        (*pnCouleurFond) = vdi_r_log_4etats_tab->couleur_fond;
        bRetour = TRUE;
        }
			}
      break;
    }
  return bRetour;
  }


//-------------------------------------------------------------------------
//                             EDITION                                     
//-------------------------------------------------------------------------
static BOOL test_couleur (BOOL bClignotteAutorisee, FLOAT fCouleurLue, PG_COULEUR pdwCouleur, BOOL bPortagePM)
	{
	BOOL bOk = FALSE;

	(*pdwCouleur) = G_WHITE;
	if (fCouleurLue >= 0)
		{
		if (bPortagePM && (fCouleurLue > 15) && (fCouleurLue < 32))  // syntaxe < 5.0 release 11. $$ac attention au passage nb couleur > 16
			{
			if (bClignotteAutorisee)
				{
				*pdwCouleur = (G_COULEUR)((DWORD)(fCouleurLue) - 16 + G_OFFSET_CLIGNOTANT);
				}
			else
				{
				*pdwCouleur = (G_COULEUR)((DWORD)(fCouleurLue) - 16);
				}
			bOk = TRUE;
			}
		else
			{
			if (fCouleurLue < G_NB_COULEURS)
				{
				*pdwCouleur = (G_COULEUR)(DWORD)fCouleurLue;
				bOk = TRUE;
				}
			else
				{
				if (bClignotteAutorisee)
					{
					if ((fCouleurLue >= G_OFFSET_CLIGNOTANT) && (fCouleurLue < (G_OFFSET_CLIGNOTANT + G_NB_COULEURS)))
						{
						(*pdwCouleur) = (G_COULEUR)(DWORD)fCouleurLue;
						bOk = TRUE;
						}
					}
				}
			}
		}
	return bOk;
	}

//-------------------------------------------------------------------------
static BOOL test_es (PUL vect_ul, ID_MOT_RESERVE tipe, DWORD *index)
   {
   DWORD pos_es;
   PENTREE_SORTIE es;
   BOOL bretour;

   bretour = TRUE;
   if (!bReconnaitESExistante (vect_ul, &pos_es))
     {
     bretour = FALSE;
     }
   else
     {
     es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, pos_es);
     if (es->genre != tipe)
       {
       bretour = FALSE;
       }
     }
   *index = pos_es;
   return (bretour);
   }

//---------------------------------------------------------------------------
static BOOL test_page (PUL vect_ul, DWORD *numero_page)
	{
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
	BOOL bretour;

	if (bReconnaitConstanteNum(vect_ul, &booleen, &reel, &mot))
		{
		if (reel >= 1)
			{
			(*numero_page) = (DWORD)(reel);
			bretour = TRUE;
			}
		else
			{
			bretour = FALSE;
			}
		}
	else
		{
		bretour = FALSE;
		}
  return (bretour);
	}

//'''''''''''''''''''''''generation vecteur UL''''''''''''''''''''''''''''''''
static void genere_vect_ul (DWORD ligne, PUL vect_ul,
                            int *nb_ul, int *niveau, int *indentation)

  {
  PGEO_VDI	donnees_geo;
  PVDI_R_NUM vdi_r_num;
  PVDI_R_NUM_TAB vdi_r_num_tab;
  PVDI_R_LOG vdi_r_log;
  PVDI_R_LOG_TAB vdi_r_log_tab;
  PVDI_R_MES vdi_r_mes;
  PVDI_R_MES_TAB vdi_r_mes_tab;
  PVDI_R_NUM_ALPHA vdi_r_num_alpha;
  PVDI_R_NUM_ALPHA_TAB vdi_r_num_alpha_tab;
  PVDI_COURBE_TEMPS vdi_courbe_temps;
  PVDI_COURBE_CMD vdi_courbe_cmd;
  PVDI_COURBE_FIC vdi_courbe_fic;
  PVDI_R_LOG_4_ETATS vdi_r_log_4etats;
  PVDI_R_LOG_4_ETATS_TAB vdi_r_log_4etats_tab;
  PVDI_R_NUM_DEP vdi_r_num_dep;
  PVDI_R_NUM_DEP_TAB vdi_r_num_dep_tab;
  PVDI_E_LOG vdi_e_log;
  PVDI_E_NUM vdi_e_num;
  PVDI_E_MES vdi_e_mes;
	PVDI_CONTROL_RADIO vdi_control_radio;
	PVDI_CONTROL_STATIC vdi_control_static;
	PVDI_CONTROL_LISTE vdi_control_liste;
	PVDI_CONTROL_COMBOBOX vdi_control_combobox;


  donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi, ligne);
  (*niveau) = 1;
  (*indentation) = 0;
  switch (donnees_geo->numero_repere)
    {
    case b_pt_constant : // commentaire
      (*nb_ul) = 1;
      init_vect_ul (vect_ul, (*nb_ul));
      consulte_cst_bd_c (donnees_geo->pos_donnees, vect_ul[0].show);
      break;

    case b_vdi_r_num :
      (*nb_ul) = 10;
      init_vect_ul (vect_ul, (*nb_ul));
      vdi_r_num = (PVDI_R_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num, donnees_geo->pos_donnees);
      bMotReserve (c_res_ign_r_bargraph, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES (vect_ul[2].show, vdi_r_num->position_es);
      bMotReserve (vdi_r_num->modele, vect_ul[3].show);
      erreur_encode_dw(&(vect_ul[4]), vdi_r_num->color);
      erreur_encode_dw(&(vect_ul[5]), vdi_r_num->style);
      erreur_encode_r(&(vect_ul[6]), vdi_r_num->min);
      erreur_encode_r(&(vect_ul[7]), vdi_r_num->max);
      erreur_encode_dw(&(vect_ul[8]), vdi_r_num->couleur_fond);
			erreur_encode_sz_avec_guillemets (&(vect_ul[9]), vdi_r_num->strIdentifiantEltGr);
      break;

    case b_vdi_r_num_tab :
      (*nb_ul) = 11;
      init_vect_ul (vect_ul, (*nb_ul));
      vdi_r_num_tab = (PVDI_R_NUM_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_tab, donnees_geo->pos_donnees);
      bMotReserve (c_res_ign_r_bargraph, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES (vect_ul[2].show, vdi_r_num_tab->position_es);
      erreur_encode_dw (&(vect_ul[3]), vdi_r_num_tab->indice);
      bMotReserve (vdi_r_num_tab->modele, vect_ul[4].show);
      erreur_encode_dw((&vect_ul[5]), vdi_r_num_tab->color);
      erreur_encode_dw((&vect_ul[6]), vdi_r_num_tab->style);
      erreur_encode_r((&vect_ul[7]), vdi_r_num_tab->min);
      erreur_encode_r((&vect_ul[8]), vdi_r_num_tab->max);
      erreur_encode_dw(&(vect_ul[9]), vdi_r_num_tab->couleur_fond);
			erreur_encode_sz_avec_guillemets (&(vect_ul[10]), vdi_r_num_tab->strIdentifiantEltGr);
      break;

    case b_vdi_r_log :
      (*nb_ul) = 8;
      init_vect_ul (vect_ul, (*nb_ul));
      vdi_r_log = (PVDI_R_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log, donnees_geo->pos_donnees);
      bMotReserve (c_res_ign_r_l, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES(vect_ul[2].show, vdi_r_log->position_es_normal);
      bMotReserve (vdi_r_log->tipe_anim, vect_ul[3].show);
      erreur_encode_dw((&vect_ul[4]), vdi_r_log->color_m_n);
      erreur_encode_dw((&vect_ul[5]), vdi_r_log->color_a_n);
      erreur_encode_dw(&(vect_ul[6]), vdi_r_log->couleur_fond);
			erreur_encode_sz_avec_guillemets (&(vect_ul[7]), vdi_r_log->strIdentifiantEltGr);
      break;

    case b_vdi_r_log_tab :
      (*nb_ul) = 9;
      init_vect_ul (vect_ul, (*nb_ul));
      vdi_r_log_tab = (PVDI_R_LOG_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_tab, donnees_geo->pos_donnees);
      bMotReserve (c_res_ign_r_l, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES (vect_ul[2].show, vdi_r_log_tab->position_es_normal);
      erreur_encode_dw((&vect_ul[3]), vdi_r_log_tab->indice);
      bMotReserve (vdi_r_log_tab->tipe_anim, vect_ul[4].show);
      erreur_encode_dw((&vect_ul[5]), vdi_r_log_tab->color_m_n);
      erreur_encode_dw((&vect_ul[6]), vdi_r_log_tab->color_a_n);
      erreur_encode_dw(&(vect_ul[7]), vdi_r_log_tab->couleur_fond);
			erreur_encode_sz_avec_guillemets (&(vect_ul[8]), vdi_r_log_tab->strIdentifiantEltGr);
      break;

    case b_vdi_r_log_4etats :
      (*nb_ul) = 11;
      init_vect_ul (vect_ul, (*nb_ul));
      vdi_r_log_4etats = (PVDI_R_LOG_4_ETATS)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats, donnees_geo->pos_donnees);
      bMotReserve (c_res_ign_r_l_4, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES(vect_ul[2].show, vdi_r_log_4etats->position_es_un);
      NomVarES(vect_ul[3].show, vdi_r_log_4etats->position_es_deux);
      bMotReserve (vdi_r_log_4etats->tipe_anim, vect_ul[4].show);
      erreur_encode_dw((&vect_ul[5]), vdi_r_log_4etats->color_h_h);
      erreur_encode_dw((&vect_ul[6]), vdi_r_log_4etats->color_h_b);
      erreur_encode_dw((&vect_ul[7]), vdi_r_log_4etats->color_b_h);
      erreur_encode_dw((&vect_ul[8]), vdi_r_log_4etats->color_b_b);
      erreur_encode_dw(&(vect_ul[9]), vdi_r_log_4etats->couleur_fond);
			erreur_encode_sz_avec_guillemets (&(vect_ul[10]), vdi_r_log_4etats->strIdentifiantEltGr);
      break;

    case b_vdi_r_log_4etats_tab :
      (*nb_ul) = 13;
      init_vect_ul (vect_ul, (*nb_ul));
      vdi_r_log_4etats_tab = (PVDI_R_LOG_4_ETATS_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats_tab, donnees_geo->pos_donnees);
      bMotReserve (c_res_ign_r_l_4, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES(vect_ul[2].show, vdi_r_log_4etats_tab->position_es_un);
      erreur_encode_dw((&vect_ul[3]), vdi_r_log_4etats_tab->indice_un);
      NomVarES(vect_ul[4].show, vdi_r_log_4etats_tab->position_es_deux);
      erreur_encode_dw((&vect_ul[5]), vdi_r_log_4etats_tab->indice_deux);
      bMotReserve (vdi_r_log_4etats_tab->tipe_anim, vect_ul[6].show);
      erreur_encode_dw((&vect_ul[7]), vdi_r_log_4etats_tab->color_h_h);
      erreur_encode_dw((&vect_ul[8]), vdi_r_log_4etats_tab->color_h_b);
      erreur_encode_dw((&vect_ul[9]), vdi_r_log_4etats_tab->color_b_h);
      erreur_encode_dw((&vect_ul[10]), vdi_r_log_4etats_tab->color_b_b);
      erreur_encode_dw(&(vect_ul[11]), vdi_r_log_4etats_tab->couleur_fond);
			erreur_encode_sz_avec_guillemets (&(vect_ul[12]), vdi_r_log_4etats_tab->strIdentifiantEltGr);
      break;

    case b_vdi_r_mes_nv2 :
      vdi_r_mes = (PVDI_R_MES)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_mes_nv2, donnees_geo->pos_donnees);
      if (vdi_r_mes->color_cste == 3)
        {    // on a un reflet message avec couleur en tableau
        (*nb_ul) = 7;
        init_vect_ul (vect_ul, (*nb_ul));
        bMotReserve (c_res_ign_r_m, vect_ul[0].show);
        erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
        NomVarES(vect_ul[2].show, vdi_r_mes->position_es);
        NomVarES(vect_ul[3].show, vdi_r_mes->color);
        erreur_encode_dw((&vect_ul[4]), vdi_r_mes->indice_color);
        erreur_encode_dw(&(vect_ul[5]), vdi_r_mes->couleur_fond);
   			erreur_encode_sz_avec_guillemets (&(vect_ul[6]), vdi_r_mes->strIdentifiantEltGr);
        }
      else
        {              // On a une couleur constante ou variable simple
        (*nb_ul) = 6;
        init_vect_ul (vect_ul, (*nb_ul));
        bMotReserve (c_res_ign_r_m, vect_ul[0].show);
        erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
        NomVarES(vect_ul[2].show, vdi_r_mes->position_es);
        if ((vdi_r_mes->color_cste == 1))
          {            // couleur constante
          erreur_encode_dw((&vect_ul[3]), vdi_r_mes->color);
          }
        else
          {
          NomVarES (vect_ul[3].show, vdi_r_mes->color);
          }
        erreur_encode_dw(&(vect_ul[4]), vdi_r_mes->couleur_fond);
   			erreur_encode_sz_avec_guillemets (&(vect_ul[5]), vdi_r_mes->strIdentifiantEltGr);
        }
      break;

    case b_vdi_r_mes_tab_nv2 :
      vdi_r_mes_tab = (PVDI_R_MES_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_mes_tab_nv2,donnees_geo->pos_donnees);
      if ((vdi_r_mes_tab->color_cste == 3))
        {    // on a un reflet message avec couleur en tableau
        (*nb_ul) = 8;
        init_vect_ul (vect_ul, (*nb_ul));
        bMotReserve (c_res_ign_r_m, vect_ul[0].show);
        erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
        NomVarES(vect_ul[2].show, vdi_r_mes_tab->position_es);
        erreur_encode_dw((&vect_ul[3]), vdi_r_mes_tab->indice);
        NomVarES(vect_ul[4].show, vdi_r_mes_tab->color);
        erreur_encode_dw((&vect_ul[5]), vdi_r_mes_tab->indice_color);
        erreur_encode_dw(&(vect_ul[6]), vdi_r_mes_tab->couleur_fond);
   			erreur_encode_sz_avec_guillemets (&(vect_ul[7]), vdi_r_mes_tab->strIdentifiantEltGr);
        }
      else
        {              // On a une couleur constante ou variable simple
        (*nb_ul) = 7;
        init_vect_ul (vect_ul, (*nb_ul));
        bMotReserve (c_res_ign_r_m, vect_ul[0].show);
        erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
        NomVarES(vect_ul[2].show, vdi_r_mes_tab->position_es);
        erreur_encode_dw((&vect_ul[3]), vdi_r_mes_tab->indice);
        if ((vdi_r_mes_tab->color_cste == 1))
          {            // couleur constante
          erreur_encode_dw((&vect_ul[4]), vdi_r_mes_tab->color);
          }
        else
          {
          NomVarES(vect_ul[4].show, vdi_r_mes_tab->color);
          }
        erreur_encode_dw(&(vect_ul[5]), vdi_r_mes_tab->couleur_fond);
   			erreur_encode_sz_avec_guillemets (&(vect_ul[6]), vdi_r_mes_tab->strIdentifiantEltGr);
        }
      break;

    case b_vdi_r_num_alpha_nv1 :
      vdi_r_num_alpha = (PVDI_R_NUM_ALPHA)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_alpha_nv1, donnees_geo->pos_donnees);
      if ((vdi_r_num_alpha->color_cste == 3))
        {
        (*nb_ul) = 9;
        init_vect_ul (vect_ul, (*nb_ul));
        bMotReserve (c_res_ign_r_an, vect_ul[0].show);
        erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
        NomVarES(vect_ul[2].show, vdi_r_num_alpha->position_es);
        NomVarES(vect_ul[3].show, vdi_r_num_alpha->color);
        erreur_encode_dw((&vect_ul[4]), vdi_r_num_alpha->indice_color);
        erreur_encode_dw((&vect_ul[5]), vdi_r_num_alpha->nbr_car);
        erreur_encode_dw((&vect_ul[6]), vdi_r_num_alpha->nbr_dec);
        erreur_encode_dw(&(vect_ul[7]), vdi_r_num_alpha->couleur_fond);
   			erreur_encode_sz_avec_guillemets (&(vect_ul[8]), vdi_r_num_alpha->strIdentifiantEltGr);
        }
      else
        {
        (*nb_ul) = 8;
        init_vect_ul (vect_ul, (*nb_ul));
        bMotReserve (c_res_ign_r_an, vect_ul[0].show);
        erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
        NomVarES(vect_ul[2].show, vdi_r_num_alpha->position_es);
        if ((vdi_r_num_alpha->color_cste == 1))
          {
          erreur_encode_dw((&vect_ul[3]), vdi_r_num_alpha->color);
          }
        else
          {
          NomVarES(vect_ul[3].show, vdi_r_num_alpha->color);
          }
        erreur_encode_dw((&vect_ul[4]), vdi_r_num_alpha->nbr_car);
        erreur_encode_dw((&vect_ul[5]), vdi_r_num_alpha->nbr_dec);
        erreur_encode_dw(&(vect_ul[6]), vdi_r_num_alpha->couleur_fond);
   			erreur_encode_sz_avec_guillemets (&(vect_ul[7]), vdi_r_num_alpha->strIdentifiantEltGr);
        }
      break;

    case b_vdi_r_num_alpha_tab_nv1 :
      vdi_r_num_alpha_tab = (PVDI_R_NUM_ALPHA_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_alpha_tab_nv1,donnees_geo->pos_donnees);
      if (vdi_r_num_alpha_tab->color_cste == 3)
        {
        (*nb_ul) = 10;
        init_vect_ul (vect_ul, (*nb_ul));
        bMotReserve (c_res_ign_r_an, vect_ul[0].show);
        erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
        NomVarES(vect_ul[2].show, vdi_r_num_alpha_tab->position_es);
        erreur_encode_dw((&vect_ul[3]), vdi_r_num_alpha_tab->indice);
        NomVarES(vect_ul[4].show, vdi_r_num_alpha_tab->color);
        erreur_encode_dw((&vect_ul[5]), vdi_r_num_alpha_tab->indice_color);
        erreur_encode_dw((&vect_ul[6]), vdi_r_num_alpha_tab->nbr_car);
        erreur_encode_dw((&vect_ul[7]), vdi_r_num_alpha_tab->nbr_dec);
        erreur_encode_dw(&(vect_ul[8]), vdi_r_num_alpha_tab->couleur_fond);
   			erreur_encode_sz_avec_guillemets (&(vect_ul[9]), vdi_r_num_alpha_tab->strIdentifiantEltGr);
        }
      else
        {
        (*nb_ul) = 9;
        init_vect_ul (vect_ul, (*nb_ul));
        bMotReserve (c_res_ign_r_an, vect_ul[0].show);
        erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
        NomVarES(vect_ul[2].show, vdi_r_num_alpha_tab->position_es);
        erreur_encode_dw((&vect_ul[3]), vdi_r_num_alpha_tab->indice);
        if (vdi_r_num_alpha_tab->color_cste == 1)
          {
          erreur_encode_dw((&vect_ul[4]), vdi_r_num_alpha_tab->color);
          }
        else
          {
          NomVarES(vect_ul[4].show, vdi_r_num_alpha_tab->color);
          }
        erreur_encode_dw((&vect_ul[5]), vdi_r_num_alpha_tab->nbr_car);
        erreur_encode_dw((&vect_ul[6]), vdi_r_num_alpha_tab->nbr_dec);
        erreur_encode_dw(&(vect_ul[7]), vdi_r_num_alpha_tab->couleur_fond);
   			erreur_encode_sz_avec_guillemets (&(vect_ul[8]), vdi_r_num_alpha_tab->strIdentifiantEltGr);
        }
      break;

    case b_vdi_r_num_dep :
      (*nb_ul) = 8;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_r_l_deplacement, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      vdi_r_num_dep = (PVDI_R_NUM_DEP)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_dep,donnees_geo->pos_donnees);
      NomVarES(vect_ul[2].show, vdi_r_num_dep->position_es_un);
      NomVarES(vect_ul[3].show, vdi_r_num_dep->position_es_deux);
      bMotReserve (vdi_r_num_dep->tipe_anim, vect_ul[4].show);
      if ((vdi_r_num_dep->color_cste == 1 ))
        {
        erreur_encode_dw((&vect_ul[5]), vdi_r_num_dep->color);   // On a un num�rique
        }
      else
        {
        NomVarES(vect_ul[5].show, vdi_r_num_dep->color);
        }
      erreur_encode_dw(&(vect_ul[6]), vdi_r_num_dep->couleur_fond);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[7]), vdi_r_num_dep->strIdentifiantEltGr);
      break;

    case b_vdi_r_num_dep_tab :
      (*nb_ul) = 10;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_r_l_deplacement, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      vdi_r_num_dep_tab = (PVDI_R_NUM_DEP_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_dep_tab,donnees_geo->pos_donnees);
      NomVarES(vect_ul[2].show, vdi_r_num_dep_tab->position_es_un);
      erreur_encode_dw((&vect_ul[3]), vdi_r_num_dep_tab->indice_un);
      NomVarES(vect_ul[4].show, vdi_r_num_dep_tab->position_es_deux);
      erreur_encode_dw((&vect_ul[5]), vdi_r_num_dep_tab->indice_deux);
      bMotReserve (vdi_r_num_dep_tab->tipe_anim, vect_ul[6].show);
      if ((vdi_r_num_dep_tab->color_cste == 1 ))
        {
        erreur_encode_dw((&vect_ul[7]), vdi_r_num_dep_tab->color);   // On a un num�rique
        }
      else
        {
        NomVarES(vect_ul[7].show, vdi_r_num_dep_tab->color);
        }
      erreur_encode_dw(&(vect_ul[8]), vdi_r_num_dep_tab->couleur_fond);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[9]), vdi_r_num_dep_tab->strIdentifiantEltGr);
      break;

    case b_vdi_courbe_temps :
      (*nb_ul) = 14;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_r_c_temps, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES (vect_ul[2].show, donnees_geo->pos_es);
      vdi_courbe_temps = (PVDI_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_temps, donnees_geo->pos_specif_es);
      NomVarES (vect_ul[3].show, vdi_courbe_temps->pos_es_a_ech);
      erreur_encode_dw((&vect_ul[4]), vdi_courbe_temps->nbr_echantillon);
      erreur_encode_i4 ((&vect_ul[5]), vdi_courbe_temps->periode_echantillon);
      erreur_encode_dw ((&vect_ul[6]), vdi_courbe_temps->couleur);
      erreur_encode_dw ((&vect_ul[7]), vdi_courbe_temps->style);
      erreur_encode_r ((&vect_ul[8]), vdi_courbe_temps->min);
      erreur_encode_r ((&vect_ul[9]), vdi_courbe_temps->max);
      erreur_encode_i ((&vect_ul[10]), vdi_courbe_temps->nb_pts_scroll);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[11].show);
      erreur_encode_dw(&(vect_ul[12]), vdi_courbe_temps->couleur_fond);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[13]), vdi_courbe_temps->strIdentifiantEltGr);
      break;

    case b_vdi_courbe_cmd :
      (*nb_ul) = 14;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_r_c_commande, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES (vect_ul[2].show, donnees_geo->pos_es);
      vdi_courbe_cmd = (PVDI_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_cmd, donnees_geo->pos_specif_es);
      NomVarES (vect_ul[3].show, vdi_courbe_cmd->pos_es_a_ech);
      erreur_encode_dw((&vect_ul[4]), vdi_courbe_cmd->nbr_echantillon);
      NomVarES (vect_ul[5].show, vdi_courbe_cmd->pos_es_echantillonnage);
      erreur_encode_dw ((&vect_ul[6]), vdi_courbe_cmd->couleur);
      erreur_encode_dw ((&vect_ul[7]), vdi_courbe_cmd->style);
      erreur_encode_r ((&vect_ul[8]), vdi_courbe_cmd->min);
      erreur_encode_r ((&vect_ul[9]), vdi_courbe_cmd->max);
      erreur_encode_i ((&vect_ul[10]), vdi_courbe_cmd->nb_pts_scroll);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[11].show);
      erreur_encode_dw(&(vect_ul[12]), vdi_courbe_cmd->couleur_fond);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[13]), vdi_courbe_cmd->strIdentifiantEltGr);
      break;

    case b_vdi_courbe_fic :
      (*nb_ul) = 14;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_r_c_archivage, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      vdi_courbe_fic = (PVDI_COURBE_FIC)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_fic,donnees_geo->pos_donnees);
      NomVarES(vect_ul[2].show, vdi_courbe_fic->pos_es_fic);
      NomVarES(vect_ul[3].show, vdi_courbe_fic->pos_es_couleur);
      NomVarES(vect_ul[4].show, vdi_courbe_fic->pos_es_style);
      NomVarES(vect_ul[5].show, vdi_courbe_fic->pos_es_modele);
      NomVarES(vect_ul[6].show, vdi_courbe_fic->pos_es_min);
      NomVarES(vect_ul[7].show, vdi_courbe_fic->pos_es_max);
      NomVarES(vect_ul[8].show, vdi_courbe_fic->pos_es_taille_enr);
      NomVarES(vect_ul[9].show, vdi_courbe_fic->pos_es_offset_enr);
      NomVarES(vect_ul[10].show, vdi_courbe_fic->pos_es_num_enr);
      NomVarES(vect_ul[11].show, vdi_courbe_fic->pos_es_nb_pts);
      NomVarES(vect_ul[12].show, vdi_courbe_fic->pos_es_cmd_affichage);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[13]), vdi_courbe_fic->strIdentifiantEltGr);
      break;

    case b_vdi_e_log_nv1 :
      (*nb_ul) = 6;
      init_vect_ul (vect_ul, (*nb_ul));
      vdi_e_log = (PVDI_E_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_log_nv1,donnees_geo->pos_specif_es);
      bMotReserve (c_res_ign_e_l, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES(vect_ul[2].show, donnees_geo->pos_es);
      consulte_cst_bd_c (donnees_geo->pos_initial,vect_ul[3].show);
      erreur_encode_dw (&(vect_ul[4]), vdi_e_log->selecteur);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[5]), vdi_e_log->strIdentifiantEltGr);
      break;

    case b_vdi_e_num :
			(*nb_ul) = 14;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_e_n, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES(vect_ul[2].show, donnees_geo->pos_es);
      vdi_e_num = (PVDI_E_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_num, donnees_geo->pos_specif_es);
      erreur_encode_dw((&vect_ul[3]), vdi_e_num->couleur);
      erreur_encode_i4((&vect_ul[4]), vdi_e_num->nbr_caractere);
      erreur_encode_r((&vect_ul[5]), vdi_e_num->min);
      erreur_encode_r((&vect_ul[6]), vdi_e_num->max);
      consulte_cst_bd_c (donnees_geo->pos_initial, vect_ul[7].show);
      erreur_encode_bw(&(vect_ul[8]), vdi_e_num->cadre);
      erreur_encode_bw(&(vect_ul[9]), vdi_e_num->masque);
      erreur_encode_bw(&(vect_ul[10]), vdi_e_num->video_focus);
      erreur_encode_dw(&(vect_ul[11]), vdi_e_num->couleur_fond);
			if (vdi_e_num->NbDecimales >= 0)
				{
				erreur_encode_i4((&vect_ul[12]), vdi_e_num->NbDecimales); //else champ vierge
				}			 
   		erreur_encode_sz_avec_guillemets (&(vect_ul[13]), vdi_e_num->strIdentifiantEltGr);
      break;

    case b_vdi_e_mes :
      (*nb_ul) = 11;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_e_m, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES(vect_ul[2].show, donnees_geo->pos_es);
      vdi_e_mes = (PVDI_E_MES)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_mes,donnees_geo->pos_specif_es);
      erreur_encode_dw((&vect_ul[3]), vdi_e_mes->couleur);
      erreur_encode_dw((&vect_ul[4]), vdi_e_mes->nbr_caractere);
      consulte_cst_bd_c (donnees_geo->pos_initial,vect_ul[5].show);
      erreur_encode_bw(&(vect_ul[6]), vdi_e_mes->cadre);
      erreur_encode_bw(&(vect_ul[7]), vdi_e_mes->masque);
      erreur_encode_bw(&(vect_ul[8]), vdi_e_mes->video_focus);
      erreur_encode_dw(&(vect_ul[9]), vdi_e_mes->couleur_fond);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[10]), vdi_e_mes->strIdentifiantEltGr);
      break;

    case b_vdi_control_static:
      (*nb_ul) = 5;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_control_static, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      NomVarES(vect_ul[2].show, donnees_geo->pos_es);
      consulte_cst_bd_c (donnees_geo->pos_initial,vect_ul[3].show);
      vdi_control_static = (PVDI_CONTROL_STATIC)pointe_enr (szVERIFSource, __LINE__, b_vdi_control_static,donnees_geo->pos_specif_es);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[4]), vdi_control_static->strIdentifiantEltGr);
      break;

    case b_vdi_control_radio :
      (*nb_ul) = 4;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_control_radio, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      vdi_control_radio = (PVDI_CONTROL_RADIO)pointe_enr (szVERIFSource, __LINE__, b_vdi_control_radio,donnees_geo->pos_donnees);
      NomVarES(vect_ul[2].show, vdi_control_radio->position_es);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[3]), vdi_control_radio->strIdentifiantEltGr);
      break;

    case b_vdi_control_liste:
      (*nb_ul) = 5;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_control_liste, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      vdi_control_liste = (PVDI_CONTROL_LISTE)pointe_enr (szVERIFSource, __LINE__, b_vdi_control_liste,donnees_geo->pos_donnees);
      NomVarES(vect_ul[2].show, vdi_control_liste->position_es_liste);
      NomVarES(vect_ul[3].show, vdi_control_liste->position_es_indice);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[4]), vdi_control_liste->strIdentifiantEltGr);
      break;

    case b_vdi_control_combobox:
      (*nb_ul) = 6;
      init_vect_ul (vect_ul, (*nb_ul));
      bMotReserve (c_res_ign_control_combobox, vect_ul[0].show);
      erreur_encode_dw (&(vect_ul[1]), donnees_geo->numero_page);
      vdi_control_combobox = (PVDI_CONTROL_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, b_vdi_control_combobox,donnees_geo->pos_donnees);
      NomVarES(vect_ul[2].show, vdi_control_combobox->position_es_liste);
      NomVarES(vect_ul[3].show, vdi_control_combobox->position_es_indice);
      NomVarES(vect_ul[4].show, vdi_control_combobox->position_es_saisie);
   		erreur_encode_sz_avec_guillemets (&(vect_ul[5]), vdi_control_combobox->strIdentifiantEltGr);
      break;

    default:
      (*nb_ul) = 1;
      init_vect_ul (vect_ul, (*nb_ul));
			StrCopy (vect_ul[0].show, "DESCRIPTION INVALIDE ");
			VerifWarningExit;
      break;

    }
  }

//--------------------------------------------------------------------
// Identifiant mis � nul si invalide ou inexistant
// Lors des appels on suppose que le vect-ul a �t� initialis�
static void MajIdentifiantEltGraphique (const UL *pUL, PSTR strId)
	{
	ID_MOT_RESERVE	IdGenre;

	StrSetNull (strId);
  if (bReconnaitConstanteValide (pUL, &IdGenre))
		{
		if (IdGenre == c_res_message)
			{
			char TmpShow[c_nb_car_ex_mes];
			StrCopy (TmpShow, pUL->show);
			StrDeleteDoubleQuotation (TmpShow);
			if (StrLength (TmpShow) <= c_NB_CAR_ID_ELT_GR_ANIM)
				{
				StrCopy (strId, TmpShow);
				}
			}
		}
	}
	
//--------------------------------------------------------------------
static DWORD valide_courbe_temps (PUL vect_ul, int nb_ul, DWORD ligne,
                                 REQUETE_EDIT action, DWORD *position_ident,
                                 DWORD *pos_init, BOOL *exist_init,
                                 PVDI_COURBE_TEMPS spec_courbe_temps, BOOL bPortagePM)

  {
  BOOL exist_ident;
  PGEO_VDI val_geo;
  DWORD position_e_s;
  PENTREE_SORTIE es;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  FLOAT initial_num;
  BOOL initial_log;
  BOOL un_log;
  BOOL un_num;

  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (action != MODIFIE_LIGNE)
    {
    if (exist_ident)
      {
      err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
      }
    }
  else
    { // modification
    val_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi, ligne);
    if ((val_geo->numero_repere == b_vdi_courbe_temps))
      {
      if ((exist_ident) && (!bStrIEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
        {
        err(IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    else
      {
      if (exist_ident)
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    }
  if (!bReconnaitESExistante((vect_ul+1), &position_e_s))
    {
    err(75);
    }
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_e_s);
  if (es->genre != c_res_numerique)
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  spec_courbe_temps->pos_es_a_ech = position_e_s;
  if (!bReconnaitConstanteNum((vect_ul+2), &booleen, &reel, &mot))
    {
    err(76);
    }
  spec_courbe_temps->nbr_echantillon = (DWORD)(reel);
  if (!bReconnaitConstanteNum((vect_ul+3), &booleen, &reel, &mot))
    {
    err(77);
    }
  spec_courbe_temps->periode_echantillon = (LONG) (reel);
  if ((!bReconnaitConstanteNum((vect_ul+4), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(spec_courbe_temps->couleur), bPortagePM)))
    {
    err(43);
    }
  if ((!bReconnaitConstanteNum((vect_ul+5), &booleen, &reel, &mot)) ||
      (reel < 0)  || (reel > 7))
    {
    err(163);
    }
  spec_courbe_temps->style = (DWORD)(reel);
  if (!bReconnaitConstanteNum((vect_ul+6), &booleen, &reel, &mot))
    {
    err(55);
    }
  spec_courbe_temps->min = reel;
  if (!bReconnaitConstanteNum((vect_ul+7), &booleen, &reel, &mot))
    {
    err(56);
    }
  spec_courbe_temps->max = reel;
  if (spec_courbe_temps->min >= spec_courbe_temps->max)
    {
    err(74);
    }
  if (!bReconnaitConstanteNum((vect_ul+8), &booleen, &reel, &mot))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (((DWORD)(reel)) > spec_courbe_temps->nbr_echantillon)
    {
    err(76);
    }
  spec_courbe_temps->nb_pts_scroll = (int)(reel);
  if (!bReconnaitConstante ((vect_ul+9), &initial_log, &initial_num,
                        &un_log, &un_num, exist_init, pos_init))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!un_log)
    {
    err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
    }
  if ((!bReconnaitConstanteNum((vect_ul+10), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(spec_courbe_temps->couleur_fond), bPortagePM)))
    {
    err(43);
    }
	MajIdentifiantEltGraphique ((vect_ul+11), spec_courbe_temps->strIdentifiantEltGr);
  return (0);
  }

//----------------------------------------------------------------------
static DWORD valide_courbe_cmd   (PUL vect_ul, int nb_ul, DWORD ligne,
                                 REQUETE_EDIT action, DWORD *position_ident,
                                 DWORD *pos_init, BOOL *exist_init,
                                 PVDI_COURBE_CMD spec_courbe_cmd, BOOL bPortagePM)

  {
  BOOL exist_ident;
  PGEO_VDI val_geo;
  DWORD position_e_s;
  PENTREE_SORTIE es;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  FLOAT initial_num;
  BOOL initial_log;
  BOOL un_log;
  BOOL un_num;

  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err (IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (action != MODIFIE_LIGNE)
    {
    if (exist_ident)
      {
      err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
      }
    }
  else
    { // modification
    val_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);
    if ((val_geo->numero_repere == b_vdi_courbe_cmd))
      {
      if ((exist_ident) && (!bStrIEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      } // deja description donnees
    else
      {
      if (exist_ident)
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    }
  if (!bReconnaitESExistante ((vect_ul+1),  &position_e_s))
    {
    err(75);
    }
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_e_s);
  if (es->genre != c_res_numerique)
    {
    err(87);
    }
  spec_courbe_cmd->pos_es_a_ech = position_e_s;
  if (!bReconnaitConstanteNum((vect_ul+2), &booleen, &reel, &mot))
    {
    err(76);
    }
  spec_courbe_cmd->nbr_echantillon = (DWORD)(reel);
  if (!bReconnaitESExistante((vect_ul+3),  &position_e_s))
    {
    err(178);
    }
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_e_s);
  if (es->genre != c_res_logique)
    {
    err(177);
    }
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s))
    {
    err(178);
    }
  spec_courbe_cmd->pos_es_echantillonnage = position_e_s;
  if ((!bReconnaitConstanteNum((vect_ul+4), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(spec_courbe_cmd->couleur), bPortagePM)))
    {
    err(43);
    }
  if ((!bReconnaitConstanteNum((vect_ul+5), &booleen, &reel, &mot)) ||
      (reel < 0)  || (reel > 7))
    {
    err(163);
    }
  spec_courbe_cmd->style = (DWORD)(reel);
  if (!bReconnaitConstanteNum((vect_ul+6), &booleen, &reel, &mot))
    {
    err(55);
    }
  spec_courbe_cmd->min = reel;
  if (!bReconnaitConstanteNum((vect_ul+7), &booleen, &reel, &mot))
    {
    err(56);
    }
  spec_courbe_cmd->max = reel;
  if (spec_courbe_cmd->min >= spec_courbe_cmd->max)
    {
    err(74);
    }
  if (!bReconnaitConstanteNum((vect_ul+8), &booleen, &reel, &mot))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (((DWORD)(reel)) > spec_courbe_cmd->nbr_echantillon)
    {
    err(76);
    }
  spec_courbe_cmd->nb_pts_scroll = (int)(reel);
  if (!bReconnaitConstante ((vect_ul+9), &initial_log, &initial_num,
                        &un_log, &un_num,exist_init,pos_init))
    {
    err(34);
    }
  if (!un_log)
    {
    err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
    }
  if ((!bReconnaitConstanteNum((vect_ul+10), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(spec_courbe_cmd->couleur_fond), bPortagePM)))
    {
    err(43);
    }
	MajIdentifiantEltGraphique ((vect_ul+11), spec_courbe_cmd->strIdentifiantEltGr);
  return (0);
  }

//--------------------------------------------------------------------------
static DWORD valide_courbe_archive   (PUL vect_ul, int nb_ul, DWORD ligne,
                                     REQUETE_EDIT action, DWORD *position_ident,
                                     DWORD *pos_init, BOOL *exist_init,
                                     PVDI_COURBE_FIC spec_courbe_fic)
  {
  DWORD position_e_s;
  PENTREE_SORTIE es;

  if (!bReconnaitESExistante((vect_ul+0), &position_e_s))
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_e_s);
  if (es->genre != c_res_message)
    {
    err(28);
    }
  spec_courbe_fic->pos_es_fic = position_e_s;
  if (!test_es ((vect_ul+1),c_res_numerique,&spec_courbe_fic->pos_es_couleur))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+2),c_res_numerique,&spec_courbe_fic->pos_es_style))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+3),c_res_numerique,&spec_courbe_fic->pos_es_modele))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+4),c_res_numerique,&spec_courbe_fic->pos_es_min))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+5),c_res_numerique,&spec_courbe_fic->pos_es_max))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+6),c_res_numerique,&spec_courbe_fic->pos_es_taille_enr))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+7),c_res_numerique,&spec_courbe_fic->pos_es_offset_enr))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+8),c_res_numerique,&spec_courbe_fic->pos_es_num_enr))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+9),c_res_numerique,&spec_courbe_fic->pos_es_nb_pts))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!test_es ((vect_ul+10),c_res_logique,&spec_courbe_fic->pos_es_cmd_affichage))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
	MajIdentifiantEltGraphique ((vect_ul+11), spec_courbe_fic->strIdentifiantEltGr);
  return (0);
  }

//-----------------------------------------------------------------------
static DWORD valide_deplacement  (PUL vect_ul, int nb_ul, DWORD ligne,
                                 REQUETE_EDIT action, DWORD *position_ident,
                                 DWORD *pos_init, BOOL *exist_init,
                                 DWORD *numero_repere,
                                 PVDI_R_NUM_DEP spec_r_num_dep,
                                 PVDI_R_NUM_DEP_TAB spec_r_num_dep_tab, BOOL bPortagePM)
  {
  BOOL exist_ident;
  BOOL exist_ident2;
  DWORD position_ident2;
  BOOL exist_ident3;
  DWORD position_ident3;
  PENTREE_SORTIE es;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  DWORD pos_es_var_x;
  DWORD pos_es_var_y;
  DWORD pos_es_var_couleur;
  DWORD indice_tab_x;
  DWORD indice_tab_y;
  DWORD index_ul;
  BOOL un_tableau_x;
  BOOL un_tableau_y;
  ID_MOT_RESERVE tipe_anim;
  DWORD color_cst;
  G_COULEUR color_m_n;
  G_COULEUR color_fond;

  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  else
    {
    if (!exist_ident)
      {
      err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
      }
    pos_es_var_x = dwIdVarESFromPosIdentif(*position_ident);
    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pos_es_var_x);
    if (es->genre != c_res_numerique)
      {
      err(28);
      }
    un_tableau_x = (BOOL)( (es->sens == c_res_variable_ta_es) ||
                              (es->sens == c_res_variable_ta_e) ||
                              (es->sens == c_res_variable_ta_s) );
    }
  if (un_tableau_x)
    {
    if (!bReconnaitConstanteNum((vect_ul+1), &booleen, &reel, &mot))
      {
      err(43);
      }
    if ((reel < 0) || (reel > 2000))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    indice_tab_x = (DWORD)(reel);
    index_ul = 2;
    }
  else
    {
    index_ul = 1;
    }
  if (!bReconnaitESValide ((vect_ul+index_ul), &exist_ident2, &position_ident2))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  else
    {
    if (!exist_ident2)
      {
      err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
      }
    pos_es_var_y = dwIdVarESFromPosIdentif (position_ident2);
    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pos_es_var_y);
    if (es->genre != c_res_numerique)
      {
      err(28);
      }
    un_tableau_y = (BOOL)( (es->sens == c_res_variable_ta_es) ||
                              (es->sens == c_res_variable_ta_e) ||
                              (es->sens == c_res_variable_ta_s) );
    if (((un_tableau_x) && (!un_tableau_y)) ||
        ((!un_tableau_x) && (un_tableau_y)))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    }
  if (un_tableau_y)
    {
    if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
      {
      err(43);
      }
    if ((reel < 0) || (reel > 2000))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    indice_tab_y = (DWORD)(reel);
    index_ul = 4;
    }
  else
    {
    index_ul = 2;
    }
  if (!bReconnaitMotReserve((vect_ul+index_ul)->show, &tipe_anim))
    {
    err(168);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
    {
    if (bReconnaitESValide ((vect_ul+index_ul+1), &exist_ident3, &position_ident3))
      {
      if (!exist_ident3)
        {
        err(43);
        }
      pos_es_var_couleur = dwIdVarESFromPosIdentif (position_ident3);
      es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pos_es_var_couleur);
      if (es->genre != c_res_numerique)
        {
        err(36);
        }
      if ((BOOL)( (es->sens == c_res_variable_ta_es) ||
                     (es->sens == c_res_variable_ta_e) ||
                     (es->sens == c_res_variable_ta_s) ))
        {
        err(43);
        }
      color_m_n = G_RED;
      color_cst = G_BLACK;
      }
    else
      {
      err(43);
      }
    }
  else
    {
    if (!test_couleur (FALSE, reel, &(color_m_n), bPortagePM))
      {
      err(43);
      }
    pos_es_var_couleur = color_m_n;
    color_cst = 1;
    }
  switch (tipe_anim)
    {
    case c_res_c:
    case c_res_couleur :
      break;

    case c_res_s:
    case c_res_style:
      if (color_m_n  > 7)
        {
        err (169);
        }
      break;
    case c_res_recoit:
    case c_res_remplissage:
      if (color_m_n  > 7)
        {
        err (170);
        }
      break;
    default:
      err(168);
      break;
    }
  color_fond = G_BLACK;
  if ((!bReconnaitConstanteNum((vect_ul+index_ul+2), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(color_fond), bPortagePM)))
    {
    err(43);
    }

	char strIdTemp [c_NB_CAR_ID_ELT_GR_ANIM + 1];
	MajIdentifiantEltGraphique ((vect_ul+index_ul+3), strIdTemp);

  if ((un_tableau_x) && (un_tableau_y))
    {
    (*numero_repere) = b_vdi_r_num_dep_tab;
    spec_r_num_dep_tab->position_es_un = pos_es_var_x;
    spec_r_num_dep_tab->indice_un = indice_tab_x;
    spec_r_num_dep_tab->position_es_deux = pos_es_var_y;
    spec_r_num_dep_tab->indice_deux = indice_tab_y;
    spec_r_num_dep_tab->tipe_anim = tipe_anim;
    spec_r_num_dep_tab->color_cste = color_cst;
    spec_r_num_dep_tab->color = pos_es_var_couleur;
    spec_r_num_dep_tab->couleur_fond = color_fond;
		StrCopy (spec_r_num_dep_tab->strIdentifiantEltGr, strIdTemp);
    }
  else
    {
    (*numero_repere) = b_vdi_r_num_dep;
    spec_r_num_dep->position_es_un = pos_es_var_x;
    spec_r_num_dep->position_es_deux = pos_es_var_y;
    spec_r_num_dep->tipe_anim = tipe_anim;
    spec_r_num_dep->color_cste = color_cst;
    spec_r_num_dep->color = pos_es_var_couleur;
    spec_r_num_dep->couleur_fond = color_fond;
		StrCopy (spec_r_num_dep->strIdentifiantEltGr, strIdTemp);
    }
  return (0);
  }

//------------------------------------------------------------------------
static DWORD valide_reflet_logique  (PUL vect_ul, int nb_ul, DWORD *position_ident,
                                    DWORD *numero_repere,
                                    PVDI_R_LOG spec_r_log,
                                    PVDI_R_LOG_TAB spec_r_log_tab, BOOL bPortagePM)
  {
  BOOL exist_ident;
  DWORD pos_es_reflet;
  BOOL un_tableau;
  PENTREE_SORTIE es;
  ID_MOT_RESERVE tipe_anim;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  DWORD indice_tab;
  G_COULEUR color_m_n;
  G_COULEUR color_a_n;
  DWORD index_ul;
  G_COULEUR color_fond;

  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!exist_ident)
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  pos_es_reflet = dwIdVarESFromPosIdentif (*position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pos_es_reflet);
  if (es->genre != c_res_logique)
    {
    err(28);
    }
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s) &&
     (es->sens != c_res_variable_ta_es) && (es->sens != c_res_variable_ta_e) &&
     (es->sens != c_res_variable_ta_s))
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  un_tableau = (BOOL)( (es->sens == c_res_variable_ta_es) ||
                          (es->sens == c_res_variable_ta_e) ||
                          (es->sens == c_res_variable_ta_s) );

  if (un_tableau)
    {
    if (!bReconnaitConstanteNum((vect_ul+1), &booleen, &reel, &mot))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    if (reel <= 0)
      {
      err(IDSERR_INDICE_TABLEAU_INVALIDE);
      }
    indice_tab = (DWORD)(reel);
    index_ul = 2;
    }
  else
    {
    index_ul = 1;
    }
  if (!bReconnaitMotReserve ((vect_ul+index_ul)->show,&tipe_anim))
    {
    err(168);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_m_n = (G_COULEUR)(DWORD)(reel);
  if (!bReconnaitConstanteNum((vect_ul+index_ul+2), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_a_n = (G_COULEUR)(DWORD)(reel);
  switch (tipe_anim)
    {
    case c_res_c:
    case c_res_couleur :
			 if ((!test_couleur (TRUE, (FLOAT)color_m_n, &(color_m_n), bPortagePM)) || (!test_couleur (TRUE, (FLOAT)color_a_n, &(color_a_n), bPortagePM)))
        {
        err (43);
        }
      break;
    case c_res_s:
    case c_res_style:
      if ((color_m_n  > 7) || (color_a_n  > 7))
        {
        err (169);
        }
      break;
    case c_res_recoit:
    case c_res_remplissage:
      if ((color_m_n  > 7) || (color_a_n  > 7))
        {
        err (170);
        }
      break;
    default:
      err(169);
      break;
    } /*case*/
  if (!bReconnaitConstanteNum((vect_ul+index_ul+3), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_fond = (G_COULEUR)(DWORD)(reel);

	char strIdTemp [c_NB_CAR_ID_ELT_GR_ANIM + 1];
	MajIdentifiantEltGraphique ((vect_ul+index_ul+4), strIdTemp);
  
	if (un_tableau)
    {
    (*numero_repere) = b_vdi_r_log_tab;
    spec_r_log_tab->position_es_normal = pos_es_reflet;
    spec_r_log_tab->indice = indice_tab;
    spec_r_log_tab->tipe_anim = tipe_anim;
    spec_r_log_tab->color_a_n = color_a_n;
    spec_r_log_tab->color_m_n = color_m_n;
    spec_r_log_tab->couleur_fond = color_fond;
		StrCopy (spec_r_log_tab->strIdentifiantEltGr, strIdTemp);
    }
  else
    {
    (*numero_repere) = b_vdi_r_log;
    spec_r_log->position_es_normal = pos_es_reflet;
    spec_r_log->tipe_anim = tipe_anim;
    spec_r_log->color_a_n = color_a_n;
    spec_r_log->color_m_n = color_m_n;
    spec_r_log->couleur_fond = color_fond;
		StrCopy (spec_r_log->strIdentifiantEltGr, strIdTemp);
    }
  return (0);
  }

//--------------------------------------------------------------------------
static DWORD valide_reflet_logique_4  (PUL vect_ul, int nb_ul, DWORD *position_ident,
                                      DWORD *numero_repere,
                                      PVDI_R_LOG_4_ETATS spec_r_log_4etats,
                                      PVDI_R_LOG_4_ETATS_TAB spec_r_log_4etats_tab, BOOL bPortagePM)
  {
  BOOL exist_ident;
  DWORD pos_es_reflet1;
  DWORD position_ident2;
  BOOL exist_ident2;
  DWORD pos_es_reflet2;
  BOOL un_tableau1;
  BOOL un_tableau2;
  PENTREE_SORTIE es;
  ID_MOT_RESERVE tipe_anim;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  DWORD indice_tab1;
  DWORD indice_tab2;
  G_COULEUR color_h_h;
  G_COULEUR color_h_b;
  G_COULEUR color_b_h;
  G_COULEUR color_b_b;
  G_COULEUR color_fond;
  DWORD index_ul;

  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!exist_ident)
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  pos_es_reflet1 = dwIdVarESFromPosIdentif (*position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pos_es_reflet1);
  if (es->genre != c_res_logique)
    {
    err(28);
    }
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s) &&
     (es->sens != c_res_variable_ta_es) && (es->sens != c_res_variable_ta_e) &&
     (es->sens != c_res_variable_ta_s))
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  un_tableau1 = (BOOL)( (es->sens == c_res_variable_ta_es) ||
                           (es->sens == c_res_variable_ta_e) ||
                           (es->sens == c_res_variable_ta_s) );

  if (un_tableau1)
    {
    if (!bReconnaitConstanteNum((vect_ul+1), &booleen, &reel, &mot))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    if (reel <= 0)
      {
      err(IDSERR_INDICE_TABLEAU_INVALIDE);
      }
    indice_tab1 = (DWORD)(reel);
    index_ul = 2;
    }
  else
    {
    index_ul = 1;
    }

  if (!bReconnaitESValide ((vect_ul+index_ul), &exist_ident2, &position_ident2))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!exist_ident2)
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  pos_es_reflet2 = dwIdVarESFromPosIdentif (position_ident2);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,pos_es_reflet2);
  if (es->genre != c_res_logique)
    {
    err(28);
    }
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s) &&
     (es->sens != c_res_variable_ta_es) && (es->sens != c_res_variable_ta_e) &&
     (es->sens != c_res_variable_ta_s))
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  un_tableau2 = (BOOL)( (es->sens == c_res_variable_ta_es) ||
                           (es->sens == c_res_variable_ta_e) ||
                           (es->sens == c_res_variable_ta_s) );
  if (((un_tableau1) && (!un_tableau2)) ||
      ((!un_tableau1) && (un_tableau2)))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (un_tableau2)
    {
    if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    if (reel <= 0)
      {
      err(IDSERR_INDICE_TABLEAU_INVALIDE);
      }
    indice_tab2 = (DWORD)(reel);
    index_ul = 4;
    }
  else
    {
    index_ul = 2;
    }
  if (!bReconnaitMotReserve ((vect_ul+index_ul)->show, &tipe_anim))
    {
    err(168);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_h_h = (G_COULEUR)(DWORD)(reel);
  if (!bReconnaitConstanteNum((vect_ul+index_ul+2), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_h_b = (G_COULEUR)(DWORD)(reel);
  if (!bReconnaitConstanteNum((vect_ul+index_ul+3), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_b_h = (G_COULEUR)(DWORD)(reel);
  if (!bReconnaitConstanteNum((vect_ul+index_ul+4), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_b_b = (G_COULEUR)(DWORD)(reel);
  switch (tipe_anim)
    {
    case c_res_c:
    case c_res_couleur :
			 if ((!test_couleur (TRUE, (FLOAT)color_h_h, &(color_h_h), bPortagePM)) || (!test_couleur (TRUE, (FLOAT)color_h_b, &(color_h_b), bPortagePM)) ||
			     (!test_couleur (TRUE, (FLOAT)color_b_h, &(color_b_h), bPortagePM)) || (!test_couleur (TRUE, (FLOAT)color_b_b, &(color_b_b), bPortagePM)))
        {
        err (43);
        }
      break;
    case c_res_s:
    case c_res_style:
      if ((color_h_h  > 7) || (color_h_b  > 7) ||
          (color_b_h  > 7) || (color_b_b  > 7))
        {
        err (169);
        }
      break;
    case c_res_recoit:
    case c_res_remplissage:
      if ((color_h_h  > 7) || (color_h_b  > 7) ||
          (color_b_h  > 7) || (color_b_b  > 7))
        {
        err (170);
        }
      break;

    default:
      err(169);
      break;
    } /*case*/
  if (!bReconnaitConstanteNum((vect_ul+index_ul+5), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_fond = (G_COULEUR)(DWORD)(reel);

	char strIdTemp [c_NB_CAR_ID_ELT_GR_ANIM + 1];
	MajIdentifiantEltGraphique ((vect_ul+index_ul+6), strIdTemp);

  if (un_tableau1)
    {
    (*numero_repere) = b_vdi_r_log_4etats_tab;
    spec_r_log_4etats_tab->position_es_un = pos_es_reflet1;
    spec_r_log_4etats_tab->indice_un = indice_tab1;
    spec_r_log_4etats_tab->position_es_deux = pos_es_reflet2;
    spec_r_log_4etats_tab->indice_deux = indice_tab2;
    spec_r_log_4etats_tab->tipe_anim = tipe_anim;
    spec_r_log_4etats_tab->color_h_h = color_h_h;
    spec_r_log_4etats_tab->color_h_b = color_h_b;
    spec_r_log_4etats_tab->color_b_h = color_b_h;
    spec_r_log_4etats_tab->color_b_b = color_b_b;
    spec_r_log_4etats_tab->couleur_fond = color_fond;
		StrCopy (spec_r_log_4etats_tab->strIdentifiantEltGr, strIdTemp);
    }
  else
    {
    (*numero_repere) = b_vdi_r_log_4etats;
    spec_r_log_4etats->position_es_un = pos_es_reflet1;
    spec_r_log_4etats->position_es_deux = pos_es_reflet2;
    spec_r_log_4etats->tipe_anim = tipe_anim;
    spec_r_log_4etats->color_h_h = color_h_h;
    spec_r_log_4etats->color_h_b = color_h_b;
    spec_r_log_4etats->color_b_h = color_b_h;
    spec_r_log_4etats->color_b_b = color_b_b;
    spec_r_log_4etats->couleur_fond = color_fond;
		StrCopy (spec_r_log_4etats->strIdentifiantEltGr, strIdTemp);
    }
  return (0);
  }

//-------------------------------------------------------------------------
static DWORD valide_reflet_message  (PUL vect_ul, int nb_ul, DWORD *position_ident,
                                    DWORD *numero_repere,
                                    PVDI_R_MES spec_r_mes,
                                    PVDI_R_MES_TAB spec_r_mes_tab, BOOL bPortagePM)
  {
  int color_cste;
  G_COULEUR color;
	DWORD pos_var_color;
  DWORD indice_color;
  DWORD indice_tab;
  DWORD position_e_s;
  BOOL exist_ident;
  BOOL un_tableau;
  PENTREE_SORTIE es;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  DWORD index_ul;
  G_COULEUR color_fond;

  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!exist_ident)
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  position_e_s = dwIdVarESFromPosIdentif (*position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_e_s);
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s) &&
     (es->sens != c_res_variable_ta_es) && (es->sens != c_res_variable_ta_e) &&
     (es->sens != c_res_variable_ta_s))
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  if (es->genre != c_res_message)
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  un_tableau = (BOOL)( (es->sens == c_res_variable_ta_es) ||
                          (es->sens == c_res_variable_ta_e) ||
                          (es->sens == c_res_variable_ta_s) );
  if (un_tableau)
    {
    if (!bReconnaitConstanteNum((vect_ul+1), &booleen, &reel, &mot))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    if (reel <= 0)
      {
      err(IDSERR_INDICE_TABLEAU_INVALIDE);
      }
    indice_tab = (DWORD)(reel);
    index_ul = 2;
    }
  else
    {
    index_ul = 1;
    }
  // gestion de la couleur
  if (!bReconnaitConstanteNum((vect_ul+index_ul), &booleen, &reel, &mot))
    { // On a une variable couleur
    if (!bReconnaitESExistante ((vect_ul+index_ul), &pos_var_color))
      {
      err(43);
      }
    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, pos_var_color);
    if (es->genre != c_res_numerique)
      {
      err(36);
      }
		color = (G_COULEUR)pos_var_color;
    if ((es->sens == c_res_variable_ta_es) || (es->sens == c_res_variable_ta_e) ||
        (es->sens == c_res_variable_ta_s))
      {   // On a une variable tableau pour la couleur
      color_cste = 3;
      if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
        {
        err(18);
        }
      indice_color = (DWORD)(reel);
      index_ul = index_ul + 2;
      }
    else
      { // variable couleur simple
      color_cste = 2;
      indice_color = 0;
      index_ul++;
      }
    }
  else
    { // couleur constante
    color_cste = 1;
    indice_color = 0;
		if (!test_couleur (FALSE, reel, &(color), bPortagePM))
      {
      err(43);
      }
    index_ul++;
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul), &booleen, &reel, &mot))
    {
    err(43);
    }
	if (!test_couleur (FALSE, reel, &(color_fond), bPortagePM))
    {
    err(43);
    }

	char strIdTemp [c_NB_CAR_ID_ELT_GR_ANIM + 1];
	MajIdentifiantEltGraphique ((vect_ul+index_ul+1), strIdTemp);

  if (un_tableau)
    {
    (*numero_repere) = b_vdi_r_mes_tab_nv2;
    spec_r_mes_tab->color_cste = color_cste;
    spec_r_mes_tab->color = color;
    spec_r_mes_tab->indice_color = indice_color;
    spec_r_mes_tab->indice = indice_tab;
    spec_r_mes_tab->position_es = position_e_s;
    spec_r_mes_tab->couleur_fond = color_fond;
		StrCopy (spec_r_mes_tab->strIdentifiantEltGr, strIdTemp);
    }
  else
    {
    (*numero_repere) = b_vdi_r_mes_nv2;
    spec_r_mes->color_cste = color_cste;
    spec_r_mes->color = color;
    spec_r_mes->indice_color = indice_color;
    spec_r_mes->position_es = position_e_s;
    spec_r_mes->couleur_fond = color_fond;
		StrCopy (spec_r_mes->strIdentifiantEltGr, strIdTemp);
    }
  return (0);
  }

//-------------------------------------------------------------------------
static DWORD valide_reflet_alpha_num  (PUL vect_ul, int nb_ul, DWORD *position_ident,
                                      DWORD *numero_repere,
                                      PVDI_R_NUM_ALPHA spec_r_num_alpha,
                                      PVDI_R_NUM_ALPHA_TAB spec_r_num_alpha_tab, BOOL bPortagePM)
  {
  DWORD color_cste;
  G_COULEUR color;
  DWORD indice_color;
  DWORD nbr_car;
  DWORD nbr_dec;
  DWORD indice_tab;
  DWORD position_e_s;
  DWORD position_e_s_2;
  BOOL exist_ident;
  BOOL un_tableau;
  PENTREE_SORTIE es;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  DWORD index_ul;
  G_COULEUR color_fond;

  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!exist_ident)
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  position_e_s = dwIdVarESFromPosIdentif (*position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_e_s);
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s) &&
     (es->sens != c_res_variable_ta_es) && (es->sens != c_res_variable_ta_e) &&
     (es->sens != c_res_variable_ta_s))
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  if (es->genre != c_res_numerique)
    {
    err(36);
    }
  un_tableau = (BOOL)( (es->sens == c_res_variable_ta_es) ||
                          (es->sens == c_res_variable_ta_e) ||
                          (es->sens == c_res_variable_ta_s) );
  if (un_tableau)
    {
    if (!bReconnaitConstanteNum((vect_ul+1), &booleen, &reel, &mot))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    if (reel <= 0)
      {
      err(IDSERR_INDICE_TABLEAU_INVALIDE);
      }
    indice_tab = (DWORD)(reel);
    index_ul = 2;
    }
  else
    {
    index_ul = 1;
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul), &booleen, &reel, &mot))
    {
    if (bReconnaitESExistante ((vect_ul+index_ul),&position_e_s_2))
      {
      es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_e_s_2);
      if (es->genre != c_res_numerique)
        {
        err(36);
        }
      if ((es->sens == c_res_variable_ta_es) || (es->sens == c_res_variable_ta_e) ||
          (es->sens == c_res_variable_ta_s))
        {   // On a une variable tableau pour la couleur
        color_cste = 3;
        if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
          {
          err(18);
          }
        color = (G_COULEUR)position_e_s_2 ;
        indice_color = (DWORD)(reel);
        index_ul++;
        }
      else
        { // variable couleur simple
        indice_color = 0;
        color = (G_COULEUR)position_e_s_2 ;
        color_cste = 2;
        }
      }
    else
      {
      err(43);
      }
    }
  else
    {
		if (!test_couleur (FALSE, reel, &(color), bPortagePM))
      {
      err(43);
      }
    color_cste = 1;
    indice_color = 0;
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
    {
    err(IDSERR_LE_NOMBRE_DE_CARACTERES_EST_INVALIDE);
    }
  nbr_car = (DWORD)(reel);
  if (nbr_car > 80)
    {
    err(IDSERR_LE_NOMBRE_DE_CARACTERES_EST_INVALIDE);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+2), &booleen, &reel, &mot))
    {
    err(59);
    }
  nbr_dec = (DWORD)(reel);
  if (nbr_dec > 20)
    {
    err(IDSERR_LE_NOMBRE_DE_CARACTERES_EST_INVALIDE);
    }
  if (nbr_car <= (nbr_dec+1))
    {
    err(IDSERR_LE_NOMBRE_DE_CARACTERES_EST_INVALIDE);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+3), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_fond = (G_COULEUR)(DWORD)(reel);

	char strIdTemp [c_NB_CAR_ID_ELT_GR_ANIM + 1];
	MajIdentifiantEltGraphique ((vect_ul+index_ul+4), strIdTemp);

  if (un_tableau)
    {
    (*numero_repere) = b_vdi_r_num_alpha_tab_nv1;
    spec_r_num_alpha_tab->color_cste = color_cste;
    spec_r_num_alpha_tab->color = color;
    spec_r_num_alpha_tab->indice_color = indice_color;
    spec_r_num_alpha_tab->nbr_dec = nbr_dec;
    spec_r_num_alpha_tab->nbr_car = nbr_car;
    spec_r_num_alpha_tab->indice = indice_tab;
    spec_r_num_alpha_tab->position_es = position_e_s;
    spec_r_num_alpha_tab->couleur_fond = color_fond;
		StrCopy (spec_r_num_alpha_tab->strIdentifiantEltGr, strIdTemp);
    }
  else
    {
    (*numero_repere) = b_vdi_r_num_alpha_nv1;
    spec_r_num_alpha->color_cste = color_cste;
    spec_r_num_alpha->color = color;
    spec_r_num_alpha->indice_color = indice_color;
    spec_r_num_alpha->nbr_dec = nbr_dec;
    spec_r_num_alpha->nbr_car = nbr_car;
    spec_r_num_alpha->position_es = position_e_s;
    spec_r_num_alpha->couleur_fond = color_fond;
		StrCopy (spec_r_num_alpha->strIdentifiantEltGr, strIdTemp);
    }
  return (0);
  }

//--------------------------------------------------------------------------
static DWORD valide_bargraph  (PUL vect_ul, int nb_ul, DWORD *position_ident,
                              DWORD *numero_repere,
                              PVDI_R_NUM spec_r_num,
                              PVDI_R_NUM_TAB spec_r_num_tab, BOOL bPortagePM)
  {
  ID_MOT_RESERVE modele;
  G_COULEUR color;
  DWORD style;
  FLOAT max;
  FLOAT min;
  DWORD indice_tab;
  DWORD position_e_s;
  BOOL exist_ident;
  BOOL un_tableau;
  PENTREE_SORTIE es;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  DWORD index_ul;
  G_COULEUR color_fond;

  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!exist_ident)
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  position_e_s = dwIdVarESFromPosIdentif (*position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,position_e_s);
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s) &&
      (es->sens != c_res_variable_ta_es) && (es->sens != c_res_variable_ta_e) &&
      (es->sens != c_res_variable_ta_s))
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  if (es->genre != c_res_numerique)
    {
    err(36);
    }
  un_tableau = (BOOL)( (es->sens == c_res_variable_ta_es) ||
                          (es->sens == c_res_variable_ta_e) ||
                          (es->sens == c_res_variable_ta_s) );
  if (un_tableau)
    {
    if (!bReconnaitConstanteNum((vect_ul+1), &booleen, &reel, &mot))
      {
      err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
      }
    if (reel <= 0)
      {
      err(IDSERR_INDICE_TABLEAU_INVALIDE);
      }
    indice_tab = (DWORD)(reel);
    index_ul = 2;
    }
  else
    {
    index_ul = 1;
    }
  if (!bReconnaitMotReserve ((vect_ul+index_ul)->show, &modele))
    {
    err (6);
    }
  if ((modele != c_res_right) &&
      (modele != c_res_left) &&
      (modele != c_res_up) &&
      (modele != c_res_down))
    {
    err (6);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+1), &booleen, &reel, &mot))
    {
    err(43);
    }
	if (!test_couleur (FALSE, reel, &(color), bPortagePM))
    {
    err(43);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+2), &booleen, &reel, &mot))
    {
    err(163);
    }
  style = (DWORD)(reel);
  if (style > 7)
    {
    err(170);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+3), &booleen, &reel, &mot))
    {
    err(55);
    }
  min = reel;
  if (!bReconnaitConstanteNum((vect_ul+index_ul+4), &booleen, &reel, &mot))
    {
    err(56);
    }
  max = reel;
  if (min >= max)
    {
    err(74);
    }
  if (!bReconnaitConstanteNum((vect_ul+index_ul+5), &booleen, &reel, &mot))
    {
    err(43);
    }
  color_fond = (G_COULEUR)(DWORD)(reel);

	char strIdTemp [c_NB_CAR_ID_ELT_GR_ANIM + 1];
	MajIdentifiantEltGraphique ((vect_ul+index_ul+6), strIdTemp);

  if (un_tableau)
    {
    (*numero_repere) = b_vdi_r_num_tab;
    spec_r_num_tab->modele  = modele;
    spec_r_num_tab->color   = color;
    spec_r_num_tab->style   = style;
    spec_r_num_tab->max     = max;
    spec_r_num_tab->min     = min;
    spec_r_num_tab->indice  = indice_tab;
    spec_r_num_tab->position_es = position_e_s;
    spec_r_num_tab->couleur_fond = color_fond;
		StrCopy (spec_r_num_tab->strIdentifiantEltGr, strIdTemp);
    }
  else
    {
    (*numero_repere) = b_vdi_r_num;
    spec_r_num->modele  = modele;
    spec_r_num->color   = color;
    spec_r_num->style   = style;
    spec_r_num->max     = max;
    spec_r_num->min     = min;
    spec_r_num->position_es = position_e_s;
    spec_r_num->couleur_fond = color_fond;
		StrCopy (spec_r_num->strIdentifiantEltGr, strIdTemp);
    }
  return (0);
  }

//----------------------------------------------------------
static DWORD valide_e_l (PUL vect_ul, int nb_ul, DWORD ligne,
                        REQUETE_EDIT action, DWORD *position_ident,
                        DWORD *pos_init, BOOL *exist_init,
                        PVDI_E_LOG spec_e_log, BOOL bPortagePM)
  {
  BOOL exist_ident;
  BOOL initial_log;
  BOOL un_log;
  BOOL un_num;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  FLOAT initial_num;
  PGEO_VDI val_geo;

  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (action != MODIFIE_LIGNE)
    {
    if (exist_ident)
      {
      err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
      }
    }
  else
    { // modification
    val_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);
    if (val_geo->numero_repere == b_vdi_e_log_nv1)
      {
      if ((exist_ident) && (!bStrIEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      } // deja description donnees
    else
      {
      if (exist_ident)
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    }
  if (nb_ul > 4)
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (!bReconnaitConstante ((vect_ul+1), &initial_log, &initial_num,
                        &un_log, &un_num, exist_init, pos_init))
    {
    err(26);
    }
  if (!un_log)
    {
    err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
    }
	if (!bReconnaitConstanteNum((vect_ul+2), &booleen, &reel, &mot))
		{
		spec_e_log->selecteur = EL_TRANSPARENTE; 
		MajIdentifiantEltGraphique ((vect_ul+2), spec_e_log->strIdentifiantEltGr);
		}
	else // avec selecteur
		{
		spec_e_log->selecteur = (DWORD)(reel);
		MajIdentifiantEltGraphique ((vect_ul+3), spec_e_log->strIdentifiantEltGr);
		}
  return (0);
  }
//----------------------------------------------------------
static DWORD valide_e_n (PUL vect_ul, int nb_ul, DWORD ligne,
                        REQUETE_EDIT action, DWORD *position_ident,
                        DWORD *pos_init, BOOL *exist_init,
                        PVDI_E_NUM spec_e_num, BOOL bPortagePM)
  {
  BOOL exist_ident;
  BOOL initial_log;
  BOOL un_log;
  BOOL un_num;
  FLOAT initial_num;
  PGEO_VDI val_geo;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;
  
  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (action != MODIFIE_LIGNE)
    {
    if (exist_ident)
      {
      err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
      }
    }
  else
    { // modification
    val_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);
    if (val_geo->numero_repere == b_vdi_e_num)
      {
      if ((exist_ident) && (!bStrIEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      } // deja description donnees
    else
      {
      if (exist_ident)
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    }
  if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(spec_e_num->couleur), bPortagePM)))
    {
    err(43);
    }
  if ((!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot)) ||
      // JS : Nb car < 0 si notation scientifique (reel < 1) || (reel > 12))
      (reel == 0) || (reel < -12) || (reel > 12))
    {
    err(IDSERR_LE_NOMBRE_DE_CARACTERES_EST_INVALIDE);
    }
  spec_e_num->nbr_caractere = (int) (reel);
  if (!bReconnaitConstanteNum ((vect_ul+3), &booleen, &reel, &mot))
    {
    err(55);
    }
  spec_e_num->min = reel;
  if (!bReconnaitConstanteNum ((vect_ul+4), &booleen, &reel, &mot))
    {
    err(56);
    }
  spec_e_num->max = reel;
  if (!bReconnaitConstante ((vect_ul+5), &initial_log, &initial_num,
                        &un_log, &un_num,exist_init,pos_init))
    {
    err(26);
    }
  if (!un_num)
    {
    err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
    }
  if ((!bReconnaitConstanteNum ((vect_ul+6), &booleen, &reel, &mot)) ||
      ((reel != 0) && (reel != 1)))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  spec_e_num->cadre = (BOOL)(reel == 1);
  if ((!bReconnaitConstanteNum ((vect_ul+7), &booleen, &reel, &mot)) ||
      ((reel != 0) && (reel != 1)))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  spec_e_num->masque = (BOOL)(reel == 1);
  if ((!bReconnaitConstanteNum ((vect_ul+8), &booleen, &reel, &mot)) ||
      ((reel != 0) && (reel != 1)))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  spec_e_num->video_focus = (BOOL)(reel == 1);
  if ((!bReconnaitConstanteNum ((vect_ul+9), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(spec_e_num->couleur_fond), bPortagePM)))
    {
    err(43);
    }
	if (!bReconnaitConstanteNum ((vect_ul+10), &booleen, &reel, &mot))
		{
		spec_e_num->NbDecimales = -1;
		MajIdentifiantEltGraphique ((vect_ul+10), spec_e_num->strIdentifiantEltGr);
		}
	else
		{
		spec_e_num->NbDecimales = (LONG)reel;
		MajIdentifiantEltGraphique ((vect_ul+11), spec_e_num->strIdentifiantEltGr);
		}
  return (0);
  }
//----------------------------------------------------------
static DWORD valide_e_m (PUL vect_ul, int nb_ul, DWORD ligne,
                        REQUETE_EDIT action, DWORD *position_ident,
                        DWORD *pos_init, BOOL *exist_init,
                        PVDI_E_MES spec_e_mes, BOOL bPortagePM)
  {
  BOOL exist_ident;
  BOOL initial_log;
  BOOL un_log;
  BOOL un_num;
  FLOAT initial_num;
  PGEO_VDI val_geo;
  BOOL booleen;
  DWORD mot;
  FLOAT reel;

  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (action != MODIFIE_LIGNE)
    {
    if (exist_ident)
      {
      err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
      }
    }
  else
    { // modification
    val_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);
    if (val_geo->numero_repere == b_vdi_e_mes)
      {
      if ((exist_ident) && (!bStrIEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      } // deja description donnees
    else
      {
      if (exist_ident)
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    }
  if ((!bReconnaitConstanteNum ((vect_ul+1), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(spec_e_mes->couleur), bPortagePM)))
    {
    err(43);
    }
  if ((!bReconnaitConstanteNum ((vect_ul+2), &booleen, &reel, &mot)) ||
      (reel < 1) || (reel > 80))
    {
    err(IDSERR_LE_NOMBRE_DE_CARACTERES_EST_INVALIDE);
    }
  spec_e_mes->nbr_caractere = (int) (reel);
  if (!bReconnaitConstante ((vect_ul+3), &initial_log, &initial_num,
                        &un_log, &un_num,exist_init,pos_init))
    {
    err(26);
    }
  if ((un_log) || (un_num))
    {
    err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
    }
  if ((!bReconnaitConstanteNum ((vect_ul+4), &booleen, &reel, &mot)) ||
      ((reel != 0) && (reel != 1)))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  spec_e_mes->cadre = (BOOL)(reel == 1);
  if ((!bReconnaitConstanteNum ((vect_ul+5), &booleen, &reel, &mot)) ||
      ((reel != 0) && (reel != 1)))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  spec_e_mes->masque = (BOOL)(reel == 1);
  if ((!bReconnaitConstanteNum ((vect_ul+6), &booleen, &reel, &mot)) ||
      ((reel != 0) && (reel != 1)))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  spec_e_mes->video_focus = (BOOL)(reel == 1);
  if ((!bReconnaitConstanteNum ((vect_ul+7), &booleen, &reel, &mot)) ||
      (!test_couleur (FALSE, reel, &(spec_e_mes->couleur_fond), bPortagePM)))
    {
    err(43);
    }
	MajIdentifiantEltGraphique ((vect_ul+8), spec_e_mes->strIdentifiantEltGr);
  return (0);
  }

//----------------------------------------------------------
static DWORD valide_control_radio (PUL vect_ul, int nb_ul, DWORD ligne,
																	 REQUETE_EDIT action, PVDI_CONTROL_RADIO spec_control_radio)
	{
  BOOL exist_ident;
  PENTREE_SORTIE es;
	DWORD position_ident;

	if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, &position_ident))
    {
    err(IDSERR_VARIABLE_ATTENDUE_MAIS_NON_TROUVEE);
    }
  if (!exist_ident)
    {
    err(385);
    }
  spec_control_radio->position_es = dwIdVarESFromPosIdentif (position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,spec_control_radio->position_es);
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s))
    {
    err(IDSERR_UN_IDENTIFICATEUR_DE_LA_VARIABLE_DOIT_FIGURER_DANS_LA_DESCRIPTION);
    }
  if (es->genre != c_res_numerique)
    {
    err(389);
    }
	MajIdentifiantEltGraphique ((vect_ul+1), spec_control_radio->strIdentifiantEltGr);
	return (0);
	}

//----------------------------------------------------------
static DWORD valide_control_static (PUL vect_ul, int nb_ul, DWORD ligne,
																		REQUETE_EDIT action, DWORD *position_ident,
																		DWORD *pos_init, BOOL *exist_init,
																		PVDI_CONTROL_STATIC spec_control_static)
	{
	BOOL exist_ident;
  BOOL initial_log;
  BOOL un_log;
  BOOL un_num;
  FLOAT initial_num;
  PGEO_VDI val_geo;

  if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, position_ident))
    {
    err(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
  if (action != MODIFIE_LIGNE)
    {
    if (exist_ident)
      {
      err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
      }
    }
  else
    { // modification
    val_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);
    if (val_geo->numero_repere == b_vdi_control_static)
      {
      if ((exist_ident) && (!bStrIEgaleANomVarES (vect_ul[0].show, val_geo->pos_es)))
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      } // deja description donnees
    else
      {
      if (exist_ident)
        {
        err (IDSERR_CET_IDENTIFICATEUR_A_DEJA_ETE_UTILISE);
        }
      }
    }
  if (!bReconnaitConstante ((vect_ul+1), &initial_log, &initial_num,
                        &un_log, &un_num,exist_init,pos_init))
    {
    err(26);
    }
  if ((un_log) || (un_num))
    {
    err(IDSERR_LA_VARIABLE_ET_SA_VALEUR_INITIALE_NE_SONT_PAS_DE_MEME_TYPE);
    }
	MajIdentifiantEltGraphique ((vect_ul+2), spec_control_static->strIdentifiantEltGr);
  return (0);
	}

//----------------------------------------------------------
static DWORD valide_control_liste (PUL vect_ul, int nb_ul, DWORD ligne,
																		  REQUETE_EDIT action, PVDI_CONTROL_LISTE spec_control_liste)
	{
  BOOL exist_ident;
  PENTREE_SORTIE es;
	DWORD position_ident;

	if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, &position_ident))
    {
    err(388);
    }
  if (!exist_ident)
    {
    err(388);
    }
  spec_control_liste->position_es_liste = dwIdVarESFromPosIdentif (position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,spec_control_liste->position_es_liste);
  if ((es->sens != c_res_variable_ta_es) && (es->sens != c_res_variable_ta_e) &&
      (es->sens != c_res_variable_ta_s))

    {
    err(388);
    }
  if (es->genre != c_res_message)
    {
    err(388);
    }
	//
	if (StrLength (vect_ul[1].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+1), &exist_ident, &position_ident))
    {
    err(IDSERR_VARIABLE_ATTENDUE_MAIS_NON_TROUVEE);
    }
  if (!exist_ident)
    {
    err(389);
    }
  spec_control_liste->position_es_indice = dwIdVarESFromPosIdentif (position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,spec_control_liste->position_es_indice);
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s))
    {
    err(389);
    }
  if (es->genre != c_res_numerique)
    {
    err(389);
    }
	MajIdentifiantEltGraphique ((vect_ul+2), spec_control_liste->strIdentifiantEltGr);
	return (0);
	}

//----------------------------------------------------------
static DWORD valide_control_combobox (PUL vect_ul, int nb_ul, DWORD ligne,
																		  REQUETE_EDIT action, PVDI_CONTROL_COMBOBOX spec_control_combobox)
	{
  BOOL exist_ident;
  PENTREE_SORTIE es;
	DWORD position_ident;

	if (StrLength (vect_ul[0].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+0), &exist_ident, &position_ident))
    {
    err(388);
    }
  if (!exist_ident)
    {
    err(388);
    }
  spec_control_combobox->position_es_liste = dwIdVarESFromPosIdentif (position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,spec_control_combobox->position_es_liste);
  if ((es->sens != c_res_variable_ta_es) && (es->sens != c_res_variable_ta_e) &&
      (es->sens != c_res_variable_ta_s))

    {
    err(388);
    }
  if (es->genre != c_res_message)
    {
    err(388);
    }
	//
	if (StrLength (vect_ul[1].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+1), &exist_ident, &position_ident))
    {
    err(IDSERR_VARIABLE_ATTENDUE_MAIS_NON_TROUVEE);
    }
  if (!exist_ident)
    {
    err(389);
    }
  spec_control_combobox->position_es_indice = dwIdVarESFromPosIdentif (position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,spec_control_combobox->position_es_indice);
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s))
    {
    err(389);
    }
  if (es->genre != c_res_numerique)
    {
    err(389);
    }
	//
	if (StrLength (vect_ul[2].show) >= c_nb_car_es)
    {
    err (IDSERR_L_IDENTIFICATEUR_EST_TROP_LONG_20_CAR_MAX);
    }
  if (!bReconnaitESValide ((vect_ul+2), &exist_ident, &position_ident))
    {
    err(IDSERR_VARIABLE_ATTENDUE_MAIS_NON_TROUVEE);
    }
  if (!exist_ident)
    {
    err(IDSERR_VARIABLE_ATTENDUE_MAIS_NON_TROUVEE);
    }
  spec_control_combobox->position_es_saisie = dwIdVarESFromPosIdentif (position_ident);
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s,spec_control_combobox->position_es_saisie);
  if ((es->sens != c_res_e) && (es->sens != c_res_s) && (es->sens != c_res_e_et_s))
    {
    err(IDSERR_VARIABLE_ATTENDUE_MAIS_NON_TROUVEE);
    }
  if (es->genre != c_res_message)
    {
    err(IDSERR_VARIABLE_ATTENDUE_MAIS_NON_TROUVEE);
    }
	MajIdentifiantEltGraphique ((vect_ul+3), spec_control_combobox->strIdentifiantEltGr);
	return (0);
	}


//------------------------------------
// validation vecteur ul
static BOOL valide_vect_ul (PUL vect_ul, int nb_ul, DWORD ligne,
                               REQUETE_EDIT action,
                               DWORD *numero_repere, DWORD *numero_page,
                               DWORD *position_ident,
                               DWORD *pos_init, BOOL *exist_init,
															 PINTER_GR_PARAM_VALIDE_VECT_UL pValide,
															 DWORD *erreur, BOOL bPortagePM)
 
	{
  ID_MOT_RESERVE n_fonction;
  BOOL booleen;
  BOOL un_log;
  BOOL un_num;
  FLOAT reel;

  (*erreur) = 0;
  if ((nb_ul < 1) || (nb_ul > 14))
    {
    err1(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
    }
	if (!bReconnaitMotReserve((vect_ul+0)->show, &n_fonction))
		{
		if (!bReconnaitConstante ((vect_ul+0), &booleen, &reel,
													&un_log,  &un_num, exist_init, pos_init))
			{
			err1(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
			}
		if ((un_log) || (un_num))
			{
			err1(53);
			}
		if (StrFirstCharInStr (vect_ul[0].show, "\"##") == STR_NOT_FOUND)
			{
			StrCopy (vect_ul[0].show, "\""); // on force avec " toute chaine ne commen�ant pas par "# 
			}
		vect_ul[0].genre = g_id;
		bReconnaitConstante ((vect_ul+0), &booleen, &reel,
											&un_log,  &un_num, exist_init, pos_init);
		(*numero_repere) = b_pt_constant;
		}
	else
		{
		// mot reserve en premier
		//----------------
		switch (n_fonction)
			{

			case c_res_ign_r_l:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*erreur) = valide_reflet_logique ((vect_ul+2), (int)(nb_ul-2), position_ident,
																					 numero_repere, pValide->spec_r_log, pValide->spec_r_log_tab, bPortagePM);
				break;

			case c_res_ign_r_l_4:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*erreur) = valide_reflet_logique_4 ((vect_ul+2), (int)(nb_ul-2), position_ident,
																							numero_repere, pValide->spec_r_log_4etats, pValide->spec_r_log_4etats_tab, bPortagePM);
				break;

			case c_res_ign_r_m:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*erreur) = valide_reflet_message ((vect_ul+2), (int)(nb_ul-2), position_ident,
																					 numero_repere, pValide->spec_r_mes, pValide->spec_r_mes_tab, bPortagePM);
				break;

			case c_res_ign_r_an:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*erreur) = valide_reflet_alpha_num ((vect_ul+2), (int)(nb_ul-2), position_ident,
																							numero_repere, pValide->spec_r_num_alpha, pValide->spec_r_num_alpha_tab, bPortagePM);
				break;

			case c_res_ign_r_bargraph:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*erreur) = valide_bargraph ((vect_ul+2), (int)(nb_ul-2), position_ident,
																		 numero_repere, pValide->spec_r_num, pValide->spec_r_num_tab, bPortagePM);
				break;


			case c_res_ign_r_c_temps:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_courbe_temps;
				(*erreur) = valide_courbe_temps ((vect_ul+2), (int)(nb_ul - 2), ligne, action,
																					position_ident, pos_init, exist_init,
																					pValide->spec_courbe_temps, bPortagePM);
				break;

			case c_res_ign_r_c_commande:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_courbe_cmd;
				(*erreur) = valide_courbe_cmd ((vect_ul+2), (int)(nb_ul - 2), ligne, action,
																			 position_ident, pos_init, exist_init,
																			 pValide->spec_courbe_cmd, bPortagePM);
				break;

			case c_res_ign_r_c_archivage:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_courbe_fic;
				(*erreur) = valide_courbe_archive ((vect_ul+2), (int)(nb_ul - 2), ligne, action,
																						position_ident, pos_init, exist_init,
																						pValide->spec_courbe_fic);
				break;

			case c_res_ign_r_l_deplacement:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*erreur) = valide_deplacement ((vect_ul+2), (int)(nb_ul - 2), ligne, action,
																				 position_ident, pos_init, exist_init,
																				 numero_repere, pValide->spec_r_num_dep,
																				 pValide->spec_r_num_dep_tab, bPortagePM);
				break;

			case c_res_ign_e_l:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_e_log_nv1;
				(*erreur) = valide_e_l ((vect_ul+2), (int)(nb_ul-2), ligne, action,
																position_ident, pos_init, exist_init,
																pValide->spec_e_log, bPortagePM);
				break;

			case c_res_ign_e_n:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_e_num;
				(*erreur) = valide_e_n ((vect_ul+2), (int)(nb_ul-2), ligne, action,
																position_ident, pos_init, exist_init,
																pValide->spec_e_num, bPortagePM);
				break;

			case c_res_ign_e_m:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_e_mes;
				(*erreur) = valide_e_m ((vect_ul+2), (int)(nb_ul-2), ligne, action,
																position_ident, pos_init, exist_init,
																pValide->spec_e_mes, bPortagePM);
				break;

			case c_res_ign_control_radio:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_control_radio;
				(*erreur) = valide_control_radio ((vect_ul+2), (int)(nb_ul-2), ligne, action, pValide->spec_control_radio);
				break;

			case c_res_ign_control_static:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_control_static;
				(*erreur) = valide_control_static ((vect_ul+2), (int)(nb_ul-2), ligne, action,
																           position_ident, pos_init, exist_init, pValide->spec_control_static);
				break;

			case c_res_ign_control_liste:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_control_liste;
				(*erreur) = valide_control_liste ((vect_ul+2), (int)(nb_ul-2), ligne, action, pValide->spec_control_liste);
				break;

			case c_res_ign_control_combobox:
				if (!test_page ((vect_ul+1), numero_page))
					{
					err1(25);
					}
				(*numero_repere) = b_vdi_control_combobox;
				(*erreur) = valide_control_combobox ((vect_ul+2), (int)(nb_ul-2), ligne, action, pValide->spec_control_combobox);
				break;

			default:
				err1(IDSERR_LA_DESCRIPTION_EST_INVALIDE);
				break;
			} // switch (n_fonction)
    } // mot res
  return ((BOOL)((*erreur) == 0));
  } // valide_vect_ul


//''''''''''''''''''''''''''validation ligne''''''''''''''''''''''''''''''''

void  valide_ligne_vdi 
	(REQUETE_EDIT action, DWORD ligne,
	PUL vect_ul, int *nb_ul,
	BOOL *supprime_ligne, BOOL *accepte_ds_bd,
	DWORD *erreur_val, int *niveau, int *indentation)
  {
  PGEO_VDI donnees_geo;
  GEO_VDI val_geo;
  ENTREE_SORTIE es;
  VDI_R_LOG spec_r_log;
  VDI_R_LOG_TAB spec_r_log_tab;
  VDI_R_LOG_4_ETATS spec_r_log_4etats;
  VDI_R_LOG_4_ETATS_TAB spec_r_log_4etats_tab;
  VDI_R_NUM spec_r_num;
  VDI_R_NUM_TAB spec_r_num_tab;
  VDI_R_NUM_DEP spec_r_num_dep;
  VDI_R_NUM_DEP_TAB spec_r_num_dep_tab;
  VDI_R_MES spec_r_mes;
  VDI_R_MES_TAB spec_r_mes_tab;
  VDI_R_NUM_ALPHA spec_r_num_alpha;
  VDI_R_NUM_ALPHA_TAB spec_r_num_alpha_tab;
  VDI_COURBE_TEMPS spec_courbe_temps;
  VDI_COURBE_CMD spec_courbe_cmd;
  VDI_COURBE_FIC spec_courbe_fic;
  VDI_E_LOG spec_e_log;
  VDI_E_NUM spec_e_num;
  VDI_E_MES spec_e_mes;
  VDI_CONTROL_RADIO spec_control_radio;
  VDI_CONTROL_LISTE spec_control_liste;
  VDI_CONTROL_COMBOBOX spec_control_combobox;
  VDI_CONTROL_STATIC spec_control_static;
  DWORD erreur;
  PVDI_R_LOG ptr_r_log;
  PVDI_R_LOG_TAB ptr_r_log_tab;
  PVDI_R_LOG_4_ETATS ptr_r_log_4etats;
  PVDI_R_LOG_4_ETATS_TAB ptr_r_log_4etats_tab;
  PVDI_R_NUM ptr_r_num;
  PVDI_R_NUM_TAB ptr_r_num_tab;
  PVDI_R_NUM_DEP ptr_r_num_dep;
  PVDI_R_NUM_DEP_TAB ptr_r_num_dep_tab;
  PVDI_R_NUM_ALPHA ptr_r_num_alpha;
  PVDI_R_NUM_ALPHA_TAB ptr_r_num_alpha_tab;
  PVDI_R_MES ptr_r_mes;
  PVDI_R_MES_TAB ptr_r_mes_tab;
  PVDI_COURBE_TEMPS ptr_courbe_temps;
  PVDI_COURBE_CMD ptr_courbe_cmd;
  PVDI_COURBE_FIC ptr_courbe_fic;
  PVDI_E_LOG ptr_e_log;
  PVDI_E_NUM ptr_e_num;
  PVDI_E_MES ptr_e_mes;
  PVDI_CONTROL_RADIO ptr_control_radio;
  PVDI_CONTROL_LISTE ptr_control_liste;
  PVDI_CONTROL_COMBOBOX ptr_control_combobox;
	PVDI_CONTROL_STATIC ptr_control_static;
  DWORD pos_ident;
  DWORD pos_init;
  DWORD numero_repere;
  DWORD numero_page;
  BOOL exist_init;

  (*erreur_val) = 0;
  (*niveau) = 0;
  (*indentation) = 0;
  switch (action)
    {
    case MODIFIE_LIGNE:
			{
			INTER_GR_PARAM_VALIDE_VECT_UL Valide;

			Valide.spec_r_log = &spec_r_log;
			Valide.spec_r_log_tab = &spec_r_log_tab;
			Valide.spec_r_log_4etats = &spec_r_log_4etats;
			Valide.spec_r_log_4etats_tab = &spec_r_log_4etats_tab;
			Valide.spec_r_num = &spec_r_num;
			Valide.spec_r_num_tab = &spec_r_num_tab;
			Valide.spec_r_num_dep = &spec_r_num_dep;
			Valide.spec_r_num_dep_tab = &spec_r_num_dep_tab;
			Valide.spec_r_mes = &spec_r_mes;
			Valide.spec_r_mes_tab = &spec_r_mes_tab;
			Valide.spec_r_num_alpha = &spec_r_num_alpha;
			Valide.spec_r_num_alpha_tab = &spec_r_num_alpha_tab;
			Valide.spec_courbe_temps = &spec_courbe_temps;
			Valide.spec_courbe_cmd = &spec_courbe_cmd;
			Valide.spec_courbe_fic = &spec_courbe_fic;
			Valide.spec_e_log = &spec_e_log;
			Valide.spec_e_num = &spec_e_num;
			Valide.spec_e_mes = &spec_e_mes;
			Valide.spec_control_radio = &spec_control_radio;
			Valide.spec_control_liste = &spec_control_liste;
			Valide.spec_control_combobox = &spec_control_combobox;
			Valide.spec_control_static = &spec_control_static;

      if (valide_vect_ul(vect_ul,(*nb_ul), ligne, MODIFIE_LIGNE,
                        &numero_repere, &numero_page,
                        &pos_ident, &pos_init, &exist_init,
												&Valide, &erreur, bPortagePM))
        { // vect_ul Ok *
        donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);
        val_geo = (*donnees_geo);
        if ((val_geo.numero_page == numero_page) && (val_geo.numero_repere == numero_repere))
          {
          switch (numero_repere)
            {
            case b_vdi_r_log :
              ptr_r_log = (PVDI_R_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log,val_geo.pos_donnees);
              if (ptr_r_log->position_es_normal != spec_r_log.position_es_normal)
                {
                ReferenceES (spec_r_log.position_es_normal);
                DeReferenceES (ptr_r_log->position_es_normal);
                }
              (*ptr_r_log) = spec_r_log;
              break;

            case b_vdi_r_log_tab :
              ptr_r_log_tab = (PVDI_R_LOG_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_tab,val_geo.pos_donnees);
              if (ptr_r_log_tab->position_es_normal != spec_r_log_tab.position_es_normal)
                {
                ReferenceES (spec_r_log_tab.position_es_normal);
                DeReferenceES (ptr_r_log_tab->position_es_normal);
                }
              (*ptr_r_log_tab) = spec_r_log_tab;
              break;

            case b_vdi_r_log_4etats :
              ptr_r_log_4etats = (PVDI_R_LOG_4_ETATS)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats,val_geo.pos_donnees);
              if (ptr_r_log_4etats->position_es_un != spec_r_log_4etats.position_es_un)
                {
                ReferenceES (spec_r_log_4etats.position_es_un);
                DeReferenceES (ptr_r_log_4etats->position_es_un);
                }
              if (ptr_r_log_4etats->position_es_deux != spec_r_log_4etats.position_es_deux)
                {
                ReferenceES (spec_r_log_4etats.position_es_deux);
                DeReferenceES (ptr_r_log_4etats->position_es_deux);
                }
              (*ptr_r_log_4etats) = spec_r_log_4etats;
              break;

            case b_vdi_r_log_4etats_tab :
              ptr_r_log_4etats_tab = (PVDI_R_LOG_4_ETATS_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats_tab,val_geo.pos_donnees);
              if (ptr_r_log_4etats_tab->position_es_un != spec_r_log_4etats_tab.position_es_un)
                {
                ReferenceES (spec_r_log_4etats_tab.position_es_un);
                DeReferenceES (ptr_r_log_4etats_tab->position_es_un);
                }
              if (ptr_r_log_4etats_tab->position_es_deux != spec_r_log_4etats_tab.position_es_deux)
                {
                ReferenceES (spec_r_log_4etats_tab.position_es_deux);
                DeReferenceES (ptr_r_log_4etats_tab->position_es_deux);
                }
              (*ptr_r_log_4etats_tab) = spec_r_log_4etats_tab;
              break;

            case b_vdi_r_num :
              ptr_r_num = (PVDI_R_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num,val_geo.pos_donnees);
              if (ptr_r_num->position_es != spec_r_num.position_es)
                {
                ReferenceES (spec_r_num.position_es);
                DeReferenceES (ptr_r_num->position_es);
                }
              (*ptr_r_num) = spec_r_num;
              break;

            case b_vdi_r_num_tab :
              ptr_r_num_tab = (PVDI_R_NUM_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_tab,val_geo.pos_donnees);
              if (ptr_r_num_tab->position_es != spec_r_num_tab.position_es)
                {
                ReferenceES (spec_r_num_tab.position_es);
                DeReferenceES (ptr_r_num_tab->position_es);
                }
              (*ptr_r_num_tab) = spec_r_num_tab;
              break;

            case b_vdi_r_mes_nv2 :
              ptr_r_mes = (PVDI_R_MES)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
              if (ptr_r_mes->position_es != spec_r_mes.position_es)
                {
                ReferenceES (spec_r_mes.position_es);
                DeReferenceES (ptr_r_mes->position_es);
                }
              if ((ptr_r_mes->color_cste != 1) || (spec_r_mes.color_cste != 1))
                {
                if ((ptr_r_mes->color_cste != spec_r_mes.color_cste))
                  {
                  if (spec_r_mes.color_cste != 1)  // La nouvelle est variable
                    {
                    ReferenceES (spec_r_mes.color);
                    if (ptr_r_mes->color_cste != 1)  // Ancienne variable
                      {
                      DeReferenceES (ptr_r_mes->color);
                      }
                    }
                  else                             // La nouvelle est constante
                    {
                    DeReferenceES (ptr_r_mes->color);   // Ancienne variable
                    }
                  }
                }
              (*ptr_r_mes) = spec_r_mes;
              break;

            case b_vdi_r_mes_tab_nv2 :
              ptr_r_mes_tab = (PVDI_R_MES_TAB)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
              if (ptr_r_mes_tab->position_es != spec_r_mes_tab.position_es)
                {
                ReferenceES (spec_r_mes_tab.position_es);
                DeReferenceES (ptr_r_mes_tab->position_es);
                }
              if ((ptr_r_mes_tab->color_cste != 1) || (spec_r_mes_tab.color_cste != 1))
                {
                if ((ptr_r_mes_tab->color_cste != spec_r_mes_tab.color_cste))
                  {
                  if (spec_r_mes_tab.color_cste !=  1)  // La nouvelle est variable
                    {
                    ReferenceES (spec_r_mes_tab.color);
                    if (ptr_r_mes_tab->color_cste != 1)  // Ancienne variable
                      {
                      DeReferenceES (ptr_r_mes_tab->color);
                      }
                    }
                  else                             // La nouvelle est constante
                    {
                    DeReferenceES (ptr_r_mes_tab->color);   // Ancienne variable
                    }
                  }
                }
              (*ptr_r_mes_tab) = spec_r_mes_tab;
              break;

            case b_vdi_r_num_alpha_nv1 :
              ptr_r_num_alpha = (PVDI_R_NUM_ALPHA)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
              if (ptr_r_num_alpha->position_es != spec_r_num_alpha.position_es)
                {
                ReferenceES (spec_r_num_alpha.position_es);
                DeReferenceES (ptr_r_num_alpha->position_es);
                }
              if ((ptr_r_num_alpha->color_cste != 1) || (spec_r_num_alpha.color_cste != 1))
                {
                if ((ptr_r_num_alpha->color_cste != spec_r_num_alpha.color_cste))
                  {
                  if (spec_r_num_alpha.color_cste != 1)  // La nouvelle est variable
                    {
                    ReferenceES (spec_r_num_alpha.color);
                    if (ptr_r_num_alpha->color_cste != 1)  // Ancienne variable
                      {
                      DeReferenceES (ptr_r_num_alpha->color);
                      }
                    }
                  else                             // La nouvelle est constante
                    {
                    DeReferenceES (ptr_r_num_alpha->color);   // Ancienne variable
                    }
                  }
                }
              (*ptr_r_num_alpha) = spec_r_num_alpha;
              break;

            case b_vdi_r_num_alpha_tab_nv1 :
              ptr_r_num_alpha_tab = (PVDI_R_NUM_ALPHA_TAB)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
              if (ptr_r_num_alpha_tab->position_es != spec_r_num_alpha_tab.position_es)
                {
                ReferenceES (spec_r_num_alpha_tab.position_es);
                DeReferenceES (ptr_r_num_alpha_tab->position_es);
                }
              if ((ptr_r_num_alpha_tab->color_cste != 1) || (spec_r_num_alpha_tab.color_cste != 1))
                {
                if ((ptr_r_num_alpha_tab->color_cste != spec_r_num_alpha_tab.color_cste))
                  {
                  if (spec_r_num_alpha_tab.color_cste != 1)  // La nouvelle est variable
                    {
                    ReferenceES (spec_r_num_alpha_tab.color);
                    if (ptr_r_num_alpha_tab->color_cste != 1)  // Ancienne variable
                      {
                      DeReferenceES (ptr_r_num_alpha_tab->color);
                      }
                    }
                  else                             // La nouvelle est constante
                    {
                    DeReferenceES (ptr_r_num_alpha_tab->color);   // Ancienne variable
                    }
                  }
                }
              (*ptr_r_num_alpha_tab) = spec_r_num_alpha_tab;
              break;

            case b_vdi_r_num_dep :
              ptr_r_num_dep = (PVDI_R_NUM_DEP)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
              if (ptr_r_num_dep->position_es_un != spec_r_num_dep.position_es_un)
                {
                ReferenceES (spec_r_num_dep.position_es_un);
                DeReferenceES (ptr_r_num_dep->position_es_un);
                }
              if (ptr_r_num_dep->position_es_deux != spec_r_num_dep.position_es_deux)
                {
                ReferenceES (spec_r_num_dep.position_es_deux);
                DeReferenceES (ptr_r_num_dep->position_es_deux);
                }
              if ((ptr_r_num_dep->color_cste == 0) || (spec_r_num_dep.color_cste == 0))
                {
                if ((ptr_r_num_dep->color_cste != spec_r_num_dep.color_cste))
                  {
                  if (spec_r_num_dep.color_cste == 0)  // La nouvelle est variable
                    {
                    ReferenceES (spec_r_num_dep.color);
                    if (ptr_r_num_dep->color_cste == 0)  // Ancienne variable
                      {
                      DeReferenceES (ptr_r_num_dep->color);
                      }
                    }
                  else                             // La nouvelle est constante
                    {
                    DeReferenceES (ptr_r_num_dep->color);   // Ancienne variable
                    }
                  }
                }
              (*ptr_r_num_dep) = spec_r_num_dep;
              break;

            case b_vdi_r_num_dep_tab :
              ptr_r_num_dep_tab = (PVDI_R_NUM_DEP_TAB)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
              if (ptr_r_num_dep_tab->position_es_un != spec_r_num_dep_tab.position_es_un)
                {
                ReferenceES (spec_r_num_dep_tab.position_es_un);
                DeReferenceES (ptr_r_num_dep_tab->position_es_un);
                }
              if (ptr_r_num_dep_tab->position_es_deux != spec_r_num_dep_tab.position_es_deux)
                {
                ReferenceES (spec_r_num_dep_tab.position_es_deux);
                DeReferenceES (ptr_r_num_dep_tab->position_es_deux);
                }
              if ((ptr_r_num_dep_tab->color_cste == 0) || (spec_r_num_dep_tab.color_cste == 0))
                {
                if ((ptr_r_num_dep_tab->color_cste != spec_r_num_dep_tab.color_cste))
                  {
                  if (spec_r_num_dep_tab.color_cste == 0)  // La nouvelle est variable
                    {
                    ReferenceES (spec_r_num_dep_tab.color);
                    if (ptr_r_num_dep_tab->color_cste == 0)  // Ancienne variable
                      {
                      DeReferenceES (ptr_r_num_dep_tab->color);
                      }
                    }
                  else                             // La nouvelle est constante
                    {
                    DeReferenceES (ptr_r_num_dep_tab->color);   // Ancienne variable
                    }
                  }
                }
              (*ptr_r_num_dep_tab) = spec_r_num_dep_tab;
              break;
							
						case b_vdi_control_radio:
              ptr_control_radio = (PVDI_CONTROL_RADIO)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
							if (ptr_control_radio->position_es != spec_control_radio.position_es)
								{
                ReferenceES (spec_control_radio.position_es);
                DeReferenceES (ptr_control_radio->position_es);
								}
							(*ptr_control_radio) = spec_control_radio;
							break;

						case b_vdi_control_static:
              es.genre = c_res_message;
              es.sens = c_res_s;
              es.i_identif = pos_ident;
              if (bModifieDeclarationES (&es, vect_ul[2].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[3].show, &(val_geo.pos_initial));
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              ptr_control_static = (PVDI_CONTROL_STATIC)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_specif_es);
							(*ptr_control_static) = spec_control_static;
							break;

						case b_vdi_control_liste:
              ptr_control_liste = (PVDI_CONTROL_LISTE)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
							if (ptr_control_liste->position_es_liste != spec_control_liste.position_es_liste)
								{
                ReferenceES (spec_control_liste.position_es_liste);
                DeReferenceES (ptr_control_liste->position_es_liste);
								}
							if (ptr_control_liste->position_es_indice != spec_control_liste.position_es_indice)
								{
                ReferenceES (spec_control_liste.position_es_indice);
                DeReferenceES (ptr_control_liste->position_es_indice);
								}
							(*ptr_control_liste) = spec_control_liste;
							break;

						case b_vdi_control_combobox:
              ptr_control_combobox = (PVDI_CONTROL_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
							if (ptr_control_combobox->position_es_liste != spec_control_combobox.position_es_liste)
								{
                ReferenceES (spec_control_combobox.position_es_liste);
                DeReferenceES (ptr_control_combobox->position_es_liste);
								}
							if (ptr_control_combobox->position_es_indice != spec_control_combobox.position_es_indice)
								{
                ReferenceES (spec_control_combobox.position_es_indice);
                DeReferenceES (ptr_control_combobox->position_es_indice);
								}
							if (ptr_control_combobox->position_es_saisie != spec_control_combobox.position_es_saisie)
								{
                ReferenceES (spec_control_combobox.position_es_saisie);
                DeReferenceES (ptr_control_combobox->position_es_saisie);
								}
							(*ptr_control_combobox) = spec_control_combobox;
							break;

            case b_vdi_courbe_temps :
              es.genre = c_res_logique;
              es.sens = c_res_s;
              es.i_identif = pos_ident;
              if (bModifieDeclarationES (&es, vect_ul[2].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[11].show, &(val_geo.pos_initial));
                ptr_courbe_temps = (PVDI_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_temps,val_geo.pos_specif_es);
                if (ptr_courbe_temps->pos_es_a_ech != spec_courbe_temps.pos_es_a_ech)
                  {
                  ReferenceES (spec_courbe_temps.pos_es_a_ech);
                  DeReferenceES (ptr_courbe_temps->pos_es_a_ech);
                  }
                (*ptr_courbe_temps) = spec_courbe_temps;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_vdi_courbe_cmd :
              es.genre = c_res_logique;
              es.sens = c_res_s;
              es.i_identif = pos_ident;
              if (bModifieDeclarationES (&es, vect_ul[2].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[11].show, &(val_geo.pos_initial));
                ptr_courbe_cmd = (PVDI_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_cmd,val_geo.pos_specif_es);
                if (ptr_courbe_cmd->pos_es_a_ech != spec_courbe_cmd.pos_es_a_ech)
                  {
                  ReferenceES (spec_courbe_cmd.pos_es_a_ech);
                  DeReferenceES (ptr_courbe_cmd->pos_es_a_ech);
                  }
                if (ptr_courbe_cmd->pos_es_echantillonnage != spec_courbe_cmd.pos_es_echantillonnage)
                  {
                  ReferenceES (spec_courbe_cmd.pos_es_echantillonnage);
                  DeReferenceES (ptr_courbe_cmd->pos_es_echantillonnage);
                  }
                (*ptr_courbe_cmd) = spec_courbe_cmd;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_vdi_courbe_fic :
              ptr_courbe_fic = (PVDI_COURBE_FIC)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
              if (ptr_courbe_fic->pos_es_fic != spec_courbe_fic.pos_es_fic)
                {
                ReferenceES (spec_courbe_fic.pos_es_fic);
                DeReferenceES (ptr_courbe_fic->pos_es_fic);
                }
              if (ptr_courbe_fic->pos_es_couleur != spec_courbe_fic.pos_es_couleur)
                {
                ReferenceES (spec_courbe_fic.pos_es_couleur);
                DeReferenceES (ptr_courbe_fic->pos_es_couleur);
                }
              if (ptr_courbe_fic->pos_es_style != spec_courbe_fic.pos_es_style)
                {
                ReferenceES (spec_courbe_fic.pos_es_style);
                DeReferenceES (ptr_courbe_fic->pos_es_style);
                }
              if (ptr_courbe_fic->pos_es_modele != spec_courbe_fic.pos_es_modele)
                {
                ReferenceES (spec_courbe_fic.pos_es_modele);
                DeReferenceES (ptr_courbe_fic->pos_es_modele);
                }
              if (ptr_courbe_fic->pos_es_min != spec_courbe_fic.pos_es_min)
                {
                ReferenceES (spec_courbe_fic.pos_es_min);
                DeReferenceES (ptr_courbe_fic->pos_es_min);
                }
              if (ptr_courbe_fic->pos_es_max != spec_courbe_fic.pos_es_max)
                {
                ReferenceES (spec_courbe_fic.pos_es_max);
                DeReferenceES (ptr_courbe_fic->pos_es_max);
                }
              if (ptr_courbe_fic->pos_es_taille_enr != spec_courbe_fic.pos_es_taille_enr)
                {
                ReferenceES (spec_courbe_fic.pos_es_taille_enr);
                DeReferenceES (ptr_courbe_fic->pos_es_taille_enr);
                }
              if (ptr_courbe_fic->pos_es_offset_enr != spec_courbe_fic.pos_es_offset_enr)
                {
                ReferenceES (spec_courbe_fic.pos_es_offset_enr);
                DeReferenceES (ptr_courbe_fic->pos_es_offset_enr);
                }
              if (ptr_courbe_fic->pos_es_num_enr != spec_courbe_fic.pos_es_num_enr)
                {
                ReferenceES (spec_courbe_fic.pos_es_num_enr);
                DeReferenceES (ptr_courbe_fic->pos_es_num_enr);
                }
              if (ptr_courbe_fic->pos_es_nb_pts != spec_courbe_fic.pos_es_nb_pts)
                {
                ReferenceES (spec_courbe_fic.pos_es_nb_pts);
                DeReferenceES (ptr_courbe_fic->pos_es_nb_pts);
                }
              if (ptr_courbe_fic->pos_es_cmd_affichage != spec_courbe_fic.pos_es_cmd_affichage)
                {
                ReferenceES (spec_courbe_fic.pos_es_cmd_affichage);
                DeReferenceES (ptr_courbe_fic->pos_es_cmd_affichage);
                }
              (*ptr_courbe_fic) = spec_courbe_fic;
              break;

            case b_vdi_e_log_nv1:
								es.genre = c_res_logique;
								es.sens = c_res_e;
								es.i_identif = pos_ident;
                if (bModifieDeclarationES (&es, vect_ul[2].show, val_geo.pos_es))
									{
									modifie_cst_bd_c (vect_ul[3].show, &(val_geo.pos_initial));
									}
         				else
         					{
         					erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
         					}
                ptr_e_log = (PVDI_E_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_log_nv1,val_geo.pos_specif_es);
              	(*ptr_e_log) = spec_e_log;
              break;

            case b_vdi_e_num:
              es.genre = c_res_numerique;
              es.sens = c_res_e;
              es.i_identif = pos_ident;
              if (bModifieDeclarationES (&es, vect_ul[2].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[7].show, &(val_geo.pos_initial));
                ptr_e_num = (PVDI_E_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_num,val_geo.pos_specif_es);
                (*ptr_e_num) = spec_e_num;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_vdi_e_mes:
              es.genre = c_res_message;
              es.sens = c_res_e;
              es.i_identif = pos_ident;
              if (bModifieDeclarationES (&es, vect_ul[2].show, val_geo.pos_es))
                {
                modifie_cst_bd_c (vect_ul[5].show, &(val_geo.pos_initial));
                ptr_e_mes = (PVDI_E_MES)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_mes,val_geo.pos_specif_es);
                (*ptr_e_mes) = spec_e_mes;
                }
              else
                {
                erreur = IDSERR_VARIABLE_REFERENCEE_MODIFICATION_DU_GENRE_IMPOSSIBLE;
                }
              break;

            case b_pt_constant :
              modifie_cst_bd_c (vect_ul[0].show, &(val_geo.pos_donnees));
              break;

            default:
              break;
            } // switch numero repere
          }// meme repere
        else
          {
          erreur = IDSERR_LA_MODIFICATION_DU_TYPE_DE_LA_LIGNE_EST_IMPOSSIBLE;
          }
        } // vect_ul Ok
      if (erreur != 0)
        {
        genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
        (*erreur_val) = erreur;
        }// erreur vect_ul
      else
        {
        donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);
        (*donnees_geo) = val_geo;
        }
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne)=FALSE;
			}
      break;

    case INSERE_LIGNE:
			{
			INTER_GR_PARAM_VALIDE_VECT_UL Valide;

			Valide.spec_r_log = &spec_r_log;
			Valide.spec_r_log_tab = &spec_r_log_tab;
			Valide.spec_r_log_4etats = &spec_r_log_4etats;
			Valide.spec_r_log_4etats_tab = &spec_r_log_4etats_tab;
			Valide.spec_r_num = &spec_r_num;
			Valide.spec_r_num_tab = &spec_r_num_tab;
			Valide.spec_r_num_dep = &spec_r_num_dep;
			Valide.spec_r_num_dep_tab = &spec_r_num_dep_tab;
			Valide.spec_r_mes = &spec_r_mes;
			Valide.spec_r_mes_tab = &spec_r_mes_tab;
			Valide.spec_r_num_alpha = &spec_r_num_alpha;
			Valide.spec_r_num_alpha_tab = &spec_r_num_alpha_tab;
			Valide.spec_courbe_temps = &spec_courbe_temps;
			Valide.spec_courbe_cmd = &spec_courbe_cmd;
			Valide.spec_courbe_fic = &spec_courbe_fic;
			Valide.spec_e_log = &spec_e_log;
			Valide.spec_e_num = &spec_e_num;
			Valide.spec_e_mes = &spec_e_mes;
			Valide.spec_control_radio = &spec_control_radio;
			Valide.spec_control_liste = &spec_control_liste;
			Valide.spec_control_combobox = &spec_control_combobox;
			Valide.spec_control_static = &spec_control_static;

      if (valide_vect_ul(vect_ul,(*nb_ul), ligne, INSERE_LIGNE,
                        &numero_repere, &numero_page,
                        &pos_ident, &pos_init, &exist_init,
                        &Valide, &erreur, bPortagePM))
        { // vect_ul Ok
        val_geo.numero_repere = numero_repere;
        val_geo.numero_page = numero_page;
        switch ((DWORD)numero_repere)
          {
          case b_pt_constant :
            AjouteConstanteBd (pos_init, exist_init, vect_ul[0].show, &(val_geo.pos_donnees));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_e_log_nv1:
            if (!existe_repere (b_vdi_e_log_nv1))
              {
              cree_bloc (b_vdi_e_log_nv1, sizeof (spec_e_log), 0);
              }
            es.genre = c_res_logique;
            es.sens = c_res_e;
            es.i_identif = pos_ident;
            es.n_desc = d_graf;
            es.taille = 0;
            InsereVarES (&es, vect_ul[2].show, &(val_geo.pos_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[3].show, &(val_geo.pos_initial));
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_e_log_nv1) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_e_log_nv1,val_geo.pos_specif_es);
            ptr_e_log = (PVDI_E_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_log_nv1,val_geo.pos_specif_es);
            (*ptr_e_log) = spec_e_log;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;


          case b_vdi_e_num:
            if (!existe_repere (b_vdi_e_num))
              {
              cree_bloc (b_vdi_e_num,sizeof (spec_e_num),0);
              }
            es.genre = c_res_numerique;
            es.sens = c_res_e;
            es.i_identif = pos_ident;
            es.n_desc = d_graf;
            es.taille = 0;
            InsereVarES (&es, vect_ul[2].show, &(val_geo.pos_es));
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_e_num) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_e_num,val_geo.pos_specif_es);
            ptr_e_num = (PVDI_E_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_num,val_geo.pos_specif_es);
            (*ptr_e_num) = spec_e_num;
            AjouteConstanteBd (pos_init, exist_init, vect_ul[7].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_e_mes:
            if (!existe_repere (b_vdi_e_mes))
              {
              cree_bloc (b_vdi_e_mes,sizeof (spec_e_mes),0);
              }
            es.genre = c_res_message;
            es.sens = c_res_e;
            es.i_identif = pos_ident;
            es.n_desc = d_graf;
            es.taille = 0;
            InsereVarES (&es, vect_ul[2].show, &(val_geo.pos_es));
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_e_mes) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_e_mes,val_geo.pos_specif_es);
            ptr_e_mes = (PVDI_E_MES)pointe_enr (szVERIFSource, __LINE__, b_vdi_e_mes,val_geo.pos_specif_es);
            (*ptr_e_mes) = spec_e_mes;
            AjouteConstanteBd (pos_init, exist_init, vect_ul[5].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

					case b_vdi_control_static:
            es.genre = c_res_message;
            es.sens = c_res_s;
            es.i_identif = pos_ident;
            es.taille = 0;
            InsereVarES (&es, vect_ul[2].show, &(val_geo.pos_es));
            AjouteConstanteBd (pos_init, exist_init, vect_ul[3].show, &(val_geo.pos_initial));
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_control_static),0);
              }
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_specif_es);
            ptr_control_static = (PVDI_CONTROL_STATIC)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_specif_es);
            (*ptr_control_static) = spec_control_static;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
						break;


          case b_vdi_r_log:
            if (!existe_repere (b_vdi_r_log))
              {
              cree_bloc (b_vdi_r_log,sizeof (spec_r_log),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_r_log) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_r_log,val_geo.pos_donnees);
            ptr_r_log = (PVDI_R_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log,val_geo.pos_donnees);
            (*ptr_r_log) = spec_r_log;
            ReferenceES (spec_r_log.position_es_normal);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_log_tab:
            if (!existe_repere (b_vdi_r_log_tab))
              {
              cree_bloc (b_vdi_r_log_tab,sizeof (spec_r_log_tab),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_r_log_tab) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_r_log_tab,val_geo.pos_donnees);
            ptr_r_log_tab = (PVDI_R_LOG_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_tab,val_geo.pos_donnees);
            (*ptr_r_log_tab) = spec_r_log_tab;
            ReferenceES (spec_r_log_tab.position_es_normal);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_log_4etats:
            if (!existe_repere (b_vdi_r_log_4etats))
              {
              cree_bloc (b_vdi_r_log_4etats,sizeof (spec_r_log_4etats),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_r_log_4etats) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_r_log_4etats,val_geo.pos_donnees);
            ptr_r_log_4etats = (PVDI_R_LOG_4_ETATS)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats,val_geo.pos_donnees);
            (*ptr_r_log_4etats) = spec_r_log_4etats;
            ReferenceES (spec_r_log_4etats.position_es_un);
            ReferenceES (spec_r_log_4etats.position_es_deux);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_log_4etats_tab:
            if (!existe_repere (b_vdi_r_log_4etats_tab))
              {
              cree_bloc (b_vdi_r_log_4etats_tab,sizeof (spec_r_log_4etats_tab),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_r_log_4etats_tab) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_r_log_4etats_tab,val_geo.pos_donnees);
            ptr_r_log_4etats_tab = (PVDI_R_LOG_4_ETATS_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats_tab,val_geo.pos_donnees);
            (*ptr_r_log_4etats_tab) = spec_r_log_4etats_tab;
            ReferenceES (spec_r_log_4etats_tab.position_es_un);
            ReferenceES (spec_r_log_4etats_tab.position_es_deux);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_num:
            if (!existe_repere (b_vdi_r_num))
              {
              cree_bloc (b_vdi_r_num,sizeof (spec_r_num),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_r_num) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_r_num,val_geo.pos_donnees);
            ptr_r_num = (PVDI_R_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num,val_geo.pos_donnees);
            (*ptr_r_num) = spec_r_num;
            ReferenceES (spec_r_num.position_es);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_num_tab:
            if (!existe_repere (b_vdi_r_num_tab))
              {
              cree_bloc (b_vdi_r_num_tab,sizeof (spec_r_num_tab),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_r_num_tab) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_r_num_tab,val_geo.pos_donnees);
            ptr_r_num_tab = (PVDI_R_NUM_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_tab,val_geo.pos_donnees);
            (*ptr_r_num_tab) = spec_r_num_tab;
            ReferenceES (spec_r_num_tab.position_es);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_mes_nv2 :
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_r_mes),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_r_mes = (PVDI_R_MES)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_r_mes) = spec_r_mes;
            ReferenceES (spec_r_mes.position_es);
            if ((spec_r_mes.color_cste != 1))
              {
              ReferenceES (spec_r_mes.color);
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_mes_tab_nv2 :
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_r_mes_tab),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_r_mes_tab = (PVDI_R_MES_TAB)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_r_mes_tab) = spec_r_mes_tab;
            ReferenceES (spec_r_mes_tab.position_es);
            if ((spec_r_mes_tab.color_cste != 1))
              {
              ReferenceES (spec_r_mes_tab.color);
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_num_alpha_nv1:
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_r_num_alpha),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_r_num_alpha = (PVDI_R_NUM_ALPHA)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_r_num_alpha) = spec_r_num_alpha;
            ReferenceES (spec_r_num_alpha.position_es);
            if ((spec_r_num_alpha.color_cste != 1))
              {
              ReferenceES (spec_r_num_alpha.color);
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_num_alpha_tab_nv1:
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_r_num_alpha_tab),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_r_num_alpha_tab = (PVDI_R_NUM_ALPHA_TAB)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_r_num_alpha_tab) = spec_r_num_alpha_tab;
            ReferenceES (spec_r_num_alpha_tab.position_es);
            if ((spec_r_num_alpha_tab.color_cste !=1))
              {
              ReferenceES (spec_r_num_alpha_tab.color);
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_num_dep :
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_r_num_dep),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_r_num_dep = (PVDI_R_NUM_DEP)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_r_num_dep) = spec_r_num_dep;
            ReferenceES (spec_r_num_dep.position_es_un);
            ReferenceES (spec_r_num_dep.position_es_deux);
            if ((spec_r_num_dep.color_cste == 0))
              {
              ReferenceES (spec_r_num_dep.color);
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_r_num_dep_tab :
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_r_num_dep_tab),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_r_num_dep_tab = (PVDI_R_NUM_DEP_TAB)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_r_num_dep_tab) = spec_r_num_dep_tab;
            ReferenceES (spec_r_num_dep_tab.position_es_un);
            ReferenceES (spec_r_num_dep_tab.position_es_deux);
            if ((spec_r_num_dep_tab.color_cste == 0))
              {
              ReferenceES (spec_r_num_dep_tab.color);
              }
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_control_radio:
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_control_radio),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_control_radio= (PVDI_CONTROL_RADIO)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_control_radio) = spec_control_radio;
            ReferenceES (spec_control_radio.position_es);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_control_liste:
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_control_liste),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_control_liste= (PVDI_CONTROL_LISTE)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_control_liste) = spec_control_liste;
            ReferenceES (spec_control_liste.position_es_liste);
            ReferenceES (spec_control_liste.position_es_indice);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_control_combobox:
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_control_combobox),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_control_combobox= (PVDI_CONTROL_COMBOBOX)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_control_combobox) = spec_control_combobox;
            ReferenceES (spec_control_combobox.position_es_liste);
            ReferenceES (spec_control_combobox.position_es_indice);
            ReferenceES (spec_control_combobox.position_es_saisie);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_courbe_fic:
            if (!existe_repere (numero_repere))
              {
              cree_bloc (numero_repere,sizeof (spec_courbe_fic),0);
              }
            val_geo.pos_donnees = nb_enregistrements (szVERIFSource, __LINE__, numero_repere) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,numero_repere,val_geo.pos_donnees);
            ptr_courbe_fic= (PVDI_COURBE_FIC)pointe_enr (szVERIFSource, __LINE__, numero_repere,val_geo.pos_donnees);
            (*ptr_courbe_fic) = spec_courbe_fic;
            ReferenceES (spec_courbe_fic.pos_es_fic);
            ReferenceES (spec_courbe_fic.pos_es_couleur);
            ReferenceES (spec_courbe_fic.pos_es_style);
            ReferenceES (spec_courbe_fic.pos_es_modele);
            ReferenceES (spec_courbe_fic.pos_es_min);
            ReferenceES (spec_courbe_fic.pos_es_max);
            ReferenceES (spec_courbe_fic.pos_es_taille_enr);
            ReferenceES (spec_courbe_fic.pos_es_offset_enr);
            ReferenceES (spec_courbe_fic.pos_es_num_enr);
            ReferenceES (spec_courbe_fic.pos_es_nb_pts);
            ReferenceES (spec_courbe_fic.pos_es_cmd_affichage);
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_courbe_temps:
            if (!existe_repere (b_vdi_courbe_temps))
              {
              cree_bloc (b_vdi_courbe_temps,sizeof (spec_courbe_temps),0);
              }
            es.genre = c_res_logique;
            es.sens = c_res_s;
            es.i_identif = pos_ident;
            es.n_desc = d_graf;
            es.taille = 0;
            InsereVarES (&es, vect_ul[2].show, &(val_geo.pos_es));
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_courbe_temps) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_courbe_temps,val_geo.pos_specif_es);
            ptr_courbe_temps = (PVDI_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_temps,val_geo.pos_specif_es);
            ReferenceES (spec_courbe_temps.pos_es_a_ech);
            (*ptr_courbe_temps) = spec_courbe_temps;
            AjouteConstanteBd (pos_init, exist_init, vect_ul[11].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          case b_vdi_courbe_cmd:
            if (!existe_repere (b_vdi_courbe_cmd))
              {
              cree_bloc (b_vdi_courbe_cmd,sizeof (spec_courbe_cmd),0);
              }
            es.genre = c_res_logique;
            es.sens = c_res_s;
            es.i_identif = pos_ident;
            es.n_desc = d_graf;
            es.taille = 0;
            InsereVarES (&es, vect_ul[2].show, &(val_geo.pos_es));
            val_geo.pos_specif_es = nb_enregistrements (szVERIFSource, __LINE__, b_vdi_courbe_cmd) + 1;
            insere_enr (szVERIFSource, __LINE__, 1,b_vdi_courbe_cmd,val_geo.pos_specif_es);
            ptr_courbe_cmd = (PVDI_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, b_vdi_courbe_cmd,val_geo.pos_specif_es);
            ReferenceES (spec_courbe_cmd.pos_es_a_ech);
            ReferenceES (spec_courbe_cmd.pos_es_echantillonnage);
            (*ptr_courbe_cmd) = spec_courbe_cmd;
            AjouteConstanteBd (pos_init, exist_init, vect_ul[11].show, &(val_geo.pos_initial));
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne)= FALSE;
            break;

          default:
            break;

          } //switch
        if (numero_repere != 0)
          {
          if (!existe_repere (b_geo_vdi))
            {
            cree_bloc (b_geo_vdi,sizeof (val_geo),0);
            }
          insere_enr (szVERIFSource, __LINE__, 1,b_geo_vdi,ligne);
          donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);

          (*donnees_geo) = val_geo;
          }
        }// vect_ul Ok
      else
        { // erreur vect_ul
        (*accepte_ds_bd) = FALSE;
        (*erreur_val) = erreur;
        }
			}
      break;

    case SUPPRIME_LIGNE :
			{
      (*accepte_ds_bd) = TRUE;
      (*supprime_ligne) = TRUE;
      donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi,ligne);
      val_geo = (*donnees_geo);
      switch (val_geo.numero_repere)
        {
        case b_pt_constant : // commentaire
          supprime_cst_bd_c (val_geo.pos_donnees);
          break;

        case b_vdi_e_log_nv1 :
            if (!bSupprimeES (val_geo.pos_es))
              {
              (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
              (*accepte_ds_bd) = TRUE;
              (*supprime_ligne) = FALSE;
              }
            else
              {
      	      supprime_cst_bd_c (val_geo.pos_initial);
              enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_specif_es);
      	      maj_pointeur_spec_dans_geo (val_geo.numero_repere,val_geo.pos_specif_es);
              }
          break;

        case b_vdi_e_num:
        case b_vdi_e_mes :
          if (!bSupprimeES (val_geo.pos_es))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.pos_initial);
            enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_specif_es);
            maj_pointeur_spec_dans_geo (val_geo.numero_repere,val_geo.pos_specif_es);
            }
          break;


        case b_vdi_control_static :
          if (!bSupprimeES (val_geo.pos_es))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.pos_initial);
            }
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_specif_es);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,val_geo.pos_specif_es);
          break;

        case b_vdi_r_log:
          ptr_r_log = (PVDI_R_LOG)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log,val_geo.pos_donnees);
          DeReferenceES (ptr_r_log->position_es_normal);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                     val_geo.pos_donnees);
          break;

        case b_vdi_r_log_tab:
          ptr_r_log_tab = (PVDI_R_LOG_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_tab,val_geo.pos_donnees);
          DeReferenceES (ptr_r_log_tab->position_es_normal);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

        case b_vdi_r_log_4etats:
          ptr_r_log_4etats = (PVDI_R_LOG_4_ETATS)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats,val_geo.pos_donnees);
          DeReferenceES (ptr_r_log_4etats->position_es_un);
          DeReferenceES (ptr_r_log_4etats->position_es_deux);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                     val_geo.pos_donnees);
          break;

        case b_vdi_r_log_4etats_tab:
          ptr_r_log_4etats_tab = (PVDI_R_LOG_4_ETATS_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_log_4etats_tab,val_geo.pos_donnees);
          DeReferenceES (ptr_r_log_4etats_tab->position_es_un);
          DeReferenceES (ptr_r_log_4etats_tab->position_es_deux);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                     val_geo.pos_donnees);
          break;

        case b_vdi_r_num:
          ptr_r_num = (PVDI_R_NUM)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num,val_geo.pos_donnees);
          DeReferenceES (ptr_r_num->position_es);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

        case b_vdi_r_num_tab:
          ptr_r_num_tab = (PVDI_R_NUM_TAB)pointe_enr (szVERIFSource, __LINE__, b_vdi_r_num_tab,val_geo.pos_donnees);
          DeReferenceES (ptr_r_num_tab->position_es);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

        case b_vdi_r_mes_nv2 :
          ptr_r_mes = (PVDI_R_MES)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_r_mes->position_es);
          if ((ptr_r_mes->color_cste != 1))
            {
            DeReferenceES (ptr_r_mes->color);
            }
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

        case b_vdi_r_mes_tab_nv2 :
          ptr_r_mes_tab = (PVDI_R_MES_TAB)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_r_mes_tab->position_es);
          if ((ptr_r_mes_tab->color_cste != 1))
            {
            DeReferenceES (ptr_r_mes_tab->color);
            }
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

        case b_vdi_r_num_alpha_nv1:
          ptr_r_num_alpha = (PVDI_R_NUM_ALPHA)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_r_num_alpha->position_es);
          if ((ptr_r_num_alpha->color_cste != 1))
            {
            DeReferenceES (ptr_r_num_alpha->color);
            }
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

        case b_vdi_r_num_alpha_tab_nv1:
          ptr_r_num_alpha_tab = (PVDI_R_NUM_ALPHA_TAB)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_r_num_alpha_tab->position_es);
          if ((ptr_r_num_alpha_tab->color_cste !=1))
            {
            DeReferenceES (ptr_r_num_alpha_tab->color);
            }
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

        case b_vdi_r_num_dep:
          ptr_r_num_dep = (PVDI_R_NUM_DEP)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_r_num_dep->position_es_un);
          DeReferenceES (ptr_r_num_dep->position_es_deux);
          if ((ptr_r_num_dep->color_cste == 0))
            {
            DeReferenceES (ptr_r_num_dep->color);
            }
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

        case b_vdi_r_num_dep_tab:
          ptr_r_num_dep_tab = (PVDI_R_NUM_DEP_TAB)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_r_num_dep_tab->position_es_un);
          DeReferenceES (ptr_r_num_dep_tab->position_es_deux);
          if ((ptr_r_num_dep_tab->color_cste == 0))
            {
            DeReferenceES (ptr_r_num_dep_tab->color);
            }
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;

				case b_vdi_control_radio:
          ptr_control_radio = (PVDI_CONTROL_RADIO) pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_control_radio->position_es);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,val_geo.pos_donnees);
					break;

				case b_vdi_control_liste:
          ptr_control_liste = (PVDI_CONTROL_LISTE) pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_control_liste->position_es_liste);
          DeReferenceES (ptr_control_liste->position_es_indice);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,val_geo.pos_donnees);
					break;

				case b_vdi_control_combobox:
          ptr_control_combobox = (PVDI_CONTROL_COMBOBOX) pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_control_combobox->position_es_liste);
          DeReferenceES (ptr_control_combobox->position_es_indice);
          DeReferenceES (ptr_control_combobox->position_es_saisie);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,val_geo.pos_donnees);
					break;

        case b_vdi_courbe_temps:
          if (!bSupprimeES (val_geo.pos_es))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.pos_initial);
            ptr_courbe_temps = (PVDI_COURBE_TEMPS)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_specif_es);
            DeReferenceES (ptr_courbe_temps->pos_es_a_ech);
            enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_specif_es);
            maj_pointeur_spec_dans_geo (val_geo.numero_repere,val_geo.pos_specif_es);
            }
          break;

        case b_vdi_courbe_cmd:
          if (!bSupprimeES (val_geo.pos_es))
            {
            (*erreur_val) = IDSERR_LA_VARIABLE_ETANT_REFERENCEE_SA_SUPPRESSION_EST_IMPOSSIBLE;
            (*accepte_ds_bd) = TRUE;
            (*supprime_ligne) = FALSE;
            }
          else
            {
            supprime_cst_bd_c (val_geo.pos_initial);
            ptr_courbe_cmd = (PVDI_COURBE_CMD)pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_specif_es);
            DeReferenceES (ptr_courbe_cmd->pos_es_a_ech);
            DeReferenceES (ptr_courbe_cmd->pos_es_echantillonnage);
            enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_specif_es);
            maj_pointeur_spec_dans_geo (val_geo.numero_repere,val_geo.pos_specif_es);
            }
          break;

        case b_vdi_courbe_fic:
          ptr_courbe_fic = (PVDI_COURBE_FIC) pointe_enr (szVERIFSource, __LINE__, val_geo.numero_repere,val_geo.pos_donnees);
          DeReferenceES (ptr_courbe_fic->pos_es_fic);
          DeReferenceES (ptr_courbe_fic->pos_es_couleur);
          DeReferenceES (ptr_courbe_fic->pos_es_style);
          DeReferenceES (ptr_courbe_fic->pos_es_modele);
          DeReferenceES (ptr_courbe_fic->pos_es_min);
          DeReferenceES (ptr_courbe_fic->pos_es_max);
          DeReferenceES (ptr_courbe_fic->pos_es_taille_enr);
          DeReferenceES (ptr_courbe_fic->pos_es_offset_enr);
          DeReferenceES (ptr_courbe_fic->pos_es_num_enr);
          DeReferenceES (ptr_courbe_fic->pos_es_nb_pts);
          DeReferenceES (ptr_courbe_fic->pos_es_cmd_affichage);
          enleve_enr (szVERIFSource, __LINE__, 1,val_geo.numero_repere,val_geo.pos_donnees);
          maj_pointeur_spec_dans_geo (val_geo.numero_repere,
                                      val_geo.pos_donnees);
          break;


        default:
          break;;

        } //switch (numero_repere
      if (supprime_ligne)
        {
        enleve_enr (szVERIFSource, __LINE__, 1,b_geo_vdi,ligne);
        }
			}
      break;


    case CONSULTE_LIGNE :
      genere_vect_ul (ligne, vect_ul, nb_ul, niveau, indentation);
      break;

    default:
      break;
    } // switch (
  } // valide_ligne

// ---------------------------------------------------------------------------
void  creer_vdi (void)
  {
  vdi_deja_initialise = FALSE;
  }

// ---------------------------------------------------------------------------
void  init_vdi (DWORD *depart, DWORD *courant, char *nom_applic)
  {

  if (!vdi_deja_initialise)
    {
    vdi_deja_initialise = TRUE;
    ligne_depart_vdi = 1;
    ligne_courante_vdi = ligne_depart_vdi;
    }
  (*depart) = ligne_depart_vdi;
  (*courant) = ligne_courante_vdi;
  }

// ---------------------------------------------------------------------------
DWORD  lis_nombre_de_ligne_vdi (void)
  {
  DWORD wretour;

  if (existe_repere (b_geo_vdi))
    {
    wretour = nb_enregistrements (szVERIFSource, __LINE__, b_geo_vdi);
    }
  else
    {
    wretour = 0;
    }
  return (wretour);
  }
// ---------------------------------------------------------------------------

void  set_tdb_vdi (DWORD ligne, BOOL nouvelle_ligne, char *tdb)
  {
  char chaine_mot [10];

  tdb[0] = '\0';
  chaine_mot [0] = '\0';
  }

// ---------------------------------------------------------------------------
void  fin_vdi (BOOL quitte, DWORD courant)
  {
  if (quitte)
    {
    }
  else
    {
    ligne_courante_vdi = courant;
    }
  }

// ---------------------------------------------------------------------------
void satellite_vdi (BOOL *pbExecutionSynchrone, char *nom, char *param, char *env, char *chemin)
  {
  *pbExecutionSynchrone = TRUE;
	StrCopy (nom, Appli.szPathExe);
  StrConcat (nom, "pcsgr");
  StrCopy (param, "/C");
	StrSetNull (env);
  StrSetNull (chemin);
  }

//----------------------------------------------------------
BOOL suppression_possible_vdi (DWORD n_ligne)
  {
  BOOL bSuppression;
  PGEO_VDI donnees_geo;
  GEO_VDI val_geo;

  donnees_geo = (PGEO_VDI)pointe_enr (szVERIFSource, __LINE__, b_geo_vdi, n_ligne);
  val_geo = (*donnees_geo);
  switch (val_geo.numero_repere)
    {
    case b_pt_constant : // commentaire
    case b_vdi_r_log:
    case b_vdi_r_log_tab:
    case b_vdi_r_log_4etats:
    case b_vdi_r_log_4etats_tab:
    case b_vdi_r_num:
    case b_vdi_r_num_tab:
    case b_vdi_r_mes_nv2 :
    case b_vdi_r_mes_tab_nv2 :
    case b_vdi_r_num_alpha_nv1:
    case b_vdi_r_num_alpha_tab_nv1:
    case b_vdi_r_num_dep:
    case b_vdi_r_num_dep_tab:
    case b_vdi_courbe_fic:
		case b_vdi_control_radio:
		case b_vdi_control_liste:
		case b_vdi_control_combobox:
      bSuppression = TRUE;
      break;

    case b_vdi_e_log_nv1 :
      bSuppression = (BOOL)(!es_referencee (val_geo.pos_es));
      break;

    case b_vdi_e_num:
    case b_vdi_e_mes :
    case b_vdi_courbe_temps:
    case b_vdi_courbe_cmd:
		case b_vdi_control_static:
      bSuppression = (BOOL)(!es_referencee (val_geo.pos_es));
      break;

    default:
      bSuppression = TRUE;
      break;;

    } //switch (numero_repere
  return (bSuppression);
  }

// Interface du descripteur d'animation
void LisDescripteurVDI (PDESCRIPTEUR_CF pDescripteurCf)
	{
	pDescripteurCf->pfnCree = creer_vdi;
	pDescripteurCf->pfnInitDesc = init_vdi;
	pDescripteurCf->pfnLisNbLignes = lis_nombre_de_ligne_vdi;
	pDescripteurCf->pfnValideLigne = valide_ligne_vdi;
	pDescripteurCf->pfnSetTdb = set_tdb_vdi;
	pDescripteurCf->pfnFin = fin_vdi;
	pDescripteurCf->pfnSatellite = satellite_vdi;
	}
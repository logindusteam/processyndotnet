//-----------------------------------------------------------
// WCouleur.h
// Gestion des fen�tres de choix ou d'affichage des couleurs
//-----------------------------------------------------------
#ifndef WCOULEUR_H
#define WCOULEUR_H

// Messages de commande couleur pour les fen�tres de couleur (Num couleur en wParam)
#define CM_SET_COULEUR_DESSIN					(WM_USER+100)
#define CM_SET_COULEUR_FOND						(WM_USER+101)
#define CM_CHANGE_MODE								(WM_USER+102)
#define CM_AFFICHE_COULEUR						(WM_USER+103)

// Cr�ation des classes des fen�tres de gestion de couleurs de PcsGr
BOOL bCreeClasseCouleur (void);

// Cr�ation d'une fen�tre de choix de couleur (contenant sa barre d'outil)
HWND hwndCreeChoixCouleurs (HWND hwndParent, BOOL bVisible, BOOL bChild);

#endif







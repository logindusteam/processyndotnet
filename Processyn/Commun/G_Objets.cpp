#include "stdafx.h"

#include "G_Objets.h"

// --------------------------------------------------------------------------
// Table des Attributs des actions (par action croissante)
// --------------------------------------------------------------------------
// defines de pr�sentation
#define _OK	G_ATTRIBUT_ACTION
#define _CO	G_ATTRIBUT_COULEUR
#define _SL	G_ATTRIBUT_STYLE_LIGNE
#define _SR	G_ATTRIBUT_STYLE_REMPLISSAGE

#define _1P	G_ATTRIBUT_1_POINT	// attributs exclusifs
#define _2P	G_ATTRIBUT_2_POINTS
#define _NP	G_ATTRIBUT_N_POINTS

#define _TX	G_ATTRIBUT_TEXTE
#define _ST	G_ATTRIBUT_STYLE_TEXTE
#define _RO	G_ATTRIBUT_SUPPORTE_ROTATION
#define _SY	G_ATTRIBUT_SUPPORTE_SYMETRIE
#define _CT	G_ATTRIBUT_CONTROLE
#define _DE	G_ATTRIBUT_DESSIN

static const G_ATTRIBUT anAttributs [G_NB_ACTIONS] =
	{
//Acti2ptsCoulStLiStReTextStTxRotaSymeDess
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_LIGNE
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_RECTANGLE
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_RECTANGLE_PLEIN
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_CERCLE
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_CERCLE_PLEIN
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_QUART_ARC
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_QUART_CERCLE_PLEIN
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_H_DEMI_CERCLE
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_H_DEMI_CERCLE_PLEIN
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_V_DEMI_CERCLE
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_V_DEMI_CERCLE_PLEIN
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_TRI_RECT
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_TRI_RECT_PLEIN
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_H_TRI_ISO
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_H_TRI_ISO_PLEIN
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_V_TRI_ISO
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_V_TRI_ISO_PLEIN
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_H_VANNE				
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_H_VANNE_PLEIN	
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_V_VANNE				
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_V_VANNE_PLEIN	
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_QUART_CERCLE		
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_QUART_ARC_PLEIN
  (G_ATTRIBUT)(_OK|_1P|_CO|  0|  0|_TX|_ST|  0|  0|_DE|0),// G_ACTION_H_TEXTE				
  (G_ATTRIBUT)(_OK|_1P|_CO|  0|  0|_TX|_ST|  0|  0|_DE|0),// G_ACTION_V_TEXTE				
  (G_ATTRIBUT)(_OK|_NP|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_POLYGONE				
  (G_ATTRIBUT)(_OK|_NP|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_POLYGONE_PLEIN	
  (G_ATTRIBUT)(_OK|  0|_CO|_SL|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_LISTE					
  (G_ATTRIBUT)(_OK|  0|_CO|_SL|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_LISTE_PLEIN		
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_COURBE_T				
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_COURBE_C				
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_COURBE_A				
  (G_ATTRIBUT)(_OK|_2P|_CO|  0|_SR|  0|  0|_RO|_SY|_DE|0),// G_ACTION_BARGRAPHE			
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|_SR|_TX|  0|_RO|_SY|_DE|0),// G_ACTION_EDIT_TEXTE			
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|_SR|_TX|  0|_RO|_SY|_DE|0),// G_ACTION_EDIT_NUM				
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|_TX|  0|_RO|_SY|_DE|0),// G_ACTION_STATIC_TEXTE		
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|_TX|  0|_RO|_SY|_DE|0),// G_ACTION_STATIC_NUM			
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|  0|  0|  0|_RO|_SY|_DE|0),// G_ACTION_ENTREE_LOG			
  (G_ATTRIBUT)(_OK|_2P|_CO|_SL|_SR|_TX|  0|  0|  0|  0|0),// G_ACTION_NOP						
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|  0|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_STATIC
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|  0|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_EDIT
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|_TX|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_GROUP
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|_TX|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_BOUTON
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|_TX|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_CHECK
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|_TX|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_RADIO
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|  0|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_COMBO 
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|  0|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_LIST
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|  0|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_SCROLL_H
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|  0|  0|_RO|_SY|_CT|0),// G_ACTION_CONT_SCROLL_V
  (G_ATTRIBUT)(_OK|_2P|  0|  0|  0|_TX|  0|_RO|_SY|_CT|0) // G_ACTION_CONT_BOUTON_VAL
	};

// Attributs graphiques
// R�cup�re les attributs d'une action
G_ATTRIBUT attributsActionGr (G_ACTION nAction)
	{
	return (nAction < G_NB_ACTIONS) ? anAttributs [nAction] : G_ATTRIBUT_INVALIDE;
	}

// Renvoie TRUE si au moins un des attributs appartient � l'action
BOOL bAttributsActionGr (G_ACTION nAction, G_ATTRIBUT nMasqueAttributs)
	{
	return (nAction < G_NB_ACTIONS) ? ((nMasqueAttributs & anAttributs [nAction]) != 0) : 0;
	}


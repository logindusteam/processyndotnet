// DocMan.h : gestion des documents d'une application
// Aujourd'hui 1 seul document est g�r� � la fois.

// -------------------------- Constantes
//

// initialise le chemim+nom sans ext du document avec une chaine vide
void InitPathNameDoc (void);

// -------------------------------------------------------------------------------
// initialise les data du document avec le premier nom d'application par d�faut non trouv� 
void InitPathNameDocVierge (PCSTR pszExtension);

// initialise le chemim+nom sans ext du document � partir de pszPathNameDoc
// pszPathNameDoc doit �tre sans ext
BOOL bSetPathNameDoc (PCSTR pszPathNameDoc);

// maj du booleen indicateur de nouvelle applic
void SetNouveauDoc (BOOL bNouveau);

// Copie dans pszDest le chemin+nom du document + extension sp�cifi�e
// renvoie pszDest ou NULL si Err
PSTR pszCopiePathNameDocExt (PSTR pszDest, int nTailleBuf, PCSTR pszExtension);

// renvoie l'adresse du chemin+nom sans ext du document
PSTR pszPathNameDoc (void);

// renvoie le r�pertoire du document
PSTR pszPathDoc (void);

// renvoie l'adresse du nom sans ext du document
PSTR pszNameDoc (void);

// renvoie le booleen indicateur de nouvelle applic
BOOL bGetNouveauDoc (void);

// modifie si possible un pathname pour le rendre relatif au path du document
PSTR pszPathNameRelatifAuDoc (PSTR pszPathNameAModifier);

// modifie si possible un pathname relatif au path du document pour le rendre absolu
PSTR pszPathNameAbsoluAuDoc (PSTR pszPathNameAModifier);

// copie et modifie si possible un pathname relatif au path du document pour le rendre absolu
void CopiePathNameAbsoluAuDoc (PSTR pszPathNameDestAbsolu, PCSTR pszPathNameSource);


/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |									    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 |									    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |   Titre   : CVT_UL.H   						    |
 |   Auteur  : AC							    |
 |   Date    : 31/12/93 						    |
 +--------------------------------------------------------------------------*/

#define CAR_INVALIDE '?'
#define CAR_COMMENTAIRE '"'
#define CAR_DEBUT_MESSAGE '"'

// Assemble le vecteur UL en une chaine de caract�res
void v_ul_to_texte (int nb_ul, PUL v_ul, PSTR pszTexteDest, DWORD dwTailleTexteDest);

// D�compose le Texte Source en une s�rie d'ULs dont le genre est d�termin�
void texte_to_v_ul (int * nb_ul, PUL v_ul, PCSTR pszTexteSource);

// Met � "" erreur = 0 Les nb_ul premiers ULs sp�cifi�s
void init_vect_ul (PUL vect_ul, DWORD nb_ul);

// ------------------------------------------------------------------------
// Ajoute des guillemets aux extr�mit�s et double les guillemets internes
// renvoie FALSE si impossible
BOOL bAjouteGuillemetsMessage (PSTR pszConstante, DWORD dwTailleDest, PCSTR pszSource);

// ------------------------------------------------------------------------
// Enl�ve les guillemets aux extr�mit�s et d�double les guillemets internes
// renvoie FALSE si impossible (guillemets internes non doubl�s)
void EnleveGuillemetsMessage (PSTR pszDestination);

//---------------------------------------------------------------------------
void erreur_encode_i (PUL ul, int i);
void erreur_encode_i4 (PUL ul, LONG i4);
void erreur_encode_bw (PUL ul, BOOL b);
void erreur_encode_dw (PUL ul, DWORD dw);
void erreur_encode_r (PUL ul, FLOAT reel);
void erreur_encode_sz (PUL ul, PCSTR pszSource);
void erreur_encode_sz_avec_guillemets (PUL ul, PCSTR pszSource);

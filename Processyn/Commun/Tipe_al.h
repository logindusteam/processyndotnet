/*---------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :                                           		|
 |	       								|
 |   Auteur  : LM							|
 |   Date    : 26/07/93 						|
 +----------------------------------------------------------------------*/

#define TAILLE_MAX_MESSAGE_AL 82
#define TAILLE_MAX_MES_AL_COMPLET 255
#define CAR_CR  '\015'           //((char)13) en OCTAL! pour AC
#define CAR_LF  '\012'           //((char)10)
#define CAR_FIN '\000'           //((char)00)

/* -------------- Variables de Bx_geo_al (pointeurs) ------------ */

#define  AL_SURV_GLOB          1 //       bool : surveillance globale
#define  AL_VISU_GLOB          2 //       bool : visualisation globale
#define  AL_ARCH_GLOB          3 //       bool : archivage global
#define  AL_IMPR_GLOB          4 //       bool : impression globale
#define  AL_CPTR_GLOB          5 //       num  : cpteur apparition al. total
#define  AL_SURV_GRP           6 //  20   bool : surveillance groupe
#define  AL_VISU_GRP          26 //  20   bool : visualisation groupe
#define  AL_ARCH_GRP          46 //  20   bool : archivage groupe
#define  AL_IMPR_GRP          66 //  20   bool : impression groupe
#define  AL_CPTR_GRP          86 //  20   num  : cpteur apparition al. par groupe
#define  AL_SURV_PRI         106 //  20   num  : surveillance priorite
#define  AL_VISU_PRI         126 //  20   num  : visualisation priorite
#define  AL_ARCH_PRI         146 //  20   num  : archivage priorite
#define  AL_IMPR_PRI         166 //  20   num  : impression priorite
#define  AL_VISU_ACTIVE      186 //  20   log  : visu al active uniquement
#define  AL_PRIORITE_UNIQUE  206 //       log  : (pour priorites en "sous groupes")
#define  AL_STATUT           207 //       num  : statut alarmes
#define  AL_VISIBLE          208 //       log  : fenetre visible
#define  AL_TOUCHES_FCT      209 //  20   log  : touches de fcts F1-->F20
#define  AL_TYPE_VISU        229 //       num  : type de fenetre
#define  AL_NB_VAR_GEO_AL    229 //              nb var bx_geo_al

#define  AL_NB_VAR_GLOB_AL     4 //              nb var bx_glob_al

#define  NB_GROUPE_MAX_AL     20 //              nbre de groupes max
#define  NB_PRIORITE_MAX_AL   10 //              nbre de priorites max

/* -------------------- mnemos reperes bloc --------------------------- */

#define  b_geo_al                     789
#define  b_titre_paragraf_al          790
#define  b_spec_titre_paragraf_al     791
#define  b_alarme                     792
#define  b_pt_libre                   793
//#define  bx_ala_global 	      794
//#define  bx_groupe		      795
//#define  bx_priorite		      796
//#define  bx_alarme_log              797
//#define  bx_alarme_num              798
//#define  bg_geo_predefini	      799
//#define  bx_geo_predefini	      800
//#define  buf_evt		      801
//#define  buf_al		      802
//#define  buf_al_visu		      803
//#define  bx_val_init		      804
//#define  bx_consultation	      805


//********************* NOUVEAUX BLOCS *********************
//
#define  bx_geo_al                794
#define  bx_glob_al               795
#define  bx_services_speciaux_al  796
#define  bx_groupe_al             797
#define  bx_priorite_al           798
#define  bx_element_al            799
#define  bx_variable_al           800
#define  bx_buffer_horodate_al    899
#define  bx_anm_geo_al            801
#define  bx_anm_element_al        802
#define  bx_anm_variable_al       803
#define  bx_anm_cour_log_al       804
#define  bx_anm_cour_num_al       805

#define  bx_anm_cour_mes_al        85
#define  bx_anm_groupe_al          87
#define  buffer_anm_surv_al        88
#define  buffer_anm_visu_al        89
#define  bx_anm_glob_al            90
#define  bx_anm_consultation_al   102
#define  bx_anm_tempo_al          113


// ----------------- Pointeurs sur bx_services_speciaux_al
//
#define  PTR_SRV_SPEC_AL_ACK   1
#define  PTR_SRV_SPEC_AL_FIN   2
#define  PTR_SRV_SPEC_AL_RAZ   3

#define  NBR_SRV_SPEC_AL       3


// ----------------- Genres des variables d'alarmes
//
#define  LOGIQUE_AL   0
#define  NUMERIQUE_AL 1
#define  MESSAGE_AL   2


// ----------------- Types des variables d'alarmes
//
#define  CONSTANTE_AL 0
#define  VARIABLE_AL  1

// ----------------- Types d'acquittement
//
#define  ACQUIT_USER 0
#define  ACQUIT_AUTO 1
#define  ACQUIT_NONE 2

// ----------------- Types du test des alarmes
//
#define  TEST_AL_ETAT   0
#define  TEST_AL_FRONT  1

// ----------------- Types d'horodatage
//
#define  HORODATAGE_PC   0
#define  HORODATAGE_SOURCE  1

// ----------------- Nombre de valeurs dans le cas HORODATAGE_SOURCE
//                   Jour Mois Annee Heure Minute Seconde MilliSeconde

#define NB_VAL_HORODATAGE 7

// ----------------- Nombre de valeurs pour une alarme (va + test)

#define NB_VAL_NUM_AL 3
#define NB_VAL_LOG_AL 1
#define NB_VAL_MES_AL 1

// ----------------- Nombre de valeurs total pour une alarme (va + test)

#define NB_VAL_TOTAL_AL (NB_VAL_NUM_AL + NB_VAL_HORODATAGE)

// ----------------- Type de donn�es pour variables d'horodatage
//                   Cette valeur doit �tre differente de b_cour_log, _num, _mes

#define VA_HORODATAGE_AL 500

/***************************** Types ***************************************/

// ------------ Types venant de la Description -------------------------
//

typedef struct
  {
  DWORD numero_priorite;
  char nom_groupe[14];
  DWORD type_horodatage;
  } t_contexte_al;

typedef union
  {
  union 
		{
		struct
			{
			BOOL val_init_surv;
			int fen_al_x;
			int fen_al_y;
			int fen_al_cx;
			int fen_al_cy;
			DWORD type_horodatage;
			LONG tempo_stabilite_evenement;
			};
		struct 
			{
			BOOL val_init_vis;
			DWORD    nbre_al_maxi;
			};
		struct 
			{
			BOOL val_init_arch;
			DWORD    taille_fic_max;
			char    nom_fichier_alarme[42];
			};
		struct 
			{
			BOOL val_init_imp;
			};
		struct 
			{
			char nom_groupe[14];
			DWORD coul_al_pres_non_ack;
			DWORD coul_al_pres_ack;
			DWORD coul_al_non_pres_non_ack;
			DWORD type_ack;
			};
		DWORD   numero_priorite;
		};
  } SPEC_TITRE_PARAGRAPHE_ALARME, *PSPEC_TITRE_PARAGRAPHE_ALARME;

typedef struct
  {
  DWORD numero_repere;
  union 
		{
    struct 
			{
			DWORD pos_paragraf;
			DWORD pos_specif_paragraf;
			};
    DWORD pos_donnees;
    };
  } GEO_ALARME, *PGEO_ALARME;

typedef struct
    {
    BOOL var_logique;           // si vrai : logique
    BOOL une_variable1;
    BOOL une_variable2;
    DWORD    pos_var1;
    DWORD    pos_var2;
    DWORD    pos_var;
    DWORD    indice_tableau;        // si variable indicee
    ID_MOT_RESERVE    test_alarme;           // numero de mot reserve
    DWORD    numero_enregistrement; // ds le fichier de messages
    FLOAT    cste1_alarme;
    FLOAT    cste2_alarme;
    DWORD    pos_message;
    } C_ALARME, *PC_ALARME;

// ------------------- Types communs � l'animateur et � Pcsexe -----------------
typedef struct
  {
  DWORD Service;
  DWORD Element;
  } HANDLE_ANM_AL;

// ---------------------- Types pour PcsExe -----------------------------
//

typedef struct
  {
  HANDLE_ANM_AL handle_al;
  } tx_services_speciaux_al;

typedef struct
  {
  DWORD   wBlocGenre;
  DWORD   wPosEs;
  BOOL		bForcage;
  HANDLE_ANM_AL handle_al;
  } tx_geo_al;

typedef struct
  {
  BOOL bValInit;
  } tx_glob_al;

typedef struct
  {
  DWORD wPosPremPriorite;
  DWORD wNbrePriorite;
  } tx_groupe_al;

typedef struct
  {
  DWORD  wNumeroPriorite;
  DWORD  wPosPremAlarme;
  DWORD  wNbreAlarme;
  } tx_priorite_al;

typedef struct
  {
  DWORD  wPosPremVariable;
  DWORD  wNbreVariable;
  HANDLE_ANM_AL handle_al;
  } tx_element_al;

typedef struct
  {
  DWORD   wBlocGenre;
  DWORD   wPosEs;
  BOOL		bTestStable;
  } tx_variable_al;


typedef struct
  {
  DWORD			wNumeroElement;
  DWORD			wAnmAction;
  MINUTERIE lValTempo;
  BOOL			rTabValLogique;      // si != 1 => [NB_VAL_LOG_AL];
  FLOAT			rTabValNumerique [NB_VAL_NUM_AL];
  char			pszTabValMessage [82];  // si != 1 => [NB_VAL_MES_AL];
  FLOAT			rTabValHorodatage [NB_VAL_HORODATAGE];
  } tx_buffer_horodate_al;


// ----------------------- Types pour l'animateur ----------------------
//

typedef union
  {
  BOOL	bValLogique;
  FLOAT rValNumerique;
  char  pszValMessage [82];
  } t_valeur_al;

typedef struct
  {
  DWORD        wCodeRetour;
  t_valeur_al  Valeur;
  } t_anm_al_notification;

typedef struct
  {
  DWORD        wTypeValeur;
  t_valeur_al Valeur;
  DWORD       wPosRet;
  } tx_anm_geo_al;

typedef struct              //Attention bloc servant aussi au scrut_al
  {
  char    pszNomFic[42];
  DWORD    wNbreAlMax;
  DWORD    wTailleFicMax;
  DWORD    hdlArchTxt;
  DWORD    hdlArchRec;
  DWORD    hdlMessage;
  BOOL bImprimanteOuverte;
  int     iFenAlX;
  int     iFenAlY;
  int     iFenAlCx;
  int     iFenAlCy;
  BOOL bScroll;
  DWORD    wTypeHoroDatage;
  LONG     lTempoStabiliteEvt;
  } tx_anm_glob_al;

typedef struct
  {
  DWORD  Jour;
  DWORD  Mois;
  DWORD  Annee;
  DWORD  Heures;
  DWORD  Minutes;
  DWORD  Secondes;
  DWORD  JourDeSemaine;
  DWORD  MilliSecondes;
  } tx_anm_horodate;

typedef struct
  {
  char pszNomGroupe[14];
  DWORD coul_al_pres_non_ack;
  DWORD coul_al_pres_ack;
  DWORD coul_al_non_pres_non_ack;
  DWORD fcoul_al_pres_non_ack;
  DWORD fcoul_al_pres_ack;
  DWORD fcoul_al_non_pres_non_ack;
  DWORD type_ack;
  } tx_anm_groupe_al;

typedef struct
  {
  DWORD    wNumeroGroupe;
  DWORD    wNumeroPriorite;
  DWORD    wTestAlarme;           // Num�ro mot r�serv� : change, stable, =, >, ...
  DWORD    wTypeTestAlarme;       // Etat ou Front
  DWORD   wNumeroEnregistrement; // dans le fichier ascii
  DWORD   wPosPremVariable;
  DWORD   wNbreVariable;
  BOOL		bEtatAcq;
  BOOL		bEtatPresent;
  DWORD   wPosTempo;
  } tx_anm_element_al;

typedef struct
  {
  DWORD wBlocGenre;
  DWORD wPosEs;
  DWORD wTypeVar; // cste ou variable
  } tx_anm_variable_al;

typedef struct
  {
  BOOL bValeur;
  } tx_anm_cour_log_al;

typedef struct
  {
  FLOAT rValeur;
  } tx_anm_cour_num_al;

typedef struct
  {
  char pszValeur[82];
  } tx_anm_cour_mes_al;

typedef struct
  {
  DWORD        wTypeValeur;
  t_valeur_al Valeur;
  } t_valeur_evt;

typedef struct
  {
  DWORD wPosEsElement;
  char pszTextAl[TAILLE_MAX_MES_AL_COMPLET+1];
  } t_buffer_anm_surv_al;

typedef struct
  {
  DWORD wPosEsElement;
  DWORD wIndexClrFore;
  DWORD wIndexClrBack;
	// $$ ac a revoir : tant qu'a faire des tests dans le paint autant generaliser
	BOOL	bAlPresente;
	BOOL	bAlAcquitee;
  } t_buffer_anm_visu_al;

typedef struct
  {
  LONG  iDate;
  LONG  iHeure;
  char szNomGroupe[12];
  BYTE wPriorite; // $$32 bits 
  BYTE wTypeEvt;
  } X_ANIM_ALARMES_ID_ALARME_ARCHIVE, *PX_ANIM_ALARMES_ID_ALARME_ARCHIVE;

typedef struct
  {
  DWORD wPosEnreg;
  } X_ANIM_ALARMES_CONSULTATION_ARCHIVAGE, *PX_ANIM_ALARMES_CONSULTATION_ARCHIVAGE;

typedef struct
  {
  MINUTERIE lwTempo;
  BOOL  bTempoEnCour;
  DWORD wPosEsElement;
  } tx_anm_tempo_al;

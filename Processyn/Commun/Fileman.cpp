/*--------------------------------------------------------------------------+
 | Ce fichier est la propriet� de                                           |
 |                    Societ� LOGIQUE INDUSTRIE                             |
 |              Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3               |
 | Il demeure sa propriet� exclusive, et est confidentiel.                  |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/
//	FileMan.c
//	Gestion de fichiers par J.B. le 11/05/94                                              |
//	Historique : 
//	11/04/96	eric	Passage � Win32
//  2/4/97		JS		passage vers multi thread
#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "MemMan.h"
#include "FMan.h"
#include "PathMan.h"
#include "Verif.h"

#include "FileMan.h"
VerifInit;

#define SIGNATURE_FICHIER 'FILE'

// Objet fichier
typedef struct
  {
	DWORD			dwSignature;					// Doit valoir SIGNATURE_FICHIER
  CFman::HF				Handle;								// Handle du fichier
  DWORD     dwTailleEnr;					// Taille d'un enregistrement
  DWORD     dwNEnrFin;						// Index dernier enregistrement + 1
	DWORD			dwNEnrLecture;				// Num�ro du prochain enr lu
	DWORD			dwNEnrEcriture;				// Num�ro du prochain enr �crit
  CHAR      szFullPath [MAX_PATH];// Nom du fichier
  } FICHIER, * PFICHIER;

#define TAILLE_BUFFER_COPIE_DEFAUT (4096 * 8)

//---------------------------------------------------------------------------
// converion du handle en pointeur sur l'objet
static __inline PFICHIER	pFromhFIC (HFIC hfic)
	{
	PFICHIER	pFic = (PFICHIER) hfic;

	if (!pFic || (pFic->dwSignature != SIGNATURE_FICHIER))
		pFic = NULL;

	return pFic;
	}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//													Fonctions export�es
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
UINT uFileOuvre (PHFIC phFile, PCSTR pszFile, DWORD dwTailleEnr, DWORD wFlags)
  {
  UINT uErreur = NO_ERROR;
  PFICHIER pFile = NULL;

	// non => On le cr�e
  if (dwTailleEnr > 0)
    {
		//DWORD    wOpenFlags;
		DWORD    wFileLength = 0;

		// allocation de l'objet m�moire
    pFile = (PFICHIER) pMemAlloue (sizeof (FICHIER));
    if (pFile)
      {
      StrCopy (pFile->szFullPath, pszFile);

      // Ouverture du fichier
      if (CFman::bFOuvre (&pFile->Handle, pFile->szFullPath, wFlags))
        {
				if (CFman::bFLongueur (pFile->Handle, &wFileLength))
          {
          if ((wFileLength % dwTailleEnr) == 0)
            {
            pFile->dwSignature = SIGNATURE_FICHIER;
            pFile->dwTailleEnr = dwTailleEnr;
            pFile->dwNEnrFin     = wFileLength / dwTailleEnr;
            pFile->dwNEnrLecture = 0;
            pFile->dwNEnrEcriture = 0;

            *phFile = (HFIC) pFile;
            }
          else // longueur fichier non multiple de la taille d'un enregistrement
            {
            uErreur = ERROR_INVALID_DATATYPE;
            CFman::bFFerme (&pFile->Handle);
            MemLibere ((PVOID *)(&pFile));
            }
          }
        else // erreur lecture taille fichier
          {
					uErreur = GetLastError();
          CFman::bFFerme (&pFile->Handle);
          MemLibere ((PVOID *)(&pFile));
          }
        }
      else // erreur ouverture fichier
        {
				uErreur = GetLastError();
        MemLibere ((PVOID *)(&pFile));
        }
      }
    else // erreur allocation m�moire pour handle
      {
      uErreur = ERROR_NOT_ENOUGH_MEMORY;
      }
    } // if (dwTailleEnr > 0)
  else 
    {
		// erreur dwTailleEnr invalide
    uErreur = ERROR_INVALID_PARAMETER;
    }

  return uErreur;
  } // uFileOuvre

//---------------------------------------------------------------------------
// Fichier texte : ouverture
UINT uFileTexteOuvre (PHFIC phFile, PCSTR pszFile, DWORD wFlags)
	{
	return uFileOuvre (phFile, pszFile, 1, wFlags);
	}

//---------------------------------------------------------------------------
// Fermeture d'un fichier
UINT uFileFerme (PHFIC phFile)
  {
  UINT			uErreur = NO_ERROR;
  PFICHIER  pFile = pFromhFIC (*phFile);

	if (pFile)
		{
		if (CFman::bFFerme (&pFile->Handle))
			{
			pFile->dwSignature = 0;
			MemLibere ((PVOID *)(&pFile));
			*phFile = NULL;
			}
		else
			uErreur = GetLastError();
		}
	else
		uErreur = ERROR_INVALID_HANDLE;

  return uErreur;
  }

//----------------------------------------------
// Renvoie le nom associ� � un handle ouvert
PCSTR pszFileNom (HFIC hFile)
	{
	PCSTR	pszRes = NULL;
  PFICHIER pFile = pFromhFIC (hFile);

	if (pFile)
		pszRes = pFile->szFullPath;

	return pszRes;
	}

//---------------------------------------------------------------------------
// Met � jour le Nb d'enr courant du fichier et renvoie une err syst�me
UINT uFileNbEnr (HFIC hFile, DWORD * pdwNbEnr)
  {
  PFICHIER	pFile = pFromhFIC (hFile);
	UINT			uRes = NO_ERROR;

 	if (pFile)
	 (*pdwNbEnr) = pFile->dwNEnrFin;
	else
		uRes = ERROR_INVALID_HANDLE;

  return uRes;
  }

//---------------------------------------------------------------------------
// Recopie le Num�ro du prochain enr lu et renvoie une err syst�me
UINT uFilePointeurLecture (HFIC hFile, DWORD * pwIdx)
  {
	UINT			uRes = NO_ERROR;
  PFICHIER	pFile = pFromhFIC (hFile);

 	if (pFile)
		(*pwIdx) = pFile->dwNEnrLecture;
	else
		uRes = ERROR_INVALID_HANDLE;

  return uRes;
  }

//---------------------------------------------------------------------------
// Enregistre le num�ro du prochain enr � lire et renvoie une err syst�me
UINT uFilePointeLecture (HFIC hFile, DWORD wIdx)
  {
  UINT			uRes = NO_ERROR;
  PFICHIER	pFile = pFromhFIC (hFile);

	if (pFile)
		{
		if (wIdx < pFile->dwNEnrFin) // $$(wIdx <= pFile->dwNEnrFin)
			pFile->dwNEnrLecture = wIdx;
		else
			uRes = ERROR_HANDLE_EOF;
		}
	else
		uRes = ERROR_INVALID_HANDLE;

  return uRes;
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Renvoie le num�ro du prochain Enr �crit
UINT uFilePointeurEcriture (HFIC hFile, DWORD * pwIdx)
  {
  UINT			uRes = NO_ERROR;
  PFICHIER  pFile = pFromhFIC (hFile);

	if (pFile)
	  (*pwIdx) = pFile->dwNEnrEcriture;
	else
		uRes = ERROR_INVALID_HANDLE;

  return uRes;
  }

//---------------------------------------------------------------------------
// Enregistre le num�ro du prochain enr � �crire
UINT uFilePointeEcriture (HFIC hFile, DWORD wIdx)
  {
  UINT			uRes = NO_ERROR;
  PFICHIER  pFile = pFromhFIC (hFile);

	if (pFile)
		{
		if (wIdx <= pFile->dwNEnrFin)
			pFile->dwNEnrEcriture = wIdx;
		else
			uRes = ERROR_HANDLE_EOF;
		}
	else
		uRes = ERROR_INVALID_HANDLE;

  return uRes;
  }

//---------------------------------------------------------------------------
// Lecture de wNbEnrALire Enregistrements
UINT uFileLire (HFIC hFile, PVOID pBuffer, DWORD wNbEnrALire, DWORD * pdwNbEnrLus)
  {
  UINT			uRes = NO_ERROR;
  PFICHIER  pFile = pFromhFIC (hFile);

	if (pFile)
		{
		DWORD			dwTailleALire = wNbEnrALire * pFile->dwTailleEnr;
  
		*pdwNbEnrLus = 0;
		if (dwTailleALire > 0)
			{
			// Acc�s � l'enr de lecture courant ?
			if (CFman::bFPointe (pFile->Handle, pFile->dwNEnrLecture * pFile->dwTailleEnr))
				{
				DWORD	dwTailleLue;

				// oui => lecture et d�compte des donn�es lues
				if (!CFman::bFLire (pFile->Handle, pBuffer, dwTailleALire, &dwTailleLue))
					uRes = GetLastError();

				// d�compte de ce qui a �t� lu
				wNbEnrALire = dwTailleLue / pFile->dwTailleEnr;
				pFile->dwNEnrLecture += wNbEnrALire;
				*pdwNbEnrLus += wNbEnrALire;
				}
			else
				uRes = GetLastError();
			}
		}
	else
		uRes = ERROR_INVALID_HANDLE;

  return uRes;
  }

//-----------------------------------------------------------------------
UINT uFileLireTout (HFIC hFile, PVOID pBuffer, DWORD dwNbEnrALire)
  {
  PFICHIER  pFile = pFromhFIC (hFile);
  UINT	uRes = ERROR_INVALID_HANDLE;
	
	if (pFile)
		{
		DWORD	wRecRead = 0;
		uRes = uFileLire (hFile, pBuffer, dwNbEnrALire, &wRecRead);

		if (uRes == NO_ERROR)
			{
			if (wRecRead != dwNbEnrALire)
				uRes = ERROR_HANDLE_EOF;
			}
		}

  return uRes;
  }

//-----------------------------------------------------------------------
// Lecture acc�s direct d'1 Enr
UINT uFileLireDirect	(HFIC hFile, DWORD dwNEnr, PVOID pBuffer)
	{
	// Pointe
	UINT	uRes = uFilePointeLecture (hFile, dwNEnr);

	// Ok ?
	if (uRes == NO_ERROR)
		// oui => lit
		uFileLireTout (hFile, pBuffer, 1);

	return uRes;
	}

//---------------------------------------------------------------------------
UINT uFileLireLn (HFIC hFile, PSTR pszDest, DWORD wLnSize)
  {
  UINT			uRes = NO_ERROR;
  PFICHIER  pFile = pFromhFIC (hFile);

	if (pFile)
		{
		// Acc�s � l'enr de lecture courant ?
		if (CFman::bFPointe (pFile->Handle, pFile->dwNEnrLecture))
			{
			DWORD dwNbOctetsLus = 0;
			if (!CFman::bFLireLn (pFile->Handle, pszDest, wLnSize, &dwNbOctetsLus))
				uRes = GetLastError();
			pFile->dwNEnrLecture += dwNbOctetsLus;
			}
		else
			uRes = GetLastError();
		}
	else
		uRes = ERROR_INVALID_HANDLE;

  return uRes;
  }

//---------------------------------------------------------------------------
UINT uFileEcrire (HFIC hFile, const void * pBuffer, DWORD dwNbEnrAEcrire, DWORD * pdwNbEnrEcrits)
  {
  UINT			uRes = NO_ERROR;
  PFICHIER  pFile = pFromhFIC (hFile);

	*pdwNbEnrEcrits = 0;
 	if (pFile)
		{
		if (dwNbEnrAEcrire > 0)
			{
			DWORD    wSize = dwNbEnrAEcrire * pFile->dwTailleEnr;
			DWORD    wSizeWritten = 0;

			if (CFman::bFPointe (pFile->Handle, pFile->dwNEnrEcriture * pFile->dwTailleEnr) &&
				CFman::bFEcrire (pFile->Handle, pBuffer, wSize, &wSizeWritten))
				{
				*pdwNbEnrEcrits = wSizeWritten / pFile->dwTailleEnr; //$$
				pFile->dwNEnrEcriture += (*pdwNbEnrEcrits);

				// Mise � jour �ventuelle de la taille de fin de fichier
				if (pFile->dwNEnrFin < pFile->dwNEnrEcriture)
					pFile->dwNEnrFin = pFile->dwNEnrEcriture;
				}
			else
				uRes = GetLastError();
			}
		}
	else
		uRes = ERROR_INVALID_HANDLE;
	return uRes;
  }

//---------------------------------------------------------------------------
// Lecture de wNbEnrALire Enregistrements avec Err si �criture non compl�te
UINT uFileEcrireTout (HFIC hFile,  const void * pBuffer, DWORD dwNbEnrAEcrire)
	{
  PFICHIER  pFile = pFromhFIC (hFile);
  UINT	uRes = ERROR_INVALID_HANDLE;
	
	if (pFile)
		{
		DWORD	dwNbEnrEcrits = 0;
		uRes = uFileEcrire (hFile, pBuffer, dwNbEnrAEcrire, &dwNbEnrEcrits);

		if (uRes == NO_ERROR)
			{
			if (dwNbEnrAEcrire != dwNbEnrEcrits)
				uRes = ERROR_HANDLE_DISK_FULL;
			}
		}

  return uRes;
	}

//---------------------------------------------------------------------------
// Ecriture acc�s direct d'1 Enr
UINT uFileEcrireDirect (HFIC hFile, DWORD dwNEnr, const void * pBuffer)
	{
	// Pointe
	UINT	uRes = uFilePointeEcriture (hFile, dwNEnr);

	// Ok ?
	if (uRes == NO_ERROR)
		// oui => lit
		uFileEcrireTout (hFile, pBuffer, 1);

	return uRes;
	}

//---------------------------------------------------------------------------
// Ecriture s�quentielle d'une chaine de caract�re puis d'un Cr Lf
UINT uFileEcrireSzLn (HFIC hFile, PCSTR pszBufferLn)
  {
  UINT			uRes = NO_ERROR;
  PFICHIER  pFile = pFromhFIC (hFile);

 	if (pFile)
		{
		if (!CFman::bFEcrireSzLn (pFile->Handle, pszBufferLn))
			uRes = GetLastError();
		}
	else
		uRes = ERROR_INVALID_HANDLE;

  return uRes;
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
BOOL bFileExiste (PCSTR pszFile)
  {
	return CFman::bFAcces (pszFile, CFman::modeRead|CFman::shareDenyNone|CFman::modeNoTruncate);
  //return (_access (pszFile, 4) == 0); // szFullPath, 4) == 0);
  }

//---------------------------------------------------------------------------
UINT uFileRenomme (PCSTR pszFile, PCSTR pszFileDest)
  {
  UINT uRes = NO_ERROR;

	if (!MoveFile(pszFile, pszFileDest))
		uRes = GetLastError();

  return uRes;
  }

//---------------------------------------------------------------------------
UINT uFileEfface (PCSTR pszFile)
  {
  UINT uRes = NO_ERROR;

	if (!DeleteFile(pszFile))
		uRes = GetLastError();

  return uRes;
  }

//---------------------------------------------------------------------------
// Recopie d'un fichier �ventuellement au bout d'un autre
UINT uFileCopie (PCSTR pszFileSource, PCSTR pszFileDest, BOOL bAjoutFinDest)
  {
  UINT	uRes = NO_ERROR;
	CFman::HF		hSrc = INVALID_HF;

	// Ouverture du fichier source Ok ?
  if (CFman::bFOuvre (&hSrc, pszFileSource, CFman::modeRead|CFman::shareDenyWrite|CFman::modeNoTruncate))
    {
		// oui => allocation du buffer de recopie
		DWORD	wOpenFlagsDst;
		CFman::HF		hDst = INVALID_HF;
		PVOID	pBuff = pMemAlloue (TAILLE_BUFFER_COPIE_DEFAUT);

		// choix du travail � effectuer sur le fichier dest
    if (bAjoutFinDest)
      wOpenFlagsDst = CFman::modeWrite|CFman::shareExclusive|CFman::modeCreate|CFman::modeNoTruncate; //O_BINARY | O_CREAT | O_WRONLY;
    else
      wOpenFlagsDst = CFman::modeWrite|CFman::shareExclusive|CFman::modeCreate; //O_BINARY | O_CREAT | O_WRONLY | O_TRUNC;

		// Ouverture ou cr�ation du fichier dest Ok ?
    if (CFman::bFOuvre (&hDst, pszFileDest, wOpenFlagsDst))
      {
			DWORD	wSizeCopy = 0;
			DWORD	wSizeCopied;

			// pointe en fin du fichier dest ?
      if (CFman::bFPointeFin (hDst))
        {
				// oui => boucle de transfert par buffer des donn�es
        do
          {
          if (CFman::bFLire (hSrc, pBuff, TAILLE_BUFFER_COPIE_DEFAUT, &wSizeCopy))
            {
            if (wSizeCopy > 0)
              {
              if (!CFman::bFEcrire (hDst, pBuff, wSizeCopy, &wSizeCopied))
								uRes = GetLastError();
              }
            } // if (bFLire (hSrc, 
					else
						uRes = GetLastError();
          } while ((uRes == NO_ERROR) && (wSizeCopy > 0));
        } // if (bFPointeFin (hDst))
			else
				uRes = GetLastError();

      if (!CFman::bFFerme (&hDst) && (uRes == NO_ERROR))
				uRes = GetLastError();
      } // if (bFOuvre (&hDst, 
		else
			uRes = GetLastError();
    if (!CFman::bFFerme (&hSrc) &&(uRes == NO_ERROR))
			uRes = GetLastError();

		// lib�ration du buffer de recopie
	  MemLibere ((PVOID *)(&pBuff));

    } // if (bFOuvre (&hSrc, ...
	else
		uRes = GetLastError();
  
  return uRes;
  } // uFileCopie

//-----------------------------------------------------------------------
//---------------------------------------------------------------------------
UINT FileGetEnvVarLength (PCSTR pszEnvVar)
  {
  PSTR pszEnv = getenv (pszEnvVar);
  UINT wEnvLength = 0;

  if (pszEnv)
    wEnvLength = StrLength (pszEnv);

  return wEnvLength;
  }

//---------------------------------------------------------------------------
void FileGetEnvVarValue (PCSTR pszEnvVar, PSTR pszEnvValue)
  {
  PSTR pszEnv = getenv (pszEnvVar);

  if (pszEnv)
    {
    StrCopy (pszEnvValue, pszEnv);
    }
  else
    {
    StrSetNull (pszEnvValue);
    }
  }


//---------------------- fin fileman.c ---------------------------------

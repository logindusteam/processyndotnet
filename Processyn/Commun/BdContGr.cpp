//-------------------------------------------------------------------------
// BdContGr.c (li� � Bdgr)
// Gestion des �l�ments graphiques controles de la base de donn�e Processyn
//-------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"        // std types
#include "MemMan.h"

#include "Appli.h"      // hinst
#include "mem.h"        // mem
#include "UStr.h"     //
#include "G_Objets.h"
#include "g_sys.h"      // g_xxx()
#include "pcsspace.h"   // Coordonn�es Min et MAX de l'espace
#include "dicho.h"      // Tri dicho pour espacement

#include "lng_res.h"
#include "Tipe.h"			// types de proc�dures du descripteur
#include "Descripteur.h"

#include "bdgr.h"			// PBDGR
#include "bdanmgr.h"   // verbes maj inter_vi
#include "Pcsfont.h"
#include "UChrono.h"
#include "Tipe_gr.h"			// types de proc�dures du descripteur
#include "BdPageGr.h"
#include "BdPageGr.h"

#include "BdContGr.h"
#include "Verif.h"
VerifInit;

static PCSTR pszClasseSTATIC			= "STATIC";
static PCSTR pszClasseEDIT				= "EDIT";
static PCSTR pszClasseBUTTON			= "BUTTON";
static PCSTR pszClasseCOMBOBOX		= "COMBOBOX";
static PCSTR pszClasseLISTBOX		= "LISTBOX";
static PCSTR pszClasseSCROLLBAR	= "SCROLLBAR";

// $$ mettre en dynamique
typedef struct
	{
	HWND		hwnd;
	WNDPROC pwndprocInitiale;
	} ENV_CONTROLE, *PENV_CONTROLE;

#define NB_ENV_CONTROLE_MAX 2000

static int	nNbEnvControles = 0;
static ENV_CONTROLE aEnvControles [NB_ENV_CONTROLE_MAX];

// ---------------------------------------------------------------
// Recherche un EnvControle pour le hwnd sp�cifi�
static PENV_CONTROLE pTrouveEnvControle (HWND hwnd)
	{
	int		NEnvControle;
	PENV_CONTROLE pEnvControle = NULL;

	// Cherche un EnvControle libre
	for (NEnvControle = 0; NEnvControle < nNbEnvControles; NEnvControle++)
		{
		if (aEnvControles [NEnvControle].hwnd == hwnd)
			{
			pEnvControle = &aEnvControles [NEnvControle];
			break;
			}
		}
	return pEnvControle;
	}

// ---------------------------------------------------------------
// Ajoute un EnvControle global
static BOOL bAjouteEnvControle (const ENV_CONTROLE * pEnvControleNouveau)
	{
	// Recherche un EnvControle libre
	PENV_CONTROLE pEnvControle = pTrouveEnvControle (NULL);

	// aucun trouv� => ajoute le si possible
	if (!pEnvControle && (nNbEnvControles < NB_ENV_CONTROLE_MAX - 1))
		{
		pEnvControle = &aEnvControles [nNbEnvControles];
		nNbEnvControles ++;
		}

	// un emplacement a �t� trouv� ?
	if (pEnvControle)
		// oui => on le remplit
		*pEnvControle = *pEnvControleNouveau;

	return pEnvControle != NULL;
	} // bAjouteEnvControle

// ---------------------------------------------------------------
// Termine le sous classement et ferme l'EnvControle
static BOOL bFermeEnvControle (PENV_CONTROLE pEnvControle)
	{
	// Env controle valide ?
	if (pEnvControle)
		{
		// oui => lib�re les infos du EnvControle
		pEnvControle->hwnd = NULL;
		pEnvControle->pwndprocInitiale = NULL;
		}

	return pEnvControle != NULL;
	} // bFermeEnvControle

// ---------------------------------------------------------------
// Renvoie le pwndprocInitiale associ� au hwnd d'un contr�le
static WNDPROC pWndProcInitiale (HWND hwnd)
	{
	PENV_CONTROLE pEnvControle = pTrouveEnvControle (hwnd);
	WNDPROC pRes = pEnvControle ? pEnvControle->pwndprocInitiale : NULL;

	return pRes;
	}

// --------------------------------------------------------------------------
// Sous classe un controle
static BOOL bSousClasseControle (HWND hwnd, WNDPROC pfnSousClasse)
	{
	BOOL	bRes = FALSE;

	// Sous classement � effectuer ?
	if (pfnSousClasse && hwnd)
		{
		// oui => sous classe et enregistre le sous classement du contr�le
		ENV_CONTROLE EnvControle;

		EnvControle.hwnd = hwnd;
		EnvControle.pwndprocInitiale = (WNDPROC) SetWindowLong (hwnd, GWL_WNDPROC, (LONG)pfnSousClasse);
		bRes = bAjouteEnvControle (&EnvControle);

		// Ok ?
		if (bRes)
			{
			// oui => sous classe un �ventuel sous contr�le
			HWND	hwndSousControl = GetWindow (hwnd, GW_CHILD);

			if (hwndSousControl)
				bRes = bSousClasseControle (hwndSousControl, pfnSousClasse);
			}
		}
	return bRes;
	}

// ---------------------------------------------------------------
// Termine le sous classement et ferme l'EnvControle
static BOOL bFinSousClasseControle (HWND hwnd)
	{
	PENV_CONTROLE pEnvControle = pTrouveEnvControle (hwnd);
	BOOL					bRes = FALSE;

	// Env controle correspondant trouv� ?
	if (pEnvControle)
		{
		// oui => restitue la WNDPROC initiale du controle
		if (pEnvControle->pwndprocInitiale)
			{
			SetWindowLong (hwnd, GWL_WNDPROC, (LONG) pEnvControle->pwndprocInitiale);
			bRes = bFermeEnvControle (pEnvControle);
			}
		}

	return bRes;
	} // bFinSousClasseControle

// ---------------------------------------------------------------
// WNDPROC de sous classement des controles en phase d'�dition
// ---------------------------------------------------------------
static LRESULT CALLBACK wndprocSousClasseControleEnEdition (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
  {
  LRESULT mres = 0;
  BOOL    bTraite = TRUE;

  switch (msg)
    {
		case WM_DESTROY:
			{
			// Destruction => laisse le controle se fermer ...
			WNDPROC wndprocInitiale = pWndProcInitiale(hwnd);

			VerifWarning (wndprocInitiale);
			mres = CallWindowProc (wndprocInitiale, hwnd, msg, wparam, lparam);

			// ... puis termine le sous classement pour les messages suivants
			bFinSousClasseControle (hwnd);
			}
			break;

		//case	WM_CAPTURECHANGED:

		// Messages souris � renvoyer au parent
		// Conversion � effectuer pour le parent :
		//	xPos = LOWORD(lParam);  // horizontal position of cursor 
		//	yPos = HIWORD(lParam);  // vertical position of cursor 
		case	WM_LBUTTONDBLCLK:
		case	WM_LBUTTONDOWN:
		case	WM_LBUTTONUP:
		case	WM_MBUTTONDBLCLK:
		case	WM_MBUTTONDOWN:
		case	WM_MBUTTONUP:
		case	WM_RBUTTONDBLCLK:
		case	WM_RBUTTONDOWN:
		case	WM_RBUTTONUP:
		case	WM_MOUSEMOVE:
		//case	WM_MOUSEWHEEL: utiliser RegisterWindowMessage
//		case	WM_NCHITTEST:
			{
			POINT pt = {(int)LOWORD(lparam), (int)HIWORD(lparam)};

			::MapWindowPoints(hwnd, ::GetParent(hwnd), &pt, 1);
			mres = ::SendMessage (::GetParent (hwnd), msg, wparam, MAKELPARAM (pt.x, pt.y));
			}
			break;

		// Conversion 
		//nHittest = (INT) LOWORD(lParam);    // hit-test value 
/*	// $$ mapper, rajouter l'�tat des touches et renvoyer au parent (cf zone cliente EDIT)
		case	WM_MOUSEACTIVATE: 
		case	WM_NCLBUTTONDBLCLK:
		case	WM_NCLBUTTONDOWN:
		case	WM_NCLBUTTONUP:
		case	WM_NCMBUTTONDBLCLK:
		case	WM_NCMBUTTONDOWN:
		case	WM_NCMBUTTONUP:
		case	WM_NCMOUSEMOVE:
		case	WM_NCRBUTTONDBLCLK:
		case	WM_NCRBUTTONDOWN:
		case	WM_NCRBUTTONUP:
			mres = ::SendMessage (::GetParent (hwnd), msg, wparam, lparam);
			break;
*/

		// Messages clavier non utilis�s pour les CHILD
		//case WM_GETHOTKEY: 
		//case WM_SETHOTKEY:
		//	break;

		// Messages clavier � renvoyer au parent (apr�s conversion)
		case WM_ACTIVATE:
		case WM_CHAR:
		case WM_DEADCHAR:
		case WM_HOTKEY:
		case WM_KEYDOWN: 
		case WM_KEYUP:
		//case WM_KILLFOCUS:
		case WM_SETFOCUS:
		case WM_SYSCHAR:
		case WM_SYSDEADCHAR:
		case WM_SYSKEYDOWN:
		case WM_SYSKEYUP:
			mres = ::SendMessage (::GetParent (hwnd), msg, wparam, lparam);
			break;

    default:
			bTraite = FALSE;
			break;
    }

  if (!bTraite)
    {
		WNDPROC wndprocInitiale = pWndProcInitiale(hwnd);

		VerifWarning (wndprocInitiale);
    mres = CallWindowProc (wndprocInitiale, hwnd, msg, wparam, lparam);
    }

  return mres;
  }

// -------------------------------------------------------------------------
// Cr�ation d'une fen�tre controle child � l'image d'un �l�ment Gsys
HWND hwndCreeContGR (HWND hwndParent, HGSYS hGsys, HELEMENT hElmtGsys, DWORD id, BOOL bEnEdition)
	{
	HWND		hwndRes = NULL;
	G_ATTRIBUT		wAttributs;
  G_ACTION			wAction;
  G_COULEUR			nCouleurLigne;
  G_COULEUR			nCouleurRemplissage;
  G_STYLE_LIGNE wStyleLigne;
  G_STYLE_REMPLISSAGE wStyleRemplissage;
  DWORD   wPolice;
  DWORD   wStylePolice;
  char *	pszTexte;
  DWORD   wNbPoints;

	// r�cup�re les param�tres du controle
  g_element_get (hGsys, hElmtGsys, &wAttributs, &wAction, &nCouleurLigne, &nCouleurRemplissage,
		&wStyleLigne, &wStyleRemplissage, &wPolice, &wStylePolice, &pszTexte, &wNbPoints);

	// controle valide ?
	if (TRUE) //wNbPoints == 2)
		{
		// oui => r�cup�re sa classe et son style de fen�tre
		PCSTR pszClasse = NULL;
		WNDPROC pfnSousClasse = wndprocSousClasseControleEnEdition;
		DWORD		dwWS = 0;
		DWORD		dwWSEx = 0;

		switch (wAction)
			{
			case G_ACTION_CONT_STATIC: 
				pszClasse = pszClasseSTATIC; 
				dwWS = SS_LEFT | WS_GROUP;
				if (bEnEdition)
					pszTexte = "[Static]";
				break;

			case G_ACTION_CONT_EDIT: 
				pszClasse = pszClasseEDIT; 
				dwWS = ES_AUTOHSCROLL| ES_LEFT | WS_TABSTOP;
				dwWSEx = WS_EX_CLIENTEDGE;
				if (bEnEdition)
					pszTexte = "[Edit]";
				break;

			case G_ACTION_CONT_GROUP: 
				pszClasse = pszClasseBUTTON;
				dwWS = BS_GROUPBOX | WS_GROUP;
				break;

			case G_ACTION_CONT_BOUTON: 
				pszClasse = pszClasseBUTTON;
				dwWS = BS_PUSHBUTTON | WS_TABSTOP;
				break;

			case G_ACTION_CONT_BOUTON_VAL: 
				pszClasse = pszClasseBUTTON;
				dwWS = BS_PUSHBUTTON | WS_TABSTOP;
				// force l'Id en ex�cution
				if (!bEnEdition)
					{
					switch (wStyleRemplissage)
						{
						case ANM_BOUTON_VAL_OK:
							id = IDOK;
							break;
						case ANM_BOUTON_VAL_APPLIQUER:
							id = IDAPPLY;
							break;
						case ANM_BOUTON_VAL_ANNULER:
							id = IDCANCEL;
							break;
						}
					}
				break;

			case G_ACTION_CONT_CHECK: 
				pszClasse = pszClasseBUTTON; 
				dwWS = BS_CHECKBOX | WS_TABSTOP;
				break;

			case G_ACTION_CONT_RADIO: 
				pszClasse = pszClasseBUTTON; 
				dwWS = BS_RADIOBUTTON | WS_TABSTOP;

				// r�cup�re l'�tat WS_GROUP
				if (!bEnEdition)
					{
					if (wStyleRemplissage & WS_GROUP)
						dwWS |= WS_GROUP;
					}
				break;

			case G_ACTION_CONT_COMBO: 
				pszClasse = pszClasseCOMBOBOX;
				dwWS = CBS_DROPDOWN|CBS_AUTOHSCROLL | WS_TABSTOP |WS_VSCROLL;
				break;

			case G_ACTION_CONT_LIST:
				pszClasse = pszClasseLISTBOX;
				dwWS = WS_TABSTOP |WS_VSCROLL;
				if (!bEnEdition)
					dwWS |= LBS_NOTIFY;
				break;

			case G_ACTION_CONT_SCROLL_H: 
				pszClasse = pszClasseSCROLLBAR; 
				dwWS = SBS_HORZ | WS_TABSTOP;
				break;

			case G_ACTION_CONT_SCROLL_V: 
				pszClasse = pszClasseSCROLLBAR;
				dwWS = SBS_VERT | WS_TABSTOP;
				break;
			}

		// action valide ?
		if (pszClasse)
			{
			// oui => r�cup�re ses coordonn�es
			POINT point0;
			POINT point1;

			g_element_get_point (hGsys, hElmtGsys, 0, &point0.x, &point0.y);
			g_element_get_point (hGsys, hElmtGsys, 1, &point1.x, &point1.y);

			// ordonne les bipoints des contr�les apr�s une op�ration d'�dition ($$en attendant mieux)
			G_OrdonneBipoint (&point0, &point1);

			// Convertis les en pixels
			g_convert_point_page_to_dev (hGsys, &point0.x, &point0.y);
			g_convert_point_page_to_dev (hGsys, &point1.x, &point1.y);

			// Cr�e la fen�tre du contr�le
			hwndRes = CreateWindowEx (dwWSEx, pszClasse, pszTexte, dwWS|WS_VISIBLE|WS_CHILD,
				point0.x, point0.y, point1.x - point0.x, point1.y - point0.y, hwndParent, (HMENU) id, Appli.hinst, 0);

			// $$ Modifie la police du contr�le 
			if (TRUE)
			{
				// Init Police
				HDC hdc=GetDC(hwndRes);
				HFONT hFont = fnt_create(hdc,1,PCS_FONT_STYLE_GRAS,14,Appli.byCharSetFont); // ARIAL 14 normal
				// Nouvelle fonte et redessin de l'exemple
				SendMessage (hwndRes, WM_SETFONT, (WPARAM)hFont,0);
			}
			// sous classe le contr�le si n�cessaire
			if (bEnEdition)
				VerifWarning (bSousClasseControle (hwndRes, pfnSousClasse));
			}
		}
	return hwndRes;
	} // hwndCreeContGR

// -------------------------------------------------------------------------
// Ajustement de la position d'une fen�tre controle child
BOOL bChangePosContGR (HWND hwndControle, HGSYS hGsys, POINT point0, POINT point1)
	{
	BOOL	bRes = FALSE;

	// ordonne les bipoints des contr�les apr�s une op�ration d'�dition ($$en attendant mieux)
	G_OrdonneBipoint (&point0, &point1);

	// D�place la fen�tre (en coordonn�es device)
	g_convert_point_page_to_dev (hGsys, &point0.x, &point0.y);
	g_convert_point_page_to_dev (hGsys, &point1.x, &point1.y);
	return ::MoveWindow (hwndControle, point0.x, point0.y, point1.x - point0.x, point1.y - point0.y, TRUE);
	} // bChangePosContGR

// -------------------------------------------------------------------------
// Ajustement de l'ID d'une fen�tre controle child
BOOL bChangeIdContGR (HWND hwndControle, DWORD dwNouvelID)
	{
	BOOL	bRes = FALSE;

	if (hwndControle)
		{
		bRes = TRUE;
		SetWindowLong (hwndControle, GWL_ID, dwNouvelID);
		}
	return bRes;
	} // bChangeIdContGR



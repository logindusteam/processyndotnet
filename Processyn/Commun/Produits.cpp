// Produits.c gestion des produits LI pour protection
// --------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"                  // Types standard
#include "UStr.h"               // Str...()
#include "rand.h"

#include "Produits.h"               


//---------------------------------------------------------------------
// Renvoie la chaine de caract�re associ�e � un mod�le de cl� 
PCSTR pszNomModele (UINT uNModele)
	{
	PCSTR pszRet = "MODELE INCONNU";

	switch (uNModele)
		{
		case modele_delos: pszRet = "DELOS"; break;
		case modele_argos1: pszRet = "ARGOS 1"; break;
		case modele_argos2: pszRet = "ARGOS 2"; break;
		case modele_argos3: pszRet = "ARGOS 3"; break;            
		case modele_microphar1: pszRet = "MICROPHAR 1"; break;
		case modele_microphar2: pszRet = "MICR0PHAR 2"; break;
		case modele_SentinelSuperPro1: pszRet = "SENTINEL 1"; break;
		}
	return pszRet;
	}

//---------------------------------------------------------------------
// Renvoie la chaine de caract�re associ�e � un produit 
PCSTR pszNomProduit (UINT uNProduit)
	{
	PCSTR pszRet = "PRODUIT INCONNU";

	switch (uNProduit)
		{
		case PRODUIT_PROCESSYN_DOS: pszRet = "Pcs DOS"; break;
		case PRODUIT_PROCESSYN_OS2: pszRet = "Pcs OS/2 PE"; break;
		case PRODUIT_PROCESSYN_OS2PM: pszRet = "Pcs PM"; break;
		case PRODUIT_PROCESSYN_WIN32: pszRet = "Processyn"; break;
		case PRODUIT_DLL_WIN32: pszRet = "DLL NT(W95)"; break;
		case PRODUIT_SERIZ_2G_MEXICO: pszRet = "Seriz 2G Mexico"; break;
		case PRODUIT_SERIZ_2G: pszRet = "Seriz 2G"; break;
		case PRODUIT_SERVEUR_OPC: pszRet = "Serveur OPC"; break;
		}
	return pszRet;
	}

//---------------------------------------------------------------------
// Renvoie la chaine de caract�re associ�e � un produit
PCSTR pszNomFonction (UINT uNFonction)
	{
	PCSTR pszRet = "FONCTION INCONNUE";

	switch (uNFonction)
		{
		case FONCTION_PCS_COMPATIBLE_DELOS   : pszRet = "Compatible Delos"; break;
		case FONCTION_PCS_ENSEIGNEMENT       : pszRet = "ENSEIGNEMENT"; break;
		case FONCTION_PCS_SD1                : pszRet = "DEVELOPPEMENT"; break;
		case FONCTION_PCS_NORMAL             : pszRet = "SINGLE USE"; break;
		case FONCTION_PCS_RUN_TIME           : pszRet = "RUNTIME"; break;
		case FONCTION_PCS_MAINTENANCE        : pszRet = "MAINTENANCE"; break;
		case FONCTION_PCS_NANO               : pszRet = "NANO"; break;
		case FONCTION_PCS_MICRO              : pszRet = "MICRO NORMAL"; break;
		case FONCTION_PCS_SD2                : pszRet = "SD2"; break;
		case FONCTION_PCS_PARAMETRAGE        : pszRet = "PARAMETRAGE"; break;
		case FONCTION_PCS_MICRO_RUNTIME      : pszRet = "MICRO RUNTIME"; break;
		case FONCTION_PCS_DEPANNAGE          : pszRet = "DEPANNAGE"; break;
		case FONCTION_DLL_WIN32_MODBUS			 : pszRet = "DLL MODBUS"; break;
		case FONCTION_SERIZ_2G               : pszRet = "Seriz 2G"; break;
		case FONCTION_SERVEUR_OPC_STANDARD   : pszRet = "OPC Standard"; break;
		}
	return pszRet;
	}

//--------------------------------------------------------------------------------------------------------
// Renvoie la chaine de caract�re associ�e � un entier version
PSTR pszVersion (UINT uNVersion, PSTR szRet)
	{
	FLOAT fTemp;

	StrCopy (szRet,"VERSION INCONNUE");
	fTemp = (FLOAT)(uNVersion) / 100;
	StrFLOATToStr(szRet,fTemp);
	return szRet;
	}

//--------------------------------------------------------------------------------------------------------
// Renvoie le num�ro de cl� correspondant aux param�tres
DWORD CalculNumeroLi (DWORD wNumeroSerie, DWORD wNumeroVersion, DWORD dwProduit, DWORD dwFonction, 
											DWORD dwNbVariables, DWORD dwNbHeures, DWORD dwDateLimite)
  {
  DWORD dwRes = 0;

  switch (dwProduit)
    {
    case PRODUIT_PROCESSYN_DOS:
			dwRes = get_rand_dos (wNumeroSerie, wNumeroVersion);
      break;

    case PRODUIT_PROCESSYN_OS2:
			dwRes = get_rand_os2 (wNumeroSerie, wNumeroVersion);
      break;

    case PRODUIT_PROCESSYN_OS2PM:
			dwRes = get_rand_os2pm (wNumeroSerie, wNumeroVersion);
      break;

    case PRODUIT_PROCESSYN_WIN32:
			dwRes = get_rand_OS_WIN32 (wNumeroSerie, wNumeroVersion, dwProduit, dwFonction, dwNbVariables, 0,34,12,0,12, 5);
      break;

		case PRODUIT_DLL_WIN32:
			dwRes = get_rand_OS_WIN32 (wNumeroSerie, wNumeroVersion, dwProduit, dwFonction,12,0,34,12,0,12, 4);
      break;

		case PRODUIT_SERIZ_2G_MEXICO:
			if (dwFonction == FONCTION_SERIZ_2G)
				{
				dwRes = get_rand_OS_WIN32 (wNumeroSerie, dwProduit, dwFonction, wNumeroVersion,12,0,34,12,0,12, 4);
				}
      break;

		case PRODUIT_SERIZ_2G:
			if (dwFonction == FONCTION_SERIZ_2G)
				{
				dwRes = get_rand_OS_WIN32 (wNumeroSerie, dwProduit, dwFonction, wNumeroVersion, dwNbVariables, dwNbHeures, dwDateLimite,12,0,12, 7);
				}
      break;

		case PRODUIT_SERVEUR_OPC:
			if (dwFonction == FONCTION_SERVEUR_OPC_STANDARD)
				{
				dwRes = get_rand_OS_WIN32 (wNumeroSerie, dwProduit, dwFonction, wNumeroVersion, dwNbVariables, dwNbHeures, dwDateLimite,12,0,12, 7);
				}
      break;
    } // switch (dwProduit)

  return dwRes;
  }


/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |									    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 |									    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |   Titre   : GEREBDC.C 						    |
 |   Auteur  : LM							    |
 |   Date    : 06/02/93 						    |
 +--------------------------------------------------------------------------*/
// Version Win32

#include "stdafx.h"
#include <stdlib.h>

#include "std.h"
#include "UStr.h"
#include "mem.h"
#include "dicho.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "Verif.h"
#include "gerebdc.h"
VerifInit;

// types locaux
// constantes num�riques et chaines de caract�res
typedef struct
	{
	char valeur[c_nb_car_ex_mes];
	DWORD ref;
	} CONSTANTE, *PCONSTANTE; // bloc bn_constant

// structure de la table de conversion Id constante vers Num Enr constante
typedef struct
	{
	DWORD dwPosConstante;
	} ID_CONSTANTE, *PID_CONSTANTE; // bloc b_pt_constant

// --------------------------------------------------------------------------
//                        PROCEDURES  LOCALES
// --------------------------------------------------------------------------
static DWORD cherche_pos_libre_es (void)
  {
	DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_e_s);
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, 1);
  DWORD rretour;

  if (es->ref != 0)
    {
		DWORD  i = 1; // Saute le premier enr
		BOOL trouve = FALSE;
    while ((i < nbr_enr) && (!trouve))
      {
      i++;
      es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, i);
      trouve = (es->genre == libre);
      }
    if (trouve)
      {
      rretour = i;
      }
    else
      {
      rretour = nbr_enr + 1;
      }
    }
  else
    {
    rretour = nbr_enr + 1;
    }
  return rretour;
  }

// --------------------------------------------------------------------------
static void maj_pt_es_ident (DWORD limite, REQUETE_EDIT Requete)
  {
	int v_signe = 1;

  if (Requete == SUPPRIME_LIGNE)
      v_signe = -1;

  DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_e_s);
  for (DWORD i = 2; (i <= nbr_enr); i++)
    {
		PENTREE_SORTIE pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, i);
    if (pES->i_identif >= limite)
      {
      (pES->i_identif) += v_signe;
      }
    }
  }

// --------------------------------------------------------------------------
// recherche l'Id constante r�f�ren�ant telle position de constante
static DWORD dwIdFromPosConstante (DWORD dwPosConstanteCherchee)
  {
  DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_pt_constant);
	DWORD dwId = 1;
	for (dwId = 1; dwId <= nbr_enr; dwId++)
		{
		PID_CONSTANTE pIdConstante = (PID_CONSTANTE)pointe_enr (szVERIFSource, __LINE__, b_pt_constant, dwId); // $$ type ?
    if ((pIdConstante->dwPosConstante) == dwPosConstanteCherchee)
			break;
		}
  return dwId;
  }

//--------------------------------------------------------------------------
// Mise � jour de la table de conversion Id / pos constante apr�s modification dans les constantes
static void MAJIdsConstantes (DWORD limite, REQUETE_EDIT Requete)
  {
  DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_pt_constant);
	int	v_signe = 1;
	
  if (Requete == SUPPRIME_LIGNE)
		v_signe = -1;
	
  for (DWORD dwId = 1; dwId <= nbr_enr; dwId++)
    {//$$ cr�e bloc � mettre � jour, g�n�raliser
		PID_CONSTANTE pIdConstante = (PID_CONSTANTE) pointe_enr (szVERIFSource, __LINE__, b_pt_constant, dwId);
    if ((pIdConstante->dwPosConstante) >= limite)
      {
      (pIdConstante->dwPosConstante) += v_signe;
      }
    }
  }

// --------------------------------------------------------------------------
static int comparer_nom_variables (const void *element_1, DWORD numero)
  {
  PIDENTIF	element_2 = (PIDENTIF)pointe_enr (szVERIFSource, __LINE__, bn_identif, numero);

  return (StrCompare ((const char *)element_1, element_2->nom));
  }

// --------------------------------------------------------------------------
static int comparer_constantes (const void *element_1, DWORD numero)
  {
  PCONSTANTE element_2 = (PCONSTANTE)pointe_enr (szVERIFSource, __LINE__, bn_constant, numero);

  return (StrCompare ((const char *)element_1, element_2->valeur));
  }

// --------------------------------------------------------------------------
static int comparer_organise_es (const void *element_1, DWORD numero)
  {
  // les chaines sont sauvegard�es au format PASCAL
  PORGANISATION_ES element_2;

  element_2 = (PORGANISATION_ES)pointe_enr (szVERIFSource, __LINE__, b_organise_es, numero);
  return (StrCompare ((const char *)element_1, element_2->cle));
  }

// --------------------------------------------------------------------------
static int comparer_organise_cle (const void *element_1, DWORD numero)
  {
  // les chaines sont sauvegard�es au format PASCAL
  char *element_2;

  element_2 = (char *)pointe_enr (szVERIFSource, __LINE__, b_organise_cle, numero);
  return (StrCompare ((const char *)element_1, element_2));
  }

// --------------------------------------------------------------------------
//                        PROCEDURES  PUBLIQUES
// --------------------------------------------------------------------------

//--------------------------------------------------------------------------
// Ajoute une nouvelle Variable ES aux variables ES en cours
// Cr�e les blocs � la vol�e si n�cessaire
void InsereVarES (PENTREE_SORTIE	pESInitial, PCSTR pszNomVarES, DWORD * pdwPositionNouvelleES)
  {
  PENTREE_SORTIE es;

  // bloc ES cr�� si n�cessaire
	// avec premier enr 
  if (existe_repere (b_e_s))
    {
    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, 1);
    }
  else
    {
    cree_bloc (b_e_s, sizeof (ENTREE_SORTIE), 1);
    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, 1);
    es->i_identif = 2;
    es->ref = 0;
    }
  (*pdwPositionNouvelleES) = es->i_identif;
  maj_pt_es_ident (pESInitial->i_identif, INSERE_LIGNE);
  if ((*pdwPositionNouvelleES) > nb_enregistrements (szVERIFSource, __LINE__, b_e_s))
    {
    insere_enr (szVERIFSource, __LINE__, 1, b_e_s, (*pdwPositionNouvelleES));
    }
  pESInitial->ref = 0;
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, (*pdwPositionNouvelleES));
  (*es) = (*pESInitial);

  // Mise � jour du bloc des noms de variables bn_identif
  if (!existe_repere (bn_identif))
    {
    cree_bloc (bn_identif, sizeof (IDENTIF), 0);
    }
  insere_enr (szVERIFSource, __LINE__, 1, bn_identif, pESInitial->i_identif);
  PIDENTIF identif = (PIDENTIF)pointe_enr (szVERIFSource, __LINE__, bn_identif, pESInitial->i_identif);
  StrCopy (identif->nom, pszNomVarES);
  identif->dwPosES = (*pdwPositionNouvelleES);

  // maj en tete bloc es
  es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, 1);
  if (es->ref > 0)
    {
    es->ref = es->ref--;
    }
  es->i_identif = cherche_pos_libre_es ();
  }

//--------------------------------------------------------------------------
void consulte_es_bd_c (DWORD position_es, char *ident, char *sens, char *genre)
  {
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);

  NomES (ident, es->i_identif);
  bMotReserve (es->sens, sens);
  bMotReserve (es->genre, genre);
  }

//--------------------------------------------------------------------------
BOOL es_referencee (DWORD position_es)
  {
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
  return es->ref != 0;
  }

//--------------------------------------------------------------------------
BOOL bSupprimeES (DWORD position_es)
  {
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, position_es);
  ENTREE_SORTIE es_cour = (*es);
  BOOL rretour;

  if (es_cour.ref != 0)
    {
    rretour = FALSE;
    }
  else
    {
    rretour = TRUE;
    es->genre = libre;
    enleve_enr (szVERIFSource, __LINE__, 1, bn_identif, es_cour.i_identif);
    maj_pt_es_ident (es_cour.i_identif, SUPPRIME_LIGNE);
    es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, 1);
    es->ref++;
    es->i_identif = cherche_pos_libre_es ();
    }
  return (rretour);
  }

//--------------------------------------------------------------------------
// Mise � jour identificateur, sens, genre et taille  d'une ES existante
// si M�me genre que l'ancienne ES et plus de r�f�rences
// renvoie TRUE dans ce cas, FALSE sinon
BOOL bModifieDeclarationES (PENTREE_SORTIE pNouveauDescriptifES, PCSTR pszNouveauNom, DWORD dwPositionESAModifier)
  {
	// Lecture des param�tres actuels de l'ES � modifier
  PENTREE_SORTIE	pESAModifier = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionESAModifier);
  ENTREE_SORTIE es_old = (*pESAModifier);
  ID_MOT_RESERVE genre_old = es_old.genre;
  DWORD position_ident_old = es_old.i_identif;
  PIDENTIF identif;
  DWORD position_ident_new;
  BOOL bRet = TRUE;

	// M�me genre que l'ancienne ES et plus de r�f�rences ?
  if ((pNouveauDescriptifES->genre == genre_old) || (es_old.ref == 0))
    {
		// r�cup�re ses anciens param�tres
		char nom_old[82];
    NomES (nom_old, position_ident_old);

		// identificateur different ?
    if (!bStrEgales (pszNouveauNom, nom_old))
      {

      // recherche position nouvelle e/s (ne doit pas exister)
      if (!bTrouveChaineDansIdenti (pszNouveauNom, &position_ident_new))
        {
				// suppression ancienne e/s ...
				enleve_enr (szVERIFSource, __LINE__, 1, bn_identif, position_ident_old);
				maj_pt_es_ident (position_ident_old, SUPPRIME_LIGNE);

				// recalcul de la position apr�s suppression de l'ancienne
				bTrouveChaineDansIdenti (pszNouveauNom, &position_ident_new);

        // insertion nouvelle e/s
        maj_pt_es_ident (position_ident_new, INSERE_LIGNE);
        insere_enr (szVERIFSource, __LINE__, 1, bn_identif, position_ident_new);
        identif = (PIDENTIF)pointe_enr (szVERIFSource, __LINE__, bn_identif, position_ident_new);
        identif->dwPosES = dwPositionESAModifier;
        StrCopy (identif->nom, pszNouveauNom);
        }
			else
				{
				bRet = FALSE;
				}
      }
		else
			{
			position_ident_new = position_ident_old;
			}
		
		if (bRet)
			{
			// recopie les autre propri�t�s 
			pESAModifier = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionESAModifier);
			pESAModifier->sens = pNouveauDescriptifES->sens;
			pESAModifier->genre = pNouveauDescriptifES->genre;
			pESAModifier->taille = pNouveauDescriptifES->taille;
			pESAModifier->i_identif = position_ident_new;
			pESAModifier->ref = es_old.ref;
			}
    }
	else
		{
    bRet = FALSE;
		}

  return bRet;
  } // bModifieDeclarationES


//--------------------------------------------------------------------------
void ReferenceES (DWORD num_enr)
  {
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, num_enr);

  (es->ref)++;
  }

//--------------------------------------------------------------------------
void DeReferenceES (DWORD num_enr)
  {
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, num_enr);

  es->ref--;
  }

//--------------------------------------------------------------------------
// R�cup�re le nom d'une variable d'entr�e sortie 
// � partir de sa position d'entr�e sortie
void NomVarES (PSTR pszNomES, DWORD dwPositionES)
	{
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
	NomES (pszNomES, es->i_identif);
	}

//--------------------------------------------------------------------------
// Compare une chaine et le nom d'une variable d'entr�e sortie 
// � partir de sa position d'entr�e sortie
BOOL bStrEgaleANomVarES (PCSTR pszNom, DWORD dwPositionES)
	{
	char szNomES[c_nb_car_es];
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
	NomES (szNomES, es->i_identif);
	return bStrEgales (szNomES ,pszNom);
	}

//--------------------------------------------------------------------------
// Compare (Hors casse) une chaine et le nom d'une variable d'entr�e sortie 
// � partir de sa position d'entr�e sortie
BOOL bStrIEgaleANomVarES (PCSTR pszNom, DWORD dwPositionES)
	{
	char szNomES[c_nb_car_es];
  PENTREE_SORTIE es = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
	NomES (szNomES, es->i_identif);
	return bStrIEgales (szNomES ,pszNom);
	}

//--------------------------------------------------------------------------
// Cherche une variable d'apr�s son UL
// Si trouv�e, renvoie TRUE et sa position dans le bloc des entr�es sorties
// Renvoie FALSE sinon.
BOOL bReconnaitESExistante (PUL pUL, DWORD *position_bes)
  {
  BOOL bTrouve = FALSE;
  DWORD pos_ident;
  BOOL exist_ident;

  if (bReconnaitESValide (pUL, &exist_ident, &pos_ident))
    {
    if (exist_ident)
      {
			PIDENTIF pIdentif = (PIDENTIF)pointe_enr (szVERIFSource, __LINE__, bn_identif, pos_ident);
      (*position_bes) = pIdentif->dwPosES;
			bTrouve = TRUE;
      }
		}
  return bTrouve;
  }

//--------------------------------------------------------------------------
// Cherche une variable ES d'apr�s son nom et son type (c_res_logique, c_res_numerique, c_res_message)
// Si trouv�e, et de m�me type renvoie TRUE et sa position dans le bloc des entr�es sorties
// Renvoie FALSE sinon.
BOOL bReconnaitESExistanteDeType (PUL pUL, DWORD *position_bes, ID_MOT_RESERVE idType)
  {
  BOOL bTrouve = FALSE;
  DWORD pos_ident;
  BOOL exist_ident;

  if (bReconnaitESValide (pUL, &exist_ident, &pos_ident) && exist_ident)
    {
		DWORD dwPositionES = dwIdVarESFromPosIdentif (pos_ident);
		PENTREE_SORTIE pEntreeSortie = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
		if (pEntreeSortie->genre == idType)
			{
			(*position_bes) = dwPositionES;
			bTrouve = TRUE;
			}
		}
  return bTrouve;
  }

//--------------------------------------------------------------------------
// R�cup�re les infos d'entr�e sortied'une Var ES Existante
void InfoVarES (PENTREE_SORTIE pEntreeSortie, DWORD dwPositionES)
	{
  PENTREE_SORTIE pEntreeSortieExistante = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
	*pEntreeSortie = *pEntreeSortieExistante;
	}

//--------------------------------------------------------------------------
// Renvoie TRUE si une variable d'entr�e sortie sp�cifi�e par sa position
// d'entr�e sortie est num�rique, FALSE sinon
BOOL bESEstNumerique (DWORD dwPositionES)
	{
  PENTREE_SORTIE pEntreeSortie = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
	return pEntreeSortie->genre == c_res_numerique;
	}

//--------------------------------------------------------------------------
// Renvoie TRUE si une variable d'entr�e sortie sp�cifi�e par sa position
// d'entr�e sortie est logique, FALSE sinon
BOOL bESEstLogique (DWORD dwPositionES)
	{
  PENTREE_SORTIE pEntreeSortie = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
	return pEntreeSortie->genre == c_res_logique;
	}

//--------------------------------------------------------------------------
// Renvoie le genre (c_res_logique, ...) d'une variable d'entr�e sortie sp�cifi�e
// par sa position d'entr�e sortie
ID_MOT_RESERVE nGenreES (DWORD dwPositionES)
	{
  PENTREE_SORTIE pEntreeSortie = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
	return pEntreeSortie->genre;
	}

//--------------------------------------------------------------------------
// Renvoie 0 si variable simple, sinon le Nb d'�l�ments du tableau
DWORD dwTailleES (DWORD dwPositionES)
	{
  PENTREE_SORTIE pEntreeSortie = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, dwPositionES);
	return pEntreeSortie->taille;
	}

//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// Gestion des constantes
//--------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------------------
// Apr�s recherche : ajoute une nouvelle constante ou une r�f�rence � une constante existante
void AjouteConstanteBd (DWORD pos_alphabetic, BOOL bExisteDeja, PCSTR pszValeur, DWORD * pdwIdConstante)
  {
	// Constante existe d�ja ?
  if (!bExisteDeja)
    {
		// Non => initialise les blocs � la vol�e
    if (!existe_repere (bn_constant))
      {
      cree_bloc (bn_constant, sizeof (CONSTANTE), 0);
      }
    if (!existe_repere (b_pt_constant))
      {
      cree_bloc (b_pt_constant,sizeof (ID_CONSTANTE), 0);
      }

    // maj bloc b_pt_constant
		// Renvoie l'adresse du premier trou dans la table des Ids
    (*pdwIdConstante) = dwIdFromPosConstante (0);

		// prends en compte l'insertion dans les constantes � venir
    MAJIdsConstantes (pos_alphabetic, INSERE_LIGNE);

		// Ajoute un enr � la table des Ids de constante si n�cessaire
    if ((*pdwIdConstante) > nb_enregistrements (szVERIFSource, __LINE__, b_pt_constant))
      {
      insere_enr (szVERIFSource, __LINE__, 1, b_pt_constant, (*pdwIdConstante));
      }
    PID_CONSTANTE pIdConstante = (PID_CONSTANTE) pointe_enr (szVERIFSource, __LINE__, b_pt_constant, (*pdwIdConstante));
    pIdConstante->dwPosConstante = pos_alphabetic;

    // maj dans bloc bn_constant des pointeurs sur b_cst_nom
    insere_enr (szVERIFSource, __LINE__, 1, bn_constant, pos_alphabetic);

		// Ajoute la constante avec 1 r�f�rence
		PCONSTANTE pConstante = (PCONSTANTE)pointe_enr (szVERIFSource, __LINE__, bn_constant, pos_alphabetic);
    pConstante->ref = 1;
    StrCopy (pConstante->valeur, pszValeur);
    // maj bloc b_cst_nom
    }
  else
    {
    (*pdwIdConstante) = dwIdFromPosConstante (pos_alphabetic);
    PCONSTANTE pConstante = (PCONSTANTE)pointe_enr (szVERIFSource, __LINE__, bn_constant, pos_alphabetic);
    (pConstante->ref)++;
    }
  } // AjouteConstanteBd

//--------------------------------------------------------------------------
// Ajoute une nouvelle constante ou une r�f�rence � une constante existante
void AjouteConstanteBd (PCSTR pszValeur, DWORD * pdwIdConstante)
  {
	// Recherche la constante
	DWORD pos_alphabetic;
	BOOL bExisteDeja  = bTrouveChaineDansConstant (pszValeur, &pos_alphabetic);

	AjouteConstanteBd (pos_alphabetic, bExisteDeja, pszValeur, pdwIdConstante);
  } // AjouteConstanteBd

//--------------------------------------------------------------------------
void consulte_cst_bd_c (DWORD pos_pt_cste, PSTR pszBuf)
  {
  PID_CONSTANTE pIdConstante = (PID_CONSTANTE) pointe_enr (szVERIFSource, __LINE__, b_pt_constant,pos_pt_cste);
  PCONSTANTE pConstante = (PCONSTANTE)pointe_enr (szVERIFSource, __LINE__, bn_constant, pIdConstante->dwPosConstante);

  StrCopy (pszBuf, pConstante->valeur);
  }

//--------------------------------------------------------------------------
void supprime_cst_bd_c (DWORD pos_pt_cste)
  {
  PID_CONSTANTE pIdConstante = (PID_CONSTANTE) pointe_enr (szVERIFSource, __LINE__, b_pt_constant, pos_pt_cste);
  DWORD pos_a_supprimer = pIdConstante->dwPosConstante;
  PCONSTANTE cste = (PCONSTANTE)pointe_enr (szVERIFSource, __LINE__, bn_constant, pos_a_supprimer);
  if (cste->ref == 1)
    {
    pIdConstante->dwPosConstante = 0;
    enleve_enr (szVERIFSource, __LINE__, 1, bn_constant, pos_a_supprimer);
    MAJIdsConstantes (pos_a_supprimer, SUPPRIME_LIGNE);
    }
  else
    {
    (cste->ref)--;
    }
  }

//--------------------------------------------------------------------------
void modifie_cst_bd_c (char *valeur, DWORD *pos_pt_cst)
  {
  char cste_old[82];
  BOOL exist_cst;
  DWORD pos_cst;

  consulte_cst_bd_c ((*pos_pt_cst), cste_old);
  if (!bStrEgales (valeur, cste_old))
    {
    supprime_cst_bd_c ((*pos_pt_cst));
    exist_cst = bTrouveChaineDansConstant (valeur, &pos_cst);
    AjouteConstanteBd (pos_cst, exist_cst, valeur, pos_pt_cst);
    }
  }

//--------------------------------------------------------------------------
void action_cst_bd_c (REQUETE_EDIT action, char *valeur, DWORD pos_pt_cste)
  {
  switch (action)
    {
    case CONSULTE_LIGNE :
      consulte_cst_bd_c (pos_pt_cste, valeur);
      break;
    case SUPPRIME_LIGNE :
      supprime_cst_bd_c (pos_pt_cste);
      break;
    default:
      break;
    }
  }

//--------------------------------------------------------------------------
BOOL renomme_cst_bd_c (char *new_valeur, DWORD pos_pt_cst)
  {
  BOOL possible = TRUE;
  char old_valeur[82];

  consulte_cst_bd_c (pos_pt_cst, old_valeur);
	// Nouvelle valeur diff�rente de l'ancienne ?
  if (!bStrEgales (old_valeur, new_valeur))
    {
    // oui => recherche existence de new_valeur
		DWORD pos_alpha;
    possible = (!bTrouveChaineDansConstant (new_valeur, &pos_alpha));
    if (possible)
      {
      // Suppression
			PID_CONSTANTE pIdConstante = (PID_CONSTANTE)pointe_enr (szVERIFSource, __LINE__, b_pt_constant, pos_pt_cst);
			DWORD pos_a_supprimer = pIdConstante->dwPosConstante;
			PCONSTANTE cste = (PCONSTANTE) pointe_enr (szVERIFSource, __LINE__, bn_constant, pos_a_supprimer);
			CONSTANTE cste_tmp;

      // m�morisation de l'enre � supprimer pour r�insertion...
      cste_tmp.ref = cste->ref;
      // 
      enleve_enr (szVERIFSource, __LINE__, 1, bn_constant, pos_a_supprimer);
      MAJIdsConstantes (pos_a_supprimer, SUPPRIME_LIGNE);

      // Recherche position d'insertion de new_valeur
      bTrouveChaineDansConstant (new_valeur, &pos_alpha);

      // Insertion

      // maj bloc b_pt_constant
      MAJIdsConstantes (pos_alpha, INSERE_LIGNE);
      pIdConstante = (PID_CONSTANTE)pointe_enr (szVERIFSource, __LINE__, b_pt_constant, pos_pt_cst);
      pIdConstante->dwPosConstante = pos_alpha;

      // maj dans bloc bn_constant
      insere_enr (szVERIFSource, __LINE__, 1, bn_constant, pos_alpha);
      cste = (PCONSTANTE)pointe_enr (szVERIFSource, __LINE__, bn_constant, pos_alpha);

      // restoration de l'enre supprimer
      cste->ref = cste_tmp.ref;
      StrCopy (cste->valeur, new_valeur);

      }  // new_valeur inexistante
    } // new_valeur <> old_valeur
  return possible;
  }

// ------------------------------------------------------------------------
// Cherche une chaine dans bn_constant
BOOL bTrouveChaineDansConstant (PCSTR pszChaine, DWORD * pdwNInsertion)
  {
  DWORD nbr_enr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bn_constant);

  return trouve_position_dichotomie (pszChaine, 1, nbr_enr, comparer_constantes, pdwNInsertion);
  }

// ------------------------------------------------------------------------
// Cherche une chaine dans bn_identif
BOOL bTrouveChaineDansIdenti (PCSTR pszChaine, DWORD * pdwNInsertion)
  {
  DWORD nbr_enr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, bn_identif);

  return trouve_position_dichotomie (pszChaine, 1, nbr_enr, comparer_nom_variables, pdwNInsertion);
  }

// ------------------------------------------------------------------------
// Cherche une chaine dans b_organise_es
BOOL bTrouveChaineDansOrganiseES (PCSTR pszChaine, DWORD * pdwNInsertion)
  {
  DWORD nbr_enr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_organise_es);
	return trouve_position_dichotomie (pszChaine, 1, nbr_enr, comparer_organise_es, pdwNInsertion);
  }

// ------------------------------------------------------------------------
// Cherche une chaine dans b_organise_cle
BOOL bTrouveChaineDansOrganiseCle (PCSTR pszChaine, DWORD * pdwNInsertion)
  {
  DWORD nbr_enr = dwNbEnregistrementsSansErr (szVERIFSource, __LINE__, b_organise_cle);
	return trouve_position_dichotomie (pszChaine, 1, nbr_enr, comparer_organise_cle, pdwNInsertion);
  }

//--------------------------------------------------------------------------
// Verifie si l'UL est un identifiant de var valide (format Ok, ni mot r�serv� ni constante)
// Si oui, une recherche dans la base est effectu�e pour savoir si
// l'identifiant est d�ja r�f�renc� dans la base (*pbExisteDansBase et *pdwPositionIdentTrouvee mis � jour)
// Renvoie TRUE s'il s'agit d'un identifiant de var valide, FALSE sinon.
//--------------------------------------------------------------------------
BOOL bReconnaitESValide (PUL ul, BOOL *pbExisteDansBase, DWORD *pdwPositionIdentTrouvee)
  {
  BOOL bValide = FALSE;

	// genre ul = identifiant ?
  if (ul->genre == g_id)
    {
		// oui => ni mot r�serv�, ni constante ?
		ID_MOT_RESERVE nTemp;

    if ((!bReconnaitMotReserve (ul->show, &nTemp)) &&
       (!bReconnaitConstanteValide (ul, &nTemp)))
      {
			// oui : taille compatible avec un identifiant ?
      if (StrLength (ul->show) < c_nb_car_es)
        {
				// oui => identifiant valide
        bValide = TRUE;
				// regarde s'il est d�ja r�f�renc�
        (*pbExisteDansBase) = bTrouveChaineDansIdenti (ul->show, pdwPositionIdentTrouvee);
        }
      }
    }
  return bValide;
  } // bReconnaitESValide

//--------------------------------------------------------------------------
// Verifie si l'UL est un identifiant de var valide (format Ok, ni mot r�serv� ni constante)
// Si oui, *pVarReconnue est mise � jour : une recherche dans la base est effectu�e pour savoir si
// l'identifiant est d�ja r�f�renc� dans la base
// Renvoie TRUE s'il s'agit d'un identifiant de var valide, FALSE sinon.
BOOL bReconnaitESValide (int nUL, PUL ul, PVARIABLE_RECONNUE_CF pVarReconnue)
  {
  BOOL bValide = FALSE;

	// genre ul = identifiant ?
  if (ul->genre == g_id)
    {
		// oui => ni mot r�serv�, ni constante ?
		ID_MOT_RESERVE nTemp;

    if ((!bReconnaitMotReserve (ul->show, &nTemp)) &&
       (!bReconnaitConstanteValide (ul, &nTemp)))
      {
			// oui : taille compatible avec un identifiant ?
      if (StrLength (ul->show) < c_nb_car_es)
        {
				// oui => identifiant valide
        bValide = TRUE;

				// Met � jour *pVarReconnue
				pVarReconnue->nULVariable = nUL;

				// regarde s'il est d�ja r�f�renc�
        if (bTrouveChaineDansIdenti (ul->show, &pVarReconnue->es.i_identif))
					{
					pVarReconnue->dwPosESExistante = dwIdVarESFromPosIdentif (pVarReconnue->es.i_identif);
					InfoVarES (&pVarReconnue->es, pVarReconnue->dwPosESExistante);
					}
				else
					{
					pVarReconnue->dwPosESExistante = 0;
					pVarReconnue->es.genre = libre;
					pVarReconnue->es.sens = libre;
					pVarReconnue->es.ref = 0;
					// pVarReconnue->es.i_identif; d�ja initialis�
					pVarReconnue->es.n_desc = 0;
					pVarReconnue->es.taille = 0;	// Par d�faut var simple
					}
        }
      }
    }
  return bValide;
  } // bReconnaitESValide

//--------------------------------------------------------------------------
// Verifie si l'UL est une constante num�rique valide (format Ok)
// Si oui, une recherche dans la base est effectu�e pour savoir si
// la constante est d�ja r�f�renc�e dans la base (*pbExisteDansBase et *pdwPositionTrouvee mis � jour)
// Renvoie TRUE s'il s'agit d'une constante num�rique valide, FALSE sinon.
BOOL bReconnaitConstanteNum (PUL ul, BOOL *pbExisteDansBase, FLOAT *valeur, DWORD *pdwPositionTrouve)
  {
  BOOL bretour = FALSE;

  (*pbExisteDansBase) = bTrouveChaineDansConstant (ul->show, pdwPositionTrouve);
  if ((ul->genre == g_num) && (StrToFLOAT (valeur, ul->show)))
    {
    bretour = TRUE;
    }

  return bretour;
  }


//--------------------------------------------------------------------------
// Cherche un mot dans la liste des mots r�serv�s.
// S'il y est reconnu, renvoie TRUE et *pdwNMotReserve est mis � jour
// Renvoie FALSE sinon
BOOL bReconnaitMotReserve (PCSTR pszMotAReconnaitre, ID_MOT_RESERVE *pdwNMotReserve)
  {
  BOOL bretour = FALSE;

  if (StrLength (pszMotAReconnaitre) <= (c_nb_car_message_res-2))
    {
    (*pdwNMotReserve) = nChercheIdMotReserve (pszMotAReconnaitre);
    if ((*pdwNMotReserve) != 0)
      bretour = TRUE;
    }
  return bretour;
  }

//--------------------------------------------------------------------------
// Renvoie TRUE si l'ul pass� est une constante message valide;
// si oui renvoie son �ventuelle existence dans la table des constantes
BOOL bReconnaitConstanteMessage 
	(const UL *ul, BOOL * pbExisteDansConstantes, DWORD * pdwPositionTrouve)
  {
  BOOL bRetour = FALSE;

	// Recherche si Ul existe d�ja dans les constantes
  (*pbExisteDansConstantes) = bTrouveChaineDansConstant (ul->show, pdwPositionTrouve);

	// constante message si genre g_id et commence par "
  return (ul->genre == g_id) && (ul->show[0] == '"');
  }

//--------------------------------------------------------------------------
// Renvoie TRUE si l'ul pass� est une constante valide;
// si oui renvoie son genre, sa valeur et son �ventuelle existence dans la table des constantes
BOOL bReconnaitConstante
	(const UL * pUL, BOOL * pbValeurLog, FLOAT * prValeurNum,
	BOOL * pbEstLog, BOOL * pbEstNum, BOOL * pbExisteDansConstantes, DWORD * pdwPositionTrouve)
  {
  BOOL bRetour = FALSE;

	// par d�faut ni num�rique ni logique
	(*pbEstNum) = FALSE;
	(*pbEstLog) = FALSE;

	// Recherche si Ul existe d�ja dans les constantes
  (*pbExisteDansConstantes) = bTrouveChaineDansConstant (pUL->show, pdwPositionTrouve);

	// selon le type de l'UL
  switch (pUL->genre)
    {
    case g_num:
			{
			// essaye de reconnaitre un num�rique
      (*pbEstNum) = StrToFLOAT (prValeurNum, pUL->show);
      bRetour = (*pbEstNum);
			}
      break;

    case g_id:
			{
			// d�bute par un " ?
      if ((pUL->show[0]) == '"')
        {
				// oui => Ok (constante ni num�rique ni logique)
        bRetour = TRUE;
        }
      else
        {
				// non => Mot r�serv� H ?
				if (bChaineEstMotReserve (pUL->show, c_res_un))
					{
					// oui => logique
					(*pbEstLog) = TRUE;
					(*pbValeurLog) = TRUE;
					}
				else
					{
					// non => Mot r�serv� L ?
					if (bChaineEstMotReserve (pUL->show, c_res_zero))
						{
						// oui => logique
						(*pbEstLog) = TRUE;
						(*pbValeurLog) = TRUE;
						}
					}
				// constante valide si Logique
        bRetour = (*pbEstLog);
        }
			}
      break;
    } // switch (pUL->genre)
  return bRetour;
  } // bReconnaitConstante

//--------------------------------------------------------------------------
// Renvoie TRUE si l'ul pass� est une constante valide;
// si oui renvoie son genre
BOOL bReconnaitConstanteValide
	(const UL * pUL, PID_MOT_RESERVE	pIdGenre)
  {
  BOOL bRetour = FALSE;

	// par d�faut pas de genre
	*pIdGenre = libre;

	// selon le type de l'UL
  switch (pUL->genre)
    {
    case g_num:
			{
			// essaye de reconnaitre un num�rique
			FLOAT rValeurNum;
      if (StrToFLOAT (&rValeurNum, pUL->show))
				{
				*pIdGenre = c_res_numerique;
				bRetour = TRUE;
				}
			}
      break;

    case g_id:
			{
			// d�bute par un " ?
      if ((pUL->show[0]) == '"')
        {
				// oui => Ok (constante ni num�rique ni logique)
				*pIdGenre = c_res_message;
        bRetour = TRUE;
        }
      else
        {
				// non => Mot r�serv� H ?
				if (bChaineEstMotReserve (pUL->show, c_res_un) || bChaineEstMotReserve (pUL->show, c_res_zero))
					{
					// oui => logique
					*pIdGenre = c_res_logique;
					bRetour = TRUE;
					}
        }
			}
      break;
    } // switch (pUL->genre)
  return bRetour;
  } // bReconnaitConstanteValide


//---------------------------------------------------------------------------
// R�cup�re le nom d'une variable d'entr�e sortie 
// � partir de sa position d'identifiant(char nom[c_nb_car_es])
void NomES (PSTR pszNomES, DWORD position_identif)
  {
  PIDENTIF identif = (PIDENTIF)pointe_enr (szVERIFSource, __LINE__, bn_identif, position_identif);

	StrCopy (pszNomES, identif->nom);
  }

//----------------------------------------------------------------------------
// Renvoie l'Id d'une variable ES � partir de sa position d'identifiant
DWORD dwIdVarESFromPosIdentif (DWORD position_identif)
  {
  PIDENTIF identif = (PIDENTIF)pointe_enr (szVERIFSource, __LINE__, bn_identif, position_identif);
  return (identif->dwPosES);
  }


//------------------------------- fin Gerebdc.c -------------------------


//---------------------------------------------------------------
// UExcept.c
//
// Gestion des exceptions utilisateurs
// Version WIN32 11/6/97 JS
//
//---------------------------------------------------------------
#include "stdafx.h"
#include "USignale.h"
#include "UExcept.h"

//---------------------------------------------------------------
// Renvoie le nom du module correspondant a son Code d'exception
static PCSTR pszExcepCode (DWORD dwExcepCode)
	{
	PCSTR	pszRes = "Cat�gorie d'exception inconnue";

	switch (dwExcepCode)
		{
		// User
		case EXCEPTION_VERIF:
			pszRes = "Un �tat anormal a �t� d�tect�";
			break;
		case EXCEPTION_MEMMAN:
			pszRes = "Gestion m�moire";
			break;
		case EXCEPTION_BLOCMAN:
			pszRes = "Gestion de blocs m�moire";
			break;
		case EXCEPTION_BLKMAN:
			pszRes = "Gestion de Base de donn�e m�moire";
			break;
		case EXCEPTION_SIGNALE:
			pszRes = "Message d'information";
			break;
		case EXCEPTION_ERR_COMPILE:
			pszRes = "Erreur de compilation";
			break;

		// hardware
		case EXCEPTION_ACCESS_VIOLATION :
			pszRes = "The thread attempted to read from or write to a virtual address for which it does not have the appropriate access.";
			break;
		case EXCEPTION_DATATYPE_MISALIGNMENT	:
			pszRes = "The thread attempted to read or write data that is misaligned on hardware that does not provide alignment. For example, 16-bit values must be aligned on 2-byte boundaries, 32-bit values on 4-byte boundaries, and so on.";
			break;
		case EXCEPTION_ARRAY_BOUNDS_EXCEEDED	:
			pszRes = "The thread attempted to access an array element that is out of bounds, and the underlying hardware supports bounds checking.";
			break;
		case EXCEPTION_FLT_DENORMAL_OPERAND	:
			pszRes = "One of the operands in a floating-point operation is denormal. A denormal value is one that is too small to represent as a standard floating-point value.";
			break;
		case EXCEPTION_FLT_DIVIDE_BY_ZERO	:
			pszRes = "The thread attempted to divide a floating-point value by a floating-point divisor of zero.";
			break;
		case EXCEPTION_FLT_INEXACT_RESULT	:
			pszRes = "The result of a floating-point operation cannot be represented exactly as a decimal fraction.";
			break;
		case EXCEPTION_FLT_INVALID_OPERATION	:
			pszRes = "This exception represents any floating-point exception not included in this list.";
			break;
		case EXCEPTION_ILLEGAL_INSTRUCTION	:
			pszRes = "The thread tried to execute an invalid instruction.";
			break;
		case EXCEPTION_IN_PAGE_ERROR	:
			pszRes = "The thread tried to access a page that was not present, and the system was unable to load the page. For example, this exception might occur if a network connection is lost while running a program over the network.";
			break;
		case EXCEPTION_FLT_OVERFLOW	:
			pszRes = "The exponent of a floating-point operation is greater than the magnitude allowed by the corresponding type.";
			break;
		case EXCEPTION_FLT_STACK_CHECK	:
			pszRes = "The stack overflowed or underflowed as the result of a floating-point operation.";
			break;
		case EXCEPTION_FLT_UNDERFLOW	:
			pszRes = "The exponent of a floating-point operation is less than the magnitude allowed by the corresponding type.";
			break;
		case EXCEPTION_INT_DIVIDE_BY_ZERO	:
			pszRes = "The thread attempted to divide an integer value by an integer divisor of zero.";
			break;
		case EXCEPTION_INT_OVERFLOW	:
			pszRes = "The result of an integer operation caused a carry out of the most significant bit of the result.";
			break;
		case EXCEPTION_PRIV_INSTRUCTION	:
			pszRes = "The thread attempted to execute an instruction whose operation is not allowed in the current machine mode.";
			break;
		case EXCEPTION_NONCONTINUABLE_EXCEPTION	:
			pszRes = "The thread attempted to continue execution after a noncontinuable exception occurred.";
			break;

		// debuguer
		case EXCEPTION_BREAKPOINT	:
			pszRes = "A breakpoint was encountered.";
			break;
		case EXCEPTION_SINGLE_STEP :
			pszRes = "A trace trap or other single-instruction mechanism signaled that one instruction has been executed.";
			break;
		}
	return pszRes;
	}

//---------------------------------------------------------------
// G�n�re une exception en sp�cifiant une ligne d'explication
static void GenereExcep (DWORD dwExcepCode, PCSTR	pszExplication, BOOL bContinuable)
	{
	char szExplication [EXCEP_NB_CAR_MAX_EXPLICATION+1];
	DWORD adwArguments [1] = {(DWORD)szExplication};

	// formate les explication
	if (pszExplication)
		wsprintf (szExplication, "%s\n%s", pszExcepCode (dwExcepCode), pszExplication);
	else
		wsprintf (szExplication, "%s\n", pszExcepCode (dwExcepCode));

	// Monte l'exception
	RaiseException 
		(
		dwExcepCode, 							// exception code
		bContinuable ? 0 : EXCEPTION_NONCONTINUABLE,	// continuable exception flag
    1,												// number of arguments in array
    adwArguments							// address of array of arguments
		);
	}


//---------------------------------------------------------------
// G�n�re une exception en passant un mot
static void GenereExcep (DWORD dwExcepCode, DWORD	dwMot1)
	{
	DWORD adwArguments [1] = {dwMot1};

	// Monte l'exception
	RaiseException 
		(
		dwExcepCode,// exception code
		0,					// continuable exception flag
    1,					// number of arguments in array
    &dwMot1			// address of array of arguments
		);
	}


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//	FONCTIONS EXPORTEES
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------
// Envoie une exception avec pour seule explication son nom
void ExcepBrute 
	(
	DWORD dwExcepCode	// exemple : EXCEPTION_MEMMAN
	)
	{
	// g�n�re l'exception, sans explication
	GenereExcep (dwExcepCode, NULL, FALSE);
	}

//---------------------------------------------------------------
// Envoie une exception continuable avec une ligne d'explication
void ExcepWarnContexte 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte		// exemple : "adresse invalide"
	)
	{
	// g�n�re l'exception
	GenereExcep (dwExcepCode, pszContexte, TRUE);
	}

//---------------------------------------------------------------
// Envoie une exception avec une ligne d'explication
void ExcepContexte 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte		// exemple : "adresse invalide"
	)
	{
	// g�n�re l'exception
	GenereExcep (dwExcepCode, pszContexte, FALSE);
	}

//---------------------------------------------------------------
// Envoie une exception avec une ligne d'explication � formater avec un param�tre
void ExcepFormatContexte1 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte,	// exemple : "insertion en %lu invalide" ou "queue %s non trouv�e"
	DWORD dwP1					// param�tre � inclure dans le formatage du contexte
	)
	{
	char szContexte [EXCEP_NB_CAR_MAX_EXPLICATION+1];

	// formate le contexte 
	wsprintf (szContexte, pszContexte, dwP1);

	// g�n�re l'exception
	GenereExcep (dwExcepCode, szContexte, FALSE);
	}

//---------------------------------------------------------------
// Envoie une exception avec une ligne d'explication � formater avec deux param�tre
void ExcepFormatContexte2 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte,	// exemple : "insertion enr %ul invalide dans bloc %lu impossible"
	DWORD dwP1,					// param�tre � inclure dans le formatage du contexte en position 1
	DWORD dwP2					// param�tre � inclure dans le formatage du contexte en position 2
	)
	{
	char szContexte [EXCEP_NB_CAR_MAX_EXPLICATION+1];

	// formate le contexte 
	wsprintf (szContexte, pszContexte, dwP1, dwP2);

	// g�n�re l'exception
	GenereExcep (dwExcepCode, szContexte, FALSE);
	}

//---------------------------------------------------------------
// Envoie une exception avec une ligne d'explication � formater avec trois param�tre
void ExcepFormatContexte3
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte,	// exemple : "insertion enr %lu de %lu enr invalide dans bloc %lu impossible"
	DWORD dwP1,					// param�tre � inclure dans le formatage du contexte en position 1
	DWORD dwP2,					// param�tre � inclure dans le formatage du contexte en position 2
	DWORD dwP3					// param�tre � inclure dans le formatage du contexte en position 3
	)
	{
	char szContexte [EXCEP_NB_CAR_MAX_EXPLICATION+1];

	// formate le contexte 
	wsprintf (szContexte, pszContexte, dwP1, dwP2, dwP3);

	// g�n�re l'exception
	GenereExcep (dwExcepCode, szContexte, FALSE);
	}

//---------------------------------------------------------------
// Envoie une exception avec un code d'erreur Processyn
void ExcepErreur
	(
	DWORD dwExcepCode,		// exemple : EXCEPTION_ERR_COMPILE
	DWORD dwErreurCode		// Num�ro d'erreur dans la nomenclature Processyn
	)
	{
	// g�n�re l'exception
	GenereExcep (dwExcepCode, dwErreurCode);
	}

//---------------------------------------------------------------
// Envoie une exception de type EXCEPTION_ERR_COMPILE
// avec son code d'erreur Processyn s'il est != 0
void ExcepErreurCompile
	(
	DWORD dwErreurCode		// Num�ro d'erreur dans la nomenclature Processyn
	)
	{
	// Num�ro d'erreur != 0 ?
	if (dwErreurCode)
		{
		// oui => g�n�re une exception EXCEPTION_ERR_COMPILE
		GenereExcep (EXCEPTION_ERR_COMPILE, dwErreurCode);
		}
	}

//---------------------------------------------------------------
// Signale une exception User
// renvoie l'ordre de traiter le handler ou de continuer
int nSignaleExceptionUser (EXCEPTION_POINTERS * pExp)
	{
	int									nRes = EXCEPTION_CONTINUE_SEARCH; //EXCEPTION_EXECUTE_HANDLER;
	EXCEPTION_RECORD *	pExceptionRecord = pExp->ExceptionRecord;
	DWORD								ExceptionCode = pExceptionRecord->ExceptionCode;
	BOOL								bExceptionSysteme = TRUE;
	BOOL								bExceptionWarning = FALSE;

	// d�termine s'il s'agit d'une exception utilisateur
	switch (ExceptionCode)
		{
		case EXCEPTION_VERIF:
		case EXCEPTION_MEMMAN:
		case EXCEPTION_BLOCMAN:
		case EXCEPTION_BLKMAN:
		case EXCEPTION_SIGNALE:
		case EXCEPTION_ERR_COMPILE:
			bExceptionSysteme = FALSE;
			break;
		}

	// Exception utilisateur ?
	if (!bExceptionSysteme)
		{
		// oui => regarde si l'utilisateur doit �tre inform� ...
		BOOL	bAppelMessageBox = FALSE;
		PCSTR	pszTitreMessageBox = "Avertissement de l'application";
		UINT	uTypeMessageBox = MB_OK | MB_ICONHAND; // par d�faut pas de ::MessageBox
		BOOL	bContinuable = (pExceptionRecord->ExceptionFlags != EXCEPTION_NONCONTINUABLE);
		static BOOL bSignaleWarning = TRUE; // TRUE : on signale les warnings Verif

		// Exception continuable ?
		if (bContinuable)
			{
			// oui => Prise en compte d'un warning
			switch (ExceptionCode)
				{
				case EXCEPTION_VERIF:
					{
					bExceptionWarning = TRUE;

					if (bSignaleWarning)
						{
						// Avertissement de l'utilisateur et demande s'il doit signaler les autres erreurs
						uTypeMessageBox = MB_YESNOCANCEL | MB_ICONHAND;
						bAppelMessageBox = TRUE;
						}
					}
					break;
				case EXCEPTION_SIGNALE:
					{
					bExceptionWarning = TRUE;
					// Avertissement de l'utilisateur et demande s'il doit continuer
					uTypeMessageBox = MB_YESNO | MB_ICONHAND;
					bAppelMessageBox = TRUE;
					}
					break;
				} // switch (ExceptionCode)
			nRes = EXCEPTION_CONTINUE_EXECUTION;
			}
		else
			{
			// Avertissement de l'utilisateur :
			pszTitreMessageBox = "Fermeture de l'application sur erreur";

			bAppelMessageBox = TRUE;
			uTypeMessageBox = MB_OK | MB_ICONHAND;
			nRes = EXCEPTION_EXECUTE_HANDLER;
			}

		// ::MessageBox � signaler ?
		if (bAppelMessageBox)
			{
			// oui => s�rialisation des acc�s au ::MessageBox par un s�maphore :
			AccesSignale();

			// Entre temps on a pu sp�cifier "ne plus signaler les warnings"
			if (bExceptionWarning && (!bSignaleWarning))
				bAppelMessageBox = FALSE;

			if (bAppelMessageBox)
				{
				PCSTR	pszTexteMessageBox;

				// Appel et traitement de la message box
				// au moins un param�tre non null ?
				if ((pExceptionRecord->NumberParameters) && (pExceptionRecord->ExceptionInformation [0]))
					pszTexteMessageBox = (PCSTR)pExceptionRecord->ExceptionInformation [0];
				else
					pszTexteMessageBox = pszExcepCode (ExceptionCode);

				switch (nSignaleSansSerialisation (pszTitreMessageBox, pszTexteMessageBox, uTypeMessageBox))
					{
					case IDCANCEL:
						if (bExceptionWarning)
							bSignaleWarning = FALSE; 
						else
							nRes = EXCEPTION_EXECUTE_HANDLER;
						break;

					case IDNO:
						if (bExceptionWarning)
							{
							bSignaleWarning = FALSE; 
							nRes = EXCEPTION_EXECUTE_HANDLER;
							}
						break;
					}
				} // if (bAppelMessageBox)

			// Lib�re le s�maphore
			FinAccesSignale();
			}
		} // if (!bExceptionSysteme)

	return nRes;
	} // nSignaleExceptionUser


#include "stdafx.h"
#include "UStr.h"
#include "ULangues.h"
#include "Appli.h"
#include "Verif.h"
#include "USignale.h"
#include "LireLng.h"
VerifInit;

// Nom de la dll de ressource
static PCSTR szNomDllLng = "PcsLng.dll";

static HINSTANCE hInstDllLng = NULL;
  
// --------------------------------------------------------------------------
// Acc�s � une ressource
// --------------------------------------------------------------------------
static BOOL bLireTexte (DWORD dwNumero, PSTR pszBuf, DWORD dwTailleBuf, PCSTR pszErr)
	{
	// Erreur en lecture de la ressource ?
	int nRes = LoadString (hInstDllLng, dwNumero, pszBuf, dwTailleBuf);

	if (!nRes)
		{
		// oui => signale l'erreur
		char szErr[MAX_PATH+100];

		StrPrintFormat (szErr, pszErr, dwNumero);
		SignaleWarnExit (szErr);
		pszBuf	[0] = (char)0;
		}

  return nRes != 0;
	}

// --------------------------------------------------------------------------
// Initialiser le gestionnaire des donn�es traduites
// n�cessite la dll de ressources dans la langue courante de l'appli sur path exe
// --------------------------------------------------------------------------
BOOL bOuvrirLng (void)
  {
	// charge la DLL de ressources dans la langue courante de l'appli sur path exe
	if (!hInstDllLng)
		hInstDllLng = hChargeDLLLangue (szNomDllLng, Appli.szAbbrevLangueUtilisateur);
	return hInstDllLng != NULL;
  }

// --------------------------------------------------------------------------
// Fermer le gestionnaire des donn�es traduites
// --------------------------------------------------------------------------
void bFermerLng (void)
  {
	// D�charge la DLL contenant les ressources
	if (FreeLibrary (hInstDllLng))
		hInstDllLng = NULL;
  }

// --------------------------------------------------------------------------
// R�cup�rer une chaine dans la Dll de langue
// --------------------------------------------------------------------------
BOOL bLngLireString
	(
	DWORD dwNumero,	// Num�ro du texte demand�
	char * pszBufDest,		// Buffer pour recevoir le texte demand�
	DWORD dwTailleBuffDest // Nombre d'octets allou�s sur le buffer
	)								// Renvoie TRUE si pas d'erreur FALSE sinon (pszBuffer mis � "")
  {
	// Lecture de la ressource
  return bLireTexte (dwNumero, pszBufDest, c_nb_car_message_err, "Can't find ressource string # %i");
  } // bLngLireString 

// --------------------------------------------------------------------------
// R�cup�rer un texte d'erreur !!! c_nb_car_message_err cars max
// (ancien message_erreur)
// --------------------------------------------------------------------------
BOOL bLngLireErreur
	(
	DWORD dwNumero,	// Num�ro du texte demand�
	char * pszBuf		// Buffer pour recevoir le texte demand�
	)								// Renvoie TRUE si pas d'erreur FALSE sinon (pszBuffer mis � "")
  {
	// Lecture de la ressource
  return bLireTexte (dwNumero, pszBuf, c_nb_car_message_err, "Can't find Error message # %i");
  } // bLngLireErreur 

// --------------------------------------------------------------------------
// R�cup�rer un texte d'informations !!! c_nb_car_message_inf cars max
// (ancien message_information et message_commentaire)
// --------------------------------------------------------------------------
BOOL bLngLireInformation 
	(
	DWORD dwIdTexte,// Identifiant du texte demand�
	char * pszBuf		// Buffer pour recevoir le texte demand�
	)								// Renvoie TRUE si pas d'erreur FALSE sinon (pszBuffer mis � "")
  {
  // Lecture de la ressource
  return bLireTexte (dwIdTexte, pszBuf, c_nb_car_message_inf, "Can't find Information message # %i");
  } // bLngLireInformation



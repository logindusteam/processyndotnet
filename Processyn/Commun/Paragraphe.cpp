/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |									    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 |									    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/
// Paragraphe.cpp
// JS Win32 13/5/94
// Gestion commune des "titres de paragraphes" pour les interpr�teurs
// de Processyn Configuration

#include "stdafx.h"
#include "mem.h"
#include "lng_res.h"
#include "tipe.h"
#include "Descripteur.h"
#include "Verif.h"
#include "Paragraphe.h"
VerifInit;

//--------------------------------------------------------------------------
// renvoie vrai et la position dans le bloc des paragraf si la ligne se trouve
// dans l'intervalle delimitant un paragraf du type demand�
// pos_geo_debut pointe sur ligne declaration paragraf
// nbr_ligne du paragraphe
// un paragraf se termine sur un paragraf de meme type ou non ou un
// paragraf de profondeur inferieur ou egale
BOOL bLigneDansParagraphe
	(
	DWORD b_titre_paragraf, DWORD ligne,
	ID_PARAGRAPHE IdParagrapheCherche, REQUETE_EDIT	RequeteEdit,
	DWORD *pos_paragraf
	)
  {
  BOOL bTrouve = FALSE;
  DWORD nbr_enr = 0;

  if (existe_repere (b_titre_paragraf))
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf);

  (*pos_paragraf) = 0;

  while (((*pos_paragraf) < nbr_enr) && (!bTrouve))
    {
    (*pos_paragraf)++;
		PTITRE_PARAGRAPHE pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf, (*pos_paragraf));

    if ((pTitreParagraphe->IdParagraphe == IdParagrapheCherche))
      {
      switch (RequeteEdit)
        {
        case CONSULTE_LIGNE :
          bTrouve = ((ligne >= pTitreParagraphe->pos_geo_debut) &&
                   (ligne <= pTitreParagraphe->pos_geo_debut + pTitreParagraphe->nbr_ligne));
          break;

        case INSERE_LIGNE :
          bTrouve = ((ligne > pTitreParagraphe->pos_geo_debut) &&
                    (ligne <= pTitreParagraphe->pos_geo_debut + pTitreParagraphe->nbr_ligne + 1));
          break;
        }
      } // trouve tipe paragraf
    } // while
  return bTrouve;
  } // bLigneDansParagraphe

//--------------------------------------------------------------------------
void MajDonneesParagraphe
	(
	DWORD b_titre_paragraf, DWORD ligne,
	REQUETE_EDIT RequeteEdit, BOOL un_paragraphe,
	DWORD profondeur, DWORD pos_paragraf_insere
	)
  {
  DWORD nbr_enr = 0;
  DWORD i = 0;

  if (existe_repere (b_titre_paragraf))
    nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf);

  while (i < nbr_enr)
    {
    i++;
		PTITRE_PARAGRAPHE pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf, i);

    switch (RequeteEdit)
      {
      case INSERE_LIGNE :
        if (i != pos_paragraf_insere)
          {
          if (ligne <= pTitreParagraphe->pos_geo_debut)
            {
            (pTitreParagraphe->pos_geo_debut)++;
            }
          else
            {
            if (ligne <= pTitreParagraphe->pos_geo_debut + pTitreParagraphe->nbr_ligne + 1)
              {
              if ((!un_paragraphe)  ||
                ((un_paragraphe) && (profondeur > pTitreParagraphe->profondeur)))
                {
                pTitreParagraphe->nbr_ligne++;
                }
              }
            }
          }
        break;

      case SUPPRIME_LIGNE :
        if (ligne <= pTitreParagraphe->pos_geo_debut)
          {
          pTitreParagraphe->pos_geo_debut--;
          }
        else
          {
          if (ligne <= pTitreParagraphe->pos_geo_debut + pTitreParagraphe->nbr_ligne)
            {
            pTitreParagraphe->nbr_ligne--;
            }
          }
        break;
      } // switch
    } // while
  } // MajDonneesParagraphe

//--------------------------------------------------------------------------
BOOL bValideInsereLigneParagraphe (DWORD b_titre_paragraf, DWORD ligne,
	DWORD profondeur, DWORD nbr_imbrication, DWORD *pdwErreur)
  {
  DWORD degre_imbrication = 0;

  for (ID_PARAGRAPHE IdParagraphe = premier_tipe_paragraf; (IdParagraphe <= dernier_tipe_paragraf); IdParagraphe = (ID_PARAGRAPHE)((int)IdParagraphe+1))
    {
		DWORD pos_paragraf;

    if (bLigneDansParagraphe (b_titre_paragraf, ligne, IdParagraphe, INSERE_LIGNE, &pos_paragraf))
      {
			PTITRE_PARAGRAPHE pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf, pos_paragraf);
      if (pTitreParagraphe->profondeur == profondeur)
        {
        if (ligne != (pTitreParagraphe->pos_geo_debut + pTitreParagraphe->nbr_ligne + 1))
          {
          (*pdwErreur) = 63;
          return (FALSE);   // cochon
          }
        } // meme profondeur
      else
        {
        if (pTitreParagraphe->profondeur < profondeur)
          {
          degre_imbrication++;
          }
        }
      } // dans paragraf
    } // for
  if (degre_imbrication != nbr_imbrication)
    {
    (*pdwErreur) = 63;
    return FALSE;
    }
  else
    {
    (*pdwErreur) = 0;
    return TRUE;
    }
  }

//--------------------------------------------------------------------------
void InsereTitreParagraphe (DWORD b_titre_paragraf, DWORD ligne,
	DWORD profondeur, ID_PARAGRAPHE IdParagraphe,
	DWORD *pos_paragraf)
  {
  PTITRE_PARAGRAPHE pTitreParagraphe;

  if (!existe_repere (b_titre_paragraf))
    {
    cree_bloc (b_titre_paragraf, sizeof (TITRE_PARAGRAPHE), 0);
    (*pos_paragraf) = 1;
    }
  else
    {
    (*pos_paragraf) = nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf) + 1;
    }
  insere_enr (szVERIFSource, __LINE__, 1, b_titre_paragraf, (*pos_paragraf));
  pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf, (*pos_paragraf));
  pTitreParagraphe->pos_geo_debut = ligne;
  pTitreParagraphe->IdParagraphe = IdParagraphe;
  pTitreParagraphe->nbr_ligne = 0;
  pTitreParagraphe->profondeur = profondeur;
  } // InsereTitreParagraphe

//--------------------------------------------------------------------------
BOOL bSupprimeTitreParagraphe (DWORD b_titre_paragraf, DWORD pos_a_supprimer)
  {
	BOOL bRet = FALSE;

  PTITRE_PARAGRAPHE pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf, pos_a_supprimer);

  if (pTitreParagraphe->nbr_ligne == 0)
    {
    enleve_enr (szVERIFSource, __LINE__, 1, b_titre_paragraf, pos_a_supprimer);
    bRet = TRUE;
    }
	return bRet;
  } // bSupprimeTitreParagraphe

//----------------------------------------------------------------------------
//// renvoie vrai si ligne presente et et la position dans le bloc des paragraf de chaque niveau dont depend la ligne
// dans TITRE_PARAGRAPHE, pos_geo_debut pointe sur ligne declaration paragraf
// nbr_ligne du paragraphe
// un paragraf se termine sur un paragraf de meme type ou non ou un
// paragraf de profondeur inferieur ou egale
BOOL bDonneContexteParagrapheLigne
	(
	DWORD b_titre_paragraf, DWORD ligne,
	DWORD * pdwProfondeurLigne,
	PCONTEXTE_PARAGRAPHE_LIGNE aContexteParagrapheLigne
	) 
  {
  BOOL bTrouve = FALSE;

  // bloc des paragraphes existe ?
  if (existe_repere (b_titre_paragraf))
    {
		// oui => scrute les paragraphes � la recherche du 
		DWORD nbr_enr = nb_enregistrements (szVERIFSource, __LINE__, b_titre_paragraf);
		DWORD pos_paragraf = 1;
    (*pdwProfondeurLigne) = 0;
    while (pos_paragraf <= nbr_enr)
      {
			PTITRE_PARAGRAPHE pTitreParagraphe = (PTITRE_PARAGRAPHE)pointe_enr (szVERIFSource, __LINE__, b_titre_paragraf, pos_paragraf);

      if ((ligne >= pTitreParagraphe->pos_geo_debut) &&
          (ligne <= pTitreParagraphe->pos_geo_debut + pTitreParagraphe->nbr_ligne))
        {
        (*pdwProfondeurLigne) = pTitreParagraphe->profondeur;
        aContexteParagrapheLigne [(*pdwProfondeurLigne)-1] .position = pos_paragraf;
        aContexteParagrapheLigne [(*pdwProfondeurLigne)-1] .IdParagraphe = pTitreParagraphe->IdParagraphe;
        bTrouve = TRUE;
        }
      pos_paragraf++;
      }
    }
  return bTrouve;
  } // bDonneContexteParagrapheLigne


//------------------------------- fin Paragraphe.cpp -------------------------



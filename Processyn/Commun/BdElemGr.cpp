//-------------------------------------------------------------------------
// BdElemGr.h (extrait de Bdgr)
// Gestion des �l�ments graphiques de la base de donn�e Processyn
//-------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"        // std types
#include "MemMan.h"

#include "mem.h"        // mem
#include "UStr.h"     //
#include "G_Objets.h"
#include "g_sys.h"      // g_xxx()
#include "pcsspace.h"   // Coordonn�es Min et MAX de l'espace
#include "dicho.h"      // Tri dicho pour espacement
#include "lng_res.h"
#include "Tipe.h"			// types de proc�dures du descripteur
#include "Descripteur.h"

#include "bdgr.h"			// PBDGR
#include "bdanmgr.h"   // verbes maj inter_vi
#include "BdPageGr.h"
#include "BdContGr.h"
#include "Verif.h"		// verbes maj inter_vi

#include "IdAutoPcsMan.h"
#include "BdElemGr.h"
VerifInit;

// Instanciation d'une constante
const FORCAGE_ELEMENT FORCAGE_NULL = 
	{
	G_COULEUR_INDETERMINEE,	//nCouleurLigne         
	G_COULEUR_INDETERMINEE,	//nCouleurRemplissage         
	G_STYLE_LIGNE_INDETERMINE,	//StyleLigne      
	G_STYLE_REMPLISSAGE_INDETERMINE,	//StyleRemplissage
	0,					//xOffset         
	0						//yOffset         
	};


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Gestion des fen�tres associ�es � certains �l�ments graphiques
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//-------------------------------------------------------------
// Cr�ation si ad�quat de la fen�tres d'un �l�ment graphique
// Renvoie TRUE si Ok - FALSE si erreur de cr�ation de fen�tre
BOOL bCreerFenetreElementGr (DWORD Repere, DWORD wPositionElement, HBDGR hbdgr, DWORD	dwIdFenetre)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
	BOOL			bRes = FALSE;

	// La page existe ?
  if (pPage)
	  {
		PBDGR			pBdGr = (PBDGR)hbdgr;
		// oui => acc�s � l'�l�ment
		PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

		bRes = TRUE;
		switch (pElement->nAction)
			{
			case G_ACTION_CONT_GROUP:
			case G_ACTION_CONT_BOUTON:
			case G_ACTION_CONT_CHECK:
			case G_ACTION_CONT_RADIO:
			case G_ACTION_CONT_BOUTON_VAL:
				{			
				HELEMENT				hElmtGsys = pBdGr->helementTravail;
				HWND			hwnd;
				PGEO_TEXTES_GR	pGeoTxt;
				PTEXTES_GR			pTxt;

				GElementSetAction (hElmtGsys, pElement->nAction);
				GElementSetCouleurs (hElmtGsys, pElement->nCouleurLigne, pElement->nCouleurRemplissage);
				GElementSetStyleRemplissage (hElmtGsys, pElement->nStyleRemplissage);

				pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne); // utilis� 
				pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
				GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
				GElementSetTexte (hElmtGsys, pTxt->Texte);
				GElementResetBiPoint  (pBdGr->hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

				hwnd = hwndCreeContGR (g_get_hwnd (pBdGr->hGsys), pBdGr->hGsys, hElmtGsys, dwIdFenetre, pBdGr->bPageEnEdition);
				if (hwnd)
					pElement->hwndControle = hwnd;
				else
					bRes = FALSE;
				}
				break;

			case G_ACTION_CONT_STATIC:
			case G_ACTION_CONT_EDIT:
			case G_ACTION_CONT_COMBO:
			case G_ACTION_CONT_LIST:
			case G_ACTION_CONT_SCROLL_H:
			case G_ACTION_CONT_SCROLL_V:
				{
				HELEMENT	hElmtGsys = pBdGr->helementTravail;
				HWND			hwnd;

				GElementSetAction (hElmtGsys, pElement->nAction);
				GElementSetCouleurs (hElmtGsys, pElement->nCouleurLigne, pElement->nCouleurRemplissage);
				GElementSetStyles (hElmtGsys, pElement->nStyleLigne, pElement->nStyleRemplissage);
				GElementResetBiPoint  (pBdGr->hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);
				hwnd = hwndCreeContGR (g_get_hwnd (pBdGr->hGsys), pBdGr->hGsys, hElmtGsys, dwIdFenetre, pBdGr->bPageEnEdition);
				if (hwnd)
					pElement->hwndControle = hwnd;
				else
					bRes = FALSE;
				}
				break;
			}
    }
	return bRes;
	}

//-------------------------------------------------------------
// Cr�ation des fen�tres de tous les �l�ments graphiques d'une page
// Renvoie TRUE si Ok - FALSE si au moins une erreur de cr�ation de fen�tre
BOOL bCreerFenetresElementGr (HBDGR hbdgr, DWORD dwPremierId)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
	BOOL			bRes = FALSE;

  if (pPage)
    {
		PBDGR			pBdGr = (PBDGR)hbdgr;
		HELEMENT	hElmtGsys = pBdGr->helementTravail;
		DWORD  wPremierElement = pPage->PremierElement;
		DWORD  wDernierElement = pPage->NbElements + wPremierElement;
		DWORD  wPositionElement;

		bRes = TRUE;
    for (wPositionElement = wPremierElement; wPositionElement < wDernierElement; wPositionElement++)
      {
			if (!bCreerFenetreElementGr (B_ELEMENTS_PAGES_GR, wPositionElement, hbdgr, dwPremierId + wPositionElement - wPremierElement))
				bRes = FALSE;
      }
    }
	return bRes;
	} // bCreerFenetresElementGr

//-------------------------------------------------------------
// Fermeture si ad�quat de la fen�tres d'un �l�ment graphique
// Renvoie TRUE si Ok - FALSE si erreur de fermeture de fen�tre
BOOL bFermerFenetreElementGr (DWORD Repere, DWORD wPositionElement)
  {
	// acc�s � l'�l�ment
	PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, wPositionElement);
	BOOL			bRes = TRUE;

	// Fen�tre associ�e � l'objet ?
	if (bAttributsActionGr (pElement->nAction, G_ATTRIBUT_CONTROLE))
		{
		// oui => handle fen�tre ouverte ?
		if (pElement->hwndControle)
			{
			// oui => ferme la
			if (::DestroyWindow (pElement->hwndControle))
				pElement->hwndControle = NULL;
			else
				bRes = FALSE;
			}
		}

	return bRes;
	} // bFermeFenetreElementGr 

//-------------------------------------------------------------
// Fermeture des fen�tres de tous les �l�ments graphiques d'une page
// Renvoie TRUE si Ok - FALSE si au moins une erreur de fermeture de fen�tre
BOOL bFermerFenetresElementGr (HBDGR hbdgr)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
	BOOL			bRes = TRUE; // On consid�re normal que la page n'existe pas

  if (pPage)
    {
		PBDGR			pBdGr = (PBDGR)hbdgr;
		HELEMENT	hElmtGsys = pBdGr->helementTravail;
		DWORD  wPremierElement = pPage->PremierElement;
		DWORD  wDernierElement = pPage->NbElements + wPremierElement;
		DWORD  wPositionElement;

    for (wPositionElement = wPremierElement; wPositionElement < wDernierElement; wPositionElement++)
      {
			if (!bFermerFenetreElementGr (B_ELEMENTS_PAGES_GR, wPositionElement))
				bRes = FALSE;
      }
    }
	return bRes;
	}

// -------------------------------------------------------------------------
// Ajustement de la fen�tre d'un contr�le aux coordonn�es courantes d'un �l�ment graphique
// -------------------------------------------------------------------------
void ChangePosFenetreElementGR (DWORD Repere, DWORD Position, PBDGR pBdGr)
  {
  PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);

	// l'�l�ment a une fen�tre ?
	if (bAttributsActionGr (pElement->nAction, G_ATTRIBUT_CONTROLE))
		{
		// oui => ajustement de la position de la fen�tre controle child si elle existe
		if (pElement->hwndControle)
			bChangePosContGR (pElement->hwndControle, pBdGr->hGsys, pElement->pt0, pElement->pt1);
		}
  } // ChangePosFenetreElementGR


/// -------------------------------------------------------------------------
// Change l'ID de la fen�tre d'un contr�le
// -------------------------------------------------------------------------
void ChangeIdFenetreElementGR (HBDGR hbdgr, DWORD PositionDansPage, DWORD dwNouvelID)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((PositionDansPage > 0) && (PositionDansPage <= pPage->NbElements))
      {
			DWORD	wPositionElement = pPage->PremierElement + PositionDansPage - 1;
			PELEMENT_PAGE_GR	pElement = pElementPage (wPositionElement);

			// l'�l�ment a une fen�tre ?
			if (pElement && bAttributsActionGr (pElement->nAction, G_ATTRIBUT_CONTROLE))
				{
				// oui => ajustement de la position de la fen�tre controle child si elle existe
				bChangeIdContGR (pElement->hwndControle, dwNouvelID);
				}
			}
		}
  } // ChangePosFenetreElementGR

/// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// R�cup�re le hwnd d'un �l�ment graphique ou NULL
HWND hwndElementGR (HBDGR hbdgr, DWORD PositionDansPage)
  {
	HWND			hwndRes = NULL;
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((PositionDansPage > 0) && (PositionDansPage <= pPage->NbElements))
      {
			DWORD	wPositionElement = pPage->PremierElement + PositionDansPage - 1;
			PELEMENT_PAGE_GR	pElement = pElementPage (wPositionElement);

			// l'�l�ment a une fen�tre ?
			if (pElement && bAttributsActionGr (pElement->nAction, G_ATTRIBUT_CONTROLE))
				{
				// oui => ajustement de la position de la fen�tre controle child si elle existe
				hwndRes = pElement->hwndControle;
				}
			}
		}
	return hwndRes;
  } // ChangePosFenetreElementGR

// -------------------------------------------------------------------------
// Dessin d'un �l�ment graphique
// -------------------------------------------------------------------------
void DessinerElementGR 
	(
	DWORD Repere, DWORD Position, const FORCAGE_ELEMENT * pForcage, PBDGR pBdGr,
	HELEMENT hElmtGsys // Le contenu n'est pas significatif en sortie
	)
  {
  PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);

  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE      :
    case G_ACTION_RECTANGLE :
    case G_ACTION_CERCLE    :
    case G_ACTION_QUART_ARC     :
    case G_ACTION_QUART_CERCLE:
    case G_ACTION_H_DEMI_CERCLE   :
    case G_ACTION_V_DEMI_CERCLE   :
    case G_ACTION_TRI_RECT  :
    case G_ACTION_H_TRI_ISO :
    case G_ACTION_V_TRI_ISO :
    case G_ACTION_H_VANNE   :
    case G_ACTION_V_VANNE   :
    case G_ACTION_COURBE_T   :
    case G_ACTION_COURBE_C   :
    case G_ACTION_COURBE_A   :
    case G_ACTION_ENTREE_LOG:
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_BARGRAPHE        :
			{
			LONG	OldOf7X0;
			LONG  OldOf7Y0;
			G_COULEUR						nCouleurLigne = pForcage->nCouleurLigne;
			G_COULEUR						nCouleurRemplissage = pForcage->nCouleurRemplissage;
			G_STYLE_LIGNE				nStyleLigne = pForcage->nStyleLigne;
			G_STYLE_REMPLISSAGE nStyleRemplissage = pForcage->nStyleRemplissage;

			// calcule les attributs compte tenu du for�age
			if (nCouleurLigne == G_COULEUR_INDETERMINEE)
				nCouleurLigne = pElement->nCouleurLigne;

			if (nCouleurRemplissage == G_COULEUR_INDETERMINEE)
				nCouleurRemplissage = pElement->nCouleurRemplissage;
			
			if (nStyleLigne == G_STYLE_LIGNE_INDETERMINE)
				nStyleLigne = pElement->nStyleLigne;

			if (nStyleRemplissage == G_STYLE_REMPLISSAGE_INDETERMINE)
				nStyleRemplissage = pElement->nStyleRemplissage;

			// cr�e un objet graphique r�plique
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSetCouleurs (hElmtGsys, nCouleurLigne, nCouleurRemplissage);
			GElementSetStyles (hElmtGsys, nStyleLigne, nStyleRemplissage);
			GElementResetBiPoint  (pBdGr->hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

			// Dessine d�cal� $$ Une seule instance de l'origine => si r�entrance en dessin => erreur
			g_deplace_origine (pBdGr->hGsys, pForcage->xOffset, pForcage->yOffset, &OldOf7X0, &OldOf7Y0);
			g_element_draw (pBdGr->hGsys, hElmtGsys);
			g_set_origine (pBdGr->hGsys, OldOf7X0, OldOf7Y0);
			}
			break;

    case G_ACTION_H_TEXTE:
    case G_ACTION_V_TEXTE:
    case G_ACTION_EDIT_TEXTE:
    case G_ACTION_STATIC_TEXTE:
    case G_ACTION_EDIT_NUM:
    case G_ACTION_STATIC_NUM:
			{			
			PGEO_TEXTES_GR			pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
			PTEXTES_GR					pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
			G_COULEUR						nCouleurLigne = pForcage->nCouleurLigne;
			LONG                OldOf7X0;
			LONG                OldOf7Y0;

			// calcule les attributs compte tenu du for�age
			if (nCouleurLigne == G_COULEUR_INDETERMINEE)
				nCouleurLigne = pElement->nCouleurLigne;

			// cr�e un objet graphique r�plique
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSetCouleurs (hElmtGsys, nCouleurLigne, G_COULEUR_INDETERMINEE); // $$ � v�rifier
			GElementSetStyles (hElmtGsys, G_STYLE_LIGNE_INDETERMINE, G_STYLE_REMPLISSAGE_INDETERMINE); // $$ � v�rifier

			GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
			GElementSetTexte (hElmtGsys, pTxt->Texte);

			GElementResetBiPoint  (pBdGr->hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

			// Dessine d�cal� $$ Une seule instance de l'origine => si r�entrance en dessin => erreur
			g_deplace_origine (pBdGr->hGsys, pForcage->xOffset, pForcage->yOffset, &OldOf7X0, &OldOf7Y0);
			g_element_draw (pBdGr->hGsys, hElmtGsys);
			g_set_origine (pBdGr->hGsys, OldOf7X0, OldOf7Y0);
			}
			break;

    case G_ACTION_POLYGONE:
    case G_ACTION_POLYGONE_PLEIN:
			{
			PGEO_POINTS_POLY_GR	pGeoPointPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, pElement->pt1.y);
			PPOINTS_POLY_GR			pPointPoly;
			DWORD               wIndexPointPoly;
			LONG                OldOf7X0;
			LONG                OldOf7Y0;
			LONG                OldBidonX;
			LONG                OldBidonY;
			G_COULEUR						nCouleurLigne = pForcage->nCouleurLigne;
			G_COULEUR						nCouleurRemplissage = pForcage->nCouleurRemplissage;
			G_STYLE_LIGNE				nStyleLigne = pForcage->nStyleLigne;
			G_STYLE_REMPLISSAGE nStyleRemplissage = pForcage->nStyleRemplissage;

			// calcule les attributs compte tenu du for�age
			if (nCouleurLigne == G_COULEUR_INDETERMINEE)
				nCouleurLigne = pElement->nCouleurLigne;

			if (nCouleurRemplissage == G_COULEUR_INDETERMINEE)
				nCouleurRemplissage = pElement->nCouleurRemplissage;
			
			if (nStyleLigne == G_STYLE_LIGNE_INDETERMINE)
				nStyleLigne = pElement->nStyleLigne;

			if (nStyleRemplissage == G_STYLE_REMPLISSAGE_INDETERMINE)
				nStyleRemplissage = pElement->nStyleRemplissage;

			// cr�e un objet graphique r�plique
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSetCouleurs (hElmtGsys, nCouleurLigne, nCouleurRemplissage);
			GElementSetStyles (hElmtGsys, nStyleLigne, nStyleRemplissage);
			GElementSupprimePoints (hElmtGsys);
			for (wIndexPointPoly = 0; wIndexPointPoly < pGeoPointPoly->NbPoints; wIndexPointPoly++)
				{
				pPointPoly = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, pGeoPointPoly->PremierPoint + wIndexPointPoly);
				GElementAjoutePoint  (pBdGr->hGsys, hElmtGsys, pPointPoly->x, pPointPoly->y);
				}

			// Dessine d�cal� $$ Une seule instance de l'origine => si r�entrance en dessin => erreur
			g_deplace_origine (pBdGr->hGsys, pForcage->xOffset, pForcage->yOffset, &OldOf7X0, &OldOf7Y0);
			g_deplace_origine (pBdGr->hGsys, pElement->pt0.x, pElement->pt0.y, &OldBidonX, &OldBidonY);
			g_element_draw (pBdGr->hGsys, hElmtGsys);
			g_set_origine (pBdGr->hGsys, OldOf7X0, OldOf7Y0);
			}
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			{
		  FORCAGE_ELEMENT ForcageListe = (*pForcage);
			PGEO_ELEMENTS_LISTES_GR	pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
			DWORD                   wPremierElmtListe = pGeoListe->PremierElement;
			DWORD                   wDernierElmtListe = wPremierElmtListe + pGeoListe->NbElements;
			DWORD                   wElmtListe;
			
			// Le for�age courant a priorit� sur le for�age de cette liste
			if (ForcageListe.nCouleurLigne == G_COULEUR_INDETERMINEE)
				{
				if (pElement->nCouleurLigne != G_COULEUR_INDETERMINEE)
					ForcageListe.nCouleurLigne = pElement->nCouleurLigne;
				}

			if (ForcageListe.nCouleurRemplissage == G_COULEUR_INDETERMINEE)
				{
				if (pElement->nCouleurRemplissage != G_COULEUR_INDETERMINEE)
					ForcageListe.nCouleurRemplissage = pElement->nCouleurRemplissage;
				}

			if (ForcageListe.nStyleLigne == G_STYLE_LIGNE_INDETERMINE)
				{
				if (pElement->nStyleLigne != G_STYLE_LIGNE_INDETERMINE)
					ForcageListe.nStyleLigne = pElement->nStyleLigne;
				}

			if (ForcageListe.nStyleRemplissage == G_STYLE_REMPLISSAGE_INDETERMINE)
				{
				if (pElement->nStyleRemplissage != G_STYLE_REMPLISSAGE_INDETERMINE)
					ForcageListe.nStyleRemplissage = pElement->nStyleRemplissage;
				}

			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);
			GElementAjoutePoint  (pBdGr->hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			g_element_move_delta (pBdGr->hGsys, hElmtGsys, ForcageListe.xOffset, ForcageListe.yOffset);
			g_element_get_point (pBdGr->hGsys, hElmtGsys, 0, &ForcageListe.xOffset, &ForcageListe.yOffset);

			// Dessin des �l�ments de la liste avec le nouveau for�age
			for (wElmtListe = wPremierElmtListe; wElmtListe < wDernierElmtListe; wElmtListe++)
				{
				DessinerElementGR (B_ELEMENTS_LISTES_GR, wElmtListe, &ForcageListe, pBdGr, hElmtGsys);
				}
			}
			break;

		//case G_ACTION_NOP:break;
    // default:	break;
    } // switch (pElement->Action)
  } // DessinerElementGR

// -------------------------------------------------------------------------
// Invalidate d'un �l�ment graphique
// -------------------------------------------------------------------------
void GommerElementGR (DWORD Repere, DWORD Position, const FORCAGE_ELEMENT *pForcage, PBDGR pBdGr, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);;
  PGEO_TEXTES_GR		pGeoTxt;
  PTEXTES_GR				pTxt;
  PGEO_POINTS_POLY_GR	pGeoPointPoly;
  PPOINTS_POLY_GR		pPointPoly;
  DWORD                      wIndexPointPoly;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD                      wPremierElmtListe;
  DWORD                      wDernierElmtListe;
  DWORD                      wElmtListe;
  //
  LONG                       OldOf7X0;
  LONG                       OldOf7Y0;
  LONG                       OldBidonX;
  LONG                       OldBidonY;

  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE      :
    case G_ACTION_RECTANGLE :
    case G_ACTION_CERCLE    :
    case G_ACTION_QUART_ARC     :
    case G_ACTION_QUART_CERCLE:
    case G_ACTION_H_DEMI_CERCLE   :
    case G_ACTION_V_DEMI_CERCLE   :
    case G_ACTION_TRI_RECT  :
    case G_ACTION_H_TRI_ISO :
    case G_ACTION_V_TRI_ISO :
    case G_ACTION_H_VANNE   :
    case G_ACTION_V_VANNE   :
    case G_ACTION_COURBE_T   :
    case G_ACTION_COURBE_C   :
    case G_ACTION_COURBE_A   :
    case G_ACTION_ENTREE_LOG:
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_BARGRAPHE:
			//
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint  (pBdGr->hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);
			// invalide l'�l�ment avec l'offset courant
			g_deplace_origine (pBdGr->hGsys, pForcage->xOffset, pForcage->yOffset, &OldOf7X0, &OldOf7Y0);
			g_elementInvalidateRect (pBdGr->hGsys, hElmtGsys);
			g_set_origine (pBdGr->hGsys, OldOf7X0, OldOf7Y0);
			//
			break;

    case G_ACTION_H_TEXTE       :
    case G_ACTION_V_TEXTE       :
    case G_ACTION_EDIT_TEXTE  :
    case G_ACTION_STATIC_TEXTE :
    case G_ACTION_EDIT_NUM  :
    case G_ACTION_STATIC_NUM :
			GElementSetAction (hElmtGsys, pElement->nAction);
			pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
			pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
			GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
			GElementSetTexte (hElmtGsys, pTxt->Texte);
			//
			GElementResetBiPoint  (pBdGr->hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);
			g_deplace_origine (pBdGr->hGsys, pForcage->xOffset, pForcage->yOffset, &OldOf7X0, &OldOf7Y0);
			g_elementInvalidateRect (pBdGr->hGsys, hElmtGsys);
			g_set_origine (pBdGr->hGsys, OldOf7X0, OldOf7Y0);
			break;

    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);
			pGeoPointPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, pElement->pt1.y);
			for (wIndexPointPoly = 0; wIndexPointPoly < pGeoPointPoly->NbPoints; wIndexPointPoly++)
				{
				pPointPoly = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, pGeoPointPoly->PremierPoint + wIndexPointPoly);
				GElementAjoutePoint  (pBdGr->hGsys, hElmtGsys, pPointPoly->x, pPointPoly->y);
				}

			g_deplace_origine (pBdGr->hGsys, pForcage->xOffset, pForcage->yOffset, &OldOf7X0, &OldOf7Y0);
			g_deplace_origine (pBdGr->hGsys, pElement->pt0.x, pElement->pt0.y, &OldBidonX, &OldBidonY);
			g_elementInvalidateRect (pBdGr->hGsys, hElmtGsys);
			g_set_origine (pBdGr->hGsys, OldOf7X0, OldOf7Y0);
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			{
		  FORCAGE_ELEMENT ForcageListe = (*pForcage);

			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);
			GElementAjoutePoint  (pBdGr->hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			g_element_move_delta (pBdGr->hGsys, hElmtGsys, ForcageListe.xOffset, ForcageListe.yOffset);
			g_element_get_point (pBdGr->hGsys, hElmtGsys, 0, &ForcageListe.xOffset, &ForcageListe.yOffset);
			//
			pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
			wPremierElmtListe = pGeoListe->PremierElement;
			wDernierElmtListe = wPremierElmtListe + pGeoListe->NbElements;
			for (wElmtListe = wPremierElmtListe; wElmtListe < wDernierElmtListe; wElmtListe++)
				{
				GommerElementGR (B_ELEMENTS_LISTES_GR, wElmtListe, &ForcageListe, pBdGr, hElmtGsys);
				}
			}
			break;

    //case G_ACTION_NOP: break;
    //default: break;
    } // switch (pElement->Action)
  } // GommerElementGR

// -------------------------------------------------------------------------
// D�place � l'�cran un �l�ment graphique
// Appel� uniquement par PcsExe
void bdgr_offset_element (HBDGR hbdgr, DWORD ModeTravail, DWORD dwPosElementDansPage, LONG x1Ref, LONG y1Ref, LONG x2Ref, LONG y2Ref, LONG Dx, LONG Dy)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((dwPosElementDansPage > 0) && (dwPosElementDansPage <= pPage->NbElements))
      {
			PBDGR			pBdGr = (PBDGR)hbdgr;
			DWORD							wPositionElement = pPage->PremierElement + dwPosElementDansPage - 1;
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
			HGSYS							hGsys = pBdGr->hGsys;
			HELEMENT					hElmtGsys = pBdGr->helementTravail;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);

			// D�placer l'�l�ment de travail
			switch (pElement->nAction)
				{
				case G_ACTION_LIGNE           :
				case G_ACTION_RECTANGLE      :
				case G_ACTION_CERCLE         :
				case G_ACTION_QUART_ARC          :
				case G_ACTION_QUART_CERCLE     :
				case G_ACTION_H_DEMI_CERCLE        :
				case G_ACTION_V_DEMI_CERCLE        :
				case G_ACTION_TRI_RECT       :
				case G_ACTION_H_TRI_ISO      :
				case G_ACTION_V_TRI_ISO      :
				case G_ACTION_H_VANNE        :
				case G_ACTION_V_VANNE        :
				case G_ACTION_RECTANGLE_PLEIN :
				case G_ACTION_CERCLE_PLEIN    :
				case G_ACTION_QUART_ARC_PLEIN     :
				case G_ACTION_QUART_CERCLE_PLEIN:
				case G_ACTION_H_DEMI_CERCLE_PLEIN   :
				case G_ACTION_V_DEMI_CERCLE_PLEIN   :
				case G_ACTION_TRI_RECT_PLEIN  :
				case G_ACTION_H_TRI_ISO_PLEIN :
				case G_ACTION_V_TRI_ISO_PLEIN :
				case G_ACTION_H_VANNE_PLEIN   :
				case G_ACTION_V_VANNE_PLEIN   :
				case G_ACTION_COURBE_T        :
				case G_ACTION_COURBE_C        :
				case G_ACTION_COURBE_A        :
				case G_ACTION_BARGRAPHE        :
				case G_ACTION_EDIT_TEXTE  :
				case G_ACTION_STATIC_TEXTE  :
				case G_ACTION_EDIT_NUM  :
				case G_ACTION_STATIC_NUM  :
				case G_ACTION_ENTREE_LOG    :

				case G_ACTION_CONT_STATIC:
				case G_ACTION_CONT_EDIT:
				case G_ACTION_CONT_GROUP:
				case G_ACTION_CONT_BOUTON:
				case G_ACTION_CONT_CHECK:
				case G_ACTION_CONT_RADIO:
				case G_ACTION_CONT_COMBO:
				case G_ACTION_CONT_LIST:
				case G_ACTION_CONT_SCROLL_H:
				case G_ACTION_CONT_SCROLL_V:
				case G_ACTION_CONT_BOUTON_VAL:
					{
					POINT	point;

					GElementSetAction (hElmtGsys, pElement->nAction);
					GElementSupprimePoints (hElmtGsys);

					GElementAjoutePoint  (hGsys, hElmtGsys, x1Ref, y1Ref);
					GElementDeplaceDernierPoint (hGsys, hElmtGsys, Dx, Dy);
					g_element_get_point (hGsys, hElmtGsys, 0, &point.x, &point.y);
					pElement->pt0 = point;

					GElementAjoutePoint  (hGsys, hElmtGsys, x2Ref, y2Ref);
					GElementDeplaceDernierPoint (hGsys, hElmtGsys, Dx, Dy);
					g_element_get_point (hGsys, hElmtGsys, 1, &point.x, &point.y);
					pElement->pt1 = point;
					}
					break;
				case G_ACTION_H_TEXTE:
				case G_ACTION_V_TEXTE:
					{
					POINT	point;

					GElementSetAction (hElmtGsys, pElement->nAction);
					GElementSupprimePoints (hElmtGsys);

					GElementAjoutePoint  (hGsys, hElmtGsys, x1Ref, y1Ref);
					GElementDeplaceDernierPoint (hGsys, hElmtGsys, Dx, Dy);
					g_element_get_point (hGsys, hElmtGsys, 0, &point.x, &point.y);
					pElement->pt0 = point;

					GElementAjoutePoint  (hGsys, hElmtGsys, x2Ref, y2Ref);
					g_element_get_point (hGsys, hElmtGsys, 1, &point.x, &point.y);
					pElement->pt1 = point;
					}
					break;

				case G_ACTION_POLYGONE:
				case G_ACTION_POLYGONE_PLEIN:
				case G_ACTION_LISTE:
				case G_ACTION_LISTE_PLEIN:
					{
					POINT	point;

					GElementSetAction (hElmtGsys, pElement->nAction);
					GElementSupprimePoints (hElmtGsys);

					GElementAjoutePoint  (hGsys, hElmtGsys, x1Ref, y1Ref);
					g_element_move_delta (hGsys, hElmtGsys, Dx, Dy);
					g_element_get_point (hGsys, hElmtGsys, 0, &point.x, &point.y);
					pElement->pt0 = point;
					}
					break;
				//case G_ACTION_NOP:default: break;
				}

			// Invalide la nouvelle position pour provoquer le redessin
      if (ModeTravail & MODE_GOMME_ELEMENT)
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);


      if (ModeTravail & MODE_DESSIN_ELEMENT)
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Travail sur les Elements
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Modifications Element GR
// -------------------------------------------------------------------------

// -------------------------------------------------------------------------
// Forcer Element
// -------------------------------------------------------------------------
void ForcerElementGR (DWORD Repere, DWORD Position, G_COULEUR nCouleurLigne, G_COULEUR nCouleurRemplissage, 
											G_STYLE_LIGNE StyleLigne, G_STYLE_REMPLISSAGE StyleRemplissage)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE      :
    case G_ACTION_RECTANGLE :
    case G_ACTION_CERCLE    :
    case G_ACTION_QUART_ARC     :
    case G_ACTION_QUART_CERCLE:
    case G_ACTION_H_DEMI_CERCLE   :
    case G_ACTION_V_DEMI_CERCLE   :
    case G_ACTION_TRI_RECT  :
    case G_ACTION_H_TRI_ISO :
    case G_ACTION_V_TRI_ISO :
    case G_ACTION_H_VANNE   :
    case G_ACTION_V_VANNE   :
    case G_ACTION_POLYGONE :
    case G_ACTION_COURBE_T   :
    case G_ACTION_COURBE_C   :
    case G_ACTION_COURBE_A   :
    case G_ACTION_ENTREE_LOG:
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_POLYGONE_PLEIN :
    case G_ACTION_BARGRAPHE        :
			if (nCouleurLigne != G_COULEUR_INDETERMINEE)
				pElement->nCouleurLigne = nCouleurLigne;
			if (StyleLigne != G_STYLE_LIGNE_INDETERMINE)
				pElement->nStyleLigne = StyleLigne;
			if (nCouleurRemplissage != G_COULEUR_INDETERMINEE)
				pElement->nCouleurRemplissage = nCouleurRemplissage;
			if (StyleRemplissage != G_STYLE_REMPLISSAGE_INDETERMINE)
				pElement->nStyleRemplissage = StyleRemplissage;
			break;

    case G_ACTION_H_TEXTE:
    case G_ACTION_V_TEXTE:
    case G_ACTION_EDIT_TEXTE:
    case G_ACTION_STATIC_TEXTE:
    case G_ACTION_EDIT_NUM:
    case G_ACTION_STATIC_NUM:
			//if (nCouleurRemplissage != G_COULEUR_INDETERMINEE)
			//	pElement->nCouleurRemplissage = nCouleurRemplissage;
			if (nCouleurLigne != G_COULEUR_INDETERMINEE)
				pElement->nCouleurLigne = nCouleurLigne;
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			if (nCouleurLigne != G_COULEUR_INDETERMINEE)
				pElement->nCouleurLigne = nCouleurLigne;
			if (nCouleurRemplissage != G_COULEUR_INDETERMINEE)
				pElement->nCouleurRemplissage = nCouleurRemplissage;
			if (StyleLigne != G_STYLE_LIGNE_INDETERMINE)
				pElement->nStyleLigne = StyleLigne;
			if (StyleRemplissage != G_STYLE_REMPLISSAGE_INDETERMINE)
				pElement->nStyleRemplissage = StyleRemplissage;
			break;

    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
    case G_ACTION_CONT_BOUTON_VAL:
    case G_ACTION_NOP: 
			break;
    default:
			VerifWarningExit;
			break;
    }
  }


// -------------------------------------------------------------------------
// Redimensionne un �l�ment graphique
void bdgr_coordonnees_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, LONG x1, LONG y1, LONG x2, LONG y2)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR			pBdGr = (PBDGR)hbdgr;
			DWORD								wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR		pElement;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      wPositionElement = pPage->PremierElement + Element - 1;
      pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
      pElement->pt0.x = x1;
      pElement->pt0.y = y1;
      pElement->pt1.x = x2;
      pElement->pt1.y = y2;

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      }
    }
  }

// -------------------------------------------------------------------------
// Change la couleur d'un �l�ment graphique et rafraichit �ventuellement l'�cran
// Appel� par PcsExe et PcsGr
void bdgr_couleur_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, G_COULEUR Couleur)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR	pBdGr = (PBDGR)hbdgr;
			DWORD wPositionElement = pPage->PremierElement + Element - 1;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);

      ForcerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, Couleur, Couleur, G_STYLE_LIGNE_INDETERMINE, G_STYLE_REMPLISSAGE_INDETERMINE);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  } // bdgr_couleur_element

// ---------------------------------------------------------------------------------------
// Change le style ligne d'un �l�ment graphique et rafraichit �ventuellement l'�cran
void bdgr_style_ligne_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, G_STYLE_LIGNE Style)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR		pBdGr = (PBDGR)hbdgr;
			DWORD  wPositionElement = pPage->PremierElement + Element - 1;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      ForcerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, G_COULEUR_INDETERMINEE, G_COULEUR_INDETERMINEE, Style, G_STYLE_REMPLISSAGE_INDETERMINE);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      }
    }
  }

// -------------------------------------------------------------------------------------------
// Change le style de remplissage d'un �l�ment graphique et rafraichit �ventuellement l'�cran
void bdgr_style_remplissage_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, G_STYLE_REMPLISSAGE Style)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR	pBdGr = (PBDGR)hbdgr;
			DWORD wPositionElement = pPage->PremierElement + Element - 1;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      ForcerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, G_COULEUR_INDETERMINEE, G_COULEUR_INDETERMINEE,
				G_STYLE_LIGNE_INDETERMINE, Style);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      }
    }
  }

// -------------------------------------------------------------------------
// Change le style de texte d'un �l�ment graphique et rafraichit �ventuellement l'�cran
BOOL bdgr_style_texte_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, DWORD Police, DWORD StylePolice)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  BOOL			bOk = FALSE;

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR							pBdGr = (PBDGR)hbdgr;
			DWORD							wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      if ((pElement->nAction == G_ACTION_H_TEXTE) || (pElement->nAction == G_ACTION_V_TEXTE) ||
          (pElement->nAction == G_ACTION_EDIT_TEXTE) || (pElement->nAction == G_ACTION_STATIC_TEXTE) ||
          (pElement->nAction == G_ACTION_EDIT_NUM) || (pElement->nAction == G_ACTION_STATIC_NUM))
        {
				BOOL						bPasTrouve;
				DWORD           wPositionGeoTexte;
				DWORD           wDernierGeoTexte;
				PGEO_TEXTES_GR	pGeoTexte;
				DWORD           wPositionTexte;
				PTEXTES_GR			pTexte;

        bOk = TRUE;
        if (ModeTravail & MODE_GOMME_ELEMENT)
          {
          GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
          }

        if (pElement->nStyleLigne == 0)
          {
          wDernierGeoTexte = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1;
          wPositionGeoTexte = 1;
          bPasTrouve = TRUE;
          while ((wPositionGeoTexte < wDernierGeoTexte) && (bPasTrouve))
            {
            pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
            bPasTrouve = (BOOL) (pGeoTexte->ReferenceTexte != 0);
            wPositionGeoTexte++;
            }
          wPositionGeoTexte--;
          if (bPasTrouve)
            {
            pElement->nStyleLigne = (G_STYLE_LIGNE)wDernierGeoTexte; //$$ bof
            wPositionGeoTexte = wDernierGeoTexte;
            insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, wDernierGeoTexte);
            pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wDernierGeoTexte);
            pGeoTexte->ReferenceTexte = 0;
            }
          else
            {
            pElement->nStyleLigne = (G_STYLE_LIGNE)wPositionGeoTexte; //$$ bof
            }
          }

        pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
        if (pGeoTexte->ReferenceTexte == 0)
          {
          wPositionTexte = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
          pGeoTexte->ReferenceTexte = wPositionTexte;
          insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, wPositionTexte);
          pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
          pTexte->Texte [0] = '\0';
          pTexte->Police      = 0;
          pTexte->StylePolice = 0;
          }
        else
          {
          wPositionTexte = pGeoTexte->ReferenceTexte;
          }

        pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
        pTexte->Police      = Police;
        pTexte->StylePolice = StylePolice;

        if (ModeTravail & MODE_DESSIN_ELEMENT)
          {
          DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
          }
        }
      }
    }

  return bOk;
  }

// -------------------------------------------------------------------------
// Change le texte d'un �l�ment graphique dans la base de donn�es et rafraichit
// �ventuellement l'�cran
BOOL bTexteElementGR (HBDGR hbdgr, DWORD ModeTravail, DWORD dwNElement, PCSTR pszTexte)
  {
	PELEMENT_PAGE_GR	pElement = pElementPage (dwNElement);
  BOOL							bOk = FALSE;
	G_ATTRIBUT				attributsAction = G_ATTRIBUT_INVALIDE;

	if (pElement)
		attributsAction = attributsActionGr (pElement->nAction);

	// Objet a du texte ?
  if (attributsAction & G_ATTRIBUT_TEXTE)
    {
		// oui => on le met � jour
	  PBDGR	pBdGr = (PBDGR)hbdgr;
		BOOL						bPasTrouve;
		DWORD           wPositionGeoTexte;
		DWORD           wDernierGeoTexte;
		PGEO_TEXTES_GR	pGeoTexte;
		DWORD           wPositionTexte;
		PTEXTES_GR			pTexte;

		bOk = TRUE;

		// Objet dessin => on l'efface
		if (attributsAction & G_ATTRIBUT_DESSIN)
			{
			if (ModeTravail & MODE_GOMME_ELEMENT)
				{
				GommerElementGR (B_ELEMENTS_PAGES_GR, dwNElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
				}
			}

		if (pElement->nStyleLigne == 0)
			{
			wDernierGeoTexte = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1;
			wPositionGeoTexte = 1;
			bPasTrouve = TRUE;
			while ((wPositionGeoTexte < wDernierGeoTexte) && (bPasTrouve))
				{
				pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
				bPasTrouve = (pGeoTexte->ReferenceTexte != 0);
				wPositionGeoTexte++;
				}
			wPositionGeoTexte--;
			if (bPasTrouve)
				{
				pElement->nStyleLigne = (G_STYLE_LIGNE)wDernierGeoTexte; //$$
				wPositionGeoTexte = wDernierGeoTexte;
				insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, wDernierGeoTexte);
				pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wDernierGeoTexte);
				pGeoTexte->ReferenceTexte = 0;
				}
			else
				{
				pElement->nStyleLigne = (G_STYLE_LIGNE)wPositionGeoTexte;
				}
			}
		else
			{
			wPositionGeoTexte = pElement->nStyleLigne;
			}

		pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
		if (pGeoTexte->ReferenceTexte == 0)
			{
			wPositionTexte = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
			pGeoTexte->ReferenceTexte = wPositionTexte;
			insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, wPositionTexte);
			pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
			pTexte->Texte [0] = '\0';
			pTexte->Police      = 0;
			pTexte->StylePolice = 0;
			}
		else
			{
			wPositionTexte = pGeoTexte->ReferenceTexte;
			}

		pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
		StrExtract (pTexte->Texte, pszTexte, 0, c_NB_CAR_TEXTE_GR);

		// Objet dessin => on le redessine
		if (attributsAction & G_ATTRIBUT_DESSIN)
			{
			if (ModeTravail & MODE_DESSIN_ELEMENT)
				{
				DessinerElementGR (B_ELEMENTS_PAGES_GR, dwNElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
				}
			}

		// Objet contr�le => on met � jour sa fen�tre
		if (attributsAction & G_ATTRIBUT_CONTROLE)
			{
			if (ModeTravail & (MODE_DESSIN_ELEMENT|MODE_GOMME_ELEMENT))
				{
				VerifWarning (pElement->hwndControle != NULL);
				::SetWindowText (pElement->hwndControle, pTexte->Texte );
				}
			}
		}

  return bOk;
  } // bTexteElementGR

// -------------------------------------------------------------------------
// Change le texte d'un �l�ment graphique et rafraichit �ventuellement l'�cran
// appel� exclusivement par pcsexe en animation
BOOL bTexteElementGRDansPage (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, PCSTR pszTexte)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  BOOL			bOk = FALSE;

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR							pBdGr = (PBDGR)hbdgr;
			DWORD							wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

			switch (pElement->nAction)
				{
				case G_ACTION_H_TEXTE:
				case G_ACTION_V_TEXTE:
				case G_ACTION_EDIT_TEXTE:
				case G_ACTION_STATIC_TEXTE:
				case G_ACTION_EDIT_NUM:
				case G_ACTION_STATIC_NUM:
					{
					BOOL						bPasTrouve;
					DWORD           wPositionGeoTexte;
					DWORD           wDernierGeoTexte;
					PGEO_TEXTES_GR	pGeoTexte;
					DWORD           wPositionTexte;
					PTEXTES_GR			pTexte;
					char TexteTemp [c_NB_CAR_TEXTE_GR + 1];

					bOk = TRUE;
//					if (ModeTravail & MODE_GOMME_ELEMENT)
//						{
//						GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
//						}

					if (pElement->nStyleLigne == 0)
						{
						wDernierGeoTexte = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1;
						wPositionGeoTexte = 1;
						bPasTrouve = TRUE;
						while ((wPositionGeoTexte < wDernierGeoTexte) && (bPasTrouve))
							{
							pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
							bPasTrouve = (pGeoTexte->ReferenceTexte != 0);
							wPositionGeoTexte++;
							}
						wPositionGeoTexte--;
						if (bPasTrouve)
							{
							pElement->nStyleLigne = (G_STYLE_LIGNE)wDernierGeoTexte;
							wPositionGeoTexte = wDernierGeoTexte;
							insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, wDernierGeoTexte);
							pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wDernierGeoTexte);
							pGeoTexte->ReferenceTexte = 0;
							}
						else
							{
							pElement->nStyleLigne = (G_STYLE_LIGNE)wPositionGeoTexte;
							}
						}
					else
						{
						wPositionGeoTexte = pElement->nStyleLigne;
						}

					pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
					if (pGeoTexte->ReferenceTexte == 0)
						{
						wPositionTexte = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
						pGeoTexte->ReferenceTexte = wPositionTexte;
						insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, wPositionTexte);
						pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
						pTexte->Texte [0] = '\0';
						pTexte->Police      = 0;
						pTexte->StylePolice = 0;
						}
					else
						{
						wPositionTexte = pGeoTexte->ReferenceTexte;
						}

					StrExtract (TexteTemp, pszTexte, 0, c_NB_CAR_TEXTE_GR);
					pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);

					if (!bStrEgales (pTexte->Texte, TexteTemp))
						{
						// Effacement de l'ancien texte 
						if (ModeTravail & MODE_GOMME_ELEMENT)
							{
							GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
							}

						StrCopy (pTexte->Texte, TexteTemp);

						// Invalider le nouveau texte pour provoquer son r�affichage 
						if (ModeTravail & MODE_GOMME_ELEMENT)
							{
							GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
							}

						if (ModeTravail & MODE_DESSIN_ELEMENT)
							{
							DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
							}
						}
					}
					break;

				case G_ACTION_CONT_STATIC:
				case G_ACTION_CONT_EDIT:
					{
					if (ModeTravail & (MODE_DESSIN_ELEMENT|MODE_GOMME_ELEMENT))
						{
						VerifWarning (pElement->hwndControle != NULL);
						::SetWindowText (pElement->hwndControle, pszTexte);
						}
					}
					break;

				case G_ACTION_CONT_GROUP:
				case G_ACTION_CONT_BOUTON:
				case G_ACTION_CONT_CHECK:
				case G_ACTION_CONT_RADIO:
				case G_ACTION_CONT_BOUTON_VAL:
					{
					BOOL						bPasTrouve;
					DWORD           wPositionGeoTexte;
					DWORD           wDernierGeoTexte;
					PGEO_TEXTES_GR	pGeoTexte;
					DWORD           wPositionTexte;
					PTEXTES_GR			pTexte;

					bOk = TRUE;

					if (pElement->nStyleLigne == 0)
						{
						wDernierGeoTexte = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1;
						wPositionGeoTexte = 1;
						bPasTrouve = TRUE;
						while ((wPositionGeoTexte < wDernierGeoTexte) && (bPasTrouve))
							{
							pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
							bPasTrouve = (pGeoTexte->ReferenceTexte != 0);
							wPositionGeoTexte++;
							}
						wPositionGeoTexte--;
						if (bPasTrouve)
							{
							pElement->nStyleLigne = (G_STYLE_LIGNE)wDernierGeoTexte;
							wPositionGeoTexte = wDernierGeoTexte;
							insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, wDernierGeoTexte);
							pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wDernierGeoTexte);
							pGeoTexte->ReferenceTexte = 0;
							}
						else
							{
							pElement->nStyleLigne = (G_STYLE_LIGNE)wPositionGeoTexte;
							}
						}
					else
						{
						wPositionGeoTexte = pElement->nStyleLigne;
						}

					pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
					if (pGeoTexte->ReferenceTexte == 0)
						{
						wPositionTexte = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
						pGeoTexte->ReferenceTexte = wPositionTexte;
						insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, wPositionTexte);
						pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
						pTexte->Texte [0] = '\0';
						pTexte->Police      = 0;
						pTexte->StylePolice = 0;
						}
					else
						{
						wPositionTexte = pGeoTexte->ReferenceTexte;
						}

					pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
					StrExtract (pTexte->Texte, pszTexte, 0, c_NB_CAR_TEXTE_GR);


					if (ModeTravail & (MODE_DESSIN_ELEMENT|MODE_GOMME_ELEMENT))
						{
						VerifWarning (pElement->hwndControle != NULL);
						::SetWindowText (pElement->hwndControle, pTexte->Texte );
						//DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
						}
					}
					break;
				} // switch (pElement->Action)
      } // if ((Element > 0) && (Element <= pPage->NbElements))
    } // if (ExistePageGR (pBdGr->NPage, &wReferencePage))

  return bOk;
  } // bTexteElementGRDansPage

// -------------------------------------------------------------------------
// Forcer Style Texte (Police, StylePolice et Taille)
// -------------------------------------------------------------------------
void ForcerStyleTexteElementGR (HBDGR hbdgr, DWORD Repere, DWORD Position, DWORD Police, DWORD StylePolice, DWORD TaillePolicePixel)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);

	switch (pElement->nAction)
		{
		case G_ACTION_H_TEXTE:
		case G_ACTION_V_TEXTE:
			{
			PGEO_TEXTES_GR	pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
			PTEXTES_GR			pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);

			if (Police != (DWORD)-1)
				pTxt->Police = Police;
			if (StylePolice != (DWORD)-1)
				pTxt->StylePolice = StylePolice;
			if (TaillePolicePixel != (DWORD)-1)
				{
				pElement->pt1.y = bdgr_taille_police_courante (hbdgr, TaillePolicePixel);
				}
			}
			break;

		case G_ACTION_EDIT_TEXTE:
		case G_ACTION_STATIC_TEXTE:
		case G_ACTION_EDIT_NUM:
		case G_ACTION_STATIC_NUM:
			{
			PGEO_TEXTES_GR	pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
			PTEXTES_GR			pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);

			if (Police != (DWORD)-1)
				pTxt->Police = Police;
			if (StylePolice != (DWORD)-1)
				pTxt->StylePolice = StylePolice;
			if (TaillePolicePixel != (DWORD)-1)
				{
				pElement->pt1.y = pElement->pt0.y + bdgr_taille_police_courante (hbdgr, TaillePolicePixel);
				}
			}
			break;

		case G_ACTION_LISTE:
		case G_ACTION_LISTE_PLEIN:
			{
			PGEO_ELEMENTS_LISTES_GR pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
			DWORD	wPremierElement = pGeoListe->PremierElement;
			DWORD	wDernierElement = wPremierElement + pGeoListe->NbElements;
			DWORD	wPositionElement;

      for (wPositionElement = wPremierElement; wPositionElement < wDernierElement; wPositionElement++)
        {
        ForcerStyleTexteElementGR (hbdgr, B_ELEMENTS_LISTES_GR, wPositionElement, Police, StylePolice, TaillePolicePixel);
        }
			}
			break;
		}
  }

// --------------------------------------------------------------------------
// Renvoie un pointeur sur l'�l�ment de page num�ro dwNElement
PELEMENT_PAGE_GR pElementPage (DWORD dwNElement)
	{
	return (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, dwNElement);
	}

// --------------------------------------------------------------------------
// Renvoie un pointeur sur l'�l�ment de page num�ro dwElement ou NULL
PELEMENT_PAGE_GR pElementDansPage (HBDGR hbdgr, DWORD dwNElementDansPage)
	{
	PPAGE_GR					pPage = ppageHBDGR (hbdgr);
	PELEMENT_PAGE_GR	pElementPageRet = NULL; 

	if (pPage) //$$ && (dwNElementDansPage > 0) && (dwNElementDansPage <= pPage->NbElements))
		{
		pElementPageRet = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, pPage->PremierElement + dwNElementDansPage - 1);
		}

	return pElementPageRet;
	} // pElementDansPage

// -------------------------------------------------------------------------
PSTR LireIdentifiantElement (DWORD dwElement)
	{
	PELEMENT_PAGE_GR	pElementPage = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, dwElement);
	return (pElementPage->strIdentifiant);
	}

// -------------------------------------------------------------------------
BOOL bdgr_lire_style_element (HBDGR hbdgr, DWORD Element, DWORD *StyleLigne, DWORD *StyleRemplissage)
	{
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  BOOL			bOk = FALSE;

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD					wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
			
			*StyleLigne = pElement->nStyleLigne;
			*StyleRemplissage = pElement->nStyleRemplissage;
			bOk = TRUE;
      }
    }

  return bOk;
	}

// -------------------------------------------------------------------------
BOOL bdgr_lire_couleur_element (HBDGR hbdgr, DWORD Element, G_COULEUR *CouleurLigne, G_COULEUR *CouleurRemplissage)
	{
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  BOOL			bOk = FALSE;

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD					wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
			
			*CouleurLigne = pElement->nCouleurLigne;
			*CouleurRemplissage = pElement->nCouleurRemplissage;
			bOk = TRUE;
      }
    }

  return bOk;
	}

// -------------------------------------------------------------------------
BOOL bdgr_lire_style_texte_element  (HBDGR hbdgr, DWORD Element, DWORD *Police, DWORD *StylePolice)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  BOOL			bOk = FALSE;

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD					wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      if ((pElement->nAction == G_ACTION_H_TEXTE) || (pElement->nAction == G_ACTION_V_TEXTE) ||
          (pElement->nAction == G_ACTION_EDIT_TEXTE) || (pElement->nAction == G_ACTION_STATIC_TEXTE) ||
          (pElement->nAction == G_ACTION_EDIT_NUM) || (pElement->nAction == G_ACTION_STATIC_NUM))
        {
        bOk = TRUE;
        if (pElement->nStyleLigne != 0)
          {
 					PGEO_TEXTES_GR	pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);

          if (pGeoTexte->ReferenceTexte != 0)
            {
						PTEXTES_GR	pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTexte->ReferenceTexte);

            (*Police) = pTexte->Police;
            (*StylePolice) = pTexte->StylePolice;
            }
          }
        }
      }
    }

  return bOk;
  }


// -------------------------------------------------------------------------
// R�cup�re le texte d'un �l�ment graphique
// Renvoie TRUE si texte r�cup�r�
BOOL bLireTexteElementGR (DWORD dwNElement, PSTR pszDest)
  {
  BOOL							bOk = FALSE;
	PELEMENT_PAGE_GR	pElement = pElementPage (dwNElement);

	// l'�l�ment a du texte ?
	if (bAttributsActionGr (pElement->nAction, G_ATTRIBUT_TEXTE))
		{
		PGEO_TEXTES_GR	pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
		PTEXTES_GR			pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTexte->ReferenceTexte);

		StrExtract (pszDest, pTexte->Texte, 0, c_NB_CAR_TEXTE_GR);
		bOk = TRUE;
		}

  return bOk;
  } // bLireTexteElementGR

// -------------------------------------------------------------------------
// Translation
// -------------------------------------------------------------------------
void TranslaterElementGR (DWORD Repere, DWORD Position, LONG Dx, LONG Dy, HGSYS hGsys, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  LONG            iX;
  LONG            iY;

  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE           :
    case G_ACTION_RECTANGLE      :
    case G_ACTION_CERCLE         :
    case G_ACTION_QUART_ARC          :
    case G_ACTION_QUART_CERCLE     :
    case G_ACTION_H_DEMI_CERCLE        :
    case G_ACTION_V_DEMI_CERCLE        :
    case G_ACTION_TRI_RECT       :
    case G_ACTION_H_TRI_ISO      :
    case G_ACTION_V_TRI_ISO      :
    case G_ACTION_H_VANNE        :
    case G_ACTION_V_VANNE        :
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :

    case G_ACTION_COURBE_T        :
    case G_ACTION_COURBE_C        :
    case G_ACTION_COURBE_A        :
    case G_ACTION_BARGRAPHE        :
    case G_ACTION_EDIT_TEXTE  : //$$ v�rifier
    case G_ACTION_STATIC_TEXTE  :
    case G_ACTION_EDIT_NUM  :
    case G_ACTION_STATIC_NUM  :
    case G_ACTION_ENTREE_LOG:

    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
    case G_ACTION_CONT_BOUTON_VAL:
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);

			GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, Dx, Dy);
			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = iX;
			pElement->pt0.y = iY;

			GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt1.x, pElement->pt1.y); //$$ optimisable
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, Dx, Dy);
			g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
			pElement->pt1.x = iX;
			pElement->pt1.y = iY;
			break;

		case G_ACTION_H_TEXTE:
		case G_ACTION_V_TEXTE:
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);

			GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, Dx, Dy);
			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = iX;
			pElement->pt0.y = iY;

			GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt1.x, pElement->pt1.y);
			g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
			pElement->pt1.x = iX;
			pElement->pt1.y = iY;
			break;

    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
    case G_ACTION_LISTE          :
    case G_ACTION_LISTE_PLEIN     :
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);

			GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			g_element_move_delta (hGsys, hElmtGsys, Dx, Dy);
			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = (int) iX;
			pElement->pt0.y = (int) iY;
			break;
    //case G_ACTION_NOP:default: break;
    }
  }

// -------------------------------------------------------------------------
// Changer le remplissage d'un �l�ment
// -------------------------------------------------------------------------
void ChangeRemplissageElementGR (DWORD Repere, DWORD Position, HGSYS hGsys, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);

	// r�cup�re l'action ayant un autre style de remplissage
  GElementSetAction (hElmtGsys, pElement->nAction);

	// changement effectu� ?
  if (bGElementChangeRemplissage (hGsys, hElmtGsys))
		{
		G_COULEUR	nCouleurTemp;

		// oui => r�cup�re la nouvelle action ...
		pElement->nAction = actionElement (hGsys, hElmtGsys);

		// substitue couleur de ligne et de remplissage
		nCouleurTemp = pElement->nCouleurRemplissage;
		pElement->nCouleurRemplissage = pElement->nCouleurLigne;
		pElement->nCouleurLigne = nCouleurTemp;

		// Style de ligne existe sur l'objet et non initialis� ?
		if (bAttributsActionGr (pElement->nAction, G_ATTRIBUT_STYLE_LIGNE) &&
			(pElement->nStyleLigne == G_STYLE_LIGNE_INDETERMINE))
			// oui => style de ligne par d�faut
			pElement->nStyleLigne = G_STYLE_LIGNE_CONTINUE;

		// Style de remplissage existe sur l'objet et non initialis� ?
		if (bAttributsActionGr (pElement->nAction, G_ATTRIBUT_STYLE_REMPLISSAGE) &&
			(pElement->nStyleRemplissage == G_STYLE_REMPLISSAGE_INDETERMINE))
			// oui => style de remplissage par d�faut
			pElement->nStyleRemplissage = G_STYLE_REMPLISSAGE_PLEIN;
		}
  }

// -------------------------------------------------------------------------
// Miroir
// -------------------------------------------------------------------------
void MiroiterElementGR (DWORD Repere, DWORD Position, DWORD Axes, LONG xAxe, LONG yAxe, HGSYS hGsys, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  LONG                       iX;
  LONG                       iY;
  PGEO_POINTS_POLY_GR	pGeoPoly;
  DWORD                      wPremierPoint;
  DWORD                      wDernierPoint;
  DWORD                      wPositionPoint;
  PPOINTS_POLY_GR						pPoint;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD                      wPremierElementListe;
  DWORD                      wDernierElementListe;
  DWORD                      wPositionElementListe;

  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE           :
    case G_ACTION_RECTANGLE      :
    case G_ACTION_CERCLE         :
    case G_ACTION_QUART_ARC          :
    case G_ACTION_QUART_CERCLE     :
    case G_ACTION_H_DEMI_CERCLE        :
    case G_ACTION_V_DEMI_CERCLE        :
    case G_ACTION_TRI_RECT       :
    case G_ACTION_H_TRI_ISO      :
    case G_ACTION_V_TRI_ISO      :
    case G_ACTION_H_VANNE        :
    case G_ACTION_V_VANNE        :
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_H_TEXTE       :
    case G_ACTION_V_TEXTE       :
    case G_ACTION_COURBE_T        :
    case G_ACTION_COURBE_C        :
    case G_ACTION_COURBE_A        :
    case G_ACTION_BARGRAPHE        :
    case G_ACTION_EDIT_TEXTE  :
    case G_ACTION_STATIC_TEXTE  :
    case G_ACTION_EDIT_NUM  :
    case G_ACTION_STATIC_NUM  :
    case G_ACTION_ENTREE_LOG    :
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint  (hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

			g_element_miror (hGsys, hElmtGsys, Axes, xAxe, yAxe);

			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = (int) iX;
			pElement->pt0.y = (int) iY;

			g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
			pElement->pt1.x = (int) iX;
			pElement->pt1.y = (int) iY;
			break;

    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
    case G_ACTION_CONT_BOUTON_VAL:
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint (hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

			g_element_miror (hGsys, hElmtGsys, Axes, xAxe, yAxe);

			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = (int) iX;
			pElement->pt0.y = (int) iY;

			g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
			pElement->pt1.x = (int) iX;
			pElement->pt1.y = (int) iY;

			// r� ordonne les bipoints du contr�le
			G_OrdonneBipoint (&pElement->pt0, &pElement->pt1);
			break;
    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
         GElementSetAction (hElmtGsys, pElement->nAction);
         GElementSupprimePoints (hElmtGsys);
         GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);

         g_element_miror (hGsys, hElmtGsys, Axes, 0, 0);
         g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
         pElement->pt0.x = (int) iX;
         pElement->pt0.y = (int) iY;

         GElementSupprimePoints (hElmtGsys);
         pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, pElement->pt1.y);
         wPremierPoint = pGeoPoly->PremierPoint;
         wDernierPoint = wPremierPoint + pGeoPoly->NbPoints;
         for (wPositionPoint = wPremierPoint; wPositionPoint < wDernierPoint; wPositionPoint++)
           {
           pPoint = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, wPositionPoint);
           GElementAjoutePoint  (hGsys, hElmtGsys, pPoint->x, pPoint->y);
           }
         g_element_miror (hGsys, hElmtGsys, Axes, xAxe, yAxe);
         for (wPositionPoint = wPremierPoint; wPositionPoint < wDernierPoint; wPositionPoint++)
           {
           g_element_get_point (hGsys, hElmtGsys, wPositionPoint - wPremierPoint, &iX, &iY);
           pPoint = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, wPositionPoint);
           pPoint->x = (int) iX;
           pPoint->y = (int) iY;
           }
         break;
    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
         GElementSetAction (hElmtGsys, pElement->nAction);
         GElementSupprimePoints (hElmtGsys);
         GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);

         g_element_miror (hGsys, hElmtGsys, Axes, 0, 0);
         g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
         pElement->pt0.x = (int) iX;
         pElement->pt0.y = (int) iY;

         pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
         wPremierElementListe = pGeoListe->PremierElement;
         wDernierElementListe = wPremierElementListe + pGeoListe->NbElements;
         for (wPositionElementListe = wPremierElementListe; wPositionElementListe < wDernierElementListe; wPositionElementListe++)
           {
           MiroiterElementGR (B_ELEMENTS_LISTES_GR, wPositionElementListe, Axes, xAxe, yAxe, hGsys, hElmtGsys);
           }
         break;
    case G_ACTION_NOP     :
         break;
    default:
         break;
    }
  }

// -------------------------------------------------------------------------
// Rotation
// -------------------------------------------------------------------------
void TournerElementGR (DWORD Repere, DWORD Position, LONG xCentre, LONG yCentre, DWORD NbQuartTour, HGSYS hGsys, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  //DWORD                      wAttributs;
  //DWORD                      wAction;
  //DWORD                      wCouleur;
  //DWORD                      wStyleLigne;
  //DWORD                      wStyleRemplissage;
  //DWORD                      wPolice;
  //DWORD                      wStylePolice;
  //char                     *pTexte;
  //DWORD                      wNbPoints;
  LONG                       iX;
  LONG                       iY;
  PGEO_POINTS_POLY_GR	pGeoPoly;
  DWORD                      wPremierPoint;
  DWORD                      wDernierPoint;
  DWORD                      wPositionPoint;
  PPOINTS_POLY_GR						pPoint;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD                      wPremierElementListe;
  DWORD                      wDernierElementListe;
  DWORD                      wPositionElementListe;

  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE           :
    case G_ACTION_RECTANGLE      :
    case G_ACTION_CERCLE         :
    case G_ACTION_QUART_ARC          :
    case G_ACTION_QUART_CERCLE     :
    case G_ACTION_H_DEMI_CERCLE        :
    case G_ACTION_V_DEMI_CERCLE        :
    case G_ACTION_TRI_RECT       :
    case G_ACTION_H_TRI_ISO      :
    case G_ACTION_V_TRI_ISO      :
    case G_ACTION_H_VANNE        :
    case G_ACTION_V_VANNE        :
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_H_TEXTE        :
    case G_ACTION_V_TEXTE        :
    case G_ACTION_COURBE_T        :
    case G_ACTION_COURBE_C        :
    case G_ACTION_COURBE_A        :
    case G_ACTION_BARGRAPHE        :
    case G_ACTION_EDIT_TEXTE  :
    case G_ACTION_STATIC_TEXTE  :
    case G_ACTION_EDIT_NUM  :
    case G_ACTION_STATIC_NUM  :
    case G_ACTION_ENTREE_LOG    :
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint  (hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

			g_element_rotate (hGsys, hElmtGsys, xCentre, yCentre, NbQuartTour);

			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = (int) iX;
			pElement->pt0.y = (int) iY;

			g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
			pElement->pt1.x = (int) iX;
			pElement->pt1.y = (int) iY;

			//g_element_get (hGsys, hElmtGsys, &wAttributs,
			//             &wAction, &wCouleur, &wStyleLigne, &wStyleRemplissage,
			//           &wPolice, &wStylePolice, &pTexte, &wNbPoints);
			pElement->nAction = actionElement(hGsys, hElmtGsys);//wAction;
			break;

    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
    case G_ACTION_CONT_BOUTON_VAL:
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint  (hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

			g_element_rotate (hGsys, hElmtGsys, xCentre, yCentre, NbQuartTour);

			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = (int) iX;
			pElement->pt0.y = (int) iY;

			g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
			pElement->pt1.x = (int) iX;
			pElement->pt1.y = (int) iY;

			//g_element_get (hGsys, hElmtGsys, &wAttributs,
			//             &wAction, &wCouleur, &wStyleLigne, &wStyleRemplissage,
			//           &wPolice, &wStylePolice, &pTexte, &wNbPoints);
			pElement->nAction = actionElement(hGsys, hElmtGsys);//wAction;
			// r� ordonne les bipoints du contr�le
			G_OrdonneBipoint (&pElement->pt0, &pElement->pt1);
			break;
    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);
			GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);

			g_element_rotate (hGsys, hElmtGsys, 0, 0, NbQuartTour);
			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = (int) iX;
			pElement->pt0.y = (int) iY;

			GElementSupprimePoints (hElmtGsys);
			pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, pElement->pt1.y);
			wPremierPoint = pGeoPoly->PremierPoint;
			wDernierPoint = wPremierPoint + pGeoPoly->NbPoints;
			for (wPositionPoint = wPremierPoint; wPositionPoint < wDernierPoint; wPositionPoint++)
				{
				pPoint = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, wPositionPoint);
				GElementAjoutePoint  (hGsys, hElmtGsys, pPoint->x, pPoint->y);
				}
			g_element_rotate (hGsys, hElmtGsys, xCentre, yCentre, NbQuartTour);
			for (wPositionPoint = wPremierPoint; wPositionPoint < wDernierPoint; wPositionPoint++)
				{
				g_element_get_point (hGsys, hElmtGsys, wPositionPoint - wPremierPoint, &iX, &iY);
				pPoint = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, wPositionPoint);
				pPoint->x = (int) iX;
				pPoint->y = (int) iY;
				}
			break;
    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);
			GElementAjoutePoint  (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);

			g_element_rotate (hGsys, hElmtGsys, 0, 0, NbQuartTour);
			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = (int) iX;
			pElement->pt0.y = (int) iY;

			pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
			wPremierElementListe = pGeoListe->PremierElement;
			wDernierElementListe = wPremierElementListe + pGeoListe->NbElements;
			for (wPositionElementListe = wPremierElementListe; wPositionElementListe < wDernierElementListe; wPositionElementListe++)
				{
				TournerElementGR (B_ELEMENTS_LISTES_GR, wPositionElementListe, xCentre, yCentre, NbQuartTour, hGsys, hElmtGsys);
				}
			break;
    //case G_ACTION_NOP: default: break;
    }
  }

// -------------------------------------------------------------------------
// Deformation
// -------------------------------------------------------------------------
void DeformerElementGR (DWORD Repere, DWORD Position, DWORD *PositionSauvegarde, LONG xRef, LONG yRef,
												LONG NCx, LONG DCx, LONG NCy, LONG DCy, HGSYS hGsys, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);

  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE:
    case G_ACTION_RECTANGLE:
    case G_ACTION_CERCLE:
    case G_ACTION_QUART_ARC:
    case G_ACTION_QUART_CERCLE:
    case G_ACTION_H_DEMI_CERCLE:
    case G_ACTION_V_DEMI_CERCLE:
    case G_ACTION_TRI_RECT:
    case G_ACTION_H_TRI_ISO:
    case G_ACTION_V_TRI_ISO:
    case G_ACTION_H_VANNE:
    case G_ACTION_V_VANNE:
    case G_ACTION_RECTANGLE_PLEIN:
    case G_ACTION_CERCLE_PLEIN:
    case G_ACTION_QUART_ARC_PLEIN:
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN:
    case G_ACTION_V_DEMI_CERCLE_PLEIN:
    case G_ACTION_TRI_RECT_PLEIN:
    case G_ACTION_H_TRI_ISO_PLEIN:
    case G_ACTION_V_TRI_ISO_PLEIN:
    case G_ACTION_H_VANNE_PLEIN:
    case G_ACTION_V_VANNE_PLEIN:
    case G_ACTION_COURBE_T:
    case G_ACTION_COURBE_C:
    case G_ACTION_COURBE_A:
    case G_ACTION_BARGRAPHE:
    case G_ACTION_EDIT_TEXTE:
    case G_ACTION_STATIC_TEXTE:
    case G_ACTION_EDIT_NUM:
    case G_ACTION_STATIC_NUM:
    case G_ACTION_ENTREE_LOG:

    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
    case G_ACTION_CONT_BOUTON_VAL:
			{
		  PPOINT	pSav;
			LONG		iX;
			LONG		iY;

			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);

			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			GElementAjoutePoint  (hGsys, hElmtGsys, pSav->x, pSav->y);
			(*PositionSauvegarde)++;

			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			GElementAjoutePoint  (hGsys, hElmtGsys, pSav->x, pSav->y);
			(*PositionSauvegarde)++;

			g_element_zoom (hGsys, hElmtGsys, xRef, yRef, NCx, DCx, NCy, DCy);

			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = iX;
			pElement->pt0.y = iY;

			g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
			pElement->pt1.x = iX;
			pElement->pt1.y = iY;
			}
			break;

    case G_ACTION_H_TEXTE:
    case G_ACTION_V_TEXTE:
			{
		  PPOINT	pSav;
			POINT		point;
			LONG		iX;
			LONG		iY;

			// $$ A terminer pour les cas de d�formation avec sym�trie verticale
			GElementSetAction (hElmtGsys, G_ACTION_RECTANGLE_PLEIN); //pElement->Action);
			GElementSupprimePoints (hElmtGsys);

			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			GElementAjoutePoint  (hGsys, hElmtGsys, pSav->x, pSav->y);
			(*PositionSauvegarde)++;
			point = *pSav;

			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			GElementAjoutePoint  (hGsys, hElmtGsys, point.x + pSav->x, point.y + pSav->y);
			(*PositionSauvegarde)++;

			g_element_zoom (hGsys, hElmtGsys, xRef, yRef, NCx, DCx, NCy, DCy);

			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = iX;
			pElement->pt0.y = iY;

			g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);

			// restaure l'action (si utilisation ult�rieure
			GElementSetAction (hElmtGsys, pElement->nAction);

			// ajuste l'origine de la chaine
			if (iX < pElement->pt0.x)
				pElement->pt0.x = iX;

			if (iY < pElement->pt0.y)
				pElement->pt0.y = iY;

			// Ajuste la taille de la police
			iY -= point.y;
			if (iY <= 0)
				iY = -iY;

			pElement->pt1.y = iY;
			}
			break;

    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			{
		  PPOINT									pSav;
			LONG                    iX;
			LONG                    iY;
			PGEO_POINTS_POLY_GR	pGeoPoly;
			DWORD                   wPremierPoint;
			DWORD                   wDernierPoint;
			DWORD                   wPositionPoint;
			PPOINTS_POLY_GR					pPoint;

			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);

			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			GElementAjoutePoint  (hGsys, hElmtGsys, pSav->x, pSav->y);
			(*PositionSauvegarde)++;

			g_element_zoom (hGsys, hElmtGsys, 0, 0, NCx, DCx, NCy, DCy);

			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = iX;
			pElement->pt0.y = iY;

			GElementSupprimePoints (hElmtGsys);

			pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, pElement->pt1.y);
			wPremierPoint = pGeoPoly->PremierPoint;
			wDernierPoint = wPremierPoint + pGeoPoly->NbPoints;
			for (wPositionPoint = wPremierPoint; wPositionPoint < wDernierPoint; wPositionPoint++)
				{
				pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
				GElementAjoutePoint  (hGsys, hElmtGsys, pSav->x, pSav->y);
				(*PositionSauvegarde)++;
				}

			g_element_zoom (hGsys, hElmtGsys, xRef, yRef, NCx, DCx, NCy, DCy);

			for (wPositionPoint = wPremierPoint; wPositionPoint < wDernierPoint; wPositionPoint++)
				{
				g_element_get_point (hGsys, hElmtGsys, wPositionPoint - wPremierPoint, &iX, &iY);
				pPoint = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, wPositionPoint);
				pPoint->x = iX;
				pPoint->y = iY;
				}
			}
			break;
    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			{
		  PPOINT	pSav;
			LONG    iX;
			LONG    iY;
			PGEO_ELEMENTS_LISTES_GR	pGeoListe;
			DWORD                   wPremierElementListe;
			DWORD                   wDernierElementListe;
			DWORD                   wPositionElementListe;

			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);

			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			GElementAjoutePoint  (hGsys, hElmtGsys, pSav->x, pSav->y);
			(*PositionSauvegarde)++;

			g_element_zoom (hGsys, hElmtGsys, 0, 0, NCx, DCx, NCy, DCy);

			g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
			pElement->pt0.x = iX;
			pElement->pt0.y = iY;

			pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
			wPremierElementListe = pGeoListe->PremierElement;
			wDernierElementListe = wPremierElementListe + pGeoListe->NbElements;
			for (wPositionElementListe = wPremierElementListe; wPositionElementListe < wDernierElementListe; wPositionElementListe++)
				{
				DeformerElementGR (B_ELEMENTS_LISTES_GR, wPositionElementListe, PositionSauvegarde, xRef, yRef, NCx, DCx, NCy, DCy, hGsys, hElmtGsys);
				}
			}
			break;
    //case G_ACTION_NOP: break;
    //default:			break;
    }
  }

// -------------------------------------------------------------------------
// D�grouper un Element de la Page
// -------------------------------------------------------------------------
DWORD DegrouperElementPageGR (HBDGR hbdgr, DWORD Position, HGSYS hGsys, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, Position);
  LONG                       iDx;
  LONG                       iDy;
  DWORD                      wPosGeoListe;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD                      wPremElementListe;
  DWORD                      wNbElements;
  DWORD                      wIndex;

  if ((pElement->nAction == G_ACTION_LISTE) || (pElement->nAction == G_ACTION_LISTE_PLEIN))
    {
    iDx            = pElement->pt0.x;
    iDy            = pElement->pt0.y;
		PELEMENT_PAGE_GR pElementTransfere;
    wPosGeoListe   = pElement->pt1.y;
    //
    pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPosGeoListe);
    wPremElementListe = pGeoListe->PremierElement;
    wNbElements = pGeoListe->NbElements;
    pGeoListe->NbReferencesListe--;
    //
    if (pGeoListe->NbReferencesListe > 0)
      {
      // insere_enr (szVERIFSource, __LINE__, wNbElements - 1, B_ELEMENTS_PAGES_GR, Position);
      enleve_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_PAGES_GR, Position);
      for (wIndex = 0; wIndex < wNbElements; wIndex++)
        {
        insere_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_PAGES_GR, Position + wIndex);
        CopierElementGR (B_ELEMENTS_LISTES_GR, wPremElementListe + wIndex, B_ELEMENTS_PAGES_GR, Position + wIndex);
				// forcage d'un nouvel identifiant pour chaque element de la liste d�group�
				pElementTransfere = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, Position + wIndex);
				GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, pElementTransfere->strIdentifiant);
        TranslaterElementGR (B_ELEMENTS_PAGES_GR, Position + wIndex, iDx, iDy, hGsys, hElmtGsys);
        }
      }
    else
      {
      enleve_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_PAGES_GR, Position);
      for (wIndex = 0; wIndex < wNbElements; wIndex++)
        {
        insere_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_PAGES_GR, Position + wIndex);
        CopierElementGR (B_ELEMENTS_LISTES_GR, wPremElementListe, B_ELEMENTS_PAGES_GR, Position + wIndex);
				// forcage d'un nouvel identifiant pour chaque element de la liste d�group�
				pElementTransfere = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, Position + wIndex);
				GetIdAutoPcs (ELEMENT_PAGE_GRAPHIQUE, pElementTransfere->strIdentifiant);
				SupprimerElementGR (B_ELEMENTS_LISTES_GR, wPremElementListe);
        TranslaterElementGR (B_ELEMENTS_PAGES_GR, Position + wIndex, iDx, iDy, hGsys, hElmtGsys);
        }
      MajGeoListesGR (MAJ_SUPPRESSION, wPosGeoListe, wPremElementListe, wNbElements);
      }
    //
    }
  else
    {
    wNbElements = 0;
    }

  return (wNbElements);
  }

// -------------------------------------------------------------------------
void SupprimerTexteGR (DWORD GeoPosition)
  {
  PGEO_TEXTES_GR	pGeoTexte;
  DWORD             wPosTexte;

  if (GeoPosition > 0)
    {
    pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, GeoPosition);
    wPosTexte = pGeoTexte->ReferenceTexte;
    pGeoTexte->ReferenceTexte = 0;
    if (wPosTexte > 0)
      {
      enleve_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, wPosTexte);
      MajGeoTextesGR (MAJ_SUPPRESSION, wPosTexte, 1);
      }
    }
  }

// -------------------------------------------------------------------------
void SupprimerPolyGR (DWORD GeoPosition)
  {
  if (GeoPosition > 0)
    {
		PGEO_POINTS_POLY_GR	pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, GeoPosition);

    if (pGeoPoly->NbReferencesPoly > 0)
      {
      pGeoPoly->NbReferencesPoly--;
      if (pGeoPoly->NbReferencesPoly == 0)
        {
				DWORD wPremierPoint = pGeoPoly->PremierPoint;
				DWORD wNbPoints = pGeoPoly->NbPoints;

        enleve_enr (szVERIFSource, __LINE__, wNbPoints, B_POINTS_POLY_GR, wPremierPoint);
        MajGeoPolyGR (MAJ_SUPPRESSION, GeoPosition, wPremierPoint, wNbPoints);
        }
      }
    }
  }

// -------------------------------------------------------------------------
void SupprimerListeGR (DWORD GeoPosition)
  {
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD                      wNbElementsListe;
  DWORD                      wPosElement;
  PELEMENT_PAGE_GR pElement;
  DWORD                      wPosGeoListe;
  DWORD                      wIndex;

  pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, GeoPosition);
  if (pGeoListe->NbReferencesListe > 0)
    {
    pGeoListe->NbReferencesListe--;
    if (pGeoListe->NbReferencesListe == 0)
      {

      wNbElementsListe = pGeoListe->NbElements;
      for (wIndex = 0; wIndex < wNbElementsListe; wIndex++)
        {
        pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, GeoPosition);
        wPosElement = pGeoListe->PremierElement;
        pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_LISTES_GR, wPosElement);
        switch (pElement->nAction)
          {
          case G_ACTION_EDIT_TEXTE   :
          case G_ACTION_STATIC_TEXTE  :
          case G_ACTION_EDIT_NUM  :
          case G_ACTION_STATIC_NUM  :
          case G_ACTION_H_TEXTE:
          case G_ACTION_V_TEXTE:
						SupprimerTexteGR (pElement->nStyleLigne);
						break;

          case G_ACTION_POLYGONE     :
          case G_ACTION_POLYGONE_PLEIN:
						SupprimerPolyGR (pElement->pt1.y);
						break;

          case G_ACTION_LISTE:
          case G_ACTION_LISTE_PLEIN:
						wPosGeoListe   = pElement->pt1.y;
						SupprimerListeGR (wPosGeoListe);
						break;

          //default: break;
          }
        pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, GeoPosition);
        wPosElement = pGeoListe->PremierElement;
        enleve_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_LISTES_GR, wPosElement);
        MajGeoListesGR (MAJ_SUPPRESSION, GeoPosition, wPosElement, 1);
        }
      }
    }
  }

// -------------------------------------------------------------------------
// supprime un �l�ment graphique
void SupprimerElementGR (DWORD Repere, DWORD Position)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  DWORD           wPosGeoListe;

	bFermerFenetreElementGr (Repere, Position);
  bdanmgr_supprimer (pElement->PosGeoAnimation);
  switch (pElement->nAction)
    {
    case G_ACTION_EDIT_TEXTE  :
    case G_ACTION_STATIC_TEXTE  :
    case G_ACTION_EDIT_NUM  :
    case G_ACTION_STATIC_NUM  :
    case G_ACTION_H_TEXTE:
    case G_ACTION_V_TEXTE:
			SupprimerTexteGR (pElement->nStyleLigne);
			break;

		case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
			break;

    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_BOUTON_VAL:
			SupprimerTexteGR (pElement->nStyleLigne);
			break;

    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			SupprimerPolyGR (pElement->pt1.y);
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			wPosGeoListe   = pElement->pt1.y;
			SupprimerListeGR (wPosGeoListe);
			break;

    //default: break;
    }
  enleve_enr (szVERIFSource, __LINE__, 1, Repere, Position);
  }

// -------------------------------------------------------------------------
BOOL SuppressionPossibleElementGR (DWORD Repere, DWORD Position)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  BOOL        bRetour;

  bRetour = bdanmgr_supprimable (pElement->PosGeoAnimation);
  return (bRetour);
  }

// -------------------------------------------------------------------------
// Distance
// -------------------------------------------------------------------------
DWORD DistanceElementGR (DWORD Repere, DWORD Position, LONG x, LONG y, HGSYS hGsys, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  PGEO_TEXTES_GR		pGeoTxt;
  PTEXTES_GR				pTxt;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD                      wPremierElement;
  DWORD                      wDernierElement;
  DWORD                      wPositionElement;
  PGEO_POINTS_POLY_GR	pGeoPointPoly;
  PPOINTS_POLY_GR			pPointPoly;
  DWORD                      wIndexPointPoly;
  LONG                       iNewX;
  LONG                       iNewY;
  DWORD                     lTmpDist;
  DWORD                     lDistance;

  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE           :
    case G_ACTION_RECTANGLE      :
    case G_ACTION_CERCLE         :
    case G_ACTION_QUART_ARC          :
    case G_ACTION_QUART_CERCLE     :
    case G_ACTION_H_DEMI_CERCLE        :
    case G_ACTION_V_DEMI_CERCLE        :
    case G_ACTION_TRI_RECT       :
    case G_ACTION_H_TRI_ISO      :
    case G_ACTION_V_TRI_ISO      :
    case G_ACTION_H_VANNE        :
    case G_ACTION_V_VANNE        :
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_H_TEXTE        :
    case G_ACTION_V_TEXTE        :
    case G_ACTION_COURBE_T        :
    case G_ACTION_COURBE_C        :
    case G_ACTION_COURBE_A        :
    case G_ACTION_BARGRAPHE        :
    case G_ACTION_EDIT_TEXTE  :
    case G_ACTION_STATIC_TEXTE  :
    case G_ACTION_EDIT_NUM  :
    case G_ACTION_STATIC_NUM  :
    case G_ACTION_ENTREE_LOG:

    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
    case G_ACTION_CONT_BOUTON_VAL:
			{
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint  (hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

			//$$ optimisable
			if ((pElement->nAction == G_ACTION_H_TEXTE) || (pElement->nAction == G_ACTION_V_TEXTE) ||
				(pElement->nAction == G_ACTION_EDIT_TEXTE) || (pElement->nAction == G_ACTION_STATIC_TEXTE) ||
				(pElement->nAction == G_ACTION_EDIT_NUM) || (pElement->nAction == G_ACTION_STATIC_NUM))
				{
				pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
				pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
				GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
				GElementSetTexte (hElmtGsys, pTxt->Texte);
				}

			lDistance = g_dist (hGsys, hElmtGsys, x, y);
			}
      break;

    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			// Soustraction Offset POLY au Point de Selection
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);
			GElementAjoutePoint  (hGsys, hElmtGsys, x, y);
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, -pElement->pt0.x, -pElement->pt0.y);
			g_element_get_point (hGsys, hElmtGsys, 0, &iNewX, &iNewY);

			// Definition des points du POLY
			GElementSupprimePoints (hElmtGsys);
			pGeoPointPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, pElement->pt1.y);
			for (wIndexPointPoly = 0; wIndexPointPoly < pGeoPointPoly->NbPoints; wIndexPointPoly++)
				{
				pPointPoly = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, pGeoPointPoly->PremierPoint + wIndexPointPoly);
				GElementAjoutePoint  (hGsys, hElmtGsys, pPointPoly->x, pPointPoly->y);
				}
			lDistance = g_dist (hGsys, hElmtGsys, iNewX, iNewY);
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			{
			// Soustraction Offset LISTE au Point de Selection
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);
			GElementAjoutePoint  (hGsys, hElmtGsys, x, y);
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, -pElement->pt0.x, -pElement->pt0.y);
			g_element_get_point (hGsys, hElmtGsys, 0, &iNewX, &iNewY);

			//Dist = min (distances � chaque element);
			pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
			wPremierElement = pGeoListe->PremierElement;
			wDernierElement = wPremierElement + pGeoListe->NbElements;
			lTmpDist = DistanceElementGR (B_ELEMENTS_LISTES_GR, wPremierElement, (int) iNewX, (int) iNewY, hGsys, hElmtGsys);
			lDistance = lTmpDist;
			for (wPositionElement = wPremierElement + 1; wPositionElement < wDernierElement; wPositionElement++)
				{
				lTmpDist = DistanceElementGR (B_ELEMENTS_LISTES_GR, wPositionElement, (int) iNewX, (int) iNewY, hGsys, hElmtGsys);
				if (lTmpDist < lDistance)
					{
					lDistance = lTmpDist;
					}
				}
			}
			break;

    case G_ACTION_NOP     :
			lDistance = 0;
			break;

    default:
			lDistance = 0;
			break;
    }

  return lDistance;
  }

// -------------------------------------------------------------------------
// Encombrement d'un �l�ment BdGr (Boite)
// -------------------------------------------------------------------------
void EncombrementElementGR (DWORD Repere, DWORD Position, PLONG x1, PLONG y1, PLONG x2, PLONG y2,
														HGSYS hGsys, HELEMENT hElmtGsys)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  POINT	pt0;
	POINT pt1;

  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE           :
    case G_ACTION_RECTANGLE      :
    case G_ACTION_CERCLE         :
    case G_ACTION_QUART_ARC          :
    case G_ACTION_QUART_CERCLE     :
    case G_ACTION_H_DEMI_CERCLE        :
    case G_ACTION_V_DEMI_CERCLE        :
    case G_ACTION_TRI_RECT       :
    case G_ACTION_H_TRI_ISO      :
    case G_ACTION_V_TRI_ISO      :
    case G_ACTION_H_VANNE        :
    case G_ACTION_V_VANNE        :
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_COURBE_T        :
    case G_ACTION_COURBE_C        :
    case G_ACTION_COURBE_A        :
    case G_ACTION_BARGRAPHE        :
    case G_ACTION_ENTREE_LOG    :
    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
    case G_ACTION_CONT_BOUTON_VAL:
			{
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint  (hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);
			g_EncombrementElement (hGsys, hElmtGsys, &pt0.x, &pt0.y, &pt1.x, &pt1.y);
			}
			break;


    case G_ACTION_H_TEXTE        :
    case G_ACTION_V_TEXTE        :
    case G_ACTION_EDIT_TEXTE  :
    case G_ACTION_STATIC_TEXTE  :
    case G_ACTION_EDIT_NUM  :
    case G_ACTION_STATIC_NUM  :
			{
			PGEO_TEXTES_GR	pGeoTxt;
			PTEXTES_GR			pTxt;

			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint  (hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

			pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
			pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
			GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
			GElementSetTexte (hElmtGsys, pTxt->Texte);

			g_EncombrementElement (hGsys, hElmtGsys, &pt0.x, &pt0.y, &pt1.x, &pt1.y);
			}
			break;

    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			{
			PGEO_POINTS_POLY_GR	pGeoPointPoly;
			PPOINTS_POLY_GR			pPointPoly;
			DWORD               wIndexPointPoly;

			// Definition des points du POLY
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementSupprimePoints (hElmtGsys);
			pGeoPointPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, pElement->pt1.y);
			for (wIndexPointPoly = 0; wIndexPointPoly < pGeoPointPoly->NbPoints; wIndexPointPoly++)
				{
				pPointPoly = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, pGeoPointPoly->PremierPoint + wIndexPointPoly);
				GElementAjoutePoint  (hGsys, hElmtGsys, pPointPoly->x, pPointPoly->y);
				}
			g_EncombrementElement (hGsys, hElmtGsys, &pt0.x, &pt0.y, &pt1.x, &pt1.y);

			// Addition Offset POLY � la BOITE
			GElementSetAction (hElmtGsys, G_ACTION_RECTANGLE);
			GElementSupprimePoints (hElmtGsys);
			GElementAjoutePoint  (hGsys, hElmtGsys, pt0.x, pt0.y); // pt0.x, pt1.y);
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			g_element_get_point (hGsys, hElmtGsys, 0, &pt0.x, &pt0.y); // &pt0.x, &pt1.y);
			GElementAjoutePoint  (hGsys, hElmtGsys, pt1.x, pt1.y); // pt1.x, pt0.y);
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			g_element_get_point (hGsys, hElmtGsys, 1, &pt1.x, &pt1.y); // &pt1.x, &pt0.y);
			}
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			{
			POINT ptTemp0;
			POINT ptTemp1;
			PGEO_ELEMENTS_LISTES_GR	pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
			DWORD   wPremierElement = pGeoListe->PremierElement;
			DWORD   wDernierElement = wPremierElement + pGeoListe->NbElements;
			DWORD   wPositionElement;

			EncombrementElementGR (B_ELEMENTS_LISTES_GR, wPremierElement, &ptTemp0.x, &ptTemp0.y, &ptTemp1.x, &ptTemp1.y, hGsys, hElmtGsys);
			pt0 = ptTemp0;
			pt1 = ptTemp1;
			for (wPositionElement = wPremierElement + 1; wPositionElement < wDernierElement; wPositionElement++)
				{
				EncombrementElementGR (B_ELEMENTS_LISTES_GR, wPositionElement, &ptTemp0.x, &ptTemp0.y, &ptTemp1.x, &ptTemp1.y, hGsys, hElmtGsys);
				// diminue les valeurs min
				if (ptTemp0.x < pt0.x)
					pt0.x = ptTemp0.x;

				if (ptTemp0.y < pt0.y)
					pt0.y = ptTemp0.y;

				// augmente les valeurs max
				if (ptTemp1.x > pt1.x)
					pt1.x = ptTemp1.x;

				if (ptTemp1.y > pt1.y)
					pt1.y = ptTemp1.y;
				}

			// Addition Offset LISTE � la BOITE
			GElementSetAction (hElmtGsys, G_ACTION_RECTANGLE);
			GElementSupprimePoints (hElmtGsys);
			GElementAjoutePoint  (hGsys, hElmtGsys, pt0.x, pt0.y);
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			g_element_get_point (hGsys, hElmtGsys, 0, &pt0.x, &pt0.y);
			GElementAjoutePoint  (hGsys, hElmtGsys, pt1.x, pt1.y);
			GElementDeplaceDernierPoint (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
			g_element_get_point (hGsys, hElmtGsys, 1, &pt1.x, &pt1.y);
			}
      break;

    default:
			//case G_ACTION_NOP     :
			pt0.x = 0;
			pt0.y = 0;
			pt1.x = 0;
			pt1.y = 0;
			break;
    }
  *x1 = pt0.x;
  *y1 = pt0.y;
  *x2 = pt1.x;
  *y2 = pt1.y;
  } // EncombrementElementGR


// -------------------------------------------------------------------------
// r�cup�rer les 2 points d'un �l�ment graphique
void bdgr_point_ref_element (HBDGR hbdgr, DWORD Element, PPOINT	pPoint0, PPOINT	pPoint1)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  *pPoint0 = POINT_NULL;
  *pPoint1 = POINT_NULL;

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR							pBdGr = (PBDGR)hbdgr;
			DWORD							wPositionElement = Element + pPage->PremierElement - 1;
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
			HELEMENT					hElmtGsys = pBdGr->helementTravail;
			PGEO_TEXTES_GR		pGeoTxt;
			PTEXTES_GR				pTxt;
			LONG              iXLeft;
			LONG              iYBottom;
			LONG              iXRight;
			LONG              iYTop;

			switch (pElement->nAction)
				{
				case G_ACTION_LIGNE:
				case G_ACTION_RECTANGLE:
				case G_ACTION_CERCLE:
				case G_ACTION_QUART_ARC:
				case G_ACTION_QUART_CERCLE:
				case G_ACTION_H_DEMI_CERCLE:
				case G_ACTION_V_DEMI_CERCLE:
				case G_ACTION_TRI_RECT:
				case G_ACTION_H_TRI_ISO:
				case G_ACTION_V_TRI_ISO:
				case G_ACTION_H_VANNE:
				case G_ACTION_V_VANNE:
				case G_ACTION_RECTANGLE_PLEIN:
				case G_ACTION_CERCLE_PLEIN:
				case G_ACTION_QUART_ARC_PLEIN:
				case G_ACTION_QUART_CERCLE_PLEIN:
				case G_ACTION_H_DEMI_CERCLE_PLEIN:
				case G_ACTION_V_DEMI_CERCLE_PLEIN:
				case G_ACTION_TRI_RECT_PLEIN:
				case G_ACTION_H_TRI_ISO_PLEIN:
				case G_ACTION_V_TRI_ISO_PLEIN:
				case G_ACTION_H_VANNE_PLEIN:
				case G_ACTION_V_VANNE_PLEIN:
				case G_ACTION_H_TEXTE:
				case G_ACTION_V_TEXTE:
				case G_ACTION_COURBE_T:
				case G_ACTION_COURBE_C:
				case G_ACTION_COURBE_A:
				case G_ACTION_BARGRAPHE:
				case G_ACTION_EDIT_TEXTE:
				case G_ACTION_STATIC_TEXTE:
				case G_ACTION_EDIT_NUM:
				case G_ACTION_STATIC_NUM:
				case G_ACTION_ENTREE_LOG:

				case G_ACTION_CONT_STATIC:
				case G_ACTION_CONT_EDIT:
				case G_ACTION_CONT_GROUP:
				case G_ACTION_CONT_BOUTON:
				case G_ACTION_CONT_CHECK:
				case G_ACTION_CONT_RADIO:
				case G_ACTION_CONT_COMBO:
				case G_ACTION_CONT_LIST:
				case G_ACTION_CONT_SCROLL_H:
				case G_ACTION_CONT_SCROLL_V:
				case G_ACTION_CONT_BOUTON_VAL:
					{
					GElementSetAction (hElmtGsys, pElement->nAction);
					GElementResetBiPoint  (pBdGr->hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);

					if ((pElement->nAction == G_ACTION_H_TEXTE) || (pElement->nAction == G_ACTION_V_TEXTE) ||
						(pElement->nAction == G_ACTION_EDIT_TEXTE) || (pElement->nAction == G_ACTION_STATIC_TEXTE) ||
						(pElement->nAction == G_ACTION_EDIT_NUM) || (pElement->nAction == G_ACTION_STATIC_NUM))
						{
						pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
						pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
						GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
						GElementSetTexte (hElmtGsys, pTxt->Texte);
						}

					g_EncombrementElement (pBdGr->hGsys, hElmtGsys, &iXLeft, &iYTop, &iXRight, &iYBottom);
					}
					break;

				case G_ACTION_POLYGONE:
				case G_ACTION_POLYGONE_PLEIN:
				case G_ACTION_LISTE:
				case G_ACTION_LISTE_PLEIN:
					GElementSetAction (hElmtGsys, pElement->nAction);
					GElementSupprimePoints (hElmtGsys);
					GElementAjoutePoint  (pBdGr->hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
					g_element_get_point (pBdGr->hGsys, hElmtGsys, 0, &iXLeft, &iYTop);
					iXRight = 0;
					iYBottom = 0;
					break;

				case G_ACTION_NOP:
				default:
					iXLeft   = 0;
					iYBottom = 0;
					iXRight  = 0;
					iYTop    = 0;
					break;
				}
			pPoint0->x = iXLeft;
			pPoint0->y = iYTop;
			pPoint1->x = iXRight;
			pPoint1->y = iYBottom;
      }
    }
  } // bdgr_point_ref_element


// -------------------------------------------------------------------------
// Regarde si le point(x,y) est sur un �l�ment texte et r�cup�re ses caract�ristiques
// -------------------------------------------------------------------------
DWORD PointerTexteElementGR (DWORD Repere, DWORD Position, LONG x, LONG y, HGSYS hGsys, 
	HELEMENT hElmtGsys, PLONG X1Car, PLONG Y1Car, PLONG X2Car, PLONG Y2Car)
  {
  DWORD							wPositionTexte = 0;
  PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);

  PGEO_TEXTES_GR		pGeoTxt;
  PTEXTES_GR				pTxt;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD             wPremierElement;
  DWORD             wDernierElement;
  DWORD             wPositionElement;
  LONG              iXLeft;
  LONG              iYBottom;
  LONG              iXRight;
  LONG              iYTop;
  LONG              iX;
  LONG              iY;

  switch (pElement->nAction)
		{
		case G_ACTION_EDIT_TEXTE:
		case G_ACTION_STATIC_TEXTE:
		case G_ACTION_EDIT_NUM:
		case G_ACTION_STATIC_NUM:
			{
			GElementSetAction (hElmtGsys, pElement->nAction);
			GElementResetBiPoint  (hGsys, hElmtGsys, &pElement->pt0, &pElement->pt1);
			g_element_move       (hGsys, hElmtGsys, (*X1Car), (*Y1Car));
			pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
			pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
			GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
			GElementSetTexte (hElmtGsys, pTxt->Texte);
			g_EncombrementElement (hGsys, hElmtGsys, &iXLeft, &iYTop, &iXRight, &iYBottom);
			if ((x > (int) iXLeft) && (x < (int) iXRight) && (y > (int) iYBottom) && (y < (int) iYTop))
				{
				wPositionTexte = pGeoTxt->ReferenceTexte;
				g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
				(*X1Car) = (int) iX;
				(*Y1Car) = (int) iY;
				g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
				(*X2Car) = (int) iX;
				(*Y2Car) = (int) iY;
				}
			}
		break;

		case G_ACTION_H_TEXTE:
		case G_ACTION_V_TEXTE:
			{
			POINT	pt0;

			GElementSetAction (hElmtGsys, pElement->nAction);
			pt0 = pElement->pt0;
	    g_add_points_page (hGsys, &pt0.x, &pt0.y, *X1Car, *Y1Car);

			GElementResetBiPoint  (hGsys, hElmtGsys, &pt0, &pElement->pt1);
			//g_element_move       (hGsys, hElmtGsys, (), ());
			pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);
			pTxt = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
			GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
			GElementSetTexte (hElmtGsys, pTxt->Texte);
			g_EncombrementElement (hGsys, hElmtGsys, &iXLeft, &iYTop, &iXRight, &iYBottom);
			if ((x > (int) iXLeft) && (x < (int) iXRight) && (y > (int) iYTop) && (y < (int) iYBottom))
				{
				wPositionTexte = pGeoTxt->ReferenceTexte;
				g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
				(*X1Car) = (int) iX;
				(*Y1Car) = (int) iY;
				g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
				(*X2Car) = (int) iX;
				(*Y2Car) = (int) iY;
				}
			}
			break;

		case G_ACTION_LISTE:
		case G_ACTION_LISTE_PLEIN:
      {
			int     OldX1Car = (*X1Car);
			int     OldY1Car = (*Y1Car);
      GElementSetAction (hElmtGsys, pElement->nAction);
      GElementSupprimePoints (hElmtGsys);
      GElementAjoutePoint  (hGsys, hElmtGsys, (*X1Car), (*Y1Car));
      GElementDeplaceDernierPoint (hGsys, hElmtGsys, pElement->pt0.x, pElement->pt0.y);
      g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
      (*X1Car) = (int) iX;
      (*Y1Car) = (int) iY;
      pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->pt1.y);
      wPremierElement  = pGeoListe->PremierElement;
      wDernierElement  = wPremierElement + pGeoListe->NbElements;
      wPositionElement = wPremierElement;
      while ((wPositionElement < wDernierElement) && (wPositionTexte == 0))
        {
        wPositionTexte = PointerTexteElementGR (B_ELEMENTS_LISTES_GR, wPositionElement, x, y, hGsys, hElmtGsys, X1Car, Y1Car, X2Car, Y2Car);
        wPositionElement++;
        }
      if (wPositionTexte == 0)
        {
        (*X1Car) = OldX1Car;
        (*Y1Car) = OldY1Car;
        }
      }
			break;
		} // switch (pElement->Action)
	/* $$ obsolete
  if ((pElement->Action == G_ACTION_H_TEXTE) || (pElement->Action == G_ACTION_V_TEXTE) ||
      (pElement->Action == G_ACTION_EDIT_TEXTE) || (pElement->Action == G_ACTION_STATIC_TEXTE) ||
      (pElement->Action == G_ACTION_EDIT_NUM) || (pElement->Action == G_ACTION_STATIC_NUM))
    {
    GElementSetAction (hElmtGsys, pElement->Action);
    GElementResetBiPoint  (hGsys, hElmtGsys, pElement->x1, pElement->y1, pElement->x2, pElement->y2);
    g_element_move       (hGsys, hElmtGsys, (*X1Car), (*Y1Car));
    pGeoTxt = pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->Style);
    pTxt = pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTxt->ReferenceTexte);
    GElementSetStyleTexte (hElmtGsys, pTxt->Police, pTxt->StylePolice);
    GElementSetTexte (hElmtGsys, pTxt->Texte);
    g_EncombrementElement (hGsys, hElmtGsys, &iXLeft, &iYTop, &iXRight, &iYBottom);
    if ((x > (int) iXLeft) && (x < (int) iXRight) && (y > (int) iYBottom) && (y < (int) iYTop))
      {
      wPositionTexte = pGeoTxt->ReferenceTexte;
      g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
      (*X1Car) = (int) iX;
      (*Y1Car) = (int) iY;
      g_element_get_point (hGsys, hElmtGsys, 1, &iX, &iY);
      (*X2Car) = (int) iX;
      (*Y2Car) = (int) iY;
      }
    }
  else
    {
    if ((pElement->Action == G_ACTION_LISTE) || (pElement->Action == G_ACTION_LISTE_PLEIN))
      {
      OldX1Car = (*X1Car);
      OldY1Car = (*Y1Car);
      GElementSetAction (hElmtGsys, pElement->Action);
      GElementSupprimePoints (hElmtGsys);
      GElementAjoutePoint  (hGsys, hElmtGsys, (*X1Car), (*Y1Car));
      GElementDeplaceDernierPoint (hGsys, hElmtGsys, pElement->x1, pElement->y1);
      g_element_get_point (hGsys, hElmtGsys, 0, &iX, &iY);
      (*X1Car) = (int) iX;
      (*Y1Car) = (int) iY;
      pGeoListe = pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElement->y2);
      wPremierElement  = pGeoListe->PremierElement;
      wDernierElement  = wPremierElement + pGeoListe->NbElements;
      wPositionElement = wPremierElement;
      while ((wPositionElement < wDernierElement) && (wPositionTexte == 0))
        {
        wPositionTexte = PointerTexteElementGR (B_ELEMENTS_LISTES_GR, wPositionElement, x, y, hGsys, hElmtGsys, X1Car, Y1Car, X2Car, Y2Car);
        wPositionElement++;
        }
      if (wPositionTexte == 0)
        {
        (*X1Car) = OldX1Car;
        (*Y1Car) = OldY1Car;
        }
      }
    }
*/
  return wPositionTexte;
  }

// -------------------------------------------------------------------------
// Duplication
// -------------------------------------------------------------------------
void CopierElementGR (DWORD RepereSource, DWORD PositionSource, DWORD RepereDestination, DWORD PositionDestination)
  {
  PELEMENT_PAGE_GR pElementDestination;
  PELEMENT_PAGE_GR pElementSource;
  DWORD                      wDernierGeo;
  DWORD                      wPositionGeo;
  PGEO_TEXTES_GR	pGeoTexte;
  DWORD                      wPositionSource;
  DWORD                      wPositionDestination;
  BOOL                   bPasTrouve;
  PGEO_POINTS_POLY_GR			pGeoPoly;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
		
  trans_enr (szVERIFSource, __LINE__, 1, RepereSource, RepereDestination, PositionSource, PositionDestination);
	
  pElementDestination = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, RepereDestination, PositionDestination);
  pElementDestination->PosGeoAnimation = 0;
  switch (pElementDestination->nAction)
    {
    case G_ACTION_H_TEXTE:
    case G_ACTION_V_TEXTE:
    case G_ACTION_EDIT_TEXTE:
    case G_ACTION_STATIC_TEXTE:
    case G_ACTION_EDIT_NUM:
    case G_ACTION_STATIC_NUM:

    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_BOUTON_VAL:
			{
			pElementDestination->hwndControle = NULL;
			if (pElementDestination->nStyleLigne != 0)
				{
				// Recherche Position Geo Texte VIDE
				wDernierGeo = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1;
				wPositionGeo = 1;
				bPasTrouve = TRUE;
				while ((wPositionGeo < wDernierGeo) && (bPasTrouve))
					{
					pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeo);
					bPasTrouve = (BOOL) (pGeoTexte->ReferenceTexte != 0);
					wPositionGeo++;
					}
				wPositionGeo--;
				
				// Si (PAS TROUVE) Alors Insertion Pos. Geo
				if (bPasTrouve)
					{
					pElementDestination->nStyleLigne = (G_STYLE_LIGNE)wDernierGeo;
					wPositionGeo = wDernierGeo;
					insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, wDernierGeo);
					}
				else
					{
					pElementDestination->nStyleLigne = (G_STYLE_LIGNE)wPositionGeo;
					}
				
				// Insertion Texte
				pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeo);
				wPositionDestination = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
				pGeoTexte->ReferenceTexte = wPositionDestination;
				insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, wPositionDestination);
				
				// Affectation Texte
				pElementSource = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, RepereSource, PositionSource);
				pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElementSource->nStyleLigne);
				wPositionSource = pGeoTexte->ReferenceTexte;
				trans_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, B_TEXTES_GR, wPositionSource, wPositionDestination);
				}
			}
			break;

    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
			pElementDestination->hwndControle = NULL;
			break;
			
    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, pElementDestination->pt1.y);
			pGeoPoly->NbReferencesPoly++;
			break;
			
    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, pElementDestination->pt1.y);
			pGeoListe->NbReferencesListe++;
			break;
			
		// case G_ACTION_NOP: break;
		// default: break;
    }
  } // CopierElementGR

// -----------------------------------------------------------------------------------------------
// Duplique un �l�ment graphique en l'ajoutant, s'il est anim� � la liste des �l�ments anim�s � dupliquer
void DupliquerElementGR (HBDGR hbdgr, DWORD RepereSource, DWORD PositionSource, DWORD RepereDestination, DWORD PositionDestination)
  {
  PELEMENT_PAGE_GR	pElementSource;
  PMEM_ANIMATION_GR	pMemAnimation;
  DWORD             nb_enr_mem_anim;
  DWORD             i;

	// Recopie l'�l�ment source en dest
  CopierElementGR (RepereSource, PositionSource, RepereDestination, PositionDestination);

	// Ajuste les positions des "�l�ments ayant des animations � dupliquer"
  nb_enr_mem_anim = nb_enregistrements (szVERIFSource, __LINE__, B_MEM_ANIMATION_GR);
  for (i = 1; i <= nb_enr_mem_anim; i++)
    {
    pMemAnimation = (PMEM_ANIMATION_GR)pointe_enr (szVERIFSource, __LINE__, B_MEM_ANIMATION_GR, i);
    if (pMemAnimation->wPositionSource >= PositionDestination)
      pMemAnimation->wPositionSource++;

    if (pMemAnimation->wPositionDestination >= PositionDestination)
      pMemAnimation->wPositionDestination++;
    }

	// Ajoute l'�l�ment source aux "�l�ments ayant des animations � dupliquer" s'il est anim�
  pElementSource = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, RepereSource, PositionSource);
  if (pElementSource->PosGeoAnimation != 0)
    {
	  nb_enr_mem_anim++;
    insere_enr (szVERIFSource, __LINE__, 1, B_MEM_ANIMATION_GR, nb_enr_mem_anim);
    pMemAnimation = (PMEM_ANIMATION_GR)pointe_enr (szVERIFSource, __LINE__, B_MEM_ANIMATION_GR, nb_enr_mem_anim);
    pMemAnimation->wPositionSource = PositionSource;
    pMemAnimation->wPositionDestination = PositionDestination;
    }
  } // DupliquerElementGR

// -------------------------------------------------------------------------
// Duplique �ventuellement les animations graphiques de la liste 
// des "�l�ments ayant des animations � dupliquer" en demandant � l'utilisateur 
// les param�tres d'animation suppl�mentaires
// et vide toujours la liste des �l�ments anim�s � dupliquer
BOOL DupliquerAnimationGR (HBDGR hbdgr, BOOL bDuplication)
  {
  BOOL bRes = TRUE;

	// Duplication des �l�ments de la liste ?
  if (bDuplication)
    {
		// oui => pr�pare le parcours de la liste
		DWORD			wNbEnrMemAnimation = nb_enregistrements (szVERIFSource, __LINE__, B_MEM_ANIMATION_GR);
		DWORD			wEnrMemAnimation;
		ANIMATION TypeAnimation;

		// pour chaque �l�ment
    for (wEnrMemAnimation = 1; wEnrMemAnimation <= wNbEnrMemAnimation; wEnrMemAnimation++)
      {
			DWORD							wError;
			DWORD             wLigneAnim = 0;
			PMEM_ANIMATION_GR	pMemAnimation = (PMEM_ANIMATION_GR)pointe_enr (szVERIFSource, __LINE__, B_MEM_ANIMATION_GR, wEnrMemAnimation);
			PELEMENT_PAGE_GR	pElementSource = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, pMemAnimation->wPositionSource);

			bdanmgr_TypeAnimation (pElementSource->nAction, &TypeAnimation);
			wError = bdanmgr_parametrer (TypeAnimation, hbdgr, pElementSource->PosGeoAnimation, &wLigneAnim, pMemAnimation->wPositionSource);

      if (wError == 0)
        {
				PELEMENT_PAGE_GR pElementDestination = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, pMemAnimation->wPositionDestination);

        pElementDestination->PosGeoAnimation = wLigneAnim;
				bdanmgr_maj_identifiant_graphique (wLigneAnim, pElementDestination->strIdentifiant);
        }
      else
        bRes = FALSE;
      }
    }
  vide_bloc (B_MEM_ANIMATION_GR);
  return bRes;
  } // DupliquerAnimationGR

// -------------------------------------------------------------------------
// Rendre unique un �lement GR et sauvegarde des Points de l'Element GR
// pour D�formation
// -------------------------------------------------------------------------
void RendreUniqueEtSauveElementGR (DWORD Repere, DWORD Position, DWORD *PositionSauvegarde)
  {
  PPOINT									pSav;
  PELEMENT_PAGE_GR				pElement;
  DWORD                   wPositionGeoSource;
  DWORD                   wPositionGeoDestination;
  DWORD                   wDernierGeo;
  DWORD                   wPremierSource;
  DWORD                   wPremierDestination;
  DWORD                   wNb;
  BOOL                    bPasTrouve;
  PGEO_POINTS_POLY_GR			pGeoPoly;
  PPOINTS_POLY_GR				 pPointPoly;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD                   wIndex;

  pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  switch (pElement->nAction)
    {
    case G_ACTION_LIGNE           :
    case G_ACTION_RECTANGLE      :
    case G_ACTION_CERCLE         :
    case G_ACTION_QUART_ARC          :
    case G_ACTION_QUART_CERCLE     :
    case G_ACTION_H_DEMI_CERCLE        :
    case G_ACTION_V_DEMI_CERCLE        :
    case G_ACTION_TRI_RECT       :
    case G_ACTION_H_TRI_ISO      :
    case G_ACTION_V_TRI_ISO      :
    case G_ACTION_H_VANNE        :
    case G_ACTION_V_VANNE        :
    case G_ACTION_RECTANGLE_PLEIN :
    case G_ACTION_CERCLE_PLEIN    :
    case G_ACTION_QUART_ARC_PLEIN     :
    case G_ACTION_QUART_CERCLE_PLEIN:
    case G_ACTION_H_DEMI_CERCLE_PLEIN   :
    case G_ACTION_V_DEMI_CERCLE_PLEIN   :
    case G_ACTION_TRI_RECT_PLEIN  :
    case G_ACTION_H_TRI_ISO_PLEIN :
    case G_ACTION_V_TRI_ISO_PLEIN :
    case G_ACTION_H_VANNE_PLEIN   :
    case G_ACTION_V_VANNE_PLEIN   :
    case G_ACTION_H_TEXTE        :
    case G_ACTION_V_TEXTE        :
    case G_ACTION_COURBE_T        :
    case G_ACTION_COURBE_C        :
    case G_ACTION_COURBE_A        :
    case G_ACTION_BARGRAPHE        :
    case G_ACTION_EDIT_TEXTE  :
    case G_ACTION_STATIC_TEXTE  :
    case G_ACTION_EDIT_NUM  :
    case G_ACTION_STATIC_NUM  :

    case G_ACTION_ENTREE_LOG    :
    case G_ACTION_CONT_STATIC:
    case G_ACTION_CONT_EDIT:
    case G_ACTION_CONT_GROUP:
    case G_ACTION_CONT_BOUTON:
    case G_ACTION_CONT_CHECK:
    case G_ACTION_CONT_RADIO:
    case G_ACTION_CONT_COMBO:
    case G_ACTION_CONT_LIST:
    case G_ACTION_CONT_SCROLL_H:
    case G_ACTION_CONT_SCROLL_V:
    case G_ACTION_CONT_BOUTON_VAL:
			insere_enr (szVERIFSource, __LINE__, 2, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			pSav->x = pElement->pt0.x;
			pSav->y = pElement->pt0.y;
			(*PositionSauvegarde)++;
			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			pSav->x = pElement->pt1.x;
			pSav->y = pElement->pt1.y;
			(*PositionSauvegarde)++;
			break;

    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			insere_enr (szVERIFSource, __LINE__, 1, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			pSav->x = pElement->pt0.x;
			pSav->y = pElement->pt0.y;
			(*PositionSauvegarde)++;

			wPositionGeoSource = pElement->pt1.y;
			pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wPositionGeoSource);

			wPremierSource = pGeoPoly->PremierPoint;
			wNb            = pGeoPoly->NbPoints;
			if (pGeoPoly->NbReferencesPoly > 1)
				{
				pGeoPoly->NbReferencesPoly--;

				// Recherche Position Geo Pt. Poly VIDE
				wDernierGeo = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR) + 1;
				wPositionGeoDestination = 1;
				bPasTrouve = TRUE;
				while ((wPositionGeoDestination < wDernierGeo) && (bPasTrouve))
					{
					pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wPositionGeoDestination);
					bPasTrouve = (BOOL) (pGeoPoly->NbReferencesPoly != 0);
					wPositionGeoDestination++;
					}
				wPositionGeoDestination--;

				// Si (PAS TROUVE) Alors Insertion Pos. Geo
				if (bPasTrouve)
					{
					pElement->pt1.y = wDernierGeo;
					wPositionGeoDestination = wDernierGeo;
					insere_enr (szVERIFSource, __LINE__, 1, B_GEO_POINTS_POLY_GR, wDernierGeo);
					}
				else
					{
					pElement->pt1.y = wPositionGeoDestination;
					}

				// Insertion Points
				pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wPositionGeoDestination);
				wPremierDestination = nb_enregistrements (szVERIFSource, __LINE__, B_POINTS_POLY_GR) + 1;
				pGeoPoly->PremierPoint     = wPremierDestination;
				pGeoPoly->NbPoints         = wNb;
				pGeoPoly->NbReferencesPoly = 1;
				insere_enr (szVERIFSource, __LINE__, wNb, B_POINTS_POLY_GR, wPremierDestination);

				// Affectation Points
				trans_enr (szVERIFSource, __LINE__, wNb, B_POINTS_POLY_GR, B_POINTS_POLY_GR, wPremierSource, wPremierDestination);
				}

			insere_enr (szVERIFSource, __LINE__, wNb, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			for (wIndex = 0; wIndex < wNb; wIndex++)
				{
				pPointPoly = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, wPremierSource + wIndex);
				pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
				pSav->x = pPointPoly->x;
				pSav->y = pPointPoly->y;
				(*PositionSauvegarde)++;
				}
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			insere_enr (szVERIFSource, __LINE__, 1, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			pSav = (PPOINT)pointe_enr (szVERIFSource, __LINE__, B_POINTS_SAUVEGARDES_GR, (*PositionSauvegarde));
			pSav->x = pElement->pt0.x;
			pSav->y = pElement->pt0.y;
			(*PositionSauvegarde)++;

			wPositionGeoSource = pElement->pt1.y;
			pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPositionGeoSource);
			wPremierSource = pGeoListe->PremierElement;
			wNb            = pGeoListe->NbElements;

			if (pGeoListe->NbReferencesListe > 1)
				{
				pGeoListe->NbReferencesListe--;

				// Recherche Position Geo Liste VIDE
				wDernierGeo = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR) + 1;
				wPositionGeoDestination = 1;
				bPasTrouve = TRUE;
				while ((wPositionGeoDestination < wDernierGeo) && (bPasTrouve))
					{
					pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPositionGeoDestination);
					bPasTrouve = (BOOL) (pGeoListe->NbReferencesListe != 0);
					wPositionGeoDestination++;
					}
				wPositionGeoDestination--;

				// Si (PAS TROUVE) Alors Insertion Pos. Geo
				if (bPasTrouve)
					{
					pElement->pt1.y = wDernierGeo;
					wPositionGeoDestination = wDernierGeo;
					insere_enr (szVERIFSource, __LINE__, 1, B_GEO_ELEMENTS_LISTES_GR, wDernierGeo);
					}
				else
					{
					pElement->pt1.y = wPositionGeoDestination;
					}

				// Insertion Elements Liste
				pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPositionGeoDestination);
				wPremierDestination = nb_enregistrements (szVERIFSource, __LINE__, B_ELEMENTS_LISTES_GR) + 1;
				pGeoListe->PremierElement     = wPremierDestination;
				pGeoListe->NbElements         = wNb;
				pGeoListe->NbReferencesListe  = 1;
				insere_enr (szVERIFSource, __LINE__, wNb, B_ELEMENTS_LISTES_GR, wPremierDestination);

				// Affectation Elements Liste et Rendre Unique les Sous Elements de la Liste
				for (wIndex = 0; wIndex < wNb; wIndex++)
					{
					CopierElementGR (B_ELEMENTS_LISTES_GR, wPremierSource + wIndex, B_ELEMENTS_LISTES_GR, wPremierDestination + wIndex);
					RendreUniqueEtSauveElementGR (B_ELEMENTS_LISTES_GR, wPremierDestination + wIndex, PositionSauvegarde);
					}
				}
			else
				{
				// Rendre Unique les Sous Elements de la Liste
				for (wIndex = 0; wIndex < wNb; wIndex++)
					{
					RendreUniqueEtSauveElementGR (B_ELEMENTS_LISTES_GR, wPremierSource + wIndex, PositionSauvegarde);
					}
				}
			break;

    // case G_ACTION_NOP: default: break;
    }
  }

// -------------------------------------------------------------------------
// Rendre unique un �lement GR pour Rotation, Sym�trie
// -------------------------------------------------------------------------
void RendreUniqueElementGR (DWORD Repere, DWORD Position)
  {
  PELEMENT_PAGE_GR pElement;
  DWORD                       wPositionGeoSource;
  DWORD                       wPositionGeoDestination;
  DWORD                       wDernierGeo;
  DWORD                       wPremierSource;
  DWORD                       wPremierDestination;
  DWORD                       wNb;
  BOOL                    bPasTrouve;
  PGEO_POINTS_POLY_GR		pGeoPoly;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;
  DWORD                       wIndex;

  pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, Repere, Position);
  switch (pElement->nAction)
    {
    case G_ACTION_POLYGONE     :
    case G_ACTION_POLYGONE_PLEIN:
			wPositionGeoSource = pElement->pt1.y;
			pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wPositionGeoSource);

			if (pGeoPoly->NbReferencesPoly > 1)
				{
				pGeoPoly->NbReferencesPoly--;
				wPremierSource = pGeoPoly->PremierPoint;
				wNb            = pGeoPoly->NbPoints;

				// Recherche Position Geo Pt. Poly VIDE
				wDernierGeo = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR) + 1;
				wPositionGeoDestination = 1;
				bPasTrouve = TRUE;
				while ((wPositionGeoDestination < wDernierGeo) && (bPasTrouve))
					{
					pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wPositionGeoDestination);
					bPasTrouve = (BOOL) (pGeoPoly->NbReferencesPoly != 0);
					wPositionGeoDestination++;
					}
				wPositionGeoDestination--;

				// Si (PAS TROUVE) Alors Insertion Pos. Geo
				if (bPasTrouve)
					{
					pElement->pt1.y = wDernierGeo;
					wPositionGeoDestination = wDernierGeo;
					insere_enr (szVERIFSource, __LINE__, 1, B_GEO_POINTS_POLY_GR, wDernierGeo);
					}
				else
					{
					pElement->pt1.y = wPositionGeoDestination;
					}

				// Insertion Points
				pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wPositionGeoDestination);
				wPremierDestination = nb_enregistrements (szVERIFSource, __LINE__, B_POINTS_POLY_GR) + 1;
				pGeoPoly->PremierPoint     = wPremierDestination;
				pGeoPoly->NbPoints         = wNb;
				pGeoPoly->NbReferencesPoly = 1;
				insere_enr (szVERIFSource, __LINE__, wNb, B_POINTS_POLY_GR, wPremierDestination);

				// Affectation Points
				trans_enr (szVERIFSource, __LINE__, wNb, B_POINTS_POLY_GR, B_POINTS_POLY_GR, wPremierSource, wPremierDestination);
				}
			break;

    case G_ACTION_LISTE:
    case G_ACTION_LISTE_PLEIN:
			wPositionGeoSource = pElement->pt1.y;
			pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPositionGeoSource);
			wPremierSource = pGeoListe->PremierElement;
			wNb            = pGeoListe->NbElements;

			if (pGeoListe->NbReferencesListe > 1)
				{
				pGeoListe->NbReferencesListe--;

				// Recherche Position Geo Liste VIDE
				wDernierGeo = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR) + 1;
				wPositionGeoDestination = 1;
				bPasTrouve = TRUE;
				while ((wPositionGeoDestination < wDernierGeo) && (bPasTrouve))
					{
					pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPositionGeoDestination);
					bPasTrouve = (BOOL) (pGeoListe->NbReferencesListe != 0);
					wPositionGeoDestination++;
					}
				wPositionGeoDestination--;

				// Si (PAS TROUVE) Alors Insertion Pos. Geo
				if (bPasTrouve)
					{
					pElement->pt1.y = wDernierGeo;
					wPositionGeoDestination = wDernierGeo;
					insere_enr (szVERIFSource, __LINE__, 1, B_GEO_ELEMENTS_LISTES_GR, wDernierGeo);
					}
				else
					{
					pElement->pt1.y = wPositionGeoDestination;
					}

				// Insertion Elements Liste
				pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPositionGeoDestination);
				wPremierDestination = nb_enregistrements (szVERIFSource, __LINE__, B_ELEMENTS_LISTES_GR) + 1;
				pGeoListe->PremierElement     = wPremierDestination;
				pGeoListe->NbElements         = wNb;
				pGeoListe->NbReferencesListe  = 1;
				insere_enr (szVERIFSource, __LINE__, wNb, B_ELEMENTS_LISTES_GR, wPremierDestination);

				// Affectation Elements Liste et Rendre Unique les Sous Elements de la Liste
				//
				for (wIndex = 0; wIndex < wNb; wIndex++)
					{
					CopierElementGR (B_ELEMENTS_LISTES_GR, wPremierSource + wIndex, B_ELEMENTS_LISTES_GR, wPremierDestination + wIndex);
					RendreUniqueElementGR (B_ELEMENTS_LISTES_GR, wPremierDestination + wIndex);
					}
				}
			else
				{
				// Rendre Unique les Sous Elements de la Liste
				for (wIndex = 0; wIndex < wNb; wIndex++)
					{
					RendreUniqueElementGR (B_ELEMENTS_LISTES_GR, wPremierSource + wIndex);
					}
				}
			break;

    // case G_ACTION_NOP: default: break;
    }
  }

// -------------------------------------------------------------------------
// Maj
// -------------------------------------------------------------------------
void MajGeoListesGR (MAJ_BDGR TypeMaj, DWORD Ref, DWORD PosEnr, DWORD NbEnr)
  {
  DWORD                   wPremierEnr;
  DWORD                   wDernierEnr;
  DWORD                   wEnr;
  PGEO_ELEMENTS_LISTES_GR	pGeoListe;

  wPremierEnr = 1;
  wDernierEnr = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR) + 1;

  switch (TypeMaj)
    {
    case MAJ_INSERTION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wEnr);
				if (pGeoListe->PremierElement > PosEnr)
					pGeoListe->PremierElement = pGeoListe->PremierElement + NbEnr;
				else
					{
					if ((pGeoListe->PremierElement == PosEnr) && (wEnr != Ref))
						pGeoListe->PremierElement = pGeoListe->PremierElement + NbEnr;
					}
				}
			break;
    case MAJ_SUPPRESSION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wEnr);
				if (pGeoListe->PremierElement >= (PosEnr + NbEnr))
					pGeoListe->PremierElement = pGeoListe->PremierElement - NbEnr;
				}
			break;
    }
  }

// -------------------------------------------------------------------------
void MajGeoTextesGR (MAJ_BDGR TypeMaj, DWORD PosEnr, DWORD NbEnr)
  {
  DWORD  wPremierEnr = 1;
  DWORD  wDernierEnr = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1;
  DWORD  wEnr;

  switch (TypeMaj)
    {
    case MAJ_INSERTION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				PGEO_TEXTES_GR	pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wEnr);

				if (pGeoTexte->ReferenceTexte >= PosEnr)
					pGeoTexte->ReferenceTexte = pGeoTexte->ReferenceTexte + NbEnr;
				}
			break;
    case MAJ_SUPPRESSION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				PGEO_TEXTES_GR	pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wEnr);

				if (pGeoTexte->ReferenceTexte >= (PosEnr + NbEnr))
					pGeoTexte->ReferenceTexte = pGeoTexte->ReferenceTexte - NbEnr;
				}
			break;
    }
  }

// -------------------------------------------------------------------------
void MajGeoPolyGR (MAJ_BDGR TypeMaj, DWORD Ref, DWORD PosEnr, DWORD NbEnr)
  {
  DWORD wPremierEnr = 1;
  DWORD wDernierEnr = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR) + 1;
  DWORD wEnr;

  switch (TypeMaj)
    {
    case MAJ_INSERTION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				PGEO_POINTS_POLY_GR	pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wEnr);

				if (pGeoPoly->PremierPoint > PosEnr)
					pGeoPoly->PremierPoint = pGeoPoly->PremierPoint + NbEnr;
				else
					{
					if ((pGeoPoly->PremierPoint == PosEnr) && (wEnr != Ref))
						pGeoPoly->PremierPoint = pGeoPoly->PremierPoint + NbEnr;
					}
				}
			break;
    case MAJ_SUPPRESSION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				PGEO_POINTS_POLY_GR	pGeoPoly = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wEnr);

				if (pGeoPoly->PremierPoint >= (PosEnr + NbEnr))
					pGeoPoly->PremierPoint = pGeoPoly->PremierPoint - NbEnr;
				}
			break;
    }
  }

// -------------------------------------------------------------------------
// Insere un nouvel �l�ment graphique dans la base de donn�es Processyn
// Renvoie son num�ro ou 0 si ajout impossible
DWORD dwValiderElementGr (HBDGR hbdgr, HELEMENT helementEpure)
  {
  PBDGR			pBdGr = (PBDGR)hbdgr;
  PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  BOOL      bElementValide = TRUE;
  DWORD     wElement = 0;

  G_ATTRIBUT			wAttributs;
  G_ACTION     wAction;
  G_COULEUR     nCouleurLigne;
  G_COULEUR     nCouleurRemplissage;
  G_STYLE_LIGNE     wStyleLigne;
  G_STYLE_REMPLISSAGE	wStyleRemplissage;
  DWORD     wPolice;
  DWORD     wStylePolice;
  char *    pszTexte;
  DWORD     wNbPoints;

  LONG      x1;
  LONG      y1;
  LONG      x2;
  LONG      y2;

  // Lecture Element Gsys
  g_element_get (pBdGr->hGsys, helementEpure, &wAttributs,
                 &wAction, &nCouleurLigne, &nCouleurRemplissage, &wStyleLigne, &wStyleRemplissage,
                 &wPolice, &wStylePolice, &pszTexte, &wNbPoints);

  // Test Validit� Element
  if (wNbPoints < 2)
    {
    bElementValide = FALSE;
    }
  else
    {
    if ((wAction != G_ACTION_POLYGONE) && (wAction != G_ACTION_POLYGONE_PLEIN))
      {
      g_element_get_point (pBdGr->hGsys, helementEpure, 0, &x1, &y1);
      g_element_get_point (pBdGr->hGsys, helementEpure, 1, &x2, &y2);
      if ((x1 == x2) && (y1 == y2))
        {
        bElementValide = FALSE;
        }
      }
    }

  if ((wAction == G_ACTION_H_TEXTE) || (wAction == G_ACTION_V_TEXTE) ||
      (wAction == G_ACTION_EDIT_TEXTE) || (wAction == G_ACTION_STATIC_TEXTE) ||
      (wAction == G_ACTION_EDIT_NUM) || (wAction == G_ACTION_STATIC_NUM))
    {
    if (pszTexte == NULL)
      {
      bElementValide = FALSE;
      }
    else
      {
      if (pszTexte [0] == '\0')
        {
        bElementValide = FALSE;
        }
      }
    }

  // Element Valide et page existe ?
  if (bElementValide && pPage)
    {
    // oui => insertion de l'�lement en derni�re position dans la page
		LONG                x;
		LONG                y;
		//DWORD               wReferencePage;
		DWORD               wPositionElement;
		PELEMENT_PAGE_GR	 pElement;
		//
		BOOL                bPasTrouve;
		//
		DWORD               wDernierGeoTexte;
		DWORD               wPositionGeoTexte;
		PGEO_TEXTES_GR			pGeoTexte;
		DWORD               wPositionTexte;
		PTEXTES_GR					pTexteGr;
		//
		DWORD               wDernierGeoPoint;
		DWORD               wPositionGeoPoint;
		PGEO_POINTS_POLY_GR	pGeoPoint;
		DWORD               wPositionPoint;
		PPOINTS_POLY_GR			pPoint;
		DWORD               iPoint;

    pPage->NbElements++;
    wElement = pPage->NbElements;
    wPositionElement = pPage->PremierElement + wElement - 1;
    insere_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_PAGES_GR, wPositionElement);
    pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
		pElement->hwndControle = NULL;
    pElement->PosGeoAnimation = 0;
		GetIdAutoPcs(ELEMENT_PAGE_GRAPHIQUE, pElement->strIdentifiant);

    // Affectation Element GR
    switch (wAction)
      {
      case G_ACTION_LIGNE          :
      case G_ACTION_RECTANGLE     :
      case G_ACTION_CERCLE        :
      case G_ACTION_QUART_ARC         :
      case G_ACTION_QUART_CERCLE    :
      case G_ACTION_H_DEMI_CERCLE       :
      case G_ACTION_V_DEMI_CERCLE       :
      case G_ACTION_TRI_RECT      :
      case G_ACTION_H_TRI_ISO     :
      case G_ACTION_V_TRI_ISO     :
      case G_ACTION_H_VANNE       :
      case G_ACTION_V_VANNE       :
      case G_ACTION_COURBE_T       :
      case G_ACTION_COURBE_C       :
      case G_ACTION_COURBE_A       :
      case G_ACTION_ENTREE_LOG   :
				{
				pElement->nAction  = wAction;
				pElement->nCouleurLigne = nCouleurLigne;
				pElement->nCouleurRemplissage = G_COULEUR_TRANSPARENTE;
				pElement->nStyleLigne   = wStyleLigne;
				pElement->nStyleRemplissage = G_STYLE_REMPLISSAGE_INDETERMINE;
				x = 0;
				y = 0;
				g_element_get_point (pBdGr->hGsys, helementEpure, 0, &x, &y);
				pElement->pt0.x      = x;
				pElement->pt0.y      = y;
				g_element_get_point (pBdGr->hGsys, helementEpure, 1, &x, &y);
				pElement->pt1.x      = x;
				pElement->pt1.y      = y;
				}
				break;

      case G_ACTION_RECTANGLE_PLEIN :
      case G_ACTION_CERCLE_PLEIN    :
      case G_ACTION_QUART_ARC_PLEIN     :
      case G_ACTION_QUART_CERCLE_PLEIN:
      case G_ACTION_H_DEMI_CERCLE_PLEIN   :
      case G_ACTION_V_DEMI_CERCLE_PLEIN   :
      case G_ACTION_TRI_RECT_PLEIN  :
      case G_ACTION_H_TRI_ISO_PLEIN :
      case G_ACTION_V_TRI_ISO_PLEIN :
      case G_ACTION_H_VANNE_PLEIN   :
      case G_ACTION_V_VANNE_PLEIN   :
      case G_ACTION_BARGRAPHE        :
				{
				pElement->nAction  = wAction;
				pElement->nCouleurLigne = G_COULEUR_TRANSPARENTE;
				pElement->nCouleurRemplissage = nCouleurRemplissage;
				pElement->nStyleLigne   = wStyleLigne;
				pElement->nStyleRemplissage = wStyleRemplissage;
				x = 0;
				y = 0;
				g_element_get_point (pBdGr->hGsys, helementEpure, 0, &x, &y);
				pElement->pt0.x      = x;
				pElement->pt0.y      = y;
				g_element_get_point (pBdGr->hGsys, helementEpure, 1, &x, &y);
				pElement->pt1.x      = x;
				pElement->pt1.y      = y;
				}
				break;

      case G_ACTION_H_TEXTE        :
      case G_ACTION_V_TEXTE        :
      case G_ACTION_EDIT_TEXTE  :
      case G_ACTION_STATIC_TEXTE  :
      case G_ACTION_EDIT_NUM  :
      case G_ACTION_STATIC_NUM  :
				{
				pElement->nAction  = wAction;
				pElement->nCouleurLigne = nCouleurLigne; // $$ v�rifier
				pElement->nCouleurRemplissage = G_COULEUR_TRANSPARENTE; //nCouleurRemplissage;
				x = 0;
				y = 0;
				g_element_get_point (pBdGr->hGsys, helementEpure, 0, &x, &y);
				pElement->pt0.x      = x;
				pElement->pt0.y      = y;
				g_element_get_point (pBdGr->hGsys, helementEpure, 1, &x, &y);
				pElement->pt1.x      = x;
				pElement->pt1.y      = y;

				// Recherche une Position Geo Texte VIDE
				wDernierGeoTexte = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1;
				wPositionGeoTexte = 1;
				bPasTrouve = TRUE;
				while ((wPositionGeoTexte < wDernierGeoTexte) && (bPasTrouve))
					{
					pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
					bPasTrouve = (BOOL) (pGeoTexte->ReferenceTexte != 0);
					wPositionGeoTexte++;
					}
				wPositionGeoTexte--;

				// Si (PAS TROUVE) Alors Insertion Pos. Geo
				if (bPasTrouve)
					{
					pElement->nStyleLigne = (G_STYLE_LIGNE)wDernierGeoTexte;
					wPositionGeoTexte = wDernierGeoTexte;
					insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, wDernierGeoTexte);
					}
				else
					pElement->nStyleLigne = (G_STYLE_LIGNE)wPositionGeoTexte;

				// Insertion du texte 
				pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
				wPositionTexte = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
				pGeoTexte->ReferenceTexte = wPositionTexte;
				insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, wPositionTexte);

				// Affectation du texte
				pTexteGr = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
				pTexteGr->Police = wPolice;
				pTexteGr->StylePolice = wStylePolice;
				StrExtract (pTexteGr->Texte, pszTexte, 0, c_NB_CAR_TEXTE_GR);
				}
				break;

      case G_ACTION_POLYGONE     :
      case G_ACTION_POLYGONE_PLEIN:
				{
				pElement->nAction  = wAction;
				pElement->nCouleurLigne = nCouleurLigne;
				pElement->nCouleurRemplissage = nCouleurRemplissage; //G_COULEUR_TRANSPARENTE;
				pElement->nStyleLigne = wStyleLigne;
				pElement->nStyleRemplissage = wStyleRemplissage;
				if (wAction == G_ACTION_POLYGONE)
					{
					pElement->nCouleurRemplissage = G_COULEUR_TRANSPARENTE;
					}
				if (wAction == G_ACTION_POLYGONE_PLEIN)
					{
					pElement->nCouleurLigne = G_COULEUR_TRANSPARENTE;
					}
				pElement->pt0.x = 0;
				pElement->pt0.y = 0;
				pElement->pt1.x = 0;
				//$$ ?pElement->pt1.y = 0;

				// Recherche une position Geo point Poly VIDE
				wDernierGeoPoint = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR) + 1;
				wPositionGeoPoint = 1;
				bPasTrouve = TRUE;
				while ((wPositionGeoPoint < wDernierGeoPoint) && (bPasTrouve))
					{
					pGeoPoint = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wPositionGeoPoint);
					bPasTrouve = (BOOL) (pGeoPoint->NbReferencesPoly != 0);
					wPositionGeoPoint++;
					}
				wPositionGeoPoint--;

				// Si (PAS TROUVE) Alors Insertion Pos. Geo
				if (bPasTrouve)
					{
					pElement->pt1.y = wDernierGeoPoint;
					wPositionGeoPoint = wDernierGeoPoint;
					insere_enr (szVERIFSource, __LINE__, 1, B_GEO_POINTS_POLY_GR, wDernierGeoPoint);
					}
				else
					{
					pElement->pt1.y = wPositionGeoPoint;
					}

				// Insertion des points du polygone
				pGeoPoint = (PGEO_POINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_POLY_GR, wPositionGeoPoint);
				wPositionPoint = nb_enregistrements (szVERIFSource, __LINE__, B_POINTS_POLY_GR) + 1;
				pGeoPoint->PremierPoint     = wPositionPoint;
				pGeoPoint->NbPoints         = wNbPoints;
				pGeoPoint->NbReferencesPoly = 1;
				insere_enr (szVERIFSource, __LINE__, wNbPoints, B_POINTS_POLY_GR, wPositionPoint);

				// Affectation des points du polygone
				x = 0;
				y = 0;
				for (iPoint = 0; iPoint < wNbPoints; iPoint++)
					{
					g_element_get_point (pBdGr->hGsys, helementEpure, iPoint, &x, &y);
					pPoint = (PPOINTS_POLY_GR)pointe_enr (szVERIFSource, __LINE__, B_POINTS_POLY_GR, wPositionPoint + iPoint);
					pPoint->x = x;
					pPoint->y = y;
					}
				}
				break;

      case G_ACTION_LISTE:
      case G_ACTION_LISTE_PLEIN:
      case G_ACTION_NOP:
				pElement->nAction  = wAction;
				pElement->nCouleurLigne = nCouleurLigne;
				pElement->nCouleurRemplissage = nCouleurLigne; // $$
				pElement->nStyleLigne   = wStyleLigne;
				pElement->nStyleRemplissage = wStyleRemplissage;
				pElement->pt0.x      = 0;
				pElement->pt0.y      = 0;
				pElement->pt1.x      = 0;
				pElement->pt1.y      = 0;
				break;

      case G_ACTION_CONT_GROUP:
      case G_ACTION_CONT_BOUTON:
      case G_ACTION_CONT_CHECK:
      case G_ACTION_CONT_RADIO:
      case G_ACTION_CONT_BOUTON_VAL:
				{
				// R�cup�re les infos de l'�pure pour les stocker dans l'�l�ment graphique
				pElement->nAction  = wAction;
				pElement->nCouleurLigne = G_COULEUR_INDETERMINEE;
				pElement->nCouleurRemplissage = G_COULEUR_INDETERMINEE;
				pElement->nStyleLigne = (G_STYLE_LIGNE)0; // $$ utilis� pour contenir un num g�o texte
				pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)0; // $$ utilis� pour contenir un discriminateur de type de bouton
				x = 0;
				y = 0;
				g_element_get_point (pBdGr->hGsys, helementEpure, 0, &x, &y);
				pElement->pt0.x      = x;
				pElement->pt0.y      = y;
				g_element_get_point (pBdGr->hGsys, helementEpure, 1, &x, &y);
				pElement->pt1.x      = x;
				pElement->pt1.y      = y;

				G_OrdonneBipoint (&pElement->pt0, &pElement->pt1);
				GElementResetBiPoint (pBdGr->hGsys, helementEpure, &pElement->pt0, &pElement->pt1);

				// Recherche une Position Geo Texte VIDE
				wDernierGeoTexte = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_TEXTES_GR) + 1;
				wPositionGeoTexte = 1;
				bPasTrouve = TRUE;
				while ((wPositionGeoTexte < wDernierGeoTexte) && (bPasTrouve))
					{
					pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
					bPasTrouve = (BOOL) (pGeoTexte->ReferenceTexte != 0);
					wPositionGeoTexte++;
					}
				wPositionGeoTexte--;

				// Si (PAS TROUVE) Alors Insertion Pos. Geo
				if (bPasTrouve)
					{
					pElement->nStyleLigne = (G_STYLE_LIGNE)wDernierGeoTexte;
					wPositionGeoTexte = wDernierGeoTexte;
					insere_enr (szVERIFSource, __LINE__, 1, B_GEO_TEXTES_GR, wDernierGeoTexte);
					}
				else
					pElement->nStyleLigne = (G_STYLE_LIGNE)wPositionGeoTexte;

				// Insertion du texte 
				pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, wPositionGeoTexte);
				wPositionTexte = nb_enregistrements (szVERIFSource, __LINE__, B_TEXTES_GR) + 1;
				pGeoTexte->ReferenceTexte = wPositionTexte;
				insere_enr (szVERIFSource, __LINE__, 1, B_TEXTES_GR, wPositionTexte);

				// Affectation du texte
				pTexteGr = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, wPositionTexte);
				pTexteGr->Police = wPolice;
				pTexteGr->StylePolice = wStylePolice;
				StrExtract (pTexteGr->Texte, pszTexte, 0, c_NB_CAR_TEXTE_GR);
				}
				break;

      case G_ACTION_CONT_STATIC:
      case G_ACTION_CONT_EDIT:
      case G_ACTION_CONT_COMBO:
      case G_ACTION_CONT_LIST:
      case G_ACTION_CONT_SCROLL_H:
      case G_ACTION_CONT_SCROLL_V:
				{
				pElement->nAction  = wAction;
				pElement->nCouleurLigne = G_COULEUR_INDETERMINEE;
				pElement->nCouleurRemplissage = G_COULEUR_INDETERMINEE;
				pElement->nStyleLigne   = (G_STYLE_LIGNE)0;
				pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)0;
				x = 0;
				y = 0;
				g_element_get_point (pBdGr->hGsys, helementEpure, 0, &x, &y);
				pElement->pt0.x      = x;
				pElement->pt0.y      = y;
				g_element_get_point (pBdGr->hGsys, helementEpure, 1, &x, &y);
				pElement->pt1.x      = x;
				pElement->pt1.y      = y;
				G_OrdonneBipoint (&pElement->pt0, &pElement->pt1);
				}
        break;

      default:
				{
				pElement->nAction  = G_ACTION_NOP;
				pElement->nCouleurLigne = G_COULEUR_INDETERMINEE;
				pElement->nCouleurRemplissage = G_COULEUR_INDETERMINEE;
				pElement->nStyleLigne = (G_STYLE_LIGNE)0; // $$ pas ind�termin� ?
				pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)0;
				pElement->pt0.x      = 0;
				pElement->pt0.y      = 0;
				pElement->pt1.x      = 0;
				pElement->pt1.y      = 0;
				}
				break;
      } // switch (wAction)

    //MajPagesGR (MAJ_INSERTION, wReferencePage, wPositionElement, 1);
    //MajGeoMarquesGR (MAJ_INSERTION, wPositionElement, 1);
    } // if (bElementValide && pPage)
  else
    {
    // Effacement de l'Epure Non Valide
		//g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
    // $$ g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
    }

  //GElementReset (pBdGr->helementEpure);
  return wElement;
  } // dwValiderElementGr


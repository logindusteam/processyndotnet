//-----------------------------------------------------------
// WStyleLi.h
// Gestion des fen�tres de choix des styles de lignes
//-----------------------------------------------------------
#ifndef WSTYLELI_H
#define WSTYLELI_H

// Cr�ation des classes des fen�tres de gestion de style ligne de PcsGr
BOOL bCreeClasseStyleLignes (void);

// Cr�ation d'une fen�tre de choix de style de ligne (contenant sa barre d'outil)
HWND hwndCreeChoixStyleLignes (HWND hwndParent, BOOL bVisible, BOOL bChild);

#endif







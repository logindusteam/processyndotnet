/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlggr.h                                               |
 |   Auteur  :	LM							|
 |   Date    :	18/01/93						|
 |   Remarques : gestion des boites de dialogue dans le gr              |
 +----------------------------------------------------------------------*/

#define NB_MAX_TITRE_FENETRE    40
#define NB_MAX_GRILLE            4
#define NB_MAX_ZOOM              4
#define NB_MAX_CAR_SAISIE_COMBO 80
#define DEFAUT_ESPACEMENT_H     40
#define DEFAUT_ESPACEMENT_V     40

typedef struct
	{
	BOOL bFenModale;	
  BOOL bFenDialog;	
	BOOL bBitmapFond;
	BOOL un_titre;
	BOOL un_bord;
	BOOL un_sysmenu;
	BOOL un_minimize;
	BOOL un_maximize;
	BOOL un_scroll_h;
	BOOL un_scroll_v;
	DWORD mode_affichage;
	DWORD mode_deformation;
	char titre [NB_MAX_TITRE_FENETRE+1];
	DWORD grille_x;
	DWORD grille_y;
	DWORD facteur_zoom;
	G_COULEUR dwCouleurFond;
	char szPathBitmapFond [MAX_PATH];
	} DLGGR_STYLE_FEN;

typedef void   t_proc_maj_liste (DWORD *, char *);
typedef BOOL   t_proc_pos_bes_de_nom_es_c (char *pszNomVar, DWORD *pPosition);
typedef char * t_proc_get_nom_es_c (DWORD);
typedef void * t_proc_pointe_enr (DWORD, DWORD);
typedef BOOL   t_proc_message_mot_reserve (DWORD , char *);
typedef BOOL   t_proc_message_information (DWORD , char *);

typedef struct
	{
	t_proc_maj_liste *ptr_proc_maj_liste;
	DWORD nb_max_car_saisie;
	char titre [NB_MAX_CAR_SAISIE_COMBO];
	char saisie [NB_MAX_CAR_SAISIE_COMBO];
	} t_dlg_liste_saisie_gr;

typedef struct
	{
	DWORD wEspaceH;
	DWORD wEspaceV;
	} t_dlg_parametres_gr;

//------------- Reflets Logiques
//
BOOL dlg_saisie_identifiant_gr (char *pstrIdentifiant);

BOOL dlg_style_fenetre_gr (HWND hwnd, DLGGR_STYLE_FEN *data_style);
BOOL dlg_liste_saisie_gr (HWND hwnd, t_dlg_liste_saisie_gr *data);
//BOOL DlgStyleAnimation (HWND hwndParent, DWORD wTypeDeStyle, DWORD *wEtat);
BOOL bParametreAlignementGr (HWND hwnd, t_dlg_parametres_gr *data);

// Appel de la boite de dialogue de configuration et d'animation d'objets graphiques
DWORD dwDlgAnimationGR (char *pszChaineAnimation, HBDGR hbdgr, ANIMATION nAnimation, DWORD dwElement);


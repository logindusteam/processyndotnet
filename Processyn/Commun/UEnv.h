//------------------------------------------------------------
// UEnv.h
// Gestion d'un environnement pour une fen�tre
// g�re un et un seul bloc de donn�es associ� � un handle de fen�tre
// utilise (Get/SetWindowLong (GWL_USERDATA)).

//------------------------------------------------------------
// Alloue un bloc de donn�es et l'enregistre sur le handle de fen�tre
// renvoie NULL ou le pointeur sur les donn�es associ�
PVOID pCreeEnv
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	DWORD dwTailleBloc); // taille de la zone m�moire � associer

//------------------------------------------------------------
// Alloue et initialise un bloc de donn�es et l'enregistre
// sur le handle de fen�tre
// renvoie NULL ou le pointeur sur les donn�es associ�
PVOID pCreeEnvInit 
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	DWORD dwTailleBloc, // taille de la zone m�moire � associer
	PVOID pBlocACopier);	// zone � recopier. Si NULL => bloc initialis� � 0

//------------------------------------------------------------
// WM_CREATE : enregistre le param�tre du CreateWindow
// sur le handle de fen�tre et renvoie ce pointeur. 
PVOID pSetEnvOnCreateParam
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	LPARAM lParam);			// lParam re�u avec le message WM_CREATE

//------------------------------------------------------------
// WM_INITDIALOG : enregistre le param�tre du DialogBoxParam
// sur le handle de fen�tre et renvoie ce pointeur. 
PVOID pSetEnvOnInitDialogParam
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	LPARAM lParam);			// lParam re�u avec le message WM_INITDIALOG

//------------------------------------------------------------
// WM_INITDIALOG : // Alloue et initialise un bloc de donn�es
// avec le param�tre du DialogBoxParam, l'enregistre sur le 
// handle de fen�tre et renvoie ce pointeur. 
PVOID pCreeEnvOnInitDialogParam
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	DWORD dwTailleBloc, // taille de la zone m�moire � allouer et � recopier
	LPARAM lParam);			// lParam re�u avec le message WM_INITDIALOG

//------------------------------------------------------------
// r�cup�re l'adresse du bloc de donn�es enregistr� par pCreeEnv
// renvoie NULL ou le pointeur sur les donn�es associ�
PVOID pGetEnv 
	(HWND hWndAssociee); // handle de fen�tre valide associ�

//------------------------------------------------------------
// associe un pointeur � la fen�tre d�sign�e
// et renvoie l'ancien pointeur associ� (�ventuellement NULL)
PVOID pSelectEnv
	(HWND hWndAssociee,  // handle de fen�tre valide associ�
	PVOID pNouvelEnv);		// nouveau pointeur � associer

//------------------------------------------------------------
// lib�re le bloc de donn�es enregistr� par pCreeEnv
// renvoie TRUE s'il n'y a pas ou plus de pointeur associ�.
BOOL bLibereEnv
	(HWND hWndAssociee); // handle de fen�tre valide associ�

//------------------ fin UEnv.h ------------------------------

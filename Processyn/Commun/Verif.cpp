#include "stdafx.h"
#include "Appli.h"
#include "UExcept.h"
#include "Verif.h"

//--------------------------------------------------
// appel de la fonction de sortie en indiquant la ligne et source
// ou l'erreur est apparue
void _ExitVerif (int sLig, const char * szNomSource )
	{
	char szErr[EXCEP_NB_CAR_MAX_EXPLICATION] = "";

	wsprintf (szErr, 
		"\n [CODE: %i-%s sur %s %s]" \
		"\n \n Cette erreur grave n�cessite la fermeture imm�diate de l'application.",
		sLig, szNomSource, Appli.szNom, Appli.szVersion);
	ExcepContexte (EXCEPTION_VERIF, szErr);
	} // _ExitVerif

//--------------------------------------------------
// appel de la fonction de sortie en indiquant la ligne et source
// ou l'erreur est apparue
// Renvoie toujours FALSE s'il retourne � l'appelant
BOOL _bDireWarning (int sLig, const char * szNomSource )
	{
	char szErr[EXCEP_NB_CAR_MAX_EXPLICATION];

	wsprintf (szErr, 
		"\n [CODE: %i-%s sur %s %s]" \
		"\n \n L'application peut cependant continuer en mode d�grad�." \
		"\n \n Voulez-vous continuer ? (Annuler pour continuer sans signaler les avertissements)",
		sLig, szNomSource, Appli.szNom, Appli.szVersion);
	ExcepWarnContexte (EXCEPTION_VERIF, szErr);
	return FALSE;
	} // _ExitWarn

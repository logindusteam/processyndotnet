/*--------------------------------------------------------------
 | Ce fichier est la propriete de	Societe LOGIQUE INDUSTRIE
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3
 | Il est et demeure sa propriete exclusive et est confidentiel.
 | Aucune diffusion n'est possible sans accord ecrit.
 |------------------------------------------------------------*/
// Gestion des textes traduits de Processyn
// Version Win32 JS 16/12/97

// Les textes sont stock�s en tant que ressources dans une DLL externe.
// Celle ci, charg�e en dynamique dans le r�pertoire de l'exe, s'appelle PcsLngXXX.dll,
// avec XXX caract�ristique de la langue (abbr�viation fournie par le syst�me).
// ex: PcsLngFRA.dll

// Les textes de PROCESSYN sont divis�s en 5 groupes
// 1) Erreurs						 exemple 'Type de paragraphe invalide.'
// 2) Informations			 exemple 'G�n�ration en cours...','SI < FIN_SI'
// 3) Mots r�serv�s			 exemple 'ALORS'
// 4) Variables syst�mes exemple 'ARRET_EXPLOITATION'

// Pour les groupes 3 et 4, des donn�es sont associ�es aux textes
// dans les ressources.
// Des fonctions sp�cifiques permettent d'acc�der aux donn�es de chaque groupe.


#define c_nb_car_message_err   81   //taille max d'un texte d'erreur (0 compris)
#define c_nb_car_message_inf   81   //taille max d'un texte d'information (0 compris)

// --------------------------------------------------------------------------
// Initialiser le gestionnaire des donn�es traduites
// n�cessite la dll de ressources dans la langue courante de l'appli sur path exe
// --------------------------------------------------------------------------
BOOL bOuvrirLng (void);

// --------------------------------------------------------------------------
// Fermer le gestionnaire des donn�es traduites
// --------------------------------------------------------------------------
void bFermerLng (void);

// --------------------------------------------------------------------------
// R�cup�rer une chaine dans la Dll de langue
// --------------------------------------------------------------------------
BOOL bLngLireString
	(
	DWORD dwNumero,					// Num�ro du texte demand�
	char * pszBufDest,				// Buffer pour recevoir le texte demand�
	DWORD dwTailleBuffDest	// Nombre d'octets allou�s sur le buffer
	); // Renvoie TRUE si pas d'erreur FALSE sinon (pszBuffer mis � "")

// --------------------------------------------------------------------------
// R�cup�rer un texte d'erreur !!! c_nb_car_message_err cars max
// --------------------------------------------------------------------------
BOOL bLngLireErreur
	(
	DWORD dwNumero,	// Num�ro du texte demand�
	char * pszBuf		// Buffer pour recevoir le texte demand�
	);							// Renvoie TRUE si pas d'erreur FALSE sinon (pszBuffer mis � "")

// --------------------------------------------------------------------------
// R�cup�rer un texte d'informations !!! c_nb_car_message_inf cars max
// --------------------------------------------------------------------------
BOOL bLngLireInformation 
	(
	DWORD dwIdTexte,// Identifiant du texte demand� (d�clar� dans IdLngLng.h)
	char * pszBuf		// Buffer pour recevoir le texte demand�
	);							// Renvoie TRUE si pas d'erreur FALSE sinon (pszBuffer mis � "")



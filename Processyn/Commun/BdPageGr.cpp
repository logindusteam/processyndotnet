//----------------------------------------------------------------------------
// BdPageGr.c (extrait de Bdgr)
// Gestion des propri�t�s d'une page de synoptique
//-------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"        // std types
#include "MemMan.h"

#include "mem.h"        // mem
#include "UStr.h"     //
#include "G_Objets.h"
#include "g_sys.h"      // g_xxx()
#include "pcsspace.h"   // Coordonn�es Min et MAX de l'espace
#include "dicho.h"      // Tri dicho pour espacement
#include "lng_res.h"
#include "Tipe.h"			// types de proc�dures du descripteur
#include "Descripteur.h"

#include "DocMan.h"			

#include "bdgr.h"			// PBDGR
#include "bdanmgr.h"   // verbes maj inter_vi
#include "BdPageGr.h"
#include "Verif.h"
VerifInit;

//----------------------------------------------------------------------------
// Renvoie TRUE si la page NPage existe (* est mis � jour avec la r�f�rence de page)
BOOL ExistePageGR (DWORD NPage, DWORD * pReferencePage)
  {
  BOOL         bExiste;
  PGEO_PAGES_GR	pGeoPage;

  if ((NPage > 0) && (NPage <= nb_enregistrements (szVERIFSource, __LINE__, B_GEO_PAGES_GR)))
    {
    pGeoPage = (PGEO_PAGES_GR	)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, NPage);
    *pReferencePage = pGeoPage->ReferencePage;
    bExiste = (pGeoPage->ReferencePage > 0);
    }
  else
    {
    bExiste = FALSE;
    }
  return bExiste;
  }

//----------------------------------------------------------------------------
// R�cup�re l'adresse des propri�t�s de la page dwIdPage ou NULL si introuvable
PPAGE_GR pGetPageGR (DWORD dwIdPage)
	{
	PPAGE_GR pRes = NULL;
	DWORD wReferencePage;

	// Page existe ?
  if (ExistePageGR (dwIdPage, &wReferencePage))
    { // oui => renvoie l'adresse de ses propri�t�s
		pRes = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		}
	return pRes;
	}

//----------------------------------------------------------------------------
// R�cup�re l'adresse des propri�t�s de la page HBDGR ou NULL si introuvable
PPAGE_GR ppageHBDGR (HBDGR hbdgr)
	{
  PBDGR pBdGr = (PBDGR)hbdgr;
	PPAGE_GR pRes = NULL;
	DWORD wReferencePage;

	// Page existe ?
  if (pBdGr && ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    { // oui => renvoie l'adresse de ses propri�t�s
		pRes = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		}
	return pRes;
	}

// -------------------------------------------------------------------------
// Page
// -------------------------------------------------------------------------

// -------------------------------------------------------------------------
BOOL bdgr_existe_page (DWORD Page)
  {
  DWORD wReference;

  return ExistePageGR (Page, &wReference);
  }

// -------------------------------------------------------------------------
BOOL bdgr_page_visible (DWORD NPage)
  {
  PPAGE_GR	pPage = pGetPageGR (NPage);
  BOOL			bVisible = FALSE;

  if (pPage)
    bVisible = pPage->bFenVisible;

  return bVisible;
  }

// -------------------------------------------------------------------------
DWORD bdgr_lire_page (HBDGR hbdgr)
  {
  DWORD page = 0;
  DWORD wReferencePage;
  PBDGR pBdGr = (PBDGR)hbdgr;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    page = pBdGr->dwIdPage;
  
  return page;
  }


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Sp�cifications Pages
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// -------------------------------------------------------------------------
DWORD dwNPageGR (HBDGR hbdgr)
  {
  PBDGR	pBdGr = (PBDGR)hbdgr;
	
	VerifWarning (pBdGr);
	return pBdGr->dwIdPage;
  }

// -------------------------------------------------------------------------
void bdgr_titre_page (HBDGR hbdgr, char *pszTitre)
  {
	PPAGE_GR pPage = ppageHBDGR (hbdgr);

  if (pPage)
    StrExtract (pPage->FenTextTitre, pszTitre, 0, c_NB_CAR_TITRE_PAGE_GR);
  }

// -------------------------------------------------------------------------
BOOL bdgr_lire_titre_page (HBDGR hbdgr, char *pszTitre)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
	BOOL      bTitre = FALSE;

  pszTitre [0] = '\0';
  if (pPage)
    {
    if (pPage->bFenTitre)
      {
      StrCopy (pszTitre, pPage->FenTextTitre);
      bTitre = TRUE;
      }
    }
  return bTitre;
  }

// -------------------------------------------------------------------------
BOOL bdgr_lire_titre_page_direct (DWORD NPage, char *pszTitre)
  {
	PPAGE_GR	pPage = pGetPageGR (NPage);
  BOOL			bTitre = FALSE;

  pszTitre [0] = '\0';
  if (pPage)
    {
    if (pPage->bFenTitre)
      {
      StrCopy (pszTitre, pPage->FenTextTitre);
      bTitre = TRUE;
      }
    }
  return bTitre;
  }

// -------------------------------------------------------------------------
// Modifie la couleur de fond de la page courante
void bdgr_couleur_fond_page (HBDGR hbdgr, G_COULEUR Couleur)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    pPage->FenCouleurFond = Couleur;
  }

// -------------------------------------------------------------------------
G_COULEUR bdgr_lire_couleur_fond_page (HBDGR hbdgr)
  {
	PPAGE_GR pPage = ppageHBDGR (hbdgr);
  G_COULEUR     wFenCouleurFond = G_BLACK;

  if (pPage)
    wFenCouleurFond = pPage->FenCouleurFond;

  return wFenCouleurFond;
  }

// -------------------------------------------------------------------------
void bdgr_styles_page (DWORD NPage, BOOL bFenModale, BOOL bFenDialog,
											 DWORD mode_aff, DWORD facteur_zoom, DWORD mode_deform,
                       BOOL sizeborder, BOOL titre, char *texte_titre,
                       BOOL un_sysmenu, BOOL un_minimize, BOOL un_maximize,
                       BOOL scroll_bar_h, BOOL scroll_bar_v,
                       DWORD grille_x, DWORD grille_y)
  {
	PPAGE_GR	pPage = pGetPageGR (NPage);

  if (pPage)
    {
    pPage->bFenModale = bFenModale;
    pPage->bFenDialog = bFenDialog;
    pPage->bFenTitre = titre;
    StrCopy (pPage->FenTextTitre, texte_titre);
    pPage->bFenSizeBorder = sizeborder;
    pPage->bFenSysMenu = un_sysmenu;
    pPage->bFenMinBouton = un_minimize;
    pPage->bFenMaxBouton = un_maximize;
    pPage->FenGrilleX = grille_x;
    pPage->FenGrilleY = grille_y;
    pPage->FenModeAff = mode_aff;
    pPage->FenFacteurZoom = facteur_zoom;
    pPage->FenModeDeform = mode_deform;
    pPage->bFenScrollBarH = scroll_bar_h;
    pPage->bFenScrollBarV = scroll_bar_v;
    }
  }

// -------------------------------------------------------------------------
// r�cup�re les propri�t�s d'une page de synoptique
void bdgr_lire_styles_page (HBDGR hbdgr, BOOL * pbFenModale, BOOL * pbFenDialog, DWORD *mode_aff, DWORD *facteur_zoom, DWORD *mode_deform,
                            BOOL *sizeborder, BOOL *titre, char *texte_titre,
                            BOOL *un_sysmenu, BOOL *un_minimize, BOOL *un_maximize,
                            BOOL *scroll_bar_h, BOOL *scroll_bar_v,
                            DWORD *grille_x, DWORD *grille_y)
  {
	PPAGE_GR pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
		*pbFenModale = pPage->bFenModale;
		*pbFenDialog = pPage->bFenDialog;
    *titre = pPage->bFenTitre;
    StrCopy (texte_titre, pPage->FenTextTitre);
    *sizeborder = pPage->bFenSizeBorder;
    *un_sysmenu = pPage->bFenSysMenu;
    *un_minimize = pPage->bFenMinBouton ;
    *un_maximize = pPage->bFenMaxBouton ;
    *grille_x = pPage->FenGrilleX;
    *grille_y = pPage->FenGrilleY;
    *mode_aff = pPage->FenModeAff;
    *facteur_zoom = pPage->FenFacteurZoom;
    *mode_deform = pPage->FenModeDeform;
    *scroll_bar_h = pPage->bFenScrollBarH;
    *scroll_bar_v = pPage->bFenScrollBarV;
    }
  }

// -------------------------------------------------------------------------
// Renvoie TRUE si la page est modale, FALSE sinon
BOOL bPageGrModale (HBDGR hbdgr)
  {
	BOOL	bRes = FALSE;
	PPAGE_GR pPage = ppageHBDGR (hbdgr);

  if (pPage)
		bRes = pPage->bFenModale;

	return bRes;
  }

// -------------------------------------------------------------------------
// Renvoie TRUE si la page est une boite de dialogue, FALSE sinon
BOOL bPageGrDialog (HBDGR hbdgr)
  {
	BOOL	bRes = FALSE;
	PPAGE_GR pPage = ppageHBDGR (hbdgr);

  if (pPage)
		bRes = pPage->bFenDialog;

	return bRes;
  }

// -------------------------------------------------------------------------
// Renvoie TRUE si le fond de page est ajust� (stretch�) � la zone client de la page
BOOL bPageStretchBmpFondZoneClient (HBDGR hbdgr)
  {
	BOOL	bRes = FALSE;
	PPAGE_GR pPage = ppageHBDGR (hbdgr);

  if (pPage)
		bRes = TRUE; // Dans cette version, toujours vrai ! 

	return bRes;
  }

// -------------------------------------------------------------------------
void bdgr_coord_page (HBDGR hbdgr, int xFen, int yFen, int cxFen, int cyFen)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    pPage->FrameFenX = xFen;
    pPage->FrameFenY = yFen;
    pPage->FrameFenCx = cxFen;
    pPage->FrameFenCy = cyFen;
    }
  }

// -------------------------------------------------------------------------
void bdgr_lire_coord_page  (HBDGR hbdgr, int *xFen, int *yFen, int *cxFen, int *cyFen)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    *xFen = pPage->FrameFenX;
    *yFen = pPage->FrameFenY;
    *cxFen = pPage->FrameFenCx;
    *cyFen = pPage->FrameFenCy;
    }
  }

// -------------------------------------------------------------------------
void bdgr_lire_coord_page_direct  (DWORD NPage, int *xFen, int *yFen, int *cxFen, int *cyFen)
  {
	PPAGE_GR pPage = pGetPageGR (NPage);

  if (pPage)
    {
    *xFen = pPage->FrameFenX;
    *yFen = pPage->FrameFenY;
    *cxFen = pPage->FrameFenCx;
    *cyFen = pPage->FrameFenCy;
    }
  }

// -------------------------------------------------------------------------
void bdgr_origine_page (HBDGR hbdgr, int xOrigine, int yOrigine)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    pPage->OrigineX = xOrigine;
    pPage->OrigineY = yOrigine;
    }
  }

// -------------------------------------------------------------------------
void bdgr_lire_origine_page  (HBDGR hbdgr, PLONG xOrigine, PLONG yOrigine)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    *xOrigine = pPage->OrigineX;
    *yOrigine = pPage->OrigineY;
    }
  }

// -------------------------------------------------------------------------
// Met � jour les premiers �l�ments des pages compte tenu de l'op�ration qui vient d'�tre faite
void MajPagesGR (MAJ_BDGR TypeMaj, DWORD Ref, DWORD PosEnr, DWORD NbEnr)
  {
  DWORD     wPremierEnr = 1;
  DWORD     wDernierEnr = nb_enregistrements (szVERIFSource, __LINE__, B_PAGES_GR) + 1;
  DWORD     wEnr;
  PPAGE_GR	pPage;

  switch (TypeMaj)
    {
    case MAJ_INSERTION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wEnr);
				if (pPage->PremierElement > PosEnr)
					pPage->PremierElement = pPage->PremierElement + NbEnr;
				else
					{
					if ((pPage->PremierElement == PosEnr) && (wEnr != Ref))
						pPage->PremierElement = pPage->PremierElement + NbEnr;
					}
				}
			break;

    case MAJ_SUPPRESSION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wEnr);
				if (pPage->PremierElement >= (PosEnr + NbEnr))
					pPage->PremierElement = pPage->PremierElement - NbEnr;
				}
			break;
    }
  } // MajPagesGR


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Nombres
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DWORD bdgr_nombre_pages (void)
  {
  DWORD nb = 0;

  if (existe_repere (B_GEO_PAGES_GR))
    nb = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_PAGES_GR);
  return nb;
  }

// -------------------------------------------------------------------------
DWORD bdgr_nombre_elements_page (HBDGR hbdgr)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  DWORD     wNombre = 0;

  if (pPage)
    wNombre = pPage->NbElements;

  return wNombre;
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Bitmap de fond de page
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// -------------------------------------------------------------------------
// enregistre le nom du fichier bitmap dans pPage->FenNomBmpFond
void bdgr_bmp_fond_page (HBDGR hbdgr, BOOL bSetClearFlag, PCSTR pszBmpFile)
  {
  PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    pPage->bFenBmpFond = bSetClearFlag;
    if (pPage->bFenBmpFond)
      StrExtract (pPage->FenNomBmpFond, pszBmpFile, 0, c_NB_CAR_NOM_BMP_FOND);
    else
      pPage->FenNomBmpFond [0] = '\0';
    }
  }

// -------------------------------------------------------------------------
// lire le nom du fichier bitmap de la page
BOOL bdgr_lire_nom_bmp_fond_page (HBDGR hbdgr, char *pszBmpFile)
  {
  PPAGE_GR		pPage = ppageHBDGR (hbdgr);
  BOOL				bRes = FALSE;

  pszBmpFile [0] = '\0';
  if (pPage)
    {
    StrCopy (pszBmpFile, pPage->FenNomBmpFond);
    bRes = pPage->bFenBmpFond;
    }

  return bRes;
  }

// -------------------------------------------------------------------------
void bdgr_charger_bmp_fond_page (HBDGR hbdgr)
  {
  PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
		PBDGR				pBdGr = (PBDGR)hbdgr;

    if (pPage->bFenBmpFond)
			{
			char NomBmpFondTemp[MAX_PATH];

			StrCopy (NomBmpFondTemp, pPage->FenNomBmpFond);
			pszPathNameAbsoluAuDoc (NomBmpFondTemp);
      pPage->hBmpFond = g_load_bmp (pBdGr->hGsys, NomBmpFondTemp);
			}
		else
			pPage->hBmpFond = NULL;
    }
  }

// -------------------------------------------------------------------------
// Lib�re le handle pPage->hBmpFond et hDrawDib
void bdgr_supprimer_bmp_fond_page (HBDGR hbdgr)
  {
  PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if (pPage->hBmpFond != NULL)
			{
			PBDGR	pBdGr = (PBDGR)hbdgr;

      g_delete_bmp (pBdGr->hGsys, &pPage->hBmpFond);
			}
    }
  }

// -------------------------------------------------------------------------
// Dessine le Bitmap de fond de page
BOOL bdgr_dessiner_bmp_fond_page (HBDGR hbdgr, BOOL bStretchZoneClient)
  {
  PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  BOOL			bRes = FALSE;

  if (pPage)
    {
    if (pPage->hBmpFond != NULL)
			{
			PBDGR			pBdGr = (PBDGR)hbdgr;
			// Remplir la zone client avec lde Bitmap ?
			if (bStretchZoneClient)
				{
				// Oui => r�cup�re la zone cliente et dessine dedans le Bitmap
				LONG dX=0;
				LONG dY=0;

				g_get_taille_background (pBdGr->hGsys, &dX, & dY);
				g_draw_bmp (pBdGr->hGsys, pPage->hBmpFond, TRUE, 0, 0, dX, dY);
				}
			else
				{
				g_draw_bmp (pBdGr->hGsys, pPage->hBmpFond, FALSE, c_G_MIN_X, c_G_MIN_Y, c_G_MAX_X, c_G_MAX_Y);
				}
			}

    bRes = pPage->bFenBmpFond;
    }
  return bRes;
  }



// Gestion d'un messageBox avec s�rialisation, �ventuelle s�rialisation
#include "stdafx.h"
#include "Appli.h"
#include "UExcept.h"
#include "USignale.h"

static HANDLE	hsemMessageBox = NULL;
static DWORD	dwTimeOutAttenteSynchro = INFINITE;

//---------------------------------------------------------------------------------------
// S�rialisation des signale : acc�s exclusif au signale
void AccesSignale (void)
	{
	if (!hsemMessageBox)
		hsemMessageBox = CreateEvent (NULL, FALSE, TRUE, NULL); //lpEventAttributes,	bManualReset,	bInitialState, lpName	

		// Attente de synchronisation d'acc�s � la message Box
	if (hsemMessageBox)
		WaitForSingleObject (hsemMessageBox, dwTimeOutAttenteSynchro);
	}


//---------------------------------------------------------------------------------------
// S�rialisation des signale : acc�s au signale termin�
void FinAccesSignale (void)
	{
	if (hsemMessageBox)
		// Lib�re le s�maphore
		SetEvent (hsemMessageBox);
	}

//---------------------------------------------------------------------------------------
// Appel de message box
// les flags de type de boite sont rajout�s � ceux pass�s Icone et Boutons
// Renvoie un r�sultat de ::MessageBox
int nSignaleSansSerialisation (PCSTR pszTitre, PCSTR pszTexte, UINT uMB_IconeEtBoutons)
	{
	return ::MessageBox (NULL, pszTexte, pszTitre, uMB_IconeEtBoutons | MB_TASKMODAL | MB_TOPMOST | MB_SETFOREGROUND);
	}

//---------------------------------------------------------------------------------------
// Idem que nSignaleSansSerialisation mais encadr� par AccesSignale et FinAccesSignale
int nSignale (PCSTR pszTexte, PCSTR pszTitre, UINT uMB_IconeEtBoutons)
	{
	int nRes;

	AccesSignale();
	nRes = nSignaleSansSerialisation (pszTitre, pszTexte, uMB_IconeEtBoutons);
	FinAccesSignale();

	return nRes;
	} // nSignale

//----------------------------------------------------------------------------------
// Exceptions

//----------------------------------------------------------------------------------
// Arr�t syst�matique de l'application avec un texte comme diagnostic (par Exception)
void SignaleExit (PCSTR pszDiagnostique)
	{	
	char szErr[EXCEP_NB_CAR_MAX_EXPLICATION] = "";

	wsprintf (szErr, "%s\n%s va �tre termin�e.\n",
		pszDiagnostique, Appli.szNom);

	ExcepContexte (EXCEPTION_SIGNALE, szErr);
	}

//----------------------------------------------------------------------------------
// Avertissement �ventuel avec un texte comme diagnostic et �ventuellement arr�t
void SignaleWarnExit (PCSTR pszDiagnostique)
	{
	char szErr[EXCEP_NB_CAR_MAX_EXPLICATION];

	wsprintf (szErr, "%s\n"
		"Voulez-vous continuer %s ?",
		pszDiagnostique, Appli.szNom);
	ExcepWarnContexte (EXCEPTION_SIGNALE, szErr);
	}

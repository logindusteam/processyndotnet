#ifndef VERIF_H
  
  #define VERIF_H

  // � invoquer une fois apr�s #include "u_verif.h"
  #define VerifInit static const char szVERIFSource [] =__FILE__
  
  // fonction interne � u_verif (appel�e par Verif et VerifExit)
  extern void _ExitVerif (int sLig, const char * szNomSource);
  
  // fonction interne � u_verif (appel�e par VerifWarning)
  // Renvoie toujours FALSE s'il retourne � l'appelant
	extern BOOL _bDireWarning (int sLig, const char * szNomSource);
  
  // Macro provoquant un arr�t syst�matique avec diagnostic sur le point d'arr�t
  #define VerifExit _ExitVerif (__LINE__, szVERIFSource) 

  // Macro provoquant un arr�t avec diagnostic si E vaut 0
  #define Verif(E) (!(E) ? _ExitVerif (__LINE__, szVERIFSource) : (void)0)
  
  // Macro provoquant syst�matiquement un diagnostic sur le point d'arr�t si utilisateur Ok
  #define VerifWarningExit _bDireWarning (__LINE__, szVERIFSource) 

  // Macro : si E vaut 0 provoque �ventuellement un diagnostic sur le point d'arr�t et un arr�t 
	// si l'utilisateur le demande. Renvoie un BOOL valant FALSE ou TRUE (�valuation de l'expression)
  #define VerifWarning(E) (!(E) ? _bDireWarning (__LINE__, szVERIFSource) : TRUE)
  
#endif


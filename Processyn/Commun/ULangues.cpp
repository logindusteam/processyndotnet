//---------------------------------------------
// Gestion des infos d�pendant des langues
// JS Win32 16/12/97
// Elles sont stock�es en ressource dans une DLL choisie en fonction de la langue d�sir�e.
// Permet de connaitre la langue par d�faut de l'utilisateur et du syst�me
// Voir Appli.c
//---------------------------------------------
#include "stdafx.h"
#include "Appli.h"
#include "UStr.h"
#include "PathMan.h"
#include "ULangues.h"

//---------------------------------------------
// Renvoie l'identifiant d'info de Localisation courant
LCID lcidCourant (void)
	{
	LCID lcidRes = GetThreadLocale();

	//BOOL SetThreadLocale (LOCALE_SYSTEM_DEFAULT)//LOCALE_SYSTEM_DEFAULT LOCALE_USER_DEFAULT
	return lcidRes;
	}

//---------------------------------------------
// Renvoie l'identifiant d'info de Localisation du syst�me
LCID lcidSysteme (void)
	{
	LCID lcidRes = GetSystemDefaultLCID();

	return lcidRes;
	}

//---------------------------------------------
// Renvoie l'identifiant d'info de Localisation de l'utilisateur
LCID lcidUtilisateur (void)
	{
	LCID lcidRes = GetUserDefaultLCID ();//MAKELCID;

	return lcidRes;
	}

//-------------------------------------------------------------
// Renvoie l'abbr�viation de la langue choisie par l'utilisateur
BOOL bGetAbbreviationLangue (LCID lcid, PSTR pszLangueUtilisateurCourt, UINT uTailleChaine)
	{
	return GetLocaleInfo (lcid, LOCALE_SABBREVLANGNAME, pszLangueUtilisateurCourt, uTailleChaine) == 0;
	}

//-------------------------------------------------------------
// Renvoie un LCID correspondant au langage principal (suppression du sous langage)
// ex : SUBLANG_FRENCH_CANADIAN => LANG_FRENCH
LCID lcidLangagePrincipal (LCID lcid)
	{
	LANGID LangId = MAKELANGID (PRIMARYLANGID(LANGIDFROMLCID(lcid)), SUBLANG_NEUTRAL);

	return MAKELCID (LangId, SORT_DEFAULT); //SORTIDFROMLCID(lcid));
	}

//-------------------------------------------------------------
// Renvoie le nom de la langue choisie par l'utilisateur dans sa langue
BOOL bGetNomLangue (LCID lcid, PSTR pszLangueUtilisateur, UINT uTailleChaine)
	{
	return GetLocaleInfo (lcid, LOCALE_SLANGUAGE, pszLangueUtilisateur, uTailleChaine) == 0;
	}

//---------------------------------------------------------
// Renvoie la chaine de caract�re associ�e � une erreur OS
BOOL bChaineErreurSysteme (PSTR lpMsgBuf, DWORD dwTailleBuf, UINT uNErreurSysteme)
	{
	DWORD dwRes =FormatMessage ( // Ressource syst�me sans gestion des % sans Cr
		FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_MAX_WIDTH_MASK,
    NULL, // lpSource ignor�
    uNErreurSysteme,
    0, //MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
    lpMsgBuf,
    dwTailleBuf,
    NULL);

	// Ok si au moins un caract�re re�u et pas d'erreur
	return ((dwRes) && (dwRes != ERROR_RESOURCE_LANG_NOT_FOUND));
	}

// ----------------------------------------------------------------------
// Charge les infos et la DLL correspondant � la langue choisie (ex : FRA)
// dans le r�pertoire de l'appli
HINSTANCE hChargeDLLLangue
	(
	PCSTR pszNomEtExtFichier, // exemple: PcsExe.exe
	PCSTR pszAbbrevLangueUtilisateur // exemple : FRA
	) // renvoie son hinst ou NULL
	{
	char szNomDllLangue [MAX_PATH];
	char szNomCompletDllLangue [MAX_PATH];

	// synth�tise le nom de la DLL de ressources dans la langue (pas de liens avec une fonction)
	// au format "PathAppli\NomAppliNomAbbrevLangue.dll"
	StrCopy (szNomDllLangue, pszNomEtExtFichier);
	pszSupprimeExt (szNomDllLangue);
	StrConcat (szNomDllLangue, pszAbbrevLangueUtilisateur);

	pszCreePathNameExt (szNomCompletDllLangue, MAX_PATH, Appli.szPathExe,  szNomDllLangue, "dll");

	// Essaye de charger la DLL
	return LoadLibrary (szNomCompletDllLangue);
	} // ChargeDLLLangue

//----------------------------------------------------------
// Appelle une boite de dialogue de la dll de Langue courante
int LangueDialogBoxParam
	(
  LPCTSTR lpTemplateName,  // identifies dialog box template (Cf MAKEINTRESOURCE)
  HWND hWndParent,      // handle to owner window
  DLGPROC lpDialogFunc, // pointer to dialog box procedure
  LPARAM dwInitParam    // initialization value
	)
	{
	int nRes = -1;

  HRSRC hResInfo = FindResource (Appli.hinstDllLng, lpTemplateName, RT_DIALOG);// resource handle

	if (hResInfo)
		{
		HGLOBAL hDlg = LoadResource(Appli.hinstDllLng, hResInfo);
		
		if (hDlg)
			nRes = DialogBoxIndirectParam (Appli.hinst, (const DLGTEMPLATE *)hDlg, hWndParent, lpDialogFunc, dwInitParam);
		}

	return nRes;
	}

// ----------------------------------------------------------------------
// CreateDialogParam en chargeant la ressource � partir de Appli.hinstDllLng
HWND LangueCreateDialogParam
	(
  LPCTSTR lpTemplateName,  // identifies dialog box template
  HWND hWndParent,      // handle to owner window
  DLGPROC lpDialogFunc, // pointer to dialog box procedure
  LPARAM dwInitParam    // initialization value
	)
	{
	HWND hwndRes = NULL;

  HRSRC hResInfo = FindResource (Appli.hinstDllLng, lpTemplateName, RT_DIALOG);// resource handle

	if (hResInfo)
		{
		HGLOBAL hDlg = LoadResource(Appli.hinstDllLng, hResInfo);

		if (hDlg)
			hwndRes = CreateDialogIndirectParam (Appli.hinst, (const DLGTEMPLATE *)hDlg, hWndParent, lpDialogFunc, dwInitParam);
		}

	return hwndRes;
	}

/*
//---------------------------------------------
static BOOL bRafraichisInfosLangue (void)
	{
	// Infos courantes d�pendant de la langue
	CPINFO cpinfo;
	//CURRENCYFMT currencyfmt;
	//NUMBERFMT	numberfmt;
	LCID lcidSysteme;
	LCID lcidUtilisateur;
	LCID lcidCourant;

	WORD	wSystemeLangId;
	WORD	wSystemeLangIdPrincipal;
	WORD	wUserLangId;
	WORD	wUserLangIdPrincipal;
	BOOL	bRes = GetCPInfo (CP_ACP, // system default ANSI code page
		&cpinfo);
	//currencyfmt;
	//numberfmt;
	lcidSysteme = GetSystemDefaultLCID ();
	lcidUtilisateur = GetUserDefaultLCID ();
	lcidCourant = GetThreadLocale();
	
	wSystemeLangId =  LANGIDFROMLCID (lcidSysteme);
	wSystemeLangIdPrincipal = PRIMARYLANGID (wSystemeLangId);
	wUserLangId =  LANGIDFROMLCID (lcidUtilisateur);
	wUserLangIdPrincipal = PRIMARYLANGID (wUserLangId);
	
	return bRes;
	}

*/
// ----------------------------- fin ULangues ----------------------------------

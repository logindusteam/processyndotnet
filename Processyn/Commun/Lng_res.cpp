/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |									    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 |									    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |									    |
 |   Titre   : LNG_RES.C      Module de gestion des mots r�serv�s	    |
 |   Auteur  : Jean							    |
 |   Date    : 27/05/92 						    |
 +--------------------------------------------------------------------------*/

// 02/06/98 Les mots r�serv�s sont charg�s � partire des ressources
// Remarques: Le mot r�serv� est cherch� par dichotomie dans le bloc des mots
// r�serv�s (b_mot_res), qui est class� par ordre alphab�tique.
//
// Chaque info mot r�serv� est stock�e sous forme d'une string
// dans les ressources traduites.
//
// IDS_NB_MOTS_RESERVES est l'identifiant d'une string contenant le nombre de mots r�serv�s
// Les identifiants des mots r�serv�s valent IDS_NB_MOTS_RESERVES+1, IDS_NB_MOTS_RESERVES+2...
//
// Une string en ressource est au format NNNN,TexteMotRes,
// avec NNNN : wId en d�cimal sur 4 chiffres
//			TexteMotRes : szNom texte du mot r�serv�

#include "stdafx.h"
#include "dicho.h"
#include "mem.h"
#include "UStr.h"
#include "Appli.h"
#include "IdLngLng.h"
#include "LireLng.h"
#include "Verif.h"
#include "lng_res.h"

VerifInit;

#define OFFSET_ID 0
#define OFFSET_SZ_NOM 5

#define TAILLE_INFO_RESSOURCE (10+c_nb_car_message_res)

// Bloc utilise pour les mots r�serv�s
#define b_mot_res 44	//Bloc des mots r�serv�s

// structure des infos d'un mot r�serv� du bloc
typedef struct
	{
	DWORD dwNEnrId;										// Index = Num�ro d'enr du mot r�serv� ayant un Id = N Enr de ce mot r�serv�
	char szMotReserve[c_nb_car_message_res];	// Texte de ce mot r�serv�
	ID_MOT_RESERVE wId;													// Id de ce mot r�serv� (allou� dans la ressource)
	}  INFO_MOT_RESERVE, *PINFO_MOT_RESERVE;

// --------------------------------------------------------------------------
// Fonction de comparaison de deux �l�ments 'mots r�serv�s'
// Fonction pass�e en parametre pour la dichotomie.
// --------------------------------------------------------------------------
static int comparer_mots_reserves (const void * element_1, DWORD dwNEnr)
  {
  PINFO_MOT_RESERVE element_2 = (PINFO_MOT_RESERVE)pointe_enr (szVERIFSource, __LINE__, b_mot_res, dwNEnr);

  return (StrCompare ((PSTR) element_1, element_2->szMotReserve));
  }

// --------------------------------------------------------------------------
// Cherche un mot r�serv� d'apr�s son nom
__inline static BOOL bTrouvePositionMotRes
	(
	PCSTR pszNom,				// Mot r�serv� recherch�
	PDWORD pdwPosition	//  *pdwPosition mis � jour avec le num�ro d'enr trouv� ou d'insertion
	) // Renvoie TRUE si pszNom trouv�.
	{
	return trouve_position_dichotomie (pszNom, 1, nb_enregistrements (szVERIFSource, __LINE__, b_mot_res), comparer_mots_reserves, pdwPosition);
	}

// --------------------------------------------------------------------------
// Initialiser le 'gestionnaire' des mots r�serv�s = chargement en m�moire des mots r�serv�s.
// Remarque : les mots r�serv�s sont stock�es par ordre alphab�tique croissant dans
// le bloc b_mot_res avec un syst�me d'indexation Id <=> position dans le tableau.
// --------------------------------------------------------------------------
void OuvrirMotsReserves (void)
  {
	char szNb [10];
	// Lecture du nombre de mots r�serv�s � charger
	DWORD dwNbMotsReserves = 0;
	VerifWarning (bLngLireString (IDS_NB_MOTS_RESERVES, szNb, 10) && StrToDWORD (&dwNbMotsReserves, szNb));

	// Reset du bloc des mots r�serv�s
  if (existe_repere (b_mot_res))
    enleve_bloc (b_mot_res);

  cree_bloc (b_mot_res, sizeof (INFO_MOT_RESERVE), 0);

	// Chargement de chaque mot r�serv� � partir des ressources
  for (DWORD dwNEnr = 1; (dwNEnr <= dwNbMotsReserves); dwNEnr++)
    {
		DWORD	dwNEnrInsertion;
		DWORD dwIdTemp;
		char szTemp [TAILLE_INFO_RESSOURCE];
		INFO_MOT_RESERVE	InfoMotRes;
		PINFO_MOT_RESERVE	pInfoMotRes;

		// lecture de la ressource
    VerifWarning (bLngLireString (dwNEnr + IDS_NB_MOTS_RESERVES, szTemp, TAILLE_INFO_RESSOURCE) && (StrLength (szTemp) >= OFFSET_SZ_NOM));
		InfoMotRes.dwNEnrId = 0;
		Verif (StrLength (&szTemp[OFFSET_SZ_NOM]) < c_nb_car_message_res);
		StrCopy(InfoMotRes.szMotReserve, &szTemp[OFFSET_SZ_NOM]);

		// ...et d�codage de son Id....
		szTemp[OFFSET_SZ_NOM-1] = '\0';
		VerifWarning(StrToDWORD  (&dwIdTemp, &szTemp[OFFSET_ID]));
		InfoMotRes.wId = (ID_MOT_RESERVE)dwIdTemp;

		// Tri des mots r�serv�s par ordre croissant :
		// recherche de la position d'insertion dans le bloc
		VerifWarning (!bTrouvePositionMotRes	(InfoMotRes.szMotReserve, &dwNEnrInsertion));

		// Insertion � la bonne place
    insere_enr (szVERIFSource, __LINE__, 1, b_mot_res, dwNEnrInsertion);
    pInfoMotRes = (PINFO_MOT_RESERVE)pointe_enr (szVERIFSource, __LINE__, b_mot_res, dwNEnrInsertion);

		// Mise � jour de l'enregistrement
    *pInfoMotRes = InfoMotRes;
    }

	VerifWarning (dwNbMotsReserves == nb_enregistrements (szVERIFSource, __LINE__, b_mot_res));

	// Cr�ation d'un index de recherche rapide Id vers mot r�serv� :
	// Pour chaque Enr : mise � jour de dwNEnrId = Num�ro de l'enr ayant un Id = au num�ro d'enr courant
  for (DWORD dwNEnr = 1; (dwNEnr <= dwNbMotsReserves); dwNEnr++)
    {
		DWORD	dwNEnrRecherche;
		PINFO_MOT_RESERVE	pInfoMotRes = (PINFO_MOT_RESERVE)pointe_enr (szVERIFSource, __LINE__, b_mot_res, dwNEnr);

		// recherche de l'Enr ayant l'Id �gal au Num�ro d'enr en cours
		for (dwNEnrRecherche = 1; dwNEnrRecherche <= dwNbMotsReserves; dwNEnrRecherche++)
			{
			PINFO_MOT_RESERVE	pInfoMotResCherche = (PINFO_MOT_RESERVE)pointe_enr (szVERIFSource, __LINE__, b_mot_res, dwNEnrRecherche);

			if (pInfoMotResCherche->wId == (ID_MOT_RESERVE)dwNEnr)
				{
				// Recherche fructueuse => mise � jour de l'index
				pInfoMotRes->dwNEnrId = dwNEnrRecherche; // Ok
				break;
				}
			}

		// signale si non trouv�
		VerifWarning (pInfoMotRes->dwNEnrId != 0);
    }
  } // OuvrirMotsReserves

// --------------------------------------------------------------------------
// Fermer le 'gestionnaire' des mots r�serv�s
// Remarques: La fermeture consiste � liberer la m�moire prise par les
//	      mots r�serv�s (enleve_bloc (b_mot_res)).
// --------------------------------------------------------------------------
void FermerMotsReserves (void)
  {
  if (existe_repere (b_mot_res))
    {
    enleve_bloc (b_mot_res);
    }
  }

// --------------------------------------------------------------------------
// Renvoie le num�ro du mot r�serv� sp�cifi� ou 0 s'il n'est pas reconnu
// --------------------------------------------------------------------------
ID_MOT_RESERVE nChercheIdMotReserve (PCSTR pszNom)
  {
  ID_MOT_RESERVE dwRes = libre;

	// Le bloc existe ?
  if (existe_repere (b_mot_res))
    {
		DWORD	dwPosition;

		// oui => Mot Res trouv� ?
    if (bTrouvePositionMotRes	(pszNom, &dwPosition))
      {
			PINFO_MOT_RESERVE pInfoMotRes = (PINFO_MOT_RESERVE)pointe_enr (szVERIFSource, __LINE__, b_mot_res, dwPosition);

			// oui => renvoie son Id
      dwRes = pInfoMotRes->wId;
      }
    }
  return dwRes;
  } // nChercheIdMotReserve

// --------------------------------------------------------------------------
// R�cup�rer la chaine d'un mot r�serv� � partir de son Id
// --------------------------------------------------------------------------
BOOL bMotReserve
	(
	ID_MOT_RESERVE nIdMotReserve,	// Id du mot r�serv�
	PSTR pszNom		// Adresse de la chaine devant recevoir le mot r�serv�
	)							// TRUE si Ok (Id trouv� et chaine copi�e)
  {
  BOOL bRes = FALSE;

	// r�cup�re le nom du mot Res num�ro dwNEnrId ?
	PINFO_MOT_RESERVE	pInfoMotRes = (PINFO_MOT_RESERVE)pPointeEnrSansErr (szVERIFSource, __LINE__, b_mot_res, nIdMotReserve);
  if (pInfoMotRes)
    {
		// oui => r�cup�re la position du mot r�serv� correspondant � l'Idrecopie le mot r�serv� lu
		// recopie le mot r�serv�
    pInfoMotRes = (PINFO_MOT_RESERVE)pointe_enr (szVERIFSource, __LINE__, b_mot_res, pInfoMotRes->dwNEnrId);
    StrCopy (pszNom, pInfoMotRes->szMotReserve);
    bRes = TRUE;
    }
  return bRes;
  }

// Compare une chaine et un mot r�serv�
// Renvoie TRUE s'ils sont identiques
BOOL bChaineEstMotReserve (PCSTR pszChaineAComparer, ID_MOT_RESERVE nIdMotReserve)
  {
  BOOL bRes = FALSE;

	// r�cup�re le nom du mot Res num�ro dwNEnrId ?
	PINFO_MOT_RESERVE pInfoMotRes = (PINFO_MOT_RESERVE)pPointeEnrSansErr (szVERIFSource, __LINE__, b_mot_res, nIdMotReserve);
  if (pInfoMotRes)
    {
		// oui => renvoie la comparaison entre la chaine lue et le mot r�serv�
    pInfoMotRes = (PINFO_MOT_RESERVE)pointe_enr (szVERIFSource, __LINE__, b_mot_res, pInfoMotRes->dwNEnrId);
    bRes = bStrEgales (pszChaineAComparer, pInfoMotRes->szMotReserve);
    }
  return bRes;
  }

// Compare hors casse une chaine et un mot r�serv�
// Renvoie TRUE s'ils sont identiques
BOOL bChaineEstMotReserveI (PCSTR pszChaineAComparer, ID_MOT_RESERVE nIdMotReserve)
  {
  BOOL bRes = FALSE;

	// r�cup�re le nom du mot Res num�ro dwNEnrId ?
	PINFO_MOT_RESERVE pInfoMotRes = (PINFO_MOT_RESERVE)pPointeEnrSansErr (szVERIFSource, __LINE__, b_mot_res, nIdMotReserve);
  if (pInfoMotRes)
    {
		// oui => renvoie la comparaison entre la chaine lue et le mot r�serv�
    pInfoMotRes = (PINFO_MOT_RESERVE)pointe_enr (szVERIFSource, __LINE__, b_mot_res, pInfoMotRes->dwNEnrId);
    bRes = bStrIEgales (pszChaineAComparer, pInfoMotRes->szMotReserve);
    }
  return bRes;
  }



//////////////////////////////////////////////////////////////////////
// Descripteur.h: interface for the Descripteur class.
//

#if !defined(AFX_DESCRIPTEUR_H__BEB4DB84_40EE_11D2_A144_0000E8D90704__INCLUDED_)
#define AFX_DESCRIPTEUR_H__BEB4DB84_40EE_11D2_A144_0000E8D90704__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

typedef enum
  {
	INSERE_LIGNE = 0,
	SUPPRIME_LIGNE = 1,
	CONSULTE_LIGNE = 2,
	MODIFIE_LIGNE = 3
  } REQUETE_EDIT; // Edition : op�ration sur les lignes


class CDescripteur
	{
	public:
		// Constructeur et destructeur
		CDescripteur();
		virtual ~CDescripteur();

		// Fonctions standard
		virtual BOOL bExiste(); // TRUE si descripteur existe 
		virtual void Cree (void);
		virtual void InitDesc (DWORD *depart, DWORD *courant, char *nom_applic);
		virtual DWORD LisNbLignes (void);
		virtual void ValideLigne (REQUETE_EDIT action, DWORD ligne, PUL vect_ul, int *nb_ul,
			BOOL *supprime_ligne, BOOL *accepte_ds_bd, DWORD *erreur_val, int *niveau, int *indentation);
		virtual void SetTdb (DWORD ligne, BOOL nouvelle_ligne, char *tdb);
		virtual void LisParamDlg (int index_boite, UINT *iddlg, FARPROC * adr_winproc);
		virtual void Fin (BOOL quitte, DWORD courant);
		virtual void Satellite (BOOL *bExecutionSynchrone, char *nom, char *param, char *env, char *chemin);
	};

#endif // !defined(AFX_DESCRIPTEUR_H__BEB4DB84_40EE_11D2_A144_0000E8D90704__INCLUDED_)

//-----------------------------------------------------------------------------------------
// Descripteur.h
// Classe de descripteurs
//-----------------------------------------------------------------------------------------
typedef struct
	{
	DWORD n_ligne_depart;
	DWORD nb_variables;
	char chaine[255];
	} t_dlg_desc; // structure utilis�e en param�tre initial des boites de dialogue en �dition

typedef struct
	{
	void (*pfnCree) (void);
	void (*pfnInitDesc) (DWORD *depart, DWORD *courant, char *nom_applic);
	DWORD (*pfnLisNbLignes) (void);
	void (*pfnValideLigne) (REQUETE_EDIT action, DWORD ligne, PUL vect_ul, int *nb_ul,
		BOOL *supprime_ligne, BOOL *accepte_ds_bd, DWORD *erreur_val, int *niveau, int *indentation);
	void (*pfnSetTdb) (DWORD ligne, BOOL nouvelle_ligne, char *tdb);
	void (*pfnLisParamDlg) (int index_boite, UINT *iddlg, FARPROC * adr_winproc);
	void (*pfnFin) (BOOL quitte, DWORD courant);
	void (*pfnSatellite) (BOOL *bExecutionSynchrone, char *nom, char *param, char *env, char *chemin);
	} DESCRIPTEUR_CF, *PDESCRIPTEUR_CF;

// ------- drivers (mettre a jour Premier_driver et Dernier_driver)-------
typedef enum
  {
	d_systeme = 1,
	d_code = 2,
	d_mem = 3,
	d_chrono = 4,
	d_disq = 5,
	d_dde = 6,
	d_repete = 7,
	d_graf = 9,
	d_jbus = 11,
	d_al = 52,
	d_ap = 54,
	d_opc = 57
	} ID_DESCRIPTEUR;

#define premier_driver d_systeme
#define dernier_driver d_opc


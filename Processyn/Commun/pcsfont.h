#ifndef PCSFONT_H
#define PCSFONT_H
// --------------------------------------------------------------------------
// Styles de polices (h�rit�s de OS/2) - Cf dwBitsStyle
#define PCS_FONT_STYLE_DEFAUT		0x0000
#define PCS_FONT_STYLE_ITALIQUE	0x0001
#define PCS_FONT_STYLE_ITALIQUE	0x0001
#define PCS_FONT_STYLE_SOULIGNE	0x0002
#define PCS_FONT_STYLE_GRAS			0x0020

// polices "standard" de PcsGr
#define PCS_FONT_NB_POLICES_STANDARD 4
#define c_ID_POLICE_DEFAUT  0

// --------------------------------------------------------------------------
// Boite de dialogue de choix d'une police et de ses attributs
BOOL fnt_user (HWND hwnd, DWORD * pdwNPolice, DWORD * pdwBitsStyle, DWORD * pdwTaillePixel, BYTE byCharSetFont);

// Cr�e une police � partir de ses caract�ristiques
HFONT fnt_create (HDC hdc, DWORD dwNPolice, DWORD dwBitsStyle, DWORD dwTaillePixel, BYTE byCharSetFont);

// D�truit la police pass�e en param�tre
void fnt_destroy (HFONT hfont);

#endif //PCSFONT_H

#ifndef FMAN_H
#define FMAN_H

// ------------------------------------------------------------------------------------
// FMan.h : Gestion simple de fichiers r�entrants si Lib Multi-thread
//
// En cas d'erreur (fonction renvoie FALSE), utiliser si l'on veut la fonction GetLastError 
// pour r�cup�rer sa valeur (une valeur courante par thread)
// ------------------------------------------------------------------------------------

class CFman  
	{
	public:
		// CFman();
		// virtual ~CFman();

	public:
		// Handle d'un fichier g�r� par ce module
		DECLARE_HANDLE (HF);
		typedef HF * PHF;

		// Valeur conventionelle d'un Handle de fichier non ouvert
		#define INVALID_HF ((CFman::HF) INVALID_HANDLE_VALUE)

		// -----------------------------------------------
		//					Ouverture / fermeture de fichiers
		// Flags : (piqu�s aux MFC)
		// Sharing and access mode. Specifies the action to take when opening the file. 
		// You can combine options listed below by using the bitwise-OR (|) operator. 
		// One access permission and one share option are required; 
		// the modeCreate and modeNoInherit modes are optional.
		enum OpenFlags 
			{
			modeRead =          0x0000,	// Opens the file for reading only.
			modeWrite =         0x0001,	// Opens the file for reading and writing.
			modeReadWrite =     0x0002,	// Opens the file for writing only.
			shareCompat =       0x0000,	// This flag is not available in 32 bit MFC. This flag maps to CFile::shareExclusive when used in CFile::Open.
			shareExclusive =    0x0010,	// Opens the file with exclusive mode, denying other processes both read and write access to the file. Construction fails if the file has been opened in any other mode for read or write access, even by the current process.
			shareDenyWrite =    0x0020,	// Opens the file and denies other processes write access to the file. Create fails if the file has been opened in compatibility mode or for write access by any other process.
			shareDenyRead =     0x0030,	// Opens the file and denies other processes read access to the file. Create fails if the file has been opened in compatibility mode or for read access by any other process.
			shareDenyNone =     0x0040,	// Opens the file without denying other processes read or write access to the file. Create fails if the file has been opened in compatibility mode by any other process.
			modeNoInherit =     0x0080,	// Prevents the file from being inherited by child processes.
			modeCreate =        0x1000,	// Directs the constructor to create a new file. If the file exists already, it is truncated to 0 length.
			modeNoTruncate =    0x2000,	// Combine this value with modeCreate. If the file being created already exists, it is not truncated to 0 length. Thus the file is guaranteed to open, either as a newly created file or as an existing file. This might be useful, for example, when opening a settings file that may or may not exist already. This option applies to CStdioFile as well.
			typeText =          0x4000, // Sets text mode with special processing for carriage return�linefeed pairs (used in derived classes only).
			typeBinary =				0x8000,	// Sets binary mode (used in derived classes only).

			OF_DRIV_DQ =(modeCreate|modeReadWrite|shareDenyNone|modeNoTruncate), // Mode d'ouverture de Driv_dq
			OF_OUVRE_LECTURE_SEULE =(modeRead|shareDenyNone|modeNoTruncate), // Lecture non exclusive d'un fichier devant exister
			OF_OUVRE_LECTURE_SEULE_EXCLUSIF =(modeRead|shareDenyWrite|modeNoTruncate), // Lecture exclusive d'un fichier devant exister
			OF_OUVRE =(modeReadWrite|shareDenyNone|modeNoTruncate), // Lecture exclusive d'un fichier devant exister
			OF_OUVRE_EXCLUSIF =(modeReadWrite|shareDenyRead|shareDenyWrite|modeNoTruncate), // Lecture exclusive d'un fichier devant exister
			OF_CREE =(modeCreate|modeReadWrite|shareDenyNone), // Lecture �criture non exclusive d'un fichier cr��
			OF_CREE_EXCLUSIF =(modeCreate|modeReadWrite|shareDenyRead|shareDenyWrite), // Lecture �criture exclusive d'un fichier cr��
			OF_OUVRE_OU_CREE =(modeCreate|modeReadWrite|shareDenyNone|modeNoTruncate), // Lecture �criture non exclusive d'un fichier cr�� s'il n'existe pas
			OF_OUVRE_OU_CREE_EXCLUSIF = (modeCreate|modeReadWrite|shareDenyRead|shareDenyWrite|modeNoTruncate) // Lecture �criture exclusive d'un fichier cr�� s'il n'existe pas
			};
		//#define OF_DRIV_DQ (modeCreate|modeReadWrite|shareDenyNone|modeNoTruncate) // Mode d'ouverture de Driv_dq
		//#define OF_OUVRE_LECTURE_SEULE (modeRead|shareDenyNone|modeNoTruncate) // Lecture non exclusive d'un fichier devant exister
		//#define OF_OUVRE_LECTURE_SEULE_EXCLUSIF (modeRead|shareDenyWrite|modeNoTruncate) // Lecture exclusive d'un fichier devant exister
		//#define OF_OUVRE (modeReadWrite|shareDenyNone|modeNoTruncate) // Lecture exclusive d'un fichier devant exister
		//#define OF_OUVRE_EXCLUSIF (modeReadWrite|shareDenyRead|shareDenyWrite|modeNoTruncate) // Lecture exclusive d'un fichier devant exister
		//#define OF_CREE (modeCreate|modeReadWrite|shareDenyNone) // Lecture �criture non exclusive d'un fichier cr��
		//#define OF_CREE_EXCLUSIF (modeCreate|modeReadWrite|shareDenyRead|shareDenyWrite) // Lecture �criture exclusive d'un fichier cr��
		//#define OF_OUVRE_OU_CREE (modeCreate|modeReadWrite|shareDenyNone|modeNoTruncate) // Lecture �criture non exclusive d'un fichier cr�� s'il n'existe pas
		//#define OF_OUVRE_OU_CREE_EXCLUSIF (modeCreate|modeReadWrite|shareDenyRead|shareDenyWrite|modeNoTruncate) // Lecture �criture exclusive d'un fichier cr�� s'il n'existe pas
		//#e ndif

		// -----------------------------------------------
		// Ouverture d'un fichier
		static BOOL bFOuvre (PHF phFile, PCSTR pszName, DWORD wFlags);

		// Fermeture d'un fichier (* phFile mis � INVALID_HF)
		static BOOL bFFerme (PHF phFile);

		// Renvoie la taille courante d'un fichier
		static BOOL bFLongueur (HF hFile, DWORD * pwSize);

		// -----------------------------------------------
		// Pointeur de lecture - �criture
		// -----------------------------------------------

		// Sp�cifie l'offset depuis le d�but de fichier du pointeur de lecture/�criture
		// Permet de positionner le pointeur AU DELA de la fin de fichier !
		static BOOL bFPointe (HF hFile, DWORD dwOffset);

		// R�cup�re l'offset depuis le d�but de fichier du pointeur de lecture/�criture
		static BOOL bFValPointe (HF hFile, DWORD * pdwOffset);

		// Pointe � la fin du fichier
		static BOOL bFPointeFin (HF hFile);

		// -----------------------------------------------
		// Ecriture fichier
		// renvoie TRUE en cas d'�criture partielle
		static BOOL bFEcrire (HF hFile, const void * pBuff, DWORD dwTailleAEcrire, DWORD * pdwTailleEcrite);

		// Ecriture s�quentielle
		// renvoie FALSE si tout n'a pas pu �tre �crit
		static BOOL bFEcrireTout (HF hFile, const void * pBuff, DWORD dwTailleAEcrire);

		// Ecriture s�quentielle d'une chaine de caract�re
		static BOOL bFEcrireSz (HF hFile, PCSTR pszBufferLn);

		// Ecriture s�quentielle d'une chaine de caract�re puis d'un Cr Lf
		static BOOL bFEcrireSzLn (HF hFile, PCSTR pszBufferLn);

		// -----------------------------------------------
		// Lecture s�quentielle
		// renvoie TRUE MEME SI LA LECTURE N'EST QUE PARTIELLE - FALSE si erreur.
		static BOOL bFLire (HF hFile, PVOID pBuff, DWORD dwTailleALire, DWORD * pdwTailleLue);

		// Lecture s�quentielle
		// renvoie FALSE si tout n'a pas pu �tre lu
		static BOOL bFLireTout (HF hFile, PVOID pBuff, DWORD dwTailleALire);

		// Lecture s�quentielle d'une ligne de texte
		static BOOL bFLireLn (HF hFile, PSTR pszDest, DWORD dwTailleDest, PDWORD pdwNbOctetsLus = NULL);

		// ---------------------------------------------------------------------
		// Teste la possibilit�s d'ouverture d'un fichier selon l'acc�s sp�cifi�
		static BOOL bFAcces (PCSTR pszName, DWORD wFlags);

		// Efface un fichier
		static BOOL bFEfface (PCSTR pszName);
	};

#endif
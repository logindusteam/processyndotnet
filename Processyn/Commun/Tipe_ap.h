 /*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------*/
// Tipe_Ap.H
// Version 5.0 JS Win32 24/4/98
// declaration des types APPLICOM
// ----------------------------------------------------------------
#ifndef Tipe_ap_h
#define Tipe_ap_h

//-------------------------- blocs
#define  bx_applicom_adresse_max  891

// ----------------------------------
#define  b_geo_ap                      853
#define  b_spec_titre_paragraf_ap      854
#define  b_titre_paragraf_ap           855
// Blocs pour l'interprétation
#define  b_ap_e                        856
#define  b_ap_e_mes                    857
#define  b_ap_s                        858
#define  b_ap_s_mes                    859
#define  b_ap_e_tab                    860
#define  b_ap_s_tab                    861
#define  b_ap_s_reflet                 862
#define  b_ap_s_reflet_mes             863
#define  b_ap_s_tab_reflet             864
#define  b_ap_s_tab_reflet_mes         865
#define  b_ap_fct_cyc                  866
// Blocs pour l'exécution
#define  bx_ap_e_log                   867
#define  bx_ap_e_log_tab               868
#define  bx_ap_e_num_entier            869
#define  bx_ap_e_num_entier_tab        870
#define  bx_ap_e_num_mot               871
#define  bx_ap_e_num_mot_tab           872
#define  bx_ap_e_num_reel              873
#define  bx_ap_e_num_reel_tab          874
#define  bx_ap_e_mes                   875
// 
#define  bx_ap_s_log                   876
#define  bx_ap_s_log_tab               877
#define  bx_ap_s_num_entier            878
#define  bx_ap_s_num_entier_tab        879
#define  bx_ap_s_num_mot               880
#define  bx_ap_s_num_mot_tab           881
#define  bx_ap_s_num_reel              882
#define  bx_ap_s_num_reel_tab          883
#define  bx_ap_s_mes                   884
// 
#define  bx_ap_s_log_tab_reflet        885
#define  bx_ap_s_num_entier_tab_reflet 886
#define  bx_ap_s_num_mot_tab_reflet    887
#define  bx_ap_s_num_reel_tab_reflet   888
#define  bx_ap_s_tab_mes_reflet        889
// Blocs pour l'exécution Cycliques
#define  bx_ap_fct_cyc                 890
// ----------------------------------


// ----------------------------------
typedef struct
  {
  DWORD numero_carte;
  } SPEC_TITRE_PARAGRAPHE_APPLICOM, *PSPEC_TITRE_PARAGRAPHE_APPLICOM;

// ----------------------------------
typedef struct
  {
  DWORD numero_repere;
  union
    {
    DWORD pos_donnees;
    struct
      {
      DWORD pos_paragraf;
      DWORD pos_specif_paragraf;
      };
    struct
      {
      DWORD pos_es;
      DWORD pos_initial;
      DWORD pos_specif_es;
      };
    };
  } GEO_APPLICOM, *PGEO_APPLICOM;

// ----------------------------------
typedef struct
  {
  DWORD i_cmd_rafraich;
  DWORD adresse;
  } C_APPLICOM_E;

typedef struct
  {
  DWORD i_cmd_rafraich;
  DWORD longueur;
  DWORD adresse;
  } C_APPLICOM_E_MES;

typedef struct
  {
  DWORD adresse;
  } C_APPLICOM_S;

typedef struct
  {
  DWORD longueur;
  DWORD adresse;
  } C_APPLICOM_S_MES;

typedef struct
  {
  DWORD i_cmd_rafraich;
  DWORD taille;
  DWORD adresse;
  } C_APPLICOM_TAB;

typedef struct
  {
  DWORD indice;
  DWORD adresse;
  } C_APPLICOM_TAB_S_REFLET;

typedef struct
  {
  DWORD indice;
  DWORD longueur;
  DWORD adresse;
  } C_APPLICOM_TAB_S_REFLET_MES;

typedef struct
  {
  DWORD numero_canal;
  DWORD numero_fonction_cyclique;
  } C_APPLICOM_FCT_CYC;

typedef struct
  {
  DWORD index_es_associe;
  DWORD i_cmd_rafraich;
  int numero_carte;
  LONG adresse;
  } X_APPLICOM_E;

typedef struct
  {
  DWORD index_es_associe;
  int numero_carte;
  LONG adresse;
  } X_APPLICOM_S;

typedef struct
  {
  DWORD longueur;
  DWORD index_es_associe;
  DWORD i_cmd_rafraich;
  int numero_carte;
  int nbre;
  LONG adresse;
  } X_APPLICOM_TAB_MES;

typedef struct
  {
  DWORD index_es_associe;
  int numero_carte;
  LONG adresse;
  } X_APPLICOM_TAB_S_REFLET_MES;

typedef struct
  {
  DWORD index_es_associe;
  DWORD numero_canal;
  int numero_fonction_cyclique;
  } X_APPLICOM_FCT_CYC ;

#endif

// ---------------------------------------------------------------------------

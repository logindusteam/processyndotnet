/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : bdanmgr.c                                                |
 |   Auteur  : AC					        	|
 |   Date    : 04/05/94					        	|
 |   Version : 4.00							|
 +----------------------------------------------------------------------*/
#include "stdafx.h"
#include <stddef.h>
#include "std.h"
#include "UStr.h"
#include "UChrono.h"
#include "mem.h"
#include "lng_res.h"
#include "tipe.h"
#include "cvt_ul.h"
#include "IdGrLng.h"
#include "Descripteur.h"
#include "G_objets.h"
#include "inter_gr.h"
#include "g_sys.h"
#include "bdgr.h"
#include "bdanmgr.h"
#include "Verif.h"
#include "Pcsdlggr.h"
#include "tipe_gr.h"
VerifInit;

#define b_anmgr_n_desc_libre 850

static BOOL	bAnmGrOuvert = FALSE;

//-----------------------------------------------------
void bdanmgr_init_proc (BOOL	bOuvert)
  {
	bAnmGrOuvert = bOuvert;
  }

//-----------------------------------------------------
void bdanmgr_TypeAnimation (G_ACTION Action, ANIMATION *pnAnimation)
	{
  switch (Action)
    {
    case G_ACTION_COURBE_T:
      *pnAnimation = ANIMATION_COURBE_TEMPS;
      break;
    case G_ACTION_COURBE_C:
      *pnAnimation = ANIMATION_COURBE_COMMANDE;
      break;
    case G_ACTION_COURBE_A:
      *pnAnimation = ANIMATION_COURBE_ARCHIVE;
      break;
    case G_ACTION_BARGRAPHE:
      *pnAnimation = ANIMATION_BARGRAPHE;
      break;
    case G_ACTION_EDIT_TEXTE:
      *pnAnimation = ANIMATION_EDIT_A;
      break;
    case G_ACTION_EDIT_NUM:
      *pnAnimation = ANIMATION_EDIT_N;
      break;
    case G_ACTION_STATIC_TEXTE:
      *pnAnimation = ANIMATION_TEXTE_STATIC_A;
      break;
    case G_ACTION_STATIC_NUM:
      *pnAnimation = ANIMATION_TEXTE_STATIC_N;
      break;
    case G_ACTION_ENTREE_LOG:
      *pnAnimation = ANIMATION_E_LOG;
      break;
		case G_ACTION_CONT_STATIC:
      *pnAnimation = ANIMATION_CONTROL_STATIC;
			break;

		case G_ACTION_CONT_EDIT:
      *pnAnimation = ANIMATION_CONTROL_EDIT; //ANIMATION_EDIT_A; //$$
			break;

    case G_ACTION_CONT_GROUP:
      *pnAnimation = ANIMATION_AUCUNE;
      break;
    case G_ACTION_CONT_BOUTON:
      *pnAnimation = ANIMATION_CONTROL_BOUTON;
      break;
    case G_ACTION_CONT_CHECK:
      *pnAnimation = ANIMATION_CONTROL_CHECK;
      break;
		case G_ACTION_CONT_RADIO:
      *pnAnimation = ANIMATION_CONTROL_RADIO;
			break;
		case G_ACTION_CONT_COMBO:
      *pnAnimation = ANIMATION_CONTROL_COMBOBOX;
			break;
		case G_ACTION_CONT_LIST:
      *pnAnimation = ANIMATION_CONTROL_LISTE;
			break;
    case G_ACTION_CONT_BOUTON_VAL:
      *pnAnimation = ANIMATION_AUCUNE;
      break;
    default:
      break;
    }
	} // bdanmgr_TypeAnimation

//-----------------------------------------------------
// ATTENTION: pas de test du type d'animation: 
// suppose que le champ identifiant est toujours le dernier
DWORD bdanmgr_maj_identifiant_graphique (DWORD n_ligne_ref, PSTR strIdentifiant)
  {

  DWORD wRetour;
  char texte_anim [513]; //$$ taille ?
	ANIMATION nAnimation;

  wRetour = 0;
  if (n_ligne_ref != 0)
    {
		UL v_ul[nb_max_ul];
		int nb_ul;

    bdanmgr_consulter (texte_anim, n_ligne_ref, &nAnimation, 513);
		texte_to_v_ul (&nb_ul, v_ul, texte_anim);
		// ATTENTION: pas de test du type d'animation: 
		//suppose que le champ identifiant est toujours le dernier
		StrCopy(v_ul [nb_ul-1].show, strIdentifiant);
		StrAddDoubleQuotation (v_ul [nb_ul-1].show);
    v_ul_to_texte (nb_ul, v_ul, texte_anim, 513);
    wRetour = bdanmgr_modifier (texte_anim, n_ligne_ref);
    }
  else
    {
    wRetour = 0; // erreur � rajouter
    }
	return (wRetour);
	}

//-----------------------------------------------------
// ATTENTION: pas de test du type d'animation: 
// suppose que le champ identifiant est toujours le dernier et le champ page le deuxi�me
BOOL bdanmgr_get_identifiant_animation (DWORD n_ligne_ref, DWORD *pNumeroPage, PSTR strIdentifiant)
  {

  BOOL bTrouve = FALSE;
  char texte_anim [513]; //$$ taille ?
	ANIMATION nAnimation;

  if (n_ligne_ref != 0)
    {
		if (n_ligne_ref <= lis_nombre_de_ligne_vdi ())
			{
			UL v_ul[nb_max_ul];
			int nb_ul;

			bdanmgr_consulter (texte_anim, n_ligne_ref, &nAnimation, 513);
			texte_to_v_ul (&nb_ul, v_ul, texte_anim);
			// ATTENTION: pas de test du type d'animation: 
			// suppose que le champ identifiant est toujours le dernier et le champ page le deuxi�me
			if (nb_ul > 1)
				{
				StrCopy(strIdentifiant, v_ul [nb_ul-1].show);
				StrDeleteDoubleQuotation (strIdentifiant);
				StrToDWORD (pNumeroPage, v_ul[1].show);
				}
			else // commentaire
				{
				(*pNumeroPage) = 0;
				}
			bTrouve = TRUE;
			}
    }
	return (bTrouve);
	}

//-----------------------------------------------------
DWORD bdanmgr_parametrer (ANIMATION nAnimation, HBDGR hbdgrAnimation, DWORD n_ligne_ref, DWORD *pn_ligne_anim, DWORD dwElement)
  {
  DWORD wRetour;
  char texte_anim [513]; //$$ taille ?

  wRetour = 0;
  if (bAnmGrOuvert)
    {
    if (n_ligne_ref != 0)
      {
      bdanmgr_consulter (texte_anim, n_ligne_ref, &nAnimation, 513);
      }
    else
      {
      texte_anim [0] = '\0';
      }

    switch (dwDlgAnimationGR (texte_anim, hbdgrAnimation, nAnimation, dwElement))
      {
      case IDOK : // VALIDER
        if ((*pn_ligne_anim) != 0)
          {
          wRetour = bdanmgr_modifier (texte_anim, (*pn_ligne_anim));
          }
        else
          {
					if (nAnimation != ANIMATION_AUCUNE)
						wRetour = bdanmgr_inserer (texte_anim, pn_ligne_anim);
          }
        break;

      case IDB_SUPPRIMER : // SUPPRIMER
        if ((*pn_ligne_anim) != 0)
          {
          if (bdanmgr_supprimable (*pn_ligne_anim))
            {
            if (bdanmgr_supprimer (*pn_ligne_anim))
              *pn_ligne_anim = 0;
            else
              wRetour = 432;
            }
          else
            wRetour = 432;
          }
        break;

      case IDCANCEL:
        break;
      }
    }
  return wRetour;
  }

//-----------------------------------------------------
DWORD bdanmgr_inserer (char *texte, DWORD *pn_ligne_anim)
  {
  UL v_ul[nb_max_ul];
  int nb_ul;
  BOOL supprime_ligne;
  BOOL accepte_ds_bd;
  DWORD erreur_val;
  DWORD erreur_val1;
  DWORD nb_ligne_libre;
  int niveau;
  int indentation;
  DWORD *ptr_n_ligne_libre;

  texte_to_v_ul (&nb_ul, v_ul, texte);
  if (!existe_repere (b_anmgr_n_desc_libre))
    {
    *pn_ligne_anim = lis_nombre_de_ligne_vdi () + 1;
    valide_ligne_vdi (INSERE_LIGNE, *pn_ligne_anim, v_ul, &nb_ul,
                         &supprime_ligne, &accepte_ds_bd,
                         &erreur_val, &niveau, &indentation);
    }
  else
    {
    nb_ligne_libre = nb_enregistrements (szVERIFSource, __LINE__, b_anmgr_n_desc_libre);
    if (nb_ligne_libre != 0)
      {
      ptr_n_ligne_libre = (DWORD *)pointe_enr (szVERIFSource, __LINE__, b_anmgr_n_desc_libre, nb_ligne_libre);
      *pn_ligne_anim = (*ptr_n_ligne_libre);
      valide_ligne_vdi (SUPPRIME_LIGNE, *pn_ligne_anim, v_ul, &nb_ul,
                           &supprime_ligne, &accepte_ds_bd,
                           &erreur_val, &niveau, &indentation);
      if (supprime_ligne)
        {
        valide_ligne_vdi (INSERE_LIGNE, *pn_ligne_anim, v_ul, &nb_ul,
                             &supprime_ligne, &accepte_ds_bd,
                             &erreur_val, &niveau, &indentation);

        if (erreur_val == 0)
          {
          enleve_enr (szVERIFSource, __LINE__, 1, b_anmgr_n_desc_libre, nb_ligne_libre);
          }
        else
          {
          texte[0] = CAR_COMMENTAIRE;
          texte[1] = '\0';
          texte_to_v_ul (&nb_ul, v_ul, texte);
          valide_ligne_vdi (INSERE_LIGNE, *pn_ligne_anim, v_ul, &nb_ul,
                               &supprime_ligne, &accepte_ds_bd,
                               &erreur_val1, &niveau, &indentation);
          }
        }
      }
    else
      {
      *pn_ligne_anim = lis_nombre_de_ligne_vdi () + 1;
      valide_ligne_vdi (INSERE_LIGNE, *pn_ligne_anim, v_ul, &nb_ul,
                           &supprime_ligne, &accepte_ds_bd,
                           &erreur_val, &niveau, &indentation);
      }
    }
  return (erreur_val);
  }

//-----------------------------------------------------
DWORD bdanmgr_modifier (char *texte, DWORD n_ligne_anim)
  {
  UL v_ul[nb_max_ul];
  int nb_ul;
  BOOL supprime_ligne;
  BOOL accepte_ds_bd;
  DWORD erreur_val;
  int niveau;
  int indentation;

  if (n_ligne_anim <= lis_nombre_de_ligne_vdi ())
    {
    texte_to_v_ul (&nb_ul, v_ul, texte);
    valide_ligne_vdi (MODIFIE_LIGNE, n_ligne_anim, v_ul, &nb_ul,
                         &supprime_ligne, &accepte_ds_bd,
                         &erreur_val, &niveau, &indentation);
    }
  else
    {
    erreur_val = 0;
    }
  return (erreur_val);
  }

//-----------------------------------------------------
void bdanmgr_consulter (char *texte, DWORD n_ligne_anim, ANIMATION *TypeAnim, DWORD dwTailleTexte)
  {
  UL v_ul[nb_max_ul];
  int nb_ul;
  BOOL supprime_ligne;
  BOOL accepte_ds_bd;
  DWORD erreur_val;
  int niveau;
  int indentation;
  DWORD mot_res;

  if (n_ligne_anim <= lis_nombre_de_ligne_vdi ())
    {
    valide_ligne_vdi (CONSULTE_LIGNE, n_ligne_anim, v_ul, &nb_ul,
                         &supprime_ligne, &accepte_ds_bd,
                         &erreur_val, &niveau, &indentation);
    mot_res =  nChercheIdMotReserve (v_ul[0].show);
    v_ul_to_texte (nb_ul, v_ul, texte, dwTailleTexte);
    switch (mot_res)
      {
      case c_res_ign_r_l:
        (*TypeAnim) = ANIMATION_R_1_LOG;
        break;
      case c_res_ign_r_l_4:
        (*TypeAnim) = ANIMATION_R_2_LOG;
        break;
      case c_res_ign_r_l_deplacement:
        (*TypeAnim) = ANIMATION_DEPLACE;
        break;
      default:
        break;
      }
    }
  else
    {
    texte [0] = '\0';
    (*TypeAnim) = ANIMATION_R_1_LOG;
    }
  }

//-----------------------------------------------------
BOOL bdanmgr_supprimable (DWORD n_ligne_anim)
  {
  BOOL bSuppressionPossible;

  if (n_ligne_anim != 0)
    {
    if (n_ligne_anim <= lis_nombre_de_ligne_vdi ())
      {
      bSuppressionPossible = suppression_possible_vdi (n_ligne_anim);
      }
    else
      {
      bSuppressionPossible = TRUE;
      }
    }
  else
    {
    bSuppressionPossible = TRUE;
    }
  return (bSuppressionPossible);
  }

//-----------------------------------------------------
BOOL bdanmgr_supprimer (DWORD n_ligne_anim)
  {
  char texte[255];
  DWORD *ptr_n_ligne_libre;
  DWORD nb_ligne_libre;
  BOOL bRetour;
  UL v_ul[nb_max_ul];
  int nb_ul;
  BOOL supprime_ligne;
  BOOL accepte_ds_bd;
  DWORD erreur_val;
  int niveau;
  int indentation;

  if ((n_ligne_anim > 0) && (n_ligne_anim <= lis_nombre_de_ligne_vdi ()))
    {
    valide_ligne_vdi (SUPPRIME_LIGNE, n_ligne_anim, v_ul, &nb_ul,
                         &bRetour, &accepte_ds_bd,
                         &erreur_val, &niveau, &indentation);
    if (bRetour)
      {
      texte[0] = CAR_COMMENTAIRE;
      texte[1] = '\0';
      texte_to_v_ul (&nb_ul, v_ul, texte);
      valide_ligne_vdi (INSERE_LIGNE, n_ligne_anim, v_ul, &nb_ul,
                           &supprime_ligne, &accepte_ds_bd,
                           &erreur_val, &niveau, &indentation);
      if (!existe_repere (b_anmgr_n_desc_libre))
        {
        cree_bloc (b_anmgr_n_desc_libre, sizeof (DWORD), 0);
        }
      nb_ligne_libre = nb_enregistrements (szVERIFSource, __LINE__, b_anmgr_n_desc_libre) + 1;
      insere_enr (szVERIFSource, __LINE__, 1, b_anmgr_n_desc_libre, nb_ligne_libre);
      ptr_n_ligne_libre = (DWORD *)pointe_enr (szVERIFSource, __LINE__, b_anmgr_n_desc_libre, nb_ligne_libre);
      (*ptr_n_ligne_libre) = n_ligne_anim;
      }
    }
  else
    {
    bRetour = FALSE;
    }
  return bRetour;
  }

//-----------------------------------------------------
BOOL bdanmgr_supprimer_tout (void)
  {
  DWORD nb_ligne_vdi = lis_nombre_de_ligne_vdi ();
  DWORD n_ligne_anim = 1;
  BOOL bPossible = TRUE;

  while ((n_ligne_anim <= nb_ligne_vdi) && (bPossible))
    {
    bPossible = bdanmgr_supprimable (n_ligne_anim);
    n_ligne_anim++;
    }
  if (bPossible)
    {
    for (n_ligne_anim = 1; (n_ligne_anim <= nb_ligne_vdi); n_ligne_anim++)
      {
      bdanmgr_supprimer (n_ligne_anim);
      }
    }
  return bPossible;
  }

//-----------------------------------------------------
void bdanmgr_supprimer_tout_inutilise (void)
  {
	// remplace par un commentaire toutes les lignes de descriptions d'animation non associ�es � un symbole
	// h�ritage d'import d'ancienne version ou plusieurs animations �taient^possibles sur le m�me symbole

  DWORD nb_ligne_vdi = lis_nombre_de_ligne_vdi ();
  DWORD n_ligne_anim = 1;
	DWORD dwNumeroPage;
	char strIdentifiant [c_NB_CAR_ID_ELT_GR_ANIM + 1];

  for (n_ligne_anim = 1; (n_ligne_anim <= nb_ligne_vdi); n_ligne_anim++)
    {
    if (bdanmgr_get_identifiant_animation (n_ligne_anim, &dwNumeroPage, strIdentifiant))
			{
			if (dwNumeroPage != 0)
				{
				StrDeleteDoubleQuotation (strIdentifiant);
				if (StrIsNull (strIdentifiant))
					{
					if (bdanmgr_supprimable (n_ligne_anim))
						{
						ANIMATION TypeAnim;
						char texte[513];
						DWORD dwNewLigneAnim;
						bdanmgr_consulter (texte, n_ligne_anim, &TypeAnim, 513);
						StrInsertStr (texte, "##", 0);
						StrInsertChar (texte, CAR_COMMENTAIRE, 0);
						bdanmgr_supprimer (n_ligne_anim);
						bdanmgr_inserer (texte, &dwNewLigneAnim);
						}
					}
				}
			}
    }
	return;
  }

//-----------------------------------------------------
BOOL bdanmgr_lis_couleurs2 (DWORD n_ligne_anim, DWORD indice, PG_COULEUR pnCouleur, PG_COULEUR pnCouleurFond)
  {
  BOOL bRetour = FALSE;

  if ((n_ligne_anim > 0) && (n_ligne_anim <= lis_nombre_de_ligne_vdi ()))
    {
    bRetour = lis_couleurs2_vdi (n_ligne_anim, indice, pnCouleur, pnCouleurFond);
    }

  return bRetour;
  }
//-----------------------------------------------------
BOOL bdanmgr_lis_couleurs4 (DWORD n_ligne_anim, DWORD indice, PG_COULEUR pnCouleur, PG_COULEUR pnCouleurFond)
  {
  BOOL bRetour = FALSE;

  if ((n_ligne_anim > 0) && (n_ligne_anim <= lis_nombre_de_ligne_vdi ()))
    {
    bRetour = lis_couleurs4_vdi (n_ligne_anim, indice, pnCouleur, pnCouleurFond);
    }

  return bRetour;
  }

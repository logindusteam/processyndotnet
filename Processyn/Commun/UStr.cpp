//---------------------------------------------------------------------------
#include "stdafx.h"
#include <stdio.h> //sprintf
#include "Std.h"

#include "UStr.h"

//---------------------------------------------------------------------------
PSTR StrCopySans0Terminateur (PSTR pStrDst, PCSTR pStrSrc)
  {
  DWORD wIdx = 0;
  while ((pStrSrc [wIdx] != '\0'))
    {
    pStrDst [wIdx] = pStrSrc [wIdx];
    wIdx++;
    }

  return pStrDst;
  }

//---------------------------------------------------------------------------
PSTR StrCopyUpToChar (PSTR pStrDst, PCSTR pStrSrc, CHAR cChar)
  {
  DWORD wIdx = 0;
  while ((pStrSrc [wIdx] != '\0') && (pStrSrc [wIdx] != cChar))
    {
    pStrDst [wIdx] = pStrSrc [wIdx];
    wIdx++;
    }
  pStrDst [wIdx] = '\0';

  return pStrDst;
  }

//---------------------------------------------------------------------------
PSTR StrCopyUpToLength (PSTR pStrDst, PCSTR pStrSrc, UINT wLen)
  {
  DWORD wIdx = 0;

  while ((wIdx < wLen) && (pStrSrc [wIdx] != '\0'))
    {
    pStrDst [wIdx] = pStrSrc [wIdx];
    wIdx++;
    }
  pStrDst [wIdx] = '\0';

  return pStrDst;
  }

//---------------------------------------------------------------------------
PSTR StrExtract (PSTR pStrDst, PCSTR pStrSrc, UINT wIdxChar, UINT wCharCount)
  {
  DWORD wIdx;

  wIdx = 0;
  while ((wIdx < wCharCount) && (pStrSrc [wIdxChar + wIdx] != '\0'))
    {
    pStrDst [wIdx] = pStrSrc [wIdxChar + wIdx];
    wIdx++;
    }
  pStrDst [wIdx] = '\0';

  return pStrDst;
  }

//---------------------------------------------------------------------------
PSTR StrConcatChar (PSTR pStrDst, CHAR cChar)
  {
  DWORD wIdx;

  wIdx = strlen (pStrDst);
  pStrDst [wIdx] = cChar;
  pStrDst [wIdx + 1] = '\0';

  return pStrDst;
  }

//---------------------------------------------------------------------------
PSTR StrInsertStr (PSTR pStrDst, PCSTR pStrSrc, UINT wIdxChar)
  {
  DWORD wSizeDst;
  DWORD wSizeSrc;
  DWORD wIdx;

  wSizeSrc = strlen (pStrSrc);
  if (wSizeSrc > 0)
    {
    wSizeDst = strlen (pStrDst);
    //
    for (wIdx = wSizeDst; wIdx > wIdxChar; wIdx--)
      {
      pStrDst [wIdx + wSizeSrc] = pStrDst [wIdx];
      }
    pStrDst [wIdxChar + wSizeSrc] = pStrDst [wIdxChar];
    //
    for (wIdx = 0; wIdx < wSizeSrc; wIdx++)
      {
      pStrDst [wIdx + wIdxChar] = pStrSrc [wIdx];
      }
    }

  return pStrDst;
  }

//---------------------------------------------------------------------------
PSTR StrInsertChar (PSTR pStrDst, CHAR cChar, UINT wIdxChar)
  {
  DWORD wSize;
  DWORD wIdx;

  wSize = strlen (pStrDst);
  //
  for (wIdx = wSize; wIdx > wIdxChar; wIdx--)
    {
    pStrDst [wIdx + 1] = pStrDst [wIdx];
    }
  pStrDst [wIdxChar + 1] = pStrDst [wIdxChar];
  //
  pStrDst [wIdxChar] = cChar;

  return pStrDst;
  }

//---------------------------------------------------------------------------
BOOL bStrEgales (PCSTR pStr1, PCSTR pStr2)
  {
	// Tant que les cars sont identiques
	while (*pStr1 == *pStr2)
		{
		// Fin de chaine ?
		if (!*pStr1)
			// oui => termin�, les chaines sont identiques
			return TRUE;
		// non => cars suivants
		pStr1++;
		pStr2++;
		}
	// diff�rence trouv�e => chaines diff�rentes
  return FALSE; //(strcmp (pStr1, pStr2) == 0);
  }

//---------------------------------------------------------------------------
DWORD StrSearchStr (PCSTR pStr1, PCSTR pStr2)
  {
  DWORD  wIdx;
  LPCSTR pStrOccurence;

  pStrOccurence = strstr (pStr1, pStr2);
  if (pStrOccurence)
    {
    wIdx = (DWORD) abs (pStrOccurence - pStr1);
    }
  else
    {
    wIdx = STR_NOT_FOUND;
    }

  return wIdx;
  }

//---------------------------------------------------------------------------
DWORD StrSearchChar (PCSTR pStr, CHAR cChar)
  {
  DWORD  wIdx = STR_NOT_FOUND;
  LPCSTR pStrOccurence = strchr (pStr, cChar);
  if (pStrOccurence)
    wIdx = pStrOccurence - pStr;

  return wIdx;
  }

//---------------------------------------------------------------------------
BOOL StrAllCharsAreInStr (PSTR pStr, PCSTR pStrSetOfChar)
  {
  BOOL	bIn = TRUE;
  DWORD	wIdx = 0;

  while (bIn && pStr [wIdx] != '\0')
    {
    bIn = (BOOL) (bIn && (StrSearchChar (pStrSetOfChar, pStr [wIdx]) != STR_NOT_FOUND));
    wIdx++;
    }

  return bIn;
  }

//---------------------------------------------------------------------------
DWORD StrFirstCharInStr (PSTR pStr, PCSTR pStrSetOfChar)
  {
  DWORD wIdx = strcspn (pStr, pStrSetOfChar);

  if (pStr [wIdx] == '\0')
    wIdx = STR_NOT_FOUND;

  return wIdx;
  }

//---------------------------------------------------------------------------
PSTR StrDelete (PSTR pStr, UINT wIdxStart, UINT wCount)
  {
  DWORD wIdxDst = wIdxStart;
  DWORD wIdxSrc = wIdxStart + wCount;

  while (pStr [wIdxSrc] != '\0')
    {
    pStr [wIdxDst] = pStr [wIdxSrc];
    wIdxDst++;
    wIdxSrc++;
    }
  pStr [wIdxDst] = '\0';

  return pStr;
  }

//---------------------------------------------------------------------------
CHAR StrUpperCaseChar (CHAR cChar)
  {
  CHAR cUpperChar;

  if ((cChar >= 'a') && (cChar <= 'z'))
    {
    cUpperChar = (CHAR) ((cChar - 'a') + 'A');
    }
  else
    {
    cUpperChar = cChar;
    }

  return cUpperChar;
  }

//---------------------------------------------------------------------------
CHAR StrLowerCaseChar (CHAR cChar)
  {
  CHAR cLowerChar;

  if ((cChar >= 'A') && (cChar <= 'Z'))
    cLowerChar = (CHAR) ((cChar - 'A') + 'a');
  else
    cLowerChar = cChar;

  return cLowerChar;
  }

//---------------------------------------------------------------------------
PSTR StrEnclose (PSTR pStr, CHAR cChar1, CHAR cChar2)
  {
  DWORD wSize = strlen (pStr);
  DWORD wIdx;

  for (wIdx = wSize; wIdx > 0; wIdx--)
    {
    pStr [wIdx] = pStr [wIdx - 1];
    }
  pStr [0] = cChar1;
  pStr [wSize + 1] = cChar2;
  pStr [wSize + 2] = '\0';
  //
  return pStr;
  }

//---------------------------------------------------------------------------
PSTR StrAddDoubleQuotation (PSTR pStr)
  {
  return (StrEnclose (pStr, '\"', '\"'));
  }

//---------------------------------------------------------------------------
PSTR StrDeleteAllChars (PSTR pStr, CHAR cChar)
  {
  DWORD wIdx = 0;

  while (pStr [wIdx] != '\0')
    {
    if (pStr [wIdx] == cChar)
      {
      StrDelete (pStr, wIdx, 1);
      }
    else
      {
      wIdx++;
      }
    }

  return pStr;
  }

//---------------------------------------------------------------------------
PSTR StrDeleteDoubleQuotation (PSTR pStr)
  {
  return (StrDeleteAllChars (pStr, '\"'));
  }

//---------------------------------------------------------------------------
PSTR StrDeleteSpaces (PSTR pStr)
  {
  return (StrDeleteAllChars (pStr, ' '));
  }

//---------------------------------------------------------------------------
PSTR StrDeleteFirstChars (PSTR pStr, CHAR cChar)
  {
  while ((pStr [0] == cChar) && (pStr [0] != '\0'))
    {
    StrDelete (pStr, 0, 1);
    }

  return pStr;
  }

//---------------------------------------------------------------------------
PSTR StrDeleteFirstSpaces (PSTR pStr)
  {
  return (StrDeleteFirstChars (pStr, ' '));
  }

//---------------------------------------------------------------------------
PSTR StrDeleteLastChars (PSTR pStr, CHAR cChar)
  {
  DWORD    wSize = strlen (pStr);
  BOOL bContinue = TRUE;

  while (bContinue && (wSize > 0))
    {
    if (pStr [wSize - 1] == cChar)
      {
      wSize--;
      }
    else
      {
      bContinue = FALSE;
      }
    }
  pStr [wSize] = '\0';

  return pStr;
  }

//---------------------------------------------------------------------------
PSTR StrDeleteLastSpaces (PSTR pStr)
  {
  return (StrDeleteLastChars (pStr, ' '));
  }

//---------------------------------------------------------------------------
PSTR StrDeleteFirstAndLastSpacesAndTabs (PSTR pStr)
  {
	StrDeleteLastChars(pStr, ' ');
	StrDeleteLastChars (pStr, '\t');
	StrDeleteFirstChars(pStr, ' ');
	StrDeleteFirstChars (pStr, '\t');

  return StrDeleteFirstChars (pStr, '\t');
  }

//---------------------------------------------------------------------------
BOOL StrCharCanBeDisplayed (CHAR cChar)
  {
  return ((BOOL) !iscntrl (cChar));
  }

//---------------------------------------------------------------------------
BOOL StrStringCanBeDisplayed (PCSTR pStr)
  {
  BOOL bDisplayable = TRUE;
  DWORD    wIdx = 0;

  while (bDisplayable && (pStr [wIdx] != '\0'))
    {
    bDisplayable = (BOOL) (bDisplayable && StrCharCanBeDisplayed (pStr [wIdx]));
    wIdx++;
    }

  return bDisplayable;
  }

//---------------------------------------------------------------------------
PSTR StrMakeStringDisplayable (PSTR pStr)
  {
  DWORD wIdx = 0;

  while (pStr [wIdx] != '\0')
    {
    if (StrCharCanBeDisplayed (pStr [wIdx]))
      {
      wIdx++;
      }
    else
      {
      StrDelete (pStr, wIdx, 1);
      }
    }

  return (pStr);
  }

//---------------------------------------------------------------------------
PSTR StrFilter (PSTR pStr, CHAR cCharMin, CHAR cCharMax)
  {
  DWORD wIdx = 0;

  while (pStr [wIdx] != '\0')
    {
    if ((pStr [wIdx] >= cCharMin) && (pStr [wIdx] <= cCharMax))
      {
      wIdx++;
      }
    else
      {
      StrDelete (pStr, wIdx, 1);
      }
    }

  return (pStr);
  }

//---------------------------------------------------------------------------
PSTR StrCHARToStr (PSTR pStr, CHAR cChar)
  {
  pStr [0] = cChar;
  pStr [1] = '\0';

  return pStr;
  }

//---------------------------------------------------------------------------
PSTR StrBOOLToStr (PSTR pStr, BOOL bBool, PCSTR pStr0, PCSTR pStr1)
  {
  if (bBool)
    {
    strcpy (pStr, pStr1);
    }
  else
    {
    strcpy (pStr, pStr0);
    }

  return pStr;
  }

//---------------------------------------------------------------------------
PSTR StrBYTEToStr (PSTR pStr, BYTE bByte)
  {
  return (ultoa (bByte, pStr, 10));
  }

//---------------------------------------------------------------------------
PSTR StrWORDToStr (PSTR pStr, WORD word)
  {
  return (ultoa (word, pStr, 10));
  }

//---------------------------------------------------------------------------
PSTR StrDWordToStr (PSTR pStr, DWORD dword)
  {
  return (ultoa (dword, pStr, 10));
  }

//---------------------------------------------------------------------------
PSTR StrINTToStr (PSTR pStr, INT integer)
  {
  return (ltoa (integer, pStr, 10));
  }

//---------------------------------------------------------------------------
PSTR StrLONGToStr (PSTR pStr, LONG longInt)
  {
  return (ltoa (longInt, pStr, 10));
  }

//---------------------------------------------------------------------------
PSTR StrBYTEToStrBase (PSTR pStr, BYTE bByte, UINT wRadix)
  {
  return (ultoa (bByte, pStr, (int) wRadix));
  }

//---------------------------------------------------------------------------
PSTR StrWORDToStrBase (PSTR pStr, WORD word, UINT wRadix)
  {
  return (ultoa (word, pStr, (int) wRadix));
  }

//---------------------------------------------------------------------------
PSTR StrDWordToStrBase (PSTR pStr, DWORD dword, UINT wRadix)
  {
  return (ultoa (dword, pStr, (int) wRadix));
  }

//---------------------------------------------------------------------------
PSTR StrINTToStrBase (PSTR pStr, INT integer, UINT wRadix)
  {
  return (ltoa (integer, pStr, (int) wRadix));
  }

//---------------------------------------------------------------------------
PSTR StrLONGToStrBase (PSTR pStr, LONG longInt, UINT wRadix)
  {
  return (ltoa (longInt, pStr, (int) wRadix));
  }

//---------------------------------------------------------------------------
PSTR StrFLOATToStr (PSTR pStr, FLOAT rReal)
  {
  sprintf (pStr, "%g", rReal);
  return pStr;
  }

//---------------------------------------------------------------------------
PSTR StrDOUBLEToStr (PSTR pStr, DOUBLE lrLongReal)
  {
  sprintf (pStr, "%lg", lrLongReal);
  return pStr;
  }

//---------------------------------------------------------------------------
BOOL StrToCHAR (PSTR pcChar, PCSTR pStr)
  {
  BOOL bOk;

  if (pStr [0] != '\0')
    {
    if (pStr [1] == '\0')
      {
      (*pcChar) = pStr [0];
      bOk = TRUE;
      }
    else
      {
      bOk = FALSE;
      }
    }
  else
    {
    bOk = FALSE;
    }

  return (bOk);
  }

//---------------------------------------------------------------------------
BOOL StrToBOOL (PBOOL pbBool, PCSTR pStr, PCSTR pStr0, PCSTR pStr1)
  {
  BOOL bOk;

  if (strcmp (pStr, pStr1) == 0)
    {
    (*pbBool) = TRUE;
    bOk = TRUE;
    }
  else
    {
    if (strcmp (pStr, pStr0) == 0)
      {
      (*pbBool) = FALSE;
      bOk = FALSE;
      }
    else
      {
      bOk = FALSE;
      }
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL StrToBYTE (PBYTE pbByte, PCSTR pStr)
  {
  BOOL     bOk = FALSE;
  PSTR       pStrEnd = NULL;
  unsigned long ulValue;

  if (strchr (pStr, '-') == NULL)
    {
    //pStrEnd = pStr;
    ulValue = strtoul (pStr, &pStrEnd, 10);
    if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
      {
     if (ulValue <= MAXBYTE)
        {
        (*pbByte) = (BYTE) ulValue;
        bOk = TRUE;
        }
      }
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL StrToWORD (WORD * pwWord, PCSTR pStr)
  {
  BOOL	bOk = FALSE;
  PSTR  pStrEnd = NULL;
  unsigned long ulValue;

  if (strchr (pStr, '-') == NULL)
    {
    ulValue = strtoul (pStr, &pStrEnd, 10);
    if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
      {
      if (ulValue <= MAXWORD)
        {
        (*pwWord) = (WORD) ulValue;
        bOk = TRUE;
        }
      }
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL StrToDWORD (PDWORD pdword, PCSTR pStr)
  {
  BOOL	bOk = FALSE;
  PSTR  pStrEnd = NULL;
  unsigned long ulValue;

  if (strchr (pStr, '-') == NULL)
    {
    ulValue = strtoul (pStr, &pStrEnd, 10);
    if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
      {
      (*pdword) = (DWORD) ulValue;
      bOk = TRUE;
      }
    }

  return bOk;
  }


//---------------------------------------------------------------------------
BOOL StrToINT (PINT piInteger, PCSTR pStr)
  {
  BOOL bOk = FALSE;
  PSTR pStrEnd = NULL;
  long lValue = strtol (pStr, &pStrEnd, 10);

  if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
    {
    (*piInteger) = (INT) lValue;
    bOk = TRUE;
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL  StrToLONG	 (PLONG	 plong,  PCSTR pStr)
  {
  BOOL	bOk = FALSE;
  PSTR  pStrEnd = NULL;
  long  lValue = strtol (pStr, &pStrEnd, 10);

  if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
    {
    (*plong) = lValue;
    bOk = TRUE;
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL  StrToSHORT (SHORT * pi16, PCSTR pStr)
  {
  BOOL	bOk = FALSE;
  PSTR  pStrEnd = NULL;
  long  lValue = strtol (pStr, &pStrEnd, 10);

  if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
    {
    if ((lValue >= (short)MINSHORT) && (lValue <= (short)MAXSHORT))
      {
      (*pi16) = (SHORT) lValue;
      bOk = TRUE;
      }
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL  StrToI64   (__int64 *pint64, PCSTR pStr)
	{
  BOOL	bOk = FALSE;
  __int64 i64Value = _atoi64 (pStr);

	if (i64Value == 0)
		{
		bOk = bStrEgales (pStr, "0");
		}
	else
		{
		bOk = TRUE;
		}
	if (bOk)
		{
		(*pint64) = i64Value;
		}
	return (bOk);
	}

//---------------------------------------------------------------------------
BOOL StrToBYTEBase (PBYTE pbByte, PCSTR pStr, UINT wRadix)
  {
  BOOL     bOk;
  PSTR       pStrEnd = NULL;
  unsigned long ulValue;

  bOk = FALSE;
  if (strchr (pStr, '-') == NULL)
    {
    //pStrEnd = pStr;
    ulValue = strtoul (pStr, &pStrEnd, (int) wRadix);
    if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
      {
     if (ulValue <= MAXBYTE)
        {
        (*pbByte) = (BYTE) ulValue;
        bOk = TRUE;
        }
      }
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL StrToWORDBase (WORD * pwWord, PCSTR pStr, UINT wRadix)
  {
  BOOL     bOk;
  PSTR       pStrEnd = NULL;
  unsigned long ulValue;

  bOk = FALSE;
  if (strchr (pStr, '-') == NULL)
    {
    //pStrEnd = pStr;
    ulValue = strtoul (pStr, &pStrEnd, (int) wRadix);
    if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
      {
      if (ulValue <= MAXWORD)
        {
        (*pwWord) = (WORD) ulValue;
        bOk = TRUE;
        }
      }
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL StrToDWORDBase (PDWORD pdword, PCSTR pStr, UINT wRadix)
  {
  BOOL     bOk;
  PSTR  pStrEnd = NULL;
  unsigned long ulValue;

  bOk = FALSE;
  if (strchr (pStr, '-') == NULL)
    {
    //pStrEnd = pStr;
    ulValue = strtoul (pStr, &pStrEnd, (int) wRadix);
    if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
      {
      (*pdword) = (DWORD) ulValue;
      bOk = TRUE;
      }
    }

  return bOk;
  }


//---------------------------------------------------------------------------
BOOL StrToINTBase (PINT piInteger, PCSTR pStr, UINT wRadix)
  {
  BOOL bOk;
  PSTR   pStrEnd = NULL;
  long      lValue;

  bOk = FALSE;
  lValue = strtol (pStr, &pStrEnd, (int) wRadix);
  if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
    {
    (*piInteger) = (INT) lValue;
    bOk = TRUE;
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL  StrToLONGBase	 (PLONG	 plong,  PCSTR pStr, UINT wRadix)
  {
  BOOL	bOk;
  PSTR  pStrEnd = NULL;
  long  lValue;

  bOk = FALSE;
  //pStrEnd = pStr;
  lValue = strtol (pStr, &pStrEnd, (int) wRadix);
  if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
    {
    (*plong) = lValue;
    bOk = TRUE;
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL  StrToSHORTBase (SHORT * pi16, PCSTR pStr, UINT wRadix)
  {
  BOOL bOk;
  PSTR   pStrEnd = NULL;
  long      lValue;

  bOk = FALSE;
  //pStrEnd = pStr;
  lValue = strtol (pStr, &pStrEnd, (int) wRadix);
  if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
    {
    if ((lValue >= MINSHORT) && (lValue <= MAXSHORT))
      {
      (*pi16) = (SHORT) lValue;
      bOk = TRUE;
      }
    }

  return bOk;
  }

//---------------------------------------------------------------------------
BOOL StrToFLOAT (PFLOAT prReal, PCSTR pStr)
  {
  BOOL bOk;
  PSTR   pStrEnd = NULL;
  double    dValue;
  double    dAbsValue;

  bOk = FALSE;
  //pStrEnd = pStr;
  dValue = strtod (pStr, &pStrEnd);
  if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
    {
    if (dValue < 0)
      {
      dAbsValue = -dValue;
      }
    else
      {
      dAbsValue = dValue;
      }
    if (dAbsValue < FLOAT_MIN)
      {
      (*prReal) = (FLOAT)0;
      bOk = TRUE;
      }
    else
      {
      if (dAbsValue <= FLOAT_MAX)
        {
        (*prReal) = (FLOAT) dValue;
        bOk = TRUE;
        }
      }
    }

  return (bOk);
  }

//---------------------------------------------------------------------------
BOOL StrToDOUBLE (double * prReal, PCSTR pStr)
  {
  BOOL bOk;
  PSTR   pStrEnd = NULL;
  double    dValue;
  double    dAbsValue;

  bOk = FALSE;
  //pStrEnd = pStr;
  dValue = strtod (pStr, &pStrEnd);
  if ((pStrEnd != pStr) && ((*pStrEnd) == '\0'))
    {
    if (dValue < 0)
      {
      dAbsValue = -dValue;
      }
    else
      {
      dAbsValue = dValue;
      }
    if (dAbsValue < DOUBLE_MIN)
      {
      (*prReal) = (DOUBLE)0;
      bOk = TRUE;
      }
    else
      {
      if (dAbsValue <= DOUBLE_MAX)
        {
        (*prReal) = (DOUBLE) dValue;
        bOk = TRUE;
        }
      }
    }

  return (bOk);
  }
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
DWORD StrToStrArray (CHAR ppStrArray [] [STR_MAX_CHAR_ARRAY], UINT wMaxArray, PSTR pStr)
  {
  DWORD    wIdxArray;
  DWORD    wIdxCharArray;
  DWORD    wIdxStr;
  BOOL bInMessage;

  wIdxArray = 0;
  if (wMaxArray > 0)
    {
    wIdxCharArray  = 0;
    wIdxStr = 0;
    bInMessage = FALSE;
    while ((pStr [wIdxStr] != '\0') && (wIdxArray < wMaxArray))
      {
      switch (pStr [wIdxStr])
        {
        case ' ':
             if (bInMessage)
               {
               if (wIdxCharArray < STR_MAX_CHAR_ARRAY)
                 {
                 ppStrArray [wIdxArray] [wIdxCharArray] = pStr [wIdxStr];
                 wIdxCharArray++;
                 }
               }
             else
               {
               if (wIdxCharArray != 0)
                 {
                 ppStrArray [wIdxArray] [wIdxCharArray] = '\0';
                 wIdxCharArray = 0;
                 wIdxArray++;
                 }
               }
             break;
        case '\"':
             if (bInMessage)
               {
               if (wIdxCharArray < STR_MAX_CHAR_ARRAY)
                 {
                 ppStrArray [wIdxArray] [wIdxCharArray] = '\"';
                 wIdxCharArray++;
                 }
               else
                 {
                 ppStrArray [wIdxArray] [wIdxCharArray - 1] = '\"';
                 }
               ppStrArray [wIdxArray] [wIdxCharArray] = '\0';
               wIdxCharArray = 0;
               wIdxArray++;
               }
             else
               {
               if (wIdxCharArray != 0)
                 {
                 ppStrArray [wIdxArray] [wIdxCharArray] = '\0';
                 wIdxCharArray = 0;
                 wIdxArray++;
                 }
               if (wIdxArray < wMaxArray)
                 {
                 ppStrArray [wIdxArray] [wIdxCharArray] = '\"';
                 wIdxCharArray++;
                 }
               }
             bInMessage = (BOOL) !bInMessage;
             break;
        default:
             if (wIdxCharArray < STR_MAX_CHAR_ARRAY)
               {
               ppStrArray [wIdxArray] [wIdxCharArray] = pStr [wIdxStr];
               wIdxCharArray++;
               }
             break;
        }
      wIdxStr++;
      }
    //
    if (wIdxCharArray != 0)
      {
      if (bInMessage)
        {
        if (wIdxCharArray < STR_MAX_CHAR_ARRAY)
          {
          ppStrArray [wIdxArray] [wIdxCharArray] = '\"';
          wIdxCharArray++;
          }
        else
          {
          ppStrArray [wIdxArray] [wIdxCharArray - 1] = '\"';
          }
        }
      ppStrArray [wIdxArray] [wIdxCharArray] = '\0';
      wIdxCharArray = 0;
      wIdxArray++;
      }
    }

  return (wIdxArray);
  }
//---------------------------------------------------------------------------
PSTR StrArrayToStrSep (PSTR pStr, CHAR ppStrArray [] [STR_MAX_CHAR_ARRAY], UINT wArrayCount, CHAR cSeparateur)
// Permet de copier les chaines d'un tableau dans une chaine, chaque champ etant separ� par cSeparateur
// Si cSeparateur est un \0, la chaine est termin�e par un double \0
  {
  DWORD wIdx;
  DWORD wIdxCharArray;
  DWORD wIdxStr;

  wIdxStr = 0;
  wIdx = 0;
  while (wIdx < wArrayCount)
    {
    wIdxCharArray = 0;
    while (ppStrArray [wIdx] [wIdxCharArray] != '\0')
      {
      pStr [wIdxStr] = ppStrArray [wIdx] [wIdxCharArray];
      wIdxStr++;
      wIdxCharArray++;
      }
    pStr [wIdxStr] = cSeparateur;
    wIdxStr++;
    wIdx++;
    }
  //
  if (wIdxStr > 0)
    {
		if (cSeparateur != '\0')
			{
      pStr [wIdxStr - 1] = '\0'; // le dernier cSeparateur est bouff�
			}
		else
			{
      pStr [wIdxStr] = '\0'; // fini par un double \0
			}
    }
  else
    {
    pStr [0] = '\0';
    }

  return (pStr);
  }


//---------------------------------------------------------------------------
PSTR StrArrayToStr (PSTR pStr, CHAR ppStrArray [] [STR_MAX_CHAR_ARRAY], UINT wArrayCount)
  {
	return StrArrayToStrSep(pStr, ppStrArray, wArrayCount, ' ');
  }

//---------------------------------------------------------------------------
INT _cdecl StrPrintFormat (PSTR pStr, PCSTR pStrFormat, ...)
  {
  va_list pArgPointer;

  va_start (pArgPointer, pStrFormat);
  INT nRet = vsprintf (pStr, pStrFormat, pArgPointer);
  va_end (pArgPointer);

  return nRet;
  }

//---------------------------------------------------------------------------


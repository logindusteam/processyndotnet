//--------------------------------------------------------------
// BufMan.c
// gestion de buffers
// Historique : 
// - Version 1.0
// -------------------------------------------------------------
#include "stdafx.h"
#include "BufMan.h"


//------------------------------------------------
// ajoute le buffer source � la fin du buffer dest
// avec allocation / r�allocation automatique
// renvoie FALSE si erreur (= allocation)
BOOL bBufAjouteFin (PSTR * ppDest, PDWORD pdwLgDest, PSTR pSource, DWORD dwLgSource)
	{
	// rien � ajouter => termin�
	if (!dwLgSource)
		return TRUE;

	// d�ja des donn�es dans Dest ?
	if (*pdwLgDest)
		{
		// oui => dest r�allou� � la taille totale
		*ppDest = (PSTR)realloc (*ppDest, (*pdwLgDest) + dwLgSource);
		if (!*ppDest)
			return FALSE;
		}
	else
		{
		// non => on alloue � la taille de source
		if (*ppDest)
			return FALSE;
		*ppDest = (PSTR)malloc (dwLgSource);
		if (!*ppDest)
			return FALSE;
		}
	// on copie source en fin de dest
	CopyMemory ((*ppDest) + (*pdwLgDest), pSource, dwLgSource);

	// on comptabilise les octets rajout�s
	*pdwLgDest += dwLgSource;
	return TRUE;
	} // bBufAjouteFin

//---------------------------------------------------------------------------
// enl�ve dwLgSuppr octets au d�but du buffer (free / r�alloc au plus juste)
// renvoie FALSE si erreur (= allocation ou dwLgSuppr trop grand)
BOOL bBufSupprimeDebut (PSTR * ppDest, PDWORD pdwLgDest, DWORD dwLgSuppr)
	{
	DWORD dwLgRestant;

	// rien � supprimer => termin� Ok
	if (!dwLgSuppr)
		return TRUE;

	// pBuf non allou� ou pas assez de donn�es dans Buf => termin� err
	if ((!*ppDest) || (dwLgSuppr > *pdwLgDest))
		return FALSE;
	
	// reste t'il quelque chose apr�s ?
	dwLgRestant = *pdwLgDest - dwLgSuppr;
	if (dwLgRestant)
		{
		// oui => on tasse le buffer
		MoveMemory (*ppDest, (*ppDest) + dwLgSuppr, dwLgRestant);

		// dest r�allou� � la taille restante
		*ppDest = (PSTR)realloc (*ppDest, dwLgRestant);
		if (!*ppDest)
			return FALSE;
		}
	else
		{
		// non => on lib�re le buffer
		free (*ppDest);
		*ppDest = NULL;
		}
	// on comptabilise les octets restants
	*pdwLgDest = dwLgRestant;
	return TRUE;
	} // bBufSupprimeDebut

//---------------------------------------------------------------------------
// enl�ve dwSupprLen octets � la fin du buffer (free / r�alloc pBuf au plus juste)
// *pdwLgDest mis � jour
// renvoie FALSE si erreur (= allocation ou dwSupprLen trop grand)
BOOL bBufSupprimeFin (PSTR * ppDest, PDWORD pdwLgDest, DWORD dwLgSuppr)
	{
	DWORD dwLgRestant;

	// rien � supprimer => termin� Ok
	if (!dwLgSuppr)
		return TRUE;

	// pBuf non allou� ou pas assez de donn�es dans Buf => termin� err
	if ((!*ppDest) || (dwLgSuppr > *pdwLgDest))
		return FALSE;
	
	// reste t'il quelque chose apr�s ?
	dwLgRestant = *pdwLgDest - dwLgSuppr;
	if (dwLgRestant)
		{
		// oui => on tasse le buffer
		MoveMemory (*ppDest, (*ppDest) + dwLgSuppr, dwLgRestant);

		// dest r�allou� � la taille restante
		*ppDest = (PSTR)realloc (*ppDest, dwLgRestant);
		if (!*ppDest)
			return FALSE;
		}
	else
		{
		// non => on lib�re le buffer
		free (*ppDest);
		*ppDest = NULL;
		}
	// on comptabilise les octets restants
	*pdwLgDest = dwLgRestant;
	return TRUE;
	} // bBufSupprimeFin

//---------------------------------------------------------------------------
// Lib�re le contenu du buffer (free / r�alloc pBuf au plus juste)
// *pdwLgDest mis � 0
// renvoie FALSE si erreur (= allocation)
BOOL bBufVide (PSTR * ppDest, PDWORD pdwLgDest)
	{
	// quelque chose � vider ?
	if (*pdwLgDest)
		{
		// oui => Buffer allou� ?
		if (*ppDest)
			// oui => on lib�re le buffer
			free (*ppDest);
		else
			// non => erreur
			return FALSE;
		*ppDest = NULL;
		}
	// plus d'octets
	*pdwLgDest = 0;
	return TRUE;
	} // bBufSupprimeFin

//---------------------------------------------------------------------------
// charge un fichier dans un buf non allou�
BOOL bBufCharge(PSTR * ppDest, PDWORD pdwLgDest, PSTR pszNomFichier)
	{
	HFILE hFichier;
	BOOL bRes = FALSE;
	
	// v�rification buf non allou�
	if (*ppDest || *pdwLgDest)
		return FALSE;

	// ouverture du fichier Ok ?
	hFichier = _lopen(pszNomFichier, OF_READ|OF_SHARE_COMPAT);
	if (hFichier != HFILE_ERROR)
		{
		DWORD dwTailleFichier;

		// Oui => lecture de la taille du fichier
		dwTailleFichier = GetFileSize((HANDLE)hFichier, NULL);
		if (dwTailleFichier)
			{
			// allocation du buffer de lecture
			*ppDest = (PSTR)malloc(dwTailleFichier);

			// Lecture fichier en m�moire Ok?
			if (*ppDest && (dwTailleFichier == _lread (hFichier, *ppDest, dwTailleFichier)))
				{
				*pdwLgDest = dwTailleFichier;
				bRes = TRUE;
				}
			else
				{
				free(*ppDest);
				*ppDest = NULL;
				}
			}

		// Fermeture du fichier
		_lclose(hFichier);
		} // if (hFichier != HFILE_ERROR)
	return bRes;
	} // bBufCharge

// Ecrit le contenu d'un buf dans un fichier
BOOL bBufSauve(PSTR * ppDest, PDWORD pdwLgDest, PSTR pszNomFichier)
	{
	HFILE hFichier;
	BOOL bRes = FALSE;
	
	// ouverture du fichier Ok ?
	hFichier = _lcreat(pszNomFichier, 0);
	if (hFichier != HFILE_ERROR)
		{
		// Oui => �criture du fichier Ok?
		if (*ppDest && (*pdwLgDest == _lwrite (hFichier, *ppDest , *pdwLgDest)))
			bRes = TRUE;

		// Fermeture du fichier
		_lclose(hFichier);
		} // if (hFichier != HFILE_ERROR)
	return bRes;
	} // bBufSauve

/* CODE UTILE MAIS - NON UTILISE - NON TESTE - BUGS VRAISEMBLABLES
// dans buffer dest (allocation / r�alloc pDest au plus juste, *pdwDestLen mis � jour)
// insere dwLgSource octets du buffer pSource a l'offset dwOffsetInsere
// renvoie FALSE si erreur (= allocation, tailles ou offset incoh�rents)
BOOL bBufInsere
(
	PSTR * ppDest, PDWORD pdwLgDest,	// buffer destination
	PSTR pSource, DWORD dwLgSource,		// donn�es � ins�rer
	DWORD dwOffsetInsere							// zone dans pDest � remplacer
)
	{
	// rien � ajouter => termin�
	if (!dwLgSource)
		return TRUE;

	// offset max = au bout du buf initial
	if (dwOffsetInsere > *pdwLgDest)
		return FALSE;

	// d�ja des donn�es dans Dest ?
	if (*pdwLgDest)
		{
		DWORD dwLgRestant;

		// oui => dest r�allou� � la taille totale
		*ppDest = realloc (*ppDest, (*pdwLgDest) + dwLgSource);
		if (!*ppDest)
			return FALSE;

		// d�place la fin du buffer dest
		dwLgRestant = *pdwLgDest - dwOffsetInsere;
		if (dwLgRestant)
			MoveMemory ((*ppDest)+ dwOffsetInsere + dwLgSource, (*ppDest)+ dwOffsetInsere, dwLgRestant);
		}
	else
		{
		// non => on alloue � la taille de source
		if (*ppDest)
			return FALSE;
		*ppDest = malloc (dwLgSource);
		if (!*ppDest)
			return FALSE;
		}
	// on copie source dans dest (� l'offset demand�)
	CopyMemory ((*ppDest) + dwOffsetInsere, pSource, dwLgSource);

	// on comptabilise les octets rajout�s
	*pdwLgDest += dwLgSource;
	return TRUE;
	} // bBufInsere


// dans buffer dest (allocation / r�alloc pDest au plus juste, *pdwDestLen mis � jour)
// Supprime dwLgSuppr octets � l'offset dwOffsetSuppr
// renvoie FALSE si erreur (= allocation, tailles ou offset incoh�rents)
BOOL bBufSupprime
(
	PSTR * ppDest, PDWORD pdwLgDest, // buffer destination
	DWORD dwLgSuppr,								// donn�es � supprimer
	DWORD dwOffsetSuppr							// zone dans pDest � supprimer
)
	{
	DWORD dwLgRestant;

	// rien � supprimer => termin� Ok
	if (!dwLgSuppr)
		return TRUE;

	// v�rifie la coh�rence des tailles et allocation pBuf
	if ((!*ppDest) || (dwOffsetSuppr >= *pdwLgDest) || (dwOffsetSuppr + dwLgSuppr >= *pdwLgDest))
		return FALSE;
	
	// reste t'il quelque chose apr�s ?
	dwLgRestant = *pdwLgDest - (dwOffsetSuppr + dwLgSuppr);
	if (dwLgRestant)
		{
		// oui => on tasse le buffer
		MoveMemory ((*ppDest) + dwOffsetSuppr, (*ppDest) + dwLgSuppr + dwOffsetSuppr, dwLgRestant);
		}

	// on r�alloue ou on lib�re
	if (dwLgRestant + dwOffsetSuppr)
		{
		// dest r�allou� � la taille restante
		*ppDest = realloc (*ppDest, dwLgRestant + dwOffsetSuppr);
		if (!*ppDest)
			return FALSE;
		}
	else
		{
		// non => on lib�re le buffer
		free (*ppDest);
		*ppDest = NULL;
		}
	// on comptabilise les octets restants
	*pdwLgDest -= dwLgSuppr;
	return TRUE;
	} // bBufSupprime

// dans buffer dest (allocation / r�alloc pDest au plus juste, *pdwLgDest mis � jour)
// remplace dwLgARemplacer octets � l'offset dwOffsetRemplacement par
// les dwLgSource octets du buffer pSource
// renvoie FALSE si erreur (= allocation, tailles ou offsets incoh�rents)
BOOL bBufRemplace 
(
	PSTR * ppDest, PDWORD pdwLgDest, // buffer destination
	PSTR pSource, DWORD dwLgSource,	// donn�es rempla�ant
	DWORD dwOffsetRemplacement, DWORD dwLgARemplacer	// zone dans pDest � remplacer
)
	{
	long lLgDeplacement = (long)dwLgSource - (long)dwLgARemplacer;
	DWORD	dwLgReste;

	// rien � supprimer => termin� Ok
	if ((dwOffsetRemplacement > *pdwLgDest) || (dwOffsetRemplacement + dwLgARemplacer > *pdwLgDest))
		return FALSE;

	dwLgReste = dwLgSource - (DWORD)lLgDeplacement;

	// redimensionnement du buffer ?
	if (lLgDeplacement)
		{
		// oui : ajout ?
		if (lLgDeplacement > 0)
			{
			//	oui => ajout d'octets
			if (!bBufInsere (ppDest, pdwLgDest, pSource + dwLgReste, (DWORD)lLgDeplacement, dwLgReste))
				return FALSE;
			}
		else
			{
			//	non => suppression d'octets
			dwLgReste = dwLgSource - (DWORD)(-lLgDeplacement);

			if (!bBufSupprime (ppDest, pdwLgDest, (DWORD)-lLgDeplacement, dwOffsetRemplacement + dwLgReste))
				return FALSE;
			}
		} // if (lLgDeplacement)

	// on copie la partie restant � transf�rer
	CopyMemory ((*ppDest) + dwOffsetRemplacement, pSource, dwLgReste);

	return TRUE;
	}
*/
// -----------------fin BufMan.c ---------------------

// --------------------------------------------------------------------------
// Nom......: trouve_position_dichotomie
//
// Objet....: Recherche dichotomique de la position d'un �l�ment parmis des
//	      �l�ments tri�s.
//
//	      La position trouv�e indique la position d'insertion si
//	      l'�l�ment n'est pas trouv�, si non, elle indique la position
//	      r�el de l'�lement.
//
// Entrees..: pointeur sur l'�l�ment recherch�
//	      position de l'�l�ment de d�but de la recherche (�l�ment compris)
//	      nombre d'�l�ments sur lesquels il faut effectuer la recherche
//	      fonction de comparaison d'un �l�ment
//				   avec un �l�ment indiqu� par sa position
//	      position trouv�e
//
// Sorties..: position trouv�e
//
// Retour...: TRUE si l'�l�ment existe
//		   ==> position trouv�e == position de l'�l�ment recherch�
//	      FALSE si l'�l�ment n'existe pas
//		   ==> position trouv�e == position d'insertion de l'�l�ment
//		       (inserer l'�l�ment avant position trouv�e)
//
// Remarques: * La fonction de comparaison doit prendre en parametre:
//		   un pointeur sur un �l�ment;
//		   la position de l'�l�ment � comparer.
//		Elle doit retourner un int
//		= 0 si les �l�ments compar�s sont identique;
//		> 0 si le 1� �l�ment est sup�rieur au 2nd �l�ment
//		< 0 si le 1� �l�ment est inf�rieur au 2nd �l�ment
//
//		Remarque: Le 1�  �l�ment est celui d�fini par son adresse
//			  Le 2nd �l�ment est celui d�fini par sa position
//
// --------------------------------------------------------------------------
BOOL trouve_position_dichotomie (const void *enregistrement_recherche,
				    DWORD position_debut_recherche,
				    DWORD taille_recherche,
				    int comparaison (const void *enre_1, DWORD position),
				    DWORD *position_trouve);

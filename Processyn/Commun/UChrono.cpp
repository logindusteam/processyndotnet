/*--------------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                         |
 |                                                                          |
 |                    Societe LOGIQUE INDUSTRIE                             |
 |            Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                 |
 |        Il demeure sa propriete exclusive et est confidentiel.            |
 |                                                                          |
 |   Aucune diffusion n'est possible sans accord ecrit.                     |
 |--------------------------------------------------------------------------*/
//   UChrono.c        
// Gestion Chronos + Minuteries en millisecondes
// Win32 15/9/97 JS

#include "stdafx.h"
#include "Verif.h"
#include "UChrono.h"
VerifInit;

// --------------------------------------------------------------------------
// Un chrono est soit en marche, soit arr�t�.
// Impl�ment� en tant que __i64 ses formats sont :
// - en marche : valeur du compte de ticks initial
// - suspendu    : BIT_SUSPENDU | dur�e cumul�e au moment de la suspension (en millisecondes)
//
// Une Minuterie est soit en marche, soit suspendue, arriv�e � �ch�ance ou pas.
// Impl�ment�e en tant que __i64 ses formats sont :
// - en marche : valeur du tick count � l'�ch�ance de la Minuterie
// - suspendu  : BIT_SUSPENDU | d�passement de l'�ch�ance (en millisecondes) ( < 0 si avant)

#define BIT_SUSPENDU        0x4000000000000000	// Le bit de signe est laiss�
#define MASQUE_BIT_SUSPENDU 0xBFFFFFFFFFFFFFFF  // (~BIT_SUSPENDU)

// --------------------------------------------------------------------------
// valeur �chantillonn�e courante de l'horloge en UNITE_HORLOGE
static CHRONO i64NbMsEchantillon = 0;

/* Test Debug
#define NB_MS_DEBUG ((DWORD)0xffffffff)
static DWORD GetTickCountDebug (void)
	{
	static DWORD	dwDernierGetTickCount = 0;

	dwDernierGetTickCount+=NB_MS_DEBUG;

	return dwDernierGetTickCount;
	}
*/
// --------------------------------------------------------------------------
// Met � jour et renvoie un comte de millisecondes syst�me
// et renvoie sa valeur (supporte un acc�s multi thread)
__int64 i64NbMsMaintenant (void)
  {
	// valeur courante du compte de millisecondes syst�me en 64 bits
	static CHRONO i64NbMsSysteme = 0;
	static HANDLE hMutex = NULL;

	// R�sultat de la fonction
	__int64 i64Res;
	DWORD dwNbMsEcoules;

	// cr�ation � la vol�e d'un Mutex (s�curit� par d�faut, pas de propri�taire, pas de nom)
	if (!hMutex)
		{
		hMutex = CreateMutex (NULL, FALSE, NULL);
		VerifWarning (hMutex);
		}

	// acc�s exclusif � la ressource
	VerifWarning (WaitForSingleObject (hMutex, INFINITE) == WAIT_OBJECT_0);

	// Nombre de Ticks �coul�s = Compte de ms syst�me actuel -  Nb de ms enregistr�s
	dwNbMsEcoules = GetTickCount() - (DWORD)i64NbMsSysteme;
	
	// L'horloge syst�me a �t� mise � jour ?
	if (dwNbMsEcoules)
		// oui => ajout de la diff�rence au compte de millisecondes syst�mes
		i64NbMsSysteme += dwNbMsEcoules;

	// r�sultat = image du compteur
	i64Res = i64NbMsSysteme;

	// fin d'acc�s exclusif � la ressource
	ReleaseMutex (hMutex);

	return i64Res;
  } // i64NbMsMaintenant


// --------------------------------------------------------------------------
// Le chronom�tre est marqu� suspendu (avec un cumul � 0)
void ChronoInitSuspendu (PCHRONO pChrono)
	{
  *pChrono = BIT_SUSPENDU;
	}

// --------------------------------------------------------------------------
// La minuterie est marqu�e suspendue (avec un d�passement d'�ch�ance � 0)
// Attention! Cette minuterie suspendue est donc � �ch�ance 
void MinuterieInitSuspendue (PCHRONO pChrono)
	{
  *pChrono = BIT_SUSPENDU;
	}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							Chronom�trage temps r�el
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Le chronom�tre est marqu� en marche � compter de maintenant
void ChronoLance (PCHRONO pChrono)
  {
  *pChrono = i64NbMsMaintenant ();
  }

// --------------------------------------------------------------------------
// Renvoie la valeur courante (en millisecondes) du chronom�tre qu'il soit arr�t� ou en marche
__int64 i64ValChronoMs (const CHRONO * pChrono)
  {
	// Chrono arr�t� ? oui => renvoie la valeur cumul�e : non => renvoie Ticks �chantillon - Ticks initial
  return ((*pChrono) & BIT_SUSPENDU) ? (*pChrono) & MASQUE_BIT_SUSPENDU : i64NbMsMaintenant () - (*pChrono);
  }

// --------------------------------------------------------------------------
// Suspends l'ex�cution d'un chrono si ce n'est pas d�ja fait
void ChronoSuspends (PCHRONO pChrono)
  {
	// Chrono en marche ?
  if (!((*pChrono) & BIT_SUSPENDU))
		// oui => arr�te le (format BIT_SUSPENDU | dur�e �coul�e (en millisecondes))
    *pChrono = (i64NbMsMaintenant () - (*pChrono)) | BIT_SUSPENDU;
  }

// --------------------------------------------------------------------------
// Reprends l'ex�cution du chrono si elle �tait suspendue
void ChronoReprends (PCHRONO pChrono)
  {
	// Chrono suspendu ?
  if ((*pChrono) & BIT_SUSPENDU)
		// oui => relance le (format Tick initial
    *pChrono = i64NbMsMaintenant () - ((*pChrono) & MASQUE_BIT_SUSPENDU);
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							Minuterie temps r�el
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Une Minuterie est arm�e pour �tre vraie au bout de la dur�e sp�cifi�e - � partir de maintenant
void MinuterieLanceMs (PMINUTERIE pMinuterie, __int64 i64DureeMinuterieMs)
  {
	// Minuterie en marche = Compte de tick de l'�ch�ance de la dur�e
  *pMinuterie = i64NbMsMaintenant () + i64DureeMinuterieMs;
  }

// --------------------------------------------------------------------------
// Renvoie TRUE si l'�ch�ance de la Minuterie est atteinte ou d�pass�e
BOOL bEcheanceMinuterie (MINUTERIE Minuterie)
  {
	BOOL	bRes;

	if (Minuterie & BIT_SUSPENDU)
		bRes =  (Minuterie & MASQUE_BIT_SUSPENDU) >= 0;
	else
		bRes =  i64NbMsMaintenant () >= Minuterie;
  return bRes;
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Fonctions avec un instant choisi comme r�f�rence de temps
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Utiliser l'instant actuel comme r�f�rence de temps pour les fonctions �chantillon�es
void ChronoEchantilloneMsMaintenant (void)
  {
  i64NbMsEchantillon = i64NbMsMaintenant ();
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							Chronom�trage par rapport au dernier EchantillonMsMaintenant
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Le chronom�tre est marqu� en marche � compter de maintenant
void ChronoLanceEch (PCHRONO pChrono)
  {
  *pChrono = i64NbMsEchantillon;
  }

// --------------------------------------------------------------------------
// Renvoie la valeur courante (en millisecondes) du chronom�tre qu'il soit arr�t� ou en marche
__int64 i64ValChronoMsEch (const CHRONO * pChrono)
  {
	// Chrono arr�t� ? oui => renvoie la valeur cumul�e : non => renvoie Ticks �chantillon - Ticks initial
  return ((*pChrono) & BIT_SUSPENDU) ? (*pChrono) & MASQUE_BIT_SUSPENDU : i64NbMsEchantillon - (*pChrono);
  }

// --------------------------------------------------------------------------
// Suspends l'ex�cution d'un chrono si ce n'est pas d�ja fait
void ChronoSuspendsEch (PCHRONO pChrono)
  {
	// Chrono en marche ?
  if (!((*pChrono) & BIT_SUSPENDU))
		// oui => arr�te le (format BIT_SUSPENDU | dur�e �coul�e (en millisecondes))
    *pChrono = (i64NbMsEchantillon - (*pChrono)) | BIT_SUSPENDU;
  }

// --------------------------------------------------------------------------
// Reprends l'ex�cution du chrono si elle �tait suspendue
void ChronoReprendsEch (PCHRONO pChrono)
  {
	// Chrono suspendu ?
  if ((*pChrono) & BIT_SUSPENDU)
		// oui => relance le (format Tick initial
    *pChrono = i64NbMsEchantillon - ((*pChrono) & MASQUE_BIT_SUSPENDU);
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//							Minuterie par rapport au dernier EchantillonMsMaintenant
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Une Minuterie est arm�e pour �tre vraie au bout de la dur�e sp�cifi�e - � partir de maintenant
void MinuterieLanceMsEch (PMINUTERIE pMinuterie, __int64 i64DureeMinuterieMs)
  {
	// Minuterie en marche = Compte de tick de l'�ch�ance de la dur�e
  *pMinuterie = i64NbMsEchantillon + i64DureeMinuterieMs;
  }

// --------------------------------------------------------------------------
// Renvoie TRUE si l'�ch�ance de la Minuterie est atteinte ou d�pass�e
BOOL bEcheanceMinuterieEch (MINUTERIE Minuterie)
  {
	BOOL	bRes;

	if (Minuterie & BIT_SUSPENDU)
		bRes =  (Minuterie & MASQUE_BIT_SUSPENDU) >= 0;
	else
		bRes =  i64NbMsEchantillon >= Minuterie;
  return bRes;
  }

// --------------------------------------------------------------------------
// Renvoie TRUE si l'ex�cution de la Minuterie est suspendue
BOOL bMinuterieSuspendueEch (MINUTERIE Minuterie)
  {
  return (Minuterie & BIT_SUSPENDU) != 0;
  }

// --------------------------------------------------------------------------
// Suspends l'ex�cution de la Minuterie si ce n'est pas d�ja fait
void MinuterieSuspendsEch (PMINUTERIE pMinuterie)
  {
	// Minuterie en marche ?
  if (!((*pMinuterie) & BIT_SUSPENDU))
		// oui => arr�te le (format BIT_SUSPENDU | d�passement �ch�ance (en millisecondes))
    *pMinuterie = (i64NbMsEchantillon - (*pMinuterie)) | BIT_SUSPENDU;
  }

// --------------------------------------------------------------------------
// Reprends l'ex�cution de la Minuterie si elle est suspendue
void MinuterieReprendsEch (PMINUTERIE pMinuterie)
	{
	// Minuterie suspendue ?
  if (((*pMinuterie) & BIT_SUSPENDU))
		{
		// oui => relance la (format Compte Tick de l'�ch�ance (en millisecondes))
		if (*pMinuterie >= 0)
			*pMinuterie = i64NbMsEchantillon - (*pMinuterie & MASQUE_BIT_SUSPENDU);
		else
			*pMinuterie = i64NbMsEchantillon - (*pMinuterie);
		}
	}

// --------------------------------------------------------------------------
// D�place l'�ch�ance d'une Minuterie (ne change pas l'�tat suspendu)
void MinuterieAjouteMsEch (PMINUTERIE pMinuterie, __int64 i64DureeAjouteeMs)
	{
	// Minuterie suspendue ?
  if (((*pMinuterie) & BIT_SUSPENDU))
		{
		// oui => enl�ve la dur�e sp�cifi�e au nombre de ms de d�passement de l'�ch�ance
		// en laissant la minuterie suspendue
		if (*pMinuterie >= 0)
			{
			*pMinuterie &= MASQUE_BIT_SUSPENDU;
			*pMinuterie -= i64DureeAjouteeMs;
			*pMinuterie |= BIT_SUSPENDU;
			}
		else
			{
			*pMinuterie -= i64DureeAjouteeMs;
			*pMinuterie |= BIT_SUSPENDU;
			}
		}
	else
		// non => ajoute la dur�e sp�cifi�e � l'�ch�ance
		*pMinuterie += i64DureeAjouteeMs;
	}

// --------------------------------------------------------------------------
// Reprends l'ex�cution de la Minuterie si elle est suspendue et la relance si elle est � �ch�ance
// Renvoie TRUE si la minuterie �tait � �ch�ance
BOOL bMinuterieRelanceSiEcheanceEch (PMINUTERIE pMinuterie, __int64 i64DureeAjouteeMs)
  {
	// Enregistre si la minuterie est � �ch�ance
	BOOL bRes = bEcheanceMinuterieEch (*pMinuterie);

	// La minuterie reprends si elle est suspendue
	MinuterieReprendsEch (pMinuterie);

	// Minuterie � �ch�ance ?
	if (bRes)
		{
		// oui => ajoute la dur�e sp�cifi�e � l'�ch�ance
		MinuterieAjouteMsEch (pMinuterie, i64DureeAjouteeMs);
		}

	return bRes;
  }

/*
// Teste les fonctions
void ChronoTeste (void)
	{
	CHRONO Chrono;
	MINUTERIE Minuterie;
	BOOL	bSuspendue;
	BOOL	bEcheance;
	__int64 nTemp;
	__int64 nRes;

	for (nRes = (__int64)NB_MS_DEBUG + (__int64)NB_MS_DEBUG; nRes < 0x3ffffffff;nRes += NB_MS_DEBUG)
		{
		if (nRes != i64NbMsMaintenant())
			break;
		}

	ChronoInitSuspendu (&Chrono);
	nRes = i64ValChronoMs (&Chrono);
	VerifWarning (nRes ==0);

	ChronoLance (&Chrono);
	nRes = i64ValChronoMs (&Chrono);
	VerifWarning ((nRes ==0));

	Sleep (3000);
	nTemp = i64ValChronoMs (&Chrono);
	VerifWarning ((nTemp >= 3000) && (nTemp <= 3100));

	ChronoSuspends (&Chrono);
	nRes = i64ValChronoMs (&Chrono);
	VerifWarning (nTemp == nRes);

	ChronoReprends (&Chrono);
	nRes = i64ValChronoMs (&Chrono);
	VerifWarning (nTemp == nRes);

	ChronoInitSuspendu (&Chrono);
	nRes = i64ValChronoMs (&Chrono);
	VerifWarning ((nRes ==0));
	ChronoSuspends (&Chrono);
	nRes = i64ValChronoMs (&Chrono);
	VerifWarning ((nRes ==0));

	ChronoInitSuspendu (&Chrono);
	nRes = i64ValChronoMs (&Chrono);
	VerifWarning ((nRes ==0));
	ChronoReprends (&Chrono);
	nRes = i64ValChronoMs (&Chrono);
	VerifWarning ((nRes ==0));

	// Minuteries
	MinuterieInitSuspendue (&Minuterie);
	bSuspendue = bMinuterieSuspendue (Minuterie);
	bEcheance = bEcheanceMinuterie (Minuterie);
	VerifWarning (bSuspendue && bEcheance);

	MinuterieLanceMs (&Minuterie, 2 * NB_MS_PAR_SECONDE);
	bSuspendue = bMinuterieSuspendue (Minuterie);
	bEcheance = bEcheanceMinuterie (Minuterie);
	VerifWarning ((!bSuspendue) && (!bEcheance));
	Sleep (2000);

	bSuspendue = bMinuterieSuspendue (Minuterie);
	bEcheance = bEcheanceMinuterie (Minuterie);
	VerifWarning ((!bSuspendue) && (bEcheance));

	MinuterieSuspends (&Minuterie);
	bSuspendue = bMinuterieSuspendue (Minuterie);
	bEcheance = bEcheanceMinuterie (Minuterie);
	VerifWarning ((bSuspendue) && (bEcheance));

	bMinuterieReprends (&Minuterie, 0);
	bSuspendue = bMinuterieSuspendue (Minuterie);
	bEcheance = bEcheanceMinuterie (Minuterie);
	VerifWarning ((!bSuspendue) && (bEcheance));

	bMinuterieReprends (&Minuterie, 2 * NB_MS_PAR_SECONDE);
	bSuspendue = bMinuterieSuspendue (Minuterie);
	bEcheance = bEcheanceMinuterie (Minuterie);
	VerifWarning ((!bSuspendue) && (!bEcheance));

	Sleep (2000);
	bSuspendue = bMinuterieSuspendue (Minuterie);
	bEcheance = bEcheanceMinuterie (Minuterie);
	VerifWarning ((!bSuspendue) && (bEcheance));
	VerifWarning (FALSE);
	}
*/

//-----------------------------------------------------
//Initialise l'origine du compte de tick pour les fonctions de datation (non utilis�)
void InitChronosEtMinuteries (void)
	{
	ChronoEchantilloneMsMaintenant();
	//ChronoTeste ();
	}


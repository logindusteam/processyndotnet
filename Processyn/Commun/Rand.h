/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Rand.h	                                                  |
 |   Auteur  : AC																							        	|
 |   Date    : 16/6/93 					        																|
 |   Version : 4.00																											|
 +----------------------------------------------------------------------*/
//----------------------------------------------------------------------------
// Nombres al�atoires optimis�s par OS
// ---------------------------------------------------------------------------

#define MIN_ID 1
#define MAX_ID RAND_MAX
#define getrandom(min, max) ((rand() % (int)(((max)+1) - (min))) + (min))


//----------------------------------------------------------------------------
// Renvoie la valeur d'une fonction pseudo-al�atoire fonction de 2 variables
__inline static DWORD get_rand_dos (DWORD dwSerie, DWORD dwNum)
  {
  DWORD dwRes = 0;
	DWORD i;

	srand (dwSerie);
  for (i = 0; (i < dwNum); i++)
    {
    dwRes = getrandom (MIN_ID, MAX_ID);
    }
  return (WORD)dwRes;
  }

//----------------------------------------------------------------------------
// Renvoie la valeur d'une fonction pseudo-al�atoire fonction de 2 variables
__inline static DWORD get_rand_os2 (DWORD dwSerie, DWORD dwNum)
  {
  DWORD dwRes = 0;
	DWORD i;

	srand (dwSerie);
  for (i = 0; (i < dwSerie + dwNum); i++)
    {
    dwRes = getrandom (MIN_ID, MAX_ID);
    }

  return (WORD)dwRes;
  }

//----------------------------------------------------------------------------
// Renvoie la valeur d'une fonction pseudo-al�atoire fonction de 2 variables
__inline static DWORD get_rand_os2pm (DWORD dwSerie, DWORD dwNum)
  {
  DWORD dwRes = 0;
	DWORD i;

  srand (dwSerie);
  for (i = 0; (i < dwSerie + dwNum + 3); i++)
    {
    dwRes = getrandom (MIN_ID, MAX_ID);
    }

  return (WORD)dwRes;
  }

//----------------------------------------------------------------------------
// Renvoie la valeur d'une fonction pseudo-al�atoire fonction de 4 variables
__inline static DWORD get_rand_OS_WIN32 (DWORD dwNb1, DWORD dwNb2, DWORD dwNb3, DWORD dwNb4, DWORD dwNb5,
																				 DWORD dwNb6, DWORD dwNb7, DWORD dwNb8, DWORD dwNb9, DWORD dwNb10,
																				 DWORD dwNbParam)
  {
  DWORD dwRes =  0;
	
	if (dwNbParam >= 1)
		{
		dwRes = dwNb1  * (DWORD)3880178699;
		if (dwNbParam >= 2)
			{
			dwRes ^= dwNb2 * (DWORD)  1772906939;
			if (dwNbParam >= 3)
				{
				dwRes ^= dwNb3 * (DWORD) 1974410527;
				dwRes ^= dwNb1 << 23;
				if (dwNbParam >= 4)
					{
					dwRes ^= dwNb4 * (DWORD) 273296411;
					dwRes ^= dwNb1 >> 15;
					if (dwNbParam >= 5)
						{
						dwRes ^= dwNb5 * (DWORD)  268555801;
						if (dwNbParam >= 6)
							{
							dwRes ^= dwNb6 * (DWORD)   1418111953;
							if (dwNbParam >= 7)
								{
								dwRes ^= dwNb7 * (DWORD)    2511021581;
								if (dwNbParam >= 8)
									{
									dwRes ^= dwNb8 * (DWORD)   4045657261;
									if (dwNbParam >= 9)
										{
										dwRes ^=  dwNb9 * (DWORD)   2023013381;
										if (dwNbParam >= 10)
											{
											dwRes ^= dwNb10 * (DWORD)   2966460019;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
  return dwRes;
  }


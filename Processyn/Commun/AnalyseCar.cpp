// AnalyseCar.cpp: implementation of the CAnalyseCar class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Lng_Res.h"
#include "Tipe.h"
#include "Cvt_UL.h"
#include "AnalyseCar.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAnalyseCar::CAnalyseCar()
	{
	// initialisation de la table de statut de caract�res
	for (int nValC = 0; nValC < TAILLE_STATUT_CAR; nValC++)
		{
		int nValCMajuscule = nValC;
		if ((nValC >= 'a') && (nValC <= 'z'))
			nValCMajuscule -=  'a' - 'A';

		int bits = nValCMajuscule;

		if (nValC == 0)
			bits |= BIT_CAR_FIN_CHAINE;

		if ((nValC == ' ') || (nValC == '\t'))
			bits |= BIT_CAR_SEPARATEUR;

		if (nValC == CAR_COMMENTAIRE)
			bits |= BIT_CAR_COMMENTAIRE;

		if ((nValC == '+') || (nValC == '-'))
			bits |= BIT_CAR_SIGNE;

		if (nValC == CAR_DEBUT_MESSAGE)
			bits |= BIT_CAR_DEBUT_MESSAGE;

		if ((nValC >= '0') && (nValC <= '9'))
			bits |= BIT_CAR_NUMERIQUE;

		if ((nValC >= '0') && (nValC <= '9') ||
			(nValCMajuscule >= 'A') && (nValCMajuscule <= 'F') )
			bits |= BIT_CAR_HEXADECIMAL;

		if (
			(nValC == '=') || (nValC == '*') ||
			(nValC == '/') || (nValC == '(') ||
			(nValC == ')') || (nValC == '<') ||
			(nValC == '>') || (nValC == ':') ||
			(nValC == '&') || (nValC == '[') ||
			(nValC == ']')
			)
			bits |= BIT_CAR_OPERATEUR;
		if (
			((nValCMajuscule >= 'A') && (nValCMajuscule <= 'Z')) ||
			(nValC == '_') || 
			(nValC == '#') || 
			(nValC == '.') ||
			(nValC == '"')
			)
			bits |= BIT_CAR_PREMIER_CAR_ID;

		// affectation du statut cr�� � sa place
		m_naStatutCars [nValC] = bits;
		}
	}

CAnalyseCar::~CAnalyseCar()
	{

	}

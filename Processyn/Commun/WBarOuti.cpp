// ----------------------------------------------------------------------------------
//									WBarOuti.c
//	Gestion de barre d'outil WIN32
//
// Une barre d'outils est une fenetre support de boutons avec eventuellement un titre, 
// un	bouton pour minimiser, un cadre standard ou un cadre de deplacement plus epais. 
// Elle sert comme support de tous les boutons dans sa zone cliente.
//
// Les boutons sont dispos�s de fa�on reguli�re sur la barre d'outil.
// Chaque bouton poss�de un identificateur unique.
//
// La validation d'un bouton se fait en relachant le bouton de la souris.
// Le bouton r�agit selon son mode (bouton relache ou bouton bAutoCheck).
// La notification est envoy� au parent de la barre d'outil.
// -----------------------------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "UStr.h"
#include "MemMan.h"
#include "UEnv.h"
#include "Verif.h"

#include "WBarOuti.h"

VerifInit;

#define BMP_EPAIS 2                  // distance entre le cadre d'un bouton et le bitmap 

static char szClasseBarreOutil [] = "ClasseBarreOutil";	// nom de la classe fenetre Barre d'outil

// Objet bouton d'une barre d'outil
class CBoutonBarreOutils  
	{
	public:
		// Construction bouton couleur (palette)
		CBoutonBarreOutils(HBO hBarreOutil, COLORREF ColorRef,int o_x, int o_y, int len_x, int len_y, int *plen_x, int *plen_y, int mnu_mode, int nIdBoutonX);
		// Construction bouton bitmap
		CBoutonBarreOutils(HBO hBarreOutil, const char * pszBitmap,int o_x, int o_y, int *plen_x, int *plen_y, int mnu_mode, int nIdBoutonX);
		virtual ~CBoutonBarreOutils();
	public:
		BOOL bCreeFenetre(HWND hwndBarre, HINSTANCE hinst);
		int       nIdBouton;    // code a retourner lors d'une validation 
		BOOL      bAutoCheck;		// la case menu reste enfoncee si activee 
		HWND      hwndBouton;   // handle de la fenetre du bouton
		int       m_len_x;				// dimensions du bouton $$ les m�mes pour toutes les BO
		int       m_len_y;
		COLORREF	m_ColorRef;
	private:
		BOOL			m_bBoutonCouleur;
		HBO       hboParent;		// Handle de la barre d'outil auquel il appartient
		HBITMAP		hBitmap;			// Handle repr�sent� dans le bouton
		int       xOrigine;     // origine du bouton par rapport � la barre d'outil
		int       yOrigine;
		int       id_pointer;		// id du pointeur souris a utiliser 
	};

typedef  CBoutonBarreOutils *pCBoutonBarreOutils;

// Constructeur bouton Bitmap
CBoutonBarreOutils::CBoutonBarreOutils(HBO hBarreOutil, const char * pszBitmap,int o_x, int o_y, 
	int *plen_x, int *plen_y, int mnu_mode, int nIdBoutonX)
	{
	bAutoCheck = (mnu_mode == BO_COCHE);
	m_bBoutonCouleur = FALSE;
	m_ColorRef = RGB(0,0,0);
  hboParent = hBarreOutil;
  id_pointer = 0;
  xOrigine = o_x;
  yOrigine = o_y;
  nIdBouton = nIdBoutonX;

	m_len_x = (2 * BMP_EPAIS);
	m_len_y = (2 * BMP_EPAIS);

	// chargement et analyse du bitmap associ�
	hBitmap = LoadBitmap (Appli.hinstDllLng, pszBitmap);
	if (hBitmap)
		{
		BITMAP bmp;

		if (GetObject (hBitmap, sizeof(bmp), &bmp))
			{
			m_len_x += bmp.bmWidth;
			m_len_y += bmp.bmHeight;
			}
		}
  *plen_x = m_len_x;
  *plen_y = m_len_y;
	}

// Constructeur bouton Couleur
CBoutonBarreOutils::CBoutonBarreOutils(HBO hBarreOutil, COLORREF ColorRef,int o_x, int o_y, int len_x, int len_y,
	int *plen_x, int *plen_y, int mnu_mode, int nIdBoutonX)
	{
	bAutoCheck = (mnu_mode == BO_COCHE);
	m_bBoutonCouleur = TRUE;
	m_ColorRef = ColorRef;
  hboParent = hBarreOutil;
  id_pointer = 0;
  xOrigine = o_x;
  yOrigine = o_y;
  nIdBouton = nIdBoutonX;

	m_len_x = (2 * BMP_EPAIS);
	m_len_y = (2 * BMP_EPAIS);

	// chargement et analyse du bitmap associ�
	m_len_x += len_x;
	m_len_y += len_y;
  *plen_x = m_len_x;
  *plen_y = m_len_y;
	}

CBoutonBarreOutils::~CBoutonBarreOutils()
	{
	if (hwndBouton)
		{
		::DestroyWindow (hwndBouton);
		hwndBouton = NULL;
		}
	if (hBitmap)
		{
		DeleteObject (hBitmap);
		hBitmap = NULL;
		}
	}

BOOL CBoutonBarreOutils::bCreeFenetre(HWND hwndBarre, HINSTANCE hinst)
	{
	BOOL bRes = FALSE;
	DWORD uStyle = WS_VISIBLE | WS_CHILD; //  | BS_NOTIFY;

	if (m_bBoutonCouleur)
		uStyle |= BS_OWNERDRAW;
	else
		{
		if (bAutoCheck)
			uStyle |= BS_AUTOCHECKBOX|BS_PUSHLIKE| BS_BITMAP;
		else
			uStyle |= BS_BITMAP;
		}
	hwndBouton = 
		CreateWindow("BUTTON", NULL, uStyle,
			xOrigine, yOrigine, 
			m_len_x, m_len_y,
			hwndBarre, (HMENU)nIdBouton, hinst, NULL);

	// cr�ation Ok ?
	if (hwndBouton)
		{
		// associe le bitmap au bouton.
		::SendMessage(hwndBouton, BM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM)(HANDLE)(hBitmap));
		bRes = TRUE;
		}
	return bRes;
	}

// -----------------------------------------------------------
// Objet barre d'outil
typedef struct
	{
	BOOL cadre;						// vrai si cadre de deplacement existe
	BOOL deplacement;			// vrai pour deplacer
	BOOL fermeture;				// vrai si bouton fermeture
	BOOL bPopUp;					// vrai si fen�tre PopUp, faux si Child
	char szTitre[BO_MAX_TITRE];  // titre
	HWND hwndBarre;				// handle de la fenetre support
	int cxMarge;					// taille en x du cadre et de la marge sp�cifi�e
	int cyMarge;					//  ........ y ............................ 
	int xBarre;						// origine du prochain bouton
	int yBarre;						// et en execution origine de la Barre d'outil
	int cxBarre;					// dimensions de la barre d'outil
	int cyBarre;
	int cxBitmap;						// longueur de la derniere case bitmap et offset d'un $$ ?
	int cyBitmap;						// 
	int nNbBoutons;				// nombre courant de boutons
	pCBoutonBarreOutils * apBoutons;	// table de pointeur sur les boutons
	} BO, * PBO;

// conversions typ�es Handle BO / structure interne BO
static __inline HBO HFromPBO (PBO  pbo)	{return (HBO)pbo;}
static __inline PBO PFromHBO (HBO  hbo)	{return (PBO)hbo;}

static HBO hDerniereBOValide = NULL;	// derni�re barre d'outil valide

//------------------------------------------------------------------------
// Calcule l'origine d'une barre d'outil par rapport a une
// fenetre donne par ses coordonnes rect et une position BO_MNU_POS_*.
// rect specifie les coord de la fenetre cliente.
static BOOL bGetOrigineBO (PBO pBarreOutil, int pos, RECT * prect, int * px, int * py)
  {
  BOOL bOk = TRUE;

  // positionnement d'une barre d'outil par rapport a la fenetre cliente
  switch (pos)
    {
    case (BO_POS_GAUCHE | BO_POS_HAUTE) :
      *px = prect->left;
      *py = prect->top;
      break;
    case (BO_POS_GAUCHE | BO_POS_BASSE) :
      *px = prect->left;
      *py = prect->bottom - pBarreOutil->cyBarre;
      break;
    case (BO_POS_GAUCHE | BO_POS_CENTRE) :
      *px = prect->left;
      *py = ((prect->bottom - prect->top) - pBarreOutil->cyBarre) / 2;
      break;
    case (BO_POS_DROITE | BO_POS_HAUTE) :
      *px = prect->right - pBarreOutil->cxBarre;
      *py = prect->top;
      break;
    case (BO_POS_DROITE | BO_POS_BASSE) :
      *px = prect->right - pBarreOutil->cxBarre;
      *py = prect->bottom - pBarreOutil->cyBarre;
      break;
    case (BO_POS_DROITE | BO_POS_CENTRE) :
      *px = prect->right - pBarreOutil->cxBarre;
      *py = ((prect->bottom - prect->top) - pBarreOutil->cyBarre) / 2;
      break;
    case (BO_POS_HAUTE | BO_POS_CENTRE) :
      *px = ((prect->right - prect->left) - pBarreOutil->cxBarre) / 2;
      *py = prect->top;
      break;
    case (BO_POS_BASSE | BO_POS_CENTRE) :
      *px = ((prect->right - prect->left) - pBarreOutil->cxBarre) / 2;
      *py = prect->bottom - pBarreOutil->cyBarre;
      break;
    case (BO_POS_CENTRE) :
      *px = ((prect->right - prect->left) - pBarreOutil->cxBarre) / 2;
      *py = ((prect->bottom - prect->top) - pBarreOutil->cyBarre) / 2;
      break;
    default :
      bOk = FALSE;
      break;
    }
  return bOk;
  }

//-------------------------------------------------------------------------
// Renvoie l'adresse du bouton ayant l'identificateur donn� dans une table 
// de pointeur bouton. nNbBoutons designe le nombre bitmap dans la table
static pCBoutonBarreOutils pBoutonBoTrouve (PBO pbo, int nIdBouton)
  {
  pCBoutonBarreOutils pBoutonBarreOutil;
  BOOL bTrouve = FALSE;
  int i;

  for (i = 0; i < pbo->nNbBoutons; i++)
    {
    pBoutonBarreOutil = pbo->apBoutons[i];
		if (pBoutonBarreOutil->nIdBouton == nIdBouton)
			{
			bTrouve = TRUE;
			break;
			}
    }

  if (!bTrouve)
    pBoutonBarreOutil = NULL;

	return pBoutonBarreOutil;
  }

//---------------------------------------------------
// Gere la mise a jour des coordonnees d'un menu apres l'avoir deplace.                  
static void OnMoveBarreOutil (PBO pBarreOutil)
  {
	// pour detecter le deplacement du menu et non de la fenetre parente
  // ou lors de l'initialisation, on teste l'existence de SWP_SHOWWINDOW
  WINDOWPLACEMENT swp;
  ::GetWindowPlacement(pBarreOutil->hwndBarre,&swp);

  if (swp.showCmd & SWP_SHOWWINDOW)
    {
    if ((pBarreOutil->xBarre != swp.rcNormalPosition.left) || (pBarreOutil->yBarre != swp.rcNormalPosition.top))
      {
      pBarreOutil->xBarre = swp.rcNormalPosition.left;
      pBarreOutil->yBarre = swp.rcNormalPosition.top;
      pBarreOutil->cxBarre  = swp.rcNormalPosition.right - swp.rcNormalPosition.left;
      pBarreOutil->cyBarre  = swp.rcNormalPosition.top - swp.rcNormalPosition.bottom;
      }
    }
  } // OnMoveBarreOutil

//---------------------------------------------------------------------------
// r�cup�re les infos d'un bouton � partir de son Id et des infos de la 
// barre d'outil parente
// renvoie NULL si non trouv�
static pCBoutonBarreOutils pBoutonBarreOutilFromId (PBO pBarreOutil, int id)
  {
	pCBoutonBarreOutils pBoutonBarreOutil = NULL;
	int i;

	for (i = 0; i < pBarreOutil->nNbBoutons; i++)
		{
		if (pBarreOutil->apBoutons[i]->nIdBouton == id)
			{
			pBoutonBarreOutil = pBarreOutil->apBoutons[i];
			break;
			}
		}

	return pBoutonBarreOutil;
	} // pBoutonBarreOutilFromId

//-------------------------------------------------------------------------
// traitement des notifications des boutons de la barre d'outil
static LRESULT lOnCommand (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
	{
	switch (HIWORD(mp1))
		{
		// Traitement selon notification
		case BN_CLICKED:
			{
			// recherche des infos du bouton ayant envoy� la notification
			PBO	pBarreOutil = (PBO)pGetEnv(hwnd);

			// trouve les infos du bouton cause de la notification ?
			if (pBarreOutil)
				{
				pCBoutonBarreOutils pBoutonBarreOutil = pBoutonBarreOutilFromId (pBarreOutil, LOWORD(mp1));
				if (pBoutonBarreOutil)
					{
					// oui => bouton courant de type AutoCheck ?
					if (pBoutonBarreOutil->bAutoCheck)
						{
						int nIndexBouton;

						// oui => UnCheck d'un autre boutons Check de la barre d'outil
						for (nIndexBouton = 0; nIndexBouton < pBarreOutil->nNbBoutons; nIndexBouton++)
							{
							pCBoutonBarreOutils pBoutonTemp = pBarreOutil->apBoutons[nIndexBouton];

							if ((pBoutonTemp->nIdBouton != pBoutonBarreOutil->nIdBouton) && pBoutonTemp->bAutoCheck)
								{
								if (::SendMessage (pBoutonTemp->hwndBouton, BM_GETCHECK, 0 ,0) == BST_CHECKED)
									{
									::SendMessage (pBoutonTemp->hwndBouton, BM_SETCHECK, BST_UNCHECKED ,0);
									break;
									}
								}
							}
						}

					// envoi de la notification
					::PostMessage (::GetParent (pBarreOutil->hwndBarre), WM_COMMAND, (WPARAM) pBoutonBarreOutil->nIdBouton, 0);

					// On rend le focus � la fen�tre de l'application
					SetFocus (Appli.hwnd); //$$ � am�liorer
					}
				}
			} //case BN_CLICKED:
			break;

//		case BN_SETFOCUS:	break;
		} // switch (HIWORD(mp1))
	return 0;
	} // lOnCommand

//-------------------------------------------------------------------------
// traitement du draw Item d'un bouton de la barre d'outil
static LRESULT lOnDrawItem (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
	{
	// message concernant un bouton ?
	LPDRAWITEMSTRUCT pDrawItem = (LPDRAWITEMSTRUCT)mp2;
	if (pDrawItem && pDrawItem->CtlType == ODT_BUTTON)
		{
		// Trouve les infos du bouton cause du message ?
		PBO	pBarreOutil = (PBO)pGetEnv(hwnd);

		if (pBarreOutil)
			{
			pCBoutonBarreOutils pBoutonBarreOutil = pBoutonBarreOutilFromId (pBarreOutil, pDrawItem->CtlID);
			if (pBoutonBarreOutil)
				{
				// oui => dessine le bouton selon son �tat
				// Dessine la bordure
				RECT rectInterieur = pDrawItem->rcItem;
				if (pDrawItem->itemState & ODS_SELECTED)
					DrawEdge (pDrawItem->hDC, &pDrawItem->rcItem, EDGE_RAISED, BF_RECT|BF_FLAT|BF_ADJUST);
				else
					DrawEdge (pDrawItem->hDC, &pDrawItem->rcItem, EDGE_RAISED, BF_RECT|BF_ADJUST);

				// Dessine la couleur
				InflateRect(&rectInterieur, -2, -2);
				HBRUSH hbrCouleur = CreateSolidBrush(pBoutonBarreOutil->m_ColorRef);
				FillRect(pDrawItem->hDC, &rectInterieur, hbrCouleur);
				DeleteObject(hbrCouleur);

				// Dessine le Focus
				if (pDrawItem->itemState & ODS_FOCUS)
					{
					DrawFocusRect(pDrawItem->hDC, &rectInterieur);
					}

				return TRUE;
				}
			}
		}
	return 0;
	} // lOnDrawItem

//-------------------------------------------------------------------------
// traitement du measure item d'un bouton de la barre d'outil
static LRESULT lOnMeasureItem (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
	{
	// message concernant un bouton ?
	LPMEASUREITEMSTRUCT pMeasureItem = (LPMEASUREITEMSTRUCT)mp2;
	if (pMeasureItem && pMeasureItem->CtlType == ODT_BUTTON)
		{
		// Trouve les infos du bouton cause du message ?
		PBO	pBarreOutil = (PBO)pGetEnv(hwnd);

		if (pBarreOutil)
			{
			pCBoutonBarreOutils pBoutonBarreOutil = pBoutonBarreOutilFromId (pBarreOutil, pMeasureItem->CtlID);
			if (pBoutonBarreOutil)
				{
				// oui => bouton courant de type AutoCheck ?
				pMeasureItem->itemHeight = pBoutonBarreOutil->m_len_y;
				pMeasureItem->itemWidth = pBoutonBarreOutil->m_len_x;
				return TRUE;
				}
			}
		}
	return 0;
	} // lOnMeasureItem

// -----------------------------------------------------------------
// Fermeture et lib�ration de l'objet et de la fen�tre bouton
static void FermeBouton (pCBoutonBarreOutils pBoutonBarreOutil)
  {
	Verif (pBoutonBarreOutil);
	delete pBoutonBarreOutil;
  }

//-------------------------------------------------------------------------
static LRESULT CALLBACK wndprocBarreOutil (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;        // Valeur de retour
  BOOL     bTraite = TRUE;  // Indique commande trait�e

  switch (msg)
    {
		case WM_CREATE:
			{
			PBO pBarreOutil = (PBO)pSetEnvOnCreateParam (hwnd, mp2);
			DWORD	i;

			// Note que la barre d'outil est ouverte
			pBarreOutil->hwndBarre = hwnd;

			// creation des fenetres boutons de la barre d'outil
      for (i = 0; i < (DWORD)(pBarreOutil->nNbBoutons); i++ )
        {
				pCBoutonBarreOutils	pBoutonBarreOutil = pBarreOutil->apBoutons[i];

				// cr�e la fen�tre bouton
				if (!pBoutonBarreOutil->bCreeFenetre(pBarreOutil->hwndBarre, Appli.hinst))
					VerifWarningExit;
        } // for
			}
			break;

		case WM_DESTROY:
			{
			PBO pBarreOutil = (PBO)pGetEnv(hwnd);
			if (pBarreOutil)
				{
				// Note que les boutons sont d�ja ferm�s
				int	i;

				for (i = 0; i < pBarreOutil->nNbBoutons; i++)
					{
					// Handle ferm�s
					pBarreOutil->apBoutons[i]->hwndBouton = NULL;

					// Lib�re les objets
					FermeBouton (pBarreOutil->apBoutons[i]);
					}
				pBarreOutil->nNbBoutons = 0;

				// Note que la barre d'outil est ferm�e
				pBarreOutil->hwndBarre = NULL;
				}
			}
			break;

		case WM_COMMAND:
			mres = lOnCommand (hwnd, msg, mp1, mp2);
			break;

		case WM_DRAWITEM:
			mres = lOnDrawItem (hwnd, msg, mp1, mp2);
			break;

		case WM_MEASUREITEM:
			mres = lOnMeasureItem (hwnd, msg, mp1, mp2);
			break;

    case WM_MOVE:
			{
			//PBO pBarreOutil = (PBO)pGetEnv(hwnd);
			//if (pBarreOutil)
				 //OnMoveBarreOutil(pBarreOutil);
      bTraite = FALSE;
			}
      break;

/* $$
    case WM_LBUTTONDOWN: // selection
    case WM_RBUTTONDOWN:
      pBarreOutil = pGetEnv(hwnd);
      if (pBarreOutil->deplacement)
        {
        SetCursor (LoadCursor (NULL, IDC_SIZE));
        bmp_mnu_move(pBarreOutil, LOWORD(mp2), HIWORD(mp2));
        }
      break;
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
      mres = TRUE;
      break;
*/
    default:
      bTraite = FALSE;
      break;
    }

  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return mres;
  } // wndprocBarreOutil


// --------------------------------------------------------------------------
// Cr�e les fenetres d'un menu et ses boutons � l'origine o_x et o_y.
static BOOL bCreeFenetreBarreOutil (PBO pBarreOutil, HWND hwndParent,int o_x,int o_y, BOOL bVisible)
  {
  BOOL bOk = TRUE;
	static BOOL bClasseEnregistree = FALSE;

	// enregistrement �ventuel de la classe barre d'outil
  if (!bClasseEnregistree)
    {
		WNDCLASS wc;

		wc.style					= 0;
    wc.lpfnWndProc		= wndprocBarreOutil; 
    wc.cbClsExtra			= 0; 
    wc.cbWndExtra			= 0; 
    wc.hInstance			= Appli.hinst; 
    wc.hIcon					= NULL;
    wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
    wc.hbrBackground	= (HBRUSH)(COLOR_MENU+1);
    wc.lpszMenuName		= NULL; 
    wc.lpszClassName	= szClasseBarreOutil; 

    bClasseEnregistree = RegisterClass (&wc);
    }

	// Enregistrement de la classe Barre d'outil Ok ?
  if (bClasseEnregistree)
    {
		int		cx = pBarreOutil->cxBarre;
		int		cy = pBarreOutil->cyBarre;
	  ULONG  uStyle = WS_CLIPSIBLINGS;
	  ULONG  uExStyle = 0;

    pBarreOutil->xBarre = o_x;
    pBarreOutil->yBarre = o_y;

    if (pBarreOutil->bPopUp)
			{
	    uStyle |= WS_POPUP;
			uExStyle |= WS_EX_TOOLWINDOW|WS_EX_TOPMOST;
			}
		else
			{
	    uStyle |= WS_CHILD;
			}

		if (bVisible)
			uStyle |= WS_VISIBLE;

    if (pBarreOutil->cadre)
			{
			uStyle |= WS_BORDER;
			}

    if (pBarreOutil->fermeture)
      uStyle |= WS_SYSMENU;
    if (pBarreOutil->szTitre[0])
      uStyle |= WS_CAPTION;

		// R�cup�re les ajustements zone cliente/fentre Windows dus aux diff�rents cadres, titres, menus (hors menu wrap et scroll bar) ?
		RECT AjusteWindowRect={0,0,0,0};
		if(AdjustWindowRectEx(&AjusteWindowRect,uStyle,false,uExStyle))
			{
			// Oui=>Corrige la taille de la fen�tre demand�e pour prendre en compte ces ajustements en gardant la m�me origine
			cx+=AjusteWindowRect.right-AjusteWindowRect.left;
			cy+=AjusteWindowRect.bottom-AjusteWindowRect.top;
			}

		// cr�ation de la fen�tre Barre d'outil devant contenir les boutons
    if (CreateWindowEx (uExStyle,
			szClasseBarreOutil, pBarreOutil->szTitre, uStyle,
			o_x, o_y, cx, cy,
			hwndParent, (HMENU)0, Appli.hinst, pBarreOutil))
      {
      // maj de la barre d'outil, si c'est une barre d'outil principale
      hDerniereBOValide = HFromPBO (pBarreOutil);
			bOk = TRUE;
      }
    }
  return bOk;
  }

//-------------------------------------------------------------------------
HBO hBOCree (const char *pszTitre, BOOL cadre,
						BOOL deplacement, BOOL fermeture, int cx_bord, int cy_bord)
  {
  PBO pBarreOutil = NULL;

  if ((pBarreOutil = (PBO)pMemAlloue(sizeof(BO))) != NULL)
    {
    pBarreOutil->cxMarge = cx_bord;
    pBarreOutil->cyMarge = cy_bord;

    pBarreOutil->hwndBarre = NULL;
    pBarreOutil->cadre = cadre;
    pBarreOutil->bPopUp = FALSE;
    pBarreOutil->deplacement = deplacement;
    pBarreOutil->fermeture = fermeture;
    if (pszTitre)
      {
			VerifWarning (StrLength (pszTitre) < BO_MAX_TITRE);
      StrCopy (pBarreOutil->szTitre, pszTitre);
      }
		else
			StrSetNull (pBarreOutil->szTitre);
    pBarreOutil->cxBarre = 2 * pBarreOutil->cxMarge;
    pBarreOutil->cyBarre = 2 * pBarreOutil->cyMarge;

		pBarreOutil->xBarre = 0;
    pBarreOutil->yBarre = 0;
    pBarreOutil->cxBitmap = 0;
    pBarreOutil->cyBitmap = 0;
    pBarreOutil->nNbBoutons = 0;
		// par d�faut pas de boutons
    pBarreOutil->apBoutons = NULL;
    }
  return HFromPBO (pBarreOutil);
  }

//-------------------------------------------------------------------------
BOOL bBOAjouteBouton (HBO hBarreOutil, const char *pszBitmap, int pos, int mnu_mode,int nIdBouton)
  {
  BOOL  bRet = FALSE;

  PBO		pBarreOutil = PFromHBO (hBarreOutil);
	Verif (pBarreOutil);

	// alloue un pointeur de bouton de plus dans le tableau de pointeurs boutons
	MemRealloue ((PVOID *)(&pBarreOutil->apBoutons), (pBarreOutil->nNbBoutons + 1) * sizeof(pCBoutonBarreOutils));

	// calcul de sa position :
  int	origin_x = pBarreOutil->xBarre;
  int	origin_y = pBarreOutil->yBarre;
	
	// premier bouton ajout� ?
  if (pBarreOutil->nNbBoutons == 0)
    {
    // oui=> pos n'a pas d'importance
    pBarreOutil->xBarre = pBarreOutil->cxMarge;
    pBarreOutil->yBarre = pBarreOutil->cyMarge;
    }
  else
    {
		// non => Nouvelle ligne ?
    if (pos == BO_NOUVELLE_LN)
      {
			// oui => origine en dessous, ligne suivante
      pBarreOutil->xBarre = pBarreOutil->cxMarge;
      pBarreOutil->yBarre += pBarreOutil->cyBitmap;
      }
    else
      {
			// non => ajout � droite du dernier bouton ajout�
      pBarreOutil->xBarre += pBarreOutil->cxBitmap;
      }
    }

	// cr�ation de l'objet bouton Ok ?
	pBarreOutil->apBoutons[pBarreOutil->nNbBoutons] = new CBoutonBarreOutils(HFromPBO (pBarreOutil),pszBitmap, pBarreOutil->xBarre,pBarreOutil->yBarre,
       &pBarreOutil->cxBitmap,&pBarreOutil->cyBitmap, mnu_mode, nIdBouton);
     
	//pboutonCree (HFromPBO (pBarreOutil),pszBitmap, pBarreOutil->xBarre,pBarreOutil->yBarre,
  //     &pBarreOutil->cxBitmap,&pBarreOutil->cyBitmap, mnu_mode, nIdBouton);

  if (pBarreOutil->apBoutons[pBarreOutil->nNbBoutons])
    {
    bRet = TRUE;
    // On ajuste la taille de la fenetre en fonction
    // de la taille du bitmap et de la dimension du cadre.
    if ((pBarreOutil->xBarre + pBarreOutil->cxBitmap) > pBarreOutil->cxBarre)
      {
      pBarreOutil->cxBarre = pBarreOutil->xBarre + pBarreOutil->cxBitmap + pBarreOutil->cxMarge;
      }
    if ((pBarreOutil->yBarre + pBarreOutil->cyBitmap) > pBarreOutil->cyBarre)
      {
      pBarreOutil->cyBarre = pBarreOutil->yBarre + pBarreOutil->cyBitmap + pBarreOutil->cyMarge;
      }
    pBarreOutil->nNbBoutons++;
    }
  else
    {
		//$$ d�sallouer le bouton suppl�mentaire cr��
    pBarreOutil->xBarre = origin_x;
    pBarreOutil->yBarre = origin_y;
    }

  return bRet;
  } // bBOAjouteBouton

// Ajoute un bouton couleur (palette) � un objet barre d'outil
BOOL bBOAjouteBoutonCouleur
	(
	HBO hBarreOutil,			// handle sur barre d'outil cr��e par hBOCree
	COLORREF ColorRef,		// nom ressource bitmap
	int cx,								// largeur de l'�chantillon de couleur
	int cy,								// hauteur de l'�chantillon de couleur
	int pos,							// bouton sur la m�me ligne (BO_COL_SUIVANTE) ou sur la ligne suivante (BO_NOUVELLE_LN)
	int mnu_mode,					// BO_BOUTON ou BO_COCHE (reste enfonc� une fois sur 2)
	int nIdBouton					// Identifiant (envoy� dans WM_COMMAND avec LOWORD (mp1) = nIdBouton et	mp2 = 0
	)	// TRUE si pas d'erreur
	{
  BOOL  bRet = FALSE;

  PBO		pBarreOutil = PFromHBO (hBarreOutil);
	Verif (pBarreOutil);

	// alloue un pointeur de bouton de plus dans le tableau de pointeurs boutons
	MemRealloue ((PVOID *)(&pBarreOutil->apBoutons), (pBarreOutil->nNbBoutons + 1) * sizeof(pCBoutonBarreOutils));

	// calcul de sa position :
  int	origin_x = pBarreOutil->xBarre;
  int	origin_y = pBarreOutil->yBarre;
	
	// premier bouton ajout� ?
  if (pBarreOutil->nNbBoutons == 0)
    {
    // oui=> pos n'a pas d'importance
    pBarreOutil->xBarre = pBarreOutil->cxMarge;
    pBarreOutil->yBarre = pBarreOutil->cyMarge;
    }
  else
    {
		// non => Nouvelle ligne ?
    if (pos == BO_NOUVELLE_LN)
      {
			// oui => origine en dessous, ligne suivante
      pBarreOutil->xBarre = pBarreOutil->cxMarge;
      pBarreOutil->yBarre += pBarreOutil->cyBitmap;
      }
    else
      {
			// non => ajout � droite du dernier bouton ajout�
      pBarreOutil->xBarre += pBarreOutil->cxBitmap;
      }
    }

	// cr�ation de l'objet bouton Ok ?
	pBarreOutil->apBoutons[pBarreOutil->nNbBoutons] = new CBoutonBarreOutils(HFromPBO (pBarreOutil),
			ColorRef, pBarreOutil->xBarre,pBarreOutil->yBarre, cx,cy,
       &pBarreOutil->cxBitmap,&pBarreOutil->cyBitmap, mnu_mode, nIdBouton);
     
	//pboutonCree (HFromPBO (pBarreOutil),pszBitmap, pBarreOutil->xBarre,pBarreOutil->yBarre,
  //     &pBarreOutil->cxBitmap,&pBarreOutil->cyBitmap, mnu_mode, nIdBouton);

  if (pBarreOutil->apBoutons[pBarreOutil->nNbBoutons])
    {
    bRet = TRUE;
    // On ajuste la taille de la fenetre en fonction
    // de la taille du bitmap et de la dimension du cadre.
    if ((pBarreOutil->xBarre + pBarreOutil->cxBitmap) > pBarreOutil->cxBarre)
      {
      pBarreOutil->cxBarre = pBarreOutil->xBarre + pBarreOutil->cxBitmap + pBarreOutil->cxMarge;
      }
    if ((pBarreOutil->yBarre + pBarreOutil->cyBitmap) > pBarreOutil->cyBarre)
      {
      pBarreOutil->cyBarre = pBarreOutil->yBarre + pBarreOutil->cyBitmap + pBarreOutil->cyMarge;
      }
    pBarreOutil->nNbBoutons++;
    }
  else
    {
		//$$ d�sallouer le bouton suppl�mentaire cr��
    pBarreOutil->xBarre = origin_x;
    pBarreOutil->yBarre = origin_y;
    }

  return bRet;
	}

//-------------------------------------------------------------------------
BOOL bBOOuvre (HBO hBarreOutil, BOOL bPopUp, HWND hwndParent,int o_x,int o_y, int pos, BOOL bVisible)
  {
  BOOL bOk = FALSE;

  PBO pBarreOutil = PFromHBO (hBarreOutil);
	Verif (pBarreOutil);

	// pas deux ouvertures cons�cutives de la m�me barre d'outils
  VerifWarning (!pBarreOutil->hwndBarre);
	pBarreOutil->bPopUp = bPopUp;

  // calcul de la position demand�e
  int origine_x;
  int origine_y;
	if (pos != 0)
    {
		RECT rect;

    if (::GetClientRect(hwndParent, &rect))
      {
      if (bGetOrigineBO(pBarreOutil, pos, &rect,&origine_x,&origine_y))
        bOk = TRUE;
      }
    }
  else
    {
    origine_x = o_x;
    origine_y = o_y;
    bOk = TRUE;
    }
	// Tout est Ok ?
  if (bOk)
		// oui => cr�ation de la fen�tre barre d'outils
    bOk = bCreeFenetreBarreOutil (pBarreOutil, hwndParent, origine_x,origine_y, bVisible);

  return bOk;
  } // bBOOuvre

//-------------------------------------------------------------------------
void BODeplace (HBO hBarreOutil,int o_x, int o_y, int pos)
  {
  int  xOrigine,yOrigine;
  RECT rect;
  PBO pBarreOutil = PFromHBO (hBarreOutil);
	Verif (pBarreOutil);

  // on calcule les coordonnes origine du menu dans la fenetre cliente
  if (pos != 0)
    {
    if (::GetClientRect(::GetParent (pBarreOutil->hwndBarre), &rect))
      {
      if (bGetOrigineBO (pBarreOutil, pos,&rect,&xOrigine,&yOrigine))
        {
        ::SetWindowPos(pBarreOutil->hwndBarre,HWND_TOP,xOrigine,yOrigine,0,0, SWP_NOACTIVATE | SWP_NOSIZE | SWP_NOZORDER);
        pBarreOutil->xBarre = xOrigine;
        pBarreOutil->yBarre = yOrigine;
        }
      }
    }
  else
    {
		::SetWindowPos(pBarreOutil->hwndBarre,HWND_TOP, o_x, o_y,0,0, SWP_NOSIZE | SWP_NOACTIVATE | SWP_NOZORDER );
		pBarreOutil->xBarre = o_x;
		pBarreOutil->yBarre = o_y;
		}
  }

//-------------------------------------------------------------------------
void BOGetRect (HBO hBarreOutil, RECT *prect)
  {
	// R�cup�re le handle de la barre d'outil sp�cifi�e ou de celle par d�faut
  PBO pBarreOutil = PFromHBO (hBarreOutil ? hBarreOutil : hDerniereBOValide);

	// r�cup�re l'encombrement de la fen�tre
	Verif (pBarreOutil);

	// fen�tre existe ?
	if (pBarreOutil->hwndBarre)
		{
		// oui => r�cup�re les coordonn�es de la barre d'outil � l'�cran
		::GetWindowRect(pBarreOutil->hwndBarre, prect);

		// Barre d'outil child ?
		if (!pBarreOutil->bPopUp)
			{
			// oui => Mappe les coordonn�es dans l'espace de la fen�tre parente
			POINT apt[2] = {prect->left, prect->top, prect->right, prect->bottom};
			::MapWindowPoints(NULL, ::GetParent (pBarreOutil->hwndBarre),apt,2);
			prect->left = apt[0].x;
			prect->top = apt[0].y;
			prect->right = apt[1].x;
			prect->bottom = apt[1].y;
			}
		}
	else
		{
		// non => taille virtuelle
		prect->left = pBarreOutil->xBarre;
		prect->top = pBarreOutil->yBarre;
		prect->right = pBarreOutil->cxBarre;
		prect->bottom = pBarreOutil->cyBarre;
		}
  } // BOGetRect

//-------------------------------------------------------------------------
void BOMontre (HBO hBarreOutil, BOOL bVisible)
  {
	// R�cup�re le handle de la barre d'outil sp�cifi�e ou de celle par d�faut
  PBO pBarreOutil = PFromHBO (hBarreOutil ? hBarreOutil : hDerniereBOValide);
	Verif (pBarreOutil);

	// Affiche la comme demand�
  ::ShowWindow(pBarreOutil->hwndBarre, bVisible ? SW_SHOW : SW_HIDE);
  }

//-------------------------------------------------------------------------
BOOL bBOVisible (HBO hBarreOutil)
  {
	// R�cup�re le handle de la barre d'outil sp�cifi�e ou de celle par d�faut
  PBO pBarreOutil = PFromHBO (hBarreOutil ? hBarreOutil : hDerniereBOValide);

	Verif (pBarreOutil);

	// renvoie l'�tat de visibilit� de la barre d'outil
  return ::IsWindowVisible(pBarreOutil->hwndBarre);
  }

//------------------------------------------------------------------------
void BODetruit (HBO * phBarreOutil)
  {
  PBO pBarreOutil = PFromHBO (*phBarreOutil);

	Verif (pBarreOutil);

	// Fen�tre barre d'outil existe  ?
  if (pBarreOutil->hwndBarre)
		{
		// oui => d�truit la barre d'outil (ce qui ferme les boutons)
		::DestroyWindow(pBarreOutil->hwndBarre);
		}
	else
		{
		// non => d�truit les boutons
		int i;

		for (i = 0; (i < pBarreOutil->nNbBoutons); i++)
			FermeBouton (pBarreOutil->apBoutons[i]);
		}

	// Lib�re les ressources de la barre d'outil
  MemLibere ((PVOID *)&pBarreOutil->apBoutons);
  MemLibere ((PVOID *)phBarreOutil);
  }

//-------------------------------------------------------------------------
// Force l'�tat Check d'un bouton
void BOCheckBouton (HBO hBarreOutil, int nIdBouton, BOOL bCheck)
  {
	// R�cup�re le handle de la barre d'outil sp�cifi�e ou de celle par d�faut
  PBO pBarreOutil = PFromHBO (hBarreOutil ? hBarreOutil : hDerniereBOValide);
	pCBoutonBarreOutils pBoutonBarreOutil;

	Verif (pBarreOutil);

	// Trouve le bouton valide ?
	pBoutonBarreOutil = pBoutonBoTrouve (pBarreOutil, nIdBouton);
  if (pBoutonBarreOutil && ::IsWindowEnabled(pBoutonBarreOutil->hwndBouton))
		::SendMessage (pBoutonBarreOutil->hwndBouton, BM_SETCHECK, bCheck ? BST_CHECKED : BST_UNCHECKED, 0);
  }

//-------------------------------------------------------------------------
// simule un click sur un bouton
void BOCliqueBouton (HBO hBarreOutil, int nIdBouton)
  {
	// R�cup�re le handle de la barre d'outil sp�cifi�e ou de celle par d�faut
  PBO pBarreOutil = PFromHBO (hBarreOutil ? hBarreOutil : hDerniereBOValide);
	pCBoutonBarreOutils pBoutonBarreOutil;

	Verif (pBarreOutil);

	// Trouve le bouton valide ?
	pBoutonBarreOutil = pBoutonBoTrouve (pBarreOutil, nIdBouton);
  if (pBoutonBarreOutil && ::IsWindowEnabled(pBoutonBarreOutil->hwndBouton))
		::SendMessage (pBoutonBarreOutil->hwndBouton, BM_CLICK, 0, 0);
  }

//-------------------------------------------------------------------------
void BOValideBouton (HBO hBarreOutil, int nIdBouton, BOOL bValide)
  {
	// R�cup�re le handle de la barre d'outil sp�cifi�e ou de celle par d�faut
  PBO pBarreOutil = PFromHBO (hBarreOutil ? hBarreOutil : hDerniereBOValide);
	pCBoutonBarreOutils pBoutonBarreOutil;

	Verif (pBarreOutil);

	// Bouton trouv� dans la barre d'outil ?
  pBoutonBarreOutil = pBoutonBoTrouve (pBarreOutil, nIdBouton);

	if (pBoutonBarreOutil)
		// oui => valide ou invalide le bouton demand�
		::EnableWindow (pBoutonBarreOutil->hwndBouton, bValide ? TRUE : FALSE);
  }


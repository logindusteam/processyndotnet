#ifndef DLLMAN_H
#define DLLMAN_H
// DLLMan.h
// Gestion du lien avec une DLL sur demande

// Table de lien � l'ex�cution avec la DLL Applicom
typedef struct
	{
	FARPROC	pfn;							// adresse de fonction � appeler si non NULL
	PCSTR		pszNomFonction;		// adresse du Nom de la fonction � initialiser avant le lien
	} LIEN_FONCTION;

// Table de lien � l'ex�cution avec la DLL Applicom
typedef struct
	{
	FARPROC	* ppfn;							// adresse de fonction � appeler si non NULL
	PCSTR		pszNomFonction;		// adresse du Nom de la fonction � initialiser avant le lien
	} LIEN_PFONCTION;

// Lien � la demande avec l'ensemble des fonctions de la DLL 
BOOL bLieDLL (HINSTANCE * phInst, PCSTR pszNomDLL, LIEN_FONCTION aLiens [], UINT uNbLiens);

// Lien � la demande avec l'ensemble des fonctions de la DLL 
BOOL bLieDLL (HINSTANCE * phInst, PCSTR pszNomDLL, LIEN_PFONCTION aLiens [], UINT uNbLiens);

// Lib�re les liens avec l'ensemble des fonctions de la DLL
BOOL bLibereLienDLL (HINSTANCE * phInst, LIEN_FONCTION aLiens [], UINT uNbLiens);

// Lib�re les liens avec l'ensemble des fonctions de la DLL
BOOL bLibereLienDLL (HINSTANCE * phInst, LIEN_PFONCTION aLiens [], UINT uNbLiens);

#endif

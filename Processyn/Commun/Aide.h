// Aide.h: interface for the CAide class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AIDE_H__85627812_6B4A_4C32_94A3_3B03BA55C77D__INCLUDED_)
#define AFX_AIDE_H__85627812_6B4A_4C32_94A3_3B03BA55C77D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "HtmlHelp.h"

class CAide  
	{
	public:
		// Constructeur / destructeur
		CAide();
		virtual ~CAide();

		// Sp�cifie le fichier d'aide
		void SetNomFichier(PCSTR pszNomFichier);

		// Affiche aide globale
		BOOL bAfficheTout(HWND hwndParent);
	private:
		void Initialize();
		void UnInitialize();
		CHAR m_szNomFichier[MAX_PATH];
		HWND m_hwndVisible;
		BOOL m_bAideInitialisee;
		DWORD m_dwCookie;
	};

#endif // !defined(AFX_AIDE_H__85627812_6B4A_4C32_94A3_3B03BA55C77D__INCLUDED_)

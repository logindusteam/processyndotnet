
// --------------------------------------------------------------------------
#define MAX_CHAR_PRINTER_NAME          80
#define MAX_CHAR_QUEUE_NAME           256
#define MAX_CHAR_MODEL_NAME           256
#define MAX_CHAR_PRINTER_DRIVER_NAME  256



// --------------------------------------------------------------------------
// Lecture de l'imprimante par d�fault
// --------------------------------------------------------------------------
void GetDefaultPrinter (char *pszPrinterName, char *pszQueueName, char *pszDriverName, char *pszModelName);


// --------------------------------------------------------------------------
// Configuration de l'imprimante par l'utilisateur
// --------------------------------------------------------------------------
LONG GetJobPropertiesLength  (char *pszPrinterName, char *pszDriverName, char *pszModelName);
void GetUserJobProperties    (char *pszPrinterName, char *pszDriverName, char *pszModelName, PDEVMODE pDriv);
void GetDefaultJobProperties (char *pszPrinterName, char *pszDriverName, char *pszModelName, PDEVMODE pDriv);


// --------------------------------------------------------------------------
// Ouverture Imprimante
// --------------------------------------------------------------------------
HDC OpenPrinterLI   (char *pszQueueName, char *pszDriverName, PDEVMODE pDriv);
void ClosePrinterLI (HDC hdcPrinter);

// --------------------------------------------------------------------------
// Dimensions du Papier
// --------------------------------------------------------------------------
void GetPagePrinter (HDC hdcPrinter, LONG *PageWidth, LONG *PageHeight);

// --------------------------------------------------------------------------
// Contr�le de l'Imprimante
// --------------------------------------------------------------------------
void StartDocumentPrinter (HDC hdcPrinter, char *pszDocumentName);
void NewPagePrinter       (HDC hdcPrinter);
void EndDocumentPrinter   (HDC hdcPrinter);

// --------------------------------------------------------------------------

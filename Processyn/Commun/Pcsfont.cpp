// --------------------------------------------------------------------------
// Gestion des polices pour PcsGr
// 21/1/97	Win32 JS
// --------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"
#include "Appli.h"
#include "UStr.h"
#include "UEnv.h"
#include "IdGrLng.h"
#include "ULangues.h"
#include "pcsfont.h"

// --------------------------------------------------------------------------
static char * aszNomsPolicesStandard [PCS_FONT_NB_POLICES_STANDARD] =
  {
  "Courier New"			,	// 0
  "Arial"						,	// 1
  "Times New Roman" ,	// 2
  "Symbol"						// 3
  };

#define TAILLE_PIXEL_MIN 4
#define TAILLE_PIXEL_MAX 300

// param�tres de dlgprocFnt
typedef struct
	{
	DWORD	dwNPolice;
	DWORD	dwBitsStyle;
	DWORD	dwTaillePixel;
	BYTE	byCharSetFont;
	HFONT hfontExemple;
	} ENV_DLG_FONT, *PENV_DLG_FONT;

//------------------------------------------------------------------------------
// dlgprocFnt : Mise � jour de l'�chantillon de police
static void MajExemple (HWND hwnd)
	{
	HFONT	hfont;

	// r�cup�re l'environnement de cette boite de dialogue
	PENV_DLG_FONT pEnv = (PENV_DLG_FONT)pGetEnv (hwnd);

	// $$ NULL pour HDC non couramment utilis�
	hfont = fnt_create (NULL, pEnv->dwNPolice, pEnv->dwBitsStyle, pEnv->dwTaillePixel, pEnv->byCharSetFont);

	// Nouvelle fonte et redessin de l'exemple
	SendDlgItemMessage (hwnd, IDC_STATIC_EXEMPLE, WM_SETFONT, (WPARAM)hfont, MAKELPARAM (TRUE, 0));

	// Supprime l'�ventuelle ancienne fonte
	if (pEnv->hfontExemple)
		DeleteObject (pEnv->hfontExemple);

	// stocke la nouvelle
	pEnv->hfontExemple = hfont;
	}

//------------------------------------------------------------------------------
// dlgproc choix d'une police
static BOOL CALLBACK dlgprocFnt (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
	{
	LRESULT mRes = 1;
	PENV_DLG_FONT pEnv;

	switch (msg)
		{
    case WM_INITDIALOG:
			{
			int	i;
			char szTaille [10];

			// m�morise l'environnement
			pEnv = (PENV_DLG_FONT)pSetEnvOnInitDialogParam (hwnd, mp2);

			// pas de fonte exemple couramment cr��e
			pEnv->hfontExemple = NULL;

			// Remplis la liste des caract�res
      for (i = 0; i < PCS_FONT_NB_POLICES_STANDARD; i++)
        {
        SendDlgItemMessage(hwnd, IDC_COMBO_POLICE, CB_ADDSTRING,0,(LPARAM)(aszNomsPolicesStandard[i]));
        }
			// Remplis la liste des tailles
      for (i = TAILLE_PIXEL_MIN; i <= 16; i+= 2)
        {
				StrDWordToStr (szTaille, i);
        SendDlgItemMessage(hwnd, IDC_COMBO_TAILLE, CB_ADDSTRING,0,(LPARAM)szTaille);
        }
      for (i = 20; i <= 60; i+= 4)
        {
				StrDWordToStr (szTaille, i);
        SendDlgItemMessage(hwnd, IDC_COMBO_TAILLE, CB_ADDSTRING,0,(LPARAM)szTaille);
        }
      for (i = 60; i <= TAILLE_PIXEL_MAX; i+= 10)
        {
				StrDWordToStr (szTaille, i);
        SendDlgItemMessage(hwnd, IDC_COMBO_TAILLE, CB_ADDSTRING,0,(LPARAM)szTaille);
        }

			// Positionne les indicateurs aux valeurs courantes
			SendDlgItemMessage(hwnd, IDC_COMBO_POLICE, CB_SETCURSEL, pEnv->dwNPolice, 0);

			StrDWordToStr (szTaille, pEnv->dwTaillePixel);
			SetDlgItemText (hwnd, IDC_COMBO_TAILLE, szTaille);

			::CheckDlgButton(hwnd,IDC_CHECK_GRAS		, pEnv->dwBitsStyle & PCS_FONT_STYLE_GRAS		 ? BST_CHECKED : BST_UNCHECKED);
			::CheckDlgButton(hwnd,IDC_CHECK_ITALIQUE, pEnv->dwBitsStyle & PCS_FONT_STYLE_ITALIQUE ? BST_CHECKED : BST_UNCHECKED);
			::CheckDlgButton(hwnd,IDC_CHECK_SOULIGNE, pEnv->dwBitsStyle & PCS_FONT_STYLE_SOULIGNE ? BST_CHECKED : BST_UNCHECKED);

			// Mise � jour de la police exemple
			MajExemple (hwnd);
			}
      break;

		case WM_DESTROY:
			// r�cup�re l'environnement de cette boite de dialogue
			pEnv = (PENV_DLG_FONT)pGetEnv (hwnd);

			// Supprime l'�ventuelle ancienne fonte
			if (pEnv->hfontExemple)
				{
				DeleteObject (pEnv->hfontExemple);
				pEnv->hfontExemple = NULL;
				}
			break;

		case WM_CLOSE:
			// abandon de la boite de dialogue
      EndDialog (hwnd,0);
			break;

    case WM_COMMAND:
			// r�cup�re l'environnement de cette boite de dialogue
			pEnv = (PENV_DLG_FONT)pGetEnv (hwnd);
			switch (LOWORD(mp1))
				{
				case IDC_COMBO_POLICE:
					if (HIWORD(mp1) == CBN_SELCHANGE)
						{
						// S�lection d'une nouvelle police
						int nIdFont = SendDlgItemMessage(hwnd, IDC_COMBO_POLICE, CB_GETCURSEL, 0, 0);

						if (nIdFont != LB_ERR)
							{
							pEnv->dwNPolice = (DWORD) nIdFont;
							MajExemple (hwnd);
							}
						}
					break;

				case IDC_COMBO_TAILLE:
					{
					DWORD wTaille = pEnv->dwTaillePixel;

					switch (HIWORD(mp1))
						{
						case CBN_SELCHANGE://CBN_EDITUPDATE)//CBN_SELENDOK) //CBN_EDITCHANGE) //CBN_SELCHANGE)
							{
							// S�lection d'une nouvelle taille
							char szTaille [10];
							
							SendDlgItemMessage(hwnd, IDC_COMBO_TAILLE, CB_GETLBTEXT, 
								(WPARAM)SendDlgItemMessage(hwnd, IDC_COMBO_TAILLE, CB_GETCURSEL, 0, 0) ,(LPARAM) (LPCSTR) szTaille);
 
							StrToDWORD (&wTaille, szTaille);
							}
							break;
						case CBN_EDITCHANGE:
							{
							// Edition d'une nouvelle taille
							char szTaille [10];
							
							GetDlgItemText(hwnd, IDC_COMBO_TAILLE, szTaille, 10);
							StrToDWORD (&wTaille, szTaille);
							}
						} // switch (HIWORD(mp1))
					if (wTaille != pEnv->dwTaillePixel)
						{
						pEnv->dwTaillePixel = wTaille;
						MajExemple (hwnd);
						}
					}
					break;

				case IDC_CHECK_GRAS:
					// action sur la check box Gras
					if (IsDlgButtonChecked (hwnd, IDC_CHECK_GRAS) == BST_CHECKED)
						pEnv->dwBitsStyle |= PCS_FONT_STYLE_GRAS;
					else
						pEnv->dwBitsStyle &= ~PCS_FONT_STYLE_GRAS;
					MajExemple (hwnd);
					break;

				case IDC_CHECK_ITALIQUE:
					// action sur la check box Italique
					if (IsDlgButtonChecked (hwnd, IDC_CHECK_ITALIQUE) == BST_CHECKED)
						pEnv->dwBitsStyle |= PCS_FONT_STYLE_ITALIQUE;
					else
						pEnv->dwBitsStyle &= ~PCS_FONT_STYLE_ITALIQUE;
					MajExemple (hwnd);
					break;

				case IDC_CHECK_SOULIGNE:
					// action sur la check box Souligne
					if (IsDlgButtonChecked (hwnd, IDC_CHECK_SOULIGNE) == BST_CHECKED)
						pEnv->dwBitsStyle |= PCS_FONT_STYLE_SOULIGNE;
					else
						pEnv->dwBitsStyle &= ~PCS_FONT_STYLE_SOULIGNE;
					MajExemple (hwnd);
					break;

				case IDOK:
					// Validation de la boite de dialogue
          EndDialog(hwnd, TRUE);
					break;

				case IDCANCEL:
          EndDialog (hwnd, FALSE);
					break;
				}
      break;

		default:
			mRes = 0;
			break;

    } // switch (msg)
	return mRes;
	}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//								Fonctions export�es
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//------------------------------------------------------------------
// Appel boite de dialogue de choix d'une police et de ses attributs
// renvoie TRUE si boite de dialogue valid�e
BOOL fnt_user (HWND hwnd, DWORD * pdwNPolice, DWORD * pIdStyle, DWORD * pdwTaillePixel,BYTE byCharSetFont)
	{
	BOOL bOk;
	ENV_DLG_FONT EnvDlgFont = {*pdwNPolice, *pIdStyle, * pdwTaillePixel, byCharSetFont};

	bOk = LangueDialogBoxParam (MAKEINTRESOURCE(DLG_FNT), hwnd, dlgprocFnt, (LPARAM)(&EnvDlgFont));
	if (bOk)
		{
		*pdwNPolice = EnvDlgFont.dwNPolice;
		*pIdStyle = EnvDlgFont.dwBitsStyle;
		*pdwTaillePixel = EnvDlgFont.dwTaillePixel;
		}
	return bOk;
	}

	
//--------------------------------------------------------
// Cr�e une police � partir de ses caract�ristiques
HFONT fnt_create (HDC hdc, DWORD dwNPolice, DWORD dwBitsStyle, DWORD dwTaillePixel, BYTE byCharSetFont)
	{
	LOGFONT	lf;

	// V�rification de l'Id de police 
	if (dwNPolice >= PCS_FONT_NB_POLICES_STANDARD)
		dwNPolice = 0;

	// Initialisation de l'objet police
	lf.lfHeight = - (int)dwTaillePixel; //$$-MulDiv (sSize, GetDeviceCaps(hdc, LOGPIXELSY), 72);
	lf.lfWidth = 0; 
	lf.lfEscapement = 0; 
	lf.lfOrientation = 0;
	lf.lfWeight = (dwBitsStyle & PCS_FONT_STYLE_GRAS) ? FW_BOLD : FW_NORMAL;
	lf.lfItalic = (dwBitsStyle & PCS_FONT_STYLE_ITALIQUE) ? TRUE : FALSE;
	lf.lfUnderline = (dwBitsStyle & PCS_FONT_STYLE_SOULIGNE) ? TRUE : FALSE;;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = byCharSetFont; //TURKISH_CHARSET; //$$ ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEVICE_PRECIS;
	lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lf.lfQuality = DRAFT_QUALITY; //$$ ?
	lf.lfPitchAndFamily = FF_DONTCARE; //FF_MODERN | FIXED_PITCH | 
	StrCopy (lf.lfFaceName, aszNomsPolicesStandard[dwNPolice]);
	return CreateFontIndirect (&lf);
	}

//--------------------------------------------------------
void fnt_destroy (HFONT hfont)
	{
	if (hfont)
		DeleteObject (hfont);
	}

/*
	// code pour appeler la boite de dialogue de choix d'une police standard
	CHOOSEFONT	cf;
	LOGFONT			lf;

	// Initialisation de l'objet police
	lf.lfHeight = -16; //$$-MulDiv (sSize, GetDeviceCaps(hdc, LOGPIXELSY), 72);
	lf.lfWidth = 0; 
	lf.lfEscapement = 0; 
	lf.lfOrientation = 0;
	lf.lfWeight = FW_NORMAL;
	lf.lfItalic = FALSE;
	lf.lfUnderline = FALSE;
	lf.lfStrikeOut = FALSE;
	lf.lfCharSet = ANSI_CHARSET;
	lf.lfOutPrecision = OUT_DEVICE_PRECIS;
	lf.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	lf.lfQuality = DRAFT_QUALITY; //$$ ?
	lf.lfPitchAndFamily = FF_MODERN; // FIXED_PITCH | 
	StrCopy (lf.lfFaceName, aszNomsPolicesStandard[0]);

	// Initialisation de l'objet de s�lection de police
	cf.lStructSize = sizeof (CHOOSEFONT);
  cf.hwndOwner = hwnd;
  cf.hDC = NULL;	// pas de flags CF_PRINTERFONTS or CF_BOTH
  cf.lpLogFont = &lf; // utilis� en entr�e car CF_INITTOLOGFONTSTRUCT
  cf.iPointSize = 120;	// $$ taille en 1/10 de points
  cf.Flags = CF_INITTOLOGFONTSTRUCT | CF_SCREENFONTS | CF_NOVECTORFONTS | CF_EFFECTS; // CF_SCALABLEONLY | 
  cf.rgbColors = RGB(0,0,0); // $$
  cf.lCustData = 0; // pas de lpfnHook
  cf.lpfnHook = NULL; // pas de CF_ENABLEHOOK
  cf.lpTemplateName = NULL; // pas de CF_ENABLETEMPLATE
  cf.hInstance = NULL; // pas de CF_ENABLETEMPLATEHANDLE ou CF_ENABLETEMPLATE
  cf.lpszStyle = NULL; // pas de CF_USESTYLE
  cf.nFontType = 0;	// pas de for�age d'attributs en sortie
  cf.nSizeMin = 0;	// pas de CF_LIMITSIZE
  cf.nSizeMax = 0;	// pas de CF_LIMITSIZE

	bOk = ChooseFont (&cf);

//------------------------------------------------------------------------------------------------
// Callback d'�num�ration des polices utilis� pour d�terminer les tailles de caract�re disponibles
static int CALLBACK EnumFamCallBack (const LOGFONT * lpelf, const TEXTMETRIC *lpntm, DWORD FontType, LPARAM lParam)
	{ 
	int * aiFontCount = (int *) lParam; 

	// Stocke le nombre de polices raster, TrueType, et vector
	if (FontType & RASTER_FONTTYPE) 
		aiFontCount[0]++; 
	else if (FontType & TRUETYPE_FONTTYPE) 
		aiFontCount[2]++; 
	else 
		aiFontCount[1]++; 

	if (aiFontCount[0] || aiFontCount[1] || aiFontCount[2]) 
		return TRUE; 
	else 
		return FALSE; 

	UNREFERENCED_PARAMETER( lpelf ); 
	UNREFERENCED_PARAMETER( lpntm ); 
	}

//------------------------------------------------------------------------------
// dlgprocFnt : Mise � jour de l'�chantillon de police
static void MajTailles (HWND hwnd)
	{
	// r�cup�re l'environnement de cette boite de dialogue
	PENV_DLG_FONT pEnv = pGetEnv (hwnd);

	// Nouvelle fonte et redessin de l'exemple
	//SendDlgItemMessage (hwnd, IDC_STATIC_EXEMPLE, WM_SETFONT, (WPARAM)hfont, MAKELPARAM (TRUE, 0));

	int aFontCount[] = { 0, 0, 0 };
	//$$ Mauvais hdc
	HDC	hdc = ::GetDC (hwnd);

	// Enum�re les polices
	EnumFontFamilies (hdc, aszNomsPolicesStandard[pEnv->dwNPolice], EnumFamCallBack, (LPARAM) aFontCount);
	::ReleaseDC (hwnd, hdc);
	//EnumFontFamilies(hdc, (LPCTSTR) NULL, (FONTENUMPROC) EnumFamCallBack, (LPARAM) aFontCount); 
	}


*/

// Gestion des infos d�pendant des langues
// Elles sont stock�es en ressource dans une DLL.
// Celle si choisie en fonction de la langue d�sir�e.
// Permet de connaitre la langue par d�faut du syst�me


//---------------------------------------------
// Renvoie l'identifiant d'info de Localisation associ� au thread
LCID lcidCourante (void);

//---------------------------------------------
// Renvoie l'identifiant d'info de Localisation du syst�me
LCID lcidSysteme (void);

//---------------------------------------------
// Renvoie l'identifiant d'info de Localisation de l'utilisateur
LCID lcidUtilisateur (void);

//-------------------------------------------------------------
// Renvoie l'abbr�viation de la langue choisie par l'utilisateur
BOOL bGetAbbreviationLangue (LCID lcid, PSTR pszLangueUtilisateurCourt, UINT uTailleChaine);

//-------------------------------------------------------------
// Renvoie un LCID correspondant au langage principal (suppression du sous langage)
// ex : SUBLANG_FRENCH_CANADIAN => LANG_FRENCH
LCID lcidLangagePrincipal(LCID lcid);

//-------------------------------------------------------------
// Renvoie le nom de la langue choisie par l'utilisateur dans sa langue
BOOL bGetNomLangue (LCID lcid, PSTR pszLangueUtilisateur, UINT uTailleChaine);

//---------------------------------------------------------
// Renvoie la chaine de caract�re associ�e � une erreur OS
BOOL bChaineErreurSysteme (PSTR lpMsgBuf, DWORD dwTailleBuf, UINT uNErreurSysteme);

// ----------------------------------------------------------------------
// Charge les infos et la DLL correspondant � la langue choisie (ex : FRA)
// dans le r�pertoire de l'appli
HINSTANCE hChargeDLLLangue
	(
	PCSTR pszNomEtExtFichier,					// exemple: PcsExe.exe
	PCSTR pszAbbrevLangueUtilisateur	// exemple : FRA
	); // renvoie son hinst ou NULL

// ----------------------------------------------------------------------
// DialogBoxParam en chargeant la ressource � partir de Appli.hinstDllLng
int LangueDialogBoxParam
	(
  LPCTSTR lpTemplateName,  // identifies dialog box template (Cf MAKEINTRESOURCE)
  HWND hWndParent,      // handle to owner window
  DLGPROC lpDialogFunc, // pointer to dialog box procedure
  LPARAM dwInitParam    // initialization value
	);

// ----------------------------------------------------------------------
// CreateDialogParam en chargeant la ressource � partir de Appli.hinstDllLng
HWND LangueCreateDialogParam
	(
  LPCTSTR lpTemplateName,  // identifies dialog box template
  HWND hWndParent,      // handle to owner window
  DLGPROC lpDialogFunc, // pointer to dialog box procedure
  LPARAM dwInitParam    // initialization value
	);
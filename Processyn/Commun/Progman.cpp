// Progman.c gestion des process
// --------------------------------------------------------------------------
#include "stdafx.h"
#include "UStr.h"               // Str...()
#include "MemMan.h"
#include "Verif.h"
#include "Progman.h"

VerifInit;

// instanciation constante
#define HANDLE_NON_OUVERT 0
const PROCESS_INFORMATION PROCESS_INFORMATION_NULL = {HANDLE_NON_OUVERT, HANDLE_NON_OUVERT, 0, 0};

// --------------------------------------------------------------------------
// Lib�re les handles d'un process information ouvert
BOOL bProgLibereProcessInformation (PROCESS_INFORMATION * pProcessInformation)
	{
	BOOL bOk = TRUE;

	Verif(pProcessInformation);
	if (pProcessInformation->hProcess != HANDLE_NON_OUVERT)
		{
		if (CloseHandle (pProcessInformation->hProcess))
			pProcessInformation->hProcess = HANDLE_NON_OUVERT;
		else
			bOk = FALSE;
		}

	if (pProcessInformation->hThread != HANDLE_NON_OUVERT)
		{
		if (CloseHandle (pProcessInformation->hThread))
			pProcessInformation->hThread = HANDLE_NON_OUVERT;
		else
			bOk = FALSE;
		}

	if (bOk)
		{
		pProcessInformation->dwProcessId = 0;
		pProcessInformation->dwThreadId = 0;
		}
	return bOk;
	}

// --------------------------------------------------------------------------
BOOL  bProgExecute (PROCESS_INFORMATION * pProcessInformation, BOOL bExecutionSynchrone, PCSTR pszProgramme, PCSTR pszCommandes, 
	char TabPtrEnvironnement[][STR_MAX_CHAR_ARRAY], UINT uNbMaxEnvironnment, PCSTR pszCheminExecution)
  {
  BOOL      bOk = FALSE;
	SECURITY_ATTRIBUTES sa = {sizeof(SECURITY_ATTRIBUTES), NULL, FALSE}; //n'h�rite pas des handles du parent
	STARTUPINFO si = {sizeof(STARTUPINFO), NULL,NULL,NULL,0,0,0,0,0,0,0,0,0,0,NULL,NULL,NULL,NULL};

	CHAR      szEnvironnement[256];
	PCSTR			pCheminExecution;
	PVOID *   pEnvironnement;
	Verif (pProcessInformation);
	*pProcessInformation = PROCESS_INFORMATION_NULL;

	// lance le process

	// G�n�re la ligne de commande
	CHAR szLigneDeCommande[256];
	StrCopy (szLigneDeCommande, pszProgramme);
	if (pszCommandes && pszCommandes[0])
		{
		if (pszCommandes[0] != ' ')
			StrConcat (szLigneDeCommande, " ");
		StrConcat (szLigneDeCommande, pszCommandes);
		}

	// G�n�re l'environnement
	if (uNbMaxEnvironnment != 0)
		{
		StrArrayToStrSep(szEnvironnement, TabPtrEnvironnement, uNbMaxEnvironnment, '\0');
		}
	else
		{
		pEnvironnement = NULL;
		}
	if ((!pszCheminExecution) || StrIsNull (pszCheminExecution)) 
		{
		pCheminExecution = NULL;
		}
	else
		{
		pCheminExecution = pszCheminExecution;
		}
  bOk = CreateProcess (
		NULL,								//pointer to name of executable module 
		szLigneDeCommande,	// pointer to command line string
		&sa,								// pointer to process security attributes 
		&sa,								// pointer to thread security attributes 
		FALSE,							// handle inheritance flag 
		CREATE_DEFAULT_ERROR_MODE|NORMAL_PRIORITY_CLASS,	// creation flags 
		pEnvironnement,			// pointer to new environment block 
		pCheminExecution,		// pointer to current directory name 
		&si,								// pointer to STARTUPINFO 
		pProcessInformation	// pointer to PROCESS_INFORMATION  
		);

	// ex�cution process Ok ?
  if (bOk)
    {
		// oui => process synchrone ?
		if (bExecutionSynchrone)
			{
			// oui => process synchrone : attente de la fin du process lanc� Ok ?
			if (WaitForSingleObject (pProcessInformation->hProcess, INFINITE) == WAIT_OBJECT_0)
				{
				bOk = bProgLibereProcessInformation (pProcessInformation);
				}
			else
				bOk = FALSE;
			}
    }

  return bOk;
  }

//----------------------------------------------------------------
// Callback d'�num�ration des fen�tres envoyant un WM_CLOSE � toute les fen�tres ayant
// le m�me ProcessId que l'appli que l'on d�sire fermer (Cf. bProgArrete)
static BOOL CALLBACK FermeFenetresProg (HWND hwnd, LPARAM lParam)
	{
	DWORD dwID ;
	
	GetWindowThreadProcessId(hwnd, &dwID) ;
	
	if(dwID == (DWORD)lParam)
		{
		PostMessage(hwnd, WM_CLOSE, 0, 0) ;
		}
	
	return TRUE ;
	}

//----------------------------------------------------------------
// Shut down a 32-Bit Process (or 16-bit process under Windows 95)
//----------------------------------------------------------------
BOOL bProgArrete (PROCESS_INFORMATION * pProcessInformation, DWORD dwTimeoutAvantAbort)
	{
	BOOL bOk = TRUE;

	Verif(pProcessInformation);

	if (pProcessInformation->hProcess != HANDLE_NON_OUVERT)
		{
		// Appel de FermeFenetresProg() envoie un WM_CLOSE � toutes les fen�tres ayant le m�me dwProcessId
		EnumWindows((WNDENUMPROC)FermeFenetresProg, (LPARAM) pProcessInformation->dwProcessId);
		
		// Wait on the handle. If it signals, great.
		// If it times out, then you kill it.
		if (WaitForSingleObject(pProcessInformation->hProcess, dwTimeoutAvantAbort)!=WAIT_OBJECT_0)
			{
			if (TerminateProcess (pProcessInformation->hProcess, (UINT)-1))
				bOk = bProgLibereProcessInformation (pProcessInformation);
			else
				bOk = FALSE;
			}
		}
		
	return bOk;
	}

//----------------------------------------------------------------
// Callback d'�num�ration des fen�tres en activant les fen�tres top level ayant
// le m�me ProcessId que l'appli que l'on d�sire fermer (Cf. bProgArrete)
static BOOL CALLBACK ActiveProg (HWND hwnd, LPARAM lParam)
	{
	DWORD dwID ;
	
	GetWindowThreadProcessId(hwnd, &dwID) ;
	
	if(dwID == (DWORD)lParam)
		{
		if ((GetParent (hwnd) == NULL) && IsWindowVisible(hwnd))
			SetForegroundWindow (hwnd) ;
		}
	
	return TRUE ;
	}

//----------------------------------------------------------------
// Rend actif un 32-Bit Process (or 16-bit process under Windows 95)
//----------------------------------------------------------------
BOOL bProgActive (PROCESS_INFORMATION * pProcessInformation)
	{
	BOOL bOk = TRUE;

	Verif(pProcessInformation);

	if (pProcessInformation->hProcess != HANDLE_NON_OUVERT)
		{
		// Appel de FermeFenetresProg() envoie un WM_CLOSE � toutes les fen�tres ayant le m�me dwProcessId
		EnumWindows((WNDENUMPROC)ActiveProg, (LPARAM) pProcessInformation->dwProcessId);
		}
		
	return bOk;
	}

// --------------------------------------------------------------------------
// Teste si un process est termin� et l'attends �ventuellement
BOOL bProgFini (PROCESS_INFORMATION * pProcessInformation, BOOL bAttenteFin)
  {
	// Pas de process g�r� => programme d�ja fini
  BOOL  bOk = (pProcessInformation->hProcess == HANDLE_NON_OUVERT);

	if (!bOk)
		{
		if (bAttenteFin)
			bOk = WaitForSingleObject (pProcessInformation->hProcess, INFINITE) == WAIT_OBJECT_0;
		else
			bOk = WaitForSingleObject (pProcessInformation->hProcess, 0) == WAIT_OBJECT_0;

		// destruction des handle process si termin�
		if (bOk)
			{
			bOk = bProgLibereProcessInformation (pProcessInformation);
			}
		}
  return bOk;
  }

// --------------------------------------------------------------------------
// Regarde si Process information est diff�rent de PROCESS_INFORMATION_NULL
BOOL bProgUtilise (const PROCESS_INFORMATION * pProcessInformation)
  {
	return (pProcessInformation->hProcess != HANDLE_NON_OUVERT) ||
		(pProcessInformation->hThread != HANDLE_NON_OUVERT) ||
		(pProcessInformation->dwProcessId != 0) || (pProcessInformation->dwThreadId != 0);
  }

//---------------------------------------------------------------------------
/*
Extrait de "HOWTO: Terminate an Application "Cleanly" in Win32"
	Last reviewed: April 2, 1998
	Article ID: Q178893  
	The information in this article applies to: 
	Microsoft Win32 Application Programming Interface (API) included with: 
	- Microsoft Windows NT version 4.0 
	- Microsoft Windows 95 

SUMMARY
	In a perfect world, your process could ask another process, through some form of 
	inter-process communication, to shut down. 
	However, if you do not have source-level control of the application that you wish
	to shut down, then you may not have this option. 
	Although there is no guaranteed "clean" way to shut down an application in Win32,
	there are steps that you can take to ensure that the application uses the best
	method for cleaning up resources. 

MORE INFORMATION
	32-Bit Processes (and 16-Bit Processes under Windows 95)
	Under Win32, the operating system promises to clean up resources owned by a process when it shuts down.
	This does not, however, mean that the process itself has had the opportunity to do any final flushes of
	information to disk, any final communication over a remote connection, nor does it mean that the
	process' DLL's will have the opportunity to execute their PROCESS_DETACH code.
	This is why it is generally preferable to avoid terminating an application under Windows 95 and Windows NT. 

If you absolutely must shut down a process, follow these steps: 
	Post a WM_CLOSE to all Top-Level windows owned by the process that you want to shut down.
	Many Windows applications respond to this message by shutting down.
	NOTE: A console application's response to WM_CLOSE depends on whether or not it has installed a control handler.
	For additional information, please see the following article in the Microsoft Knowledge Base: 
		ARTICLE-ID: Q102429 
		TITLE     : HOWTO: Detect Closure of Command Window from a Console App

   Use EnumWindows() to find the handles to your target windows.
	 In your callback function, check to see if the windows' process ID matches the process you want to shut down.
	 You can do this by calling GetWindowThreadProcessId().
	 Once you have established a match, use PostMessage() or SendMessageTimeout() to post the WM_CLOSE message to the window.

	Use WaitForSingleObject() to wait for the handle of the process.
	Make sure you wait with a timeout value, because there are many situations in which the WM_CLOSE will not
	shut down the application. Remember to make the timeout long enough (either with WaitForSingleObject(),
	or with SendMessageTimeout()) so that a user can respond to any dialog boxes that were created in response
	to the WM_CLOSE message. 
	If the return value is WAIT_OBJECT_0, then the application closed itself down cleanly.
	If the return value is WAIT_TIMEOUT, then you must use TerminateProcess() to shutdown the application.
	NOTE: If you are getting a return value from WaitForSingleObject() other then WAIT_OBJECT_0 or WAIT_TIMEOUT,
	use GetLastError() to determine the cause. 

	By following these steps, you give the application the best possible chance to shutdown cleanly (aside from IPC or user-intervention). 

	The 16-Bit Issue (under Windows NT)
	The preceding steps work for 16-bit applications under Windows 95, however, Windows NT 16-bit applications
	work very differently. 

Under Windows NT, all 16-bit applications run in a virtual DOS machine (VDM).
	This VDM runs as a Win32 process (NTVDM) under Windows NT. The NTVDM process has a process ID.
	You can obtain a handle to the process through OpenProcess(), just like you can with any other Win32 process.
	Nevertheless, none of the 16-bit applications running in the VDM have a process ID, and therefore you cannot
	get a Process Handle from OpenProcess().
	Each 16-bit application in a VDM has a 16-bit Task Handle and a 32-bit thread of execution.
	The handle and thread ID can be found through a call to the function VDMEnumTaskWOWEx().
	For additional information, please see the following article in the Microsoft Knowledge Base: 
   ARTICLE-ID: Q175030
   TITLE     : HOWTO: Enumerate Applications in Win32

Your first, and most straightforward, option when shutting down a 16-bit application under Windows NT is to shut
	down the entire NTVDM process. You can do this by following the steps outlined above.
	You only need to know	the process ID of the NTVDM process (see the KB article Q175030 cited above to find the
	process ID of an NTVDM).
	The downside of this approach is that it closes all 16-bit applications that are running in that VDM.
	If this is not your goal, then you need to take another approach. 

If you wish to shut down a single 16-bit application within a NTVDM process, following are the steps
	you need to take: 
	Post a WM_CLOSE to all Top-Level windows that are owned by the process, and that have the same owning thread ID
	as the 16-bit task you want to shut down.
	The most effective way to do this is by using EnumWindows(). In your callback function, check to see
	if the window's process ID and thread ID matches the 16-bit task you want to shut down.
	Remember that the process ID is going to be the process ID of the NTVDM process in which the 16-bit
	application is running. 
	Although you have a thread ID, you have no way to wait on the termination of the 16-bit process.
	As a result, you must wait for an arbitrary length of time (to allow a clean shut down), and then
	try to shut the application down anyway. If the application has already shut down, then this will do nothing.
	If it hasn't shut down, then it will terminate the application. 
	Terminate the application using a function called VDMTerminateTaskWOW(), which can be found in the Vdmdbg.dll.
	It takes the process ID of the VDM and the task number of the 16-bit task. 
	This approach allows you to shut down a single 16-bit application within a VDM under Windows NT.
	However, 16-bit Windows is not very good at cleaning up resources of a terminated task,
	and neither is the WOWExec running in the VDM.
	If you are looking for the cleanest possible approach to terminating a 16-bit application under Windows NT,
	you should consider terminating the entire VDM process.
	NOTE: If you are starting a 16-bit application that you may terminate later, then use
	the CREATE_SEPARATE_WOW_VDM with CreateProcess(). 

Sample Code
	The sample code implements the techniques described above for 16-bit and 32- bit applications using the following two functions: TerminateApp() and Terminate16App(). TerminateApp() takes a 32-bit process ID and a timeout (in miliseconds). Terminate16App(). Both functions use explicit linking to DLL functions so that they will be binary compatible across Windows NT and Windows 95. 

#define TA_FAILED 0
#define TA_SUCCESS_CLEAN 1
#define TA_SUCCESS_KILL 2
#define TA_SUCCESS_16 3

// Callback d'�num�ration des fen�tres pour d�terminer celles qui doivent �tre ferm�es
BOOL CALLBACK TerminateAppEnum (HWND hwnd, LPARAM lParam)
	{
	DWORD dwID ;
	
	GetWindowThreadProcessId(hwnd, &dwID) ;
	
	if(dwID == (DWORD)lParam)
		{
		PostMessage(hwnd, WM_CLOSE, 0, 0) ;
		}
	
	return TRUE ;
	}


//----------------------------------------------------------------
// Shut down a 32-Bit Process (or 16-bit process under Windows 95)
//----------------------------------------------------------------
DWORD WINAPI TerminateApp
	(
	DWORD dwPID,		// Process ID of the process to shut down.
	DWORD dwTimeout	// Wait time in milliseconds before shutting down the process.
	)	// Retour :
		//	TA_FAILED - If the shutdown failed.
		//	TA_SUCCESS_CLEAN - If the process was shutdown using WM_CLOSE.
		//	TA_SUCCESS_KILL - if the process was shut down with TerminateProcess().
	{
	DWORD   dwRet ;
	
	// If we can't open the process with PROCESS_TERMINATE rights,
	// then we give up immediately.
	HANDLE hProc = OpenProcess (SYNCHRONIZE|PROCESS_TERMINATE, FALSE, dwPID);
	
	if (hProc == NULL)
		{
		return TA_FAILED ;
		}
	
	// TerminateAppEnum() posts WM_CLOSE to all windows whose PID
	// matches your process's.
	EnumWindows((WNDENUMPROC)TerminateAppEnum, (LPARAM) dwPID) ;
	
	// Wait on the handle. If it signals, great.
	// If it times out, then you kill it.
	if(WaitForSingleObject(hProc, dwTimeout)!=WAIT_OBJECT_0)
		dwRet=(TerminateProcess(hProc,0)?TA_SUCCESS_KILL:TA_FAILED);
	else
		dwRet = TA_SUCCESS_CLEAN ;
	
	CloseHandle(hProc) ;
	
	return dwRet ;
	}
*/

/*----------------------------------------------------------------
DWORD WINAPI Terminate16App (DWORD dwPID, DWORD dwThread, WORD w16Task, DWORD dwTimeout)

Purpose:
	Shut down a Win16 APP.

Parameters:
	dwPID Process ID of the NTVDM in which the 16-bit application is running.
	dwThread Thread ID of the thread of execution for the 16-bit application.
	w16Task	16-bit task handle for the application.
	dwTimeout Wait time in milliseconds before shutting down the task.

Return Value:
	If successful, returns TA_SUCCESS_16
	If unsuccessful, returns TA_FAILED.
NOTE:  These values are defined in the header for this
function.

NOTE:
	You can get the Win16 task and thread ID through the
	VDMEnumTaskWOW() or the VDMEnumTaskWOWEx() functions.
----------------------------------------------------------------*/
/*
//#include <vdmdbg.h>
typedef struct
	{
	DWORD   dwID ;
	DWORD   dwThread ;
	} TERMINFO ;

BOOL CALLBACK Terminate16AppEnum( HWND hwnd, LPARAM lParam )
	{
	DWORD      dwID ;
	DWORD      dwThread ;
	TERMINFO   *termInfo ;
	
	termInfo = (TERMINFO *)lParam ;
	
	dwThread = GetWindowThreadProcessId(hwnd, &dwID) ;
	
	if(dwID == termInfo->dwID && termInfo->dwThread == dwThread )
		{
		PostMessage(hwnd, WM_CLOSE, 0, 0) ;
		}
	
	return TRUE ;
	}

DWORD WINAPI Terminate16App (DWORD dwPID, DWORD dwThread, WORD w16Task, DWORD dwTimeout)
	{
	HINSTANCE      hInstLib ;
	TERMINFO      info ;
	
	// You will be calling the functions through explicit linking
	// so that this code will be binary compatible across
	// Win32 platforms.
	BOOL (WINAPI *lpfVDMTerminateTaskWOW)(DWORD dwProcessId,
		WORD htask) ;
	
	hInstLib = LoadLibraryA( "VDMDBG.DLL" ) ;
	if( hInstLib == NULL )
		return TA_FAILED ;
	
	// Get procedure addresses.
	lpfVDMTerminateTaskWOW = (BOOL (WINAPI *)(DWORD, WORD ))
		GetProcAddress( hInstLib, "VDMTerminateTaskWOW" ) ;
	
	if( lpfVDMTerminateTaskWOW == NULL )
		{
		FreeLibrary( hInstLib ) ;
		return TA_FAILED ;
		}
	
	// Post a WM_CLOSE to all windows that match the ID and the
	// thread.
	info.dwID = dwPID ;
	info.dwThread = dwThread ;
	EnumWindows((WNDENUMPROC)Terminate16AppEnum, (LPARAM) &info) ;
	
	// Wait.
	Sleep( dwTimeout ) ;
	
	// Then terminate.
	lpfVDMTerminateTaskWOW(dwPID, w16Task) ;
	
	FreeLibrary( hInstLib ) ;
	return TA_SUCCESS_16 ;
	}

*/


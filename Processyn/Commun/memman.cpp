/*--------------------------------------------------------------------------+
 | Ce fichier est la propriet� de                                           |
 |                                                                          |
 |                    Societ� LOGIQUE INDUSTRIE                             |
 |              Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3               |
 |                                                                          |
 | Il demeure sa propriet� exclusive, et est confidentiel.                  |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |                                                                          |
 | Titre......: MemMan.c                                                    |
 | Auteur.....: J.B.                                                        |
 | Date.......: 11/04/94                                                    |
 | Version....:                                                             |
 |                                                                          |
 | Mises a jours:                                                           |
 | +========+========+===+================================================+ |
 | | Date   | Qui    |N� | Raisons                                        | |
 | + - - - -+ - - - -+ - + - - - - - - - - - - - - - - - - - - - - - - - -+ |
 | |Jj/Mm/Aa|Nom     |00x| Texte...                                       | |
 | +========+========+===+================================================+ |
 | |26/03/96|eric    |  1|adaptation � Win32                              | |
 | +--------+--------+---+------------------------------------------------+ |
 | |18/10/96|jerome  |  2|- fcts inutiles ou non multithread + Exceptions | |
 | +--------+--------+---+------------------------------------------------+ |
 | |        |        |   |                                                | |
 | +========+========+===+================================================+ |
 |                                                                          |
 | Remarques..: Allocation dynamique de m�moire.                            |
 | ===========                                                              |
 | Attention..: Si _msize() n'existe pas sur le compilateur pour l'OS donn�,|
 | ===========  il faut g�rer une ent�te pour toutes les allocations faites,|
 | qui contiendra la taille r�serv�e.                                       |
 |                                                                          |
 |                                                                          |
 +--------------------------------------------------------------------------*/
#include "stdafx.h"
#include <malloc.h>    // _msize
#include "Appli.h"
#include "UExcept.h"
#include "MemMan.h"

static char pszNomModule[] = "MemMan";

//---------------------------------------------------------------------------
// FONCTIONS LOCALES
//---------------------------------------------------------------------------
//------------------------------------------------------------------------------
// Interface op�rateur pour permettre � l'utilisateur de lib�rer de la m�moire
// pour permettre une demande d'allocation m�moire.
// L'utilisateur peut refuser de continuer.
static BOOL bDemandePlace (DWORD dwTaille)
	{
	BOOL	bRet = TRUE;
	char szErr [400];

	//$$ traduction
	wsprintf (szErr, "Demande de %u octets de m�moire impossible a satisfaire.\n\n"
		"Pour continuer, veuillez fermer d'autres applications ou lib�rer de la place\n"
		"sur votre disque dur pour lib�rer de la m�moire virtuelle, puis validez Ok", dwTaille);

	// non => demande a l'utilisateur de lib�rer de la place
	if (::MessageBox (Appli.hwnd, Appli.szNom, NULL, MB_ICONSTOP | MB_APPLMODAL | MB_RETRYCANCEL) == IDCANCEL)
		// refus de lib�rer de la place => on termine
		bRet = FALSE;
	return bRet;
	}

//---------------------------------------------------------------------------
// FONCTIONS EXPORTEES
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Alloue et renvoie l'adresse d'un bloc de uTaille octets
// Demande � l'utilisateur - si n�cessaire - de lib�rer de la place m�moire
// et fait monter une exception si la demande est refus�e.
//---------------------------------------------------------------------------
PVOID pMemAlloue (UINT uTaille)
  {	// par d�faut, rien n'est allou� 
  PVOID pRet = NULL;

	// au moins un octet � allouer ?
  if (uTaille != 0)
    {
		// oui => boucle tant que l'allocation n'est pas effectu�e
		while (!pRet)
			{
			// alloc Ok ?
			pRet = malloc(uTaille);

			// non => redemande de la place
			if (!pRet)
				{
				// Agr�ment utilisateur ?
				if (!bDemandePlace(uTaille))
					ExcepFormatContexte1 (EXCEPTION_MEMMAN, "Allocation de %lu octets impossible", uTaille);
				}
			}
    }
  return pRet;
  }

//---------------------------------------------------------------------------
// Idem pMemAlloue; la zone allou�e est initialis�e � 0
//---------------------------------------------------------------------------
PVOID pMemAlloueInit0 (UINT uTaille)
  {	
	// Allocation du bloc
  PVOID pRet = pMemAlloue (uTaille);

	// Initialisation � 0 $$ Inutile sous NT ??
	ZeroMemory (pRet, uTaille);

  return pRet;
  }

//---------------------------------------------------------------------------
// Idem pMemAlloue; la zone allou�e est initialis�e en recopiant uTaille octets
// � partir de pSource
PVOID pMemAlloueInit (UINT uTaille, const PVOID pSource)
  {	
	// Allocation du bloc
  PVOID pRet = pMemAlloue (uTaille);

	// Initialisation � 0 $$ Inutile sous NT ??
	CopyMemory (pRet, pSource, uTaille);

  return pRet;
  }


//---------------------------------------------------------------------------
// Lib�re la m�moire � l'adresse *ppBase puis remet cette adresse � NULL
void MemLibere (PVOID * ppBase)
  {
	PVOID pBase = *ppBase;

	// Quelque chose � lib�rer ?
	if (*ppBase)
		{
		// oui => Marque le libre puis lib�re le (r�entrance)
		*ppBase = NULL;
		free (pBase);
		}
  }

//---------------------------------------------------------------------------
// Renvoie l'adresse d'un bloc de uNouvelleTaille octets contenant les donn�es initiales de pBase.
// pBase peut �tre NULL. 
// Si uNouvelleTaille vaut 0, l'ancien bloc est lib�r� et NULL est renvoy�.
// Demande � l'utilisateur - si n�cessaire - de lib�rer de la place m�moire
// et fait monter une exception si la demande est refus�e.
//---------------------------------------------------------------------------
void MemRealloue (PVOID *ppBase, UINT uNouvelleTaille)
  {
	// nouvelle taille nulle ?
  if (uNouvelleTaille == 0)
    {
		// oui => lib�re le pointeur si pr�c�demment allou�
    if (*ppBase != NULL)
      {
      MemLibere (ppBase);
      }
    }
  else
    {
		// non => pointeur pas encore allou� ?
    if (*ppBase == NULL)
			// oui => alloue
      *ppBase = pMemAlloue (uNouvelleTaille);
    else
      {
			PVOID pRes = *ppBase;

			// non => boucle jusqu'a r�allocation ou fin de process
			do
				{
				pRes = realloc (*ppBase, uNouvelleTaille);
				if (pRes)
					*ppBase = pRes;
				else
					{
					if (!bDemandePlace(uNouvelleTaille))
						ExcepFormatContexte1 (EXCEPTION_MEMMAN, "R�allocation de %lu octets impossible",uNouvelleTaille);
					}
				} while (!pRes);
      }
    }
  } // MemRealloue

//---------------------------------------------------------------------------
// renvoie la taille d'un bloc allou�
UINT uMemTaille (PVOID pBase)
  {
  UINT  uTaille = 0;

	if (pBase != NULL)
		uTaille = _msize (pBase);

  return uTaille;
  }

//---------------------------------------------------------------------------
// R�alloue et ins�re uTaille octets en *ppBase + wOffset (en octets)
void MemInsere (PVOID *ppBase, UINT uOffset, UINT uTaille)
  {
  UINT  uTailleAllouee = uMemTaille (*ppBase);
  
	// Offset compatible avec la taille ?
	if (uOffset > uTailleAllouee)
		// non => exception
		ExcepFormatContexte2 (EXCEPTION_MEMMAN, "Insertion en %lu dans un buffer de %lu octets impossible", uOffset, uTailleAllouee);

	// R�alloue
	MemRealloue (ppBase, uTailleAllouee + uTaille);

	// pousse en fin de buffer les octets au dela du point d'insertion
  if (*ppBase)
    {
		PVOID pSource = pMemUOffset (*ppBase, uOffset);
		PVOID pDest = pMemUOffset (pSource, uTaille);

    MemMove (pDest, pSource, uTailleAllouee - uOffset);
    }
  }

//---------------------------------------------------------------------------
//  Supprime uTaille octets en *ppBase + wOffset (en octets) et r�alloue
void MemEnleve (PVOID * ppBase, UINT uOffset, UINT uTaille)
  {
  UINT  uTailleAllouee = uMemTaille (*ppBase);
  PVOID pSource;
  PVOID pDest;

	// Supression compatible avec les param�tres actuels ?
	if (uTailleAllouee < (uOffset + uTaille))
		// non => Exception
		ExcepFormatContexte3 (EXCEPTION_MEMMAN, "Suppression de %lu octets en %lu impossible dans un buffer de %lu octets", uTailleAllouee, uOffset, uTaille);

	uTailleAllouee -= uTaille;
	pDest = pMemUOffset (*ppBase, uOffset);
  pSource = pMemUOffset (pDest, uTaille);
  MemMove (pDest, pSource, uTailleAllouee - uOffset);
	MemRealloue (ppBase, uTailleAllouee);
  }

//---------------------------------------------------------------------------
// R�alloue et ajoute � la fin du buffer uTaille octets
void MemAjouteFin	(PVOID * ppBase, UINT uTaille)
	{
	MemInsere (ppBase, uMemTaille (ppBase), uTaille);
	}

//---------------------------------------------------------------------------
// R�alloue et ins�re au d�but du buffer uTaille octets
void MemInsereDebut	(PVOID * ppBase, UINT uTaille)
	{
	MemInsere (ppBase, 0, uTaille);
	}

//---------------------------------------------------------------------------
//  Supprime uTaille octets en *ppBase et r�alloue
void MemEnleveDebut (PVOID * ppBase, UINT wOffset, UINT uTaille)
	{
	MemEnleve (ppBase, 0, uTaille);
	}

//---------------------------------------------------------------------------
//  R�alloue Supprime les derniers uTaille octets de *ppBase 
void MemEnleveFin (PVOID * ppBase, UINT wOffset, UINT uTaille)
	{
	MemEnleve (ppBase, uMemTaille (ppBase) - uTaille, uTaille);
	}

//---------------------------------------------------------------------------

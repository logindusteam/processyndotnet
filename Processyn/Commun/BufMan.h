// -----------------------------------------------------------
// BufMan.h
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Utilitaires pour g�rer des buffers
// --------------------------------------------------------------------------
// ajoute le buffer source � la fin du buffer dest (allocation / r�alloc pDest au plus juste)
// *pdwDestLen mis � jour
// renvoie FALSE si erreur (= allocation)
BOOL bBufAjouteFin (PSTR * ppDest, PDWORD pdwLgDest, PSTR pSource, DWORD dwLgSource);

// enl�ve dwSupprLen octets au d�but du buffer (free / r�alloc pBuf au plus juste)
// *pdwLgDest mis � jour
// renvoie FALSE si erreur (= allocation ou dwSupprLen trop grand)
BOOL bBufSupprimeDebut (PSTR * ppDest, PDWORD pdwLgDest, DWORD dwLgSuppr);

// enl�ve dwSupprLen octets � la fin du buffer (free / r�alloc pBuf au plus juste)
// *pdwLgDest mis � jour
// renvoie FALSE si erreur (= allocation ou dwSupprLen trop grand)
BOOL bBufSupprimeFin (PSTR * ppDest, PDWORD pdwLgDest, DWORD dwLgSuppr);

// Lib�re le contenu du buffer (free / r�alloc pBuf au plus juste)
// *pdwLgDest mis � 0
// renvoie FALSE si erreur (= allocation)
BOOL bBufVide (PSTR * ppDest, PDWORD pdwLgDest);

// charge un fichier dans un buf non allou�
BOOL bBufCharge (PSTR * ppDest, PDWORD pdwLgDest, PSTR pszNomFichier);

// Enregistre le contenu d'un buf dans un fichier
BOOL bBufSauve (PSTR * ppDest, PDWORD pdwLgDest, PSTR pszNomFichier);


//----------- fin BufMan.h -------------------


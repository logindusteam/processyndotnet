/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : bdanmgr.h                                                |
 |   Auteur  : AC					        	|
 |   Date    : 04/05/94					        	|
 |   Version : 4.00							|
 +----------------------------------------------------------------------*/

// -------------------------------------------------------------------------
// Fonctions utilis�es
// -------------------------------------------------------------------------
void bdanmgr_init_proc (BOOL	bOuvert);

// -------------------------------------------------------------------------

//-----------------------------------------------------
// ATTENTION: pas de test du type d'animation: 
// suppose que le champ identifiant est toujours le dernier
DWORD bdanmgr_maj_identifiant_graphique (DWORD n_ligne_ref, PSTR strIdentifiant);
BOOL bdanmgr_get_identifiant_animation (DWORD n_ligne_ref, DWORD *pNumeroPage, PSTR strIdentifiant);

//
DWORD bdanmgr_parametrer (ANIMATION TypeAnimation, HBDGR hbdgrAnimation, DWORD n_ligne_ref, DWORD *n_ligne_anim, DWORD dwElement);
DWORD bdanmgr_inserer (char *texte, DWORD *n_ligne_anim);
DWORD bdanmgr_modifier (char *texte, DWORD n_ligne_anim);
void bdanmgr_consulter (char *texte, DWORD n_ligne_anim, ANIMATION *TypeAnim, DWORD dwTailleTexte);
BOOL bdanmgr_supprimable (DWORD n_ligne_anim);
BOOL bdanmgr_supprimer (DWORD n_ligne_anim);
BOOL bdanmgr_supprimer_tout (void);
void bdanmgr_supprimer_tout_inutilise (void);
BOOL bdanmgr_lis_couleurs2 (DWORD n_ligne_anim, DWORD indice, PG_COULEUR pnCouleur, PG_COULEUR pnCouleurFond);
BOOL bdanmgr_lis_couleurs4 (DWORD n_ligne_anim, DWORD indice, PG_COULEUR pnCouleur, PG_COULEUR pnCouleurFond);
void bdanmgr_TypeAnimation (G_ACTION Action, ANIMATION *TypeAnimation);
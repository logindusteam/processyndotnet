// PathMan.h : gestion des r�pertoires d'une application
// En g�n�ral on utilise deux types de r�pertoires :
// PathAppli : r�pertoire UNIQUE ou se trouve l'ex�cutable en cours d'ex�cution g�r� par (Appli)
// PathDoc : r�pertoire(s) ou se trouve(nt) le(s) document(s) (ou les donn�es) 
//					 charg�es par l'application (g�r� par DocMan).

#include "stdafx.h"
#include "std.h"
#include "UStr.h"

#include "PathMan.h"


// -------------------------------------------------------------------------------
// Copie dans pszBuf un pathname compos� du path sp�cifi� et du nom du fichier
// renvoie pszBuf ou NULL si Err
PSTR pszCreePathName (PSTR pszBuf, int nTailleBuf, PCSTR pszPath,  PCSTR pszNom)
	{
	PSTR pszRet = NULL;

	// param�tres valides ?
	if (pszPath && pszBuf && pszNom && nTailleBuf)
		{
		int nAjusteNomFichier = 0;
		int nTaillePath = StrLength (pszPath);
		int nTailleNom = StrLength (pszNom);

				// oui => cr�e le pathname
		if (nTaillePath > 1)
			{
			// path termin� par \  ?
			if (pszPath [nTaillePath-1] != '\\')
				// non => en rajouter un
				nAjusteNomFichier++;

			// nom d�bute par '\' ?
			if (pszNom [0] == '\\')
				// oui => l'enlever
				nAjusteNomFichier--;

			// taille pour cr�er le pathname suffisante ?
			if ((nTaillePath + nTailleNom + nAjusteNomFichier) < nTailleBuf)
				{
				// Path
				lstrcpy (pszBuf, pszPath);

				// rajout �ventuel d'un \ au path
				if (nAjusteNomFichier > 0)
					{
					StrConcatChar (pszBuf, '\\');
					nAjusteNomFichier = 0;
					}

				// rajout du nom du path en sautant �ventuellement son \ en trop
				StrConcat (pszBuf, pszNom - nAjusteNomFichier);

				// renvoie le pointeur sur le buffer
				pszRet = pszBuf;
				} // if (nTaillePath + StrLength (pszNom) > nTailleBuf)

			} // if (nTaillePath + StrLength (pszNom) > nTailleBuf)

		}
	return pszRet;
	}

// -------------------------------------------------------------------------------
// Copie dans pszBuf un nom de fichier suivi de son extension de type PathName.Extension
// renvoie pszBuf ou NULL si Err
PSTR pszCreeNameExt (PSTR pszBuf, int nTailleBuf, PCSTR pszNom, PCSTR pszExtension)
	{
	PSTR pszRet = NULL;

	// param�tres valides ?
	if (pszBuf && pszNom && pszExtension && nTailleBuf)
		{
		int nAjusteExt = 0;
		int nTailleNom = StrLength (pszNom);
		int nTailleExt = StrLength (pszExtension);

		// oui => cr�e le pathname
		if (nTailleNom > 1)
			{
			// Nom termin� par .  ?
			if (pszNom [nTailleNom-1] != '.')
				// non => en rajouter un
				nAjusteExt++;

			// extension d�bute par '.' ?
			if (pszExtension [0] == '.')
				// oui => l'enlever
				nAjusteExt--;

			// taille pour cr�er le nom.extension suffisante ?
			if ((nTailleNom + nTailleExt + nAjusteExt) < nTailleBuf)
				{
				// copie du nom
				StrCopy (pszBuf, pszNom);

				// rajout �ventuel d'un . au nom
				if (nAjusteExt > 0)
					{
					StrConcatChar (pszBuf, '.');
					nAjusteExt = 0;
					}

				// rajout de l'extension au nom seul en sautant �ventuellement son . en trop
				StrConcat (pszBuf, pszExtension - nAjusteExt);

				// renvoie le pointeur sur le buffer
				pszRet = pszBuf;
				} // if ((nTailleNom + nTailleExt + nAjusteExt) < nTailleBuf)

			} // if (nTailleNom > 1)

		}
	return pszRet;
	} // pszCreeNameExt

// -------------------------------------------------------------------------------
// Ajoute � pszRepDest, adresse d'un r�pertoire, un nom (ayant �ventuellement une extension).
// r�sultat : pszRepDest de type R�pertoire\Nom
// renvoie pszRepDest ou NULL si Err (pointeurs nulls ou pas assez de place pour faire le traitement)
PSTR pszRepAjouteNom (PSTR pszRepDest, int nTailleDest, PCSTR pszNom)
	{
	return pszCreePathName (pszRepDest, nTailleDest, pszRepDest, pszNom);
	}

// -------------------------------------------------------------------------------
// Ajoute � pszNomDest, adresse d'un nom de fichier (�ventuellement apr�s un r�pertoire), une extension.
// r�sultat : pszNomDest de type R�pertoire\Nom.ext
// renvoie pszPathDest ou NULL si Err (pointeurs nulls ou pas assez de place pour faire le traitement)
PSTR pszNomAjouteExt (PSTR pszNomDest, int nTailleDest, PCSTR pszExt)
	{
	return pszCreeNameExt (pszNomDest, nTailleDest, pszNomDest, pszExt);
	}

// -------------------------------------------------------------------------------
// Copie dans pszBuf un pathname de type Path\NomSeul.Extension
// renvoie pszBuf ou NULL si Err
PSTR pszCreePathNameExt (PSTR pszBuf, int nTailleBuf, PCSTR pszPath,  PCSTR pszNomSeul, PCSTR pszExtension)
	{
	PSTR pszRet = NULL;
	char BufNomExt [MAX_PATH];

	pszRet = pszCreePathName (pszBuf, nTailleBuf, pszPath,  pszCreeNameExt (BufNomExt, MAX_PATH, pszNomSeul, pszExtension));
	return pszRet;
	}

// -------------------------------------------------------------------------------
// Supprime le Path d'un nom de fichier 
PSTR pszSupprimePath (PSTR pszNom)
	{
  DWORD wIdxCar;

	// Efface ce qui pr�c�de le dernier '\' s'il y en a un
  while ((wIdxCar = StrSearchChar (pszNom, '\\')) != STR_NOT_FOUND)
    {
    StrDelete (pszNom, 0, wIdxCar + 1);
    }
	// Efface ce qui pr�c�de le dernier ':' s'il y en a un
  while ((wIdxCar = StrSearchChar (pszNom, ':')) != STR_NOT_FOUND)
    {
    StrDelete (pszNom, 0, wIdxCar + 1);
    }

  return pszNom;
	}

// -------------------------------------------------------------------------------
// Supprime le Nom et l'extension d'un pathname : garde le path
PSTR pszSupprimeNomExt (PSTR pszPathName)
	{
  int nNCar = ((int)StrLength (pszPathName))-1;

	// cherche le premier \ en partant de la fin
	while ((nNCar >= 0) && (pszPathName[nNCar] != '\\'))
		nNCar--;

	// \ trouv� ?
	if (nNCar >= 0)
		// oui => c'�tait le caract�re de la fin du path
		pszPathName[nNCar] = '\0';
  return pszPathName;
	}

// -------------------------------------------------------------------------------
// Supprime l'extension d'un nom de fichier 
PSTR pszSupprimeExt (PSTR pszNom)
	{
  int nNCar = ((int)StrLength (pszNom))-1;

	// cherche le premier '.' en partant de la fin
	while ((nNCar >= 0) && (pszNom[nNCar] != '.'))
		nNCar--;

	// \ trouv� ?
	if (nNCar >= 0)
		// oui => c'�tait le '.' s�parateur de l'extension
		pszNom[nNCar] = '\0';

  return (pszNom);
	}

// -------------------------------------------------------------------------------
// Supprime le Path et l'extension d'un nom de fichier 
PSTR pszSupprimePathEtExt (PSTR pszNom)
	{
	PSTR pszRet = pszSupprimeExt (pszNom);

	return pszSupprimePath (pszRet);
	}

// -------------------------------------------------------------------------------
// Teste la pr�sence de l'extension pr�cis�e dans le nom de fichier 
// renvoie TRUE si trouv�
BOOL bExtensionPresente (PCSTR pszNom, PCSTR pszExtension)
  {
	BOOL	bRes = FALSE;
  int nNCar = ((int)StrLength (pszNom))-1;

	// cherche le premier '.' en partant de la fin
	while ((nNCar >= 0) && (pszNom[nNCar] != '.'))
		nNCar--;

	// \ trouv� ?
	if (nNCar >= 0)
		{
		// oui => c'�tait le '.' s�parateur de l'extension :
		// on va comparer les extensions sans le point
		if (pszExtension[0] == '.')
			pszExtension++;

		bRes = bStrIEgales (&pszNom[nNCar+1], pszExtension);
		}
	return bRes;
  }

// --------------------------------------------------------------------------------
// r�cup�re le nom d'unit� ou le nom UNC commen�ant le pathName
// renvoie pszPath ou NULL si Err
PSTR pSupprimeToutSaufUnite (PSTR pszPath)
	{
	PSTR	pszRes = NULL;
  int		nNCar = ((int)StrLength (pszPath));

	// Path de forme X:... ?
	if ((nNCar >= 2) && (pszPath[1] == ':'))
		{
		// oui => Ok, on coupe apr�s
		pszPath[2] ='\0';
		pszRes = pszPath;
		}
	else
		{
		// Test si le Path est de la forme \\Serveur\partage\...
		// Path commence par \\ ?
		if ((nNCar >= 6) && (pszPath[0] == '\\') && (pszPath[1] == '\\'))
			{
		  DWORD NSeparation;

			// oui => trouve le premier '\' entre Serveur et partage et la taille de Serveur n'est pas nulle ?
			if (((NSeparation = StrSearchChar (&pszPath[2], '\\')) != STR_NOT_FOUND) && NSeparation)
				{
				DWORD NFin;
				// oui => trouve le premier '\' apr�s partage et la taille de Partage n'est pas nulle ?
				if (((NFin = StrSearchChar (&pszPath[3+NSeparation], '\\')) != STR_NOT_FOUND) && NFin)
					{
					// oui => Ok, on coupe apr�s
					pszPath[1 + 3 + NSeparation + NFin] ='\0';
					pszRes = pszPath;
					}
				}
			}
		}

	return pszRes;
	}

// -------------------------------------------------------------------------------
// Modifie si possible un pathname pour le rendre relatif � un path absolu. Le chemin renvoy� commence par un \
// Teste si le pathname contient le path absolu en tete => si oui rien sinon supprime
PSTR pszPathNameRelatif (PSTR pszPathNameAModifier, PCSTR pszPathAbsolu)
	{
	PSTR pszRet = pszPathNameAModifier;
	// $$	ac gerer le cas ou le path absolu se trouve au milieu du chemin ????

	if (StrSearchStr	(pszPathNameAModifier, pszPathAbsolu) == 0)
		{
		StrDelete (pszPathNameAModifier, 0, StrLength (pszPathAbsolu));
		}
	return pszRet;
	}

// -------------------------------------------------------------------------------
// Renvoie TRUE si un pathname est absolu
// Teste si le pathname est complet (comporte une unite ou un serveur UNC) => si oui rien sinon insere le path de reference
// (sous la forme x:xxxxxx ou \\xxxxxxxx)
// FALSE sinon 
BOOL bPathNameEstAbsolu (PCSTR pszPathName)
	{
	BOOL bRes = FALSE;
  int		nNCar = ((int)StrLength (pszPathName));

	// Le pathName est il absolu ?
	// Path de forme X:\ ou // Path commence par \\ ?
	if (((nNCar >= 3) && (pszPathName[1] == ':') && (pszPathName[2] == '\\')) ||
      ((nNCar >= 6) && (pszPathName[0] == '\\') && (pszPathName[1] == '\\')))
		bRes = TRUE;

	return bRes;
	}

// -------------------------------------------------------------------------------
// Modifie si possible un pathname relatif pour le rendre absolu
// Teste si le pathname est complet (comporte une unite ou un serveur UNC) => si oui rien sinon insere le path de reference
PSTR pszPathNameAbsolu	(
												PSTR pszPathNameAModifier, // le chemin est construit meme si ne commence pas par \ 
												PCSTR pszPathRef					// ne doit pas se terminer par un \	
												)
	{
	PSTR pszRet = pszPathNameAModifier;

	// Le pathName est relatif ?
	if (!bPathNameEstAbsolu (pszPathNameAModifier))
		{
		// oui => le Path de r�f�rence est absolu ?
		if (bPathNameEstAbsolu (pszPathRef))
			{
			// oui => fait pr�c�der le path sp�cifi� par PathRef \ $$ � am�liorer
			char szBuf[MAX_PATH];

			if (pszCreePathName (szBuf, MAX_PATH, pszPathRef,  pszPathNameAModifier))
				StrCopy (pszPathNameAModifier, szBuf);
			else
				pszRet = NULL;
			}
		pszRet = NULL;
		}
	else
		pszRet = NULL;

	return pszRet;
	}

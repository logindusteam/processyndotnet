/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------*/
// Tipe.h
// description des type de la base de donnees Processyn

// ----- mnemos reperes bloc -------
#define b_e_s 1
#define bx_e_s 297
#define bx_e_s_cour_log 298
#define bx_e_s_cour_num 383
#define bx_e_s_cour_mes 454
#define bx_log_forcage  894
#define bx_num_forcage  895
#define bx_mes_forcage  896
// #define b_identif 2
//#define b_mot_res 44
#define b_constant 3
#define b_pt_constant 5
#define b_organise_es 10
#define b_organise_cle 26
#define b_limite_driver 22

#define b_cour_log 11
#define b_cour_num 12
#define b_cour_mes 13
#define bva_mem_log 14
#define bva_mem_num 15
#define bva_mem_mes 16

#define bx_clavier 51
#define b_periph_entree 24
#define b_periph_sortie 25
#define b_va_systeme 57

#define b_geo_syst 50
#define b_represente_syst 326

#define bn_constant 420
#define b_cst_nom 421
#define bn_identif 422
#define b_es_nom 423

#define  B_SIGNATURE_FICHIER  920

#define repere_e 323

// ------------------------------------------------------------
// Nom des types de fichiers g�r�s par Processyn.

#define EXTENSION_SOURCE     "pcs"
#define EXTENSION_A_FROID    "xpc"
#define EXTENSION_A_CHAUD    "hot"
#define EXTENSION_SAT        "sat"
#define EXTENSION_PA1        "pa1"
#define EXTENSION_DBG        "dbg"
#define EXTENSION_BAK        "bak"
#define EXTENSION_SYN        "syn"
#define EXTENSION_SRC        "src"
#define EXTENSION_SRX        "srx"


// ------------------------------------------------------------
// Types bd

typedef struct // $$ manque la valeur initiale
	{
	ID_MOT_RESERVE sens; // c_res_variable_ta_e, c_res_variable_ta_s, c_res_variable_ta_es
	ID_MOT_RESERVE genre;	
	DWORD ref;         // nbre pos libre pour premier enr $$ doit �tre prot�g�
	DWORD i_identif;   // Premier enr : pos premier trou. Autres : pos dans bn_identif
	DWORD n_desc;				// ID_DESCRIPTEUR $$ pr�ciser le type d'identifiant de descripteur
	DWORD taille;				// Dimension du tableau ($$ � regrouper avec le decriptif de la variable)
	} ENTREE_SORTIE, *PENTREE_SORTIE;

#define c_nb_car_es 22
#define c_nb_car_ex_mes 82

typedef struct
	{
	char pszNomVar[c_nb_car_es];
	DWORD wPosEx;
	DWORD wBlocGenre;
	DWORD wTaille;
	} X_E_S, *PX_E_S;

typedef struct
	{
	DWORD dwPosDansBx_e_s; // pt_arriere
	} X_E_S_COUR, *PX_E_S_COUR;

typedef struct
	{
	DWORD dwPosCourEx; // pt_arriere
	DWORD wBlocGenreEx;
	} X_VAR_SYS, *PX_VAR_SYS; // Devrait �tre r�serv� � "PcsVarEx.h"

typedef struct
	{
	char nom[c_nb_car_es];
	DWORD dwPosES;
	} IDENTIF, *PIDENTIF; // bn_identif

typedef enum
  {
  // g_es = 0
  // g_res = 1
  // g_mem = 2
  // g_cst = 3
  g_id = 4,
  g_num = 5,
  g_bizarre = 6
  } GENRE_UL;

typedef struct
	{
	char show[c_nb_car_ex_mes];
	GENRE_UL genre;
	DWORD erreur;
	} UL, *PUL;

#define nb_max_ul 80
typedef UL tv_ul[nb_max_ul];


typedef struct
	{
	DWORD pos_geo;
	DWORD driver;
	char cle[12];
	}  ORGANISATION_ES, *PORGANISATION_ES;

#define NB_MAX_ELEMENTS_VAR_TABLEAU 10000

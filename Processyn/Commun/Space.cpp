//---------------------------------------------------------------------------
// Mapping de coordonn�es
// $$ utilisation : souvent on connait l'�tat de chaque espace lorsqu'on d�sire une action
// (SPC_PIXEL ou SPC_VIRTUEL) => optimisation et clarification en ayant des fonctions 
// portant explicitement le type de conversion dans le nom
//---------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"
#include "MemMan.h"

#include "space.h"

typedef struct
	{
	int	cxPixel;					// dimensions de l'espace pixel (�ventuellement n�gatif)
	int	cyPixel;
	int	xArrondiPixel;		// utilis�s pour le calcul d'arrondi de l'espace pixel
	int	yArrondiPixel;

	int	cxVirtuel;				// dimensions de l'espace virtuel (�ventuellement n�gatif)
	int	cyVirtuel;
	int	xArrondiVirtuel;	// utilis�s pour le calcul d'arrondi de l'espace virtuel
	int	yArrondiVirtuel;

	POINT	 ptOrigineVirtuel;	// d�calage de SPC_VIRTUEL par rapport � SPC_PIXEL (sp�cifi� en pixels)
	} SPACE;
typedef SPACE * PSPACE;

//---------------------------------------------------------------------------
static __inline BOOL bSignesDifferents (LONG Value1, LONG Value2)
  {
  return ((Value1 ^ Value2) < 0);
  }

//---------------------------------------------------------------------------
static void SetCoefficientsArrondi (PSPACE pSpace)
  {
  pSpace->xArrondiPixel = pSpace->cxPixel / 2;
  pSpace->xArrondiVirtuel = pSpace->cxVirtuel / 2;
  if (bSignesDifferents (pSpace->cxPixel, pSpace->cxVirtuel))
    {
    pSpace->xArrondiPixel = -pSpace->xArrondiPixel;
    pSpace->xArrondiVirtuel = -pSpace->xArrondiVirtuel;
    }

  pSpace->yArrondiPixel = pSpace->cyPixel / 2;
  pSpace->yArrondiVirtuel = pSpace->cyVirtuel / 2;
  if (bSignesDifferents (pSpace->cyPixel, pSpace->cyVirtuel))
    {
    pSpace->yArrondiPixel = -pSpace->yArrondiPixel;
    pSpace->yArrondiVirtuel = -pSpace->yArrondiVirtuel;
    }
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//			Fonctions export�es
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
HSPACE hSpcCree (LONG cxPixel, LONG cyPixel, LONG cxVirtuel, LONG cyVirtuel)
  {
  PSPACE pSpace = (PSPACE)pMemAlloue (sizeof (SPACE));

  pSpace->cxPixel = cxPixel;
  pSpace->cyPixel = cyPixel;

  pSpace->cxVirtuel = cxVirtuel;
  pSpace->cyVirtuel = cyVirtuel;

  pSpace->ptOrigineVirtuel.x = 0;
  pSpace->ptOrigineVirtuel.y = 0;

  SetCoefficientsArrondi (pSpace);

  return (HSPACE)pSpace;
  }

//---------------------------------------------------------------------------
void SpcFerme (HSPACE * phspace)
  {
  MemLibere ((PVOID *)phspace);
  }

//---------------------------------------------------------------------------
// $$ Modifications globale de la taille de l'espace Pixel ou virtuel (par exemple en cas de changement de r�solution �cran)
void SpcModifie (HSPACE hspace, LONG cxPixel, LONG cyPixel, LONG cxVirtuel, LONG cyVirtuel)
	{
  PSPACE pSpace = (PSPACE)hspace;
  pSpace->cxPixel = cxPixel;
  pSpace->cyPixel = cyPixel;

  pSpace->cxVirtuel = cxVirtuel;
  pSpace->cyVirtuel = cyVirtuel;

  pSpace->ptOrigineVirtuel.x = 0;
  pSpace->ptOrigineVirtuel.y = 0;

  SetCoefficientsArrondi (pSpace);
	}

//---------------------------------------------------------------------------
// modifications de la taille de l'espace Pixel
void SpcModifieEspacePixel (HSPACE hspace, LONG dx, LONG dy)
  {
  PSPACE pSpace = (PSPACE)hspace;

	pSpace->cxPixel = dx;
	pSpace->cyPixel = dy;
  SetCoefficientsArrondi (pSpace);
  }

//---------------------------------------------------------------------------
// modifications de la taille de l'espace virtuel
void SpcModifieEspaceVirtuel (HSPACE hspace, LONG dx, LONG dy)
  {
  PSPACE pSpace = (PSPACE)hspace;

	pSpace->cxVirtuel = dx;
	pSpace->cyVirtuel = dy;
  SetCoefficientsArrondi (pSpace);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void SpcSetXOrigineVirtuel (HSPACE hspace, LONG xOrigine)
  {
  PSPACE pSpace = (PSPACE)hspace;

  pSpace->ptOrigineVirtuel.x = lSpcCvtDxEnPixel (hspace, xOrigine);
  }

//---------------------------------------------------------------------------
void SpcSetYOrigineVirtuel (HSPACE hspace, LONG yOrigine)
  {
  PSPACE pSpace = (PSPACE)hspace;

  pSpace->ptOrigineVirtuel.y = lSpcCvtDyEnPixel (hspace, yOrigine);
  }

//---------------------------------------------------------------------------
// R�cup�re les coordonn�es de l'origine de l'espace virtuel (en coordonn�es virtuelles)
void SpcGetPointOrigineVirtuel (HSPACE hspace, POINT * pptOrigineVirtuel)
  {
  PSPACE pSpace = (PSPACE)hspace;

  (*pptOrigineVirtuel) = pSpace->ptOrigineVirtuel;
  SpcCvtPointsEnVirtuel (hspace, 1, pptOrigineVirtuel);
  }

//---------------------------------------------------------------------------
// R�cup�re les coordonn�es de l'origine de l'espace virtuel (en coordonn�es virtuelles)
void SpcGetOrigineVirtuel (HSPACE hspace, LONG * pxOrigineVirtuel, LONG * pyOrigineVirtuel)
  {
  PSPACE pSpace = (PSPACE)hspace;
	POINT	ptOrigineVirtuel = pSpace->ptOrigineVirtuel;

  SpcCvtPointsEnVirtuel (hspace, 1, &ptOrigineVirtuel);

	* pxOrigineVirtuel = ptOrigineVirtuel.x;
  * pyOrigineVirtuel = ptOrigineVirtuel.y;
  }

//---------------------------------------------------------------------------
// D�place l'origine de l'espace virtuel de dx (sp�cifi�s en virtuel)
// renvoie l'ancienne position
LONG lSpcDeplaceXOrigineVirtuel (HSPACE hspace, LONG dx)
  {
  PSPACE pSpace = (PSPACE)hspace;
  LONG  xOldOrigineVirtuel = lSpcCvtDxEnVirtuel (hspace, pSpace->ptOrigineVirtuel.x);

  pSpace->ptOrigineVirtuel.x = lSpcCvtDxEnPixel (hspace, -dx) + pSpace->ptOrigineVirtuel.x;

  return xOldOrigineVirtuel;
  }

//---------------------------------------------------------------------------
// D�place l'origine de l'espace virtuel de dy (sp�cifi�s en virtuel)
// renvoie l'ancienne position
LONG lSpcDeplaceYOrigineVirtuel (HSPACE hspace, LONG dy)
  {
  PSPACE pSpace = (PSPACE)hspace;
  LONG  yOldOrigineVirtuel = lSpcCvtDyEnVirtuel (hspace, pSpace->ptOrigineVirtuel.y);

  pSpace->ptOrigineVirtuel.y = lSpcCvtDyEnPixel (hspace, -dy) + pSpace->ptOrigineVirtuel.y;

  return yOldOrigineVirtuel;
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
LONG lSpcCvtDxEnPixel (HSPACE hspace, LONG dx)
  {
  PSPACE pSpace = (PSPACE)hspace;
  LONG    sizeCoefficientArrondi = (dx < 0) ? -pSpace->xArrondiVirtuel : pSpace->xArrondiVirtuel;

  return ((dx * pSpace->cxPixel) + sizeCoefficientArrondi) / pSpace->cxVirtuel;
  }

//---------------------------------------------------------------------------
LONG lSpcCvtDyEnPixel (HSPACE hspace, LONG dy)
  {
  PSPACE pSpace = (PSPACE)hspace;
  LONG    sizeCoefficientArrondi = (dy < 0) ? -pSpace->xArrondiVirtuel : pSpace->xArrondiVirtuel;

  return ((dy * pSpace->cyPixel) + sizeCoefficientArrondi) / pSpace->cyVirtuel;
  }

//---------------------------------------------------------------------------
void Spc_CvtSizeEnPixel (HSPACE hspace, LONG * pdx, LONG * pdy)
  {
  *pdx = lSpcCvtDxEnPixel (hspace, *pdx);
  *pdy = lSpcCvtDyEnPixel (hspace, *pdy);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
LONG lSpcCvtDxEnVirtuel (HSPACE hspace, LONG dx)
  {
  PSPACE pSpace = (PSPACE)hspace;
  LONG    sizeCoefficientArrondi = (dx < 0) ? -pSpace->xArrondiPixel : pSpace->xArrondiPixel;
  
  return ((dx * pSpace->cxVirtuel) + sizeCoefficientArrondi) / pSpace->cxPixel;
  }

//---------------------------------------------------------------------------
LONG lSpcCvtDyEnVirtuel (HSPACE hspace, LONG dy)
  {
  PSPACE pSpace = (PSPACE)hspace;
  LONG    sizeCoefficientArrondi = (dy < 0) ? -pSpace->yArrondiPixel: pSpace->yArrondiPixel;

  return ((dy * pSpace->cyVirtuel) + sizeCoefficientArrondi) / pSpace->cyPixel;
  }

//---------------------------------------------------------------------------
void Spc_CvtSizeEnVirtuel (HSPACE hspace, LONG * pdx, LONG * pdy)
  {
  *pdx = lSpcCvtDxEnVirtuel (hspace, *pdx);
  *pdy = lSpcCvtDyEnVirtuel (hspace, *pdy);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
LONG lSpcCvtXEnPixel (HSPACE hspace, LONG x)
  {
  PSPACE  pSpace = (PSPACE)hspace;

  return lSpcCvtDxEnPixel (hspace, x) - pSpace->ptOrigineVirtuel.x;
  }

//---------------------------------------------------------------------------
LONG lSpcCvtYEnPixel (HSPACE hspace, LONG y)
  {
  PSPACE pSpace = (PSPACE)hspace;

  return lSpcCvtDyEnPixel (hspace, y) - pSpace->ptOrigineVirtuel.y;
  }

//---------------------------------------------------------------------------
void SpcCvtPointsEnPixel (HSPACE hspace, DWORD wNbPt, POINT *pPoint)
  {
  DWORD w;

  for (w = 0; w < wNbPt; w++)
    {
    pPoint[w].x = lSpcCvtXEnPixel (hspace, pPoint[w].x);
    pPoint[w].y = lSpcCvtYEnPixel (hspace, pPoint[w].y);
    }
  }

//---------------------------------------------------------------------------
void Spc_CvtPtEnPixel (HSPACE hspace, LONG * px, LONG * py)
  {
  *px = lSpcCvtXEnPixel (hspace, *px);
  *py = lSpcCvtYEnPixel (hspace, *py);
  }

//---------------------------------------------------------------------------
LONG lSpcCvtXEnVirtuel (HSPACE hspace, LONG x)
  {
  PSPACE  pSpace = (PSPACE)hspace;

  return lSpcCvtDxEnVirtuel (hspace, x + pSpace->ptOrigineVirtuel.x);
  }

//---------------------------------------------------------------------------
LONG lSpcCvtYEnVirtuel (HSPACE hspace, LONG y)
  {
  PSPACE pSpace = (PSPACE)hspace;

  return lSpcCvtDyEnVirtuel (hspace, y + pSpace->ptOrigineVirtuel.y);
  }

//---------------------------------------------------------------------------
void SpcCvtPointsEnVirtuel (HSPACE hspace, DWORD wNbPt, POINT *pPoint)
  {
  DWORD w;

  for (w = 0; w < wNbPt; w++)
    {
    pPoint[w].x = lSpcCvtXEnVirtuel (hspace, pPoint[w].x);
    pPoint[w].y = lSpcCvtYEnVirtuel (hspace, pPoint[w].y);
    }
  }

//---------------------------------------------------------------------------
void Spc_CvtPtEnVirtuel (HSPACE hspace, LONG * px, LONG * py)
  {
  *px = lSpcCvtXEnVirtuel (hspace, *px);
  *py = lSpcCvtYEnVirtuel (hspace, *py);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Cadrage (alignement) par double conversion d'une coordonn�e sp�cifi�e dans l'espace Virtuel
LONG lSpcVirtuelAjusteX (HSPACE hspace, LONG x)
  {
  LONG  NewX = lSpcCvtXEnPixel (hspace, x);

  return lSpcCvtXEnVirtuel (hspace, NewX);
  }

//---------------------------------------------------------------------------
// Cadrage (alignement) par double conversion d'une coordonn�e sp�cifi�e dans l'espace Virtuel
LONG lSpcVirtuelAjusteY (HSPACE hspace, LONG y)
  {
  LONG  NewY = lSpcCvtYEnPixel (hspace, y);

  return lSpcCvtYEnVirtuel (hspace, NewY);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Additionne deux d�placements et cadre le r�sultat
// valeurs sp�cifi�es dans l'espace Virtuel
LONG lSpcVirtuelAjouteDx (HSPACE hspace, LONG dx1, LONG dx2)
  {
  LONG  NewDx1 = lSpcCvtDxEnPixel (hspace, dx1);
  LONG  NewDx2 = lSpcCvtDxEnPixel (hspace, dx2);

  return lSpcCvtDxEnVirtuel (hspace, NewDx1 + NewDx2);
  }

//---------------------------------------------------------------------------
// Additionne deux d�placements et cadre le r�sultat
// valeurs sp�cifi�es dans l'espace Virtuel
LONG lSpcVirtuelAjouteDy (HSPACE hspace, LONG dy1, LONG dy2)
  {
  LONG  NewDy1 = lSpcCvtDyEnPixel (hspace, dy1);
  LONG  NewDy2 = lSpcCvtDyEnPixel (hspace, dy2);

  return lSpcCvtDyEnVirtuel (hspace, NewDy1 + NewDy2);
  }

//---------------------------------------------------------------------------
// Additionne un d�placement � une position (renvoie une position cadr�e) 
// valeurs sp�cifi�es dans l'espace Virtuel
LONG lSpcVirtuelAjouteX (HSPACE hspace, LONG x, LONG dx)
  {
  LONG  NewX = lSpcCvtXEnPixel  (hspace, x);
  LONG  NewDx = lSpcCvtDxEnPixel (hspace, dx);

  return lSpcCvtXEnVirtuel  (hspace, NewX + NewDx);;
  }

//---------------------------------------------------------------------------
// Additionne un d�placement � une position (renvoie une position cadr�e) 
// valeurs sp�cifi�es dans l'espace Virtuel
LONG lSpcVirtuelAjouteY (HSPACE hspace, LONG y, LONG dy)
  {
  LONG  NewY = lSpcCvtYEnPixel  (hspace, y);
  LONG  NewDy = lSpcCvtDyEnPixel (hspace, dy);

  return lSpcCvtYEnVirtuel  (hspace, NewY + NewDy);
  }

//---------------------------- fin Space.c -------------------------------------

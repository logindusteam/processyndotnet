// PathMan.h : gestion des r�pertoires d'une application
// Paths : r�pertoires instanci�s par les appelant de ce module. 
//				 en g�n�ral Appli.c contient le r�pertoire de l'exe et DocMan le ou les
//				 r�pertoires documents g�r�s par l'application.	
//
// Ces fonctions ajoutent ou enl�vent juste les \ ou . � la jonction des composants.


// -------------------------------------------------------------------------------
// Ajoute � pszRepDest, adresse d'un r�pertoire, un nom (ayant �ventuellement une extension).
// r�sultat : pszRepDest de type R�pertoire\Nom
// renvoie pszRepDest ou NULL si Err (pointeurs nulls ou pas assez de place pour faire le traitement)
PSTR pszRepAjouteNom (PSTR pszRepDest, int nTailleDest, PCSTR pszNom);

// -------------------------------------------------------------------------------
// Ajoute � pszNomDest, adresse d'un nom de fichier (�ventuellement apr�s un r�pertoire), une extension.
// r�sultat : pszNomDest de type R�pertoire\Nom.ext
// renvoie pszPathDest ou NULL si Err (pointeurs nulls ou pas assez de place pour faire le traitement)
PSTR pszNomAjouteExt (PSTR pszNomDest, int nTailleDest, PCSTR pszExt);

// -------------------------------------------------------------------------------
// Copie dans pszBuf un pathname de type Path\Nom
// renvoie pszBuf ou NULL si Err
PSTR pszCreePathName (PSTR pszBuf, int nTailleBuf, PCSTR pszPath,  PCSTR pszNom);

// -------------------------------------------------------------------------------
// Copie dans pszBuf un pathname de type Path\NomSeul.Extension
// renvoie pszBuf ou NULL si Err
PSTR pszCreePathNameExt (PSTR pszBuf, int nTailleBuf, PCSTR pszPath,  PCSTR pszNomSeul, PCSTR pszExtension);

// -------------------------------------------------------------------------------
// Copie dans pszBuf un nom de fichier suivi de son extension de type PathName.Extension
// renvoie pszBuf ou NULL si Err
PSTR pszCreeNameExt (PSTR pszBuf, int nTailleBuf, PCSTR pszNom, PCSTR pszExtension);

// -------------------------------------------------------------------------------
// Supprime le Path d'un nom de fichier 
PSTR pszSupprimePath (PSTR pszNom);

// -------------------------------------------------------------------------------
// Supprime le Nom et l'extension d'un pathname : donc garde le path.
PSTR pszSupprimeNomExt (PSTR pszPathName);

// -------------------------------------------------------------------------------
// Supprime l'extension d'un nom de fichier 
PSTR pszSupprimeExt (PSTR pszNom);

// -------------------------------------------------------------------------------
// Supprime le Path et l'extension d'un nom de fichier 
PSTR pszSupprimePathEtExt (PSTR pszNom);


// -------------------------------------------------------------------------------
// Teste la pr�sence de l'extension pszExtension dans le nom de fichier pszNom
// renvoie TRUE si trouv�
BOOL bExtensionPresente (PCSTR pszNom, PCSTR pszExtension);

// -------------------------------------------------------------------------------
// r�cup�re le nom d'unit� ou le nom UNC commen�ant le pathName
// renvoie pszPath ou NULL si Err
PSTR pSupprimeToutSaufUnite (PSTR pszPath);

// -------------------------------------------------------------------------------
// Modifie si possible un pathname pour le rendre relatif � un path absolu
PSTR pszPathNameRelatif (PSTR pszPathNameAModifier, PCSTR pszPathAbsolu);

// -------------------------------------------------------------------------------
// Modifie si possible un pathname relatif pour le rendre absolu
// Teste si le pathname est complet (comporte une unite ou un serveur UNC) => si oui rien sinon insere le path de reference
PSTR pszPathNameAbsolu (PSTR pszPathNameAModifier, PCSTR pszPathRef);


// --------------------------------------------------------------------------
#define  BMP_FILE_FORMAT_UNKNOWN  0
//#define  BMP_FILE_FORMAT_PM12     1
//#define  BMP_FILE_FORMAT_PM11     2
#define  BMP_FILE_FORMAT_WINDOW   3
#define  BMP_FILE_FORMAT_PACKED   4
// --------------------------------------------------------------------------
BOOL    BmpSize          (HBITMAP hbm, PLONG plCx, PLONG plCy);

// Capture d'une r�gion de l'�cran dans un bitmap
// renvoie NULL si une erreur s'est produite pendant la capture
HBITMAP BmpFromRclScreen (PRECT prc);

HBITMAP BmpFromBmp       (HBITMAP hbmSrc);
//
void    BmpDestroy       (HBITMAP hbm);
//
BOOL    BmpToRclSpace    (HDC hdc, HBITMAP hbm, RECT * prcl, RECT * prclRefresh);
BOOL    BmpToSpace       (HDC hdc, HBITMAP hbm, POINT * pptl, RECT * prclRefresh);
//
BOOL    BmpToFile        (HBITMAP hbm, PSTR pszFile, USHORT usFileFormat);

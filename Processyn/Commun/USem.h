#ifndef USEM_H
#define USEM_H

// USem.h
// impl�mentation de s�maphores Events Win32 fonctionnant � la OS/2
//
// Un s�maphore est un objet syst�me qui DOIT �tre cr�� avant d'�tre utilis� puis ferm�.
//
// Un s�maphore ouvert est soit PRIS (= Non Signaled Win32 = Set OS/2)
//	soit LIBRE (= Signaled Win32 = Clear OS/2)
//
// A la cr�ation, on distinguera plusieurs types de s�maphores :
// - s�maphores Auto Pris : ils sont automatiquement marqu� PRIS apr�s une attente de lib�ration satisfaite.
// - s�maphores Attente seule : une attente de lib�ration satisfaite les laisse LIBRE.

// ces deux types de s�maphores peuvent �tre nomm�s (IPC) ou pas. 
// Il n'y a donc qu'un seule type de fonction d'attente de lib�ration pour plusieurs types de s�maphores.

//------------------------------ Types ------------------------------------
// Handle de s�maphore
DECLARE_HANDLE (HSEM);
typedef HSEM * PHSEM;

// erreur time out
#define	SEM_TIMEOUT WAIT_TIMEOUT

//------------------------------ Fonctions -----------------------------------

// cr�e un semaphore local au process. Renvoie son Handle ou NULL si err.
// A la cr�ation, son �tat sera PRIS si bPris est vrai et LIBRE sinon.
// L'�tat du s�maphore sera PRIS apr�s une attente de synchronisation
HSEM hSemCree
	(
	BOOL bPris   // sp�cifie l'�tat initial du s�maphore
	);

//-------------------------------------------------------------------------
// Cr�e un semaphore local au process et renvoie sa valeur (NULL si err): 
// L'�tat du s�maphore ne sera pas chang� par dwSemAttenteLibre.
// A la cr�ation, son �tat sera PRIS si bPris est vrai et LIBRE sinon.
HSEM hSemCreeAttenteSeule
	(
	BOOL bPris   // sp�cifie l'�tat initial du s�maphore
	);

//-------------------------------------------------------------------------
// bSemFerme ferme l'objet syst�me dont le handle est sp�cifi� et renvoie TRUE si tout est Ok.
// Dans le cas d'un s�maphore nomm�, la fermeture n'intervient qu'� la fermeture du dernier
// handle de s�maphore cr�� avec le m�me nom.
BOOL bSemFerme
	(
	PHSEM phsem // semaphore handle
	);

//-------------------------------------------------------------------------
// SemNommeCree cr�e un s�maphore visible entre plusieurs process, recopie
// son handle et renvoie 0. Si un s�maphore de m�me nom existe d�ja, il est ouvert, son 
// handle est recopi� et la valeur ERROR_ALREADY_EXISTS est renvoy�e.
// Un s�maphore Auto Pris initialement PRIS est cr��. $$ param�trer
DWORD SemNommeCree
	(
	PHSEM phsem,     // pointer to variable for semaphore handle
	PSTR pszSemName   // pointer to semaphore name
	);

//-------------------------------------------------------------------------
// hsem valide : marque le s�maphore LIBRE et renvoie TRUE ; renvoie FALSE sinon
BOOL bSemLibere
	(
	HSEM hsem // semaphore handle
	);

//-------------------------------------------------------------------------
// hsem valide : marque le s�maphore PRIS et renvoie TRUE ; renvoie FALSE sinon
BOOL bSemPrends
	(
	HSEM hsem // semaphore handle
	);

//-------------------------------------------------------------------------
// Attend d'au plus dwTimeOut millisecondes que le s�maphore soit marqu� libre.
// S�maphore attente seule : son �tat n'est pas chang�.
// S�maphore auto pris : son �tat est marqu� PRIS.
// S�maphore acc�s exclusif : son �tat est marqu� PRIS.
// renvoie 
//	0 si Ok, 
//	SEM_TIMEOUT si l'attente a exc�d� dwTimeOut
//	WAIT_ABANDONED si s�maphore ferm� avant d'�tre lib�r�
//	WAIT_FAILED		si autre erreur (faire un GetLastError)

DWORD dwSemAttenteLibre
	(
	HSEM hsem,        // semaphore handle
	DWORD dwTimeOut   // time out       
	);

//-------------------------------------------------------------------------
// Prends le s�maphore puis appelle dwSemAttenteLibre
DWORD DosSemSetWait
	(
	HSEM hsem,        // semaphore handle
	DWORD dwTimeOut   // time-out
	);

#endif




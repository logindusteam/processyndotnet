//-----------------------------------------------------------------------------
// DevMan.h Gestion Dessin dans un device de dessin (hwnd, hdc)
// Utiliser #include "G_Objets.h" pour les param�tres couleurs, styles etc...
//-----------------------------------------------------------------------------

// --------------------------------------- P�riph�riques de sorties
#define c_DEVICE_WINDOW         1
#define c_DEVICE_PRINTER        2
#define c_DEVICE_METAFILE       3
#define c_DEVICE_BITMAP_CLIENT  4
#define c_DEVICE_BITMAP_FRAME   5
#define c_DEVICE_BITMAP_SCREEN  6

// Types Handles
#define c_DEVICE_HDL_METAFILE   1
#define c_DEVICE_HDL_BITMAP     2

// Types affichage Handles
#define c_DEVICE_ADAPT_NORMAL   1
#define c_DEVICE_ADAPT_ADJUST   2

// Types document
#define c_DEVICE_DOC_TYPE_UNCHANGED  0
#define c_DEVICE_DOC_TYPE_PM12       1
#define c_DEVICE_DOC_TYPE_PM11       2
#define c_DEVICE_DOC_TYPE_WIN        3
#define c_DEVICE_DOC_TYPE_PKD        4
#define c_DEVICE_DOC_TYPE_DEFAULT    c_DEVICE_DOC_TYPE_PM12

// -------------------------------------------------------------------------------------
// Encapsulation selection de brush et de pen: selection par indice de tableau g_objets
// ------------------------------------------------------------------------------------
HBRUSH hCreeBrossePleine (G_COULEUR nCouleur);
HBRUSH hCreeBrosseStyle (G_STYLE_REMPLISSAGE nStyleRemplissage, G_COULEUR nCouleur);
HPEN hCreeStylo (G_STYLE_LIGNE nStyleLigne, G_COULEUR nCouleur);

// Valeur RGB d'une couleur logique
COLORREF d_RGB(G_COULEUR nCouleur);
// Valeur de la couleur logique la plus proche du RGB donn�
G_COULEUR d_CouleurProcheDuRGB(COLORREF rgbSource);
// Nom standard associ� � une couleur
PCSTR d_NomCouleur(G_COULEUR nCouleur);

// Parcours par affinit� de couleur (familles de teintes du plus clair au plus obscur)
G_COULEUR d_AffiniteCouleurPremiere();
G_COULEUR d_CouleurAffinite(int nAffiniteCouleur);
G_COULEUR d_AffiniteCouleurDerniere();

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DECLARE_HANDLE (HDEV);

// ------------------------------------------------------------------------
// Ouverture du device
HDEV	d_open (void);

// Fermeture du device
void d_close (HDEV * phdev);

// ------------------------------------------------------------------------
// Parametrage du device
// Attention le hdc r�cup�r� sur hwndDessin est suppos� rester valide
// juqu'� d_fini
// Pour cela utiliser CS_OWNDC dans la classe du hwnd
void d_set_param (HDEV hdev, HWND hwndDessin);

// ------------------------------------------------------------------------
// Sp�cifie l'origine du DC (utilis� pour l'alignement des brosses sous W95)
// ------------------------------------------------------------------------
void d_set_origine_dc (HDEV hdev, POINT ptOrigineClient);


// ------------------------------------------------------------------------
// Demande le hdc du device
// ------------------------------------------------------------------------
HDC d_get_hdc (HDEV hdev);

// ------------------------------------------------------------------------
// Demande le hwnd du device
// ------------------------------------------------------------------------
HWND d_get_hwnd (HDEV hdev);

// ------------------------------------------------------------------------
// Initialisation du device
void d_init (HDEV hdev);

// Finalisation du device
void d_fini (HDEV hdev);

// ------------------------------------------------------------------------
// Nom de document
// ------------------------------------------------------------------------
void d_set_document_name (HDEV hdev, char *pszName, DWORD wDocType);
DWORD d_get_document_name (HDEV hdev, char *pszName);

// ------------------------------------------------------------------------
// Choix du p�riph�rique de sortie
// ------------------------------------------------------------------------
void *d_select_device     (HDEV hdev, DWORD Device);
void  d_get_device_limits (HDEV hdev, LONG *x0, LONG *y0, LONG *cx, LONG *cy);
void  d_delete_handle     (HDEV hdev, void *hdl, DWORD wHdlType);
void  d_draw_handle       (HDEV hdev, void *hdl, DWORD wHdlType, DWORD wAdaptHdl, LONG x1, LONG y1, LONG x2, LONG y2);
void  d_save_handle       (HDEV hdev, void *hdl, DWORD wHdlType);

// ------------------------------------------------------------------------
// Fonctions de dessin
void d_line            (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_rectangle       (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_rectangle  (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void d_circle          (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_circle     (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
//1/4 de disque
void d_4_arc           (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_4_arc      (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
//1/4 de disque + rectangle
void d_4_arc_rect      (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_4_arc_rect (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
// 1/2 cercle horizontal
void d_h_2_arc         (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_h_2_arc    (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
// 1/2 cercle vertical
void d_v_2_arc         (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_v_2_arc    (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
// vannes
void d_h_vanne         (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_h_vanne    (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void d_v_vanne         (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_v_vanne    (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
// triangles rectangles
void d_tri_rect        (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_tri_rect   (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
// fl�ches
void d_v_arrow         (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_v_arrow    (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void d_h_arrow         (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void d_fill_h_arrow    (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
// texte
void d_h_write         (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG LargeurCar, LONG TaillePolice,
												DWORD Police, DWORD PoliceStyle, char *pszString);
void d_v_write         (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG LargeurCar, LONG TaillePolice, 
												DWORD Police, DWORD PoliceStyle, char *pszString);
// ligne bris�e
void d_poly_line       (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, DWORD nb_points, void *td_points);
void d_fill_poly_line  (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, DWORD nb_points, POINT *td_points);
// Animations
void d_trend           (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2);
void d_bargraf         (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2);
void d_entryfield      (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD Police, DWORD PoliceStyle, char *pszString);
void d_static_text     (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD Police, DWORD PoliceStyle, char *pszString);
void d_logic_entry     (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2);

// Dessin fond de fen�tre, grille, r�cup�re taille
void d_fill_background (HDEV hdev, G_COULEUR nCouleur);
void d_grille (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE style, LONG pas_x, LONG pas_y);
void d_get_taille_background (HDEV hdev, LONG * dxFond, LONG * dyFond);

// ------------------------------------------------------------------------
// Effectue un ::InvalidateRect sur un rectangle
void d_InvalidateRect (HDEV hdev, const RECT * pRectInvalide);

// ------------------------------------------------------------------------
// Encombrement d'un Texte
// ------------------------------------------------------------------------
void d_get_box_static_text (HDEV hdev, LONG *x1, LONG *y1, LONG *x2, LONG *y2, DWORD Police, DWORD PoliceStyle, char *pszString);
void d_get_box_h_write (HDEV hdev, LONG x1, LONG y1, LONG *x2, LONG *y2, DWORD Police, DWORD PoliceStyle, DWORD TaillePolice, PCSTR pszString);
void d_get_box_v_write (HDEV hdev, LONG x1, LONG y1, LONG *x2, LONG *y2, DWORD Police, DWORD PoliceStyle, DWORD TaillePolice, PCSTR pszString);
//
void d_get_size_car_ref_plus (HDEV hdev, LONG *cx, LONG *cy, LONG *offsetY, DWORD Police, DWORD PoliceStyle);
void d_get_size_car_box (HDEV hdev, LONG *x1, LONG *y1, LONG *x2, LONG *y2, DWORD Police, DWORD PoliceStyle);

// ------------------------------------------------------------------------
// Couleurs clignottantes
// ------------------------------------------------------------------------
BOOL d_color_stable (HDEV hdev, G_COULEUR nCouleur);

// ------------------------------------------------------------------------
// Dibs
// ------------------------------------------------------------------------
// Chargement  / lib�ration de bitmaps
HANDLE d_load_bmp (HDEV hdev, PCSTR pszFichierBmp);
void  d_delete_bmp (HDEV hdev, HANDLE *phBmp);
// dessin d'un bitmap
void  d_draw_bmp   (HDEV hdev, HANDLE hBmp, BOOL bStretch, LONG x1, LONG y1, LONG x2, LONG y2);

// ------------------------------------------------------------------------

//-------------------------------------------------------------------------
// BdMarqGr.h (li� � Bdgr)
// Gestion des marques (s�lection) des �l�ments graphiques de la base de donn�e Processyn
//-------------------------------------------------------------------------
#ifndef BDMARQGR_H
#define BDMARQGR_H

// caract�ristiques des �l�ments graphiques de s�lection
#define c_ACTION_SELECTION             G_ACTION_RECTANGLE
#define c_COULEUR_SELECTION            G_DARKGRAY
#define c_STYLE_FILL_SELECTION         G_STYLE_REMPLISSAGE_PLEIN
#define c_STYLE_LINE_SELECTION_NORMAL  G_STYLE_LIGNE_POINTS
#define c_STYLE_LINE_SELECTION_ANIM    G_STYLE_LIGNE_TIRETS //G_STYLE_LIGNE_CONTINUE

// caract�ristiques des �l�ments graphiques des poign�es de d�formation
#define c_ACTION_DEFORMATION          G_ACTION_RECTANGLE_PLEIN
#define c_COULEUR_DEFORMATION         G_DARKGRAY
#define c_STYLE_FILL_DEFORMATION      G_STYLE_REMPLISSAGE_PLEIN
#define c_STYLE_LINE_DEFORMATION      G_STYLE_LIGNE_POINTS

// -------------------------------------------------------------------------
DWORD bdgr_nombre_marques_page (HBDGR hbdgr);
DWORD bdgr_lire_element_marque (HBDGR hbdgr, DWORD Marque);
// -------------------------------------------------------------------------
DWORD bdgr_lire_marque_element (HBDGR hbdgr, DWORD Element);

void MajGeoMarquesGR (DWORD TypeMaj, DWORD PosEnr, DWORD NbEnr);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//						Marquer des Elements
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// -------------------------------------------------------------------------
// Aucun �l�ment marqu� (modifie l'objet Marques et l'affichage)
void bdgr_demarquer_page (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
void bdgr_marquer_page (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
void bdgr_marquer_page_elts_animes (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
BOOL bdgr_marquer_page_elt_hors_page (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
void bdgr_demarquer_page_elts_animes (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
void bdgr_marquer_page_elts_non_animes (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
void bdgr_demarquer_page_elts_non_animes (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
BOOL bdgr_marquer_element (HBDGR hbdgr, DWORD Element, DWORD ModeTravail);
// -------------------------------------------------------------------------
BOOL bdgr_demarquer_element (HBDGR hbdgr, DWORD Element, DWORD ModeTravail);
// -------------------------------------------------------------------------
// Ajouter la marque correspondant � l'objet le plus proche du point sp�cifi�
BOOL bdgr_marquer_point (HBDGR hbdgr, LONG x, LONG y, DWORD ModeTravail);
// -------------------------------------------------------------------------
BOOL bdgr_demarquer_point (HBDGR hbdgr, LONG x, LONG y, DWORD ModeTravail);
// -------------------------------------------------------------------------
void bdgr_marquer_rectangle (HBDGR hbdgr, LONG x1, LONG y1, LONG x2, LONG y2, DWORD ModeTravail);
// -------------------------------------------------------------------------
void bdgr_demarquer_rectangle (HBDGR hbdgr, LONG x1, LONG y1, LONG x2, LONG y2, DWORD ModeTravail);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Travail sur les Marques : Attention les elts Anim�s ne sont pas groupables
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// -------------------------------------------------------------------------
// Groupe les �l�ments marqu�s (Attention, �l�ments anim�s non groupables)
void bdgr_grouper_marques (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
// Fait passer les �l�ments marqu�s au premier plan ou � l'arri�re plan (Attention, �l�ments anim�s non groupables)
void bdgr_change_profondeur_marques (HBDGR hbdgr, BOOL bPremierPlan, DWORD ModeTravail);
// -------------------------------------------------------------------------
BOOL bdgr_degrouper_marques (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
void bdgr_boite_marques (HBDGR hbdgr, LONG *x1, LONG *y1, LONG *x2, LONG *y2);
// -------------------------------------------------------------------------
void bdgr_couleur_marques (HBDGR hbdgr, DWORD ModeTravail, G_COULEUR Couleur);
// -------------------------------------------------------------------------
void bdgr_style_ligne_marques (HBDGR hbdgr, DWORD ModeTravail, G_STYLE_LIGNE Style);
// -------------------------------------------------------------------------
void bdgr_style_remplissage_marques (HBDGR hbdgr, DWORD ModeTravail, G_STYLE_REMPLISSAGE Style);
// -------------------------------------------------------------------------
// Modification du style de Caract�re des �l�ments marqu�s
void bdgr_style_texte_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD Police, DWORD StylePolice, DWORD TaillePolicePixel);
// -------------------------------------------------------------------------
BOOL bdgr_ligne_anim_marques (HBDGR hbdgr, DWORD n_ligne);
// -------------------------------------------------------------------------
DWORD bdgr_param_animation_marques (HBDGR hbdgr, ANIMATION CtxtAnimation);
// -------------------------------------------------------------------------
DWORD bdgr_change_identifiant_marques (HBDGR hbdgr);
// -------------------------------------------------------------------------
BOOL bdgr_supprimer_marques (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
DWORD bdgr_dupliquer_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD PageSource, BOOL AvecAnimation);
// -------------------------------------------------------------------------
void bdgr_translater_marques (HBDGR hbdgr, DWORD ModeTravail, LONG Dx, LONG Dy);
// -------------------------------------------------------------------------
void bdgr_change_remplissage_marques (HBDGR hbdgr, DWORD ModeTravail);
// -------------------------------------------------------------------------
// Aligne les �l�ments marqu�s (avec dessin)
void bdgr_align_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD wTypeAlign,
                         LONG iGauche, LONG iBas, LONG iDroit, LONG iHaut);
// -------------------------------------------------------------------------
// r�partis l'espacement entre les �l�ments graphiques marqu�s (avec dessin)
DWORD bdgr_espace_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD wTypeEspace,
                          DWORD wTailleMax, LONG iEspacement);
// -------------------------------------------------------------------------
// Transforme les �l�ments graphiques marqu�s en leurs sym�triques (avec dessin)
void bdgr_symetrie_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD Axes, LONG xAxe, LONG yAxe);
// -------------------------------------------------------------------------
void bdgr_tourner_marques (HBDGR hbdgr, DWORD ModeTravail, LONG xCentre, LONG yCentre, DWORD NbQuartTour);
// -------------------------------------------------------------------------
void bdgr_debut_deformation_marques (HBDGR hbdgr);
// -------------------------------------------------------------------------
void bdgr_deformer_marques (HBDGR hbdgr, DWORD ModeTravail, LONG xRef, LONG yRef,
														LONG cxNew, LONG cxOld, LONG cyNew, LONG cyOld);
// -------------------------------------------------------------------------
// Voire si le point sp�cifi� est sur une marque ($$??? une poign�e?)
DWORD bdgr_pointer_marques (HBDGR hbdgr, LONG x, LONG y, LONG *CoinDeformation);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Dessin
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// ---------------------------------------------------------------------------
// Dessin des attributs de s�lection de tous les �l�ments marqu�s dans la page
void bdgr_dessiner_selection_page (HBDGR hbdgr);
// -------------------------------------------------------------------------
// Invalidate les attributs de s�lection de tous les �l�ments marqu�s dans la page
void bdgr_gommer_selection_page (HBDGR hbdgr);
// -------------------------------------------------------------------------
// Dessiner les poign�es de d�formation des �l�ments s�lectionn�s
void bdgr_dessiner_deformation_page (HBDGR hbdgr);
// -------------------------------------------------------------------------
// Invalidate des poign�es de d�formation des �l�ments s�lectionn�s
void bdgr_gommer_deformation_page (HBDGR hbdgr);
// -------------------------------------------------------------------------
void bdgr_dessiner_elements_marques (HBDGR hbdgr);
// -------------------------------------------------------------------------
void bdgr_gommer_elements_marques (HBDGR hbdgr);



// -------------------------------------------------------------------------
// Mise � jour de l'objet Marques
void MajMarquesGR (DWORD TypeMaj, DWORD wReferencePage, DWORD PosEnr, DWORD NbEnr);

#endif

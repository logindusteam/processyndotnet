//----------------------------------------------------------------------------
// BdPageGr.h (extrait de Bdgr)
// Gestion des propri�t�s d'une page de synoptique
//----------------------------------------------------------------------------

// Renvoie TRUE si la page NPage existe (* est mis � jour avec la r�f�rence de page)
BOOL ExistePageGR (DWORD NPage, DWORD * pReferencePage);

// R�cup�re l'adresse des propri�t�s de la page HBDGR ou NULL si introuvable
PPAGE_GR ppageHBDGR (HBDGR hbdgr);

// R�cup�re l'adresse des propri�t�s de la page dwIdPage ou NULL si elle n'existe pas
PPAGE_GR pGetPageGR (DWORD dwIdPage);

// R�cup�re l'adresse des propri�t�s de la page HBDGR ou NULL si introuvable
PPAGE_GR ppageHBDGR (HBDGR hbdgr);

// r�cup�re le num�ro de page du HBDGR
DWORD dwNPageGR (HBDGR hbdgr);
DWORD	bdgr_lire_page (HBDGR hbdgr);

BOOL	bdgr_existe_page (DWORD Page);
BOOL	bdgr_page_visible(DWORD Page);

// -------------------------------------------------------------------------
void bdgr_titre_page (HBDGR hbdgr, char *pszTitre);
// -------------------------------------------------------------------------
BOOL bdgr_lire_titre_page (HBDGR hbdgr, char *pszTitre);
// -------------------------------------------------------------------------
BOOL bdgr_lire_titre_page_direct (DWORD NPage, char *pszTitre);
// -------------------------------------------------------------------------
// Modifie la couleur de fond de la page courante
void bdgr_couleur_fond_page (HBDGR hbdgr, G_COULEUR Couleur);
// -------------------------------------------------------------------------
G_COULEUR bdgr_lire_couleur_fond_page (HBDGR hbdgr);
// -------------------------------------------------------------------------
void bdgr_styles_page (DWORD NPage, BOOL bFenModale, BOOL bFenDialog, DWORD mode_aff, DWORD facteur_zoom, DWORD mode_deform,
                       BOOL sizeborder, BOOL titre, char *texte_titre,
                       BOOL un_sysmenu, BOOL un_minimize, BOOL un_maximize,
                       BOOL scroll_bar_h, BOOL scroll_bar_v,
                       DWORD grille_x, DWORD grille_y);
// -------------------------------------------------------------------------
void bdgr_lire_styles_page (HBDGR hbdgr, BOOL * pbFenModale, BOOL * pbFenDialog, DWORD *mode_aff, DWORD *facteur_zoom, DWORD *mode_deform,
                            BOOL *sizeborder, BOOL *titre, char *texte_titre,
                            BOOL *un_sysmenu, BOOL *un_minimize, BOOL *un_maximize,
                            BOOL *scroll_bar_h, BOOL *scroll_bar_v,
                            DWORD *grille_x, DWORD *grille_y);
// -------------------------------------------------------------------------
// Renvoie TRUE si la page est modale, FALSE sinon
BOOL bPageGrModale (HBDGR hbdgr);
// Renvoie TRUE si la page est une boite de dialogue, FALSE sinon
BOOL bPageGrDialog (HBDGR hbdgr);
// Renvoie TRUE si le fond de page est ajust� (stretch�) � la zone client de la page
BOOL bPageStretchBmpFondZoneClient (HBDGR hbdgr);

// -------------------------------------------------------------------------
void bdgr_coord_page (HBDGR hbdgr, int xFen, int yFen, int cxFen, int cyFen);
// -------------------------------------------------------------------------
void bdgr_lire_coord_page  (HBDGR hbdgr, int *xFen, int *yFen, int *cxFen, int *cyFen);
// -------------------------------------------------------------------------
void bdgr_lire_coord_page_direct  (DWORD NPage, int *xFen, int *yFen, int *cxFen, int *cyFen);
// -------------------------------------------------------------------------
void bdgr_origine_page (HBDGR hbdgr, int xOrigine, int yOrigine);
// -------------------------------------------------------------------------
void bdgr_lire_origine_page  (HBDGR hbdgr, PLONG xOrigine, PLONG yOrigine);
// -------------------------------------------------------------------------
// Sp�cifications Pages
void	bdgr_bmp_fond_page           (HBDGR hbdgr, BOOL bSetClearFlag, PCSTR pszBmpFile);
BOOL	bdgr_lire_nom_bmp_fond_page      (HBDGR hbdgr, char *pszBmpFile);
void	bdgr_charger_bmp_fond_page   (HBDGR hbdgr);
void	bdgr_supprimer_bmp_fond_page (HBDGR hbdgr);
BOOL	bdgr_dessiner_bmp_fond_page  (HBDGR hbdgr, BOOL bStretchZoneClient);

// -------------------------------------------------------------------------
// Nombres
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
DWORD bdgr_nombre_pages (void);
// -------------------------------------------------------------------------
DWORD bdgr_nombre_elements_page (HBDGR hbdgr);

// ------------------------------------------------------------------
// Met � jour les premiers �l�ments des pages compte tenu de l'op�ration qui vient d'�tre faite
void MajPagesGR (MAJ_BDGR TypeMaj, DWORD Ref, DWORD PosEnr, DWORD NbEnr);
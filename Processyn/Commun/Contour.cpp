#include "stdafx.h"
#include "Contour.h"

// Un contour est décomposé pour chaque objet en une succession de points.
// Le nombre de points de l'objet est fourni avec wCount = 0;
// ------------------------------------------------------------------------
DWORD d_around_line (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 2)
    {
    wRequest = 2;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 2:
      aptl [1].x = x2;
      aptl [1].y = y2;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (2);
  }

// ------------------------------------------------------------------------
DWORD d_around_rectangle (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 5)
    {
    wRequest = 5;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 5:
      aptl [4].x = x1;
      aptl [4].y = y1;
    case 4:
      aptl [3].x = x1;
      aptl [3].y = y2;
    case 3:
      aptl [2].x = x2;
      aptl [2].y = y2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = y1;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (5);
  }

// ------------------------------------------------------------------------
DWORD d_around_circle (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 9)
    {
    wRequest = 9;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 9:
      aptl [8].x = (3 * x1 + x2) / 4;
      aptl [8].y = y1;
    case 8:
      aptl [7].x = x1;
      aptl [7].y = (3 * y1 + y2) / 4;
    case 7:
      aptl [6].x = x1;
      aptl [6].y = (y1 + 3 * y2) / 4;
    case 6:
      aptl [5].x = (3 * x1 + x2) / 4;
      aptl [5].y = y2;
    case 5:
      aptl [4].x = (x1 + 3 * x2) / 4;
      aptl [4].y = y2;
    case 4:
      aptl [3].x = x2;
      aptl [3].y = (y1 + 3 * y2) / 4;
    case 3:
      aptl [2].x = x2;
      aptl [2].y = (3 * y1 + y2) / 4;
    case 2:
      aptl [1].x = (x1 + 3 * x2) / 4;
      aptl [1].y = y1;
    case 1:
      aptl [0].x = (3 * x1 + x2) / 4;
      aptl [0].y = y1;
    }
  return (9);
  }

// ------------------------------------------------------------------------
DWORD d_around_4_arc (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 4)
    {
    wRequest = 4;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 4:
      aptl [3].x = x1;
      aptl [3].y = y2;
    case 3:
      aptl [2].x = (x1 + x2) / 2;
      aptl [2].y = y2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = (y1 + y2) / 2;
    case 1:
      aptl [0].x = x2;
      aptl [0].y = y1;
    }
  return (4);
  }
// ------------------------------------------------------------------------
DWORD d_around_fill_4_arc (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 5)
    {
    wRequest = 5;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 5:
      aptl [4].x = x2;
      aptl [4].y = y1;
    case 4:
      aptl [3].x = x1;
      aptl [3].y = y2;
    case 3:
      aptl [2].x = (x1 + x2) / 2;
      aptl [2].y = y2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = (y1 + y2) / 2;
    case 1:
      aptl [0].x = x2;
      aptl [0].y = y1;
    }
  return (5);
  }

// ------------------------------------------------------------------------
DWORD d_around_4_arc_rect (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 6)
    {
    wRequest = 6;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 6:
      aptl [5].x = x1;
      aptl [5].y = y1;
    case 5:
      aptl [4].x = x1;
      aptl [4].y = y2;
    case 4:
      aptl [3].x = (x1 + x2) / 2;
      aptl [3].y = y2;
    case 3:
      aptl [2].x = x2;
      aptl [2].y = (y1 + y2) / 2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = y1;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (6);
  }

// ------------------------------------------------------------------------
DWORD d_around_h_2_arc (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 6)
    {
    wRequest = 6;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 6:
      aptl [5].x = x1;
      aptl [5].y = y1;
    case 5:
      aptl [4].x = x1;
      aptl [4].y = (y1 + y2) / 2;
    case 4:
      aptl [3].x = (3 * x1 + x2) / 4;
      aptl [3].y = y2;
    case 3:
      aptl [2].x = (x1 + 3 * x2) / 4;
      aptl [2].y = y2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = (y1 + y2) / 2;
    case 1:
      aptl [0].x = x2;
      aptl [0].y = y1;
    }
  return (6);
  }
// ------------------------------------------------------------------------
DWORD d_around_fill_h_2_arc (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 7)
    {
    wRequest = 7;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 7:
      aptl [6].x = x1;
      aptl [6].y = y1;
    case 6:
      aptl [5].x = x1;
      aptl [5].y = (y1 + y2) / 2;
    case 5:
      aptl [4].x = (3 * x1 + x2) / 4;
      aptl [4].y = y2;
    case 4:
      aptl [3].x = (x1 + 3 * x2) / 4;
      aptl [3].y = y2;
    case 3:
      aptl [2].x = x2;
      aptl [2].y = (y1 + y2) / 2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = y1;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (7);
  }
// ------------------------------------------------------------------------
DWORD d_around_v_2_arc (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 6)
    {
    wRequest = 6;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 6:
      aptl [5].x = x1;
      aptl [5].y = y2;
    case 5:
      aptl [4].x = (x1 + x2) / 2;
      aptl [4].y = y2;
    case 4:
      aptl [3].x = x2;
      aptl [3].y = (y1 + 3 * y2) / 4;
    case 3:
      aptl [2].x = x2;
      aptl [2].y = (3 * y1 + y2) / 4;
    case 2:
      aptl [1].x = (x1 + x2) / 2;
      aptl [1].y = y1;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (6);
  }
// ------------------------------------------------------------------------
DWORD d_around_fill_v_2_arc (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 7)
    {
    wRequest = 7;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 7:
      aptl [6].x = x1;
      aptl [6].y = y2;
    case 6:
      aptl [5].x = (x1 + x2) / 2;
      aptl [5].y = y2;
    case 5:
      aptl [4].x = x2;
      aptl [4].y = (y1 + 3 * y2) / 4;
    case 4:
      aptl [3].x = x2;
      aptl [3].y = (3 * y1 + y2) / 4;
    case 3:
      aptl [2].x = (x1 + x2) / 2;
      aptl [2].y = y1;
    case 2:
      aptl [1].x = x1;
      aptl [1].y = y1;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y2;
    }
  return (7);
  }

// ------------------------------------------------------------------------
DWORD d_around_tri_rect (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 4)
    {
    wRequest = 4;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 4:
      aptl [3].x = x1;
      aptl [3].y = y1;
    case 3:
      aptl [2].x = x1;
      aptl [2].y = y2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = y1;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (4);
  }

// ------------------------------------------------------------------------
DWORD d_around_h_tri_iso (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 4)
    {
    wRequest = 4;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 4:
      aptl [3].x = x1;
      aptl [3].y = y1;
    case 3:
      aptl [2].x = x1;
      aptl [2].y = y2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = (y1 + y2) / 2;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (4);
  }

// ------------------------------------------------------------------------
DWORD d_around_v_tri_iso (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 4)
    {
    wRequest = 4;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 4:
      aptl [3].x = x1;
      aptl [3].y = y1;
    case 3:
      aptl [2].x = (x1 + x2) / 2;
      aptl [2].y = y2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = y1;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (4);
  }
// ------------------------------------------------------------------------
DWORD d_around_h_vanne (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 5)
    {
    wRequest = 5;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 5:
      aptl [4].x = x1;
      aptl [4].y = y1;
    case 4:
      aptl [3].x = x1;
      aptl [3].y = y2;
    case 3:
      aptl [2].x = x2;
      aptl [2].y = y1;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = y2;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (5);
  }
// ------------------------------------------------------------------------
DWORD d_around_v_vanne (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 5)
    {
    wRequest = 5;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 5:
      aptl [4].x = x1;
      aptl [4].y = y1;
    case 4:
      aptl [3].x = x2;
      aptl [3].y = y1;
    case 3:
      aptl [2].x = x1;
      aptl [2].y = y2;
    case 2:
      aptl [1].x = x2;
      aptl [1].y = y2;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (5);
  }
// ------------------------------------------------------------------------
DWORD d_around_trend (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl)
  {
  DWORD    wRequest;

  if (wCount > 8)
    {
    wRequest = 8;
    }
  else
    {
    wRequest = wCount;
    }

  switch (wRequest)
    {
    case 8:
      aptl [7].x = x2;
      aptl [7].y = y2;
    case 7:
      aptl [6].x = x2;
      aptl [6].y = y1;
    case 6:
      aptl [5].x = x1;
      aptl [5].y = y1;
    case 5:
      aptl [4].x = x1;
      aptl [4].y = y2;
    case 4:
      aptl [3].x = x2;
      aptl [3].y = y2;
    case 3:
      aptl [2].x = (x1 + x2) / 2;
      aptl [2].y = (3 * y1 + y2) / 4;
    case 2:
      aptl [1].x = (3 * x1 + x2) / 4;
      aptl [1].y = (y1 + 3 * y2) / 4;
    case 1:
      aptl [0].x = x1;
      aptl [0].y = y1;
    }
  return (8);
  }

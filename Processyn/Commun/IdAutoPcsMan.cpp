/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : IdAutoPcsMan.cpp                                                   |
 |   Auteur  : AC					        	|
 |   Date    : 3/11/99					        	|
 |   Version : 5.1						|
 +----------------------------------------------------------------------*/
// Calcul automatique sur 64 bits d'un id d'element de groupe
// Les groupes sont d�clar�s en type �num�r�s dans l'interface et
// sont en g�n�ral relatifs � un bloc alors que les elements sont relatif 
// � un enregistrement


#include "stdafx.h"
#include "stdio.h"
#include "UStr.h"
#include "mem.h"
#include "IdAutoPcsMan.h"
#include "Verif.h"
VerifInit;

//#define DEBUG_DOUBLONS_ID


// D�tection de d�claration d'Id en doublon (sur les 100 000 premiers Identifiants)
#ifdef DEBUG_DOUBLONS_ID
BOOL static IdSetEnDoublon (BOOL bReinitialise, __int64 & i64Id)
	{
	// D�clare un tableau statique d'allocation des identifiants
	BOOL bDoublonDetecte =FALSE;
	const __int64 MaxId = 100001;
	static BOOL aI64Id[MaxId];

	// Lib�re tous les identifiants (si demand�)
	if (bReinitialise)
		{
		for (__int64 NId = 0;  NId<MaxId; NId++)
			{
			aI64Id[NId]=FALSE;
			}
		}
	else
		{
		BOOL bValeurIdHorsBorneTestDoublon =FALSE;
		BOOL bEnDoublon = FALSE;
		//Id d�ja utilis� ?
		do 
			{
			// Id dans le tableau ?
			bValeurIdHorsBorneTestDoublon =(i64Id>=MaxId) || (i64Id<0);
			if (bValeurIdHorsBorneTestDoublon)
				{
				// BOF $$
				bEnDoublon = FALSE;
				bDoublonDetecte = TRUE;
				}
			else
				{
				// Oui => note si la valeur est d�ja utilis�e
				bEnDoublon = aI64Id[i64Id];
				}
			if (bEnDoublon)
				{
				// Oui => affecte l'identifiant avec + 10000 (totalement arbitraire)
				i64Id+=10000;
				bDoublonDetecte = TRUE;
				}
			} while (bEnDoublon);

		// Id modifi� ?
		if (bDoublonDetecte)
			{
			// Oui => v�rifie la validit� du nouvel Id (mettre un point d'arr�t)
			bValeurIdHorsBorneTestDoublon =(i64Id>=MaxId) || (i64Id<0);
			}

		VerifWarning (!bValeurIdHorsBorneTestDoublon);

		// Note que l'Id est utilis�
		aI64Id[i64Id]=TRUE;
		}
	return bDoublonDetecte;
	}
#endif

//-------------------------------------------------------------------------
PSTR GetIdAutoPcs (ID_GROUPE_AUTO_IDENTIFICATION eIdGroupe, PSTR pStrId)
	{
	PID_AUTO_PCS pIdAutoPcs;

	StrSetNull (pStrId);
	if (!existe_repere (b_id_auto_pcs))
		{
		cree_bloc (b_id_auto_pcs, sizeof(ID_AUTO_PCS), 1);
		pIdAutoPcs = (PID_AUTO_PCS)pointe_enr (szVERIFSource, __LINE__, b_id_auto_pcs, 1);
		pIdAutoPcs->eIdGroupe = eIdGroupe;
		pIdAutoPcs->i64IdAutoProchain = 1; 
		}
	else
		{
		BOOL bTrouve = FALSE;
		DWORD dwIndex = 1;
		while ((dwIndex <= nb_enregistrements (szVERIFSource, __LINE__, b_id_auto_pcs) && !bTrouve))
			{
			pIdAutoPcs = (PID_AUTO_PCS)pointe_enr (szVERIFSource, __LINE__, b_id_auto_pcs, dwIndex);
			bTrouve = (pIdAutoPcs->eIdGroupe == eIdGroupe);
			dwIndex++;
			}
		if (!bTrouve)
			{
			insere_enr (szVERIFSource, __LINE__, 1, b_id_auto_pcs, 1);
			pIdAutoPcs = (PID_AUTO_PCS)pointe_enr (szVERIFSource, __LINE__, b_id_auto_pcs, 1);
			pIdAutoPcs->eIdGroupe = eIdGroupe;
			pIdAutoPcs->i64IdAutoProchain = 1; 
			}
		}

	StrPrintFormat (pStrId, "%.20I64u", pIdAutoPcs->i64IdAutoProchain);
	pIdAutoPcs->i64IdAutoProchain++; 

	return pStrId;
	}

//-------------------------------------------------------------------------
// Positionne le prochain identifiant � la valeur pass�e + 1 si inf�rieur
BOOL SetIdAutoPcs (ID_GROUPE_AUTO_IDENTIFICATION eIdGroupe, PSTR pStrId)
	{
	PID_AUTO_PCS pIdAutoPcs;
	BOOL bOk = FALSE;
	__int64 i64Value = 0;
	DWORD dwIndex = 0;

	bOk = StrToI64 (&i64Value, pStrId);
	if (bOk)
		{
		if (!existe_repere (b_id_auto_pcs))
			{
			cree_bloc (b_id_auto_pcs, sizeof(ID_AUTO_PCS), 1);
			pIdAutoPcs = (PID_AUTO_PCS)pointe_enr (szVERIFSource, __LINE__, b_id_auto_pcs, 1);
			pIdAutoPcs->eIdGroupe = eIdGroupe;
			pIdAutoPcs->i64IdAutoProchain = 1; 
			dwIndex = 1;
			// Si autoris�, aide � la d�tection de doublons lors de l'import des identifiants
#ifdef DEBUG_DOUBLONS_ID
			IdSetEnDoublon(TRUE,i64Value);
#endif
			}
		else
			{
			BOOL bTrouve = FALSE;
			dwIndex = 1;
			while ((dwIndex <= nb_enregistrements (szVERIFSource, __LINE__, b_id_auto_pcs) && !bTrouve))
				{
				pIdAutoPcs = (PID_AUTO_PCS)pointe_enr (szVERIFSource, __LINE__, b_id_auto_pcs, dwIndex);
				bTrouve = (pIdAutoPcs->eIdGroupe == eIdGroupe);
				dwIndex++;
				}
			if (bTrouve)
				{
				dwIndex--;
				}
			else
				{
				insere_enr (szVERIFSource, __LINE__, 1, b_id_auto_pcs, 1);
				pIdAutoPcs = (PID_AUTO_PCS)pointe_enr (szVERIFSource, __LINE__, b_id_auto_pcs, 1);
				pIdAutoPcs->eIdGroupe = eIdGroupe;
				pIdAutoPcs->i64IdAutoProchain = 1; 
				dwIndex = 1;
				}
			}
		pIdAutoPcs = (PID_AUTO_PCS)pointe_enr (szVERIFSource, __LINE__, b_id_auto_pcs, dwIndex);
		// Si autoris�, aide � la d�tection de doublons lors de l'import des identifiants
#ifdef DEBUG_DOUBLONS_ID
		IdSetEnDoublon(FALSE,i64Value);
		StrPrintFormat (pStrId, "%.20I64u", i64Value);
#endif
		if (pIdAutoPcs->i64IdAutoProchain <= i64Value)
			{
			pIdAutoPcs->i64IdAutoProchain = i64Value + 1;
			}
		}
	return (bOk);
	}



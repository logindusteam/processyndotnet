//---------------------------------------------------------------
// UExcept.h
//
// Gestion des exceptions utilisateurs
// Version WIN32 11/6/97 JS
//
//---------------------------------------------------------------

// CODES D'EXCEPTION
#define EXCEPTION_VERIF		100
#define EXCEPTION_MEMMAN  101
#define EXCEPTION_BLOCMAN	102
#define EXCEPTION_BLKMAN	103
#define EXCEPTION_SIGNALE	104
#define EXCEPTION_ERR_COMPILE	105


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//					GESTION DES EXCEPTIONS
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Un thread ou programme sera structur� ainsi
// __try 
//	{
//	... corps du thread ou programme
//	}	
// __except (nSignaleExceptionUser(GetExceptionInformation()))
//	{
//	ExitProcess((UINT)-1);// handler des exceptions choisies par le filtre
//	}

// Une gestion locale d'erreur sera structur� ainsi
// __try 
//	{
//	... instructions dont on veut intercepter l'exception
//	}	
// __except (nRecupereErreur(GetExceptionInformation(), &Erreur))
//	{
//	GereErreur(Erreur);
//	}

//---------------------------------------------------------------
// Signale une exception User
// renvoie l'ordre de traiter le handler ou de continuer
int nSignaleExceptionUser (EXCEPTION_POINTERS * pExp);

//---------------------------------------------------------------
// Envoie une exception avec un code d'erreur Processyn
void ExcepErreur
	(
	DWORD dwExcepCode,		// exemple : EXCEPTION_ERR_COMPILE
	DWORD dwErreurCode		// Num�ro d'erreur dans la nomenclature Processyn
	);

//---------------------------------------------------------------
// Envoie une exception de type EXCEPTION_ERR_COMPILE
// avec son code d'erreur Processyn s'il est != 0
void ExcepErreurCompile
	(
	DWORD dwErreurCode		// Num�ro d'erreur dans la nomenclature Processyn
	);


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//				GENERATION D'EXCEPTIONS
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#define EXCEP_NB_CAR_MAX_EXPLICATION 1024

//---------------------------------------------------------------
// Envoie une exception avec pour seule explication son nom
void ExcepBrute 
	(
	DWORD dwExcepCode	// exemple : EXCEPTION_MEMMAN
	);

//---------------------------------------------------------------
// Envoie une exception avec une ligne d'explication
void ExcepContexte 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte		// exemple : "adresse invalide"
	);

//---------------------------------------------------------------
// Envoie une exception continuable avec une ligne d'explication
void ExcepWarnContexte 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_VERIF
	PCSTR	pszContexte		// exemple : "Warning dans 'toto.c' ligne 43"
	);

//---------------------------------------------------------------
// Envoie une exception avec une ligne d'explication � formater avec un param�tre
void ExcepFormatContexte1 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte,	// exemple : "insertion en %lu invalide" ou "queue %s non trouv�e"
	DWORD dwP1					// param�tre � inclure dans le formatage du contexte
	);
//---------------------------------------------------------------
// Envoie une exception avec une ligne d'explication � formater avec deux param�tre
void ExcepFormatContexte2 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte,	// exemple : "insertion enr %lu invalide dans bloc %lu impossible"
	DWORD dwP1,					// param�tre � inclure dans le formatage du contexte en position 1
	DWORD dwP2					// param�tre � inclure dans le formatage du contexte en position 2
	);

//---------------------------------------------------------------
// Envoie une exception avec une ligne d'explication � formater avec trois param�tre
void ExcepFormatContexte3 
	(
	DWORD dwExcepCode,	// exemple : EXCEPTION_MEMMAN
	PCSTR	pszContexte,	// exemple : "insertion enr %lu de %lu enr invalide dans bloc %lu impossible"
	DWORD dwP1,					// param�tre � inclure dans le formatage du contexte en position 1
	DWORD dwP2,					// param�tre � inclure dans le formatage du contexte en position 2
	DWORD dwP3					// param�tre � inclure dans le formatage du contexte en position 3
	);

// AnalyseCar.h: interface for the CAnalyseCar class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ANALYSECAR_H__CDF9D9C2_0C31_11D2_B438_444553540000__INCLUDED_)
#define AFX_ANALYSECAR_H__CDF9D9C2_0C31_11D2_B438_444553540000__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

// Un tableau est cr�� ayant un statut pour chaque caract�re
// Le premier octet de statut repr�sente le car converti en majuscule lorsque c'est possible
// Les autres bits servent � l'anlyse d'ULs
#define MASQUE_CAR_MAJUSCULE 0xff
#define BIT_CAR_FIN_CHAINE 0x100
#define BIT_CAR_SEPARATEUR 0x200
#define BIT_CAR_COMMENTAIRE 0x400
#define BIT_CAR_NOM 0x800
#define BIT_CAR_SIGNE 0x1000
#define BIT_CAR_DEBUT_MESSAGE 0x2000
#define BIT_CAR_NUMERIQUE 0x4000
#define BIT_CAR_HEXADECIMAL 0x4000
#define BIT_CAR_OPERATEUR 0x10000
#define BIT_CAR_PREMIER_CAR_ID 0x20000

#define TAILLE_STATUT_CAR 256

class CAnalyseCar  
	{
	public:
		CAnalyseCar();
		virtual ~CAnalyseCar();
	protected:
		int m_naStatutCars [TAILLE_STATUT_CAR];
	public:
		int nStatutCar (char c) const {return m_naStatutCars [c];}
	};

#endif // !defined(AFX_ANALYSECAR_H__CDF9D9C2_0C31_11D2_B438_444553540000__INCLUDED_)

//-------------------------------------------------------------------------
// BdContGr.h (li� � Bdgr)
// Gestion des �l�ments graphiques controles de la base de donn�e Processyn
//-------------------------------------------------------------------------

// -------------------------------------------------------------------------
// Cr�ation d'une fen�tre controle child � l'image d'un �l�ment Gsys
HWND hwndCreeContGR (HWND hwndParent, HGSYS hGsys, HELEMENT hElmtGsys, DWORD id, BOOL bEnEdition);

// -------------------------------------------------------------------------
// Ajustement de la position d'une fen�tre controle child
BOOL bChangePosContGR (HWND hwndControle, HGSYS hGsys, POINT point0, POINT point1);

// -------------------------------------------------------------------------
// Ajustement de l'ID d'une fen�tre controle child
BOOL bChangeIdContGR (HWND hwndControle, DWORD dwNouvelID);





// lien dynamique Vers Applicom.dll Win32
// interface = APPLICOM.H              Version : 2.8.0              date :25/03/96

#include "stdafx.h"
#include "Verif.h"
#include "UStr.h"
#include "USignale.h"
#include "Appli.h"
#include "DLLMan.h"
#include "Applicom.h"
VerifInit;

// Table de lien � l'ex�cution avec la DLL Applicom
static LIEN_FONCTION aLiens [] =
	{
		{NULL, "initbus"},
		{NULL, "exitbus"},
		{NULL, "getword"},
		{NULL, "setword"},
		{NULL, "getpackbit"},
		{NULL, "setpackbit"},
		{NULL, "startcyc"},
		{NULL, "stopcyc"},
		{NULL, "actcyc"},
	};

typedef enum 
	{
	INITBUS,
	EXITBUS,
	GETWORD,
	SETWORD,
	GETPACKBIT,
	SETPACKBIT,
	STARTCYC,
	STOPCYC,
	ACTCYC
	} N_FN_APPLICOM;
#define NB_FN_APPLICOM (ACTCYC + 1)


static HINSTANCE hmodDLLApplicom = NULL;
static PCSTR pszNomDLLApplicom = "Applicom.dll";

//-----------------------------------------------------
// Lie l'application � la DLL Applicom (tableau local aLiens mis � jour)
BOOL bInitDLLApplicom (void)
	{
	BOOL bRes = bLieDLL(&hmodDLLApplicom, pszNomDLLApplicom, aLiens, NB_FN_APPLICOM);

	if (!bRes)
		{
		char szErr [350];

		StrPrintFormat (szErr, "Le fichier %s est invalide ou introuvable.\nCopiez en une version valide dans le r�pertoire %s", 
			pszNomDLLApplicom, Appli.szPathExe);
		SignaleWarnExit (szErr);
		}

	return bRes;
	}

//-----------------------------------------------------
// Libere la DLL Applicom (tableau local aLiens mis � jour)
BOOL bLibereDLLApplicom (void)
	{
	BOOL bRes = TRUE;

	// DLL charg�e ?
	if (hmodDLLApplicom)
		// oui => essaye de la d�charger
		bRes = bLibereLienDLL(&hmodDLLApplicom, aLiens, NB_FN_APPLICOM);
	if (!bRes)
		{
		char szErr [350];

		StrPrintFormat (szErr, "Probl�me � la fermeture de la DLL %s.", pszNomDLLApplicom);
		SignaleWarnExit (szErr);
		}
	return bRes;
	} // bLibereDLLApplicom

//-----------------------------------------------------
void WINAPI initbus (short int* ps)
	{
	typedef void  (WINAPI *PFN_INITBUS)(short int*);
	FARPROC pfn = aLiens[INITBUS].pfn;
	if (pfn)
		((PFN_INITBUS)pfn) (ps);
	}

void WINAPI exitbus(short int* ps)
	{
	typedef void (WINAPI *PFN_EXITBUS)(short int*);
	FARPROC pfn = aLiens[EXITBUS].pfn;
	if (pfn)
		((PFN_EXITBUS)pfn) (ps);
	}

void WINAPI getword(short int* ps1, short int* ps2, short int* ps3, short int* psBufMot, short int* ps5)
	{
	typedef void (WINAPI *PFN_GETWORD)(short int*,short int*,short int*,short int*,short int*);
	FARPROC pfn = aLiens[GETWORD].pfn;
	if (pfn)
		((PFN_GETWORD)pfn) (ps1, ps2, ps3, psBufMot, ps5);
	}

void WINAPI setword(short int* ps1,short int* ps2,short int* ps3,short int* psBufMot,short int* ps5)
	{
	FARPROC pfn = aLiens[SETWORD].pfn;
	typedef void (WINAPI *PFN_SETWORD)(short int*,short int*,short int*,short int*,short int*);
	if (pfn)
		((PFN_SETWORD)pfn) (ps1, ps2, ps3, psBufMot, ps5);
	}

void WINAPI getpackbit(short int* ps1,short int* ps2,short int* ps3,short int* psBufMot,short int* ps5)
	{
	FARPROC pfn = aLiens[GETPACKBIT].pfn;
	typedef void (WINAPI *PFN_GETPACKBIT)(short int*,short int*,short int*,short int*,short int*);
	if (pfn)
		((PFN_GETPACKBIT)pfn)	(ps1, ps2, ps3, psBufMot, ps5);
	}

void WINAPI setpackbit(short int* ps1,short int* ps2,short int* ps3,short int* psBufMot,short int* ps5)
	{
	FARPROC pfn = aLiens[SETPACKBIT].pfn;
	typedef void (WINAPI *PFN_SETPACKBIT)(short int*,short int*,short int*,short int*,short int*);
	if (pfn)
		((PFN_SETPACKBIT)pfn) (ps1, ps2, ps3, psBufMot, ps5);
	}

void WINAPI startcyc(short int* ps1,short int* ps2,short int* ps3)
	{
	FARPROC pfn = aLiens[STARTCYC].pfn;
	typedef void (WINAPI *PFN_STARTCYC)(short int*,short int*,short int*);
	if (pfn)
		((PFN_STARTCYC)pfn) (ps1, ps2, ps3);
	}

void WINAPI stopcyc(short int* ps1,short int* ps2,short int* ps3)
	{
	FARPROC pfn = aLiens[STOPCYC].pfn;
	typedef void (WINAPI *PFN_STOPCYC)(short int*,short int*,short int*);
	if (pfn)
		((PFN_STOPCYC)pfn) (ps1, ps2, ps3);
	}

void WINAPI actcyc(short int* ps1,short int* ps2,short int* ps3)
	{
	FARPROC pfn = aLiens[ACTCYC].pfn;
	typedef void (WINAPI *PFN_ACTCYC)(short int*,short int*,short int*);
	if (pfn)
		((PFN_ACTCYC)pfn) (ps1, ps2, ps3);
	}


//%%%%%%%%%%%%%%%%%% non utilis�s %%%%%%%%%%%%%%%%%%%%

/*
void WINAPI masterinfo(short int*,short int*,char*,short int*)
	{
	}

void WINAPI readpackbit(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI readpackibit(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI readpackqbit(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI readbyte(short int*,short int*,short int*,long int*,short int*,short int*)
	{
	}

void WINAPI readibyte(short int*,short int*,short int*,long int*,short int*,short int*)
	{
	}

void WINAPI readqbyte(short int*,short int*,short int*,long int*,short int*,short int*)
	{
	}

void WINAPI readpackbyte(short int*,short int*,short int*,long int*,char*,short int*)
	{
	}

void WINAPI readpackibyte(short int*,short int*,short int*,long int*,char*,short int*)
	{
	}

void WINAPI readpackqbyte(short int*,short int*,short int*,long int*,char*,short int*)
	{
	}

void WINAPI readword(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI readiword(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI readqword(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI readwordbcd(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI readdword(short int*,short int*,short int*,long*,long*,short int*)
	{
	}

void WINAPI readfword(short int*,short int*,short int*,long*,float*,short int*)
	{
	}

void WINAPI writepackbit(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writepackqbit(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writebyte(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writeqbyte(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writepackbyte(short int*,short int*,short int*,long*,char*,short int*)
	{
	}

void WINAPI writepackqbyte(short int*,short int*,short int*,long*,char*,short int*)
	{
	}

void WINAPI writeword(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writeqword(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writewordbcd(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writedword(short int*,short int*,short int*,long*,long*,short int*)
	{
	}

void WINAPI writefword(short int*,short int*,short int*,long*,float*,short int*)
	{
	}

void WINAPI readquickbit(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI readdiag(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI readeven(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI readtrace(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI readident(short int*,short int*,short int*,char*,short int*)
	{
	}

void WINAPI manual(short int*,short int*,short int*)
	{
	}

void WINAPI automatic(short int*,short int*,short int*)
	{
	}

void WINAPI AppConnect(short int*,short int*,short int*)
	{
	}

void WINAPI AppUnconnect(short int*,short int*,short int*)
	{
	}

void WINAPI statjbus(short int*,short int*,short int*)
	{
	}

void WINAPI iocounter(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI resetiocounter(short int*,short int*,short int*)
	{
	}

void WINAPI readdifbit(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifibit(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifqbit(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifbyte(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifibyte(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifqbyte(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifiword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifqword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdifdword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI readdiffword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI writedifpackbit(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writedifpackqbit(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writedifpackbyte(short int*,short int*,short int*,long*,char*,short int*)
	{
	}

void WINAPI writedifpackqbyte(short int*,short int*,short int*,long*,char*,short int*)
	{
	}

void WINAPI writedifword(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writedifqword(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writedifdword(short int*,short int*,short int*,long*,long*,short int*)
	{
	}

void WINAPI writediffword(short int*,short int*,short int*,long*,float*,short int*)
	{
	}

void WINAPI readdifquickbit(short int*,short int*,short int*)
	{
	}

void WINAPI transdif(short int*,short int*,short int*,void*,short int*)
	{
	}

void WINAPI transdifpack(short int*,short int*,short int*,void*,short int*)
	{
	}

void WINAPI testtransdif(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI createcyc(short int*,short int*,short int*,short int*,short int*,short int*,short int*,short int*,long*,short int*,short int*,short int*)
	{
	}

void WINAPI transcyc(short int*,short int*,short int*,short int*,void*,short int*)
	{
	}

void WINAPI transcycpack(short int*,short int*,short int*,short int*,void*,short int*)
	{
	}

void WINAPI getbit(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI getpackbyte(short int*,short int*,short int*,char*,short int*)
	{
	}

void WINAPI getdword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI getfword(short int*,short int*,short int*,float*,short int*)
	{
	}

void WINAPI setbit(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI setpackbyte(short int*,short int*,short int*,char*,short int*)
	{
	}

void WINAPI setdword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI setfword(short int*,short int*,short int*,float*,short int*)
	{
	}

void WINAPI getdispbit(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI getdispword(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI getdispdword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI getdispfword(short int*,short int*,short int*,float*,short int*)
	{
	}

void WINAPI setdispbit(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI setdispword(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI setdispdword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI setdispfword(short int*,short int*,short int*,float*,short int*)
	{
	}

void WINAPI incdispword(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI decdispword(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI incdispdword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI decdispdword(short int*,short int*,short int*,long*,short int*)
	{
	}

void WINAPI confdb(short int*,short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI getevent(short int*,short int*,short int*,short int*,long*,short int*,short int*,short int*)
	{
	}

void WINAPI setevent(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI transwordbit(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI transbitword(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI compword(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI invbit(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI unpackdate(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI unpacktime(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI bcdbin(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI binbcd(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI writemes(short int*,short int*,char*,short int*)
	{
	}

void WINAPI readmes(short int*,short int*,short int*,short int*,short int*,char*,short int*)
	{
	}

void WINAPI writereadmes(short int*,short int*,char*,short int*,short int*,short int*,short int*,char*,short int*)
	{
	}

void WINAPI writedifmes(short int*,short int*,char*,short int*)
	{
	}

void WINAPI readdifmes(short int*,short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI writereaddifmes(short int*,short int*,char*,short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI getmodem(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI setmodem(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI watchdog(short int*,short int*,short int*)
	{
	}

void WINAPI accesskey(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI txtute(short int*,short int*,short int*,short int*,char*,char*,short int*,short int*,short int*)
	{
	}

void WINAPI readdiagute(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI resetcounterute(short int*,short int*,short int*)
	{
	}

void WINAPI readiobitute(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI writeiobitute(short int*,short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI readtimerute(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI readmonostableute(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI readcounterute(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI writetimerute(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI writemonostableute(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI writecounterute(short int*,short int*,short int*,short int*,short int*)
	{
	}

void WINAPI readtimer(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writetimer(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI readcounter(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI writecounter(short int*,short int*,short int*,long*,short int*,short int*)
	{
	}

void WINAPI AppIniTime(short int*,short int*)
	{
	}

void WINAPI AppFmsGetOd(short int*,short int*,short int*,short int*,short int*,
                 short int*,char*,short int*,short int*)
	{
	}

void WINAPI AppFmsStatus(short int*,short int*,unsigned short int*,unsigned short int*,short int*)
	{
	}

void WINAPI AppGetWatchDog(short int*,unsigned short int*,unsigned short int*,short int*)
	{
	}

void WINAPI AppDestroyCyc(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI CycExecuted(short int*,short int*,short int*,short int*)
	{
	}

void WINAPI AppGetCycParam(short int*,short int*,long*,short int*)
	{
	}

*/
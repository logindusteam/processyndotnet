#ifndef   MEMMAN_H
  #define MEMMAN_H
//---------------------------------------------------------------------------
// MemMan.h
// Gestionaire de m�moire en buffers pour WIN32
// Une exception structur�e est g�n�r�e en cas d'impossibilit� d'ex�cution Cf UExcept.h
// Sous WIN32, l'acc�s en lecture ou �criture � l'adresse 0 provoque une exception
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// Alloue et renvoie l'adresse d'un bloc de uTaille octets
// Demande � l'utilisateur - si n�cessaire - de lib�rer de la place m�moire
// et fait monter une exception si la demande est refus�e.
// => ne renvoie NULL que si uTaille vaut 0
//---------------------------------------------------------------------------
PVOID pMemAlloue (UINT uTaille);

//---------------------------------------------------------------------------
// Idem pMemAlloue; la zone allou�e est initialis�e � 0
PVOID pMemAlloueInit0 (UINT uTaille);

//---------------------------------------------------------------------------
// Idem pMemAlloue; la zone allou�e est initialis�e en recopiant uTaille octets
// � partir de pSource
PVOID pMemAlloueInit (UINT uTaille, const PVOID pSource);

//---------------------------------------------------------------------------
// Lib�re la m�moire � l'adresse *ppBase puis met cette adresse � NULL
void MemLibere (PVOID * ppBase);

//---------------------------------------------------------------------------
// Renvoie la taille en octets d'un bloc allou�
UINT uMemTaille (PVOID pBase);

//---------------------------------------------------------------------------
// Met � jour *ppBase avec l'adresse d'un bloc de uNouvelleTaille octets
// contenant les donn�es initiales de pBase.
// *ppBase peut �tre NULL. 
// Si uNouvelleTaille vaut 0, l'ancien bloc est lib�r� et *ppBase mis � NULL.
// Demande � l'utilisateur - si n�cessaire - de lib�rer de la place m�moire
// et fait monter une exception si la demande est refus�e.
void MemRealloue (PVOID *ppBase, UINT uNouvelleTaille);

//---------------------------------------------------------------------------
// R�alloue et ins�re uTaille octets en *ppBase + wOffset (en octets)
void MemInsere	(PVOID * ppBase, UINT wOffset, UINT uTaille);

//---------------------------------------------------------------------------
// R�alloue et ajoute � la fin du buffer uTaille octets
void MemAjouteFin	(PVOID * ppBase, UINT uTaille);

//---------------------------------------------------------------------------
// R�alloue et ins�re au d�but du buffer uTaille octets
void MemInsereDebut	(PVOID * ppBase, UINT uTaille);

//---------------------------------------------------------------------------
//  Supprime uTaille octets en *ppBase + wOffset (en octets) et r�alloue
void MemEnleve (PVOID * ppBase, UINT wOffset, UINT uTaille);

//---------------------------------------------------------------------------
//  Supprime uTaille octets en *ppBase et r�alloue
void MemEnleveDebut (PVOID * ppBase, UINT wOffset, UINT uTaille);

//---------------------------------------------------------------------------
//  R�alloue Supprime les derniers uTaille octets de *ppBase 
void MemEnleveFin (PVOID * ppBase, UINT wOffset, UINT uTaille);

// Copie uTaille octets de pSource vers pDest
__inline void MemCopy (PVOID pDest, const void * pSource, UINT uTaille) {memcpy (pDest, pSource, uTaille);}

#pragma warning(disable : 4996)
// Copie au plus uTaille octets de pSource vers pDest
// S'arr�te apr�s la premi�re occurence de byValue
// renvoie NULL ou l'adresse de l'octet suivant byFin dans pDest
_inline PVOID pMemCopyToByte (PVOID pDest, const void * pSource, BYTE byFin, UINT uTaille){return (memccpy (pDest, pSource, byFin, uTaille));}

// Copie uTaille octets de pSource vers pDest
// Les donn�es seront transf�r�es m�me si pSource et pDest se recouvrent, 
_inline void MemMove (PVOID pDest, const void * pSource, UINT uTaille){memmove (pDest, pSource, uTaille);}

// Remplis uTaille octets sur pDest avec la valeur byRemplissage
_inline void MemSet (PVOID pDest, BYTE byRemplissage, UINT uTaille){memset (pDest, byRemplissage, uTaille);}

//
_inline int MemCompare (const void * pSource1, const void * pSource2, UINT uTaille){return (memcmp (pSource1, pSource2, uTaille));}
_inline int MemICompare (const void * pSource1, const void * pSource2, UINT uTaille){return (memicmp (pSource1, pSource2, uTaille));}
//
_inline const void * MemSearch (const void * pSource, BYTE bValue, UINT uTaille){return (memchr (pSource, bValue, uTaille));}


//---------------------------------------------------------------------------
// Arithm�tique de pointeurs utile
__inline PVOID pMemIOffset (PVOID pSource, int iOffset)
	{
	return ((PBYTE) pSource) + iOffset;
	}
__inline PVOID pMemUOffset (PVOID pSource, UINT uOffset)
	{
	return ((PBYTE) pSource) + uOffset;
	}
__inline PVOID pConstMemIOffset (const void * pSource, int iOffset)
	{
	return ((PBYTE) pSource) + iOffset;
	}
__inline PVOID pConstMemUOffset (const void * pSource, UINT uOffset)
	{
	return ((PBYTE) pSource) + uOffset;
	}
__inline UINT uMemOffset (const void * pOrigine, const void * pSource)
	{
	return ((UINT) (((PBYTE)pSource) - ((PBYTE)pOrigine)));
	}
__inline int iMemOffset (const void * pOrigine, const void * pSource)
	{
	return ((((PBYTE)pSource) - ((PBYTE)pOrigine)));
	}

//---------------------------------------------------------------------------

#endif //MEMMAN_H

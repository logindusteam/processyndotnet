/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : Interface de declaration des types de VDI 		|
 +----------------------------------------------------------------------*/

//----------------------------------------------------------------------------//
// Const
//----------------------------------------------------------------------------

#define b_geo_vdi                  58
//#define b_titre_paragraf_vdi       59
//#define b_spec_titre_paragraf_vdi  60
#define b_vdi_r_log                61
#define b_vdi_r_num                62
#define b_vdi_r_mes                66
#define b_vdi_r_num_alpha          96
#define b_vdi_r_log_tab            440
#define b_vdi_r_num_tab            441
#define b_vdi_r_mes_tab            442
#define b_vdi_r_num_alpha_tab      443
#define b_vdi_e_log                119
#define b_vdi_e_num                120
#define b_vdi_e_mes                127
#define b_vdi_courbe_temps         121
#define b_vdi_courbe_cmd           86
#define b_vdi_courbe_fic       300
#define b_vdi_r_log_4etats         573
#define b_vdi_r_num_dep            575
#define b_vdi_r_mes_nv1            579
#define b_vdi_r_mes_tab_nv1        580
#define b_vdi_r_num_alpha_nv1      581
#define b_vdi_r_num_alpha_tab_nv1  582
#define b_vdi_r_log_4etats_tab     583
#define b_vdi_r_num_dep_tab        584
#define b_vdi_r_mes_nv2            808
#define b_vdi_r_mes_tab_nv2        809
#define b_vdi_e_log_nv1            810
#define b_vdi_control_radio        813
#define b_vdi_control_static       814
#define b_vdi_control_liste        815
#define b_vdi_control_combobox     816

#define bx_entrees_vi              843
#define bx_anm_fenetre_vi          838

#define bx_vdi_nom_page            849
#define bx_fenetre_vi              837
#define bx_element_vi              840
#define bx_variable_vi             841
#define bx_vdi_courbe_temps        122
#define bx_vdi_courbe_cmd           95
#define bx_vdi_courbe_archive      299
#define bx_vdi_dlg_combo_liste     830

// ------------ blocs des entrees
#define bx_entrees_vi                843
#define bx_anm_e_vi                  105
#define bx_anm_e_log_vi              845
#define bx_anm_e_num_vi              846
#define bx_anm_e_mes_vi              847
#define bx_anm_e_control_log         831
#define bx_anm_e_liste               832
#define bx_anm_e_combobox            848
#define bx_anm_e_radio							   4

// ------------ blocs des sorties
#define bx_services_speciaux_vi      842
#define bx_fenetre_vi                837
#define bx_page_vi                   839
#define bx_element_vi                840
#define bx_variable_vi               841
#define bx_vdi_courbe_temps          122
#define bx_vdi_courbe_cmd            95
#define bx_vdi_courbe_archive        299

#define bx_anm_fenetre_vi            838
#define bx_anm_r_log_vi              63
#define bx_anm_r_num_vi              64
#define bx_anm_r_mes_vi              65
#define bx_anm_r_num_alpha_vi        97
#define bx_anm_r_log_4etats_vi       574
#define bx_anm_r_num_dep_vi          576
#define bx_anm_courbe_vi             129
#define bx_anm_courbe_archive_vi     94

#define bx_anm_liste_elts_cli        812

// ----------------- Mode fenetre
#define FEN_CLIP    0
#define FEN_ZOOM    1
#define FEN_ZOOMP   2

// ----------------- Type Animation Logique
#define STYLE_RIEN        0
#define STYLE_COULEUR     1
#define STYLE_REMPLISSAGE 2
#define STYLE_LIGNE       3

// ----------------- Dimension Fen�tre
#define DIM_NORMALE 0
#define DIM_ICONE   1
#define DIM_MAX     2

// ----------------- Pointeurs sur bx_services_speciaux_vi
#define  PTR_SRV_SPEC_VI_ACK           1
#define  PTR_SRV_SPEC_VI_PAGE          2

//----------------- Direction des Bar-Graphes

#define  BAR_UP     0
#define  BAR_DOWN   1
#define  BAR_LEFT   2
#define  BAR_RIGHT  3

// ----------------- Type Courbes
#define LINEAIRE    0
#define ESCALIER    1
#define HISTOGRAMME 2

// ---------------- Modeles des courbes
//                  Fonctions du service courbes:   SERVICE_ANM_GR_ANM_COURBES
//                  Param�tres de la sous fonction: ANM_GR_COURBES_MOD

#define  ANM_GR_MODELE_NOP  0
#define  ANM_GR_MODELE_LGN  1
#define  ANM_GR_MODELE_BAR  2


// S�lecteur de type d'entr�e logique
#define EL_TRANSPARENTE 0
#define EL_CONTROL_CHECK 1
#define EL_CONTROL_BOUTON_PULSE 2

// S�lecteur de type de validation pour les boutons de validation
#define ANM_BOUTON_VAL_OK 1
#define ANM_BOUTON_VAL_APPLIQUER 2
#define ANM_BOUTON_VAL_ANNULER 3

#define IDAPPLY 12

// Identifiant de l'element graphique anim�
#define c_NB_CAR_ID_ELT_GR_ANIM      22 //(c_NB_CAR_ID_ELT_GR + 2) pour guillemets

//----------------------------------------------------------------------------
// Type
//----------------------------------------------------------------------------
//  Type du service demand�
#define  ANM_GR_TYPE_SRV_EXEC              1


// Liste des services de l'animateur
typedef enum
	{
	SERVICE_ANM_GR_ACK                   = 0,   // NULL
	SERVICE_ANM_GR_FEN_AFFICHE           = 1,   // real x, y, cx, cy, dim
	SERVICE_ANM_GR_FEN_EFFACE            = 2,   // NULL
	SERVICE_ANM_GR_FEN_TAILLE            = 3,   // real x, y, cx, cy, dim
	SERVICE_ANM_GR_SELECT_PAGE           = 4,   // DWORD
	SERVICE_ANM_GR_PAGE_IMPRESSION       = 5,   // real
	SERVICE_ANM_GR_ANM_LOG_2_ETATS       = 6,   // bool
	SERVICE_ANM_GR_ANM_LOG_4_ETATS       = 7,   // bool, bool
	SERVICE_ANM_GR_ANM_NUM_BARAGRAPHE    = 8,   // real
	SERVICE_ANM_GR_ANM_NUM_DEPLACEMENT   = 9,   // real, real
	SERVICE_ANM_GR_ANM_MES_SIMPLE        =10,   // pszMessage
	SERVICE_ANM_GR_ANM_MES_COULEUR       =11,   // reel, pszMessage
	SERVICE_ANM_GR_ANM_MES_NUM_SIMPLE    =12,   // reel
	SERVICE_ANM_GR_ANM_MES_NUM_COULEUR   =13,   // reel, reel
	SERVICE_ANM_GR_ANM_COURBES           =14,   // bloc de donn�es
	SERVICE_ANM_GR_ANM_ESCALIERS         =15,   // bloc de donn�es
	SERVICE_ANM_GR_ANM_ARCHIVES          =16,   // bloc de donn�es
	SERVICE_ANM_GR_ANM_FORCAGE_NUM       =17,   // reel
	SERVICE_ANM_GR_ANM_FORCAGE_MES       =18,   // message

	SERVICE_ANM_GR_ANM_LISTE_COMBOBOX		 =19,   // bloc de donn�es
	SERVICE_ANM_GR_ANM_FORCAGE_RADIO     =20,   // real
	SERVICE_ANM_GR_ANM_FORCAGE_CHECK     =21,   // bool
	SERVICE_ANM_GR_ANM_FORCAGE_TEXT      =22,   // message
	SERVICE_ANM_GR_ANM_FORCAGE_LOG_TRANSPARENT      =23,   // bool
	SERVICE_ANM_GR_ANM_FORCAGE_BOUTON_PULSE        =24   // bool
	} ID_SERVICE_ANM_GR;

#define  ANM_GR_NOMBRE_SERVICES (SERVICE_ANM_GR_ANM_FORCAGE_BOUTON_PULSE+1)

// Sous fonctions du service courbes: SERVICE_ANM_GR_ANM_COURBES
#define  ANM_GR_COURBES_FIN      0
#define  ANM_GR_COURBES_ERASE    1
#define  ANM_GR_COURBES_BUFFER   2
#define  ANM_GR_COURBES_VALEUR   3


// Modeles des courbes
// Fonctions du service courbes:   SERVICE_ANM_GR_ANM_COURBES
// Param�tres de la sous fonction: ANM_GR_COURBES_MOD
#define  ANM_GR_MODELE_NOP           0
#define  ANM_GR_MODELE_LGN           1
#define  ANM_GR_MODELE_BAR           2

// Sous fonctions du service courbes: SERVICE_ANM_GR_ANM_ARCHIVES
#define  ANM_GR_ARCHIVE_FIN      0
#define  ANM_GR_ARCHIVE_ERASE    1
#define  ANM_GR_ARCHIVE_BUFFER   2
#define  ANM_GR_ARCHIVE_COULEUR  3
#define  ANM_GR_ARCHIVE_STYLE    4
#define  ANM_GR_ARCHIVE_MODELE   5
#define  ANM_GR_ARCHIVE_MIN      6
#define  ANM_GR_ARCHIVE_MAX      7

// Modeles des courbes Achives
// Fonctions du service courbes:   SERVICE_ANM_GR_ANM_ARCHIVES
// Param�tres de la sous fonction: ANM_GR_ARCHIVE_MODELE
#define  ANM_GR_MODELE_ARCHIVE_NOP  0
#define  ANM_GR_MODELE_ARCHIVE_LGN  1
#define  ANM_GR_MODELE_ARCHIVE_ESC  2
#define  ANM_GR_MODELE_ARCHIVE_HST  3

// Taille Buffer de reception pour l'animateur
//		"     "    d' emission  pour PCS-EXE
#define  ANM_GR_MAX_BUFF_SERVICES      87040

// Taille Buffer de emission  pour l'animateur
//		"     "    de reception pour PCS-EXE
#define  ANM_GR_MAX_BUFF_EVENTS        87040


// Type Message
#define DEBUT_TRAME    1
#define CONTENU_TRAME  2
#define FIN_TRAME      3


// Type Des Donn�es Re�ues par l'animateur
//									Emises par PCS-EXE
typedef struct
  {
  DWORD NbServices;
  BYTE  bBufferServices [ANM_GR_MAX_BUFF_SERVICES];
  } t_anm_gr_data_service;

// Type Des Donn�es Emises par l'animateur
//                  Re�ues par PCS-EXE
typedef union
  {
  BOOL		ValLogique;
  FLOAT   ValNumerique;
  char    ValMessage [256];
  DWORD   ValType;
  } t_valeur;

typedef struct
  {
  DWORD     CodeRetour;
  t_valeur  Valeur;
  } t_anm_gr_notification;


typedef struct
  {
  ID_SERVICE_ANM_GR Service;
  DWORD Element;
  } HEADER_SERVICE_ANM_GR;

typedef struct
  {
  DWORD numero_page;
  DWORD tipe_anim;
  LONG coord[4];
  } t_contexte_vdi;

typedef struct
  {
  DWORD numero_page;
  DWORD numero_repere;
  union
    {
    DWORD pos_donnees;
    struct
      {
      DWORD pos_es;
      DWORD pos_initial;
      DWORD pos_specif_es;
      };
    };
  } GEO_VDI, *PGEO_VDI;


typedef struct
	{
	HEADER_SERVICE_ANM_GR HeaderServiceAnmGr;
	DWORD prem_variable;
	DWORD nbr_variable;
	} tx_element_vi;

typedef struct
	{
	DWORD bloc_genre;
	DWORD pos_es;
	} tx_variable_vi;

typedef struct
  {
  G_COULEUR color_m_n;
  G_COULEUR color_a_n;
  DWORD position_es_normal;
  ID_MOT_RESERVE tipe_anim;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_LOG, *PVDI_R_LOG;

typedef struct
  {
  G_COULEUR color_m_n;
  G_COULEUR color_a_n;
  DWORD position_es_normal;
  DWORD indice;
  ID_MOT_RESERVE tipe_anim;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_LOG_TAB, *PVDI_R_LOG_TAB;

typedef struct
  {
  G_COULEUR color_h_h;
  G_COULEUR color_h_b;
  G_COULEUR color_b_h;
  G_COULEUR color_b_b;
  DWORD position_es_un;
  DWORD position_es_deux;
  ID_MOT_RESERVE tipe_anim;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_LOG_4_ETATS, *PVDI_R_LOG_4_ETATS;

typedef struct
  {
  G_COULEUR color_h_h;
  G_COULEUR color_h_b;
  G_COULEUR color_b_h;
  G_COULEUR color_b_b;
  DWORD indice_un;
  DWORD indice_deux;
  DWORD position_es_un;
  DWORD position_es_deux;
  ID_MOT_RESERVE tipe_anim;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_LOG_4_ETATS_TAB, *PVDI_R_LOG_4_ETATS_TAB;

typedef struct
  {
  DWORD color_cste;
  DWORD color;
  DWORD position_es_un;
  DWORD position_es_deux;
  ID_MOT_RESERVE tipe_anim;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_NUM_DEP, *PVDI_R_NUM_DEP;

typedef struct
  {
  DWORD color_cste;
  DWORD indice_un;
  DWORD indice_deux;
  DWORD color;
  DWORD position_es_un;
  DWORD position_es_deux;
  ID_MOT_RESERVE tipe_anim;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_NUM_DEP_TAB, *PVDI_R_NUM_DEP_TAB;

typedef struct
  {
  ID_MOT_RESERVE modele;
  DWORD color;
  DWORD style;
  FLOAT max;
  FLOAT min;
  DWORD position_es;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_NUM, *PVDI_R_NUM;

typedef struct
  {
  ID_MOT_RESERVE modele;
  DWORD color;
  DWORD style;
  FLOAT max;
  FLOAT min;
  DWORD indice;
  DWORD position_es;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_NUM_TAB, *PVDI_R_NUM_TAB;

typedef struct
  {
  DWORD color_cste;
  DWORD color;
  DWORD indice_color;
  DWORD position_es;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_MES, *PVDI_R_MES;

typedef struct
  {
  DWORD color_cste;
  DWORD color;
  DWORD indice_color;
  DWORD indice;
  DWORD position_es;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_MES_TAB, *PVDI_R_MES_TAB;

typedef struct
  {
  DWORD color_cste;
  DWORD color;
  DWORD indice_color;
  DWORD nbr_car;
  DWORD nbr_dec;
  DWORD position_es;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_NUM_ALPHA, *PVDI_R_NUM_ALPHA;

typedef struct
  {
  DWORD color_cste;
  DWORD color;
  DWORD indice_color;
  DWORD nbr_car;
  DWORD nbr_dec;
  DWORD indice;
  DWORD position_es;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_R_NUM_ALPHA_TAB, *PVDI_R_NUM_ALPHA_TAB;

typedef struct
  {
  DWORD pos_es_a_ech;
  DWORD nbr_echantillon;
  LONG periode_echantillon;
  G_COULEUR couleur;
  DWORD style;
  FLOAT min;
  FLOAT max;
  int nb_pts_scroll;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_COURBE_TEMPS, *PVDI_COURBE_TEMPS;

typedef struct
  {
  DWORD position_es;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_CONTROL_RADIO, *PVDI_CONTROL_RADIO;

typedef struct
  {
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_CONTROL_STATIC, *PVDI_CONTROL_STATIC;

typedef struct
  {
  DWORD position_es_liste;
  DWORD position_es_indice;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_CONTROL_LISTE, *PVDI_CONTROL_LISTE;

typedef struct
  {
  DWORD position_es_liste;
  DWORD position_es_indice;
  DWORD position_es_saisie;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_CONTROL_COMBOBOX, *PVDI_CONTROL_COMBOBOX;

typedef struct
	{
	BOOL		tampon_plein;            // attention type BOOL -> voir tipe_vi.int
	BOOL		cadrage_pascal;
	FLOAT * pfBufferEchantillons;                  // initialis� en exploitation
	DWORD   pos_es_enclanche;
	DWORD   pos_es_stocke;
	DWORD   courant_tampon;
	DWORD   nbr_echantillon;
	DWORD   nbr_stocke;              // initialis� en exploitation
	LONG    periode_ech_duree;
	MINUTERIE  periode_ech_tampon;      // initialis� en exploitation
	} XGR_COURBE_TEMPS, *PXGR_COURBE_TEMPS;

typedef struct
  {
  DWORD pos_es_a_ech;
  DWORD nbr_echantillon;
  DWORD pos_es_echantillonnage;
  G_COULEUR couleur;
  DWORD style;
  FLOAT min;
  FLOAT max;
  int nb_pts_scroll;
  G_COULEUR couleur_fond;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_COURBE_CMD, *PVDI_COURBE_CMD;

typedef struct
	{
	BOOL tampon_plein;            // attention type BOOL -> voir tipe_vi.int
	BOOL cadrage_pascal;
	FLOAT * pfBufferEchantillons;                  // initialis� en exploitation
	DWORD pos_es_reset_buffer;
	DWORD pos_es_stocke;           // var num
	DWORD pos_es_echantillonnage;  // var log commande
	DWORD courant_tampon;
	DWORD nbr_echantillon;
	} XGR_COURBE_CMD, *PXGR_COURBE_CMD;

typedef struct
  {
  DWORD pos_es_fic;
  DWORD pos_es_couleur;
  DWORD pos_es_style;
  DWORD pos_es_modele;
  DWORD pos_es_min;
  DWORD pos_es_max;
  DWORD pos_es_taille_enr;
  DWORD pos_es_offset_enr;
  DWORD pos_es_num_enr;
  DWORD pos_es_nb_pts;
  DWORD pos_es_cmd_affichage;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_COURBE_FIC, *PVDI_COURBE_FIC;

typedef struct
	{
	DWORD pos_spec_fichier;
	DWORD pos_spec_couleur;
	DWORD pos_spec_style;
	DWORD pos_spec_modele;
	DWORD pos_spec_min;
	DWORD pos_spec_max;
	DWORD pos_spec_taille;
	DWORD pos_spec_offset;
	DWORD pos_spec_enr;
	DWORD pos_spec_nb_enr;
	DWORD pos_spec_affichage;
	} tx_vdi_courbe_archive;

typedef struct
  {
  DWORD selecteur;
  DWORD pos_var;
  DWORD indice_tab;
  DWORD page_vdi;
  DWORD page_calque;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_E_LOG, *PVDI_E_LOG;

typedef struct
  {
  G_COULEUR couleur;
  UINT nbr_caractere;
  FLOAT min;
  FLOAT max;
	LONG NbDecimales; //!ac
  G_COULEUR couleur_fond;
  BOOL cadre;
  BOOL masque;
  BOOL video_focus;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_E_NUM, *PVDI_E_NUM;

typedef struct
  {
  G_COULEUR couleur;
  UINT nbr_caractere;
  G_COULEUR couleur_fond;
  BOOL cadre;
  BOOL masque;
  BOOL video_focus;
	char strIdentifiantEltGr [c_NB_CAR_ID_ELT_GR_ANIM + 1];
  } VDI_E_MES, *PVDI_E_MES;

typedef struct
	{
	DWORD bloc_genre;
	DWORD pos_es;
	} tx_entrees_vi;

typedef struct
	{
	char titre_page[80];
	BOOL existe;
	} tx_vdi_nom_page;

typedef struct
	{
	DWORD pos_es;
	HEADER_SERVICE_ANM_GR handle;
	} t_fen;

typedef struct
	{
	DWORD pos_es_impression;
	DWORD pos_es_nom_doc;
	HEADER_SERVICE_ANM_GR handle;
	} t_fen_impression;

typedef struct
	{
	DWORD pos_es_x;
	DWORD pos_es_y;
	DWORD pos_es_cx;
	DWORD pos_es_cy;
	DWORD pos_es_dimension;
	HEADER_SERVICE_ANM_GR handle;
	} t_fen_taille;

typedef struct
	{
	BOOL          existe;
	BOOL          cadrage_pascal;
	t_fen            invisible;
	t_fen            visible;
	t_fen_impression impression;
	t_fen_taille     taille;
	DWORD             prem_elt;
	DWORD             nbr_elt;
	} tx_fenetre_vi;

typedef struct
	{
	BOOL		visible;
	BOOL		pas_forcage;
	//
	DWORD   nbr_entree;
	DWORD   prem_entree;
	//
	DWORD   code_ret_visible;
	DWORD   code_ret_x;
	DWORD   code_ret_y;
	DWORD   code_ret_cx;
	DWORD   code_ret_cy;
	DWORD   code_ret_dimension;
	DWORD   code_ret_user;
	//
	DWORD   page;
	DWORD   mode_aff;
	DWORD   dimension;
	//
	HWND		hwndVi;
	HGSYS		hGsys;
	HBDGR		hBdGr;
	//
	DWORD   facteur_zoom;
	FLOAT   coeff_zoom;
	LONG     cx_client_ref;
	LONG     cy_client_ref;
	} X_ANM_FENETRE_VI, *PX_ANM_FENETRE_VI;

typedef struct
	{
	DWORD   n_enr;
	DWORD   n_bloc;
	} X_ANM_E_VI, *PX_ANM_E_VI;

typedef struct
	{
	HEADER_SERVICE_ANM_GR	handle;
	} tx_services_speciaux_vi;

typedef struct
	{
	LONG  x;
	LONG  y;
	DWORD cx;
	DWORD cy;
	DWORD pos_ret;
	BOOL	bValeurCourante;
	HWND hwnd;
	} X_ANM_E_LOG_VI, *PX_ANM_E_LOG_VI;

typedef struct
	{
	DWORD n_elt;
	DWORD pos_ret;
	BOOL	bValeurCourante;
	} X_ANM_E_CONTROL_LOG_VI, *PX_ANM_E_CONTROL_LOG_VI;

typedef struct
	{
	DWORD n_elt;
	DWORD pos_ret;
	} X_ANM_E_LISTE, *PX_ANM_E_LISTE;

typedef struct
	{
	DWORD n_elt;
	DWORD pos_ret;
	} X_ANM_E_RADIO, *PX_ANM_E_RADIO;

typedef struct
	{
	DWORD n_elt;
	DWORD pos_ret_indice;
	DWORD pos_ret_saisie;
	} X_ANM_E_COMBOBOX, *PX_ANM_E_COMBOBOX;

typedef struct
	{
	LONG  x1;
	LONG  y1;
	LONG  cx;
	LONG  cy;
	int nbr_car;
	FLOAT min;
	FLOAT max;
	LONG	NbDecimales;  //!ac
	G_COULEUR couleur;
	G_COULEUR couleur_fond;
	DWORD police;
	DWORD style_police;
	DWORD pos_ret;
	float val_init;
	HWND hdl_inline;
	BOOL cadre;
	BOOL masque;
	BOOL video_focus;
	} X_ANM_E_NUM_VI, *PX_ANM_E_NUM_VI;

typedef struct
	{
	LONG  x1;
	LONG  y1;
	LONG  cx;
	LONG  cy;
	LONG  nbr_car;
	G_COULEUR couleur;
	G_COULEUR couleur_fond;
	DWORD police;
	DWORD style_police;
	DWORD pos_ret;
	char val_init[256];
	HWND hdl_inline;
	BOOL cadre;
	BOOL masque;
	BOOL video_focus;
	} X_ANM_E_MES_VI, *PX_ANM_E_MES_VI;

typedef struct
	{
	DWORD			numero_gr;
	G_COULEUR color_m_n;
	G_COULEUR color_a_n;
	DWORD			tipe_anim;
	} X_ANM_R_LOG_VI, *PX_ANM_R_LOG_VI;

typedef struct
	{
	DWORD numero_gr;
	G_COULEUR color_h_h;
	G_COULEUR color_h_b;
	G_COULEUR color_b_h;
	G_COULEUR color_b_b;
	DWORD tipe_anim;
	} X_ANM_R_LOG_4_ETATS, *PX_ANM_R_LOG_4_ETATS;

typedef struct
	{
	DWORD numero_gr;
	POINT	pt0;
	POINT	pt1;
	DWORD tipe_anim;
	DWORD couleur_fond;
	} X_ANM_R_NUM_DEPLACE, *PX_ANM_R_NUM_DEPLACE;

typedef struct
	{
	DWORD numero_gr;
	DWORD modele;
	FLOAT homothetie;
	int translation;
	FLOAT max;
	FLOAT min;
	} X_ANM_R_NUM, *PX_ANM_R_NUM;

typedef struct
	{
	DWORD numero_gr;
	DWORD couleur_fond;
	} X_ANM_R_MES, *PX_ANM_R_MES;

typedef struct
	{
	DWORD numero_gr;
	char pszformat [16];
	G_COULEUR couleur_fond;
	} X_ANM_R_NUM_ALPHA, *PX_ANM_R_NUM_ALPHA;

typedef struct
	{
	DWORD		numero_page;

	FLOAT		min;
	FLOAT		max;

	FLOAT		homothetie;
	FLOAT		translation;
	//
	LONG		yHaut;
	LONG		yBas;
	LONG		x_org;
	LONG		pas_x;
	//
	G_COULEUR		couleur;
	DWORD		style;
	DWORD		type_courbe;
	//
	POINT *	paPoints;
	DWORD		nb_points_max;
	DWORD		nb_points_scroll;
	DWORD		nb_points;
	DWORD		old_nb_points;
	//
	} X_ANM_COURBE_VI, *PX_ANM_COURBE_VI;


typedef struct
	{
	BOOL		affichage;
	DWORD   numero_page;
	G_COULEUR   couleur;
	DWORD   style;
	DWORD   modele;
	FLOAT   min;
	FLOAT   max;
	DWORD   nb_points_max;
	PPOINT	paPoints;
	LONG    x_min;
	LONG    coef_en_x;
	LONG    yHaut;
	LONG    yBas;
	} X_ANM_COURBE_FIC_VI, *PX_ANM_COURBE_FIC_VI;


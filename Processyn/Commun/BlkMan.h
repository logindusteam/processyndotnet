//---------------------------------------------------------------------------
// Constantes, types, et prototypes pour le gestionaire de m�moire
//---------------------------------------------------------------------------

#ifndef   BLKMAN_H
#define BLKMAN_H

// Handles sur les bases de donn�e BlkMan
DECLARE_HANDLE (HBLK_BD);
typedef HBLK_BD * PHBLKBD;

//----------------------- Constantes ----------------------------------------
#define BLK_MAX_CHAR_NOM_BD 256

#define BLK_INSERT_END            0xffffffff
#define BLK_REC_BASE_SIZE         0xffffffff

//------------ bases de donn�es -----------------------------

// cr�e une base de donn�es vierge
HBLK_BD hBlkCreeBDVierge (PCSTR pszNomBD);

// lib�re et d�truit une base de donn�e (met � NULL *phbd)
void BlkFermeBD (HBLK_BD *phbd);

// Renvoie la taille en octets d'une base de donn�es
DWORD dwBlkTailleBD (HBLK_BD hbd);

// transferts bases de donn�es <-> fichiers :

// Cr�e une base de donn�e � partir d'un fichier base de donn�es
HBLK_BD	hBlkChargeBD (PCSTR pszFile, PCSTR pszNomBD);

// Enregistre une base de donn�e sur un fichier base de donn�es
void BlkSauveBD (HBLK_BD hbd, PCSTR pszFile);

// ----------- blocs dans une base de donn�es ----------------------

// Cr�e un bloc dans une base de donn�es
PVOID	pBlkCreeBloc		(HBLK_BD hbd, DWORD dwIdBloc, DWORD dwTailleEnr, DWORD dwNbEnr);

// lib�re et d�truit un bloc dans une base de donn�es
void BlkFermeBloc		(HBLK_BD hbd, DWORD dwIdBloc);

// renvoie vrai si le bloc existe dans la base de donn�es
BOOL bBlkBlocExiste	(HBLK_BD hbd, DWORD dwIdBloc);

// Renvoie le nombre d'enregistrements dans ce bloc dans cette base de donn�e 
DWORD dwBlkNbEnr (HBLK_BD hbd, DWORD dwIdBloc, PCSTR pszSource = NULL, DWORD dwNLine = 0);

// Renvoie le nombre d'enregistrements dans ce bloc dans cette base de donn�e 
// renvoie 0 si le bloc n'existe pas
DWORD dwBlkNbEnrSansErr (HBLK_BD hbd, DWORD dwIdBloc);

// Renvoie la taille de l'enregistrement de base dans ce bloc dans cette base de donn�e 
DWORD dwBlkTailleEnrDeBase (HBLK_BD hbd, DWORD dwIdBloc);
//
// Ins�re de 0 � N enregistrements dans ce bloc dans cette base de donn�e 
PVOID pBlkInsereEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr, PCSTR pszSource = NULL, DWORD dwNLine = 0);

// Supprime de 0 � N enregistrements dans ce bloc dans cette base de donn�e 
void BlkEnleveEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr, DWORD dwNbEnr, PCSTR pszSource = NULL, DWORD dwNLine = 0);

// Change la taille de 0 � N enregistrements dans ce bloc dans cette base de donn�e 
PVOID pBlkChangeTailleEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr);

// Renvoie la taille d'un enregistrement dans ce bloc dans cette base de donn�e 
DWORD dwBlkTailleEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr);

// Renvoie l'adresse d'un enregistrement dans ce bloc dans cette base de donn�e 
PVOID pBlkPointeEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr, PCSTR pszSource = NULL, DWORD dwNLine = 0);

// Renvoie l'adresse d'un enregistrement dans ce bloc dans cette base de donn�e 
PVOID pBlkPointeEnrSansErr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr);

// Copie de 0 � N enregistrements entre deux blocs dans deux bases de donn�es
void BlkCopieEnr (HBLK_BD hBlkManDst, DWORD wBlkIdDst, DWORD wRecIdDst, HBLK_BD hBlkManSrc, DWORD wBlkIdSrc, DWORD wRecIdSrc, DWORD dwNbEnr);

#endif //BLKMAN_H

//----------------------------------------------------------------------------
//   DibMan.h
// Gestion des Device Independent Bitmaps
//
// A utiliser avec les fonctions Dib
//
// Version WIN32 (initialement sample Microsoft)
// 7/3/97
// Pour afficher un DIB, utiliser DrawDibOpen() DrawDibDraw() DrawDibClose ()
// plus gestion de la palette DrawDibRealize(), DrawDibSetPalette()
//----------------------------------------------------------------------------

// Handle sur un DIB (handle m�moire contenant BITMAP INFO, palette data, bits....)
DECLARE_HANDLE (HDIB);

//----------------------------------------------------------
// Ouvre un fichier DIB, le charge en m�moire et l'upgrade si n�cessaire
HDIB hChargeDIB (PCSTR szFile);	// renvoie le HDIB si Ok, NULL sinon

//----------------------------------------------------------
// Ferme le handle et lib�re le DIB en m�moire
BOOL bFermeDIB (HDIB * phdib);	// renvoie TRUE si l'objet a �t� lib�r�, FALSE sinon

//---------------------------------------------------
// Ecris un HDIB dans un fichier au format CF_DIB
BOOL bEcrisDIB (PCSTR szFile, HDIB hdib);

/*
//----------------------------------------------------------
// Will create a global memory block in DIB format that represents the DDB passed in
HDIB hDibFromBitmap (HBITMAP hbm, DWORD biStyle, WORD biBits, HPALETTE hpal, WORD wUsage);

// Lecture d'un fichier au format DIB. Renvoie un HANDLE global sur un BITMAPINFO.
// Convertis si n�cessaire les anciens BITMAPINFO en nouveaux.
HDIB        ReadDibBitmapInfo   (int fh);

HPALETTE    CreateBIPalette     (LPBITMAPINFOHEADER lpbi);
HBITMAP     BitmapFromDib       (HDIB hdib, HPALETTE hpal, WORD wUsage);
BOOL        SetDibUsage         (HDIB hdib, HPALETTE hpal,WORD wUsage);
HDIB        CreateDib           (int bits, int dx, int dy);
BOOL        StretchDib          (HDC hdc, int x, int y, int dx, int dy, HDIB hdib, HPALETTE hpal, int x0, int y0, int dx0, int dy0, LONG rop, WORD wUsage);
*/

//----------------------------------------------------------------------------
//   Macros
//----------------------------------------------------------------------------

//#define DrawDib(hdc,x,y,hdib,hpal,wUsage)  StretchDib(hdc,x,y,-1,-1,hdib,hpal,0,0,-1,-1,SRCCOPY,wUsage)
//#define CreateDibPalette(hdib)  CreateBIPalette(GlobalLock(hdib))

//#define ALIGNULONG(i)           ((i+3)/4*4)        // ULONG aligned !

#define WIDTHBYTES(i)           ((unsigned)((i+31)&(~31))/8)  // ULONG aligned !

#define DIBWIDTHBYTES(bi)       DibWidthBytes(&bi)

#define DibWidthBytesN(lpbi, n) (UINT)WIDTHBYTES((UINT)(lpbi)->biWidth * (UINT)(n))
#define DibWidthBytes(lpbi)     DibWidthBytesN(lpbi, (lpbi)->biBitCount)

#define DibSizeImage(lpbi)      ((lpbi)->biSizeImage == 0 \
                                    ? ((DWORD)(UINT)DibWidthBytes(lpbi) * (DWORD)(UINT)(lpbi)->biHeight) \
                                    : (lpbi)->biSizeImage)

#define DibSize(lpbi)           ((lpbi)->biSize + (lpbi)->biSizeImage + (int)(lpbi)->biClrUsed * sizeof(RGBQUAD))

#define DibPtr(lpbi)            (LPVOID)(DibColors(lpbi) + (UINT)(lpbi)->biClrUsed)
#define DibColors(lpbi)         ((LPRGBQUAD)((LPBYTE)(lpbi) + (int)(lpbi)->biSize))

#define DibNumColors(lpbi)      ((lpbi)->biClrUsed == 0 && (lpbi)->biBitCount <= 8 \
                                    ? (int)(1 << (int)(lpbi)->biBitCount)          \
                                    : (int)(lpbi)->biClrUsed)
/*
#define DibXYN(lpbi,pb,x,y,n)   (LPVOID)(                                     \
                                (PBYTE)(pb) +                                 \
                                (UINT)((UINT)(x) * (UINT)(n) / 8u) +          \
                                ((DWORD)DibWidthBytesN(lpbi,n) * (DWORD)(UINT)(y)))


#define DibXY(lpbi,x,y)         DibXYN(lpbi,DibPtr(lpbi),x,y,(lpbi)->biBitCount)


#define FixBitmapInfo(lpbi)     if ((lpbi)->biSizeImage == 0)                 \
                                    (lpbi)->biSizeImage = DibSizeImage(lpbi); \
                                if ((lpbi)->biClrUsed == 0)                   \
                                    (lpbi)->biClrUsed = DibNumColors(lpbi);
*/
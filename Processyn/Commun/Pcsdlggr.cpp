/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   :	pcsdlggr.c                                               |
 |   Auteur  :	LM							|
 |   Date    :	18/01/93						|
 |   Remarques : gestion des boites de dialogue dans gr                 |
 +----------------------------------------------------------------------*/


#include "stdafx.h"
#include <stdlib.h>

#include "std.h"
#include "Appli.h"
#include "MemMan.h"
#include "Mem.h"
#include "UStr.h"
#include "UEnv.h"
#include "CheckMan.h"
#include "DocMan.h"
#include "WBarOuti.h"
#include "lng_res.h"  // pour boites
#include "tipe.h"      // BOITES
#include "Descripteur.h"
#include "G_Objets.h"
#include "g_sys.h"
#include "bdgr.h"
#include "bdElemgr.h"      // pour constante MODE_XXX
#include "UChrono.h"
#include "tipe_gr.h"      // BOITES
#include "bdPagegr.h"      // pour constante MODE_XXX
#include "bdanmgr.h"
#include "LireLng.h"
#include "IdLngLng.h"
#include "idmenugr.h"
#include "DevMan.h"
#include "Couleurs.h"
#include "WEchanti.h"
#include "WStyleLi.h"
#include "WStyleRe.h"
#include "WCouleur.h"
#include "IdGrLng.h"
#include "ULangues.h"
#include "pcsdlg.h"
#include "Paragraphe.h"
#include "gerebdc.h"
#include "Verif.h"

#include "pcsdlggr.h"
VerifInit;

// param�tres boites de dialogues d'animation
typedef struct
	{
	char * pszAnimation;
	DWORD  wNumPage; // redondance � supprimer
	HBDGR	hbdgr;
	DWORD  wNbVar;
	DWORD  dwElement;
	} PARAM_DLG_GR;

#define c_ABANDON_BOITE 65500

#define MAX_ESPACEMENT 5000

typedef struct
	{
	char *pstrIdentifiant;
	} ENV_IDENTIFICATION, *PENV_IDENTIFICATION;

typedef struct
	{
	PARAM_DLG_GR	param;
	} ENV_ENT_LOG, *PENV_ENT_LOG;

typedef struct
	{
	PARAM_DLG_GR				param;
	DWORD								wStyleAnimation;
	G_COULEUR						anCouleurs[3];
	G_STYLE_REMPLISSAGE anStylesRemplissage[2];
	G_STYLE_LIGNE				wLigne[2];
	} ENV_REFLET_LOG, *PENV_REFLET_LOG;

typedef struct
	{
	PARAM_DLG_GR				param;
	DWORD								wStyleAnimation;
	G_COULEUR						anCouleurs[5];
	G_STYLE_REMPLISSAGE anStylesRemplissage[4];
	G_STYLE_LIGNE       wLigne[4];
	} ENV_REFLET_LOG_4, *PENV_REFLET_LOG_4;

typedef struct
	{
	PARAM_DLG_GR param;
	G_COULEUR			anCouleurs[2];
	G_STYLE_LIGNE wLigne;
	} ENV_REFLET_NUM, *PENV_REFLET_NUM;

// variables locales
static char    TabChamps[13][STR_MAX_CHAR_ARRAY];
static DWORD   wNbChamps;

// reflets logiques
#define COULEUR_HAUT_INIT     ((G_COULEUR)(CMD_MENU_BMP_GREEN - CMD_MENU_BMP_OFFSET_COULEUR))
#define COULEUR_BAS_INIT      ((G_COULEUR)(CMD_MENU_BMP_RED - CMD_MENU_BMP_OFFSET_COULEUR))
#define COULEUR_FOND_INIT     ((G_COULEUR)(CMD_MENU_BMP_BLACK - CMD_MENU_BMP_OFFSET_COULEUR))
#define COULEUR_BH_INIT       ((G_COULEUR)(CMD_MENU_BMP_BLUE - CMD_MENU_BMP_OFFSET_COULEUR))
#define COULEUR_HB_INIT       ((G_COULEUR)(CMD_MENU_BMP_DARKPINK - CMD_MENU_BMP_OFFSET_COULEUR))

#define REMPLISSAGE_HAUT_INIT ((G_STYLE_REMPLISSAGE)(CMD_MENU_BMP_STR0 - CMD_MENU_BMP_OFFSET_STR))
#define REMPLISSAGE_BAS_INIT  ((G_STYLE_REMPLISSAGE)(CMD_MENU_BMP_STR1 - CMD_MENU_BMP_OFFSET_STR))
#define REMPLISSAGE_BH_INIT   ((G_STYLE_REMPLISSAGE)(CMD_MENU_BMP_STR2 - CMD_MENU_BMP_OFFSET_STR))
#define REMPLISSAGE_HB_INIT   ((G_STYLE_REMPLISSAGE)(CMD_MENU_BMP_STR3 - CMD_MENU_BMP_OFFSET_STR))

#define LIGNE_HAUT_INIT       ((G_STYLE_LIGNE)(CMD_MENU_BMP_STL0 - CMD_MENU_BMP_OFFSET_STL))
#define LIGNE_BAS_INIT        ((G_STYLE_LIGNE)(CMD_MENU_BMP_STL1 - CMD_MENU_BMP_OFFSET_STL))
#define LIGNE_BH_INIT         ((G_STYLE_LIGNE)(CMD_MENU_BMP_STL2 - CMD_MENU_BMP_OFFSET_STL))
#define LIGNE_HB_INIT         ((G_STYLE_LIGNE)(CMD_MENU_BMP_STL3 - CMD_MENU_BMP_OFFSET_STL))

//--------- Attention : les cstes suivantes servent d'indices tableaux
#define NBR_STYLE             3

#define DLG_STYLE_COULEUR         0
#define DLG_STYLE_REMPLISSAGE     1
#define DLG_STYLE_LIGNE           2
#define DLG_STYLE_RIEN            3

// ------------------------------------------------------------------------
// Check ou uncheck un bouton d'une boite de dialogue � partir d'un bool�en
void static CheckDlgButtonBool (HWND hwnd, UINT	uIdItem, BOOL bCheck)
  {
	::CheckDlgButton (hwnd, uIdItem, bCheck ? BST_CHECKED : BST_UNCHECKED);
  }

// -----------------------------------------------------------------------
// Vars globale boite de dialogue propri�t�s de fen�tre de synoptique
static char titre_fenetre [NB_MAX_TITRE_FENETRE];
static char chaine_grille_x [NB_MAX_GRILLE];
static char chaine_grille_y [NB_MAX_GRILLE];
static char chaine_zoom [NB_MAX_ZOOM];

// -----------------------------------------------------------------------
// Initialise la boite de dialogue propri�t�s de fen�tre de synoptique
void static InitDialogProprietesPage (HWND hwnd)
  {
  DLGGR_STYLE_FEN * pEnv = (DLGGR_STYLE_FEN *)pGetEnv (hwnd); // recupere le pteur sur la structure;
  BOOL grille = (pEnv->grille_x != 0);

  CheckDlgButtonBool (hwnd, ID_CHECK_MODALE, pEnv->bFenModale);
	CheckRadioButton (hwnd, ID_RADIO_SAISIE_IMMEDIATE, ID_RADIO_SAISIE_PAR_VALIDATION, 
		(pEnv->bFenDialog ? ID_RADIO_SAISIE_PAR_VALIDATION : ID_RADIO_SAISIE_IMMEDIATE));
  CheckDlgButtonBool (hwnd, ID_CHECK_TITRE256, pEnv->un_titre);
  CheckDlgButtonBool (hwnd, ID_CHECK_BORDS, pEnv->un_bord);
  CheckDlgButtonBool (hwnd, ID_CHECK_BTN_SYSTEME, pEnv->un_sysmenu);
  CheckDlgButtonBool (hwnd, ID_CHECK_BTN_ICONE, pEnv->un_minimize);
  CheckDlgButtonBool (hwnd, ID_CHECK_BTN_TAILLE, pEnv->un_maximize);
  CheckDlgButtonBool (hwnd, ID_CHECK_BARRREH, pEnv->un_scroll_h);
  CheckDlgButtonBool (hwnd, ID_CHECK_BARREV, pEnv->un_scroll_v);

  CheckDlgButtonBool (hwnd, ID_CHECK_GRILLEX, grille);
  SendDlgItemMessage (hwnd, ID_EF_GRILLEX, EM_SETLIMITTEXT, NB_MAX_GRILLE, 0);
  if (grille)
    {
    StrDWordToStr (chaine_grille_x, pEnv->grille_x);
    }
  else
    {
    StrSetNull (chaine_grille_x);
    }
  SetDlgItemText (hwnd, ID_EF_GRILLEX, chaine_grille_x);
  ::EnableWindow(::GetDlgItem(hwnd, ID_EF_GRILLEX), grille);

  grille = (pEnv->grille_y != 0);
  CheckDlgButtonBool (hwnd, ID_CHECK_GRILLEY, grille);
  SendDlgItemMessage (hwnd, ID_EF_GRILLEY, EM_SETLIMITTEXT, NB_MAX_GRILLE, 0);
  if (grille)
    {
    StrDWordToStr (chaine_grille_y, pEnv->grille_y);
    }
  else
    {
    StrSetNull (chaine_grille_y);
    }
  SetDlgItemText (hwnd, ID_EF_GRILLEY, chaine_grille_y);
  ::EnableWindow(::GetDlgItem(hwnd, ID_EF_GRILLEY), grille);

  switch (pEnv->mode_affichage)
    {
    case MODE_CLIP:
      CheckDlgButtonBool (hwnd, ID_RADIO_CLIP, TRUE);
      // $$ en attendant que �a remache ::EnableWindow(::GetDlgItem(hwnd, ID_CHECK_BARRREH), TRUE);
      // $$ en attendant que �a remache ::EnableWindow(::GetDlgItem(hwnd, ID_CHECK_BARREV), TRUE);
      chaine_zoom[0] = '\0';
      break;
    case MODE_ZOOM:
      CheckDlgButtonBool (hwnd, ID_RADIO_ZOOM, TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_CHECK_BARRREH), TRUE);
      ::EnableWindow(::GetDlgItem(hwnd, ID_CHECK_BARREV), TRUE);
      SendDlgItemMessage (hwnd, ID_EF_ZOOM, EM_SETLIMITTEXT, NB_MAX_ZOOM, 0);
      StrDWordToStr (chaine_zoom, (pEnv->facteur_zoom));
      break;
    default:
      chaine_zoom[0] = '\0';
      break;
    }
  SetDlgItemText (hwnd, ID_EF_ZOOM, chaine_zoom);

  CheckDlgButtonBool (hwnd, ID_RADIO_DSTANDARD, TRUE);
  // $$ en attendant que �a remache ::EnableWindow (::GetDlgItem (hwnd, ID_RADIO_DPROPORTION), FALSE);

  switch (pEnv->mode_deformation)
    {
    case MODE_STANDARD:
      CheckDlgButtonBool (hwnd, ID_RADIO_DSTANDARD, TRUE);
      break;
    case MODE_PROPORTIONNEL:
      CheckDlgButtonBool (hwnd, ID_RADIO_DPROPORTION, TRUE);
      break;
    default:
      break;
    }
  
  SendDlgItemMessage (hwnd, ID_EF_TITRE_FEB, EM_SETLIMITTEXT, NB_MAX_TITRE_FENETRE, 0);
  SetDlgItemText (hwnd, ID_EF_TITRE_FEB, pEnv->titre);
  StrCopy (titre_fenetre, pEnv->titre);
  ::EnableWindow(::GetDlgItem(hwnd, ID_EF_TITRE_FEB), pEnv->un_titre);

	// Fond de page
	SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1, pEnv->dwCouleurFond);
  CheckDlgButtonBool (hwnd, ID_CHECK_BMP_EX, pEnv->bBitmapFond);
	if (pEnv->bBitmapFond)
		SetDlgItemText (hwnd, ID_PATH_BITMAP, pEnv->szPathBitmapFond);
	::EnableWindow (::GetDlgItem (hwnd, IDC_PARCOURIR_BITMAP), pEnv->bBitmapFond);
	::EnableWindow (::GetDlgItem (hwnd, ID_PATH_BITMAP), pEnv->bBitmapFond);
  } // InitDialogProprietesPage

typedef struct
	{
	DWORD dwTypeDeStyle; //DLG_STYLE_COULEUR, DLG_STYLE_REMPLISSAGE, DLG_STYLE_LIGNE
	DWORD dwNStyleCourant; // G_COULEUR, etc...
	} DLGPROC_GRSTYLE_PARAM, *PDLGPROC_GRSTYLE_PARAM;
// -------------------------------------------------------------------------
// Boite de dialogue de choix d'un style d'animation
// -------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrStyle(HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;
  
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			HWND hwndChoix = NULL;
			char pszTitre[c_nb_car_message_inf] = "";

			// Mise � jour du titre de la boite de dialogue
			PDLGPROC_GRSTYLE_PARAM pParam = (PDLGPROC_GRSTYLE_PARAM) mp2;
			switch (pParam->dwTypeDeStyle)
				{
				case DLG_STYLE_COULEUR :
					{
					bLngLireInformation (c_inf_couleur, pszTitre);
					hwndChoix  = hwndCreeChoixCouleurs (hwnd, TRUE, TRUE);
					::SendMessage (hwndChoix, CM_SET_COULEUR_DESSIN, pParam->dwNStyleCourant,0);
					}
					break;
				case DLG_STYLE_REMPLISSAGE :
					bLngLireInformation (c_inf_style_remplissage, pszTitre);
					hwndChoix  = hwndCreeChoixStyleRemplissages (hwnd, TRUE, TRUE);
					break;
				case DLG_STYLE_LIGNE :
					bLngLireInformation (c_inf_style_ligne, pszTitre);
					hwndChoix  = hwndCreeChoixStyleLignes (hwnd, TRUE, TRUE);
					break;
				default :
					pszTitre[0] = '\0';
					break;
				}
			::SetWindowText (hwnd, pszTitre);

			if (hwndChoix)
				{
				RECT rcChoix;
				RECT rcClient;
				::GetWindowRect (hwndChoix, &rcChoix);
				::GetClientRect (hwnd, &rcClient);
				int dx = ((rcClient.right - rcClient.left) - (rcChoix.right - rcChoix.left))/2;
				int dy = ((rcClient.bottom - rcClient.top) - (rcChoix.bottom - rcChoix.top))/2;
				::MoveWindow (hwndChoix, dx, dy, rcChoix.right - rcChoix.left, rcChoix.bottom - rcChoix.top, TRUE);
				}
			}
      break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
				case IDCANCEL:
				case IDOK:
          EndDialog (hwnd, (USHORT)c_ABANDON_BOITE);
          break;
				default:
					// L'utilisateur a choisi une commande
					EndDialog (hwnd, LOWORD(mp1));
					break;
        }  // switch command
			}
			break;

    default:
      mres = 0;
      break;
    }
  return (mres);
  }


// -----------------------------------------------------------------------
// Boite de dialogue de choix d'une couleur, d'un style de ligne ou de remplissage
static BOOL DlgStyleAnimation (HWND hwndParent, DWORD wTypeDeStyle, DWORD *wEtat)
  {
	DLGPROC_GRSTYLE_PARAM Param;
	Param.dwNStyleCourant = *wEtat;
	Param.dwTypeDeStyle = wTypeDeStyle;
	*wEtat = LangueDialogBoxParam (MAKEINTRESOURCE (DLG_STYLE), hwndParent, dlgprocGrStyle, (LPARAM) &Param);
  return (*wEtat) != c_ABANDON_BOITE;
  }

// -----------------------------------------------------------------------
// Attributs d'une page de synoptique
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocProprietesPage (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL							mres = TRUE;			   // Valeur de retour
  DLGGR_STYLE_FEN * pEnv;

  switch (msg)
    {
    case WM_INITDIALOG:
			pSetEnvOnInitDialogParam (hwnd, mp2);
		  InitDialogProprietesPage (hwnd);
      mres = FALSE;
      break;

    case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
				case ID_CHECK_TITRE256:
					if (HIWORD (mp1) == BN_CLICKED)
						{
						if (IsDlgButtonChecked (hwnd, ID_CHECK_TITRE256)  == BST_CHECKED)
							{
							::EnableWindow(::GetDlgItem(hwnd, ID_EF_TITRE_FEB), TRUE);
							SetDlgItemText(hwnd, ID_EF_TITRE_FEB, titre_fenetre);
							}
						else
							{
							::EnableWindow(::GetDlgItem(hwnd, ID_EF_TITRE_FEB), FALSE);
							GetDlgItemText (hwnd, ID_EF_TITRE_FEB, titre_fenetre, NB_MAX_TITRE_FENETRE);
							SetDlgItemText(hwnd, ID_EF_TITRE_FEB, "");
							}
						}
					break;

				case ID_CHECK_GRILLEX:
					if (HIWORD (mp1) == BN_CLICKED)
						{
						if (IsDlgButtonChecked(hwnd, ID_CHECK_GRILLEX)  == BST_CHECKED)
							{
							::EnableWindow(::GetDlgItem(hwnd, ID_EF_GRILLEX), TRUE);
							if (StrIsNull (chaine_grille_x))
								{
								SetDlgItemText(hwnd, ID_EF_GRILLEX, "24");
								}
							else
								{
								SetDlgItemText(hwnd, ID_EF_GRILLEX, chaine_grille_x);
								}
							}
						else
							{
							::EnableWindow(::GetDlgItem(hwnd, ID_EF_GRILLEX), FALSE);
							GetDlgItemText (hwnd, ID_EF_GRILLEX, chaine_grille_x, NB_MAX_GRILLE);
							SetDlgItemText(hwnd, ID_EF_GRILLEX, "");
							}
						}
					break;

				case ID_CHECK_GRILLEY:
					if (HIWORD (mp1) == BN_CLICKED)
						{
						if (IsDlgButtonChecked(hwnd, ID_CHECK_GRILLEY)  == BST_CHECKED)
							{
							::EnableWindow(::GetDlgItem(hwnd, ID_EF_GRILLEY), TRUE);
							if (StrIsNull (chaine_grille_y))
								{
								SetDlgItemText(hwnd, ID_EF_GRILLEY, "24");
								}
							else
								{
								SetDlgItemText(hwnd, ID_EF_GRILLEY, chaine_grille_y);
								}
							}
						else
							{
							::EnableWindow(::GetDlgItem(hwnd, ID_EF_GRILLEY), FALSE);
							GetDlgItemText (hwnd, ID_EF_GRILLEY, chaine_grille_x, NB_MAX_GRILLE);
							SetDlgItemText(hwnd, ID_EF_GRILLEY, "");
							}
						}
					break;

				case ID_RADIO_ZOOM:
				case ID_RADIO_CLIP:
					if (HIWORD (mp1) == BN_CLICKED)
						{
						if (IsDlgButtonChecked(hwnd, ID_RADIO_ZOOM) == BST_CHECKED)
							{
							::EnableWindow(::GetDlgItem(hwnd, ID_EF_ZOOM), TRUE);
							if (StrIsNull (chaine_zoom))
								{
								StrCopy (chaine_zoom, "100");
								}
							SetDlgItemText(hwnd, ID_EF_ZOOM, chaine_zoom);
							}
						else
							{
							::EnableWindow(::GetDlgItem(hwnd, ID_EF_ZOOM), FALSE);
							GetDlgItemText (hwnd, ID_EF_ZOOM, chaine_zoom, NB_MAX_ZOOM);
							SetDlgItemText(hwnd, ID_EF_ZOOM, "");
							}
						}
					break;

				case IDC_CUSTOM_ECH1:
					{
					// couleur de fond
					G_COULEUR	nCouleur;
					DWORD		dwCouleur;

					if (GetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1, &nCouleur))
						{
						// appel de la boite de dialogue de choix de couleur
						dwCouleur = (DWORD)nCouleur;
						if (DlgStyleAnimation (hwnd, DLG_STYLE_COULEUR, &dwCouleur))
							{
							dwCouleur -= CMD_MENU_BMP_OFFSET_COULEUR;
							SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1, (G_COULEUR)dwCouleur);
							}
						}
					}
					break;

				case ID_CHECK_BMP_EX:
					{
					BOOL	bBitmap = (IsDlgButtonChecked (hwnd, ID_CHECK_BMP_EX) == BST_CHECKED);

					// Valide / autorise la modif du nom du bitmap
					::EnableWindow (::GetDlgItem (hwnd, IDC_PARCOURIR_BITMAP), bBitmap);
					::EnableWindow (::GetDlgItem (hwnd, ID_PATH_BITMAP), bBitmap);
					}
					break;

				case IDC_PARCOURIR_BITMAP:
					{
					char szPath [MAX_PATH] = "";
					char	titre [80];
					char	action [80];

					// Appel de la boite de dialogue d'ouverture de bitmap
					GetDlgItemText (hwnd, ID_PATH_BITMAP, szPath, MAX_PATH);
			    bLngLireInformation (c_inf_tdlg_charge_bmp_fond, titre);
			    bLngLireInformation (c_inf_action_charge_bmp_fond, action);

					if (bDlgFichierOuvrir (hwnd, szPath, titre, "BMP", action))
						{
						// Rends le path relatif au path document si possible
						pszPathNameRelatifAuDoc (szPath);
						SetDlgItemText (hwnd, ID_PATH_BITMAP, szPath);
						}
					}
					break;

	    	case IDOK:
					{
          pEnv = (DLGGR_STYLE_FEN *)pGetEnv (hwnd);
          //
          pEnv->bFenModale = (IsDlgButtonChecked(hwnd, ID_CHECK_MODALE)  == BST_CHECKED);
					pEnv->bFenDialog =  (nCheckIndex (hwnd, ID_RADIO_SAISIE_IMMEDIATE) == 2);
          pEnv->un_titre = (IsDlgButtonChecked(hwnd, ID_CHECK_TITRE256)  == BST_CHECKED);
          pEnv->un_bord = (IsDlgButtonChecked (hwnd, ID_CHECK_BORDS)  == BST_CHECKED);
          pEnv->un_sysmenu = (IsDlgButtonChecked (hwnd, ID_CHECK_BTN_SYSTEME)  == BST_CHECKED);
          pEnv->un_maximize = (IsDlgButtonChecked (hwnd, ID_CHECK_BTN_TAILLE)  == BST_CHECKED);
          pEnv->un_minimize = (IsDlgButtonChecked (hwnd, ID_CHECK_BTN_ICONE)  == BST_CHECKED);
          pEnv->un_scroll_h = (IsDlgButtonChecked (hwnd, ID_CHECK_BARRREH)  == BST_CHECKED);
          pEnv->un_scroll_v = (IsDlgButtonChecked (hwnd, ID_CHECK_BARREV)  == BST_CHECKED);
          if (nCheckIndex (hwnd, ID_RADIO_ZOOM) == 1)
						pEnv->mode_affichage = MODE_ZOOM;
					else
						pEnv->mode_affichage = MODE_CLIP;

          if (GetDlgItemText (hwnd, ID_EF_ZOOM, chaine_zoom, NB_MAX_ZOOM) != 0)
            {
            if (!StrToDWORD (&pEnv->facteur_zoom, chaine_zoom))
              {
              pEnv->facteur_zoom = 0;
              }
            }
          else
            {
            pEnv->facteur_zoom = 100;
            }

					if (nCheckIndex (hwnd, ID_RADIO_DPROPORTION) == 1)
						pEnv->mode_deformation = MODE_PROPORTIONNEL;
					else
						pEnv->mode_deformation = MODE_STANDARD;

          if (GetDlgItemText (hwnd, ID_EF_TITRE_FEB, pEnv->titre, NB_MAX_TITRE_FENETRE) == 0)
            pEnv->titre [0] = '\0';

          if (GetDlgItemText (hwnd, ID_EF_GRILLEX, chaine_grille_x, NB_MAX_GRILLE) != 0)
            {
            if (!StrToDWORD (&pEnv->grille_x, chaine_grille_x))
              pEnv->grille_x = 0;
            }
          else
            pEnv->grille_x = 0;

          if (GetDlgItemText (hwnd, ID_EF_GRILLEY, chaine_grille_y, NB_MAX_GRILLE) != 0)
            {
            if (!StrToDWORD (&pEnv->grille_y, chaine_grille_y))
              pEnv->grille_y = 0;
            }
          else
            pEnv->grille_y = 0;

					// Fond
					GetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1, &pEnv->dwCouleurFond);
          pEnv->bBitmapFond = (IsDlgButtonChecked (hwnd, ID_CHECK_BMP_EX) == BST_CHECKED);
					GetDlgItemText (hwnd, ID_PATH_BITMAP, pEnv->szPathBitmapFond, MAX_PATH);

          EndDialog(hwnd, TRUE);
					}
          break;

				case ID_DEFAUT:
					pEnv = (DLGGR_STYLE_FEN *)pGetEnv (hwnd); // recupere le pteur sur la structure
          pEnv->bFenModale = FALSE;
          pEnv->bFenDialog = FALSE;
          pEnv->bBitmapFond = FALSE;
          pEnv->un_titre = TRUE;
          pEnv->un_bord = TRUE;
          pEnv->un_sysmenu = TRUE;
          pEnv->un_minimize = TRUE;
          pEnv->un_maximize = TRUE;
          pEnv->un_scroll_h = FALSE;
          pEnv->un_scroll_v = FALSE;
          pEnv->mode_affichage = MODE_CLIP;
          pEnv->facteur_zoom = 100;
          pEnv->mode_deformation = MODE_STANDARD;
          pEnv->titre [0] = '\0';
          pEnv->grille_x = 0;
          pEnv->grille_y = 0;
          InitDialogProprietesPage (hwnd);
          break;

				case IDCANCEL:
          mres = TRUE;
          EndDialog(hwnd, FALSE);
					break;
        } // switch(LOWORD(mp1))
			} // WM_COMMAND
      break;

    default:
      mres = 0;
      break;
    }
  return mres;
  } // dlgprocProprietesPage

// -----------------------------------------------------------------------
// Appel de la boite de dialogue propri�t�s de la fen�tre de synoptique
BOOL dlg_style_fenetre_gr (HWND hwnd, DLGGR_STYLE_FEN *data_style)
  {
  return LangueDialogBoxParam (MAKEINTRESOURCE(DLG_GR_PROP_PAGE), hwnd, dlgprocProprietesPage,(LPARAM)data_style);
  }

// -----------------------------------------------------------------------
// parametres d'espacement des objets graphiques
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrAlign (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT                 mres = 0;   // Valeur de retour
  t_dlg_parametres_gr * * ptr_sur_data_a_init;
  int                     iEspaceH;
  int                     iEspaceV;

  switch (msg)
    {
    case WM_INITDIALOG:
      ptr_sur_data_a_init = (t_dlg_parametres_gr * *)pMemAlloue (sizeof (t_dlg_parametres_gr *));
      (*ptr_sur_data_a_init) = (t_dlg_parametres_gr *) (PVOID) (mp2);
      pSelectEnv (hwnd, ptr_sur_data_a_init);
      //
      SendDlgItemMessage (hwnd, ID_EF_1, EM_SETLIMITTEXT, NB_MAX_GRILLE, 0);
      SendDlgItemMessage (hwnd, ID_EF_2, EM_SETLIMITTEXT, NB_MAX_GRILLE, 0);
      SetDlgItemInt (hwnd, ID_EF_1, (*ptr_sur_data_a_init)->wEspaceH, FALSE);
      SetDlgItemInt (hwnd, ID_EF_2, (*ptr_sur_data_a_init)->wEspaceV, FALSE);
      //
      mres = (LRESULT)TRUE;
      break;

    case WM_DESTROY:
			bLibereEnv(hwnd);
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
    		case IDOK : // Valider
					{
					BOOL bOk;

          ptr_sur_data_a_init = (t_dlg_parametres_gr **)pGetEnv (hwnd); // recupere le pteur sur la structure
					iEspaceH = GetDlgItemInt (hwnd, ID_EF_1, &bOk, TRUE);
          if (bOk)
            {
						iEspaceV = GetDlgItemInt (hwnd, ID_EF_2, &bOk, TRUE);
            if (bOk)
              {
              if ((iEspaceH >= 0) && (iEspaceH < MAX_ESPACEMENT) &&
                  (iEspaceV >= 0) && (iEspaceV < MAX_ESPACEMENT))
                {
                (*ptr_sur_data_a_init)->wEspaceH = (DWORD)iEspaceH;
                (*ptr_sur_data_a_init)->wEspaceV = (DWORD)iEspaceV;
                }
              }
            }
          EndDialog (hwnd, TRUE);
					}
          break;

    		case ID_DEFAUT : // Defaut
          SetDlgItemInt (hwnd, ID_EF_1, DEFAUT_ESPACEMENT_H, FALSE);
          SetDlgItemInt (hwnd, ID_EF_2, DEFAUT_ESPACEMENT_V, FALSE);
          break;

				case IDCANCEL:
          EndDialog (hwnd, FALSE);
          break;
        }
      break;

    case WM_CLOSE:
      EndDialog (hwnd, FALSE);
      break;

    default:
      mres = 0;
      break;
    }

  return mres;
  } // dlgprocGrAlign

// -----------------------------------------------------------------------
BOOL bParametreAlignementGr (HWND hwnd, t_dlg_parametres_gr *data)
  {
  return LangueDialogBoxParam (MAKEINTRESOURCE (DLG_ALIGN), hwnd, dlgprocGrAlign, (LPARAM)data);
  }


// ---------------------------------------------------------------------------------
// Choix d'une page de synoptique � ouvrir parmi les existantes
// ---------------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrOuvrePage (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;			   // Valeur de retour
  t_dlg_liste_saisie_gr * ptr_sur_data_a_init;
  BOOL  touche_traite = FALSE;

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			char    chaine [NB_MAX_CAR_SAISIE_COMBO];
			DWORD w;

      ptr_sur_data_a_init = (t_dlg_liste_saisie_gr *)pSetEnvOnInitDialogParam (hwnd, mp2);
			::SetWindowText(hwnd, ptr_sur_data_a_init->titre);
			if (ptr_sur_data_a_init->nb_max_car_saisie > NB_MAX_CAR_SAISIE_COMBO)
				{
				ptr_sur_data_a_init->nb_max_car_saisie = NB_MAX_CAR_SAISIE_COMBO;
				}
			SendDlgItemMessage (hwnd, ID_CB_GR, EM_SETLIMITTEXT, ptr_sur_data_a_init->nb_max_car_saisie, 0);
			chaine [0] = '\0';
			w = 1;
			do
				{
				(ptr_sur_data_a_init->ptr_proc_maj_liste) (&w, chaine);
				if (w != 0)
					SendDlgItemMessage (hwnd, ID_CB_GR, CB_ADDSTRING, 0, (LPARAM)chaine);
				} while (w != 0);
      SetFocus (::GetDlgItem (hwnd, ID_CB_GR));
      mres = TRUE;
			}
      break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
 				case ID_CB_GR:
					if (HIWORD(mp1) == CBN_DBLCLK) //double click
						{
						ptr_sur_data_a_init = (t_dlg_liste_saisie_gr *)pGetEnv (hwnd); // recupere le pteur sur la structure
						if (GetDlgItemText (hwnd, ID_CB_GR,ptr_sur_data_a_init->saisie, ptr_sur_data_a_init->nb_max_car_saisie) == 0)
							{
							ptr_sur_data_a_init->saisie [0] = '\0';
							}
						EndDialog(hwnd, TRUE);
						}
					break;

				case IDOK:
          ptr_sur_data_a_init = (t_dlg_liste_saisie_gr *)pGetEnv (hwnd); // recupere le pteur sur la structure
          if (GetDlgItemText (hwnd, ID_CB_GR, ptr_sur_data_a_init->saisie, ptr_sur_data_a_init->nb_max_car_saisie) == 0)
            {
            ptr_sur_data_a_init->saisie [0] = '\0';
            }
          EndDialog(hwnd, TRUE);
          break;

				case IDCANCEL:
					EndDialog(hwnd, FALSE);
        }
      break;

		case WM_CLOSE:
      EndDialog(hwnd, FALSE);
      break;

    default:
      mres = 0;
      break;
    }
  return (mres);
  }

// -----------------------------------------------------------------------
BOOL dlg_liste_saisie_gr (HWND hwnd, t_dlg_liste_saisie_gr *data)
  {
  return LangueDialogBoxParam (MAKEINTRESOURCE (DLG_OUVRE_PAGE), hwnd, dlgprocGrOuvrePage,(LPARAM)data);
  }

//-------------------------------------------------------------------------
// Saisie Identifiant element graphique
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrIdentifiant (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT				mres = 0;
  PENV_IDENTIFICATION	pEnv;
  char					szText[c_NB_CAR_ID_ELT_GR + 1];

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			pEnv = (PENV_IDENTIFICATION)pCreeEnv (hwnd, sizeof (PENV_IDENTIFICATION));
      pEnv->pstrIdentifiant = (char *) mp2;

			// Mise � jour du texte
			StrCopy (szText, pEnv->pstrIdentifiant);
      SetDlgItemText (hwnd, ID_EF_IDENTIFIANT, szText);
			}
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
				case IDOK :
					{
          pEnv = (PENV_IDENTIFICATION)pGetEnv (hwnd);

					// R�cup�re le texte du contr�le
		      GetDlgItemText (hwnd, ID_EF_IDENTIFIANT, szText, (c_NB_CAR_ID_ELT_GR+1));
					StrCopy (pEnv->pstrIdentifiant, szText);
					// Termin�
          EndDialog (hwnd, IDOK);
					} // case IDOK :
          break;

        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;
        }  // switch(LOWORD(mp1))
			} // case WM_COMMAND:
      break;

    default:
      mres = 0;
      break;
    } // switch (msg)

  return mres;
  } // dlgprocGrIdentifiant

// -----------------------------------------------------------------------
// Saisie Identifiant element graphique
// -----------------------------------------------------------------------
BOOL dlg_saisie_identifiant_gr (char *pstrIdentifiant)
  {
	BOOL bRetour;

  bRetour = (LangueDialogBoxParam (MAKEINTRESOURCE (DLG_IDENTIFIANT), Appli.hwnd, dlgprocGrIdentifiant,(LPARAM)pstrIdentifiant) == IDOK);
	return (bRetour);
  }

// -----------------------------------------------------------------------
//                    GESTION DES ENTREES LOGIQUES
// -----------------------------------------------------------------------
static void DefautEL (HWND hwnd, ENV_ENT_LOG *pEnv)
  {
  ::CheckDlgButton (hwnd, ID_RB_1, 1);
  }

//-------------------------------------------------------------------------
// Propri�t�s d'une zone d'entr�e logique (champ de saisie invisible)
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrELog (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT      mres = 0;
  PENV_ENT_LOG	pEnv;
  char         szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
			pEnv = (PENV_ENT_LOG)pCreeEnv (hwnd, sizeof (ENV_ENT_LOG));
      pEnv->param = *(PARAM_DLG_GR *) mp2;

      // Check de la valeur initiale Bas
      DefautEL (hwnd, pEnv);
			// description existe ?
      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// oui => decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 5, pEnv->param.pszAnimation);

				// sp�cifie le nom de la variable
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[2]);

        bMotReserve (c_res_un, szText);
        if (bStrEgales (TabChamps[3], szText))
          {
				// Check valeur initiale Haut
          ::CheckDlgButton (hwnd, ID_RB_1, 0);
          ::CheckDlgButton (hwnd, ID_RB_2, 1);
          }
        }
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case IDOK :
          pEnv = (PENV_ENT_LOG)pGetEnv (hwnd);
          bMotReserve (c_res_ign_e_l, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom de la variable
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          //
          if (IsDlgButtonChecked (hwnd, ID_RB_1))
            {
            bMotReserve (c_res_zero, szText);
            }
          else
            {
            bMotReserve (c_res_un, szText);
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          StrDWordToStr (szText, EL_TRANSPARENTE);
          StrConcat (pEnv->param.pszAnimation, szText);

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);

          EndDialog (hwnd, IDOK);
          break;

				case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return mres;
  }

//-------------------------------------------------------------------------
// Propri�t�s d'une zone d'entr�e logique (champ de saisie invisible)
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrBouton (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT      mres = 0;
  PENV_ENT_LOG	pEnv;
  char         szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
			pEnv = (PENV_ENT_LOG)pCreeEnv (hwnd, sizeof (ENV_ENT_LOG));
      pEnv->param = *(PARAM_DLG_GR *) mp2;

      // Check de la valeur initiale Bas
      DefautEL (hwnd, pEnv);
			// description existe ?
      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// oui => decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 5, pEnv->param.pszAnimation);

				// sp�cifie le nom de la variable
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[2]);

        bMotReserve (c_res_un, szText);
        if (bStrEgales (TabChamps[3], szText))
          {
				// Check valeur initiale Haut
          ::CheckDlgButton (hwnd, ID_RB_1, 0);
          ::CheckDlgButton (hwnd, ID_RB_2, 1);
          }
        }

			// Mise � jour du texte
			VerifWarning (bLireTexteElementGR (pEnv->param.dwElement, szText));
      SetDlgItemText (hwnd, ID_TEXT_1, szText);
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case IDOK :
          pEnv = (PENV_ENT_LOG)pGetEnv (hwnd);
          bMotReserve (c_res_ign_e_l, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom de la variable
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // �tat initial
          if (IsDlgButtonChecked (hwnd, ID_RB_1))
            {
            bMotReserve (c_res_zero, szText);
            }
          else
            {
            bMotReserve (c_res_un, szText);
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          StrDWordToStr (szText, EL_CONTROL_BOUTON_PULSE);
          StrConcat (pEnv->param.pszAnimation, szText);

					// R�cup�re le texte du contr�le
		      GetDlgItemText (hwnd, ID_TEXT_1, szText, 80);
					bTexteElementGR (pEnv->param.hbdgr, MODE_GOMME_ELEMENT, pEnv->param.dwElement, szText);

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

				case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return mres;
  }

//-------------------------------------------------------------------------
// Propri�t�s d'une zone d'entr�e case � cocher 
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrBoutonCheck (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT				mres = 0;
  PENV_ENT_LOG	pEnv;
  char					szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
			pEnv = (PENV_ENT_LOG)pCreeEnv (hwnd, sizeof (ENV_ENT_LOG));
      pEnv->param = *(PARAM_DLG_GR *) mp2;

      // Check de la valeur initiale Bas
      DefautEL (hwnd, pEnv);
			// description existe ?
      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// oui => decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 5, pEnv->param.pszAnimation);

				// sp�cifie le nom de la variable
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[2]);

        bMotReserve (c_res_un, szText);
        if (bStrEgales (TabChamps[3], szText))
          {
				// Check valeur initiale Haut
          ::CheckDlgButton (hwnd, ID_RB_1, 0);
          ::CheckDlgButton (hwnd, ID_RB_2, 1);
          }
        }

			// Mise � jour du texte
			VerifWarning (bLireTexteElementGR (pEnv->param.dwElement, szText));
      SetDlgItemText (hwnd, ID_TEXT_1, szText);
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case IDOK :
          pEnv = (PENV_ENT_LOG)pGetEnv (hwnd);
          bMotReserve (c_res_ign_e_l, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom de la variable
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // �tat initial
          if (IsDlgButtonChecked (hwnd, ID_RB_1))
            {
            bMotReserve (c_res_zero, szText);
            }
          else
            {
            bMotReserve (c_res_un, szText);
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          StrDWordToStr (szText, EL_CONTROL_CHECK);
          StrConcat (pEnv->param.pszAnimation, szText);


					// R�cup�re le texte du contr�le
		      GetDlgItemText (hwnd, ID_TEXT_1, szText, 80);
					bTexteElementGR (pEnv->param.hbdgr, MODE_GOMME_ELEMENT, pEnv->param.dwElement, szText);

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
					// Termin� Ok
          EndDialog (hwnd, IDOK);
          break;

				case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return mres;
  } //dlgprocGrBoutonCheck


//-------------------------------------------------------------------------
// Propri�t�s d'une zone d'entr�e logique (champ de saisie invisible)
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrBoutonVal (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT				mres = 0;
  PENV_ENT_LOG	pEnv;
  char					szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			PELEMENT_PAGE_GR pElement;

			pEnv = (PENV_ENT_LOG)pCreeEnv (hwnd, sizeof (ENV_ENT_LOG));
      pEnv->param = *(PARAM_DLG_GR *) mp2;

      // Check du type de validation
			pElement = pElementPage (pEnv->param.dwElement);
			Verif (pElement);

			switch (pElement->nStyleRemplissage)
				{
				case 0: // $$ � am�liorer pour la valeur initiale
					pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)ANM_BOUTON_VAL_ANNULER;
					CheckRadioButton (hwnd, ID_RB_1, ID_RB_3, ID_RB_3);
					break;

				case ANM_BOUTON_VAL_OK:
					CheckRadioButton (hwnd, ID_RB_1, ID_RB_3, ID_RB_1);
					break;
				case ANM_BOUTON_VAL_APPLIQUER:
					CheckRadioButton (hwnd, ID_RB_1, ID_RB_3, ID_RB_2);
					break;
				case ANM_BOUTON_VAL_ANNULER:
					CheckRadioButton (hwnd, ID_RB_1, ID_RB_3, ID_RB_3);
					break;
				default:
					VerifWarningExit;
					break;
				}

			// Mise � jour du texte
			VerifWarning (bLireTexteElementGR (pEnv->param.dwElement, szText));
      SetDlgItemText (hwnd, ID_TEXT_1, szText);
			}
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
				case IDOK :
					{
					PELEMENT_PAGE_GR pElement;

          pEnv = (PENV_ENT_LOG)pGetEnv (hwnd);

					pElement = pElementPage (pEnv->param.dwElement);
					Verif (pElement);

          // type de validation
          switch (nCheckIndex (hwnd, ID_RB_1))
            {
						case 1:
							pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)ANM_BOUTON_VAL_OK;
							break;
						case 2:
							pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)ANM_BOUTON_VAL_APPLIQUER;
							break;
						case 3:
							pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)ANM_BOUTON_VAL_ANNULER;
							break;
						default:
							VerifWarningExit;
							break;
						}

					// R�cup�re le texte du contr�le
		      GetDlgItemText (hwnd, ID_TEXT_1, szText, 80);
					bTexteElementGR (pEnv->param.hbdgr, MODE_GOMME_ELEMENT, pEnv->param.dwElement, szText);

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
					// Termin�
          EndDialog (hwnd, IDOK);
					} // case IDOK :
          break;

        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;
        }  // switch(LOWORD(mp1))
			} // case WM_COMMAND:
      break;

    default:
      mres = 0;
      break;
    } // switch (msg)

  return mres;
  } // dlgprocGrBoutonVal

//-------------------------------------------------------------------------
// Propri�t�s d'une zone groupe
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrGroupe (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT				mres = 0;
  PENV_ENT_LOG	pEnv;
  char					szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
			{
			pEnv = (PENV_ENT_LOG)pCreeEnv (hwnd, sizeof (ENV_ENT_LOG));
      pEnv->param = *(PARAM_DLG_GR *) mp2;

			// Mise � jour du texte
			VerifWarning (bLireTexteElementGR (pEnv->param.dwElement, szText));
      SetDlgItemText (hwnd, ID_TEXT_1, szText);
			}
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
				case IDOK :
					{
          pEnv = (PENV_ENT_LOG)pGetEnv (hwnd);

					// R�cup�re le texte du contr�le
		      GetDlgItemText (hwnd, ID_TEXT_1, szText, 80);
					bTexteElementGR (pEnv->param.hbdgr, MODE_GOMME_ELEMENT, pEnv->param.dwElement, szText);

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
					// Termin�
          EndDialog (hwnd, IDOK);
					} // case IDOK :
          break;

        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;
        }  // switch(LOWORD(mp1))
			} // case WM_COMMAND:
      break;

    default:
      mres = 0;
      break;
    } // switch (msg)

  return mres;
  } // dlgprocGrGroupe

// -----------------------------------------------------------------------
//                    Gestion des reflets logiques
// -----------------------------------------------------------------------
// Remplis une combo box avec la liste des variables sp�cifi�es
static void InitComboVariables (HWND hwnd, PARAM_DLG_GR * pParam, ID_MOT_RESERVE wGenre, DWORD wID, BOOL bTableau)
  {
  DWORD             wIndex;

  for (wIndex = 2; wIndex <= pParam->wNbVar; wIndex++)
    {
		PENTREE_SORTIE	pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wIndex);

    if (pES->genre == wGenre)
      { // variables simples ou tableaux
      if (((pES->sens == c_res_e) || (pES->sens == c_res_s) || (pES->sens == c_res_e_et_s)) || bTableau)
        {
				char szNomES[c_nb_car_es];
				NomES (szNomES, pES->i_identif);

        SendDlgItemMessage (hwnd, wID, CB_ADDSTRING,	0, (LPARAM)szNomES);
        }
      }
    }
  }

// -----------------------------------------------------------------------
static PENTREE_SORTIE	pESVariableTableau (PARAM_DLG_GR *pParam, char *pszText)
  {
  DWORD           wPos;
  PENTREE_SORTIE	pES;
  UL ul;

  StrCopyUpToLength (ul.show, pszText, 81);
  StrUpperCase (pszText);
  ul.genre = g_id;
  ul.erreur = 0;

  if (bReconnaitESExistante (&ul, &wPos))
    {
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wPos);
    if ((pES->sens == c_res_e) || (pES->sens == c_res_s) || (pES->sens == c_res_e_et_s))
      { // c'est une variable simple
      pES = NULL;
      }
    }
  else
    {
    pES = NULL;
    }

  return (pES);
  }

// -----------------------------------------------------------------------
// R�cup�ration du texte s�lectionn�
static void GetTextSelectCBVariable (HWND hwnd, PARAM_DLG_GR *pParam, // $$ param inutile
                                     DWORD wID_VAR, PSTR szText)
  {
  DWORD  wIndex;

	wIndex = SendDlgItemMessage (hwnd, wID_VAR, CB_GETCURSEL, 0, 0);
	if (wIndex != CB_ERR)
		{
		wIndex = SendDlgItemMessage (hwnd, wID_VAR, CB_GETLBTEXT, wIndex, (LPARAM)szText);
		VerifWarning (wIndex != CB_ERR);
		}
  }

// -----------------------------------------------------------------------
// R�cup�ration du texte �dit�
static void GetTextEditCBVariable (HWND hwnd, PARAM_DLG_GR *pParam, // $$ param inutile
                                     DWORD wID_VAR, PSTR szText)
  {
  GetDlgItemText (hwnd, wID_VAR, szText, 20); // $$ taille
	}

// -----------------------------------------------------------------------
// Mise � jour combo index de tableau
static BOOL bMajCBIndexTableau (HWND hwnd, PARAM_DLG_GR *pParam,
                                PSTR szText, DWORD wID_TAB, LONG lID_TEXT)

  {
  DWORD           wIndex;
  PENTREE_SORTIE	pES;
  BOOL						bOk;

  SendDlgItemMessage (hwnd, wID_TAB, CB_RESETCONTENT, 0, 0);
  pES = pESVariableTableau (pParam, szText);
  if (pES != NULL)
    {
    bOk = TRUE;
    for (wIndex = 1; wIndex <= pES->taille; wIndex++)
      {
      StrDWordToStr (szText, wIndex);
      SendDlgItemMessage (hwnd, wID_TAB, CB_INSERTSTRING,
				(WPARAM)-1, (LPARAM)(szText));
      }
    if (lID_TEXT != IDC_STATIC)
			{
			::EnableWindow (::GetDlgItem (hwnd, lID_TEXT), TRUE);
			}
    ::EnableWindow (::GetDlgItem (hwnd, wID_TAB), TRUE);
    SendDlgItemMessage (hwnd, wID_TAB, CB_SETCURSEL, 0, 0);
    }
  else
    {
    bOk = FALSE;
    SetDlgItemText (hwnd, wID_TAB, "");
    if (lID_TEXT != IDC_STATIC)
			{
	    ::EnableWindow (::GetDlgItem (hwnd, lID_TEXT), FALSE);
			}
    ::EnableWindow (::GetDlgItem (hwnd, wID_TAB), FALSE);
    }

  return (bOk);
  }

//--------------------------------------------------------------------------
static void GestionUserBoutonClicked (HWND hwnd, DWORD idBouton, ENV_REFLET_LOG *pEnv)
  {
	// D�termination de l'�tat courant dont la modification a �t� demand�e
  DWORD wEtat = 0;
    switch (pEnv->wStyleAnimation)
      {
      case DLG_STYLE_COULEUR :
				{
        //wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            wEtat = (DWORD)pEnv->anCouleurs[0];
            break;
          case IDC_CUSTOM_ECH2 :
            wEtat = (DWORD)pEnv->anCouleurs[1];
            break;
          case IDC_CUSTOM_ECH3 :
            wEtat = (DWORD)pEnv->anCouleurs[2];
            break;
          }
				}
        break;
      }

	// Choix d'un nouveau style valid� par l'utilisateur ?
  if (DlgStyleAnimation (hwnd, pEnv->wStyleAnimation, &wEtat))
    {
		// oui => mise � jour du r�sultat
    switch (pEnv->wStyleAnimation)
      {
      case DLG_STYLE_COULEUR :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->anCouleurs[0] = (G_COULEUR)wEtat;
            break;
          case IDC_CUSTOM_ECH2 :
            pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
            break;
          case IDC_CUSTOM_ECH3 :
            pEnv->anCouleurs[2] = (G_COULEUR)wEtat;
            break;
          }
				SetDlgItemCouleurEchantillon (hwnd, idBouton, (G_COULEUR)wEtat);
        break;

      case DLG_STYLE_REMPLISSAGE :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_STR;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->anStylesRemplissage[0] = (G_STYLE_REMPLISSAGE)wEtat;
            break;
          case IDC_CUSTOM_ECH2 :
            pEnv->anStylesRemplissage[1] = (G_STYLE_REMPLISSAGE)wEtat;
            break;
          }
				SetDlgItemStyleRemplissageEchantillon (hwnd, idBouton, (G_STYLE_REMPLISSAGE)wEtat);
        break;

      case DLG_STYLE_LIGNE :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_STL;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->wLigne[0] = (G_STYLE_LIGNE)wEtat;
            break;
          case IDC_CUSTOM_ECH2 :
            pEnv->wLigne[1] = (G_STYLE_LIGNE)wEtat;
            break;
          }
				SetDlgItemStyleLigneEchantillon (hwnd, idBouton, (G_STYLE_LIGNE)wEtat);
        break;
      }
    ::InvalidateRect (::GetDlgItem (hwnd, idBouton), NULL, TRUE);
    }
  }

//------------------------------------------------------------------------
static void InitCouleurClignot (HWND hwnd, DWORD wEtat, DWORD wID, G_COULEUR * pnCouleur)
  {
  if (wEtat >= G_OFFSET_CLIGNOTANT)
    {
    wEtat -= G_OFFSET_CLIGNOTANT;
    ::CheckDlgButton (hwnd, wID, 1);
    }
  *pnCouleur = (G_COULEUR)wEtat;
  }

//------------------------------------------------------------------------
// Transforme le cas �ch�ant une couleur clignottante en couleur non clignottante
static void EnleveClignottementCouleur (G_COULEUR * pnCouleur)
  {
  if (((DWORD)(*pnCouleur)) >= G_OFFSET_CLIGNOTANT)
    *pnCouleur = (G_COULEUR)(((DWORD)(*pnCouleur)) - G_OFFSET_CLIGNOTANT);
  }

//------------------------------------------------------------------------
static void RecupCouleurClignot (HWND hwnd, DWORD *wEtat, DWORD wID, DWORD wCouleur)
  {
  (*wEtat) = wCouleur;
  if (IsDlgButtonChecked (hwnd, wID))
    (*wEtat) += G_OFFSET_CLIGNOTANT;
  }

// -----------------------------------------------------------------------
// Propri�t�s d'animation d'un �l�ment graphique comme reflet logique 2 �tats
static BOOL CALLBACK dlgprocGrRLog (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT						mres = 0;	          // Valeur de retour
  PENV_REFLET_LOG 	pEnv;
  DWORD             wIndice;
  DWORD             wIndex;
  DWORD             wEtat[3];
  char							szText[80]; //$$ szText
  char							szText2[80];

  switch (msg)
    {
    case WM_INITDIALOG:
			pEnv = (PENV_REFLET_LOG)pCreeEnv (hwnd, sizeof (ENV_REFLET_LOG));
      pEnv->param = *((PARAM_DLG_GR *) mp2);

			// Initialisation par d�faut
			pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
			// init couleurs
			pEnv->anCouleurs[0]     = COULEUR_HAUT_INIT;
			pEnv->anCouleurs[1]     = COULEUR_BAS_INIT;
			pEnv->anCouleurs[2]     = COULEUR_FOND_INIT; // $$
			// init remplissage
			pEnv->anStylesRemplissage[0] = REMPLISSAGE_HAUT_INIT;
			pEnv->anStylesRemplissage[1] = REMPLISSAGE_BAS_INIT;
			// init style de ligne
			pEnv->wLigne[0]       = LIGNE_HAUT_INIT;
			pEnv->wLigne[1]       = LIGNE_BAS_INIT;

			// rempli combo variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_logique, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0); 
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);

			// Analyse de la ligne de description
			if (!StrIsNull(pEnv->param.pszAnimation))
        { // decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 8, pEnv->param.pszAnimation);

        wIndice = 2;
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
        if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_2, ID_TEXT_1))
          {
          wIndice++;
          SetDlgItemText (hwnd, ID_CMB_2, TabChamps[wIndice]);
          }
        wIndice++;

        // decodages etat haut, etat bas et fond
        wEtat[0] = 0;
        wEtat[1] = 0;
        wEtat[2] = 0;
        StrToDWORD (&wEtat[0], TabChamps[wIndice + 1]);
        StrToDWORD (&wEtat[1], TabChamps[wIndice + 2]);
        StrToDWORD (&wEtat[2], TabChamps[wIndice + 3]);
        //
        bMotReserve (c_res_c, szText);
        bMotReserve (c_res_couleur, szText2);
        if (bStrEgales (TabChamps[wIndice], szText) ||
            bStrEgales (TabChamps[wIndice], szText2))
          { 
					// animation couleur
          pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
          // init couleurs
          InitCouleurClignot (hwnd, wEtat[0], ID_CB_1, &pEnv->anCouleurs[0]);
          InitCouleurClignot (hwnd, wEtat[1], ID_CB_2, &pEnv->anCouleurs[1]);
          pEnv->anCouleurs[2] = (G_COULEUR)wEtat[2];
          }
        else
          {
          bMotReserve (c_res_recoit, szText);
          bMotReserve (c_res_remplissage, szText2);
          if (bStrEgales (TabChamps[wIndice], szText) ||
              bStrEgales (TabChamps[wIndice], szText2))
            { // animation style de remplissage
            pEnv->wStyleAnimation = DLG_STYLE_REMPLISSAGE;
            // init remplissage
            pEnv->anStylesRemplissage[0] = (G_STYLE_REMPLISSAGE)wEtat[0];
            pEnv->anStylesRemplissage[1] = (G_STYLE_REMPLISSAGE)wEtat[1];
            }
          else
            {
            bMotReserve (c_res_s, szText);
            bMotReserve (c_res_style, szText2);
            if (bStrEgales (TabChamps[wIndice], szText) ||
                bStrEgales (TabChamps[wIndice], szText2))
              { // animation style de ligne
              pEnv->wStyleAnimation = DLG_STYLE_LIGNE;
              // init style de ligne
              pEnv->wLigne[0] = (G_STYLE_LIGNE)wEtat[0];
              pEnv->wLigne[1] = (G_STYLE_LIGNE)wEtat[1];
              }
            }
          }
        }

			// Init des contr�les
			switch (pEnv->wStyleAnimation)
				{
				case DLG_STYLE_COULEUR:
          SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
					SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anCouleurs[1]);
          // init radio couleur
          ::CheckDlgButton (hwnd, ID_RB_1, 1);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_1, BN_CLICKED), 0L);
					break;
				case DLG_STYLE_REMPLISSAGE:
					SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anStylesRemplissage[0]); // $$
					SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anStylesRemplissage[1]);
          // init radio remplissage
          ::CheckDlgButton (hwnd, ID_RB_2, 1);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_2, BN_CLICKED), 0L); //$$
					break;
				case DLG_STYLE_LIGNE:
					SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->wLigne[0]); // $$
					SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->wLigne[1]);
          // init radio style ligne
          ::CheckDlgButton (hwnd, ID_RB_3, 1);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_3, BN_CLICKED), 0L);
					break;
        }
      mres = FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1: // user button
        case IDC_CUSTOM_ECH2:
        //case IDC_CUSTOM_ECH3:
          pEnv = (PENV_REFLET_LOG)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
            GestionUserBoutonClicked (hwnd, LOWORD (mp1), pEnv);
          break;

        case ID_RB_1 : // couleur
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_LOG)pGetEnv (hwnd);
              //::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH3), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_4), TRUE);
              pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
							SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
							SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anCouleurs[1]);
              // inits checks box couleurs clignotantes
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_5), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_2), TRUE);
              bMotReserve (c_res_couleur, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;

            default:
              break;
            }
          break;

        case ID_RB_2 : // remplissage
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_LOG)pGetEnv (hwnd);
              pEnv->wStyleAnimation = DLG_STYLE_REMPLISSAGE;
							SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anStylesRemplissage[0]); // $$
							SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anStylesRemplissage[1]);
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_4), FALSE);
              //::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH3), FALSE);
              // inits checks box couleurs clignotantes
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_5), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_2), FALSE);
              bMotReserve (c_res_remplissage, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;

            default:
              break;
            }
          break;

        case ID_RB_3 : // style ligne
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_LOG)pGetEnv (hwnd);
              pEnv->wStyleAnimation = DLG_STYLE_LIGNE;
							SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->wLigne[0]); // $$
							SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->wLigne[1]);
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_4), FALSE);
              //::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH3), FALSE);
              // inits checks box couleurs clignotantes
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_5), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_2), FALSE);
              bMotReserve (c_res_style, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;

            default:
              break;
            }
          break;

        case ID_CMB_1 : // selection d'une variable
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_LOG)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_LOG)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
              break;
            }

          break;
        case IDOK :
          pEnv = (PENV_REFLET_LOG)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_l, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom de la variable
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // indice tableau facultatif
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          if (StrToDWORD (&wIndex, szText))
            {
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          // style d'animation et etats haut et bas
          switch (pEnv->wStyleAnimation)
            {
            case DLG_STYLE_COULEUR :
              bMotReserve (c_res_couleur, szText);
              RecupCouleurClignot (hwnd, &wEtat[0], ID_CB_1, pEnv->anCouleurs[0]);
              RecupCouleurClignot (hwnd, &wEtat[1], ID_CB_2, pEnv->anCouleurs[1]);
              wEtat[2] = pEnv->anCouleurs[2];
              break;

            case DLG_STYLE_REMPLISSAGE :
              bMotReserve (c_res_remplissage, szText);
              wEtat[0] = pEnv->anStylesRemplissage[0];
              wEtat[1] = pEnv->anStylesRemplissage[1];
              wEtat[2] = 0;
              break;

            case DLG_STYLE_LIGNE :
              bMotReserve (c_res_style, szText);
              wEtat[0] = pEnv->wLigne[0];
              wEtat[1] = pEnv->wLigne[1];
              wEtat[2] = 0;
              break;

            default :
              szText[0] = '\0';
              break;
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");

          for (wIndex = 0; wIndex < 3; wIndex++)
            {
            StrDWordToStr (szText, wEtat[wIndex]);
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

        default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  }

// -----------------------------------------------------------------------
//            GESTION DES REFLETS LOGIQUES 4 ETATS
// -----------------------------------------------------------------------
// -----------------------------------------------------------------------

//---------------------------------------------------------------------------
static void GestionUserBoutonClickedRL4 (HWND hwnd, DWORD idBouton, PENV_REFLET_LOG_4 pEnv)
  {
  DWORD wEtat = 0;

  switch (pEnv->wStyleAnimation)
    {
    case DLG_STYLE_COULEUR :
      wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
      switch (idBouton)
        {
        case IDC_CUSTOM_ECH1 :
          wEtat = (DWORD)pEnv->anCouleurs[0];
          break;
        case IDC_CUSTOM_ECH2 :
          wEtat = (DWORD)pEnv->anCouleurs[1];
          break;
        case IDC_CUSTOM_ECH3 :
          wEtat = (DWORD)pEnv->anCouleurs[2];
          break;
        case IDC_CUSTOM_ECH4 :
          wEtat = (DWORD)pEnv->anCouleurs[3];
          break;
        }
      break;
    } // switch (pEnv->wStyleAnimation)

  if (DlgStyleAnimation (hwnd, pEnv->wStyleAnimation, &wEtat))
    {
    switch (pEnv->wStyleAnimation)
      {
      case DLG_STYLE_COULEUR :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->anCouleurs[0] = (G_COULEUR)wEtat;
            break;
          case IDC_CUSTOM_ECH2 :
            pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
            break;
          case IDC_CUSTOM_ECH3 :
            pEnv->anCouleurs[2] = (G_COULEUR)wEtat;
            break;
          case IDC_CUSTOM_ECH4 :
            pEnv->anCouleurs[3] = (G_COULEUR)wEtat;
            break;
          }
				SetDlgItemCouleurEchantillon (hwnd, idBouton, (G_COULEUR)wEtat);
        break;

      case DLG_STYLE_REMPLISSAGE :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_STR;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->anStylesRemplissage[0] = (G_STYLE_REMPLISSAGE)wEtat;
            break;
          case IDC_CUSTOM_ECH2 :
            pEnv->anStylesRemplissage[1] = (G_STYLE_REMPLISSAGE)wEtat;
            break;
          case IDC_CUSTOM_ECH3 :
            pEnv->anStylesRemplissage[2] = (G_STYLE_REMPLISSAGE)wEtat;
            break;
          case IDC_CUSTOM_ECH4 :
            pEnv->anStylesRemplissage[3] = (G_STYLE_REMPLISSAGE)wEtat;
            break;
          }
				SetDlgItemStyleRemplissageEchantillon (hwnd, idBouton, (G_STYLE_REMPLISSAGE)wEtat);
        break;

      case DLG_STYLE_LIGNE :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_STL;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->wLigne[0] = (G_STYLE_LIGNE)wEtat;
            break;
          case IDC_CUSTOM_ECH2 :
            pEnv->wLigne[1] = (G_STYLE_LIGNE)wEtat;
            break;
          case IDC_CUSTOM_ECH3 :
            pEnv->wLigne[2] = (G_STYLE_LIGNE)wEtat;
            break;
          case IDC_CUSTOM_ECH4 :
            pEnv->wLigne[3] = (G_STYLE_LIGNE)wEtat;
            break;
          }
				SetDlgItemStyleLigneEchantillon (hwnd, idBouton, (G_STYLE_LIGNE)wEtat);
        break;
      } // switch (pEnv->wStyleAnimation)
    ::InvalidateRect (::GetDlgItem (hwnd, idBouton), NULL, TRUE);
    }
  }

// -----------------------------------------------------------------------
// Propri�t�s d'animation  d'un �l�ment graphique comme reflet logique 4 �tats
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrRLog4 (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT          mres = 0;	          // Valeur de retour
  PENV_REFLET_LOG_4	pEnv;
  DWORD             wIndex;
  DWORD             wIndice;
  DWORD             wEtat[5];
  char             szText[80];
  char             szText2[80];

  switch (msg)
    {
    case WM_INITDIALOG:
 			pEnv = (PENV_REFLET_LOG_4)pCreeEnv (hwnd, sizeof (ENV_REFLET_LOG_4));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
      //
			// Initialisation par d�faut
			pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
			// init couleurs
			pEnv->anCouleurs[0]     = COULEUR_BAS_INIT;
			pEnv->anCouleurs[1]     = COULEUR_BH_INIT;
			pEnv->anCouleurs[2]     = COULEUR_HB_INIT;
			pEnv->anCouleurs[3]     = COULEUR_HAUT_INIT;
			pEnv->anCouleurs[4]     = COULEUR_FOND_INIT;
			// init remplissage
			pEnv->anStylesRemplissage[0] = REMPLISSAGE_BAS_INIT;
			pEnv->anStylesRemplissage[1] = REMPLISSAGE_BH_INIT;
			pEnv->anStylesRemplissage[2] = REMPLISSAGE_HB_INIT;
			pEnv->anStylesRemplissage[3] = REMPLISSAGE_HAUT_INIT;
			// init style de ligne
			pEnv->wLigne[0]       = LIGNE_HAUT_INIT;
			pEnv->wLigne[1]       = LIGNE_BAS_INIT;
			pEnv->wLigne[2]       = LIGNE_BH_INIT;
			pEnv->wLigne[3]       = LIGNE_HB_INIT;

			// rempli combos variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_logique, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
			// 
			InitComboVariables (hwnd, &(pEnv->param), c_res_logique, ID_CMB_3, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_3, CB_SETCURSEL, 1, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, ID_TEXT_4);

			// Analyse de la ligne de description
      if (!StrIsNull(pEnv->param.pszAnimation))
        { // decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 12, pEnv->param.pszAnimation);

        wIndice = 2;
        //--------- 1� variable
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
        if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_2, ID_TEXT_1))
          {
          wIndice++;
          SetDlgItemText (hwnd, ID_CMB_2, TabChamps[wIndice]);
          }
        wIndice++;
        //--------- 2� variable
        SetDlgItemText (hwnd, ID_CMB_3, TabChamps[wIndice]);
        if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_4, ID_TEXT_4))
          {
          wIndice++;
          SetDlgItemText (hwnd, ID_CMB_4, TabChamps[wIndice]);
          }
        wIndice++;
        // decodages etat haut, etat bas et fond
        wEtat[4] = 0;
        wEtat[3] = 0;
        wEtat[2] = 0;
        wEtat[1] = 0;
        wEtat[0] = 0;
        StrToDWORD (&wEtat[3], TabChamps[wIndice + 1]);
        StrToDWORD (&wEtat[2], TabChamps[wIndice + 2]);
        StrToDWORD (&wEtat[1], TabChamps[wIndice + 3]);
        StrToDWORD (&wEtat[0], TabChamps[wIndice + 4]);
        StrToDWORD (&wEtat[4], TabChamps[wIndice + 5]);
        //
        bMotReserve (c_res_c, szText);
        bMotReserve (c_res_couleur, szText2);
        if (bStrEgales (TabChamps[wIndice], szText) ||
            bStrEgales (TabChamps[wIndice], szText2))
          { // animation couleur
          pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
          // init couleurs
          InitCouleurClignot (hwnd, wEtat[0], ID_CB_1, &pEnv->anCouleurs[0]);
          InitCouleurClignot (hwnd, wEtat[1], ID_CB_2, &pEnv->anCouleurs[1]);
          InitCouleurClignot (hwnd, wEtat[2], ID_CB_3, &pEnv->anCouleurs[2]);
          InitCouleurClignot (hwnd, wEtat[3], ID_CB_4, &pEnv->anCouleurs[3]);
          pEnv->anCouleurs[4] = (G_COULEUR)wEtat[4];
          }
        else
          {
          bMotReserve (c_res_recoit, szText);
          bMotReserve (c_res_remplissage, szText2);
          if (bStrEgales (TabChamps[wIndice], szText) ||
              bStrEgales (TabChamps[wIndice], szText2))
            { // animation style de remplissage
            pEnv->wStyleAnimation = DLG_STYLE_REMPLISSAGE;
            // init remplissage
            for (wIndex = 0; wIndex < 4; wIndex++)
              {
              pEnv->anStylesRemplissage[wIndex] = (G_STYLE_REMPLISSAGE)wEtat[wIndex];
              }
            }
          else
            {
            bMotReserve (c_res_s, szText);
            bMotReserve (c_res_style, szText2);
            if (bStrEgales (TabChamps[wIndice], szText) ||
                bStrEgales (TabChamps[wIndice], szText2))
              { // animation style de ligne
              pEnv->wStyleAnimation = DLG_STYLE_LIGNE;
              // init style de ligne
              for (wIndex = 0; wIndex < 4; wIndex++)
                {
                pEnv->wLigne[wIndex] = (G_STYLE_LIGNE)wEtat[wIndex];
                }
              }
            }
          }
        }

			// Init des contr�les
			switch (pEnv->wStyleAnimation)
				{
				case DLG_STYLE_COULEUR:
          SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
					SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anCouleurs[1]);
          SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH3,pEnv->anCouleurs[2]);
					SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH4,pEnv->anCouleurs[3]);
          // init radio couleur
          ::CheckDlgButton (hwnd, ID_RB_1, 1);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_1, BN_CLICKED), 0L); 
					break;
				case DLG_STYLE_REMPLISSAGE:
					SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anStylesRemplissage[0]);
					SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anStylesRemplissage[1]);
					SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH3,pEnv->anStylesRemplissage[2]);
					SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH4,pEnv->anStylesRemplissage[3]);
          // init radio remplissage
          ::CheckDlgButton (hwnd, ID_RB_2, 1);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_2, BN_CLICKED), 0L); 
					break;
				case DLG_STYLE_LIGNE:
					SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->wLigne[0]); 
					SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->wLigne[1]);
					SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH3,pEnv->wLigne[2]); 
					SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH4,pEnv->wLigne[3]);
          // init radio style ligne
          ::CheckDlgButton (hwnd, ID_RB_3, 1);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_3, BN_CLICKED), 0L);
					break;
        }
      mres = (LRESULT)FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
        case IDC_CUSTOM_ECH3 :
        case IDC_CUSTOM_ECH4 :
          pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedRL4 (hwnd, LOWORD (mp1), pEnv);
          break;

        case ID_RB_1 : // couleur
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_3), TRUE);
              pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
							SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
							SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anCouleurs[1]);
							SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH3,pEnv->anCouleurs[2]);
							SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH4,pEnv->anCouleurs[3]);
              // inits checks box couleurs clignotantes
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_2), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_2), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_3), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_4), TRUE);
              bMotReserve (c_res_couleur, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;

            default:
              break;
            }
          break;

        case ID_RB_2 : // remplissage
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
              pEnv->wStyleAnimation = DLG_STYLE_REMPLISSAGE;
							SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anStylesRemplissage[0]);
							SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anStylesRemplissage[1]);
							SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH3,pEnv->anStylesRemplissage[2]);
							SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH4,pEnv->anStylesRemplissage[3]);
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_3), FALSE);
              // inits checks box couleurs clignotantes
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_2), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_2), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_3), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_4), FALSE);
              bMotReserve (c_res_remplissage, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;

            default:
              break;
            }
          break;

        case ID_RB_3 : // style ligne
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
              pEnv->wStyleAnimation = DLG_STYLE_LIGNE;
							SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->wLigne[0]); 
							SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->wLigne[1]);
							SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH3,pEnv->wLigne[2]); 
							SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH4,pEnv->wLigne[3]);
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_3), FALSE);
              // inits checks box couleurs clignotantes
              ::EnableWindow (::GetDlgItem (hwnd, ID_TEXT_2), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_2), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_3), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_4), FALSE);
              bMotReserve (c_res_style, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;

            default:
              break;
            }
          break;

        case ID_CMB_1 : // selection d'une variable
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
              break;
            }

          break;

        case ID_CMB_3 : // selection d'une variable
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE :
              pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
 							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, ID_TEXT_4);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
 							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, ID_TEXT_4);
              break;
            }

          break;

        case IDOK :
          pEnv = (PENV_REFLET_LOG_4)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_l_4, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom de la variable 1
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // indice tableau facultatif
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          if (StrToDWORD (&wIndex, szText))
            {
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          // nom de la variable 2
          GetDlgItemText (hwnd, ID_CMB_3, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // indice tableau facultatif
          GetDlgItemText (hwnd, ID_CMB_4, szText, 80);
          if (StrToDWORD (&wIndex, szText))
            {
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          // style d'animation et etats haut et bas
          switch (pEnv->wStyleAnimation)
            {
            case DLG_STYLE_COULEUR :
              bMotReserve (c_res_couleur, szText);
              RecupCouleurClignot (hwnd, &wEtat[0], ID_CB_1, pEnv->anCouleurs[0]);
              RecupCouleurClignot (hwnd, &wEtat[1], ID_CB_2, pEnv->anCouleurs[1]);
              RecupCouleurClignot (hwnd, &wEtat[2], ID_CB_3, pEnv->anCouleurs[2]);
              RecupCouleurClignot (hwnd, &wEtat[3], ID_CB_4, pEnv->anCouleurs[3]);
              wEtat[4] = pEnv->anCouleurs[4];
              break;

            case DLG_STYLE_REMPLISSAGE :
              bMotReserve (c_res_remplissage, szText);
              for (wIndex = 0; wIndex < 4; wIndex++)
                {
                wEtat[wIndex] = pEnv->anStylesRemplissage[wIndex];
                }
              break;

            case DLG_STYLE_LIGNE :
              bMotReserve (c_res_style, szText);
              for (wIndex = 0; wIndex < 4; wIndex++)
                {
                wEtat[wIndex] = pEnv->wLigne[wIndex];
                }
              break;

            default :
              szText[0] = '\0';
              break;
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");

          for (wIndex = 4; wIndex > 0; wIndex--)
            {
            StrDWordToStr (szText, wEtat[wIndex - 1]);
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }

          StrDWordToStr (szText, wEtat[4]);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

				default:
          break;
        }  // switch command
      break; // WM_COMMAND

    default:
      mres = 0;
      break;
    }

  return (mres);
  }

//---------------------------------------------------------------------------
static void GestionUserBoutonClickedRNB (HWND hwnd, DWORD idBouton, PENV_REFLET_NUM	pEnv)
  {
  DWORD wEtat = 0;

  DWORD wIndexStyle = 0;

  switch (idBouton)
    {
    case IDC_CUSTOM_ECH1 :
      wIndexStyle = DLG_STYLE_COULEUR;
      wEtat = (DWORD)pEnv->anCouleurs[0];
      break;
    case IDC_CUSTOM_ECH2 :
      wIndexStyle = DLG_STYLE_REMPLISSAGE;
      break;
    case IDC_CUSTOM_ECH3 :
      wIndexStyle = DLG_STYLE_COULEUR;
      wEtat = (DWORD)pEnv->anCouleurs[1];
		  break;
    } // switch (idBouton)

  if (DlgStyleAnimation (hwnd, wIndexStyle, &wEtat))
    {
    switch (idBouton)
      {
      case IDC_CUSTOM_ECH1 :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
        pEnv->anCouleurs[0] = (G_COULEUR)wEtat;
		    SetDlgItemCouleurEchantillon (hwnd, idBouton,(G_COULEUR)wEtat);
        break;
      case IDC_CUSTOM_ECH2 :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_STR;
        pEnv->wLigne = (G_STYLE_LIGNE)wEtat;
		    SetDlgItemStyleRemplissageEchantillon (hwnd, idBouton,(G_STYLE_REMPLISSAGE)wEtat);
        break;
      case IDC_CUSTOM_ECH3 :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
		    SetDlgItemCouleurEchantillon (hwnd, idBouton,(G_COULEUR)wEtat);
        break;
      } // switch (idBouton)
    }
  }

// -------------------------------------------------------------------------
// Propri�t�s d'un objet Bargraphe
// -------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrBargraphe (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT						mres = 0;	          // Valeur de retour
  PENV_REFLET_NUM 	pEnv;
  DWORD             wIndice;
  DWORD             wIndex;
  DWORD             wEtat;
  char							szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);

			// Init par d�faut :
			// init couleurs
			pEnv->anCouleurs[0]  = COULEUR_HAUT_INIT;
			pEnv->anCouleurs[1]  = COULEUR_FOND_INIT;
			// init remplissage
			pEnv->wLigne = (G_STYLE_LIGNE)REMPLISSAGE_HAUT_INIT; //$$ Bof

			// rempli combo variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);

      if (!StrIsNull(pEnv->param.pszAnimation))
        { // decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 10, pEnv->param.pszAnimation);

        wIndice = 2;
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
        if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_2, ID_TEXT_1))
          {
          wIndice++;
          SetDlgItemText (hwnd, ID_CMB_2, TabChamps[wIndice]);
          }
        wIndice++;

        // init Modele
        bMotReserve (c_res_up, szText);
        if (bStrEgales (TabChamps[wIndice], szText))
          { // Haut : init radio 1
					::CheckDlgButton (hwnd, ID_RB_1, BST_CHECKED);
          }
        else
          {
          bMotReserve (c_res_down, szText);
          if (bStrEgales (TabChamps[wIndice], szText))
            { // Bas : init radio 2
            ::CheckDlgButton (hwnd, ID_RB_2, BST_CHECKED);
            }
          else
            {
            bMotReserve (c_res_right, szText);
            if (bStrEgales (TabChamps[wIndice], szText))
              { // Droit : init radio 3
              ::CheckDlgButton (hwnd, ID_RB_3, BST_CHECKED);
              }
            else
              { // Gauche : init radio 4
              ::CheckDlgButton (hwnd, ID_RB_4, BST_CHECKED);
              }
            }
          }
        wIndice++;
        // init couleur
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[wIndice++]);
        InitCouleurClignot (hwnd, wEtat, ID_CB_1, &pEnv->anCouleurs[0]);
        // init style remplissage
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[wIndice++]);
        pEnv->wLigne = (G_STYLE_LIGNE)wEtat;
        // init Val Min
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[wIndice++]);
        // init Val Max
        SetDlgItemText (hwnd, ID_EF_2, TabChamps[wIndice++]);
        // init couleur de fond
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[wIndice]);
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
        }
 			// Init des contr�les
      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
      SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH2,(G_STYLE_REMPLISSAGE)pEnv->wLigne);
      mres = FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
        case IDC_CUSTOM_ECH3 :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedRNB (hwnd, LOWORD (mp1), pEnv);
          break;

        case ID_CMB_1 : // selection d'une variable
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
              break;
            }

          break;

        case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_bargraph, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom de la variable
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // indice tableau facultatif
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          if (StrToDWORD (&wIndex, szText))
            {
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          // Modele
          wIndex =  nCheckIndex (hwnd, ID_RB_1); //$$(LONG) SendDlgItemMessage (hwnd, ID_RB_1, BM_QUERYCHECKINDEX, 0L, 0L);
          switch (wIndex)
            {
            case 1 : // Haut
              bMotReserve (c_res_up, szText);
              break;
            case 2 : // Bas
              bMotReserve (c_res_down, szText);
              break;
            case 3 : // Droit
              bMotReserve (c_res_right, szText);
              break;
            case 4 : // Gauche
              bMotReserve (c_res_left, szText);
              break;
            default :
              break;
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur
          RecupCouleurClignot (hwnd, &wEtat, ID_CB_1, pEnv->anCouleurs[0]);
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // style de remplissage
          StrDWordToStr (szText, pEnv->wLigne);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val Min
          GetDlgItemText (hwnd, ID_EF_1, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val Max
          GetDlgItemText (hwnd, ID_EF_2, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur de fond
          wEtat = pEnv->anCouleurs[1];
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

        default:
          break;
        }  // switch command
      break;


    default:
      mres = 0;
      break;
    }

  return (mres);
  }

// -----------------------------------------------------------------------
//            GESTION DES COURBES TEMPORELLES
// -----------------------------------------------------------------------

//---------------------------------------------------------------------------
static void GestionUserBoutonClickedRNC (HWND hwnd, DWORD idBouton, PENV_REFLET_NUM	pEnv)
  {
  DWORD wEtat = 0;
  DWORD wIndexStyle = 0;

    switch (idBouton)
      {
      case IDC_CUSTOM_ECH1 :
				wIndexStyle = DLG_STYLE_COULEUR;
        wEtat = (DWORD)pEnv->anCouleurs[0];
        break;
      case IDC_CUSTOM_ECH2 :
				wIndexStyle = DLG_STYLE_LIGNE;
        break;
      case IDC_CUSTOM_ECH3 :
				wIndexStyle = DLG_STYLE_COULEUR;
        wEtat = (DWORD)pEnv->anCouleurs[1];
        break;
      }

  if (DlgStyleAnimation (hwnd, wIndexStyle, &wEtat))
    {
    switch (idBouton)
      {
      case IDC_CUSTOM_ECH1 :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
        pEnv->anCouleurs[0] = (G_COULEUR)wEtat;
				SetDlgItemCouleurEchantillon (hwnd, idBouton,(G_COULEUR)wEtat);
        break;
      case IDC_CUSTOM_ECH2 :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_STL;
        pEnv->wLigne = (G_STYLE_LIGNE)wEtat;
				SetDlgItemStyleLigneEchantillon (hwnd, idBouton,(G_STYLE_LIGNE)wEtat);
        break;
      case IDC_CUSTOM_ECH3 :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
				SetDlgItemCouleurEchantillon (hwnd, idBouton,(G_COULEUR)wEtat);
        break;
      }
    }
  }

//-------------------------------------------------------------------------
// Propri�t�s d'un objet Courbe avec �chantillonnage en fr�quence
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrCourbeTemps (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT       mres = 0;
  PENV_REFLET_NUM	pEnv;
  DWORD         wEtat;
  char          szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
			// init couleurs
			pEnv->anCouleurs[0] = COULEUR_HAUT_INIT;
			pEnv->anCouleurs[1] = COULEUR_FOND_INIT;
			// init remplissage
			pEnv->wLigne = LIGNE_HAUT_INIT;

			// rempli combo variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_1, FALSE);
			// selectionne la premiere var
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
			// init radio Haut
			::CheckDlgButton (hwnd, ID_RB_1, 1);

      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// d�composer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 13, pEnv->param.pszAnimation);
        // init var log echantillonnage
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[2]);
        // init var num a echantillonner
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[3]);
        // init nbre ech
        SetDlgItemText (hwnd, ID_EF_2, TabChamps[4]);
        // init periode ech
        SetDlgItemText (hwnd, ID_EF_3, TabChamps[5]);
        // init couleur
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[6]);
        InitCouleurClignot (hwnd, wEtat, ID_CB_1, &pEnv->anCouleurs[0]);
        // init style remplissage
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[7]);
        pEnv->wLigne = (G_STYLE_LIGNE)wEtat;
        // init Val Min
        SetDlgItemText (hwnd, ID_EF_4, TabChamps[8]);
        // init Val Max
        SetDlgItemText (hwnd, ID_EF_5, TabChamps[9]);
        // init nbre pts scroll
        SetDlgItemText (hwnd, ID_EF_6, TabChamps[10]);
        // val init H/B
        bMotReserve (c_res_zero, szText);
        if (bStrEgales (TabChamps[11], szText))
          { // valeur initiale Bas
          ::CheckDlgButton (hwnd, ID_RB_2, 1);
          }
        // init couleur fond
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[12]);
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
        }
			// Init des contr�les
      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
      SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->wLigne);

      mres = (LRESULT)FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
        case IDC_CUSTOM_ECH3 :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedRNC (hwnd, LOWORD (mp1), pEnv);
          break;

        case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_c_temps, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // var log ech
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // variable num a ech
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nbre ech
          GetDlgItemText (hwnd, ID_EF_2, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // periode ech
          GetDlgItemText (hwnd, ID_EF_3, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur
          RecupCouleurClignot (hwnd, &wEtat, ID_CB_1, pEnv->anCouleurs[0]);
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // style de remplissage
          StrDWordToStr (szText, pEnv->wLigne);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val Min
          GetDlgItemText (hwnd, ID_EF_4, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val Max
          GetDlgItemText (hwnd, ID_EF_5, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nbre pts scroll
          GetDlgItemText (hwnd, ID_EF_6, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          if (IsDlgButtonChecked (hwnd, ID_RB_1))
            {
            bMotReserve (c_res_un, szText);
            }
          else
            {
            bMotReserve (c_res_zero, szText);
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur
          wEtat = pEnv->anCouleurs[1];
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

        default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  }

// -----------------------------------------------------------------------
//            GESTION DES COURBES COMMANDES
// -----------------------------------------------------------------------
//-------------------------------------------------------------------------
// Propri�t�s d'un objet Courbe avec �chantillonnage sur �v�nement
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrCourbeCommande (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT       mres = 0;
  PENV_REFLET_NUM pEnv;
  DWORD          wEtat;
  char          szText[80];
  
  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
			// init couleurs
			pEnv->anCouleurs[0] = COULEUR_HAUT_INIT;
			pEnv->anCouleurs[1] = COULEUR_FOND_INIT;
			// init remplissage
			pEnv->wLigne = LIGNE_HAUT_INIT;

			// rempli combo variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_1, FALSE);
			// selectionne la premiere var
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
			// rempli combo variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_logique, ID_CMB_2, FALSE);
			// selectionne la premiere var
			SendDlgItemMessage (hwnd, ID_CMB_2, CB_SETCURSEL, 0, 0);
			// init radio Haut
			::CheckDlgButton (hwnd, ID_RB_1, 1);

			// inits boutons users
      if (!StrIsNull(pEnv->param.pszAnimation))
        { // decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 13, pEnv->param.pszAnimation);
        // init var log reset
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[2]);
        // init var num a echantillonner
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[3]);
        // init nbre ech
        SetDlgItemText (hwnd, ID_EF_2, TabChamps[4]);
        // init var log cde echantillonnage
        SetDlgItemText (hwnd, ID_CMB_2, TabChamps[5]);
        // init couleur
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[6]);
        InitCouleurClignot (hwnd, wEtat, ID_CB_1, &pEnv->anCouleurs[0]);
        // init style remplissage
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[7]);
        pEnv->wLigne = (G_STYLE_LIGNE)wEtat;
        // init Val Min
        SetDlgItemText (hwnd, ID_EF_4, TabChamps[8]);
        // init Val Max
        SetDlgItemText (hwnd, ID_EF_5, TabChamps[9]);
        // init nbre pts scroll
        SetDlgItemText (hwnd, ID_EF_6, TabChamps[10]);
        // val init H/B
        bMotReserve (c_res_zero, szText);
        if (bStrEgales (TabChamps[11], szText))
          { // valeur initiale Bas
          ::CheckDlgButton (hwnd, ID_RB_2, 1);
          }
        // init couleur fond
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[12]);
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
        }
			// Init des contr�les
      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
      SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->wLigne);

      mres = (LRESULT)FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
        case IDC_CUSTOM_ECH3 :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedRNC (hwnd, LOWORD (mp1), pEnv);
          break;

        case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_c_commande, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // var log reset
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // variable num a ech
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nbre ech
          GetDlgItemText (hwnd, ID_EF_2, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // variable log cde ech
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur
          RecupCouleurClignot (hwnd, &wEtat, ID_CB_1, pEnv->anCouleurs[0]);
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // style de remplissage
          StrDWordToStr (szText, pEnv->wLigne);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val Min
          GetDlgItemText (hwnd, ID_EF_4, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val Max
          GetDlgItemText (hwnd, ID_EF_5, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nbre pts scroll
          GetDlgItemText (hwnd, ID_EF_6, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          if (IsDlgButtonChecked (hwnd, ID_RB_1))
            {
            bMotReserve (c_res_un, szText);
            }
          else
            {
            bMotReserve (c_res_zero, szText);
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur de fond
          wEtat = pEnv->anCouleurs[1];
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

        default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  }

// -----------------------------------------------------------------------
//            GESTION DES COURBES ARCHIVAGES
// -----------------------------------------------------------------------
static void DefautRNCA (HWND hwnd, PENV_REFLET_NUM pEnv)
  {
  DWORD           wIndex;
  char            szNomVar[c_nb_car_es];
  PENTREE_SORTIE	pES;

  // rempli combos variables avec les entr�es sorties disponibles,
  for (wIndex = 2; wIndex <= pEnv->param.wNbVar; wIndex++)
    {
    pES = (PENTREE_SORTIE)pointe_enr (szVERIFSource, __LINE__, b_e_s, wIndex);
    if ((pES->sens == c_res_e) || (pES->sens == c_res_s) || (pES->sens == c_res_e_et_s))
      { 
			// variables simples dispatch�es dans le bon type de combo box
      NomES (szNomVar, pES->i_identif);
      switch (pES->genre)
        {
        case c_res_logique :
          SendDlgItemMessage (hwnd, ID_CMB_11, CB_ADDSTRING, 0, (LPARAM)(szNomVar));
          break;

        case c_res_numerique :
          SendDlgItemMessage (hwnd, ID_CMB_2, CB_ADDSTRING, 0, (LPARAM)szNomVar);
          SendDlgItemMessage (hwnd, ID_CMB_3, CB_ADDSTRING, 0, (LPARAM)szNomVar);
          SendDlgItemMessage (hwnd, ID_CMB_4, CB_ADDSTRING, 0, (LPARAM)szNomVar);
          SendDlgItemMessage (hwnd, ID_CMB_5, CB_ADDSTRING, 0, (LPARAM)szNomVar);
          SendDlgItemMessage (hwnd, ID_CMB_6, CB_ADDSTRING, 0, (LPARAM)szNomVar);
          SendDlgItemMessage (hwnd, ID_CMB_7, CB_ADDSTRING, 0, (LPARAM)szNomVar);
          SendDlgItemMessage (hwnd, ID_CMB_8, CB_ADDSTRING, 0, (LPARAM)szNomVar);
          SendDlgItemMessage (hwnd, ID_CMB_9, CB_ADDSTRING, 0, (LPARAM)szNomVar);
          SendDlgItemMessage (hwnd, ID_CMB_10,CB_ADDSTRING, 0, (LPARAM)szNomVar);
          break;

        case c_res_message :
          SendDlgItemMessage (hwnd, ID_CMB_1, CB_ADDSTRING, 0, (LPARAM) szNomVar);
          break;
// $$ v�rifier si normal qu'il n'y soit pas => default : VerifWarningExit; break;
        }
      }
    }
	// selectionne la premiere var de chaque combo box
  SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_2, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_3, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_4, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_5, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_6, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_7, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_8, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_9, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_10, CB_SETCURSEL, 0, 0);
	SendDlgItemMessage (hwnd, ID_CMB_11, CB_SETCURSEL, 0, 0);
  }

//-------------------------------------------------------------------------
// Affiche et pr�selectionne un nom de variable dans une combo box
static void InitSelectionVariableDansCombo (HWND hwnd, DWORD dwId, PCSTR pszNomVar)
	{
	DWORD dwIndex = SendDlgItemMessage (hwnd, dwId , CB_FINDSTRING, (WPARAM) -1, (LPARAM)pszNomVar);

	if (dwIndex != CB_ERR)
		SendDlgItemMessage (hwnd, dwId , CB_SETCURSEL, (WPARAM)dwIndex, 0);
	SetDlgItemText (hwnd, dwId , pszNomVar);
	}

//-------------------------------------------------------------------------
// Propri�t�s d'un objet Courbe �labor�e � partir de fichiers
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrCourbeArchive (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT       mres = 0;	          // Valeur de retour
  PENV_REFLET_NUM	pEnv;
  char          szText[80];

  switch (msg)
    {
    case WM_INITDIALOG :
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);

			// Initialise les combos box avec les valeurs par d�faut
      DefautRNCA (hwnd, pEnv);

			// Il existe une animation ?
      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// oui => d�composer la chaine d'animation et initialiser les combo box
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);

        wNbChamps = StrToStrArray (TabChamps, 13, pEnv->param.pszAnimation);

        // init des s�lections des champs des combo boxes des variables
        InitSelectionVariableDansCombo (hwnd, ID_CMB_1 , TabChamps[2] );
        InitSelectionVariableDansCombo (hwnd, ID_CMB_2 , TabChamps[3] );
        InitSelectionVariableDansCombo (hwnd, ID_CMB_3 , TabChamps[4] );
        InitSelectionVariableDansCombo (hwnd, ID_CMB_4 , TabChamps[5] );
        InitSelectionVariableDansCombo (hwnd, ID_CMB_5 , TabChamps[6] );
        InitSelectionVariableDansCombo (hwnd, ID_CMB_6 , TabChamps[7] );
        InitSelectionVariableDansCombo (hwnd, ID_CMB_7 , TabChamps[8] );
        InitSelectionVariableDansCombo (hwnd, ID_CMB_8 , TabChamps[9] );
        InitSelectionVariableDansCombo (hwnd, ID_CMB_9 , TabChamps[10]);
        InitSelectionVariableDansCombo (hwnd, ID_CMB_10, TabChamps[11]);
        InitSelectionVariableDansCombo (hwnd, ID_CMB_11, TabChamps[12]);
        }
      mres = FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_c_archivage, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // gestion variables
          GetDlgItemText (hwnd, ID_CMB_1 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_2 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_3 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_4 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_5 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_6 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_7 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_8 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_9 , szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_10, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          GetDlgItemText (hwnd, ID_CMB_11, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

        default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return mres;
  }

// -----------------------------------------------------------------------
//            GESTION DES REFLETS DEPLACEMENTS
// -----------------------------------------------------------------------
typedef struct
	{
	PARAM_DLG_GR param;
	DWORD								wStyleAnimation;
	G_COULEUR						anCouleurs[2];
	G_STYLE_REMPLISSAGE	anStylesRemplissage;
	G_STYLE_LIGNE       wLigne;
	} ENV_REFLET_NUM_DEPLACE, *PENV_REFLET_NUM_DEPLACE;

//---------------------------------------------------------------------------
static void GestionUserBoutonClickedRND (HWND hwnd, DWORD idBouton, ENV_REFLET_NUM_DEPLACE *pEnv)
  {
  DWORD wEtat = 0;

  switch (pEnv->wStyleAnimation)
    {
    case DLG_STYLE_COULEUR :
      switch (idBouton)
        {
        case IDC_CUSTOM_ECH1 :
          wEtat = (DWORD)pEnv->anCouleurs[0];
          break;
        case IDC_CUSTOM_ECH2 :
          wEtat = (DWORD)pEnv->anCouleurs[1];
          break;
        }
      break;
    } // switch (pEnv->wStyleAnimation)
  
  if (DlgStyleAnimation (hwnd, pEnv->wStyleAnimation, &wEtat))
    {
    switch (pEnv->wStyleAnimation)
      {
      case DLG_STYLE_COULEUR :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->anCouleurs[0] = (G_COULEUR)wEtat;
            break;
          case IDC_CUSTOM_ECH2 :
            pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
            break;
          }
				SetDlgItemCouleurEchantillon (hwnd, idBouton, (G_COULEUR)wEtat);
        break;

      case DLG_STYLE_REMPLISSAGE :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_STR;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->anStylesRemplissage = (G_STYLE_REMPLISSAGE)wEtat;
            break;
          }
				SetDlgItemStyleRemplissageEchantillon (hwnd, idBouton, (G_STYLE_REMPLISSAGE)wEtat);
        break;

      case DLG_STYLE_LIGNE :
        wEtat = wEtat - CMD_MENU_BMP_OFFSET_STL;
        switch (idBouton)
          {
          case IDC_CUSTOM_ECH1 :
            pEnv->wLigne = (G_STYLE_LIGNE)wEtat;
            break;
          }
				SetDlgItemStyleLigneEchantillon (hwnd, idBouton, (G_STYLE_LIGNE)wEtat);
        break;
      } // switch (pEnv->wStyleAnimation)
    } // if (DlgStyleAnimation (hwnd, pEnv->wStyleAnimation, &wEtat))
  }

// -----------------------------------------------------------------------
// Propri�t�s d'animation d'un �l�ment graphique par d�placement selon 2 var num.
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrDeplace (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT          bRes = FALSE;	          // Valeur de retour
  PENV_REFLET_NUM_DEPLACE	pEnv;
  DWORD             wIndex;
  DWORD             wIndice;
  DWORD             wEtat;
  char             szText[80];
  char             szText2[80];
  BOOL          bConstante;
  
  switch (msg)
    {
    case WM_INITDIALOG:
			{
      pEnv = (PENV_REFLET_NUM_DEPLACE)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM_DEPLACE));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
			pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
			// init couleurs
			pEnv->anCouleurs[0]     = COULEUR_HAUT_INIT;
			pEnv->anCouleurs[1]     = COULEUR_FOND_INIT;
			// init remplissage
			pEnv->anStylesRemplissage    = REMPLISSAGE_HAUT_INIT;
			// init style de ligne
			pEnv->wLigne          = LIGNE_HAUT_INIT;

			// rempli combos variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
			// 
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_3, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_3, CB_SETCURSEL, 1, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, ID_TEXT_2);
			//
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_5, FALSE);
			SendDlgItemMessage (hwnd, ID_CMB_5, CB_SETCURSEL, 3, 0);
			::EnableWindow (::GetDlgItem (hwnd, ID_CMB_5), FALSE);

			//
      if (!StrIsNull(pEnv->param.pszAnimation))
        { // decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 9, pEnv->param.pszAnimation);

        wIndice = 2;
        // variable dx
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
        if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_2, ID_TEXT_1))
          {
          wIndice++;
          SetDlgItemText (hwnd, ID_CMB_2, TabChamps[wIndice]);
          }
        wIndice++;
        // variable dy
        SetDlgItemText (hwnd, ID_CMB_3, TabChamps[wIndice]);
        if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_4, ID_TEXT_2))
          {
          wIndice++;
          SetDlgItemText (hwnd, ID_CMB_4, TabChamps[wIndice]);
          }
        wIndice++;
        // decodages etat animation
        wEtat = 0;
        if (StrToDWORD (&wEtat, TabChamps[wIndice + 1]))
          { // c'est une constante numerique
          bConstante = TRUE;
          }
        else
          { // c'est une variable
          bConstante = FALSE;
          SetDlgItemText (hwnd, ID_CMB_5, TabChamps[wIndice + 1]);
          }

        // style d'animation
        bMotReserve (c_res_c, szText);
        bMotReserve (c_res_couleur, szText2);
        if (bStrEgales (TabChamps[wIndice], szText) ||
            bStrEgales (TabChamps[wIndice], szText2))
          { // animation couleur
          pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
          // init couleurs
          if (bConstante)
            {
            InitCouleurClignot (hwnd, wEtat, ID_CB_1, &pEnv->anCouleurs[0]);
			      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
            ::CheckDlgButton (hwnd, ID_RB_4, 1);
            ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_4, BN_CLICKED), 0);
            }
					else
						{
					  SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,G_PALEGRAY);
            ::CheckDlgButton (hwnd, ID_RB_5, 1);
            ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_5, BN_CLICKED), 0);
						}
          ::CheckDlgButton (hwnd, ID_RB_1, 1);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_1, BN_CLICKED), 0);
          }
        else
          {
          bMotReserve (c_res_recoit, szText);
          bMotReserve (c_res_remplissage, szText2);
          if (bStrEgales (TabChamps[wIndice], szText) ||
              bStrEgales (TabChamps[wIndice], szText2))
            { // animation style de remplissage
            pEnv->wStyleAnimation = DLG_STYLE_REMPLISSAGE;
            // init remplissage
            if (bConstante)
              {
              pEnv->anStylesRemplissage = (G_STYLE_REMPLISSAGE)wEtat;
				      SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anStylesRemplissage);
							::CheckDlgButton (hwnd, ID_RB_4, 1);
							::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_4, BN_CLICKED), 0);
              }
						else
							{
					    SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,G_PALEGRAY);
							::CheckDlgButton (hwnd, ID_RB_5, 1);
							::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_5, BN_CLICKED), 0);
							}
            // init radio remplissage
            ::CheckDlgButton (hwnd, ID_RB_2, 1);
            ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_2, BN_CLICKED), 0);
            }
          else
            {
            bMotReserve (c_res_s, szText);
            bMotReserve (c_res_style, szText2);
            if (bStrEgales (TabChamps[wIndice], szText) ||
                bStrEgales (TabChamps[wIndice], szText2))
              { // animation style de ligne
              pEnv->wStyleAnimation = DLG_STYLE_LIGNE;
              // init style de ligne
              if (bConstante)
                {
                pEnv->wLigne = (G_STYLE_LIGNE)wEtat;
					      SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->wLigne);
								::CheckDlgButton (hwnd, ID_RB_4, 1);
								::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_4, BN_CLICKED), 0);
                }
							else
								{
					      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,G_PALEGRAY);
								::CheckDlgButton (hwnd, ID_RB_5, 1);
								::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_5, BN_CLICKED), 0);
								}
              // init radio style ligne
              ::CheckDlgButton (hwnd, ID_RB_3, 1);
              ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_3, BN_CLICKED), 0L);
              }
            }
          }
        // couleur de fond
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[wIndice + 2]);
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
        }
			else
				{
				::CheckDlgButton (hwnd, ID_RB_1, 1);
				::CheckDlgButton (hwnd, ID_RB_4, 1);
				}


      bRes = FALSE;
			}
      break;

    case WM_COMMAND:
			{
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
					{
          pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedRND (hwnd, LOWORD (mp1), pEnv);
					}
          break;

        case ID_RB_1 : // couleur
					{
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
              pEnv->wStyleAnimation = DLG_STYLE_COULEUR;
              if (IsDlgButtonChecked (hwnd, ID_RB_4))
                { // constante
                ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), TRUE);
					      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
                }
              GetDlgItemText (hwnd, ID_RB_1, szText, 80); //bMotReserve (c_res_couleur, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;
            }
					}
          break;

        case ID_RB_2 : // remplissage
					{
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
              pEnv->wStyleAnimation = DLG_STYLE_REMPLISSAGE;
              if (IsDlgButtonChecked (hwnd, ID_RB_4))
                { // constante
                ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
					      SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anStylesRemplissage);
                // inits checks box couleurs clignotantes
                }
              GetDlgItemText (hwnd, ID_RB_2, szText, 80); //bMotReserve (c_res_remplissage, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;
            }
					}
          break; // ID_RB_2

        case ID_RB_3 : // style ligne
					{
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
              pEnv->wStyleAnimation = DLG_STYLE_LIGNE;
              if (IsDlgButtonChecked (hwnd, ID_RB_4))
                { // constante
					      SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->wLigne);
                // inits checks box couleurs clignotantes
                ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
                }
              GetDlgItemText (hwnd, ID_RB_3, szText, 80); //bMotReserve (c_res_style, szText);
              SetDlgItemText (hwnd, ID_GRP_2, szText);
              break;
            }
					}
          break; // ID_RB_3

        case ID_RB_4 : // constante
					{
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_5), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH1), TRUE);
							switch (pEnv->wStyleAnimation)
								{
								case DLG_STYLE_COULEUR:
						      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
									break;
								case DLG_STYLE_REMPLISSAGE:
						      SetDlgItemStyleRemplissageEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anStylesRemplissage);
									break;
								case DLG_STYLE_LIGNE:
						      SetDlgItemStyleLigneEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->wLigne);
									break;
								}
              // inits checks box couleurs clignotantes
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), (pEnv->wStyleAnimation == DLG_STYLE_COULEUR));
              break;
            }
					}
          break;

        case ID_RB_5 : // variable
					{
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_5), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH1), FALSE);
              //::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH2), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
              break;
            }
					}
          break;

        case ID_CMB_1 : // selection d'une variable
					{
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, ID_TEXT_1);
              break;
            }
					}
          break;

        case ID_CMB_3 : // selection d'une variable
					{
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, ID_TEXT_2);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, ID_TEXT_2);
              break;
            }
					}
          break; // ID_CMB_3

        case IDOK :
					{
          pEnv = (PENV_REFLET_NUM_DEPLACE)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_l_deplacement, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom de la variable 1
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // indice tableau facultatif
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          if (StrToDWORD (&wIndex, szText))
            {
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          // nom de la variable 2
          GetDlgItemText (hwnd, ID_CMB_3, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // indice tableau facultatif
          GetDlgItemText (hwnd, ID_CMB_4, szText, 80);
          if (StrToDWORD (&wIndex, szText))
            {
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          // style d'animation et etats haut et bas
          switch (pEnv->wStyleAnimation)
            {
            case DLG_STYLE_COULEUR :
              bMotReserve (c_res_couleur, szText);
              RecupCouleurClignot (hwnd, &wEtat, ID_CB_1, pEnv->anCouleurs[0]);
              break;

            case DLG_STYLE_REMPLISSAGE :
              bMotReserve (c_res_remplissage, szText);
              wEtat = pEnv->anStylesRemplissage;
              break;

            case DLG_STYLE_LIGNE :
              bMotReserve (c_res_style, szText);
              wEtat = pEnv->wLigne;
              break;

            default :
              szText[0] = '\0';
              break;
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // variable ou constante
          if (IsDlgButtonChecked (hwnd, ID_RB_4))
            { // constante
            StrDWordToStr (szText, wEtat);
            }
          else
            { // variable
            GetDlgItemText (hwnd, ID_CMB_5, szText, 80);
            }
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur de fond
          wEtat = pEnv->anCouleurs[1];
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
					}
          break; //IDOK

        case IDB_SUPPRIMER :
        case IDCANCEL :
					{
          EndDialog (hwnd, LOWORD(mp1));
					}
          break;

        default:
          break;
        }  // switch command
			} // WM_COMMAND
      break;


    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;
    }

  return bRes;
  }

// -----------------------------------------------------------------------
//            GESTION DES ENTREES NUMERIQUES
// -----------------------------------------------------------------------
static void DefautEN (HWND hwnd, PENV_REFLET_NUM pEnv)
  {
  // init styles (boites a cocher)
  ::CheckDlgButton (hwnd, ID_CB_2, 1);
  ::CheckDlgButton (hwnd, ID_CB_3, 1);
  // init couleurs
  pEnv->anCouleurs[0] = COULEUR_HAUT_INIT;
  pEnv->anCouleurs[1] = COULEUR_FOND_INIT;
  }

//---------------------------------------------------------------------------
static void GestionUserBoutonClickedERNM (HWND hwnd, DWORD idBouton, PENV_REFLET_NUM pEnv)
  {
  DWORD wEtat = 0;
  switch (idBouton)
    {
    case IDC_CUSTOM_ECH1 :
      wEtat = (DWORD)pEnv->anCouleurs[0];
      break;
    case IDC_CUSTOM_ECH2 :
      wEtat = (DWORD)pEnv->anCouleurs[1];
      break;
    }

  if (DlgStyleAnimation (hwnd, DLG_STYLE_COULEUR, &wEtat))
    {
    wEtat = wEtat - CMD_MENU_BMP_OFFSET_COULEUR;
    switch (idBouton)
      {
      case IDC_CUSTOM_ECH1 :
        pEnv->anCouleurs[0] = (G_COULEUR)wEtat;
        break;
      case IDC_CUSTOM_ECH2 :
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
        break;
      }
		SetDlgItemCouleurEchantillon (hwnd, idBouton, (G_COULEUR)wEtat);
    }
  }

//-------------------------------------------------------------------------
// Propri�t�s d'un objet Entr�e Num�rique
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocAnimationEntreeNumerique (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT       mres = 0;
  PENV_REFLET_NUM pEnv;
  DWORD         wEtat;
  char          szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
      DefautEN (hwnd, pEnv);
      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				int nNbCars = 0;

				// Prise en compte de la chaine de description de l'animation
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 13, pEnv->param.pszAnimation);
        // nom var
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[2]);
        // init couleur
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[3]);
				pEnv->anCouleurs[0] = (G_COULEUR)wEtat;

        // init nbre car
        StrToINT (&nNbCars, TabChamps[4]);
				// $$ ex clignotant InitCouleurClignot (hwnd, wEtat, ID_CB_1, &pEnv->anCouleurs[0]);
				EnleveClignottementCouleur(&pEnv->anCouleurs[0]);
				if (nNbCars < 0)
					{
					// Notation scientifique
					::CheckDlgButton (hwnd, ID_CB_1, TRUE);
					nNbCars = - nNbCars;
					}
        SetDlgItemInt (hwnd, ID_EF_3, nNbCars, TRUE);

        // init Val Min
        SetDlgItemText (hwnd, ID_EF_4, TabChamps[5]);
        // init Val Max
        SetDlgItemText (hwnd, ID_EF_5, TabChamps[6]);
        // val init
        SetDlgItemText (hwnd, ID_EF_6, TabChamps[7]);

        // gestion des styles (boites a cocher)
        StrToDWORD (&wEtat, TabChamps[8]);
        ::CheckDlgButton (hwnd, ID_CB_2, wEtat);
        //
        StrToDWORD (&wEtat, TabChamps[9]);
        ::CheckDlgButton (hwnd, ID_CB_4, wEtat);
        //
        StrToDWORD (&wEtat, TabChamps[10]);
        ::CheckDlgButton (hwnd, ID_CB_3, wEtat);

        // couleur de fond
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[11]);
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;

        // Nombre de d�cimales
        SetDlgItemText (hwnd, ID_EF_7, TabChamps[12]);
        }

			// Init des contr�les
      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anCouleurs[1]);

      mres = (LRESULT)FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedERNM (hwnd, LOWORD (mp1), pEnv);
          break;

				case IDOK :
					{
					int nNbCars = 0;

          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_e_n, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom var
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur
          //RecupCouleurClignot (hwnd, &wEtat, ID_CB_1, pEnv->anCouleurs[0]);
          //StrDWordToStr (szText, wEtat, 10);
          StrDWordToStr (szText, pEnv->anCouleurs[0]);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nbr car n�gatif si notation scientifique
          GetDlgItemText (hwnd, ID_EF_3, szText, 32);
					if (StrToINT (&nNbCars, szText))
						{
						if (IsDlgButtonChecked (hwnd, ID_CB_1) == BST_CHECKED)
							nNbCars = - nNbCars;
						}
					StrINTToStr (szText, nNbCars);
					StrConcat (pEnv->param.pszAnimation, szText);
					StrConcat (pEnv->param.pszAnimation, " ");
					// Val Min
          GetDlgItemText (hwnd, ID_EF_4, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val Max
          GetDlgItemText (hwnd, ID_EF_5, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val init
          GetDlgItemText (hwnd, ID_EF_6, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // gestion des boites a cocher
          wEtat = (IsDlgButtonChecked (hwnd, ID_CB_2) == BST_CHECKED);
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          //
          wEtat = (IsDlgButtonChecked (hwnd, ID_CB_4) == BST_CHECKED);
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          //
          wEtat = (IsDlgButtonChecked (hwnd, ID_CB_3) == BST_CHECKED);
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur de fond
          wEtat = pEnv->anCouleurs[1];
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Nombre de d�cimales
          GetDlgItemText (hwnd, ID_EF_7, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
					}
          break;

				case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

				default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return mres;
  } // dlgprocAnimationEntreeNumerique

// -----------------------------------------------------------------------
// Propri�t�s d'un objet Entr�e Message (texte)
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocAnimationEntreeMessage (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT        mres = 0;	          // Valeur de retour
  PENV_REFLET_NUM	pEnv;
  DWORD           wEtat;
  char           szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
      DefautEN (hwnd, pEnv);
      if (!StrIsNull(pEnv->param.pszAnimation))
        { // decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 10, pEnv->param.pszAnimation);
        // nom var
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[2]);
        // init couleur
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[3]);
				pEnv->anCouleurs[0] = (G_COULEUR)wEtat;

        //InitCouleurClignot (hwnd, wEtat, ID_CB_1, &pEnv->anCouleurs[0]);
				EnleveClignottementCouleur(&pEnv->anCouleurs[0]);
        // init nbre car
        SetDlgItemText (hwnd, ID_EF_3, TabChamps[4]);
        // val init
        SetDlgItemText (hwnd, ID_EF_4, TabChamps[5]);
        // gestion des styles (boites a cocher)
        StrToDWORD (&wEtat, TabChamps[6]);
        ::CheckDlgButton (hwnd, ID_CB_2, wEtat);
        //
        StrToDWORD (&wEtat, TabChamps[7]);
        ::CheckDlgButton (hwnd, ID_CB_4, wEtat);
        //
        StrToDWORD (&wEtat, TabChamps[8]);
        ::CheckDlgButton (hwnd, ID_CB_3, wEtat);
        // couleur de fond
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[9]);
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
        }

			// Init des contr�les
      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH2,pEnv->anCouleurs[1]);

      mres = FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedERNM (hwnd, LOWORD (mp1), pEnv);
          break;

        case IDOK :
					{
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_e_m, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom var
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur
          //RecupCouleurClignot (hwnd, &wEtat, ID_CB_1, pEnv->anCouleurs[0]);
          //StrDWordToStr (szText, wEtat, 10);
          StrDWordToStr (szText, pEnv->anCouleurs[0]);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nbr car
          GetDlgItemText (hwnd, ID_EF_3, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val init
          GetDlgItemText (hwnd, ID_EF_4, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // gestion des boites a cocher
          wEtat = LOWORD (IsDlgButtonChecked (hwnd, ID_CB_2));
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          //
          wEtat = LOWORD (IsDlgButtonChecked (hwnd, ID_CB_4));
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          //
          wEtat = LOWORD (IsDlgButtonChecked (hwnd, ID_CB_3));
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");

          // couleur de fond
          wEtat = pEnv->anCouleurs[1];
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
					}
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

        default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  } // dlgprocAnimationEntreeMessage


// -----------------------------------------------------------------------
//            GESTION DES REFLETS NUMERIQUES
// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
// Propri�t�s d'un objet Reflet Num�rique
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrStaticN (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT          mres = 0;	          // Valeur de retour
  PENV_REFLET_NUM  pEnv;
  DWORD             wIndice;
  DWORD             wEtat;
  DWORD             wIndex;
  char             szText[80];
  
  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
			// init couleurs
			pEnv->anCouleurs[0]  = COULEUR_HAUT_INIT;
			pEnv->anCouleurs[1]  = COULEUR_FOND_INIT;
			// rempli combos variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, IDC_STATIC);
			//
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_3, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_3, CB_SETCURSEL, 1, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, IDC_STATIC);
			// init radios
			::CheckDlgButton (hwnd, ID_RB_4, 1);
			// inits dessins couleurs
			::EnableWindow (::GetDlgItem (hwnd, ID_CMB_3), FALSE);
			::EnableWindow (::GetDlgItem (hwnd, ID_CMB_4), FALSE);

      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 10, pEnv->param.pszAnimation);

        wIndice = 2;
        // nom variable
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
        if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_2, IDC_STATIC))
          {
          wIndice++;
          SetDlgItemText (hwnd, ID_CMB_2, TabChamps[wIndice]);
          }
        wIndice++;
        // decodages etat animation
        wEtat = 0;
        if (StrToDWORD (&wEtat, TabChamps[wIndice]))
          { // c'est une constante numerique
          InitCouleurClignot (hwnd, wEtat, ID_CB_1, &pEnv->anCouleurs[0]);
          }
        else
          { // c'est une variable
          SetDlgItemText (hwnd, ID_CMB_3, TabChamps[wIndice]);
          if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_4, IDC_STATIC))
            {
            wIndice++;
            SetDlgItemText (hwnd, ID_CMB_4, TabChamps[wIndice]);
            }
          // maj enable
          CheckRadioButton (hwnd, ID_RB_4, ID_RB_5, ID_RB_5);
          //::CheckDlgButton (hwnd, ID_RB_5, 1);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_5, BN_CLICKED), 0L);
          }
        wIndice++;
        // nbr caracteres
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[wIndice++]);
        // nbr decimales
        SetDlgItemText (hwnd, ID_EF_2, TabChamps[wIndice++]);
        // couleur de fond
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[wIndice]);
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
        //
        }
 			// Init des contr�les
     SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);
     break; // WM_INITDIALOG

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedERNM (hwnd, LOWORD (mp1), pEnv);
          break;

        case ID_RB_4 : // constante
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH1), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH2), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_3), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_4), FALSE);
              break;

            default:
              break;
            }
          break;

        case ID_RB_5 : // variable
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_3), TRUE);
              GetDlgItemText (hwnd, ID_CMB_3, szText, 80);
              if (pESVariableTableau (&(pEnv->param), szText) != NULL)
                {
                ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_4), TRUE);
                }
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH1), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH2), FALSE);
              break;

            default:
              break;
            }
          break;

        case ID_CMB_1 : // selection d'une variable
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, IDC_STATIC);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, IDC_STATIC);
              break;
            }
					break;

        case ID_CMB_3 : // selection d'une variable couleur
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, IDC_STATIC);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, IDC_STATIC);
              break;
            }

          break;

        case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_an, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom variable
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // indice tableau facultatif
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          if (StrToDWORD (&wIndex, szText))
            {
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          // variable ou constante
          RecupCouleurClignot (hwnd, &wEtat, ID_CB_1, pEnv->anCouleurs[0]);
          if (IsDlgButtonChecked (hwnd, ID_RB_4))
            { // constante
            StrDWordToStr (szText, wEtat);
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          else
            { // variable
            GetDlgItemText (hwnd, ID_CMB_3, szText, 80);
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            // indice tableau facultatif
            GetDlgItemText (hwnd, ID_CMB_4, szText, 80);
            if (StrToDWORD (&wIndex, szText))
              {
              StrConcat (pEnv->param.pszAnimation, szText);
              StrConcat (pEnv->param.pszAnimation, " ");
              }
            }
          // nbr de caracteres
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nbr de decimales
          GetDlgItemText (hwnd, ID_EF_2, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // couleur de fond
          wEtat = pEnv->anCouleurs[1];
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

				default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  }

// -----------------------------------------------------------------------
//            GESTION DES REFLETS MESSAGES
// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
// Propri�t�s d'un objet Reflet Message (texte)
static BOOL CALLBACK dlgprocGrStaticA (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT          mres = 0;	          // Valeur de retour
  PENV_REFLET_NUM  pEnv;
  DWORD             wIndice;
  DWORD             wEtat;
  DWORD             wIndex;
  char             szText[80];

  // ------------ Switch message re�u.... --------------------------------
  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
			// init couleurs
			pEnv->anCouleurs[0]  = COULEUR_HAUT_INIT;
			pEnv->anCouleurs[1]  = COULEUR_FOND_INIT;
			// rempli combos variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_message, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, IDC_STATIC);
      // 
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_3, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_3, CB_SETCURSEL, 1, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
      bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, IDC_STATIC);
			// init radios
			::CheckDlgButton (hwnd, ID_RB_4, 1);
			// inits dessins couleurs
			::EnableWindow (::GetDlgItem (hwnd, ID_CMB_3), FALSE);
			::EnableWindow (::GetDlgItem (hwnd, ID_CMB_4), FALSE);

      if (!StrIsNull(pEnv->param.pszAnimation))
        { // decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 7, pEnv->param.pszAnimation);

        wIndice = 2;
        //--------- nom variable
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
        if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_2, IDC_STATIC))
          {
          wIndice++;
          SetDlgItemText (hwnd, ID_CMB_2, TabChamps[wIndice]);
          }
        wIndice++;
        // decodages etat animation
        wEtat = 0;
        if (StrToDWORD (&wEtat, TabChamps[wIndice]))
          { // c'est une constante numerique
          InitCouleurClignot (hwnd, wEtat, ID_CB_1, &pEnv->anCouleurs[0]);
          }
        else
          { // c'est une variable
          SetDlgItemText (hwnd, ID_CMB_3, TabChamps[wIndice]);
          if (bMajCBIndexTableau (hwnd, &(pEnv->param), TabChamps[wIndice], ID_CMB_4, IDC_STATIC))
            {
            wIndice++;
            SetDlgItemText (hwnd, ID_CMB_4, TabChamps[wIndice]);
            }
          // maj enable
          CheckRadioButton (hwnd, ID_RB_4, ID_RB_5, ID_RB_5);
          ::SendMessage (hwnd, WM_COMMAND, MAKEWPARAM (ID_RB_5, BN_CLICKED), 0L);
          }
        wIndice++;
        // couleur de fond
        wEtat = 0;
        StrToDWORD (&wEtat, TabChamps[wIndice]);
        pEnv->anCouleurs[1] = (G_COULEUR)wEtat;
        //
        }
			// Init des contr�les
      SetDlgItemCouleurEchantillon (hwnd, IDC_CUSTOM_ECH1,pEnv->anCouleurs[0]);

      mres = (LRESULT)FALSE;
      break; // WM_INITDIALOG

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case IDC_CUSTOM_ECH1 : // user button
        case IDC_CUSTOM_ECH2 :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          if (HIWORD (mp1) == BN_CLICKED)
						GestionUserBoutonClickedERNM (hwnd, LOWORD (mp1), pEnv);
          break;

        case ID_RB_4 : // constante
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH1), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH2), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), TRUE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_3), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_4), FALSE);
              break;
            }
          break;

        case ID_RB_5 : // variable
          switch (HIWORD (mp1))
            {
            case BN_CLICKED :
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
              ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_3), TRUE);
              GetDlgItemText (hwnd, ID_CMB_3, szText, 80);
              if (pESVariableTableau (&(pEnv->param), szText) != NULL)
                {
                ::EnableWindow (::GetDlgItem (hwnd, ID_CMB_4), TRUE);
                }
              ::EnableWindow (::GetDlgItem (hwnd, ID_CB_1), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH1), FALSE);
              ::EnableWindow (::GetDlgItem (hwnd, IDC_CUSTOM_ECH2), FALSE);
              break;

            default:
              break;
            }
          break;

        case ID_CMB_1 : // selection d'une variable
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, IDC_STATIC);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_2, IDC_STATIC);
              break;
            }
					break;

        case ID_CMB_3 : // selection d'une variable couleur
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, IDC_STATIC);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
              bMajCBIndexTableau (hwnd, &(pEnv->param), szText, ID_CMB_4, IDC_STATIC);
              break;
            }

          break;

        case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_r_m, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom variable
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // indice tableau facultatif
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          if (StrToDWORD (&wIndex, szText))
            {
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          // variable ou constante
          RecupCouleurClignot (hwnd, &wEtat, ID_CB_1, pEnv->anCouleurs[0]);
          if (IsDlgButtonChecked (hwnd, ID_RB_4))
            { // constante
            StrDWordToStr (szText, wEtat);
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            }
          else
            { // variable
            GetDlgItemText (hwnd, ID_CMB_3, szText, 80);
            StrConcat (pEnv->param.pszAnimation, szText);
            StrConcat (pEnv->param.pszAnimation, " ");
            // indice tableau facultatif
            GetDlgItemText (hwnd, ID_CMB_4, szText, 80);
            if (StrToDWORD (&wIndex, szText))
              {
              StrConcat (pEnv->param.pszAnimation, szText);
              StrConcat (pEnv->param.pszAnimation, " ");
              }
            }
          // couleur de fond
          wEtat = pEnv->anCouleurs[1];
          StrDWordToStr (szText, wEtat);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

        default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  }


// -----------------------------------------------------------------------
//            GESTION DES BOUTONS RADIO
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrRadio (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT          mres = 0;	          // Valeur de retour
  PENV_REFLET_NUM  pEnv;
  DWORD             wIndice;
  char             szText[80];
  
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			PELEMENT_PAGE_GR pElement;

      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
			// rempli combos variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
       
      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 3, pEnv->param.pszAnimation);

        wIndice = 2;
        // nom variable
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
        //
        }

			// Check de l'�tat WS_GROUP
			pElement = pElementPage (pEnv->param.dwElement);
			Verif (pElement);

			if (pElement->nStyleRemplissage & WS_GROUP)
				CheckDlgButtonBool (hwnd, ID_CB_1, TRUE);

			// Mise � jour du texte
			VerifWarning (bLireTexteElementGR (pEnv->param.dwElement, szText));
      SetDlgItemText (hwnd, ID_TEXT_1, szText);
			}
     break; // WM_INITDIALOG

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_CMB_1 : // selection d'une variable
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
              break;
            }
					break;

        case IDOK :
					{
					PELEMENT_PAGE_GR pElement;
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);

          bMotReserve (c_res_ign_control_radio, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom variable
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);

					// R�cup�re le texte du contr�le ...
		      GetDlgItemText (hwnd, ID_TEXT_1, szText, 80);
					bTexteElementGR (pEnv->param.hbdgr, MODE_GOMME_ELEMENT, pEnv->param.dwElement, szText);

					// Et l'�tat WS_GROUP
					pElement = pElementPage (pEnv->param.dwElement);
					Verif (pElement);

					if (IsDlgButtonChecked(hwnd, ID_CB_1)  == BST_CHECKED)
						pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)(((DWORD)(pElement->nStyleRemplissage)) | WS_GROUP);
					else
						pElement->nStyleRemplissage = (G_STYLE_REMPLISSAGE)(((DWORD)(pElement->nStyleRemplissage)) & (~WS_GROUP));

          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
					// Termin�
          EndDialog (hwnd, IDOK);
					}
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

				default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  } // dlgprocGrRadio

// -----------------------------------------------------------------------
//            GESTION DES LISTES
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrListe (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT          mres = 0;	          // Valeur de retour
  PENV_REFLET_NUM   pEnv;
  DWORD             wIndice;
  char             szText[80];
  
  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
			// rempli combos variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_message, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
       
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_2, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_2, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_2, szText);
       
      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 4, pEnv->param.pszAnimation);

        wIndice = 2;
        // nom variable tableau
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
				wIndice++;

        // nom variable indice
        SetDlgItemText (hwnd, ID_CMB_2, TabChamps[wIndice]);
        //
        }
     break; // WM_INITDIALOG

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_CMB_1 : // selection d'une variable tableau
        case ID_CMB_2 : // selection d'une variable indice
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),LOWORD(mp1), szText);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),LOWORD(mp1), szText);
              break;
            }
					break;

        case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_control_liste, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom variable tableau
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom variable indice
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

				default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  }

// -----------------------------------------------------------------------
//            GESTION DES COMBOBOX
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrCombobox (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT          mres = 0;	          // Valeur de retour
  PENV_REFLET_NUM  pEnv;
  DWORD             wIndice;
  char             szText[80];
  
  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
			// rempli combos variables
			InitComboVariables (hwnd, &(pEnv->param), c_res_message, ID_CMB_1, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_1, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_1, szText);
       
			InitComboVariables (hwnd, &(pEnv->param), c_res_numerique, ID_CMB_2, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_2, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_2, szText);
       
			InitComboVariables (hwnd, &(pEnv->param), c_res_message, ID_CMB_3, TRUE);
			SendDlgItemMessage (hwnd, ID_CMB_3, CB_SETCURSEL, 0, 0);
      GetTextSelectCBVariable (hwnd, &(pEnv->param),ID_CMB_3, szText);
       
      if (!StrIsNull(pEnv->param.pszAnimation))
        { 
				// decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 5, pEnv->param.pszAnimation);

        wIndice = 2;
        // nom variable tableau
        SetDlgItemText (hwnd, ID_CMB_1, TabChamps[wIndice]);
				wIndice++;

        // nom variable indice
        SetDlgItemText (hwnd, ID_CMB_2, TabChamps[wIndice]);
				wIndice++;

        // nom variable saisie
        SetDlgItemText (hwnd, ID_CMB_3, TabChamps[wIndice]);
        //
        }
     break; // WM_INITDIALOG

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
        case ID_CMB_1 : // selection d'une variable tableau
        case ID_CMB_2 : // selection d'une variable indice
        case ID_CMB_3 : // selection d'une variable saisie
          switch (HIWORD (mp1))
            {
            case CBN_SELCHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextSelectCBVariable (hwnd, &(pEnv->param),LOWORD(mp1), szText);
              break;
            case EN_CHANGE:
              pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
							GetTextEditCBVariable (hwnd, &(pEnv->param),LOWORD(mp1), szText);
              break;
            }
					break;

        case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_control_combobox, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom variable tableau
          GetDlgItemText (hwnd, ID_CMB_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom variable indice
          GetDlgItemText (hwnd, ID_CMB_2, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom variable saisie
          GetDlgItemText (hwnd, ID_CMB_3, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

        case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

				default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  }

//-------------------------------------------------------------------------
// Propri�t�s d'un objet STATIC TEXT
//-------------------------------------------------------------------------
static BOOL CALLBACK dlgprocGrStaticText (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT       mres = 0;
  PENV_REFLET_NUM	pEnv;
  char          szText[80];

  switch (msg)
    {
    case WM_INITDIALOG:
      pEnv = (PENV_REFLET_NUM)pCreeEnv (hwnd, sizeof (ENV_REFLET_NUM));
      pEnv->param = *((PARAM_DLG_GR *) mp2);
      DefautEN (hwnd, pEnv);
      if (!StrIsNull(pEnv->param.pszAnimation))
        { // decomposer la chaine
        StrDeleteFirstSpaces (pEnv->param.pszAnimation);
        wNbChamps = StrToStrArray (TabChamps, 4, pEnv->param.pszAnimation);
        // nom var
        SetDlgItemText (hwnd, ID_EF_1, TabChamps[2]);
        // val init
        SetDlgItemText (hwnd, ID_EF_2, TabChamps[3]);
        }

      mres = (LRESULT)FALSE;
      break;

    case WM_DESTROY:
      bLibereEnv (hwnd);
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case IDOK :
          pEnv = (PENV_REFLET_NUM)pGetEnv (hwnd);
          bMotReserve (c_res_ign_control_static, szText);
          StrCopy (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // numero de page
          StrDWordToStr (szText, pEnv->param.wNumPage);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // nom var
          GetDlgItemText (hwnd, ID_EF_1, szText, 80);
          StrConcat (pEnv->param.pszAnimation, szText);
          StrConcat (pEnv->param.pszAnimation, " ");
          // Val init
          GetDlgItemText (hwnd, ID_EF_2, szText, 32);
          StrConcat (pEnv->param.pszAnimation, szText);
          //
          StrConcat (pEnv->param.pszAnimation, " ");
					char strIdentifiantElement [c_NB_CAR_ID_ELT_GR_ANIM + 1];
					StrCopy (strIdentifiantElement, LireIdentifiantElement (pEnv->param.dwElement));
					StrAddDoubleQuotation (strIdentifiantElement);
					StrConcat (pEnv->param.pszAnimation, strIdentifiantElement);
					//
          EndDialog (hwnd, IDOK);
          break;

				case IDB_SUPPRIMER :
        case IDCANCEL :
          EndDialog (hwnd, LOWORD(mp1));
          break;

				default:
          break;
        }  // switch command
      break;

    default:
      mres = 0;
      break;
    }

  return (mres);
  }

// ----------------------------------------------------------------------
//                 Appel des boites de dialogue d'animation
// ----------------------------------------------------------------------
DWORD dwDlgAnimationGR (char *pszChaineAnimation, HBDGR hbdgr, ANIMATION nAnimation, DWORD dwElement)
  {
  DLGPROC fnwpDlgProc;
	DWORD		wIdDlg = 0;
  DWORD		wRetour = IDCANCEL;
  PARAM_DLG_GR param;

  if (existe_repere (b_e_s))
    param.wNbVar = nb_enregistrements (szVERIFSource, __LINE__, b_e_s);
  else
    param.wNbVar = 0;

  param.pszAnimation = pszChaineAnimation;
  param.hbdgr = hbdgr; // $$ redondant
  param.wNumPage = dwNPageGR (hbdgr); // $$ redondant
	param.dwElement = dwElement;

  // R�cup�re les param�tres de la boite de dialogue choisie
  switch (nAnimation)
    {
    case ANIMATION_E_LOG:// E L
      wIdDlg = DLG_E_LOG;
      fnwpDlgProc = dlgprocGrELog;
      break;

    case ANIMATION_R_1_LOG : // R L
      wIdDlg = DLG_R_LOG;
      fnwpDlgProc = dlgprocGrRLog;
      break;

    case ANIMATION_R_2_LOG:// RL4
      wIdDlg = DLG_R_LOG_4;
      fnwpDlgProc = dlgprocGrRLog4;
      break;

    case ANIMATION_BARGRAPHE :// RNB reflet num�rique bargraphe
      wIdDlg = DLG_BARGRAPHE;
      fnwpDlgProc = dlgprocGrBargraphe;
      break;

    case ANIMATION_COURBE_TEMPS: // RNCT
      wIdDlg = DLG_COURBE_TEMPS;
      fnwpDlgProc = dlgprocGrCourbeTemps;
      break;

    case ANIMATION_COURBE_COMMANDE: // RNCC
      wIdDlg = DLG_COURBE_COMMANDE;
      fnwpDlgProc = dlgprocGrCourbeCommande;
      break;

    case ANIMATION_COURBE_ARCHIVE: // RNCA
      wIdDlg = DLG_COURBE_ARCHIVE;
      fnwpDlgProc = dlgprocGrCourbeArchive;
      break;

    case ANIMATION_DEPLACE: // RND
      wIdDlg = DLG_DEPLACE;
      fnwpDlgProc = dlgprocGrDeplace;
      break;

    case ANIMATION_EDIT_N: // E N
      wIdDlg = DLG_EDIT_N;
      fnwpDlgProc = dlgprocAnimationEntreeNumerique;
      break;

    case ANIMATION_EDIT_A: // E M
      wIdDlg = DLG_EDIT_A;
      fnwpDlgProc = dlgprocAnimationEntreeMessage;
      break;

    case ANIMATION_TEXTE_STATIC_N: // R N
      wIdDlg = DLG_STATIC_N;
      fnwpDlgProc = dlgprocGrStaticN;
      break;

    case ANIMATION_TEXTE_STATIC_A: // R M
      wIdDlg = DLG_STATIC_A;
      fnwpDlgProc = dlgprocGrStaticA;
      break;

    case ANIMATION_CONTROL_BOUTON:
      wIdDlg = DLG_BOUTON;
      fnwpDlgProc = dlgprocGrBouton;
      break;

    case ANIMATION_CONTROL_CHECK:
      wIdDlg = DLG_BOUTON_CHECK;
      fnwpDlgProc = dlgprocGrBoutonCheck;
      break;

    case ANIMATION_CONTROL_RADIO :
      wIdDlg = DLG_RADIO;
      fnwpDlgProc = dlgprocGrRadio;
      break;

    case ANIMATION_CONTROL_STATIC :
      wIdDlg = DLG_STATIC_TEXT;
      fnwpDlgProc = dlgprocGrStaticText;
      break;

    case ANIMATION_CONTROL_LISTE :
      wIdDlg = DLG_LISTE;
      fnwpDlgProc = dlgprocGrListe;
      break;

    case ANIMATION_CONTROL_COMBOBOX :
      wIdDlg = DLG_COMBOBOX;
      fnwpDlgProc = dlgprocGrCombobox;
      break;

		case ANIMATION_AUCUNE:
			{
			PELEMENT_PAGE_GR pElement = pElementPage (param.dwElement);
			Verif (pElement);

			switch (pElement->nAction)
				{
				case G_ACTION_CONT_BOUTON_VAL:
					wIdDlg = DLG_BOUTON_VAL;
					fnwpDlgProc = dlgprocGrBoutonVal;
					break;
				case G_ACTION_CONT_GROUP:
					wIdDlg = DLG_GROUPE;
					fnwpDlgProc = dlgprocGrGroupe;
					break;
				default:
					VerifWarningExit;
					break;
				}
			}
      break;

		case ANIMATION_CONTROL_EDIT:
			// $$ � compl�ter
			break;

    default:
			VerifWarningExit;
      break;
    }

	if (wIdDlg)
		wRetour = LangueDialogBoxParam (MAKEINTRESOURCE (wIdDlg), Appli.hwnd, fnwpDlgProc, (LPARAM)(&param));

  return wRetour;
  } // dwDlgAnimationGR


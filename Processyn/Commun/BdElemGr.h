//-------------------------------------------------------------------------
// BdElemGr.h (extrait de Bdgr)
// Gestion des �l�ments graphiques de la base de donn�e Processyn
//-------------------------------------------------------------------------

// For�age des �l�ments graphiques
typedef struct
  {
  G_COULEUR						nCouleurLigne;
  G_COULEUR						nCouleurRemplissage;
  G_STYLE_LIGNE				nStyleLigne;
  G_STYLE_REMPLISSAGE nStyleRemplissage;
  LONG								xOffset;
  LONG								yOffset;
  } FORCAGE_ELEMENT;

// Constante pas de for�age
extern const FORCAGE_ELEMENT FORCAGE_NULL;

//-------------------------------------------------------------
// Cr�ation si ad�quat de la fen�tres d'un �l�ment graphique
// Renvoie TRUE si Ok - FALSE si erreur de cr�ation de fen�tre
BOOL bCreerFenetreElementGr (DWORD Repere, DWORD Position, HBDGR hbdgr, DWORD	dwIdFenetre);

//-------------------------------------------------------------
// Cr�ation des fen�tres de tous les �l�ments graphiques d'une page
// Renvoie TRUE si Ok - FALSE si au moins une erreur de cr�ation de fen�tre
BOOL bCreerFenetresElementGr (HBDGR hbdgr, DWORD	dwPremierId);

//-------------------------------------------------------------
// Fermeture si ad�quat de la fen�tres d'un �l�ment graphique
// Renvoie TRUE si Ok - FALSE si pas de fermeture de fen�tre ou erreur
BOOL bFermerFenetreElementGr (DWORD Repere, DWORD wPositionElement);
//-------------------------------------------------------------
// Fermeture des fen�tres de tous les �l�ments graphiques d'une page
// Renvoie TRUE si Ok - FALSE si au moins une erreur de fermeture de fen�tre
BOOL bFermerFenetresElementGr (HBDGR hbdgr);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Repr�sentation d'un �l�ment
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//-------------------------------------------------------------------------
// Dessin d'un �l�ment graphique
void DessinerElementGR 
	(
	DWORD Repere, DWORD Position, const FORCAGE_ELEMENT * pForcage, PBDGR pBdGr,
	HELEMENT hElmtGsys // Le contenu n'est pas significatif en sortie
	);

//--------------------------------------------------------------------------
// Invalidate d'un �l�ment graphique
void GommerElementGR (DWORD Repere, DWORD Position, const FORCAGE_ELEMENT *pForcage, PBDGR pBdGr, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// Ajustement �ventuel d'une fen�tre d'un �l�ment graphique
void ChangePosFenetreElementGR (DWORD Repere, DWORD Position, PBDGR pBdGr);

// -------------------------------------------------------------------------
// Change l'ID de la fen�tre d'un contr�le
void ChangeIdFenetreElementGR (HBDGR hbdgr, DWORD PositionDansPage, DWORD dwNouvelID);

// -------------------------------------------------------------------------
// D�placer � l'�cran un �l�ment graphique
void bdgr_offset_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, LONG x1Ref, LONG y1Ref, LONG x2Ref, LONG y2Ref, LONG Dx, LONG Dy);

// -------------------------------------------------------------------------
// Forcer Element
void ForcerElementGR (DWORD Repere, DWORD Position, G_COULEUR nCouleurLigne, G_COULEUR nCouleurRemplissage, 
											G_STYLE_LIGNE StyleLigne, G_STYLE_REMPLISSAGE StyleRemplissage);

// -------------------------------------------------------------------------
// Forcer Style Texte (Police, StylePolice)
void ForcerStyleTexteElementGR (HBDGR hbdgr, DWORD Repere, DWORD Position, DWORD Police, DWORD StylePolice, DWORD TaillePolicePixel);

// -------------------------------------------------------------------------
// Translation
void TranslaterElementGR (DWORD Repere, DWORD Position, LONG Dx, LONG Dy, HGSYS hGsys, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// Substituer
void ChangeRemplissageElementGR (DWORD Repere, DWORD Position, HGSYS hGsys, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// Sym�trie
void MiroiterElementGR (DWORD Repere, DWORD Position, DWORD Axes, LONG xAxe, LONG yAxe, HGSYS hGsys, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// Rotation
void TournerElementGR (DWORD Repere, DWORD Position, LONG xCentre, LONG yCentre, DWORD NbQuartTour, HGSYS hGsys, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// Deformation
void DeformerElementGR (DWORD Repere, DWORD Position, DWORD *PositionSauvegarde, LONG xRef, LONG yRef,
												LONG NCx, LONG DCx, LONG NCy, LONG DCy, HGSYS hGsys, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// D�grouper un Element de la Page
DWORD DegrouperElementPageGR (HBDGR hbdgr, DWORD Position, HGSYS hGsys, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// Change la couleur d'un �l�ment graphique et rafraichit �ventuellement l'�cran
// Appel� par PcsExe et PcsGr
void bdgr_couleur_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, G_COULEUR Couleur);

// -------------------------------------------------------------------------------------------
// Change le style de remplissage d'un �l�ment graphique et rafraichit �ventuellement l'�cran
void bdgr_style_remplissage_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, G_STYLE_REMPLISSAGE Style);

// ---------------------------------------------------------------------------------------
// Change le style ligne d'un �l�ment graphique et rafraichit �ventuellement l'�cran
void bdgr_style_ligne_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, G_STYLE_LIGNE Style);

// -------------------------------------------------------------------------
// Change le texte d'un �l�ment graphique et rafraichit �ventuellement l'�cran
BOOL bTexteElementGR (HBDGR hbdgr, DWORD ModeTravail, DWORD dwNElement, PCSTR pszTexte);

// -------------------------------------------------------------------------
// Change le texte d'un �l�ment graphique et rafraichit �ventuellement l'�cran
BOOL bTexteElementGRDansPage (HBDGR hbdgr, DWORD ModeTravail, DWORD dwNElementDansPage, PCSTR pszTexte);

// -------------------------------------------------------------------------
// Redimensionne un �l�ment graphique
void bdgr_coordonnees_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, LONG x1, LONG y1, LONG x2, LONG y2);

// --------------------------------------------------------------------------
// Renvoie un pointeur sur l'�l�ment de page num�ro dwNElement
PELEMENT_PAGE_GR pElementPage (DWORD dwNElement);

// -------------------------------------------------------------------------
PSTR LireIdentifiantElement (DWORD dwElement);

// -------------------------------------------------------------------------
BOOL bdgr_lire_style_texte_element (HBDGR hbdgr, DWORD Element, DWORD *Police, DWORD *StylePolice);

// -------------------------------------------------------------------------
BOOL bdgr_lire_style_element (HBDGR hbdgr, DWORD Element, DWORD *StyleLigne, DWORD *StyleRemplissage);

// -------------------------------------------------------------------------
BOOL bdgr_lire_couleur_element (HBDGR hbdgr, DWORD Element, G_COULEUR *CouleurLigne, G_COULEUR *CouleurRemplissage);

// -------------------------------------------------------------------------
// R�cup�re le texte d'un �l�ment graphique
// Renvoie TRUE si texte r�cup�r�
BOOL bLireTexteElementGR (DWORD Element, char *pszTexte);

// -------------------------------------------------------------------------
// R�cup�re le hwnd d'un �l�ment graphique ou NULL
HWND hwndElementGR (HBDGR hbdgr, DWORD PositionDansPage);


// -------------------------------------------------------------------------
// Suppression
void SupprimerElementGR (DWORD Repere, DWORD Position);
void SupprimerForcageGR (DWORD GeoPosition);
void SupprimerTexteGR   (DWORD GeoPosition);
void SupprimerPolyGR    (DWORD GeoPosition);
void SupprimerListeGR   (DWORD GeoPosition);
BOOL SuppressionPossibleElementGR (DWORD Repere, DWORD Position);

// -------------------------------------------------------------------------
// Distance
DWORD DistanceElementGR (DWORD Repere, DWORD Position, LONG x, LONG y, HGSYS hGsys, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// Encombrement
void EncombrementElementGR (DWORD Repere, DWORD Position, PLONG x1, PLONG y1, PLONG x2, PLONG y2, HGSYS hGsys, HELEMENT hElmtGsys);

// -------------------------------------------------------------------------
// r�cup�rer les 2 points d'un �l�ment graphique
void bdgr_point_ref_element (HBDGR hbdgr, DWORD Element, PPOINT	pPoint0, PPOINT	pPoint1);

// -------------------------------------------------------------------------
// Texte
DWORD PointerTexteElementGR (DWORD Repere, DWORD Position, LONG x, LONG y, HGSYS hGsys, HELEMENT hElmtGsys, LONG *X1Car, LONG *Y1Car, LONG *X2Car, LONG *Y2Car);

// -------------------------------------------------------------------------
// Copie : force l'animation a zero
void CopierElementGR (DWORD RepereSource, DWORD PositionSource, DWORD RepereDestination, DWORD PositionDestination);

// -------------------------------------------------------------------------
// Duplique un �l�ment graphique en l'ajoutant, s'il est anim� � la liste des �l�ments anim�s � dupliquer
void DupliquerElementGR (HBDGR hbdgr, DWORD RepereSource, DWORD PositionSource, DWORD RepereDestination, DWORD PositionDestination);
// Duplique �ventuellement les animations graphiques de la liste 
// des "�l�ments ayant des animations � dupliquer" en demandant � l'utilisateur 
// les param�tres d'animation suppl�mentaires
// et vide toujours la liste des �l�ments anim�s � dupliquer
BOOL DupliquerAnimationGR (HBDGR hbdgr, BOOL bDuplication);

// -------------------------------------------------------------------------
// Rendre Unique un �lement GR et sauvegarde des points de l'Element GR
// pour D�formation
void RendreUniqueEtSauveElementGR (DWORD Repere, DWORD Position, DWORD *PositionSauvegarde);

// -------------------------------------------------------------------------
// Rendre unique un element GR pour Rotation, Sym�trie, Substitution
void RendreUniqueElementGR (DWORD Repere, DWORD Position);

// -------------------------------------------------------------------------
// Maj de la base de donn�es graphique (sans dessin)
// -------------------------------------------------------------------------
void MajGeoListesGR   (MAJ_BDGR TypeMaj, DWORD Ref, DWORD PosEnr, DWORD NbEnr);
void MajGeoTextesGR   (MAJ_BDGR TypeMaj, DWORD PosEnr, DWORD NbEnr);
void MajGeoPolyGR     (MAJ_BDGR TypeMaj, DWORD Ref, DWORD PosEnr, DWORD NbEnr);

// -------------------------------------------------------------------------
// Insere un nouvel �l�ment graphique dans la base de donn�es Processyn
// Renvoie son num�ro ou 0 si ajout impossible
DWORD dwValiderElementGr (HBDGR hbdgr, HELEMENT helementEpure);


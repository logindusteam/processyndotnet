/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |									    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 |									    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |									    |
 |   Titre   : CVT_UL.C   						    |
 |   Auteur  : AC							    |
 |   Date    : 31/12/93 						    |
 +--------------------------------------------------------------------------*/

#include "stdafx.h"
#include "UStr.h"
#include "lng_res.h"
#include "tipe.h"
#include "MemMan.h"
#include "Verif.h"
#include "AnalyseCar.h"
#include "cvt_ul.h"
VerifInit;

// Instancie un outil d'analyse caract�re
static CAnalyseCar AnalyseCar;

// ------------------------------------------------------------------------
// Ajoute des guillemets aux extr�mit�s et double les guillemets internes
// renvoie FALSE si impossible
BOOL bAjouteGuillemetsMessage (PSTR pszDestination, DWORD dwTailleDest, PCSTR pszSource)
	{
	BOOL bRes = FALSE;
	VerifWarningExit;
	return bRes;
	}

// ------------------------------------------------------------------------
// Enl�ve les guillemets aux extr�mit�s et d�double les guillemets internes
// renvoie FALSE si impossible (guillemets internes non doubl�s)
void EnleveGuillemetsMessage (PSTR pszDestination)
	{
	PSTR pszOrigine = pszDestination;
	PSTR pszRecopie = pszDestination;

	// saute le prmier guillemet �ventuel
	if (*pszOrigine == '"')
		pszOrigine++;

	// longueur = nb cars + 0 terminateur - dernier guillemet
	int nLen = StrLength (pszOrigine);

	// coupe l'�ventuel dernier guillemet
	if (pszOrigine [nLen-1]== '"')
		{
		nLen--;
		pszOrigine [nLen]= '\0';
		}

	for (int nCar = 0; nCar <= nLen; nCar++)
		{
		//caract�re guillemet ?
		if (*pszOrigine == '"')
			{
			// oui => passe au caract�re suivant
			pszOrigine ++;
			nCar ++;
			// lui m�me guillemet ?
			if (*pszOrigine == '"')
				{
				// oui => recopie le
				*pszRecopie = *pszOrigine;
				pszRecopie ++;
				pszOrigine ++;
				}
			}
		else
			{
			// non => recopie le et passe au suivant
			*pszRecopie = *pszOrigine;
			pszOrigine ++;
			pszRecopie ++;
			}
		} // for (int nCar = 0; nCar <= nLen; nCar++)
	}

// -------------------------------------------------------------------------------------
// detecte les chaines de type commentaire ou message, tronque si > 80
static void ExtraitUL (int n_ul, PCSTR pszSource, PUL pUL, DWORD *pNbTraites)
  {
  PSTR pszDestination = &pUL->show[0];
	BOOL bCommenceParGuillemet;
  BOOL bFinULTrouvee = TRUE;
  int position;

	// Ligne en commentaire ?
  if ((n_ul == 0) && (pszSource[0] == CAR_COMMENTAIRE))
    {
		// oui => Toute la ligne est prise en compte
    (*pNbTraites) = StrLength (pszSource);
    bCommenceParGuillemet = TRUE;
    }
  else
    {
		// non => d�but de message ?
    if (pszSource[0] == CAR_DEBUT_MESSAGE)
      {
			bCommenceParGuillemet = TRUE;

			//JS : accepte les doubles guillemets dans un message
			int nDebutRecherche = 1;
			BOOL bContinueRecherche = TRUE;
			do
				{
				// Trouve le guillemet de fin ?
				position = StrSearchChar (pszSource+nDebutRecherche, CAR_COMMENTAIRE);
				if (position != STR_NOT_FOUND)
					{
					// oui => comptabilise les cars parcourus
					nDebutRecherche += position+1;

					// suivi par un autre guillemet ?
					if (*(pszSource + nDebutRecherche) == CAR_COMMENTAIRE)
						{
						// oui => recherche imm�diatement apr�s ce guillemet
						nDebutRecherche ++;
						}
					else
						{
						// non => termin�, d�compte tous les char parcourus
						position = nDebutRecherche;
						bContinueRecherche = FALSE;
						}
					}
				else
					{
					// non => termin�
					bFinULTrouvee = FALSE;
					bContinueRecherche = FALSE;
					}
				} while (bContinueRecherche);

      if (bFinULTrouvee)
        {
        (*pNbTraites) = position;    // pour " du debut + index
        }
      else
        {
        (*pNbTraites) = StrLength (pszSource);
        }
      }
    else // pas message
      {
      bCommenceParGuillemet = FALSE;
      position = StrSearchChar (pszSource, ' '); //pos C de l'espace = nombre - espace
      bFinULTrouvee = (position != STR_NOT_FOUND);
      if (bFinULTrouvee)
        {
        (*pNbTraites) = (DWORD)(position);
        }
      else
        {
        (*pNbTraites) = StrLength (pszSource);
        }
      }
    }

  if (bFinULTrouvee)
    {
    if ((*pNbTraites) <= 80)
      {
      StrExtract (pszDestination, pszSource, 0, (*pNbTraites));
      }
    else
      {
      StrExtract (pszDestination, pszSource, 0, 80);
      }
    }
  else  // pas bFinULTrouvee
    {
    if ((*pNbTraites) <= 80)
      {
      StrCopy (pszDestination , pszSource);
      }
    else
      {
      StrExtract (pszDestination, pszSource, 0, 80);
      }
    }
  if (!bCommenceParGuillemet)
    {
    StrUpperCase (pszDestination);
    }

	// d�termination du .genre de l'UL :
	// par d�faut
	pUL->genre = g_bizarre;

	// D�termine le statut du premier car
  int nStatutCar0 = AnalyseCar.nStatutCar(pUL->show[0]);
	
	// Premier caract�re = caract�re de type g_id ?
  if (nStatutCar0 & BIT_CAR_PREMIER_CAR_ID)
    {
		// oui => genre identifiant
    pUL->genre = g_id;
    }
  else
		{
		// Premier caract�re <> caract�re de type g_id : Num�rique ?
		if (nStatutCar0 & BIT_CAR_NUMERIQUE)
			{
			// oui => genre num�rique
			pUL->genre = g_num;
			}
		else
			{
			// Premier car n'est ni Identifiant ni num�rique : signe + ou - ?
			if (nStatutCar0 & BIT_CAR_SIGNE)
				{
				int nStatutCar1 = AnalyseCar.nStatutCar(pUL->show[1]);

				// oui => plus de 1 caract�re de long ?
				if (!(nStatutCar1 & BIT_CAR_FIN_CHAINE))
					{
					// oui => commence comme un num�rique ?
					if (nStatutCar1 & BIT_CAR_NUMERIQUE)
						{
						// oui => genre num�rique
						pUL->genre = g_num;
						}
					}
				else
					{
					// non => op�rateur + ou - => identifiant
					pUL->genre = g_id;
					}
				}
			else
				{
				if (nStatutCar0 & BIT_CAR_OPERATEUR)
					{
					pUL->genre = g_id;
					}
				}
			}
		}
  } // ExtraitUL

// Renvoie l'adresse du premier car d'une chaine qui ne soit pas TAB ou espace
static _inline PCSTR pPremierCarNonSeparateur (PCSTR pStr)
	{
  while (*pStr != '\0')
    {
		if ((*pStr == ' ') || (*pStr == '\t'))
			pStr++;
		else
			break;
    }
	return pStr;
	}

//---------------------------------------------------------------------------
// Met � "" erreur = 0 Les nb_ul premiers ULs sp�cifi�s
void init_vect_ul (PUL vect_ul, DWORD nb_ul)
  {
  for (DWORD i = 0 ; (i < nb_ul); i++)
    {
    vect_ul[i].erreur = 0;
    vect_ul[i].show [0] = '\0';
    }
  }

// ----------------------------------------------------------------
// D�compose le Texte Source en une s�rie d'ULs dont le genre est d�termin�
void texte_to_v_ul (int * pNbUls, PUL v_ul, PCSTR pszTexteSource)
  {
	// Reset de tous les �l�ments du vecteur ul
  init_vect_ul (v_ul, nb_max_ul);

	// Entame le parcours de la chaine source
	PCSTR pStr = pPremierCarNonSeparateur (pszTexteSource);
	PUL pUL = v_ul;
	int n_ul = 0;
	
	// parcours le texte jusqu'� sa fin ou le nb max d'ul atteint
  for (n_ul = 0; (*pStr != '\0') && (n_ul < nb_max_ul); n_ul++)
    {
		// r�cup�re l'UL suivante
		DWORD nb_transfere;
    ExtraitUL (n_ul, pStr, pUL, &nb_transfere);

		// note l'avancement
    pStr += nb_transfere;

		// saute les s�parateurs
    pStr = pPremierCarNonSeparateur (pStr);
		pUL++;
    }

  //DetermineGenreULs (n_ul, v_ul);
  (*pNbUls) = n_ul;
  }

// ----------------------------------------------------------------
// Assemble le vecteur UL en une chaine de caract�res
void v_ul_to_texte (int nb_ul, PUL v_ul, PSTR pszTexteDest, DWORD dwTailleTexteDest)
  {
	// Entame le parcours de la chaine dest
	PSTR pStr = pszTexteDest;

	// pour chaque UL
  for (int n = 0; ; )
    {
		UINT nTailleUL = StrLength(v_ul[n].show);

		// Ajoute l'UL � la chaine
		MemCopy (pStr, v_ul[n].show, nTailleUL);

		// avance l'adresse d'�criture
		pStr += nTailleUL;

		// Une UL trait�e en plus
		n++;

		// derni�re UL ?
		if (n == nb_ul)
			// oui => boucle termin�e
			break;

		// rajoute un s�parateur
    *pStr = ' ';
		pStr++;
    }

	// Termine la chaine
	*pStr = '\0';
	VerifWarning (pStr < &pszTexteDest[dwTailleTexteDest]);
  }

//---------------------------------------------------------------------------
void erreur_encode_i (PUL ul, int i)
  {
  StrINTToStr (ul->show, i);
  ul->erreur = 0;
  }

//---------------------------------------------------------------------------
void erreur_encode_i4 (PUL ul, LONG i4)
  {
  StrLONGToStr (ul->show, i4);
  ul->erreur = 0;
  }

//---------------------------------------------------------------------------
void erreur_encode_bw (PUL ul, BOOL b)
  {
  if (b)
    {
    StrWORDToStr (ul->show, 1);
    }
  else
    {
    StrWORDToStr (ul->show, 0);
    }
  ul->erreur = 0;
  }

//---------------------------------------------------------------------------
void erreur_encode_dw (PUL ul, DWORD dw)
  {
  StrDWordToStr (ul->show, dw);
  ul->erreur = 0;
  }

//---------------------------------------------------------------------------
void erreur_encode_r (PUL ul, FLOAT reel)
  {
  StrFLOATToStr (ul->show, reel);
  ul->erreur = 0;
  }

//---------------------------------------------------------------------------
void erreur_encode_sz (PUL ul, PCSTR pszSource)
  {
  StrCopy (ul->show, pszSource);
  ul->erreur = 0;
  }

//---------------------------------------------------------------------------
void erreur_encode_sz_avec_guillemets (PUL ul, PCSTR pszSource)
  {
  StrCopy (ul->show, pszSource);
  StrAddDoubleQuotation (ul->show); 
  ul->erreur = 0;
  }

/*
// ----------------------------------------------------------------
// D�compose le Texte Source en une s�rie d'ULs dont le genre est d�termin�
void texte_to_v_ul2 (int * pNbUls, PUL v_ul, PCSTR pszTexteSource)
  {
	PUL pULCourante = v_ul;
	int nNbUls = 0;

	// Reset de tous les �l�ments du vecteur ul
  init_vect_ul (v_ul, nb_max_ul);
	* pNbUls = 0;

	// Entame le parcours de la chaine source
	PCSTR pStr = pPremierCarNonSeparateur (pszTexteSource);

	// Ligne de commentaire ?
	if (*pStr == CAR_COMMENTAIRE)
		{
		// oui recopie la ligne jusqu'au bout
		StrCopyUpToLength (pULCourante->show, pStr, c_nb_car_ex_mes);
		pULCourante ++;
		nNbUls++;
		}
	else
		{
		}

  DWORD nb_transfere;
	// parcours le texte jusqu'� sa fin ou le nb max d'ul atteint
  for (int n_ul = 0; (*pStr != '\0') && (n_ul < nb_max_ul); n_ul++)
    {
		// r�cup�re l'UL suivante
    extrait_ul (n_ul, pStr, v_ul[n_ul].show, &nb_transfere);
		DetermineGenreUL (v_ul+n_ul);
		// note l'avancement
    pStr += nb_transfere;

		// saute les s�parateurs
    pStr = pPremierCarNonSeparateur (pStr);
    }

  //DetermineGenreULs (n_ul, v_ul);
  (*pNbUls) = n_ul;
  }
*/

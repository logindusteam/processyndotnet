//----------------------------------------------------------------------------//
// Const
//----------------------------------------------------------------------------

#define b_id_auto_pcs  29

// "Groupes connus"
typedef enum
	{
  ELEMENT_PAGE_GRAPHIQUE = 1
	} ID_GROUPE_AUTO_IDENTIFICATION;

typedef struct
	{
	ID_GROUPE_AUTO_IDENTIFICATION eIdGroupe;
	__int64 i64IdAutoProchain;
	} ID_AUTO_PCS, *PID_AUTO_PCS;


// renvoi un identifiant et positionne le prochain � la valeur pass�e + 1
PSTR GetIdAutoPcs (ID_GROUPE_AUTO_IDENTIFICATION eIdGroupe, PSTR pStrId);

// Positionne le prochain identifiant � la valeur pass�e + 1 si inf�rieur
BOOL SetIdAutoPcs (ID_GROUPE_AUTO_IDENTIFICATION eIdGroupe, PSTR pStrId);

//---------------------------------------------------------------------------
// Gestionaire de chaines Str...()
//---------------------------------------------------------------------------

#ifndef USTR_H
#define USTR_H

//---------------------------------------------------------------------------
#define STR_NOT_FOUND  MAXDWORD
#define STR_MAX_CHAR_ARRAY 260
#pragma warning(disable : 4996)

//---------------------------------------------------------------------------
// caractéristiques d'une chaine
_inline BOOL StrIsNull (PCSTR pStr)  {return (pStr [0] == '\0');}
_inline DWORD StrLength (PCSTR pStr)  {return (strlen (pStr));}

// comparaison de chaines
__inline INT StrCompare (PCSTR pStr1, PCSTR pStr2) {return (strcmp (pStr1, pStr2));}
__inline INT StrICompare (PCSTR pStr1, PCSTR pStr2) {return (stricmp (pStr1, pStr2));}
BOOL bStrEgales		(PCSTR pStr1, PCSTR pStr2);
_inline BOOL bStrIEgales (PCSTR pStr1, PCSTR pStr2) {return (stricmp (pStr1, pStr2) == 0);}


// recherches dans une chaine
DWORD StrSearchStr					(PCSTR pStr1, PCSTR pStr2);
DWORD StrSearchChar					(PCSTR pStr, CHAR cChar);
//
_inline BOOL StrCharIsInStr (CHAR cChar, PCSTR pStrSetOfChar) {return StrSearchChar (pStrSetOfChar, cChar) != STR_NOT_FOUND;}
BOOL StrAllCharsAreInStr		(PSTR pStr, PCSTR pStrSetOfChar);
DWORD StrFirstCharInStr			(PSTR pStr, PCSTR pStrSetOfChar);
_inline CHAR StrGetChar (PCSTR pStr, UINT wIdx) {return pStr [wIdx];}

//---------------------------------------------------------------------------
// modifications de chaine
_inline PSTR StrSetLength (PSTR pStr, UINT wLen) {pStr [wLen] = '\0';  return pStr;}
_inline PSTR StrSetNull (PSTR pStr)  {pStr [0] = '\0';  return pStr;}
//
_inline PSTR StrCopy (PSTR pStrDst, PCSTR pStrSrc) {return (strcpy (pStrDst, pStrSrc));}
PSTR    StrCopySans0Terminateur (PSTR pStrDst, PCSTR pStrSrc);
PSTR    StrCopyUpToChar      (PSTR pStrDst, PCSTR pStrSrc, CHAR cChar);
PSTR    StrCopyUpToLength    (PSTR pStrDst, PCSTR pStrSrc, UINT wLen);
PSTR    StrExtract           (PSTR pStrDst, PCSTR pStrSrc, UINT wIdxChar, UINT wCharCount);
//
_inline PSTR StrConcat (PSTR pStrDst, PCSTR pStrSrc) {return (strcat (pStrDst, pStrSrc));}
PSTR    StrConcatChar        (PSTR pStrDst, CHAR cChar);
PSTR    StrInsertStr         (PSTR pStrDst, PCSTR pStrSrc, UINT wIdxChar);
PSTR    StrInsertChar        (PSTR pStrDst, CHAR cChar, UINT wIdxChar);
//
PSTR    StrDelete            (PSTR pStr, UINT wIdxStart, UINT wCount);
//
_inline PSTR StrSetChar (PSTR pStr, UINT wIdx, CHAR cChar) {pStr [wIdx] = cChar; return pStr;}
//
_inline PSTR StrUpperCase (PSTR pStr) {return strupr (pStr);}
_inline PSTR StrLowerCase (PSTR pStr) {return strlwr (pStr);}
CHAR     StrUpperCaseChar     (CHAR cChar);
CHAR     StrLowerCaseChar         (CHAR cChar);
//
PSTR    StrEnclose               (PSTR pStr, CHAR cChar1, CHAR cChar2);
PSTR    StrAddDoubleQuotation    (PSTR pStr);
//
PSTR    StrDeleteAllChars        (PSTR pStr, CHAR cChar);
PSTR    StrDeleteDoubleQuotation (PSTR pStr);
PSTR    StrDeleteSpaces          (PSTR pStr);
PSTR    StrDeleteFirstChars      (PSTR pStr, CHAR cChar);
PSTR    StrDeleteFirstSpaces     (PSTR pStr);
PSTR    StrDeleteLastChars       (PSTR pStr, CHAR cChar);
PSTR    StrDeleteLastSpaces      (PSTR pStr);
PSTR		StrDeleteFirstAndLastSpacesAndTabs (PSTR pStr);
//
BOOL		StrCharCanBeDisplayed    (CHAR cChar);
BOOL		StrStringCanBeDisplayed  (PCSTR pStr);
PSTR		StrMakeStringDisplayable (PSTR pStr);
//
PSTR    StrFilter                (PSTR pStr, CHAR cCharMin, CHAR cCharMax);

// conversions  de variable en chaine
PCHAR StrCHARToStr  (PSTR pStr, CHAR   cSource);
PSTR  StrBOOLToStr  (PSTR pStr, BOOL   bSource, PCSTR pStr0, PCSTR pStr1);
PSTR  StrBYTEToStr  (PSTR pStr, BYTE   bySource);
PSTR  StrWORDToStr  (PSTR pStr, WORD   wSource);
PSTR  StrDWordToStr (PSTR pStr, DWORD  dwSource);
PSTR  StrINTToStr   (PSTR pStr, INT    nSource);
PSTR  StrLONGToStr  (PSTR pStr, LONG   lSource);
PSTR  StrBYTEToStrBase  (PSTR pStr, BYTE   bySource,UINT wRadix);
PSTR  StrWORDToStrBase  (PSTR pStr, WORD   wSource, UINT wRadix);
PSTR  StrDWordToStrBase (PSTR pStr, DWORD  dwSource,UINT wRadix);
PSTR  StrINTToStrBase   (PSTR pStr, INT    nSource, UINT wRadix);
PSTR  StrLONGToStrBase  (PSTR pStr, LONG   lSource, UINT wRadix);
PSTR  StrFLOATToStr (PSTR pStr, FLOAT  fSource);
PSTR  StrDOUBLEToStr(PSTR pStr, DOUBLE dSource);

// conversions de chaine en variable
BOOL  StrToCHAR  (PCHAR	 pcChar, PCSTR pStr);
BOOL  StrToBOOL  (PBOOL	 pbBool, PCSTR pStr, PCSTR pStr0, PCSTR pStr1);
BOOL  StrToBYTE  (PBYTE	 pbByte, PCSTR pStr);
BOOL  StrToWORD  (PWORD	 pwWord, PCSTR pStr);
BOOL  StrToDWORD (PDWORD pdword, PCSTR pStr);
BOOL  StrToINT	 (PINT	 pint,   PCSTR pStr);
BOOL  StrToLONG	 (PLONG	 plong,  PCSTR pStr);
BOOL  StrToSHORT (SHORT *pshort, PCSTR pStr);
BOOL  StrToI64   (__int64 *pint64, PCSTR pStr);
BOOL  StrToFLOAT (PFLOAT pfloat, PCSTR pStr);
BOOL StrToDOUBLE (double * prReal, PCSTR pStr);


BOOL  StrToBYTEBase  (PBYTE	 pbByte, PCSTR pStr, UINT wRadix);
BOOL  StrToWORDBase  (PWORD	 pwWord, PCSTR pStr, UINT wRadix);
BOOL  StrToDWORDBase (PDWORD pdword, PCSTR pStr, UINT wRadix);
BOOL  StrToINTBase	 (PINT	 pint,   PCSTR pStr, UINT wRadix);
BOOL  StrToLONGBase	 (PLONG	 plong,  PCSTR pStr, UINT wRadix);
BOOL  StrToSHORTBase (SHORT *pshort, PCSTR pStr, UINT wRadix);

// conversions de tableaux de chaine
DWORD   StrToStrArray	(CHAR ppStrArray [] [STR_MAX_CHAR_ARRAY], UINT wMaxArray, PSTR pStr);
PSTR    StrArrayToStr	(PSTR pStr, CHAR ppStrArray [] [STR_MAX_CHAR_ARRAY], UINT wArrayCount);
PSTR		StrArrayToStrSep (PSTR pStr, CHAR ppStrArray [] [STR_MAX_CHAR_ARRAY], UINT wArrayCount, CHAR cSeparateur);

// formatage de chaines
INT __cdecl  StrPrintFormat  (PSTR pStr, PCSTR pStrFormat, ...);

//---------------------------------------------------------------------------
//
#endif //USTR_H

// ------------- Coordonnées Généralisées Pour PROCESSYN -----

#define c_G_MIN_X     0
#define c_G_MIN_Y     0
#define c_G_MAX_X  4095
#define c_G_MAX_Y  3071
#define c_G_NB_X   (c_G_MAX_X - c_G_MIN_X + 1)
#define c_G_NB_Y   (c_G_MAX_Y - c_G_MIN_Y + 1)



//
// Appli.cpp: implementation of the CAppli class.
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "std.h"
#include "UStr.h" // StrLength
#include "ULangues.h"
#include "PathMan.h"
#include "USignale.h"
#include "appli.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAppli::CAppli()
	{
	//variables de l'application
	hinst = 0;											// HINST process
	hinstDllLng = 0;								// HINST DLL infos langues
	hwnd = NULL;										// HWND fen�tre principale de l'appli
	StrCopy (szPathExe, "..\\..");  // Chemin courant de l'application
	StrCopy (szNomExe, "?");				// Nom de l'exe
	StrCopy (szNom, "Processyn");		// Nom pour l'utilisateur de l'application
	StrCopy (szVersion, "");				// Nom pour l'utilisateur de l'application

	if (!GetCurrentDirectory (MAX_PATH, szPathInitial))
		StrSetNull(szPathInitial);
	lcid = LOCALE_USER_DEFAULT;									// Locale Identifier de l'interface courant
	StrCopy (szAbbrevLangueUtilisateur, "?");	// 
	StrCopy (szLangueUtilisateur, "Langue par d�faut");	// 
	GetTailleEcran();		// Taille de l'�cran (Cf. GetTailleEcranAppli())
	GetNbBitsParPixel();
	rectEcran.left = 0;		// Taille de l'�cran
	rectEcran.right = 0;		// Taille de l'�cran
	rectEcran.top = 0;		// Taille de l'�cran
	rectEcran.bottom = 0;		// Taille de l'�cran
	static const	OSVERSIONINFOEX OS_VERSION_DEFAUT = {0,0,0,0,0,"?"};
	OSVersionInfoEx = OS_VERSION_DEFAUT;
	cSeparateurListe = ';';
	dwCodePage = 0;
	byCharSetFont = ANSI_CHARSET; // Jeu de caract�re � utiliser dans les polices type TURKISH_CHARSET;
	nOrdreDateInternationale = JMA;
	}

CAppli::~CAppli()
	{

	}


// ----------------------------------------------------------------------
// r�cup�re la taille de l'�cran (en pixels) et la met dans
// sizEcran et rectEcran
void CAppli::GetTailleEcran (void)
	{
	sizEcran.cx = GetSystemMetrics(SM_CXSCREEN);
	sizEcran.cy = GetSystemMetrics(SM_CYSCREEN);
	rectEcran.right = sizEcran.cx;
	rectEcran.bottom = sizEcran.cy;
	}

// ----------------------------------------------------------------------
// Mise � jour des polices � utiliser � partir du code page � utiliser
BOOL CAppli::SetCodePagePourFont ()
	{
	BOOL bRes = FALSE;
	static DWORD aAnsiCodePage[64]= 
		{
		1252,	//0 1252 Latin 1 
		1250,	//1 1250 Latin 2: Central Europe 
		1251,	//2 1251 Cyrillic 
		1253,	//3 1253 Greek 
		1254,	//4 1254 Turkish 
		1255,	//5 1255 Hebrew 
		1256,	//6 1256 Arabic 
		1257,	//7 1257 Baltic 
		1258,	//8 1258 Vietnamese 
		-1,		//9  Reserved for ANSI 
		-1,		//10  Reserved for ANSI 
		-1,		//11  Reserved for ANSI 
		-1,		//12  Reserved for ANSI 
		-1,		//13  Reserved for ANSI 
		-1,		//14  Reserved for ANSI 
		-1,		//15  Reserved for ANSI  
		//ANSI and OEM   
		874,	//16 874 Thai 
		932,//17 932 Japanese, Shift-JIS 
		936,//18 936 Simplified Chinese (PRC, Singapore)  
		949,//19 949 Korean Unified Hangul Code (Hangul TongHabHyung Code) 
		950,//20 950 Traditional Chinese (Taiwan; Hong Kong SAR, PRC)  
		1361,//21 1361 Korean (Johab) 
		-1,		//22  Reserved for alternate ANSI and OEM 
		-1,		//23  Reserved for alternate ANSI and OEM 
		-1,		//24  Reserved for alternate ANSI and OEM 
		-1,		//25  Reserved for alternate ANSI and OEM 
		-1,		//26  Reserved for alternate ANSI and OEM 
		-1,		//27  Reserved for alternate ANSI and OEM 
		-1,		//28  Reserved for alternate ANSI and OEM 
		-1,		//29  Reserved for alternate ANSI and OEM 
		-1,		//30  Reserved by system. 
		-1,		//31  Reserved by system. 
		//OEM   
		-1,		//32  Reserved for OEM 
		-1,		//33  Reserved for OEM 
		-1,		//34  Reserved for OEM 
		-1,		//35  Reserved for OEM 
		-1,		//36  Reserved for OEM 
		-1,		//37  Reserved for OEM 
		-1,		//38  Reserved for OEM 
		-1,		//39  Reserved for OEM 
		-1,		//40  Reserved for OEM 
		-1,		//41  Reserved for OEM 
		-1,		//42  Reserved for OEM 
		-1,		//43  Reserved for OEM 
		-1,		//44  Reserved for OEM 
		-1,		//45  Reserved for OEM 
		-1,		//46  Reserved for OEM 
		1258,	//47 1258 Vietnamese 
		869,	//48 869 Modern Greek 
		866,	//49 866 Russian 
		865,	//50 865 Nordic 
		864,	//51 864 Arabic 
		863,	//52 863 Canadian French 
		862,	//53 862   
		861,	//54 861 Icelandic 
		860,	//55 860 Portuguese 
		857,	//56 857 Turkish 
		855,	//57 855 Cyrillic; primarily Russian 
		852,	//58 852 Latin 2 
		775,	//59 775 Baltic 
		737,	//60 737 Greek; formerly 437G 
		708,	//61 708 Arabic; ASMO 708 
		850,	//62 850 Multilingual Latin 1 
		437		//63 437 US 
		};
	// Chargement du caract�re s�parateur de liste (un seul car support�)
	for(BYTE byNCharSet = 0; byNCharSet < 64; byNCharSet++)
		{
		if (aAnsiCodePage[byNCharSet]== dwCodePage)
			{
			byCharSetFont = byNCharSet;
			bRes = TRUE;
			break;
			}
		}
	return bRes;
	}

// ----------------------------------------------------------------------
// r�cup�re le Nb de bits par pixel de l'�cran
void CAppli::GetNbBitsParPixel (void)
	{
	// 
	HDC iDCHandle = GetDC(0);

	INT nNbPlans = GetDeviceCaps(iDCHandle, PLANES);
	INT nNbBitsPixel = GetDeviceCaps(iDCHandle,BITSPIXEL);

	nNbBitsParPixel = nNbPlans * nNbBitsPixel;
	}

// ----------------------------------------------------------------------
// Charge les infos et la DLL correspondant � la langue courante
void CAppli::MiseAJourLangue (void)
	{


	// si n�cessaire, lib�re la Dll d�ja charg�e
	if (hinstDllLng)
		{
		FreeLibrary (hinstDllLng);
		hinstDllLng = NULL;
		}
	
	// r�cup�re les infos de langue :
	// Essai de chargement langue utilisateur avec sous langage
	lcid = lcidUtilisateur();
	bGetAbbreviationLangue (lcid, szAbbrevLangueUtilisateur, sizeof(szAbbrevLangueUtilisateur));

	// Chargement du caract�re s�parateur de liste (un seul car support�)
	CHAR szSeparateur[5];
	if (GetLocaleInfo(lcid, LOCALE_SLIST, szSeparateur, 5) == 2)
		cSeparateurListe = szSeparateur[0];

	// Chargement du jeu de caract�re dans la base de registre
	CHAR szCodePageFont[6];
	if (GetLocaleInfo(lcid, LOCALE_IDEFAULTANSICODEPAGE, szCodePageFont, 6) != 0)
		{
		if (StrToDWORD(&dwCodePage, szCodePageFont) )
			SetCodePagePourFont();
		}

	// Chargement du caract�re s�parateur de liste (un seul car support�)
	CHAR szIDate[5];
	if (GetLocaleInfo(lcid, LOCALE_IDATE, szIDate, 5) != 0)
		{
		DWORD dwNOrdreDate = 0;
		if (StrToDWORD(&dwNOrdreDate, szIDate) && (dwNOrdreDate <= (DWORD)AMJ))
			nOrdreDateInternationale = (eOrdreDate)dwNOrdreDate;
		}
	// charge la DLL de ressources dans la langue courante
	hinstDllLng = hChargeDLLLangue (szNomExe, szAbbrevLangueUtilisateur);
	
	// Dll non trouv�e => on essaye de la charger dans la langue principale
	if (!hinstDllLng)
		{
		lcid = lcidLangagePrincipal (lcid);
		szAbbrevLangueUtilisateur[2] ='\0';

		// charge la DLL de ressources dans la langue courante
		hinstDllLng = hChargeDLLLangue (szNomExe, szAbbrevLangueUtilisateur);
		}

	// Dll non trouv�e => on essaye de la charger en anglais
	if (hinstDllLng)
		// oui => on lit le nom de la langue
		bGetNomLangue (lcid, szLangueUtilisateur, sizeof(szLangueUtilisateur));
	else
		{
		lcid = LOCALE_NEUTRAL;
		StrCopy (szAbbrevLangueUtilisateur, "EN");
		StrCopy (szLangueUtilisateur, "Default : standard english");

		// charge la DLL de ressources dans la langue courante
		hinstDllLng = hChargeDLLLangue (szNomExe, szAbbrevLangueUtilisateur);
		}

	// Dll charg�e ?
	if (!hinstDllLng)
		{
		// non => on prend l'instance de l'appli par d�faut
		char szErr[MAX_PATH + 40];

		lcid = LANG_NEUTRAL;
		hinstDllLng = NULL;
		StrCopy (szAbbrevLangueUtilisateur, "None");
		StrCopy (szLangueUtilisateur, "Not loaded");

		// Ferme l'application en signalant la raison
		StrPrintFormat (szErr, "%s cannot execute : missing language DLL", szNomExe);
		SignaleExit(szErr);
		}
	}

// ----------------------------------------------------------------------
// mise � jour du hinst, du nom et path de l'exe et du nom courant de l'application
// et met � jour la taille de l'�cran
void CAppli::Init (HINSTANCE hInst, PCSTR pszNomAppliNouveau, PCSTR pszVersion)
	{
	PSTR pszPointeurNomExe;

	// note ou r�cup�re les param�tres courants de l'application
	hinst = hInst;
	if (GetFullPathName (_pgmptr, MAX_PATH, szPathExe, &pszPointeurNomExe) < MAX_PATH)
		{
		StrCopy (szNomExe, pszPointeurNomExe);
		pszPointeurNomExe [0] = '\0'; // Astuce qui permet de ne conserver que le path dans szPathExe
		}
	else
		{
		StrSetNull (szPathExe);
		StrSetNull (szNomExe);
		}
	if (pszNomAppliNouveau && (StrLength (pszNomAppliNouveau) < 80))
		StrCopy (szNom, pszNomAppliNouveau);
	if (pszVersion && (StrLength (pszVersion) < MAX_PATH))
		StrCopy (szVersion, pszVersion);
	GetTailleEcran();
	GetNbBitsParPixel();

	// r�cup�re la version courante de la version de l'OS
	OSVersionInfoEx.dwOSVersionInfoSize = sizeof(OSVersionInfoEx);
	GetVersionEx ((LPOSVERSIONINFO)&OSVersionInfoEx);

	// Charge les infos et la DLL correspondant � la langue courante
	MiseAJourLangue ();
	} // Init

// ----------------------------------------------------------------------
// enregistre le nouveau handle de la fen�tre principale de l'application
BOOL CAppli::bSetHWndPrincipal (HWND hwndAppliNouveau)
	{
	if (IsWindow(hwndAppliNouveau))
		hwnd = hwndAppliNouveau;
	else
		hwnd = NULL;

	return (hwnd != NULL);
	}

// ----------------------------------------------------------------------
// enregistre le nouveau handle de la fen�tre principale de l'application
void CAppli::FormateDateInternationale (PSTR pszText, DWORD dwJour, DWORD dwMois, DWORD dwAn) const
  {
	switch (Appli.nOrdreDateInternationale)
    {
		case JMA:
			StrPrintFormat (pszText, "%02d/%02d/%02d", dwJour, dwMois, dwAn);
			break;
		case AMJ:
			StrPrintFormat (pszText, "%02d/%02d/%02d", dwAn, dwMois, dwJour);
			break;
		//case MJA:
		default:
			StrPrintFormat (pszText, "%02d/%02d/%02d", dwMois, dwJour, dwAn);
    }
  }

// R�cup�re le nom de la Version d'OS couramment utilis�e
void CAppli::GetNomOS (PSTR pszNomOSDest)
	{
	if (pszNomOSDest)
		{
		switch (Appli.OSVersionInfoEx.dwPlatformId)
			{
			case VER_PLATFORM_WIN32s: wsprintf(pszNomOSDest, "Win32s on Windows 3.1"); break;

			case VER_PLATFORM_WIN32_WINDOWS:
				{
				switch (Appli.OSVersionInfoEx.dwMajorVersion)
					{
					case 4: // Famille Windows 95
						{
						switch (Appli.OSVersionInfoEx.dwMinorVersion)
							{
							case 0:	wsprintf(pszNomOSDest, "Windows 95"); break;
							case 10: wsprintf(pszNomOSDest, "Windows 98"); break;
							case 90: wsprintf(pszNomOSDest, "Windows Me"); break;
							default: wsprintf(pszNomOSDest, "Windows 95 (%lu.%lu)", Appli.OSVersionInfoEx.dwMajorVersion, Appli.OSVersionInfoEx.dwMinorVersion); break;
							}
						} break;
					default: wsprintf(pszNomOSDest, "Windows (%lu.%lu)", Appli.OSVersionInfoEx.dwMajorVersion, Appli.OSVersionInfoEx.dwMinorVersion); break;
					}
				} break;

			case VER_PLATFORM_WIN32_NT: 
				{
				switch (Appli.OSVersionInfoEx.dwMajorVersion)
					{
					case 5: // Famille NT, 2000, XP, 2003
						{
						switch (Appli.OSVersionInfoEx.dwMinorVersion)
							{
							case 0:	wsprintf(pszNomOSDest, "Windows 2000"); break;
							case 1: wsprintf(pszNomOSDest, "Windows XP"); break;
							case 2: wsprintf(pszNomOSDest, "Windows Server 2003"); break;
							default: wsprintf(pszNomOSDest, "Windows %lu.%lu", Appli.OSVersionInfoEx.dwMajorVersion, Appli.OSVersionInfoEx.dwMinorVersion); break;
							}
						} break;
					case 6: // Famille Vista, ...
						{
						BOOL bTypeWorkStation = Appli.OSVersionInfoEx.wProductType == VER_NT_WORKSTATION;
						if (bTypeWorkStation)
							{
							switch (Appli.OSVersionInfoEx.dwMinorVersion)
								{
								case 0:	wsprintf(pszNomOSDest, "Windows Vista"); break;
								case 1: wsprintf(pszNomOSDest, "Windows 7"); break;
								case 2: wsprintf(pszNomOSDest, "Windows 8"); break;
								case 3: wsprintf(pszNomOSDest, "Windows 8.1"); break;
								default: wsprintf(pszNomOSDest, "Windows %lu.%lu", Appli.OSVersionInfoEx.dwMajorVersion, Appli.OSVersionInfoEx.dwMinorVersion); break;
								}
							}
						else
							{
							switch (Appli.OSVersionInfoEx.dwMinorVersion)
								{
								case 0:	wsprintf(pszNomOSDest, "Windows Server 2008"); break;
								case 1: wsprintf(pszNomOSDest, "Windows Server 2008 R2"); break;
								case 2: wsprintf(pszNomOSDest, "Windows Server 2012"); break;
								case 3: wsprintf(pszNomOSDest, "Windows Server 2012 R2"); break;
								default: wsprintf(pszNomOSDest, "Windows %lu.%lu", Appli.OSVersionInfoEx.dwMajorVersion, Appli.OSVersionInfoEx.dwMinorVersion); break;
								}
							}
						} break;
					default: wsprintf(pszNomOSDest, "Windows NT %lu.%lu", Appli.OSVersionInfoEx.dwMajorVersion, Appli.OSVersionInfoEx.dwMinorVersion); break;
					}
				} break;
			}
		}
	}

// Instancie Syst�matiquement une appli
CAppli Appli;


/*--------------------------------------------------------------------------+
 | Ce fichier est la propriete de					    |
 |		    Societe LOGIQUE INDUSTRIE				    |
 |	     Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3		    |
 | Il est demeure sa propriete exclusive et est confidentiel.		    |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/
//
// Gerebdc.h
//
// Gestion des Variables Entrees Sorties et des constantes de l'applicatif utilisateur
// Win32 10/6/98 JS


// ------------------------------------------------------------------------
// Aide � la cr�ation de variables d'Entrees Sorties
// ------------------------------------------------------------------------
typedef struct
	{
	BOOL bConstanteExiste;
	INT	nULConstante;
	DWORD dwPosConstante; // Id de la constante
	} CONSTANTE_RECONNUE_CF, *PCONSTANTE_RECONNUE_CF;
const CONSTANTE_RECONNUE_CF CONSTANTE_RECONNUE_CF_NULLE = {0,0,0};
typedef struct
	{
	DWORD					dwPosESExistante; // pos ES existante - 0 si ES n'existe pas. 
	ENTREE_SORTIE	es;								// Infos lues si existe - � compl�ter sinon
	INT						nULVariable;			// nULVariable Num�ro de l'UL dans laquelle on effectue la sortie
	} VARIABLE_RECONNUE_CF, *PVARIABLE_RECONNUE_CF;


// Verifie si l'UL est un identifiant de var valide (format Ok, ni mot r�serv� ni constante)
// Si oui, *pVarReconnue est mise � jour : une recherche dans la base est effectu�e pour savoir si
// l'identifiant est d�ja r�f�renc� dans la base
// Renvoie TRUE s'il s'agit d'un identifiant de var valide, FALSE sinon.
BOOL bReconnaitESValide (int nUL, PUL ul, PVARIABLE_RECONNUE_CF pVarReconnue);

// Verifie si l'UL est un identifiant de var valide (format Ok, ni mot r�serv� ni constante)
// Si oui, une recherche dans la base est effectu�e pour savoir si
// l'identifiant est d�ja r�f�renc� dans la base (*pbExisteDansBase et *pdwPositionTrouvee mis � jour)
// Renvoie TRUE s'il s'agit d'un identifiant de var valide, FALSE sinon.
BOOL bReconnaitESValide (PUL ul, BOOL *pbExisteDansBase, DWORD *pdwPositionIdentTrouvee);

// Cherche une variable d'apr�s son nom
// Si trouv�e, renvoie TRUE et sa position dans le bloc des entr�es sorties
// Renvoie FALSE sinon.
BOOL bReconnaitESExistante (PUL pUL, DWORD *position_bes);

// Cherche une variable d'apr�s son nom et son type (c_res_logique, c_res_numerique, c_res_message)
// Si trouv�e, et de m�me type renvoie TRUE et sa position dans le bloc des entr�es sorties
// Renvoie FALSE sinon.
BOOL bReconnaitESExistanteDeType (PUL pUL, DWORD *position_bes, ID_MOT_RESERVE idType);

void InsereVarES (PENTREE_SORTIE	pESInitial, PCSTR pszNomVarES, DWORD * pdwPositionNouvelleES);

// Compare une chaine et le nom d'une variable d'entr�e sortie 
// � partir de sa position d'entr�e sortie
BOOL bStrEgaleANomVarES (PCSTR pszNom, DWORD dwPositionES);


//-----------------------------------------------------------------------
// Gestion des ES existantes
//-----------------------------------------------------------------------

// Compare (Hors casse) une chaine et le nom d'une variable d'entr�e sortie 
// � partir de sa position d'entr�e sortie
BOOL bStrIEgaleANomVarES (PCSTR pszNom, DWORD dwPositionES);

// Renvoie TRUE si une variable d'entr�e sortie sp�cifi�e par sa position
// d'entr�e sortie est num�rique, FALSE sinon
BOOL bESEstNumerique (DWORD dwPositionES);


// Renvoie le genre (c_res_logique, ...) d'une variable d'entr�e sortie sp�cifi�e
// par sa position d'entr�e sortie
ID_MOT_RESERVE nGenreES (DWORD dwPositionES);

// Renvoie TRUE si une variable d'entr�e sortie sp�cifi�e par sa position
// d'entr�e sortie est logique, FALSE sinon
BOOL bESEstLogique (DWORD dwPositionES);

// Renvoie 0 si variable simple, sinon le Nb d'�l�ments du tableau
DWORD dwTailleES (DWORD dwPositionES);

BOOL es_referencee (DWORD position_es);

// R�cup�re les infos d'entr�e sortie d'une Var ES Existante
void InfoVarES (PENTREE_SORTIE pEntreeSortie, DWORD dwPositionES);

void consulte_es_bd_c (DWORD position_es, char *ident, char *sens, char *genre);

// R�cup�re le nom d'une variable d'entr�e sortie 
// � partir de sa position d'entr�e sortie
void NomVarES (PSTR pszNomES, DWORD dwPositionES);

BOOL bSupprimeES (DWORD position_es);

// si M�me genre que l'ancienne ES et plus de r�f�rences
// renvoie TRUE dans ce cas, FALSE sinon
BOOL bModifieDeclarationES (PENTREE_SORTIE pNouveauDescriptifES, PCSTR pszNouveauNom, DWORD dwPositionESAModifier);

void ReferenceES (DWORD num_enr);

void DeReferenceES (DWORD num_enr);

//--------------------------------------------------------------------------
// POUR UTILITAIRES acc�s aux ES � partir de la position des identifs
//--------------------------------------------------------------------------
// Renvoie l'Id d'une variable ES � partir de sa position d'identifiant
DWORD dwIdVarESFromPosIdentif (DWORD position_identif);

// R�cup�re le nom d'une variable d'entr�e sortie 
// � partir de sa position d'identifiant(char nom[c_nb_car_es])
void NomES (PSTR pszNomES, DWORD position_identif);

//--------------------------------------------------------------------------
// Identification de mots r�serv�s
//--------------------------------------------------------------------------
// Cherche un mot dans la liste des mots r�serv�s.
// S'il y est reconnu, renvoie TRUE et *pdwNMotReserve est mis � jour
// Renvoie FALSE sinon
BOOL bReconnaitMotReserve (PCSTR pszMotAReconnaitre, ID_MOT_RESERVE *pdwNMotReserve);

//--------------------------------------------------------------------------
// Acc�s aux constantes
//--------------------------------------------------------------------------
// Renvoie TRUE si l'ul pass� est une constante valide;
// si oui renvoie son genre, sa valeur et son �ventuelle existence dans la table des constantes
BOOL bReconnaitConstante
	(const UL * ul, BOOL * pbValeurLog, FLOAT * prValeurNum,
	BOOL * pbEstLog, BOOL * pbEstNum, BOOL * pbExisteDansConstantes, DWORD * pdwPositionTrouve);

// Renvoie TRUE si l'ul pass� est une constante valide;
// si oui renvoie son genre
BOOL bReconnaitConstanteValide
	(const UL * pUL, PID_MOT_RESERVE	pIdGenre);

// Apr�s recherche : ajoute une nouvelle constante ou une r�f�rence � une constante existante
void AjouteConstanteBd (DWORD pos_alphabetic, BOOL bExisteDeja, PCSTR pszValeur, DWORD * pdwIdConstante);

// Ajoute une nouvelle constante ou une r�f�rence � une constante existante
void AjouteConstanteBd (PCSTR pszValeur, DWORD * pdwIdConstante);

void consulte_cst_bd_c (DWORD pos_pt_cste, char *valeur);

void supprime_cst_bd_c (DWORD pos_pt_cste);

void modifie_cst_bd_c (char *valeur, DWORD *pos_pt_cst);

void action_cst_bd_c (REQUETE_EDIT action, char *valeur, DWORD pos_pt_cste);

BOOL renomme_cst_bd_c (char *new_valeur, DWORD pos_pt_cst);

// Verifie si l'UL est une constante num�rique valide (format Ok)
// Si oui, une recherche dans la base est effectu�e pour savoir si
// la constante est d�ja r�f�renc�e dans la base (*pbExisteDansBase et *pdwPositionTrouvee mis � jour)
// Renvoie TRUE s'il s'agit d'une constante num�rique valide, FALSE sinon.
BOOL bReconnaitConstanteNum (PUL ul, BOOL *pbExisteDansBase, FLOAT *valeur, DWORD *position_trouve);

// Renvoie TRUE si l'ul pass� est une constante message valide;
// si oui renvoie son �ventuelle existence dans la table des constantes
BOOL bReconnaitConstanteMessage 
	(const UL * ul, BOOL * pbExisteDansConstantes, DWORD * pdwPositionTrouve);

// ------------------------------------------------------------------------
//	Divers $$
// ------------------------------------------------------------------------
// Cherche une chaine dans bn_constant
BOOL bTrouveChaineDansConstant (PCSTR pszChaine, DWORD * pdwNInsertion);

// Cherche une chaine dans bn_identif
BOOL bTrouveChaineDansIdenti (PCSTR pszChaine, DWORD * pdwNInsertion);

// Cherche une chaine dans b_organise_es
BOOL bTrouveChaineDansOrganiseES (PCSTR pszChaine, DWORD * pdwNInsertion);

// Cherche une chaine dans b_organise_cle
BOOL bTrouveChaineDansOrganiseCle (PCSTR pszChaine, DWORD * pdwNInsertion);

// ------------------------------- fin Gerebdc.h ----------------------------------



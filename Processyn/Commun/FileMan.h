//---------------------------------------------------------------------------
// FileMan.h
// Gestion des fichiers textes et acc�s direct, disques et r�pertoires
// Version Win32
// Les erreurs renvoy�es sont des erreurs WIN32
//---------------------------------------------------------------------------

#ifndef FILEMAN_H
#define FILEMAN_H

DECLARE_HANDLE (HFIC);
typedef HFIC * PHFIC;
#define INVALID_HFIC	((HFIC)NULL)

//					Ouverture / fermeture de fichiers
// Flags : (piqu�s aux MFC)
// Sharing and access mode. Specifies the action to take when opening the file. 
// You can combine options listed below by using the bitwise-OR (|) operator. 
// One access permission and one share option are required; 
// the modeCreate and modeNoInherit modes are optional.
#ifndef OPENFLAGS
#define OPENFLAGS
// ATTENTION : Les flags suivants sont d�clar�s dans CFman

//enum OpenFlags 
//	{
//	modeRead =          0x0000,	// Opens the file for reading only.
//	modeWrite =         0x0001,	// Opens the file for reading and writing.
//	modeReadWrite =     0x0002,	// Opens the file for writing only.
//	shareCompat =       0x0000,	// This flag is not available in 32 bit MFC. This flag maps to CFile::shareExclusive when used in CFile::Open.
//	shareExclusive =    0x0010,	// Opens the file with exclusive mode, denying other processes both read and write access to the file. Construction fails if the file has been opened in any other mode for read or write access, even by the current process.
//	shareDenyWrite =    0x0020,	// Opens the file and denies other processes write access to the file. Create fails if the file has been opened in compatibility mode or for write access by any other process.
//	shareDenyRead =     0x0030,	// Opens the file and denies other processes read access to the file. Create fails if the file has been opened in compatibility mode or for read access by any other process.
//	shareDenyNone =     0x0040,	// Opens the file without denying other processes read or write access to the file. Create fails if the file has been opened in compatibility mode by any other process.
//	modeNoInherit =     0x0080,	// Prevents the file from being inherited by child processes.
//	modeCreate =        0x1000,	// Directs the constructor to create a new file. If the file exists already, it is truncated to 0 length.
//	modeNoTruncate =    0x2000,	// Combine this value with modeCreate. If the file being created already exists, it is not truncated to 0 length. Thus the file is guaranteed to open, either as a newly created file or as an existing file. This might be useful, for example, when opening a settings file that may or may not exist already. This option applies to CStdioFile as well.
//	typeText =          0x4000, // Sets text mode with special processing for carriage return�linefeed pairs (used in derived classes only).
//	typeBinary =   (int)0x8000	// Sets binary mode (used in derived classes only).
//	};
//#define OF_OUVRE_LECTURE_SEULE (modeRead|shareDenyNone|modeNoTruncate) // Lecture non exclusive d'un fichier devant exister
//#define OF_OUVRE_LECTURE_SEULE_EXCLUSIF (modeRead|shareDenyWrite|modeNoTruncate) // Lecture exclusive d'un fichier devant exister
//#define OF_OUVRE (modeReadWrite|shareDenyNone|modeNoTruncate) // Lecture exclusive d'un fichier devant exister
//#define OF_OUVRE_EXCLUSIF (modeReadWrite|shareDenyRead|shareDenyWrite|modeNoTruncate) // Lecture exclusive d'un fichier devant exister
//#define OF_CREE (modeCreate|modeReadWrite|shareDenyNone) // Lecture �criture non exclusive d'un fichier cr��
//#define OF_CREE_EXCLUSIF (modeCreate|modeReadWrite|shareDenyRead|shareDenyWrite) // Lecture �criture exclusive d'un fichier cr��
//#define OF_OUVRE_OU_CREE (modeCreate|modeReadWrite|shareDenyNone|modeNoTruncate) // Lecture �criture non exclusive d'un fichier cr�� s'il n'existe pas
//#define OF_OUVRE_OU_CREE_EXCLUSIF (modeCreate|modeReadWrite|shareDenyRead|shareDenyWrite|modeNoTruncate) // Lecture �criture exclusive d'un fichier cr�� s'il n'existe pas
#define OF_DRIV_DQ (CFman::modeCreate|CFman::modeReadWrite|CFman::shareDenyNone|CFman::modeNoTruncate) // Mode d'ouverture de Driv_dq
#endif


//---------------------------------------------------------------------------
// Fichier binaire : ouverture
UINT uFileOuvre		(PHFIC phFile, PCSTR pszFile, DWORD dwTailleEnr, DWORD wFlags);

// Fichier texte : ouverture
UINT uFileTexteOuvre (PHFIC phFile, PCSTR pszFile, DWORD wFlags);

// Fermeture d'un fichier
UINT uFileFerme	(PHFIC phFile);

// Met � jour le Nb d'enr courant du fichier et renvoie une err syst�me
UINT uFileNbEnr (HFIC hFile, DWORD * pdwNbEnr);

//------------------------------------------------------------
// Acc�s direct (sp�cifi� en num�ro d'enr de 0 � N) - pointeurs lecture et �criture ind�pendants.

// Enregistre le num�ro du prochain enr � lire et renvoie une err syst�me
UINT uFilePointeLecture		(HFIC hFile, DWORD   wIdx);
// Recopie le Num�ro du prochain enr lu et renvoie une err syst�me
UINT uFilePointeurLecture	(HFIC hFile, DWORD * pwIdx);
// Enregistre le num�ro du prochain enr � �crire et renvoie une err syst�me
UINT uFilePointeEcriture	(HFIC hFile, DWORD   wIdx);
// Renvoie le num�ro du prochain Enr �crit et renvoie une err syst�me
UINT uFilePointeurEcriture(HFIC hFile, DWORD * pwIdx);

// Lecture
// Lecture de wNbEnrALire Enregistrements
UINT uFileLire (HFIC hFile, PVOID pBuffer, DWORD wNbEnrALire, DWORD * pdwNbEnrLus);
// Lecture de wNbEnrALire Enregistrements avec Err si lecture non compl�te
UINT uFileLireTout (HFIC hFile, PVOID pBuffer, DWORD dwNbEnrALire);
UINT uFileLireLn (HFIC hFile, PSTR pcBufferLn, DWORD wLnSize);
// Lecture acc�s direct d'1 Enr
UINT uFileLireDirect	(HFIC hFile, DWORD dwNEnr, PVOID pBuffer);

// Ecriture
UINT uFileEcrire (HFIC hFile, const void * pBuffer, DWORD dwNbEnrAEcrire, DWORD * pdwNbEnrEcrits);
// Lecture de wNbEnrALire Enregistrements avec Err si �criture non compl�te
UINT uFileEcrireTout (HFIC hFile,  const void * pBuffer, DWORD dwNbEnrAEcrire);
// Ecriture acc�s direct d'1 Enr
UINT uFileEcrireDirect (HFIC hFile, DWORD dwNEnr, const void * pBuffer);
// Ecriture s�quentielle d'une chaine de caract�re puis d'un Cr Lf
UINT uFileEcrireSzLn (HFIC hFile, PCSTR pszBufferLn);

//----------------------------------------------
// Renvoie le nom associ� � un handle ouvert
PCSTR pszFileNom (HFIC hFile);

//---------------------------------------------------------------------------
BOOL bFileExiste	(PCSTR pszFile);
UINT uFileRenomme (PCSTR pszFileSource, PCSTR pszFileDest);
UINT uFileEfface	(PCSTR pszFile);
// Recopie d'un fichier �ventuellement au bout d'un autre
UINT uFileCopie (PCSTR pszFileSource, PCSTR pszFileDest, BOOL bAjoutFinDest);

//---------------------------------------------------------------------------
// r�cup�re la longueur d'une variable d'environnement
UINT FileGetEnvVarLength   (PCSTR pszEnvVar);
// r�cup�re la valeur d'une variable d'environnement
void FileGetEnvVarValue    (PCSTR pszEnvVar, PSTR pszEnvValue);

#endif // FILEMAN_H


/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : tipe_de.h                                                |
 |   Auteur  : LM					        	|
 |   Date    : 02/02/94					        	|
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/
// Objets Processyn DDE
// Win32 4/9/97

//-------------------------- BLOCS EN DESCRIPTION
#define b_geo_dde                     114
#define b_spec_titre_paragraf_de      106
#define b_titre_paragraf_de           107
#define b_dde_e                       123
#define b_dde_r                       124
#define b_dde_e_dyn                   208
#define b_dde_r_dyn                   209

//-------------------------- BLOCS D'EXECUTION
#define bx_dde_lien_e                 125
#define bx_dde_lien_r                 126
#define bx_dde_e_advise               211
#define bx_dde_e_request              212
#define bx_dde_r                      213
#define bx_var_dde_client_dyn         210
#define bx_dde_var_serveur            263
#define b_sav_dde_e_dyn               264


#define TIMEOUTDDE                    3000  //3 secondes

//-------------------------- TYPES EN DESCRIPTION
typedef union
  {
  struct
    {
    char pszLien[80];
    char pszApplic[80];
    char pszTopic[80];
    DWORD wPosEsLien;
    DWORD wTimeOut;
    char pszParam[80];
    };
  } t_spec_titre_paragraf_de;

typedef struct
  {
  DWORD wNumeroRepere;
  union
    {
    DWORD wPosDonnees;
    struct
      {
      DWORD wPosParagraf;
      DWORD wPosSpecifParagraf;
      };
    struct
      {
      DWORD wPosEs;
      DWORD wPosInitial;
      DWORD wPosSpecifEs;
      };
    };
  } GEO_DDE, *PGEO_DDE;

typedef struct
  {
  DWORD wTaille;
  ID_MOT_RESERVE wSens;       // c_res_e ou c_res_variable_ta_e
  ID_MOT_RESERVE wGenre;      // c_res_logique, c_res_numerique, c_res_message
  char pszItem[80];
  DWORD wPosEsRaf;   // mettre 0 si pas de RAF : faire ADVISE
  } tc_dde_e;

typedef struct
  {
  DWORD wPosEs;
  char pszItem[80];
  DWORD wGenre;
  DWORD wPosEsRaf;   // mettre 0 si pas de RAF : variable simple
  } tc_dde_r;

//------------- Liens dynamiques
//
typedef struct
  {
  DWORD wTaille;
  ID_MOT_RESERVE wSens;       // c_res_e ou c_res_variable_ta_e
  ID_MOT_RESERVE wGenre;      // c_res_logique, c_res_numerique, c_res_message
  } tc_dde_e_dyn;

typedef struct
  {
  DWORD wPosEs;
  DWORD wGenre;
  } tc_dde_r_dyn;


// TYPES EN EXECUTION

// Un Handle sur une conversation DDE est un HCONV

// Lien pr�d�fini E pour les variables clientes en entr�e (REQUEST et ADVISE)
// (objets du bloc bx_dde_lien_e) - Ouvrir une conversation pendant toute la dur�e de l'exploitation.
typedef struct
  {
  char    pszLien[80];
  char    pszApplic[80];
  char    pszTopic[80];
  DWORD   wPosEsLien;
  BOOL		bMemLien;
  DWORD   wTimeOut;
  char    pszParam[80];
  HCONV		hconvClient;
  BOOL		bLienOk;
  BOOL		bLienDetruit;
  DWORD   wPremItemAdvise;
  DWORD   wNbItemAdvise;
  DWORD   wPremItemRequest;
  DWORD   wNbItemRequest;
  } tx_dde_lien_e;

// Lien pr�d�fini R pour les variables clientes en sortie (POKE)
// (objets du bloc ?) - Ouvrir une conversation pendant toute la dur�e de l'exploitation.
typedef struct
  {
  char    pszLien[80];
  char    pszApplic[80];
  char    pszTopic[80];
  DWORD   wPosEsLien;
  DWORD   wTimeOut;
  char    pszParam[80];
  HCONV		hconvClient;
  BOOL		bLienOk;
  BOOL		bLienDetruit;
  DWORD   wPremItem;
  DWORD   wNbItem;
  } tx_dde_lien_r;

// Variables clientes en entr�e (li�es au serveur et topic pr�d�fini � l'ex�cution)
// (objets du bloc bx_dde_e_advise) - Etablir une surveillance ADVISE et recevoir les ADVDATA du serveur li�.
typedef struct
  {
  DWORD   wPosEs;
  DWORD   wTaille;
  DWORD   wGenre;
  char    pszItem[80];
  BOOL		bDataRecue;
  } tx_dde_e_advise;

// Variables clientes en entr�e (li�es au serveur et topic pr�d�fini � l'ex�cution)
// (objets du bloc ?) - Demander un REQUEST au serveur li� � chaque cycle si RAF activ�.
typedef struct
  {
  DWORD	wPosEs;
  DWORD wTaille;
  DWORD wGenre;
  char	pszItem[80];
  DWORD wPosEsRaf;
  } tx_dde_e_request;

// Variables clientes en sortie (li�es au serveur et topic pr�d�fini � l'ex�cution)
// (objets du bloc bx_dde_r) - Envoyer un POKE au serveur li� � chaque cycle si RAF activ�.
typedef struct
  {
  DWORD wPosEs;
  char	pszItem[80];
  DWORD wGenre;
  UINT	wTaille;     // 0 si var simple (pas tableau)
  DWORD wPosEsRaf;   // mettre 0 si pas de RAF : variable simple
  } X_VAR_DDE_CLIENT_R, *PX_VAR_DDE_CLIENT_R;

// Variables CLIENT (li�es � l'aide du presse papier � l'ex�cution)
// (objets du bloc bx_var_dde_client_dyn) - Etablir une surveillance ADVISE et recevoir les ADVDATA du serveur li�.
typedef struct  
  {							
  DWORD wPosBxEs;
  BOOL	bLienOk;
  BOOL	bLienDetruit;
  BOOL	bDataRecue;
  DWORD wPosEs;
  DWORD wTaille;
  DWORD wGenre;
  HCONV hconvClient;
  } X_VAR_DDE_CLIENT_DYN, *PX_VAR_DDE_CLIENT_DYN;

// Variables export�es par le SERVEUR DDE Processyn
// (objets du bloc bx_dde_var_serveur) - recoivent REQUEST, POKE et ADVSTART.
typedef struct	
  {							
  DWORD  wPosBxEs;
  DWORD  wNbClientsEnAdvise;
  //void * ptrClients;
  } X_VAR_SERVEUR_DDE, *PX_VAR_SERVEUR_DDE; 

typedef struct // Objets du fichier "Appli".DDE, sauvegarde des liens des variables CLIENT
  {
  char   pszApplic[80];
  char   pszTopic[80];
  char   pszItem[80];
  DWORD  wPosItem;		// $$ Probl�me si changement dans les variables
  } t_sav_dde_e_dyn;

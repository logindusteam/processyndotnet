// -------------------------------------------------------------------------
// Bdgr.h Gestion de synoptique
// Version WIN32 30/4/97
// -------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"                 // std types
#include "MemMan.h"

#include "mem.h"                 // mem
#include "UStr.h"              //
#include "G_Objets.h"
#include "g_sys.h"               // g_xxx()
#include "pcsspace.h"            // Coordonn�es Min et MAX de l'espace
#include "dicho.h"               // Tri dicho pour espacement

#include "lng_res.h"
#include "Tipe.h"								// types de proc�dures du descripteur
#include "Descripteur.h"

#include "Verif.h"							// verbes maj inter_vi
#include "bdgr.h"

#include "bdanmgr.h"            // verbes maj inter_vi
#include "BdElemGr.h"
#include "BdPageGr.h"
#include "BdMarqGr.h"

VerifInit;

// constante Nop
const ELEMENT_PAGE_GR ELEMENT_PAGE_GR_NULL = 
	{
	G_ACTION_NOP,										//	G_ACTION						Action;
	G_COULEUR_INDETERMINEE,					//  G_COULEUR						nCouleurLigne;
	G_COULEUR_INDETERMINEE,					// 	G_COULEUR						nCouleurRemplissage;
	G_STYLE_LIGNE_INDETERMINE,			//  G_STYLE_LIGNE				nStyleLigne;
	G_STYLE_REMPLISSAGE_INDETERMINE,//  G_STYLE_REMPLISSAGE nStyleRemplissage;
	0,0,														// 	POINT								pt0;		// ancien x1, y1
	0,0,														// 	POINT								pt1;		// ancien x2, y2
	0,															//  DWORD								PosGeoAnimation;
	NULL,														// 	HWND								hwndControle;
};

// -------------------------------------------------------------------------
// Page
// -------------------------------------------------------------------------

// -------------------------------------------------------------------------
static void CreePageGR (DWORD Page, DWORD *ReferencePage)
  {
  DWORD           wDernierePage;
  DWORD           wNumeroPage;
  PGEO_PAGES_GR		pGeoPage;
  PMARQUES_GR			pMarque;
  PPAGE_GR				pPage;
  PGEO_POINTS_SAUVEGARDES_GR	pGeoSav;

	#define c_FEN_POS_X_DEFAUT    300
	#define c_FEN_POS_Y_DEFAUT    900
	#define c_FEN_POS_CX_DEFAUT  2930
	#define c_FEN_POS_CY_DEFAUT  1730

  if (Page > nb_enregistrements (szVERIFSource, __LINE__, B_GEO_PAGES_GR))
    {
    wDernierePage = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_PAGES_GR) + 1;
    insere_enr (szVERIFSource, __LINE__, Page - wDernierePage + 1, B_GEO_PAGES_GR, wDernierePage);
    for (wNumeroPage = wDernierePage; wNumeroPage <= Page; wNumeroPage++)
      {
      pGeoPage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, wNumeroPage);
      pGeoPage->ReferencePage = 0;
      }
    }
  (*ReferencePage) = nb_enregistrements (szVERIFSource, __LINE__, B_PAGES_GR) + 1;
  pGeoPage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, Page);
  pGeoPage->ReferencePage = (*ReferencePage);
  //
  insere_enr (szVERIFSource, __LINE__, 1, B_PAGES_GR, (*ReferencePage));
  pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, (*ReferencePage));
  pPage->PremierElement = nb_enregistrements (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR) + 1;
  pPage->NbElements = 0;
  //
  pPage->bFenModale = FALSE;
  pPage->bFenDialog = FALSE;
  pPage->bFenBmpFond = FALSE;
  pPage->FenNomBmpFond [0] = '\0';
  pPage->hBmpFond = NULL;
  pPage->bFenTitre = TRUE;
  pPage->FenTextTitre [0] = '\0';
  pPage->FenCouleurFond = G_BLACK;
  pPage->bFenMenu = FALSE;
  pPage->bFenBorder = TRUE;
  pPage->bFenSizeBorder = TRUE;
  pPage->bFenScrollBarH = FALSE;
  pPage->bFenScrollBarV = FALSE;
  pPage->bFenSysMenu = TRUE;
  pPage->bFenMinBouton = TRUE;
  pPage->bFenMaxBouton = TRUE;
  pPage->FenGrilleX = 0;
  pPage->FenGrilleY = 0;
  pPage->FrameFenX =  c_FEN_POS_X_DEFAUT;
  pPage->FrameFenY =  c_FEN_POS_Y_DEFAUT;
  pPage->FrameFenCx = c_FEN_POS_CX_DEFAUT;
  pPage->FrameFenCy = c_FEN_POS_CY_DEFAUT;
  pPage->OrigineX = 0;
  pPage->OrigineY = 0;
  pPage->bFenVisible = FALSE;
  pPage->FenModeAff = MODE_CLIP;
  pPage->FenFacteurZoom = 100;
  pPage->FenModeDeform = MODE_STANDARD;
  pPage->FenPageMere = 0;
  pPage->FenEtat = 0;
  //
  insere_enr (szVERIFSource, __LINE__, 1, B_MARQUES_GR, (*ReferencePage));
  pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, (*ReferencePage));
  pMarque->PremiereMarque = 1;
  pMarque->NbMarques = 0;
  (pMarque->RectMarques).x1 = 0;
  (pMarque->RectMarques).y1 = 0;
  (pMarque->RectMarques).x2 = 0;
  (pMarque->RectMarques).y2 = 0;
  //
  insere_enr (szVERIFSource, __LINE__, 1, B_GEO_POINTS_SAUVEGARDES_GR, (*ReferencePage));
  pGeoSav = (PGEO_POINTS_SAUVEGARDES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR, (*ReferencePage));
  pGeoSav->PremierPointSauvegarde = 1;
  pGeoSav->NbPointsSauvegarde = 0;
  }

// -------------------------------------------------------------------------
static void MajGeoPagesGR (DWORD TypeMaj, DWORD PosEnr, DWORD NbEnr)
  {
  DWORD         wPremierEnr;
  DWORD         wDernierEnr;
  DWORD         wEnr;
  PGEO_PAGES_GR	pGeoPage;

  wPremierEnr = 1;
  wDernierEnr = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_PAGES_GR) + 1;

  switch (TypeMaj)
    {
    case MAJ_INSERTION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pGeoPage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, wEnr);
				if (pGeoPage->ReferencePage >= PosEnr)
					pGeoPage->ReferencePage = pGeoPage->ReferencePage + NbEnr;
				}
			break;
    case MAJ_SUPPRESSION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pGeoPage = (PGEO_PAGES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_PAGES_GR, wEnr);
				if (pGeoPage->ReferencePage >= (PosEnr + NbEnr))
					pGeoPage->ReferencePage = pGeoPage->ReferencePage - NbEnr;
				else
					{
					if ((pGeoPage->ReferencePage >= PosEnr) && (pGeoPage->ReferencePage < (PosEnr + NbEnr)))
						pGeoPage->ReferencePage = 0;
					}
				}
			break;
    }
  }

// -------------------------------------------------------------------------
// Maj
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
void MajSavPointsGR (DWORD TypeMaj, DWORD Ref, DWORD PosEnr, DWORD NbEnr)
  {
  DWORD                         wPremierEnr;
  DWORD                         wDernierEnr;
  DWORD                         wEnr;
  PGEO_POINTS_SAUVEGARDES_GR		pGeoSav;

  wPremierEnr = 1;
  wDernierEnr = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR) + 1;

  switch (TypeMaj)
    {
    case MAJ_INSERTION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pGeoSav = (PGEO_POINTS_SAUVEGARDES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR, wEnr);
				if (pGeoSav->PremierPointSauvegarde > PosEnr)
					pGeoSav->PremierPointSauvegarde = pGeoSav->PremierPointSauvegarde + NbEnr;
				else
					{
					if ((pGeoSav->PremierPointSauvegarde == PosEnr) && (wEnr != Ref))
						pGeoSav->PremierPointSauvegarde = pGeoSav->PremierPointSauvegarde + NbEnr;
					}
				}
			break;
    case MAJ_SUPPRESSION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pGeoSav = (PGEO_POINTS_SAUVEGARDES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR, wEnr);
				if (pGeoSav->PremierPointSauvegarde >= (PosEnr + NbEnr))
					pGeoSav->PremierPointSauvegarde = pGeoSav->PremierPointSauvegarde - NbEnr;
				}
			break;
	  }
  }

// -------------------------------------------------------------------------

// -------------------------------------------------------------------------
// Epure
// -------------------------------------------------------------------------

// -------------------------------------------------------------------------
// dessine l'�l�ment graphique �pure de la page
static void bdgr_dessine_epure (HBDGR hbdgr)
	{
  PBDGR pBdGr = (PBDGR)hbdgr;

	g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
	}

// -------------------------------------------------------------------------
void bdgr_action_epure (HBDGR hbdgr, G_ACTION Action)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  GElementSetAction (pBdGr->helementEpure, Action);
	g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  }

// -------------------------------------------------------------------------
void bdgr_couleur_epure (HBDGR hbdgr, G_COULEUR Couleur)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  GElementSetCouleur (pBdGr->helementEpure, Couleur);
	g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  }

// -------------------------------------------------------------------------
void bdgr_style_ligne_epure (HBDGR hbdgr, G_STYLE_LIGNE Style)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  GElementSetStyleLigne (pBdGr->helementEpure, Style);
	g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  }

// -------------------------------------------------------------------------
void bdgr_style_remplissage_epure (HBDGR hbdgr, G_STYLE_REMPLISSAGE Style)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  GElementSetStyleRemplissage (pBdGr->helementEpure, Style);
	g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  }

// -------------------------------------------------------------------------
void bdgr_style_texte_epure (HBDGR hbdgr, DWORD Police, DWORD StylePolice)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  GElementSetStyleTexte (pBdGr->helementEpure, Police, StylePolice);
	g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  }

// -------------------------------------------------------------------------
void bdgr_texte_epure (HBDGR hbdgr, char *pszTexte)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  GElementSetTexte (pBdGr->helementEpure, pszTexte);
	g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  }

// -------------------------------------------------------------------------
BOOL bdgr_ajouter_point_epure (HBDGR hbdgr, LONG x, LONG y)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  BOOL    bOk;

  //g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
	g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  bOk = GElementAjoutePoint (pBdGr->hGsys, pBdGr->helementEpure, x, y);
	//g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
  return bOk;
  }

// -------------------------------------------------------------------------
void bdgr_modifier_point_epure (HBDGR hbdgr, LONG x, LONG y)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

//  g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
	g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  GElementSetDernierPoint (pBdGr->hGsys, pBdGr->helementEpure, x, y);
	//g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
  g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
	//$$::UpdateWindow()
  }

// ---------------------------------------------------------------------------
// Validation de l'�pure : insertion de ses param�tres dans la base de donn�es
DWORD bdgr_valider_epure (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
	DWORD dwElement = dwValiderElementGr (hbdgr, pBdGr->helementEpure);
	DWORD	wReferencePage;

	// Ajout �l�ment Ok ?
	if ((dwElement != 0) && (ExistePageGR (pBdGr->dwIdPage, &wReferencePage)))
    {
    // oui => insertion de l'�lement en derni�re position dans la page
    PPAGE_GR pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		DWORD		wPositionElement = pPage->PremierElement + dwElement - 1;

    MajPagesGR (MAJ_INSERTION, wReferencePage, wPositionElement, 1);
    MajGeoMarquesGR (MAJ_INSERTION, wPositionElement, 1);

		// Cr�ation �ventuelle de la fen�tre associ�e � l'�l�ment graphique
		VerifWarning (bCreerFenetreElementGr (B_ELEMENTS_PAGES_GR, wPositionElement, hbdgr, PREMIER_ID_PAGE+wPositionElement));
		}
	else
		g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);

  GElementReset (pBdGr->helementEpure);
	return dwElement;
	}

// -------------------------------------------------------------------------
void bdgr_abandonner_epure (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

	if (!bGElementNop (pBdGr->helementEpure))
		{
		//g_element_draw (pBdGr->hGsys, pBdGr->helementEpure);
		g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementEpure);
		GElementReset (pBdGr->helementEpure);
		}
  }


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Verbes d'Initialisations
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
void bdgr_creer (void)
  {
  if (!existe_repere (B_GEO_PAGES_GR))
    {
    cree_bloc (B_GEO_PAGES_GR, sizeof (GEO_PAGES_GR), 0);
    }

  if (!existe_repere (B_PAGES_GR))
    {
    cree_bloc (B_PAGES_GR, sizeof (PAGE_GR), 0);
    }

  if (!existe_repere (B_ELEMENTS_PAGES_GR))
    {
    cree_bloc (B_ELEMENTS_PAGES_GR, sizeof (ELEMENT_PAGE_GR), 0);
    }

  if (!existe_repere (B_GEO_ELEMENTS_LISTES_GR))
    {
    cree_bloc (B_GEO_ELEMENTS_LISTES_GR, sizeof (GEO_ELEMENTS_LISTES_GR), 0);
    }

  if (!existe_repere (B_ELEMENTS_LISTES_GR))
    {
    cree_bloc (B_ELEMENTS_LISTES_GR, sizeof (ELEMENT_PAGE_GR), 0);
    }

  if (!existe_repere (B_GEO_TEXTES_GR))
    {
    cree_bloc (B_GEO_TEXTES_GR, sizeof (GEO_TEXTES_GR), 0);
    }

  if (!existe_repere (B_TEXTES_GR))
    {
    cree_bloc (B_TEXTES_GR, sizeof (TEXTES_GR), 0);
    }

  if (!existe_repere (B_GEO_POINTS_POLY_GR))
    {
    cree_bloc (B_GEO_POINTS_POLY_GR, sizeof (GEO_POINTS_POLY_GR), 0);
    }

  if (!existe_repere (B_POINTS_POLY_GR))
    {
    cree_bloc (B_POINTS_POLY_GR, sizeof (POINTS_POLY_GR), 0);
    }

  if (!existe_repere (B_MARQUES_GR))
    {
 		DWORD	i;

		cree_bloc (B_MARQUES_GR, sizeof (MARQUES_GR), nb_enregistrements (szVERIFSource, __LINE__, B_PAGES_GR));
    for (i = nb_enregistrements (szVERIFSource, __LINE__, B_MARQUES_GR); i > 0; i--)
      {
			PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, i);

      pMarque->PremiereMarque = 1;
      pMarque->NbMarques = 0;
      pMarque->RectMarques.x1 = 0;
      pMarque->RectMarques.y1 = 0;
      pMarque->RectMarques.x2 = 0;
      pMarque->RectMarques.y2 = 0;
      }
    }

  if (!existe_repere (B_GEO_MARQUES_GR))
    {
    cree_bloc (B_GEO_MARQUES_GR, sizeof (GEO_MARQUES_GR), 0);
    }

  if (!existe_repere (B_GEO_POINTS_SAUVEGARDES_GR))
    {
 		DWORD	i;
    cree_bloc (B_GEO_POINTS_SAUVEGARDES_GR, sizeof (GEO_POINTS_SAUVEGARDES_GR), nb_enregistrements (szVERIFSource, __LINE__, B_PAGES_GR));
    for (i = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR); i > 0; i--)
      {
			PGEO_POINTS_SAUVEGARDES_GR	pGeoSav = (PGEO_POINTS_SAUVEGARDES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR, i);

      pGeoSav->PremierPointSauvegarde = 1;
      pGeoSav->NbPointsSauvegarde = 0;
      }
    }

  if (!existe_repere (B_POINTS_SAUVEGARDES_GR))
    {
    cree_bloc (B_POINTS_SAUVEGARDES_GR, sizeof (POINT), 0);
    }

	// cr�e le bloc des "�l�ments ayant des animations" m�moris�s
  if (!existe_repere (B_MEM_ANIMATION_GR))
    {
    cree_bloc (B_MEM_ANIMATION_GR, sizeof (MEM_ANIMATION_GR), 0);
    }

  bdanmgr_init_proc (FALSE); //NULL, NULL, NULL, NULL, NULL, NULL);
  }

// -------------------------------------------------------------------------
void bdgr_init_proc_anm (void)
	//(t_proc_lis_nb_ligne  ptr_proc_lis_nb_ligne,
	//t_proc_valide  ptr_proc_valide,
	//t_proc_suppression_pos ptr_proc_suppression_pos,
	//t_proc_boite_dlg ptr_proc_boite_dlg,
	//t_proc_lis_couleurs2 ptr_proc_lis_couleurs2,
	//t_proc_lis_couleurs4 ptr_proc_lis_couleurs4)

  {
  bdanmgr_init_proc (TRUE); //ptr_proc_lis_nb_ligne, ptr_proc_valide,
                     //ptr_proc_suppression_pos, ptr_proc_boite_dlg,
                     //ptr_proc_lis_couleurs2, ptr_proc_lis_couleurs4);
  }

// -------------------------------------------------------------------------
void bdgr_supprimer_blocs_marques (void)
  {
  if (existe_repere (B_MARQUES_GR))
    {
    enleve_bloc (B_MARQUES_GR);
    }

  if (existe_repere (B_GEO_MARQUES_GR))
    {
    enleve_bloc (B_GEO_MARQUES_GR);
    }

  if (existe_repere (B_GEO_POINTS_SAUVEGARDES_GR))
    {
    enleve_bloc (B_GEO_POINTS_SAUVEGARDES_GR);
    }

  if (existe_repere (B_POINTS_SAUVEGARDES_GR))
    {
    enleve_bloc (B_POINTS_SAUVEGARDES_GR);
    }

	// supprime le bloc des "�l�ments ayant des animations" m�moris�s
  if (existe_repere (B_MEM_ANIMATION_GR))
    {
    enleve_bloc (B_MEM_ANIMATION_GR);
    }
  }

// -------------------------------------------------------------------------
BOOL bdgr_supprimer_tout_dessin (void)
  {
  BOOL bRetour;

  bRetour = TRUE;
  enleve_bloc (B_GEO_PAGES_GR);
  enleve_bloc (B_PAGES_GR);
  enleve_bloc (B_ELEMENTS_PAGES_GR);
  enleve_bloc (B_GEO_ELEMENTS_LISTES_GR);
  enleve_bloc (B_ELEMENTS_LISTES_GR);
  enleve_bloc (B_GEO_TEXTES_GR);
  enleve_bloc (B_TEXTES_GR);
  enleve_bloc (B_GEO_POINTS_POLY_GR);
  enleve_bloc (B_POINTS_POLY_GR);
  enleve_bloc (B_MARQUES_GR);
  enleve_bloc (B_GEO_MARQUES_GR);
  enleve_bloc (B_GEO_POINTS_SAUVEGARDES_GR);
  enleve_bloc (B_POINTS_SAUVEGARDES_GR);
  enleve_bloc (B_MEM_ANIMATION_GR); // "�l�ments ayant des animations" m�moris�s
  return bRetour;
  }


// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Cr�e ou ouvre un objet page de synoptique
HBDGR hbdgr_ouvrir_page (DWORD dwIdPage, BOOL MajVisibilite, BOOL bEnEdition)
  {
  DWORD wReference;
  PBDGR pBdGr = (PBDGR)pMemAlloue (sizeof (BDGR));

  pBdGr->hGsys              = NULL;
  pBdGr->helementTravail    = NULL;
  pBdGr->helementEpure      = NULL;
  pBdGr->helementSelection  = NULL;
  pBdGr->helementRectanglePoignee  = NULL;
  pBdGr->dwIdPage           = dwIdPage;
	pBdGr->bPageEnEdition			= bEnEdition;
  if (!ExistePageGR (dwIdPage, &wReference))
    {
    CreePageGR (dwIdPage, &wReference);
    }
  if (MajVisibilite)
    {
		PPAGE_GR pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReference);

    pPage->bFenVisible = TRUE;
    }
  return (HBDGR) pBdGr;
  }

// -------------------------------------------------------------------------
void bdgr_fermer_page (HBDGR *phbdgr, BOOL MajVisibilite)
  {
  PBDGR pBdGr = (PBDGR)(*phbdgr);

  if (MajVisibilite)
    {
		PPAGE_GR pPage = (PPAGE_GR)pGetPageGR (pBdGr->dwIdPage);
    if (pPage)
      pPage->bFenVisible = FALSE;
    }
  if (pBdGr->hGsys != NULL) // $$ Test inutile ?
    {
    if (pBdGr->helementTravail)
      GElementFerme (&pBdGr->helementTravail);

    if (pBdGr->helementEpure)
      GElementFerme (&pBdGr->helementEpure);

    if (pBdGr->helementSelection)
      GElementFerme (&pBdGr->helementSelection);

    if (pBdGr->helementRectanglePoignee)
      GElementFerme (&pBdGr->helementRectanglePoignee);
    }

  MemLibere ((PVOID *)(&pBdGr));
  *phbdgr = (HBDGR) pBdGr;
  }

// -------------------------------------------------------------------------
void bdgr_associer_espace (HBDGR hbdgr, HGSYS hGsys)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

  if (pBdGr->hGsys != NULL)
    {
    if (pBdGr->helementTravail)
      {
      GElementFerme (&pBdGr->helementTravail);
      }
    if (pBdGr->helementEpure)
      {
      GElementFerme (&pBdGr->helementEpure);
      }
    if (pBdGr->helementSelection)
      {
      GElementFerme (&pBdGr->helementSelection);
      }
    if (pBdGr->helementRectanglePoignee)
      {
      GElementFerme (&pBdGr->helementRectanglePoignee);
      }
    }
  pBdGr->hGsys = hGsys;
  pBdGr->helementTravail = hGElementCree ();
  pBdGr->helementEpure   = hGElementCree ();
  pBdGr->helementSelection = hGElementCree ();
  pBdGr->helementRectanglePoignee = hGElementCree ();
  GElementSetAction (pBdGr->helementSelection, c_ACTION_SELECTION);
  GElementSetCouleur (pBdGr->helementSelection, c_COULEUR_SELECTION);
  GElementSetStyleLigne (pBdGr->helementSelection, c_STYLE_LINE_SELECTION_NORMAL);
  GElementSetStyleRemplissage (pBdGr->helementSelection, c_STYLE_FILL_SELECTION);
  GElementSetAction (pBdGr->helementRectanglePoignee, c_ACTION_DEFORMATION);
  GElementSetCouleur (pBdGr->helementRectanglePoignee, c_COULEUR_DEFORMATION);
  GElementSetStyleLigne (pBdGr->helementRectanglePoignee, c_STYLE_LINE_DEFORMATION);
  GElementSetStyleRemplissage (pBdGr->helementRectanglePoignee, c_STYLE_FILL_DEFORMATION);
  }

// -------------------------------------------------------------------------
BOOL bdgr_supprimer_page  (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  BOOL  bRetour = TRUE;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
    bdgr_marquer_page (hbdgr, MODE_VIRTUEL);
    if (bdgr_supprimer_marques (hbdgr, MODE_VIRTUEL))
      {
      enleve_enr  (szVERIFSource, __LINE__, 1, B_PAGES_GR, wReferencePage);
      enleve_enr  (szVERIFSource, __LINE__, 1, B_MARQUES_GR, wReferencePage);
      enleve_enr  (szVERIFSource, __LINE__, 1, B_GEO_POINTS_SAUVEGARDES_GR, wReferencePage);
      MajGeoPagesGR (MAJ_SUPPRESSION, wReferencePage, 1);
      }
    else
      {
      bdgr_demarquer_page (hbdgr, MODE_VIRTUEL);
      bRetour = FALSE;
      }
    }
  return bRetour;
  }

// -------------------------------------------------------------------------
static void bdgr_dessiner_couleur_fond_page (HBDGR hbdgr)
  {
  PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
		{
		PBDGR			pBdGr = (PBDGR)hbdgr;

    g_fill_background (pBdGr->hGsys, pPage->FenCouleurFond);
		}
  }

// -------------------------------------------------------------------------
static void bdgr_dessiner_grille_page (HBDGR hbdgr)
  {
  PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
		PBDGR			pBdGr = (PBDGR)hbdgr;
		G_COULEUR     Couleur;

		// Fond noir -> grille blanche sinon grille noire
    if (pPage->FenCouleurFond == G_BLACK)
      Couleur = G_WHITE;
    else
      Couleur = G_BLACK;

    g_grille (pBdGr->hGsys, Couleur, G_STYLE_LIGNE_POINTS, pPage->FenGrilleX, pPage->FenGrilleY);
    }
  }

// -------------------------------------------------------------------------
static void bdgr_dessiner_elements_page (HBDGR hbdgr)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
		PBDGR		pBdGr = (PBDGR)hbdgr;
		DWORD  wPremierElement = pPage->PremierElement;
		DWORD  wDernierElement = pPage->NbElements + wPremierElement;
		DWORD  wPositionElement;

    for (wPositionElement = wPremierElement; wPositionElement < wDernierElement; wPositionElement++)
      {
      DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_dessiner_page (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;

  if (ModeTravail & (MODE_GOMME_ELEMENT|MODE_GOMME_GRILLE|MODE_GOMME_COULEUR_FOND))
		VerifWarningExit;

  if (ModeTravail & MODE_GOMME_SELECTION)
    {
    VerifWarningExit;
		bdgr_gommer_selection_page (hbdgr);
    }

  if (ModeTravail & MODE_GOMME_DEFORMATION)
    {
    VerifWarningExit;
		bdgr_gommer_deformation_page (hbdgr);
    }

  if (ModeTravail & MODE_DESSIN_COULEUR_FOND)
    {
		// $$ optimiser
		bdgr_dessiner_couleur_fond_page (hbdgr);
		// dessin fond �cran selon param�tre Stretch de la page
		bdgr_dessiner_bmp_fond_page (hbdgr, bPageStretchBmpFondZoneClient(hbdgr));
    }

  if (ModeTravail & MODE_DESSIN_GRILLE)
    {
		bdgr_dessiner_grille_page (hbdgr);
    }

  if (ModeTravail & MODE_DESSIN_ELEMENT)
    {
		bdgr_dessiner_elements_page (hbdgr);
    }

  if (ModeTravail & MODE_DESSIN_ELTS_MARQUES)
    {
    bdgr_dessiner_elements_marques (hbdgr);
    }

  if (ModeTravail & MODE_DESSIN_SELECTION)
    {
    bdgr_dessiner_selection_page (hbdgr);
    }

  if (ModeTravail & MODE_DESSIN_DEFORMATION)
    {
    bdgr_dessiner_deformation_page (hbdgr);
    }
	// $$ V�rifier
	bdgr_dessine_epure (hbdgr);
  } // bdgr_dessiner_page

// -------------------------------------------------------------------------
// Effectue un ::InvalidateRect sur toute la page
void bdgr_invalide_page (HBDGR hbdgr)
	{
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
		g_InvalidateRect (pBdGr->hGsys, NULL);
	}

// -------------------------------------------------------------------------
// $$ Pas appel� par PcsExe
void bdgr_animer_couleurs2_page (HBDGR hbdgr, DWORD Indice)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
		DWORD             wPremierElement = pPage->PremierElement;
		DWORD             wDernierElement = pPage->NbElements + wPremierElement;
		DWORD             wPositionElement;
		G_COULEUR         nCouleur;
		G_COULEUR         nCouleurFond;

    for (wPositionElement = wPremierElement; wPositionElement < wDernierElement; wPositionElement++)
      {
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
      if (pElement->PosGeoAnimation != 0)
        {
        if (bdanmgr_lis_couleurs2 (pElement->PosGeoAnimation, Indice, &nCouleur, &nCouleurFond))
          bdgr_couleur_element (hbdgr, MODE_MAJ_ECRAN, (wPositionElement-wPremierElement+1), nCouleur);
        }
      }
    }
  }

// -------------------------------------------------------------------------
// $$ Pas appel� par PcsExe
void bdgr_animer_couleurs4_page     (HBDGR hbdgr, DWORD Indice)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
		DWORD	wPremierElement = pPage->PremierElement;
		DWORD	wDernierElement = pPage->NbElements + wPremierElement;
		DWORD	wPositionElement;

//    pPage = pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
    for (wPositionElement = wPremierElement; wPositionElement < wDernierElement; wPositionElement++)
      {
			PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      if (pElement->PosGeoAnimation != 0)
        {
				G_COULEUR	nCouleur;
				G_COULEUR	nCouleurFond;

        if (bdanmgr_lis_couleurs4 (pElement->PosGeoAnimation, Indice, &nCouleur, &nCouleurFond))
          bdgr_couleur_element (hbdgr, MODE_MAJ_ECRAN, (wPositionElement-wPremierElement+1), nCouleur);
        }
      }
    }
  }



// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Travail sur les Animations : Attention tout est bas� sur l'hypoth�se
//                              que les elements anim�s ne sont pas dans une
//                              liste
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
BOOL bdgr_trouver_animation (HBDGR hbdgr, DWORD *Element, DWORD *n_ligne)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  BOOL			bTrouve = FALSE;

  if (pPage)
    {
		DWORD  wPositionElement = pPage->PremierElement + (*Element) - 1;

    while (((*Element) <= pPage->NbElements) && (!bTrouve))
      {
			PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      if (pElement->PosGeoAnimation != 0)
        {
        bTrouve = TRUE;
        (*n_ligne) = pElement->PosGeoAnimation;
        }
      else
        {
        (*Element)++;
        wPositionElement++;
        }
      }
    }
  return bTrouve;
  }

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Travail sur les Elements
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
void bdgr_ligne_anim_element (HBDGR hbdgr, DWORD Element, DWORD n_ligne)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD       wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      pElement->PosGeoAnimation = n_ligne;
      }
    }
  }

// -------------------------------------------------------------------------
BOOL bdgr_lire_texte_element (HBDGR hbdgr, DWORD Element, char *pszTexte)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  BOOL			bOk = FALSE;

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD							wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      if ((pElement->nAction == G_ACTION_H_TEXTE) || (pElement->nAction == G_ACTION_V_TEXTE) ||
          (pElement->nAction == G_ACTION_EDIT_TEXTE) || (pElement->nAction == G_ACTION_STATIC_TEXTE) ||
          (pElement->nAction == G_ACTION_EDIT_NUM) || (pElement->nAction == G_ACTION_STATIC_NUM))
        {
        bOk = TRUE;
        pszTexte [0] = '\0';
        if (pElement->nStyleLigne != 0)
          {
					PGEO_TEXTES_GR	pGeoTexte = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);

          if (pGeoTexte->ReferenceTexte != 0)
            {
						PTEXTES_GR	pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, pGeoTexte->ReferenceTexte);

            StrCopy (pszTexte, pTexte->Texte);
            }
          }
        }
      }
    }

  return bOk;
  }

// -------------------------------------------------------------------------
BOOL bdgr_supprimer_element (HBDGR hbdgr, DWORD ModeTravail, DWORD Element)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  BOOL  bRetour = TRUE;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR	pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);

    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD             wPositionElement = pPage->PremierElement + Element - 1;
			PGEO_MARQUES_GR		pGeoMarque;
			PMARQUES_GR				pMarque;
			DWORD             wPremiereGeoMarque;
			DWORD             wNbGeoMarques;
			DWORD             wPositionGeoMarque;
			DWORD             i;

      if (SuppressionPossibleElementGR (B_ELEMENTS_PAGES_GR, wPositionElement))
        {
        pPage->NbElements--;

        if (ModeTravail & MODE_GOMME_ELEMENT)
          {
          GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
          }
        //
        SupprimerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement);
        //
        pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
        wPremiereGeoMarque = pMarque->PremiereMarque;
        wNbGeoMarques = pMarque->NbMarques;
        wPositionGeoMarque = 0;
        i = 0;
        while ((i < wNbGeoMarques) && (wPositionGeoMarque == 0))
          {
          pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPremiereGeoMarque + i);
          if (pGeoMarque->Element == wPositionElement)
            {
            wPositionGeoMarque = wPremiereGeoMarque + i;
            }
          i++;
          }
        if (wPositionGeoMarque != 0)
          {
          pMarque->NbMarques--;
          enleve_enr (szVERIFSource, __LINE__, 1, B_GEO_MARQUES_GR, wPositionGeoMarque);

          MajMarquesGR (MAJ_SUPPRESSION, wReferencePage, wPremiereGeoMarque, 1);
          }
        //
        MajPagesGR (MAJ_SUPPRESSION, wReferencePage, wPositionElement, 1);
        MajGeoMarquesGR (MAJ_SUPPRESSION, wPositionElement, 1);
        }
      else
        {
        bRetour = FALSE;
        }
      }
    }
  return bRetour;
  }

// -----------------------------------------------------------------------------------
// R�cup�rer la position de l'�l�ment graphique le plus proche du point sp�cifi� ou 0
DWORD bdgr_pointer_element (HBDGR hbdgr, LONG x, LONG y)
  {
  PPAGE_GR	pPage = ppageHBDGR (hbdgr);
	DWORD     wElement = 0;

  if (pPage)
    {
    if (pPage->NbElements > 0)
      {
			PBDGR	pBdGr = (PBDGR)hbdgr;
			DWORD wPremierElement = pPage->PremierElement;
			DWORD wDernierElement = wPremierElement + pPage->NbElements;
			DWORD wPositionElement;
			DWORD lOldDist;
			DWORD lNewDist;

      wElement = wPremierElement;
      lNewDist = DistanceElementGR (B_ELEMENTS_PAGES_GR, wPremierElement, x, y, pBdGr->hGsys, pBdGr->helementTravail);
      lOldDist = lNewDist;
      for (wPositionElement = wPremierElement + 1; wPositionElement < wDernierElement; wPositionElement++)
        {
        lNewDist = DistanceElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, x, y, pBdGr->hGsys, pBdGr->helementTravail);
        if (lNewDist < lOldDist)
          {
          lOldDist = lNewDist;
          wElement = wPositionElement;
          }
        }
      wElement = wElement - wPremierElement + 1;
      }
    }
  return wElement;
  }

// -------------------------------------------------------------------------
// Renvoie la taille virtuelle de la police courante pour la page
DWORD bdgr_taille_police_courante (HBDGR hbdgr, DWORD dwTaillePolicePixel)
	{
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
	DWORD			dwTaillePolice = 48; //$$ ?

  if (pPage)
		{
		PBDGR	pBdGr = (PBDGR)hbdgr;

		dwTaillePolice = g_convert_dy_dev_to_page (pBdGr->hGsys, (LONG)dwTaillePolicePixel);
		}

	return dwTaillePolice;
	}

// -------------------------------------------------------------------------
// regarde si l'utilisateur a cliqu� sur un �l�ment graphique texte existant
DWORD bdgr_pointer_texte_element (HBDGR hbdgr, LONG x, LONG y, DWORD *NumeroTexte,
	DWORD *Police, DWORD *StylePolice, char *pszTexte, LONG *x1, LONG *y1, LONG *x2, LONG *y2)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  DWORD			wElement = 0;

  (*NumeroTexte) = 0;
  if (pPage)
    {
    if (pPage->NbElements > 0)
      {
			PBDGR							pBdGr = (PBDGR)hbdgr;
			DWORD							wPremierElement  = pPage->PremierElement;
			DWORD							wDernierElement = wPremierElement + pPage->NbElements;
			DWORD							wPositionElement = wPremierElement;
			PELEMENT_PAGE_GR	pElement;
			PTEXTES_GR				pTexte;
			LONG							X1Car;
			LONG							Y1Car;
			LONG							X2Car;
			LONG							Y2Car;

      while ((wPositionElement < wDernierElement) && ((*NumeroTexte) == 0))
        {
        X1Car = 0;
        Y1Car = 0;
        X2Car = 0;
        Y2Car = 0;
        (*NumeroTexte) = PointerTexteElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, x, y, pBdGr->hGsys, pBdGr->helementTravail, &X1Car, &Y1Car, &X2Car, &Y2Car);
        wPositionElement++;
        }
      if ((*NumeroTexte) != 0)
        {
        wElement = wPositionElement - 1;
        pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wElement);
        if ((pElement->nAction == G_ACTION_H_TEXTE) || (pElement->nAction == G_ACTION_V_TEXTE) ||
            (pElement->nAction == G_ACTION_EDIT_TEXTE) || (pElement->nAction == G_ACTION_STATIC_TEXTE) ||
            (pElement->nAction == G_ACTION_EDIT_NUM) || (pElement->nAction == G_ACTION_STATIC_NUM))
          {
          wElement = wPositionElement - wPremierElement;
          }
        else
          {
          wElement = (DWORD) -1;
          }
        (*x1) = X1Car;
        (*y1) = Y1Car;
        (*x2) = X2Car;
        (*y2) = Y2Car;
        pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, *NumeroTexte);
        StrCopy (pszTexte, pTexte->Texte);
        (*Police) = pTexte->Police;
        (*StylePolice) = pTexte->StylePolice;
        }
      else
        {
        wElement = 0;
        }
      }
    }
  return wElement;
  }

// -------------------------------------------------------------------------
void bdgr_style_texte (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, DWORD NumeroTexte, DWORD Police, DWORD StylePolice)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR				pBdGr = (PBDGR)hbdgr;
			DWORD				wPositionElement = pPage->PremierElement + Element - 1;
			PTEXTES_GR	pTexte;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, NumeroTexte);
      pTexte->Police      = Police;
      pTexte->StylePolice = StylePolice;

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_texte (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, DWORD NumeroTexte, char *pszTexte)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			PBDGR				pBdGr = (PBDGR)hbdgr;
			DWORD				wPositionElement = pPage->PremierElement + Element - 1;
			PTEXTES_GR	pTexte;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);

      pTexte = (PTEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_TEXTES_GR, NumeroTexte);
      StrExtract (pTexte->Texte, pszTexte, 0, c_NB_CAR_TEXTE_GR);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  }

//-------------------------------------------------------------------------
BOOL bIdentifiantDejaPresent (DWORD repere_element, DWORD index_repere_nombre, PSTR strIdentifiant)
	{
	BOOL bDejaPresent = FALSE;
	if (repere_element == B_ELEMENTS_PAGES_GR)
		{
		PPAGE_GR ppage_gr = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, index_repere_nombre);
		DWORD dwNbElements = ppage_gr->NbElements;
		DWORD dwIndiceElement = 0;
		DWORD index_repere_element;
	  PELEMENT_PAGE_GR pelt_gr;
    for (dwIndiceElement = 0; ((dwIndiceElement < ppage_gr->NbElements) && (!bDejaPresent)); dwIndiceElement++)
      {
      index_repere_element = ppage_gr->PremierElement + dwIndiceElement;
	    pelt_gr = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, repere_element, index_repere_element);
			bDejaPresent = bStrEgales (pelt_gr->strIdentifiant, strIdentifiant); 
      }
		}
	else
		{
		// on s'en fout, insertion dans bloc des elts de listes. doublon autoris�
		}

	return (bDejaPresent);
	}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Fonctions utilitaires pour la g�n�ration et l'exploitation.
// ATTENTION: Fonctions � utiliser avec pr�cautions.
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


// -------------------------------------------------------------------------
// appel� par gener_gr uniquement
void bdgr_substituer_action_element   (HBDGR hbdgr, DWORD Element, G_ACTION NewAction)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD         wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      pElement->nAction = NewAction;
      }
    }
  }

// -------------------------------------------------------------------------
DWORD bdgr_lire_numero_texte_element (HBDGR hbdgr, DWORD Element)
  {
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);
  DWORD			wNumeroTexte = 0;

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
	 		DWORD					wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      if ((pElement->nAction == G_ACTION_H_TEXTE) || (pElement->nAction == G_ACTION_V_TEXTE) ||
          (pElement->nAction == G_ACTION_EDIT_TEXTE) || (pElement->nAction == G_ACTION_STATIC_TEXTE) ||
          (pElement->nAction == G_ACTION_EDIT_NUM) || (pElement->nAction == G_ACTION_STATIC_NUM))
        {
				PGEO_TEXTES_GR pGeoTxt = (PGEO_TEXTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_TEXTES_GR, pElement->nStyleLigne);

        wNumeroTexte = pGeoTxt->ReferenceTexte;
        }
      }
    }
  return wNumeroTexte;
  }

// -------------------------------------------------------------------------
// Animation : ajustement du dessin d'un bargraphe (appel� exclusivement par pcsexe)
void bdgr_maj_bargraphe (HBDGR hbdgr, DWORD Element, DWORD wTypeBar, int iValeur)
  {
	PBDGR pBdGr = (PBDGR)hbdgr;
	PPAGE_GR	pPage = ppageHBDGR (hbdgr);

  if (pPage)
    {
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD					wPositionElement = pPage->PremierElement + Element - 1;
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
			RECT					rect;
			BOOL					bModif = FALSE;

      switch (wTypeBar)
				{
				case c_BAR_HAUT:
					rect.left = pElement->pt0.x;
					rect.right = pElement->pt1.x;
					if (iValeur > pElement->pt0.y)
						{
						rect.top = pElement->pt0.y;
						rect.bottom = iValeur;
						bModif = (pElement->pt0.y != iValeur);
						pElement->pt0.y = iValeur;
						}
					else
						{
						rect.top = iValeur;
						rect.bottom = pElement->pt0.y;
						bModif = (pElement->pt0.y != iValeur);
						pElement->pt0.y = iValeur;
						}
					break;
				case c_BAR_BAS:
					rect.left = pElement->pt0.x;
					rect.right = pElement->pt1.x;
					if (iValeur > pElement->pt1.y)
						{
						rect.top = pElement->pt1.y;
						rect.bottom = iValeur;
						bModif = (pElement->pt1.y != iValeur);
						pElement->pt1.y = iValeur;
						}
					else
						{
						rect.top = iValeur;
						rect.bottom = pElement->pt1.y;
						bModif = (pElement->pt1.y != iValeur);
						pElement->pt1.y = iValeur;
						}
					break;
				case c_BAR_DROITE:
					rect.top = pElement->pt0.y;
					rect.bottom = pElement->pt1.y;
					if (iValeur > pElement->pt1.x)
						{
						rect.left = pElement->pt1.x;
						rect.right = iValeur;
						bModif = (pElement->pt1.x != iValeur);
						pElement->pt1.x = iValeur;
						}
					else
						{
						rect.left = iValeur;
						rect.right = pElement->pt1.x;
						bModif = (pElement->pt1.x != iValeur);
						pElement->pt1.x = iValeur;
						}
					break;
				case c_BAR_GAUCHE:
					rect.top = pElement->pt0.y;
					rect.bottom = pElement->pt1.y;
					if (iValeur > pElement->pt0.x)
						{
						rect.left = pElement->pt0.x;
						rect.right = iValeur;
						bModif = (pElement->pt0.x != iValeur);
						pElement->pt0.x = iValeur;
						}
					else
						{
						rect.left = iValeur;
						rect.right = pElement->pt0.x;
						bModif = (pElement->pt0.x != iValeur);
						pElement->pt0.x = iValeur;
						}
					break;
				}
      //g_fill_rectangle (pBdGr->hGsys, pElement->Couleur, pElement->Style, rect.left, rect.top, rect.right, rect.bottom);
			if (bModif)
				{
				//rect.right++;
				g_InvalidateRect (pBdGr->hGsys, &rect);
				}
      }
    }
  } // bdgr_maj_bargraphe

//-------------------------- fin BDGR.C ----------------------------------

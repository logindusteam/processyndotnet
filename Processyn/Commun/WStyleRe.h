//-----------------------------------------------------------
// WStyleRe.h
// Gestion des fen�tres de choix des styles de remplissages
//-----------------------------------------------------------
#ifndef WSTYLERE_H
#define WSTYLERE_H

// Cr�ation des classes des fen�tres de gestion de remplissage de PcsGr
BOOL bCreeClasseStyleRemplissages (void);

// Cr�ation d'une fen�tre de choix de remplissage (contenant sa barre d'outil)
HWND hwndCreeChoixStyleRemplissages (HWND hwndParent, BOOL bVisible, BOOL bChild);

#endif







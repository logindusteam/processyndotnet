// -------------------------------------------------------------------------
// Bdgr.h Gestion de synoptique
// Version WIN32 30/4/97
// -------------------------------------------------------------------------

DECLARE_HANDLE (HBDGR);

typedef struct
  {
  HGSYS	hGsys;									// handles li�s � la page
  HELEMENT	helementTravail;
  HELEMENT	helementEpure;			// Element utilis� pour le dessin d'un nouvel �l�ment graphique
  HELEMENT	helementSelection;
  HELEMENT	helementRectanglePoignee;	// Element utilis� pour le rectangle de d�formation
  DWORD			dwIdPage;						// Num�ro de page associ�e � ce BDGR
	BOOL			bPageEnEdition;			// TRUE si page en �dition
  } BDGR, * PBDGR;

// Type de mise � jour (pour les verbes Maj)
typedef enum
	{
	MAJ_INSERTION = 1,
	MAJ_SUPPRESSION = 2
	} MAJ_BDGR;

// Valeurs de ModeTravail
#define MODE_VIRTUEL             0x0000
#define MODE_GOMME_ELEMENT       0x0001
#define MODE_DESSIN_ELEMENT      0x0002
#define MODE_MAJ_ELEMENT         (MODE_GOMME_ELEMENT | MODE_DESSIN_ELEMENT)   // 0x0003
#define MODE_GOMME_SELECTION     0x0004
#define MODE_DESSIN_SELECTION    0x0008
#define MODE_MAJ_SELECTION       (MODE_GOMME_SELECTION | MODE_DESSIN_SELECTION)   // 0x000C
#define MODE_GOMME_DEFORMATION   0x0010
#define MODE_DESSIN_DEFORMATION  0x0020
#define MODE_MAJ_DEFORMATION     (MODE_GOMME_DEFORMATION | MODE_DESSIN_DEFORMATION)   // 0x0030
#define MODE_GOMME_COULEUR_FOND  0x0040
#define MODE_DESSIN_COULEUR_FOND 0x0080
#define MODE_MAJ_COULEUR_FOND    (MODE_GOMME_COULEUR_FOND | MODE_DESSIN_COULEUR_FOND)  // 0x00C0
#define MODE_GOMME_GRILLE        0x0100
#define MODE_DESSIN_GRILLE       0x0200
#define MODE_DESSIN_ELTS_MARQUES 0x0800
#define MODE_MAJ_ECRAN           (MODE_MAJ_ELEMENT | MODE_MAJ_SELECTION | MODE_MAJ_DEFORMATION | MODE_MAJ_COULEUR_FOND)   // 0x00FF surtout pas marque

// Sym�tries
#define c_SYMETRIE_NULLE        c_G_NULL_MIRROR
#define c_SYMETRIE_VERTICALE    c_G_X_MIRROR
#define c_SYMETRIE_HORIZONTALE  c_G_Y_MIRROR
#define c_SYMETRIE_CENTRALE     c_G_XY_MIRROR

// Alignements
#define c_ALIGN_VC 1
#define c_ALIGN_HC 2
#define c_ALIGN_D  3
#define c_ALIGN_B  4
#define c_ALIGN_G  5
#define c_ALIGN_H  6

// Espacements
#define c_ESPACE_H 1
#define c_ESPACE_V 2

#define ERR_RIEN_GR       0
#define ERR_ESPACEMENT_GR 1

// BarGraphes
#define c_BAR_HAUT    1
#define c_BAR_BAS     2
#define c_BAR_DROITE  3
#define c_BAR_GAUCHE  4


// -------------------------------------------------------------------------
#define c_NB_CAR_TEXTE_GR       80  // Nb de caracteres d'un texte graphique
                                    // Sans compter '\0'

// ATTENTION :  SI MODIF VOIR EGALEMENT TIPE_GR.H
#define c_NB_CAR_ID_ELT_GR      20  // Nb de caracteres de l'identifiant d'un element graphique
                                    // Sans compter '\0'

#define c_NB_CAR_TITRE_PAGE_GR  40  // Nb de caracteres d'un titre de page
                                    // Sans compter '\0'

#define c_NB_CAR_NOM_BMP_FOND   80  // Nb de caracteres pour fichier bmp de fond page
                                    // Sans compter '\0'

// Valeurs coins de deformation
#define c_COIN_BG  1
#define c_COIN_HG  2
#define c_COIN_BD  3
#define c_COIN_HD  4
#define c_PAS_COIN 0

// Mode dessin en exploitaton
#define MODE_CLIP  1
#define MODE_ZOOM  2

#define MODE_STANDARD  1
#define MODE_PROPORTIONNEL  2

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Listes des REPERES Bloc utilis�s
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
#define B_GEO_PAGES_GR               817   // GEO_PAGES_GR
#define B_PAGES_GR                   818   // PAGE_GR

#define B_ELEMENTS_PAGES_GR          819   // ELEMENT_PAGE_GR

#define B_GEO_ELEMENTS_LISTES_GR     820   // GEO_ELEMENTS_LISTES_GR
#define B_ELEMENTS_LISTES_GR         821   // ELEMENT_PAGE_GR

#define B_GEO_TEXTES_GR              824   // GEO_TEXTES_GR
#define B_TEXTES_GR                  825   // TEXTES_GR

#define B_GEO_POINTS_POLY_GR         826   // GEO_POINTS_POLY_GR
#define B_POINTS_POLY_GR             827   // POINTS_POLY_GR

#define B_MARQUES_GR                 828   // MARQUES_GR
#define B_GEO_MARQUES_GR             829   // GEO_MARQUES_GR

#define B_GEO_POINTS_SAUVEGARDES_GR  833   // GEO_POINTS_SAUVEGARDES_GR
#define B_POINTS_SAUVEGARDES_GR      834   // t_points_sauvegardes_gr

#define B_ESPACEMENT_GR              822   // ESPACEMENT_GR

#define B_MEM_ANIMATION_GR           823   // Memorisation animation pour duplication

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Types des REPERES Blocs utilis�s
// -------------------------------------------------------------------------
// B_GEO_PAGES_GR
typedef struct
  {
  DWORD ReferencePage;
  } GEO_PAGES_GR, *PGEO_PAGES_GR;

// B_PAGES_GR
typedef struct
  {
	BOOL		bFenModale;	
  BOOL		bFenDialog;	
  BOOL		bFenBmpFond;		// Existence d'un bitmap en fond de page
  char		FenNomBmpFond [c_NB_CAR_NOM_BMP_FOND + 1];
  HANDLE	hBmpFond;	// en fait handle de DIB interne � DevMan
  //
  G_COULEUR   FenCouleurFond;
  BOOL		bFenMenu;
  char    FenTextTitre [c_NB_CAR_TITRE_PAGE_GR + 1];
  BOOL		bFenBorder;
  BOOL		bFenSizeBorder;
  BOOL		bFenTitre;
  BOOL		bFenScrollBarH;
  BOOL		bFenScrollBarV;
  BOOL		bFenSysMenu;
  BOOL		bFenMinBouton;
  BOOL		bFenMaxBouton;
  LONG     FenGrilleX;
  LONG     FenGrilleY;
  int     FrameFenX;
  int     FrameFenY;
  int     FrameFenCx;
  int     FrameFenCy;
  int     OrigineX;
  int     OrigineY;
  BOOL		bFenVisible;
  DWORD		FenModeAff;
  DWORD		FenFacteurZoom;
  DWORD		FenModeDeform;
  DWORD		FenPageMere;
  DWORD		FenEtat;
  //
  DWORD		PremierElement;
  DWORD		NbElements;
  } PAGE_GR, *PPAGE_GR;

// B_ELEMENTS_PAGES_GR
// B_ELEMENTS_LISTES_GR
typedef struct
  {
  G_ACTION						nAction;
  G_COULEUR						nCouleurLigne; // Rajouter un style et un HANDLE ()
	G_COULEUR						nCouleurRemplissage;
  G_STYLE_LIGNE				nStyleLigne;
  G_STYLE_REMPLISSAGE nStyleRemplissage;
	POINT								pt0;		// ancien x1, y1
	POINT								pt1;		// ancien x2, y2
  DWORD								PosGeoAnimation;
	HWND								hwndControle;		// utilis� pour stocker les hwnd des controles
	char								strIdentifiant [c_NB_CAR_ID_ELT_GR + 1];
  } ELEMENT_PAGE_GR, *PELEMENT_PAGE_GR;

// constante Nop
extern	const ELEMENT_PAGE_GR ELEMENT_PAGE_GR_NULL;

// B_GEO_ELEMENTS_LISTES_GR
typedef struct
  {
  DWORD PremierElement;
  DWORD NbElements;
  DWORD NbReferencesListe;
  } GEO_ELEMENTS_LISTES_GR, *PGEO_ELEMENTS_LISTES_GR;

// B_GEO_TEXTES_GR
typedef struct
  {
  DWORD ReferenceTexte;
  } GEO_TEXTES_GR, *PGEO_TEXTES_GR;

// B_TEXTES_GR
typedef struct
  {
  DWORD Police;
  DWORD StylePolice;
  char Texte [c_NB_CAR_TEXTE_GR + 1];
  } TEXTES_GR, *PTEXTES_GR;

// B_GEO_POINTS_POLY_GR
typedef struct
  {
  DWORD PremierPoint;
  DWORD NbPoints;
  DWORD NbReferencesPoly;
  } GEO_POINTS_POLY_GR, *PGEO_POINTS_POLY_GR;

// B_POINTS_POLY_GR
typedef struct
  {
  LONG x;
  LONG y;
  } POINTS_POLY_GR, *PPOINTS_POLY_GR;

// 
typedef struct
  {
  LONG x1;
  LONG y1;
  LONG x2;
  LONG y2;
  } RECT_BDGR, *PRECT_BDGR;

// B_MARQUES_GR
typedef struct
  {
  DWORD       PremiereMarque;
  DWORD       NbMarques;
  RECT_BDGR RectMarques;
  } MARQUES_GR, *PMARQUES_GR;

// B_GEO_MARQUES_GR
typedef struct
  {
  DWORD Element;
  } GEO_MARQUES_GR, *PGEO_MARQUES_GR;

// B_GEO_POINTS_SAUVEGARDES_GR
typedef struct
  {
  DWORD PremierPointSauvegarde;
  DWORD NbPointsSauvegarde;
  } GEO_POINTS_SAUVEGARDES_GR, *PGEO_POINTS_SAUVEGARDES_GR;

// B_POINTS_SAUVEGARDES_GR : POINT

// B_ESPACEMENT_GR
typedef struct
  {
  DWORD wPosGeoMarques;
  LONG  iVal;
  LONG  Dx;
  LONG  Dy;
  } ESPACEMENT_GR, *PESPACEMENT_GR;

// B_MEM_ANIMATION_GR Memorisation animation pour duplication
typedef struct
  {
  DWORD wPositionSource;
  DWORD wPositionDestination;
  } MEM_ANIMATION_GR, *PMEM_ANIMATION_GR;

// Identifiants de contr�les child d'une page
#define PREMIER_ID_PAGE 2000	// et suivants

// -------------------------------------------------------------------------
// Initialisations de la BDD GR
void bdgr_creer (void);
void bdgr_init_proc_anm (void);

void bdgr_supprimer_blocs_marques (void);
BOOL bdgr_supprimer_tout_dessin (void);

// Getsion Page
HBDGR hbdgr_ouvrir_page       (DWORD dwIdPage, BOOL MajVisibilite, BOOL bEnEdition);
void  bdgr_fermer_page       (HBDGR * phbdgr, BOOL MajVisibilite);

BOOL  bdgr_supprimer_page    (HBDGR hbdgr);
void  bdgr_associer_espace   (HBDGR hbdgr, HGSYS hGsys);

//-----------------------------------------
// Page
void	bdgr_dessiner_page     (HBDGR hbdgr, DWORD ModeTravail);
// Effectue un ::InvalidateRect sur toute la page
void	bdgr_invalide_page     (HBDGR hbdgr);

//
// Nombres
DWORD bdgr_nombre_marques_page  (HBDGR hbdgr);

// Epure
void	bdgr_action_epure            (HBDGR hbdgr, G_ACTION Action)   ;
void	bdgr_couleur_epure           (HBDGR hbdgr, G_COULEUR Couleur)  ;
void	bdgr_style_ligne_epure       (HBDGR hbdgr, G_STYLE_LIGNE Style)    ;
void	bdgr_style_remplissage_epure (HBDGR hbdgr, G_STYLE_REMPLISSAGE Style)    ;
void	bdgr_style_texte_epure       (HBDGR hbdgr, DWORD Police, DWORD StylePolice);
void	bdgr_texte_epure             (HBDGR hbdgr, char *pszTexte);
BOOL	bdgr_ajouter_point_epure     (HBDGR hbdgr, LONG x, LONG y)  ;
void	bdgr_modifier_point_epure    (HBDGR hbdgr, LONG x, LONG y)  ;
DWORD	bdgr_valider_epure           (HBDGR hbdgr)                ;
void	bdgr_abandonner_epure        (HBDGR hbdgr)                ;

// Animation
BOOL	bdgr_trouver_animation				 (HBDGR hbdgr, DWORD *n_elt, DWORD *n_ligne);
DWORD	bdgr_param_animation_marques	 (HBDGR hbdgr, ANIMATION CtxtAnimation); // Renvoie erreur si impossible
BOOL	bdgr_ligne_anim_marques				 (HBDGR hbdgr, DWORD n_ligne);
void	bdgr_ligne_anim_element        (HBDGR hbdgr, DWORD Element, DWORD n_ligne);
void	bdgr_animer_couleurs2_page     (HBDGR hbdgr, DWORD Indice);
void	bdgr_animer_couleurs4_page     (HBDGR hbdgr, DWORD Indice);

// Travail sur les Elements
BOOL	bdgr_supprimer_element				 (HBDGR hbdgr, DWORD ModeTravail, DWORD Element);

DWORD	bdgr_pointer_element           (HBDGR hbdgr, LONG x, LONG y);

// Travail sur les Textes
// Renvoie la taille virtuelle de la police courante pour la page
DWORD bdgr_taille_police_courante (HBDGR hbdgr, DWORD dwTaillePolicePixel);
DWORD	bdgr_pointer_texte_element (HBDGR hbdgr, LONG x, LONG y, DWORD *NumeroTexte,
                                 DWORD *Police, DWORD *StylePolice, char *pszTexte,
                                 LONG *x1, LONG *y1, LONG *x2, LONG *y2);
void	bdgr_style_texte (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, DWORD NumeroTexte, DWORD Police, DWORD StylePolice);
void	bdgr_texte       (HBDGR hbdgr, DWORD ModeTravail, DWORD Element, DWORD NumeroTexte, char *pszTexte);

// Travail sur Rectangle
//void bdgr_dessiner_contenu_rectangle (HBDGR hbdgr, LONG x1, LONG y1, LONG x2, LONG y2);
//void bdgr_gommer_contenu_rectangle   (HBDGR hbdgr, LONG x1, LONG y1, LONG x2, LONG y2);
BOOL	bdgr_point_dans_rectangle (LONG x, LONG y, LONG Rectx1, LONG Recty1, LONG Rectx2, LONG Recty2);

// ------------------------------------------------------------------------
void MajSavPointsGR (DWORD TypeMaj, DWORD Ref, DWORD PosEnr, DWORD NbEnr);



// Travail sur les Identificateurs
//--------------------------------------------------------------------------
BOOL bIdentifiantDejaPresent (DWORD repere_element, DWORD index_repere_nombre, PSTR strIdentifiant);

//--------------------------------------------------------------------------
// Insertion d'Elements Dans une Page
// (Fonctions utilitaires pour la g�n�ration et l'exploitation).
// Attention: Fonctions � utiliser avec pr�cautions !
// -------------------------------------------------------------------------
void	bdgr_substituer_action_element   (HBDGR hbdgr, DWORD Element, G_ACTION NewAction);
DWORD	bdgr_lire_numero_texte_element   (HBDGR hbdgr, DWORD Element);
void	bdgr_maj_bargraphe               (HBDGR hbdgr, DWORD Element, DWORD wTypeBar, int iValeur);


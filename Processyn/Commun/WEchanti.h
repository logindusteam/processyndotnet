//-----------------------------------------------------------
// WEchanti.h
// Gestion des fen�tres de choix ou d'affichage des couleurs
//-----------------------------------------------------------
#ifndef WECHANTI_H
#define WECHANTI_H

// Cr�ation des classes des fen�tres de gestion d'�chantillon de PcsGr
BOOL bCreeClasseEchantillon (void);

// Couleur d'un �chantillon 
BOOL SetWindowCouleurEchantillon (HWND hwnd, G_COULEUR dwNCouleur);
BOOL SetDlgItemCouleurEchantillon (HWND hwnd, UINT uIdItem, G_COULEUR dwNCouleur);
BOOL GetWindowCouleurEchantillon (HWND hwnd, G_COULEUR * pdwNCouleur);
BOOL GetDlgItemCouleurEchantillon (HWND hwnd, UINT uIdItem, G_COULEUR * pdwNCouleur);


// Style de ligne d'un �chantillon 
BOOL SetDlgItemStyleLigneEchantillon (HWND hwnd, UINT uIdItem, G_STYLE_LIGNE dwNStyleLigne);
BOOL GetDlgItemStyleLigneEchantillon (HWND hwnd, UINT uIdItem, G_STYLE_LIGNE * pdwNStyleLigne);

// Style de Remplissage d'un �chantillon 
BOOL SetDlgItemStyleRemplissageEchantillon (HWND hwnd, UINT uIdItem, G_STYLE_REMPLISSAGE dwNStyleRemplissage);
BOOL GetDlgItemStyleRemplissageEchantillon (HWND hwnd, UINT uIdItem, G_STYLE_REMPLISSAGE * pdwNStyleRemplissage);


// Cr�ation d'une fen�tre de choix de couleur (contenant sa barre d'outil)
HWND hwndCreeChoixEchantillon (HWND hwndParent, HMENU id, int x, int y, int nWidth, int nHeight);

#endif







// 20/9/96
// WBarOuti.h
// Gestion de barre d'outils WIN32

DECLARE_HANDLE (HBO);

// Position d'une case menu dans le menu
#define BO_COL_SUIVANTE          0          // position menu colonne suivante
#define BO_NOUVELLE_LN           1          // position menu ligne suivante

// Positions du menu par rapport a la fenetre cliente
#define BO_POS_GAUCHE      0x0001
#define BO_POS_DROITE      0x0002
#define BO_POS_HAUTE       0x0004
#define BO_POS_BASSE       0x0008
#define BO_POS_CENTRE      0x0020
#define BO_POS_REMPLACE    0x00FF

// types de bouton
#define BO_BOUTON          0          // case menu bouton a enfoncer
#define BO_COCHE					 1          // case menu bouton qui reste enfonce

#define BO_MAX_TITRE      32         // taille max d'un titre de barre d'outil

// cr�e mais n'affiche pas un objet Barre d'outil, sans boutons.
HBO hBOCree
	(
	const char *pszTitre, // NULL ou adresse du titre de la barre d'outil (permet un d�placement)
	BOOL cadre,						// dessin d'un cadre autour de la BO
	BOOL deplacement,			// permet le d�placement de la BO avec le bouton droit
	BOOL fermeture,				// autorise le dessin d'un bouton de fermeture de la BO
	int cx_bord, int cy_bord	// distance entre le cadre et les boutons
	);

// Ajoute un bouton � un objet barre d'outil
BOOL bBOAjouteBouton
	(
	HBO hBarreOutil,			// handle sur barre d'outil cr��e par hBOCree
	const char *bmp_name,	// nom ressource bitmap
	int pos,							// bouton sur la m�me ligne (BO_COL_SUIVANTE) ou sur la ligne suivante (BO_NOUVELLE_LN)
	int mnu_mode,					// BO_BOUTON ou BO_COCHE (reste enfonc� une fois sur 2)
	int nIdBouton					// Identifiant (envoy� dans WM_COMMAND avec LOWORD (mp1) = nIdBouton et	mp2 = 0
	);	// TRUE si pas d'erreur

// Ajoute un bouton couleur (palette) � un objet barre d'outil
BOOL bBOAjouteBoutonCouleur
	(
	HBO hBarreOutil,			// handle sur barre d'outil cr��e par hBOCree
	COLORREF ColorRef,		// Couleur � afficher
	int cx,								// largeur de l'�chantillon de couleur
	int cy,								// hauteur de l'�chantillon de couleur
	int pos,							// bouton sur la m�me ligne (BO_COL_SUIVANTE) ou sur la ligne suivante (BO_NOUVELLE_LN)
	int mnu_mode,					// BO_BOUTON ou BO_COCHE (reste enfonc� une fois sur 2)
	int nIdBouton					// Identifiant (envoy� dans WM_COMMAND avec LOWORD (mp1) = nIdBouton et	mp2 = 0
	);	// TRUE si pas d'erreur


// Ouvre une fen�tre BO.
BOOL bBOOuvre 
	(
	HBO hBarreOutil,	// handle de l'objet barre d'outil
	BOOL bPopUp,			// Barre d'outil pop up ou child
	HWND hwndParent,	// Fenetre Parente de la barre d'outil (doit �tre visible) recevant les notifications
	int o_x, int o_y, // origine en x de la barre d'outil par rapport a l'origine de la fenetre parente (si pos = 0).
	int pos,					// (0, BO_POS_GAUCHE, BO_POS_CENTRE, BO_POS_DROITE) |(0, BO_POS_HAUTE, BO_POS_CENTRE, BO_POS_BASSE)
	BOOL bVisible			// TRUE pour la rendre imm�diatement visible
	);								// TRUE si pas d'erreur


// Positionner une fen�tre BO par rapport � sa fen�tre parente
void BODeplace 
	(
	HBO hBarreOutil,
	int o_x, int o_y, // origine en x de la barre d'outil par rapport a l'origine de la fenetre parente (si pos = 0).
	int pos						// (0, BO_POS_GAUCHE, BO_POS_CENTRE, BO_POS_DROITE) |(0, BO_POS_HAUTE, BO_POS_CENTRE, BO_POS_BASSE)
	);

// R�cup�re l'encombrement d'une fen�tre BO par rapport � la fenetre parente
// Si hBarreOutil est NULLE on agit sur la barre d'outil courante.
void BOGetRect (HBO hBarreOutil, RECT *prect);

// Affiche ou cache une barre d'outil dej� ouverte.
void BOMontre (HBO hBarreOutil, BOOL bVisible);

// Si hBarreOutil est NULLE on teste la barre d'outil courante.
BOOL bBOVisible (HBO hBarreOutil); // TRUE si Ok et visible

// D�truit une barre d'outil et lib�re ses ressources
void BODetruit (HBO * phBarreOutil);


//------------------------------------------------------------
// Acc�s aux boutons de la barre d'outils

// Valider par programme un bouton d'une barre d'outil
// Si hBarreOutil est NULL on agit sur la barre d'outil courante.
void BOCliqueBouton (HBO hBarreOutil, int nIdBouton);

// Force l'�tat Check d'un bouton
// Si hBarreOutil est NULL on agit sur la barre d'outil courante.
void BOCheckBouton (HBO hBarreOutil, int nIdBouton, BOOL bCheck);

// Valider ou inhiber un bouton d'une barre d'outil
// Si hBarreOutil est NULL on agit sur la barre d'outil courante.
void BOValideBouton (HBO hBarreOutil, int nIdBouton, BOOL bValide);


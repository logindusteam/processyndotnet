/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : TIPE_JB.H         Declaration des types du driver MODBUS |
 |   Auteur  : ??                                                       |
 |   Date    : ??                                                       |
 |   Version : ??                                                       |
 |                    Auto Bonne Moyenne Impossible                     |
 |   Portabilite :                                                      |
 |                                                                      |
 |   Mise a jours:                                                      |
 |                                                                      |
 |   +--------+--------+---+--------------------------------------+     |
 |   | date   | qui    |no | raisons                              |     |
 |   +--------+--------+---+--------------------------------------+     |
 |   |04/08/92| PR     |80 | Int�gration Time_out April s�rie 1000|     |
 |   |25/03/93| JB     |xx | Extraction de la fonction            |     |
 |   |        |        |   | conversion_adr_auto_jb () de tipe_jb |     |
 |   |        |        |   | pour rendre tipe_jb ind�pendent de   |     |
 |   |        |        |   | gerebd, car il est utilis� en exe-   |     |
 |   |        |        |   | cution PROCESSYN; La fonction a �t�  |     |
 |   |        |        |   | mise dans un nouveau module: u_jb.*  |     |
 |   |        |        |   |                                      |     |
 |   +--------+--------+---+--------------------------------------+     |
 |                                                                      |
 |   Remarques :                                                        |
 |                                                                      |
 +----------------------------------------------------------------------*/

// ------------------------------- Const -------------------------------------
#define b_geo_jbus               6
#define b_spec_titre_paragraf_jb 49
#define b_titre_paragraf_jb      45
#define b_jbus_e                 7
#define b_jbus_s                 46
#define b_jbus_e_mes             318
#define b_jbus_s_mes             319
#define bx_jbus_e_log            18
#define bx_jbus_e_num_entier     19
#define bx_jbus_e_num_mot        529
#define bx_jbus_e_num_reel       530
#define bx_jbus_e_mes            320
#define bx_jbus_s_log            20
#define bx_jbus_s_num_entier     21
#define bx_jbus_s_num_mot        531
#define bx_jbus_s_num_reel       532
#define bx_jbus_s_mes            321
#define bx_para_jbus_log         27
#define bx_para_jbus_num_entier  28
#define bx_para_jbus_num_mot     539
#define bx_para_jbus_num_reel    540
#define bx_para_jbus_mes         322
#define bx_para_jbus_com         79
#define bx_table_transfert_jb    140
#define bg_jbus_e_log            444
#define bg_jbus_e_num_entier     445
#define bg_jbus_e_num_mot        533
#define bg_jbus_e_num_reel       534
#define bg_jbus_e_mes            446
#define b_jbus_e_tab             447
#define b_jbus_s_tab             448
#define bx_jbus_e_log_tab        449
#define bx_jbus_e_num_entier_tab 450
#define bx_jbus_e_num_mot_tab    535
#define bx_jbus_e_num_reel_tab   536
#define bx_jbus_s_log_tab        451
#define bx_jbus_s_num_entier_tab 452
#define bx_jbus_s_num_mot_tab    537
#define bx_jbus_s_num_reel_tab   538

#define ID_CHAMP_VAR              1
#define ID_CHAMP_CTE              0

// ------------------------------- Type ------------------------------
typedef union
  {
  struct
    {
    DWORD numero_canal;
    DWORD CanalVarOuCste;
    DWORD vitesse;
    DWORD nb_de_bits;
    DWORD parite;
    DWORD encadrement;
    };
  struct
    {
    DWORD numero_esclave;
    DWORD EsclVarOuCste;
    DWORD tipe;
    //DWORD time_out;
		FLOAT fNbUnitesTimeOut;
    DWORD retry;
    };
  } t_spec_titre_paragraf_jb;


typedef struct
  {
  DWORD numero_repere;
  union
    {
    DWORD pos_donnees;
    struct
      {
      DWORD pos_paragraf;
      DWORD pos_specif_paragraf;
      };
    struct
      {
      DWORD pos_es;
      DWORD pos_initial;
      DWORD pos_specif_es;
      };
    };
  } GEO_MODBUS, *PGEO_MODBUS;

typedef struct
  {
  char adresse_automate[8];
  DWORD i_cmd_rafraich;
  } tc_jbus_e;


typedef struct
  {
  char adresse_automate[8];
  } tc_jbus_s;


typedef struct
  {
  char adresse_automate[8];
  DWORD longueur;
  DWORD i_cmd_rafraich;
  } tc_jbus_e_mes;


typedef struct
  {
  char adresse_automate[8];
  DWORD longueur;
  } tc_jbus_s_mes;


typedef struct
  {
  char adresse_automate[8];
  DWORD PosVarAdresse;
  DWORD AdrVarOuCste;
  DWORD LongVarOuCste;
  DWORD longueur;
  DWORD i_cmd_rafraich;
  } tc_jbus_tab;


#define NB_MAX_SORTE_AUTO 8
typedef char t_sorte_auto[NB_MAX_SORTE_AUTO][22];


typedef struct
  {
	DWORD index_es_associe;
  DWORD adresse_automate;
  DWORD numero_canal;
  DWORD CanalVarOuCste;
	DWORD sens_sur_auto;
  DWORD numero_esclave;
  DWORD EsclVarOuCste;
  DWORD tipe_esclave;
  //DWORD time_out;
	FLOAT fNbUnitesTimeOut;
  DWORD retry;
	DWORD i_cmd_rafraich;
	DWORD longueur;
  } tg_jbus;

typedef struct
  {
  DWORD numero_canal;
  DWORD CanalVarOuCste;
	FLOAT fNbUnitesTimeOut;
  //DWORD time_out;
  DWORD retry;
  DWORD numero_esclave;
  DWORD EsclVarOuCste;
  DWORD tipe_esclave;
  DWORD numero_fonction;
  DWORD adresse_automate_depart;
  DWORD i_cmd_rafraich;
  DWORD index_debut;
  DWORD index_fin;
  DWORD nbre_a_lire;
  } X_PARAMETRE_TRAME_MODBUS, *PX_PARAMETRE_TRAME_MODBUS;

typedef struct
  {
  DWORD longueur;
  DWORD index_es_associe;
  int i_tab_recep_significatif;
  } X_MODBUS_E, *PX_MODBUS_E;

typedef struct
  {
  DWORD longueur;
  DWORD index_es_associe;
  DWORD numero_canal;
  DWORD CanalVarOuCste;
  DWORD numero_esclave;
  DWORD EsclVarOuCste;
  DWORD tipe_esclave;
  //DWORD time_out;
	FLOAT fNbUnitesTimeOut;
  DWORD retry;
  DWORD numero_fonction;
  DWORD adresse_automate;
  DWORD nbre_a_ecrire;
  } X_MODBUS_S, *PX_MODBUS_S;

typedef struct
  {
  DWORD i_cmd_rafraich;
  DWORD index_es_associe;
  DWORD numero_canal;
  DWORD CanalVarOuCste;
  DWORD numero_esclave;
  DWORD EsclVarOuCste;
  DWORD PosVarAdresse;
  DWORD AdrVarOuCste;
  DWORD LongVarOuCste;
  DWORD espece_d_auto;
  DWORD tipe_esclave;
  //DWORD time_out;
	FLOAT fNbUnitesTimeOut;
  DWORD retry;
  DWORD numero_fonction;
  DWORD adresse_automate;
  DWORD longueur;
  } X_MODBUS_TAB, *PX_MODBUS_TAB;

typedef struct
  {
  DWORD numero_canal;
  DWORD CanalVarOuCste;
  DWORD vitesse;
  DWORD nb_de_bits;
  DWORD parite;
  DWORD encadrement;
  } t_parametre_com;

// ------------------------------- Variables ------------------------------
static t_sorte_auto sorte_auto = 
	{
	"SMC",
	"PB",
	"TELEMECANIQUE",
	"GOULD",
	"SIEMENS",
	"ALSPA",
	"F15_F16",
	"SMCX000"
	};

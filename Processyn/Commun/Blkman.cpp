/*--------------------------------------------------------------------------+
 | Ce fichier est la propriet� de                                           |
 |                    Societ� LOGIQUE INDUSTRIE                             |
 |              Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3               |
 | Il demeure sa propriet� exclusive, et est confidentiel.                  |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |                                                                          |
 | Titre......: BlkMan.c                                                    |
 | Auteur.....: J.B.                                                        |
 | Date.......: 11/04/94                                                    |
+--------------------------------------------------------------------------*/
// Gestion de plusieurs bases de donn�es (nomenclature BD) identifi�s par leur handle
// Chaque BD contient de 0 � N blocs (nomenclature BLOC) identifi�s par une BD et un num�ro
// Chaque bloc contient de 0 � N enregistrements (nomenclature Enr), identifi�s par un BLOC et un index de 0 � N-1
//
// R�entrance : 
// Acc�s aux blocs : 
//		lors de la cr�ation de la table des blocs, la dimensionner au nombre max de blocs utilis�s
//		=> pas de r�allocation en cours d'activit�
//		Cr�ation d'un bloc : initialiser le bloc puis l'attacher � la table
//		Fermeture d'un bloc : d�tacher le bloc de la table puis le fermer
// Acc�s aux Enrs des blocs : 
//	pas de protection vis � vis d'un acc�s multi thread � un bloc dont on change le Nb d'enr
//	=> v�rifier la protection en pointe_enr simultan�s sur le m�me bloc
//	Remplacer bExiste par une signature de type de bloc
//
// Monte une exception en cas d'erreur
// $$ cr�er des fonctions chargeMem et sauvemem qui renvoient une �ventuelle err OS
//---------------------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "MemMan.h"
#include "FMan.h"
#include "FileMan.h"
#include "Verif.h"
#include "UExcept.h"
#include "BlkMan.h"

VerifInit;

// Taille de la table des blocs (�vite r�alloc en multi-thread)
#define NB_BLOCS_MIN 1000

// Structures de fichiers bases de donn�es
#define BLK_MAX_CHAR_COPY_RIGHT        256
#define BLK_MEMORY_MANAGER_COPY_RIGHT "(c) Logique Industrie 1994"
#define BLK_MEMORY_MANAGER_VERSION_0   0x302E3130                    //'01.0'
#define BLK_MEMORY_MANAGER_VERSION_1   0x302E3131                    //'01.1'
#define BLK_MEMORY_MANAGER_VERSION     BLK_MEMORY_MANAGER_VERSION_0
//
#define BLK_TYPE_FILE_MANAGER_ARRAY    0x414D4654                    //'TFMA'
#define BLK_TYPE_FILE_MANAGER          0x204D4654                    //'TFM '
#define BLK_TYPE_FILE_BLOCK            0x20424654                    //'TFB '
#define BLK_TYPE_FILE_DATA             0x20444654                    //'TFD '

// Un fichier base de donn�es contient une suite contigue d'objets
// ENTETE_ELEMENT_FICHIER_BD suivie imm�diatement des �ventuelles donn�es le composant.

// Ent�te pour tout objet dans un fichier base de donn�es
typedef struct
  {
  DWORD wcbSize;                    // Taille de l'enregistrement (sizeof())
  DWORD wType;                      // Type de l'information courante
  DWORD wNextType;                  // Type des informations suivantes
  DWORD wCount;                     // Nb d'informations suivantes
  } ENTETE_ELEMENT_FICHIER_BD, * PENTETE_ELEMENT_FICHIER_BD;

// Ent�te fichier base de donn�es
typedef struct
  {
  ENTETE_ELEMENT_FICHIER_BD	Info;               // Position des gestionaires
  DWORD	wVersion;
  CHAR	pszCopyRight [BLK_MAX_CHAR_COPY_RIGHT]; // Info: copyright
  } ENTETE_FICHIER_BD, * PENTETE_FICHIER_BD;

typedef struct
  {
  ENTETE_ELEMENT_FICHIER_BD Info;						// Position des blocs
  CHAR      szNomBD [BLK_MAX_CHAR_NOM_BD];	// Info: Nom du gestionaire
  } t_blk_file_manager, *tp_blk_file_manager;

typedef struct
  {
  ENTETE_ELEMENT_FICHIER_BD	Info;					// Position des blocs
  DWORD         wId;											// N� du bloc
  DWORD         dwTailleEnrDeBase;				// Taille de base des enregistrements
  } t_blk_file_block, * tp_blk_file_block;

typedef struct
  {
  DWORD wSize;                      // Taille de l'enregistrement
  } t_blk_file_data_hdr;

//----------------------- Erreurs -------------------------------------------
#define BLK_ERR_MEMORY                   0
#define BLK_ERR_ALREADY_EXIST            1
#define BLK_ERR_NOT_EXIST                2
#define BLK_ERR_RECORD_OVER_END          3
#define BLK_ERR_RECORD_OVERFLOW          4
#define BLK_ERR_RECORD_SIZE              5
#define BLK_ERR_RECORD_OVERLAP           6

#define BLK_ERR_READ_FILE                7
#define BLK_ERR_INVALID_FILE             8
#define BLK_ERR_VERSION_FILE             9
#define BLK_ERR_WRITE_FILE              10
#define BLK_ERR_OPEN_FILE               11
#define BLK_ERR_CLOSE_FILE              12
#define BLK_ERR_RECORD_SIZE_NOT_CONST   13
#define BLK_ERR_BAD_SIZE                14
//
#define BLK_MANAGER_NONE                NULL
#define BLK_BLOCK_NONE                  0xFFFFFFFF
#define BLK_RECORD_NONE                 0xFFFFFFFF
#define BLK_COUNT_NONE                  0xFFFFFFFF

//------------------------------------ Types pour structure en m�moire ------
// $$ d�localiser dans BlocMan
// Ent�te d'un enregistrement
typedef struct
  {
  DWORD		dwTailleEnr;						// Taille d'un enregistrement
  PBYTE	  pEnr;                   // Pointeur sur enregistrement
  } HEADER_ENR;
typedef HEADER_ENR *PHEADER_ENR;

// Ent�te d'un bloc d'enregistrements
typedef struct
  {
  BOOL				bExist;              // Indicateur de cr�ation
  DWORD				dwTailleEnrDeBase;        // Taille de base d'un enregistrement
  DWORD				dwNbEnr;           // Nombre d'enregistrements
  PHEADER_ENR pHdrEnr;           // Pointeur sur ent�te d'un enregistrement
  } BLOC;
typedef BLOC *PBLOC;

// Structure interne d'un objet HBLK_BD
typedef struct
  {
  CHAR	szNomBD [BLK_MAX_CHAR_NOM_BD];// Nom de la base de donn�es
  DWORD	dwNbBlocsBD;									// Nombre de blocs dans apBlocsBD
  PBLOC	apBlocsBD;										// Pointeur sur bloc $$ remplacer par des HBLOCS ou 0
  } BD, *PBD;													// ou un objet tableau dont chaque �l�ment est accessible par son
																			// Id (et non son num�ro) d'�l�ment (plusieurs strat�gies possibles)
//---------------------------------------------------------------------------
static PCSTR pszBlkCopyRight = BLK_MEMORY_MANAGER_COPY_RIGHT;
//
static PCSTR pszBlkManagerCreate = "hBlkCreeBDVierge";
static PCSTR pszBlkCreate        = "pBlkCreeBloc";
static PCSTR pszBlkDelete        = "BlkFermeBloc";
static PCSTR pszBlkRecCount      = "dwBlkNbEnr";
static PCSTR pszBlkRecBaseSize   = "dwBlkTailleEnrDeBase";
static PCSTR pszBlkRecSize       = "dwBlkTailleEnr";
static PCSTR pszBlkRecInsert     = "pBlkInsereEnr";
static PCSTR pszBlkRecDelete     = "BlkEnleveEnr";
static PCSTR pszBlkRecSetSize    = "pBlkChangeTailleEnr";
static PCSTR pszBlkRecPointer    = "pBlkPointeEnr";
static PCSTR pszBlkRecCopy       = "BlkCopieEnr";
//static PCSTR pszBlkRecXChange    = "BlkEchangeEnr";
static PCSTR pszLoad             = "Load function";
static PCSTR pszSave             = "Save function";

//---------------------------------------------------------------------------
static PCSTR pszErrorMessage [] = 
	{
	"System memory allocation failure",
	"User is attempting to create an existing block",
	"User is attempting to reference a non-existant block",
	"User is attempting to access over the block end",
	"User has reached block limits",
	"User \"record size\" is smaler than \"block base size\"",
	"User is attempting to exchange overlaped regions",
	"Unable to read data-base file",
	"User file is not a data-base",
	"User is attempting to load a bad version of data-base",
	"Unable to write in data-base file",
	"Unable to open data-base file",
	"Unable to close data-base file",
	"User is attempting to save a block with non-constant \"record size\"",
	"User is attempting to load a block with a bad \"record size\""
	};

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
static void BlkError 
	(
	HBLK_BD hbd,
	DWORD   dwIdBloc,
	DWORD   dwNEnr,
	DWORD   dwNbEnr,
	DWORD   wError,
	PCSTR  pszFuncName,
	PCSTR  pszManagerName,
	DWORD dwNLine = 0, PCSTR pszSource = NULL)
  {
  PBD pbd = (PBD) hbd;
	char szErr[5000] = "";

	if (pbd)
		pszManagerName = pbd->szNomBD;

	if (pszSource)
		{
		wsprintf (szErr, "Error %d: %s\n"\
			"Data base = %s; Function= %s ()\n"\
			"Block   = %d; Record  = %d; Count   = %d\n",
			"Line %d (%s)",
			wError, pszErrorMessage [wError],
			pszManagerName, pszFuncName,
			(dwIdBloc+1), dwNEnr, dwNbEnr,
			dwNLine, pszSource);
		}
	else
		{
		wsprintf (szErr, "Error %d: %s\n"\
			"Data base = %s; Function= %s ()\n"\
			"Block   = %d; Record  = %d; Count   = %d",
			wError, pszErrorMessage [wError],
			pszManagerName, pszFuncName,
			(dwIdBloc+1), dwNEnr, dwNbEnr);
		}

	// Envoie une exception avec le texte d'explication
	ExcepContexte (EXCEPTION_BLKMAN, szErr);
  }

//---------------------------------------------------------------------------
static void BlkFileRead (HFIC hFile, PVOID pBuffer, DWORD wSize)
  {
  DWORD wSizeRead = 0;

  if ((uFileLire (hFile, pBuffer, wSize, &wSizeRead) != NO_ERROR) || (wSizeRead !=wSize))
    {
    BlkError (BLK_MANAGER_NONE, BLK_BLOCK_NONE, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_READ_FILE, pszLoad, "");
    }
  }

//---------------------------------------------------------------------------
static void BlkChkExternInfo (PENTETE_ELEMENT_FICHIER_BD pFileInfo, DWORD wType, DWORD wNextType, DWORD wSize)
  {
  if (pFileInfo->wcbSize != wSize)
    {
    BlkError (BLK_MANAGER_NONE, BLK_BLOCK_NONE,BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_INVALID_FILE, pszLoad, "");
    }
  if (pFileInfo->wType != wType)
    {
    BlkError (BLK_MANAGER_NONE, BLK_BLOCK_NONE,BLK_RECORD_NONE,BLK_COUNT_NONE, BLK_ERR_INVALID_FILE, pszLoad, "");
    }
  if (pFileInfo->wNextType != wNextType)
    {
    BlkError (BLK_MANAGER_NONE, BLK_BLOCK_NONE, BLK_RECORD_NONE,BLK_COUNT_NONE, BLK_ERR_INVALID_FILE,pszLoad,"");
    }
  }

//---------------------------------------------------------------------------
static void BlkWriteFile (HFIC hFile, PVOID pBuffer, DWORD wSize)
  {
  DWORD wSizeWritten = 0;

  if ((uFileEcrire (hFile, pBuffer, wSize, &wSizeWritten) != NO_ERROR) || (wSizeWritten!= wSize))
    {
    BlkError (BLK_MANAGER_NONE, BLK_BLOCK_NONE, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_WRITE_FILE, pszSave, "");
    }
  }

//---------------------------------------------------------------------------
// charge une base de donn�es � partir du handle ouvert
static HBLK_BD BlkLisCorpsBD (HFIC hFile)
  {
  ENTETE_FICHIER_BD		ManArray;
  t_blk_file_manager  Man;
  DWORD               wBlkCount;
  t_blk_file_block    Blk;
  DWORD               dwNbEnr;
  t_blk_file_data_hdr RecHdr;
  //
  HBLK_BD							hbd;
  PVOID								pRecData;
  DWORD								wSizeRead = 0;

  BlkFileRead (hFile, &ManArray, sizeof (ManArray));
  BlkChkExternInfo (&ManArray.Info, BLK_TYPE_FILE_MANAGER_ARRAY, BLK_TYPE_FILE_MANAGER, sizeof (ManArray));
  switch (ManArray.wVersion)
    {
    case BLK_MEMORY_MANAGER_VERSION:
			break;
    default:
			BlkError (BLK_MANAGER_NONE, BLK_BLOCK_NONE, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_VERSION_FILE, pszLoad, "");
			break;
    }
  //
  DWORD wManCount = ManArray.Info.wCount;
  while (wManCount > 0)
    {
    wManCount--;
    BlkFileRead (hFile, &Man, sizeof (Man));
    BlkChkExternInfo (&Man.Info, BLK_TYPE_FILE_MANAGER, BLK_TYPE_FILE_BLOCK, sizeof (Man));
    //
    hbd = hBlkCreeBDVierge (Man.szNomBD);
    wBlkCount = Man.Info.wCount;
    while (wBlkCount > 0)
      {
      wBlkCount--;
      BlkFileRead (hFile, &Blk, sizeof (Blk));
      BlkChkExternInfo (&Blk.Info, BLK_TYPE_FILE_BLOCK, BLK_TYPE_FILE_DATA, sizeof (Blk));
      //
      pBlkCreeBloc (hbd, Blk.wId, Blk.dwTailleEnrDeBase, 0);
      dwNbEnr = Blk.Info.wCount;
      while (dwNbEnr > 0)
        {
        dwNbEnr--;
        BlkFileRead (hFile, &RecHdr, sizeof (RecHdr));
        pRecData = pBlkInsereEnr (hbd, Blk.wId, BLK_INSERT_END, 1, RecHdr.wSize);
        BlkFileRead (hFile, pRecData, RecHdr.wSize);
        }
      }
    }
  return hbd;
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// sauve la base de donn�es sur le handle fichier
static void BlkManagerSave (HBLK_BD hbd, HFIC hFile)
  {
  ENTETE_FICHIER_BD ManArray;
  ManArray.Info.wcbSize   = sizeof (ManArray);
  ManArray.Info.wType     = BLK_TYPE_FILE_MANAGER_ARRAY;
  ManArray.Info.wNextType = BLK_TYPE_FILE_MANAGER;
  ManArray.Info.wCount    = 1;
  ManArray.wVersion = BLK_MEMORY_MANAGER_VERSION;
  pMemCopyToByte (ManArray.pszCopyRight, pszBlkCopyRight, '\0', BLK_MAX_CHAR_COPY_RIGHT);
  BlkWriteFile (hFile, &ManArray, sizeof (ManArray));
  //
  t_blk_file_manager       Man;
  Man.Info.wcbSize   = sizeof (Man);
  Man.Info.wType     = BLK_TYPE_FILE_MANAGER;
  Man.Info.wNextType = BLK_TYPE_FILE_BLOCK;
  Man.Info.wCount    = 0;
  PBD											pbd = (PBD) hbd;
  DWORD                   wIdxBlk;
  for (wIdxBlk = 0; wIdxBlk < pbd->dwNbBlocsBD; wIdxBlk++)
    {
    if (pbd->apBlocsBD [wIdxBlk].bExist)
      {
      Man.Info.wCount++;
      }
    }
  pMemCopyToByte (Man.szNomBD, pbd->szNomBD, '\0', BLK_MAX_CHAR_COPY_RIGHT);
  BlkWriteFile (hFile, &Man, sizeof (Man));
  //
  if (Man.Info.wCount > 0)
    {
		t_blk_file_block         Blk;
		t_blk_file_data_hdr      RecHdr;
    Blk.Info.wcbSize   = sizeof (Blk);
    Blk.Info.wType     = BLK_TYPE_FILE_BLOCK;
    Blk.Info.wNextType = BLK_TYPE_FILE_DATA;
    for (wIdxBlk = 0; wIdxBlk < pbd->dwNbBlocsBD; wIdxBlk++)
      {
			PBLOC                   pBlk;
      pBlk = &pbd->apBlocsBD [wIdxBlk];
      if (pBlk->bExist)
        {
        Blk.Info.wCount  = pBlk->dwNbEnr;
        Blk.wId          = wIdxBlk;
        Blk.dwTailleEnrDeBase = pBlk->dwTailleEnrDeBase;
        BlkWriteFile (hFile, &Blk, sizeof (Blk));
        //
				DWORD                   wIdxRec;
        for (wIdxRec = 0; wIdxRec < pBlk->dwNbEnr; wIdxRec++)
          {
					PHEADER_ENR               pHdrEnr;
          pHdrEnr = &pBlk->pHdrEnr [wIdxRec];
          RecHdr.wSize = pHdrEnr->dwTailleEnr;
          BlkWriteFile (hFile, &RecHdr, sizeof (RecHdr));
          //
          BlkWriteFile (hFile, pHdrEnr->pEnr, pHdrEnr->dwTailleEnr);
          }
        }
      }
    }
  }

//---------------------------------------------------------------------------
static void BlkSave (HBLK_BD hbd, DWORD dwIdBloc, HFIC hFile)
  {
  PBD			pbd = (PBD) hbd;
  
  //-------------------------------------
  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszSave, "");
    }

  PBLOC   pBlk = &pbd->apBlocsBD [dwIdBloc];
  //-------------------------------------
  if (!pBlk->bExist)
    {
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszSave, "");
    }

  if (pBlk->dwNbEnr > 0)
    {
	  DWORD   wIdx;
    for (wIdx = 0; wIdx < pBlk->dwNbEnr; wIdx++)
      {
      if (pBlk->pHdrEnr [wIdx].dwTailleEnr != pBlk->dwTailleEnrDeBase)
        {
        BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_RECORD_SIZE_NOT_CONST, pszSave, "");
        }
      BlkWriteFile (hFile, pBlk->pHdrEnr [wIdx].pEnr, pBlk->pHdrEnr [wIdx].dwTailleEnr);
      }
    }
  }


//---------------------------------------------------------------------------
//										FONCTIONS EXPORTEES
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// cr�e une base de donn�es vierge
HBLK_BD hBlkCreeBDVierge (PCSTR pszNomBD)
  {
  PBD   pbd = (PBD)pMemAlloue (sizeof (*pbd));
	DWORD	dwNBloc;

  if (!pbd)
    {
    BlkError (BLK_MANAGER_NONE, BLK_BLOCK_NONE, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_MEMORY, pszBlkManagerCreate, pszNomBD);
    }

	// Initialise la racine de la base de donn�e avec des valeurs par d�faut
  pMemCopyToByte (pbd->szNomBD, pszNomBD, '\0', BLK_MAX_CHAR_NOM_BD);
  pbd->dwNbBlocsBD    = NB_BLOCS_MIN;
  pbd->apBlocsBD      = (PBLOC)pMemAlloue ((pbd->dwNbBlocsBD + 1) * sizeof (*pbd->apBlocsBD));

	// Initialise la table des blocs
	// (pour �viter des redimensionnements ult�rieurs probl�matiques en multithread)
	for (dwNBloc = 0; dwNBloc <= NB_BLOCS_MIN; dwNBloc++)
		{
		PBLOC pBlk							= &pbd->apBlocsBD [dwNBloc];

		pBlk->bExist						= FALSE;
		pBlk->dwTailleEnrDeBase = 0;
		pBlk->dwNbEnr						= 0;
		pBlk->pHdrEnr						= NULL;
		}

  return (HBLK_BD)pbd;
  }

//---------------------------------------------------------------------------
// lib�re et d�truit une base de donn�e
void BlkFermeBD (HBLK_BD *phbd)
  {
  PBD pbd = (PBD) *phbd;
  if (pbd && (pbd->dwNbBlocsBD > 0))
    {
    for (DWORD wIdxBlk = 0; wIdxBlk < pbd->dwNbBlocsBD; wIdxBlk++)
      {
			PBLOC pBlk = &pbd->apBlocsBD [wIdxBlk];
      if (pBlk->bExist)
        {
        if (pBlk->dwNbEnr > 0)
          {
          for (DWORD wIdxRec = 0; wIdxRec < pBlk->dwNbEnr; wIdxRec++)
            {
            MemLibere ((PVOID*)(&pBlk->pHdrEnr [wIdxRec].pEnr));
            }
          MemLibere ((PVOID*)(&pBlk->pHdrEnr));
          }
        }
      }
    MemLibere ((PVOID*)(&pbd->apBlocsBD));
    }
  MemLibere ((PVOID*)phbd);
  }

//---------------------------------------------------------------------------
// Renvoie la taille en octets d'une base de donn�es
DWORD dwBlkTailleBD (HBLK_BD hbd)
  {
  PBD		pbd = (PBD) hbd;
  DWORD wSize = 0;

  if (pbd->dwNbBlocsBD > 0)
    {
    for (DWORD wIdxBlk = 0; wIdxBlk < pbd->dwNbBlocsBD; wIdxBlk++)
      {
			PBLOC pBlk = &pbd->apBlocsBD [wIdxBlk];
      if (pBlk->bExist)
        {
        if (pBlk->dwNbEnr > 0)
          {
          for (DWORD wIdxRec = 0; wIdxRec < pBlk->dwNbEnr; wIdxRec++)
            {
            wSize = wSize + pBlk->pHdrEnr [wIdxRec].dwTailleEnr;
            }
          }
        }
      }
    }

  return wSize;
  }

//---------------------------------------------------------------------------
// Cr�e un bloc dans une base de donn�e
PVOID pBlkCreeBloc (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwTailleEnr, DWORD dwNbEnr)
  {
  PBD				pbd = (PBD) hbd;

  //-------------------------------------
  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
		// $$ Fixer au moins � la cr�ation de la BDD le Nb de blocs max
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
/* $$ interdit pour �viter pb acc�s multi-thread
    MemRealloue (&pbd->apBlocsBD, (dwIdBloc + 1) * sizeof (*pbd->apBlocsBD));
    do
      {
      pBlk = &pbd->apBlocsBD [pbd->dwNbBlocsBD];
      pBlk->bExist        = FALSE;
      pBlk->dwTailleEnrDeBase  = 0;
      pBlk->dwNbEnr				= 0;
      pBlk->pHdrEnr       = NULL;
      pbd->dwNbBlocsBD++;
      } while (pbd->dwNbBlocsBD <= dwIdBloc);
*/
    }

  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];

	//-------------------------------------
  if (pBlk->bExist)
    {
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_ALREADY_EXIST, pszBlkCreate, "");
    }

  pBlk->bExist        = TRUE;
  pBlk->dwTailleEnrDeBase  = dwTailleEnr;
  pBlk->dwNbEnr     = 0;
  pBlk->pHdrEnr     = NULL;

  PVOID			ptr = NULL;
  if (dwNbEnr > 0)
    {
    pBlk->dwNbEnr = dwNbEnr;
    pBlk->pHdrEnr = (PHEADER_ENR)pMemAlloue (dwNbEnr * sizeof (*pBlk->pHdrEnr));
    if (pBlk->pHdrEnr == NULL)
      {
      BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
      }
    for (DWORD wIdx = 0; wIdx < dwNbEnr; wIdx++)
      {
      pBlk->pHdrEnr [wIdx].dwTailleEnr = dwTailleEnr;
      pBlk->pHdrEnr [wIdx].pEnr     = (PBYTE)pMemAlloue (dwTailleEnr);
      if (pBlk->pHdrEnr [wIdx].pEnr == NULL)
        {
        BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, dwNbEnr, BLK_ERR_MEMORY, pszBlkCreate, "");
        }
      }
    ptr = pBlk->pHdrEnr [0].pEnr;
    }

  return ptr;
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void BlkFermeBloc (HBLK_BD hbd, DWORD dwIdBloc)
  {
  PBD				pbd = (PBD) hbd;

  //-------------------------------------
  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkDelete, ""); 
    }

  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];

  //-------------------------------------
  if (pBlk->bExist)
		{
		if (pBlk->dwNbEnr > 0)
			{
			for (DWORD  wIdx = 0; wIdx < pBlk->dwNbEnr; wIdx++)
				{
				MemLibere ((PVOID *)(&pBlk->pHdrEnr [wIdx].pEnr));
				}
			}

		MemLibere ((PVOID *)(&pBlk->pHdrEnr));
		pBlk->bExist        = FALSE;
		pBlk->dwTailleEnrDeBase  = 0;
		pBlk->dwNbEnr     = 0;
		pBlk->pHdrEnr       = NULL;
		}
			
	else
    { 
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkDelete, "");
    } 
  } // BlkFermeBloc

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
BOOL bBlkBlocExiste (HBLK_BD hbd, DWORD dwIdBloc)
  {
  PBD				pbd = (PBD) hbd;
  BOOL      bExist = FALSE;

  if (dwIdBloc < pbd->dwNbBlocsBD)
    {
		PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];
    bExist = pBlk->bExist;
    }

  return bExist;
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
DWORD dwBlkNbEnr (HBLK_BD hbd, DWORD dwIdBloc, PCSTR pszSource, DWORD dwNLine)
  {
  PBD pbd = (PBD) hbd;

  //-------------------------------------
  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecCount, "", dwNLine, pszSource);
    }

  //-------------------------------------
  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];
  if (!pBlk->bExist)
    {
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecCount, "", dwNLine, pszSource);
    }

  return pBlk->dwNbEnr;
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
DWORD dwBlkNbEnrSansErr (HBLK_BD hbd, DWORD dwIdBloc)
  {
	// par d�faut 0 enr
	DWORD dwRet = 0; 
  PBD   pbd = (PBD) hbd;

  // num�ro bloc valide ?
  if (dwIdBloc < pbd->dwNbBlocsBD)
    {
		// oui => bloc existe ?
	  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];
		if (pBlk->bExist)
			// oui => r�cup�re son nb d'enr
			dwRet = pBlk->dwNbEnr;
    }

  return dwRet;
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
DWORD dwBlkTailleEnrDeBase (HBLK_BD hbd, DWORD dwIdBloc)
  {
  PBD					pbd = (PBD) hbd;

  //-------------------------------------
  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecBaseSize, "");
    }

  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];
  //-------------------------------------
  if (!pBlk->bExist)
    {
    BlkError (hbd, dwIdBloc, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecBaseSize, "");
    }

  return (pBlk->dwTailleEnrDeBase);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
DWORD dwBlkTailleEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr)
  {
  PBD		   pbd = (PBD) hbd;

  if (dwIdBloc >= pbd->dwNbBlocsBD)
    BlkError (hbd, dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecSize, "");

  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];

  if (!pBlk->bExist)
    BlkError (hbd, dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecSize, "");

  if (dwNEnr >= pBlk->dwNbEnr)
    BlkError (hbd, dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_RECORD_OVER_END, pszBlkRecSize, "");

  return (pBlk->pHdrEnr [dwNEnr].dwTailleEnr);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
PVOID pBlkInsereEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr, PCSTR pszSource, DWORD dwNLine)
  {
  PBD							pbd = (PBD) hbd;

  //-------------------------------------
  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecInsert, "", dwNLine, pszSource);
    }

  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];
  //-------------------------------------
  if (!pBlk->bExist)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecInsert, "", dwNLine, pszSource);
    }

  //-------------------------------------
  DWORD wRealRecId;
  if (dwNEnr == BLK_INSERT_END)
    {
    wRealRecId = pBlk->dwNbEnr;
    }
  else
    {
    wRealRecId = dwNEnr;
    if (wRealRecId > pBlk->dwNbEnr)
      {
      BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecInsert, "", dwNLine, pszSource);
      }
    }

  DWORD wNewRecCount = pBlk->dwNbEnr + dwNbEnr;
  //-------------------------------------
  if ((wNewRecCount < pBlk->dwNbEnr) || (wNewRecCount < dwNbEnr))
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVERFLOW, pszBlkRecInsert, "", dwNLine, pszSource);
    }

  //-------------------------------------
  DWORD wRealRecSize;
  if (dwTailleEnr == BLK_REC_BASE_SIZE)
    {
    wRealRecSize = pBlk->dwTailleEnrDeBase;
    }
  else
    {
    wRealRecSize = dwTailleEnr;
    if (wRealRecSize < pBlk->dwTailleEnrDeBase)
      {
      BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_SIZE, pszBlkRecInsert, "", dwNLine, pszSource);
      }
    }

  //-------------------------------------
  if (dwNbEnr > 0)
    {
    if (wRealRecId == pBlk->dwNbEnr)
      MemRealloue ((PVOID *)(&pBlk->pHdrEnr), wNewRecCount * sizeof (*pBlk->pHdrEnr));
    else
      MemInsere ((PVOID *)(&pBlk->pHdrEnr), wRealRecId * sizeof (*pBlk->pHdrEnr), dwNbEnr * sizeof (*pBlk->pHdrEnr));

    pBlk->dwNbEnr = wNewRecCount;
    //
		DWORD wLastRecId = wRealRecId + dwNbEnr;
    for (DWORD wIdx = wRealRecId; wIdx < wLastRecId; wIdx++)
      {
		  PHEADER_ENR pHdrEnr = &pBlk->pHdrEnr [wIdx];
      pHdrEnr->dwTailleEnr = wRealRecSize;
      pHdrEnr->pEnr     = (PBYTE)pMemAlloue (wRealRecSize);
      if (pHdrEnr->pEnr == NULL)
        {
        BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_MEMORY, pszBlkRecInsert, "", dwNLine, pszSource);
        }
      }
    }

  return (pBlk->pHdrEnr [wRealRecId].pEnr);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void BlkEnleveEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr, DWORD dwNbEnr, PCSTR pszSource, DWORD dwNLine)
  {
  PBD							pbd = (PBD) hbd;
//  PHEADER_ENR     pHdrEnr;
  
  //-------------------------------------
  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecDelete, "", dwNLine, pszSource);
    }

  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];
  //-------------------------------------
  if (!pBlk->bExist)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecDelete, "", dwNLine, pszSource);
    }

  //-------------------------------------
  if (dwNEnr >= pBlk->dwNbEnr)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecDelete, "", dwNLine, pszSource);
    }

  DWORD wLastRecId = dwNEnr + dwNbEnr;
  //-------------------------------------
  if ((wLastRecId < dwNEnr) || (wLastRecId < dwNbEnr))
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecDelete, "");
    }

  //-------------------------------------
  if (wLastRecId > pBlk->dwNbEnr)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecDelete, "", dwNLine, pszSource);
    }

  //-------------------------------------
  if (dwNbEnr > 0)
    {
    for (DWORD wIdx = dwNEnr; wIdx < wLastRecId; wIdx++)
      {
      MemLibere ((PVOID *)(&pBlk->pHdrEnr [wIdx].pEnr));
      }
                              
    pBlk->dwNbEnr = pBlk->dwNbEnr - dwNbEnr;
    if (pBlk->dwNbEnr == 0)
      {
      MemLibere ((PVOID *)(&pBlk->pHdrEnr));
      pBlk->pHdrEnr = NULL;
      }
    else
      {
      MemEnleve ((PVOID *)(&pBlk->pHdrEnr), dwNEnr * sizeof (*pBlk->pHdrEnr), dwNbEnr * sizeof (*pBlk->pHdrEnr));
      }
    }
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
PVOID pBlkChangeTailleEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr, DWORD dwNbEnr, DWORD dwTailleEnr)
  {
  PBD	pbd = (PBD) hbd;

  //-------------------------------------
  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecSetSize, "");
    }

  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];
  //-------------------------------------
  if (!pBlk->bExist)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecSetSize, "");
    }

  //-------------------------------------
  DWORD wLastRecId = dwNEnr + dwNbEnr;
  if (wLastRecId > pBlk->dwNbEnr)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecSetSize, "");
    }

  //-------------------------------------
  DWORD wRealRecSize;
  if (dwTailleEnr == BLK_REC_BASE_SIZE)
    {
    wRealRecSize = pBlk->dwTailleEnrDeBase;
    }
  else
    {
    wRealRecSize = dwTailleEnr;
    if (wRealRecSize < pBlk->dwTailleEnrDeBase)
      {
      BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_RECORD_SIZE, pszBlkRecSetSize, "");
      }
    }

  //-------------------------------------
  for (DWORD wIdx = dwNEnr; wIdx < wLastRecId; wIdx++)
    {
    PHEADER_ENR pHdrEnr = &pBlk->pHdrEnr [wIdx];
    pHdrEnr->dwTailleEnr = wRealRecSize;
    pHdrEnr->pEnr     = (PBYTE)pMemAlloue (wRealRecSize);
    if (pHdrEnr->pEnr == NULL)
      {
      BlkError (hbd, dwIdBloc, dwNEnr, dwNbEnr, BLK_ERR_MEMORY, pszBlkRecSetSize, "");
      }
    }

  return (pBlk->pHdrEnr [dwNEnr].pEnr);
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
PVOID pBlkPointeEnr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr, PCSTR pszSource, DWORD dwNLine)
  {
  PBD			pbd = (PBD) hbd;

  if (dwIdBloc >= pbd->dwNbBlocsBD)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecPointer, "", dwNLine, pszSource);
    }

  PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];

  if (!pBlk->bExist)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, BLK_COUNT_NONE, BLK_ERR_NOT_EXIST, pszBlkRecPointer, "", dwNLine, pszSource);
    }

  if (dwNEnr >= pBlk->dwNbEnr)
    {
    BlkError (hbd, dwIdBloc, dwNEnr, BLK_COUNT_NONE,BLK_ERR_RECORD_OVER_END, pszBlkRecPointer, "", dwNLine, pszSource);
    }

  return (pBlk->pHdrEnr[dwNEnr].pEnr);
  }

//---------------------------------------------------------------------------
// Renvoie l'adresse d'un enregistrement dans ce bloc dans cette base de donn�e 
PVOID pBlkPointeEnrSansErr (HBLK_BD hbd, DWORD dwIdBloc, DWORD dwNEnr)
  {
	PVOID pRet = NULL;
  PBD	pbd = (PBD) hbd;

  if (dwIdBloc < pbd->dwNbBlocsBD)
    {
		PBLOC pBlk = &pbd->apBlocsBD [dwIdBloc];

		if (pBlk->bExist)
			{
			if (dwNEnr < pBlk->dwNbEnr)
				pRet = pBlk->pHdrEnr[dwNEnr].pEnr;
			}
		}
  return pRet;
  }
                        
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void BlkCopieEnr (HBLK_BD hBlkManDst, DWORD wBlkIdDst, DWORD wRecIdDst, HBLK_BD hBlkManSrc, DWORD wBlkIdSrc, DWORD wRecIdSrc, DWORD dwNbEnr)
  {
  PBD pManDst = (PBD) hBlkManDst;

  //-------------------------------------
  if (wBlkIdDst >= pManDst->dwNbBlocsBD)
    {
    BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecCopy, "");
    }

  PBLOC pBlkDst = &pManDst->apBlocsBD [wBlkIdDst];
  //-------------------------------------
  if (!pBlkDst->bExist)
    {
    BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecCopy, "");
    }

  //-------------------------------------
  if (wRecIdDst >= pBlkDst->dwNbEnr)
    {
    BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }

  DWORD wLastRecIdDst = wRecIdDst + dwNbEnr;

  //-------------------------------------
  if ((wLastRecIdDst < wRecIdDst) || (wLastRecIdDst < dwNbEnr))
    {
    BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }

  //-------------------------------------
  if (wLastRecIdDst > pBlkDst->dwNbEnr)
    {
    BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }


  //-------------------------------------
  PBD   pManSrc = (PBD) hBlkManSrc;

  //-------------------------------------
  if (wBlkIdSrc >= pManSrc->dwNbBlocsBD)
    {
    BlkError (hBlkManSrc, wBlkIdSrc, wRecIdSrc, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecCopy, "");
    }

	PBLOC pBlkSrc = &pManSrc->apBlocsBD [wBlkIdSrc];
  //-------------------------------------
  if (!pBlkSrc->bExist)
    {
    BlkError (hBlkManSrc, wBlkIdSrc, wRecIdSrc, dwNbEnr, BLK_ERR_NOT_EXIST, pszBlkRecCopy, "");
    }

  //-------------------------------------
  if (wRecIdSrc >= pBlkSrc->dwNbEnr)
    {
    BlkError (hBlkManSrc, wBlkIdSrc, wRecIdSrc, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }

  DWORD wLastRecIdSrc = wRecIdSrc + dwNbEnr;
  //-------------------------------------
  if ((wLastRecIdSrc < wRecIdSrc) || (wLastRecIdSrc < dwNbEnr))
    {
    BlkError (hBlkManSrc, wBlkIdSrc, wRecIdSrc, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }

  //-------------------------------------
  if (wLastRecIdSrc > pBlkSrc->dwNbEnr)
    {
    BlkError (hBlkManSrc, wBlkIdSrc, wRecIdSrc, dwNbEnr, BLK_ERR_RECORD_OVER_END, pszBlkRecCopy, "");
    }


  //-------------------------------------
  if (dwNbEnr > 0)
    {
		DWORD           wIdx;
		DWORD           wIdxStart;
		DWORD           wIdxEnd;
		INT        iIdxInc;
    if (wRecIdSrc >= wRecIdDst)
      {
      wIdxStart = 0;
      wIdxEnd   = dwNbEnr;
      iIdxInc   = 1;
      }
    else
      {
      wIdxStart = dwNbEnr - 1;
      wIdxEnd   = (DWORD) -1;
      iIdxInc   = -1;
      }
		PHEADER_ENR       pRecHdrDst;
		//
		PHEADER_ENR       pRecHdrSrc;
		//
    if (pBlkSrc->dwTailleEnrDeBase >=  pBlkDst->dwTailleEnrDeBase)
      {
      for (wIdx = wIdxStart; wIdx != wIdxEnd; wIdx = wIdx + iIdxInc)
        {
        pRecHdrDst = &pBlkDst->pHdrEnr [wRecIdDst + wIdx];
        pRecHdrSrc = &pBlkSrc->pHdrEnr [wRecIdSrc + wIdx];
        if (pRecHdrDst->dwTailleEnr != pRecHdrSrc->dwTailleEnr)
          {
          pRecHdrDst->dwTailleEnr = pRecHdrSrc->dwTailleEnr;
          MemLibere ((PVOID *)(&pRecHdrDst->pEnr));
          pRecHdrDst->pEnr = (PBYTE)pMemAlloue (pRecHdrDst->dwTailleEnr);
          if (pRecHdrDst->pEnr == NULL)
            {
            BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_MEMORY, pszBlkRecCopy, "");
            }
          }
        MemCopy (pRecHdrDst->pEnr, pRecHdrSrc->pEnr, pRecHdrSrc->dwTailleEnr);
        }
      }
    else
      {
      for (wIdx = wIdxStart; wIdx != wIdxEnd; wIdx = wIdx + iIdxInc)
        {
        pRecHdrDst = &pBlkDst->pHdrEnr [wRecIdDst + wIdx];
        pRecHdrSrc = &pBlkSrc->pHdrEnr [wRecIdSrc + wIdx];
        if (pRecHdrSrc->dwTailleEnr < pBlkDst->dwTailleEnrDeBase)
          {
          BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_RECORD_SIZE, pszBlkRecCopy, "");
          }
        if (pRecHdrDst->dwTailleEnr != pRecHdrSrc->dwTailleEnr)
          {
          pRecHdrDst->dwTailleEnr = pRecHdrSrc->dwTailleEnr;
          MemLibere ((PVOID *)(&pRecHdrDst->pEnr));
          pRecHdrDst->pEnr = (PBYTE)pMemAlloue (pRecHdrDst->dwTailleEnr);
          if (pRecHdrDst->pEnr == NULL)
            {
            BlkError (hBlkManDst, wBlkIdDst, wRecIdDst, dwNbEnr, BLK_ERR_MEMORY, pszBlkRecCopy, "");
            }
          }
        MemCopy (pRecHdrDst->pEnr, pRecHdrSrc->pEnr, pRecHdrSrc->dwTailleEnr);
        }
      }
    }
  }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
HBLK_BD hBlkChargeBD (PCSTR pszFile, PCSTR pszNomBD)
  {
  HFIC		hFile = NULL;
  HBLK_BD hbd;

  if (uFileOuvre (&hFile, pszFile, 1, CFman::modeRead|CFman::shareDenyWrite|CFman::modeNoTruncate) == NO_ERROR)
    {
    hbd = BlkLisCorpsBD (hFile);
    uFileFerme (&hFile);
    }
  else
    {
		//$$ NON !!! si erreur, pas de BD par d�faut
    if (pszNomBD != NULL)
      {
      hbd = hBlkCreeBDVierge (pszNomBD);
      }
    else
      {
      hbd = (HBLK_BD)0;
      }
    }

  return hbd;
  }

//---------------------------------------------------------------------------
void BlkSauveBD (HBLK_BD hbd, PCSTR pszFile)
  {
  HFIC  hFile = NULL;

  if (uFileOuvre (&hFile, pszFile, 1, CFman::modeCreate|CFman::modeWrite|CFman::shareExclusive) != NO_ERROR)
    {
    BlkError (hbd, BLK_BLOCK_NONE, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_OPEN_FILE, pszSave, "");
    }

  BlkManagerSave (hbd, hFile);

  if (uFileFerme (&hFile) != NO_ERROR)
    {
    BlkError (hbd, BLK_BLOCK_NONE, BLK_RECORD_NONE, BLK_COUNT_NONE, BLK_ERR_CLOSE_FILE, pszSave, "");
    }
  }

//---------------------------------------------------------------------------

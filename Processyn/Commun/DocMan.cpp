// DocMan : gestion des r�pertoires d'une application
// Sont g�r�s deux types de r�pertoires :
// PathAppli : r�pertoire UNIQUE ou se trouve l'ex�cutable en cours d'ex�cution.
//	 Ce r�pertoire est instanci� par ce module. (Appli.c)
// PathDoc : r�pertoire(s) ou se trouve(nt) le(s) document(s) (ou les donn�es) 
//	charg�es par l'application. Ce ou ces r�pertoires sont instanci�s par les 
//  appelants de ce module.

#include "stdafx.h"
#include "std.h"
#include "UStr.h"
#include "PathMan.h"
#include "FMan.h"

#include "DocMan.h"

#define DEFAULT_PATH_DOC "c:\\ProcessynNT\\Applications\\"

// variables globales au process
typedef struct
	{
	char szPathName [MAX_PATH];	// Path et nom du document sans extension.
	char szName [MAX_PATH];			// Nom du document sans extension.
	char szPath [MAX_PATH];			// R�pertoire du document.
	BOOL bNouveau;              // a vrai tant que aucune sauvegarde sur disque 
	} DOCUMENT;

//extern DOCUMENT	Document;

DOCUMENT	Document = {"","", "",TRUE};

//$$ d�velopper la gestion du r�pertoire courant des documents

/// -------------------------------------------------------------------------------
// maj du booleen indicateur de nouvelle applic
void SetNouveauDoc (BOOL bNouveau)
	{
	Document.bNouveau = bNouveau;
	}
/// -------------------------------------------------------------------------------
// initialise les data du document � partir de pszPathNameDoc
// pszPathNameDoc doit �tre sans ext
BOOL bSetPathNameDoc (PCSTR pszPathNameDoc)
	{
	BOOL bRet = FALSE;

	if (StrLength(pszPathNameDoc) <= MAX_PATH)
		{
		StrCopy (Document.szPathName, pszPathNameDoc);
		StrCopy (Document.szPath, pszPathNameDoc);
		pszSupprimeNomExt (Document.szPath);
		StrCopy (Document.szName, pszPathNameDoc);
		pszSupprimePathEtExt (Document.szName);

		bRet = TRUE;
		}
	return bRet;
	}

// -------------------------------------------------------------------------------
// initialise le chemim+nom sans ext du document sur path appli. Le nom est initialis� � null
void InitPathNameDoc (void)  
	{
	char szPathNameDoc[MAX_PATH];

	pszCreePathName (szPathNameDoc, MAX_PATH, DEFAULT_PATH_DOC, "");
	bSetPathNameDoc (szPathNameDoc);
	SetNouveauDoc (TRUE);
	}

// -------------------------------------------------------------------------------
// initialise les data du document avec le premier nom d'application par d�faut non trouv� 
void InitPathNameDocVierge (PCSTR pszExtension)
	{
	DWORD dwNumIndiceNom = 0;
	char szName[MAX_PATH];
	char szPathName[MAX_PATH];
	char szNumIndiceNom[15];

	StrSetNull (szPathName);
	do
		{
		dwNumIndiceNom++;
		StrCopy (szName, "Applic");
		StrConcat (szName, StrDWordToStr (szNumIndiceNom, dwNumIndiceNom));
		pszCreePathName (szPathName, MAX_PATH, DEFAULT_PATH_DOC, szName);
		pszCreeNameExt (szName, MAX_PATH, szPathName, pszExtension);
		} while (CFman::bFAcces (szName, CFman::OF_OUVRE_LECTURE_SEULE));
	bSetPathNameDoc (szPathName);
	SetNouveauDoc (TRUE);
	}

// -------------------------------------------------------------------------------
// Copie dans pszDest le chemin+nom du document + extension sp�cifi�e
// renvoie pszDest ou NULL si Err
PSTR pszCopiePathNameDocExt (PSTR pszDest, int nTailleBuf, PCSTR pszExtension)
	{
	PSTR pszRet = NULL;

	// param�tres valides ?
	if (pszDest && pszExtension)
		// oui => assemble le pathname et l'extension dans le Buffer destination
		pszRet = pszCreeNameExt (pszDest, nTailleBuf, Document.szPathName, pszExtension);

	return pszRet;
	}

// -------------------------------------------------------------------------------
// renvoie l'adresse du chemin+nom sans ext du document
PSTR pszPathNameDoc (void)
	{
	return Document.szPathName;
	}

// -------------------------------------------------------------------------------
// renvoie l'adresse du nom sans ext du document
PSTR pszNameDoc (void)
	{
	return Document.szName;
	}

// -------------------------------------------------------------------------------
// renvoie le r�pertoire du document
PSTR pszPathDoc (void)
	{
	return Document.szPath;
	}

/// -------------------------------------------------------------------------------
// renvoie le booleen indicateur de nouvelle applic
BOOL bGetNouveauDoc (void)
	{
	return (Document.bNouveau);
	}

// -------------------------------------------------------------------------------
// modifie si possible un pathname pour le rendre relatif au path du document
PSTR pszPathNameRelatifAuDoc (PSTR pszPathNameAModifier)
	{
	return pszPathNameRelatif (pszPathNameAModifier, Document.szPath);
	}

// -------------------------------------------------------------------------------
// modifie si possible un pathname relatif au path du document pour le rendre absolu
PSTR pszPathNameAbsoluAuDoc (PSTR pszPathNameAModifier)
	{
	return pszPathNameAbsolu (pszPathNameAModifier, Document.szPath);
	}

// -------------------------------------------------------------------------------
// Copie et modifie si possible un pathname relatif au path du document pour le rendre absolu
void CopiePathNameAbsoluAuDoc (PSTR pszPathNameDestAbsolu, PCSTR pszPathNameSource)
	{
	StrCopy (pszPathNameDestAbsolu, pszPathNameSource);
	pszPathNameAbsolu (pszPathNameDestAbsolu, Document.szPath);
	}

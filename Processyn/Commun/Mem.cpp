//-----------------
// Gestion m�moire
//-----------------

#include "stdafx.h"
#include "std.h"
#include "BlkMan.h"
#include "Mem.h"

//---------------------------------------------------------------------------
static PCSTR  pszDefaultBlockManager = "Processyn";
static HBLK_BD hBlkBDParDefaut = NULL;

//---------------------------------------------------------------------------
void creer_bd_vierge (void)
  {
  hBlkBDParDefaut = hBlkCreeBDVierge (pszDefaultBlockManager);
  }

//---------------------------------------------------------------------------
void fermer_bd (void)
  {
  BlkFermeBD (&hBlkBDParDefaut);
  }

//---------------------------------------------------------------------------
DWORD mem_libre (void)
  {
  return 0;
  }

//---------------------------------------------------------------------------
DWORD long_user (void)
  {
  return (dwBlkTailleBD (hBlkBDParDefaut));
  }

//---------------------------------------------------------------------------
void raz_mem (void)
  {
  BlkFermeBD (&hBlkBDParDefaut);
  hBlkBDParDefaut = hBlkCreeBDVierge (pszDefaultBlockManager);
  }

//---------------------------------------------------------------------------
void raz_mem_total (void)
  {
  raz_mem ();
  }

//---------------------------------------------------------------------------
void cree_bloc (DWORD repere, DWORD taille_enr, DWORD nb_enr)
  {
  pBlkCreeBloc (hBlkBDParDefaut, repere - 1, taille_enr, nb_enr);
  }

//---------------------------------------------------------------------------
void vide_bloc (DWORD repere)
  {
  DWORD wRecBaseSize = dwBlkTailleEnrDeBase (hBlkBDParDefaut, repere - 1);
  BlkFermeBloc (hBlkBDParDefaut, repere - 1);
  pBlkCreeBloc (hBlkBDParDefaut, repere - 1, wRecBaseSize, 0);
  }

//---------------------------------------------------------------------------
void enleve_bloc (DWORD repere)
  {
	if (bBlkBlocExiste (hBlkBDParDefaut, repere - 1))
		{
		BlkFermeBloc (hBlkBDParDefaut, repere - 1);
		}
  }

//---------------------------------------------------------------------------
// Renvoie le nombre d'enregistrements d'un bloc
DWORD nb_enregistrements (PCSTR pszSource, DWORD dwNLine, DWORD repere)
  {
  return (dwBlkNbEnr (hBlkBDParDefaut, repere - 1));
  }

//---------------------------------------------------------------------------
// Renvoie le nombre d'enregistrements d'un bloc
// renvoie 0 si le bloc n'existe pas
DWORD dwNbEnregistrementsSansErr (PCSTR pszSource, DWORD dwNLine, DWORD repere)
  {
  return (dwBlkNbEnrSansErr (hBlkBDParDefaut, repere - 1));
  }

//---------------------------------------------------------------------------
PVOID pointe_enr (PCSTR pszSource, DWORD dwNLine, DWORD repere, DWORD n_enr)
  {
  return (pBlkPointeEnr (hBlkBDParDefaut, repere - 1, n_enr - 1));
  }

//---------------------------------------------------------------------------
// Renvoie l'adresse de l'objet sp�cifi�.
// En cas de non existence renvoie NULL
PVOID pPointeEnrSansErr (PCSTR pszSource, DWORD dwNLine, DWORD repere, DWORD n_enr)
  {
  return (pBlkPointeEnrSansErr (hBlkBDParDefaut, repere - 1, n_enr - 1));
  }

//---------------------------------------------------------------------------
void trans_enr (PCSTR pszSource, DWORD dwNLine, DWORD nb_enr_a_transferer, DWORD de_repere, DWORD a_repere, DWORD de_n_enr, DWORD a_n_enr)
  {
  BlkCopieEnr (hBlkBDParDefaut, a_repere - 1, a_n_enr - 1, hBlkBDParDefaut, de_repere - 1, de_n_enr - 1, nb_enr_a_transferer);
  }

//---------------------------------------------------------------------------
void insere_enr (PCSTR pszSource, DWORD dwNLine, DWORD nb_enr_a_inserer, DWORD repere, DWORD n_enr_origine)
  {
  pBlkInsereEnr (hBlkBDParDefaut, repere - 1, n_enr_origine - 1, nb_enr_a_inserer, BLK_REC_BASE_SIZE, pszSource, dwNLine);
  }

//---------------------------------------------------------------------------
// Ajoute nb_enr_a_inserer au bout du bloc repere
// et renvoie l'adresse du premier enr ajout�
PVOID pAjouteEnrFin (PCSTR pszSource, DWORD dwNLine, DWORD dwNbEnrAAjouter, DWORD repere)
  {
  return pBlkInsereEnr (hBlkBDParDefaut, repere - 1, BLK_INSERT_END, dwNbEnrAAjouter, BLK_REC_BASE_SIZE, pszSource, dwNLine);
  }

//---------------------------------------------------------------------------
void enleve_enr (PCSTR pszSource, DWORD dwNLine, DWORD nb_enr_a_enlever, DWORD repere, DWORD n_enr_origine)
  {
  BlkEnleveEnr (hBlkBDParDefaut, repere - 1, n_enr_origine - 1, nb_enr_a_enlever, pszSource, dwNLine);
  }

//---------------------------------------------------------------------------
BOOL existe_repere (DWORD repere)
  {
  return (bBlkBlocExiste (hBlkBDParDefaut, repere - 1));
  }

//---------------------------------------------------------------------------
DWORD sauve_memoire (PCSTR nom_path)
  {
  BlkSauveBD (hBlkBDParDefaut, nom_path);
  return 0;
  }

//---------------------------------------------------------------------------
DWORD charge_memoire (PCSTR nom_path)
  {
  BlkFermeBD (&hBlkBDParDefaut);
  hBlkBDParDefaut = hBlkChargeBD (nom_path, pszDefaultBlockManager);
  return 0;
  }

//---------------------------------------------------------------------------

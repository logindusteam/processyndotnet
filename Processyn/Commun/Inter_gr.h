/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : inter_gr.h                                               |
 |   Version : 4.0							|
 +----------------------------------------------------------------------*/

BOOL suppression_possible_vdi (DWORD n_ligne);

BOOL lis_couleurs2_vdi (DWORD n_ligne, DWORD indice, PG_COULEUR pnCouleur, PG_COULEUR pnCouleurFond);
BOOL lis_couleurs4_vdi (DWORD n_ligne, DWORD indice, PG_COULEUR pnCouleur, PG_COULEUR pnCouleurFond);

// Interface du descripteur d'animation
void LisDescripteurVDI (PDESCRIPTEUR_CF pDescripteurCf);

// ---------------------------------------------------------------------------
// $$ A virer si "objetisation" des descripteurs
DWORD  lis_nombre_de_ligne_vdi (void);
void  valide_ligne_vdi 
	(REQUETE_EDIT action, DWORD ligne,
	PUL vect_ul, int *nb_ul,
	BOOL *supprime_ligne, BOOL *accepte_ds_bd,
	DWORD *erreur_val, int *niveau, int *indentation);

// � utiliser si compatibilit� avec import PM d�sir�
extern BOOL bPortagePM;

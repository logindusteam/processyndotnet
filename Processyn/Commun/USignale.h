//---------------------------------------------------------------------------------------
// USignale.h
// Gestion d'un messageBox avec �ventuelle s�rialisation d'acc�s
//---------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
// S�rialisation des signale

// acc�s exclusif au signale
void AccesSignale (void);

// acc�s au signale termin�
void FinAccesSignale (void);

//---------------------------------------------------------------------------------------
// Appel de message box
// les flags de type de boite sont rajout�s � ceux pass�s Icone et Boutons
// Renvoie un r�sultat de ::MessageBox
int nSignaleSansSerialisation (PCSTR pszTexte, PCSTR pszTitre, UINT uMB_IconeEtBoutons);

// Idem nSignaleSansSerialisation mais encadr� en interne par AccesSignale et FinAccesSignale
int nSignale (PCSTR pszTexte, PCSTR pszTitre, UINT uMB_IconeEtBoutons);


//----------------------------------------------------------------------------------
// Exceptions

// Arr�t syst�matique de l'application avec un texte comme diagnostic (par Exception)
void SignaleExit (PCSTR pszDiagnostique);

// Avertissement �ventuel avec un texte comme diagnostic et �ventuellement arr�t
void SignaleWarnExit (PCSTR pszDiagnostique);


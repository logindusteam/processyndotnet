// ------------------------------------------------------------
// Paragraphe.h
// Interface des fonctions de gestion de pargraphe utilis�s par
// les descripteurs de Processyn en configuration
// ------------------------------------------------------------
typedef enum 
	{
  entre_num = 0,
  entre_log = 1,
  entre_mes = 2,
  sorti_num = 3,
  sorti_log = 4,
  sorti_mes = 5,
  reflet_log = 6,
  reflet_num = 7,
  reflet_mes = 8,
  //#define paje_ts 9
  //#define fen_ts 10
  canal_jb = 11,
  esclave_jb = 12,
  fichier_dq = 13,
  page_vi = 14,
  ecrit_dq = 15,
  lit_dq = 16,
  enr_dq = 17,
  metronome_cn = 18,
  chronometre_cn = 19,
  //#define canal_cz 20
  //#define station_cz 21
  //#define bloc_note_cz 22
  //#define station_ca 23
  //#define paquet_ca 24
  //#define canal_tcs 25
  //#define esclave_tcs 26
  //#define canal_om 27
  //#define esclave_om 28
  //#define io_om 29
  //#define lr_om 30
  //#define hr_om 31
  //#define pv_om 32
  //#define ti_om 33
  //#define dm_om 34
  //#define canal_km 35
  //#define station_km 36
  //#define statut_om 37
  //#define dz_tim_om 38
  //#define dz_timh_om 39
  //#define dz_cnt_om 40
  //#define dz_cntr_om 41
  structure_re = 42,
  liste_re = 43,
  //#define canal_ad 44
  //#define esclave_ad 45
  //#define canal_fp 46
  //#define esclave_fp 47
  //#define canal_opto 48
  //#define carte_opto 49
  //#define canal_ss 50
  //#define memento_ss 51
  //#define databloc_ss 52
  //#define entree_ss 53
  //#define sortie_ss 54
  //define compteur_ss 55
  //#define canal_sa 56
  //#define esclave_sa 57
  //#define canal_mo 58
  //#define concentrateur_mo 59
  //#define satellite_mo 60
  entree = 61,
  sortie = 62,
  //#define esclave_sp 63
  //#define parametre_sp 64
  //#define variable_sp 65
  //#define canal_l1 66
  //#define esclave_l1 67
  //#define canal_fa 68
  //#define esclave_fa 69
  //#define transparent_fa 70
  //#define tqdd_fa 71
  //#define tgdqs_fa 72
  //#define entre_log_surv 73
  //#define entre_num_surv 74
  //#define las_la 75
  //#define esclave_la 76
  //#define transparent_la 77
  //#define surveillance_la 78
  //#define repetition_la 79
  //#define entre_log_rep 80
  //#define entre_num_rep 81
  //#define esclave_rc 82
  //#define parametre_rc 83
  //#define variable_rc 84
  //#define cmd_log_ric 85
  //#define cmd_num_ric 86
  //#define cmd_mes_ric 87
  //#define las_kmlac 88
  //#define station_kmlac 89
  //#define canal_ce 90
  //#define carte_ce 91
  //#define esclave_h1 92
  //#define domaine_h1_e 93
  //#define domaine_h1_s 94
  //#define canal_ex 95
  //#define station_ex 96
  //#define dsd_ex_e 97
  //#define dsd_ex_s 98
  //#define direct_ex 99
  //#define record_ex 100
  //#define canal_ccm 101
  //#define esclave_ccm 102
  //#define carte_opn 103
  //#define log_opn 104
  //#define num_opn 105
  //#define canal_wi 106
  //#define esclave_wi 107
  //#define param_wi 108
  //#define journal_wi 109
  //#define canal_ab 110
  //#define esclave_ab 111
  //#define diagnostic_ab 112
  //#define canal_ti 113
  //#define esclave_ti 114
  //#define canal_mi 115
  //#define esclave_mi 116
  //#define tampon_efi 117
  //#define para_efi 118
  //#define extension_memoire_ma 119
  entre_num_mot = 120,
  entre_num_reel = 121,
  sorti_num_mot = 122,
  sorti_num_reel = 123,
  //#define canal_eu 124
  //#define esclave_eu 125
  //#define adresse_eu 126
  //#define programmation_eu 127
  entre_num_mot_rep = 128,
  entre_num_reel_rep = 129,
  //#define canal_ut 130
  //#define equipement_ut 131
  //#define fct_attente_ut 132
  //#define entre_log_tor 133
  //#define sorti_log_tor 134
  //#define fct_cyclique_ut 135
  //#define systeme_mos 136
  //#define para_mos 137
  //#define entre_num_reel_surv 138
  //#define entre_num_mot_surv 139
  //#define canal_t3 140
  //#define local_nb 141
  //#define station_nb 142
  //#define initialisation_ov 143
  //#define module_ov 144
  //#define parametres_ov 145
  //#define variables_ov 146
  //#define traitement_ov 147
  groupe_ala = 148,
  archivage_ala = 149,
  impression_ala = 150,
  surveillance_ala = 151,
  priorite_ala = 152,
  visualisation_ala = 153,
  //#define dialogue_dlg 154
  //#define groupe_dlg 155
  liens_predefinis_de = 156,
  variables_importees_de = 157,
  variables_exportees_de = 158,
  carte_ap = 159,
  fct_cyclique_ap = 160,
  PARAGRAPHE_SERVEUR_OPC = 161,
	PARAGRAPHE_GROUPE_OPC = 162
	} ID_PARAGRAPHE, *PID_PARAGRAPHE;

  #define premier_tipe_paragraf entre_num
  #define dernier_tipe_paragraf PARAGRAPHE_GROUPE_OPC // ATTENTION ATTENTION ATTENTION ATTENTION ATTENTION
          
typedef struct
	{
	ID_PARAGRAPHE IdParagraphe;
	DWORD pos_geo_debut;
	DWORD nbr_ligne;
	DWORD profondeur;
	} TITRE_PARAGRAPHE, *PTITRE_PARAGRAPHE;

typedef struct
  {
  DWORD		  position;
  ID_PARAGRAPHE IdParagraphe;
  } CONTEXTE_PARAGRAPHE_LIGNE, *PCONTEXTE_PARAGRAPHE_LIGNE;


// ------------------------------------------------------------------------
//// renvoie vrai si ligne presente et et la position dans le bloc des paragraf de chaque niveau dont depend la ligne
// dans TITRE_PARAGRAPHE, pos_geo_debut pointe sur ligne declaration paragraf
// nbr_ligne du paragraphe
// un paragraf se termine sur un paragraf de meme type ou non ou un
// paragraf de profondeur inferieur ou egale
BOOL bDonneContexteParagrapheLigne
	(
	DWORD b_titre_paragraf,
	DWORD ligne,
	DWORD * pdwProfondeurLigne,
	PCONTEXTE_PARAGRAPHE_LIGNE aContexteParagrapheLigne
	);

// renvoie vrai et la position dans le bloc des paragraf si la ligne se trouve
// dans l'intervalle delimitant un paragraf du type demand�
// pos_geo_debut pointe sur ligne declaration paragraf
// nbr_ligne du paragraphe
// un paragraf se termine sur un paragraf de meme type ou non ou un
// paragraf de profondeur inferieur ou egale
BOOL bLigneDansParagraphe
	(
	DWORD b_titre_paragraf, DWORD ligne,
	ID_PARAGRAPHE IdParagrapheCherche, REQUETE_EDIT	RequeteEdit,
	DWORD *pos_paragraf
	);

void MajDonneesParagraphe
	(
	DWORD b_titre_paragraf, DWORD ligne,
	REQUETE_EDIT RequeteEdit, BOOL un_paragraphe,
	DWORD profondeur, DWORD pos_paragraf_insere
	);

BOOL bValideInsereLigneParagraphe
	(DWORD b_titre_paragraf, DWORD ligne,
	DWORD profondeur, DWORD nbr_imbrication, DWORD *pdwErreur);

void InsereTitreParagraphe (DWORD b_titre_paragraf, DWORD ligne,
	DWORD profondeur, ID_PARAGRAPHE IdParagraphe,
	DWORD *pos_paragraf);

BOOL bSupprimeTitreParagraphe (DWORD b_titre_paragraf, DWORD pos_a_supprimer);





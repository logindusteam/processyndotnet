// Aide.cpp: implementation of the CAide class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UStr.h"
#include "HtmlHelp.h"
#include "Aide.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CAide::CAide()
	{
	StrCopy (m_szNomFichier, "");				// Par d�faut : pas d'aide
	m_hwndVisible = NULL;
	m_bAideInitialisee = FALSE;
	m_dwCookie = 0;
	}

CAide::~CAide()
	{
	// Ne doit �tre appel� qu'une fois dans la vie de l'application
	UnInitialize();
	}

void CAide::Initialize()
	{
	if (!m_bAideInitialisee)
		{
		// Ne doit �tre appel� qu'une fois dans la vie de l'application
		HtmlHelp(NULL, NULL, HH_INITIALIZE, (DWORD)&m_dwCookie) ; // Cookie returned by Hhctrl.ocx.
		m_bAideInitialisee = TRUE;
		}
	}

void CAide::UnInitialize()
	{
	if (m_bAideInitialisee)
		{
		HtmlHelp(NULL, NULL, HH_UNINITIALIZE, (DWORD)m_dwCookie) ; // Pass in cookie		m_bAideInitialisee = TRUE;
		m_bAideInitialisee = FALSE;
		}
	}

BOOL CAide::bAfficheTout(HWND hwndParent)
	{
	Initialize();
	m_hwndVisible = HtmlHelp(hwndParent, m_szNomFichier,HH_DISPLAY_TOPIC,NULL); // "c:\\Help.chm::/Intro.htm>Mainwin",

	return m_hwndVisible != NULL;
	}

void CAide::SetNomFichier(PCSTR pszNomFichier)
	{
	StrCopy (m_szNomFichier, pszNomFichier);
	}


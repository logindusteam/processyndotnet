// WCouleur.c
// Version Win32 11/2/97

#include "stdafx.h"
#include "Appli.h"
#include "UEnv.h"

#include "WBarOuti.h"
#include "idmenugr.h"           // �tre effectu�es par une commande de actiongr

#include "IdGrLng.h"
#include "LireLng.h"
#include "IdLngLng.h"

#include "G_Objets.h"
#include "WEchanti.h"
#include "DevMan.h" 

#include "Verif.h"
#include "WCouleur.h"
VerifInit;

static const char szClasseChoixCouleursGr [] = "ClasseChoixCouleurs";
#define CX_COULEUR 10
#define CY_COULEUR 10

// -----------------------------------------------------------------------
// Fen�tre de s�lection d'une couleur
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocChoixCouleurs (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
	#define ID_FEN_CHOIX_COULEUR_FOND 1
	#define ID_FEN_CHOIX_COULEUR_DESSIN 2
	#define ID_COMBO_COULEUR_DESSIN 1

  LRESULT  mres = 0;
  BOOL     bTraite = TRUE;
	typedef struct
		{
		HBO		hbo;
		G_COULEUR NCouleurDessin;
		G_COULEUR NCouleurFond;
		char	szWindowTextOriginal[1000];
		} ENV, *PENV;

	PENV pEnv;

  switch (msg)
    {
    case WM_CREATE:
			{  

			pEnv = (PENV)pCreeEnv (hwnd, sizeof (ENV));
			// Enregistre le texte initial de la fen�tre
			GetWindowText(hwnd, pEnv->szWindowTextOriginal, 1000);

			// cr�ation de la barre d'outil fille pour le choix de couleur
			pEnv->hbo = hBOCree (NULL, FALSE, TRUE, FALSE, 0, 0);
			int PositionBouton = BO_COL_SUIVANTE;
			int nIndexCouleur = 0;

			for (int nNLuminosite =0; nNLuminosite < G_NB_LUMINOSITES; nNLuminosite++)
				{
				for (int nNTeinte =0; nNTeinte < G_NB_TEINTES; nNTeinte++)
					{
					G_COULEUR nCouleur = d_CouleurAffinite(nIndexCouleur);
					bBOAjouteBoutonCouleur (pEnv->hbo, d_RGB(nCouleur), CX_COULEUR,CY_COULEUR, PositionBouton, BO_BOUTON,CMD_MENU_BMP_OFFSET_COULEUR+(int)nCouleur);
					nIndexCouleur += 1;
					if (nNTeinte == G_NB_TEINTES -1)
						PositionBouton = BO_NOUVELLE_LN;
					else
						PositionBouton = BO_COL_SUIVANTE;
					}
				}

			bBOOuvre (pEnv->hbo, FALSE, hwnd, 0, 0, BO_POS_DROITE | BO_POS_HAUTE, TRUE);

			// fen�tre de choix calcul�e � partir de la barre d'outil
			RECT	rectBO;
			BOGetRect (pEnv->hbo, &rectBO);
			int cy = rectBO.bottom - rectBO.top;
			int cx = rectBO.right - rectBO.left + cy;
			RECT	rect;
			rect.left = (Appli.sizEcran.cx - cx) / 2;
			rect.top = (Appli.sizEcran.cy - cy) / 2;
			rect.right = rect.left + cx;
			rect.bottom = rect.top + cy;
			if (AdjustWindowRectEx (&rect, GetWindowLong (hwnd, GWL_STYLE), FALSE, GetWindowLong (hwnd, GWL_EXSTYLE)))
				{
				// redimensionne cette fen�tre � partir des dimensions de la barre d'outil choix couleur
				::MoveWindow (hwnd, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, FALSE);
				BODeplace (pEnv->hbo, 0, 0, BO_POS_DROITE | BO_POS_HAUTE);
				}
			// cr�ation des deux fen�tres filles imbriqu�es d'�chantillon couleur et dessin
			hwndCreeChoixEchantillon (hwnd, (HMENU)ID_FEN_CHOIX_COULEUR_FOND, 0, 0, cy, cy);

			// Fond Noir par d�faut
			pEnv->NCouleurFond = G_BLACK;
			SetDlgItemCouleurEchantillon (hwnd, ID_FEN_CHOIX_COULEUR_FOND, pEnv->NCouleurFond);

			hwndCreeChoixEchantillon (hwnd, (HMENU)ID_FEN_CHOIX_COULEUR_DESSIN, 
																cy/4, cy/4, cy/2, cy/2);
			// Dessin blanc par d�faut
			pEnv->NCouleurDessin = G_WHITE;
			SetDlgItemCouleurEchantillon (hwnd, ID_FEN_CHOIX_COULEUR_DESSIN, pEnv->NCouleurDessin);

			PostMessage(hwnd, CM_AFFICHE_COULEUR, 0, 0);
			}
			break;

		case CM_SET_COULEUR_DESSIN:
			{
			pEnv = (PENV)pGetEnv (hwnd);
			pEnv->NCouleurDessin = (G_COULEUR)mp1;
			SetDlgItemCouleurEchantillon (hwnd, ID_FEN_CHOIX_COULEUR_DESSIN, (G_COULEUR)mp1);
			PostMessage(hwnd, CM_AFFICHE_COULEUR, 0, 0);
			}
			break;

		case CM_SET_COULEUR_FOND:
			{
			pEnv = (PENV)pGetEnv (hwnd);
			pEnv->NCouleurFond = (G_COULEUR)mp1;
			SetDlgItemCouleurEchantillon (hwnd, ID_FEN_CHOIX_COULEUR_FOND, pEnv->NCouleurFond);
			}
			break;

		case CM_CHANGE_MODE:
			{
			SendDlgItemMessage (hwnd, ID_FEN_CHOIX_COULEUR_DESSIN, CM_CHANGE_MODE, 0, 0);
			SendDlgItemMessage (hwnd, ID_FEN_CHOIX_COULEUR_FOND, CM_CHANGE_MODE, 0, 0);
			}
			break;

		case CM_AFFICHE_COULEUR:
			{
			char szCouleur [2000];
			pEnv = (PENV)pGetEnv (hwnd);
			wsprintf(szCouleur, "%s (%s - %lu)", pEnv->szWindowTextOriginal, d_NomCouleur(pEnv->NCouleurDessin), (int)pEnv->NCouleurDessin);
			LONG lWndStyle = GetWindowLong (hwnd, GWL_STYLE);
			if (lWndStyle & WS_CHILD)
				SetWindowText(GetParent(hwnd), szCouleur);
			else
				SetWindowText(hwnd, szCouleur);
			}
			break;

    case WM_DESTROY:
			{
			pEnv = (PENV)pGetEnv (hwnd);
			BODetruit (&pEnv->hbo);
			// NB : la destruction des fen�tres filles est syst�matique
			bLibereEnv (hwnd);
			}
			break;


    case WM_COMMAND:
			{
      switch (LOWORD (mp1))
        {
				case ID_FEN_CHOIX_COULEUR_FOND:
					{
					pEnv = (PENV)pGetEnv (hwnd);
					//MessageBox(hwnd, "",pEnv->szWindowTextOriginal, MB_OK|MB_ICONINFORMATION);
					}
					break;
				case ID_FEN_CHOIX_COULEUR_DESSIN:
					// Ne fait rien
					{
					pEnv = (PENV)pGetEnv (hwnd);
					//MessageBox(hwnd, "",pEnv->szWindowTextOriginal, MB_OK|MB_ICONINFORMATION);
					}
					break;

				default:
					{
					DWORD CtlId = LOWORD (mp1);
					int nCouleur = (int)(CtlId - CMD_MENU_BMP_OFFSET_COULEUR);
					if ((nCouleur >= 0 ) && (nCouleur < G_NB_COULEURS))
						{
						pEnv = (PENV)pGetEnv (hwnd);
						::SendMessage (::GetParent (hwnd), WM_COMMAND, mp1, mp2);
						pEnv->NCouleurDessin = (G_COULEUR)nCouleur;
						PostMessage(hwnd, CM_AFFICHE_COULEUR, 0, 0);
						}
					}
					break;
        } // switch (LOWORD (mp1))
			}
			break;

		case WM_CLOSE:
			{
			// demande de fermeture de la fen�tre :
			// on simule l'appui sur le bouton couleurs de la barre d'outil des barres d'outils
			}
			break;
    default:
			bTraite = FALSE;
			break;
    } // switch (msg)

  // message non trait�, traitement par d�faut ---------------
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  } // wndprocChoixCouleurs



// -----------------------------------------------------------------------
// Cr�ation de la classe MenuCouleurGr de PcsGr
// -----------------------------------------------------------------------
BOOL bCreeClasseCouleur (void)
  {
	static BOOL bClasseEnregistree = FALSE;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= 0;//CS_HREDRAW | CS_VREDRAW; 
		wc.lpfnWndProc		= wndprocChoixCouleurs; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= NULL; //(HBRUSH)(COLOR_APPWORKSPACE+1); pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseChoixCouleursGr; 
		bClasseEnregistree = RegisterClass (&wc);

		}

  return bClasseEnregistree;
  } // bCreeClasseCouleur

// -----------------------------------------------------------------------
// Cr�ation d'une fen�tre de choix de couleur (contenant sa barre d'outil)
// -----------------------------------------------------------------------
HWND hwndCreeChoixCouleurs (HWND hwndParent, BOOL bVisible, BOOL bChild)
  {
	HWND	hwndRet;
  char  titre [c_nb_car_message_inf] = "";
	DWORD	dwStyle = WS_BORDER;
	
	if (bVisible)
		dwStyle |= WS_VISIBLE;

  bLngLireInformation (c_inf_couleur, titre);
	if (bChild)
		hwndRet = CreateWindow (szClasseChoixCouleursGr,titre,	dwStyle | WS_CHILD,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,	hwndParent,	NULL,	Appli.hinst, NULL);
	else
		hwndRet = CreateWindowEx (WS_EX_PALETTEWINDOW, szClasseChoixCouleursGr,titre,	dwStyle | WS_CAPTION | WS_POPUP,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,	hwndParent,	NULL,	Appli.hinst, NULL);

  return hwndRet;
  } // hwndCreeChoixCouleurs


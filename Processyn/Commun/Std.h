#ifndef   STD_H
  #define STD_H
/*--------------------------------------------------------------------------+
 | Ce fichier est la propriet� de                                           |
 |                                                                          |
 |                    Societ� LOGIQUE INDUSTRIE                             |
 |              Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3               |
 |                                                                          |
 | Il demeure sa propriet� exclusive, et est confidentiel.                  |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------|
 |                                                                          |
 | Titre......: Std.h                                                       |
 | Auteur.....: J.B.                                                        |
 | Date.......: 11/04/94                                                    |
 | Version....: Win32                                                       |
 |                                                                          |
 | Mises a jours:                                                           |
 | +========+========+===+================================================+ |
 | | Date   | Qui    |N� | Raisons                                        | |
 | + - - - -+ - - - -+ - + - - - - - - - - - - - - - - - - - - - - - - - -+ |
 | |14/11/96|JS      |1  | Int�gration � WIN32                            | |
 | +--------+--------+---+------------------------------------------------+ |
 | |        |        |   |                                                | |
 | +========+========+===+================================================+ |
 |                                                                          |
 | Remarques..: Ce fichier contient des d�clarations compl�mentaires aux		|
 | ===========  d�clarations des types standards de WIN32                   |
 |                                                                          |
 | Un pointeur sur unt type X s'�crit PX (rajout de p au d�but du pr�fixe)  |
 | Utiliser la fonction sizeof () pour le calcul des tailles des types			|
 +--------------------------------------------------------------------------*/

  
  //---------------------------------------------------------------------------
  // CHAR utiliser CHAR (1) pr�fixe c et PCCH pour const char *
	// utiliser PSTR pr�fixe psz et PCSTR pour des CHAR * et des const CHAR *
	// adresse de chaine de caract�res termin�s par \0
  //---------------------------------------------------------------------------
  // d�clar� dans <limits.h>
	//#define                CHAR_MIN  0         // 0
  //#define                CHAR_MAX  0xFF         // 255

	// v�rification type de compilation caract�re non sign� par d�faut
	#if '\x00' > '\xff'
		#error Les caract�res doivent �tre NON sign�s par d�faut
	#endif

  //---------------------------------------------------------------------------
  // BOOLEAN utiliser BOOL (2 ou 4) pr�fixe b, TRUE et FALSE
  //---------------------------------------------------------------------------
  
  //---------------------------------------------------------------------------
	// Entiers sign�s : utiliser SHORT (2), LONG (4) ou INT (2 ou 4)
	// pr�fixe s, l, i ou n,x,y pour un ordinal ou cn,cx,cy pour un cardinal
	// utiliser MINSHORT, MAXSHORT, MINLONG, MAXLONG
  //---------------------------------------------------------------------------
  typedef signed char       INT1;
  typedef INT1 *						PINT1;

  #define                   MININT1     MINCHAR  // 0x80
  #define                   MAXINT1     MAXCHAR  // 0x7F
  //
	#undef MININT
	#undef MAXINT
  #define                   MININT ((int)MINLONG)
  #define                   MAXINT ((int)MAXLONG)

  //---------------------------------------------------------------------------
  // Entiers non sign�s utiliser BYTE (1), WORD (2), DWORD(4) et UINT(2 ou 4)
	// pr�fixes	by, w, dw et u
	// utiliser MAXBYTE, MAXWORD ou MAXDWORD
  //---------------------------------------------------------------------------

  //---------------------------------------------------------------------------
  // r�els utiliser FLOAT (4) ou DOUBLE (8) 
	// pr�fixes	r ou f, d
  //---------------------------------------------------------------------------
  #define              FLOAT_DIG         7                //# of decimal digits of precision
  #define              FLOAT_EPSILON     1.192092896e-07F //smallest such that 1.0+FLOAT_EPSILON != 1.0
  #define              FLOAT_MAX         3.402823466e+38F //max value
  #define              FLOAT_MIN         1.175494351e-38F //min positive value
  #define              FLOAT_MAX_10_EXP  38               //max decimal exponent
  #define              FLOAT_MIN_10_EXP  (-37)            //min decimal exponent
  //
  #define              DOUBLE_DIG         15                      //# of decimal digits of precision
  #define              DOUBLE_EPSILON     2.2204460492503131e-016 //smallest such that 1.0+REAL8_EPSILON != 1.0
  #define              DOUBLE_MAX         1.7976931348623158e+308 //max value
  #define              DOUBLE_MIN         2.2250738585072014e-308 //min positive value
  #define              DOUBLE_MAX_10_EXP  308                     //max decimal exponent
  #define              DOUBLE_MIN_10_EXP  (-307)                  //min decimal exponent

  //---------------------------------------------------------------------------
  // HANDLE utiliser HANDLE (pr�fixe h) et PHANDLE
  //---------------------------------------------------------------------------
#endif //STD_H

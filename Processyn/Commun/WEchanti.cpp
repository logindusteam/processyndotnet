// WEchanti.c
// Version Win32 11/2/97
// Permet la repr�sentation de la couleur, du style de ligne, de remplisage sous forme d'�chantillon

#include "stdafx.h"
#include "Appli.h"
#include "UEnv.h"

#include "WBarOuti.h"
#include "idmenugr.h"           // �tre effectu�es par une commande de actiongr

#include "IdGrLng.h"
#include "LireLng.h"
#include "IdLngLng.h"

#include "G_Objets.h" 
#include "DevMan.h" 

#include "Verif.h"
#include "WEchanti.h"
VerifInit;

// Messages de commande couleur pour les fen�tres echantillon (Num couleur en wParam)
#define CM_SET_COULEUR_ECHANTILLON							(WM_USER+100)
#define CM_GET_COULEUR_ECHANTILLON							(WM_USER+101)
#define CM_SET_STYLE_LIGNE_ECHANTILLON					(WM_USER+102)
#define CM_GET_STYLE_LIGNE_ECHANTILLON					(WM_USER+103)
#define CM_SET_STYLE_REMPLISSAGE_ECHANTILLON		(WM_USER+104)
#define CM_GET_STYLE_REMPLISSAGE_ECHANTILLON		(WM_USER+105)
#define CM_CHANGE_MODE													(WM_USER+106)

static const char szClasseEchantillon [] = "ClasseEchantillonCouleur";

// -----------------------------------------------------------------------
// Force la couleur d'un �chantillon (CM_SET_COULEUR_ECHANTILLON)
BOOL SetWindowCouleurEchantillon (HWND hwnd, G_COULEUR dwNCouleur)
	{
	return ::SendMessage (hwnd, CM_SET_COULEUR_ECHANTILLON, (WPARAM)dwNCouleur, 0);
	}

// -----------------------------------------------------------------------
// Force la couleur d'un �chantillon (CM_SET_COULEUR_ECHANTILLON)
BOOL SetDlgItemCouleurEchantillon (HWND hwnd, UINT uIdItem, G_COULEUR dwNCouleur)
	{
	return SendDlgItemMessage (hwnd, uIdItem, CM_SET_COULEUR_ECHANTILLON, (WPARAM)dwNCouleur, 0);
	}

// -----------------------------------------------------------------------
// R�cup�re la couleur d'un �chantillon (CM_GET_COULEUR_ECHANTILLON)
BOOL GetWindowCouleurEchantillon (HWND hwnd, G_COULEUR * pdwNCouleur)
	{
	return ::SendMessage (hwnd, CM_GET_COULEUR_ECHANTILLON, (WPARAM)pdwNCouleur, 0);
	}

// -----------------------------------------------------------------------
// R�cup�re la couleur d'un �chantillon (CM_GET_COULEUR_ECHANTILLON)
BOOL GetDlgItemCouleurEchantillon (HWND hwnd, UINT uIdItem, G_COULEUR * pdwNCouleur)
	{
	return SendDlgItemMessage (hwnd, uIdItem, CM_GET_COULEUR_ECHANTILLON, (WPARAM)pdwNCouleur, 0);
	}

// -----------------------------------------------------------------------
// Force le style de ligne d'un �chantillon (CM_SET_STYLE_LIGNE_ECHANTILLON)
BOOL SetDlgItemStyleLigneEchantillon (HWND hwnd, UINT uIdItem, G_STYLE_LIGNE dwNStyleLigne)
	{
	return SendDlgItemMessage (hwnd, uIdItem, CM_SET_STYLE_LIGNE_ECHANTILLON, (WPARAM)dwNStyleLigne, 0);
	}

// -----------------------------------------------------------------------
// R�cup�re le style de ligne d'un �chantillon (CM_GET_STYLE_LIGNE_ECHANTILLON)
BOOL GetDlgItemStyleLigneEchantillon (HWND hwnd, UINT uIdItem, G_STYLE_LIGNE * pdwNStyleLigne)
	{
	return SendDlgItemMessage (hwnd, uIdItem, CM_GET_STYLE_LIGNE_ECHANTILLON, (WPARAM)pdwNStyleLigne, 0);
	}


// -----------------------------------------------------------------------
// Force le style de Remplissage d'un �chantillon (CM_SET_STYLE_REMPLISSAGE_ECHANTILLON)
BOOL SetDlgItemStyleRemplissageEchantillon (HWND hwnd, UINT uIdItem, G_STYLE_REMPLISSAGE dwNStyleRemplissage)
	{
	return SendDlgItemMessage (hwnd, uIdItem, CM_SET_STYLE_REMPLISSAGE_ECHANTILLON, (WPARAM)dwNStyleRemplissage, 0);
	}

// -----------------------------------------------------------------------
// R�cup�re le style de Remplissage d'un �chantillon (CM_GET_STYLE_REMPLISSAGE_ECHANTILLON)
BOOL GetDlgItemStyleRemplissageEchantillon (HWND hwnd, UINT uIdItem, G_STYLE_REMPLISSAGE * pdwNStyleRemplissage)
	{
	return SendDlgItemMessage (hwnd, uIdItem, CM_GET_STYLE_REMPLISSAGE_ECHANTILLON, (WPARAM)pdwNStyleRemplissage, 0);
	}

// -----------------------------------------------------------------------
// Fen�tre d'affichage d'un �chantillon 
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocEchantillon (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
	typedef struct
	 {
	 G_COULEUR						dwNCouleur;
	 G_STYLE_LIGNE				iStyleLigne;
	 G_STYLE_REMPLISSAGE	iStyleRemplissage;
	 BOOL									FormePleine;
	 BOOL									select;
	 HBO									hbo;
	 } ENV_ECHANTILLON, *PENV_ECHANTILLON;

  LRESULT  mres = 0;
  BOOL     bTraite = TRUE;
  PENV_ECHANTILLON pEnv;

  switch (msg)
    {
    case WM_CREATE:
			pEnv = (PENV_ECHANTILLON)pCreeEnv (hwnd, sizeof (ENV_ECHANTILLON));
			pEnv->FormePleine = TRUE;
	    pEnv->dwNCouleur = G_PALEGRAY;
	    pEnv->iStyleLigne = G_STYLE_LIGNE_CONTINUE;
	    pEnv->iStyleRemplissage = G_STYLE_REMPLISSAGE_PLEIN;
		  pEnv->select = FALSE;
      break;

    case WM_DESTROY:
			pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
			bLibereEnv (hwnd);
			break;

    case CM_SET_COULEUR_ECHANTILLON:
			if (mp1 < G_NB_COULEURS)
				{
				pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
				pEnv->dwNCouleur = (G_COULEUR) mp1;
				pEnv->FormePleine = TRUE;
				pEnv->iStyleRemplissage = G_STYLE_REMPLISSAGE_PLEIN;
				mres = TRUE;
				::InvalidateRect (hwnd, NULL, TRUE);
				}
			else
				VerifWarningExit;
			break;

    case CM_SET_STYLE_LIGNE_ECHANTILLON:
			if (mp1 < G_NB_STYLES_LIGNE)
				{
				pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
				pEnv->iStyleLigne = (G_STYLE_LIGNE)mp1;
				pEnv->FormePleine = FALSE;
				pEnv->dwNCouleur = G_BLACK;
				mres = TRUE;
				::InvalidateRect (hwnd, NULL, TRUE);
				}
			else
				VerifWarningExit;
			break;

    case CM_SET_STYLE_REMPLISSAGE_ECHANTILLON:
			if (mp1 < G_NB_STYLES_REMPLISSAGE)
				{
				pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
				pEnv->iStyleRemplissage = (G_STYLE_REMPLISSAGE)mp1;
				pEnv->FormePleine = TRUE;
				pEnv->dwNCouleur = G_BLACK;
				mres = TRUE;
				::InvalidateRect (hwnd, NULL, TRUE);
				}
			else
				VerifWarningExit;
			break;

    case CM_GET_COULEUR_ECHANTILLON:
			if (mp1)
				{
				pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
				*((G_COULEUR *)mp1) = pEnv->dwNCouleur;
				mres = TRUE;
				}
			else
				VerifWarningExit;
			break;

    case CM_GET_STYLE_LIGNE_ECHANTILLON:
			if (mp1)
				{
				pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
				*((DWORD *)mp1) = pEnv->iStyleLigne;
				mres = TRUE;
				}
			else
				VerifWarningExit;
			break;

    case CM_GET_STYLE_REMPLISSAGE_ECHANTILLON:
			if (mp1)
				{
				pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
				*((DWORD *)mp1) = pEnv->iStyleRemplissage;
				mres = TRUE;
				}
			else
				VerifWarningExit;
			break;

    case CM_CHANGE_MODE:
			pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
			pEnv->select = !pEnv->select;
			::InvalidateRect (hwnd, NULL, TRUE);
			mres = TRUE;
			break;

    case WM_PAINT:
			{
			HBRUSH	hbrush;

			pEnv = (PENV_ECHANTILLON)pGetEnv (hwnd);
			PAINTSTRUCT ps;
			::BeginPaint (hwnd, &ps);
			RECT		rect;
			::GetClientRect (hwnd, &rect);
			HBRUSH	hbrushFond = hCreeBrossePleine (G_PALEGRAY);
			FillRect (ps.hdc, &rect, hbrushFond);
			HPEN		hpen;
			if (pEnv->FormePleine)
				{
				hpen = (HPEN)GetStockObject(NULL_PEN);
				if (pEnv->iStyleRemplissage == G_STYLE_REMPLISSAGE_PLEIN)
					{
					hbrush = hCreeBrossePleine (pEnv->dwNCouleur);
					}
				else
					{
					hbrush = hCreeBrosseStyle (pEnv->iStyleRemplissage, pEnv->dwNCouleur);
					}
				}
			else
				{
				hbrush = (HBRUSH)GetStockObject(NULL_BRUSH);
				hpen = hCreeStylo (pEnv->iStyleLigne, pEnv->dwNCouleur);
				}
			int BkModeOld = SetBkMode (ps.hdc, TRANSPARENT);
			HBRUSH	hbrushOld = (HBRUSH)SelectObject (ps.hdc, hbrush);
			HPEN		hpenOld	= (HPEN)SelectObject (ps.hdc, hpen);
			if (pEnv->FormePleine)
				{
				Rectangle (ps.hdc, rect.left, rect.top, rect.right, rect.bottom);
				}
			else
				{
				LONG lYMilieu = rect.top + ((rect.bottom - rect.top)/2);
				MoveToEx (ps.hdc, rect.left, lYMilieu, NULL);
				LineTo (ps.hdc, rect.right, lYMilieu);
				}
			SelectObject (ps.hdc, hbrushOld);
			SelectObject (ps.hdc, hpenOld);
			SetBkMode (ps.hdc, BkModeOld);

			//FillRect (ps.hdc, &rect, hbrush);
			DeleteObject(hbrushFond);
			DeleteObject(hbrush);
			DeleteObject(hpen);
			DrawEdge (ps.hdc, &rect, pEnv->select ? BDR_SUNKENINNER : BDR_RAISEDINNER, BF_RECT);
			::EndPaint (hwnd, &ps);
			}
      break;

    case WM_LBUTTONDOWN:
			{
			// Notification au parent de type Click bouton
			::SendMessage (::GetParent(hwnd), WM_COMMAND, MAKEWPARAM (::GetDlgCtrlID(hwnd), BN_CLICKED), (LPARAM)hwnd);
			bTraite = FALSE;
			}
			break;

    default:
			bTraite = FALSE;
			break;
    } // switch (msg) 

  // message non trait� => traitement par d�faut
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return mres;
  } // wndprocEchantillon


// -----------------------------------------------------------------------
// Cr�ation de la classe Echantillon de PcsGr
// -----------------------------------------------------------------------
BOOL bCreeClasseEchantillon (void)
  {
	static BOOL bClasseEnregistree = FALSE;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= 0; // CS_OWNDC 
		wc.lpfnWndProc		= wndprocEchantillon; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= (HBRUSH)NULL; //(COLOR_APPWORKSPACE+1); //pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseEchantillon; 
		bClasseEnregistree = RegisterClass (&wc);
		}

  return bClasseEnregistree;
  } // bCreeClasseEchantillon

// -----------------------------------------------------------------------
// Cr�ation d'une fen�tre d'echantillon
// -----------------------------------------------------------------------
HWND hwndCreeChoixEchantillon (HWND hwndParent, HMENU id, int x, int y, int nWidth, int nHeight)
  {
	HWND	hwndRet = CreateWindow (szClasseEchantillon,	NULL,	WS_CHILD | WS_VISIBLE,
													x, y, nWidth, nHeight, hwndParent, id,	Appli.hinst, NULL);

  return hwndRet;
  } // hwndCreeChoixCouleurs


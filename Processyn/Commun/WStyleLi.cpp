// WStyleLi.c
// Version Win32 11/2/97

#include "stdafx.h"
#include "Appli.h"
#include "UEnv.h"

#include "WBarOuti.h"
#include "idmenugr.h"           // �tre effectu�es par une commande de actiongr

#include "IdGrLng.h"
#include "LireLng.h"
#include "IdLngLng.h"

#include "G_Objets.h" 
#include "WEchanti.h" 

#include "Verif.h"
#include "WStyleLi.h"
VerifInit;

static const char szClasseChoixStyleLignesGr [] = "ClasseChoixStyleLignes";

// -----------------------------------------------------------------------
// Fen�tre de s�lection d'un Style de Ligne
// -----------------------------------------------------------------------
static LRESULT CALLBACK wndprocChoixStyleLignes (HWND hwnd, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = 0;
  BOOL     bTraite = TRUE;
	typedef struct
		{
		HBO		hbo;
		} ENV, *PENV;

	PENV pEnv;

  switch (msg)
    {
    case WM_CREATE:
			{  
			int		cx;
			int		cy;
			RECT	rectBO;
			RECT	rect;

			pEnv = (PENV)pCreeEnv (hwnd, sizeof (ENV));

			// cr�ation de la barre d'outil fille choix de Ligne
			pEnv->hbo = hBOCree (NULL, FALSE, TRUE, FALSE, 0, 0);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STL0),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL0);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STL1),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL1);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STL2),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL2);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STL3),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL3);
			bBOAjouteBouton (pEnv->hbo, MAKEINTRESOURCE(IDB_CMD_STL4),BO_NOUVELLE_LN,BO_COCHE,CMD_MENU_BMP_STL4);

			bBOOuvre (pEnv->hbo, FALSE, hwnd, 0, 0, BO_POS_DROITE | BO_POS_HAUTE, TRUE);

			//$ac old param	bBOOuvre (pEnv->hbo, TRUE, hwnd, rectBO.right, rectBO.top, 0, BOStyleLigne.bVisible);

			// fen�tre de choix calcul�e � partir de la barre d'outil
			BOGetRect (pEnv->hbo, &rectBO);
			cy = rectBO.bottom - rectBO.top;
			//cx = rectBO.right - rectBO.left + cy;
			cx = rectBO.right - rectBO.left;
			rect.left = (Appli.sizEcran.cx - cx) / 2;
			rect.top = (Appli.sizEcran.cy - cy) / 2;
			rect.right = rect.left + cx;
			rect.bottom = rect.top + cy;
			if (AdjustWindowRectEx (&rect, GetWindowLong (hwnd, GWL_STYLE), FALSE, GetWindowLong (hwnd, GWL_EXSTYLE)))
				{
				// redimensionne cette fen�tre � partir des dimensions de la barre d'outil choix Ligne
				::MoveWindow (hwnd, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, FALSE);
				//BODeplace (pEnv->hbo, 0, 0, BO_POS_DROITE | BO_POS_HAUTE);
				BODeplace (pEnv->hbo, 0, 0, BO_POS_CENTRE);
				}
			}
			break;

    case WM_DESTROY:
			pEnv = (PENV)pGetEnv (hwnd);
			BODetruit (&pEnv->hbo);
			// NB : la destruction des fen�tres filles est syst�matique
			bLibereEnv (hwnd);
			break;


    case WM_COMMAND:
      switch (LOWORD (mp1))
        {
				case CMD_MENU_BMP_STL0:
				case CMD_MENU_BMP_STL1:
				case CMD_MENU_BMP_STL2:
				case CMD_MENU_BMP_STL3:
				case CMD_MENU_BMP_STL4:
					::SendMessage (::GetParent (hwnd), WM_COMMAND, mp1, mp2);
          break;
        } // switch (LOWORD (mp1))
				break;

			case WM_CLOSE:
					// demande de fermeture de la fen�tre :
					// on simule l'appui sur le bouton Lignes de la barre d'outil des barres d'outils
					break;
    default:
			bTraite = FALSE;
			break;
    } // switch (msg)

  // message non trait�, traitement par d�faut ---------------
  if (!bTraite)
    mres = DefWindowProc (hwnd, msg, mp1, mp2);

  return (mres);
  } // wndprocChoixStyleLignes

// -----------------------------------------------------------------------
// Cr�ation de la classe StyleLigneGr de PcsGr
// -----------------------------------------------------------------------
BOOL bCreeClasseStyleLignes (void)
  {
	static BOOL bClasseEnregistree = FALSE;

	if (!bClasseEnregistree)
		{
		WNDCLASS wc;

		wc.style					= 0;//CS_HREDRAW | CS_VREDRAW; 
		wc.lpfnWndProc		= wndprocChoixStyleLignes; 
		wc.cbClsExtra			= 0; 
		wc.cbWndExtra			= 0; 
		wc.hInstance			= Appli.hinst; 
		wc.hIcon					= NULL;
		wc.hCursor				= LoadCursor (NULL, IDC_ARROW); 
		wc.hbrBackground	= NULL; //(HBRUSH)(COLOR_APPWORKSPACE+1); pas d'effacement
		wc.lpszMenuName		= NULL; 
		wc.lpszClassName	= szClasseChoixStyleLignesGr; 
		bClasseEnregistree = RegisterClass (&wc);
		}

  return bClasseEnregistree;
  } // bCreeClasseLigne

// -----------------------------------------------------------------------
// Cr�ation d'une fen�tre de choix de style de ligne (contenant sa barre d'outil)
// -----------------------------------------------------------------------
HWND hwndCreeChoixStyleLignes (HWND hwndParent, BOOL bVisible, BOOL bChild)
  {
	HWND	hwndRet;
  char  titre [c_nb_car_message_inf] = "";
	DWORD	dwStyle = WS_BORDER;
	
	if (bVisible)
		dwStyle |= WS_VISIBLE;

  bLngLireInformation (c_inf_style_ligne, titre);
	if (bChild)
		hwndRet = CreateWindow (szClasseChoixStyleLignesGr,titre,	dwStyle | WS_CHILD,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,	hwndParent,	NULL,	Appli.hinst, NULL);
	else
		hwndRet = CreateWindowEx (WS_EX_PALETTEWINDOW, szClasseChoixStyleLignesGr,titre,	dwStyle | WS_CAPTION | WS_POPUP,
			CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,	hwndParent,	NULL,	Appli.hinst, NULL);

  return hwndRet;
  } // hwndCreeChoixStyleLignes


/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |                                                                      |
 |   Titre   : pcsdlg.c                                                 |
 |   Auteur  : LM                                                       |
 |   Date    : 01/09/92 						|
 |   Remarques : Gestion des boites de dialogue dans Pcsconf            |
 +----------------------------------------------------------------------*/

#include "stdafx.h"
#include <stdlib.h>
#include "std.h"
#include "Appli.h"
#include "MemMan.h"
#include "UStr.h"
#include "IdConfLng.h"
#include "ULangues.h"
#include "UEnv.h"
#include "USignale.h"
#include "IdStrProcessyn.h"
#include "Produits.h"
#include "ULangues.h"
#include "LireLng.h"

#include "pcsdlg.h"


// -----------------------------------------------------------------------
// Affiche une boite de dialogue modale de demande de confirmation
// renvoie IDYES IDNO ou IDCANCEL
// -----------------------------------------------------------------------
int dlg_confirmation (HWND hwndParent, char *pszMess)
  {
  return (nSignale (pszMess, Appli.szNom, MB_ICONQUESTION | MB_YESNOCANCEL));
  }


// -----------------------------------------------------------------------
// Afficher une boite de dialogue modale avec message erreur
// -----------------------------------------------------------------------
int dlg_erreur (char *pszMess)
  {
	return (nSignale (pszMess, NULL, MB_ICONINFORMATION | MB_OK));
  }

// -----------------------------------------------------------------------
// Afficher une boite de dialogue modale d'erreur � partir du num�ro de message erreur
// -----------------------------------------------------------------------
void dlg_num_erreur (DWORD dwNErr)
  {
  char szErr[c_nb_car_message_err];

  bLngLireErreur (dwNErr, szErr);
  dlg_erreur (szErr);
  }


// -----------------------------------------------------------------------
//                    DIALOGUE AFFICHAGE PREAMBULE
// Objet....: Traitement des messages de la fen�tre preambule
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocPreambule (HWND hDlg, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = TRUE;			   // Valeur de retour
  
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			char   str_num_ver [10];
			int	 lg_str;

			StrWORDToStr (str_num_ver, PRODUIT_PROCESSYN_WIN32);
			lg_str = StrLength (str_num_ver);
			StrInsertChar (str_num_ver, '.', (DWORD)(lg_str - 2));
			SetDlgItemText(hDlg, ID_PROPOS_VER, str_num_ver);

			// limitation dans le temps de la boite de dialogue ?
			if (mp2)
				::SetTimer (hDlg, 2, 3000, NULL);
			}
      break;

		case WM_DESTROY:
			::KillTimer (hDlg, 2);
//      mres = FALSE;
      break;

		case WM_COMMAND:
    case WM_TIMER:
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
			EndDialog(hDlg, TRUE);
//			mres = FALSE;
			break;

    default:
			mres = FALSE;
      break;
    }
  return mres;
  }

// -----------------------------------------------------------------------
//                    DIALOGUE AFFICHAGE PREAMBULE
// Objet....: Traitement des messages de la fen�tre preambule
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocPreambuleModeless (HWND hDlg, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  LRESULT  mres = TRUE;			   // Valeur de retour
  
  switch (msg)
    {
    case WM_INITDIALOG:
			{
			// limitation dans le temps de la boite de dialogue ?
			if (mp2)
				::SetTimer (hDlg, 2, 3000, NULL);

			// Le bitmap de splash est affich� ?
			if (0 == SendDlgItemMessage (hDlg, ID_PROPOS_VER, STM_GETIMAGE, IMAGE_BITMAP, 0))
				{
				// non => charge et affiche le bitmap dans le contr�le static - � partir de la DLL de langue
				HBITMAP hBitmap = (HBITMAP)LoadImage(Appli.hinstDllLng, MAKEINTRESOURCE(IDB_LOGO_NT_16), IMAGE_BITMAP, 0, 0, LR_DEFAULTCOLOR);

				if (hBitmap)
					SendDlgItemMessage (hDlg, ID_PROPOS_VER, STM_SETIMAGE, (WPARAM)IMAGE_BITMAP, (LPARAM) (HANDLE)hBitmap);
				}
			}
      break;

		case WM_DESTROY:
			::KillTimer (hDlg, 2);
      mres = FALSE;
      break;

    case WM_TIMER:
		case WM_LBUTTONDOWN:
		case WM_RBUTTONDOWN:
			{
			::DestroyWindow(hDlg);
			// $$ faut il lib�rer le bitmap ? DeleteObject ()
			mres = FALSE;
			}
			break;

    default:
			mres = FALSE;
      break;
    }
  return mres;
  }

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
int dlg_preambule (HWND hwnd,BOOL preamb_timer)
  {
	LangueDialogBoxParam (MAKEINTRESOURCE(DLG_PREAMBULE), hwnd,
		dlgprocPreambule, (LPARAM)preamb_timer);
  return (0);
  }

// -----------------------------------------------------------------------
HWND hwndCreeSplash (HWND hwnd)
  {
	LangueCreateDialogParam (MAKEINTRESOURCE(DLG_PREAMBULE), hwnd,
		dlgprocPreambuleModeless, TRUE);
  return (0);
  }


// -----------------------------------------------------------------------
//                   DIALOGUE SAISIE TEXTE
// Objet....: fen�tre de saisie d'un texte
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocSaisieTexte (HWND hDlg, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  mres = TRUE;			   // Valeur de retour
  PPARAM_DLG_SAISIE_TEXTE* ppSaisieTexte;
  
  switch (msg)
    {
    case WM_INITDIALOG:
			// associe � cette boite de dialogue l'adresse de la structure pass�e en mp2
			ppSaisieTexte = (PPARAM_DLG_SAISIE_TEXTE*)pCreeEnvInit (hDlg, sizeof (PPARAM_DLG_SAISIE_TEXTE), (PVOID)&mp2);

			// initialisations des textes de la boite de dialogue
			::SetWindowText (hDlg, (*ppSaisieTexte)->titre);
			::SendMessage (::GetDlgItem (hDlg, ID_SAISIE_TEXTE), EM_SETLIMITTEXT ,(WPARAM)((*ppSaisieTexte)->nb_max_car_saisie), 0); // texte en inverse video
			SetDlgItemText (hDlg, ID_SAISIE_TEXTE, (*ppSaisieTexte)->saisie);
			::SendMessage (::GetDlgItem (hDlg, ID_SAISIE_TEXTE), EM_SETSEL, (WPARAM)0, (LPARAM)255); // pr�s�lection du texte
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_OK_SAISIE:
					{
					ppSaisieTexte = (PPARAM_DLG_SAISIE_TEXTE*)pGetEnv (hDlg);
          GetDlgItemText (hDlg, ID_SAISIE_TEXTE, (*ppSaisieTexte)->saisie, 255);
          EndDialog(hDlg, TRUE);
					}
          break;

				case IDCANCEL:
				case ID_ANNULER_SAISIE:
          EndDialog(hDlg, FALSE);
          break;
				default:
					mres = FALSE;
          break;
        }
      break;

    case WM_DESTROY:
			bLibereEnv (hDlg);
      break;

    default:
      mres = FALSE;
      break;
    }
  return mres;
  }


// -----------------------------------------------------------------------
// Nom......: dlg_saisie_texte
// Objet....: saisie d'un texte dans une boite de dialogue
// Entr�es..: initialisation des champs textes
// Sorties..:
// Retour...: retour_b indique qu'on a bien recupere le texte
// Remarques: l'adresse de retour_recherche est passee la WndProc lorsqu'on recoit le message
//            WM_INITDIALOG, l'adresse se trouve dans mp2
// -----------------------------------------------------------------------
BOOL dlg_saisie_texte (HWND hDlg, PPARAM_DLG_SAISIE_TEXTE retour_saisie_texte)
  {
  return LangueDialogBoxParam (MAKEINTRESOURCE(DLG_SAISIE_TEXTE), hDlg, dlgprocSaisieTexte,(LPARAM)retour_saisie_texte);
  }

// -----------------------------------------------------------------------
// Boite de dialogue parametrable de saisie d'un num
// -----------------------------------------------------------------------
static BOOL CALLBACK dlgprocSaisieNum (HWND hDlg, UINT msg, WPARAM mp1, LPARAM mp2)
  {
  BOOL  mres = TRUE;			   // Valeur de retour
  PPARAM_DLG_SAISIE_NUM * ppSaisieNum;

  switch (msg)
    {
    case WM_INITDIALOG:
			// associe � cette boite de dialogue l'adresse de la structure pass�e en mp2
			ppSaisieNum = (PPARAM_DLG_SAISIE_NUM *)pCreeEnvInit (hDlg, sizeof (PPARAM_DLG_SAISIE_NUM), (PVOID) &mp2);

			// initialisations des textes de la boite de dialogue
			::SetWindowText (hDlg, (*ppSaisieNum)->titre);
			SetDlgItemInt (hDlg, ID_SAISIE_NUM, (*ppSaisieNum)->saisie, FALSE); // non sign�
			break;

    case WM_COMMAND:
      switch(LOWORD(mp1))
        {
				case ID_VALIDE_SAISIE_NUM:
					{
				  BOOL	bNombreValide;

					ppSaisieNum = (PPARAM_DLG_SAISIE_NUM *)pGetEnv (hDlg);

					(*ppSaisieNum)->saisie = (GetDlgItemInt (hDlg, ID_SAISIE_NUM, &bNombreValide, FALSE));
					EndDialog(hDlg, bNombreValide);
					}
					break;

				case IDCANCEL:
				case ID_ABANDON_SAISIE_NUM:
					EndDialog(hDlg, FALSE);
					break;
				}
			break;

    case WM_DESTROY:
      bLibereEnv (hDlg);
      break;

    default:
      mres = 0;
      break;
    }
  return (mres);
  }


// -----------------------------------------------------------------------
// Nom......: dlg_saisie_num
// Objet....: saisie d'un num dans une boite de dialogue
// Entr�es..: initialisation des champs textes
// Sorties..:
// Retour...: retour_b indique qu'on a bien recupere le num
// Remarques: l'adresse de retour_recherche est passee a la WndProc lorsqu'on recoit le message
//            WM_INITDIALOG, l'adresse se trouve dans mp2
// -----------------------------------------------------------------------
BOOL dlg_saisie_num (HWND hDlg, PPARAM_DLG_SAISIE_NUM retour_saisie_num)
  {
	return LangueDialogBoxParam (MAKEINTRESOURCE(DLG_SAISIE_NUM), hDlg, dlgprocSaisieNum,(LPARAM)retour_saisie_num);
  }



// -----------------------------------------------------------------------
// Choisir le pathname d'un fichier existant � ouvrir 
// -----------------------------------------------------------------------
BOOL bDlgFichierOuvrir (HWND hwndParent, PSTR pszPathNameCourant,
	LPCTSTR pszTitre, LPCTSTR pszExtension, LPCTSTR pszAction)
  {
	BOOL bOk = FALSE;

	// File input and output (I/O) global variables. 
	OPENFILENAME OpenFileName; 
	char		szFilter[80];
	char    szDirName[MAX_PATH]; 
	LPTSTR	pszFileName;
	LPTSTR	pszTemp = szFilter;

	// cr�ation du filtre au format "Fichiers X (*.ext)\0*.ext\0"
	StrPrintFormat (szFilter, "Fichiers %s (*.%s)", pszAction, pszExtension);
	pszTemp += StrLength (szFilter) + 1;
	StrPrintFormat (pszTemp, "*.%s", pszExtension);
	StrConcatChar(pszTemp, '\0');

	// r�cup�re le path du fichier (en compl�tant � d�faut par le path courant)
	if (GetFullPathName (pszPathNameCourant, MAX_PATH, szDirName, &pszFileName) != 0)
		{
		// Ok : s�pare chemin et fichier
		if (pszFileName)
			(pszFileName-1)[0] = '\0';
		}
	else
		{
		// �chec : pas de rep
		szDirName[0]= '\0';
		}

	OpenFileName.lStructSize       = sizeof(OPENFILENAME); 
	OpenFileName.hwndOwner         = hwndParent; 
	OpenFileName.hInstance         = NULL; // pas de OFN_ENABLETEMPLATE 
	OpenFileName.lpstrFilter       = szFilter; 
	OpenFileName.lpstrCustomFilter = (LPTSTR) NULL; 
	OpenFileName.nMaxCustFilter    = 0L; 
	OpenFileName.nFilterIndex      = 1L; 
	OpenFileName.lpstrFile         = pszPathNameCourant; 
	OpenFileName.nMaxFile          = MAX_PATH; 
	OpenFileName.lpstrFileTitle    = NULL;
	OpenFileName.nMaxFileTitle     = 0;
	OpenFileName.lpstrInitialDir   = szDirName; 
	OpenFileName.lpstrTitle        = pszTitre; 
	OpenFileName.nFileOffset       = 0; 
	OpenFileName.nFileExtension    = 0; 
	OpenFileName.lpstrDefExt       = pszExtension; 
	OpenFileName.lCustData         = 0; 
//	OpenFileName.Flags             = OFN_PATHMUSTEXIST
//		| OFN_FILEMUSTEXIST | OFN_LONGNAMES | OFN_HIDEREADONLY; // OFN_SHOWHELP | 
	OpenFileName.Flags             = OFN_LONGNAMES | OFN_HIDEREADONLY; // OFN_SHOWHELP | 

	// appel de la boite de dialogue syt�me
	bOk = GetOpenFileName (&OpenFileName);
  return bOk;
  } // bDlgFichierOuvrir

// -----------------------------------------------------------------------
// Choisir le pathname d'un fichier � enregistrer
// -----------------------------------------------------------------------

BOOL bDlgFichierEnregistrerSous (HWND hwndParent, PSTR pszPathNameCourant,
	LPCTSTR pszTitre, LPCTSTR pszExtension, LPCTSTR pszAction)
  {
	BOOL bOk = FALSE;

	// File input and output (I/O) global variables. 
	OPENFILENAME OpenFileName; 
	char		szFilter[80];
	char    szDirName[MAX_PATH]; 
	LPTSTR	pszFileName;
	LPTSTR	pszTemp = szFilter;

	// cr�ation du filtre au format "Fichiers X (*.ext)\0*.ext\0"
	StrPrintFormat (szFilter, "Fichiers %s (*.%s)", pszAction, pszExtension);
	pszTemp += StrLength (szFilter) + 1;
	StrPrintFormat (pszTemp, "*.%s", pszExtension);
	StrConcatChar(pszTemp, '\0');

	// r�cup�re le path du fichier (en compl�tant � d�faut par le path courant)
	if (GetFullPathName (pszPathNameCourant, MAX_PATH, szDirName, &pszFileName) == 0)
		szDirName[0]= '\0';
	else
		(pszFileName-1)[0] = '\0';

	OpenFileName.lStructSize       = sizeof(OPENFILENAME); 
	OpenFileName.hwndOwner         = hwndParent; 
	OpenFileName.hInstance         = NULL; // pas de OFN_ENABLETEMPLATE 
	OpenFileName.lpstrFilter       = szFilter; 
	OpenFileName.lpstrCustomFilter = (LPTSTR) NULL; 
	OpenFileName.nMaxCustFilter    = 0L; 
	OpenFileName.nFilterIndex      = 1L; 
	OpenFileName.lpstrFile         = pszPathNameCourant; 
	OpenFileName.nMaxFile          = MAX_PATH; 
	OpenFileName.lpstrFileTitle    = NULL;
	OpenFileName.nMaxFileTitle     = 0;
	OpenFileName.lpstrInitialDir   = szDirName; 
	OpenFileName.lpstrTitle        = pszTitre; 
	OpenFileName.nFileOffset       = 0; 
	OpenFileName.nFileExtension    = 0; 
	OpenFileName.lpstrDefExt       = pszExtension; 
	OpenFileName.lCustData         = 0; 
	OpenFileName.Flags             = OFN_PATHMUSTEXIST
		| OFN_OVERWRITEPROMPT | OFN_LONGNAMES | OFN_HIDEREADONLY; // OFN_SHOWHELP | 

	// appel de la boite de dialogue syt�me
	bOk = GetSaveFileName (&OpenFileName);
  return bOk;
  } // bDlgFichierEnregistrerSous


//----------------------------------------------------------------------------
// fonction qui transforme un chemin pour qu'il rentre ds une boite avec (...)
//----------------------------------------------------------------------------
PSTR DlgFitPathToBox (HWND hDlg, int idStatic, PSZ lpch)
	{
	RECT rc;
	SIZE sizeText;
	int cxField;
	char chDrive;
	HDC hdc;

	// get length of static field
	::GetClientRect(::GetDlgItem(hDlg, idStatic), &rc);
	cxField = rc.right - rc.left;

	hdc = ::GetDC(hDlg);
	GetTextExtentPoint32 (hdc, lpch, StrLength (lpch), &sizeText);
	if (cxField < sizeText.cx)
		{
		chDrive = *lpch;
		// coupe les caract�res jusqu'� ce que la chaine soit assez courte
		do
			{
			do
				lpch = lpch++; // $$ ??? remplace NextChar(lpch);
			while (*(lpch+6) != '\\' && *(lpch+6) != '\0');
			GetTextExtentPoint32 (hdc, lpch, StrLength (lpch), &sizeText);
			}
		while (cxField < sizeText.cx);
		// insert header
		*lpch++ = chDrive;
		*lpch++ = ':';
		*lpch++ = '\\';
		*lpch++ = '.';
		*lpch++ = '.';
		*lpch++ = '.';
		lpch -= 6;
		}
	::ReleaseDC(hDlg, hdc);
	return (lpch);
	}

// ------------------ fin Pcsdlg.c ----------------------------



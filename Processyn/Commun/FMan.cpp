/*--------------------------------------------------------------------------+
 | Ce fichier est la propriet� de                                           |
 |                    Societ� LOGIQUE INDUSTRIE                             |
 |              Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3               |
 | Il demeure sa propriet� exclusive, et est confidentiel.                  |
 | Aucune diffusion n'est possible sans accord ecrit.                       |
 |--------------------------------------------------------------------------*/

//	FMan.c : Gestion simple de fichiers WIN32
// 28/1/97		JS	extraction � partir de FileMan.c
// 26/2/97		JS	utilisation de fonctions WIN32 au lieu des run times C pour obtenir
//								des handles de fichier et des erreurs OS + flags ouverture = flags MFC
#include "stdafx.h"
#include "Verif.h"
#include "UStr.h"
#include "MemMan.h"
#include "FMan.h"
VerifInit;


// Caract�res de fin de lignes
static const char szCrLf [] = "\r\n";
static const char szSeparateursLigne [] = "\r\f\n";

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//										Fonctions export�es
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
// Ouverture d'un fichier
BOOL CFman::bFOuvre (HF * phFile, PCSTR pszName, DWORD wFlags)
  {
	DWORD	dwAccesDemande = GENERIC_READ;	// d�faut : ac�s en lecture
	DWORD dwShare = 0;										// d�faut : pas de partage
	DWORD	dwCreation = 0;									// d�faut : ouverture ou cr�ation si n'existe pas

	// Conversion du type d'acc�s
	if (wFlags & modeReadWrite)
		dwAccesDemande = GENERIC_READ | GENERIC_WRITE;
	else
		{
		if (wFlags & modeWrite)
			dwAccesDemande = GENERIC_WRITE;
		}

	// Conversion du type de partage
	switch (wFlags & (shareExclusive|shareDenyWrite|shareDenyRead|shareDenyNone))
		{
		case shareDenyWrite:
			dwShare = FILE_SHARE_WRITE;
			break;
		case shareDenyRead:
			dwShare = FILE_SHARE_READ;
			break;
		case shareDenyNone:
			dwShare = FILE_SHARE_READ|FILE_SHARE_WRITE;
			break;
		//case shareExclusive:	break
		//default :	break
		}

	// Conversion de l'action � l'ouverture
	if (wFlags & modeCreate)
		{
		if (wFlags & modeNoTruncate)
			// Combine this value with modeCreate. If the file being created already exists, it is not truncated to 0 length. Thus the file is guaranteed to open, either as a newly created file or as an existing file. This might be useful, for example, when opening a settings file that may or may not exist already. This option applies to CStdioFile as well.
			dwCreation = OPEN_ALWAYS;
		else
			// Directs the constructor to create a new file. If the file exists already, it is truncated to 0 length.
			dwCreation = CREATE_ALWAYS;
		}
	else
		// Par d�faut pas de cr�ation si le fichier existe
		dwCreation = OPEN_EXISTING;

	// Tentative d'ouverture
	*phFile = (HF) CreateFile (pszName, dwAccesDemande, dwShare,
		(LPSECURITY_ATTRIBUTES) NULL, dwCreation, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL); 

  return *phFile != INVALID_HF;
  } // bFOuvre

//---------------------------------------------------------------------------
// Fermeture fichier et remise du handle � INVALID_HF
BOOL CFman::bFFerme (HF * phFile)
  {
  BOOL bOk = CloseHandle (* phFile);

  if (bOk)
		*phFile = INVALID_HF;

  return bOk;
  }

//---------------------------------------------------------------------------
// Teste la possibilit�s d'ouverture d'un fichier selon l'acc�s sp�cifi�
BOOL CFman::bFAcces (PCSTR pszName, DWORD wFlags)
  {
	BOOL	bOk = FALSE;
  HF		hFile = INVALID_HF;

	//Ouverture Ok ?
  if (bFOuvre (&hFile, pszName, wFlags))
		{
		// oui => fermeture Ok ?
    if (bFFerme (&hFile))
			// oui => Ok;
			bOk = TRUE;
		}

  return bOk;
  }

//---------------------------------------------------------------------------
// Efface un fichier
BOOL CFman::bFEfface (PCSTR pszName)
  {
  return DeleteFile (pszName);
  }

//---------------------------------------------------------------------------
// Ecriture fichier
// renvoie TRUE en cas d'�criture partielle
BOOL CFman::bFEcrire (HF hFile, const void * pBuff, DWORD dwTailleAEcrire, DWORD * pdwTailleEcrite)
  {
  return WriteFile (hFile, pBuff, dwTailleAEcrire, pdwTailleEcrite, (LPOVERLAPPED) NULL);
  }


//---------------------------------------------------------------------------
// Ecriture fichier
// renvoie FALSE si tout n'a pas pu �tre �crit
BOOL CFman::bFEcrireTout (HF hFile, const void * pBuff, DWORD dwTailleAEcrire)
  {
  BOOL	bOk = FALSE;
	DWORD	dwTailleEcrite;

	// �criture de la taille demand�e
  if (WriteFile (hFile, pBuff, dwTailleAEcrire, &dwTailleEcrite, (LPOVERLAPPED) NULL))
    {
		// Tout �crit ?
		if (dwTailleEcrite == dwTailleAEcrire)
			// oui => Ok
			bOk = TRUE;
		else
			// Non => erreur plus de place
			SetLastError (ERROR_HANDLE_DISK_FULL);
    }

  return bOk;
  }

//---------------------------------------------------------------------------
// Lecture fichier
// renvoie TRUE m�me si la lecture n'est que partielle
// et FALSE s'il y a eu erreur.
BOOL CFman::bFLire (HF hFile, PVOID pBuff, DWORD dwTailleALire, DWORD * pdwTailleLue)
  {
  return ReadFile (hFile, pBuff, dwTailleALire, pdwTailleLue, (LPOVERLAPPED) NULL);
  }

//---------------------------------------------------------------------------
// Lecture fichier
// renvoie FALSE si tout n'a pas pu �tre lu
BOOL CFman::bFLireTout (HF hFile, PVOID pBuff, DWORD dwTailleALire)
  {
	BOOL	bOk = FALSE;
	DWORD	dwTailleLue;

	// Lecture de la taille demand�e ?
	if (ReadFile (hFile, pBuff, dwTailleALire, &dwTailleLue, (LPOVERLAPPED) NULL))
		{
		// oui => tout a �t� lu ?
		if (dwTailleALire == dwTailleLue)
			// oui => Ok
			bOk = TRUE;
		else
			// Non => erreur plus rien � lire
			SetLastError (ERROR_HANDLE_EOF);
		}

	return bOk;
  }

//---------------------------------------------------------------------------
// Sp�cifie l'offset depuis le d�but de fichier du pointeur de lecture/�criture
// Permet de positionner le pointeur AU DELA de la fin de fichier
BOOL CFman::bFPointe (HF hFile, DWORD dwOffset)
  {
	// Pointe � l'offset demand� (partie haute � 0) � partir du d�but du fichier
	LONG dwHighOffset = 0;
	DWORD dwRes = SetFilePointer (hFile, (LONG)dwOffset, &dwHighOffset, FILE_BEGIN);

	// Renvoie TRUE si pas d'erreur
  return ((dwRes != 0xFFFFFFFF) || (GetLastError() != NO_ERROR));
  }

//---------------------------------------------------------------------------
// R�cup�re l'offset depuis le d�but de fichier du pointeur de lecture/�criture
BOOL CFman::bFValPointe (HF hFile, DWORD * pdwOffset)
  {
	BOOL	bOk = FALSE;

	// Pointe � l'offset demand� (partie haute � 0) � partir du d�but du fichier
	LONG dwHighOffset = 0;
	DWORD dwRes = SetFilePointer (hFile, 0, &dwHighOffset, FILE_CURRENT);

	// pas d'errreur ?
	if ((dwRes != 0xFFFFFFFF) || (GetLastError() != NO_ERROR))
		{
		// oui => overflow sur le r�sultat ?
		if (dwHighOffset)
			SetLastError (ERROR_OUTOFMEMORY);
		else
			{
			bOk = TRUE;
			* pdwOffset = dwRes;
			}
		}
	// Renvoie TRUE si pas d'erreur
  return bOk;
  }

//---------------------------------------------------------------------------
// Pointe � la fin du fichier
BOOL CFman::bFPointeFin (HF hFile)
  {
	DWORD dwRes = SetFilePointer (hFile, 0, NULL, FILE_END);

	// Renvoie TRUE si pas d'erreur
  return (dwRes != 0xFFFFFFFF);
  }

//---------------------------------------------------------------------------
// R�cup�re la longueur du fichier
BOOL CFman::bFLongueur (HF hFile, DWORD * pwSize)
  {
	DWORD dwRes = GetFileSize (hFile, NULL);

	// pas d'erreur ?
	if (dwRes != 0xFFFFFFFF)
		// oui => renvoie la taille lue
		* pwSize = dwRes;

	// Renvoie TRUE si pas d'erreur
  return (dwRes != 0xFFFFFFFF);
  }

//---------------------------------------------------------------------------
// Lecture s�quentielle d'un fichier texte
BOOL CFman::bFLireLn (HF hFile, PSTR pszDest, DWORD dwTailleDest, PDWORD pdwNbOctetsLus)
  {
	BOOL		bOk = FALSE;

	// Chaine lue vide par d�faut
	pszDest[0] = '\0';

	// place pour au moins un car dans le buffer de lecture ?
  if (dwTailleDest > 1)
    {
		#define TAILLE_BUF_DEFAUT 512
		char		pszDefaut [TAILLE_BUF_DEFAUT];
		char *	pszBuf = pszDefaut;
		DWORD		dwTailleBuf = dwTailleDest+2; // (cars chaine + Cr + Lf + 0)
		DWORD		uNbLus = 0;

		// Alloue un buffer plus grand que celui par d�faut si n�cessaire
		if (dwTailleBuf > TAILLE_BUF_DEFAUT)
			pszBuf = (char *)pMemAlloue (dwTailleBuf);

		// r�cup�ration pointeur de lecture et lecture du buffer ok ?
			DWORD		dwSeekInitial;
		if (bFValPointe (hFile, &dwSeekInitial) &&
			bFLire (hFile, pszBuf, dwTailleBuf-1, &uNbLus))
			{
			// oui => trouve un caract�re de fin de ligne ?
			UINT		uNCarFin;

			// Termine par un 0 la chaine lue
			pszBuf [uNbLus] = '\0';
			uNCarFin = StrSearchChar (pszBuf, '\r');
			if (uNCarFin == STR_NOT_FOUND)
				{
				uNCarFin = StrSearchChar (pszBuf, '\f');
				if (uNCarFin == STR_NOT_FOUND)
					{
					uNCarFin = StrSearchChar (pszBuf, '\n');
					if (uNCarFin == STR_NOT_FOUND)
						{
						// non => une fin de fichier a �t� rencontr�e ?
						if (uNbLus != (dwTailleBuf - 1))
							{
							// oui => quelque chose a �t� lu ?
							if (uNbLus)
								// oui => Texte termin� par fin de fichier
								uNCarFin = uNbLus;
							else
								// non => erreur fin de fichier
								SetLastError (ERROR_HANDLE_EOF);
							}
						}
					}
				}
			// caract�re de fin trouv� ?
			if (uNCarFin != STR_NOT_FOUND)
				{
				// oui => chaine dest suffisamment grande ?
				if (dwTailleDest > uNCarFin)
					{
					// oui => on recopie
					MemCopy (pszDest, pszBuf, uNCarFin);
					pszDest[uNCarFin]  = '\0';
					bOk = TRUE;
					}
				else
					// non => erreur overflow
			    SetLastError (ERROR_OUTOFMEMORY);

				// Fichier pointe sur la chaine suivante :
				// Calcule le nombre d'octets parcourus pour cette ligne
				DWORD dwNbOctetsLus = uNCarFin+1;
				if ((pszBuf [uNCarFin] != pszBuf [uNCarFin+1]) &&
					(StrCharIsInStr (pszBuf [uNCarFin+1], szSeparateursLigne)))
					{
					dwNbOctetsLus = uNCarFin+2;
					}
				// Met � jour le cas �ch�ant le nombre d'octets lus
				if (pdwNbOctetsLus != NULL)
					{
					*pdwNbOctetsLus = dwNbOctetsLus;
					}
				// pointe dans le fichier au d�but de la ligne suivante
				bFPointe (hFile, dwSeekInitial + dwNbOctetsLus);
				} // if (uNCarFin != STR_NOT_FOUND)
			else
				{
				// Met � jour le cas �ch�ant le nombre d'octets lus
				if (pdwNbOctetsLus != NULL)
					{
					*pdwNbOctetsLus = uNbLus;
					}
				}
			} // if (bFValPointe (hFile...

		// Libere le buffer �ventuellement allou�
		if (pszBuf != pszDefaut)
			MemLibere ((PVOID *)(&pszBuf));
    } // if (dwTailleDest > 1)
  else
    SetLastError (ERROR_OUTOFMEMORY);

  return bOk;
  }

//------------------------------------------------------------
// Ecriture s�quentielle d'une chaine de caract�re
BOOL CFman::bFEcrireSz (HF hFile, PCSTR pszBufferLn)
	{
	return bFEcrireTout (hFile, pszBufferLn, StrLength (pszBufferLn));
	}

//----------------------------------------------------------------
// Ecriture s�quentielle d'une chaine de caract�re puis d'un Cr Lf
BOOL CFman::bFEcrireSzLn (HF hFile, PCSTR pszBufferLn)
  {
  BOOL	bOk = FALSE;
	
	if (bFEcrireTout (hFile, pszBufferLn, StrLength (pszBufferLn)) &&
		bFEcrireTout (hFile, szCrLf, StrLength (szCrLf)))
		bOk = TRUE;

  return bOk;
  }



#include "stdafx.h"

#include <io.h>          // open(), close(), read(), write(), tell(), lseek()
#include <stdio.h>       // SEEK_      constant definitions
#include <fcntl.h>       // O_         constant definitions
#include <sys\types.h>
#include <sys\stat.h>    // S_         constant definitions

#include <string.h>      // memcpy () , memmove ()

#include "std.h"         //
#include "BufMan.h"					// buffers
#include "MemMan.h"
//#include "implode.h"     // implode (), explode ()

// --------------------------------------------------------------------------
#include "PmBmp.h"

#define  SIGNATURE_BMP_WINDOW  0x4d42  // "BM"

#define  SIZE_IO_BUFF  65535U

// --------------------------------------------------------------------------
#define  PACKED_TYPE  0x4650  // "PK"
//
#define  WINDOW_NO_COMPRESSION  0
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Data globales pour sauvegarde BMP en COMPACTE
// --------------------------------------------------------------------------
// ----------------------------------------------
#define ZIP_IMPLODE_SIZE_WORK_BUFF  35256U
#define ZIP_EXPLODE_SIZE_WORK_BUFF  12574U
//
#define ZIP_TYPE_BINARY             CMP_BINARY
#define ZIP_TYPE_ASCII              CMP_ASCII
//
#define ZIP_DICTIONARY_SIZE_SMALL   1024U
#define ZIP_DICTIONARY_SIZE_MEDIUM  2048U
#define ZIP_DICTIONARY_SIZE_LARGE   4096U
// ----------------------------------------------
static int         iFileGlob      = -1;
static HDC         hpsGlob        = (HDC) NULL;
static BITMAPINFO *pbmiGlob       = NULL;
static USHORT      usSizeLineGlob = 0;
static USHORT      usNbLinesGlob  = 0;
static USHORT      usLineGlob     = 0;
static USHORT      usSizeRestGlob = 0;
static BYTE       *pbRestDataGlob = NULL;

// --------------------------------------------------------------------------
static BOOL BmpCreateMemSpace (HDC hdcComp, HDC *hdc)
  {
  BOOL  bOk = FALSE;

	(*hdc) = CreateCompatibleDC(hdcComp);
	if (*hdc)
		bOk = TRUE;
  return (bOk);
  }

// --------------------------------------------------------------------------
static void BmpDestroyMemSpace (HDC hdc)
  {
  DeleteDC (hdc);
  }

// --------------------------------------------------------------------------
static BOOL BmpCreate (BITMAP *pbmp, HBITMAP *phbm)
  {
  BOOL  bOk = FALSE;

  (*phbm) = CreateBitmap (pbmp->bmWidth, pbmp->bmHeight, pbmp->bmPlanes,
		pbmp->bmBitsPixel, pbmp->bmBits);
  if (*phbm)
    bOk = TRUE;
  return (bOk);
  }

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
void BmpDestroy (HBITMAP hbm)
  {
  DeleteObject (hbm);
  }

// --------------------------------------------------------------------------
BOOL BmpSize (HBITMAP hbm, PLONG plCx, PLONG plCy)
  {
  BOOL bOk;
	BITMAP bmp;
  
	bOk = GetObject (hbm, sizeof(BITMAP), &bmp);
  if (bOk)
    {
    (*plCx) = bmp.bmWidth;
    (*plCy) = bmp.bmHeight;
    }
  return (bOk);
  }

// --------------------------------------------------------------------------
HBITMAP BmpFromRclScreen (PRECT prect)
  {
	HBITMAP hbmpRet = NULL;
	HBITMAP hbmpOld = NULL;
	HDC			hdcEcran = NULL;
	HDC			hdcMem = NULL;
	BOOL		bOk = FALSE;

	// Cr�e un DC normal et un DC en m�moire pour l'�cran.
	// Le DC normal fournit la photo de l'�cran. Le DC m�moire
	// contient la copie de cette photo dans le bitmap associ�
	hdcEcran = CreateDC ("DISPLAY", NULL, NULL, NULL); 
	hdcMem = CreateCompatibleDC (hdcEcran); 

	if (hdcEcran && hdcMem)
		{
		// Cr�e un bitmap compatible pour le DC �cran
		hbmpRet = CreateCompatibleBitmap(hdcEcran, 
			prect->right - prect->left, prect->bottom - prect->top); 

		if (hbmpRet != NULL)
			{
			// S�lectionne le bitmap dans le DC compatible
			hbmpOld = (HBITMAP)SelectObject(hdcMem, hbmpRet);
			if (hbmpOld)
				{
				// copie de l'image de l'�cran dans le bitmap du DC compatible Ok ?
				if (BitBlt(hdcMem, 
					0,0, prect->right - prect->left, prect->bottom - prect->top, // x, y, larg, haut dest
					hdcEcran, 
					prect->left, prect->top, // x, y source
					SRCCOPY))
					bOk = TRUE;

				// d�selectionne le bitmap du DC Mem
				SelectObject(hdcMem, hbmpOld);
				} // if (hbmpOld)
			} // if (hbmpRet != NULL)

		// lib�re les DC cr��s
		DeleteDC(hdcEcran);
		DeleteDC(hdcMem);
		} // if (hdcEcran && hdcMem)

	// lib�re le bitmap image si tout ne s'est pas bien pass�
	if (!bOk && hbmpRet)
		{
		DeleteObject (hbmpRet);
		hbmpRet = NULL;
		}

	return hbmpRet;

	/*
  HDC              hdc;
  HDC              hdc;
  LONG             alBmpFormats [2];
  BITMAPINFOHEADER bmp;
  HBITMAP          hbm;
  HDC              hpsScreen;
  POINT           aptl [3];

  hbm = NULL;
  if (BmpCreateMemSpace (NULL, &hdc))
    {
    GpiQueryDeviceBitmapFormats (hdc, 2L, alBmpFormats);
    bmp.biSize     = sizeof (bmp);
    bmp.biWidth    = (LONG) (prect->right - prect->left);
    bmp.biHeight        = (LONG) (prect->left   - prect->bottom);
    bmp.biPlanes   = (USHORT) alBmpFormats [0];
    bmp.biBitCount = (USHORT) alBmpFormats [1];//$$autres champs ?
    if (BmpCreate (&bmp, &hbm))
      {
      SelectObject (hdc, hbm);
      //
      hpsScreen = WinGetScreenPS (HWND_DESKTOP);
      aptl [0].x = 0L;
      aptl [0].y = 0L;
      aptl [1].x = bmp.biWidth;
      aptl [1].y = bmp.biHeight;
      aptl [2].x = prect->left;
      aptl [2].y = prect->bottom;
      WinLockVisRegions (HWND_DESKTOP, TRUE);
      //
      GpiBitBlt (hdc, hpsScreen, 3L, aptl, ROP_SRCCOPY, BBO_IGNORE);
      //
      WinLockVisRegions (HWND_DESKTOP, FALSE);
      ::ReleaseDC (hpsScreen); //$$
      //
      }
    BmpDestroyMemSpace (hdc);
    }
  return (hbm);
	*/
  }

// --------------------------------------------------------------------------
HBITMAP BmpFromBmp (HBITMAP hbmSrc)
	{
	//$$ utilis� par l'utilitaire de copie d'�cran : transfert bitmap presse papier vers zone client
	return NULL;
	}
/* 
  {
	hbmSrc = hbmSrc;
	return NULL;

  HDC              hdcSrc;
  HDC              hdcDst;
  HDC              hpsDst;
  BITMAPINFOHEADER bmp;
  HBITMAP          hbmDst;
  POINT           aptl [3];

  hbmDst = NULL;
  if (BmpCreateMemSpace (NULL, &hdcSrc))
    {
    if (BmpCreateMemSpace (NULL, &hdcDst))
      {
      if (GpiQueryBitmapParameters (hbmSrc, &bmp))
        {
        if (BmpCreate (&bmp, &hbmDst))
          {
          SelectObject (hdcSrc, hbmSrc);
          SelectObject (hpsDst, hbmDst);
          aptl [0].x = 0L;
          aptl [0].y = 0L;
          aptl [1].x = bmp.biWidth;
          aptl [1].y = bmp.biHeight;
          aptl [2].x = 0L;
          aptl [2].y = 0L;
          GpiBitBlt (hpsDst, hdcSrc, 3L, aptl, ROP_SRCCOPY, BBO_IGNORE);
          }
        }
      BmpDestroyMemSpace (hdcDst);
      }
    BmpDestroyMemSpace (hdcSrc);
    }
  return (hbmDst);
	//$$
  }
*/
// --------------------------------------------------------------------------
BOOL BmpToRclSpace (HDC hdc, HBITMAP hbm, RECT * prect, RECT * prectRefresh)
  {
  BOOL             bOk;
  HDC              hdcSrc;
  BITMAP					 bmp;
  HBITMAP          hbmSrc;
  HBITMAP          hbmDst;
  
  bOk = FALSE;
  hbmSrc = BmpFromBmp (hbm);
  if (hbmSrc != NULL)
    {
    if (BmpCreateMemSpace (hdc, &hdcSrc))
      {
      if (GetObject (hbmSrc, sizeof(bmp), &bmp))
        {
        if (BmpCreate (&bmp, &hbmDst))
          {
          SelectObject (hdcSrc, hbmSrc);
          SelectObject (hdc, hbmDst);
          // $$ � v�rifier (types et dimensions)+rectRefresh
					BitBlt (
						hdc, // DC dest
						prect->left, prect->top, // x,y, dest
						bmp.bmWidth, bmp.bmHeight, // largeur, hauteur dest
						hdcSrc, // DC source
						0,0, // x,y source
						SRCCOPY );// ROP
          //aptl [1].x = prect->right;
          //aptl [1].y = prect->top;
          //aptl [2].x = 0L;
          //aptl [2].y = 0L;
          //aptl [3].x = bmp.bmWidth;
          //aptl [3].y = bmp.bmHeight;
          //GpiBitBlt (, hdcSrc, 4L, aptl, ROP_SRCCOPY, BBO_IGNORE);
          BmpDestroy (hbmDst); // $$ destruction objet s�lectionn�
          }
        }
      BmpDestroyMemSpace (hdcSrc);
      }
    BmpDestroy (hbmSrc);
    }
  return (bOk);
  }

// --------------------------------------------------------------------------
BOOL BmpToSpace (HDC hdc, HBITMAP hbm, POINT * ppointOrigine, RECT * prectRefresh)
  {
  BOOL  bOk;
  LONG  lCx;
  LONG  lCy;
  RECT rect;

  bOk = FALSE;
  if (BmpSize (hbm, &lCx, &lCy))
    {
    rect.left   = ppointOrigine->x;
    rect.bottom = ppointOrigine->y;
    rect.right  = rect.left   + lCx;
    rect.top    = rect.bottom + lCy;
    bOk = BmpToRclSpace (hdc, hbm, &rect, prectRefresh);
    }

  return (bOk);
  }
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
static void SavePackedBitmapBits (int iFile, HDC hdc, BITMAPINFO *pbmi, USHORT usNbLines, USHORT usSizeLine)
  {
	iFile = iFile;
	hdc = hdc;
	pbmi = pbmi;
	usNbLines = usNbLines;
	usSizeLine = usSizeLine;

  // $$ non utilis� dans pcsconf
	/*
  char            *pchZipWorkBuff;
  UINT  usZipType;
  UINT  usZipDictionarySize;
  unsigned int        uiError;

  pchZipWorkBuff = pMemAlloue (ZIP_IMPLODE_SIZE_WORK_BUFF);
  if (pchZipWorkBuff != NULL)
    {
    iFileGlob      = iFile;
    hpsGlob        = hdc;
    pbmiGlob       = pbmi;
    usSizeLineGlob = usSizeLine;
    usNbLinesGlob  = usNbLines;
    usLineGlob     = 0;
    usSizeRestGlob = 0;
    pbRestDataGlob  = pMemAlloue (2 * usSizeLine);
    if (pbRestDataGlob != NULL)
      {
      usZipType = ZIP_TYPE_BINARY;
      usZipDictionarySize = ZIP_DICTIONARY_SIZE_LARGE;
      uiError = implode (ReadBitmapBitsToPack,
                         WritePackedDataToFile,
                         pchZipWorkBuff,
                         &usZipType,
                         &usZipDictionarySize);
      if (uiError != CMP_NO_ERROR)
        {
        Beep (200, 100);
        }
      MemLibere (&pbRestDataGlob);
      }
    MemLibere (&pchZipWorkBuff);
    }
		*/
  }
// --------------------------------------------------------------------------
static unsigned   ReadBitmapBitsToPack (char  *buffer, UINT  *size)
  {
	buffer = buffer;
	size = size;
	return 0;
	/* $$ non utilis� dans pcsconf
  USHORT       usSizeFreeBuffDst;
  BYTE        *pbBuffDst;
  USHORT       usSizeCpy;
  USHORT       usNbLinesToRead;
  USHORT       usNbLinesRead;
  unsigned int usSizeRead;

  usSizeFreeBuffDst = (*size);
  pbBuffDst         = buffer;
  usSizeRead        = 0;
  //
  if (usSizeFreeBuffDst > usSizeRestGlob)
    {
    usSizeCpy = usSizeRestGlob;
    }
  else
    {
    usSizeCpy = usSizeFreeBuffDst;
    }
  memcpy (pbBuffDst, pbRestDataGlob, usSizeCpy);
  usSizeRead        = usSizeRead        + usSizeCpy;
  usSizeFreeBuffDst = usSizeFreeBuffDst - usSizeCpy;
  pbBuffDst         = pbBuffDst         + usSizeCpy;
  usSizeRestGlob    = usSizeRestGlob    - usSizeCpy;
  memmove (pbRestDataGlob, pbRestDataGlob + usSizeCpy, usSizeRestGlob);
  //
  if (usSizeFreeBuffDst > 0)
    {
    usNbLinesToRead = usSizeFreeBuffDst / usSizeLineGlob;
    if (usNbLinesToRead > usNbLinesGlob)
      {
      usNbLinesToRead = usNbLinesGlob;
      }
    usNbLinesRead = (USHORT) GpiQueryBitmapBits (hpsGlob, usLineGlob, usNbLinesToRead, pbBuffDst, pbmiGlob);
    usLineGlob    = usLineGlob     + usNbLinesRead;
    usNbLinesGlob = usNbLinesGlob  - usNbLinesRead;
    usSizeCpy     = usSizeLineGlob * usNbLinesRead;
    usSizeRead        = usSizeRead        + usSizeCpy;
    usSizeFreeBuffDst = usSizeFreeBuffDst - usSizeCpy;
    pbBuffDst         = pbBuffDst         + usSizeCpy;
    //
    if (usSizeFreeBuffDst > 0)
      {
      if (usNbLinesGlob > 0)
        {
        if (GpiQueryBitmapBits (hpsGlob, usLineGlob, 1L, pbRestDataGlob, pbmiGlob) == 1L)
          {
          usLineGlob++;
          usNbLinesGlob--;
          usSizeRestGlob = usSizeLineGlob;
          memcpy (pbBuffDst, pbRestDataGlob, usSizeFreeBuffDst);
          usSizeRead     = usSizeRead     + usSizeFreeBuffDst;
          usSizeRestGlob = usSizeRestGlob - usSizeFreeBuffDst;
          memmove (pbRestDataGlob, pbRestDataGlob + usSizeFreeBuffDst, usSizeRestGlob);
          }
        }
      }
    }
  return (usSizeRead);
	*/
  }
// --------------------------------------------------------------------------
static void WritePackedDataToFile (char  *buffer, UINT  *size)
  {
  write (iFileGlob, buffer, (*size));
  }

// --------------------------------------------------------------------------
static void LoadPackedBitmapBits (int iFile, HDC hdc, BITMAPINFO *pbmi, USHORT usNbLines, USHORT usSizeLine)
  {
	/* $$
  char  *pchZipWorkBuff;
  unsigned int uiError;

  pchZipWorkBuff = pMemAlloue (ZIP_EXPLODE_SIZE_WORK_BUFF);
  if (pchZipWorkBuff != NULL)
    {
    iFileGlob      = iFile;
    hpsGlob        = hdc;
    pbmiGlob       = pbmi;
    usSizeLineGlob = usSizeLine;
    usNbLinesGlob  = usNbLines;
    usLineGlob     = 0;
    usSizeRestGlob = 0;
    pbRestDataGlob  = pMemAlloue (usSizeLine);
    if (pbRestDataGlob != NULL)
      {
      uiError = explode (ReadPackedDataFromFile, WriteBitmapBitsToBmp, pchZipWorkBuff);
      if (uiError != CMP_NO_ERROR)
        {
        Beep (200, 100);
        }
      MemLibere (&pbRestDataGlob);
      }
    MemLibere (&pchZipWorkBuff);
    }
		*/
  }
// --------------------------------------------------------------------------
static unsigned   ReadPackedDataFromFile  (char  *buffer, UINT  *size)
  {
  unsigned int usSizeRead;

  usSizeRead = read (iFileGlob, buffer, (*size));

  return (usSizeRead);
  }
// --------------------------------------------------------------------------
/*
static void WriteBitmapBitsToBmp (char  *buffer, UINT  *size)
  {
  USHORT  usSizeBuffSrc;
  BYTE   *pbBuffSrc;
  USHORT  usSizeFreeBuffDst;
  BYTE   *pbBuffDst;
  USHORT  usSizeToCpy;
  USHORT  usNbLinesToWrite;
  USHORT  usSizeLinesToWrite;

  usSizeBuffSrc     = (*size);
  pbBuffSrc         = buffer;
  usSizeFreeBuffDst = usSizeLineGlob - usSizeRestGlob;
  pbBuffDst         = pbRestDataGlob + usSizeRestGlob;
  //
  if (usSizeBuffSrc > usSizeFreeBuffDst)
    {
    usSizeToCpy = usSizeFreeBuffDst;
    }
  else
    {
    usSizeToCpy = usSizeBuffSrc;
    }
  memcpy (pbBuffDst, pbBuffSrc, usSizeToCpy);
  pbBuffDst         = pbBuffDst         + usSizeToCpy;
  usSizeRestGlob    = usSizeRestGlob    + usSizeToCpy;
  usSizeFreeBuffDst = usSizeFreeBuffDst - usSizeToCpy;
  pbBuffSrc         = pbBuffSrc         + usSizeToCpy;
  usSizeBuffSrc     = usSizeBuffSrc     - usSizeToCpy;
  //
  if (usSizeFreeBuffDst == 0)
    {
    usSizeRestGlob = 0;
    GpiSetBitmapBits (hpsGlob, usLineGlob, 1, pbRestDataGlob, pbmiGlob);
    usLineGlob++;
    //
    usNbLinesToWrite   = usSizeBuffSrc    / usSizeLineGlob;
    usSizeLinesToWrite = usNbLinesToWrite * usSizeLineGlob;
    GpiSetBitmapBits (hpsGlob, usLineGlob, usNbLinesToWrite, pbBuffSrc, pbmiGlob);
    usLineGlob = usLineGlob + usNbLinesToWrite;
    pbBuffSrc     = pbBuffSrc     + usSizeLinesToWrite;
    usSizeBuffSrc = usSizeBuffSrc - usSizeLinesToWrite;
    //
    memcpy (pbRestDataGlob, pbBuffSrc, usSizeBuffSrc);
    usSizeRestGlob = usSizeBuffSrc;
    }
  } // WriteBitmapBitsToBmp
*/
// --------------------------------------------------------------------------
static PBITMAPINFO CreeBitmapInfo (HWND hwnd, HBITMAP hBmp)
	{ 
	BITMAP bmp; 
	PBITMAPINFO pbmi; 
	DWORD    cClrBits; 

	// r�cup�re hauteur, largeur et format de couleurs du bitmap
	if (!GetObject(hBmp, sizeof(BITMAP), (LPSTR)&bmp))
		return NULL;

	// convertit le format de couleurs en un compte de bits
	cClrBits = (bmp.bmPlanes * bmp.bmBitsPixel); 

	if (cClrBits == 1) 
		cClrBits = 1; 
	else if (cClrBits <= 4) 
		cClrBits = 4; 
	else if (cClrBits <= 8) 
		cClrBits = 8; 
	else if (cClrBits <= 16) 
		cClrBits = 16; 
	else if (cClrBits <= 24) 
		cClrBits = 24; 
	else 
		cClrBits = 32; 

	// alloue la m�moire pour une structure BITMAPINFO
	// (cette structure contient un BITMAPINFOHEADER suivi d'un tableau de RGBQUAD)
	if (cClrBits != 24) 
		pbmi = (PBITMAPINFO) LocalAlloc(LPTR, sizeof(BITMAPINFOHEADER) + 
			sizeof(RGBQUAD) * (2 << cClrBits)); 
	// il n'y a pas de tableau de RGBQUAD pour le format 24-bit-per-pixel.
	else 
		pbmi = (PBITMAPINFO) LocalAlloc(LPTR, sizeof(BITMAPINFOHEADER)); 

	// initialise la structure BITMAPINFO
	pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
	pbmi->bmiHeader.biWidth = bmp.bmWidth; 
	pbmi->bmiHeader.biHeight = bmp.bmHeight; 
	pbmi->bmiHeader.biPlanes = bmp.bmPlanes; 
	pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel; 
	if (cClrBits < 24) 
		pbmi->bmiHeader.biClrUsed = 2 << cClrBits; 

	// si le bitmap n'est pas compress�, sp�cifie le flag BI_RGB
	pbmi->bmiHeader.biCompression = BI_RGB; 

	// calcule biSizeImage, le nombre d'octets du tableau d'indices
	pbmi->bmiHeader.biSizeImage = (pbmi->bmiHeader.biWidth + 7) /8 
		* pbmi->bmiHeader.biHeight * cClrBits; 

	// biClrImportant � 0, indiquant que toutes les couleurs device sont importantes.
	pbmi->bmiHeader.biClrImportant = 0; 

	return pbmi; 
	} // CreeBitmapInfo

static void EnregistreBMP (HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi, 
  HBITMAP hBMP, HDC hDC) 
	{ 
	HANDLE hf;                  // handle fichier
	BITMAPFILEHEADER hdr;       // header fichier bitmap
	PBITMAPINFOHEADER pbih;     // info-header bitmap
	LPBYTE lpBits;              // pointeur m�moire
	DWORD dwTotal;              // Nb total octets
	DWORD dwTmp; 
//$$ v�rifier r�sultats write ?

	pbih = (PBITMAPINFOHEADER) pbi; 
	lpBits = (LPBYTE) GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);
	if (!lpBits)
		return; //$$ r�gles d'�criture

	// r�cup�re la table des couleurs (tableau de RGBQUAD) et les bits
	// (tableau d'indices de palette) du DIB
	if (GetDIBits(hDC, hBMP, 0, pbih->biHeight, lpBits, pbi, DIB_RGB_COLORS))
		{
		// cr�e le fichier .BMP 
		hf = CreateFile(pszFile, GENERIC_READ | GENERIC_WRITE, (DWORD) 0, 
			(LPSECURITY_ATTRIBUTES) NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL); 

		if (hf != INVALID_HANDLE_VALUE) 
			{
			hdr.bfType = SIGNATURE_BMP_WINDOW;        // 0x42 = "B" 0x4d = "M"

			// calcule la taille globale du fichier
			hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + 
				pbih->biSize + pbih->biClrUsed * sizeof(RGBQUAD) + pbih->biSizeImage); 

			hdr.bfReserved1 = 0; 
			hdr.bfReserved2 = 0; 

			// calcule l'offset des indices de couleur
			hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) + 
				pbih->biSize + pbih->biClrUsed * sizeof (RGBQUAD); 

			// copie le BITMAPFILEHEADER dans le fichier .BMP
			if (WriteFile(hf, (LPVOID) &hdr, sizeof(BITMAPFILEHEADER), &dwTmp, (LPOVERLAPPED) NULL)) 
				{
				// copie le BITMAPINFOHEADER et le tableau de RGBQUAD dans le fichier
				if (WriteFile(hf, (LPVOID) pbih, sizeof(BITMAPINFOHEADER) 
					+ pbih->biClrUsed * sizeof (RGBQUAD), &dwTmp, (LPOVERLAPPED) NULL)) 
					{
					// le tableau d'indices de couleur dans le fichier .BMP
					dwTotal = pbih->biSizeImage; 
					WriteFile(hf, lpBits, dwTotal, &dwTmp, (LPOVERLAPPED) NULL); 
					//if (WriteFile(hf, lpBits, dwTotal, &dwTmp, (LPOVERLAPPED) NULL)); $$ si erreur
					}
				}

			// ferme le fichier .BMP
			CloseHandle(hf);
			}
		} // if (GetDIBits...

	// Lib�re la m�moire
	GlobalFree((HGLOBAL)lpBits);
	} // EnregistreBMP
 
// --------------------------------------------------------------------------
BOOL BmpToFile (HBITMAP hbm, PSTR pszFile, USHORT usFileFormat)
  {
  BOOL                    bOk = FALSE; // $$
	/*
  HDC                     hdc;
  HDC                     hdc;
  BITMAPINFOHEADER        bmp;
  USHORT                  usNbColor;
  USHORT                  usSizeColorTable;
  USHORT                  usSizeBmi;
  BITMAPINFO             *pbmi;
  USHORT                  usSizeLine;
  USHORT                  usNbLines;
  //
  USHORT                  usSizeHdr;
  void                   *ptrHdr;
  //
  BITMAPARRAYFILEHEADER  *pbafh;
  BITMAPFILEHEADER       *pbfh;
  BITMAPFILEHEADER			 *pWndfh;
  USHORT                  usClr;
  //
  USHORT                 usNbLinesToRead;
  USHORT                 usNbLinesRead;
  USHORT                 usLine;
  BYTE                  *aPel;
  int                    iFile;

  bOk = FALSE;
  if (BmpCreateMemSpace (NULL, &hdc))
    {
    if (SelectObject (hdc, hbm) != (HBITMAP) BMB_ERROR)
      {
      if (GpiQueryBitmapParameters (hbm, &bmp))
        {
        usNbColor = 1 << bmp.biBitCount;
        usSizeColorTable = usNbColor * sizeof (RGB);
        usSizeBmi = sizeof (BITMAPINFOHEADER) + usSizeColorTable;
        pbmi = pMemAlloue (usSizeBmi);
        if (pbmi != NULL)
          {
          memcpy (pbmi, &bmp, sizeof (BITMAPINFOHEADER));
          if (GpiQueryBitmapBits (hdc, 0L, 0L, NULL, pbmi) != BMB_ERROR)
            {
            // --------------------- Code rajout� pour OS/2 V.2.1,
            //                       car la structure pbmi est modifi�e (.cy)
            //
            memcpy (pbmi, &bmp, sizeof (BITMAPINFOHEADER));
            //
            usNbLines = pbmi->cy * pbmi->cPlanes;
            usSizeLine = (USHORT) (((((ULONG) pbmi->cBitCount * (ULONG) pbmi->cx) + 31L) / 32L) * 4L);
            //
            switch (usFileFormat)
              {
              case BMP_FILE_FORMAT_PM12:
                usSizeHdr = sizeof (BITMAPARRAYFILEHEADER) + (usNbColor * sizeof (RGB));
                ptrHdr = pMemAlloue (usSizeHdr);
                if (ptrHdr != NULL)
                  {
                  pbafh = ptrHdr;
                  pbafh->usType       = BFT_BITMAPARRAY;
                  pbafh->cbSize       = sizeof (BITMAPARRAYFILEHEADER);
                  pbafh->offNext      = 0L;
                  pbafh->cxDisplay    = 0;
                  pbafh->cyDisplay    = 0;
                  pbafh->bfh.usType   = BFT_BMAP;
                  pbafh->bfh.cbSize   = sizeof (BITMAPFILEHEADER);
                  pbafh->bfh.xHotspot = 0;
                  pbafh->bfh.yHotspot = 0;
                  pbafh->bfh.offBits  = usSizeHdr;
                  memcpy (&(pbafh->bfh.bmp), pbmi, usSizeBmi);
                  bOk = TRUE;
                  }
                break;
              case BMP_FILE_FORMAT_PM11:
                usSizeHdr = sizeof (BITMAPFILEHEADER) + (usNbColor * sizeof (RGB));
                ptrHdr = pMemAlloue (usSizeHdr);
                if (ptrHdr != NULL)
                  {
                  pbfh = ptrHdr;
                  pbfh->usType   = BFT_BMAP;
                  pbfh->cbSize   = sizeof (BITMAPFILEHEADER);
                  pbfh->xHotspot = 0;
                  pbfh->yHotspot = 0;
                  pbfh->offBits  = usSizeHdr;
                  memcpy (&(pbfh->bmp), pbmi, usSizeBmi);
                  bOk = TRUE;
                  }
                break;

							case BMP_FILE_FORMAT_WINDOW:
                usSizeHdr = sizeof (BITMAPFILEHEADER) + (usNbColor * sizeof (RGBQUAD));
                ptrHdr = pMemAlloue (usSizeHdr);
                if (ptrHdr != NULL)
                  {
                  pWndfh = ptrHdr;
                  pWndfh->bfType                                 = 0x4d42; // "BM"
                  pWndfh->bfSize                                 = (usSizeHdr + (usNbLines * usSizeLine)) / 4;
                  pWndfh->bfReserved1                            = 0;
                  pWndfh->bfReserved2                            = 0;
                  pWndfh->bfOffBits                              = usSizeHdr;
                  pWndfh->bfBitmapInfo.bmiHeader.biSize          = sizeof (BITMAPINFOHEADER);
                  pWndfh->bfBitmapInfo.bmiHeader.biWidth         = pbmi->cx;
                  pWndfh->bfBitmapInfo.bmiHeader.biHeight        = pbmi->cy;
                  pWndfh->bfBitmapInfo.bmiHeader.biPlanes        = pbmi->cPlanes;
                  pWndfh->bfBitmapInfo.bmiHeader.biBitCount      = pbmi->cBitCount;
                  pWndfh->bfBitmapInfo.bmiHeader.biCompression   = WINDOW_NO_COMPRESSION;
                  pWndfh->bfBitmapInfo.bmiHeader.biSizeImage     = usNbLines * usSizeLine;
                  pWndfh->bfBitmapInfo.bmiHeader.biXPelsPerMeter = 0;
                  pWndfh->bfBitmapInfo.bmiHeader.biYPelsPerMeter = 0;
                  pWndfh->bfBitmapInfo.bmiHeader.biClrUsed       = 0;
                  pWndfh->bfBitmapInfo.bmiHeader.biClrImportant  = 0;
                  for (usClr = 0; usClr < usNbColor; usClr++)
                    {
                    pWndfh->bfBitmapInfo.bmiColors [usClr].rgbBlue     = pbmi->argbColor [usClr].bBlue;
                    pWndfh->bfBitmapInfo.bmiColors [usClr].rgbGreen    = pbmi->argbColor [usClr].bGreen;
                    pWndfh->bfBitmapInfo.bmiColors [usClr].rgbRed      = pbmi->argbColor [usClr].bRed;
                    pWndfh->bfBitmapInfo.bmiColors [usClr].rgbReserved = 0;
                    }
                  bOk = TRUE;
                  }
                break;

							case BMP_FILE_FORMAT_PACKED:
                usSizeHdr = sizeof (BITMAPFILEHEADER) + (usNbColor * sizeof (RGB));
                ptrHdr = pMemAlloue (usSizeHdr);
                if (ptrHdr != NULL)
                  {
                  pbfh = ptrHdr;
                  pbfh->usType   = PACKED_TYPE;
                  pbfh->cbSize   = 0;
                  pbfh->xHotspot = 0;
                  pbfh->yHotspot = 0;
                  pbfh->offBits  = usSizeHdr;
                  memcpy (&(pbfh->bmp), pbmi, usSizeBmi);
                  bOk = TRUE;
                  }
                break;
              }
            // -------------------
            if (bOk)
              {
              bOk = FALSE;
              iFile = open (pszFile, O_BINARY | O_WRONLY | O_CREAT | O_TRUNC, S_IREAD | S_IWRITE);
              if (iFile != -1)
                {
                write (iFile, ptrHdr , usSizeHdr);
                switch (usFileFormat)
                 {
                 case BMP_FILE_FORMAT_PM12:
                 case BMP_FILE_FORMAT_PM11:
                 case BMP_FILE_FORMAT_WINDOW:
                   usNbLinesToRead = SIZE_IO_BUFF / usSizeLine;
                   aPel = pMemAlloue (usNbLinesToRead * usSizeLine);
                   if (aPel != NULL)
                     {
                     usLine = 0;
                     usNbLinesRead = 1;
                     while ((usLine < usNbLines) && (usNbLinesRead != 0))
                       {
                       usNbLinesRead = (USHORT) GpiQueryBitmapBits (hdc, usLine, usNbLinesToRead, aPel, pbmi);
                       write (iFile, aPel, usNbLinesRead * usSizeLine);
                       usLine = usLine + usNbLinesRead;
                       }
                     MemLibere (&aPel);
                     }
                   break;
                 case BMP_FILE_FORMAT_PACKED:
                   SavePackedBitmapBits (iFile, hdc, pbmi, usNbLines, usSizeLine);
                   break;
                 }
               close (iFile);
               }
             MemLibere (&ptrHdr);
             }
           }
         MemLibere (&pbmi);
         }
       }
     }
    BmpDestroyMemSpace (hdc);
    }
		*/
  return (bOk);
  }


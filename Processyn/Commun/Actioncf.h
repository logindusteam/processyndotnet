/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : actioncf.h                                               |
 |   Auteur  : AC					        	|
 |   Date    : 31/1/92 					        	|
 |   Version : 3.20							|
 +----------------------------------------------------------------------*/
/* -------------- actions editeur ----------------------*/
#define  action_charge_application     0
#define  action_sauve_application      1
#define  action_import                 2
#define  action_export                 3

#define  action_initialisation         4
#define  action_fermeture              5
#define  action_redessine              6
#define  action_taille                 7
#define  action_imprime                8
#define  action_quitte                 9

#define  action_efface_car             10
#define  action_back_car               11
#define  action_efface_ligne           12

#define  action_insere_ligne           13
#define  action_ecrire_car             14

#define  action_validation             15
#define  action_valide_toute_ligne     16
#define  action_abandon                17
#define  action_mode_saisie            18
#define  action_analyse_syntaxique     19
#define  action_debut_selection        20
#define  action_fin_selection          21
#define  action_aide                   22

#define  action_selectionne_tout       23
#define  action_nomme_pp               24
#define  action_copie                  25
#define  action_colle                  26
#define  action_coupe                  27
#define  action_premiere_ligne         28
#define  action_derniere_ligne         29
#define  action_debut_ligne            30
#define  action_fin_ligne              31
#define  action_curseur_droite         32
#define  action_curseur_gauche         33
#define  action_mot_suivant            34
#define  action_mot_precedent          35
#define  action_page_precedente        36
#define  action_page_suivante          37
#define  action_curseur_bas            38
#define  action_curseur_haut           39
#define  action_va_a_la_ligne          40

#define  action_init_recherche_mot     41
#define  action_recherche_mot          42
#define  action_recherche_invalide     43
#define  action_pose_marque            44
#define  action_va_a_la_marque         45

#define  action_enchaine_boite         46
#define  action_ouvrir_boite           47
#define  action_nouvelle_ligne_dlg     48
#define  action_validation_dlg         49

#define  action_generation				     50
#define  action_nouvelle_application   51
#define action_traite_ligne_de_commande 52 
#define action_option_police 53

#define  action_cf_rien 255

// dernier utilis� : 54

#define  BOITE_DIRECTE TRUE
#define  BOITE_INDIRECTE FALSE
#define  CHANGEMENT_DESCRIPTEUR 0
#define  FERME_TOUT 1
#define  QUITTE 2

#define  IMPORT_REMPLACEMENT 1
#define  IMPORT_AJOUT        2

typedef struct
  {
  char	lettre;
  char	chaine[MAX_PATH];
  BOOL	booleen;
  int		entier;
  DWORD mot;
  }  t_param_action;

typedef struct
  {
  int mode_lecture;
  DWORD n_ligne_depart;
  char chaine[MAX_PATH];
  FARPROC ptr_wnd_proc_boite;
  UINT id_dlg_boite;
  DWORD wErreur;
  } t_retour_action;


#define insertion TRUE
#define remplacement FALSE

typedef struct 
	{
	DWORD dwNFenetreEdit;				// Num�ro de la fen�tre d'�dition
	DWORD NFenetreTitre;				// Num�ro de la fen�tre de titre
	DWORD NFenetreCommentaire;	// Num�ro de la fen�tre de commentaires de la barre d'�tat
	DWORD NFenetreIndicateurs;	// Num�ro de la fen�tre des indicateurs de la barre d'�tat
	} CONTEXTE_CF;
// Var globale
extern CONTEXTE_CF ContexteCf;

void InitContexteConf (void);

//#define ID_MESSAGE_ACTION 0xffff0000
#define ID_MESSAGE_ACTION 0x100
#define WM_ACTION_CF (WM_USER+2038)

// Ajout d'une action dans la queue des action
void bAjouterActionCf (DWORD action, t_param_action *param);

// Traite un message WM_NOTIFY
// S'il s'agit d'un message d'action, ex�cution imm�diate et renvoie TRUE
// sinon renvoie FALSE
BOOL bTraiteNotifyActionCf (LPARAM lparam);

// Ex�cution imm�diate d'une action
BOOL bExecuteActionCf // Renvoie FALSE si re�u action_quitte
	(t_retour_action *retour_action);

void lire_mot_recherche_courant (char *mot_recherche);

#define avec_attente TRUE
#define sans_attente FALSE

// -------------------------------------------------------------------------
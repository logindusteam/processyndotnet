#include "stdafx.h"
#include <VfW.h>
#include "std.h"
#include "UStr.h"
#include "MemMan.h"

#include "pmPrint.h"        // Imprimante PM
#include "DibMan.h"
#include "pmBmp.h"          // Bit-Map Bmpxxx()

#include "pcsfont.h"        // Polices de Caract�res PM

#include "couleurs.h"
#include "appli.h"
#include "G_Objets.h"

#include "Verif.h"
#include "DevMan.h"

VerifInit;

// $$ Brosses Bitmaps Ok mais restent Pb lib�ration bitmap + couleur fond + devant si hbrush monochrome
// ex : static UINT bBrushBits[8] = {1,2,4,8,16,32,64,128};HBITMAP hbm = CreateBitmap(8, 8, 1, 1, (LPBYTE)bBrushBits); *phb = CreatePatternBrush(hbm); 

// --------------------------------------------------------------------------
// Attributs Graphiques (Modes, Couleurs, Styles)
// --------------------------------------------------------------------------
// Table des Couleurs
static COLORREF aColorRefsG_COULEUR [G_NB_COULEURS] =
  {
	CLR_BLACK, // 0
	CLR_WHITE, // 1
	CLR_RED, // 2
	CLR_DARKCYAN, // 3
	CLR_YELLOW, // 4
	CLR_DARKBLUE, // 5
	CLR_DARKGREEN, // 6
	CLR_PINK, // 7
	CLR_CYAN, // 8
	CLR_DARKRED, // 9
	CLR_DARKGRAY, // 10
	CLR_PALEGRAY, // 11
	CLR_DARKPINK, // 12
	CLR_GREEN, // 13
	CLR_BLUE, // 14
	CLR_BROWN, // 15
	CLR_WHITERED, // 16
	CLR_WHITEORANGE, // 17
	CLR_WHITEYELLOW, // 18
	CLR_WHITEYELLOWGREEN, // 19
	CLR_WHITEGREEN, // 20
	CLR_WHITECYANGREEN, // 21
	CLR_WHITECYAN, // 22
	CLR_WHITEAZUR, // 23
	CLR_WHITEBLUE, // 24
	CLR_WHITEVIOLET, // 25
	CLR_WHITEPINK, // 26
	CLR_WHITEPURPLE, // 27
	CLR_WHITEGRAY, // 28
	CLR_PALERED, // 29
	CLR_PALEORANGE, // 30
	CLR_PALEYELLOW, // 31
	CLR_PALEYELLOWGREEN, // 32
	CLR_PALEGREEN, // 33
	CLR_PALECYANGREEN, // 34
	CLR_PALECYAN, // 35
	CLR_PALEAZUR, // 36
	CLR_PALEBLUE, // 37
	CLR_PALEVIOLET, // 38
	CLR_PALEPINK, // 39
	CLR_PALEPURPLE, // 40
	CLR_ORANGE, // 41
	CLR_YELLOWGREEN, // 42
	CLR_CYANGREEN, // 43
	CLR_AZUR, // 44
	CLR_VIOLET, // 45
	CLR_PURPLE, // 46
	CLR_GRAY, // 47
	CLR_SHADOWRED, // 48
	CLR_SHADOWORANGE, // 49
	CLR_SHADOWYELLOW, // 50
	CLR_SHADOWYELLOWGREEN, // 51
	CLR_SHADOWGREEN, // 52
	CLR_SHADOWCYANGREEN, // 53
	CLR_SHADOWCYAN, // 54
	CLR_SHADOWAZUR, // 55
	CLR_SHADOWBLUE, // 56
	CLR_SHADOWVIOLET, // 57
	CLR_SHADOWPINK, // 58
	CLR_SHADOWPURPLE, // 59
	CLR_DARKORANGE, // 60
	CLR_DARKYELLOWGREEN, // 61
	CLR_DARKCYANGREEN, // 62
	CLR_DARKAZUR, // 63
	CLR_DARKVIOLET, // 64
	CLR_DARKPURPLE, // 65
	CLR_BLACKRED, // 66
	CLR_BLACKORANGE, // 67
	CLR_BLACKYELLOW, // 68
	CLR_BLACKYELLOWGREEN, // 69
	CLR_BLACKGREEN, // 70
	CLR_BLACKCYANGREEN, // 71
	CLR_BLACKCYAN, // 72
	CLR_BLACKAZUR, // 73
	CLR_BLACKBLUE, // 74
	CLR_BLACKVIOLET, // 75
	CLR_BLACKPINK, // 76
	CLR_BLACKPURPLE // 77
  };

#define STYLE_LINE_BARGRAF     G_STYLE_LIGNE_TIRETS_2_POINTS
#define STYLE_LINE_TREND       G_STYLE_LIGNE_TIRETS_2_POINTS
#define STYLE_LINE_LOGIC       G_STYLE_LIGNE_TIRETS_2_POINTS
#define STYLE_LINE_STATIC_TEXT G_STYLE_LIGNE_TIRETS_2_POINTS

#define STYLE_FILL_BARGRAF G_STYLE_REMPLISSAGE_HACHURE // $$r�serv� pour �a mais moche!!!

// Cache Outil maintient la liste des outils utilis�s pour un device
typedef struct	
	{ // Attention aahbrush [0] = G_COULEUR_INDETERMINEE, aahbrush [1] = G_COULEUR_(0) etc... idem aahpen
	HBRUSH	aahbrush [G_NB_COULEURS + 1][G_NB_STYLES_REMPLISSAGE];// NULL ou hbrush cr�� pour la couleur et le style
	HBRUSH	hbrushTransparent;																		// NULL ou hbrush transparent
	HPEN		aahpen [G_NB_COULEURS + 1][G_NB_STYLES_LIGNE];				// NULL ou hpen cr�� pour la couleur et le style
	HPEN		hpenTransparent;																			// NULL ou hbrush transparent
	} CACHE_OUTILS;

// structure interne d'un objet HDEV
typedef struct
  {
  DWORD	dwTypeDevice;
  HWND	hwnd;
  HDC   hdc;
	POINT ptOffsetOrigine; // coordonn�es pixels de l'origine de la zone cliente
  DWORD wGroupCount;
  char  pszDocumentName [256];
  DWORD wDocumentType;
	CACHE_OUTILS	CacheOutils;
  } DEV, *PDEV;

#define c_LETTRE_REF_Y "j"

// conversions HDEV <-> PDEV
static _inline HDEV hFromP (PDEV pdev)
	{
	return (HDEV)pdev;
	}

static _inline PDEV pFromH (HDEV hdev)
	{
	return (PDEV)hdev;
	}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//				Gestion de cache HBRUSH et HPEN
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//---------------------------------------------------------------------------
// Rends une couleur valide
// Si la valeur est invalide un warning est g�n�r� et une couleur par d�faut est prise
// nCouleur ne doit pas �tre G_COULEUR_TRANSPARENTE
static __inline void ValideCouleur (PG_COULEUR pnCouleur)
	{
	// couleur valide ?
	if ((*pnCouleur < G_COULEUR_TRANSPARENTE) || (*pnCouleur > G_N_COULEUR_MAX))
		{
		VerifWarningExit;
		*pnCouleur = G_BLACK;
		}
	}

//---------------------------------------
// Rends un style de ligne valide
// Si la valeur est invalide un warning est g�n�r� et un style par d�faut est pris
static __inline void ValideStyleLigne (PG_STYLE_LIGNE pnNStyleLigne)
	{
	// style valide ?
	if ((*pnNStyleLigne <= G_STYLE_LIGNE_INDETERMINE) || (*pnNStyleLigne > G_N_STYLE_LIGNE_MAX))
		{
		VerifWarningExit;
		*pnNStyleLigne = G_STYLE_LIGNE_CONTINUE;
		}
	} // ValideStyleLigne

//---------------------------------------
// Rends un style de ligne valide
// Si la valeur est invalide un warning est g�n�r� et un style par d�faut est pris
static __inline void ValideStyleRemplissage (PG_STYLE_REMPLISSAGE pnNStyleRemplissage)
	{
	// nStyleRemplissage valide ?
	if ((*pnNStyleRemplissage <= G_STYLE_REMPLISSAGE_INDETERMINE) || (*pnNStyleRemplissage > G_N_STYLE_REMPLISSAGE_MAX))
		{
		VerifWarningExit;
		*pnNStyleRemplissage = G_STYLE_REMPLISSAGE_HACHURE;
		}
	} // ValideStyleRemplissage

//---------------------------------------------------------------------
// Renvoie un COLORREF pour un remplissage correspondant � nCouleur
// Si la valeur est invalide un warning est g�n�r� et une couleur par d�faut est prise
// nCouleur ne doit pas �tre G_COULEUR_TRANSPARENTE
static __inline COLORREF colorrefRemplissage (G_COULEUR nCouleur)
	{
	return (nCouleur == G_COULEUR_INDETERMINEE) ? GetSysColor(COLOR_3DFACE) : aColorRefsG_COULEUR [nCouleur];
	}

//---------------------------------------------------------------------------
// Renvoie un COLORREF pour un trait ou un texte correspondant � nCouleur
// Si la valeur est invalide un warning est g�n�r� et une couleur par d�faut est prise
// nCouleur ne doit pas �tre G_COULEUR_TRANSPARENTE
static __inline COLORREF colorrefLigne (G_COULEUR nCouleur)
	{
	return (nCouleur == G_COULEUR_INDETERMINEE) ? GetSysColor(COLOR_WINDOWTEXT) : aColorRefsG_COULEUR [nCouleur];
	}

//---------------------------------------
// Renvoie un style OS de remplissage correspondant � nStyleRemplissage
static __inline LONG HS_StyleHachure (G_STYLE_REMPLISSAGE nStyleRemplissage)
	{
	// Table des styles de Remplissages
	static LONG aHS [G_NB_STYLES_REMPLISSAGE] =
		{
		0,	// valeur erron�e
		HS_FDIAGONAL,
		HS_BDIAGONAL,
		HS_DIAGCROSS,
		HS_VERTICAL,
		HS_HORIZONTAL,
		HS_CROSS,
		};

	// Renvoie le bon style de remplissage
	return aHS [nStyleRemplissage];
	}

//---------------------------------------
// Renvoie un style OS de ligne correspondant � nStyleLigne
static __inline LONG PS_StyleLigne (G_STYLE_LIGNE nStyleLigne)
	{
	// Table des styles de Ligne
	static LONG aPS [G_NB_STYLES_LIGNE] =
		{
		PS_SOLID,
		PS_DASH,
		PS_DOT,
		PS_DASHDOT,
		PS_DASHDOTDOT
		};

	// Renvoie le bon style de ligne
	return aPS[nStyleLigne];
	} // PS_StyleLigne

//---------------------------------------
static void FermeCache (PDEV pdev)
	{
	CACHE_OUTILS *	pCache = &pdev->CacheOutils;

	// HBRUSH
	for (LONG nCouleur = G_COULEUR_INDETERMINEE; nCouleur < G_NB_COULEURS; nCouleur++)
		{
		for (LONG nStyle = 0; nStyle < G_NB_STYLES_REMPLISSAGE; nStyle++)
			{
			HBRUSH * phb = &pCache->aahbrush[nCouleur+1][nStyle];
			if (*phb)
				{
				VerifWarning (DeleteObject (*phb));
				*phb = NULL;
				}
			}
		}
	pCache->hbrushTransparent = NULL;

	// HPEN
	for (DWORD nCouleur = G_COULEUR_INDETERMINEE; nCouleur < G_NB_COULEURS; nCouleur++)
		{
		for (LONG nStyle = 0; nStyle < G_NB_STYLES_LIGNE; nStyle++)
			{
			HPEN * php = &pCache->aahpen[nCouleur+1][nStyle];
			if (*php)
				{
				VerifWarning (DeleteObject (*php));
				*php = NULL;
				}
			}
		}
	pCache->hpenTransparent = NULL;
	}

// --------------------------------------------------------------------------
// s�lectionne dans le DC du HDEV un hbrush avec une couleur et un style
// Ce hbrush sera pris ou cr�� et stock� dans le cache d'outils
static HBRUSH hbrushTransparenteCacheSelect (PDEV pdev)
	{
	CACHE_OUTILS * pCache = &pdev->CacheOutils;

	// non => hbrush transparent d�ja cr�� ?
	if (!pCache->hbrushTransparent)
		// non => on le cr�e
		pCache->hbrushTransparent = (HBRUSH)GetStockObject(NULL_BRUSH);

	// on le s�lectionne
	return (HBRUSH)SelectObject (pdev->hdc, pCache->hbrushTransparent);
	} // hbrushTransparenteCacheSelect

// --------------------------------------------------------------------------
// s�lectionne dans le DC du HDEV un hbrush avec une couleur et un style
// Ce hbrush sera pris ou cr�� et stock� dans le cache d'outils
static HBRUSH hbrushCacheSelect (PDEV pdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage)
	{
	CACHE_OUTILS * pCache = &pdev->CacheOutils;
	HBRUSH * phb;

	// couleur invisible ?
	if (nCouleur == G_COULEUR_TRANSPARENTE)
		{
		// oui => cr�e � la vol�e un hpen transparent si ce n'est pas d�ja fait
		phb = &pCache->hbrushTransparent;

		if (!*phb)
			*phb = (HBRUSH)GetStockObject(NULL_BRUSH);
		}
	else
		{
		// non => autre couleur : rends les param�tres valides
		ValideCouleur (&nCouleur);
		ValideStyleRemplissage (&nStyleRemplissage);

		// hbrush d�ja cr�� ?
		phb = &pCache->aahbrush[nCouleur+1][nStyleRemplissage];

		if (!*phb)
			{
			// non => on le cr�e
			COLORREF colorref = colorrefRemplissage(nCouleur);

			if (nStyleRemplissage == G_STYLE_REMPLISSAGE_PLEIN)
				*phb = CreateSolidBrush (colorref);
			else
				*phb = CreateHatchBrush (HS_StyleHachure (nStyleRemplissage), colorref);

			VerifWarning (*phb);
			}

		// Windows 95 ?
		if (Appli.OSVersionInfoEx.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
			{
			// oui => aligne la brosse sur l'origine du DC
			UnrealizeObject (*phb);
			SetBrushOrgEx (pdev->hdc, pdev->ptOffsetOrigine.x, pdev->ptOffsetOrigine.y, NULL);
			}
		}

	// on le s�lectionne
	return (HBRUSH)SelectObject (pdev->hdc, *phb);
	} // hbrushCacheSelect

// --------------------------------------------------------------------------
// s�lectionne dans le DC du HDEV un hbrush avec une couleur et un style
// Ce hbrush sera pris ou cr�� et stock� dans le cache d'outils
static HPEN hpenTransparentCacheSelect (PDEV pdev)
	{
	CACHE_OUTILS * pCache = &pdev->CacheOutils;

	// non => hbrush transparent d�ja cr�� ?
	if (!pCache->hpenTransparent)
		// non => on le cr�e
		pCache->hpenTransparent = (HPEN)GetStockObject(NULL_PEN);

	// on le s�lectionne
	return (HPEN)SelectObject (pdev->hdc, pCache->hpenTransparent);
	} // hpenTransparentCacheSelect

// --------------------------------------------------------------------------
// s�lectionne dans le DC du HDEV un hbrush avec une couleur et un style
// Ce hbrush sera pris ou cr�� et stock� dans le cache d'outils
static HPEN hpenCacheSelect (PDEV pdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne)
	{
	CACHE_OUTILS * pCache = &pdev->CacheOutils;
	HPEN * php;

	// couleur invisible ?
	if (nCouleur == G_COULEUR_TRANSPARENTE)
		{
		// oui => cr�e � la vol�e un hpen transparent si ce n'est pas d�ja fait
		php = &pCache->hpenTransparent;

		if (!*php)
			*php = (HPEN)GetStockObject(NULL_PEN);
		}
	else
		{
		// non => autre couleur : rends les param�tres valides
		ValideCouleur (&nCouleur);
		ValideStyleLigne (&nStyleLigne);
		
		// hpen d�ja cr�� ?
		php = &pCache->aahpen[nCouleur+1][nStyleLigne];
		if (!*php)
			{
			// non => on le cr�e et on l'enregistre
			*php = CreatePen (PS_StyleLigne(nStyleLigne), 1, colorrefLigne (nCouleur));
			VerifWarning (*php);
			}
		}

	// on le s�lectionne
	return (HPEN)SelectObject (pdev->hdc, *php);
	} // hpenCacheSelect

// ------------------------------------------------------------------------
static void GetBipoint (const POINT *aPoint, LONG *x1, LONG *y1, LONG *x2, LONG *y2)
  {
  *x1 = aPoint[0].x;
  *y1 = aPoint[0].y;
  *x2 = aPoint[1].x;
  *y2 = aPoint[1].y;
  }

// ------------------------------------------------------------------------
static void SetBipoint (POINT *aPoint, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  aPoint[0].x = x1;
  aPoint[0].y = y1;
  aPoint[1].x = x2;
  aPoint[1].y = y2;
  }
// ------------------------------------------------------------------------
static void OrdonneBipoint (POINT *aPoint)
  {
  LONG nSwap;

  if (aPoint[0].x > aPoint[1].x)
    {
    nSwap = aPoint[0].x;
    aPoint[0].x = aPoint[1].x;
    aPoint[1].x = nSwap;
    }
  if (aPoint[0].y > aPoint[1].y)
    {
    nSwap = aPoint[0].y;
    aPoint[0].y = aPoint[1].y;
    aPoint[1].y = nSwap;
    }
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//												Fonctions export�es
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// ------------------------------------------------------------------------
// Ouverture du device
// ------------------------------------------------------------------------
HDEV d_open (void)
  {
  PDEV pdev = (PDEV)pMemAlloueInit0 (sizeof (DEV));

  pdev->dwTypeDevice       = c_DEVICE_WINDOW;
  StrSetNull (pdev->pszDocumentName);
  pdev->wDocumentType      = c_DEVICE_DOC_TYPE_DEFAULT;

  return hFromP(pdev);
  }

// ------------------------------------------------------------------------
// Fermeture du device
// ------------------------------------------------------------------------
void d_close (HDEV * phdev)
  {
  d_fini (*phdev);
	FermeCache (pFromH(*phdev));
  MemLibere ((PVOID *) phdev);
  }

// ------------------------------------------------------------------------
// Param�trage du device
// Attention le hdc r�cup�r� sur hwndDessin est suppos� rester valide
// juqu'� d_fini
// Pour cela utiliser CS_OWNDC dans la classe du hwnd
// ------------------------------------------------------------------------
void d_set_param (HDEV hdev, HWND hwndDessin)
  {
  PDEV pdev = pFromH(hdev);

  pdev->hwnd = hwndDessin;
  pdev->hdc  = ::GetDC (pdev->hwnd);
	VerifWarning (pdev->hdc);
	SetGraphicsMode (pdev->hdc, GM_COMPATIBLE);
  }

// ------------------------------------------------------------------------
// Sp�cifie l'origine du DC (utilis� pour l'alignement des brosses sous W95)
// ------------------------------------------------------------------------
void d_set_origine_dc (HDEV hdev, POINT ptOrigineClient)
  {
  PDEV pdev = pFromH(hdev);

	pdev->ptOffsetOrigine = ptOrigineClient;
  }

// ------------------------------------------------------------------------
// Demande le hdc du device
// ------------------------------------------------------------------------
HDC d_get_hdc (HDEV hdev)
  {
  PDEV pdev = pFromH(hdev);

  return pdev->hdc;
  }

// ------------------------------------------------------------------------
// Demande le hwnd du device
// ------------------------------------------------------------------------
HWND d_get_hwnd (HDEV hdev)
  {
  PDEV pdev = pFromH(hdev);

  return pdev->hwnd;
  }

// ------------------------------------------------------------------------
// Initialisation du device
// ------------------------------------------------------------------------
void d_init (HDEV hdev)
  {
  PDEV pdev = pFromH(hdev);

  if (pdev->hdc != NULL)
    {
		SetROP2 (pdev->hdc, R2_COPYPEN);
    }
  }

// ------------------------------------------------------------------------
// Finalisation du device
// ------------------------------------------------------------------------
void d_fini (HDEV hdev)
  {
  PDEV pdev = pFromH(hdev);
	HENHMETAFILE	hmf;

  if (pdev->hdc != NULL)
    {
    switch (pdev->dwTypeDevice)
      {
      case c_DEVICE_WINDOW:
           break;
      case c_DEVICE_PRINTER:
           ClosePrinterLI (pdev->hdc);
           break;
      case c_DEVICE_METAFILE:
           hmf = CloseEnhMetaFile (pdev->hdc);
           DeleteEnhMetaFile (hmf);
           break;
      case c_DEVICE_BITMAP_CLIENT:
           break;
      case c_DEVICE_BITMAP_FRAME:
           break;
      case c_DEVICE_BITMAP_SCREEN:
           break;
      default:
           break;
      }
		VerifWarning (::ReleaseDC (pdev->hwnd, pdev->hdc));
    pdev->hdc = (HDC) NULL;
    }
  }

//---------------------------------------
// Cr�e une brosse de couleur sp�cifi�e (avec v�rification des param�tres)
HBRUSH hCreeBrossePleine (G_COULEUR nCouleur)
	{
	HBRUSH	hbrushRes;

	// Valide les param�tres
	ValideCouleur (&nCouleur);

	if (nCouleur == G_COULEUR_TRANSPARENTE)
		hbrushRes = (HBRUSH)GetStockObject(NULL_BRUSH);
	else
		hbrushRes = CreateSolidBrush (colorrefRemplissage(nCouleur));

	return hbrushRes;
	}

//-------------------------------------------------------------------------------------
// Cr�e une brosse de couleur et style sp�cifi�s (avec v�rification des param�tres)
HBRUSH hCreeBrosseStyle (G_STYLE_REMPLISSAGE nStyleRemplissage, G_COULEUR nCouleur)
	{
	HBRUSH	hbrushRes;

	// Valide les param�tres
	ValideCouleur (&nCouleur);
	ValideStyleRemplissage (&nStyleRemplissage);

	if (nCouleur == G_COULEUR_TRANSPARENTE)
		hbrushRes = (HBRUSH)GetStockObject(NULL_BRUSH);
	else
		{
		COLORREF colorref = colorrefRemplissage(nCouleur);

		if (nStyleRemplissage == G_STYLE_REMPLISSAGE_PLEIN)
			hbrushRes = CreateSolidBrush (colorref);
		else
			hbrushRes = CreateHatchBrush (HS_StyleHachure (nStyleRemplissage), colorref);
		}
	return hbrushRes;
	}

//---------------------------------------
// Cr�e un HPEN de couleur et style sp�cifi�s (avec v�rification des param�tres)
HPEN hCreeStylo (G_STYLE_LIGNE nStyleLigne, G_COULEUR nCouleur)
	{
	ValideCouleur (&nCouleur);
	ValideStyleLigne (&nStyleLigne);
	return (CreatePen (PS_StyleLigne(nStyleLigne), 1, colorrefLigne (nCouleur)));
	}

//---------------------------------------
// R�cup�re la valeur RGB d'une G_COULEUR ou RGB(0,0,0) si G_COULEUR invalide
COLORREF d_RGB(G_COULEUR nCouleur)
	{
	ValideCouleur (&nCouleur);
	return aColorRefsG_COULEUR [nCouleur];
	}

// Valeur de la couleur logique la plus proche du RGB donn�
G_COULEUR d_CouleurProcheDuRGB(COLORREF rgbSource)
	{
	G_COULEUR nRet = G_BLACK;
	int nDistanceCourante = MAXDWORD;
	int nR = GetRValue(rgbSource);
	int nG = GetGValue(rgbSource);
	int nB = GetBValue(rgbSource);
	for (int nCouleur = 0; nCouleur < G_NB_COULEURS; nCouleur++)
		{
		COLORREF rgbTest = aColorRefsG_COULEUR[nCouleur];
		int ndR = nR - GetRValue(rgbTest);
		int ndG = nG - GetGValue(rgbTest);
		int ndB = nB - GetBValue(rgbTest);
		int nDistance = nR*nR + nG*nG + nB*nB;
		if (nDistance < nDistanceCourante)
			{
			nDistanceCourante = nDistance;
			nRet = G_COULEUR(nCouleur);
			}
		}
	return nRet;
	}

static G_COULEUR aGCouleursAffinite [] =
	{
	G_WHITE,
	G_WHITERED,
	G_WHITEORANGE,
	G_WHITEYELLOW,
	G_WHITEYELLOWGREEN,
	G_WHITEGREEN,
	G_WHITECYANGREEN,
	G_WHITECYAN,
	G_WHITEAZUR,
	G_WHITEBLUE,
	G_WHITEVIOLET,
	G_WHITEPINK,
	G_WHITEPURPLE,
	G_WHITEGRAY,
	G_PALERED,
	G_PALEORANGE,
	G_PALEYELLOW,
	G_PALEYELLOWGREEN,
	G_PALEGREEN,
	G_PALECYANGREEN,
	G_PALECYAN,
	G_PALEAZUR,
	G_PALEBLUE,
	G_PALEVIOLET,
	G_PALEPINK,
	G_PALEPURPLE,
	G_PALEGRAY,
	G_RED,
	G_ORANGE,
	G_YELLOW,
	G_YELLOWGREEN,
	G_GREEN,
	G_CYANGREEN,
	G_CYAN,
	G_AZUR,
	G_BLUE,
	G_VIOLET,
	G_PINK,
	G_PURPLE,
	G_GRAY,
	G_SHADOWRED,
	G_SHADOWORANGE,
	G_SHADOWYELLOW,
	G_SHADOWYELLOWGREEN,
	G_SHADOWGREEN,
	G_SHADOWCYANGREEN,
	G_SHADOWCYAN,
	G_SHADOWAZUR,
	G_SHADOWBLUE,
	G_SHADOWVIOLET,
	G_SHADOWPINK,
	G_SHADOWPURPLE,
	G_DARKGRAY,
	G_DARKRED,
	G_DARKORANGE,
	G_BROWN,
	G_DARKYELLOWGREEN,
	G_DARKGREEN,
	G_DARKCYANGREEN,
	G_DARKCYAN,
	G_DARKAZUR,
	G_DARKBLUE,
	G_DARKVIOLET,
	G_DARKPINK,
	G_DARKPURPLE,
	G_BLACK,
	G_BLACKRED,
	G_BLACKORANGE,
	G_BLACKYELLOW,
	G_BLACKYELLOWGREEN,
	G_BLACKGREEN,
	G_BLACKCYANGREEN,
	G_BLACKCYAN,
	G_BLACKAZUR,
	G_BLACKBLUE,
	G_BLACKVIOLET,
	G_BLACKPINK,
	G_BLACKPURPLE
	};

// ------------------------------------------------------------------------
G_COULEUR d_AffiniteCouleurPremiere()
	{
	return G_WHITE;
	}

// ------------------------------------------------------------------------
G_COULEUR d_CouleurAffinite(int nAffiniteCouleur)
	{
	G_COULEUR nRet = G_RED;
	if ((nAffiniteCouleur >= 0) && (nAffiniteCouleur < G_NB_COULEURS))
		nRet = aGCouleursAffinite[nAffiniteCouleur];
	return nRet;
	}

// ------------------------------------------------------------------------
G_COULEUR d_AffiniteCouleurDerniere()
	{
	return G_BLACKPURPLE;
	}

static PCSTR aszCouleur [] =
	{
	"BLACK",
	"WHITE",
	"RED",
	"DARKCYAN",
	"YELLOW",
	"DARKBLUE",
	"DARKGREEN",
	"PINK",
	"CYAN",
	"DARKRED",
	"DARKGRAY",
	"PALEGRAY",
	"DARKPINK",
	"GREEN",
	"BLUE",
	"BROWN",
	"WHITERED",
	"WHITEORANGE",
	"WHITEYELLOW",
	"WHITEYELLOWGREEN",
	"WHITEGREEN",
	"WHITECYANGREEN",
	"WHITECYAN",
	"WHITEAZUR",
	"WHITEBLUE",
	"WHITEVIOLET",
	"WHITEPINK",
	"WHITEPURPLE",
	"WHITEGRAY",
	"PALERED",
	"PALEORANGE",
	"PALEYELLOW",
	"PALEYELLOWGREEN",
	"PALEGREEN",
	"PALECYANGREEN",
	"PALECYAN",
	"PALEAZUR",
	"PALEBLUE",
	"PALEVIOLET",
	"PALEPINK",
	"PALEPURPLE",
	"ORANGE",
	"YELLOWGREEN",
	"CYANGREEN",
	"AZUR",
	"VIOLET",
	"PURPLE",
	"GRAY",
	"SHADOWRED",
	"SHADOWORANGE",
	"SHADOWYELLOW",
	"SHADOWYELLOWGREEN",
	"SHADOWGREEN",
	"SHADOWCYANGREEN",
	"SHADOWCYAN",
	"SHADOWAZUR",
	"SHADOWBLUE",
	"SHADOWVIOLET",
	"SHADOWPINK",
	"SHADOWPURPLE",
	"DARKORANGE",
	"DARKYELLOWGREEN",
	"DARKCYANGREEN",
	"DARKAZUR",
	"DARKVIOLET",
	"DARKPURPLE",
	"BLACKRED",
	"BLACKORANGE",
	"BLACKYELLOW",
	"BLACKYELLOWGREEN",
	"BLACKGREEN",
	"BLACKCYANGREEN",
	"BLACKCYAN",
	"BLACKAZUR",
	"BLACKBLUE",
	"BLACKVIOLET",
	"BLACKPINK",
	"BLACKPURPLE"
	};

// ------------------------------------------------------------------------
PCSTR d_NomCouleur(G_COULEUR nCouleur)
	{
	PCSTR pszRet = "???";

	if ((int)nCouleur < G_NB_COULEURS)
		pszRet = aszCouleur[(int)nCouleur];
	return pszRet;
	}

// ------------------------------------------------------------------------
void d_set_document_name (HDEV hdev, char *pszName, DWORD wDocType)
  {
  PDEV pdev = pFromH(hdev);

  StrCopy (pdev->pszDocumentName, pszName);
  if (wDocType != c_DEVICE_DOC_TYPE_UNCHANGED)
    {
    pdev->wDocumentType = wDocType;
    }
  }

// ------------------------------------------------------------------------
DWORD d_get_document_name (HDEV hdev, char *pszName)
  {
  PDEV pdev = pFromH(hdev);

  StrCopy (pszName, pdev->pszDocumentName);
  return (pdev->wDocumentType);
  }

// ------------------------------------------------------------------------
// Choix du p�riph�rique de sortie
// ------------------------------------------------------------------------
void *d_select_device (HDEV hdev, DWORD Device)
  {
  PDEV pdev = pFromH(hdev);
  //
  void          *hdlResult;
  //
  char           Printer [MAX_CHAR_PRINTER_NAME];
  char           Queue   [MAX_CHAR_QUEUE_NAME];
  char           Model   [MAX_CHAR_MODEL_NAME];
  char           Driver  [MAX_CHAR_PRINTER_DRIVER_NAME];
  UINT           lpDrivLength;
  DEVMODE *     pDriv;
  //
	HENHMETAFILE	hmf;
  //
  HWND           hwnd;
  RECT          rclWin;
  RECT          rclScreen;
  RECT          rcl;
  HBITMAP        hbm;


  //GpiAssociate (pdev->hdc, (HDC) NULL);
  switch (pdev->dwTypeDevice)
    {
		case c_DEVICE_WINDOW:
			hdlResult = NULL;
			break;
		case c_DEVICE_PRINTER:
			NewPagePrinter (pdev->hdc);
			EndDocumentPrinter (pdev->hdc);
			ClosePrinterLI (pdev->hdc);
			hdlResult = NULL;
			break;
		case c_DEVICE_METAFILE:
			hmf = CloseEnhMetaFile (pdev->hdc);
			hdlResult = hmf;
			break;
		case c_DEVICE_BITMAP_CLIENT:
			::GetWindowRect (pdev->hwnd, &rclWin);
			::MapWindowPoints (pdev->hwnd, HWND_DESKTOP, (POINT *) &rclWin, 2);
			rclScreen = Appli.rectEcran;
			IntersectRect (&rcl, &rclWin, &rclScreen);
			if (!IsRectEmpty (&rcl))
				{
				hbm = BmpFromRclScreen (&rcl);
				hdlResult = hbm;
				}
			else
				hdlResult = NULL;
			break;
		case c_DEVICE_BITMAP_FRAME:
			hwnd = ::GetParent (pdev->hwnd);
			::GetWindowRect (hwnd, &rclWin);
			::MapWindowPoints (hwnd, HWND_DESKTOP, (POINT *) &rclWin, 2);
			rclScreen = Appli.rectEcran;
			IntersectRect (&rcl, &rclWin, &rclScreen);
			if (!IsRectEmpty (&rcl))
				{
				hbm = BmpFromRclScreen (&rcl);
				hdlResult = hbm;
				}
			else
				hdlResult = NULL;
			break;
		case c_DEVICE_BITMAP_SCREEN:
			rcl = Appli.rectEcran;
			hbm = BmpFromRclScreen (&rcl);
			hdlResult = hbm;
			break;
		default:
			break;
    }
  pdev->hdc = (HDC) NULL;

  pdev->dwTypeDevice = Device;

  switch (pdev->dwTypeDevice)
    {
		case c_DEVICE_WINDOW:
			pdev->hdc = ::GetDC (pdev->hwnd);
			break;
		case c_DEVICE_PRINTER:
			GetDefaultPrinter (Printer, Queue, Driver, Model);
			lpDrivLength = GetJobPropertiesLength (Printer, Driver, Model);
			pDriv = (DEVMODE *)pMemAlloue (lpDrivLength);
			GetDefaultJobProperties (Printer, Driver, Model, pDriv);
			//GetUserJobProperties (Printer, Driver, Model, pDriv);
			pdev->hdc = OpenPrinterLI (Queue, Driver, pDriv);
			MemLibere ((PVOID*)(&pDriv));
			StartDocumentPrinter (pdev->hdc, pdev->pszDocumentName);
			break;
		case c_DEVICE_METAFILE:
			::GetWindowRect (pdev->hwnd, &rclWin);
			pdev->hdc = CreateEnhMetaFile (NULL, // $$ hdc de r�f�rence = �cran
				NULL, // $$ en m�moire
				&rclWin, // $$ dimensions m�ta file : Ok ?
				NULL); // (LPSTR)szDescription
			break;
		case c_DEVICE_BITMAP_CLIENT:
		case c_DEVICE_BITMAP_FRAME:
		case c_DEVICE_BITMAP_SCREEN:
			break;
		default:
			pdev->hdc = (HDC) NULL;
			break;
    }
  // SetROP2 (pdev->hdc, R2_COPYPEN);
  return (hdlResult);
  }

// ------------------------------------------------------------------------
void d_get_device_limits (HDEV hdev, LONG *x0, LONG *y0, LONG *cx, LONG *cy)
  {
  PDEV pdev = pFromH(hdev);
	RECT          rclDevice;
  HWND           hwnd;

  switch (pdev->dwTypeDevice)
    {
		case c_DEVICE_WINDOW:
			::GetWindowRect (pdev->hwnd, &rclDevice);
			break;
		case c_DEVICE_PRINTER:
			rclDevice.left   = 0;
			rclDevice.top = 0;
			GetPagePrinter (pdev->hdc, &rclDevice.right, &rclDevice.bottom);
			break;
		case c_DEVICE_METAFILE:
			rclDevice.left   = 0;
			rclDevice.top    = 0;
			rclDevice.right  = 0;
			rclDevice.bottom = 0;
			break;
		case c_DEVICE_BITMAP_CLIENT:
			::GetWindowRect (pdev->hwnd, &rclDevice);
			::MapWindowPoints (pdev->hwnd, HWND_DESKTOP, (POINT *) &rclDevice, 2);
			break;
		case c_DEVICE_BITMAP_FRAME:
			hwnd = ::GetParent (pdev->hwnd);
			::GetWindowRect (hwnd, &rclDevice);
			::MapWindowPoints (hwnd, HWND_DESKTOP, (POINT *) &rclDevice, 2);
			break;
		case c_DEVICE_BITMAP_SCREEN:
			rclDevice = Appli.rectEcran;
			break;
		default:
			rclDevice.left   = 0;
			rclDevice.top    = 0;
			rclDevice.right  = 0;
			rclDevice.bottom = 0;
			break;
    }

  (*x0) = rclDevice.left;
  (*y0) = rclDevice.bottom;
  (*cx) = rclDevice.right - rclDevice.left;
  (*cy) = rclDevice.bottom   - rclDevice.top;
  }

// ------------------------------------------------------------------------
void d_delete_handle (HDEV hdev, void *hdl, DWORD wHdlType)
  {
  if (hdl)
    {
    switch (wHdlType)
      {
      case c_DEVICE_HDL_METAFILE:
        DeleteEnhMetaFile ((HENHMETAFILE)hdl);
				break;
      case c_DEVICE_HDL_BITMAP:
				DeleteObject ((HGDIOBJ)hdl);
				break;
      default:
				break;
      }
    }
  }

// ------------------------------------------------------------------------
void d_draw_handle (HDEV hdev, void *hdl, DWORD wHdlType, DWORD wAdaptHdl, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV pdev = pFromH(hdev);
  POINT         ptl;
  RECT          rcl;

  if (hdl)
    {
    switch (wHdlType)
      {
      case c_DEVICE_HDL_METAFILE:
				::GetWindowRect (pdev->hwnd, &rcl);	//rectangle limite
				PlayEnhMetaFile (pdev->hdc, (HENHMETAFILE)hdl, &rcl);
        break;
      case c_DEVICE_HDL_BITMAP:
				switch (wAdaptHdl)
					{
					case c_DEVICE_ADAPT_NORMAL:
						ptl.x = x1;
						ptl.y = y1;
						BmpToSpace (pdev->hdc, (HBITMAP)hdl, &ptl, NULL);
						break;
					case c_DEVICE_ADAPT_ADJUST:
						rcl.left   = x1;
						rcl.top = y1;
						rcl.right  = x2;
						rcl.bottom    = y2;
						VerifWarningExit;
						// $$ BmpToRclSpace (pdev->hdc, hdl, &rcl, NULL);
						break;
					}
				break;
      }
    }
  }

// ------------------------------------------------------------------------
void d_save_handle (HDEV hdev, void *hdl, DWORD wHdlType)
  {
  PDEV pdev = pFromH(hdev);
  HENHMETAFILE   hmf;
  USHORT         usFileFormat;

  if (hdl != NULL)
    {
    switch (wHdlType)
      {
      case c_DEVICE_HDL_METAFILE:
       hmf = CopyEnhMetaFile ((HENHMETAFILE)hdl, pdev->pszDocumentName);
       break;
      case c_DEVICE_HDL_BITMAP:
				/*switch (pdev->wDocumentType)
					{ $$
					case c_DEVICE_DOC_TYPE_PM11:
						usFileFormat = BMP_FILE_FORMAT_PM11;
						break;
					case c_DEVICE_DOC_TYPE_WIN:
						usFileFormat = BMP_FILE_FORMAT_WINDOW;
						break;
					case c_DEVICE_DOC_TYPE_PKD:
						usFileFormat = BMP_FILE_FORMAT_PACKED;
						break;
					
					default:
						break;
					}*/
					usFileFormat = BMP_FILE_FORMAT_WINDOW;
				BmpToFile ((HBITMAP)hdl, pdev->pszDocumentName, usFileFormat);
				break;
      default:
           break;
      }
    }
  }


// ------------------------------------------------------------------------
// trac� d'une ligne droite
void d_line (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV pdev = pFromH(hdev);
  POINT aPt [2];

	// s�lection des attributs
	LONG		BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN	hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

  SetBipoint (aPt, x1, y1, x2, y2);

  MoveToEx (pdev->hdc, aPt [0].x, aPt [0].y, NULL);
  LineTo (pdev->hdc, aPt [1].x, aPt [1].y);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
	}

// ------------------------------------------------------------------------
// dessin du contour d'un rectangle
void d_rectangle (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  if ((x1 != x2) && (y1 != y2))
    {
		PDEV		pdev = pFromH(hdev);
		POINT   aPt [2];

		// s�lection des attributs
		LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
		HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);
		HBRUSH	hbrushOld = hbrushTransparenteCacheSelect (pdev);

		// adaptation des coordonn�es
    SetBipoint (aPt, x1, y1, x2, y2);
    OrdonneBipoint (aPt);

		// Dessin de la figure
		Rectangle (pdev->hdc, aPt [0].x, aPt [0].y, aPt [1].x, aPt [1].y);

		// restauration des attributs
		SelectObject (pdev->hdc, hpenOld);
		SelectObject (pdev->hdc, hbrushOld);
		SetBkMode (pdev->hdc, BkModeOld);
    }
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'un rectangle
void d_fill_rectangle (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT	aPt [2];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

	// adaptation des coordonn�es
	SetBipoint (aPt, x1, y1, x2, y2);
  OrdonneBipoint (aPt);

	// Dessin de la figure (avec attributs)
	Rectangle (pdev->hdc, aPt [0].x, aPt [0].y, aPt [1].x, aPt [1].y);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }


// ------------------------------------------------------------------------
// dessin d'une ellipse
void d_circle (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT	aPt [2];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);
	HBRUSH	hbrushOld = hbrushTransparenteCacheSelect (pdev);

	// adaptation des coordonn�es
  SetBipoint (aPt, x1, y1, x2, y2);
  OrdonneBipoint (aPt);

	// Dessin de la figure
  Ellipse(pdev->hdc, 
    aPt [0].x, aPt [0].y, // haut gauche
    aPt [1].x, aPt [1].y);	// bas droit

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
	}

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'une ellipse
void d_fill_circle (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPt [2];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

	SetROP2 (pdev->hdc, R2_COPYPEN);
  SetBipoint (aPt, x1, y1, x2, y2);
  OrdonneBipoint (aPt);

	Ellipse (pdev->hdc, aPt [0].x, aPt [0].y, aPt [1].x, aPt [1].y);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// -----------------------------------------------------------------
// calcule les points d'un 1/4 d'ellipse (coupure aux axes)
static void _4_arc (POINT *aPtArc, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT aPt [2];
	INT		xDelta;
	INT		yDelta;
	INT		xDemiArc;
	INT		yDemiArc;

  SetBipoint (aPt, x1, y1, x2, y2);

	xDelta = aPt [1].x - aPt [0].x;
	yDelta = aPt[1].y - aPt[0].y;

	xDemiArc = (xDelta < 0) ? -xDelta : xDelta;
	yDemiArc = (yDelta < 0) ? -yDelta : yDelta;
	
	// point haut gauche encadrement de l'ellipse
	aPtArc [0].x = aPt [0].x - xDemiArc; 
	aPtArc [0].y = aPt [0].y - yDemiArc;
	
	// point bas droit encadrement de l'ellipse
	aPtArc [1].x = aPt [0].x + xDemiArc; 
	aPtArc [1].y = aPt [0].y + yDemiArc;
	
	// initialisation point de d�part et point d'arriv�e de l'ellipse (sens des aiguilles d'une montre)
	aPtArc [2] = aPt [0];
	aPtArc [3] = aPt [0];

  if (xDelta >= 0)
    {
    if (yDelta >= 0)
      {
			// quart bas droit
			aPtArc [2].y += yDemiArc;
			aPtArc [3].x += xDemiArc;
      }
    else
      {
			// quart haut droit
			aPtArc [2].x += xDemiArc;
			aPtArc [3].y -= yDemiArc;
      }
    }
  else
    {
    if (yDelta >= 0)
      {
			// quart bas gauche
			aPtArc [2].x -= xDemiArc;
			aPtArc [3].y += yDemiArc;
      }
    else
      {
			// quart haut gauche
			aPtArc [2].y -= yDemiArc;
			aPtArc [3].x -= xDemiArc;
      }
    }

	// Supprime la correction de Windows sur le point bas droit
	aPtArc [1].x ++;
	aPtArc [1].y ++;
	}

// ------------------------------------------------------------------------
// dessin du contour d'un 1/4 d'ellipse Arc + corde (coupure aux axes)
void d_4_arc (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV pdev = pFromH(hdev);
	POINT	aPtArc [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

	_4_arc (aPtArc, x1, y1, x2, y2);
	Arc(
    pdev->hdc, // handle to device context 
    aPtArc[0].x,	// x-coordinate of bounding rectangle's upper-left corner 
    aPtArc[0].y,	// y-coordinate of bounding rectangle's upper-left corner  
    aPtArc[1].x,	// x-coordinate of bounding rectangle's lower-right corner   
    aPtArc[1].y,	// y-coordinate of bounding rectangle's lower-right corner    
    aPtArc[2].x,	// first radial ending point 
    aPtArc[2].y,	// first radial ending point 
    aPtArc[3].x,	// second radial ending point 
    aPtArc[3].y		// second radial ending point 
   );
	
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'un 1/4 d'ellipse Arc + corde (coupure aux axes)
void d_fill_4_arc (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
	POINT	aPtArc [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

	_4_arc (aPtArc, x1, y1, x2, y2);
	Chord(
    pdev->hdc, // handle to device context 
    aPtArc[0].x,	// x-coordinate of bounding rectangle's upper-left corner 
    aPtArc[0].y,	// y-coordinate of bounding rectangle's upper-left corner  
    aPtArc[1].x,	// x-coordinate of bounding rectangle's lower-right corner   
    aPtArc[1].y,	// y-coordinate of bounding rectangle's lower-right corner    
    aPtArc[2].x,	// first radial ending point 
    aPtArc[2].y,	// first radial ending point 
    aPtArc[3].x,	// second radial ending point 
    aPtArc[3].y		// second radial ending point 
   );

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// -----------------------------------------------------------------
// calcule les points d'un 1/4 d'ellipse (coupure aux axes) + le centre
static void _4_arc_rect (POINT *aPtArc, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT aPt [2];
	INT		xDelta;
	INT		yDelta;
	INT		xDemiArc;
	INT		yDemiArc;

  SetBipoint (aPt, x1, y1, x2, y2);

	xDelta = aPt [1].x - aPt [0].x;
	yDelta = aPt[1].y - aPt[0].y;

	xDemiArc = (xDelta < 0) ? -xDelta : xDelta;
	yDemiArc = (yDelta < 0) ? -yDelta : yDelta;
	
	// point haut gauche encadrement de l'ellipse
	aPtArc [0].x = aPt [0].x - xDemiArc; 
	aPtArc [0].y = aPt [0].y - yDemiArc;
	
	// point bas droit encadrement de l'ellipse
	aPtArc [1].x = aPt [0].x + xDemiArc; 
	aPtArc [1].y = aPt [0].y + yDemiArc;
	
	// initialisation point de d�part et point d'arriv�e de l'ellipse (sens des aiguilles d'une montre)
	aPtArc [2] = aPt [0];
	aPtArc [4] = aPt [0];

	// centre de l'ellipse
	aPtArc [3] = aPt [0];


  if (xDelta >= 0)
    {
    if (yDelta >= 0)
      {
			// quart bas droit
			aPtArc [2].y += yDemiArc;
			aPtArc [4].x += xDemiArc;
      }
    else
      {
			// quart haut droit
			aPtArc [2].x += xDemiArc;
			aPtArc [4].y -= yDemiArc;
      }
    }
  else
    {
    if (yDelta >= 0)
      {
			// quart bas gauche
			aPtArc [2].x -= xDemiArc;
			aPtArc [4].y += yDemiArc;
      }
    else
      {
			// quart haut gauche
			aPtArc [2].y -= yDemiArc;
			aPtArc [4].x -= xDemiArc;
      }
    }

	// Supprime la correction de Windows sur le point bas droit
	aPtArc [1].x ++;
	aPtArc [1].y ++;
	}

// ------------------------------------------------------------------------
// dessin du contour d'un 1/4 d'ellipse (coupure aux axes) Arc + jonction au centre 
void d_4_arc_rect (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV pdev = pFromH(hdev);
	POINT	aPtArc [5];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

	_4_arc_rect (aPtArc, x1, y1, x2, y2);
	
	// trac� de l'arc (ArcTo, plus indiqu� n'est pas implant� sous WIndows 95)
	Arc(
    pdev->hdc, // handle to device context 
    aPtArc[0].x,	// left of bounding rectangle
    aPtArc[0].y,	// top of bounding rectangle
    aPtArc[1].x,	// right of bounding rectangle
    aPtArc[1].y,	// bottom of bounding rectangle
    aPtArc[2].x,	// first radial ending point 
    aPtArc[2].y,	// first radial ending point 
    aPtArc[4].x,	// second radial ending point 
    aPtArc[4].y		// second radial ending point 
   );

	// borde l'arc (fin arc, centre de l'ellipse, d�but de l'arc
	Polyline (pdev->hdc, &aPtArc[2], 3);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'un
void d_fill_4_arc_rect (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
	POINT	aPtArc [5];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

	_4_arc_rect (aPtArc, x1, y1, x2, y2);
	Pie(
    pdev->hdc, // handle to device context 
    aPtArc[0].x,	// x-coordinate of bounding rectangle's upper-left corner 
    aPtArc[0].y,	// y-coordinate of bounding rectangle's upper-left corner  
    aPtArc[1].x,	// x-coordinate of bounding rectangle's lower-right corner   
    aPtArc[1].y,	// y-coordinate of bounding rectangle's lower-right corner    
    aPtArc[2].x,	// first radial ending point 
    aPtArc[2].y,	// first radial ending point 
    aPtArc[4].x,	// second radial ending point 
    aPtArc[4].y		// second radial ending point 
   );

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// -----------------------------------------------------------------
// calcule les points d'une demi ellipse (section horizontale)
static void h_2_arc (POINT *aPtArc, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT aPt [2];
	INT		yDelta;
	INT		yDemiArc;

  SetBipoint (aPt, x1, y1, x2, y2);

	yDelta = aPt [1].y - aPt [0].y;
	OrdonneBipoint (aPt);

	yDemiArc = aPt [1].y - aPt [0].y;
	
	// init encadrement de l'ellipse
	aPtArc [0] = aPt [0]; // haut gauche
	aPtArc [1] = aPt [1]; // bas droit
	
  if (yDelta < 0)
    {
		// demi ellipse haute
		aPtArc [1].y += yDemiArc;	// correction point bas droit encadrement de l'ellipse
		aPtArc [2] = aPt [1];			// d�part de l'arc
		aPtArc [3] = aPt [0];			// arriv�e de l'arc
		aPtArc [3].y += yDemiArc;	// encadrement de l'ellipse
    }
  else
    {
		// demi ellipse basse
		aPtArc [0].y -= yDemiArc; // correction point haut gauche encadrement de l'ellipse
		aPtArc [2] = aPt [0];			// d�part de l'arc
		aPtArc [3] = aPt [1];			// arriv�e de l'arc
		aPtArc [3].y -= yDemiArc;	// encadrement de l'ellipse
    }

	// Supprime la correction de Windows sur le point bas droit
	aPtArc [1].x ++;
	aPtArc [1].y ++;
	}

// ------------------------------------------------------------------------
// dessin du contour d'une demi ellipse (section horizontale)
void d_h_2_arc (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV pdev = pFromH(hdev);
	POINT	aPtArc [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

	h_2_arc (aPtArc, x1, y1, x2, y2);
	
	// trac� de l'arc (ArcTo, plus indiqu� n'est pas implant� sous WIndows 95)
	Arc(
    pdev->hdc, // handle to device context 
    aPtArc[0].x,	// left of bounding rectangle
    aPtArc[0].y,	// top of bounding rectangle
    aPtArc[1].x,	// right of bounding rectangle
    aPtArc[1].y,	// bottom of bounding rectangle
    aPtArc[2].x,	// first radial ending point 
    aPtArc[2].y,	// first radial ending point 
    aPtArc[3].x,	// second radial ending point 
    aPtArc[3].y		// second radial ending point 
   );

	// On ne borde PAS l'arc (fin arc, centre de l'ellipse, d�but de l'arc
	//Polyline (pdev->hdc, &aPtArc[2], 2);
	
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
	}

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'une demi ellipse (section horizontale)
void d_fill_h_2_arc (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
	POINT	aPtArc [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

	h_2_arc (aPtArc, x1, y1, x2, y2);
	Pie(
    pdev->hdc, // handle to device context 
    aPtArc[0].x,	// x-coordinate of bounding rectangle's upper-left corner 
    aPtArc[0].y,	// y-coordinate of bounding rectangle's upper-left corner  
    aPtArc[1].x,	// x-coordinate of bounding rectangle's lower-right corner   
    aPtArc[1].y,	// y-coordinate of bounding rectangle's lower-right corner    
    aPtArc[2].x,	// first radial ending point 
    aPtArc[2].y,	// first radial ending point 
    aPtArc[3].x,	// second radial ending point 
    aPtArc[3].y		// second radial ending point 
   );

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// -----------------------------------------------------------------
// calcule les points d'une demi ellipse (section verticale)
static void v_2_arc (POINT *aPtArc, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT aPt [2];
	INT		xDelta;
	INT		xDemiArc;

  SetBipoint (aPt, x1, y1, x2, y2);

	xDelta = aPt [1].x - aPt [0].x;
	OrdonneBipoint (aPt);

	xDemiArc = aPt [1].x - aPt [0].x;
	
	// init encadrement de l'ellipse
	aPtArc [0] = aPt [0]; // haut gauche
	aPtArc [1] = aPt [1]; // bas droit
	
  if (xDelta < 0)
    {
		// demi ellipse gauche
		aPtArc [1].x += xDemiArc;	// correction point bas droit encadrement de l'ellipse
		aPtArc [2] = aPt [0];			// d�part de l'arc
		aPtArc [2].x += xDemiArc;
		aPtArc [3] = aPt [1];			// arriv�e de l'arc
    }
  else
    {
		// demi ellipse droite
		aPtArc [0].x -= xDemiArc; // correction point haut gauche encadrement de l'ellipse
		aPtArc [2] = aPt [1];			// d�part de l'arc
		aPtArc [2].x -= xDemiArc;
		aPtArc [3] = aPt [0];			// arriv�e de l'arc
    }

	// Supprime la correction de Windows sur le point bas droit
	//aPtArc [1].x ++;
	//aPtArc [1].y ++;
	}

// ------------------------------------------------------------------------
// dessin du contour d'une demi ellipse (section verticale)
void d_v_2_arc (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV pdev = pFromH(hdev);
	POINT	aPtArc [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

	v_2_arc (aPtArc, x1, y1, x2, y2);
	
	// d�part au centre de l'ellipse
	//MoveToEx (pdev->hdc, aPtArc[4].x,	aPtArc[4].y,NULL); 

	// trac� de l'arc (ArcTo, plus indiqu�, n'est pas implant� sous Windows 95)
	Arc(
    pdev->hdc, // handle to device context 
    aPtArc[0].x,	// left of bounding rectangle
    aPtArc[0].y,	// top of bounding rectangle
    aPtArc[1].x,	// right of bounding rectangle
    aPtArc[1].y,	// bottom of bounding rectangle
    aPtArc[2].x,	// first radial ending point 
    aPtArc[2].y,	// first radial ending point 
    aPtArc[3].x,	// second radial ending point 
    aPtArc[3].y		// second radial ending point 
   );

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'une demi ellipse (section verticale)
void d_fill_v_2_arc (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
	POINT	aPtArc [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

	v_2_arc (aPtArc, x1, y1, x2, y2);
	Pie(
    pdev->hdc, // handle to device context 
    aPtArc[0].x,	// x-coordinate of bounding rectangle's upper-left corner 
    aPtArc[0].y,	// y-coordinate of bounding rectangle's upper-left corner  
    aPtArc[1].x,	// x-coordinate of bounding rectangle's lower-right corner   
    aPtArc[1].y,	// y-coordinate of bounding rectangle's lower-right corner    
    aPtArc[2].x,	// first radial ending point 
    aPtArc[2].y,	// first radial ending point 
    aPtArc[3].x,	// second radial ending point 
    aPtArc[3].y		// second radial ending point 
   );

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// -----------------------------------------------------------------
// calcule les points d'une h_vanne
static void h_vanne (POINT *aPtPoly, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT aPt [2];

  SetBipoint (aPt, x1, y1, x2, y2);
  OrdonneBipoint (aPt);

  aPtPoly [0] = aPt [0];
  aPtPoly [1] = aPt [1];
  aPtPoly [2].x = aPt [1].x;
  aPtPoly [2].y = aPt [0].y;
  aPtPoly [3].x = aPt [0].x;
  aPtPoly [3].y = aPt [1].y;
  aPtPoly [4] = aPt [0];
	}

// ------------------------------------------------------------------------
// dessin du contour d'une vanne � axe horizontal
void d_h_vanne (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPtPoly [5];
	
	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

	h_vanne (aPtPoly, x1, y1, x2, y2);
  Polyline (pdev->hdc, aPtPoly, 5);
	
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'une vanne � axe horizontal
void d_fill_h_vanne (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPtPoly [5];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

 	h_vanne (aPtPoly, x1, y1, x2, y2);
	Polygon (pdev->hdc, aPtPoly, 5);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// -----------------------------------------------------------------
// calcule les points d'une vanne � axe vertical
static void v_vanne (POINT *aPtPoly, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT	aPt [2];

  SetBipoint (aPt, x1, y1, x2, y2);
  OrdonneBipoint (aPt);

  aPtPoly [0] = aPt [0];
  aPtPoly [1].x = aPt [1].x;
  aPtPoly [1].y = aPt [0].y;
  aPtPoly [2].x = aPt [0].x;
  aPtPoly [2].y = aPt [1].y;
  aPtPoly [3] = aPt [1];
  aPtPoly [4] = aPt [0];
	}

// ------------------------------------------------------------------------
// dessin du contour d'une vanne � axe vertical
void d_v_vanne (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT	aPtPoly [5];

	// s�lection des attributs
	LONG		BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN	hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

 	v_vanne (aPtPoly, x1, y1, x2, y2);
  Polyline (pdev->hdc, aPtPoly, 5);
	
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'une vanne � axe vertical
void d_fill_v_vanne (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPtPoly [5];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

 	v_vanne (aPtPoly, x1, y1, x2, y2);
	Polygon (pdev->hdc, aPtPoly, 5);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// -----------------------------------------------------------------
// calcule les points d'un triangle rectangle (angle droit selon l'orientation des points)
static void tri_rect (POINT *aPtPoly, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT aPt [2];

  SetBipoint (aPt, x1, y1, x2, y2);

  aPtPoly [2] = aPt [0];
  if (aPt [1].x < aPt [0].x)
    {
    if (aPt [1].y < aPt [0].y)
      {
      aPtPoly [0].x = aPt [1].x;
      aPtPoly [0].y = aPt [0].y;
      aPtPoly [1].x = aPt [0].x;
      aPtPoly [1].y = aPt [1].y;
      }
    else
      {
      aPtPoly [0].x = aPt [0].x;
      aPtPoly [0].y = aPt [1].y;
      aPtPoly [1].x = aPt [1].x;
      aPtPoly [1].y = aPt [0].y;
      }
    }
  else
    {
    if (aPt [1].y < aPt [0].y)
      {
      aPtPoly [0].x = aPt [0].x;
      aPtPoly [0].y = aPt [1].y;
      aPtPoly [1].x = aPt [1].x;
      aPtPoly [1].y = aPt [0].y;
      //aPtPoly [2].x++;                         // [!] Correction mode XOR
      }
    else
      {
      aPtPoly [0].x = aPt [1].x;
      aPtPoly [0].y = aPt [0].y;
      aPtPoly [1].x = aPt [0].x;
      aPtPoly [1].y = aPt [1].y;
      //aPtPoly [2].y++;                         // [!] Correction mode XOR
      }
    }
  aPtPoly [3] = aPtPoly [0];
	}

// ------------------------------------------------------------------------
// dessin du contour d'un triangle rectangle
void d_tri_rect (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPtPoly [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

 	tri_rect (aPtPoly, x1, y1, x2, y2);
  Polyline (pdev->hdc, aPtPoly, 4);
	
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'un triangle rectangle
void d_fill_tri_rect (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPtPoly [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

 	tri_rect (aPtPoly, x1, y1, x2, y2);
	Polygon (pdev->hdc, aPtPoly, 4);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// -----------------------------------------------------------------
// calcule les points d'un triangle isoc�le base verticale
static void v_arrow (POINT *aPtPoly, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT   aPt [2];

  SetBipoint (aPt, x1, y1, x2, y2);

  if (aPt [1].x < aPt [0].x)
    {
    if (aPt [1].y < aPt [0].y)
      {
      aPtPoly [0].x = aPt [1].x;
      aPtPoly [0].y = aPt [0].y;
      aPtPoly [1].x = (aPt [0].x + aPt [1].x) / 2;
      aPtPoly [1].y = aPt [1].y;
      }
    else
      {
      aPtPoly [0].x = (aPt [0].x + aPt [1].x) / 2;
      aPtPoly [0].y = aPt [1].y;
      aPtPoly [1].x = aPt [1].x;
      aPtPoly [1].y = aPt [0].y;
      }
    }
  else
    {
    if (aPt [1].y < aPt [0].y)
      {
      aPtPoly [0].x = (aPt [0].x + aPt [1].x) / 2;
      aPtPoly [0].y = aPt [1].y;
      aPtPoly [1].x = aPt [1].x;
      aPtPoly [1].y = aPt [0].y;
      }
    else
      {
      aPtPoly [0].x = aPt [1].x;
      aPtPoly [0].y = aPt [0].y;
      aPtPoly [1].x = (aPt [0].x + aPt [1].x) / 2;
      aPtPoly [1].y = aPt [1].y;
      }
    }
  aPtPoly [2] = aPt [0];
  aPtPoly [3] = aPtPoly [0];
	}

// ------------------------------------------------------------------------
// dessin du contour d'un triangle isoc�le base verticale
void d_v_arrow (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV		pdev = pFromH(hdev);
  POINT   aPtPoly [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

 	v_arrow (aPtPoly, x1, y1, x2, y2);
  Polyline (pdev->hdc, aPtPoly, 4);
	
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'un triangle isoc�le base verticale
void d_fill_v_arrow (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPtPoly [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

 	v_arrow (aPtPoly, x1, y1, x2, y2);
	Polygon (pdev->hdc, aPtPoly, 4);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
	}

// -----------------------------------------------------------------
// calcule les points d'un triangle isoc�le base horizontale
static void h_arrow (POINT *aPtPoly, LONG x1, LONG y1, LONG x2, LONG y2)
	{
  POINT aPt [2];

  SetBipoint (aPt, x1, y1, x2, y2);

  if (aPt [1].x < aPt [0].x)
    {
    if (aPt [1].y < aPt [0].y)
      {
      aPtPoly [0].x = aPt [1].x;
      aPtPoly [0].y = (aPt [0].y + aPt [1].y) / 2;
      aPtPoly [1].x = aPt [0].x;
      aPtPoly [1].y = aPt [1].y;
      }
    else
      {
      aPtPoly [0].x = aPt [0].x;
      aPtPoly [0].y = aPt [1].y;
      aPtPoly [1].x = aPt [1].x;
      aPtPoly [1].y = (aPt [0].y + aPt [1].y) / 2;
      }
    }
  else
    {
    if (aPt [1].y < aPt [0].y)
      {
      aPtPoly [0].x = aPt [0].x;
      aPtPoly [0].y = aPt [1].y;
      aPtPoly [1].x = aPt [1].x;
      aPtPoly [1].y = (aPt [0].y + aPt [1].y) / 2;
      }
    else
      {
      aPtPoly [0].x = aPt [1].x;
      aPtPoly [0].y = (aPt [0].y + aPt [1].y) / 2;
      aPtPoly [1].x = aPt [0].x;
      aPtPoly [1].y = aPt [1].y;
      }
    }
  aPtPoly [2] = aPt [0];
  aPtPoly [3] = aPtPoly [0];
	}


// ---------------------------------------------------------------------------------
// dessin du contour d'un triangle isoc�le base horizontale
void d_h_arrow (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPtPoly [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

 	h_arrow (aPtPoly, x1, y1, x2, y2);
  Polyline (pdev->hdc, aPtPoly, 4);
	
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin de l'int�rieur d'un triangle isoc�le base horizontale
void d_fill_h_arrow (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT aPtPoly [4];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);

 	h_arrow (aPtPoly, x1, y1, x2, y2);
	Polygon (pdev->hdc, aPtPoly, 4);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
void d_h_write (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG LargeurCar, LONG TaillePolice, 
								DWORD Police, DWORD PoliceStyle, char *pszString)
  {
  if (TaillePolice > 0)
    {
		// s�lection des attributs
		if (nCouleur != G_COULEUR_TRANSPARENTE)
			{
			PDEV			pdev = pFromH(hdev);
			LONG				BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
			COLORREF	OldTextColor = SetTextColor (pdev->hdc, colorrefLigne (nCouleur));
			HFONT     hfont = fnt_create (pdev->hdc, Police, PoliceStyle, (DWORD)TaillePolice, Appli.byCharSetFont);
			HFONT			hOldFont = (HFONT)SelectObject (pdev->hdc, hfont);

			TextOut (pdev->hdc, x1, y1, pszString, StrLength (pszString));

			// restauration des attributs
			SelectObject (pdev->hdc, hOldFont);
			fnt_destroy (hfont);
			SetTextColor (pdev->hdc, OldTextColor);
			SetBkMode (pdev->hdc, BkModeOld);
			}
    }
  }

// ------------------------------------------------------------------------
void d_v_write (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG LargeurCar, LONG TaillePolice,
								DWORD Police, DWORD PoliceStyle, char *pszString)
  {
  if (TaillePolice > 0)
    {
		PDEV			pdev = pFromH(hdev);
		LONG				BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
		COLORREF	OldTextColor = SetTextColor (pdev->hdc, colorrefLigne (nCouleur));
		HFONT			hfont = fnt_create (pdev->hdc, Police, PoliceStyle, (DWORD)TaillePolice, Appli.byCharSetFont);
		HFONT			hOldFont = (HFONT)SelectObject (pdev->hdc, hfont);

    TextOut (pdev->hdc, x1, y1, pszString, StrLength (pszString));

		// restauration des attributs
		SelectObject (pdev->hdc, hOldFont);
		fnt_destroy (hfont);
		SetTextColor (pdev->hdc, OldTextColor);
		SetBkMode (pdev->hdc, BkModeOld);
    }
  }

// ------------------------------------------------------------------------
void d_poly_line (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, DWORD nb_points, void *td_points)
  {
  PDEV pdev = pFromH(hdev);
  POINT * aptl = (POINT *) td_points;

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, nStyleLigne);

  Polyline (pdev->hdc, aptl, (INT) nb_points);
		
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
void d_fill_poly_line (HDEV hdev, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, DWORD nb_points, POINT *td_points)
  {
  PDEV pdev = pFromH(hdev);

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenTransparentCacheSelect (pdev);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, nStyleRemplissage);
	LONG			iPolyFillModeOld = SetPolyFillMode (pdev->hdc, ALTERNATE);

  Polygon (pdev->hdc, td_points, nb_points);

	// restauration des attributs
	SetPolyFillMode (pdev->hdc, iPolyFillModeOld);
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// Repr�sentation hors animation d'une courbe
void d_trend (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV		pdev = pFromH(hdev);
  POINT   aPt [2];
  POINT   aPtPoly [4];
	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, STYLE_LINE_TREND);

	// trac� du contour
  d_rectangle (hdev, nCouleur, STYLE_LINE_TREND, x1, y1, x2, y2);

  SetBipoint (aPt, x1, y1, x2, y2);
	OrdonneBipoint (aPt);

  aPtPoly [0].x = aPt [0].x;
  aPtPoly [0].y = aPt [1].y;
  aPtPoly [1].x  = ( (3 * aPt [0].x) + aPt [1].x) / 4;
  aPtPoly [1].y  = (aPt [1].y + (3 * aPt [0].y)) / 4;
  aPtPoly [2].x  = (aPt [0].x + aPt [1].x) / 2;
  aPtPoly [2].y  = ((3 * aPt [1].y) + aPt [0].y) / 4;
  aPtPoly [3].x  = aPt [1].x - 1;
  aPtPoly [3].y  = aPt [0].y - 1;

  //MoveToEx (pdev->hdc, aPtPoly [0].x, aPtPoly [0].y, NULL);
  Polyline (pdev->hdc, aPtPoly, 4);
	
	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// Repr�sentation hors animation d'un bargraphe
void d_bargraf (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PDEV	pdev = pFromH(hdev);
  POINT	aPt [2];

	// s�lection des attributs
	LONG			BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN		hpenOld	= hpenCacheSelect (pdev, nCouleur, STYLE_LINE_BARGRAF);
	HBRUSH	hbrushOld = hbrushCacheSelect (pdev, nCouleur, STYLE_FILL_BARGRAF);

	// adaptation des coordonn�es
	SetBipoint (aPt, x1, y1, x2, y2);
  OrdonneBipoint (aPt);

	// Dessin de la figure (avec attributs)
	Rectangle (pdev->hdc, aPt [0].x, aPt [0].y, aPt [1].x, aPt [1].y);

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SelectObject (pdev->hdc, hbrushOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// dessin d'un texte 
void d_static_text (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD Police, DWORD PoliceStyle, char *pszString)
  {
  POINT         aPt [2];

  SetBipoint (aPt, x1, y1, x2, y2);
  OrdonneBipoint (aPt);
  // taille de caract�re d�duites de l'encombrement
  d_h_write (hdev, nCouleur, aPt [0].x, aPt [0].y, 0, aPt [1].y - aPt [0].y, Police, PoliceStyle, pszString);
  }

// ------------------------------------------------------------------------
void d_entryfield (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD Police, DWORD PoliceStyle, char *pszString)
  {
  PDEV pdev = pFromH(hdev);
  POINT         aPt [2];
  LONG            CxBox;
  char           CarRef;
	char           pszEntryField[256] ="";


  SetBipoint (aPt, x1, y1, x2, y2);
  OrdonneBipoint (aPt);
	GetBipoint (aPt, &x1, &y1, &x2, &y2);
  d_rectangle (hdev, nCouleur, STYLE_LINE_STATIC_TEXT, x1, y1, x2, y2);
  CxBox = aPt[1].x - aPt[0].x;
  //d_get_size_car_box (hdev, &(aPt [0].x), &(aPt [0].y), &(aPt [1].x), &(aPt [1].y), Police, PoliceStyle);
  if (CxBox > 4)
    {
    CxBox = CxBox - 2;
    }
	// Trouve la chaine "TTTTTT" la plus longue rentrant dans la place
  if ((CxBox > 2) && ((y2-y1)>1))
    {
		DWORD	wNbCar = (DWORD) (CxBox / (aPt [1].x - aPt [0].x));
		BOOL  bTrouve = FALSE;
		DWORD w;

		if (wNbCar>255)
			wNbCar=255;

    StrCopyUpToLength (pszEntryField, pszString, 1);
    StrToCHAR (&CarRef, pszEntryField);
    for (w = 1; (w < wNbCar); w++)
      {
      StrConcatChar (pszEntryField, CarRef);
      }
    while (!bTrouve)
      {
      x2 = 0;
      y2 = aPt [1].y - aPt [0].y; //$$ signe � v�rifier
      d_get_box_h_write (hdev, 0, 0, &x2, &y2, Police, PoliceStyle, y2, pszEntryField);
      bTrouve =  (x2 >= CxBox);
      if (bTrouve)
        {
        StrSetLength (pszEntryField, StrLength (pszEntryField) - 1);
        }
      else
        {
        StrConcatChar (pszEntryField, CarRef);
        }
      }
    }
	// $$ v�rifier
  d_h_write (hdev, nCouleur, aPt [0].x, aPt [0].y, 0, aPt [1].y - aPt [0].y, Police, PoliceStyle, pszEntryField);
  }

// ------------------------------------------------------------------------
void d_logic_entry (HDEV hdev, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  d_rectangle (hdev, nCouleur, STYLE_LINE_LOGIC, x1, y1, x2, y2);
  }

// ------------------------------------------------------------------------
// Encombrement de textes
// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
// Encombrement de texte horizontal
void d_get_box_h_write (HDEV hdev, LONG x1, LONG y1, LONG *px2, LONG *py2, DWORD Police, DWORD PoliceStyle, DWORD TaillePolice, PCSTR pszString)
  {
	PDEV	pdev = pFromH(hdev);
	SIZE	sizeText = {0,0};
	HFONT hfont = fnt_create (pdev->hdc, Police, PoliceStyle, TaillePolice, Appli.byCharSetFont);
	HFONT	hOldFont = (HFONT)SelectObject (pdev->hdc, hfont);

	GetTextExtentPoint32 (pdev->hdc, pszString,	StrLength (pszString), &sizeText);
	*px2 = x1 + sizeText.cx;
	*py2 = y1 + sizeText.cy;

	// lib�re la police
	SelectObject (pdev->hdc, hOldFont);
	fnt_destroy (hfont);
  }

// ------------------------------------------------------------------------
// Encombrement de texte vertical
void d_get_box_v_write (HDEV hdev, LONG x1, LONG y1, LONG *px2, LONG *py2, DWORD Police, DWORD PoliceStyle, DWORD TaillePolice, PCSTR pszString)
  {
	PDEV	pdev = pFromH(hdev);
	SIZE	sizeText;
	HFONT     hfont = fnt_create (pdev->hdc, Police, PoliceStyle,  TaillePolice, Appli.byCharSetFont);
	HFONT			hOldFont = (HFONT)SelectObject (pdev->hdc, hfont);

	GetTextExtentPoint32(pdev->hdc, pszString,	StrLength (pszString),	&sizeText);

	// par d�faut : �criture en montant
	*px2 = x1 + sizeText.cy;
	*py2 = y1 - sizeText.cx;

	// lib�re la police
	SelectObject (pdev->hdc, hOldFont);
	fnt_destroy (hfont);
	}

// ------------------------------------------------------------------------
// Encombrement de texte "reflet"
void d_get_box_static_text (HDEV hdev, LONG *px1, LONG *py1, LONG *px2, LONG *py2, DWORD Police, DWORD PoliceStyle, char *pszString)
  {
  POINT	aPt [2];

  SetBipoint (aPt, *px1, *py1, *px2, *py2);
  OrdonneBipoint (aPt);
  GetBipoint (aPt, px1, py1, px2, py2);

  if ((*py1) != (*py2))
    {
		PDEV	pdev = pFromH(hdev);
		SIZE	sizeText;
		HFONT	hfont = fnt_create (pdev->hdc, Police, PoliceStyle, *py2 - *py1, Appli.byCharSetFont);
		HFONT	hOldFont = (HFONT)SelectObject (pdev->hdc, hfont);

		GetTextExtentPoint32(
			pdev->hdc, // handle of device context 
			pszString,	// address of text string 
			StrLength (pszString),	// number of characters in string 
			&sizeText 	// address of structure for string size  
		 );

    *px2 = *px1 + sizeText.cx;
    *py2 = *py1 + sizeText.cy;

		// lib�re la police
		SelectObject (pdev->hdc, hOldFont);
		fnt_destroy (hfont);
    }
  else
    {
    *px2 = *px1;
    *py2 = *py1;
    }
  }

// ------------------------------------------------------------------------
void d_get_size_car_ref_plus (HDEV hdev, LONG *cx, LONG *cy, LONG *offsetY, DWORD Police, DWORD PoliceStyle)
  {
  PDEV pdev = pFromH(hdev);
  TEXTMETRIC   fm;

  // $$utilise des polices normales GpiSetCharMode (pdev->hdc, CM_DEFAULT);

  GetTextMetrics (pdev->hdc, &fm);
  (*cx) = (LONG) fm.tmMaxCharWidth;
  (*cy) = (LONG) fm.tmHeight;
  (*offsetY) = (LONG) (fm.tmHeight - fm.tmAscent);
  //(*offsetY) = (LONG) fm.lMaxDescender;
  }

// ------------------------------------------------------------------------
void d_get_size_car_box (HDEV hdev, LONG *x1, LONG *y1, LONG *x2, LONG *y2, DWORD Police, DWORD PoliceStyle)
  {
  PDEV pdev = pFromH(hdev);
  POINT         aPt [2];
  LONG            Taille;
  LONG            CxBase;
  LONG            CyBase;
  LONG            OffsetYBase;
  LONG            CxBox;
  LONG            CyBox;
  LONG            PosY;

  SetBipoint (aPt, *x1, *y1, *x2, *y2);
  OrdonneBipoint (aPt);
  GetBipoint (aPt, x1, y1, x2, y2);
  CyBox = (*y2) - (*y1);
  CxBox = (*x2) - (*x1);
  d_get_size_car_ref_plus (hdev, &CxBase, &CyBase, &OffsetYBase, Police, PoliceStyle);
  Taille = CyBox / CyBase;
  if (Taille > 0)
    {
    OffsetYBase = OffsetYBase * Taille;
    }
  PosY = (*y1) + OffsetYBase;
  if (PosY < (*y2))
    {
    (*y1) = PosY;
    }
  PosY = (*y2) + OffsetYBase;
  (*y2) = PosY;
  if (CxBox > 4)
    {
    (*x1) = (*x1) + 2;
    }
  (*x2) = (*x1) + (3 * ((*y2) - (*y1)) / 4);
  }

// ------------------------------------------------------------------------
// remplissage du fond avec la couleur donn�e unie
void d_fill_background (HDEV hdev, G_COULEUR nCouleur)
  {
  PDEV pdev = pFromH(hdev);
  RECT          rcl;

  ::GetClientRect (pdev->hwnd, &rcl);
  d_fill_rectangle (hdev, nCouleur, G_STYLE_REMPLISSAGE_PLEIN, rcl.left, rcl.top, rcl.right+1, rcl.bottom+1);
	}

// ------------------------------------------------------------------------
void d_grille (HDEV hdev, G_COULEUR nCouleur, G_STYLE_LIGNE style, LONG pas_x, LONG pas_y)
  {
  PDEV pdev = pFromH(hdev);
  RECT rcl;
  LONG  x;
  LONG  y;

	// s�lection des attributs
	LONG		BkModeOld = SetBkMode (pdev->hdc, TRANSPARENT);
	HPEN	hpenOld	= hpenCacheSelect (pdev, nCouleur, style);


  // r�cup�re la taille de la fen�tre
	::GetClientRect (pdev->hwnd, &rcl);

	// lignes verticales et horizontales ? 
  if ((pas_x > 2) && (pas_y > 2))
    {
		// oui => un point aux intersection
    for (x = pas_x - 1; x < rcl.right; x += pas_x)
			{
			for (y = pas_y - 1; y < rcl.bottom; y += pas_y)
				{
				MoveToEx (pdev->hdc, x, y, NULL);
				LineTo (pdev->hdc, x+1, y);
				}
			}
    }
	else
		{
		// non
		// lignes verticales
		if (pas_x > 2)
			{
			for (x = pas_x - 1; x < rcl.right; x += pas_x)
				{
				MoveToEx (pdev->hdc, x, rcl.top, NULL);
				LineTo (pdev->hdc, x, rcl.bottom);
				}
			}

		// lignes horizontales
		if (pas_y > 2)
			{
			for (y = pas_y - 1; y < rcl.bottom; y += pas_y)
				{
				MoveToEx (pdev->hdc, rcl.left, y, NULL);
				LineTo (pdev->hdc, rcl.right, y);
				}
			}
		}

	// restauration des attributs
	SelectObject (pdev->hdc, hpenOld);
	SetBkMode (pdev->hdc, BkModeOld);
  }

// ------------------------------------------------------------------------
// renvoie la taille  de la zone cliente de la fen�tre
void d_get_taille_background (HDEV hdev, LONG * dxFond, LONG * dyFond)
  {
  PDEV pdev = pFromH(hdev);
  RECT          rcl;
	
  ::GetClientRect (pdev->hwnd, &rcl);
	*dxFond=rcl.right-rcl.left;
	*dyFond=rcl.bottom-rcl.top; 
 	}
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
BOOL d_color_stable (HDEV hdev, G_COULEUR nCouleur)
  {
  return nCouleur < G_NB_COULEURS;
  }

// ------------------------------------------------------------------------
// Effectue un ::InvalidateRect sur un rectangle
void d_InvalidateRect (HDEV hdev, const RECT * pRectInvalide)
	{
  PDEV pdev = pFromH(hdev);

	if (pdev->hwnd)
		{
		if (pRectInvalide)
			{
			RECT rect;

			if ((pRectInvalide->top != pRectInvalide->bottom) || (pRectInvalide->left != pRectInvalide->right))
				{
				// ordonne si n�cessaire les ccordonn�es du rectangle (n�cessaire sous Windows95)
				// et neutralise la restriction Windows ($$ x n�cessaire pour les animations de bargraphes)
				if (pRectInvalide->top > pRectInvalide->bottom)
					{
					rect.top		= pRectInvalide->bottom - 1;
					rect.bottom = pRectInvalide->top +1;
					}
				else
					{
					rect.top		= pRectInvalide->top - 1;
					rect.bottom = pRectInvalide->bottom +1;
					}
				if (pRectInvalide->left > pRectInvalide->right)
					{
					rect.left		= pRectInvalide->right -1;
					rect.right	= pRectInvalide->left +1;
					}
				else
					{
					rect.left		= pRectInvalide->left -1;
					rect.right	= pRectInvalide->right +1;
					}
				// Invalide la zone demand�e
				::InvalidateRect (pdev->hwnd, &rect, TRUE);
				} // if ((pRectInvalide->top != pRectInvalide->bottom) || (pRectInvalide->left != pRectInvalide->right)
			} // if (pRectInvalide)
		else
			// Invalide toute la page
			::InvalidateRect (pdev->hwnd, pRectInvalide, TRUE);
		}
	}

// ------------------------------------------------------------------------
// d�claration interne d'un objet de gestion de dib
typedef struct
	{
	HDIB								hdib;
	HDRAWDIB						hdrawdib;
	LPBITMAPINFOHEADER	pbi;
	BITMAPINFOHEADER		bi;
	RECT								rectdib;
	} DIB_SUPPORT, *PDIB_SUPPORT; //ds, pds

// ------------------------------------------------------------------------
// BitMap
// ------------------------------------------------------------------------
HANDLE d_load_bmp (HDEV hdev, PCSTR pszFichierBmp)
  {
	BOOL					bOk = FALSE;
	PDIB_SUPPORT	pds	= (PDIB_SUPPORT)pMemAlloueInit0 (sizeof(DIB_SUPPORT));

	// Initialise l'objet support de DIB :
	// Ouverture du DC de dessin DIB ok ?
	if (pds->hdrawdib = DrawDibOpen())
		{
		// oui => chargement du fichier Ok ?
		if (pds->hdib = hChargeDIB (pszFichierBmp))
			{
			// R�cup�ration du pointeur associ� au handle Ok ?
			if (pds->pbi = (LPBITMAPINFOHEADER)GlobalLock(pds->hdib))
				{
				// Ok recopie son header et r�cup�re ses dimensions (erreur si acc�s � pbi apr�s DrawDibDraw)
				pds->bi = *pds->pbi;
				SetRect(&pds->rectdib, 0, 0, (LONG)pds->bi.biWidth, (LONG)pds->bi.biHeight);
				bOk = TRUE;
				}
			}
		}

	// Erreur ?
	if (!bOk)
		//oui => ferme l'objet
		d_delete_bmp (hdev, (HANDLE *) &pds);

  return (HANDLE) pds;
  }

// ------------------------------------------------------------------------
// Supprime le HDIB et ferme le hDrawDib
void d_delete_bmp (HDEV hdev, HANDLE * phBmp)
  {
	PDIB_SUPPORT	pds	= (PDIB_SUPPORT) * phBmp;

	if (pds)
		{
		// Fermeture DC de dessin DIB
		if (pds->hdrawdib)
			DrawDibClose (pds->hdrawdib);

		// Fermeture du handle DIB
		if (pds->hdib)
			VerifWarning (bFermeDIB (&pds->hdib));

		MemLibere ((PVOID *)(&pds));
		* phBmp = NULL;
		}
	//	BmpDestroy (*phBmp);
  }

// ------------------------------------------------------------------------
void d_draw_bmp (HDEV hdev, HANDLE hBmp, BOOL bStretch, LONG x1, LONG y1, LONG x2, LONG y2)
  {
	if (hBmp)
		{
		PDIB_SUPPORT	pds	= (PDIB_SUPPORT)hBmp;
	  PDEV pdev = pFromH(hdev);

		if (pds->hdrawdib && pds->hdib && pds->pbi)
			{
			DrawDibRealize(pds->hdrawdib, pdev->hdc, TRUE);
			// Dessin Ok ?
			BOOL bRes=FALSE;
			if (bStretch)
				{
				//CRect rect(origin, size);
				//LOGPIXELSX
				//pDC->LPtoDP(rect); // Convert DIB's rectangle to MM_TEXT coordinates

				bRes=DrawDibDraw 
					(
					pds->hdrawdib,	//HDRAWDIB hdd,	
					pdev->hdc,//HDC hdc,	
					x1, //LONG xDst,	
					y1, //LONG yDst,	
					x2-x1, //LONG dxDst,	
					y2-y1, //LONG dyDst,	
					pds->pbi, //LPBITMAPINFOHEADER lpbi,	
					NULL, //LPVOID lpBits,	
					0, //LONG xSrc,	
					0, //LONG ySrc,	
					pds->rectdib.right, //LONG dxSrc,	
					pds->rectdib.bottom, //LONG dySrc,	
					DDF_SAME_HDC //UINT wFlags	
					);
				}
			else
				{
				bRes=DrawDibDraw 
					(
					pds->hdrawdib,	//HDRAWDIB hdd,	
					pdev->hdc,//HDC hdc,	
					0, //LONG xDst,	
					0, //LONG yDst,	
					pds->rectdib.right, //LONG dxDst,	
					pds->rectdib.bottom, //LONG dyDst,	
					pds->pbi, //LPBITMAPINFOHEADER lpbi,	
					NULL, //LPVOID lpBits,	
					0, //LONG xSrc,	
					0, //LONG ySrc,	
					pds->rectdib.right, //LONG dxSrc,	
					pds->rectdib.bottom, //LONG dySrc,	
					DDF_SAME_HDC //UINT wFlags	
					);
				}
			if (!bRes)
				{
				// non => dessin d'un rectangle gris en lieu et place
				VerifWarningExit;
				FillRect(pdev->hdc, &pds->rectdib, (HBRUSH)GetStockObject(DKGRAY_BRUSH));
				}

			//GlobalUnlock (hBmp);
			}
		}
  }

// ----------------------- fin DevMan.c -----------------------------------------


// --------------------------------------------------------------------------
// ULpt.h Gestion de ports LPTx
// Version WIN32 25/7/97
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Erreurs retourn�es par les fonctions
// --------------------------------------------------------------------------
class CLpt
	{
	public:
		// CLpt();
		// virtual ~CLpt();

	typedef enum
		{
		ERREUR_LPT_NONE               =    0, // Pas d'erreur.
		ERREUR_LPT_PORT_REDEFINITION  =    1, // Le port de communication a d�j� �t� d�fini.
		ERREUR_LPT_CREATE_THREAD      =    2, // Echec � la cr�ation de t�che.
		ERREUR_LPT_PORT_UNDEFINE      =    3, // Le port de communication n'a pas �t� d�fini.
		ERREUR_LPT_DRIVER_BUSY        =    4, // Une fonction en asynchrone est en cours d'execution; la fonction demand�e n'a pas �t� execut�e.
		ERREUR_LPT_OPEN               =    5, // Erreur d'ouverture du port de communication [CreateFile()].
		ERREUR_LPT_INIT_PRINTER       =    6, // Erreur d'initialisation de l'imprimante parallele; le port de communication n'est pas ouvert.
		ERREUR_LPT_INIT_LINE_CTRL     =    7, // Erreur d'initialisation de l'imprimante s�rie; le port de communication n'est pas ouvert.
		ERREUR_LPT_INIT_BAUDE_RATE    =    8, // Erreur d'initialisation de l'imprimante s�rie; le port de communication n'est pas ouvert.
		ERREUR_LPT_INIT_DEVICE_CTRL   =    9, // Erreur d'initialisation de l'imprimante s�rie; le port de communication n'est pas ouvert.
		ERREUR_LPT_INIT_MODEM_CTRL    =   10, // Erreur d'initialisation de l'imprimante s�rie; le port de communication n'est pas ouvert.
		ERREUR_LPT_CLOSE              =   11, // Erreur de fermeture du port de communication [CloseHandle()].
		ERREUR_LPT_STATUS             =   12, // Erreur lecture status
		ERREUR_LPT_STATUS_WRITE       =   13, // Status imprimante invalide pendant l'�criture.
		ERREUR_LPT_WRITE              =   14, // Erreur �criture [WriteFile()].
		ERREUR_LPT_WRITE_TIME_OUT     =   15  // Time-out pendant �criture; le buffer � �crire est dans le driver!
		} ERREUR_LPT;
	//

	private:
		// Structure point�e par un HLPT
		typedef struct
			{
			DWORD				dwSignature;				// SIGNATURE_HLPT si ouvert
			char 				szNomPort[20];
			HANDLE			hf;									// handle OS sur port
			BOOL				bOverlappedEnCours;	//pour op�ration d'�criture asynchrone
			OVERLAPPED	ovl;
			BYTE *			pbBufEmission;
			DWORD				dwTailleBufEmission;
			DWORD				dwTailleEcrite;
			LONG        lTimeOutWrite;
			} ENV_LPT, *PENV_LPT;
	public:
		// Handle sur port LPT
		DECLARE_HANDLE (HLPT);

		typedef HLPT * PHLPT;

		// Ouverture d'un port d'imprimante parall�le
		static BOOL bLptOuvre 
			(
			PHLPT phLpt,	// adresse du handle � mettre � jour (mis � NULL si err)
			PCSTR szNomPort // Nom du port : "LPT1", "LPT2"
			);							// Renvoie TRUE si le port est ouvert

		// Fermeture d'un port d'imprimante parall�le.
		// Renvoie TRUE si le port est ferm�
		static BOOL bFermeLpt (PHLPT phLpt);		// adresse du handle � mettre � jour (remis � NULL)

		// Ecriture sur l'imprimante
		static ERREUR_LPT LptEcrire (HLPT hLpt, DWORD dwTailleBuf, BYTE * pbBuf, DWORD * pdwErreurSysteme);

		// Attente de la fin d'une ecriture
		static ERREUR_LPT LptAttenteFinEcriture (HLPT hLpt, DWORD * pdwErreurSysteme, DWORD * pdwTailleEcrite);

		// Force le timeout d'attente fin d'une ecriture
		static void LptSetTimeOut (HLPT hLpt, LONG lTimeOut);
		
	private:
		static PENV_LPT	pFromhLPT (HLPT hlpt);
		static void DebugSignaleErr (PENV_LPT pEnvLpt, PCSTR pszContexte);
		static void StatutLastError (DWORD * pdwStatut);
	};

// -------------------------- fin ULpt.h ----------------------------

//------------------------------------------------------------
// UEnv.c
// JS 25/6/96
// Gestion d'un environnement pour une fen�tre
// g�re un et un seul bloc de donn�es associ� � un handle de fen�tre
// utilise (Get/SetWindowLong (GWL_USERDATA)).

// code redondant pour optimisation
//$$ test validit� HWND ?
// $$ renvoyer une �ventuelle erreur sur SetWindowLong ou GetWindowLong
//------------------------------------------------------------

#include "stdafx.h"
#include "std.h"
#include "MemMan.h"

#include "UEnv.h"

//------------------------------------------------------------
// Alloue un bloc de donn�es et l'enregistre sur le handle de fen�tre
// renvoie NULL ou le pointeur sur les donn�es associ�
PVOID pCreeEnv
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	DWORD dwTailleBloc) // taille de la zone m�moire � associer
	{
	PVOID pEnv = pMemAlloue(dwTailleBloc);
	SetWindowLong (hWndAssociee, GWL_USERDATA, (LPARAM)pEnv);
	return pEnv ;
	}


//------------------------------------------------------------
// Alloue et initialise un bloc de donn�es et l'enregistre
// sur le handle de fen�tre
// renvoie NULL ou le pointeur sur les donn�es associ�
PVOID pCreeEnvInit 
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	DWORD dwTailleBloc, // taille de la zone m�moire � associer
	CONST PVOID pBlocACopier)	// zone � recopier. Si NULL => bloc initialis� � 0
	{
	PVOID pEnv = pMemAlloue(dwTailleBloc);
	if (pBlocACopier)
		CopyMemory (pEnv, pBlocACopier, dwTailleBloc);
	else
		FillMemory (pEnv, dwTailleBloc, 0);
	SetWindowLong (hWndAssociee, GWL_USERDATA, (LPARAM)pEnv);
	return pEnv ;
	}
//------------------------------------------------------------
// WM_CREATE : enregistre le param�tre du CreateWindow
// sur le handle de fen�tre et renvoie ce pointeur. 
PVOID pSetEnvOnCreateParam
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	LPARAM lParam)			// taille de la zone m�moire � associer
	{
	PVOID pEnv = ((LPCREATESTRUCT)lParam)->lpCreateParams;
	SetWindowLong (hWndAssociee, GWL_USERDATA, (LPARAM)pEnv);
	return pEnv;
	}


//------------------------------------------------------------
// WM_INITDIALOG : enregistre le param�tre du DialogBoxParam
// sur le handle de fen�tre et renvoie ce pointeur. 
PVOID pSetEnvOnInitDialogParam
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	LPARAM lParam)			// taille de la zone m�moire � associer
	{
	SetWindowLong (hWndAssociee, GWL_USERDATA, lParam);
	return (PVOID)lParam;
	}

//------------------------------------------------------------
// WM_INITDIALOG : // Alloue et initialise un bloc de donn�es
// avec le param�tre du DialogBoxParam, l'enregistre sur le 
// handle de fen�tre et renvoie ce pointeur. 
PVOID pCreeEnvOnInitDialogParam
	(HWND hWndAssociee, // handle de fen�tre valide associ�
	DWORD dwTailleBloc, // taille de la zone m�moire � allouer et � recopier
	LPARAM lParam)			// lParam re�u avec le message WM_INITDIALOG
	{
	PVOID pEnv = pMemAlloue(dwTailleBloc);
	if (lParam)
		CopyMemory (pEnv, (PVOID)lParam, dwTailleBloc);
	else
		FillMemory (pEnv, dwTailleBloc, 0);
	SetWindowLong (hWndAssociee, GWL_USERDATA, (LPARAM)pEnv);
	return pEnv;
	}


//------------------------------------------------------------
// r�cup�re l'adresse du bloc de donn�es enregistr� par pCreeEnv
// renvoie NULL ou le pointeur sur les donn�es associ�
PVOID pGetEnv 
	(HWND hWndAssociee) // handle de fen�tre valide associ�
	{
	return (PVOID)GetWindowLong (hWndAssociee, GWL_USERDATA);
	}

//------------------------------------------------------------
// associe un pointeur � la fen�tre d�sign�e
// et renvoie l'ancien pointeur associ� (�ventuellement NULL)
PVOID pSelectEnv
	(HWND hWndAssociee,  // handle de fen�tre valide associ�
	PVOID pNouvelEnv)		// nouveau pointeur � associer
	{
	return (PVOID) SetWindowLong (hWndAssociee, GWL_USERDATA, (LPARAM)pNouvelEnv);
	}

//------------------------------------------------------------
// lib�re le bloc de donn�es enregistr� par pCreeEnv
// renvoie TRUE s'il n'y a pas ou plus de pointeur associ�.
BOOL bLibereEnv
	(HWND hWndAssociee)// handle de fen�tre valide associ�
	{
	BOOL	bRes = TRUE;
	PVOID pEnv = (PVOID)GetWindowLong(hWndAssociee,GWL_USERDATA);

	if (pEnv)
		{
		MemLibere (&pEnv); // $$ err ?
		SetWindowLong (hWndAssociee, GWL_USERDATA, 0);
		}
	return bRes;
	}

//------------------ fin EnvMan.c ------------------------------


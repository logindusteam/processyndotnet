//////////////////////////////////////////////////////////////////////
// Appli.h: interface for the CAppli class.
// JS Win32 21/8/98
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEMPAPPLI_H__4982EE24_38DD_11D2_A131_0000E8D90704__INCLUDED_)
#define AFX_TEMPAPPLI_H__4982EE24_38DD_11D2_A131_0000E8D90704__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Aide.h"

#define MAX_ABBREV_LANGUE 4
#define MAX_LANGUE 80


class CAppli  
	{
	public:
		typedef enum
			{
			MJA = 0,
			JMA = 1,
			AMJ = 2
			} eOrdreDate;
	// constructeur / destructeur
		CAppli();
		virtual ~CAppli();

	// m�thodes
	public:
		// mise � jour du hinst , du path de l'exe et du nom courants de l'application
		// met � jour la taille de l'�cran et la version courante d'OS
		void Init (HINSTANCE hInst, PCSTR pszNomAppliNouveau, PCSTR pszVersion);

		// enregistre le nouveau handle de la fen�tre principale de l'application
		// renvoie TRUE si hwndAppliNouveau est valide FALSE sinon
		BOOL bSetHWndPrincipal (HWND hwndAppliNouveau);
 
		// Mise � jour de la taille de l'�cran
		void GetTailleEcran (void);

		// Mise � jour des polices � utiliser � partir du code page � utiliser
		BOOL SetCodePagePourFont ();

		// Mise � jour du Nb de bits par pixel de l'�cran
		void GetNbBitsParPixel (void);

		void FormateDateInternationale (PSTR pszText, DWORD dwJour, DWORD dwMois, DWORD dwAn)const;

		// R�cup�re le nom de la Version d'OS couramment utilis�e
		void GetNomOS (PSTR pszNomOSDest);

	// propri�t�s
	public:
		HINSTANCE hinst;						// HINST process
		HINSTANCE hinstDllLng;			// HINST DLL infos langues
		HWND hwnd;									// HWND fen�tre principale de l'appli
		char szPathExe [MAX_PATH];  // Chemin courant de l'application
		char szNomExe[MAX_PATH];		// Nom de l'exe
		char szNom[MAX_PATH];				// Nom pour l'utilisateur de l'application
		char szVersion[MAX_PATH];		// Identification de la version
		char szPathInitial[MAX_PATH];// Chemin initial (par defaut) de l'application
		LCID lcid;									// Locale Identifier de l'interface courant
		char szAbbrevLangueUtilisateur[MAX_ABBREV_LANGUE];	// 
		char szLangueUtilisateur[MAX_LANGUE];	// 
		SIZE	sizEcran;		// Taille de l'�cran (Cf. GetTailleEcran())
		RECT rectEcran;		// Taille de l'�cran
		INT nNbBitsParPixel; // Nb de bits par pixels de l'�cran
		OSVERSIONINFOEX OSVersionInfoEx;	// Infos sur la version courante d'OS
		CHAR cSeparateurListe; // Caract�re utilis� comme s�parateur de liste type CSV
		DWORD dwCodePage; // Code page � utiliser pour les langages non unicodes
		BYTE byCharSetFont; // Jeu de caract�re ANSI � utiliser dans les polices type TURKISH_CHARSET; par d�faut ANSI_CHARSET
		eOrdreDate nOrdreDateInternationale;
		CAide m_Aide;
	private:
		void MiseAJourLangue (void);
	};

#endif // !defined(AFX_TEMPAPPLI_H__4982EE24_38DD_11D2_A131_0000E8D90704__INCLUDED_)



extern	CAppli Appli;// Param�tres courants de l'application





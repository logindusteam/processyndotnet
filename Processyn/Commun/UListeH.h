/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|*/
// UListeH.h
// Version Win32 9/6/97 par JS
// Permet de maintenir une liste de handles (Cf	DECLARE_HANDLE ())
// Remarques : 
// - Pas d'IPC.
// - Le parcours d'une liste peut passer par des handles anciennements ferm�s.
// - Les handles list�s ne sont pas g�r�s par cet utilitaire.
// - UTILISATION MULTITHREAD DE LISTE_H :
//	Il est de la responsabilit� de l'utilitaire d'appeler un des couples de 
//	fonctions d'acc�s pour permettre un acc�s multithread � la liste de handles.
//	Aucune des fonctions de travail sur les listes ne sont autoprotegees pour des acces multithread.

// Handle d'une liste d'objets
DECLARE_HANDLE (HLISTE_H);
typedef HLISTE_H * PHLISTE_H;

//------------------------------------------------------------------------------
// initialisation de la liste de handle avec une liste vide
// On fixe la valeur d'un handle invalide et si l'on veut un acc�s multi thread � la liste
BOOL bCreeListeH (PHLISTE_H phListeH, HANDLE handleInvalide, BOOL bAccesMultiThread);

// fermeture de la liste de handle (la fermeture des objets n'est PAS effectu�e)
BOOL bFermeListeH (PHLISTE_H phListeH);

//------------------------------------------------------------------------------
// Assure un acc�s multi thread en modification de la liste des handles.
void	AccesModifListeH (HLISTE_H hListeH);
void	FinAccesModifListeH	 (HLISTE_H hListeH);

// Assure un acc�s multi thread en lecture de la liste des handles.
void	AccesLectureListeH (HLISTE_H hListeH);
void	FinAccesLectureListeH	 (HLISTE_H hListeH);

// -----------------------------------------------------------------------------
// Recherche d'un handle dans la liste. Renvoie l'adresse du handle dans la liste ou NULL
// ATTENTION cette adresse n'est valable que tant qu'on n'a pas pas modifi� la liste des handles
PHANDLE phTrouveDansListeH (HLISTE_H hListeH, HANDLE handle);

// -----------------------------------------------------------------------------
// ajoute un handle � la liste des handles (remplace un handle invalide trouv� ou allonge la liste)
BOOL bAjouteDansListeH (HLISTE_H hListeH, HANDLE handle);

// -----------------------------------------------------------------------------
// Supprime un handle de la liste (le remplace par un handle invalide et raccourcit �ventuellement la liste)
BOOL bSupprimeDansListeH (HLISTE_H hListeH, HANDLE handle);

// -----------------------------------------------------------------------------
// R�cup�re le handle num�ro dwNHandle de la liste (de 0 � n) qui n'est pas n�cessairement valide
// Renvoie handleInvalide si hors limite
HANDLE hDansListeH (HLISTE_H hListeH, DWORD dwNHandle);

// -----------------------------------------------------------------------------
// Renvoie le nombre de handle ouverts et invalides maintenus dans la liste
DWORD dwNbHDansListeH (HLISTE_H hListeH);


// Progman.h gestion de process WIN 32

extern const PROCESS_INFORMATION PROCESS_INFORMATION_NULL;
#define TIME_OUT_ABORT_STANDARD 10000 // 10 secondes

// lance un process
BOOL  bProgExecute (PROCESS_INFORMATION * pProcessInformation, BOOL bExecutionSynchrone, PCSTR pszProgramme, PCSTR pszCommandes, 
	char TabPtrEnvironnement[][STR_MAX_CHAR_ARRAY], UINT uNbMaxEnvironnment, PCSTR pszCheminExecution);

// Lib�re les handles d'un process information ouvert
BOOL bProgLibereProcessInformation (PROCESS_INFORMATION * pProcessInformation);

// Arrete un process (avec abort si WM_CLOSE ne fait rien pendant un certain temps)
BOOL bProgArrete (PROCESS_INFORMATION * pProcessInformation, DWORD dwTimeoutAvantAbort);

// Rend actif un 32-Bit Process (or 16-bit process under Windows 95)
BOOL bProgActive (PROCESS_INFORMATION * pProcessInformation);

// Teste si un process est termin� et attends �ventuellement sa fin d'ex�cution
// et lib�re les handles de PROCESS_INFORMATION si termin�
BOOL  bProgFini (PROCESS_INFORMATION * pProcessInformation, BOOL bAttenteFin);

// Regarde si Process information est diff�rent de PROCESS_INFORMATION_NULL
BOOL bProgUtilise (const PROCESS_INFORMATION * pProcessInformation);


//---------------------------------------------------------------------------

// Contour.h
// Gestion de contours d'objets
DWORD d_around_line         (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_rectangle    (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_circle       (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_4_arc        (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_fill_4_arc   (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_4_arc_rect   (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_h_2_arc      (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_fill_h_2_arc (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_v_2_arc      (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_fill_v_2_arc (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_tri_rect     (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_h_tri_iso    (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_v_tri_iso    (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_h_vanne      (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_v_vanne      (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);
DWORD d_around_trend        (int x1, int y1, int x2, int y2, DWORD wCount, POINT *aptl);

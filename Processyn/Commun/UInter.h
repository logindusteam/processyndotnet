//---------------------------------------------------------------------------
//								UInter.h
// Utilitaire pour interpr�teurs Processyn
// JS 15/6/98 Win32
//
// Une gestion locale d'exception erreur de compil sera structur� ainsi
// __try 
//	{
//	... instructions dont on veut intercepter l'exception
//	}	
// __except (nRecupereErreur(GetExceptionInformation(), &Erreur))
//	{
//	GereErreur(Erreur);
//	}
//---------------------------------------------------------------------------

#ifndef UINTER_H
#define UINTER_H

//---------------------------------------------------------------------------
// Passe � l'UL suivante si elle existe sinon Erreur
_inline void ULSuivante (PINT pNUl, INT NbUls)
	{
	if (((*pNUl)+1) >= NbUls)
		ExcepErreurCompile (IDSERR_LA_DESCRIPTION_EST_INCOMPLETE);
	(*pNUl)++;
	}

//---------------------------------------------------------------------------
// Passe � l'UL suivante si elle existe (renvoie TRUE) sinon renvoie FALSE
_inline BOOL bULSuivante (PINT pNUl, INT NbUls)
	{
	BOOL bRet = FALSE;

	if (*pNUl < NbUls - 1)
		{
		(*pNUl)++;
		bRet = TRUE;
		}

	return bRet;
	}

//---------------------------------------------------------------------------
// Verifie qu'il n'existe pas d'uls en trop
_inline void ULsTerminees (INT NUl, INT NbUls)
	{
	if ((NUl+1) < NbUls)
		ExcepErreurCompile (IDSERR_LA_LIGNE_EST_TROP_LONGUE);
	}

//---------------------------------------------------------------
// Interception d'une exception EXCEPTION_ERR_COMPILE
// renvoie l'ordre de traiter le handler ou de continuer
_inline int nRecupereErreur (EXCEPTION_POINTERS * pExp, DWORD * pErreur)
	{
	int	nRes = EXCEPTION_CONTINUE_SEARCH;
	
	if (pExp->ExceptionRecord->ExceptionCode == EXCEPTION_ERR_COMPILE)
		{
		nRes = EXCEPTION_EXECUTE_HANDLER;
		*pErreur = pExp->ExceptionRecord->ExceptionInformation [0];
		}
	return nRes;
	} // nRecupereErreur

//---------------------------------------------------------------------------
// Macro de r�cup�ration d'une erreur envoy�e par une exception EXCEPTION_ERR_COMPILE
#define CATCH_ERREUR(Erreur) __except (nRecupereErreur (GetExceptionInformation(), &Erreur)){}


//---------------------------------------------------------------------------

#endif //UINTER_H

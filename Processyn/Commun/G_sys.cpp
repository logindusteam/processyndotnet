// G_sys.c 
// Version Win32

#include "stdafx.h"
#include <math.h>        // ceil()

#include "std.h"         // std types
#include "MemMan.h"
#include "UStr.h"      //

#include "G_Objets.h"
#include "DevMan.h"      // Fonctions Graphiques Device: d_ ()
#include "space.h"       // Fonctions Coordonn�es......: Spc ()
#include "Contour.h"

#include "Verif.h"
#include "g_sys.h"
VerifInit;

// -------------------------------------------------------------------------
typedef struct
  {
  HDEV		hDev;
  HSPACE	hSpc;
  } GSYS;
typedef GSYS * PGSYS;

// �l�ment Graphique
typedef struct
  {
  G_ACTION						nAction;
  G_COULEUR						nCouleurLigne;
  G_COULEUR						nCouleurRemplissage;
  G_STYLE_LIGNE				nStyleLigne;
  G_STYLE_REMPLISSAGE	nStyleRemplissage;
  DWORD								wPolice;
  DWORD								wPoliceStyle;
  char *							pszText;
  DWORD								wNbPoints;
	POINT *							pPoints;
	HWND								hwndControle;
  } ELEMENT_GSYS;
typedef ELEMENT_GSYS * PELEMENT_GSYS;

// constante point (0,0)
const POINT POINT_NULL = {0,0};

static const G_ACTION td_Rotations [G_NB_ACTIONS] =
  {
  G_ACTION_LIGNE,							  //G_ACTION_LIGNE							
  G_ACTION_RECTANGLE,						//G_ACTION_RECTANGLE					
  G_ACTION_RECTANGLE_PLEIN,			//G_ACTION_RECTANGLE_PLEIN		
  G_ACTION_CERCLE,							//G_ACTION_CERCLE							
  G_ACTION_CERCLE_PLEIN,				//G_ACTION_CERCLE_PLEIN				
  G_ACTION_QUART_ARC,						//G_ACTION_QUART_ARC          
  G_ACTION_QUART_CERCLE_PLEIN,  //G_ACTION_QUART_CERCLE_PLEIN 
  G_ACTION_V_DEMI_CERCLE,				//G_ACTION_H_DEMI_CERCLE      
  G_ACTION_V_DEMI_CERCLE_PLEIN, //G_ACTION_H_DEMI_CERCLE_PLEIN
  G_ACTION_H_DEMI_CERCLE,				//G_ACTION_V_DEMI_CERCLE      
  G_ACTION_H_DEMI_CERCLE_PLEIN, //G_ACTION_V_DEMI_CERCLE_PLEIN
  G_ACTION_TRI_RECT,						//G_ACTION_TRI_RECT						
  G_ACTION_TRI_RECT_PLEIN,			//G_ACTION_TRI_RECT_PLEIN			
  G_ACTION_V_TRI_ISO,						//G_ACTION_H_TRI_ISO					
  G_ACTION_V_TRI_ISO_PLEIN,			//G_ACTION_H_TRI_ISO_PLEIN		
  G_ACTION_H_TRI_ISO,						//G_ACTION_V_TRI_ISO					
  G_ACTION_H_TRI_ISO_PLEIN,			//G_ACTION_V_TRI_ISO_PLEIN		
  G_ACTION_V_VANNE,							//G_ACTION_H_VANNE						
  G_ACTION_V_VANNE_PLEIN,				//G_ACTION_H_VANNE_PLEIN			
  G_ACTION_H_VANNE,							//G_ACTION_V_VANNE						
  G_ACTION_H_VANNE_PLEIN,				//G_ACTION_V_VANNE_PLEIN			
  G_ACTION_QUART_CERCLE,				//G_ACTION_QUART_CERCLE				
  G_ACTION_QUART_ARC_PLEIN,			//G_ACTION_QUART_ARC_PLEIN		
  G_ACTION_H_TEXTE,						  //G_ACTION_H_TEXTE						
  G_ACTION_V_TEXTE,							//G_ACTION_V_TEXTE						
  G_ACTION_POLYGONE,						//G_ACTION_POLYGONE						
  G_ACTION_POLYGONE_PLEIN,			//G_ACTION_POLYGONE_PLEIN			
  G_ACTION_LISTE,								//G_ACTION_LISTE							
  G_ACTION_LISTE_PLEIN,         //G_ACTION_LISTE_PLEIN				
  G_ACTION_COURBE_T,						//G_ACTION_COURBE_T						
  G_ACTION_COURBE_C,						//G_ACTION_COURBE_C						
  G_ACTION_COURBE_A,						//G_ACTION_COURBE_A						
  G_ACTION_BARGRAPHE,						//G_ACTION_BARGRAPHE					
  G_ACTION_EDIT_TEXTE,					//G_ACTION_EDIT_TEXTE					
  G_ACTION_EDIT_NUM,						//G_ACTION_EDIT_NUM						
  G_ACTION_STATIC_TEXTE,				//G_ACTION_STATIC_TEXTE				
  G_ACTION_STATIC_NUM,					//G_ACTION_STATIC_NUM					
  G_ACTION_ENTREE_LOG,					//G_ACTION_ENTREE_LOG					
  G_ACTION_NOP,									//G_ACTION_NOP								
	G_ACTION_CONT_STATIC,					//G_ACTION_CONT_STATIC  			
	G_ACTION_CONT_EDIT,						//G_ACTION_CONT_EDIT    			
	G_ACTION_CONT_GROUP,					//G_ACTION_CONT_GROUP   			
	G_ACTION_CONT_BOUTON,					//G_ACTION_CONT_BOUTON  			
	G_ACTION_CONT_CHECK,					//G_ACTION_CONT_CHECK   			
	G_ACTION_CONT_RADIO,					//G_ACTION_CONT_RADIO   			
	G_ACTION_CONT_COMBO,					//G_ACTION_CONT_COMBO   			
	G_ACTION_CONT_LIST,						//G_ACTION_CONT_LIST    			
	G_ACTION_CONT_SCROLL_H,				//G_ACTION_CONT_SCROLL_H			
	G_ACTION_CONT_SCROLL_V,				//G_ACTION_CONT_SCROLL_V			
	G_ACTION_CONT_BOUTON_VAL			//G_ACTION_CONT_BOUTON_VAL		
	};


// --------------------------------------------------------------------------
// Table de correspondance entre les ACTIONS                 (index tableau)
//                            et le  Nb POINTS MAXI de l'action (contenu du tableau)
// --------------------------------------------------------------------------
static const DWORD td_MaxPoints [G_NB_ACTIONS] =
	{
	2,		//G_ACTION_LIGNE							
	2,		//G_ACTION_RECTANGLE					
	2,		//G_ACTION_RECTANGLE_PLEIN		
	2,		//G_ACTION_CERCLE							
	2,		//G_ACTION_CERCLE_PLEIN				
	2,		//G_ACTION_QUART_ARC          
	2,		//G_ACTION_QUART_CERCLE_PLEIN 
	2,		//G_ACTION_H_DEMI_CERCLE      
	2,		//G_ACTION_H_DEMI_CERCLE_PLEIN
	2,		//G_ACTION_V_DEMI_CERCLE      
	2,		//G_ACTION_V_DEMI_CERCLE_PLEIN
	2,		//G_ACTION_TRI_RECT						
	2,		//G_ACTION_TRI_RECT_PLEIN			
	2,		//G_ACTION_H_TRI_ISO					
	2,		//G_ACTION_H_TRI_ISO_PLEIN		
	2,		//G_ACTION_V_TRI_ISO					
	2,		//G_ACTION_V_TRI_ISO_PLEIN		
	2,		//G_ACTION_H_VANNE						
	2,		//G_ACTION_H_VANNE_PLEIN			
	2,		//G_ACTION_V_VANNE						
	2,		//G_ACTION_V_VANNE_PLEIN			
	2,		//G_ACTION_QUART_CERCLE				
	2,		//G_ACTION_QUART_ARC_PLEIN		
	2,		//G_ACTION_H_TEXTE						
	2,		//G_ACTION_V_TEXTE						
	65535,//G_ACTION_POLYGONE						
	65535,//G_ACTION_POLYGONE_PLEIN			
	1,		//G_ACTION_LISTE							
	1,		//G_ACTION_LISTE_PLEIN				
	2,		//G_ACTION_COURBE_T						
	2,		//G_ACTION_COURBE_C						
	2,		//G_ACTION_COURBE_A						
	2,		//G_ACTION_BARGRAPHE					
	2,		//G_ACTION_EDIT_TEXTE					
	2,		//G_ACTION_EDIT_NUM						
	2,		//G_ACTION_STATIC_TEXTE				
	2,		//G_ACTION_STATIC_NUM					
	2,		//G_ACTION_ENTREE_LOG					
	2,		//G_ACTION_NOP								
	2,		//G_ACTION_CONT_STATIC  			
	2,		//G_ACTION_CONT_EDIT    			
	2,		//G_ACTION_CONT_GROUP   			
	2,		//G_ACTION_CONT_BOUTON  			
	2,		//G_ACTION_CONT_CHECK   			
	2,		//G_ACTION_CONT_RADIO   			
	2,		//G_ACTION_CONT_COMBO   			
	2,		//G_ACTION_CONT_LIST    			
	2,		//G_ACTION_CONT_SCROLL_H			
	2,		//G_ACTION_CONT_SCROLL_V			
	2			//G_ACTION_CONT_BOUTON_VAL		
	};

// -------------------------------------------------------------------------
// En sortie *px1 <= *px2 et *py1 <= *py2 
static void normaliser_bi_point (LONG *px1, LONG *py1,LONG *px2, LONG *py2)
  {
  LONG nTemp;

  if ((*px1) > (*px2))
    {
    nTemp = *px2;
    *px2 = *px1;
    *px1 = nTemp;
    }
  if ((*py1) > (*py2))
    {
    nTemp = *py2;
    *py2 = *py1;
    *py1 = nTemp;
    }
  }

// ------------------------------------------------------------------------------------------------
// permute les x et les y si n�cessaire pour avoir (pPoint0->x <= pPoint1->x) et (pt0->y <= pPoint1->y)
void G_OrdonneBipoint (PPOINT pPoint0, PPOINT pPoint1)
	{
	// ordonne les x 
	if (pPoint0->x > pPoint1->x)
		{
		LONG	xTemp = pPoint1->x;
		
		pPoint1->x = pPoint0->x;
		pPoint0->x = xTemp;
		}

	// ordonne les y
	if (pPoint0->y > pPoint1->y)
		{
		LONG	yTemp = pPoint1->y;
		
		pPoint1->y = pPoint0->y;
		pPoint0->y = yTemp;
		}
	}



// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Fonctions locales pour calculer les distances
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// -------------------------------------------------------------------------
// renvoie le carr� de la distance entre deux points
static __inline DWORD DistPoint2 (const POINT *pPt1, const POINT *pPt2)
  {
  LONG dx = pPt1->x - pPt2->x;
  LONG dy = pPt1->y - pPt2->y;

  return (DWORD)(dx * dx) + (DWORD)(dy * dy);
  }

// -------------------------------------------------------------------------
// renvoie le carr� de la distance entre un point et un segment de droite 
// d�fini par ses extr�mit�s
static DWORD DistSegment (const POINT * pPt0, const POINT * pPtSegment1, const POINT * pPtSegment2)
  {
  POINT PtMin;
  POINT PtMax;
  POINT Pt;
  SIZE  Vector_1_2;
  LONG  iY_Of_X_Min;
  LONG  iY_Of_X_Max;
  LONG  iC1;
  LONG  iC2;
  LONG  iDenom;

  if (pPtSegment1->x < pPtSegment2->x)
    {
    PtMin.x     = pPtSegment1->x;
    iY_Of_X_Min = pPtSegment1->y;
    PtMax.x     = pPtSegment2->x;
    iY_Of_X_Max = pPtSegment2->y;
    }
  else
    {
    PtMin.x     = pPtSegment2->x;
    iY_Of_X_Min = pPtSegment2->y;
    PtMax.x     = pPtSegment1->x;
    iY_Of_X_Max = pPtSegment1->y;
    }

  if (pPtSegment1->y < pPtSegment2->y)
    {
    PtMin.y = pPtSegment1->y;
    PtMax.y = pPtSegment2->y;
    }
  else
    {
    PtMin.y = pPtSegment2->y;
    PtMax.y = pPtSegment1->y;
    }

  Vector_1_2.cx = pPtSegment2->x - pPtSegment1->x;
  Vector_1_2.cy = pPtSegment2->y - pPtSegment1->y;
  if (Vector_1_2.cx == 0)           //segment vertical ou nul
    {
    Pt.x = pPtSegment1->x;
    if (Vector_1_2.cy == 0)         //segment nul
      {
      Pt.y = pPtSegment1->y;
      }
    else                           //segment vertical non nul
      {
      if (pPt0->y < PtMin.y)      //point 0 en dessous du segment vertical non nul
        {
        Pt.y = PtMin.y;
        }
      else
        {
        if (pPt0->y > PtMax.y)    //point 0 au dessus du segment vertical non nul
          {
          Pt.y = PtMax.y;
          }
        else                      //point 0 au m�me niveau du segment vertical non nul
          {
          Pt.y = pPt0->y;
          }
        }
      }
    }
  else                            //segment non vertical et non nul
    {
    if (Vector_1_2.cy == 0)       //segment horizontal et non nul
      {
      Pt.y = pPtSegment1->y;
      if (pPt0->x < PtMin.x)      //point 0 � gauche du segment horizontal non nul
        {
        Pt.x = PtMin.x;
        }
      else
        {
        if (pPt0->x > PtMax.x)    //point 0 � droite du segment horizontal non nul
          {
          Pt.x = PtMax.x;
          }
        else                      //point 0 au m�me niveau du segment horizontal non nul
          {
          Pt.x = pPt0->x;
          }
        }
      }
    else                          //segment oblique non nul
      {
      iDenom = (Vector_1_2.cx * Vector_1_2.cx) + (Vector_1_2.cy * Vector_1_2.cy);
      iC1    = (pPtSegment2->x * pPtSegment1->y) - (pPtSegment1->x * pPtSegment2->y);
      iC2    = (Vector_1_2.cy * pPt0->y)       + (Vector_1_2.cx * pPt0->x);
      //
      Pt.x = ((Vector_1_2.cx * iC2) - (Vector_1_2.cy * iC1)) / iDenom;
      //
      if (Pt.x < PtMin.x)         //point 0 � 'gauche' du segment oblique
        {
        Pt.x = PtMin.x;
        Pt.y = iY_Of_X_Min;
        }
      else
        {
        if (Pt.x > PtMax.x)       //point 0 � 'droite' du segment oblique
          {
          Pt.x = PtMax.x;
          Pt.y = iY_Of_X_Max;
          }
        else                      //point 0 au 'niveau' du segment oblique
          {
          Pt.y = ((Vector_1_2.cx * iC1) + (Vector_1_2.cy * iC2)) / iDenom;
          }
        }
      }
    }

  return DistPoint2 (pPt0, &Pt);
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Fonctions permettant de r�cup�rer le contour d'un objet en tant que suite de points
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// -------------------------------------------------------------------------
static DWORD Around_Nop (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  return 0;
  }
// -------------------------------------------------------------------------
static DWORD Around_Line (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_line (aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_Box (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  POINT  aPoint [2];
  DWORD  wCountArround;

  if (wCount > 0)
    {
		// $$ incoh�rence d'ordre de passage des param�tres
    g_EncombrementElement ((HGSYS)pGSys, (HELEMENT)pElementGSys, &aPoint[0].x, &aPoint[0].y, &aPoint[1].x, &aPoint[1].y);
    SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
    }
  wCountArround = d_around_rectangle (
                                      aPoint[0].x, aPoint[0].y,
                                      aPoint[1].x, aPoint[1].y,
                                      wCount, aPt);
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_Circle (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_circle (
                                     aPoint[0].x, aPoint[0].y,
                                     aPoint[1].x, aPoint[1].y,
                                     wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_4_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_4_arc (
                                    aPoint[0].x, aPoint[0].y,
                                    aPoint[1].x, aPoint[1].y,
                                    wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_Fill_4_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_fill_4_arc (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y,wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_4_Arc_Rect (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

		if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_4_arc_rect (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y,wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_H_2_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_h_2_arc (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_Fill_H_2_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_fill_h_2_arc (
			aPoint[0].x, aPoint[0].y,aPoint[1].x, aPoint[1].y,wCount, aPt);
    }
  return wCountArround;
  }
// -------------------------------------------------------------------------
static DWORD Around_V_2_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_v_2_arc (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }
// -------------------------------------------------------------------------
static DWORD Around_Fill_V_2_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_fill_v_2_arc (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }
// -------------------------------------------------------------------------
static DWORD Around_Tri_Rect (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_tri_rect (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_H_Tri_Iso (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_h_tri_iso (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }
// -------------------------------------------------------------------------
static DWORD Around_V_Tri_Iso (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_v_tri_iso (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }
// -------------------------------------------------------------------------
static DWORD Around_H_Vanne (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_h_vanne (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }
// -------------------------------------------------------------------------
static DWORD Around_V_Vanne (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		POINT  aPoint [2];

    if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_v_vanne (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_Polygonne (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD wCountToTransfert;
  DWORD wCountArround = pElementGSys->wNbPoints;

  if (wCountArround < wCount)
    wCountToTransfert = wCountArround;
  else
    wCountToTransfert = wCount;

  if (wCountToTransfert > 0)
    {
    memcpy (aPt, pElementGSys->pPoints, wCountToTransfert * sizeof (POINT));
    SpcCvtPointsEnPixel (pGSys->hSpc, wCountToTransfert, aPt);
    }

  return wCountArround;
  }
// -------------------------------------------------------------------------
static DWORD Around_Fill_Polygonne (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD wCountArround = pElementGSys->wNbPoints + 1;
  DWORD wCountToTransfert;

  if (wCountArround < wCount)
    wCountToTransfert = wCountArround;
  else
    wCountToTransfert = wCount;

  if (wCountToTransfert > 0)
    {
    memcpy (aPt, pElementGSys->pPoints, (wCountToTransfert - 1) * sizeof (POINT));
    aPt [wCountToTransfert - 1] = aPt [0];
    SpcCvtPointsEnPixel (pGSys->hSpc, wCountToTransfert, aPt);
    }

  return wCountArround;
  }

// -------------------------------------------------------------------------
static DWORD Around_Trend (PGSYS pGSys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt)
  {
  DWORD  wCountArround = 0;

  if (pElementGSys->wNbPoints >= 2)
    {
		 POINT  aPoint [2];

   if (wCount > 0)
      {
      aPoint[0] = pElementGSys->pPoints [0];
      aPoint[1] = pElementGSys->pPoints [1];
      SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
      }
    wCountArround = d_around_trend (
			aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, wCount, aPt);
    }
  return wCountArround;
  }

// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// Fonctions locales pour dessin des Actions Graphiques
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
static void Action_Nop (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  }
// -------------------------------------------------------------------------
static void Action_Line (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_line ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Rectangle (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_rectangle ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_Rectangle (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_rectangle ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Circle (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_circle ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_Circle (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_circle ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_4_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_4_arc ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_4_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_4_arc ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_4_Arc_Rect (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_4_arc_rect ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_4_Arc_Rect (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_4_arc_rect ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_H_2_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_h_2_arc ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_H_2_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_h_2_arc ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_V_2_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_v_2_arc ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_V_2_Arc (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_v_2_arc ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Tri_Rect (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_tri_rect ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_Tri_Rect (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_tri_rect ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_H_Tri_Iso (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_h_arrow ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_H_Tri_Iso (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_h_arrow ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_V_Tri_Iso (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_v_arrow ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_V_Tri_Iso (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_v_arrow ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_H_Vanne (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_h_vanne ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_H_Vanne (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_h_vanne ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_V_Vanne (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_v_vanne ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_V_Vanne (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_v_vanne ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_H_Write (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_h_write ((HGSYS)pGSys, pElementGSys->nCouleurLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y,
                      pElementGSys->wPolice, pElementGSys->wPoliceStyle,
                      pElementGSys->pszText);
    }
  }
// -------------------------------------------------------------------------
static void Action_V_Write (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_v_write ((HGSYS)pGSys, pElementGSys->nCouleurLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y,
                      pElementGSys->wPolice, pElementGSys->wPoliceStyle,
                      pElementGSys->pszText);
    }
  }
// -------------------------------------------------------------------------
static void Action_Polygonne (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_polygonne ((HGSYS)pGSys, pElementGSys->nCouleurLigne, pElementGSys->nStyleLigne, 
			pElementGSys->wNbPoints, pElementGSys->pPoints);
    }
  }
// -------------------------------------------------------------------------
static void Action_Fill_Polygonne (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_fill_polygonne ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage, pElementGSys->nStyleRemplissage, 
			pElementGSys->wNbPoints, pElementGSys->pPoints);
    }
  }
// -------------------------------------------------------------------------
static void Action_List (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  }
// -------------------------------------------------------------------------
static void Action_Fill_List (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  }
// -------------------------------------------------------------------------
static void Action_Trend (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_trend ((HGSYS)pGSys, pElementGSys->nCouleurLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_BarGraf (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_bargraf ((HGSYS)pGSys, pElementGSys->nCouleurRemplissage,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }
// -------------------------------------------------------------------------
static void Action_EntryField (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_entry_field ((HGSYS)pGSys, pElementGSys->nCouleurLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y,
                          pElementGSys->wPolice, pElementGSys->wPoliceStyle,
                          pElementGSys->pszText);
    }
  }
// -------------------------------------------------------------------------
static void Action_StaticText (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_static_text ((HGSYS)pGSys, pElementGSys->nCouleurLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y,
                          pElementGSys->wPolice, pElementGSys->wPoliceStyle,
                          pElementGSys->pszText);
    }
  }
// -------------------------------------------------------------------------
static void Action_LogicEntry (PGSYS pGSys, PELEMENT_GSYS pElementGSys)
  {
  if (pElementGSys->wNbPoints >= 2)
    {
    g_logic_entry ((HGSYS)pGSys, pElementGSys->nCouleurLigne,
                   pElementGSys->pPoints [0].x, pElementGSys->pPoints [0].y,
                   pElementGSys->pPoints [1].x, pElementGSys->pPoints [1].y);
    }
  }

// -------------------------------------------------------------------------
// Ouverture G-sys
// -------------------------------------------------------------------------
HGSYS g_open (HWND hwndDessin)
  {
  PGSYS pGSys = NULL;
  HDEV hDev = d_open ();

  if (hDev != NULL)
    {
		// cr�ation d'un espace par d�faut
	  HSPACE hSpc = hSpcCree (1, 1, 1, 1);

    if (hSpc != NULL)
      {
      pGSys = (PGSYS)pMemAlloue (sizeof (GSYS));
      pGSys->hDev = hDev;
      pGSys->hSpc = hSpc;
		  d_set_param (pGSys->hDev, hwndDessin);
      }
    else
      d_close (&hDev);
    }

  return (HGSYS)pGSys;
  }

// -------------------------------------------------------------------------
// Fermeture G-sys
// -------------------------------------------------------------------------
void g_close (HGSYS * phgsys)
  {
  PGSYS pGSys = (PGSYS) *phgsys;

  SpcFerme (&pGSys->hSpc);
  d_close (&pGSys->hDev);
  MemLibere ((PVOID *)phgsys);
  }

// -------------------------------------------------------------------------
// Initialisations de G-sys
// -------------------------------------------------------------------------
void g_init (HGSYS hgsys)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  d_init (pGSys->hDev);
  }

// -------------------------------------------------------------------------
// Fermeture de G-sys
// -------------------------------------------------------------------------
void g_fini (HGSYS hgsys)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  d_fini (pGSys->hDev);
  }

// -------------------------------------------------------------------------
// Sp�cifie l'origine du DC (utilis� pour l'alignement des brosses sous W95)
void g_set_origine_dc (HGSYS hgsys, POINT ptOrigineClientPix)
	{
  PGSYS pGSys = (PGSYS)  hgsys;

  d_set_origine_dc (pGSys->hDev, ptOrigineClientPix);
	}

// -------------------------------------------------------------------------
// Demande le hwnd associ� au HGSYS
// -------------------------------------------------------------------------
HWND g_get_hwnd (HGSYS hgsys)
	{
  PGSYS pGSys = (PGSYS)  hgsys;

	return d_get_hwnd(pGSys->hDev);
	}

// -------------------------------------------------------------------------
// Demande le hdc associ� au HGSYS
// -------------------------------------------------------------------------
HDC g_get_hdc (HGSYS hgsys)
	{
  PGSYS pGSys = (PGSYS)  hgsys;

	return d_get_hdc(pGSys->hDev);
	}

// ------------------------------------------------------------------------
// D�finition de l'echelle de coordonn�es
// ------------------------------------------------------------------------
void g_set_page_units (HGSYS hgsys, LONG XLengthPage, LONG YLengthPage, LONG XLengthWorkSpace, LONG YLengthWorkSpace)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  //Espace Pixel      = SPC_PIXEL = (xy)WorkSpace
  //Espace G�n�ralis� = SPC_VIRTUEL      = (xy)Page
  SpcModifieEspacePixel (pGSys->hSpc, XLengthWorkSpace, YLengthWorkSpace);
  SpcModifieEspaceVirtuel (pGSys->hSpc, XLengthPage, YLengthPage);
  }

// ------------------------------------------------------------------------
// Origine de l'echelle de coordonn�es
// ------------------------------------------------------------------------
void g_set_x_origine (HGSYS hgsys, LONG x)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  SpcSetXOrigineVirtuel (pGSys->hSpc, x);
  }

// ------------------------------------------------------------------------
void g_set_y_origine (HGSYS hgsys, LONG y)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  SpcSetYOrigineVirtuel (pGSys->hSpc, y);
  }

// ------------------------------------------------------------------------
void g_set_origine (HGSYS hgsys, LONG x, LONG y)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  SpcSetXOrigineVirtuel (pGSys->hSpc, x);
  SpcSetYOrigineVirtuel (pGSys->hSpc, y);
  }


// ------------------------------------------------------------------------
void g_get_origin (HGSYS hgsys, LONG *px, LONG *py)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  SpcGetOrigineVirtuel (pGSys->hSpc, px, py);
  }

// ------------------------------------------------------------------------
void g_deplace_origine (HGSYS hgsys, LONG dx, LONG dy, LONG * OldOf7X0, LONG *OldOf7Y0)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  *OldOf7X0 = lSpcDeplaceXOrigineVirtuel (pGSys->hSpc, dx);
  *OldOf7Y0 = lSpcDeplaceYOrigineVirtuel (pGSys->hSpc, dy);
  }


// ------------------------------------------------------------------------
// Nom du document
// ------------------------------------------------------------------------
void g_set_document_name (HGSYS hgsys, char *pszName, DWORD wDocType)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  d_set_document_name (pGSys->hDev, pszName, wDocType);
  }

DWORD g_get_document_name (HGSYS hgsys, char *pszName)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return (d_get_document_name (pGSys->hDev, pszName));
  }

// ------------------------------------------------------------------------
// Choix du p�riph�rique de sortie
// ------------------------------------------------------------------------
void * g_select_device (HGSYS hgsys, DWORD Device)
  {
  PGSYS pGSys = (PGSYS) hgsys;
  void * hdl = d_select_device (pGSys->hDev, Device);

  return hdl;
  }

// -------------------------------------------------------------------------
void g_get_device_limits (HGSYS hgsys, LONG *x0, LONG *y0, LONG *cx, LONG *cy)
  {
  PGSYS pGSys = (PGSYS) hgsys;
  POINT  aPoint [2];

  d_get_device_limits (pGSys->hDev, &aPoint[0].x, &aPoint[0].y, &aPoint[1].x, &aPoint[1].y);
  SpcCvtPointsEnVirtuel (pGSys->hSpc, 2, aPoint);
  (*x0) = aPoint[0].x;
  (*y0) = aPoint[0].y;
  (*cx) = aPoint[1].x;
  (*cy) = aPoint[1].y;
  }

// -------------------------------------------------------------------------
void g_delete_handle (HGSYS hgsys, void *hdl, DWORD wHdlType)
  {
  PGSYS pGSys = (PGSYS) hgsys;

  d_delete_handle (pGSys->hDev, hdl, wHdlType);
  }

// -------------------------------------------------------------------------
void g_draw_handle (HGSYS hgsys, void *hdl, DWORD wHdlType, DWORD wAdaptHdl, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS) hgsys;
  POINT  aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_draw_handle (pGSys->hDev, hdl, wHdlType, wAdaptHdl, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

// -------------------------------------------------------------------------
void g_save_handle (HGSYS hgsys, void *hdl, DWORD wHdlType)
  {
  PGSYS pGSys = (PGSYS) hgsys;

  d_save_handle (pGSys->hDev, hdl, wHdlType);
  }

// -------------------------------------------------------------------------
// Verbes de Dessins
// -------------------------------------------------------------------------

// ------------------------------------------------------------------------
// Effectue un ::InvalidateRect sur un rectangle ou sur tout le device (prect NULL)
void g_InvalidateRect   (HGSYS hgsys, const RECT * prect)
  {
  PGSYS 	pGSys = (PGSYS) hgsys;

	// Redessine tout ?
	if (prect)
		{
		// non => taille de l'�l�ment non nulle ?
		if ((prect->left != prect->right) || (prect->top != prect->bottom))
			{
			// oui => conversion en coordonn�es device
			RECT	rectTemp = * prect;

		  Spc_CvtPtEnPixel (pGSys->hSpc, &rectTemp.left, &rectTemp.top);
		  Spc_CvtPtEnPixel (pGSys->hSpc, &rectTemp.right, &rectTemp.bottom);

			//invalidation de la zone dans le device
			d_InvalidateRect (pGSys->hDev, &rectTemp);
			}
		}
	else
		// oui => redessine tout
		d_InvalidateRect (pGSys->hDev, prect);
  }

// -------------------------------------------------------------------------
void g_line (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_line (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

// -------------------------------------------------------------------------
void g_rectangle (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_rectangle (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_rectangle (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_rectangle (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_circle (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_circle (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_circle (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_circle (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_4_arc (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_4_arc (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_4_arc (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_4_arc (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_4_arc_rect (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_4_arc_rect (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_4_arc_rect (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_4_arc_rect (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_h_2_arc (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_h_2_arc (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_h_2_arc (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_h_2_arc (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_v_2_arc (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_v_2_arc (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_v_2_arc (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_v_2_arc (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_tri_rect (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_tri_rect (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_tri_rect (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_tri_rect (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_h_arrow (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_h_arrow (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_h_arrow (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_h_arrow (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_v_arrow (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_v_arrow (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_v_arrow (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_v_arrow (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_h_vanne (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_h_vanne (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_h_vanne (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_h_vanne (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_v_vanne (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_v_vanne (pGSys->hDev, nCouleur, nStyleLigne, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_fill_v_vanne (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_fill_v_vanne (pGSys->hDev, nCouleur, nStyleRemplissage, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_h_write (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD police, DWORD policestyle, char *pszText)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  //SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
	// origine
	Spc_CvtPtEnPixel (pGSys->hSpc, &aPoint[0].x, &aPoint[0].y);
	// attributs car
	Spc_CvtSizeEnPixel (pGSys->hSpc, &aPoint[1].x, &aPoint[1].y);
  d_h_write (pGSys->hDev, nCouleur, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, police, policestyle, pszText);
  }

void g_v_write (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD police, DWORD policestyle, char *pszText)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  //SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
	// origine
	Spc_CvtPtEnPixel (pGSys->hSpc, &aPoint[0].x, &aPoint[0].y);
	// attributs car
	Spc_CvtSizeEnPixel (pGSys->hSpc, &aPoint[1].x, &aPoint[1].y);
  d_v_write (pGSys->hDev, nCouleur, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, police, policestyle, pszText);
  }

void g_polygonne (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, DWORD nb_points, PPOINT td_points)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  SpcCvtPointsEnPixel (pGSys->hSpc, nb_points, td_points);
  d_poly_line (pGSys->hDev, nCouleur, nStyleLigne, nb_points, td_points);
  SpcCvtPointsEnVirtuel (pGSys->hSpc, nb_points, td_points);
  }

void g_fill_polygonne (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, DWORD nb_points, PPOINT td_points)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  SpcCvtPointsEnPixel (pGSys->hSpc, nb_points, td_points);
  d_fill_poly_line (pGSys->hDev, nCouleur, nStyleRemplissage, nb_points, td_points);
  SpcCvtPointsEnVirtuel (pGSys->hSpc, nb_points, td_points);
  }

void g_trend (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_trend (pGSys->hDev, nCouleur, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_bargraf (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_bargraf (pGSys->hDev, nCouleur, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

void g_entry_field (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD police, DWORD policestyle, char *pszText)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_entryfield (pGSys->hDev, nCouleur, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, police, policestyle, pszText);
  }

void g_static_text (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD police, DWORD policestyle, char *pszText)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_static_text (pGSys->hDev, nCouleur, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y, police, policestyle, pszText);
  }

void g_logic_entry (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT     aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_logic_entry (pGSys->hDev, nCouleur, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//					Fonctions de travail sur un GElement
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// pour optimisation un Nb de points mini est toujours allou� pour un �l�ment
#define NB_POINTS_BUFF_ELEMENT_MIN 2

// ------------------------------------------------------------------------
HELEMENT hGElementCree (void)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS)pMemAlloueInit0 (sizeof (ELEMENT_GSYS));

  pElementGSys->pPoints  = (PPOINT)pMemAlloueInit0 (NB_POINTS_BUFF_ELEMENT_MIN * sizeof (POINT));
  pElementGSys->pszText  = (PSTR)pMemAlloue (sizeof (char));
  (pElementGSys->pszText)[0] = '\0';
  pElementGSys->nAction  = G_ACTION_NOP;

  return (HELEMENT)pElementGSys;
  }

// ------------------------------------------------------------------------
void GElementReset (HELEMENT helement)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  pElementGSys->nAction							= G_ACTION_NOP;
  pElementGSys->nCouleurLigne       = G_BLACK;
  pElementGSys->nCouleurRemplissage = G_BLACK;
  pElementGSys->nStyleLigne					= G_STYLE_LIGNE_CONTINUE;
  pElementGSys->nStyleRemplissage					= G_STYLE_REMPLISSAGE_PLEIN;
  pElementGSys->wPolice							= 0;
  pElementGSys->wPoliceStyle				= 0;
  MemRealloue ((PVOID *)(&pElementGSys->pszText), sizeof (char));
  (pElementGSys->pszText)[0]				= '\0';
  pElementGSys->wNbPoints						= 0;

	// Ram�ne si n�cessaire le buffer au nombre minimal de points
	if (uMemTaille(pElementGSys->pPoints) != (NB_POINTS_BUFF_ELEMENT_MIN * sizeof (POINT)))
		MemRealloue ((PVOID *)(&pElementGSys->pPoints), NB_POINTS_BUFF_ELEMENT_MIN * sizeof (POINT));
  }

// ------------------------------------------------------------------------
BOOL bGElementNop (HELEMENT helement)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  return pElementGSys->nAction == G_ACTION_NOP;
  }

// ------------------------------------------------------------------------
void GElementFerme (HELEMENT * phelement)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) *phelement;

  MemLibere ((PVOID *)(&pElementGSys->pszText));
  MemLibere ((PVOID *)(&pElementGSys->pPoints));
  MemLibere ((PVOID *)(phelement));
  }

// ------------------------------------------------------------------------
void GElementSetAction (HELEMENT helement, G_ACTION Action)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

	// Je conc�de que cette ligne est un peu charg�e - JS
	pElementGSys->nAction = ((Action < G_NB_ACTIONS) ? Action : (VerifWarningExit, G_ACTION_NOP));
  }

// ------------------------------------------------------------------------
void GElementSetCouleurLigne (HELEMENT helement, G_COULEUR nCouleurLigne)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  pElementGSys->nCouleurLigne = nCouleurLigne;
  }

// ------------------------------------------------------------------------
void GElementSetCouleurRemplissage (HELEMENT helement, G_COULEUR nCouleurRemplissage)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  pElementGSys->nCouleurRemplissage = nCouleurRemplissage;
  }

// ------------------------------------------------------------------------
void GElementSetCouleur (HELEMENT helement, G_COULEUR nCouleur)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  pElementGSys->nCouleurLigne = nCouleur;
  pElementGSys->nCouleurRemplissage = nCouleur;
  }

// ------------------------------------------------------------------------
void GElementSetCouleurs (HELEMENT helement, G_COULEUR nCouleurLigne, G_COULEUR nCouleurRemplissage)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  pElementGSys->nCouleurLigne = nCouleurLigne;
  pElementGSys->nCouleurRemplissage = nCouleurRemplissage;
  }

// ------------------------------------------------------------------------
void GElementSetStyleRemplissage (HELEMENT helement, G_STYLE_REMPLISSAGE nStyleRemplissage)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  pElementGSys->nStyleRemplissage = nStyleRemplissage;
  }
// ------------------------------------------------------------------------
void GElementSetStyleLigne (HELEMENT helement, G_STYLE_LIGNE nStyleLigne)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  pElementGSys->nStyleLigne = nStyleLigne;
  }

// ------------------------------------------------------------------------
void GElementSetStyles (HELEMENT helement, G_STYLE_LIGNE nStyleLigne, G_STYLE_REMPLISSAGE nStyleRemplissage)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  pElementGSys->nStyleLigne = nStyleLigne;
  pElementGSys->nStyleRemplissage = nStyleRemplissage;
  }

// ------------------------------------------------------------------------
void GElementSetStyleTexte (HELEMENT helement, DWORD Police, DWORD PoliceStyle)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  pElementGSys->wPolice      = Police;
  pElementGSys->wPoliceStyle = PoliceStyle;
  }

// ------------------------------------------------------------------------
void GElementSetTexte (HELEMENT helement, char *pszText)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  MemLibere ((PVOID *)(&pElementGSys->pszText));
  pElementGSys->pszText = (PSTR)pMemAlloue ((StrLength (pszText) + 1) * (sizeof (char)));
  StrCopy (pElementGSys->pszText, pszText);
  }

// ------------------------------------------------------------------------
// Ajoute un point au hElement et l'initialise si le nombre de points permis n'est pas d�pass�
BOOL GElementAjoutePoint (HGSYS hgsys, HELEMENT helement, LONG x, LONG y)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  BOOL					bAjout = (td_MaxPoints [pElementGSys->nAction] > pElementGSys->wNbPoints);

  if (bAjout)
    {
    pElementGSys->wNbPoints++;

		// r�allocation du buffer de points que s'il d�passe 2 points
		if (pElementGSys->wNbPoints > NB_POINTS_BUFF_ELEMENT_MIN)
			MemRealloue ((PVOID *)(&pElementGSys->pPoints), pElementGSys->wNbPoints * sizeof (POINT));

    // $$ Fonctions de map �ventuellement inutiles
		g_map_point_page (hgsys, &x, &y);
    pElementGSys->pPoints [pElementGSys->wNbPoints - 1].x = x;
    pElementGSys->pPoints [pElementGSys->wNbPoints - 1].y = y;
    }

  return bAjout;
  }

// ------------------------------------------------------------------------
// Met deux points dans le hElement et les initialise si le nombre de points permis n'est pas d�pass�
BOOL GElementResetBiPoint (HGSYS hgsys, HELEMENT helement, const POINT * pPoint0, const POINT * pPoint1)
//BOOL GElementResetBiPoint (HGSYS hgsys, HELEMENT helement, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
	BOOL bOk = FALSE;

	// Alloue 2 points
  pElementGSys->wNbPoints = 2;

	if (uMemTaille(pElementGSys->pPoints) != (2 * sizeof (POINT)))
		MemRealloue ((PVOID *)(&pElementGSys->pPoints), 2 * sizeof (POINT));

	// 2 points permis pour cet �l�ment ?
  if (td_MaxPoints [pElementGSys->nAction] >= 2)
    {
		// oui => initialise les deux points
		bOk = TRUE;

    // recopie les 2 points et les aligne sur les pixels $$ Fonctions de map �ventuellement inutiles
    pElementGSys->pPoints [0] = *pPoint0;
		g_map_point_page (hgsys, &pElementGSys->pPoints [0].x, &pElementGSys->pPoints [0].y);

    pElementGSys->pPoints [1] = *pPoint1;
		g_map_point_page (hgsys, &pElementGSys->pPoints [1].x, &pElementGSys->pPoints [1].y);
    }
		
  return bOk;
  }

// ------------------------------------------------------------------------
void GElementSetDernierPoint (HGSYS hgsys, HELEMENT helement, LONG x, LONG y)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  if (pElementGSys->wNbPoints)
    {
    g_map_point_page (hgsys, &x, &y);
    pElementGSys->pPoints [pElementGSys->wNbPoints - 1].x = x;
    pElementGSys->pPoints [pElementGSys->wNbPoints - 1].y = y;
    }
  }

// ------------------------------------------------------------------------
void GElementDeplaceDernierPoint (HGSYS hgsys, HELEMENT helement, LONG dx, LONG dy)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  if (pElementGSys->wNbPoints)
    {
		POINT * pPoint = &pElementGSys->pPoints [pElementGSys->wNbPoints - 1];

    g_add_points_page (hgsys, &pPoint->x, &pPoint->y, dx, dy);
    }
  }

// ------------------------------------------------------------------------
void g_element_move_delta (HGSYS hgsys, HELEMENT helement, LONG dx, LONG dy)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  if (pElementGSys->wNbPoints)
    {
		POINT * pPoint = &pElementGSys->pPoints [pElementGSys->wNbPoints - 1];

    g_add_deltas_page (hgsys, &pPoint->x, &pPoint->y, dx, dy);
    }
  }

// ------------------------------------------------------------------------
// d�place tous les points d'un �l�ment
void g_element_move (HGSYS hgsys, HELEMENT helement, LONG dx, LONG dy)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  DWORD           IndexPoint;

  for (IndexPoint = 0; IndexPoint < pElementGSys->wNbPoints; IndexPoint++)
    {
		POINT * pPoint = &pElementGSys->pPoints [IndexPoint];

    g_add_points_page (hgsys, &pPoint->x, &pPoint->y, dx, dy);
    }
  }

// ------------------------------------------------------------------------
void GElementSupprimePoints (HELEMENT helement)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  pElementGSys->wNbPoints = 0;

	if (uMemTaille(pElementGSys->pPoints) != (NB_POINTS_BUFF_ELEMENT_MIN * sizeof (POINT)))
		MemRealloue ((PVOID *)(&pElementGSys->pPoints), NB_POINTS_BUFF_ELEMENT_MIN * sizeof (POINT));
  }

// ------------------------------------------------------------------------
// fait �ventuellement alterner un objet de plein � vide et inversement
BOOL bGElementChangeRemplissage (HGSYS hgsys, HELEMENT helement)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
	BOOL	bOk = FALSE;

	// Table de correspondance entre les ACTIONS et leurs pendants plein/vide
	static const G_ACTION anChangeRemplissageAction [G_NB_ACTIONS] =
		{
		G_ACTION_LIGNE,								//G_ACTION_LIGNE							
		G_ACTION_RECTANGLE_PLEIN,			//G_ACTION_RECTANGLE					
		G_ACTION_RECTANGLE,						//G_ACTION_RECTANGLE_PLEIN		
		G_ACTION_CERCLE_PLEIN,				//G_ACTION_CERCLE							
		G_ACTION_CERCLE,							//G_ACTION_CERCLE_PLEIN				
		G_ACTION_QUART_ARC_PLEIN,			//G_ACTION_QUART_ARC          
		G_ACTION_QUART_CERCLE,				//G_ACTION_QUART_CERCLE_PLEIN 
		G_ACTION_H_DEMI_CERCLE_PLEIN, //G_ACTION_H_DEMI_CERCLE      
		G_ACTION_H_DEMI_CERCLE,				//G_ACTION_H_DEMI_CERCLE_PLEIN
		G_ACTION_V_DEMI_CERCLE_PLEIN, //G_ACTION_V_DEMI_CERCLE      
		G_ACTION_V_DEMI_CERCLE,				//G_ACTION_V_DEMI_CERCLE_PLEIN
		G_ACTION_TRI_RECT_PLEIN,			//G_ACTION_TRI_RECT						
		G_ACTION_TRI_RECT,						//G_ACTION_TRI_RECT_PLEIN			
		G_ACTION_H_TRI_ISO_PLEIN,			//G_ACTION_H_TRI_ISO					
		G_ACTION_H_TRI_ISO,						//G_ACTION_H_TRI_ISO_PLEIN		
		G_ACTION_V_TRI_ISO_PLEIN,			//G_ACTION_V_TRI_ISO					
		G_ACTION_V_TRI_ISO,						//G_ACTION_V_TRI_ISO_PLEIN		
		G_ACTION_H_VANNE_PLEIN,				//G_ACTION_H_VANNE						
		G_ACTION_H_VANNE,							//G_ACTION_H_VANNE_PLEIN			
		G_ACTION_V_VANNE_PLEIN,				//G_ACTION_V_VANNE						
		G_ACTION_V_VANNE,							//G_ACTION_V_VANNE_PLEIN			
		G_ACTION_QUART_CERCLE_PLEIN,  //G_ACTION_QUART_CERCLE				
		G_ACTION_QUART_ARC,						//G_ACTION_QUART_ARC_PLEIN		
		G_ACTION_H_TEXTE,							//G_ACTION_H_TEXTE						
		G_ACTION_V_TEXTE,							//G_ACTION_V_TEXTE						
		G_ACTION_POLYGONE_PLEIN,			//G_ACTION_POLYGONE						
		G_ACTION_POLYGONE,						//G_ACTION_POLYGONE_PLEIN			
		G_ACTION_LISTE,								//G_ACTION_LISTE							// pas de remplissage de liste
		G_ACTION_LISTE,								//G_ACTION_LISTE_PLEIN				
		G_ACTION_COURBE_T,						//G_ACTION_COURBE_T						
		G_ACTION_COURBE_C,						//G_ACTION_COURBE_C						
		G_ACTION_COURBE_A,						//G_ACTION_COURBE_A						
		G_ACTION_BARGRAPHE,						//G_ACTION_BARGRAPHE					
		G_ACTION_EDIT_TEXTE,					//G_ACTION_EDIT_TEXTE					
		G_ACTION_EDIT_NUM,						//G_ACTION_EDIT_NUM						
		G_ACTION_STATIC_TEXTE,				//G_ACTION_STATIC_TEXTE				
		G_ACTION_STATIC_NUM,					//G_ACTION_STATIC_NUM					
		G_ACTION_ENTREE_LOG,					//G_ACTION_ENTREE_LOG					
		G_ACTION_NOP,									//G_ACTION_NOP								
		G_ACTION_CONT_STATIC,					//G_ACTION_CONT_STATIC  			
		G_ACTION_CONT_EDIT,						//G_ACTION_CONT_EDIT    			
		G_ACTION_CONT_GROUP,					//G_ACTION_CONT_GROUP   			
		G_ACTION_CONT_BOUTON,					//G_ACTION_CONT_BOUTON  			
		G_ACTION_CONT_CHECK,					//G_ACTION_CONT_CHECK   			
		G_ACTION_CONT_RADIO,					//G_ACTION_CONT_RADIO   			
		G_ACTION_CONT_COMBO,					//G_ACTION_CONT_COMBO   			
		G_ACTION_CONT_LIST,						//G_ACTION_CONT_LIST    			
		G_ACTION_CONT_SCROLL_H,				//G_ACTION_CONT_SCROLL_H			
		G_ACTION_CONT_SCROLL_V,				//G_ACTION_CONT_SCROLL_V			
		G_ACTION_CONT_BOUTON_VAL			//G_ACTION_CONT_BOUTON_VAL		
	};

	// Action valide ?
	if (pElementGSys->nAction < G_NB_ACTIONS)
		{
		G_ACTION	nNouvelleAction = anChangeRemplissageAction [pElementGSys->nAction];

		// oui => nouvelle action diff�rente ?
		if (nNouvelleAction != pElementGSys->nAction)
			{
			// oui => nouvelle action prise en compte
			pElementGSys->nAction = nNouvelleAction;
			bOk = TRUE;
			}
		}

	return bOk;
  }

// ------------------------------------------------------------------------
void g_element_miror (HGSYS hgsys, HELEMENT helement, UINT Axes, LONG xAxe, LONG yAxe)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  DWORD         IndexPoint;
  LONG          xtmp;
  LONG          ytmp;
	DWORD					NbPoints;
  LONG          xAxeDevice = xAxe;
  LONG          yAxeDevice = yAxe;

  g_convert_point_page_to_dev (hgsys, &xAxeDevice, &yAxeDevice);
  xAxeDevice += xAxeDevice;
  yAxeDevice += yAxeDevice;

	// L'�l�ment supporte la sym�trie (<=> la rotation aujourd'hui) ?
	if (bAttributsActionGr (pElementGSys->nAction, G_ATTRIBUT_SUPPORTE_SYMETRIE))
		// oui => sym�trise tous ses points
		NbPoints = pElementGSys->wNbPoints;
	else
		// Non => sym�trise juste son point d'ancrage $$ SALE valable pour les textes
		NbPoints = 1;

  for (IndexPoint = 0; IndexPoint < NbPoints; IndexPoint++)
    {
		POINT * pPoint = &pElementGSys->pPoints [IndexPoint];

    xtmp = pPoint->x;
    ytmp = pPoint->y;
    g_convert_point_page_to_dev (hgsys, &xtmp, &ytmp);
    if (Axes & c_G_X_MIRROR)
      xtmp = xAxeDevice - xtmp;
    if (Axes & c_G_Y_MIRROR)
      {
      ytmp = yAxeDevice - ytmp;
      }
    g_convert_point_dev_to_page (hgsys, &xtmp, &ytmp);
    pPoint->x = xtmp;
    pPoint->y = ytmp;
    }
  }

// ------------------------------------------------------------------------
void g_element_rotate (HGSYS hgsys, HELEMENT helement, LONG xCentre, LONG yCentre, UINT NbQuartTours)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  DWORD         IndexPoint;
  DWORD         NbPoints;

  switch (NbQuartTours % 4)
    {
    case 0:
      break;
    case 1:
			if (bAttributsActionGr (pElementGSys->nAction, G_ATTRIBUT_SUPPORTE_ROTATION))
				NbPoints = pElementGSys->wNbPoints;
			else
				NbPoints = 1;

			Verif (pElementGSys->nAction < G_NB_ACTIONS);
      pElementGSys->nAction = td_Rotations [pElementGSys->nAction];
      for (IndexPoint = 0; IndexPoint < NbPoints; IndexPoint++)
        {
				POINT * pPoint = &pElementGSys->pPoints [IndexPoint];
				LONG xTemp = pPoint->x;

        pPoint->x = xCentre - pPoint->y + yCentre;
        pPoint->y = yCentre + xTemp - xCentre;
        }
      break;
    case 2:
      g_element_miror (hgsys, helement, c_G_XY_MIRROR, xCentre, yCentre);
      break;
    case 3:
			if (bAttributsActionGr (pElementGSys->nAction, G_ATTRIBUT_SUPPORTE_ROTATION))
				NbPoints = pElementGSys->wNbPoints;
			else
				NbPoints = 1;
      pElementGSys->nAction = td_Rotations [pElementGSys->nAction];
      for (IndexPoint = 0; IndexPoint < NbPoints; IndexPoint++)
        {
				POINT * pPoint = &pElementGSys->pPoints [IndexPoint];
				LONG xTemp = pPoint->x;

        pPoint->x = xCentre + pPoint->y - yCentre;
        pPoint->y = yCentre - xTemp + xCentre;
        }
      break;
    }
  }

// ------------------------------------------------------------------------
void g_element_zoom (HGSYS hgsys, HELEMENT helement, LONG xRef, LONG yRef,
										 LONG cxNew, LONG cxOld, LONG cyNew, LONG cyOld)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  DWORD           IndexPoint;
  LONG            xReftmp = xRef;
  LONG            yReftmp = yRef;

  g_convert_point_page_to_dev (hgsys, &xReftmp, &yReftmp);

	// Boucle pour zoomer chaque point
  for (IndexPoint = 0; IndexPoint < pElementGSys->wNbPoints; IndexPoint++)
    {
		LONG	oldx = pElementGSys->pPoints [IndexPoint].x;
		LONG	oldy = pElementGSys->pPoints [IndexPoint].y;
		LONG	newx;
		LONG	newy;

    g_convert_point_page_to_dev (hgsys, &oldx, &oldy);

    if (cxOld != 0)
      {
			double  fnewx;

      newx = (oldx - xReftmp) * (cxNew - cxOld);
      fnewx = ((double) newx) / ((double) cxOld);
      if (fnewx >= 0)
        {
        fnewx = ceil (fnewx);
        }
      else
        {
        fnewx = floor (fnewx);
        }
      newx = oldx + (LONG)fnewx;
      }
    else
      {
      newx = oldx;
      }

    if (cyOld != 0)
      {
			double  fnewy;

      newy = (oldy - yReftmp) * (cyNew - cyOld);
      fnewy = ((double) newy) / ((double) cyOld);
      if (fnewy >= 0)
        {
        fnewy = ceil (fnewy);
        }
      else
        {
        fnewy = floor (fnewy);
        }
      newy = oldy + (LONG)fnewy;
      }
    else
      {
      newy = oldy;
      }

    g_convert_point_dev_to_page (hgsys, &newx, &newy);
    pElementGSys->pPoints [IndexPoint].x = newx;
    pElementGSys->pPoints [IndexPoint].y = newy;
    } // for (IndexPoint = 0; IndexPoint < pElementGSys->wNbPoints; IndexPoint++)
  } // g_element_zoom

// ------------------------------------------------------------------------
// Dessine un �l�ment graphique
void g_element_draw (HGSYS hgsys, HELEMENT helement)
  {
	// Correspondance entre les ACTIONS et les FONCTIONS pour le dessin de l'action
	typedef void (*FN_DESSIN_ACTION) (PGSYS pGSys, PELEMENT_GSYS pElementGSys);

	static FN_DESSIN_ACTION afnDessinAction [G_NB_ACTIONS] =
		{
		Action_Line             , //G_ACTION_LIGNE							
		Action_Rectangle        , //G_ACTION_RECTANGLE					
		Action_Fill_Rectangle   , //G_ACTION_RECTANGLE_PLEIN		
		Action_Circle           , //G_ACTION_CERCLE							
		Action_Fill_Circle      , //G_ACTION_CERCLE_PLEIN				
		Action_4_Arc            , //G_ACTION_QUART_ARC          
		Action_Fill_4_Arc_Rect  , //G_ACTION_QUART_CERCLE_PLEIN 
		Action_H_2_Arc          , //G_ACTION_H_DEMI_CERCLE      
		Action_Fill_H_2_Arc     , //G_ACTION_H_DEMI_CERCLE_PLEIN
		Action_V_2_Arc          , //G_ACTION_V_DEMI_CERCLE      
		Action_Fill_V_2_Arc     , //G_ACTION_V_DEMI_CERCLE_PLEIN
		Action_Tri_Rect         , //G_ACTION_TRI_RECT						
		Action_Fill_Tri_Rect    , //G_ACTION_TRI_RECT_PLEIN			
		Action_H_Tri_Iso        , //G_ACTION_H_TRI_ISO					
		Action_Fill_H_Tri_Iso   , //G_ACTION_H_TRI_ISO_PLEIN		
		Action_V_Tri_Iso        , //G_ACTION_V_TRI_ISO					
		Action_Fill_V_Tri_Iso   , //G_ACTION_V_TRI_ISO_PLEIN		
		Action_H_Vanne          , //G_ACTION_H_VANNE						
		Action_Fill_H_Vanne     , //G_ACTION_H_VANNE_PLEIN			
		Action_V_Vanne          , //G_ACTION_V_VANNE						
		Action_Fill_V_Vanne     , //G_ACTION_V_VANNE_PLEIN			
		Action_4_Arc_Rect       , //G_ACTION_QUART_CERCLE				
		Action_Fill_4_Arc       , //G_ACTION_QUART_ARC_PLEIN		
		Action_H_Write          , //G_ACTION_H_TEXTE						
		Action_V_Write          , //G_ACTION_V_TEXTE						
		Action_Polygonne        , //G_ACTION_POLYGONE						
		Action_Fill_Polygonne   , //G_ACTION_POLYGONE_PLEIN			
		Action_List             , //G_ACTION_LISTE							
		Action_Fill_List        , //G_ACTION_LISTE_PLEIN				
		Action_Trend            , //G_ACTION_COURBE_T						
		Action_Trend            , //G_ACTION_COURBE_C						
		Action_Trend            , //G_ACTION_COURBE_A						
		Action_BarGraf          , //G_ACTION_BARGRAPHE					
		Action_EntryField       , //G_ACTION_EDIT_TEXTE					
		Action_EntryField       , //G_ACTION_EDIT_NUM						
		Action_StaticText       , //G_ACTION_STATIC_TEXTE				
		Action_StaticText       , //G_ACTION_STATIC_NUM					
		Action_LogicEntry       , //G_ACTION_ENTREE_LOG					
		Action_Nop              , //G_ACTION_NOP								
		Action_Rectangle        , //G_ACTION_CONT_STATIC  			
		Action_Rectangle        , //G_ACTION_CONT_EDIT    			
		Action_Rectangle        , //G_ACTION_CONT_GROUP   			
		Action_Rectangle        , //G_ACTION_CONT_BOUTON  			
		Action_Rectangle        , //G_ACTION_CONT_CHECK   			
		Action_Rectangle        , //G_ACTION_CONT_RADIO   			
		Action_Rectangle        , //G_ACTION_CONT_COMBO   			
		Action_Rectangle        , //G_ACTION_CONT_LIST    			
		Action_Rectangle        , //G_ACTION_CONT_SCROLL_H			
		Action_Rectangle        , //G_ACTION_CONT_SCROLL_V			
		Action_Rectangle					//G_ACTION_CONT_BOUTON_VAL		
		};
  PGSYS 	pGSys = (PGSYS) hgsys;
  PELEMENT_GSYS	pElementGSys = (PELEMENT_GSYS) helement;

	if (VerifWarning (pElementGSys->nAction < G_NB_ACTIONS))
		afnDessinAction [pElementGSys->nAction] (pGSys, pElementGSys);
  }


// ------------------------------------------------------------------------
// Effectue un ::InvalidateRect sur un �l�ment graphique
void g_elementInvalidateRect (HGSYS hgsys, HELEMENT helement)
  {
  PGSYS 	pGSys = (PGSYS) hgsys;
  PELEMENT_GSYS	pElementGSys = (PELEMENT_GSYS) helement;
	RECT rectContour;

	// r�cup�re l'encombrement de l'�l�ment
	// $$ confusion top / bottom g_EncombrementElement (hgsys, helement, &rectContour.left, &rectContour.bottom, &rectContour.right, &rectContour.top);
	g_EncombrementElement (hgsys, helement, &rectContour.left, &rectContour.top, &rectContour.right, &rectContour.bottom);

	// Taille de l'�l�ment non nulle ?
	if ((rectContour.left != rectContour.right) || (rectContour.top != rectContour.bottom))
		{
		// oui => conversion en coordonn�es device
		g_convert_point_page_to_dev (hgsys, &rectContour.left, &rectContour.top);
		g_convert_point_page_to_dev (hgsys, &rectContour.right, &rectContour.bottom);

		//invalidation de la zone dans le device
		d_InvalidateRect (pGSys->hDev, &rectContour);
		}
  }

// ------------------------------------------------------------------------
G_ACTION actionElement (HGSYS hgsys, HELEMENT helement)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  return pElementGSys->nAction;
  }

// ------------------------------------------------------------------------
void g_element_get (HGSYS hgsys, HELEMENT helement, G_ATTRIBUT * pAttributs,
	G_ACTION * pAction, 
	G_COULEUR * pnCouleurLigne, G_COULEUR *pnCouleurRemplissage,
	G_STYLE_LIGNE *pLineStyle, G_STYLE_REMPLISSAGE * pFillStyle,
  DWORD *pPolice, DWORD *pPoliceStyle, 
	char **ppszText, DWORD  *pNbPoints)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  *pAction      = pElementGSys->nAction;
  *pnCouleurLigne				 = pElementGSys->nCouleurLigne;
  *pnCouleurRemplissage  = pElementGSys->nCouleurRemplissage;
  *pLineStyle   = pElementGSys->nStyleLigne;
  *pFillStyle   = pElementGSys->nStyleRemplissage;
  *pPolice      = pElementGSys->wPolice;
  *pPoliceStyle = pElementGSys->wPoliceStyle;
  *ppszText     = pElementGSys->pszText;
  *pNbPoints    = pElementGSys->wNbPoints;
  *pAttributs   = attributsActionGr (pElementGSys->nAction);
  }

// ------------------------------------------------------------------------
void g_element_get_point (HGSYS hgsys, HELEMENT helement, DWORD IndexPoint, LONG *x, LONG *y)
  {
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;

  if (IndexPoint < pElementGSys->wNbPoints)
    {
    *x = pElementGSys->pPoints [IndexPoint].x;
    *y = pElementGSys->pPoints [IndexPoint].y;
    }
  }


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//		Fonctions de calcul sur un �l�ment graphique
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// ------------------------------------------------------------------------
// encombrement
void g_EncombrementElement (HGSYS hgsys, HELEMENT helement, LONG * pxLeft, LONG * pyTop, LONG * pxRight, LONG * pyBottom)
  {
  PGSYS 				pGSys = (PGSYS)    hgsys;
  PELEMENT_GSYS	pElementGSys = (PELEMENT_GSYS) helement;
  DWORD         IndexPoint;

  if (pElementGSys->wNbPoints > 0)
    {
    *pxLeft = pElementGSys->pPoints [0].x;
    *pyTop = pElementGSys->pPoints [0].y;

    switch (pElementGSys->nAction)
      {
      case G_ACTION_STATIC_TEXTE:
      case G_ACTION_STATIC_NUM:
        if (pElementGSys->wNbPoints > 1)
          {
          *pxRight = pElementGSys->pPoints [1].x;
          *pyBottom   = pElementGSys->pPoints [1].y;
					Spc_CvtPtEnPixel (pGSys->hSpc, pxLeft, pyTop);
					Spc_CvtPtEnPixel (pGSys->hSpc, pxRight, pyBottom);
          d_get_box_static_text (pGSys->hDev, pxLeft, pyTop, pxRight, pyBottom, pElementGSys->wPolice, pElementGSys->wPoliceStyle, pElementGSys->pszText);
					Spc_CvtPtEnVirtuel (pGSys->hSpc, pxLeft, pyTop);
					Spc_CvtPtEnVirtuel (pGSys->hSpc, pxRight, pyBottom);
          }
        break;

      case G_ACTION_H_TEXTE:
        if (pElementGSys->wNbPoints > 1)
          {
          *pxRight = pElementGSys->pPoints [1].x;
          *pyBottom   = pElementGSys->pPoints [1].y;
					Spc_CvtPtEnPixel (pGSys->hSpc, pxLeft, pyTop);
					Spc_CvtPtEnPixel (pGSys->hSpc, pxRight, pyBottom); //$$ Size pour les tailles caract�re
          // lSpcCvtDyEnPixel
					d_get_box_h_write (pGSys->hDev, *pxLeft, *pyTop, pxRight, pyBottom, 
						pElementGSys->wPolice, pElementGSys->wPoliceStyle, *pyBottom, pElementGSys->pszText);
					Spc_CvtPtEnVirtuel (pGSys->hSpc, pxLeft, pyTop);
					Spc_CvtPtEnVirtuel (pGSys->hSpc, pxRight, pyBottom);
          }
        break;

      case G_ACTION_V_TEXTE:
        if (pElementGSys->wNbPoints > 1)
          {
          (*pxRight ) = pElementGSys->pPoints [1].x;
          (*pyBottom   ) = pElementGSys->pPoints [1].y;
					Spc_CvtPtEnPixel (pGSys->hSpc, pxLeft, pyTop);
					Spc_CvtPtEnPixel (pGSys->hSpc, pxRight, pyBottom);
          d_get_box_v_write (pGSys->hDev, *pxLeft, *pyTop, pxRight, pyBottom, pElementGSys->wPolice, pElementGSys->wPoliceStyle, *pyBottom, pElementGSys->pszText);
					Spc_CvtPtEnVirtuel (pGSys->hSpc, pxLeft, pyTop);
					Spc_CvtPtEnVirtuel (pGSys->hSpc, pxRight, pyBottom);
          }
        break;

      default:
				// Encombrement = premier point de l'�l�ment
				*pxRight = *pxLeft;
				*pyBottom = *pyTop;

				// augmente l'encombrement � chaque nouveau point � l'ext�rieur du rectangle
        for (IndexPoint = 1; IndexPoint < pElementGSys->wNbPoints; IndexPoint++)
          {
					POINT	pt = pElementGSys->pPoints [IndexPoint];

          if (pt.x < *pxLeft)
            *pxLeft = pt.x;
          else
						{
            if (pt.x	> *pxRight)
              *pxRight = pt.x;
            }

          if (pt.y < *pyTop)
            *pyTop = pt.y;
          else
            {
            if (pt.y > *pyBottom)
              *pyBottom = pt.y;
            }
          } //for

				/*
				// agrandis de 1 l'encombrement (en pixel)
				Spc_CvtPtEnPixel (pGSys->hSpc, pxLeft, pyBottom);
				Spc_CvtPtEnPixel (pGSys->hSpc, pxRight, pyTop);
				(*pxLeft)-= 4;
				(*pyBottom)-= 4;
				(*pxRight)+=4;
				(*pyTop)+=4;
				Spc_CvtPtEnVirtuel (pGSys->hSpc, pxLeft, pyBottom);
				Spc_CvtPtEnVirtuel (pGSys->hSpc, pxRight, pyTop);
				*/
        break;
      }
    }
  else
    {
    *pxLeft   = 0;
    *pyTop		= 0;
    *pxRight  = 0;
    *pyBottom = 0;
    }

	// ordonne le rectangle d'encombrement
	normaliser_bi_point (pxLeft, pyTop, pxRight, pyBottom);
  } // g_EncombrementElement

// --------------------------------------------------------------------------
// Table de correspondance entre les ACTIONS              (index tableau)
//                            et les FONCTIONS � EXECUTER (contenu du tableau)
// Pour le calcul du contour (Around)
// --------------------------------------------------------------------------
typedef DWORD (*pFctAround) (PGSYS pgsys, PELEMENT_GSYS pElementGSys, DWORD wCount, POINT *aPt);
static const pFctAround td_Around [G_NB_ACTIONS] =
  {
  Around_Line             , //G_ACTION_LIGNE							
  Around_Box              , //G_ACTION_RECTANGLE					
  Around_Box              , //G_ACTION_RECTANGLE_PLEIN		
  Around_Circle           , //G_ACTION_CERCLE							
  Around_Circle           , //G_ACTION_CERCLE_PLEIN				
  Around_4_Arc            , //G_ACTION_QUART_ARC          
  Around_4_Arc_Rect       , //G_ACTION_QUART_CERCLE_PLEIN 
  Around_H_2_Arc          , //G_ACTION_H_DEMI_CERCLE      
  Around_Fill_H_2_Arc     , //G_ACTION_H_DEMI_CERCLE_PLEIN
  Around_V_2_Arc          , //G_ACTION_V_DEMI_CERCLE      
  Around_Fill_V_2_Arc     , //G_ACTION_V_DEMI_CERCLE_PLEIN
  Around_Tri_Rect         , //G_ACTION_TRI_RECT						
  Around_Tri_Rect         , //G_ACTION_TRI_RECT_PLEIN			
  Around_H_Tri_Iso        , //G_ACTION_H_TRI_ISO					
  Around_H_Tri_Iso        , //G_ACTION_H_TRI_ISO_PLEIN		
  Around_V_Tri_Iso        , //G_ACTION_V_TRI_ISO					
  Around_V_Tri_Iso        , //G_ACTION_V_TRI_ISO_PLEIN		
  Around_H_Vanne          , //G_ACTION_H_VANNE						
  Around_H_Vanne          , //G_ACTION_H_VANNE_PLEIN			
  Around_V_Vanne          , //G_ACTION_V_VANNE						
  Around_V_Vanne          , //G_ACTION_V_VANNE_PLEIN			
  Around_4_Arc_Rect       , //G_ACTION_QUART_CERCLE				
  Around_Fill_4_Arc       , //G_ACTION_QUART_ARC_PLEIN		
  Around_Box              , //G_ACTION_H_TEXTE						
  Around_Box              , //G_ACTION_V_TEXTE						
  Around_Polygonne        , //G_ACTION_POLYGONE						
  Around_Fill_Polygonne   , //G_ACTION_POLYGONE_PLEIN			
  Around_Nop              , //G_ACTION_LISTE							
  Around_Nop              , //G_ACTION_LISTE_PLEIN				
  Around_Trend            , //G_ACTION_COURBE_T						
  Around_Trend            , //G_ACTION_COURBE_C						
  Around_Trend            , //G_ACTION_COURBE_A						
  Around_Box              , //G_ACTION_BARGRAPHE					
  Around_Box              , //G_ACTION_EDIT_TEXTE					
  Around_Box              , //G_ACTION_EDIT_NUM						
  Around_Box              , //G_ACTION_STATIC_TEXTE				
  Around_Box              , //G_ACTION_STATIC_NUM					
  Around_Box              , //G_ACTION_ENTREE_LOG					
  Around_Nop              , //G_ACTION_NOP								
	Around_Box              , //G_ACTION_CONT_STATIC  			
	Around_Box              , //G_ACTION_CONT_EDIT    			
  Around_Box              , //G_ACTION_CONT_GROUP   			
  Around_Box              , //G_ACTION_CONT_BOUTON  			
  Around_Box              , //G_ACTION_CONT_CHECK   			
  Around_Box              , //G_ACTION_CONT_RADIO   			
  Around_Box              , //G_ACTION_CONT_COMBO   			
  Around_Box              , //G_ACTION_CONT_LIST    			
  Around_Box              , //G_ACTION_CONT_SCROLL_H			
  Around_Box              , //G_ACTION_CONT_SCROLL_V			
  Around_Box                //G_ACTION_CONT_BOUTON_VAL		
  };

// ------------------------------------------------------------------------
DWORD g_dist (HGSYS hgsys, HELEMENT helement, LONG x, LONG y)
  {
	#define NB_POINTS_MAX 9
  PGSYS pGSys = (PGSYS)  hgsys;
  PELEMENT_GSYS pElementGSys = (PELEMENT_GSYS) helement;
  DWORD     wPointCount;
  POINT			aPointBuf [NB_POINTS_MAX]; // Buffer de points par d�faut
  POINT			* aPoint = aPointBuf;
  POINT     PtOrig;
  DWORD     lwCurDist;  //Fausse distance: x^2 + y^2
  DWORD     lwMinDist;  //Fausse distance: x^2 + y^2
  DWORD     w;

  // r�cup�re le nombre de segments constituant le contour de l'objet
	wPointCount = td_Around [pElementGSys->nAction] (pGSys, pElementGSys, 0, NULL);
  if (wPointCount > 0)
    {
		// Le contour a trop de points pour le buffer par d�faut ?
		if (wPointCount > NB_POINTS_MAX)
			// oui => on alloue un autre buffer
			aPoint = (PPOINT)pMemAlloue (wPointCount * sizeof (POINT));

    td_Around [pElementGSys->nAction] (pGSys, pElementGSys, wPointCount, aPoint);
    //
    PtOrig.x = x;
    PtOrig.y = y;
    SpcCvtPointsEnPixel (pGSys->hSpc, 1, &PtOrig);
    if (wPointCount > 1)
      {
			// prend le min des distances � chaque segment
      lwCurDist = DistSegment (&PtOrig, &aPoint[0], &aPoint[1]);
      lwMinDist = lwCurDist;
      for (w = 2; w < wPointCount; w++)
        {
        lwCurDist = DistSegment (&PtOrig, &aPoint[w - 1], &aPoint[w]);
        if (lwCurDist < lwMinDist)
          {
          lwMinDist = lwCurDist;
          }
        }
      }
    else
      {
			// distance au point
      lwCurDist = DistPoint2 (&PtOrig, &aPoint[0]);
      }
		
		// Buffer point autre que celui par d�faut ?
		if (aPoint != aPointBuf)
			// oui => on le lib�re
			MemLibere ((PVOID *)(&aPoint));
    }
  else
    {
    lwMinDist = (DWORD) -1;
    }

  return lwMinDist;
  }

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
// Verbes de Coordonn�es
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
// Pixel Device ---> Pixel Page (g�neralis�)
// ------------------------------------------------------------------------
void g_convert_point_dev_to_page (HGSYS hgsys, LONG * px, LONG * py)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  Spc_CvtPtEnVirtuel (pGSys->hSpc, px, py);
  }

// ------------------------------------------------------------------------
LONG g_convert_x_dev_to_page (HGSYS hgsys, LONG x)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcCvtXEnVirtuel (pGSys->hSpc, x);
  }

// ------------------------------------------------------------------------
LONG g_convert_y_dev_to_page (HGSYS hgsys, LONG y)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcCvtYEnVirtuel (pGSys->hSpc, y);
  }

// ------------------------------------------------------------------------
// Conversions Pixel Page (g�neralis�) ---> Pixel Device
// ------------------------------------------------------------------------
void g_convert_point_page_to_dev (HGSYS hgsys, LONG * px, LONG * py)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  Spc_CvtPtEnPixel (pGSys->hSpc, px, py);
  }

// ------------------------------------------------------------------------
LONG g_convert_x_page_to_dev (HGSYS hgsys, LONG x)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcCvtXEnPixel (pGSys->hSpc, x);
  }

// ------------------------------------------------------------------------
LONG g_convert_y_page_to_dev (HGSYS hgsys, LONG y)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcCvtYEnPixel (pGSys->hSpc, y);
  }

// ------------------------------------------------------------------------
// DELTA Pixel Page (g�neralis�) ---> DELTA Pixel Device
// ------------------------------------------------------------------------
void g_convert_delta_page_to_dev (HGSYS hgsys, LONG * pDx, LONG * pDy)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  Spc_CvtSizeEnPixel (pGSys->hSpc, pDx, pDy);
  }

// ------------------------------------------------------------------------
LONG g_convert_dx_page_to_dev (HGSYS hgsys, LONG dx)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcCvtDxEnPixel (pGSys->hSpc, dx);
  }

// ------------------------------------------------------------------------
LONG g_convert_dy_page_to_dev (HGSYS hgsys, LONG dy)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcCvtDyEnPixel (pGSys->hSpc, dy);
  }

// ------------------------------------------------------------------------
// DELTA Pixel Device (g�neralis�) ---> DELTA Pixel Page
// ------------------------------------------------------------------------
void g_convert_delta_dev_to_page (HGSYS hgsys, LONG * pDx, LONG * pDy)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  Spc_CvtSizeEnVirtuel (pGSys->hSpc, pDx, pDy);
  }

// ------------------------------------------------------------------------
LONG g_convert_dx_dev_to_page (HGSYS hgsys, LONG dx)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcCvtDxEnVirtuel (pGSys->hSpc, dx);
  }

// ------------------------------------------------------------------------
LONG g_convert_dy_dev_to_page (HGSYS hgsys, LONG dy)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcCvtDyEnVirtuel (pGSys->hSpc, dy);
  }

// ------------------------------------------------------------------------
// Pixel Page (g�neralis�) Quelconque ---> Pixel Page (g�neralis�) Cadr�
// ------------------------------------------------------------------------
void g_map_point_page (HGSYS hgsys, LONG *x, LONG *y)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  *x = lSpcVirtuelAjusteX (pGSys->hSpc, *x);
  *y = lSpcVirtuelAjusteY (pGSys->hSpc, *y);
  }

// ------------------------------------------------------------------------
LONG g_map_x_page (HGSYS hgsys, LONG x)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcVirtuelAjusteX (pGSys->hSpc, x);
  }

// ------------------------------------------------------------------------
LONG g_map_y_page (HGSYS hgsys, LONG y)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcVirtuelAjusteY (pGSys->hSpc, y);
  }

// ------------------------------------------------------------------------
// Addition Pixels Page (g�neralis�s) Quelconques ---> Pixel Page (g�neralis�) Cadr�
// ------------------------------------------------------------------------
void g_add_points_page (HGSYS hgsys, LONG *xTarget, LONG *yTarget, LONG xOffset, LONG yOffset)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  *xTarget = lSpcVirtuelAjouteX (pGSys->hSpc, *xTarget, xOffset);
  *yTarget = lSpcVirtuelAjouteY (pGSys->hSpc, *yTarget, yOffset);
  }

// ------------------------------------------------------------------------
LONG g_add_x_page (HGSYS hgsys, LONG x, LONG dx)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcVirtuelAjouteX (pGSys->hSpc, x, dx);
  }

// ------------------------------------------------------------------------
LONG g_add_y_page (HGSYS hgsys, LONG y, LONG dy)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcVirtuelAjouteY (pGSys->hSpc, y, dy);
  }

// ------------------------------------------------------------------------
// Addition DELTA Pixels Page (g�neralis�s) Quelconques ---> DELTA Pixel Page (g�neralis�) Cadr�
// ------------------------------------------------------------------------
void g_add_deltas_page (HGSYS hgsys, LONG *dxTarget, LONG *dyTarget, LONG xOffset, LONG yOffset)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  *dxTarget = lSpcVirtuelAjouteDx (pGSys->hSpc, *dxTarget, xOffset);
  *dyTarget = lSpcVirtuelAjouteDy (pGSys->hSpc, *dyTarget, yOffset);
  }

// ------------------------------------------------------------------------
LONG g_add_dx_page (HGSYS hgsys, LONG dx1, LONG dx2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcVirtuelAjouteDx (pGSys->hSpc, dx1, dx2);
  }

// ------------------------------------------------------------------------
LONG g_add_dy_page (HGSYS hgsys, LONG dy1, LONG dy2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return lSpcVirtuelAjouteDy (pGSys->hSpc, dy1, dy2);
  }

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
void g_fill_background (HGSYS hgsys, G_COULEUR nCouleur)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  d_fill_background (pGSys->hDev, nCouleur);
  }

// ------------------------------------------------------------------------
void g_grille (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE style, LONG pas_x, LONG pas_y)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  d_grille (pGSys->hDev, nCouleur, style, pas_x, pas_y);
  }

void g_get_taille_background (HGSYS hgsys, LONG * dxFond, LONG * dyFond)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  d_get_taille_background (pGSys->hDev, dxFond, dyFond);
	*dxFond=lSpcCvtDxEnVirtuel (pGSys->hSpc, *dxFond);
	*dyFond=lSpcCvtDyEnVirtuel (pGSys->hSpc, *dyFond);
  }

// ------------------------------------------------------------------------
BOOL g_color_stable (HGSYS hgsys, G_COULEUR nCouleur)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return (d_color_stable (pGSys->hDev, nCouleur));
  }

// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
// DIBs (Device Independant Bitmaps)
// ------------------------------------------------------------------------
HANDLE g_load_bmp (HGSYS hgsys, PCSTR pszBmpFile)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  return d_load_bmp (pGSys->hDev, pszBmpFile);
  }

// ------------------------------------------------------------------------
void g_delete_bmp (HGSYS hgsys, HANDLE * phdlBmp)
  {
  PGSYS pGSys = (PGSYS)  hgsys;

  d_delete_bmp (pGSys->hDev,phdlBmp);
  }

// ------------------------------------------------------------------------
// Dessine un DIB
void g_draw_bmp (HGSYS hgsys, HANDLE hdlBmp, BOOL bStretch, LONG x1, LONG y1, LONG x2, LONG y2)
  {
  PGSYS pGSys = (PGSYS)  hgsys;
  POINT   aPoint [2] = {x1, y1, x2, y2};

  SpcCvtPointsEnPixel (pGSys->hSpc, 2, aPoint);
  d_draw_bmp (pGSys->hDev, hdlBmp, bStretch, aPoint[0].x, aPoint[0].y, aPoint[1].x, aPoint[1].y);
  }

// --------------------------- fin G_sys.c ----------------------------------

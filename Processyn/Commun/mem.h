/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |	Objet		:	Gestion m�moire
 |   Titre   : mem.h							|
 |   Auteur  : J.B.							|
 |   Date    : 20-02-92 						|
 |   Version : 0                                                        |
 |   Historique : 
 |		20-02-92 Jean creation                                            |
 +----------------------------------------------------------------------*/

// Ouverture  / fermeture base de donn�es courante (une seule � la fois)
void creer_bd_vierge (void);
void fermer_bd (void);


// Fonctions tableaux en m�moire
DWORD mem_libre (void);
DWORD long_user (void);
void raz_mem (void);
void raz_mem_total (void);
void cree_bloc (DWORD repere, DWORD taille_enr, DWORD nb_enr);
void vide_bloc (DWORD repere);
void enleve_bloc (DWORD repere);

// Renvoie le nombre d'enregistrements d'un bloc
DWORD nb_enregistrements (PCSTR pszSource, DWORD dwNLine, DWORD repere);

// Renvoie le nombre d'enregistrements d'un bloc
// renvoie 0 si le bloc n'existe pas
DWORD dwNbEnregistrementsSansErr (PCSTR pszSource, DWORD dwNLine, DWORD repere);

// Renvoie l'adresse de l'objet sp�cifi�.
void * pointe_enr (PCSTR pszSource, DWORD dwNLine, DWORD repere, DWORD n_enr);

// Renvoie l'adresse de l'objet sp�cifi� dans le bloc.
// En cas de non existence renvoie NULL
PVOID pPointeEnrSansErr (PCSTR pszSource, DWORD dwNLine, DWORD repere, DWORD n_enr);

void trans_enr (PCSTR pszSource, DWORD dwNLine, DWORD nb_enr_a_transferer, DWORD de_repere, DWORD a_repere, DWORD de_n_enr, DWORD a_n_enr);
void insere_enr (PCSTR pszSource, DWORD dwNLine, DWORD nb_enr_a_inserer, DWORD repere, DWORD n_enr_origine);
// Ajoute nb_enr_a_inserer au bout du bloc repere
// et renvoie l'adresse du premier enr ajout�
PVOID pAjouteEnrFin (PCSTR pszSource, DWORD dwNLine, DWORD dwNbEnrAAjouter, DWORD repere);
void enleve_enr (PCSTR pszSource, DWORD dwNLine, DWORD nb_enr_a_enlever, DWORD repere, DWORD n_enr_origine);
BOOL chk_bloc (DWORD repere);
BOOL existe_repere (DWORD repere);


// Gestion m�moire/disque
DWORD sauve_memoire (PCSTR nom_path);
DWORD charge_memoire (PCSTR nom_path);

//-------------------------------------------------------------------------

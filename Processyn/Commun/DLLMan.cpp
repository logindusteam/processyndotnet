// DLLMAN.C
// Gestion du lien avec une DLL sur demande
// Win32 3/2/97

#include "stdafx.h"
#include "DLLMan.h"

//------------------------------------------------------------------------------
// Lien � la demande avec l'ensemble des fonctions de la DLL 
BOOL bLieDLL (HINSTANCE * phInst, PCSTR pszNomDLL, LIEN_FONCTION aLiens [], UINT uNbLiens)
	{
	BOOL bOk = TRUE;

	// chargement de la DLL r�ussi ?
	* phInst = LoadLibrary (pszNomDLL);
	if (* phInst)
		{
		UINT nNFonc;

		// oui => lien avec les fonctions de la DLL
		for (nNFonc = 0; nNFonc < uNbLiens; nNFonc++)
			{
			aLiens[nNFonc].pfn = GetProcAddress (* phInst, aLiens[nNFonc].pszNomFonction);
			if (!aLiens[nNFonc].pfn)
				bOk = FALSE;
			}
		}
	else
		bOk = FALSE;
	return bOk;
	}

//------------------------------------------------------------------------------
// Lien � la demande avec l'ensemble des fonctions de la DLL 
BOOL bLieDLL (HINSTANCE * phInst, PCSTR pszNomDLL, LIEN_PFONCTION aLiens [], UINT uNbLiens)
	{
	BOOL bOk = TRUE;

	// chargement de la DLL r�ussi ?
	* phInst = LoadLibrary (pszNomDLL);
	if (* phInst)
		{
		UINT nNFonc;

		// oui => lien avec les fonctions de la DLL
		for (nNFonc = 0; nNFonc < uNbLiens; nNFonc++)
			{
			*aLiens[nNFonc].ppfn = GetProcAddress (* phInst, aLiens[nNFonc].pszNomFonction);
			if (!*aLiens[nNFonc].ppfn)
				bOk = FALSE;
			}
		}
	else
		bOk = FALSE;
	return bOk;
	}

//------------------------------------------------------------------------------
// Lib�re les liens avec l'ensemble des fonctions de la DLL
BOOL bLibereLienDLL (HINSTANCE * phInst, LIEN_FONCTION aLiens [], UINT uNbLiens)
	{
	BOOL bOk = FALSE;

	// Instance DLL valide ?
	if (* phInst)
		{
		// oui => lib�ration DLL Ok ?
		if (FreeLibrary (* phInst))
			{
			// oui => Ok : note le nouvel �tat
			bOk = TRUE;

			// DLL ferm�e
			* phInst = NULL;

			// note la fermeture des lien avec les fonctions de la DLL
			for (UINT	nNFonc = 0; nNFonc < uNbLiens; nNFonc++)
				aLiens[nNFonc].pfn = NULL;
			}
		}
	return bOk;
	}

//------------------------------------------------------------------------------
// Lib�re les liens avec l'ensemble des fonctions de la DLL
BOOL bLibereLienDLL (HINSTANCE * phInst, LIEN_PFONCTION aLiens [], UINT uNbLiens)
	{
	BOOL bOk = FALSE;

	// Instance DLL valide ?
	if (* phInst)
		{
		// oui => lib�ration DLL Ok ?
		if (FreeLibrary (* phInst))
			{
			// oui => Ok : note le nouvel �tat
			bOk = TRUE;

			// DLL ferm�e
			* phInst = NULL;

			// note la fermeture des lien avec les fonctions de la DLL
			for (UINT nNFonc = 0; nNFonc < uNbLiens; nNFonc++)
				*aLiens[nNFonc].ppfn = NULL;
			}
		}
	return bOk;
	}


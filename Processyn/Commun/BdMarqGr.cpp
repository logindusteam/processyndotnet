//-------------------------------------------------------------------------
// BdMarqGr.c (li� � Bdgr)
// Gestion des marques (s�lection) des �l�ments graphiques de la base de donn�e Processyn
//-------------------------------------------------------------------------
#include "stdafx.h"
#include "std.h"        // std types
#include "MemMan.h"

#include "Appli.h"      // hinst
#include "mem.h"        // mem
#include "UStr.h"     //
#include "G_Objets.h"
#include "g_sys.h"      // g_xxx()
#include "pcsspace.h"   // Coordonn�es Min et MAX de l'espace
#include "dicho.h"      // Tri dicho pour espacement
#include "lng_res.h"
#include "IdLngLng.h"
#include "Tipe.h"			// types de proc�dures du descripteur
#include "Descripteur.h"
#include "bdgr.h"			// PBDGR
#include "bdanmgr.h"   // verbes maj inter_vi
#include "BdPageGr.h"
#include "Verif.h"
#include "IdAutoPcsMan.h"
#include "BdElemGr.h"
#include "BdMarqGr.h"
#include "pcsdlggr.h"
VerifInit;

#define TAILLE_PEL_DEFORMATION				8	// cot� d'une poign�e de d�formation en pixel
#define OFFSET_PEL_CADRE_SELECTION		2	// d�calage d'une poign�e de d�formation sur le rectangle de s�lection

// Variables Globales memorisation deplacement rectangle selection
static RECT_BDGR RectMarqueMem;


// -------------------------------------------------------------------------
//  Travail sur Rectangle
// -------------------------------------------------------------------------
static void PositionnerRectGR (PBDGR pBdGr, PRECT_BDGR pRectangle)
  {
	POINT	pt0 = {pRectangle->x1, pRectangle->y1};
	POINT	pt1 = {pRectangle->x2, pRectangle->y2};

  GElementResetBiPoint (pBdGr->hGsys, pBdGr->helementRectanglePoignee, &pt0, &pt1);
  }

// -------------------------------------------------------------------------
static void LireRectGR (PBDGR pBdGr, PRECT_BDGR pRectangle)
  {
  LONG x;
  LONG y;

  g_element_get_point (pBdGr->hGsys, pBdGr->helementRectanglePoignee, 0, &x, &y);
  pRectangle->x1 = x;
  pRectangle->y1 = y;
  g_element_get_point (pBdGr->hGsys, pBdGr->helementRectanglePoignee, 1, &x, &y);
  pRectangle->x2 = x;
  pRectangle->y2 = y;
  }
// -------------------------------------------------------------------------
static void TranslaterRectGR (PBDGR pBdGr, LONG Dx, LONG Dy, PRECT_BDGR pRectangle)
  {
  PositionnerRectGR (pBdGr, pRectangle);
  g_element_move (pBdGr->hGsys, pBdGr->helementRectanglePoignee, (LONG) Dx, (LONG) Dy);
  LireRectGR (pBdGr, pRectangle);
  }
// -------------------------------------------------------------------------
static void TournerRectGR (PBDGR pBdGr, LONG xCentre, LONG yCentre, DWORD NbQuartTour, PRECT_BDGR pRectangle)
  {
  PositionnerRectGR (pBdGr, pRectangle);
  g_element_rotate (pBdGr->hGsys, pBdGr->helementRectanglePoignee, (LONG) xCentre, (LONG) yCentre, NbQuartTour);
  LireRectGR (pBdGr, pRectangle);
  }
// -------------------------------------------------------------------------
static void DeformerRectGR (PBDGR pBdGr, LONG xRef, LONG yRef, 
														LONG cxNew, LONG cxOld, LONG cyNew, LONG cyOld, PRECT_BDGR pRectangle)
  {
  PositionnerRectGR (pBdGr, pRectangle);
  g_element_zoom (pBdGr->hGsys, pBdGr->helementRectanglePoignee, xRef, yRef, cxNew, cxOld, cyNew, cyOld);
  LireRectGR (pBdGr, pRectangle);
  }
// -------------------------------------------------------------------------
static void SubstituerRectGR (PBDGR pBdGr, PRECT_BDGR pRectangle)
  {
  PositionnerRectGR (pBdGr, pRectangle);
  LireRectGR (pBdGr, pRectangle);
  }
// -------------------------------------------------------------------------
static void MiroiterRectGR (PBDGR pBdGr, DWORD Axes, LONG xAxe, LONG yAxe, PRECT_BDGR pRectangle)
  {
  PositionnerRectGR (pBdGr, pRectangle);
  g_element_miror (pBdGr->hGsys, pBdGr->helementRectanglePoignee, Axes, (LONG) xAxe, (LONG) yAxe);
  LireRectGR (pBdGr, pRectangle);
  }
// -------------------------------------------------------------------------
static void AjouterRectGR (RECT_BDGR RectangleAjout, PRECT_BDGR pRectangleGlobal)
  {

  if ((pRectangleGlobal->x1 != 0) || (pRectangleGlobal->y1 != 0) ||
      (pRectangleGlobal->x2 != 0) || (pRectangleGlobal->y2 != 0))
    {
    if (RectangleAjout.x1 < pRectangleGlobal->x1)
      {
      pRectangleGlobal->x1 = RectangleAjout.x1;
      }
    if (RectangleAjout.y1 < pRectangleGlobal->y1)
      {
      pRectangleGlobal->y1 = RectangleAjout.y1;
      }
    if (RectangleAjout.x2 > pRectangleGlobal->x2)
      {
      pRectangleGlobal->x2 = RectangleAjout.x2;
      }
    if (RectangleAjout.y2 > pRectangleGlobal->y2)
      {
      pRectangleGlobal->y2 = RectangleAjout.y2;
      }
    }
  else
    {
    pRectangleGlobal->x1 = RectangleAjout.x1;
    pRectangleGlobal->y1 = RectangleAjout.y1;
    pRectangleGlobal->x2 = RectangleAjout.x2;
    pRectangleGlobal->y2 = RectangleAjout.y2;
    }
  }

// -------------------------------------------------------------------------
BOOL bdgr_point_dans_rectangle (LONG x, LONG y, LONG Rectx1, LONG Recty1, LONG Rectx2, LONG Recty2)
  {
  return ((BOOL)((x >= Rectx1) && (x <= Rectx2) && (y >= Recty1) && (y <= Recty2)));
  }

// -------------------------------------------------------------------------
// force x1<x2 et y1<y2
static void OrienterRectGR (PRECT_BDGR pRectangle)
  {
  LONG temp;

  if (pRectangle->x1 > pRectangle->x2)
    {
    temp = pRectangle->x2;
    pRectangle->x2 = pRectangle->x1;
    pRectangle->x1 = temp;
    }
  if (pRectangle->y1 > pRectangle->y2)
    {
    temp = pRectangle->y2;
    pRectangle->y2 = pRectangle->y1;
    pRectangle->y1 = temp;
    }
  }

// -------------------------------------------------------------------------
// Marques
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
void MajGeoMarquesGR (DWORD TypeMaj, DWORD PosEnr, DWORD NbEnr)
  {
  DWORD              wPremierEnr;
  DWORD              wDernierEnr;
  DWORD              wEnr;
  PGEO_MARQUES_GR		pGeoMarque;

  wPremierEnr = 1;
  wDernierEnr = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_MARQUES_GR) + 1;

  switch (TypeMaj)
    {
    case MAJ_INSERTION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wEnr);
				if (pGeoMarque->Element >= PosEnr)
					pGeoMarque->Element = pGeoMarque->Element + NbEnr;
				}
			break;
    case MAJ_SUPPRESSION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wEnr);
				if (pGeoMarque->Element >= (PosEnr + NbEnr))
					pGeoMarque->Element = pGeoMarque->Element - NbEnr;
				}
			break;
    }
  }
// Renvoie le num�ro de marque de l'�l�ment sp�cifi�
static DWORD LireMarqueElementGR (DWORD ReferencePage, DWORD PositionElement)
  {
  PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, ReferencePage);
  DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
  DWORD       wNbGeoMarques = pMarque->NbMarques;
  DWORD       wIndex = 0;
  DWORD       wPositionGeoMarqueTrouve = 0;

  while ((wPositionGeoMarqueTrouve == 0) && (wIndex < wNbGeoMarques))
    {
		PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPremiereGeoMarque + wIndex);

    if (pGeoMarque->Element == PositionElement)
      {
      wPositionGeoMarqueTrouve = wPremiereGeoMarque + wIndex;
      }
    wIndex++;
    }

  return wPositionGeoMarqueTrouve;
  }

// -------------------------------------------------------------------------
// renvoie le rectangle de marque associ� � la page sp�cifi�e
static void LireRectMarquesGR (DWORD ReferencePage, PRECT_BDGR pRectangle)
  {
  PMARQUES_GR pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, ReferencePage);

  *pRectangle = pMarque->RectMarques;
  }

// -------------------------------------------------------------------------
static void MajRectMarquesGR (DWORD ReferencePage, RECT_BDGR Rectangle)
  {
  PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, ReferencePage);

  pMarque->RectMarques = Rectangle;
  }

// -------------------------------------------------------------------------
// calcule le rectangle global des marques.
static void CalculerRectMarquesGR (PBDGR pBdGr, DWORD ReferencePage, PRECT_BDGR pRectangle)
  {
  PMARQUES_GR			pMarque;
  PGEO_MARQUES_GR	pGeoMarque;
  DWORD           wPremiereGeoMarque;
  DWORD           wDerniereGeoMarque;
  DWORD           wPositionGeoMarque;
  RECT_BDGR       TempRect;

  pRectangle->x1 = 0;
  pRectangle->y1 = 0;
  pRectangle->x2 = 0;
  pRectangle->y2 = 0;
  pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, ReferencePage);
  if (pMarque->NbMarques > 0)
    {
    wPremiereGeoMarque = pMarque->PremiereMarque;
    wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
    pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPremiereGeoMarque);
    EncombrementElementGR (B_ELEMENTS_PAGES_GR, pGeoMarque->Element,
                    &(pRectangle->x1), &(pRectangle->y1), &(pRectangle->x2), &(pRectangle->y2),
                    pBdGr->hGsys, pBdGr->helementTravail);
    for (wPositionGeoMarque = wPremiereGeoMarque + 1; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
      pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
      EncombrementElementGR (B_ELEMENTS_PAGES_GR, pGeoMarque->Element,
                      &(TempRect.x1), &(TempRect.y1), &(TempRect.x2), &(TempRect.y2),
                      pBdGr->hGsys, pBdGr->helementTravail);
      AjouterRectGR (TempRect, pRectangle);
      }
    }
  }

// -------------------------------------------------------------------------

static DWORD MettreMarqueGR  (DWORD ReferencePage, DWORD PositionElement)
  {
  PMARQUES_GR				pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, ReferencePage);
  DWORD             wPositionGeoMarque = pMarque->PremiereMarque + pMarque->NbMarques;
  PGEO_MARQUES_GR		pGeoMarque;

  pMarque->NbMarques++;

  insere_enr (szVERIFSource, __LINE__, 1, B_GEO_MARQUES_GR, wPositionGeoMarque);
  pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
  pGeoMarque->Element = PositionElement;

  return wPositionGeoMarque;
  }

// -------------------------------------------------------------------------
static void EnleverMarqueGR (DWORD ReferencePage, DWORD PositionGeoMarque)
  {
  PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, ReferencePage);

  pMarque->NbMarques--;
  enleve_enr (szVERIFSource, __LINE__, 1, B_GEO_MARQUES_GR, PositionGeoMarque);
  }

// -------------------------------------------------------------------------
// Mise � jour de l'objet Marques
void MajMarquesGR (DWORD TypeMaj, DWORD wReferencePage, DWORD PosEnr, DWORD NbEnr)
  {
  DWORD          wPremierEnr = 1;
  DWORD          wDernierEnr = nb_enregistrements (szVERIFSource, __LINE__, B_MARQUES_GR) + 1;
  DWORD          wEnr;

  switch (TypeMaj)
    {
    case MAJ_INSERTION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wEnr);

				if (pMarque->PremiereMarque > PosEnr)
					pMarque->PremiereMarque = pMarque->PremiereMarque + NbEnr;
				else
					{
					if ((pMarque->PremiereMarque == PosEnr) && (wEnr != wReferencePage))
						pMarque->PremiereMarque = pMarque->PremiereMarque + NbEnr;
					}
				}
			break;
    case MAJ_SUPPRESSION:
			for (wEnr = wPremierEnr; wEnr < wDernierEnr; wEnr++)
				{
				PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wEnr);

				if (pMarque->PremiereMarque >= (PosEnr + NbEnr))
					pMarque->PremiereMarque = pMarque->PremiereMarque - NbEnr;
				}
			break;
    }
  }

// -------------------------------------------------------------------------
// Selection-D�formation
// -------------------------------------------------------------------------
// Modifie le rectangle de d�formation courant
static void PositionnerRectDeformationGR (PBDGR pBdGr, LONG x, LONG y)
  {
  LONG offsetxplus;
  LONG offsetyplus;
  LONG lx;
  LONG ly;

  offsetxplus  = g_convert_dx_dev_to_page (pBdGr->hGsys, (LONG) (TAILLE_PEL_DEFORMATION));
  offsetyplus  = g_convert_dy_dev_to_page (pBdGr->hGsys, (LONG) (TAILLE_PEL_DEFORMATION));
  GElementSupprimePoints (pBdGr->helementRectanglePoignee);
  lx = (LONG) x;
  ly = (LONG) y;
  GElementAjoutePoint (pBdGr->hGsys, pBdGr->helementRectanglePoignee, lx, ly);
  g_add_points_page (pBdGr->hGsys, &lx, &ly, offsetxplus, offsetyplus);
  GElementAjoutePoint (pBdGr->hGsys, pBdGr->helementRectanglePoignee, lx, ly);
  }

// ------------------------------------------------------------------------------------------------------
// renvoie l'encombrement de pBdGr->helementTravail - augment� d'OFFSET_PEL_CADRE_SELECTION (converti)
static void EncombrementSelectionElementTravailGR (PBDGR pBdGr, DWORD Position, PPOINT pPtHG, PPOINT pPtBD)
  {
  EncombrementElementGR (B_ELEMENTS_PAGES_GR, Position, &pPtHG->x, &pPtHG->y, &pPtBD->x, &pPtBD->y, pBdGr->hGsys, pBdGr->helementTravail);
  pPtHG->x = g_add_x_page (pBdGr->hGsys, pPtHG->x, - g_convert_dx_dev_to_page (pBdGr->hGsys, OFFSET_PEL_CADRE_SELECTION));
  pPtHG->y = g_add_y_page (pBdGr->hGsys, pPtHG->y, - g_convert_dy_dev_to_page (pBdGr->hGsys, OFFSET_PEL_CADRE_SELECTION));
  pPtBD->x = g_add_x_page (pBdGr->hGsys, pPtBD->x, g_convert_dx_dev_to_page (pBdGr->hGsys, OFFSET_PEL_CADRE_SELECTION));
  pPtBD->y = g_add_y_page (pBdGr->hGsys, pPtBD->y, g_convert_dy_dev_to_page (pBdGr->hGsys, OFFSET_PEL_CADRE_SELECTION));
  }

// -------------------------------------------------------------------------
// Dessine le rectangle de s�lection autour d'un �l�ment graphique d'une page
static void DessinerSelectionGR (PBDGR pBdGr, DWORD Position)
  {
  PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, Position);
	POINT					pt0;
	POINT					pt1;

	// r�cup�re le cadre de s�lection de l'�l�ment en question
  EncombrementSelectionElementTravailGR (pBdGr, Position, &pt0, &pt1);

	// applique le � l'�l�ment de s�lection et dessine le
  GElementResetBiPoint (pBdGr->hGsys, pBdGr->helementSelection, &pt0, &pt1);

	// Ligne de fond noir continue ...
	GElementSetStyleLigne (pBdGr->helementSelection, G_STYLE_LIGNE_CONTINUE);
  GElementSetCouleurLigne (pBdGr->helementSelection, G_BLACK);
  g_element_draw (pBdGr->hGsys, pBdGr->helementSelection);

	// Surmont�e d'une ligne pointill�e blanche
  G_STYLE_LIGNE		dwStyle =  (pElement->PosGeoAnimation != 0) ? c_STYLE_LINE_SELECTION_ANIM:c_STYLE_LINE_SELECTION_NORMAL;
	GElementSetStyleLigne (pBdGr->helementSelection, dwStyle);
	GElementSetCouleurLigne (pBdGr->helementSelection, G_WHITE);
  g_element_draw (pBdGr->hGsys, pBdGr->helementSelection);
	}

// -------------------------------------------------------------------------
// Invalidate le rectangle de s�lection autour d'un �l�ment graphique d'une page
static void GommerSelectionGR (PBDGR pBdGr, DWORD Position)
  {
	POINT					pt0;
	POINT					pt1;

	// r�cup�re le cadre de s�lection de l'�l�ment en question
  EncombrementSelectionElementTravailGR (pBdGr, Position, &pt0, &pt1);
  GElementResetBiPoint (pBdGr->hGsys, pBdGr->helementSelection, &pt0, &pt1);

	// invalide le contenu de ce cadre ($$ ne devrait invalider que le cadre lui m�me
  g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementSelection);
  }

// -------------------------------------------------------------------------
// Dessin des 4 poign�es de d�formation (taille constante en pixel) autour du rectangle sp�cifi�
static void DessinerUnePoignee (PBDGR pBdGr)
  {
	// Dessine un rectangle blanc ...bord� de noir :
	GElementSetAction (pBdGr->helementRectanglePoignee, G_ACTION_RECTANGLE_PLEIN);
	GElementSetStyleRemplissage (pBdGr->helementRectanglePoignee, G_STYLE_REMPLISSAGE_PLEIN);
  GElementSetCouleurRemplissage (pBdGr->helementRectanglePoignee, G_WHITE);
  g_element_draw (pBdGr->hGsys, pBdGr->helementRectanglePoignee);

	// ...bord� de noir :
	GElementSetAction (pBdGr->helementRectanglePoignee, G_ACTION_RECTANGLE);
	GElementSetStyleLigne (pBdGr->helementRectanglePoignee, G_STYLE_LIGNE_CONTINUE);
  GElementSetCouleurLigne (pBdGr->helementRectanglePoignee, G_BLACK);
  g_element_draw (pBdGr->hGsys, pBdGr->helementRectanglePoignee);
	}

// -------------------------------------------------------------------------
// Dessin des 4 poign�es de d�formation (taille constante en pixel) autour du rectangle sp�cifi�
static void DessinerPoigneesGR (PBDGR pBdGr, RECT_BDGR RectDeformation)
  {
	// Dessin des poign�es � effectuer ?
  if ((RectDeformation.x1) || (RectDeformation.y1) || (RectDeformation.x2) || (RectDeformation.y2))
    {
		// oui => calcule la position hors tout du rectangle � cerner
    OrienterRectGR (&RectDeformation);

    LONG offsetx  = g_convert_dx_dev_to_page (pBdGr->hGsys, -TAILLE_PEL_DEFORMATION);
    LONG offsety  = g_convert_dy_dev_to_page (pBdGr->hGsys, -TAILLE_PEL_DEFORMATION);
    LONG lx = g_add_x_page (pBdGr->hGsys, RectDeformation.x1, offsetx);
    LONG ly = g_add_y_page (pBdGr->hGsys, RectDeformation.y1, offsety);

		// Dessine chacune des poign�es
    PositionnerRectDeformationGR (pBdGr, lx, ly);
    DessinerUnePoignee (pBdGr);
		
    PositionnerRectDeformationGR (pBdGr, lx, RectDeformation.y2);
    DessinerUnePoignee (pBdGr);

    PositionnerRectDeformationGR (pBdGr, RectDeformation.x2, ly);
    DessinerUnePoignee (pBdGr);

    PositionnerRectDeformationGR (pBdGr, RectDeformation.x2, RectDeformation.y2);
    DessinerUnePoignee (pBdGr);
    }
  }

// -------------------------------------------------------------------------
// Invalidate des 4 poign�es de d�formation (taille constante en pixel) autour du rectangle sp�cifi�
static void GommerPoigneesGR (PBDGR pBdGr, RECT_BDGR RectDeformation)
  {
  if ((RectDeformation.x1) || (RectDeformation.y1) || (RectDeformation.x2) || (RectDeformation.y2))
    {
		LONG offsetx;
		LONG offsety;
		LONG lx;
		LONG ly;

    OrienterRectGR (&RectDeformation);
    offsetx  = g_convert_dx_dev_to_page (pBdGr->hGsys, -TAILLE_PEL_DEFORMATION);
    offsety  = g_convert_dy_dev_to_page (pBdGr->hGsys, -TAILLE_PEL_DEFORMATION);
    lx = g_add_x_page (pBdGr->hGsys, RectDeformation.x1, offsetx);
    ly = g_add_y_page (pBdGr->hGsys, RectDeformation.y1, offsety);

    PositionnerRectDeformationGR (pBdGr, lx, ly);
    g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementRectanglePoignee);

    PositionnerRectDeformationGR (pBdGr, lx, RectDeformation.y2);
    g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementRectanglePoignee);

    PositionnerRectDeformationGR (pBdGr, RectDeformation.x2, ly);
    g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementRectanglePoignee);

    PositionnerRectDeformationGR (pBdGr, RectDeformation.x2, RectDeformation.y2);
    g_elementInvalidateRect (pBdGr->hGsys, pBdGr->helementRectanglePoignee);
    }
  }


// -------------------------------------------------------------------------
DWORD bdgr_nombre_marques_page (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD          wReferencePage;
  DWORD          wNombre = 0;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);

    wNombre = pMarque->NbMarques;
    }
  return wNombre;
  }

// -------------------------------------------------------------------------
// Renvoie la position du Ni�me �l�ment marqu� ou 0
DWORD bdgr_lire_element_marque (HBDGR hbdgr, DWORD Marque)
  {
  PBDGR	pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  DWORD wElement = 0;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);

    if ((Marque >= 1) && (Marque <= pMarque->NbMarques))
      {
			PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, Marque - 1 + pMarque->PremiereMarque);
			PPAGE_GR				pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);

      wElement = pGeoMarque->Element - pPage->PremierElement + 1;
      }
    }
  return wElement;
  } // bdgr_lire_element_marque

// -------------------------------------------------------------------------
DWORD bdgr_lire_marque_element (HBDGR hbdgr, DWORD Element)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  DWORD wMarque = 0;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR	pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);

    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD           wPositionElement = pPage->PremierElement + Element - 1;
			DWORD           wPositionGeoMarque = LireMarqueElementGR (wReferencePage, wPositionElement);

      if (wPositionGeoMarque > 0)
        {
				PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);

        wMarque = wPositionGeoMarque - pMarque->PremiereMarque + 1;
        }
      }
    }

  return wMarque;
  }


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//						Marquer des Elements
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// -------------------------------------------------------------------------
// Aucun �l�ment marqu� (modifie l'objet Marques et l'affichage)
void bdgr_demarquer_page (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD          wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
	  DWORD          wNbGeoMarques;
		DWORD          wPremiereGeoMarque;
		RECT_BDGR    Rectangle;
		PMARQUES_GR		pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);

    wNbGeoMarques = pMarque->NbMarques;
    if (wNbGeoMarques > 0)
      {
      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        bdgr_gommer_selection_page ((HBDGR)pBdGr);
        }
      if (ModeTravail & MODE_GOMME_DEFORMATION)
        {
        bdgr_gommer_deformation_page ((HBDGR)pBdGr);
        }
      pMarque->NbMarques = 0;
      wPremiereGeoMarque = pMarque->PremiereMarque;
      enleve_enr (szVERIFSource, __LINE__, wNbGeoMarques, B_GEO_MARQUES_GR, wPremiereGeoMarque);
			// mise � jour de l'objet Marques
      MajMarquesGR (MAJ_SUPPRESSION, wReferencePage, wPremiereGeoMarque, wNbGeoMarques);
      }
    Rectangle.x1 = 0;
    Rectangle.y1 = 0;
    Rectangle.x2 = 0;
    Rectangle.y2 = 0;

    MajRectMarquesGR (wReferencePage, Rectangle);
    }
  }

// -------------------------------------------------------------------------
void bdgr_marquer_page (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR					pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		DWORD             wPremierElement = pPage->PremierElement;
		DWORD             wNbElements = pPage->NbElements;
		PMARQUES_GR				pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD             wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD             wNbGeoMarques = pMarque->NbMarques;
		PGEO_MARQUES_GR		pGeoMarque;
		DWORD              i;
		RECT_BDGR        Rectangle;

    if ((wNbGeoMarques > 0) && (ModeTravail & MODE_GOMME_SELECTION))
      {
      bdgr_gommer_selection_page ((HBDGR)pBdGr);
      }

    if ((wNbGeoMarques > 0) && (ModeTravail & MODE_GOMME_DEFORMATION))
      {
      bdgr_gommer_deformation_page ((HBDGR)pBdGr);
      }

    pMarque->NbMarques = wNbElements;

    if (wNbElements > wNbGeoMarques)
      {
      insere_enr (szVERIFSource, __LINE__, wNbElements - wNbGeoMarques, B_GEO_MARQUES_GR, wPremiereGeoMarque);
      MajMarquesGR (MAJ_INSERTION, wReferencePage, wPremiereGeoMarque, wNbElements - wNbGeoMarques);
      }

    for (i = 0; i < wNbElements; i++)
      {
      pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPremiereGeoMarque + i);
      pGeoMarque->Element = wPremierElement + i;
      }

    CalculerRectMarquesGR (pBdGr, wReferencePage, &Rectangle);
    MajRectMarquesGR (wReferencePage, Rectangle);

    if (ModeTravail & MODE_DESSIN_SELECTION)
      {
      bdgr_dessiner_selection_page ((HBDGR)pBdGr);
      }

    if (ModeTravail & MODE_DESSIN_DEFORMATION)
      {
      bdgr_dessiner_deformation_page ((HBDGR)pBdGr);
      }
    }
  }


// -------------------------------------------------------------------------
BOOL bdgr_marquer_element (HBDGR hbdgr, DWORD Element, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  BOOL  bMarqueOk = FALSE;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR	pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		DWORD     wPositionElement;
		DWORD     wPositionGeoMarque;
		RECT_BDGR RectangleMarques;
		RECT_BDGR RectangleElmt;

    LireRectMarquesGR (wReferencePage, &RectangleMarques);
    if (ModeTravail & MODE_GOMME_DEFORMATION)
      {
      GommerPoigneesGR (pBdGr, RectangleMarques);
      }
    if ((Element > 0) && (Element <= pPage->NbElements))
      {
      wPositionElement = pPage->PremierElement + Element - 1;

      wPositionGeoMarque = LireMarqueElementGR (wReferencePage, wPositionElement);
      if (wPositionGeoMarque == 0)
        {
        wPositionGeoMarque = MettreMarqueGR (wReferencePage, wPositionElement);
        MajMarquesGR (MAJ_INSERTION, wReferencePage, wPositionGeoMarque, 1);
        EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement,
                        &(RectangleElmt.x1), &(RectangleElmt.y1),
                        &(RectangleElmt.x2), &(RectangleElmt.y2),
                        pBdGr->hGsys, pBdGr->helementTravail);
        AjouterRectGR (RectangleElmt, &RectangleMarques);
        MajRectMarquesGR (wReferencePage, RectangleMarques);
        if (ModeTravail & MODE_DESSIN_SELECTION)
          {
          DessinerSelectionGR (pBdGr, wPositionElement);
          }
        bMarqueOk = TRUE;
        }
      }
    if (ModeTravail & MODE_DESSIN_DEFORMATION)
      {
      DessinerPoigneesGR (pBdGr, RectangleMarques);
      }
    }
  return (bMarqueOk);
  }

// -------------------------------------------------------------------------
BOOL bdgr_demarquer_element (HBDGR hbdgr, DWORD Element, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  BOOL  bDemarqueOk = FALSE;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR		pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);

    if ((Element > 0) && (Element <= pPage->NbElements))
      {
			DWORD       wPositionElement = pPage->PremierElement + Element - 1;
			DWORD       wPositionGeoMarque = LireMarqueElementGR (wReferencePage, wPositionElement);
			RECT_BDGR  Rectangle;

      if (wPositionGeoMarque > 0)
        {
        EnleverMarqueGR (wReferencePage, wPositionGeoMarque);
        MajMarquesGR (MAJ_SUPPRESSION, wReferencePage, wPositionGeoMarque, 1);
        if (ModeTravail & MODE_GOMME_SELECTION)
          {
          GommerSelectionGR (pBdGr, wPositionElement);
          }
        LireRectMarquesGR (wReferencePage, &Rectangle);
        if (ModeTravail & MODE_GOMME_DEFORMATION)
          {
          GommerPoigneesGR (pBdGr, Rectangle);
          }
        CalculerRectMarquesGR (pBdGr, wReferencePage, &Rectangle);
        MajRectMarquesGR (wReferencePage, Rectangle);
        if (ModeTravail & MODE_DESSIN_DEFORMATION)
          {
          DessinerPoigneesGR (pBdGr, Rectangle);
          }
        bDemarqueOk = TRUE;
        }
      }
    }
  return bDemarqueOk;
  }

// -------------------------------------------------------------------------
void bdgr_marquer_page_elts_animes (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR					pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		DWORD             wPremierElement = pPage->PremierElement;
		DWORD             wNbElements = pPage->NbElements;
		DWORD							i;
		DWORD							wPositionElement;

		for (i = 1; i <= wNbElements; i++)
			{
			PELEMENT_PAGE_GR  pElement;

      wPositionElement = wPremierElement + i - 1;
      pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
      if (pElement->PosGeoAnimation != 0)
				{
				bdgr_marquer_element (hbdgr, i, ModeTravail);
				}
			}
		}
	}

// -------------------------------------------------------------------------
// S�lectionne le premier �l�ment situ� hors de la page et aligne le sur le bord d'�cran
BOOL bdgr_marquer_page_elt_hors_page (HBDGR hbdgr, DWORD ModeTravail)
  {
	BOOL	bRes = FALSE;
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
	RECT rectClient;

	// page existe, pas en icone et on r�cup�re la taille de sa zone cliente ?
  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage) && 
		(!::IsIconic(g_get_hwnd(pBdGr->hGsys))) && 
		::GetClientRect(g_get_hwnd(pBdGr->hGsys), &rectClient))
    {
		// oui => conversion de sa zone cliente en unit�s Device
		if (rectClient.right>0)
			rectClient.right--;
		if (rectClient.bottom>0)
			rectClient.bottom--;

		g_convert_point_dev_to_page (pBdGr->hGsys, &rectClient.right, &rectClient.bottom);

		// parcours de tous les �l�ments de la page
		PPAGE_GR	pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		DWORD     wPremierElement = pPage->PremierElement;
		DWORD     wNbElements = pPage->NbElements;

		for (DWORD i = 1; i <= wNbElements; i++)
			{
      DWORD	wPositionElement = wPremierElement + i - 1;
			PELEMENT_PAGE_GR  pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

			// R�cup�re l'encombrement de l'�lement courant
			RECT_BDGR rectElmt;
      EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement,
				&(rectElmt.x1), &(rectElmt.y1), &(rectElmt.x2), &(rectElmt.y2),
        pBdGr->hGsys, pBdGr->helementTravail);

			// par d�faut pas de d�placement � faire
			LONG dx = 0;
			LONG dy = 0;

			// �lement trop � gauche ?
			if (rectElmt.x2 < 0)
				// oui => aligne le sur le bord gauche de la fen�tre
				dx = - rectElmt.x1;
			else
				{
				// non=> �l�ment trop � droite ?
				if (rectElmt.x1 > rectClient.right)
					// oui => aligne le sur le bord droit de la fen�tre
					dx = rectClient.right - rectElmt.x2;
				}
			
			// �lement trop en haut ?
			if (rectElmt.y2 < 0)
				// oui => aligne le sur le bord haut de la fen�tre
				dy = - rectElmt.y1;
			else
				{
				// non=> �l�ment trop en bas ?
				if (rectElmt.y1 > rectClient.bottom)
					// oui => aligne le sur le bord bas de la fen�tre
					dy = rectClient.bottom - rectElmt.y2;
				}
			
			// Un d�placement � faire ?
      if ((dx != 0) ||(dy != 0))
				{
				// oui => on le marque puis on le d�place
				bdgr_marquer_element (hbdgr, i, ModeTravail);
				bdgr_translater_marques (hbdgr, MODE_MAJ_ECRAN, dx, dy);
				bRes = TRUE;

				// Mission accomplie, parcours termin�
				break;
				}
			}
		}
	return bRes;
	}

// -------------------------------------------------------------------------
void bdgr_demarquer_page_elts_animes (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR					pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		DWORD             wPremierElement = pPage->PremierElement;
		DWORD             wNbElements = pPage->NbElements;
		DWORD							i;
		DWORD							wPositionElement;

		for (i = 1; i <= wNbElements; i++)
			{
			PELEMENT_PAGE_GR  pElement;

      wPositionElement = wPremierElement + i - 1;
      pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
      if (pElement->PosGeoAnimation != 0)
				{
				bdgr_demarquer_element (hbdgr, i, ModeTravail);
				}
			}
		}
	}

// -------------------------------------------------------------------------
void bdgr_marquer_page_elts_non_animes (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR					pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		DWORD             wPremierElement = pPage->PremierElement;
		DWORD             wNbElements = pPage->NbElements;
		DWORD							i;
		DWORD							wPositionElement;

		for (i = 1; i <= wNbElements; i++)
			{
			PELEMENT_PAGE_GR  pElement;

      wPositionElement = wPremierElement + i - 1;
      pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
      if (pElement->PosGeoAnimation == 0)
				{
				bdgr_marquer_element (hbdgr, i, ModeTravail);
				}
			}
		}
	}

// -------------------------------------------------------------------------
void bdgr_demarquer_page_elts_non_animes (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR					pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
		DWORD             wPremierElement = pPage->PremierElement;
		DWORD             wNbElements = pPage->NbElements;
		DWORD							i;
		DWORD							wPositionElement;

		for (i = 1; i <= wNbElements; i++)
			{
			PELEMENT_PAGE_GR  pElement;

      wPositionElement = wPremierElement + i - 1;
      pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
      if (pElement->PosGeoAnimation == 0)
				{
				bdgr_demarquer_element (hbdgr, i, ModeTravail);
				}
			}
		}
	}

// -------------------------------------------------------------------------
// Ajouter la marque correspondant � l'objet le plus proche du point sp�cifi�
BOOL bdgr_marquer_point (HBDGR hbdgr, LONG x, LONG y, DWORD ModeTravail)
  {
  DWORD wElement = bdgr_pointer_element (hbdgr, x, y);

  return (bdgr_marquer_element (hbdgr, wElement, ModeTravail));
  }

// -------------------------------------------------------------------------
BOOL bdgr_demarquer_point (HBDGR hbdgr, LONG x, LONG y, DWORD ModeTravail)
  {
  DWORD wElement;

  wElement = bdgr_pointer_element (hbdgr, x, y);
  return (bdgr_demarquer_element (hbdgr, wElement, ModeTravail));
  }

// -------------------------------------------------------------------------
void bdgr_marquer_rectangle (HBDGR hbdgr, LONG x1, LONG y1, LONG x2, LONG y2, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PPAGE_GR		pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);

    if (pPage->NbElements > 0)
      {
			DWORD       wPremierElement = pPage->PremierElement;
			DWORD       wDernierElement = wPremierElement + pPage->NbElements;
			DWORD       wPositionElement;
			DWORD       wPositionGeoMarque;
			RECT_BDGR  Rectangle;
			RECT_BDGR  RectangleMarques;

      LireRectMarquesGR (wReferencePage, &RectangleMarques);
      if (ModeTravail & MODE_GOMME_DEFORMATION)
        {
        GommerPoigneesGR (pBdGr, RectangleMarques);
        }

      //----------- Arrangement du Rectangle $$ programm� ailleurs
      if (x1 > x2)
        {
        Rectangle.x1 = x1;
        x1 = x2;
        x2 = Rectangle.x1;
        }
      if (y1 > y2)
        {
        Rectangle.y1 = y1;
        y1 = y2;
        y2 = Rectangle.y1;
        }
      //
      for (wPositionElement = wPremierElement; wPositionElement < wDernierElement; wPositionElement++)
        {
        EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement,
                        &(Rectangle.x1), &(Rectangle.y1),
                        &(Rectangle.x2), &(Rectangle.y2),
                        pBdGr->hGsys, pBdGr->helementTravail);

        //---------- Si Boite inclue dans Rectangle
        //
        if ( ((Rectangle.x1 >= x1) && (Rectangle.x2 <= x2)) &&
             ((Rectangle.y1 >= y1) && (Rectangle.y2 <= y2)) )
          {
          if (LireMarqueElementGR (wReferencePage, wPositionElement) == 0)
            {
            wPositionGeoMarque = MettreMarqueGR (wReferencePage, wPositionElement);
            MajMarquesGR (MAJ_INSERTION, wReferencePage, wPositionGeoMarque, 1);
            AjouterRectGR (Rectangle, &RectangleMarques);
            if (ModeTravail & MODE_DESSIN_SELECTION)
              {
              DessinerSelectionGR (pBdGr, wPositionElement);
              }
            }
          }
        }
      MajRectMarquesGR (wReferencePage, RectangleMarques);
      if (ModeTravail & MODE_DESSIN_DEFORMATION)
        {
        DessinerPoigneesGR (pBdGr, RectangleMarques);
        }
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_demarquer_rectangle (HBDGR hbdgr, LONG x1, LONG y1, LONG x2, LONG y2, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD              wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR				pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD             wPremiereGeoMarque;
		DWORD             wDerniereGeoMarque;
		DWORD             wPositionGeoMarque;
		PGEO_MARQUES_GR		pGeoMarque;
		DWORD             wPositionElement;
		RECT_BDGR        Rectangle;
		LONG               ix1;
		LONG               iy1;
		LONG               ix2;
		LONG               iy2;

    if (pMarque->NbMarques > 0)
      {
      // Arrangement du Rectangle
      if (x1 > x2)
        {
        ix1 = x1;
        x1 = x2;
        x2 = ix1;
        }
      if (y1 > y2)
        {
        iy1 = y1;
        y1 = y2;
        y2 = iy1;
        }
      //
      wPremiereGeoMarque = pMarque->PremiereMarque;
      wDerniereGeoMarque = wPremiereGeoMarque + pMarque->NbMarques;

      wPositionGeoMarque = wPremiereGeoMarque;
      while (wPositionGeoMarque < wDerniereGeoMarque)
        {
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
        wPositionElement = pGeoMarque->Element;
        EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &ix1, &iy1, &ix2, &iy2, pBdGr->hGsys, pBdGr->helementTravail);

        // Boite inclue dans Rectangle ?
        if ( ((ix1 >= x1) && (ix2 <= x2)) && ((iy1 >= y1) && (iy2 <= y2)) )
          {
          wPositionGeoMarque = LireMarqueElementGR (wReferencePage, wPositionElement);
          if (wPositionGeoMarque > 0)
            {
            EnleverMarqueGR (wReferencePage, wPositionGeoMarque);
            MajMarquesGR (MAJ_SUPPRESSION, wReferencePage, wPositionGeoMarque, 1);
            if (ModeTravail & MODE_GOMME_SELECTION)
              {
              GommerSelectionGR (pBdGr, wPositionElement);
              }
            wDerniereGeoMarque--;
            }
          else
            {
            wPositionGeoMarque++;
            }
          }
        else
          {
          wPositionGeoMarque++;
          }
        }
      LireRectMarquesGR (wReferencePage, &Rectangle);
      if (ModeTravail & MODE_GOMME_DEFORMATION)
        {
        GommerPoigneesGR (pBdGr, Rectangle);
        }
      CalculerRectMarquesGR (pBdGr, wReferencePage, &Rectangle);
      MajRectMarquesGR (wReferencePage, Rectangle);
      if (ModeTravail & MODE_DESSIN_DEFORMATION)
        {
        DessinerPoigneesGR (pBdGr, Rectangle);
        }
      }
    }
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Travail sur les Marques : Attention ni les �l�ments anim�s ni les contr�les ne sont groupables
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// -------------------------------------------------------------------------
// Groupe les �l�ments marqu�s (Attention, �l�ments anim�s non groupables)
void bdgr_grouper_marques (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD     wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wNbElementsMarques = pMarque->NbMarques;

		// plus d'un �l�ment marqu� ?
    if (wNbElementsMarques > 1)
      {
			// Copie des �l�ments marqu�s ni anim�s ni controles dans une liste
			DWORD                   wPremiereGeoMarque = pMarque->PremiereMarque;
			DWORD                   wPositionGeoMarque = wPremiereGeoMarque;
			DWORD                   wPremierElementListe = nb_enregistrements (szVERIFSource, __LINE__, B_ELEMENTS_LISTES_GR) + 1;
			DWORD                   wPositionElementListe = wPremierElementListe;
			DWORD                   wNbElementsGroupes = 0;

			DWORD                   wPositionElementPage;
			DWORD                   wIndex;
			PGEO_MARQUES_GR					pGeoMarque;
			PPAGE_GR								pPage;
			PELEMENT_PAGE_GR				pElement;
			DWORD                   wDernierGeoListe;
			DWORD                   wPositionGeoListe;
			BOOL										bPasTrouve;
			PGEO_ELEMENTS_LISTES_GR	pGeoListe;

      for (wIndex = 0; wIndex < wNbElementsMarques; wIndex++)
        {
				// acc�de � l'�l�ment courant
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
        wPositionElementPage = pGeoMarque->Element;
        pElement  = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElementPage);

				// �l�ment peut �tre inclus dans une liste ?
        if ((pElement->PosGeoAnimation == 0) && (!bAttributsActionGr (pElement->nAction, G_ATTRIBUT_CONTROLE)))
          {
					// oui => transf�re le des �l�ments marqu�s aux �l�ments de la liste
          insere_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_LISTES_GR, wPositionElementListe);

					// Efface la s�lection courante
          if (ModeTravail & MODE_GOMME_SELECTION)
            {
            GommerSelectionGR (pBdGr, wPositionElementPage);
            }
          CopierElementGR (B_ELEMENTS_PAGES_GR, wPositionElementPage, B_ELEMENTS_LISTES_GR, wPositionElementListe);
          SupprimerElementGR (B_ELEMENTS_PAGES_GR, wPositionElementPage);
          enleve_enr (szVERIFSource, __LINE__, 1, B_GEO_MARQUES_GR, wPositionGeoMarque);
          MajGeoMarquesGR (MAJ_SUPPRESSION, wPositionElementPage, 1);
          wPositionElementListe++;
          wNbElementsGroupes++;
          }
        else
          {
          wPositionGeoMarque++;
          }
        }
      // des �l�ments ont �t� transf�r�s dans ce bloc de liste ?
      if (wNbElementsGroupes != 0)
        {
				// oui => mise � jour des �l�ments de la page
        pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
        pPage->NbElements = pPage->NbElements - wNbElementsGroupes + 1;
        wPositionElementPage = pPage->PremierElement + pPage->NbElements - 1;
        //
        wDernierGeoListe = nb_enregistrements (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR) + 1;
        wPositionGeoListe = 1;
        bPasTrouve = TRUE;
        while ((wPositionGeoListe < wDernierGeoListe) && (bPasTrouve))
          {
          pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPositionGeoListe);
          bPasTrouve = (BOOL) (pGeoListe->NbReferencesListe != 0);
          wPositionGeoListe++;
          }
        if (bPasTrouve)
          {
          wPositionGeoListe = wDernierGeoListe;
          insere_enr (szVERIFSource, __LINE__, 1, B_GEO_ELEMENTS_LISTES_GR, wDernierGeoListe);
          }
        else
          {
          wPositionGeoListe--;
          }
        pGeoListe = (PGEO_ELEMENTS_LISTES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_ELEMENTS_LISTES_GR, wPositionGeoListe);
        pGeoListe->NbReferencesListe = 1;
        pGeoListe->NbElements        = wNbElementsGroupes;
        pGeoListe->PremierElement    = wPremierElementListe;

        // insere dans la page la nouvelle liste
        insere_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_PAGES_GR, wPositionElementPage);
        pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElementPage);
        pElement->nAction							= G_ACTION_LISTE;
        pElement->nCouleurLigne				= G_COULEUR_INDETERMINEE;
        pElement->nCouleurRemplissage = G_COULEUR_INDETERMINEE;
        pElement->nStyleLigne					= G_STYLE_LIGNE_INDETERMINE;
        pElement->nStyleRemplissage		= G_STYLE_REMPLISSAGE_INDETERMINE;
        pElement->pt0.x              = 0;
        pElement->pt0.y              = 0;
        pElement->pt1.x              = 0; // non utilis� (ex style remplissage)
        pElement->pt1.y              = wPositionGeoListe;
				pElement->hwndControle			 = NULL;
        pElement->PosGeoAnimation = 0;
				GetIdAutoPcs(ELEMENT_PAGE_GRAPHIQUE, pElement->strIdentifiant);
        //
        MajPagesGR (MAJ_SUPPRESSION, wReferencePage, wPositionElementPage, wNbElementsGroupes - 1);
        MajGeoMarquesGR (MAJ_INSERTION, wPositionElementPage, 1);

        // Elemnts marqu�s = la nouvelle liste
        wPositionGeoMarque = wPremiereGeoMarque + wNbElementsMarques - wNbElementsGroupes;
        insere_enr (szVERIFSource, __LINE__, 1, B_GEO_MARQUES_GR, wPositionGeoMarque);
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
        pGeoMarque->Element = wPositionElementPage;
        //
        pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
        pMarque->NbMarques = wNbElementsMarques - wNbElementsGroupes + 1;
        MajMarquesGR (MAJ_SUPPRESSION, wReferencePage, wPositionGeoMarque, wNbElementsGroupes - 1);

        // affiche la nouvelle liste
        if (ModeTravail & MODE_DESSIN_SELECTION)
          {
          DessinerSelectionGR (pBdGr, wPositionElementPage);
          }
        } // if (wNbElementsGroupes != 0)
      } // if (wNbElementsMarques > 1)
    } // if (ExistePageGR (pBdGr->NPage, &wReferencePage))
  } // bdgr_grouper_marques


// -------------------------------------------------------------------------
// Fait passer les �l�ments marqu�s au premier plan ou � l'arri�re plan (Attention, �l�ments anim�s non groupables)
void bdgr_change_profondeur_marques (HBDGR hbdgr, BOOL bPremierPlan, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);

		// Au moins un �l�ment marqu� ?
    if (pMarque->NbMarques >= 1)
      {
			// oui => efface les �l�ments � d�placer et leur s�lection
			DWORD			wPosGeoMarqueRangement;
			DWORD     wDernierElementMarques = pMarque->PremiereMarque + pMarque->NbMarques;
			DWORD     wIndex;
			PPAGE_GR	pPage;

      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        bdgr_gommer_selection_page (hbdgr);
        }
      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        bdgr_gommer_elements_marques (hbdgr);
        }

			// Trie les marques par position d'�l�ment croissant
			pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);

			// Stocke par ordre croissant dans la table des marques les valeurs croissantes de position d'�l�ment
			for (wPosGeoMarqueRangement = pMarque->PremiereMarque; wPosGeoMarqueRangement < wDernierElementMarques; wPosGeoMarqueRangement++)
				{
				PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPosGeoMarqueRangement);
				DWORD	wPosGeoMarqueMin = wPosGeoMarqueRangement;
				DWORD	wPosElementMin = pGeoMarque->Element;

				// recherche la position de la marque correspondant � la plus petite position d'�l�ment
				for (wIndex = wPosGeoMarqueRangement + 1; wIndex < wDernierElementMarques; wIndex++)
					{
					PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wIndex);

					if (pGeoMarque->Element < wPosElementMin)
						{
						wPosGeoMarqueMin = wIndex;
						wPosElementMin = pGeoMarque->Element;
						}
					}

				// marque courante autre que position de l'�l�ment min ?
				if (pGeoMarque->Element != wPosElementMin)
					{
					DWORD	wPosTemp;

					// oui => �change son contenu � l'emplacement de remplissage courant
					PGEO_MARQUES_GR	pGeoMarqueMin = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPosGeoMarqueMin);
//$$ Manque une fonction EchangeEnr !!
					wPosTemp = pGeoMarque->Element;
					pGeoMarque->Element = pGeoMarqueMin->Element;
					pGeoMarqueMin->Element = wPosTemp;
					}
				}

			/*
			// $$ Debug parcours du bloc de marques apr�s tri
			for (wIndex = 0; wIndex < pMarque->NbMarques; wIndex++)
				{
				PGEO_MARQUES_GR	pGeoMarque = pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, pMarque->PremiereMarque + wIndex);
				PELEMENT_PAGE_GR pElement = pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, pGeoMarque->Element);
				ELEMENT_PAGE_GR			ElementTemp = *pElement;
				}
*/

			// marques tri�es par position d'�l�ment croissants
			// Passage des �l�ments marqu�s au premier plan ?
			if (bPremierPlan)
				{
				DWORD	dwNDernierElementPage = pPage->PremierElement + pPage->NbElements -1;

				// oui => Marqu�s au premier plan :
				// pour chaque �l�ment marqu� (par ordre croissant)
				for (wIndex = 0; wIndex < pMarque->NbMarques; wIndex++)
					{
					// Element en fin de page ?
					PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, pMarque->PremiereMarque + wIndex);

					DWORD	dwNEnrPageADeplacer = pGeoMarque->Element - wIndex;

					if (dwNEnrPageADeplacer != dwNDernierElementPage)
						{
						// non => mets le en fin de page : met de cot� cet �l�ment
						PELEMENT_PAGE_GR pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, dwNEnrPageADeplacer);
						ELEMENT_PAGE_GR	ElementTemp = *pElement;
						
						// tasse tous les �l�ments suivants de la page
						trans_enr (szVERIFSource, __LINE__, dwNDernierElementPage - dwNEnrPageADeplacer, B_ELEMENTS_PAGES_GR, B_ELEMENTS_PAGES_GR, 
							dwNEnrPageADeplacer + 1, dwNEnrPageADeplacer);

						// dernier �l�ment = celui mis de cot�
						pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, dwNDernierElementPage);
						*pElement = ElementTemp;
						}
					// La marque pointera sur un �l�ment en fin de page
					pGeoMarque->Element = dwNDernierElementPage - pMarque->NbMarques + wIndex + 1;
					}
				}
			else
				{
				// non => Marqu�s en arri�re plan : d�placement des �l�ments marqu�s en d�but de page

				// pour chaque �l�ment marqu� (par ordre croissant)
				for (wIndex = 0; wIndex < pMarque->NbMarques; wIndex++)
					{
					// Element marqu� � d�placer ?
					DWORD	dwNEnrPageDest = pPage->PremierElement+wIndex;
					PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, pMarque->PremiereMarque + wIndex);
					DWORD	dwNEnrPageSource = pGeoMarque->Element;

					if (dwNEnrPageDest != dwNEnrPageSource)
						{
						// oui => mets le � sa place : mets cet �l�ment source de cot�
						PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, dwNEnrPageSource);
						ELEMENT_PAGE_GR		ElementTemp = *pElement;
						
						// d�place les �l�ments interm�diaires vers la source
						trans_enr (szVERIFSource, __LINE__, dwNEnrPageSource - dwNEnrPageDest, B_ELEMENTS_PAGES_GR, B_ELEMENTS_PAGES_GR, 
							dwNEnrPageDest, dwNEnrPageDest + 1);

						// �l�ment dest = celui mis de cot�
						pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, dwNEnrPageDest);
						*pElement = ElementTemp;
						}
					// La marque pointera sur un �l�ment en d�but de page
					pGeoMarque->Element = pPage->PremierElement + wIndex;
					}
				}
      } // if (pMarque->NbMarques >= 1)
    } // if (ExistePageGR ...
  } // bdgr_change_profondeur_marques

// -------------------------------------------------------------------------
BOOL bdgr_degrouper_marques (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  BOOL  bDemarquable = TRUE;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR				pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD             wPremiereGeoMarque;
		DWORD             wDerniereGeoMarque;
		DWORD             wPositionGeoMarque;
		PGEO_MARQUES_GR		pGeoMarque;
		DWORD             wPositionElement;
		PELEMENT_PAGE_GR	pElement;
		DWORD             wNbElementsListe;
		DWORD             wIndex;
		PPAGE_GR					pPage;

    if (pMarque->NbMarques > 0)
      {
      //
      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        bdgr_gommer_selection_page (hbdgr);
        }
      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        bdgr_gommer_elements_marques (hbdgr);
        }
      //
      wPremiereGeoMarque = pMarque->PremiereMarque;
      wDerniereGeoMarque = wPremiereGeoMarque + pMarque->NbMarques;
      wPositionGeoMarque = wPremiereGeoMarque;
      while ((wPositionGeoMarque < wDerniereGeoMarque) && (bDemarquable))
        {
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
        wPositionElement = pGeoMarque->Element;
        pElement  = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
        bDemarquable = (BOOL)(pElement->PosGeoAnimation == 0);
        wPositionGeoMarque++;
        }
      if (bDemarquable)
        {
        wPositionGeoMarque = wPremiereGeoMarque;
        while (wPositionGeoMarque < wDerniereGeoMarque)
          {
          pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
          wPositionElement = pGeoMarque->Element;
          wNbElementsListe = DegrouperElementPageGR (hbdgr, wPositionElement, pBdGr->hGsys, pBdGr->helementTravail);
          if (wNbElementsListe > 0)
            {
            pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
            pPage->NbElements = pPage->NbElements + wNbElementsListe - 1;
            //
            MajPagesGR (MAJ_INSERTION, wReferencePage, wPositionElement, wNbElementsListe - 1);
            MajGeoMarquesGR (MAJ_INSERTION,  wPositionElement, wNbElementsListe - 1);
            //
            insere_enr (szVERIFSource, __LINE__, wNbElementsListe, B_GEO_MARQUES_GR, wDerniereGeoMarque);
            for (wIndex = 0; wIndex < wNbElementsListe; wIndex++)
              {
              pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wDerniereGeoMarque + wIndex);
              pGeoMarque->Element = wPositionElement + wIndex;
              }
            //
            enleve_enr (szVERIFSource, __LINE__, 1, B_GEO_MARQUES_GR, wPositionGeoMarque);
            //
            pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
            pMarque->NbMarques = pMarque->NbMarques + wNbElementsListe - 1;
            //
            MajMarquesGR (MAJ_INSERTION, wReferencePage, wDerniereGeoMarque, wNbElementsListe - 1);
            //
            wDerniereGeoMarque--;
            }
          else
            {
            wPositionGeoMarque++;
            }
          }
        }
      //
      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        bdgr_dessiner_elements_marques (hbdgr);
        }
      if (ModeTravail & MODE_DESSIN_SELECTION)
        {
        bdgr_dessiner_selection_page (hbdgr);
        }
      //
      }
    }
  return bDemarquable;
  }

// -------------------------------------------------------------------------
void bdgr_boite_marques (HBDGR hbdgr, LONG *x1, LONG *y1, LONG *x2, LONG *y2)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		RECT_BDGR        Rectangle;

    LireRectMarquesGR (wReferencePage, &Rectangle);
    *x1 = Rectangle.x1;
    *x2 = Rectangle.x2;
    *y1 = Rectangle.y1;
    *y2 = Rectangle.y2;
    }
  }

// -------------------------------------------------------------------------
// Change la couleur globale des �l�ments marqu�s
void bdgr_couleur_marques (HBDGR hbdgr, DWORD ModeTravail, G_COULEUR Couleur)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD              wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
	  PMARQUES_GR pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD       wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD       wPositionGeoMarque;
		
    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
			PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
			DWORD           wPositionElement = pGeoMarque->Element;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);

      ForcerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, Couleur, Couleur, G_STYLE_LIGNE_INDETERMINE, G_STYLE_REMPLISSAGE_INDETERMINE);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_style_ligne_marques (HBDGR hbdgr, DWORD ModeTravail, G_STYLE_LIGNE Style)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD              wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD       wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD       wPositionGeoMarque;
		
    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
 			PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
			DWORD								wPositionElement = pGeoMarque->Element;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);

      ForcerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, G_COULEUR_INDETERMINEE, G_COULEUR_INDETERMINEE,
				Style, G_STYLE_REMPLISSAGE_INDETERMINE);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_style_remplissage_marques (HBDGR hbdgr, DWORD ModeTravail, G_STYLE_REMPLISSAGE Style)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD              wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD       wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD       wPositionGeoMarque;

    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
			PGEO_MARQUES_GR pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
			DWORD              wPositionElement = pGeoMarque->Element;

      if (ModeTravail & MODE_GOMME_ELEMENT)
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);

      ForcerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, G_COULEUR_INDETERMINEE, G_COULEUR_INDETERMINEE,
				G_STYLE_LIGNE_INDETERMINE, Style);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  }

// -------------------------------------------------------------------------
// Modification du style de Caract�re des �l�ments marqu�s
void bdgr_style_texte_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD Police, DWORD StylePolice, DWORD TaillePolicePixel)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR				pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage); // $$init initialement apr�s GommerPoigneesGR
		DWORD              wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD              wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD              wPositionGeoMarque;
		PGEO_MARQUES_GR		pGeoMarque;
		DWORD              wPositionElement;
		RECT_BDGR        Rectangle;

    LireRectMarquesGR (wReferencePage, &Rectangle);
    if (ModeTravail & MODE_GOMME_DEFORMATION)
      {
      GommerPoigneesGR (pBdGr, Rectangle);
      }

    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
      pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
      wPositionElement = pGeoMarque->Element;
      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        GommerSelectionGR (pBdGr, wPositionElement);
        }
      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      ForcerStyleTexteElementGR (hbdgr, B_ELEMENTS_PAGES_GR, wPositionElement, Police, StylePolice, TaillePolicePixel);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      if (ModeTravail & MODE_DESSIN_SELECTION)
        {
        DessinerSelectionGR (pBdGr, wPositionElement);
        }
      }
    CalculerRectMarquesGR (pBdGr, wReferencePage, &Rectangle);
    MajRectMarquesGR (wReferencePage, Rectangle);
    if (ModeTravail & MODE_DESSIN_DEFORMATION)
      {
      DessinerPoigneesGR (pBdGr, Rectangle);
      }
    }
  }

// -------------------------------------------------------------------------
BOOL bdgr_ligne_anim_marques (HBDGR hbdgr, DWORD n_ligne)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  BOOL  bOk = FALSE;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wNbMarques = pMarque->NbMarques;

    if (wNbMarques == 1)
      {
			DWORD             wPositionGeoMarque = pMarque->PremiereMarque;
			PGEO_MARQUES_GR		pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
			DWORD             wPositionElement = pGeoMarque->Element;
			PELEMENT_PAGE_GR	pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);

      bOk = TRUE;
      pElement->PosGeoAnimation = n_ligne;
      }
    }
  return bOk;
  }

// -------------------------------------------------------------------------
DWORD bdgr_param_animation_marques (HBDGR hbdgr, ANIMATION CtxtAnimation)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  DWORD wRetour = 0;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR				pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD             wPositionGeoMarque;
		PGEO_MARQUES_GR	pGeoMarque;
		DWORD             wPositionElement;
		PELEMENT_PAGE_GR	pElement;
		DWORD             wLigneAnim;
		ANIMATION         TypeAnimation;
		DWORD							wNbMarques = pMarque->NbMarques;

    switch (wNbMarques)
      {
      case 0:
        wRetour = IDSERR_VEUILLEZ_SELECTIONNER_LE_SYMBOLE_A_ANIMER;
        break;
      case 1:
        wPositionGeoMarque = pMarque->PremiereMarque;
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
        wPositionElement = pGeoMarque->Element;
        pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
				TypeAnimation = CtxtAnimation;
				bdanmgr_TypeAnimation (pElement->nAction, &TypeAnimation);


        wLigneAnim = pElement->PosGeoAnimation;

				// param�trer l'�l�ment graphique
        wRetour = bdanmgr_parametrer (TypeAnimation, hbdgr, wLigneAnim, &wLigneAnim, wPositionElement);

				// Invalider la s�lection
        GommerSelectionGR (pBdGr, wPositionElement);
        if (wRetour == 0)
          {
          pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
          pElement->PosGeoAnimation = wLigneAnim;
          }

				// redessiner la s�lection
        DessinerSelectionGR (pBdGr, wPositionElement);
        break;
      default:
        wRetour = IDSERR_VEUILLEZ_SELECTIONNER_UN_SEUL_SYMBOLE_POUR_EFFECTUER_CETTE_OPERATION;
        break;
      }
    }
  return wRetour;
  } // bdgr_param_animation_marques

// -------------------------------------------------------------------------
//  Modifiacation de l'identifiant de l'�lement marqu�
DWORD bdgr_change_identifiant_marques (HBDGR hbdgr)
	{
	DWORD dwRetour = 0;
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR				pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD             wPositionGeoMarque;
		PGEO_MARQUES_GR	pGeoMarque;
		DWORD             wPositionElement;
		PELEMENT_PAGE_GR	pElement;
		DWORD							wNbMarques = pMarque->NbMarques;
		char							strIdentifiant [c_NB_CAR_ID_ELT_GR + 1];

    switch (wNbMarques)
      {
      case 0:
        dwRetour = IDSERR_VEUILLEZ_SELECTIONNER_L_ELEMENT_A_IDENTIFIER;
        break;
      case 1:
        wPositionGeoMarque = pMarque->PremiereMarque;
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
        wPositionElement = pGeoMarque->Element;
				StrCopy (strIdentifiant, LireIdentifiantElement(wPositionElement));

				if (dlg_saisie_identifiant_gr(strIdentifiant))
					{
					if (!StrIsNull (strIdentifiant))
						{
						if (StrLength(strIdentifiant) <= c_NB_CAR_ID_ELT_GR)
							{
							__int64 i64Temp = 0;

							if (!StrToI64 (&i64Temp, strIdentifiant))
								{
								if (!bIdentifiantDejaPresent(B_ELEMENTS_PAGES_GR, wReferencePage, strIdentifiant))
									{
									pElement = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPositionElement);
									StrCopy (pElement->strIdentifiant, strIdentifiant);
									if (pElement->PosGeoAnimation != 0)
										{
										dwRetour = bdanmgr_maj_identifiant_graphique (pElement->PosGeoAnimation, pElement->strIdentifiant);
										}
									}
								else
									{
									dwRetour = IDSERR_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EN_DOUBLON;
									}
								}
							else
								{
								dwRetour = IDSERR_L_IDENTIFIANT_NE_DOIT_PAS_ETRE_NUMERIQUE;
								}
							}
						else
							{
							dwRetour = IDSERR_L_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EST_TROP_LONG_20_CAR_MAX;
							}
						}
					else
						{
						dwRetour = IDSERR_L_IDENTIFIANT_D_ELEMENT_GRAPHIQUE_EST_INVALIDE;
						}
					}

        break;
      default:
        dwRetour = IDSERR_VEUILLEZ_SELECTIONNER_UN_SEUL_SYMBOLE_POUR_EFFECTUER_CETTE_OPERATION;
        break;
      }
    }
	return (dwRetour);
	}

// -------------------------------------------------------------------------
// Supprime les �l�ments marqu�s
BOOL bdgr_supprimer_marques (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;
  BOOL  bRetour = TRUE;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR		pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		RECT_BDGR			Rectangle;

		// Des �l�ments sont marqu�s ?
    if (pMarque->NbMarques > 0)
      {
			// oui => regarde si, parmi les marqu�s, certains sont supprimables
			DWORD             wNbGeoMarques = pMarque->NbMarques;
			DWORD             wPremiereGeoMarque = pMarque->PremiereMarque;
			DWORD             wDerniereGeoMarque = wPremiereGeoMarque + wNbGeoMarques;
			DWORD             wIndex = wPremiereGeoMarque;
			BOOL							bSuppressionPossible = TRUE;
			PGEO_MARQUES_GR	pGeoMarque;
			DWORD             wPositionElement;

      while ((wIndex < wDerniereGeoMarque) && (bSuppressionPossible))
        {
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wIndex);
        wPositionElement = pGeoMarque->Element;
        bSuppressionPossible = SuppressionPossibleElementGR (B_ELEMENTS_PAGES_GR, wPositionElement);
        wIndex++;
        }
			// Suppression des �l�ments possible ?
      if (bSuppressionPossible)
        {
				// oui => c'est parti
				PPAGE_GR					pPage;

        LireRectMarquesGR (wReferencePage, &Rectangle);
        if (ModeTravail & MODE_GOMME_DEFORMATION)
          {
          GommerPoigneesGR (pBdGr, Rectangle);
          }
        pMarque->NbMarques = 0;
        for (wIndex = 0; wIndex < wNbGeoMarques; wIndex++)
          {
          pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPremiereGeoMarque);
          wPositionElement = pGeoMarque->Element;
          if (ModeTravail & MODE_GOMME_SELECTION)
            {
            GommerSelectionGR (pBdGr, wPositionElement);
            }
          if (ModeTravail & MODE_GOMME_ELEMENT)
            {
            GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
            }
          SupprimerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement);
          pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePage);
          pPage->NbElements--;
          MajPagesGR (MAJ_SUPPRESSION, wReferencePage, wPositionElement, 1);
          MajGeoMarquesGR (MAJ_SUPPRESSION, wPositionElement, 1);
          enleve_enr (szVERIFSource, __LINE__, 1, B_GEO_MARQUES_GR, wPremiereGeoMarque);
          }
        MajMarquesGR (MAJ_SUPPRESSION, wReferencePage, wPremiereGeoMarque, wNbGeoMarques);
        Rectangle.x1 = 0;
        Rectangle.x2 = 0;
        Rectangle.y1 = 0;
        Rectangle.y2 = 0;
        MajRectMarquesGR (wReferencePage, Rectangle);
        }
      else
        {
        bRetour = FALSE;
        }
      }
    else
      {
      Rectangle.x1 = 0;
      Rectangle.x2 = 0;
      Rectangle.y1 = 0;
      Rectangle.y2 = 0;
      MajRectMarquesGR (wReferencePage, Rectangle);
      }
    }
  return bRetour;
  }

// -------------------------------------------------------------------------
// Dupliquer les �l�ments marqu�s
DWORD bdgr_dupliquer_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD dwIdPageSource, BOOL bAvecAnimation)
  {
  PBDGR	pBdGr = (PBDGR)hbdgr;
  DWORD dwIdPageDest = pBdGr->dwIdPage;
  DWORD wReferencePageSource;
  DWORD wRes = 0; // par d�faut pas d'erreur

	// Page source existe ?
  if (ExistePageGR (dwIdPageSource, &wReferencePageSource))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePageSource);

		// oui => des �l�ments y sont marqu�s ?
    if (pMarque->NbMarques > 0)
      {
			DWORD        wPremiereGeoMarque;
			DWORD        wNbGeoMarques;
			RECT_BDGR    Rectangle;

      // Page Source == Destination ?
      if (dwIdPageSource == dwIdPageDest)
        {
				// oui => effacer (si demand�) les marques des �l�ments sources
        if (ModeTravail & MODE_GOMME_SELECTION)
          {
          bdgr_gommer_selection_page (hbdgr);
          }

        LireRectMarquesGR (wReferencePageSource, &Rectangle);
        if (ModeTravail & MODE_GOMME_DEFORMATION)
          {
          GommerPoigneesGR (pBdGr, Rectangle);
          }

				// Supprime le marquage des �l�ments
        wPremiereGeoMarque = pMarque->PremiereMarque;
        wNbGeoMarques = pMarque->NbMarques;
        pMarque->NbMarques = 0;
        }
      else
        {
        wPremiereGeoMarque = pMarque->PremiereMarque;
        wNbGeoMarques = pMarque->NbMarques;
        }

      // page destination valide ?
      if (dwIdPageDest > 0)
        {
				PPAGE_GR					pPage;
				DWORD             wPosElementSource;
				DWORD             wPosElementDest;
				PGEO_MARQUES_GR	pGeoMarque;
				DWORD							wIndex;
				DWORD							wReferencePageDest;
				BOOL							bInsereApres;
				PELEMENT_PAGE_GR	pElementDest;

				// Page destination n'existe pas ?
        if (!ExistePageGR (dwIdPageDest, &wReferencePageDest))
					// oui => Cr�er Page Destination $$ A priori jamais utilis� CreePageGR (dwIdPageDest, &wReferencePageDest);
					VerifWarningExit;

        // Ajout des �lements marqu�s � la page Destination et suppression de leurs Marques :
				// Pr�pare l'ajout dans page dest
        pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePageDest);
        wPosElementDest = pPage->PremierElement + pPage->NbElements;
        pPage->NbElements = pPage->NbElements + wNbGeoMarques;

				// Duplication et redessin �l�ment par �l�ment
        pPage = (PPAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_PAGES_GR, wReferencePageSource);
				bInsereApres = wPosElementDest > pPage->PremierElement;
        for (wIndex = 0; wIndex < wNbGeoMarques; wIndex++)
          {
          pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPremiereGeoMarque + wIndex);
          wPosElementSource = pGeoMarque->Element;
          insere_enr (szVERIFSource, __LINE__, 1, B_ELEMENTS_PAGES_GR, wPosElementDest + wIndex);
          if (bInsereApres)
						{
						DupliquerElementGR (hbdgr, B_ELEMENTS_PAGES_GR, wPosElementSource, B_ELEMENTS_PAGES_GR, wPosElementDest + wIndex);
						}
					else
						{
						//d�calage de la posititon source provoqu� par l'insere_enr de la destination
						DupliquerElementGR (hbdgr, B_ELEMENTS_PAGES_GR, wPosElementSource + wIndex + 1, B_ELEMENTS_PAGES_GR, wPosElementDest + wIndex);
						}

					// forcage de l'identifiant de l'element dupliqu� avec un nouvel identifiant
					pElementDest = (PELEMENT_PAGE_GR)pointe_enr (szVERIFSource, __LINE__, B_ELEMENTS_PAGES_GR, wPosElementDest + wIndex);
					GetIdAutoPcs(ELEMENT_PAGE_GRAPHIQUE, pElementDest->strIdentifiant);

					// Dessine si demand� l'�l�ment dupliqu�
          if (ModeTravail & MODE_DESSIN_ELEMENT)
            DessinerElementGR (B_ELEMENTS_PAGES_GR, wPosElementDest + wIndex, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
          if (ModeTravail & MODE_DESSIN_SELECTION)
            DessinerSelectionGR (pBdGr, wPosElementDest + wIndex);
					// Cr�e si demand� la fen�tre de l'�ventuel contr�le
					if (ModeTravail & (MODE_DESSIN_ELEMENT | MODE_DESSIN_SELECTION | MODE_GOMME_SELECTION))
						bCreerFenetreElementGr (B_ELEMENTS_PAGES_GR, wPosElementDest + wIndex, hbdgr, PREMIER_ID_PAGE+wPosElementDest + wIndex);
          } //for (wIndex = 0; wIndex < wNbGeoMarques; wIndex++)

				// Duplication dans la m�me page ?
        if (dwIdPageSource == dwIdPageDest)
          // oui => Enleve les marques de cette page
          enleve_enr (szVERIFSource, __LINE__, wNbGeoMarques, B_GEO_MARQUES_GR, wPremiereGeoMarque);

				// Met � jour les premiers �l�ments des pages compte tenu de l'insertion qui vient d'�tre faite
        MajPagesGR (MAJ_INSERTION, wReferencePageDest, wPosElementDest, wNbGeoMarques);

				// Idem pour les marques
        MajGeoMarquesGR (MAJ_INSERTION, wPosElementDest, wNbGeoMarques);
        if (dwIdPageSource == dwIdPageDest)
          MajMarquesGR (MAJ_SUPPRESSION, wReferencePageSource, wPremiereGeoMarque, wNbGeoMarques);

        // Marquage des Elements Dupliqu�s
        pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePageDest);
        wPremiereGeoMarque = pMarque->PremiereMarque + pMarque->NbMarques;
        pMarque->NbMarques = pMarque->NbMarques + wNbGeoMarques;
        insere_enr (szVERIFSource, __LINE__, wNbGeoMarques, B_GEO_MARQUES_GR, wPremiereGeoMarque);
        for (wIndex = 0; wIndex < wNbGeoMarques; wIndex++)
          {
          pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPremiereGeoMarque + wIndex);
          pGeoMarque->Element = wPosElementDest + wIndex;
          }
        MajMarquesGR (MAJ_INSERTION, wReferencePageDest, wPremiereGeoMarque, wNbGeoMarques);

				// Mise � jour et affichage si demand� des rectangles de s�lection
        CalculerRectMarquesGR (pBdGr, wReferencePageDest, &Rectangle);
        MajRectMarquesGR (wReferencePageDest, Rectangle);
        if (ModeTravail & MODE_DESSIN_DEFORMATION)
          {
          DessinerPoigneesGR (pBdGr, Rectangle);
          }

        // duplication des infos d'animation
        if (!DupliquerAnimationGR (hbdgr, bAvecAnimation))
          {
          wRes = 427;
          }
        } // if (dwIdPageDest > 0)
      } // if (pMarque->NbMarques > 0)
    } // if (ExistePageGR (dwIdPageSource, &wReferencePageSource))
  return wRes;
  } // bdgr_dupliquer_marques

// -------------------------------------------------------------------------
void bdgr_translater_marques (HBDGR hbdgr, DWORD ModeTravail, LONG Dx, LONG Dy)
  {
  PBDGR	pBdGr = (PBDGR)hbdgr;
  DWORD	wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
	  RECT_BDGR        RectangleMarques;
    LireRectMarquesGR (wReferencePage, &RectangleMarques);
    if (ModeTravail & MODE_GOMME_DEFORMATION)
      {
      GommerPoigneesGR (pBdGr, RectangleMarques);
      }

		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
    for (DWORD wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
      PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
      DWORD wPositionElement = pGeoMarque->Element;
      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        GommerSelectionGR (pBdGr, wPositionElement);
        }
      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      TranslaterElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, Dx, Dy, pBdGr->hGsys, pBdGr->helementTravail);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      if (ModeTravail & MODE_DESSIN_SELECTION)
        {
        DessinerSelectionGR (pBdGr, wPositionElement);
        }

      if (ModeTravail & (MODE_DESSIN_ELEMENT | MODE_DESSIN_SELECTION | MODE_GOMME_SELECTION))
        {
				// Ajustement �ventuel d'une fen�tre d'un �l�ment graphique
				ChangePosFenetreElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, pBdGr);
        }
      }
    TranslaterRectGR (pBdGr, Dx, Dy, &RectangleMarques);
    MajRectMarquesGR (wReferencePage, RectangleMarques);
    if (ModeTravail & MODE_DESSIN_DEFORMATION)
      {
      DessinerPoigneesGR (pBdGr, RectangleMarques);
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_change_remplissage_marques (HBDGR hbdgr, DWORD ModeTravail)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD              wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD              wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD              wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD              wPositionGeoMarque;
		RECT_BDGR        RectangleMarques;

    LireRectMarquesGR (wReferencePage, &RectangleMarques);
    if (ModeTravail & MODE_GOMME_DEFORMATION)
      {
      GommerPoigneesGR (pBdGr, RectangleMarques);
      }

    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
			PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
			DWORD              wPositionElement = pGeoMarque->Element;

      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        GommerSelectionGR (pBdGr, wPositionElement);
        }
      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      ChangeRemplissageElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, pBdGr->hGsys, pBdGr->helementTravail);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      if (ModeTravail & MODE_DESSIN_SELECTION)
        {
        DessinerSelectionGR (pBdGr, wPositionElement);
        }
      }
    SubstituerRectGR (pBdGr, &RectangleMarques);
    MajRectMarquesGR (wReferencePage, RectangleMarques);
    if (ModeTravail & MODE_DESSIN_DEFORMATION)
      {
      DessinerPoigneesGR (pBdGr, RectangleMarques);
      }
    }
  }

// -------------------------------------------------------------------------
// Aligne les �l�ments marqu�s avec dessin
void bdgr_align_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD wTypeAlign,
                         LONG iGauche, LONG iBas, LONG iDroit, LONG iHaut)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR		pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);

    if (pMarque->NbMarques > 1)
      {
 			PGEO_MARQUES_GR	pGeoMarque;
			DWORD              wPositionGeoMarque;
			DWORD              wPositionElement;
			RECT_BDGR        RectangleMarques;
			RECT_BDGR        RectangleMarquesInit;
			LONG               iX1, iY1, iX2, iY2, Dx, Dy;
			DWORD              wPremiereGeoMarque = pMarque->PremiereMarque;
			DWORD              wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;

      LireRectMarquesGR (wReferencePage, &RectangleMarques);
      if (ModeTravail & MODE_GOMME_DEFORMATION)
        {
        GommerPoigneesGR (pBdGr, RectangleMarques);
        }
      RectangleMarquesInit.x1  = 0;
      RectangleMarquesInit.y1  = 0;
      RectangleMarquesInit.x2  = 0;
      RectangleMarquesInit.y2  = 0;
      MajRectMarquesGR (wReferencePage, RectangleMarquesInit);
      LireRectMarquesGR (wReferencePage, &RectangleMarques);
      for (wPositionGeoMarque = wPremiereGeoMarque;
           wPositionGeoMarque < wDerniereGeoMarque;
           wPositionGeoMarque++)
        {
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
        wPositionElement = pGeoMarque->Element;
        if (ModeTravail & MODE_GOMME_SELECTION)
          {
          GommerSelectionGR (pBdGr, wPositionElement);
          }
        if (ModeTravail & MODE_GOMME_ELEMENT)
          {
          GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
          }
        //
        EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &iX1, &iY1, &iX2, &iY2, pBdGr->hGsys, pBdGr->helementTravail);
        switch (wTypeAlign)
          {
          case c_ALIGN_VC :
            Dx = ((iGauche + iDroit - iX1 - iX2) / 2);
            Dy = 0;
            break;
          case c_ALIGN_HC :
            Dx = 0;
            Dy = ((iBas + iHaut - iY1 - iY2) / 2);
            break;
          case c_ALIGN_D :
            Dx = iDroit - iX2;
            Dy = 0;
            break;
          case c_ALIGN_B :
            Dx = 0;
            Dy = iHaut - iY2;
            break;
          case c_ALIGN_G :
            Dx = iGauche - iX1;
            Dy = 0;
            break;
          case c_ALIGN_H :
            Dx = 0;
            Dy = iBas - iY1;
            break;
          default :
            break;
          }
        TranslaterElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, Dx, Dy, pBdGr->hGsys, pBdGr->helementTravail);
        //
        if (ModeTravail & MODE_DESSIN_ELEMENT)
          {
          DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
          }
        if (ModeTravail & MODE_DESSIN_SELECTION)
          {
          DessinerSelectionGR (pBdGr, wPositionElement);
          }
				if (ModeTravail & (MODE_DESSIN_ELEMENT | MODE_DESSIN_SELECTION))
					{
					// Ajustement �ventuel d'une fen�tre d'un �l�ment graphique
					ChangePosFenetreElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, pBdGr);
					}
        EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement,
                        &RectangleMarquesInit.x1, &RectangleMarquesInit.y1,
                        &RectangleMarquesInit.x2, &RectangleMarquesInit.y2,
                        pBdGr->hGsys, pBdGr->helementTravail);
        AjouterRectGR (RectangleMarquesInit, &RectangleMarques);
        }
      MajRectMarquesGR (wReferencePage, RectangleMarques);
      if (ModeTravail & MODE_DESSIN_DEFORMATION)
        {
        DessinerPoigneesGR (pBdGr, RectangleMarques);
        }
      } // plus d'un elt marqu�
    }
  } // bdgr_align_marques

// -------------------------------------------------------------------------
// Tri Dicho pour gestion Espacements
// -------------------------------------------------------------------------
static int ComparaisonCoordonnees (const void *piVal, DWORD wNumero)
  {
  PESPACEMENT_GR pEspacement = (PESPACEMENT_GR)pointe_enr (szVERIFSource, __LINE__, B_ESPACEMENT_GR, wNumero);

  return (int)((*(LONG *)piVal) - pEspacement->iVal);
  }

// -------------------------------------------------------------------------
// r�partis l'espacement entre plusieurs �l�ments graphiques
DWORD bdgr_espace_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD wTypeEspace,
                          DWORD wTailleMax, LONG iEspacement)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wErreur = 0;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);

    if (pMarque->NbMarques > 1)
      {
			DWORD              wPositionGeoMarque;
			PGEO_MARQUES_GR	pGeoMarque;
			DWORD              wPositionElement;
			RECT_BDGR        RectangleMarques;
			RECT_BDGR        RectangleMarquesInit;
			LONG               iCourX, iCourY, iX1, iY1, iX2, iY2, Dx, Dy, iSomme;
			DWORD              wPosition;
			DWORD              wNbrEnr;
			PESPACEMENT_GR		pEspacement;
			DWORD              wPremiereGeoMarque = pMarque->PremiereMarque;
			DWORD              wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;


      if (!existe_repere (B_ESPACEMENT_GR))
        {
        cree_bloc (B_ESPACEMENT_GR, sizeof (ESPACEMENT_GR), 0);
        }

      // initialiser iSomme
      iSomme = 0;
      // premiere boucle sur les elts selectionnes
      for (wPositionGeoMarque = wPremiereGeoMarque;
           (wPositionGeoMarque < wDerniereGeoMarque) && ((DWORD)iSomme < wTailleMax);
           wPositionGeoMarque++)
        {
        pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
        wPositionElement = pGeoMarque->Element;
        //
        EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &iX1, &iY1, &iX2, &iY2, pBdGr->hGsys, pBdGr->helementTravail);
        switch (wTypeEspace)
          {
          case c_ESPACE_V :
						{
            trouve_position_dichotomie (&iY1, 1, nb_enregistrements (szVERIFSource, __LINE__, B_ESPACEMENT_GR),
                                        ComparaisonCoordonnees, &wPosition);
            insere_enr (szVERIFSource, __LINE__, 1, B_ESPACEMENT_GR, wPosition);
            pEspacement = (PESPACEMENT_GR)pointe_enr (szVERIFSource, __LINE__, B_ESPACEMENT_GR, wPosition);
            pEspacement->iVal = iY1;
            pEspacement->wPosGeoMarques = wPositionGeoMarque;
            iSomme = iSomme + iY2 - iY1 + iEspacement;
						}
            break;

          case c_ESPACE_H :
            trouve_position_dichotomie (&iX1, 1, nb_enregistrements (szVERIFSource, __LINE__, B_ESPACEMENT_GR),
							ComparaisonCoordonnees, &wPosition);
            insere_enr (szVERIFSource, __LINE__, 1, B_ESPACEMENT_GR, wPosition);
            pEspacement = (PESPACEMENT_GR)pointe_enr (szVERIFSource, __LINE__, B_ESPACEMENT_GR, wPosition);
            pEspacement->iVal = iX1;
            pEspacement->wPosGeoMarques = wPositionGeoMarque;
            iSomme = iSomme + iX2 - iX1 + iEspacement;
            break;
          default :
            break;
          }
        //
        } // fin premier boucle sur les elts selectionnes
      // Ajout de la premiere coordonnee et inits iCour
      pEspacement = (PESPACEMENT_GR)pointe_enr (szVERIFSource, __LINE__, B_ESPACEMENT_GR, 1);
      pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, pEspacement->wPosGeoMarques);
      EncombrementElementGR (B_ELEMENTS_PAGES_GR, pGeoMarque->Element, &iX1, &iY1, &iX2, &iY2, pBdGr->hGsys, pBdGr->helementTravail);
      switch (wTypeEspace)
        {
        case c_ESPACE_V :
          iSomme = iSomme + iY1;
          break;
        case c_ESPACE_H :
          iSomme = iSomme + iX1;
          break;
        default :
          break;
        }
      iSomme = iSomme - iEspacement; // enleve l'espacement du dernier
      iCourX = iX1;
      iCourY = iY1;
      //
      wNbrEnr = nb_enregistrements (szVERIFSource, __LINE__, B_ESPACEMENT_GR);
      if ((DWORD)iSomme < wTailleMax)
        { // espacement possible
        LireRectMarquesGR (wReferencePage, &RectangleMarques);
        if (ModeTravail & MODE_GOMME_DEFORMATION)
          {
          GommerPoigneesGR (pBdGr, RectangleMarques);
          }
        RectangleMarquesInit.x1  = 0;
        RectangleMarquesInit.y1  = 0;
        RectangleMarquesInit.x2  = 0;
        RectangleMarquesInit.y2  = 0;
        MajRectMarquesGR (wReferencePage, RectangleMarquesInit);
        LireRectMarquesGR (wReferencePage, &RectangleMarques);
        //
        for (wPosition = 1; wPosition <= wNbrEnr; wPosition++)
          { // deuxieme boucle sur les elts selectionnes ds le bon ordre
          pEspacement = (PESPACEMENT_GR)pointe_enr (szVERIFSource, __LINE__, B_ESPACEMENT_GR, wPosition);
          pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, pEspacement->wPosGeoMarques);
          wPositionElement = pGeoMarque->Element;
          if (ModeTravail & MODE_GOMME_SELECTION)
            {
            GommerSelectionGR (pBdGr, wPositionElement);
            }
          if (ModeTravail & MODE_GOMME_ELEMENT)
            {
            GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
            }
					if (ModeTravail & (MODE_DESSIN_ELEMENT | MODE_DESSIN_SELECTION))
						{
						// Ajustement �ventuel d'une fen�tre d'un �l�ment graphique
						ChangePosFenetreElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, pBdGr);
						}

          EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &iX1, &iY1, &iX2, &iY2, pBdGr->hGsys, pBdGr->helementTravail);
          switch (wTypeEspace)
            {
            case c_ESPACE_V :
              Dx = 0;
              Dy = iCourY - iY1;
              iCourY = iCourY + iY2 - iY1 + iEspacement;
            break;
            case c_ESPACE_H :
              Dx = iCourX - iX1;
              Dy = 0;
              iCourX = iCourX + iX2 - iX1 + iEspacement;
              break;
            default :
              break;
            }
          TranslaterElementGR (B_ELEMENTS_PAGES_GR, wPositionElement,
                               Dx, Dy, pBdGr->hGsys, pBdGr->helementTravail);
          //
          if (ModeTravail & MODE_DESSIN_ELEMENT)
            {
            DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
            }
          if (ModeTravail & MODE_DESSIN_SELECTION)
            {
            DessinerSelectionGR (pBdGr, wPositionElement);
            }

          EncombrementElementGR (B_ELEMENTS_PAGES_GR, wPositionElement,
                          &RectangleMarquesInit.x1, &RectangleMarquesInit.y1,
                          &RectangleMarquesInit.x2, &RectangleMarquesInit.y2,
                          pBdGr->hGsys, pBdGr->helementTravail);
          AjouterRectGR (RectangleMarquesInit, &RectangleMarques);
          } // fin 2� boucle (sur bloc trie)
        MajRectMarquesGR (wReferencePage, RectangleMarques);
        if (ModeTravail & MODE_DESSIN_DEFORMATION)
          {
          DessinerPoigneesGR (pBdGr, RectangleMarques);
          }
        }
      else
        { // espacement impossible
        wErreur = ERR_ESPACEMENT_GR;
        }
      //
      enleve_bloc (B_ESPACEMENT_GR);
      } // plus d'un element marqu�
    } // existe page GR

  return (wErreur);
  }

// -------------------------------------------------------------------------
// Transforme les �l�ments graphiques marqu�s en leurs sym�triques (avec dessin)
void bdgr_symetrie_marques (HBDGR hbdgr, DWORD ModeTravail, DWORD Axes, LONG xAxe, LONG yAxe)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR					pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD								wPremiereGeoMarque;
		DWORD								wDerniereGeoMarque;
		DWORD								wPositionGeoMarque;
		PGEO_MARQUES_GR			pGeoMarque;
		DWORD								wPositionElement;
		RECT_BDGR						RectangleMarques;

    LireRectMarquesGR (wReferencePage, &RectangleMarques);
    if (ModeTravail & MODE_GOMME_DEFORMATION)
      {
      GommerPoigneesGR (pBdGr, RectangleMarques);
      }

    wPremiereGeoMarque = pMarque->PremiereMarque;
    wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
      pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
      wPositionElement = pGeoMarque->Element;
      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        GommerSelectionGR (pBdGr, wPositionElement);
        }
      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      RendreUniqueElementGR (B_ELEMENTS_PAGES_GR, wPositionElement);
      MiroiterElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, Axes, xAxe, yAxe, pBdGr->hGsys, pBdGr->helementTravail);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      if (ModeTravail & MODE_DESSIN_SELECTION)
        {
        DessinerSelectionGR (pBdGr, wPositionElement);
        }
			if (ModeTravail & (MODE_DESSIN_ELEMENT | MODE_DESSIN_SELECTION))
				{
				// Ajustement �ventuel d'une fen�tre d'un �l�ment graphique
				ChangePosFenetreElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, pBdGr);
				}
      }
    MiroiterRectGR (pBdGr,  Axes, xAxe, yAxe, &RectangleMarques);
    MajRectMarquesGR (wReferencePage, RectangleMarques);
    if (ModeTravail & MODE_DESSIN_DEFORMATION)
      {
      DessinerPoigneesGR (pBdGr, RectangleMarques);
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_tourner_marques (HBDGR hbdgr, DWORD ModeTravail, LONG xCentre, LONG yCentre, DWORD NbQuartTour)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD              wReferencePage;
  PMARQUES_GR	pMarque;
  DWORD              wPremiereGeoMarque;
  DWORD              wDerniereGeoMarque;
  DWORD              wPositionGeoMarque;
  PGEO_MARQUES_GR		pGeoMarque;
  DWORD              wPositionElement;
  RECT_BDGR        RectangleMarques;


  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
    LireRectMarquesGR (wReferencePage, &RectangleMarques);
    if (ModeTravail & MODE_GOMME_DEFORMATION)
      {
      GommerPoigneesGR (pBdGr, RectangleMarques);
      }

    pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
    wPremiereGeoMarque = pMarque->PremiereMarque;
    wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
      pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
      wPositionElement = pGeoMarque->Element;
      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        GommerSelectionGR (pBdGr, wPositionElement);
        }
      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      RendreUniqueElementGR (B_ELEMENTS_PAGES_GR, wPositionElement);
      TournerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, xCentre, yCentre, NbQuartTour, pBdGr->hGsys, pBdGr->helementTravail);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      if (ModeTravail & MODE_DESSIN_SELECTION)
        {
        DessinerSelectionGR (pBdGr, wPositionElement);
        }
			if (ModeTravail & (MODE_DESSIN_ELEMENT | MODE_DESSIN_SELECTION))
				{
				// Ajustement �ventuel d'une fen�tre d'un �l�ment graphique
				ChangePosFenetreElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, pBdGr);
				}
      }
    TournerRectGR (pBdGr, xCentre, yCentre, NbQuartTour, &RectangleMarques);
    MajRectMarquesGR (wReferencePage, RectangleMarques);
    if (ModeTravail & MODE_DESSIN_DEFORMATION)
      {
      DessinerPoigneesGR (pBdGr, RectangleMarques);
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_debut_deformation_marques (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD                         wReferencePage;
  PGEO_POINTS_SAUVEGARDES_GR		pGeoSav;
  DWORD                         wPositionSauvegarde;
  DWORD                         wNbSauvegarde;
  PMARQUES_GR										pMarque;
  DWORD                         wPremiereGeoMarque;
  DWORD                         wDerniereGeoMarque;
  DWORD                         wPositionGeoMarque;
  PGEO_MARQUES_GR								pGeoMarque;
  DWORD                         wPositionElement;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
    LireRectMarquesGR (wReferencePage, &RectMarqueMem);
    pGeoSav = (PGEO_POINTS_SAUVEGARDES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR, wReferencePage);
    wPositionSauvegarde = pGeoSav->PremierPointSauvegarde;
    wNbSauvegarde       = pGeoSav->NbPointsSauvegarde;
    if (wNbSauvegarde > 0)
      {
      pGeoSav->NbPointsSauvegarde = 0;
      enleve_enr (szVERIFSource, __LINE__, wNbSauvegarde, B_POINTS_SAUVEGARDES_GR, wPositionSauvegarde);
      MajSavPointsGR (MAJ_SUPPRESSION, wReferencePage, wPositionSauvegarde, wNbSauvegarde);
      }
    //
    pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
    wPremiereGeoMarque = pMarque->PremiereMarque;
    wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
      pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
      wPositionElement = pGeoMarque->Element;
      RendreUniqueEtSauveElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &wPositionSauvegarde);
      }
    //
    pGeoSav = (PGEO_POINTS_SAUVEGARDES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR, wReferencePage);
    wNbSauvegarde = wPositionSauvegarde - pGeoSav->PremierPointSauvegarde;
    pGeoSav->NbPointsSauvegarde = wNbSauvegarde;
    MajSavPointsGR (MAJ_INSERTION, wReferencePage, pGeoSav->PremierPointSauvegarde, wNbSauvegarde);
    }
  }

// -------------------------------------------------------------------------
void bdgr_deformer_marques (HBDGR hbdgr, DWORD ModeTravail, LONG xRef, LONG yRef,
														LONG cxNew, LONG cxOld, LONG cyNew, LONG cyOld)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD                         wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PGEO_POINTS_SAUVEGARDES_GR	pGeoSav;
		DWORD                       wPositionSauvegarde;
		PMARQUES_GR					pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD               wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD               wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD               wPositionGeoMarque;
		PGEO_MARQUES_GR			pGeoMarque;
		DWORD               wPositionElement;
		RECT_BDGR        RectangleMarques;

    LireRectMarquesGR (wReferencePage, &RectangleMarques);
    if (ModeTravail & MODE_GOMME_DEFORMATION)
      {
      GommerPoigneesGR (pBdGr, RectangleMarques);
      }
    pGeoSav = (PGEO_POINTS_SAUVEGARDES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_POINTS_SAUVEGARDES_GR, wReferencePage);
    wPositionSauvegarde = pGeoSav->PremierPointSauvegarde;

    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
      pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);
      wPositionElement = pGeoMarque->Element;
      if (ModeTravail & MODE_GOMME_SELECTION)
        {
        GommerSelectionGR (pBdGr, wPositionElement);
        }
      if (ModeTravail & MODE_GOMME_ELEMENT)
        {
        GommerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }

      DeformerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &wPositionSauvegarde, xRef, yRef, cxNew, cxOld, cyNew, cyOld, pBdGr->hGsys, pBdGr->helementTravail);

      if (ModeTravail & MODE_DESSIN_ELEMENT)
        {
        DessinerElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
        }
      if (ModeTravail & MODE_DESSIN_SELECTION)
        {
        DessinerSelectionGR (pBdGr, wPositionElement);
        }
      if (ModeTravail & (MODE_DESSIN_ELEMENT | MODE_DESSIN_SELECTION))
        {
				// Ajustement �ventuel d'une fen�tre d'un �l�ment graphique
				ChangePosFenetreElementGR (B_ELEMENTS_PAGES_GR, wPositionElement, pBdGr);
        }
      }
    RectangleMarques = RectMarqueMem;
    DeformerRectGR (pBdGr, xRef, yRef, cxNew, cxOld, cyNew, cyOld, &RectangleMarques);
    MajRectMarquesGR (wReferencePage, RectangleMarques);
    if (ModeTravail & MODE_DESSIN_DEFORMATION)
      {
      DessinerPoigneesGR (pBdGr, RectangleMarques);
      }
    }
  }

// -------------------------------------------------------------------------
// Voire si le point sp�cifi� est sur une marque ($$??? une poign�e?)
DWORD bdgr_pointer_marques (HBDGR hbdgr, LONG x, LONG y, LONG *CoinDeformation)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wMarque = 0;
  DWORD wReferencePage;

  *CoinDeformation = c_PAS_COIN;
  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
    if (pMarque->NbMarques > 0)
      {
			DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
			DWORD       wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
			DWORD       wPositionGeoMarque;
			BOOL        bTrouve = FALSE;
			RECT_BDGR  Rectangle;
			LONG         x1;
			LONG         y1;
			LONG         x2;
			LONG         y2;
			LONG        offsetxplus;
			LONG        offsetyplus;
			LONG        offsetxmoins;
			LONG        offsetymoins;

      for (wPositionGeoMarque = wPremiereGeoMarque; ((wPositionGeoMarque < wDerniereGeoMarque) && (!bTrouve)); wPositionGeoMarque++)
        {
				PGEO_MARQUES_GR pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);

        EncombrementElementGR (B_ELEMENTS_PAGES_GR, pGeoMarque->Element, &x1, &y1, &x2, &y2, pBdGr->hGsys, pBdGr->helementTravail);
        bTrouve = bdgr_point_dans_rectangle (x, y, x1, y1, x2, y2);
        }
      if (bTrouve)
        {
        wMarque = wPositionGeoMarque - pMarque->PremiereMarque;
        }
      else
        {
        // Test rectangle interieur aux marques
        LireRectMarquesGR (wReferencePage, &Rectangle);
        bTrouve = bdgr_point_dans_rectangle (x, y, Rectangle.x1, Rectangle.y1, Rectangle.x2, Rectangle.y2);
        if (!bTrouve)
          {
          // Test rectangle exterieur aux marques
          offsetxplus  = g_convert_dx_dev_to_page (pBdGr->hGsys, (LONG) (TAILLE_PEL_DEFORMATION + 1));
          offsetyplus  = g_convert_dy_dev_to_page (pBdGr->hGsys, (LONG) (TAILLE_PEL_DEFORMATION + 1));
          offsetxmoins  = - g_convert_dx_dev_to_page (pBdGr->hGsys, (LONG) TAILLE_PEL_DEFORMATION);
          offsetymoins  = - g_convert_dy_dev_to_page (pBdGr->hGsys, (LONG) TAILLE_PEL_DEFORMATION);
          x1 = g_add_x_page (pBdGr->hGsys, (LONG) Rectangle.x1, offsetxmoins);
          y1 = g_add_y_page (pBdGr->hGsys, (LONG) Rectangle.y1, offsetymoins);
          x2 = g_add_x_page (pBdGr->hGsys, (LONG) Rectangle.x2, offsetxplus);
          y2 = g_add_y_page (pBdGr->hGsys, (LONG) Rectangle.y2, offsetyplus);
          bTrouve = bdgr_point_dans_rectangle (x, y, x1, y1, x2, y2);
          if (bTrouve)
            {
            // Tests si sur l'un des 4 rectangles de marque
            if (bdgr_point_dans_rectangle (x, y, x1, y1, Rectangle.x1, Rectangle.y1))
              (*CoinDeformation) = c_COIN_BG;
            else
              if (bdgr_point_dans_rectangle (x, y, Rectangle.x2, y1, x2, Rectangle.y1))
                (*CoinDeformation) = c_COIN_BD;
              else
                if (bdgr_point_dans_rectangle (x, y, Rectangle.x2, Rectangle.y2, x2, y2))
                  (*CoinDeformation) = c_COIN_HD;
                else
                  if (bdgr_point_dans_rectangle (x, y, x1, Rectangle.y2, Rectangle.x1, y2))
                    (*CoinDeformation) = c_COIN_HG;
            }
          }
        }
      }
    }
  return wMarque;
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Dessin
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// ---------------------------------------------------------------------------
// Dessin des attributs de s�lection de tous les �l�ments marqu�s dans la page
void bdgr_dessiner_selection_page (HBDGR hbdgr)
  {
  PBDGR	pBdGr = (PBDGR)hbdgr;
  DWORD	wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD       wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD       wPositionGeoMarque;

    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
			PGEO_MARQUES_GR pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);

      DessinerSelectionGR (pBdGr, pGeoMarque->Element);
      }
    }
  }

// -------------------------------------------------------------------------
// Invalidate les attributs de s�lection de tous les �l�ments marqu�s dans la page
void bdgr_gommer_selection_page (HBDGR hbdgr)
  {
  PBDGR	pBdGr = (PBDGR)hbdgr;
  DWORD	wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD       wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD       wPositionGeoMarque;

    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
			PGEO_MARQUES_GR pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);

      GommerSelectionGR (pBdGr, pGeoMarque->Element);
      }
    }
  }


// -------------------------------------------------------------------------
// Dessiner les poign�es de d�formation des �l�ments s�lectionn�s
void bdgr_dessiner_deformation_page (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		RECT_BDGR  Rectangle;

    LireRectMarquesGR (wReferencePage, &Rectangle);
    DessinerPoigneesGR (pBdGr, Rectangle);
    }
  }

// -------------------------------------------------------------------------
// Invalidate des poign�es de d�formation des �l�ments s�lectionn�s
void bdgr_gommer_deformation_page (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		RECT_BDGR  Rectangle;

    LireRectMarquesGR (wReferencePage, &Rectangle);
    GommerPoigneesGR (pBdGr, Rectangle);
    }
  }

// -------------------------------------------------------------------------
void bdgr_dessiner_elements_marques (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD       wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD       wPositionGeoMarque;

    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
			PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);

      DessinerElementGR (B_ELEMENTS_PAGES_GR, pGeoMarque->Element, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  }

// -------------------------------------------------------------------------
void bdgr_gommer_elements_marques (HBDGR hbdgr)
  {
  PBDGR pBdGr = (PBDGR)hbdgr;
  DWORD wReferencePage;

  if (ExistePageGR (pBdGr->dwIdPage, &wReferencePage))
    {
		PMARQUES_GR	pMarque = (PMARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_MARQUES_GR, wReferencePage);
		DWORD       wPremiereGeoMarque = pMarque->PremiereMarque;
		DWORD       wDerniereGeoMarque = pMarque->NbMarques + wPremiereGeoMarque;
		DWORD       wPositionGeoMarque;
		
    for (wPositionGeoMarque = wPremiereGeoMarque; wPositionGeoMarque < wDerniereGeoMarque; wPositionGeoMarque++)
      {
			PGEO_MARQUES_GR	pGeoMarque = (PGEO_MARQUES_GR)pointe_enr (szVERIFSource, __LINE__, B_GEO_MARQUES_GR, wPositionGeoMarque);

      GommerElementGR (B_ELEMENTS_PAGES_GR, pGeoMarque->Element, &FORCAGE_NULL, pBdGr, pBdGr->helementTravail);
      }
    }
  }

//-----------------------------------------------------------------------------
// D�claration des objets, param�tres et attributs d'objets graphiques
// ind�pendants de leur impl�mentation (communs � g_sys bdgr et DevMan)
//-----------------------------------------------------------------------------
#ifndef G_OBJETS_H

// Actions Graphiques
typedef enum
	{
	G_ACTION_INDETERMINEE				= -1,
	G_ACTION_LIGNE							= 0,
	G_ACTION_RECTANGLE					= 1,
	G_ACTION_RECTANGLE_PLEIN		= 2,
	G_ACTION_CERCLE							= 3,
	G_ACTION_CERCLE_PLEIN				= 4,
	G_ACTION_QUART_ARC          = 5,
	G_ACTION_QUART_CERCLE_PLEIN = 6,
	G_ACTION_H_DEMI_CERCLE      = 7,
	G_ACTION_H_DEMI_CERCLE_PLEIN= 8,
	G_ACTION_V_DEMI_CERCLE      = 9,
	G_ACTION_V_DEMI_CERCLE_PLEIN= 10,
	G_ACTION_TRI_RECT						= 11,
	G_ACTION_TRI_RECT_PLEIN			= 12,
	G_ACTION_H_TRI_ISO					= 13,
	G_ACTION_H_TRI_ISO_PLEIN		= 14,
	G_ACTION_V_TRI_ISO					= 15,
	G_ACTION_V_TRI_ISO_PLEIN		= 16,
	G_ACTION_H_VANNE						= 17,
	G_ACTION_H_VANNE_PLEIN			= 18,
	G_ACTION_V_VANNE						= 19,
	G_ACTION_V_VANNE_PLEIN			= 20,
	G_ACTION_QUART_CERCLE				= 21,
	G_ACTION_QUART_ARC_PLEIN		= 22,
	G_ACTION_H_TEXTE						= 23,
	G_ACTION_V_TEXTE						= 24,
	G_ACTION_POLYGONE						= 25,
	G_ACTION_POLYGONE_PLEIN			= 26,
	G_ACTION_LISTE							= 27,
	G_ACTION_LISTE_PLEIN				= 28,
	G_ACTION_COURBE_T						= 29,
	G_ACTION_COURBE_C						= 30,
	G_ACTION_COURBE_A						= 31,
	G_ACTION_BARGRAPHE					= 32,
	G_ACTION_EDIT_TEXTE					= 33,
	G_ACTION_EDIT_NUM						= 34,
	G_ACTION_STATIC_TEXTE				= 35,
	G_ACTION_STATIC_NUM					= 36,
	G_ACTION_ENTREE_LOG					= 37,
	G_ACTION_NOP								= 38,  // L'action NOP est indispensabe � G_SYS
	G_ACTION_CONT_STATIC  			= 39, // Nouveaux contr�les
	G_ACTION_CONT_EDIT    			= 40,
	G_ACTION_CONT_GROUP   			= 41,
	G_ACTION_CONT_BOUTON  			= 42,
	G_ACTION_CONT_CHECK   			= 43,
	G_ACTION_CONT_RADIO   			= 44,
	G_ACTION_CONT_COMBO   			= 45,
	G_ACTION_CONT_LIST    			= 46,
	G_ACTION_CONT_SCROLL_H			= 47,
	G_ACTION_CONT_SCROLL_V			= 48,
	G_ACTION_CONT_BOUTON_VAL		= 49
	} G_ACTION, *PG_ACTION;

#define G_NB_ACTIONS						50  // Nombre d'actions

// couleurs (g_sys et devman)
typedef enum
	{
	G_COULEUR_TRANSPARENTE	= -2,
	G_COULEUR_INDETERMINEE	= -1,
	G_BLACK = 0,
	G_WHITE = 1,
	G_RED = 2,
	G_DARKCYAN = 3,
	G_YELLOW = 4,
	G_DARKBLUE = 5,
	G_DARKGREEN = 6,
	G_PINK = 7,
	G_CYAN = 8,
	G_DARKRED = 9,
	G_DARKGRAY = 10,
	G_PALEGRAY = 11,
	G_DARKPINK = 12,
	G_GREEN = 13,
	G_BLUE = 14,
	G_BROWN = 15,
	G_WHITERED = 16,
	G_WHITEORANGE = 17,
	G_WHITEYELLOW = 18,
	G_WHITEYELLOWGREEN = 19,
	G_WHITEGREEN = 20,
	G_WHITECYANGREEN = 21,
	G_WHITECYAN = 22,
	G_WHITEAZUR = 23,
	G_WHITEBLUE = 24,
	G_WHITEVIOLET = 25,
	G_WHITEPINK = 26,
	G_WHITEPURPLE = 27,
	G_WHITEGRAY = 28,
	G_PALERED = 29,
	G_PALEORANGE = 30,
	G_PALEYELLOW = 31,
	G_PALEYELLOWGREEN = 32,
	G_PALEGREEN = 33,
	G_PALECYANGREEN = 34,
	G_PALECYAN = 35,
	G_PALEAZUR = 36,
	G_PALEBLUE = 37,
	G_PALEVIOLET = 38,
	G_PALEPINK = 39,
	G_PALEPURPLE = 40,
	G_ORANGE = 41,
	G_YELLOWGREEN = 42,
	G_CYANGREEN = 43,
	G_AZUR = 44,
	G_VIOLET = 45,
	G_PURPLE = 46,
	G_GRAY = 47,
	G_SHADOWRED = 48,
	G_SHADOWORANGE = 49,
	G_SHADOWYELLOW = 50,
	G_SHADOWYELLOWGREEN = 51,
	G_SHADOWGREEN = 52,
	G_SHADOWCYANGREEN = 53,
	G_SHADOWCYAN = 54,
	G_SHADOWAZUR = 55,
	G_SHADOWBLUE = 56,
	G_SHADOWVIOLET = 57,
	G_SHADOWPINK = 58,
	G_SHADOWPURPLE = 59,
	G_DARKORANGE = 60,
	G_DARKYELLOWGREEN = 61,
	G_DARKCYANGREEN = 62,
	G_DARKAZUR = 63,
	G_DARKVIOLET = 64,
	G_DARKPURPLE = 65,
	G_BLACKRED = 66,
	G_BLACKORANGE = 67,
	G_BLACKYELLOW = 68,
	G_BLACKYELLOWGREEN = 69,
	G_BLACKGREEN = 70,
	G_BLACKCYANGREEN = 71,
	G_BLACKCYAN = 72,
	G_BLACKAZUR = 73,
	G_BLACKBLUE = 74,
	G_BLACKVIOLET = 75,
	G_BLACKPINK = 76,
	G_BLACKPURPLE = 77
	} G_COULEUR, *PG_COULEUR;

#define G_NB_COULEURS				   78  // Nombre de couleurs: attention limit� par (G_OFFSET_CLIGNOTANT-1)
#define G_NB_TEINTES				   13  // Nombre de familles de teintes parmi les couleurs
#define G_NB_LUMINOSITES		   6  // Nombre de luminosit�s pour chaque famille de teintes
#define G_OFFSET_CLIGNOTANT    (MAXWORD+1)  // Sale en attendant mieux

#define G_N_COULEUR_MIN			G_COULEUR_TRANSPARENTE
#define G_N_COULEUR_MAX			G_BLACKPURPLE

// styles de lignes (g_sys et devman)
typedef enum
	{
	G_STYLE_LIGNE_INDETERMINE			= -1,
	G_STYLE_LIGNE_CONTINUE				= 0,
	G_STYLE_LIGNE_TIRETS					= 1,
	G_STYLE_LIGNE_POINTS					= 2,
	G_STYLE_LIGNE_TIRETS_POINTS		= 3,
	G_STYLE_LIGNE_TIRETS_2_POINTS	= 4
	} G_STYLE_LIGNE, *PG_STYLE_LIGNE;
#define G_NB_STYLES_LIGNE					5 // Nombre de styles de lignes
#define G_N_STYLE_LIGNE_MIN			G_STYLE_LIGNE_INDETERMINE
#define G_N_STYLE_LIGNE_MAX			G_STYLE_LIGNE_TIRETS_2_POINTS

// styles de remplissages (g_sys et devman)
typedef enum
	{
	G_STYLE_REMPLISSAGE_INDETERMINE		= -1,
	G_STYLE_REMPLISSAGE_PLEIN					= 0,
	G_STYLE_REMPLISSAGE_DIAG_AV				= 1,
	G_STYLE_REMPLISSAGE_DIAG_AR				= 2,
	G_STYLE_REMPLISSAGE_HACHURE_DIAG	= 3,
	G_STYLE_REMPLISSAGE_LIGNES_V			= 4,
	G_STYLE_REMPLISSAGE_LIGNES_H			= 5,
	G_STYLE_REMPLISSAGE_HACHURE				= 6
	} G_STYLE_REMPLISSAGE, *PG_STYLE_REMPLISSAGE;
#define G_NB_STYLES_REMPLISSAGE			7 // Nombre de styles de lignes
#define G_N_STYLE_REMPLISSAGE_MIN			G_STYLE_REMPLISSAGE_INDETERMINE
#define G_N_STYLE_REMPLISSAGE_MAX			G_STYLE_REMPLISSAGE_HACHURE

// types d'animations
typedef enum
	{
	ANIMATION_R_1_LOG							= 0,
	ANIMATION_R_2_LOG							= 1,
	ANIMATION_BARGRAPHE						= 2,
	ANIMATION_COURBE_TEMPS				= 3,
	ANIMATION_COURBE_COMMANDE			= 4,
	ANIMATION_COURBE_ARCHIVE			= 5,
	ANIMATION_TEXTE_STATIC_A			= 6,
	ANIMATION_TEXTE_STATIC_N			= 7,
	ANIMATION_DEPLACE							= 8,
	ANIMATION_E_LOG								= 9,
	ANIMATION_E_LOG_BITMAP				= 10,
	ANIMATION_EDIT_N							= 11,
	ANIMATION_EDIT_A							= 12,
	ANIMATION_CONTROL_BOUTON			= 13,
	ANIMATION_CONTROL_RADIO				= 14,
	ANIMATION_CONTROL_STATIC			= 15,
	ANIMATION_CONTROL_LISTE				= 16,
	ANIMATION_CONTROL_COMBOBOX		= 17,
	ANIMATION_CONTROL_CHECK	      = 18,
	ANIMATION_AUCUNE							= 19,
	ANIMATION_CONTROL_EDIT				= 20
	} ANIMATION, *PANIMATION;

#define NB_ANIMATIONS 20

// propri�t�s des actions graphiques
typedef enum
	{
	G_ATTRIBUT_INVALIDE					 = 0,
	G_ATTRIBUT_ACTION            = 0x1,		// Id action valide
	G_ATTRIBUT_COULEUR           = 0x2,		// On peut lui appliquer une couleur
	G_ATTRIBUT_STYLE_LIGNE       = 0x4,		// On peut lui appliquer un style de ligne
	G_ATTRIBUT_STYLE_REMPLISSAGE = 0x8,		// On peut lui appliquer un style de remplissage
	G_ATTRIBUT_1_POINT           = 0x10,	// Coordonn�es d�finies par un bipoint (d�faut = un point)
	G_ATTRIBUT_2_POINTS          = 0x20,	// Coordonn�es d�finies par un bipoint (d�faut = un point)
	G_ATTRIBUT_N_POINTS          = 0x40,	// Coordonn�es d�finies par N points
	G_ATTRIBUT_TEXTE             = 0x80,	// Contient un texte
	G_ATTRIBUT_STYLE_TEXTE       = 0x100,	// On peut lui appliquer un style de texte 
	G_ATTRIBUT_SUPPORTE_ROTATION = 0x400,	// On peut le faire tourner
	G_ATTRIBUT_SUPPORTE_SYMETRIE = 0x800,	// Identique G_ATTRIBUT_SUPPORTE_ROTATION actuellement
	G_ATTRIBUT_CONTROLE		       = 0x1000,	// Il s'agit d'un contr�le
	G_ATTRIBUT_DESSIN			       = 0x2000,	// Il s'agit d'un �l�ment de dessin
	} G_ATTRIBUT, *PG_ATTRIBUT;

// Attributs graphiques
// R�cup�re un ou entre tous les attributs d'une action graphique
G_ATTRIBUT attributsActionGr (G_ACTION nAction);

// Renvoie TRUE si au moins un des attributs appartient � l'action
BOOL bAttributsActionGr (G_ACTION nAction, G_ATTRIBUT nMasqueAttributs);


#endif

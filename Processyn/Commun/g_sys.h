//----------------------------------------------------------------
// G_sys.h
// Gestion d'objets graphiques dans un espace de dessin
// ! n�cessite #include "G_Objets.h"
//----------------------------------------------------------------

// Handle sur un objet GSYS (espace de dessin)
DECLARE_HANDLE (HGSYS);

// Handle sur un objet ELEMENT (�l�ment graphique)
DECLARE_HANDLE (HELEMENT);

// constante point (0,0)
extern const POINT POINT_NULL;

// -------------------------------------------------------------------------
// Ouverture d'un espace de dessin (associ� � une fen�tre)
HGSYS g_open (HWND hwndDessin);

// Fermeture de l'espace de dessin (* phgsys remis � NULL)
void g_close (HGSYS * phgsys);

// -------------------------------------------------------------------------
// Initialisations de G-sys
void g_init (HGSYS hgsys);

// Finalisations de G-sys
void g_fini (HGSYS hgsys);

// ------------------------------------------------------------------------
// Sp�cifie l'origine du DC (utilis� pour l'alignement des brosses sous W95)
void g_set_origine_dc (HGSYS hgsys, POINT ptOrigineClientPix);

// ------------------------------------------------------------------------------------------------
// permute les x et les y si n�cessaire pour avoir (pPoint0->x <= pPoint1->x) et (pt0->y <= pPoint1->y)
void G_OrdonneBipoint (PPOINT pPoint0, PPOINT pPoint1);

// ------------------------------------------------------------------------
// D�finition de l'echelle de coordonn�es
void g_set_page_units (HGSYS hgsys, LONG XLengthPage, LONG YLengthPage, LONG XLengthWorkSpace, LONG YLengthWorkSpace);

// Origine de l'echelle de coordonn�es
void g_set_x_origine   (HGSYS hgsys, LONG x);
void g_set_y_origine   (HGSYS hgsys, LONG y);
void g_set_origine     (HGSYS hgsys, LONG x, LONG y);
void g_get_origin      (HGSYS hgsys, LONG *px, LONG *py);
void g_deplace_origine (HGSYS hgsys, LONG dx, LONG dy, LONG *OldOf7X0, LONG *OldOf7Y0);

// ------------------------------------------------------------------------
// Nom du document
void g_set_document_name (HGSYS hgsys, char *pszName, DWORD wDocType);
DWORD g_get_document_name (HGSYS hgsys, char *pszName);

// ------------------------------------------------------------------------
// Choix du p�riph�rique de sortie
void *g_select_device     (HGSYS hgsys, DWORD Device);
void  g_get_device_limits (HGSYS hgsys, LONG *x0, LONG *y0, LONG *cx, LONG *cy);
void  g_delete_handle     (HGSYS hgsys, void *hdl, DWORD wHdlType);
void  g_draw_handle       (HGSYS hgsys, void *hdl, DWORD wHdlType, DWORD wAdaptHdl, LONG x1, LONG y1, LONG x2, LONG y2);
void  g_save_handle       (HGSYS hgsys, void *hdl, DWORD wHdlType);
//void *g_load_handle       (HGSYS hgsys, DWORD wHdlType);

// -------------------------------------------------------------------------
// Demande le hwnd associ� au HGSYS
HWND g_get_hwnd (HGSYS hgsys);
// Demande le hdc associ� au HGSYS
HDC g_get_hdc (HGSYS hgsys);

// -------------------------------------------------------------------------
// Effectue un ::InvalidateRect sur un rectangle ou sur tout le device (prect NULL)
void g_InvalidateRect   (HGSYS hgsys, const RECT * prect);

// Fonctions de dessin
void g_line            (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_rectangle       (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_rectangle  (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_circle          (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_circle     (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_4_arc           (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_4_arc      (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_4_arc_rect      (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_4_arc_rect (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_h_2_arc         (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_h_2_arc    (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_v_2_arc         (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_v_2_arc    (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_tri_rect        (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_tri_rect   (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_h_arrow         (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_h_arrow    (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_v_arrow         (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_v_arrow    (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_h_vanne         (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_h_vanne    (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_v_vanne         (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, LONG x1, LONG y1, LONG x2, LONG y2);
void g_fill_v_vanne    (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, LONG x1, LONG y1, LONG x2, LONG y2);
void g_h_write         (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD police, DWORD policestyle, char *pszText);
void g_v_write         (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD police, DWORD policestyle, char *pszText);
void g_polygonne       (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE nStyleLigne, DWORD nb_points, PPOINT td_points);
void g_fill_polygonne  (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_REMPLISSAGE nStyleRemplissage, DWORD nb_points, PPOINT td_points);
void g_trend           (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2);
void g_bargraf         (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2);
void g_entry_field     (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD police, DWORD policestyle, char *pszText);
void g_static_text     (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2, DWORD police, DWORD policestyle, char *pszText);
void g_logic_entry     (HGSYS hgsys, G_COULEUR nCouleur, LONG x1, LONG y1, LONG x2, LONG y2);

// ------------------------------------------------------------------------
// Gestion des �lement graphique
// ------------------------------------------------------------------------
// Cr�ation / destruction
HELEMENT hGElementCree	(void);
void GElementFerme			(HELEMENT * phelement);

// Remise � NOP
void GElementReset      (HELEMENT helement);
// �l�ment Nop ?
BOOL bGElementNop				(HELEMENT helement);

// Modifications
void GElementSetAction           (HELEMENT helement, G_ACTION Action);
void GElementSetCouleurLigne (HELEMENT helement, G_COULEUR nCouleurLigne);
void GElementSetCouleurRemplissage (HELEMENT helement, G_COULEUR nCouleurRemplissage);
void GElementSetCouleur (HELEMENT helement, G_COULEUR nCouleur); // les 2 avec m�me valeur
void GElementSetCouleurs (HELEMENT helement, G_COULEUR nCouleurLigne, G_COULEUR nCouleurRemplissage);

void GElementSetStyleLigne (HELEMENT helement, G_STYLE_LIGNE nStyleLigne);
void GElementSetStyleRemplissage (HELEMENT helement, G_STYLE_REMPLISSAGE nStyleRemplissage);
void GElementSetStyles (HELEMENT helement, G_STYLE_LIGNE nStyleLigne, G_STYLE_REMPLISSAGE nStyleRemplissage);

void GElementSetStyleTexte       (HELEMENT helement, DWORD Police, DWORD PoliceStyle);
void GElementSetTexte            (HELEMENT helement, char *pszText);
BOOL GElementAjoutePoint				 (HGSYS hgsys, HELEMENT helement, LONG x, LONG y);
BOOL GElementResetBiPoint (HGSYS hgsys, HELEMENT helement, const POINT * pPoint0, const POINT * pPoint1);
void GElementSetDernierPoint     (HGSYS hgsys, HELEMENT helement, LONG x, LONG y);
void GElementDeplaceDernierPoint (HGSYS hgsys, HELEMENT helement, LONG dx, LONG dy);
void g_element_move_delta        (HGSYS hgsys, HELEMENT helement, LONG dx, LONG dy);

// d�place tous les points d'un �l�ment
void g_element_move              (HGSYS hgsys, HELEMENT helement, LONG dx, LONG dy);
void GElementSupprimePoints      (HELEMENT helement);

// Alterne action Vide <-> Plein. Renvoie TRUE si changement effectif de l'action
BOOL bGElementChangeRemplissage   (HGSYS hgsys, HELEMENT helement);

// Sym�tries
#define c_G_NULL_MIRROR            0x0
#define c_G_X_MIRROR               0x1
#define c_G_Y_MIRROR               0x2
#define c_G_XY_MIRROR              (c_G_X_MIRROR | c_G_Y_MIRROR) // 0x3
void g_element_miror            (HGSYS hgsys, HELEMENT helement, UINT Axes, LONG xAxe, LONG yAxe);

void g_element_rotate           (HGSYS hgsys, HELEMENT helement, LONG xCentre, LONG yCentre, UINT NbQuartTours);

// Zoome un �l�ment graphique
void g_element_zoom (HGSYS hgsys, HELEMENT helement, LONG xRef, LONG yRef,
										 LONG cxNew, LONG cxOld, LONG cyNew, LONG cyOld);

// Dessine un �l�ment graphique
void g_element_draw             (HGSYS hgsys, HELEMENT helement);
// Effectue un ::InvalidateRect sur un �l�ment graphique
void g_elementInvalidateRect    (HGSYS hgsys, HELEMENT helement);
// r�cup�ration d'infos
G_ACTION actionElement (HGSYS hgsys, HELEMENT helement);

void g_element_get (HGSYS hgsys, HELEMENT helement, G_ATTRIBUT * pAttributs,
	G_ACTION * pAction, 
	G_COULEUR * pnCouleurLigne, G_COULEUR *pnCouleurRemplissage,
	G_STYLE_LIGNE *pLineStyle, G_STYLE_REMPLISSAGE * pFillStyle,
  DWORD *pPolice, DWORD *pPoliceStyle, 
	char **ppszText, DWORD  *pNbPoints);

void g_element_get_point        (HGSYS hgsys, HELEMENT helement, DWORD IndexPoint, LONG *x, LONG *y);
// Encombrement
void g_EncombrementElement   (HGSYS hgsys, HELEMENT helement, LONG *left, LONG *top, LONG *right, LONG *bottom);
// Distance � un point
DWORD g_dist  (HGSYS hgsys, HELEMENT helement, LONG x, LONG y);

// R�cup�re les attributs d'un �l�ment Graphique
G_ATTRIBUT attributsElementGr (HELEMENT helement);

// Renvoie TRUE si au moins un des attributs appartient � un �l�ment Graphique
BOOL bAttributsElementGr (HELEMENT helement, G_ATTRIBUT nMasqueAttributs);

// ------------------------------------------------------------------------
// Conversions de Coordonn�es

// Pixel Device ---> Pixel Page (g�neralis�)
void g_convert_point_dev_to_page (HGSYS hgsys, LONG * px, LONG * py);
LONG  g_convert_x_dev_to_page (HGSYS hgsys, LONG x);
LONG  g_convert_y_dev_to_page (HGSYS hgsys, LONG y);

// Pixel Page (g�neralis�) ---> Pixel Device
void g_convert_point_page_to_dev (HGSYS hgsys, LONG * px, LONG * py);
LONG  g_convert_x_page_to_dev (HGSYS hgsys, LONG x);
LONG  g_convert_y_page_to_dev (HGSYS hgsys, LONG y);

// DELTA Pixel Page (g�neralis�) ---> DELTA Pixel Device
void g_convert_delta_page_to_dev (HGSYS hgsys, LONG * pDx, LONG * pDy);
LONG  g_convert_dx_page_to_dev (HGSYS hgsys, LONG dx);
LONG  g_convert_dy_page_to_dev (HGSYS hgsys, LONG dy);

// DELTA Pixel Device (g�neralis�) ---> DELTA Pixel Page
void g_convert_delta_dev_to_page (HGSYS hgsys, LONG * pDx, LONG * pDy);
LONG  g_convert_dx_dev_to_page (HGSYS hgsys, LONG dx);
LONG  g_convert_dy_dev_to_page (HGSYS hgsys, LONG dy);

// Pixel Page (g�neralis�) Quelconque ---> Pixel Page (g�neralis�) Cadr�
void g_map_point_page (HGSYS hgsys, LONG * px, LONG * py);
LONG  g_map_x_page (HGSYS hgsys, LONG x);
LONG  g_map_y_page (HGSYS hgsys, LONG y);

// Addition Pixels Page (g�neralis�s) Quelconques ---> Pixel Page (g�neralis�) Cadr�
void g_add_points_page (HGSYS hgsys, LONG *xTarget, LONG *yTarget, LONG xOffset, LONG yOffset);
LONG  g_add_x_page (HGSYS hgsys, LONG x1, LONG x2);
LONG  g_add_y_page (HGSYS hgsys, LONG y1, LONG y2);

// Addition DELTA Pixels Page (g�neralis�s) Quelconques ---> DELTA Pixel Page (g�neralis�) Cadr�
void g_add_deltas_page (HGSYS hgsys, LONG *dxTarget, LONG *dyTarget, LONG xOffset, LONG yOffset);
LONG  g_add_dx_page (HGSYS hgsys, LONG dx1, LONG dx2);
LONG  g_add_dy_page (HGSYS hgsys, LONG dy1, LONG dy2);

// ------------------------------------------------------------------------
// Dessine le fond (zone cliente de la fen�tre) avec une couleur, une grille, r�cup�re sa taille
void g_fill_background (HGSYS hgsys, G_COULEUR nCouleur);
void g_grille (HGSYS hgsys, G_COULEUR nCouleur, G_STYLE_LIGNE style, LONG pas_x, LONG pas_y);
void g_get_taille_background (HGSYS hgsys, LONG * dxFond, LONG * dyFond);

// ------------------------------------------------------------------------
BOOL g_color_stable (HGSYS hgsys, G_COULEUR nCouleur);

// Dibs
HANDLE g_load_bmp  (HGSYS hgsys, PCSTR pszBmpFile);
void  g_delete_bmp (HGSYS hgsys, HANDLE * phBmp);
void  g_draw_bmp   (HGSYS hgsys, HANDLE hdlBmp, BOOL bStretch, LONG x1, LONG y1, LONG x2, LONG y2);

// ------------------------------------------------------------------------

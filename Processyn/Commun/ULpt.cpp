// --------------------------------------------------------------------------
// ULpt.c Gestion de ports LPTx
// Version WIN32 25/7/97
// --------------------------------------------------------------------------
#include "stdafx.h"
#include "MemMan.h"
#include "UStr.h"
#include "USignale.h"
//#include "Verif.h"

#include "ULpt.h"
//VerifInit;

#define SIGNATURE_HLPT	'HLPT'
// --------------------------------------------------------------------------
// Valeurs par d�faut
// --------------------------------------------------------------------------
#define PRN_TIMEOUT_WRITE   1000	  // Time-out �criture, en mili-seconde
// --------------------------------------------------------------------------
//---------------------------------------------------------------------------
// conversion du handle en pointeur sur l'objet
CLpt::PENV_LPT	CLpt::pFromhLPT (HLPT hlpt)
	{
	PENV_LPT	pEnvLpt = (PENV_LPT) hlpt;

	if (!pEnvLpt || (pEnvLpt->dwSignature != SIGNATURE_HLPT))
		pEnvLpt = NULL;

	return pEnvLpt;
	}


// --------------------------------------------------------------------------
// $$ Debug : popup Err systeme
void CLpt::DebugSignaleErr (PENV_LPT pEnvLpt, PCSTR pszContexte)
	{
	char szErr [1000];

	wsprintf (szErr, "%s :\nl'ex�cution de %s a provoqu� l'erreur OS %lu.", pEnvLpt->szNomPort, pszContexte, GetLastError());
	SignaleWarnExit(szErr);
	}

//---------------------------------------------------------------------
void CLpt::StatutLastError (DWORD * pdwStatut)
	{
	(*pdwStatut) = GetLastError();
	}

// --------------------------------------------------------------------------
// Ouverture d'un port d'imprimante parall�le
BOOL CLpt::bLptOuvre 
(
 PHLPT phLpt,	// adresse du handle � mettre � jour (mis � NULL si err)
 PCSTR szNomPort // Nom du port : "LPT1", "LPT2"
 )							// Renvoie TRUE si le port est ouvert
	{
	BOOL	bRes = FALSE;
	PENV_LPT	pEnvLpt = (PENV_LPT)pMemAlloueInit0 (sizeof(ENV_LPT));
	HLPT			hLpt = (HLPT)pEnvLpt;

	pEnvLpt->dwSignature = SIGNATURE_HLPT;
	StrCopy (pEnvLpt->szNomPort, szNomPort);
	pEnvLpt->hf = INVALID_HANDLE_VALUE;

	// Ouverture du port Ok ?
	pEnvLpt->hf = CreateFile (szNomPort, GENERIC_READ | GENERIC_WRITE,
		0,										// lpt devices must be opened w/exclusive-access
		NULL,									// no security attrs
		OPEN_EXISTING,				// comm devices must use OPEN_EXISTING
		FILE_FLAG_OVERLAPPED, // overlapped I/O
		NULL									// hTemplate must be NULL for comm devices
		);
	if (pEnvLpt->hf != INVALID_HANDLE_VALUE)
		{
		// oui => Initialisation de la structure OVERLAPPED Ok ?
		if (pEnvLpt->ovl.hEvent = CreateEvent (NULL, TRUE, TRUE, NULL))
			{
			bRes = TRUE;
			pEnvLpt->lTimeOutWrite = PRN_TIMEOUT_WRITE;
			}
		else
			DebugSignaleErr (pEnvLpt, "CreateEvent");
		}

	// Erreur lors de l'ouverture ?
	if (!bRes)
		// oui => lib�re les ressources ouvertes
		bFermeLpt (&hLpt);

	// Mise � jour du handle utilisateur
	*phLpt = hLpt;

	return bRes;
	}


// --------------------------------------------------------------------------
// Fermeture d'un port d'imprimante parall�le
BOOL CLpt::bFermeLpt 
(
 PHLPT phLpt	// adresse du handle � mettre � jour (remis � NULL)
 )						// Renvoie TRUE si le port est ferm�
	{
	BOOL	bRes = FALSE;
	PENV_LPT	pEnvLpt = pFromhLPT (*phLpt);

	// Handle valide ?
	if (pEnvLpt)
		{
		// oui => Ferme le handle
		*phLpt = NULL;

		// note que l'objet est invalide
		pEnvLpt->dwSignature = 0;

		// fermeture du s�maphore d'overlapped
		if (pEnvLpt->ovl.hEvent)
			{
			CloseHandle (pEnvLpt->ovl.hEvent);
			pEnvLpt->ovl.hEvent = NULL;
			}

		// Fermeture du handle fichier
		if (pEnvLpt->hf != INVALID_HANDLE_VALUE)
			{
			if (CloseHandle (pEnvLpt->hf))
				{
				pEnvLpt->hf = INVALID_HANDLE_VALUE;
				}
			}

		// Lib�ration de l'objet
		MemLibere ((PVOID *)(&pEnvLpt));

		bRes = TRUE;
		}

	return bRes;
	}

//---------------------------------------------------------------------
// Force le timeout d'attente fin d'une ecriture
void CLpt::LptSetTimeOut (HLPT hLpt, LONG lTimeOut)
	{
	PENV_LPT	pEnvLpt = pFromhLPT (hLpt);

	pEnvLpt->lTimeOutWrite = lTimeOut;
	}

//---------------------------------------------------------------------
// Attente de la fin d'une �criture en cours
CLpt::ERREUR_LPT CLpt::LptAttenteFinEcriture (HLPT hLpt, DWORD * pdwErreurSysteme, DWORD * pdwTailleEcrite)
	{
	ERREUR_LPT	LRes = ERREUR_LPT_NONE;
	PENV_LPT	pEnvLpt = pFromhLPT (hLpt);
	DWORD dwRetourWait;

	*pdwTailleEcrite = 0;
	*pdwErreurSysteme = NO_ERROR;

	if (pEnvLpt->bOverlappedEnCours)
		{
		// Attends que l'�mission se termine
		dwRetourWait = WaitForSingleObject (pEnvLpt->ovl.hEvent, pEnvLpt->lTimeOutWrite);

		switch (dwRetourWait)
			{
			case WAIT_OBJECT_0:
				// Emission Ok ?
				if (GetOverlappedResult (pEnvLpt->hf, &pEnvLpt->ovl, &pEnvLpt->dwTailleEcrite, FALSE))
					{
					pEnvLpt->bOverlappedEnCours = FALSE;
					if (pEnvLpt->dwTailleEcrite == pEnvLpt->dwTailleBufEmission)
						{
						// Libere le Buffer d'�mission
						MemLibere ((PVOID *)(&pEnvLpt->pbBufEmission));
						}
					else
						{
						LRes = ERREUR_LPT_WRITE;
						}
					(*pdwTailleEcrite) = pEnvLpt->dwTailleEcrite;
					}
				else
					{
					LRes = ERREUR_LPT_STATUS_WRITE;
					StatutLastError(pdwErreurSysteme);
					}
				break;

			case WAIT_TIMEOUT:
				LRes = ERREUR_LPT_WRITE_TIME_OUT;
				break;

			default:
				LRes = ERREUR_LPT_WRITE;
				DebugSignaleErr (pEnvLpt, "Wait");
				break;

			} // switch Wait
		} // pas overlapped en cours
	return LRes;
	} // bAttenteFinEmission

// ------------------------------------------------------------------------------------------------------------------
// Ecriture sur l'imprimante
CLpt::ERREUR_LPT CLpt::LptEcrire (HLPT hLpt, DWORD dwTailleBuf, BYTE * pbBuf, DWORD * pdwErreurSysteme)
	{
	ERREUR_LPT  LRes = ERREUR_LPT_NONE;
	BOOL			bOkWrite = FALSE;
	PENV_LPT	pEnvLpt = pFromhLPT (hLpt);

	(*pdwErreurSysteme) = NO_ERROR;

	if (pEnvLpt)
		{
		// Pas d'�mission en cours et quelque chose � �crire ?
		if ((!pEnvLpt->bOverlappedEnCours) && dwTailleBuf && pbBuf)
			{
			// oui copie des donn�es � �crire au buffer d'attente d'�mission
			pEnvLpt->dwTailleBufEmission = dwTailleBuf;
			pEnvLpt->pbBufEmission = (PBYTE)pMemAlloueInit (dwTailleBuf, pbBuf);

			// Emission Ok ?
			if (ResetEvent (pEnvLpt->ovl.hEvent))
				{
				bOkWrite = WriteFile (pEnvLpt->hf, pEnvLpt->pbBufEmission,
					pEnvLpt->dwTailleBufEmission, &pEnvLpt->dwTailleEcrite, &pEnvLpt->ovl);
				if ((bOkWrite ) || (GetLastError() == ERROR_IO_PENDING))
					{
					// oui => Ok
					pEnvLpt->bOverlappedEnCours = TRUE;
					}
				else
					{
					LRes = ERREUR_LPT_STATUS_WRITE;
					StatutLastError (pdwErreurSysteme);
					// Libere le Buffer d'�mission
					MemLibere ((PVOID *)(&pEnvLpt->pbBufEmission));
					}
				} 
			else // ResetEvent pas Ok
				{
				LRes = ERREUR_LPT_STATUS_WRITE;
				StatutLastError (pdwErreurSysteme);
				// Libere le Buffer d'�mission
				MemLibere ((PVOID *)(&pEnvLpt->pbBufEmission));
				}
			} // if (dwTailleBuf && pbBuf)
		} // if (pEnvLpt)
	else
		{
		LRes = ERREUR_LPT_NONE;
		}

	return LRes;
	} // bLptEcrire



/*----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                     |
 |              Societe LOGIQUE INDUSTRIE                               |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                  |
 |   Il est demeure sa propriete exclusive et est confidentiel.         |
 |   Aucune diffusion n'est possible sans accord ecrit                  |
 |----------------------------------------------------------------------|
 |   Titre   : pcsdlg.h
 |   Auteur  : LM                                                       |
 |   Date    : 01/09/92 						|
 |   Remarques : Gestion des boites de dialogue dans Pcsconf            |
 +----------------------------------------------------------------------*/

#define c_NB_MAX_CAR_TIT_DLGPCS 20

// -----------------------------------------------------------------------
// Structures renvoy�es aux boites de dialogue
// -----------------------------------------------------------------------
typedef struct
	{
	char titre[c_NB_MAX_CAR_TIT_DLGPCS];
	char saisie[255];
	void *pteur_param;
	} t_dlg_fichier;

typedef struct
	{
	char titre[c_NB_MAX_CAR_TIT_DLGPCS];
	char saisie[255];
	DWORD nb_max_car_saisie;
	void *pteur_param;
	} PARAM_DLG_SAISIE_TEXTE, *PPARAM_DLG_SAISIE_TEXTE;

typedef struct
	{
	char titre[c_NB_MAX_CAR_TIT_DLGPCS];
	int saisie;
	void *pteur_param;
	} PARAM_DLG_SAISIE_NUM, *PPARAM_DLG_SAISIE_NUM;

// --------------------------------------------------------------------------
HWND hwndCreeSplash (HWND hwndParent);

int  dlg_preambule (HWND hwnd, BOOL preamb_timer);

// Choisir le pathname d'un fichier existant � ouvrir 
BOOL bDlgFichierOuvrir 
	(HWND hwndParent, 
	PSTR pszPathNameCourant,	// valeur initiale puis r�sultat : taille = MAX_PATH
	LPCTSTR pszTitre,					// titre de la boite de dialogue
	LPCTSTR pszExtension,			// extension � utiliser (sans \ ni .)
	LPCTSTR pszAction					// libell� utilis� sur le bouton d'ouverture
	);

// Choisir le pathname d'un fichier � enregistrer
BOOL bDlgFichierEnregistrerSous (HWND hwndParent, PSTR pszPathNameCourant,
	LPCTSTR pszTitre, LPCTSTR pszExtension, LPCTSTR pszAction);

BOOL dlg_saisie_texte (HWND hwnd, PPARAM_DLG_SAISIE_TEXTE retour_saisie_texte);
BOOL dlg_saisie_num (HWND hwnd, PPARAM_DLG_SAISIE_NUM retour_saisie_num);

// Afficher une boite de dialogue modale avec message erreur
int dlg_erreur (char *mess);

// Afficher une boite de dialogue modale d'erreur � partir du num�ro de message erreur
void dlg_num_erreur (DWORD dwNErr);

// Affiche une boite de dialogue modale de demande de confirmation
// renvoie IDYES IDNO ou IDCANCEL
int dlg_confirmation (HWND hwndParent, char *mess);

//----------------------------------------------------------------------------
// fonction qui transforme un chemin pour qu'il rentre ds une boite avec (...)
//----------------------------------------------------------------------------
PSTR DlgFitPathToBox (HWND hDlg, int idStatic, PSZ lpch);


//-----------------------------------------
// CheckMan.h
// Gestion de groupes de RADIO BUTTON
//-----------------------------------------

// renvoie l'index 1..n du premier bouton check� d'une suite de radio boutons
// ou -1 si quelque chose n'a pas march�
// Remarque : un bouton dans l'�tat BST_INDETERMINATE sera consid�r� comme BST_CHECKED
int nCheckIndex (HWND hDlgParente, int nIdPremierBouton);

// R�cup�re l'Id du premier radio bouton du groupe auquel appartient le radio bouton sp�cifi�
BOOL bIdPremierRadio (HWND hDlgParente, HWND hwndRadio, int * pnIdPremierRadio);

// Check du n-i�me (1..n) radio bouton du groupe auquel appartient le radio bouton sp�cifi�
BOOL bCheckRadioBouton (HWND hDlgParente, HWND hwndRadio, int nNRadio);

// R�cup�re la position (1..n) dans son groupe du radio bouton sp�cifi�
BOOL bPositionRadioDansGroupe (HWND hDlgParente, HWND hwndRadio, int * pnNPositionDansGroupe);



// SoftKey.h
// Gestion des cl�s logicielles
// JS WIN32 28/3/97

//-----------------------------------------------------------------------------------------
// Renvoie un Num�ro de cl� logicielle stock� crypt� dans la base de registre
DWORD dwGetNumeroCle (PCSTR szPathKey, PCSTR szNomKey);

//-----------------------------------------------------------------------------------------
// Enregistre un Num�ro de cl� encrypt� dans la base de registre
BOOL bSetNumeroCle (PCSTR szPathKey, PCSTR szNomKey, DWORD dwNumCleSaisie);

//-----------------------------------------------------------------------------------------
// Renvoie un num�ro caract�ristique de la machine (en fait du formatage de son disque dur)
DWORD dwGetIdMachine (void);


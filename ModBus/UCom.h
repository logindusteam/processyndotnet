/*-----------------------------------------------------------------------+
 |   Ce fichier est la propriete de                                      |
 |              Societe LOGIQUE INDUSTRIE                                |
 |       Z.I. LES MILLES 13851 AIX-EN-PROVENCE CEDEX 3                   |
 |   Il est demeure sa propriete exclusive et est confidentiel.          |
 |   Aucune diffusion n'est possible sans accord ecrit                   |
 |-----------------------------------------------------------------------*/
// Gestion des COMs WIN32
// 28/11/97
#ifndef UCOM_H
#define UCOM_H

// Un num�ro de canal x va de 1 � NB_MAX_VOIE et d�signe le device 'COMx' corespondant
#define NB_MAX_VOIE 36

// Valeurs d'erreur (0 = NO_ERROR = pas d'erreur)
typedef enum
	{
	ERREUR_COM_OK								 =   0,	// pas d'erreur
	ERREUR_COM_PARITE            =   1,	// parit� invalide
	ERREUR_COM_ENCADREMENT       =   2,	// nombre de stop bits invalide
	ERREUR_COM_OVERRUN           =   4,	// au moins un caract�re a �t� perdu ou buffer r�ception trop petit
	ERREUR_COM_BUFFER_VIDE       =   8,	// rien re�u
	ERREUR_COM_CANAL_INVALIDE    =  16,	// canal (non ouvert, inexistant, ouvert par une autre appli...)
	ERREUR_COM_TIME_OUT          =  32, // time out global
	ERREUR_COM_TIME_OUT_INTER_CAR=  64, // time out entre la r�ception de 2 cars
	ERREUR_COM_SYSTEME           = 128	// autre erreur
	} ERREUR_COM;

// Crit�re de fin de r�ception de plusieurs caract�res
typedef enum
	{
	MODE_COM_TIME_OUT_IC = 1,
	MODE_COM_CAR_CONTROLE,
	MODE_COM_NBRE_CAR
	} FIN_RECEPTION_COM;


// --------------------------------------------------------------------------------------
// Ouverture d'un canal de com
ERREUR_COM WINAPI nComOuvre 
	(
	UINT uCanal,					// num�ro de COM de 1 � NB_MAX_VOIE
	BOOL bReceptionAsync,	// TRUE si mode async en r�ception (affecte les autres fonctions automatiquement)
	UINT vitesse,					// en Bauds (ex: 9600)
	UINT nb_de_bits,			// 5, 6, 7 ou 8 (selon les capacit�s du driver)
	UINT parite,					// 0 = sans, 1 = impaire, 2 = paire
	UINT encadrement			// 1 = 1 stop bit, 2 = 2 stop bit, autre = erreur
	); // Renvoie le code d'erreur (test� si != 0 dans Pcs)

// --------------------------------------------------------------------------------------
// Fermeture d'un canal de com ouvert
ERREUR_COM WINAPI nComFerme (UINT uCanal);

// --------------------------------------------------------------------------------------
// Reception d'un caract�re
// Attention le r�sultat peut �tre un OU entre plusieurs erreurs
// si rien re�u renvoie ERREUR_COM_BUFFER_VIDE
ERREUR_COM WINAPI nComLireCar (UINT uCanal, CHAR *pc);  // Renvoie le code d'erreur (test� si != 0 dans Pcs)

// --------------------------------------------------------------------------
//  R�ception de N caract�res avec crit�res de sortie
//  ! Ne fonctionne qu'en mode asynchrone (sinon renvoie ERREUR_COM_CANAL_INVALIDE)
//  MODE_COM_TIME_OUT_IC :	sortie par time_out inter caracteres => retour = 0
//  MODE_COM_CAR_CONTROLE : sortie par caractere de controle => retour = 0
//  MODE_COM_NBRE_CAR :			sortie par nombre de caracteres recus => retour = 0
//  Peut renvoyer un OU entre plusieurs ERREUR_COM
// --------------------------------------------------------------------------
ERREUR_COM WINAPI nComLire
	(
	UINT uCanal,
	void * pBuffer,										// adresse du buffer de r�ception
	DWORD dwTailleBuffer,							// taille en octets du buffer de r�ception
	FIN_RECEPTION_COM nFinReception,	// MODE_COM_TIME_OUT_IC, MODE_COM_CAR_CONTROLE ou MODE_COM_NBRE_CAR
	LONG lTimeOutPremierCarMs,				// dur�e max d'attente du premier caract�re (en millisecondes)
	LONG lTimeOutCarsSuivantsMs,			// dur�e max d'attente de chaque caract�re suivant (en millisecondes)
	DWORD *pdwNbLus,									// Nombre de cars re�us (si MODE_COM_NBRE_CAR : � initialiser au Nb de cars attendus)
	CHAR cFinReception								// si MODE_COM_CAR_CONTROLE : car terminant la r�ception
	);

// --------------------------------------------------------------------------------------
// Vide les caract�res �ventuellement re�us dans le canal ouvert
BOOL WINAPI bComVideReception (UINT uCanal); // Renvoie TRUE si pas d'erreur, FALSE sinon

// --------------------------------------------------------------------------
// Emission de N caract�res : bloquant tant que tous les caract�res ne sont pas �mis
//	time out 1s en cas de probleme en emission
// ! renvoie une erreur OS
UINT WINAPI uComEcrire
	(
	UINT uCanal,
	void * pBuffer, DWORD dwNbOctetsAEcrire, // Donn�es � �mettre
	BOOL bAttenteFin			// TRUE pour attente fin �mission
	);

// --------------------------------------------------------------------------
// Gestion des signaux
// Fixe de la broche RTS
BOOL WINAPI bComEtablisRts (UINT uCanal, BOOL bSignalOn);
// r�cup�re l'�tat des signaux du Modem (voir la signification des bits)
BOOL WINAPI bComEtatModem (UINT uCanal, DWORD *pEtat);

// --------------------------------------------------------------------------
//	Ouvre puis ferme un canal. Renvoie le statut de ces op�rations
ERREUR_COM WINAPI nComTestCanal (UINT uCanal);	// num�ro de COM de 1 � NB_MAX_VOIE

// --------------------------------------------------------------------------
// Renvoie TRUE si le canal est ouvert, FALSE sinon
BOOL WINAPI bComOuverte (UINT uCanal);

#endif
// -------------------------------- fin UCom.h --------------------------------




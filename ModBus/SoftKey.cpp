// SoftKey.c
// Gestion des cl�s logicielles
// JS WIN32 28/3/97

#include <stdafx.h>
#include "RegMan.h"
#include "SoftKey.h"

#define CRYPT_32 0x7ae439fb

//-----------------------------------------------------------------------------------------
// Renvoie un Num�ro de cl� stock� dans la base de registre
DWORD dwGetNumeroCle (PCSTR szPathKey, PCSTR szNomKey)
	{
	DWORD	dwRes = 0;
	DWORD dwTypeCle;
	DWORD dwTailleData = sizeof(dwRes);
	HKEY	hKey;

	// Lecture de la valeur de cl� Ok ?
	if (ERROR_SUCCESS == RegOpenKeyEx (HKEY_LOCAL_MACHINE,	szPathKey, 0, KEY_QUERY_VALUE, &hKey))
		{
		DWORD	dwErr = RegQueryValueEx (hKey, szNomKey, NULL, &dwTypeCle, (LPBYTE)&dwRes, &dwTailleData);

		// Lecture de la valeur de cl� Ok ?
		if ((dwErr == NO_ERROR) && (dwTypeCle == REG_DWORD))
			{
			// oui => r�cup�re la valeur non encod�e de la cl�
			dwRes ^= CRYPT_32;
			}
		else
			dwErr = 0;

		// Fermeture de la cl�
		RegCloseKey (hKey);
		}
 
	return dwRes;
	}

//-----------------------------------------------------------------------------------------
// Enregistre un Num�ro de cl� dans la base de registre
BOOL bSetNumeroCle (PCSTR szPathKey, PCSTR szNomKey, DWORD dwNumCleSaisie)
	{
	BOOL	bRes = FALSE;
	DWORD	dwRes = dwNumCleSaisie;

	if (dwNumCleSaisie)
		{
		HKEY	hKey = 0;
		dwRes ^= CRYPT_32;

		// Lecture de la valeur de cl� Ok ?
		if (bRegOuvreOuCreeChemin (HKEY_LOCAL_MACHINE, szPathKey, &hKey))
			{
			// Enregistrement de la valeur de cl� Ok ?
			if (ERROR_SUCCESS == RegSetValueEx(
				hKey,	// handle of key to query 
				szNomKey,	// address of name of value to query 
				0,	// reserved 
				REG_DWORD,	// address of buffer for value type 
				(LPBYTE)&dwRes,	// address of data buffer 
				sizeof(dwRes) 	// address of data buffer size 
			 ))
				{
				// oui => Ok => Attends une seconde
				Sleep (1000);
				bRes = TRUE;
				}

			// Fermeture de la cl�
			RegCloseKey (hKey);
			}
		}
	
	return bRes;
	}

//-----------------------------------------------------------------------------------------
// Renvoie un Num�ro caract�ristique de la machine (en fait du formatage de son disque dur)
DWORD dwGetIdMachine (void)
	{
	char pszNomVolume [MAX_PATH];
	char pszFileSystemNameBuffer [MAX_PATH];
	DWORD dwVolumeSerialNumber = 0;
	DWORD dwMaximumComponentLength;
	DWORD dwFileSystemFlags;

	// Lecture des caract�ristiques du disque Ok ?
	if (GetVolumeInformation(
		"C:\\",	// address of root directory of the file system 
    pszNomVolume,	// address of name of the volume 
    MAX_PATH,	// length of lpVolumeNameBuffer 
    &dwVolumeSerialNumber,	// address of volume serial number 
    &dwMaximumComponentLength,	// address of system's maximum filename length
    &dwFileSystemFlags,	// address of file system flags 
    pszFileSystemNameBuffer,	// address of name of file system 
    MAX_PATH	// length of lpFileSystemNameBuffer 
   ))
		{
		// Cryptage inviolable : rend une cl� 32 bits
		dwVolumeSerialNumber ^= ((dwVolumeSerialNumber+CRYPT_32) << 23) ^ ((dwVolumeSerialNumber+CRYPT_32) << 14);
		dwVolumeSerialNumber &= 0xffffefff;
		return (dwVolumeSerialNumber + 23);
		}

	return 0;
	}


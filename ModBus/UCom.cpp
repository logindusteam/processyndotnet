// --------------------------------------------------------------------------
// UCom.c
// Gestion des Coms sous WIN32
// 13/3/97
#include "stdafx.h"
#include "UCom.h"

// Objet d'une voie de COM
typedef struct
  {
  HANDLE  hCanal;
	BOOL		bReceptionAsync;
	OVERLAPPED ovlReception;
	OVERLAPPED ovlEmission;
	COMMTIMEOUTS CommTimeoutsInitiaux;
	DWORD dwNbCaractereParSeconde;
  } VOIE_COM, *PVOIE_COM;

// Constantes
#define TIME_OUT_READ    2000          // 2 secondes
#define TIME_OUT_WRITE   999           // 1 seconde

#define TAILLE_BUF_DEVICE_RX 4096
#define TAILLE_BUF_DEVICE_TX 4096

#define ASCII_XON       0x11
#define ASCII_XOFF      0x13

#define VOIE_COM_NULLE {INVALID_HANDLE_VALUE, FALSE, {0,0,0,0,NULL}, {0,0,0,0,NULL},{0,0,0,0,0},0}

// Tableau des infos de voie, acc�d� de 1 � NB_MAX_VOIE
static VOIE_COM aVoiesCom [NB_MAX_VOIE+1] =
  {
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,VOIE_COM_NULLE,
	VOIE_COM_NULLE
  };

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//					FONCTIONS LOCALES
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Renvoie TRUE si le canal est ouvert, FALSE sinon
BOOL WINAPI bComOuverte (UINT uCanal) 
	{
	return (uCanal >= 1) && (uCanal <= NB_MAX_VOIE) &&	(aVoiesCom[uCanal].hCanal != INVALID_HANDLE_VALUE);
	}

//------------------------------------------------------
// R�cup�re l'�ventuelle erreur bloquant un canal
static ERREUR_COM nDebloqueErreur (HANDLE hdlCom)
	{
	DWORD	nErreur = ERREUR_COM_OK;
	DWORD	dwComErr;

	if (ClearCommError (hdlCom, &dwComErr, NULL))
		{
		if (dwComErr)
			{
			if ((dwComErr & (CE_RXOVER|CE_OVERRUN)) != 0)
    		nErreur |= ERREUR_COM_OVERRUN;

			if ((dwComErr & CE_RXPARITY) != 0)
    		nErreur |= ERREUR_COM_PARITE;

			if ((dwComErr & CE_FRAME) != 0)
    		nErreur |= ERREUR_COM_ENCADREMENT;

			if (dwComErr & (CE_BREAK|CE_TXFULL|CE_MODE))
    		nErreur |= ERREUR_COM_SYSTEME; // $$ mauvaise erreur!!! (nouvelle sous WIN32)
			}
		} // if (ClearCommError (hdl, &dwComErr, &comstat))
	else
		nErreur = ERREUR_COM_CANAL_INVALIDE;

	return (ERREUR_COM)nErreur;
	} // nDebloqueErreur
							
// --------------------------------------------------------------------------------------------
// Initialise les param�tres de configuration d'une COM
static void InitDCB (DCB * pdcb, UINT vitesse, UINT nb_de_bits, UINT parite, UINT encadrement)
  {
	pdcb->DCBlength	= sizeof(DCB);
	pdcb->BaudRate	= vitesse;
	pdcb->fBinary		= TRUE;									// Pas de gestion de caract�re EOF (obligatoire)
	pdcb->fParity		= TRUE;									// Valide le test de parit�
	pdcb->fOutxCtsFlow = FALSE;							// pas d'arr�t d'�mission si CTS (entr�e) est bas
	pdcb->fOutxDsrFlow = FALSE;							// pas d'arr�t d'�mission si DSR (entr�e) est bas
	pdcb->fDtrControl = DTR_CONTROL_ENABLE;	// DTR (sortie) Haut si COM ouverte (EscapeCommFunction pour le changer)
	pdcb->fDsrSensitivity = FALSE;					// pas de r�ception de caract�re ignor�e si DSR (entr�e) est bas
	pdcb->fTXContinueOnXoff = TRUE;					// continue la transmission m�me si le driver a envoy� un Xoff
	pdcb->fOutX = FALSE;										// pas de contr�le de l'�mission selon la r�ception de Xon Xoff
	pdcb->fInX = FALSE;											// pas d'�mission de Xon Xoff selon l'�tat du buffer de r�ception
	pdcb->fErrorChar = FALSE;								// pas de remplacement des caract�res re�us avec erreur de parit�
	pdcb->fNull = FALSE;										// pas d'omission des caract�res NUL re�us
	pdcb->fRtsControl = RTS_CONTROL_DISABLE;// RTS (sortie) Bas si COM ouverte (EscapeCommFunction pour le changer)
	pdcb->fAbortOnError = TRUE; //FALSE;		// Read et Write s'arr�tent sur erreur et renvoient ERROR_IO_ABORTED => ClearCommError
	pdcb->fDummy2 = 0;
	pdcb->wReserved = 0;
	pdcb->XonLim = 4095;										// Nb limite dans la taille du buffer (NT ne supporte pas plus)
	pdcb->XoffLim = 4094;										//512;	//$$65535;
	pdcb->ByteSize	= (BYTE)nb_de_bits;
	switch (parite)//$$ utiliser constante
		{
		case 0:	pdcb->Parity = NOPARITY; break;
		case 1:	pdcb->Parity = ODDPARITY; break;
		case 2:	pdcb->Parity = EVENPARITY; break;
		default:pdcb->Parity = (char)-1; break; //MARKPARITY
		}
	switch (encadrement)//$$ utiliser constante
		{
		case 1:	pdcb->StopBits = ONESTOPBIT; break;
		case 2:	pdcb->StopBits = TWOSTOPBITS; break;
		default:pdcb->StopBits = (char)-1; break; //ONE5STOPBIT
		}

	pdcb->XonChar = ASCII_XON;							// Valeur du caract�re XON en �mission et en r�ception
	pdcb->XoffChar = ASCII_XOFF;						// Valeur du caract�re XOFF en �mission et en r�ception
	pdcb->ErrorChar = '\0';									// Valeur du caract�re rempla�ant ceux re�us avec erreur de parit�
	pdcb->EofChar = '\0';										// Valeur du caract�re de fin de fichier (si mode non binaire)
	pdcb->EvtChar = '\0';										// Valeur du caract�re provoquant un �v�nement (EV_RXFLAG, SetCommMask et WaitCommEvent)
	pdcb->wReserved1 = 0;
  } // InitDCB

// --------------------------------------------------------------------------
// Forcage Time_out Lecture: Fixe l'attente globale du read et positionne le timeout inter-car � MAXWORD
// Ne peut �tre utilis� que dans le cas de la lecture d'un octet.
static void MajTimeOutEcoute (UINT uCanal, DWORD dwTimeOutRead)
	{
	COMMTIMEOUTS	CommTimeouts;
	PVOIE_COM pVoieCom = &aVoiesCom[uCanal];
	HANDLE hdl = pVoieCom->hCanal;

	GetCommTimeouts (hdl, &CommTimeouts);
	CommTimeouts.ReadIntervalTimeout = MAXDWORD;
	CommTimeouts.ReadTotalTimeoutMultiplier = 0;
	CommTimeouts.ReadTotalTimeoutConstant = dwTimeOutRead;
	SetCommTimeouts(hdl, &CommTimeouts);
	}

// --------------------------------------------------------------------------
// Forcage Time_out Ecriture: Fixe l'attente globale du write 
static void MajTimeOutParle (UINT uCanal, DWORD dwTimeOutWrite)
	{
	COMMTIMEOUTS	CommTimeouts;
	PVOIE_COM pVoieCom = &aVoiesCom[uCanal];
	HANDLE hdl = pVoieCom->hCanal;

	GetCommTimeouts (hdl, &CommTimeouts);
	CommTimeouts.WriteTotalTimeoutMultiplier = 0; 
	CommTimeouts.WriteTotalTimeoutConstant = dwTimeOutWrite;
	SetCommTimeouts(hdl, &CommTimeouts);
	}

// --------------------------------------------------------------------------
// Ouverture OVERLAPPED du device associ� � un canal (Attention ! la nomenclature d'erreur a chang�)
static ERREUR_COM nOuvreDeviceCanal (UINT uCanal, BOOL bReceptionAsync, DWORD dwTimeOutRead, DWORD dwTimeOutWrite, DCB *pdcb)
  {
  DWORD nRes = ERREUR_COM_SYSTEME;

	// teste la validit� des param�tres ($$ limiter d'apr�s les capacit�s du device (Cf GetCommConfig)
  if ((uCanal < 1) || (uCanal > NB_MAX_VOIE))
    nRes = ERREUR_COM_CANAL_INVALIDE; //1;
  else
    {
		PVOIE_COM pVoieCom = &aVoiesCom[uCanal];

    if ((pdcb->BaudRate < 50) || (pdcb->BaudRate > CBR_256000))
      nRes = ERREUR_COM_SYSTEME; //2;
    else
      {
      if ((pdcb->ByteSize < 5) || (pdcb->ByteSize > 8))
				nRes = ERREUR_COM_SYSTEME; //3;
      else
				{
        if (pdcb->Parity > SPACEPARITY)
					nRes = ERREUR_COM_PARITE; //4;
				else
					{
					if (pdcb->StopBits > TWOSTOPBITS)
						nRes = ERREUR_COM_ENCADREMENT; //5;
					else
						{
						if (pVoieCom->hCanal != INVALID_HANDLE_VALUE)
							nRes = ERREUR_COM_CANAL_INVALIDE; //6;
						else
								{  
								CHAR		szNomVoie[20];
								HANDLE  hdl = INVALID_HANDLE_VALUE;

								// cr�e le nom du device sous la forme "COMx" (x vaut uCanal)
								wsprintf (szNomVoie, "\\\\.\\COM%lu", uCanal);

								// Ouverture Ok ?
								hdl = CreateFile (szNomVoie, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 
									FILE_FLAG_OVERLAPPED, NULL);
								if (hdl != INVALID_HANDLE_VALUE)
									{
									// oui => fixe les tailles de buffers ?
									if (SetupComm (hdl, TAILLE_BUF_DEVICE_RX,  TAILLE_BUF_DEVICE_TX))
										{
										// oui => fixe la configuration ?
										if (SetCommState(hdl, pdcb))
											{
											COMMTIMEOUTS	CommTimeoutsInitiaux;
											COMMTIMEOUTS	CommTimeOuts;
											DWORD					dwComErr = 0;

											// fixe les time outs de fa�on � avoir une sortie sur time out en r�ception et en �mission
											if (dwTimeOutRead)
												CommTimeOuts.ReadIntervalTimeout = 0;
											else
												CommTimeOuts.ReadIntervalTimeout = MAXDWORD;
											CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
											CommTimeOuts.ReadTotalTimeoutConstant = dwTimeOutRead;
											CommTimeOuts.WriteTotalTimeoutMultiplier = 0; 
											CommTimeOuts.WriteTotalTimeoutConstant = dwTimeOutWrite;

											// Termine la config sans probl�mes ?
											if (GetCommTimeouts (hdl, &CommTimeoutsInitiaux) && SetCommTimeouts(hdl, &CommTimeOuts) &&
												ClearCommBreak (hdl) && ClearCommError(hdl, &dwComErr, NULL))
												{
												// config Ok
												pVoieCom->ovlReception.hEvent = CreateEvent (NULL, TRUE, FALSE, NULL);
												pVoieCom->ovlEmission.hEvent = CreateEvent (NULL, TRUE, FALSE, NULL);
												if (pVoieCom->ovlReception.hEvent && pVoieCom->ovlEmission.hEvent)
													{
													nRes = NO_ERROR;
													pVoieCom->bReceptionAsync = bReceptionAsync;
													pVoieCom->CommTimeoutsInitiaux = CommTimeoutsInitiaux;
													pVoieCom->dwNbCaractereParSeconde = (pdcb->BaudRate / 12); // Max pour Parite et nbStop
													pVoieCom->hCanal = hdl;
													}
												else
													CloseHandle (hdl);
												}
											else
												CloseHandle (hdl);
											}
										else
											CloseHandle (hdl);
										}
									else
										CloseHandle (hdl);
									}
								}
						}  // encadrement ok
					} // parite ok
				} // data bit ok
			} // vitesse ok
		} // numero de uCanal

  return (ERREUR_COM)nRes;
  } // nOuvreDeviceCanal

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//					FONCTIONS EXPORTEES
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Ouverture d'un canal de com
// Renvoie une erreur sp�cifique test�e si != 0 dans Pcs
// --------------------------------------------------------------------------
ERREUR_COM WINAPI nComOuvre 
	(UINT uCanal, 
	BOOL bReceptionAsync,	// TRUE si non bloquant en r�ception
	UINT vitesse, UINT nb_de_bits, UINT parite, UINT encadrement)
  {
	ERREUR_COM nRes;
  DCB dcb;

	// Initialise les param�tres de la com
	InitDCB (&dcb, vitesse, nb_de_bits, parite, encadrement);

	if (bReceptionAsync)
		{
		// D�croche asynchrone
		// Ouvre le canal de COM en asynchrone
		nRes = nOuvreDeviceCanal (uCanal, TRUE, TIME_OUT_READ, TIME_OUT_WRITE, &dcb);
		}
	else
		{
		// D�croche synchrone
		nRes = nOuvreDeviceCanal (uCanal, FALSE, 0, TIME_OUT_WRITE, &dcb);
		}

	// Ouvre le device non bloquant en lecture
  return nRes;
  } // nComOuvre

// --------------------------------------------------------------------------
// Fermeture d'un canal ouvert
ERREUR_COM WINAPI nComFerme (UINT uCanal)
  {
  ERREUR_COM nRes = ERREUR_COM_CANAL_INVALIDE;

	// canal ouvert ?
	if (bComOuverte (uCanal))
		{
		PVOIE_COM pVoieCom = &aVoiesCom[uCanal];

		// oui => Restaure les time outs et ferme le handle de comm ?
		if (SetCommTimeouts (pVoieCom->hCanal, &pVoieCom->CommTimeoutsInitiaux) && 
			CloseHandle (pVoieCom->hCanal))
			{
			// oui => fermeture des ressources
			if (pVoieCom->ovlReception.hEvent)
				{
				CloseHandle (pVoieCom->ovlReception.hEvent);
				pVoieCom->ovlReception.hEvent = NULL;
				}
			if (pVoieCom->ovlEmission.hEvent)
				{
				CloseHandle (pVoieCom->ovlEmission.hEvent);
				pVoieCom->ovlEmission.hEvent = NULL;
				}

			pVoieCom->bReceptionAsync = FALSE;
			pVoieCom->hCanal = INVALID_HANDLE_VALUE;
			nRes = ERREUR_COM_OK;
			}
		else
			{
			nRes = ERREUR_COM_SYSTEME;
			}
    }
  return nRes;
  } // nComFerme

// --------------------------------------------------------------------------------------
// Reception d'un caract�re
// Attention le r�sultat peut �tre un OU entre plusieurs erreurs
// si rien re�u renvoie ERREUR_COM_BUFFER_VIDE
// --------------------------------------------------------------------------
ERREUR_COM WINAPI nComLireCar (UINT uCanal, CHAR * pc)  // Renvoie le code d'erreur (test� si != 0 dans Pcs)
  {
  DWORD nRes = ERREUR_COM_CANAL_INVALIDE;

	if (bComOuverte (uCanal))
		{
		PVOIE_COM pVoieCom = &aVoiesCom[uCanal];
		nRes = ERREUR_COM_OK;

		// Arme l'attente de r�ception ?
		if (ResetEvent (pVoieCom->ovlReception.hEvent))
			{
			// oui => lecture Ok ?
			HANDLE hdl = pVoieCom->hCanal;
			DWORD nb_lus;

			if (ReadFile (hdl, pc, 1, &nb_lus, &pVoieCom->ovlReception))
				{
				// oui => tout est lu ?
				if (nb_lus == 1)
					// oui => Ok
					nRes = ERREUR_COM_OK;
				else
					// non => time out
					nRes = ERREUR_COM_BUFFER_VIDE;
				} // if (ReadFile (hdl, pc, 1, &nb_lus, &pVoieCom->ovlReception))
			else
				{
				DWORD	dwErrRead = GetLastError ();

				// Traitement selon l'erreur
				switch (dwErrRead)
					{
					case ERROR_IO_PENDING:
						{
						// lecture en cours => attente infinie de la fin de la lecture (soumise � time out)
						if (GetOverlappedResult (hdl, &pVoieCom->ovlReception, &nb_lus, TRUE))
							{
							// oui => tout est lu ?
							if (nb_lus == 1)
								// oui => Ok
								nRes = ERREUR_COM_OK;
							else
								// non => time out
								nRes = ERREUR_COM_BUFFER_VIDE;
							}
						else
							nRes |= nDebloqueErreur (hdl);
						} // ERROR_IO_PENDING
						break;

//					case ERROR_IO_ABORTED:
						// oui => termine sur erreur
//						nRes |= nDebloqueErreur (hdl);
//						break;

					default:
						nRes = ERREUR_COM_SYSTEME | nDebloqueErreur (hdl);
						break;
					}

				} // else if (ReadFile (hdl, pc, 1, &nb_lus, &ovlReception))
			} // if (ovlReception.hEvent)
		else
			nRes = ERREUR_COM_SYSTEME;
		} // if (bComOuverte (uCanal))
  return (ERREUR_COM)nRes;
  } // nComLireCar

// --------------------------------------------------------------------------
//  R�ception de N caract�res avec crit�res de sortie
//  MODE_COM_TIME_OUT_IC :	sortie par time_out inter caracteres => retour = 0
//  MODE_COM_CAR_CONTROLE : sortie par caractere de controle => retour = 0
//  MODE_COM_NBRE_CAR :			sortie par nombre de caracteres recus => retour = 0
//  Renvoie  R�sultat OU erreur_parite OU erreur_encadrement OU erreur_overrun
// --------------------------------------------------------------------------
ERREUR_COM WINAPI nComLire
	(
	UINT uCanal,
	void * pBuffer,										// adresse du buffer de r�ception
	DWORD dwTailleBuffer,							// taille en octets du buffer de r�ception
	FIN_RECEPTION_COM nFinReception,	// MODE_COM_TIME_OUT_IC, MODE_COM_CAR_CONTROLE ou MODE_COM_NBRE_CAR
	LONG lTimeOutPremierCarMs,				// dur�e max d'attente du premier caract�re (en millisecondes)
	LONG lTimeOutCarsSuivantsMs,			// dur�e max d'attente de chaque caract�re suivant (en millisecondes)
	DWORD *pdwNbLus,									// Nombre de cars re�us (si MODE_COM_NBRE_CAR : � initialiser au Nb de cars attendus)
	CHAR cFinReception								// si MODE_COM_CAR_CONTROLE : car terminant la r�ception
	)
  {
  ERREUR_COM nRes = ERREUR_COM_CANAL_INVALIDE;

	// Canal ouvert et en mode asynchrone ?
	if (bComOuverte (uCanal) && aVoiesCom[uCanal].bReceptionAsync)
		{
		DWORD   nb_lus = 0;
		BOOL		bFinReception = FALSE;
		CHAR *	pcReception = (CHAR *) pBuffer;
		CHAR    cLu;
		ERREUR_COM nErreurCom;

		// lecture du premier car avec time out = lTimeOutPremierCarMs
		nRes = ERREUR_COM_OK;
		MajTimeOutEcoute (uCanal, lTimeOutPremierCarMs);
		nErreurCom = nComLireCar (uCanal, &cLu);

		// time out = lTimeOutCarsSuivantsMs
		MajTimeOutEcoute (uCanal, lTimeOutCarsSuivantsMs);
		switch (nFinReception)
			{
			case MODE_COM_TIME_OUT_IC:
				{
				do
					{
					// car lu Ok ?
					if (nErreurCom == 0)
						{
						// oui => ajoute le
						if (nb_lus <= dwTailleBuffer)
							{
							*pcReception = cLu;
							pcReception++;
							nb_lus++;
							nErreurCom = nComLireCar (uCanal, &cLu);
							}
						else
							nErreurCom = ERREUR_COM_OVERRUN;
						}
					else
						{
						// non => termine
						bFinReception = TRUE;
						if (nErreurCom == ERREUR_COM_BUFFER_VIDE)
							{
							if (nb_lus == 0)
								nRes = ERREUR_COM_TIME_OUT; //rien re�u
							}
						else
							{
							nRes = nErreurCom;
							}
						}
					}  while (!bFinReception);

				*pdwNbLus = nb_lus;
				}
				break;

			case MODE_COM_CAR_CONTROLE:
				{
				do
					{
					if (nErreurCom == 0)
						{
						if (nb_lus <= dwTailleBuffer)
							{
							*pcReception = cLu;
							pcReception++;
							nb_lus++;
							if (cLu != cFinReception)
 								nErreurCom = nComLireCar (uCanal, &cLu);
							else
  							bFinReception = TRUE;
							}
						else
							nErreurCom = ERREUR_COM_OVERRUN;
						}
					else
						{
						bFinReception = TRUE;
						if (nErreurCom == ERREUR_COM_BUFFER_VIDE)
							nRes = ERREUR_COM_TIME_OUT;
						else
							nRes = nErreurCom;
						}
					}  while (!bFinReception);

				*pdwNbLus = nb_lus;
				}
				break;

			case MODE_COM_NBRE_CAR:
				{
				do
					{
					if (nErreurCom == 0)
						{
						if (nb_lus <= dwTailleBuffer)
							{
							*pcReception = cLu;
							pcReception++;
							nb_lus++;
							if (nb_lus != *pdwNbLus)
								nErreurCom = nComLireCar (uCanal, &cLu);
							else
								bFinReception = TRUE;
							}
						else
							nErreurCom = ERREUR_COM_OVERRUN;
						}
					else
						{
						bFinReception = TRUE;
						if (nErreurCom == ERREUR_COM_BUFFER_VIDE)
							nRes = ERREUR_COM_TIME_OUT;
						else
							nRes = nErreurCom;
						}
					}  while (!bFinReception);
				*pdwNbLus = nb_lus;
				}
				break;
			} // switch (nFinReception)
		} // if (bComOuverte (uCanal) && aVoiesCom[uCanal].bReceptionAsync)
  return nRes;
  } // nComLire

// --------------------------------------------------------------------------
// Emission de N caract�res : bloquant tant que tous les caract�res ne sont pas �mis
//	time out 1s en cas de probleme en emission
// ! renvoie une erreur OS (seulement compar�e � 0 dans Pcs)
// --------------------------------------------------------------------------
UINT WINAPI uComEcrire 
	(
	UINT uCanal,
	void * pBuffer, DWORD dwNbOctetsAEcrire,
	BOOL bAttenteFin
	)
  {
  UINT   uRes = ERROR_GEN_FAILURE;

	if (bComOuverte (uCanal))
		{
		DWORD dwTimeOutWrite;
		PVOIE_COM pVoieCom = &aVoiesCom[uCanal];
		uRes = 0;

		dwTimeOutWrite = ((dwNbOctetsAEcrire * 1000) / pVoieCom->dwNbCaractereParSeconde) + TIME_OUT_WRITE; // calcul de la duree de la trame d'emission en ms+ epsilon
		MajTimeOutParle (uCanal, dwTimeOutWrite);

		// Arme l'attente d'�mission ?
		if (ResetEvent (pVoieCom->ovlEmission.hEvent))
			{
			DWORD  nb_ecrits;
			HANDLE hdl = pVoieCom->hCanal;
			if (WriteFile (hdl, pBuffer, dwNbOctetsAEcrire, &nb_ecrits, &pVoieCom->ovlEmission))
				{
				if (nb_ecrits != dwNbOctetsAEcrire)
					{
					if (GetOverlappedResult (hdl, &pVoieCom->ovlEmission, &nb_ecrits, TRUE))
						{
						// oui => tout est �crit ?
						if (nb_ecrits == dwNbOctetsAEcrire)
							uRes = NO_ERROR;
						else
							uRes = ERROR_BAD_LENGTH;  // $$ mauvais choix d'erreur
						}
					else
						{
						// non => une erreur est survenue $$ ??
						uRes |= nDebloqueErreur (hdl); //$$ pas une erreur syst�me
						}
					}
				else
					{
					uRes = NO_ERROR;
					}
				}
			else
				{
				DWORD	dwErrEmission = GetLastError();

				switch (dwErrEmission)
					{
					case ERROR_IO_PENDING:
						{
						// Emssion en cours => attente infinie de la fin d'�mission (soumise � time out)
						if (GetOverlappedResult (hdl, &pVoieCom->ovlEmission, &nb_ecrits, TRUE))
							{
							// oui => tout est �crit ?
							if (nb_ecrits == dwNbOctetsAEcrire)
								uRes = NO_ERROR;
							else
								uRes = ERROR_BAD_LENGTH;  // $$ mauvais choix d'erreur
							}
						else
							{
							// non => une erreur est survenue $$ ??
							uRes |= nDebloqueErreur (hdl);
							}
						} // if (GetLastError () ==  ERROR_IO_PENDING)
						break;

					default :
						nDebloqueErreur (hdl);
						uRes = dwErrEmission;
						break;
					} // switch (dwErrEmission)
				} 
			}
		else
			uRes = GetLastError();
		}
  return uRes;
  }

// --------------------------------------------------------------------------
//                         RESET - FERMETURE - TEST
// --------------------------------------------------------------------------

// --------------------------------------------------------------------------
// Vide les caract�res �ventuellement re�us dans un canal ouvert
// Renvoie TRUE si tout est Ok
BOOL WINAPI bComVideReception (UINT uCanal)
  {
  BOOL	bRes = FALSE;

	if (bComOuverte (uCanal))
		{
		DWORD	dwComErr = 0;
		COMSTAT comstat;

		// R�ception synchrone
		if (ClearCommError (aVoiesCom[uCanal].hCanal,  &dwComErr, &comstat)
			&& PurgeComm (aVoiesCom[uCanal].hCanal, PURGE_RXABORT | PURGE_RXCLEAR))
			{
			bRes = TRUE;
			}
		}
  return bRes;
  }

// --------------------------------------------------------------------------
// Test d'acc�s � un canal (qui ne doit pas �tre ouvert)
// R�sultat test� si != 0 dans Pcs
ERREUR_COM WINAPI nComTestCanal (UINT uCanal)
  {
  ERREUR_COM nRes = nComOuvre (uCanal, FALSE, 9600, 8, 1, 1);

	if (nRes == 0)
		nRes = nComFerme (uCanal);

  return nRes;
  }

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%
//		Gestion des signaux
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%

// --------------------------------------------------------------------------
// Fixe l'�tat de la broche RTS
BOOL WINAPI bComEtablisRts (UINT uCanal, BOOL bSignalOn)
  {
	BOOL bRes = FALSE;

	if (bComOuverte (uCanal))
		{
		if (EscapeCommFunction (aVoiesCom[uCanal].hCanal, bSignalOn ? SETRTS : CLRRTS))
			bRes = TRUE;
		}
	return bRes;
  }

// --------------------------------------------------------------------------
// r�cup�re l'�tat des signaux du Modem (voir la signification des bits)
BOOL WINAPI bComEtatModem (UINT uCanal, DWORD *pEtat)
  {
  BOOL	bRes = FALSE;
	if (bComOuverte (uCanal))
		{
		// R�cup�re l'�tat des signaux de contr�le de Modem
		if (GetCommModemStatus (aVoiesCom[uCanal].hCanal, pEtat))
			bRes = TRUE;
		}
  return bRes;
  }

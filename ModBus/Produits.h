// Produits.h

// D�finitions de constantes caract�ristiques des diff�rents mod�les de dongle
#define modele_delos             1
#define modele_argos1            2
#define modele_argos2            3
#define modele_argos3            4
#define modele_microphar1        5
#define modele_microphar2        6
#define modele_SentinelSuperPro1 7

// D�finitions de constantes caract�ristiques des diff�rents produits de LI
// (Les produits doivent �tre num�rot� de mani�re cons�cutive)
#define PRODUIT_FIRST            1  //Num�ro du 1� produit
#define PRODUIT_PROCESSYN_DOS    1
#define PRODUIT_PROCESSYN_OS2    2
#define PRODUIT_PROCESSYN_OS2PM  3
#define PRODUIT_PROCESSYN_WIN32  4
#define PRODUIT_DLL_WIN32        5
#define PRODUIT_SERIZ_2G_MEXICO  6
#define PRODUIT_SERIZ_2G         7
#define PRODUIT_SERVEUR_OPC      8
#define PRODUIT_LAST             8  //Num�ro du dernier produit 

// D�finitions de constantes caract�ristiques des diff�rentes fonctions pour le produit processyn
#define FONCTION_PCS_COMPATIBLE_DELOS  1
#define FONCTION_PCS_ENSEIGNEMENT      2
#define FONCTION_PCS_SD1               3
#define FONCTION_PCS_NORMAL            4
#define FONCTION_PCS_RUN_TIME          5
#define FONCTION_PCS_MAINTENANCE       6
#define FONCTION_PCS_NANO              7
#define FONCTION_PCS_MICRO             8
#define FONCTION_PCS_SD2               9
#define FONCTION_PCS_PARAMETRAGE      10
#define FONCTION_PCS_MICRO_RUNTIME    11
#define FONCTION_PCS_DEPANNAGE        12

// D�finitions de constantes caract�ristiques des diff�rentes fonctions pour les DLL WIN32
#define FONCTION_DLL_WIN32_MODBUS     101 
//      autres dll de communication ... 

// D�finitions de constantes caract�ristiques des diff�rentes fonctions pour SERIZ
#define FONCTION_SERIZ_2G  201 
//      autres fonctions de seriz

// D�finitions de constantes caract�ristiques des diff�rentes fonctions des serveurs OPC
#define FONCTION_SERVEUR_OPC_STANDARD  301 
//      autres fonctions des serveurs OPC

// Renvoie la chaine de caract�re associ�e � un mod�le de cl�
PCSTR pszNomModele (UINT uNModele);
// Renvoie la chaine de caract�re associ�e � un produit
PCSTR pszNomProduit (UINT uNProduit);
// Renvoie la chaine de caract�re associ�e � un produit
PCSTR pszNomFonction (UINT uNFonction);
// Renvoie la chaine de caract�re associ�e � un entier version
PSTR pszVersion (UINT uNVersion, PSTR szRet);

// Renvoie le num�ro de cl� correspondant aux param�tres
DWORD CalculNumeroLi (DWORD wNumeroSerie, DWORD wNumeroVersion, DWORD dwProduit, DWORD dwFonction, 
											DWORD dwNbVariables, DWORD dwNbHeures, DWORD dwDateLimite);

//-------------------------------------------------------------
// DllModbus.c		Corps de la DLL Modbus
// Version WIN32	25/3/97
//-------------------------------------------------------------
#include "stdafx.h"
#include "Modbus.h"
#include "UCom.h"
#include "SoftKey.h"
#include "Produits.h"
#include "Rand.h"
#include "DllModbus.h"
#include "LiSentinel.h"

#ifndef _DLL_MODBUS_COMPILE
  #error Ce module doit �tre compil� en d�clarant la constante _DLL_MODBUS_COMPILE
#endif

// Variables locales (instanci�es dans l'espace d'adresse de chaque process appelant
static const char szPathKey [] = "SOFTWARE\\Logique Industrie\\DLLs\\1.00";
static const char szNomKey [] = "SoftKey";
static HINSTANCE	hInst;

static ERREUR_COM  (WINAPI *pComOuvre) (UINT uCanal, BOOL bReceptionAsync, UINT vitesse, UINT nb_de_bits, UINT parite, UINT encadrement) = NULL;

// --------------------------------------------------------------------------
// Fonction Ouverture d'un canal de com avec cl� interdite
// --------------------------------------------------------------------------
static ERREUR_COM WINAPI nComOuvreInterdit 
	(UINT uCanal, 
	BOOL bReceptionAsync,
	UINT vitesse, UINT nb_de_bits, UINT parite, UINT encadrement)
  {
	ERREUR_COM nRes = (ERREUR_COM)ERREUR_MODBUS_CLE;

	UNREFERENCED_PARAMETER (uCanal);
	UNREFERENCED_PARAMETER (bReceptionAsync);
	UNREFERENCED_PARAMETER (vitesse);
	UNREFERENCED_PARAMETER (nb_de_bits);
	UNREFERENCED_PARAMETER (parite);
	UNREFERENCED_PARAMETER (encadrement);

	Sleep (300);
  return nRes;
  } // nComOuvreInterdit

//---------------------------------------------------------------------------
// Ouverture d'un canal pour utilisation Modbus
__DLL ERREUR_MODBUS WINAPI nModbusOuvre 
	(
	UINT uCanal,					// num�ro de COM de 1 � NB_MAX_VOIE
	UINT vitesse,					// en Bauds
	UINT nb_de_bits,			// 1 = un stop bit, 2 = 2 stops autre = erreur
	UINT parite,					// 
	UINT encadrement			//
	) // Renvoie le code d'erreur
	{
	ERREUR_COM	nErr = pComOuvre (uCanal, TRUE, vitesse, nb_de_bits, parite, encadrement);

	return (ERREUR_MODBUS)nErr;
	} // nModbusOuvre

// --------------------------------------------------------------------------------------
// Fermeture d'un canal ouvert
__DLL ERREUR_MODBUS WINAPI nModbusFerme (UINT uCanal)
	{
	return (ERREUR_MODBUS)nComFerme (uCanal);
	} // nModbusFerme

//-------------------------------------------------------------------------
// Question - r�ponse Modbus �l�mentaire sur un canal ouvert
// Le poste est maitre et l'automate est esclave
__DLL MODBUS_ERR WINAPI nEchangeModbusMaitre 
	(
	DWORD canal,									// canal ouvert
	MODBUS_F fonction,						// Fonction � faire ex�cuter par l'automate
	DWORD nombre,									// Nombre de variables � �changer
	PBUFFER_MODBUS table_modbus,	// Adresse des variables � �changer
	DWORD esclave,								// Num�ro d'esclave de l'automate cible
	MODBUS_AUTOMATE tipe_esclave,	// Type de comportement de l'automate cible
	DWORD adresse,								// Adresse de la premi�re variable adress�e
	DWORD time_out,								// Dur�e du time out en millisecondes
	DWORD retry,									// Nombres d'essais de transmissions en cas d'erreur
	MODBUS_VAR genre_num					// Type de donn�es �chang�es si format non bit
	)
	{
	return nEchangeModbus (canal, fonction, nombre, table_modbus, 
		esclave, tipe_esclave, adresse, time_out, retry,genre_num);
	}



//---------------------------------------------------------------------------
// Initialisation de la DLL pour un process
static BOOL bAttacheDll(HINSTANCE hinstance)
	{
	// M�morisation du hInst
	hInst = hinstance;

	// Quelques v�rifications sur l'homog�n�it� des sources
	if ((NB_MAX_VOIE_MODBUS != NB_MAX_VOIE) || 
		(ERREUR_MODBUS_TIME_OUT_INTER_CAR != ERREUR_COM_TIME_OUT_INTER_CAR))
		return FALSE;

// Commentariser la ligne suivante pour laisser la protection de cette DLL
//#define MODBUS_SANS_PROTECTION 1
	
#ifdef MODBUS_SANS_PROTECTION
#pragma message( "***** Attention !!! MODBUS_SANS_PROTECTION ***** (fichier " __FILE__ ")") 

	pComOuvre = nComOuvre;

#else

	pComOuvre = nComOuvreInterdit;
	if (dwGetNumeroCle (szPathKey, szNomKey) != 
		get_rand_OS_WIN32 (dwGetIdMachine (), 100, PRODUIT_DLL_WIN32, FONCTION_DLL_WIN32_MODBUS, 0,0,0,0,0,0,4))
		{
		if (InitSentinel(DEV_ID))
			{
			DWORD  Valeur = 0;
			SentinelSuperProAcces (LECTURE_CLE,REGISTRE_VERSION,&Valeur,0,0,0);
			if (Valeur == 100)
				{	
				SentinelSuperProAcces(LECTURE_CLE,REGISTRE_TYPE_PRODUIT,&Valeur,0,0,0);
				if (Valeur == PRODUIT_DLL_WIN32)
					{
					SentinelSuperProAcces (LECTURE_CLE,REGISTRE_FONCTION,&Valeur,0,0,0);
					if (Valeur == FONCTION_DLL_WIN32_MODBUS)
						pComOuvre = nComOuvre;
					}
				}
			}
		
		}
	else
		pComOuvre = nComOuvre;

#endif
	
	return TRUE;
	}

//---------------------------------------------------------------------------
// Contr�le de la DLL par Windows.
//---------------------------------------------------------------------------
BOOL APIENTRY DllMain (HINSTANCE hinstance, DWORD dwReason, LPVOID lpReserved)
	{
	BOOL	bOk = TRUE;

	switch (dwReason)
		{
		case DLL_PROCESS_ATTACH:
			// DLL mapp�e dans l'espace d'adresse du process :
			bOk = bAttacheDll(hinstance);
			break;

		// Pas de traitement par thread dans la mesure ou seul le thread code acc�de � cette DLL
		//case DLL_THREAD_ATTACH:  break;// un thread est cr�� break;
		//case DLL_THREAD_DETACH: break;// un thread se termine proprement : Cf DLL_THREAD_ATTACH

		case DLL_PROCESS_DETACH:
			// La DLL est d� - mapp�e de l'espace d'adresse du process :

			// reset du hInst
			hInst = 0;
			break;
		}
	return bOk;
	}

//------------------------- fin ----------------------------------


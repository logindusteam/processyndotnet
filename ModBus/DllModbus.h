// DllModbus.h
// Interface de DLL Modbus WIN32 pour Windows 95 et Windows NT 4.0

// L'utilisateur de la DLL ne doit PAS d�finir DLL_COMPILE
#ifdef _DLL_MODBUS_COMPILE
	#define	__DLL __declspec (dllexport)
#else
	#define __DLL WINBASEAPI
#endif


// Un num�ro de canal x va de 1 � NB_MAX_VOIE et d�signe le device 'COMx' corespondant
#define NB_MAX_VOIE_MODBUS      36

// Valeurs d'erreur pour nModbusOuvre et nModbusFerme (0 = NO_ERROR = pas d'erreur)
typedef enum
	{
	ERREUR_MODBUS_PARITE            =   1,
	ERREUR_MODBUS_ENCADREMENT       =   2,
	ERREUR_MODBUS_OVERRUN           =   4,
	ERREUR_MODBUS_BUFFER_VIDE       =   8,
	ERREUR_MODBUS_CANAL_INVALIDE    =  16,
	ERREUR_MODBUS_TIME_OUT          =  32,
	ERREUR_MODBUS_TIME_OUT_INTER_CAR=  64,
	ERREUR_MODBUS_SYSTEME           = 128,
	ERREUR_MODBUS_CLE		        = 256	// DLL non d�v�rouill�e sur ce poste
	} ERREUR_MODBUS;


//---------------------------------------------------------------------------
// Ouverture d'un canal pour utilisation Modbus
__DLL ERREUR_MODBUS WINAPI nModbusOuvre 
	(
	UINT uCanal,					// num�ro de COM de 1 � NB_MAX_VOIE
	UINT vitesse,					// en Bauds (ex: 9600)
	UINT nb_de_bits,				// 5, 6, 7 ou 8 (selon les capacit�s du driver)
	UINT parite,					// 0 = sans, 1 = impaire, 2 = paire
	UINT encadrement				// 1 = 1 stop bit, 2 = 2 stop bit, autre = erreur
	); // Renvoie le code d'erreur

// --------------------------------------------------------------------------------------
// Fermeture d'un canal pr�c�demment ouvert
__DLL ERREUR_MODBUS WINAPI nModbusFerme (UINT uCanal);

// --------------------------------------------------------------------------------------
// D�finitions concernant les �changes Modbus
#ifndef MODBUS_H
#define MODBUS_H

// types d'automates (permet d'effectuer certaines conversions. (Par exemple inversion mot poids fort-
// poids faible pour les r�els sur les automates de type GOULD).
typedef enum
	{
	MODBUS_AUTOMATE_SMC       = 0,
	MODBUS_AUTOMATE_PB        = 1,
	MODBUS_AUTOMATE_TELE      = 2,
	MODBUS_AUTOMATE_GOULD     = 3,
	MODBUS_AUTOMATE_SIEMENS   = 4,
	MODBUS_AUTOMATE_ALSPA     = 5,
	MODBUS_AUTOMATE_F15_F16   = 6,
	MODBUS_AUTOMATE_SMCX00    = 7
	} MODBUS_AUTOMATE;

// fonctions Modbus
typedef enum
	{
	MODBUS_F_LECTURE_BITS_SORTIE  = 1,
	MODBUS_F_LECTURE_BITS_ENTREE  = 2,
	MODBUS_F_LECTURE_MOTS_SORTIE  = 3,
	MODBUS_F_LECTURE_MOTS_ENTREE  = 4,
	MODBUS_F_ECRITURE_BIT		  = 5,
	MODBUS_F_ECRITURE_MOT		  = 6,
	MODBUS_F_ECRITURE_N_BITS	  = 15,
	MODBUS_F_ECRITURE_N_MOTS	  = 16
	} MODBUS_F;

// erreurs EchangeModbus
typedef enum
	{
	MODBUS_ERR_OK					= 0x0000,
	MODBUS_ERR_AUTOMATE		= 0x0008,
	MODBUS_ERR_TIME_OUT		= 0x0010,
	MODBUS_ERR_RECEPTION	= 0x0020,
	MODBUS_ERR_CRC				= 0x0040,
	MODBUS_ERR_N_FONCTION	= 0x0080,
	MODBUS_ERR_EMISSION		= 0x8000 
	MODBUS_ERR_OUVERTURE	= 0xFFFF, 
	MODBUS_ERR_FERMETURE	= 0xFFFF // $$ cochon
	} MODBUS_ERR;

// Buffer de variables � �changer avec l'automate
// La DLL effectue la conversion entre le type des tables et la trame �mise
typedef union
  {
  BOOL	table_log [2048];         // BOOL  = bit MODBUS
  int	table_num_entier [128];   // int   = mot MODBUS 
  DWORD table_num_mot [128];      // DWORD = mot MODBUS
  FLOAT table_num_reel [64];      // REEL  = 2 mots MODBUS 
  char	table_car[256];           // 1 caract�re = 1 octet MODBUS
  } BUFFER_MODBUS, *PBUFFER_MODBUS;

// Types de variables � �changer avec l'automate
typedef enum
	{
	MODBUS_VAR_LOG,
	MODBUS_VAR_ENTIER,
	MODBUS_VAR_REEL,
	MODBUS_VAR_MOT,
	MODBUS_VAR_MESSAGE
	} MODBUS_VAR;
#endif

//-------------------------------------------------------------------------
// Question - r�ponse Modbus �l�mentaire sur un canal ouvert
// Le poste est maitre et l'automate est esclave
__DLL MODBUS_ERR WINAPI nEchangeModbusMaitre 
	(
	DWORD canal,					// canal ouvert
	MODBUS_F fonction,				// Fonction MODBUS � faire ex�cuter par l'automate
	DWORD nombre,					// Nombre de variables � �changer (bit, mot, r�el ou nombre de (caractere/2))
	PBUFFER_MODBUS	table_modbus,	// Adresse de la table des variables � �changer
	DWORD esclave,					// Num�ro d'esclave de l'automate cible
	MODBUS_AUTOMATE tipe_esclave,	// Type de comportement de l'automate cible
	DWORD adresse,					// Adresse MODBUS de la premi�re variable 
	DWORD time_out,					// Dur�e du time out en millisecondes
	DWORD retry,					// Nombres d'essais de transmissions en cas d'erreur
	MODBUS_VAR genre_var			// Type de donn�es �chang�es
	);


//-------------------------- fin DllModbus.h ---------------------

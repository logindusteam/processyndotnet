// Modbus.c
// Gestion du protocole Modbus WIN32 27/3/96

#include "stdafx.h"
#include "UCom.h"

#include "Modbus.h"

#define LONGUEUR_MAX_TRAME_MODBUS 262

//-------------------------------------------------------------------------
// calcul du CRC 16 bits Fa�on Modbus d'un buffer
static WORD wModbusCRC (void *buffer, DWORD longueur)
	{
	// � partir du premier octet du buffer
	PBYTE pBuf = (PBYTE)buffer;
	WORD wCRC = 0xffff;

	// pour chaque octet
	for (; longueur; longueur --)
		{
		int nTraite;

		// prise en compte de l'octet dans le CRC
		wCRC ^= *pBuf;
		for (nTraite = 0; nTraite < 8; nTraite++)
			{
			if (wCRC & (WORD)1)
				{
				wCRC >>= 1;
				wCRC ^= 0xa001;
				}
			else
				wCRC >>= 1;
			}
		// octet suivant
		pBuf++;
		}
	return wCRC;
	}

//-------------------------------------------------------------------------
// Emission d'une trame Modbus
static void ModbusEmission (DWORD canal, DWORD *pdwStatutModbus, DWORD *taille_modbus, BYTE *pabTrameModbus)
  {
  WORD crc;
  BYTE *ptr_byte;

  crc = wModbusCRC (pabTrameModbus, (*taille_modbus));
  ptr_byte = (BYTE *)&crc;
  pabTrameModbus [(*taille_modbus)] = (BYTE)crc;
  (*taille_modbus)++;
  pabTrameModbus [(*taille_modbus)] = HIBYTE (crc);
  (*taille_modbus)++;
  bComVideReception (canal);
  if (uComEcrire (canal, pabTrameModbus, (*taille_modbus), TRUE) != 0)
    {
    *pdwStatutModbus = MODBUS_ERR_EMISSION;
    }
  } // ModbusEmission

//-------------------------------------------------------------------------
static BOOL code_fonction_ok (DWORD fonction, DWORD *taille, BYTE *pabTrameModbus, DWORD *pdwStatutModbus)
  {
  BOOL bretour;

  if ((*taille) >= 3)
    {
    if (pabTrameModbus [1] != (BYTE)(fonction) )
      {
      bretour = FALSE;
      if (pabTrameModbus [1] == (BYTE)(fonction + 128) )
        {
        *pdwStatutModbus |= ((WORD)(pabTrameModbus [2]) * 256);
        *pdwStatutModbus |= MODBUS_ERR_AUTOMATE;
        }
      else
        {
        *pdwStatutModbus |= MODBUS_ERR_RECEPTION;
        }
      }
    else
      {
      bretour = TRUE;
      }
    }
  else
    {
    bretour = FALSE;
    *pdwStatutModbus |= MODBUS_ERR_RECEPTION;
    }
  return bretour;
  }

//-------------------------------------------------------------------------
// Reception d'une trame Modbus
static BOOL bModbusReception (DWORD canal, DWORD fonction, DWORD *taille_attendue, 
	DWORD *pdwStatutModbus, BYTE *pabTrameModbus, DWORD time_out, DWORD tipe_esclave)
  {
  LONG time_out_trame = time_out;
  LONG time_out_car = time_out;  // arbitraire
  int statut_phone;
  BOOL trame_ok;
  WORD ctrl_crc;
  DWORD wtempo;

  statut_phone =  nComLire (canal, pabTrameModbus, LONGUEUR_MAX_TRAME_MODBUS, MODE_COM_NBRE_CAR, 
		time_out_trame, time_out_car, taille_attendue, ' ');
  if (statut_phone == 0)
    {
    ctrl_crc = wModbusCRC (pabTrameModbus, (*taille_attendue) - 2);
    if (pabTrameModbus [(*taille_attendue) - 2] != (BYTE)ctrl_crc)
      {
      *pdwStatutModbus |= MODBUS_ERR_CRC;
      trame_ok = FALSE;
      }
    else
      {
      if (pabTrameModbus [(*taille_attendue)-1]  != HIBYTE (ctrl_crc))
        {
        *pdwStatutModbus |= MODBUS_ERR_CRC;
        trame_ok = FALSE;
        }
      else
        {
        trame_ok = code_fonction_ok (fonction, taille_attendue, pabTrameModbus, pdwStatutModbus);
        }
      }
    }
  else
    {
    trame_ok = FALSE;
    switch (statut_phone)
      {
      case ERREUR_COM_CANAL_INVALIDE:
      case ERREUR_COM_PARITE:
      case ERREUR_COM_ENCADREMENT:
      case ERREUR_COM_OVERRUN:
        *pdwStatutModbus |= MODBUS_ERR_RECEPTION;
        *pdwStatutModbus |= (DWORD) (statut_phone);
        break;
      case ERREUR_COM_TIME_OUT:
        *pdwStatutModbus |= MODBUS_ERR_TIME_OUT;
        break;
      case ERREUR_COM_TIME_OUT_INTER_CAR:
        code_fonction_ok (fonction, taille_attendue, pabTrameModbus, pdwStatutModbus);
        wtempo = (*pdwStatutModbus) & MODBUS_ERR_AUTOMATE;
        if (wtempo == 0)
          {
          *pdwStatutModbus |= MODBUS_ERR_TIME_OUT;
          }
        break;
      case ERREUR_COM_SYSTEME:
        *pdwStatutModbus |= MODBUS_ERR_RECEPTION;
        break;
      default:
        *pdwStatutModbus |= MODBUS_ERR_RECEPTION;
        break;
      }
    }

  if ((tipe_esclave == MODBUS_AUTOMATE_SMCX00) && (trame_ok))
    // Tempo de 5 ms pour la serie 1000 APRIL
		// $$ devrait �tre le temps de transmission de 3,5 car
		Sleep (5);

  return trame_ok;
  } // bModbusReception

//-------------------------------------------------------------------------
// Question - r�ponse Modbus �l�mentaire sur un canal ouvert
// Le poste est maitre et l'automate est esclave
MODBUS_ERR nEchangeModbus 
	(
	DWORD canal,									// canal ouvert
	MODBUS_F fonction,						// Fonction MODBUS � faire ex�cuter par l'automate
	DWORD nombre,									// Nombre de variables � �changer(bit, mot, r�el ou nombre de (caractere/2))
	PBUFFER_MODBUS	table_modbus,	// Adresse de la table des variables � �changer
	DWORD esclave,								// Num�ro d'esclave de l'automate cible
	MODBUS_AUTOMATE tipe_esclave,	// Type de comportement de l'automate cible
	DWORD adresse,								// Adresse MODBUS de la premi�re variable adress�e
	DWORD time_out,								// Dur�e du time out en millisecondes
	DWORD retry,									// Nombres d'essais de transmissions en cas d'erreur
	MODBUS_VAR genre_num					// Type de donn�es �chang�es
	)
  {
  DWORD statut_modbus;
  DWORD taille_attendue;
  DWORD val_ecr;
  DWORD bit;
  DWORD taille_modbus;
  DWORD dwNombreQuestion;
  DWORD nombre_lu;
  DWORD ptr;
  BYTE abTrameModbus[LONGUEUR_MAX_TRAME_MODBUS];
  BOOL dialogue_ok;
  FLOAT reel;
  BYTE *ptr_reel;
  int i;
  int j;

  dialogue_ok = TRUE;
  dwNombreQuestion = 0;
  do
    {
    dwNombreQuestion++;
    statut_modbus = MODBUS_ERR_OK;
    abTrameModbus [0] = (BYTE) (esclave);
    abTrameModbus [1] = (BYTE) (fonction);
    abTrameModbus [2] = HIBYTE (adresse);
    abTrameModbus [3] = (BYTE) (adresse);

		// Traitement selon la fonction modbus demand�e
    switch (fonction)
      {
      case MODBUS_F_LECTURE_BITS_SORTIE:
      case MODBUS_F_LECTURE_BITS_ENTREE:
        abTrameModbus [4] = HIBYTE (nombre);
        abTrameModbus [5] = (BYTE) (nombre);
        taille_modbus = 6;
        ModbusEmission (canal, &statut_modbus, &taille_modbus, abTrameModbus);
        taille_attendue = ((nombre - 1) / 8) + 6;  //////// 6 <=> 1 + 5 intellectuel non ??
        if (bModbusReception (canal, fonction, &taille_attendue, &statut_modbus,
                        abTrameModbus, time_out, tipe_esclave))
          { // recu OK
          dialogue_ok = TRUE;
          nombre_lu = abTrameModbus [2];
          for (i = 3; (i <= (int)(nombre_lu + 2)); i++)
            { // for (... octet
            for (j = 0; (j <= 7); j++)
              { // for (... bit
              table_modbus->table_log [((i - 3) * 8) + j] = (BOOL)((abTrameModbus [i] & 1) == 1);
              abTrameModbus [i] = (BYTE)(abTrameModbus [i] >> 1);
              }  // for (... bit
            }  // for (... octet
          }// recu OK
        else
          {
          dialogue_ok = FALSE;
          }
        break; // lecture bit

      case MODBUS_F_LECTURE_MOTS_SORTIE:
      case MODBUS_F_LECTURE_MOTS_ENTREE:
        switch (genre_num)
          {
					case MODBUS_VAR_ENTIER:
          case MODBUS_VAR_MOT:
          case MODBUS_VAR_MESSAGE:
						{
            abTrameModbus [4] = HIBYTE (nombre);
            abTrameModbus [5] = (BYTE) (nombre);
            taille_modbus = 6;
            ModbusEmission (canal, &statut_modbus, &taille_modbus, abTrameModbus);
            taille_attendue = ((nombre * 2) + 5);
            if (bModbusReception (canal, fonction ,&taille_attendue,
                            &statut_modbus, abTrameModbus, time_out, tipe_esclave))
              { // recu OK
              dialogue_ok = TRUE;
              ptr = 3;
							switch (genre_num)
								{
								case MODBUS_VAR_ENTIER:
									for (i = 0; (i < (int)nombre); i++)
										{ // for (...
										table_modbus->table_num_entier [i] = (int) (short)((abTrameModbus [ptr] * 256) + abTrameModbus [ptr + 1]);
										ptr = ptr + 2;
										}  // for 
									break;
								case MODBUS_VAR_MOT:
									for (i = 0; (i < (int)nombre); i++)
										{ // for (...
										table_modbus->table_num_mot [i] = (WORD) ((abTrameModbus [ptr] * 256) + abTrameModbus [ptr + 1]);
										ptr = ptr + 2;
										}  // for 
									break;
								case MODBUS_VAR_MESSAGE:
									for (i = 0; (i < (int) nombre); i++)
										{
										table_modbus->table_car [(2*i)+1] = (char) abTrameModbus [ptr];
										ptr++;
										table_modbus->table_car [2*i] = (char) abTrameModbus [ptr];
										ptr++;
										}  // for (i ... octet
									break;
								}

             }// recu OK
            else
              {
              dialogue_ok = FALSE;
              }
						}
            break;

          case MODBUS_VAR_REEL:
            nombre = nombre * 2; // reels sur 4 bytes
            abTrameModbus [4] = HIBYTE (nombre);  // nombre de paires de bytes
            abTrameModbus [5] = (BYTE) (nombre);
            nombre = nombre / 2;
            taille_modbus = 6;
            ModbusEmission (canal, &statut_modbus, &taille_modbus, abTrameModbus);
            taille_attendue = ((nombre * 4) + 5);  // nombre de bytes
            if (bModbusReception (canal, fonction ,&taille_attendue,
                            &statut_modbus, abTrameModbus, time_out, tipe_esclave) )
              { // recu OK
              dialogue_ok = TRUE;
              ptr = 3;
              for (i = 0; (i < (int)nombre); i++) // nbre de reels
                { // for (...
                ptr_reel  = (BYTE *)&reel;
                if (tipe_esclave == MODBUS_AUTOMATE_GOULD)           // GOULD
                  {
                  ptr_reel [1] = abTrameModbus [ptr];
                  ptr++;
                  ptr_reel [0] = abTrameModbus [ptr];
                  ptr++;
                  ptr_reel [3] = abTrameModbus [ptr];
                  ptr++;
                  ptr_reel [2] = abTrameModbus [ptr];
                  ptr++;
                  }
                else                             // Non Gould
                  {
                  for (j = 3; (j >= 0); j--)
                    {
                    ptr_reel [j] = abTrameModbus [ptr];
                    ptr++;
                    }
                  }
                table_modbus->table_num_reel [i] = reel;
                }  // for 
              }// recu OK
            else
              {
              dialogue_ok = FALSE;
              }
            break;

				default:
					statut_modbus = statut_modbus | MODBUS_ERR_N_FONCTION;
					break;

          } // fin switch
        break;

      case MODBUS_F_ECRITURE_BIT:
        if (table_modbus->table_log [0])
          {
          abTrameModbus [4] = 255;
          }
        else
          {
          abTrameModbus [4] = 0;
          }
        abTrameModbus [5] = 0;
        taille_modbus = 6;
        ModbusEmission (canal, &statut_modbus, &taille_modbus, abTrameModbus);
        taille_attendue = 8;
        dialogue_ok = bModbusReception (canal, fonction ,&taille_attendue,
                                   &statut_modbus, abTrameModbus, time_out, tipe_esclave);
        break;

      case MODBUS_F_ECRITURE_MOT:
				switch (genre_num)
					{
					case MODBUS_VAR_ENTIER:
						abTrameModbus [4] = HIBYTE (table_modbus->table_num_entier [0]);
						abTrameModbus [5] = (BYTE) (table_modbus->table_num_entier [0]);
						break;
					case MODBUS_VAR_MOT:
            abTrameModbus [4] = HIBYTE (table_modbus->table_num_mot [0]);
            abTrameModbus [5] = (BYTE) (table_modbus->table_num_mot [0]);
						break;
					default:
						statut_modbus = statut_modbus | MODBUS_ERR_N_FONCTION;
						break;
          }
        taille_modbus = 6;
        ModbusEmission (canal, &statut_modbus, &taille_modbus, abTrameModbus);
        taille_attendue = 8;
        dialogue_ok = bModbusReception (canal, fonction ,&taille_attendue,
                                   &statut_modbus, abTrameModbus, time_out, tipe_esclave);
        break;

      case MODBUS_F_ECRITURE_N_BITS:
        abTrameModbus [4] = HIBYTE (nombre);
        abTrameModbus [5] = (BYTE) (nombre);
        nombre_lu = ((nombre - 1) / 8) + 1;
        abTrameModbus [6] = (BYTE) (nombre_lu);
        ptr = 6;
        for (i = 0; (i < (int)nombre_lu); i++)
          { // for (i ... octet
          val_ecr = 0;
          bit = 1;
          for (j = 0; (j <= 7); j++)
            { // for (bit
            if (table_modbus->table_log [(i * 8) + j])
              {
              val_ecr = val_ecr + bit;
              }
            bit = bit * 2;
            } // for (bit
          ptr++;
          abTrameModbus [ptr] = (BYTE) (val_ecr);
          }  // for (i ... octet
        taille_modbus = ptr+1;
        ModbusEmission (canal, &statut_modbus, &taille_modbus, abTrameModbus);
        taille_attendue = 8;
        dialogue_ok = bModbusReception (canal, fonction ,&taille_attendue,
                                   &statut_modbus, abTrameModbus, time_out, tipe_esclave);
        break;

      case MODBUS_F_ECRITURE_N_MOTS:
        switch (genre_num)
          {
					case MODBUS_VAR_ENTIER:
          case MODBUS_VAR_MOT:
          case MODBUS_VAR_MESSAGE:
						{
						abTrameModbus [4] = HIBYTE (nombre);	// Poids fort Nombre de mots � forcer
						abTrameModbus [5] = (BYTE) (nombre);	// Poids faible Nombre de mots � forcer
						abTrameModbus [6] = (BYTE) (nombre * 2);// Nombre d'octets des valeurs � forcer
						ptr = 7;
						switch (genre_num)
							{
							case MODBUS_VAR_ENTIER:
								for (i = 0; (i < (int)nombre); i++)
									{ // for (i ... octet
									abTrameModbus [ptr] = HIBYTE (table_modbus->table_num_entier [i]);
									ptr++;
									abTrameModbus [ptr] = (BYTE) (table_modbus->table_num_entier [i]);
									ptr++;
									}  // for (i ... octet
								break;
							case MODBUS_VAR_MOT:
								for (i = 0; (i < (int)nombre); i++)
									{ // for (i ... octet
									abTrameModbus [ptr] = HIBYTE (table_modbus->table_num_mot [i]);
									ptr++;
									abTrameModbus [ptr] = (BYTE) (table_modbus->table_num_mot [i]);
									ptr++;
									}  // for (i ... octet
								break;
							case MODBUS_VAR_MESSAGE:
								for (i = 0; (i < (int) nombre); i++)
									{ // for (i ... octet
									abTrameModbus [ptr] = (BYTE) (table_modbus->table_car [(2*i)+1]);
									ptr++;
									abTrameModbus [ptr] = (BYTE) (table_modbus->table_car [2*i]);
									ptr++;
									}  // for (i ... octet
								break;
							} // switch (genre_num)
						taille_modbus = ptr;
						ModbusEmission (canal, &statut_modbus, &taille_modbus, abTrameModbus);
						taille_attendue = 8;
						dialogue_ok = bModbusReception (canal, fonction ,&taille_attendue,
							&statut_modbus, abTrameModbus, time_out, tipe_esclave);
						}
           break;

         case MODBUS_VAR_REEL:
					 {
           nombre = nombre * 2;  // nombre de paires de bytes
           abTrameModbus [4] = HIBYTE (nombre);
           abTrameModbus [5] = (BYTE) (nombre);
           abTrameModbus [6] = (BYTE) (nombre * 2);
           nombre = nombre / 2;  // pour retry
           ptr = 7;
           for (i = 0; (i < (int)nombre); i++)   // de 1 � nbr de reels
             { // for (i
             reel = table_modbus->table_num_reel[i];
             ptr_reel = (BYTE *)&reel;
             if (tipe_esclave == MODBUS_AUTOMATE_GOULD)           // GOULD
               {
               abTrameModbus [ptr] = ptr_reel[1];
               ptr++;
               abTrameModbus [ptr] = ptr_reel[0];
               ptr++;
               abTrameModbus [ptr] = ptr_reel[3];
               ptr++;
               abTrameModbus [ptr] = ptr_reel[2];
               ptr++;
               }
             else                             // Non Gould
               {
              for (j = 3; (j >= 0); j--)
                 {
                 abTrameModbus [ptr] = ptr_reel[j];
                 ptr++;
                 }
               }
             }  // for (i 
           taille_modbus = ptr;
           ModbusEmission (canal, &statut_modbus, &taille_modbus, abTrameModbus);
           taille_attendue = 8;
           dialogue_ok = bModbusReception (canal, fonction ,&taille_attendue,
                                      &statut_modbus, abTrameModbus, time_out, tipe_esclave);
					 }
           break;

					default:
						statut_modbus = statut_modbus | MODBUS_ERR_N_FONCTION;
						break;
          } // fin switch  genre_num
        break;  // ecriture n trucs

      default:
        statut_modbus = statut_modbus | MODBUS_ERR_N_FONCTION;
        break;

      } // switch (fonction)

    } while ((!dialogue_ok) && (dwNombreQuestion < (retry+1)));
  return (MODBUS_ERR)statut_modbus;
  } // nEchangeModbus

//-----------------------------------------------------------------
// Ouverture voie de communication pour dialogue modbus
MODBUS_ERR nOuvertureVoieModbus (
	UINT uCanal,					// num�ro de COM de 1 � NB_MAX_VOIE
	UINT vitesse,					// en Bauds (ex: 9600)
	UINT nb_de_bits,			// 5, 6, 7 ou 8 (selon les capacit�s du driver)
	UINT parite,					// 0 = sans, 1 = impaire, 2 = paire
	UINT encadrement			// 1 = 1 stop bit, 2 = 2 stop bit, autre = erreur
	)
	{
	MODBUS_ERR nRes = MODBUS_ERR_OK;

	if (nComOuvre (uCanal, TRUE, vitesse, nb_de_bits, parite,encadrement) != 0)
		{
		nRes = MODBUS_ERR_OUVERTURE;
		}

	return nRes;
	}

//-----------------------------------------------------------------
// Fermeture voie de communication pour dialogue modbus
MODBUS_ERR nFermetureVoieModbus (
	UINT uCanal					// num�ro de COM de 1 � NB_MAX_VOIE
	)
	{
	MODBUS_ERR nRes = MODBUS_ERR_FERMETURE;

	if (0 == nComFerme (uCanal))
		nRes = MODBUS_ERR_OK;

	return nRes;
	}

// LiSentinel.cpp
// Acces aux cl�s sentinel

#include "stdafx.h"
#include "spromeps.h"

#include "LiSentinel.h"


static RB_SPRO_APIPACKET ApiPacket; 

//----------------------------------------------------------------------------
BOOL InitSentinel (DWORD gDevID)
  {
	SP_STATUS spStatus;
	BOOL bOk;

	bOk = FALSE;
  spStatus = RNBOsproFormatPacket(ApiPacket, sizeof(ApiPacket));
  if (spStatus == SP_SUCCESS)
    {
		spStatus = RNBOsproInitialize (ApiPacket);  
		if (spStatus == SP_SUCCESS)
			{
      spStatus = RNBOsproFindFirstUnit (ApiPacket, (RB_WORD)gDevID);
			bOk = (spStatus == SP_SUCCESS);
			}
		}
	return (bOk);
  }

//----------------------------------------------------------------------------
BOOL SentinelSuperProAcces (DWORD fct, DWORD registre, DWORD *valeur, DWORD PasseWrite, DWORD PasseOverwrite1, DWORD PasseOverwrite2)
  {
  BOOL rretour;
  RB_WORD   wCell;
  RB_WORD   wData;
  SP_STATUS SpStatus;

  switch  (fct)
    {

    case LECTURE_CLE:
      (*valeur) = 0;
			wData = (WORD)*valeur;
			wCell = (WORD)registre;
      SpStatus = RNBOsproRead (ApiPacket, wCell, &wData);
      if (SpStatus != SP_SUCCESS)
        {
        (*valeur) = 0;
        rretour = FALSE;
        }
      else
        {
				*valeur = wData;
        rretour = TRUE;
        }
      break;

    case ECRITURE_CLE:
 			wCell = (WORD)registre;
			wData = (WORD)(*valeur);
      SpStatus = RNBOsproWrite (ApiPacket, (RB_WORD)PasseWrite, wCell, wData, 0);
      rretour = (SpStatus == SP_SUCCESS);
      break;

    case SURECRITURE_CLE:
 			wCell = (WORD)registre;
			wData = (WORD)(*valeur);
      SpStatus = RNBOsproOverwrite (ApiPacket, (RB_WORD)PasseWrite, (RB_WORD)PasseOverwrite1, (RB_WORD)PasseOverwrite2, wCell, wData, 0);
      rretour = (SpStatus == SP_SUCCESS);
      break;

    default:
      (*valeur) = 0;
      rretour = FALSE;
      break;
    }

  return (rretour);
  }
